﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AjustePosicaoComeCotasAvancado.aspx.cs"
    Inherits="CadastrosBasicos_AjustePosicaoComeCotasAvancado" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript">    
		var isCustomCallback = false;   // usada para controlar o refresh após um delete feito no grid
		var operacao = '';
		
    function OnGetDataCliente(data) {
        btnEditCodigoCliente.SetValue(data);        
        callbackCliente.SendCallback(btnEditCodigoCliente.GetValue());
        popupCliente.HideWindow();
        btnEditCodigoCliente.Focus();
    }    		
    
    function OnGetDataPosicao(data) {
        hiddenIdPosicao.SetValue(data);
        callbackPosicao.SendCallback(data);
        popupPosicao.HideWindow();
        btnEditPosicao.Focus();
    }        
    
    function MudaTextoLabel(tipoPosicao)
    {
        if(tipoPosicao == 1)
            labelContraParte.SetText("Carteira");
        else
            labelContraParte.SetText("Cliente");
    }
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <dxcb:ASPxCallback ID="callbackCliente" runat="server" OnCallback="callbackCliente_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) { 
                var textNomeCliente = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCliente');
                OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigoCliente, textNomeCliente);   
                }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                if (operacao == 'salvar')
                {        
                    gridCadastro.UpdateEdit();
                }
                else
                {
                    callbackAdd.SendCallback();
                }  
            } 
            operacao = '';
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackPosicao" runat="server" OnCallback="callbackPosicao_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {    if (e.result != null) 
                                                                    { 
                                                                        var resultSplit = e.result.split('|'); 
                                                                        btnEditPosicao.SetValue(resultSplit[0]);
                                                                        textValorCota.SetValue(resultSplit[1]);
                                                                        textValorCotaAplic.SetValue(resultSplit[2]);
                                                                        textQuantidade.SetValue(resultSplit[3]);
                                                                        textQuantidadeAntesCortes.SetValue(resultSplit[4]);
                                                                        textRendimentoBrutoDesdeAplicacao.SetValue(resultSplit[5]);
                                                                     }
                                                               } " />
        </dxcb:ASPxCallback>
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Ajuste de Posições de Come Cotas (Avançado)"></asp:Label>
                            </div>
                            <div id="mainContent">
                                <div class="linkButton">
                                    <dxpc:ASPxPopupControl ID="popupPosicao" runat="server" HeaderText="" Width="500px"
                                        ContentStyle-VerticalAlign="Top" EnableClientSideAPI="True" PopupVerticalAlign="Middle"
                                        PopupHorizontalAlign="OutsideRight" AllowDragging="True">
                                        <ContentCollection>
                                            <dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                                                <div>
                                                    <dxwgv:ASPxGridView ID="gridPosicao" runat="server" Width="100%" ClientInstanceName="gridPosicao"
                                                        AutoGenerateColumns="False" DataSourceID="EsDSPosicao" KeyFieldName="IdPosicao"
                                                        OnCustomDataCallback="gridPosicao_CustomDataCallback" OnCustomCallback="gridPosicao_CustomCallBack">
                                                        <Columns>
                                                            <dxwgv:GridViewDataTextColumn FieldName="IdPosicao" Caption="Id.Posição" VisibleIndex="1" />
                                                            <dxwgv:GridViewDataTextColumn FieldName="IdContraParte" VisibleIndex="2" />
                                                            <dxwgv:GridViewDataTextColumn FieldName="Apelido" Caption="Apelido" VisibleIndex="3"
                                                                UnboundType="String" />
                                                            <dxwgv:GridViewDataTextColumn FieldName="Quantidade" Caption="Quantidade" VisibleIndex="4" />
                                                            <dxwgv:GridViewDataTextColumn FieldName="CotaDia" Caption="Cota" VisibleIndex="5" />
                                                        </Columns>
                                                        <Settings ShowFilterRow="True" ShowTitlePanel="True" />
                                                        <SettingsBehavior ColumnResizeMode="Disabled" />
                                                        <ClientSideEvents RowDblClick="function(s, e) {
                                                                gridPosicao.GetValuesOnCustomCallback(e.visibleIndex, OnGetDataPosicao);}"
                                                            Init="function(s, e) {e.cancel = true; }" />
                                                        <SettingsDetail ShowDetailButtons="False" />
                                                        <Styles Cell-Wrap="False" AlternatingRow-CssClass="MyClass">
                                                            <Header ImageSpacing="5px" SortingImageSpacing="5px" />
                                                        </Styles>
                                                        <Images>
                                                        </Images>
                                                        <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Posição" />
                                                    </dxwgv:ASPxGridView>
                                                </div>
                                            </dxpc:PopupControlContentControl>
                                        </ContentCollection>
                                        <ClientSideEvents CloseUp="function(s, e) {gridPosicao.ClearFilter();}" PopUp="function(s, e) {gridPosicao.PerformCallback();}" />
                                    </dxpc:ASPxPopupControl>
                                    <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;">
                                        <asp:Literal ID="Literal1" runat="server" Text="Novo" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;">
                                        <asp:Literal ID="Literal2" runat="server" Text="Excluir" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnPdf" OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal4" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnExcel" OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal5" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh"
                                        OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal6" runat="server" Text="Atualizar" /><div>
                                        </div>
                                    </asp:LinkButton>
                                </div>
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true" KeyFieldName="IdAgendamentoComeCotas"
                                        DataSourceID="EsDSAgendaComeCotas" OnRowUpdating="gridCadastro_RowUpdating" OnRowInserting="gridCadastro_RowInserting"
                                        OnCustomCallback="gridCadastro_CustomCallback" OnBeforeGetCallbackResult="gridCadastro_PreRender"
                                        OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="13%" ButtonType="Image" ShowClearFilterButton="True">
                                                <HeaderTemplate>
                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                                </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataColumn FieldName="IdAgendamentoComeCotas" VisibleIndex="1" Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="IdCarteira" Caption="Id.Carteira" VisibleIndex="2" Width="5%"/>
                                            <dxwgv:GridViewDataColumn FieldName="Apelido" VisibleIndex="3" UnboundType="string" Width="10%"/>
                                            <dxwgv:GridViewDataColumn FieldName="IdCotistaFundo" Caption="Id. Cotista/Fundo" UnboundType="String" VisibleIndex="4" Width="5%" Settings-AutoFilterCondition="Equals"/>
                                            
                                            
                                            <dxwgv:GridViewDataColumn FieldName="NomeCotistaFundo" Caption="Nome Cotista/Fundo" UnboundType="String"  VisibleIndex="5" Settings-AutoFilterCondition="Contains" />
                                            <dxwgv:GridViewDataColumn FieldName="DataAplicacao" Caption="Dt. Aplicação" VisibleIndex="6"  UnboundType="DateTime" />
                                            <dxwgv:GridViewDataColumn FieldName="IdPosicao" Caption="Id.Posição" Visible="false"/>
                                            <dxwgv:GridViewDataColumn FieldName="DataLancamento" Caption="Dt.Lançamento" VisibleIndex="7" />
                                            <dxwgv:GridViewDataColumn FieldName="DataVencimento" Caption="Dt.Vencimento" VisibleIndex="8" Visible="false"/>
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="TipoEvento" VisibleIndex="9">
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Text="Manual" value="1" />   
                                                        <dxe:ListEditItem Text="Distribuição Dividendo" value="2"/>
                                                        <dxe:ListEditItem Text="Resgate Tributos" value="3"/>  
                                                        <dxe:ListEditItem Text="Zera Caixa" value="4"/>
                                                        <dxe:ListEditItem Text="Incorporação" value="5"/>   
                                                        <dxe:ListEditItem Text="Operação Mãe" value="6"/>   
                                                        <dxe:ListEditItem Text="Ordem Cotista" value="7"/>  
                                                        <dxe:ListEditItem Text="Cisão Incorporação Fusão" value="8"/> 
                                                        <dxe:ListEditItem Text="Mudança Condominio" value="9"/>   
                                                        <dxe:ListEditItem Text="Mudança Classificação" value="10"/>   
                                                        <dxe:ListEditItem Text="Come Cotas" value="11"/>  
                                                        <dxe:ListEditItem Text="Inclusão Retirada" value="12"/> 
                                                        <dxe:ListEditItem Text="Transferência Serie" value="13"/>
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataColumn FieldName="DataUltimoIR" Caption="Dt.Último IR" VisibleIndex="10" />
                                            <dxwgv:GridViewDataTextColumn FieldName="Quantidade" VisibleIndex="9" Caption="Qtde" Visible="false">
                                                <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="QuantidadeAntesCortes" Caption="Qtde.Antes Cortes"
                                                VisibleIndex="10">
                                                <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="QuantidadeComida" VisibleIndex="28" Caption="Qtde.Comida" Visible="false">
                                                <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="ValorCotaAplic" VisibleIndex="11" Visible="false">
                                                <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="ValorCotaUltimoPagamentoIR" VisibleIndex="12"
                                                Visible="false">
                                                <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="ValorCota" VisibleIndex="13" Visible="false">
                                                <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="AliquotaCC" VisibleIndex="14" Visible="false">
                                                <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="RendimentoBrutoDesdeAplicacao" VisibleIndex="15"
                                                Visible="false">
                                                <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="RendimentoDesdeUltimoPagamentoIR" VisibleIndex="16"
                                                Visible="false">
                                                <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataColumn FieldName="NumDiasCorridosDesdeAquisicao" VisibleIndex="17"
                                                Visible="false" />
                                            <dxwgv:GridViewDataColumn FieldName="PrazoIOF" VisibleIndex="17" Visible="false" />
                                            <dxwgv:GridViewDataTextColumn FieldName="AliquotaIOF" VisibleIndex="18" Visible="false">
                                                <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="ValorIOF" VisibleIndex="18" Visible="false">
                                                <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="ValorIOFVirtual" VisibleIndex="19" Visible="false">
                                                <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="PrejuizoUsado" VisibleIndex="20" Visible="false">
                                                <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="RendimentoCompensado" VisibleIndex="21"
                                                Visible="false">
                                                <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="ValorIRAgendado" VisibleIndex="22" Visible="false">
                                                <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="ValorIRPago" VisibleIndex="23" Visible="false">
                                                <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="Residuo15" VisibleIndex="24" Visible="false">
                                                <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="Residuo175" VisibleIndex="25" Visible="false">
                                                <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="Residuo20" VisibleIndex="26" Visible="false">
                                                <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="Residuo225" VisibleIndex="27" Visible="false">
                                                <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="QuantidadeFinal" VisibleIndex="29" Visible="false">
                                                <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="ExecucaoRecolhimento" VisibleIndex="30">
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="1" Text="Não recolher IR" />
                                                        <dxe:ListEditItem Value="2" Text="Recolhe IR na propria data" />
                                                        <dxe:ListEditItem Value="3" Text="Agenda o recollhimento do IR" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="TipoAliquotaIR" VisibleIndex="31">
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="1" Text="Utilizar alíquota IR padrão do Come-Cot" />
                                                        <dxe:ListEditItem Value="2" Text="Utilizar alíquota IR vigente de cada cautela" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataColumn FieldName="AliquotaIR" VisibleIndex="32" Visible="false" />
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="TipoPosicao" VisibleIndex="33">
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="1" Text="Cotista" />
                                                        <dxe:ListEditItem Value="2" Text="Fundo" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="Fator" VisibleIndex="29" Visible="false" UnboundType="Decimal">
                                            </dxwgv:GridViewDataTextColumn>
                                        </Columns>
                                        <Templates>
                                            <EditForm>
                                                <div class="editForm">
                                                    <table>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <dxe:ASPxLabel ID="labelDataLancamento" runat="server" CssClass="labelRequired" Text="Data Lançamento (Filtro):"
                                                                    OnLoad="labelDataLancamento_Load" />
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxDateEdit ID="textDataLancamento" runat="server" ClientInstanceName="textDataLancamento"
                                                                    Value='<%#Eval("DataLancamento")%>' />
                                                            </td>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelDataVencimento" runat="server" CssClass="labelRequired" Text="Data Vencimento:" />
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxDateEdit ID="textDataVencimento" runat="server" ClientInstanceName="textDataVencimento"
                                                                    Value='<%#Eval("DataVencimento")%>' />
                                                            </td>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelDataUltimoIR" runat="server" CssClass="labelRequired" Text="Data Último IR:" />
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxDateEdit ID="textDataUltimoIR" runat="server" ClientInstanceName="textDataUltimoIR"
                                                                    Value='<%#Eval("DataUltimoIR")%>' />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelTipoPosicao" runat="server" CssClass="labelRequired" Text="Tipo Posição:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxComboBox ID="dropTipoPosicao" runat="server" ClientInstanceName="dropTipoPosicao"
                                                                    ShowShadow="true" DropDownStyle="DropDown" IncrementalFilteringMode="Contains"
                                                                    CssClass="dropDownListCurto" Text='<%#Eval("TipoPosicao")%>' OnLoad="dropTipoPosicao_Load">
                                                                    <Items>
                                                                        <dxe:ListEditItem Value="1" Text="Cotista" />
                                                                        <dxe:ListEditItem Value="2" Text="Fundo" />
                                                                    </Items>
                                                                    <ClientSideEvents SelectedIndexChanged="function(s, e) { if(s.GetSelectedIndex() != -1) MudaTextoLabel(s.GetSelectedItem().value); }" />
                                                                </dxe:ASPxComboBox>
                                                            </td>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelTipoAliquotaIR" runat="server" CssClass="labelRequired" Text="Tipo Aliquota IR:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxComboBox ID="dropTipoAliquotaIR" runat="server" ClientInstanceName="dropTipoAliquotaIR"
                                                                    ShowShadow="true" DropDownStyle="DropDown" IncrementalFilteringMode="Contains"
                                                                    CssClass="dropDownListCurto" Text='<%#Eval("TipoAliquotaIR")%>'>
                                                                    <Items>
                                                                        <dxe:ListEditItem Value="1" Text="Utilizar alíquota IR padrão do Come-Cot" />
                                                                        <dxe:ListEditItem Value="2" Text="Utilizar alíquota IR vigente de cada cautela" />
                                                                    </Items>
                                                                </dxe:ASPxComboBox>
                                                            </td>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelExecucaoRecolhimento" runat="server" CssClass="labelRequired"
                                                                    Text="Execução Recolhimento:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxComboBox ID="dropExecucaoRecolhimento" runat="server" ClientInstanceName="dropExecucaoRecolhimento"
                                                                    ShowShadow="true" DropDownStyle="DropDown" IncrementalFilteringMode="Contains"
                                                                    CssClass="dropDownListCurto" Text='<%#Eval("ExecucaoRecolhimento")%>'>
                                                                    <Items>
                                                                        <dxe:ListEditItem Value="1" Text="Não recolher IR" />
                                                                        <dxe:ListEditItem Value="2" Text="Recolhe IR na propria data" />
                                                                        <dxe:ListEditItem Value="3" Text="Agenda o recollhimento do IR" />
                                                                    </Items>
                                                                </dxe:ASPxComboBox>
                                                            </td>
                                                            <td class="td_Label">
                                                                <dxe:ASPxLabel ID="labelTipoEvento" ClientInstanceName="labelTipoEvento" runat="server"
                                                                    CssClass="labelRequired" Text="Tipo Evento:">
                                                                </dxe:ASPxLabel>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxComboBox ID="dropTipoEvento" runat="server" ClientInstanceName="dropTipoEvento"
                                                                    ShowShadow="true" DropDownStyle="DropDown" IncrementalFilteringMode="Contains"
                                                                    CssClass="dropDownListCurto" Text='<%#Eval("TipoEvento")%>'>
                                                                    <Items>
                                                                        <dxe:ListEditItem Text="Manual" />   
                                                                        <dxe:ListEditItem Text="Distribuição Dividendo" value="2"/>
                                                                        <dxe:ListEditItem Text="Resgate Tributos" value="3"/>  
                                                                        <dxe:ListEditItem Text="Zera Caixa" value="4"/>
                                                                        <dxe:ListEditItem Text="Incorporação" value="5"/>   
                                                                        <dxe:ListEditItem Text="Operação Mãe" value="6"/>   
                                                                        <dxe:ListEditItem Text="Ordem Cotista" value="7"/>  
                                                                        <dxe:ListEditItem Text="Cisão Incorporação Fusão" value="8"/> 
                                                                        <dxe:ListEditItem Text="Mudança Condominio" value="9"/>   
                                                                        <dxe:ListEditItem Text="Mudança Classificação" value="10"/>   
                                                                        <dxe:ListEditItem Text="Come Cotas" value="11"/>  
                                                                        <dxe:ListEditItem Text="Inclusão Retirada" value="12"/> 
                                                                        <dxe:ListEditItem Text="Transferência Serie" value="13"/>
                                                                    </Items>
                                                                </dxe:ASPxComboBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <dxe:ASPxLabel ID="labelContraParte" ClientInstanceName="labelContraParte" runat="server"
                                                                    CssClass="labelRequired" Text="Contra Parte:">
                                                                </dxe:ASPxLabel>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="btnEditCodigoCliente" runat="server" CssClass="textButtonEdit"
                                                                    ClientInstanceName="btnEditCodigoCliente" Text='<%#Eval("IdCarteira")%>' MaxLength="10"
                                                                    NumberType="Integer" OnLoad="btnEditCodigoCliente_Load">
                                                                    <Buttons>
                                                                        <dxe:EditButton>
                                                                        </dxe:EditButton>
                                                                    </Buttons>
                                                                    <ClientSideEvents KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCliente').value = '';} "
                                                                        ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" LostFocus="function(s, e) {OnLostFocus(popupMensagemCliente, callbackCliente, btnEditCodigoCliente);}"
                                                                        Init="function(s, e) { if(dropTipoPosicao.GetSelectedIndex() != -1) MudaTextoLabel(dropTipoPosicao.GetSelectedItem().value); }" />
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                            <td colspan="4">
                                                                <asp:TextBox ID="textNomeCliente" runat="server" CssClass="textNome" Style="width: 386px;"
                                                                    Enabled="false" Text='<%#Eval("Apelido")%>'></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelPosicao" runat="server" CssClass="labelRequired" Text="Id Posição:" />
                                                            </td>
                                                            <td colspan="4">
                                                                <dxe:ASPxTextBox ID="hiddenIdPosicao" ClientInstanceName="hiddenIdPosicao" runat="server"
                                                                    Text='<%#Eval("IdPosicao")%>' CssClass="hiddenField" />
                                                                <dxe:ASPxButtonEdit ID="btnEditPosicao" runat="server" CssClass="textButtonEdit"
                                                                    OnLoad="btnEditPosicao_Load" ClientInstanceName="btnEditPosicao" ReadOnly="true"
                                                                    Width="380px" Text='<%#Eval("IdPosicao")%>'>
                                                                    <Buttons>
                                                                        <dxe:EditButton>
                                                                        </dxe:EditButton>
                                                                    </Buttons>
                                                                    <ClientSideEvents ButtonClick="function(s, e) { gridPosicao.PerformCallback('refresh');  popupPosicao.ShowAtElementByID(s.name);}" />
                                                                </dxe:ASPxButtonEdit>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelQuantidade" runat="server" CssClass="labelRequired" Text="Quantidade:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="textQuantidade" runat="server" CssClass="textValor_5" ClientInstanceName="textQuantidade"
                                                                    MaxLength="28" DecimalPlaces="12" NumberType="Float" Text='<%#Eval("Quantidade")%>'>
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelQuantidadeAntesCortes" runat="server" CssClass="labelRequired"
                                                                    Text="Quantidade Antes Cortes:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="textQuantidadeAntesCortes" runat="server" CssClass="textQuantidadeAntesCortes"
                                                                    ClientInstanceName="textQuantidadeAntesCortes" MaxLength="28" NumberType="Float" DecimalPlaces="12" Text='<%#Eval("QuantidadeAntesCortes")%>'>
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelValorCotaAplic" runat="server" CssClass="labelRequired" Text="Valor Cota Aplicação:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="textValorCotaAplic" runat="server" CssClass="textValor_5" ClientInstanceName="textValorCotaAplic"
                                                                    MaxLength="28" NumberType="Float" DecimalPlaces="12"
                                                                    Text='<%#Eval("ValorCotaAplic")%>'>
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelValorCota" runat="server" CssClass="labelRequired" Text="Valor Cota:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="textValorCota" runat="server" CssClass="textValor_5" ClientInstanceName="textValorCota"
                                                                    MaxLength="28" DecimalPlaces="12" NumberType="Float" Text='<%#Eval("ValorCota")%>'>
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelValorCotaUltimoPagamentoIR" runat="server" CssClass="labelRequired"
                                                                    Text="Valor Cota Último Pagamento IR:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="textValorCotaUltimoPagamentoIR" runat="server" CssClass="textValor_5"
                                                                    ClientInstanceName="textValorCotaUltimoPagamentoIR" MaxLength="28" NumberType="Float"
                                                                    DecimalPlaces="12" Text='<%#Eval("ValorCotaUltimoPagamentoIR")%>'>
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelAliquotaCC" runat="server" CssClass="labelRequired" Text="Aliquota CC:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="textAliquotaCC" runat="server" CssClass="textValor_5" ClientInstanceName="textAliquotaCC"
                                                                    MaxLength="5" NumberType="Float" DecimalPlaces="2" Text='<%#Eval("AliquotaCC")%>'>
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelRendimentoBrutoDesdeAplicacao" runat="server" CssClass="labelRequired"
                                                                    Text="Rendimento Bruto Desde Aplicação:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="textRendimentoBrutoDesdeAplicacao" runat="server" CssClass="textValor_5"
                                                                    ClientInstanceName="textRendimentoBrutoDesdeAplicacao" MaxLength="16" DecimalPlaces="2" NumberType="Float"
                                                                    Text='<%#Eval("RendimentoBrutoDesdeAplicacao")%>'>
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelRendimentoDesdeUltimoPagamentoIR" runat="server" CssClass="labelRequired"
                                                                    Text="Rendimento desde último Pagamento IR:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="textRendimentoDesdeUltimoPagamentoIR" runat="server" CssClass="textValor_5"
                                                                    ClientInstanceName="textRendimentoDesdeUltimoPagamentoIR" MaxLength="16" DecimalPlaces="2" NumberType="Float"
                                                                    Text='<%#Eval("RendimentoDesdeUltimoPagamentoIR")%>'>
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelPrazoIOF" runat="server" CssClass="labelRequired" Text="Prazo IOF:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="textPrazoIOF" runat="server" CssClass="textValor_5" ClientInstanceName="textPrazoIOF"
                                                                    MaxLength="10" NumberType="Integer" Text='<%#Eval("PrazoIOF")%>'>
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelAliquotaIOF" runat="server" CssClass="labelRequired" Text="Aliquota IOF:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="textAliquotaIOF" runat="server" CssClass="textValor_5" ClientInstanceName="textAliquotaIOF"
                                                                    MaxLength="5" DecimalPlaces="2" NumberType="Float" Text='<%#Eval("AliquotaIOF")%>'>
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelValorIOF" runat="server" CssClass="labelRequired" Text="Valor IOF:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="textValorIOF" runat="server" CssClass="textValor_5" ClientInstanceName="textValorIOF"
                                                                    MaxLength="28" DecimalPlaces="12" NumberType="Float" Text='<%#Eval("ValorIOF")%>'>
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelValorIOFVirtual" runat="server" CssClass="labelRequired" Text="Valor IOF Virtual:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="textValorIOFVirtual" runat="server" CssClass="textValor_5"
                                                                    ClientInstanceName="textValorIOFVirtual" MaxLength="28" DecimalPlaces="12" NumberType="Float" Text='<%#Eval("ValorIOFVirtual")%>'>
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelNumDiasCorridosDesdeAquisicao" runat="server" CssClass="labelRequired"
                                                                    Text="Num dias corridos desde aquisição:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="textNumDiasCorridosDesdeAquisicao" runat="server" CssClass="textValor_5"
                                                                    ClientInstanceName="textNumDiasCorridosDesdeAquisicao" MaxLength="10" NumberType="Integer"
                                                                    Text='<%#Eval("NumDiasCorridosDesdeAquisicao")%>'>
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelPrejuizoUsado" runat="server" CssClass="labelRequired" Text="Prejuizo Usado:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="textPrejuizoUsado" runat="server" CssClass="textValor_5" ClientInstanceName="textPrejuizoUsado"
                                                                    MaxLength="16" DecimalPlaces="2" NumberType="Float" Text='<%#Eval("PrejuizoUsado")%>'>
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelValor" runat="server" CssClass="labelRequired" Text="Rendimento Compensado:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="textRendimentoCompensado" runat="server" CssClass="textValor_5"
                                                                    ClientInstanceName="textRendimentoCompensado" MaxLength="16" DecimalPlaces="2" NumberType="Float"
                                                                    Text='<%#Eval("RendimentoCompensado")%>'>
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelValorIRAgendado" runat="server" CssClass="labelRequired" Text="Valor IR Agendado:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="textValorIRAgendado" runat="server" CssClass="textValor_5"
                                                                    ClientInstanceName="textValorIRAgendado" MaxLength="28" DecimalPlaces="12" NumberType="Float" Text='<%#Eval("ValorIRAgendado")%>'>
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelResiduo15" runat="server" CssClass="labelRequired" Text="Residuo 15:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="textResiduo15" runat="server" CssClass="textValor_5" ClientInstanceName="textResiduo15"
                                                                    MaxLength="28" DecimalPlaces="12" NumberType="Float" Text='<%#Eval("Residuo15")%>'>
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelResiduo175" runat="server" CssClass="labelRequired" Text="Residuo 175:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="textResiduo175" runat="server" CssClass="textValor_5" ClientInstanceName="textResiduo175"
                                                                    MaxLength="28" DecimalPlaces="12" NumberType="Float" Text='<%#Eval("Residuo175")%>'>
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelResiduo20" runat="server" CssClass="labelRequired" Text="Residuo 20:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="textResiduo20" runat="server" CssClass="textValor_5" ClientInstanceName="textResiduo20"
                                                                    MaxLength="28" DecimalPlaces="12" NumberType="Float" Text='<%#Eval("Residuo20")%>'>
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelResiduo225" runat="server" CssClass="labelRequired" Text="Residuo 225:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="textResiduo225" runat="server" CssClass="textValor_5" ClientInstanceName="textResiduo225"
                                                                    MaxLength="28" DecimalPlaces="12" NumberType="Float" Text='<%#Eval("Residuo225")%>'>
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelValorIRPago" runat="server" CssClass="labelRequired" Text="ValorIRPago:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="textValorIRPago" runat="server" CssClass="textValor_5" ClientInstanceName="textValorIRPago"
                                                                    MaxLength="16" DecimalPlaces="2" NumberType="Float" Text='<%#Eval("ValorIRPago")%>'>
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelQuantidadeComida" runat="server" CssClass="labelRequired" Text="Quantidade Comida:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="textQuantidadeComida" runat="server" CssClass="textValor_5"
                                                                    ClientInstanceName="textQuantidadeComida" MaxLength="28" DecimalPlaces="12" NumberType="Float" Text='<%#Eval("QuantidadeComida")%>'>
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelQuantidadeFinal" runat="server" CssClass="labelRequired" Text="Quantidade Final:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="textQuantidadeFinal" runat="server" CssClass="textValor_5"
                                                                    ClientInstanceName="textQuantidadeFinal" MaxLength="28" DecimalPlaces="12" NumberType="Float"
                                                                    Text='<%#Eval("QuantidadeFinal")%>'>
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelAliquotaIR" runat="server" CssClass="labelRequired" Text="Aliquota IR:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="textAliquotaIR" runat="server" CssClass="textValor_5" ClientInstanceName="textAliquotaIR"
                                                                    MaxLength="10" NumberType="Integer" Text='<%#Eval("AliquotaIR")%>'>
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="label1" runat="server" CssClass="labelNormal" Text="Fator:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="textFator" runat="server" CssClass="textValor_5" ClientInstanceName="textFator" Enabled="false"
                                                                    MaxLength="16" DecimalPlaces="12" NumberType="Float" Text='<%#Eval("Fator")%>' >
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div class="linhaH">
                                                    </div>
                                                    <div class="linkButton linkButtonNoBorder popupFooter">
                                                        <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                            OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                                                            <asp:Literal ID="Literal5" runat="server" Text="OK" /><div>
                                                            </div>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel"
                                                            OnClientClick="gridCadastro.CancelEdit(); return false;">
                                                            <asp:Literal ID="Literal9" runat="server" Text="Cancelar" /><div>
                                                            </div>
                                                        </asp:LinkButton>
                                                    </div>
                                                </div>
                                            </EditForm>
                                        </Templates>
                                        <SettingsPopup EditForm-Width="450px" />
                                        <ClientSideEvents BeginCallback="function(s, e) {
		                if (e.command == 'CUSTOMCALLBACK') {
                            isCustomCallback = true;
                        }						
                    }" EndCallback="function(s, e) {
			            if (isCustomCallback) {
                            isCustomCallback = false;
                            s.Refresh();
                        }
                    }" />
                                        <SettingsCommandButton>
                                            <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                                        </SettingsCommandButton>
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro"
            Landscape="true">
        </dxwgv:ASPxGridViewExporter>
        <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" />
        <cc1:esDataSource ID="EsDSPosicao" runat="server" OnesSelect="EsDSPosicao_esSelect" />
        <cc1:esDataSource ID="EsDSAgendaComeCotas" runat="server" OnesSelect="EsDSAgendaComeCotas_esSelect" />
    </form>
</body>
</html>
