﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using Financial.Investidor;
using Financial.ContaCorrente;
using System.Drawing;
using Financial.ContaCorrente.Enums;
using Financial.Security;
using Financial.Fundo;
using Financial.Bolsa;
using Financial.BMF;
using Financial.RendaFixa;
using Financial.Investidor.Enums;
using Financial.InvestidorCotista;
using Financial.CRM;
using Financial.WebConfigConfiguration;
using Financial.Security.Enums;
using System.Collections.Specialized;
using EntitySpaces.Interfaces;
using Financial.Web.Util;
using Financial.InvestidorCotista.Enums;
using Financial.Web.Common;
using System.Web.Configuration;
using System.Xml;
using Financial.Export;
using Financial.Interfaces.Export;

public partial class CadastrosBasicos_Cotista : Financial.Web.Common.CadastroBasePage
{
    private bool _integracaoCrm;

    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasPopupPessoa = true;
        this.HasPopupCliente = true;

        base.Page_Load(sender, e);

        _integracaoCrm = WebConfigurationManager.AppSettings["IntegraCotistaCrm"] != null ? Convert.ToBoolean(WebConfigurationManager.AppSettings["IntegraCotistaCrm"]) : false;

        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
                  new List<string>(new string[] { CotistaMetadata.ColumnNames.StatusAtivo
                          }));
    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSCotista_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        CotistaQuery cot = new CotistaQuery("cot");
        PermissaoCotistaQuery perCotistaQuery = new PermissaoCotistaQuery("permissao");
        ClienteQuery cliEspelho = new ClienteQuery("cliEspelho");        
        PessoaQuery pes = new PessoaQuery("pes");
        
        cot.Select(cot, cliEspelho.Apelido.As("ApelidoClienteEspelho"), pes.Apelido.As("ApelidoPessoa"));
        cot.InnerJoin(perCotistaQuery).On(cot.IdCotista.Equal(perCotistaQuery.IdCotista));        
        cot.LeftJoin(cliEspelho).On(cot.IdClienteEspelho.Equal(cliEspelho.IdCliente));        
        cot.LeftJoin(pes).On(cot.IdPessoa.Equal(pes.IdPessoa));
        cot.Where(perCotistaQuery.IdUsuario.Equal(idUsuario));
        cot.OrderBy(cot.Apelido.Ascending);

        CotistaCollection coll = new CotistaCollection();
        coll.Load(cot);

        e.Collection = coll;
    }

    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CarteiraCollection coll = new CarteiraCollection();
        coll.BuscaCarteirasComAcesso(Context.User.Identity.Name);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSAgenteDistribudor_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        AgenteMercadoCollection coll = new AgenteMercadoCollection();
        coll.Query.Where(coll.Query.FuncaoDistribuidor.Equal("S"));
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSPessoa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        PessoaCollection coll = new PessoaCollection();
        coll.Query.OrderBy(coll.Query.Apelido.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ClienteCollection coll = new ClienteCollection();
        coll.Query.OrderBy(coll.Query.Apelido.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    private string RetornaLoginAssociado(int idCotista)
    {
        PermissaoCotistaQuery permissaoCotistaQuery = new PermissaoCotistaQuery("P");
        UsuarioQuery usuarioQuery = new UsuarioQuery("U");
        usuarioQuery.InnerJoin(permissaoCotistaQuery).On(permissaoCotistaQuery.IdUsuario == usuarioQuery.IdUsuario);
        usuarioQuery.Where(permissaoCotistaQuery.IdCotista.Equal(idCotista));
        usuarioQuery.Where(usuarioQuery.TipoTrava.Equal((byte)TipoTravaUsuario.TravaCotista));

        UsuarioCollection usuarioCollection = new UsuarioCollection();
        usuarioCollection.Load(usuarioQuery);

        string login = "";
        if (usuarioCollection.Count > 0)
        {
            login = usuarioCollection[0].Login.ToString();
        }

        return login;
    }

    private string RetornaCarteiraAssociada(int idCotista)
    {
        PermissaoCotistaQuery permissaoCotistaQuery = new PermissaoCotistaQuery("P");
        UsuarioQuery usuarioQuery = new UsuarioQuery("U");
        usuarioQuery.InnerJoin(permissaoCotistaQuery).On(permissaoCotistaQuery.IdUsuario == usuarioQuery.IdUsuario);
        usuarioQuery.Where(permissaoCotistaQuery.IdCotista.Equal(idCotista));
        usuarioQuery.Where(usuarioQuery.TipoTrava.Equal((byte)TipoTravaUsuario.TravaCliente));

        UsuarioCollection usuarioCollection = new UsuarioCollection();
        usuarioCollection.Load(usuarioQuery);

        string apelido = "";
        if (usuarioCollection.Count > 0)
        {
            int idUsuario = usuarioCollection[0].IdUsuario.Value;
            PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
            usuarioQuery = new UsuarioQuery("U");
            permissaoClienteQuery.InnerJoin(usuarioQuery).On(usuarioQuery.IdUsuario == permissaoClienteQuery.IdUsuario);
            permissaoClienteQuery.Where(permissaoClienteQuery.IdUsuario.Equal(idUsuario));
            permissaoClienteQuery.Where(usuarioQuery.TipoTrava.Equal((byte)TipoTravaUsuario.TravaCliente));

            PermissaoClienteCollection permissaoClienteCollection = new PermissaoClienteCollection();
            permissaoClienteCollection.Load(permissaoClienteQuery);

            if (permissaoClienteCollection.Count > 0)
            {
                int idCliente = permissaoClienteCollection[0].IdCliente.Value;
                Cliente cliente = new Cliente();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.Apelido);
                cliente.LoadByPrimaryKey(campos, idCliente);
                apelido = cliente.Apelido;
            }
        }

        return apelido;
    }

    protected void labelIdClienteEspelho_Load(object sender, EventArgs e)
    {
        if (Financial.WebConfigConfiguration.WebConfig.AppSettings.Cliente.ToUpper() == "PORTOPAR")
        {
            (sender as Label).Visible = true;
        }
        else
        {
            (sender as Label).Visible = false;
        }
    }

    protected void textApelidoClienteEspelho_Load(object sender, EventArgs e)
    {
        if (Financial.WebConfigConfiguration.WebConfig.AppSettings.Cliente.ToUpper() == "PORTOPAR")
        {
            (sender as TextBox).Visible = true;
        }
        else
        {
            (sender as TextBox).Visible = false;
        }
    }

    protected void btnEditCodigoClienteEspelho_Load(object sender, EventArgs e)
    {
        if (Financial.WebConfigConfiguration.WebConfig.AppSettings.Cliente.ToUpper() == "PORTOPAR")
        {
            (sender as ASPxSpinEdit).Visible = true;

            if (gridCadastro.IsEditing && !gridCadastro.IsNewRowEditing)
            {
                //Preenche o textBox de Login (pega o 1o login com trava de cotista na lista de permissaoCotista
                string apelido = "";
                if (gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "IdClienteEspelho") != DBNull.Value)
                {
                    int idClienteEspelho = Convert.ToInt32(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "IdClienteEspelho"));

                    Cliente cliente = new Cliente();
                    if (cliente.LoadByPrimaryKey(idClienteEspelho))
                        apelido = cliente.Apelido;
                }

                TextBox textApelidoClienteEspelho = gridCadastro.FindEditFormTemplateControl("textApelidoClienteEspelho") as TextBox;
                textApelidoClienteEspelho.Text = apelido;

                if (textApelidoClienteEspelho.Text != "")
                {
                    textApelidoClienteEspelho.Enabled = false;
                }
            }
        }
        else
        {
            (sender as ASPxSpinEdit).ClientVisible = false;
        }
    }

    protected void btnEditCodigoPessoa_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
        {
            (sender as ASPxSpinEdit).Enabled = false;
            (sender as ASPxSpinEdit).BackColor = Color.FromName("#EBEBEB");
        }

        if (gridCadastro.IsEditing && !gridCadastro.IsNewRowEditing)
        {
            //Preenche o textBox de Login (pega o 1o login com trava de cotista na lista de permissaoCotista
            int idCotista = Convert.ToInt32(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "IdCotista"));

            string login = RetornaLoginAssociado(idCotista);

            TextBox textLogin = gridCadastro.FindEditFormTemplateControl("textLogin") as TextBox;
            textLogin.Text = login;

        }
    }

    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
        ASPxSpinEdit btnEditCodigoPessoa = gridCadastro.FindEditFormTemplateControl("btnEditCodigoPessoa") as ASPxSpinEdit;
        ASPxComboBox dropStatusAtivo = gridCadastro.FindEditFormTemplateControl("dropStatusAtivo") as ASPxComboBox;
        ASPxComboBox dropIsentoIR = gridCadastro.FindEditFormTemplateControl("dropIsentoIR") as ASPxComboBox;
        ASPxComboBox dropIsentoIOF = gridCadastro.FindEditFormTemplateControl("dropIsentoIOF") as ASPxComboBox;
        ASPxComboBox dropTipoTributacao = gridCadastro.FindEditFormTemplateControl("dropTipoTributacao") as ASPxComboBox;
        ASPxTextBox textNomeCotista = gridCadastro.FindEditFormTemplateControl("textNomeCotista") as ASPxTextBox;
        ASPxTextBox textApelidoCotista = gridCadastro.FindEditFormTemplateControl("textApelidoCotista") as ASPxTextBox; 

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(btnEditCodigoPessoa);
        controles.Add(dropStatusAtivo);
        controles.Add(dropIsentoIR);
        controles.Add(dropIsentoIOF);
        controles.Add(dropTipoTributacao);
        controles.Add(textNomeCotista);
        controles.Add(textApelidoCotista);

        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        ASPxTextBox textIdCotista = gridCadastro.FindEditFormTemplateControl("textIdCotista") as ASPxTextBox;
        int idCotista = Convert.ToInt32(textIdCotista.Text);

        if (gridCadastro.IsNewRowEditing)
        {
            Cotista cotista = new Cotista();
            if (cotista.LoadByPrimaryKey(idCotista))
            {
                e.Result = "O Id Cotista reservado foi utilizado por outro usuário, o campo foi atualizado para o próximo ID disponível!" + "|" + "updateIdCotista";
            }
        }

        TextBox textLogin = gridCadastro.FindEditFormTemplateControl("textLogin") as TextBox;
        string login = RetornaLoginAssociado(idCotista);

        if (!String.IsNullOrEmpty(login) && login.Trim() != textLogin.Text.Trim())
        {
            string descricaoGrupo = WebConfig.AppSettings.GrupoCotista;
            GrupoUsuarioCollection grupoUsuarioCollection = new GrupoUsuarioCollection();
            grupoUsuarioCollection.Query.Select(grupoUsuarioCollection.Query.IdGrupo);
            grupoUsuarioCollection.Query.Where(grupoUsuarioCollection.Query.Descricao.Equal(descricaoGrupo));
            if (!grupoUsuarioCollection.Query.Load())
            {
                e.Result = "Não existe grupo definido para login automático, ou não está devidamente associado no config.";
            }

            Usuario usuario = new Usuario();
            if (usuario.BuscaUsuario(textLogin.Text.Trim()))
            {
                e.Result = "Login já existente! Escolha outro login para associar com o cotista.";
            }
        }
    }

    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idPessoa = Convert.ToInt32(e.Parameter);
            Pessoa pessoa = new Pessoa();
            if (pessoa.LoadByPrimaryKey(idPessoa))
            {
                nome = pessoa.Apelido;
            }
        }
        e.Result = nome;
    }

    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            if (cliente.LoadByPrimaryKey(idCliente))
            {
                nome = cliente.Apelido;
            }
        }
        e.Result = nome;
    }

    private string RetornaEmailCustomizado()
    {
        string email = "";
        if (Financial.WebConfigConfiguration.WebConfig.AppSettings.Cliente.ToUpper() == "PORTOPAR")
        {
            email = "usuario@hide.com";
        }

        return email;

    }

    private void PropagaPermissaoTemplateCotista(int idCotista, int idUsuarioNovo)
    {
        //Na PortoPar propagar permissao cliente do cotista padrao
        if (Financial.WebConfigConfiguration.WebConfig.AppSettings.Cliente.ToUpper() == "PORTOPAR")
        {
            TemplateCotista templateCotista = new TemplateCotista();
            templateCotista.Query.Where(templateCotista.Query.Descricao.ToUpper().Equal("COTISTA PORTAL"));
            templateCotista.Load(templateCotista.Query);

            Cotista template = new Cotista();
            template.LoadByPrimaryKey(templateCotista.IdCotista.Value);

            //Encontrar usuario associado ao cotista template
            string loginTemplate = RetornaLoginAssociado(template.IdCotista.Value);
            UsuarioCollection usuariosTemplate = new UsuarioCollection();
            usuariosTemplate.BuscaUsuarioCollectionPorLogin(loginTemplate);

            if (usuariosTemplate.Count >= 1)
            {
                Usuario usuarioTemplate = usuariosTemplate[0];

                PermissaoClienteCollection permissaoClienteTplCol = new PermissaoClienteCollection();
                permissaoClienteTplCol.Query.Where(permissaoClienteTplCol.Query.IdUsuario.Equal(usuarioTemplate.IdUsuario.Value));
                permissaoClienteTplCol.Load(permissaoClienteTplCol.Query);

                foreach (PermissaoCliente permissaoClienteTpl in permissaoClienteTplCol)
                {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    permissaoCliente.IdCliente = permissaoClienteTpl.IdCliente;
                    permissaoCliente.IdUsuario = idUsuarioNovo;
                    permissaoCliente.Save();
                }

                //Propagar para operacaofundo
                PermissaoOperacaoFundoCollection permissaoFundoTplCol = new PermissaoOperacaoFundoCollection();
                permissaoFundoTplCol.Query.Where(permissaoFundoTplCol.Query.IdCotista.Equal(template.IdCotista.Value));
                permissaoFundoTplCol.Load(permissaoFundoTplCol.Query);

                foreach (PermissaoOperacaoFundo permissaoFundoTpl in permissaoFundoTplCol)
                {
                    PermissaoOperacaoFundo permissaoFundo = new PermissaoOperacaoFundo();
                    permissaoFundo.IdCotista = idCotista;
                    permissaoFundo.IdFundo = permissaoFundoTpl.IdFundo;
                    permissaoFundo.Save();
                }


            }
        }
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        int idCotista = (int)e.Keys[0];

        #region Update
        ASPxComboBox dropStatusAtivo = gridCadastro.FindEditFormTemplateControl("dropStatusAtivo") as ASPxComboBox;
        ASPxComboBox dropIsentoIR = gridCadastro.FindEditFormTemplateControl("dropIsentoIR") as ASPxComboBox;
        ASPxComboBox dropIsentoIOF = gridCadastro.FindEditFormTemplateControl("dropIsentoIOF") as ASPxComboBox;
        ASPxComboBox dropTipoCotista = gridCadastro.FindEditFormTemplateControl("dropTipoCotista") as ASPxComboBox;
        ASPxComboBox dropTipoCotistaAnbima = gridCadastro.FindEditFormTemplateControl("dropTipoCotistaAnbima") as ASPxComboBox;
        ASPxComboBox dropTipoTributacao = gridCadastro.FindEditFormTemplateControl("dropTipoTributacao") as ASPxComboBox;
        TextBox textCodigoInterface = gridCadastro.FindEditFormTemplateControl("textCodigoInterface") as TextBox;
        ASPxDateEdit textDataExpiracao = gridCadastro.FindEditFormTemplateControl("textDataExpiracao") as ASPxDateEdit;
        TextBox textPendenciaCadastral = gridCadastro.FindEditFormTemplateControl("textPendenciaCadastral") as TextBox;
        ASPxSpinEdit textIdClienteEspelho = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxTextBox textNomeCotista = gridCadastro.FindEditFormTemplateControl("textNomeCotista") as ASPxTextBox;
        ASPxTextBox textApelidoCotista = gridCadastro.FindEditFormTemplateControl("textApelidoCotista") as ASPxTextBox;
        ASPxCheckBox chkOffShore = gridCadastro.FindEditFormTemplateControl("chkOffShore") as ASPxCheckBox;
        ASPxTextBox textCodigoConsolidacaoExterno = gridCadastro.FindEditFormTemplateControl("textCodigoConsolidacaoExterno") as ASPxTextBox;
        ASPxComboBox dropCarteiraAssociada = gridCadastro.FindEditFormTemplateControl("dropCarteiraAssociada") as ASPxComboBox;
        //        
        Cotista cotista = new Cotista();
        if (cotista.LoadByPrimaryKey(idCotista))
        {
            cotista.StatusAtivo = Convert.ToByte(dropStatusAtivo.SelectedItem.Value);
            cotista.IsentoIR = Convert.ToString(dropIsentoIR.SelectedItem.Value);
            cotista.IsentoIOF = Convert.ToString(dropIsentoIOF.SelectedItem.Value);
            cotista.TipoTributacao = Convert.ToByte(dropTipoTributacao.SelectedItem.Value);
            cotista.CodigoInterface = textCodigoInterface.Text.Trim();

            if (dropCarteiraAssociada.SelectedIndex != -1)
                cotista.IdCarteira = Convert.ToInt32(dropCarteiraAssociada.SelectedItem.Value);
            else
                cotista.IdCarteira = null;

            if (!string.IsNullOrEmpty(textNomeCotista.Text))
                cotista.Nome = textNomeCotista.Text;

            if (!string.IsNullOrEmpty(textApelidoCotista.Text))
                cotista.Apelido = textApelidoCotista.Text;

            if (textDataExpiracao.Text != "")
            {
                cotista.DataExpiracao = Convert.ToDateTime(textDataExpiracao.Value);
            }
            else
            {
                cotista.DataExpiracao = null;
            }

            cotista.PendenciaCadastral = textPendenciaCadastral.Text.Trim();

            if (dropTipoCotista.SelectedIndex != -1)
            {
                cotista.TipoCotistaCVM = Convert.ToInt32(dropTipoCotista.SelectedItem.Value);
            }

            if (dropTipoCotistaAnbima.SelectedIndex != -1)
            {
                cotista.TipoCotistaAnbima = Convert.ToInt32(dropTipoCotistaAnbima.SelectedItem.Value);
            }

            if (textIdClienteEspelho.Value != null)
            {
                cotista.IdClienteEspelho = Convert.ToInt32(textIdClienteEspelho.Value);
            }
            else
            {
                cotista.IdClienteEspelho = null;
            }

            cotista.OffShore = chkOffShore.Checked ? "S" : "N";

            if (!string.IsNullOrEmpty(textCodigoConsolidacaoExterno.Text))
                cotista.CodigoConsolidacaoExterno = textCodigoConsolidacaoExterno.Text;
            else
                cotista.CodigoConsolidacaoExterno = null;

        }
        #endregion

        #region Save
        if (cotista.IdCotista.HasValue)
        {
            cotista.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de Cotista - Operacao: Update Cotista: " + idCotista + UtilitarioWeb.ToString(cotista),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }
        #endregion

        TextBox textLogin = gridCadastro.FindEditFormTemplateControl("textLogin") as TextBox;
        string login = textLogin.Text.Trim();


        UsuarioCollection usuariosExistentes = new UsuarioCollection();
        usuariosExistentes.BuscaUsuarioCollectionPorLogin(login);

        if (usuariosExistentes.Count == 0)
        {
            #region Cadastra novo usuário associado ao cotista, cria PermissaoCotista associada ao usuário cadastrado
            int idUsuarioNovo = 0;

            if (!String.IsNullOrEmpty(login))
            {
                //Busca grupo que deve ser associado ao novo login
                int? idGrupo = null;
                string descricaoGrupo = WebConfig.AppSettings.GrupoCotista;
                GrupoUsuarioCollection grupoUsuarioCollection = new GrupoUsuarioCollection();
                grupoUsuarioCollection.Query.Select(grupoUsuarioCollection.Query.IdGrupo);
                grupoUsuarioCollection.Query.Where(grupoUsuarioCollection.Query.Descricao.Equal(descricaoGrupo));
                if (grupoUsuarioCollection.Query.Load())
                {
                    idGrupo = grupoUsuarioCollection[0].IdGrupo.Value;
                }

                if (idGrupo.HasValue)
                {
                    //Checar se campo login foi alterado
                    string loginStored = RetornaLoginAssociado(idCotista);
                    bool loginAlterado = login != loginStored;
                    
                    if (loginAlterado)
                    {
                        //Remover usuario com login associado
                        //Da erro de cascade no LimiteOperacaoUsuario
                        if (Financial.WebConfigConfiguration.WebConfig.AppSettings.Cliente.ToUpper() == "PORTOPAR")
                        {
                            Usuario usuarioRemover = new Usuario();
                            if (usuarioRemover.BuscaUsuario(loginStored))
                            {
                                try
                                {
                                    usuarioRemover.MarkAsDeleted();
                                    usuarioRemover.Save();
                                }catch {
                                }
                            }
                        }

                        Usuario usuario = new Usuario();
                        string email = RetornaEmailCustomizado();
                        usuario.Save(idGrupo.Value, login, login, login, email, "S", (byte)TipoTravaUsuario.TravaCotista, null);

                        idUsuarioNovo = usuario.IdUsuario.Value;

                        PermissaoCotista permissaoCotista = new PermissaoCotista();
                        permissaoCotista.IdCotista = cotista.IdCotista.Value;
                        permissaoCotista.IdUsuario = usuario.IdUsuario.Value;
                        permissaoCotista.Save();

                        PropagaPermissaoTemplateCotista(cotista.IdCotista.Value, idUsuarioNovo);
                    }
                }
            }
            #endregion
        }

        #region Trata associação cart (acesso ao cotista em PermissaoCotista e ao usuário do cotista em PermissaoCliente)        
        if (dropCarteiraAssociada.SelectedIndex != -1)
        {
            int idCarteira = Convert.ToInt32(dropCarteiraAssociada.SelectedItem.Value);

            //Trata acesso ao cotista em PermissaoCotista para o usuário vinculado à carteira
            PermissaoCliente permissaoClienteConsulta = new PermissaoCliente();
            int idUsuarioCarteira = (permissaoClienteConsulta.RetornaIdUsuarioTravaCliente(idCarteira));

            if (idUsuarioCarteira != 0)
            {
                PermissaoCotista permissaoCotista = new PermissaoCotista();
                if (!permissaoCotista.LoadByPrimaryKey(idUsuarioCarteira, cotista.IdCotista.Value))
                {
                    permissaoCotista.IdCotista = cotista.IdCotista.Value;
                    permissaoCotista.IdUsuario = idUsuarioCarteira;
                    permissaoCotista.Save();
                }
            }
            //

            //Trata acesso ao cliente (carteira) em PermissaoCliente para o usuário do cotista
            if (login != "")
            {
                Usuario usuarioCotista = new Usuario();
                usuarioCotista.BuscaUsuario(login);
                int idUsuarioCotista = usuarioCotista.IdUsuario.Value;

                PermissaoCliente permissaoCliente = new PermissaoCliente();
                if (!permissaoCliente.LoadByPrimaryKey(idUsuarioCotista, idCarteira))
                {
                    permissaoCliente.IdCliente = idCarteira;
                    permissaoCliente.IdUsuario = idUsuarioCotista;
                    permissaoCliente.Save();
                }
            }
        }
        #endregion

        #region IntegraCotistaItau
        bool integraCotistaItau = WebConfigurationManager.AppSettings["IntegraCotistaItau"] != null ?
            Convert.ToBoolean(WebConfigurationManager.AppSettings["IntegraCotistaItau"]) : false;
        if (integraCotistaItau)
        {
            Itau itau = new Itau();
            if (String.IsNullOrEmpty(cotista.CodigoInterface) || Convert.ToInt64(cotista.CodigoInterface) == 0)
            {
                //tentar incluir novamente e armazenar retorno
                cotista.CodigoInterface = itau.EnviaCac_XML(idCotista);
                cotista.Save();
            }
            else
            {
                //apenas submeter alterações
                itau.EnviaCac_XML(idCotista);
            }
        }
        #endregion

        #region Interface com CRM
        if (_integracaoCrm)
        {
            string url = WebConfigurationManager.AppSettings["IntegracaoCrmService.ClienteWebService"];

            string emailInterfaceCrm = WebConfigurationManager.AppSettings["EmailIntegracaoCrm"];
            string senhaInterfaceCrm = WebConfigurationManager.AppSettings["SenhaIntegracaoCrm"];

            Pessoa pessoa = new Pessoa();
            pessoa.LoadByPrimaryKey(idCotista);
            pessoa.IntegraCrm(url, emailInterfaceCrm, senhaInterfaceCrm);
        }
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {

        #region Insert
        TextBox textApelido = gridCadastro.FindEditFormTemplateControl("textApelido") as TextBox;
        //
        ASPxComboBox dropStatusAtivo = gridCadastro.FindEditFormTemplateControl("dropStatusAtivo") as ASPxComboBox;
        ASPxComboBox dropIsentoIR = gridCadastro.FindEditFormTemplateControl("dropIsentoIR") as ASPxComboBox;
        ASPxComboBox dropIsentoIOF = gridCadastro.FindEditFormTemplateControl("dropIsentoIOF") as ASPxComboBox;
        ASPxComboBox dropTipoCotista = gridCadastro.FindEditFormTemplateControl("dropTipoCotista") as ASPxComboBox;
        ASPxComboBox dropTipoCotistaAnbima = gridCadastro.FindEditFormTemplateControl("dropTipoCotistaAnbima") as ASPxComboBox;
        ASPxComboBox dropTipoTributacao = gridCadastro.FindEditFormTemplateControl("dropTipoTributacao") as ASPxComboBox;
        TextBox textCodigoInterface = gridCadastro.FindEditFormTemplateControl("textCodigoInterface") as TextBox;
        ASPxDateEdit textDataExpiracao = gridCadastro.FindEditFormTemplateControl("textDataExpiracao") as ASPxDateEdit;
        TextBox textPendenciaCadastral = gridCadastro.FindEditFormTemplateControl("textPendenciaCadastral") as TextBox;
        ASPxSpinEdit textIdClienteEspelho = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxTextBox textNomeCotista = gridCadastro.FindEditFormTemplateControl("textNomeCotista") as ASPxTextBox;
        ASPxTextBox textApelidoCotista = gridCadastro.FindEditFormTemplateControl("textApelidoCotista") as ASPxTextBox;
        ASPxTextBox textIdCotista = gridCadastro.FindEditFormTemplateControl("textIdCotista") as ASPxTextBox;
        ASPxSpinEdit btnEditCodigoPessoa = gridCadastro.FindEditFormTemplateControl("btnEditCodigoPessoa") as ASPxSpinEdit;
        ASPxCheckBox chkOffShore = gridCadastro.FindEditFormTemplateControl("chkOffShore") as ASPxCheckBox;
        ASPxTextBox textCodigoConsolidacaoExterno = gridCadastro.FindEditFormTemplateControl("textCodigoConsolidacaoExterno") as ASPxTextBox;
        ASPxComboBox dropCarteiraAssociada = gridCadastro.FindEditFormTemplateControl("dropCarteiraAssociada") as ASPxComboBox;
        //        
        Cotista cotista = new Cotista();
        //

        int idCotista = Convert.ToInt32(textIdCotista.Text);

        Pessoa pessoa = new Pessoa();
        pessoa.LoadByPrimaryKey(Convert.ToInt32(btnEditCodigoPessoa.Text));

        cotista.IdCotista = idCotista;

        if (string.IsNullOrEmpty(textNomeCotista.Text))
        cotista.Nome = pessoa.Nome;
        else
            cotista.Nome = textNomeCotista.Text;

        if (string.IsNullOrEmpty(textApelidoCotista.Text))
        cotista.Apelido = pessoa.Apelido;
        else
            cotista.Apelido = textApelidoCotista.Text;

        cotista.IdPessoa = pessoa.IdPessoa;
        //
        cotista.StatusAtivo = Convert.ToByte(dropStatusAtivo.SelectedItem.Value);
        cotista.IsentoIR = Convert.ToString(dropIsentoIR.SelectedItem.Value);
        cotista.IsentoIOF = Convert.ToString(dropIsentoIOF.SelectedItem.Value);
        cotista.TipoTributacao = Convert.ToByte(dropTipoTributacao.SelectedItem.Value);
        cotista.CodigoInterface = textCodigoInterface.Text.Trim();

        if (textDataExpiracao.Text != "")
        {
            cotista.DataExpiracao = Convert.ToDateTime(textDataExpiracao.Value);
        }
        else
        {
            cotista.DataExpiracao = null;
        }

        cotista.PendenciaCadastral = textPendenciaCadastral.Text.Trim();

        if (dropTipoCotista.SelectedIndex != -1)
        {
            cotista.TipoCotistaCVM = Convert.ToInt32(dropTipoCotista.SelectedItem.Value);
        }

        if (dropTipoCotistaAnbima.SelectedIndex != -1)
        {
            cotista.TipoCotistaAnbima = Convert.ToInt32(dropTipoCotistaAnbima.SelectedItem.Value);
        }

        if (textIdClienteEspelho.Value != null)
        {
            cotista.IdClienteEspelho = Convert.ToInt32(textIdClienteEspelho.Value);
        }

        if (dropCarteiraAssociada.SelectedIndex != -1)
            cotista.IdCarteira = Convert.ToInt32(dropCarteiraAssociada.SelectedItem.Value);
        else
            cotista.IdCarteira = null;
        #endregion

        cotista.OffShore = chkOffShore.Checked ? "S" : "N";

        if (!string.IsNullOrEmpty(textCodigoConsolidacaoExterno.Text))
            cotista.CodigoConsolidacaoExterno = textCodigoConsolidacaoExterno.Text;
        else
            cotista.CodigoConsolidacaoExterno = null;

        #region Saves
        if (cotista.IdCotista.HasValue)
        {
            //Verifica se o Id já existe 
            bool IdCotistaNovo = true;
            do
            {
                try
                {
                    cotista.Save();
                    IdCotistaNovo = true;
                }
                catch (Exception)
                {
                    IdCotistaNovo = false;
                    cotista.IdCotista = cotista.IdCotista + 1;
                }
            } while (IdCotistaNovo.Equals(false));

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de Cotista - Operacao: Insert Cotista: " + cotista.IdCotista + UtilitarioWeb.ToString(cotista),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }
        #endregion

        #region Se tem permisão automática para usuários de back/admin, salva a permissão
        bool permissaoInternoAuto = ParametrosConfiguracaoSistema.Outras.PermissaoInternoAuto;

        if (permissaoInternoAuto)
        {
            GrupoUsuarioQuery grupoUsuarioQuery = new GrupoUsuarioQuery("G");
            UsuarioQuery usuarioQuery = new UsuarioQuery("U");

            usuarioQuery.InnerJoin(grupoUsuarioQuery).On(grupoUsuarioQuery.IdGrupo == usuarioQuery.IdGrupo);
            usuarioQuery.Where(grupoUsuarioQuery.TipoPerfil.In((byte)TipoPerfilGrupo.Administrador, (byte)TipoPerfilGrupo.BackOffice));

            UsuarioCollection usuarioCollection = new UsuarioCollection();
            usuarioCollection.Load(usuarioQuery);


            PermissaoCotistaCollection permissaoCotistaCollection = new PermissaoCotistaCollection();
            foreach (Usuario usuarioPermissao in usuarioCollection)
            {
                int idUsuario = usuarioPermissao.IdUsuario.Value;

                PermissaoCotista permissaoCotista = new PermissaoCotista();
                permissaoCotista.IdUsuario = idUsuario;
                permissaoCotista.IdCotista = cotista.IdCotista.Value;
                permissaoCotistaCollection.AttachEntity(permissaoCotista);
            }
            permissaoCotistaCollection.Save();
        }
        #endregion

        #region Cadastra novo usuário associado ao cotista, cria PermissaoCotista associada ao usuário cadastrado
        TextBox textLogin = gridCadastro.FindEditFormTemplateControl("textLogin") as TextBox;
        string login = textLogin.Text.Trim();
        int idUsuarioNovo = 0;

        if (!String.IsNullOrEmpty(login))
        {
            //Busca grupo que deve ser associado ao novo login
            int? idGrupo = null;
            string descricaoGrupo = WebConfig.AppSettings.GrupoCotista;
            GrupoUsuarioCollection grupoUsuarioCollection = new GrupoUsuarioCollection();
            grupoUsuarioCollection.Query.Select(grupoUsuarioCollection.Query.IdGrupo);
            grupoUsuarioCollection.Query.Where(grupoUsuarioCollection.Query.Descricao.Equal(descricaoGrupo));
            if (grupoUsuarioCollection.Query.Load())
            {
                idGrupo = grupoUsuarioCollection[0].IdGrupo.Value;
            }

            if (idGrupo.HasValue)
            {
                //Remover usuario se ja existir outro com mesmo login associado
                if (Financial.WebConfigConfiguration.WebConfig.AppSettings.Cliente.ToUpper() == "PORTOPAR")
                {
                    Usuario usuarioRemover = new Usuario();
                    if (usuarioRemover.BuscaUsuario(login))
                    {
                        try
                        {
                            usuarioRemover.MarkAsDeleted();
                            usuarioRemover.Save();
                        }
                        catch
                        {
                        }
                    }
                }

                Usuario usuario = new Usuario();
                string email = RetornaEmailCustomizado();
                usuario.Save(idGrupo.Value, textApelido.Text, login, login, email, "S", (byte)TipoTravaUsuario.TravaCotista, null);

                idUsuarioNovo = usuario.IdUsuario.Value;

                PermissaoCotista permissaoCotista = new PermissaoCotista();
                permissaoCotista.IdCotista = cotista.IdCotista.Value;
                permissaoCotista.IdUsuario = usuario.IdUsuario.Value;
                permissaoCotista.Save();

                PropagaPermissaoTemplateCotista(cotista.IdCotista.Value, idUsuarioNovo);
            }
        }
        #endregion

        #region Trata associação cart (acesso ao cotista em PermissaoCotista e ao usuário do cotista em PermissaoCliente)       
        if (dropCarteiraAssociada.SelectedIndex != -1)
        {
            int idCarteira = Convert.ToInt32(dropCarteiraAssociada.SelectedItem.Value);

            //Trata acesso ao cotista em PermissaoCotista para o usuário vinculado à carteira
            PermissaoCliente permissaoClienteConsulta = new PermissaoCliente();
            int idUsuarioCarteira = (permissaoClienteConsulta.RetornaIdUsuarioTravaCliente(idCarteira));

            if (idUsuarioCarteira != 0)
            {
                PermissaoCotista permissaoCotista = new PermissaoCotista();
                permissaoCotista.IdCotista = cotista.IdCotista.Value;
                permissaoCotista.IdUsuario = idUsuarioCarteira;
                permissaoCotista.Save();
            }
            //

            //Trata acesso ao cliente (carteira) em PermissaoCliente para o usuário do cotista criado acima
            if (login != "")
            {
                PermissaoCliente permissaoCliente = new PermissaoCliente();
                permissaoCliente.IdCliente = idCarteira;
                permissaoCliente.IdUsuario = idUsuarioNovo;
                permissaoCliente.Save();
            }
        }
        #endregion

        #region CAC_XML
        bool integraCotistaItau = WebConfigurationManager.AppSettings["IntegraCotistaItau"] != null ?
            Convert.ToBoolean(WebConfigurationManager.AppSettings["IntegraCotistaItau"]) : false;
        if (integraCotistaItau)
        {
            Itau itau = new Itau();
            cotista.CodigoInterface = itau.EnviaCac_XML(idCotista);
            cotista.Save();
        }
        #endregion


            string url = WebConfigurationManager.AppSettings["IntegracaoCrmService.ClienteWebService"];

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            #region Delete
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdCotista");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idCotista = Convert.ToInt32(keyValuesId[i]);

                Cotista cotista = new Cotista();
                if (cotista.LoadByPrimaryKey(idCotista))
                {
                    bool achou = false;
                    PosicaoCotistaAberturaCollection posicaoCotistaAberturaCollection = new PosicaoCotistaAberturaCollection();
                    posicaoCotistaAberturaCollection.Query.Where(posicaoCotistaAberturaCollection.Query.IdCotista.Equal(idCotista));
                    if (posicaoCotistaAberturaCollection.Query.Load())
                    {
                        achou = true;
                    }
                    else
                    {
                        OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();
                        operacaoCotistaCollection.Query.Where(operacaoCotistaCollection.Query.IdCotista.Equal(idCotista));
                        if (operacaoCotistaCollection.Query.Load())
                        {
                            achou = true;
                        }
                    }

                    if (achou)
                    {
                        throw new Exception("Cotista " + cotista.Nome + " não pode ser excluído por ter posições e/ou movimentos relacionadas.");
                    }

                    //Deleta todos os usuários de PermissaoCotista associados com idCotista e TipoTrava = TravaCotista
                    PermissaoCotistaQuery permissaoCotistaQuery = new PermissaoCotistaQuery("P");
                    UsuarioQuery usuarioQuery = new UsuarioQuery("U");

                    permissaoCotistaQuery.InnerJoin(usuarioQuery).On(usuarioQuery.IdUsuario == permissaoCotistaQuery.IdUsuario);
                    permissaoCotistaQuery.Where(permissaoCotistaQuery.IdCotista.Equal(idCotista));
                    permissaoCotistaQuery.Where(usuarioQuery.TipoTrava.Equal((byte)TipoTravaUsuario.TravaCotista));

                    UsuarioCollection usuarioCollectionDeletar = new UsuarioCollection();
                    PermissaoCotistaCollection permissaoCotistaCollection = new PermissaoCotistaCollection();
                    permissaoCotistaCollection.Load(permissaoCotistaQuery);

                    foreach (PermissaoCotista permissaoCotista in permissaoCotistaCollection)
                    {
                        int idUsuario = permissaoCotista.IdUsuario.Value;
                        Usuario usuario = new Usuario();
                        usuario.LoadByPrimaryKey(idUsuario);
                        usuarioCollectionDeletar.AttachEntity(usuario);
                    }
                    usuarioCollectionDeletar.MarkAllAsDeleted();
                    usuarioCollectionDeletar.Save();
                    //

                    //TransferenciaCota (não tem Cascade)
                    TransferenciaCotaCollection transferenciaCotaCollection = new TransferenciaCotaCollection();
                    transferenciaCotaCollection.Query.Where(transferenciaCotaCollection.Query.IdCotistaOrigem.Equal(idCotista) |
                                                            transferenciaCotaCollection.Query.IdCotistaDestino.Equal(idCotista));
                    transferenciaCotaCollection.Query.Load();
                    transferenciaCotaCollection.MarkAllAsDeleted();
                    transferenciaCotaCollection.Save();
                    //

                    //TemplateCotista (não tem Cascade)
                    TemplateCotistaCollection templateCotistaCollection = new TemplateCotistaCollection();
                    templateCotistaCollection.Query.Where(templateCotistaCollection.Query.IdCotista.Equal(idCotista));
                    templateCotistaCollection.Query.Load();
                    templateCotistaCollection.MarkAllAsDeleted();
                    templateCotistaCollection.Save();
                    //
                    //DetalheResgateCotista (não tem Cascade)
                    DetalheResgateCotistaCollection d = new DetalheResgateCotistaCollection();
                    d.Query.Where(d.Query.IdCotista == idCotista);
                    d.Query.Load();
                    d.MarkAllAsDeleted();
                    d.Save();

                    //ContaCorrente
                    ContaCorrenteCollection contaCorrenteColl = new ContaCorrenteCollection();
                    contaCorrenteColl.Query.Where(contaCorrenteColl.Query.IdCotista.Equal(idCotista));
                    if (contaCorrenteColl.Query.Load())
                    {
                        foreach (ContaCorrente conta in contaCorrenteColl)
                        {
                            conta.IdCotista = null;
                            conta.ContaDefaultCotista = "N";
                        }

                        contaCorrenteColl.Save();
                    }
                    //
                    Cotista cotistaClone = (Cotista)Utilitario.Clone(cotista);
                    //

                    cotista.MarkAsDeleted();
                    cotista.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de Cotista - Operacao: Delete Cotista: " + idCotista + UtilitarioWeb.ToString(cotistaClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
            #endregion
        }
        else if (e.Parameters == "btnAtivar")
        {
            #region Ativa Cotista

            List<object> keyValuesId = this.gridCadastro.GetSelectedFieldValues(CotistaMetadata.ColumnNames.IdCotista);
            //
            // Se tiver cotista Selecionado
            if (keyValuesId.Count != 0)
            {
                #region Cotista
                CotistaCollection cotistaCollection = new CotistaCollection();
                cotistaCollection.Query.Select(cotistaCollection.Query.IdCotista, cotistaCollection.Query.StatusAtivo)
                                  .Where(cotistaCollection.Query.IdCotista.In(keyValuesId));

                cotistaCollection.Query.Load();

                for (int i = 0; i < cotistaCollection.Count; i++)
                {
                    cotistaCollection[i].StatusAtivo = (byte)StatusAtivoCotista.Ativo;
                }
                #endregion

                cotistaCollection.Save();

                foreach (Cotista cotista in cotistaCollection)
                {
                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cotista - Operacao: Ativação: " + cotista.IdCotista + ";" + UtilitarioWeb.ToString(cotista),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
            #endregion
        }
        else if (e.Parameters == "btnDesativar")
        {
            #region Desativa Cotista

            List<object> keyValuesId = this.gridCadastro.GetSelectedFieldValues(CotistaMetadata.ColumnNames.IdCotista);
            //
            // Se tiver cotista Selecionado
            if (keyValuesId.Count != 0)
            {
                #region Cotista
                CotistaCollection cotistaCollection = new CotistaCollection();
                cotistaCollection.Query.Select(cotistaCollection.Query.IdCotista, cotistaCollection.Query.StatusAtivo)
                                  .Where(cotistaCollection.Query.IdCotista.In(keyValuesId));

                cotistaCollection.Query.Load();

                for (int i = 0; i < cotistaCollection.Count; i++)
                {
                    cotistaCollection[i].StatusAtivo = (byte)StatusAtivoCotista.Inativo;
                }
                #endregion

                cotistaCollection.Save();

                foreach (Cotista cotista in cotistaCollection)
                {
                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cotista - Operacao: Desativação: " + cotista.IdCotista + ";" + UtilitarioWeb.ToString(cotista),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
            #endregion
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e)
    {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing)
        {
            ASPxSpinEdit btnEditCodigoPessoa = gridCadastro.FindEditFormTemplateControl("btnEditCodigoPessoa") as ASPxSpinEdit;
            e.Properties["cpBtnEditCodigoPessoa"] = btnEditCodigoPessoa.ClientID;

            ASPxComboBox dropStatusAtivo = gridCadastro.FindEditFormTemplateControl("dropStatusAtivo") as ASPxComboBox;
            e.Properties["cpDropStatusAtivo"] = dropStatusAtivo.ClientID;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e)
    {
        this.FocaCampoGrid("btnEditCodigoPessoa", "dropStatusAtivo");
        base.gridCadastro_PreRender(sender, e);
    }

    protected void callBackIdCotista_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        #region Busca Max Value Cotista
        Cotista cotista = new Cotista();
        cotista.Query.Select(cotista.Query.IdCotista.Max());
        int idCotista = 1;
        if (cotista.Query.Load())
            idCotista = cotista.IdCotista.GetValueOrDefault(0) + idCotista;
        #endregion

        e.Result = idCotista.ToString();
    }

    protected void callbackChkOffShore_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "N";

        ASPxTextBox textIdCotista = gridCadastro.FindEditFormTemplateControl("textIdCotista") as ASPxTextBox;

        if (textIdCotista != null && !string.IsNullOrEmpty(textIdCotista.Text))
        {
            int idCotista = Convert.ToInt32(textIdCotista.Text);

            Cotista cotista = new Cotista();
            cotista.LoadByPrimaryKey(idCotista);

            e.Result = cotista.OffShore;
        }
    }
}