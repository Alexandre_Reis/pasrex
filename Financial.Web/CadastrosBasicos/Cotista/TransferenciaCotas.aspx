﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TransferenciaCotas.aspx.cs" Inherits="CadastrosBasicos_TransferenciaCotas" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>  
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">

    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    var popup = true;    
    document.onkeydown=onDocumentKeyDown;
    var operacao = '';
            
    function OnGetDataCotista(data) 
    {
        popupCotista.HideWindow();
        
        if (cotista == 'origem')
        {   
            btnEditCodigoCotistaOrigem.SetValue(data);
            ASPxCallback1.SendCallback(btnEditCodigoCotistaOrigem.GetValue());
            btnEditCodigoCotistaOrigem.Focus();
        }
        else
        {
            btnEditCodigoCotistaDestino.SetValue(data);
            ASPxCallback1.SendCallback(btnEditCodigoCotistaDestino.GetValue());
            btnEditCodigoCotistaDestino.Focus();
        }
    }        
    function OnGetDataCarteira(data) {
        btnEditCodigoCarteira.SetValue(data);        
        ASPxCallback2.SendCallback(btnEditCodigoCarteira.GetValue());
        popupCarteira.HideWindow();
        btnEditCodigoCarteira.Focus();
    }    
    function OnGetDataCarteiraFiltro(data) {
        btnEditCodigoCarteiraFiltro.SetValue(data);        
        ASPxCallback2.SendCallback(btnEditCodigoCarteiraFiltro.GetValue());
        popupCarteira.HideWindow();
        btnEditCodigoCarteiraFiltro.Focus();
    }    
    function ClearTextObservacao()
    {
        var textObservacao = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textObservacao');
        textObservacao.value = '';                                                                                            
    }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {
                alert(e.result);             
            }
            else
            {
                if (operacao == 'deletar')
                {
                    gridCadastro.PerformCallback('btnDelete');
                }
                else
                {                
                    if (operacao == 'salvar')
                    {             
                        gridCadastro.UpdateEdit();
                    }
                    else
                    {
                        callbackAdd.SendCallback();
                    }                       
                }                
            }
            
            operacao = '';
        }        
        "/>
    </dxcb:ASPxCallback> 
        <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            alert('Operação feita com sucesso.');
        }        
        " />
        </dxcb:ASPxCallback>   
    
    <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {            
            var textNomeCotista;
            
            if (cotista == 'origem')
            {
                textNomeCotista = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCotistaOrigem');;
                OnCallBackCompleteCliente(s, e, popupMensagemCotista, btnEditCodigoCotistaOrigem, textNomeCotista);
            }
            else
            {
                textNomeCotista = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCotistaDestino');;
                OnCallBackCompleteCliente(s, e, popupMensagemCotista, btnEditCodigoCotistaDestino, textNomeCotista);
            }            
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="ASPxCallback2" runat="server" OnCallback="ASPxCallback2_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) { 
            var resultSplit = e.result.split('|');
            if (gridCadastro.cp_EditVisibleIndex == -1)
            {                                  
                e.result = resultSplit[0];
                var textNomeCarteiraFiltro = document.getElementById('popupFiltro_textNomeCarteiraFiltro');
                OnCallBackCompleteCliente(s, e, popupMensagemCarteira, btnEditCodigoCarteiraFiltro, textNomeCarteiraFiltro);
            }
            else
            {
                var textNomeCarteira = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCarteira');
                OnCallBackCompleteCliente(s, e, popupMensagemCarteira, btnEditCodigoCarteira, textNomeCarteira, textData);
                
                if (resultSplit[1] != null)            
                {   
                    //var dataAux = resultSplit[1].split('/');
                    //var newDate = new Date(dataAux[2], dataAux[1] - 1, dataAux[0]);
                                       
                    var newDate = LocalizedData(resultSplit[1], resultSplit[2]);                                        
                    textData.SetValue(newDate);                             
                }
                else
                {
                    textData.SetValue(null);
                }                                    
            }
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">    
    
    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Transferência de Cotas"></asp:Label>
    </div>
        
    <div id="mainContent">
  
            <dxpc:ASPxPopupControl ID="popupFiltro" AllowDragging="true" PopupElementID="popupFiltro" EnableClientSideAPI="True"                    
                                    PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" CloseAction="CloseButton" 
                                    Width="400" Left="250" Top="70" HeaderText="Filtros adicionais para consulta" runat="server"
                                    HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">            
                <ContentCollection><dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                    
                    <table>
                        <tr>
                            <td class="td_Label_Longo">
                                <asp:Label ID="labelCarteiraFiltro" runat="server" CssClass="labelNormal" Text="Carteira:"></asp:Label>
                            </td>        
                            
                            <td>                                                                                                                
                                <dxe:ASPxSpinEdit ID="btnEditCodigoCarteiraFiltro" runat="server" CssClass="textButtonEdit" 
                                            ClientInstanceName="btnEditCodigoCarteiraFiltro" MaxLength="10" NumberType="Integer">            
                                <Buttons>                                           
                                    <dxe:EditButton>
                                    </dxe:EditButton>                                
                                </Buttons>       
                                <ClientSideEvents                                                           
                                         KeyPress="function(s, e) {document.getElementById('popupFiltro_textNomeCarteiraFiltro').value = '';} " 
                                         ButtonClick="function(s, e) {popupCarteira.ShowAtElementByID(s.name);}" 
                                         LostFocus="function(s, e) {OnLostFocus(popupMensagemCarteira, ASPxCallback2, btnEditCodigoCarteiraFiltro);}"
                                        />               
                                </dxe:ASPxSpinEdit>
                            </td>
                        
                            <td colspan="2" width="450">
                                <asp:TextBox ID="textNomeCarteiraFiltro" runat="server" CssClass="textNome" Enabled="false" ></asp:TextBox>
                            </td>
                        </tr>        
                        
                        <tr>
                            <td>                
                                <asp:Label ID="labelDataInicio" runat="server" CssClass="labelNormal" Text="Início:"></asp:Label>
                            </td>    
                                                
                            <td>
                                <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" />
                            </td>  
                            
                            <td colspan="2">
                            <table>
                                <tr><td><asp:Label ID="labelDataFim" runat="server" CssClass="labelNormal" Text="Fim:"/></td>
                                <td><dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim"/></td></tr>
                            </table>
                            </td>
                        </tr>
                    </table>        
                    
                    <div class="linkButton linkButtonNoBorder" style="margin-top:20px">              
                        <asp:LinkButton ID="btnOKFilter" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnOK" OnClientClick="popupFiltro.Hide(); gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal7" runat="server" Text="Aplicar"/><div></div></asp:LinkButton>
                        <asp:LinkButton ID="btnFilterCancel" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnFilterCancel" OnClientClick="OnClearFilterClick_FiltroDatas(); return false;"><asp:Literal ID="Literal1" runat="server" Text="Limpar"/><div></div></asp:LinkButton>
                    </div>                    
                </dxpc:PopupControlContentControl></ContentCollection>                             
            </dxpc:ASPxPopupControl>        
    
            <div class="linkButton" >               
               <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal3" runat="server" Text="Novo"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) {operacao='deletar'; callbackErro.SendCallback();} return false;"><asp:Literal ID="Literal4" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnFilter" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnFilter" OnClientClick="return OnButtonClick_FiltroDatas()"><asp:Literal ID="Literal5" runat="server" Text="Filtro"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal6" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal8" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal9" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>               
            </div>
        
            <div class="divDataGrid">
                <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                        KeyFieldName="IdTransferencia" DataSourceID="EsDSTransferenciaCotas"
                        OnRowUpdating="gridCadastro_RowUpdating"
                        OnRowInserting="gridCadastro_RowInserting"
                        OnCustomCallback="gridCadastro_CustomCallback"
                        OnPreRender="gridCadastro_PreRender"
                        OnCustomJSProperties="gridCadastro_CustomJSProperties"        
                        OnBeforeGetCallbackResult="gridCadastro_PreRender"                                   
                        >     
                    
                    <Columns>                    
                                             
                        <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                            <HeaderTemplate>
                                <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                            </HeaderTemplate>
                        </dxwgv:GridViewCommandColumn>
                        
                        <dxwgv:GridViewDataColumn FieldName="IdCarteira" VisibleIndex="1" Width="8%" CellStyle-HorizontalAlign="left"/>
                        <dxwgv:GridViewDataColumn FieldName="ApelidoCotistaOrigem" Caption="Cotista Origem" VisibleIndex="3" Width="16%" Settings-AutoFilterCondition="Contains" />
                        <dxwgv:GridViewDataColumn FieldName="ApelidoCotistaDestino" Caption="Cotista Destino" VisibleIndex="4" Width="16%" Settings-AutoFilterCondition="Contains" />                                                                    
                        <dxwgv:GridViewDataDateColumn FieldName="DataTransferencia" Caption="Data" VisibleIndex="5" Width="8%"/>
                        
                        <dxwgv:GridViewDataComboBoxColumn FieldName="Tipo" VisibleIndex="6" Width="13%" ExportWidth="140">
                            <PropertiesComboBox EncodeHtml="false">
                                <Items>
                                    <dxe:ListEditItem Value="1" Text="<div title='Integral'>Integral</div>" />
                                    <dxe:ListEditItem Value="2" Text="<div title='Posições (Qtde)'>Posições (Qtde)</div>" />
                                    <dxe:ListEditItem Value="3" Text="<div title='Posições (%)'>Posições (%)</div>" />
                                </Items>
                            </PropertiesComboBox>
                        </dxwgv:GridViewDataComboBoxColumn>
                        
                        <dxwgv:GridViewDataTextColumn FieldName="Quantidade" VisibleIndex="9" Width="15%"                                 
                                HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right"
                                CellStyle-HorizontalAlign="Right">                    
                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}"></PropertiesTextEdit>                    
                        </dxwgv:GridViewDataTextColumn>
                        
                        <dxwgv:GridViewDataTextColumn FieldName="Taxa" VisibleIndex="10" Width="15%"
                                HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right"
                                CellStyle-HorizontalAlign="Right">                    
                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.0000000000;(#,##0.0000000000);0.0000000000}"></PropertiesTextEdit>                    
                        </dxwgv:GridViewDataTextColumn>
                        
                        <dxwgv:GridViewDataColumn FieldName="IdCotistaOrigem" Visible="false">
                        </dxwgv:GridViewDataColumn>
                        <dxwgv:GridViewDataColumn FieldName="IdCotistaDestino" Visible="false">
                        </dxwgv:GridViewDataColumn>
                        
                    </Columns>
                    
                    <Templates>            
                    <EditForm>                          
                        <asp:Label ID="labelEdicao" runat="server" CssClass="labelInformation" Text=""></asp:Label>
                        
                        <asp:Panel ID="panelEdicao" runat="server" OnLoad="panelEdicao_Load">
                        
                        <div class="editForm">                            
                            
                            <table>
                                <tr>
                                    <td class="td_Label">
                                        <asp:Label ID="labelCarteira" runat="server" CssClass="labelRequired" Text="Carteira:"></asp:Label>
                                    </td>        
                                    
                                    <td>                                        
                                        <dxe:ASPxSpinEdit ID="btnEditCodigoCarteira" runat="server" CssClass="textButtonEdit" 
                                                    ClientInstanceName="btnEditCodigoCarteira" Text='<%#Eval("IdCarteira")%>'                                                    
                                                    MaxLength="10" NumberType="Integer">            
                                        <Buttons>                                           
                                            <dxe:EditButton>
                                            </dxe:EditButton>                                
                                        </Buttons>       
                                        <ClientSideEvents                                                           
                                                 KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCarteira').value = '';} " 
                                                 ButtonClick="function(s, e) {popupCarteira.ShowAtElementByID(s.name);}" 
                                                 LostFocus="function(s, e) {OnLostFocus(popupMensagemCarteira, ASPxCallback2, btnEditCodigoCarteira);}"
                                                />               
                                        </dxe:ASPxSpinEdit>
                                    </td>
                                
                                    <td>
                                        <asp:TextBox ID="textNomeCarteira" runat="server" CssClass="textNome" Enabled="false" Text='<%#Eval("Apelido")%>' ></asp:TextBox>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td class="td_Label">
                                        <asp:Label ID="labelCotista" runat="server" CssClass="labelRequired" Text="De:"></asp:Label>
                                    </td>
                                    
                                    <td>
                                        <dxe:ASPxSpinEdit ID="btnEditCodigoCotistaOrigem" runat="server" CssClass="textButtonEdit" 
                                                            ClientInstanceName="btnEditCodigoCotistaOrigem" Text='<%#Eval("IdCotistaOrigem")%>'>  
                                        <Buttons>
                                            <dxe:EditButton>
                                            </dxe:EditButton>                                
                                        </Buttons>     
                                        <ClientSideEvents
                                                 KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCotistaOrigem').value = '';}" 
                                                 ButtonClick="function(s, e) {cotista = 'origem'; popupCotista.ShowAtElementByID(s.name);}" 
                                                 LostFocus="function(s, e) {cotista = 'origem'; OnLostFocus(popupMensagemCotista, ASPxCallback1, btnEditCodigoCotistaOrigem);}"
                                        />                               
                                        </dxe:ASPxSpinEdit>                
                                    </td>                          
                                    
                                    <td>
                                        <asp:TextBox ID="textNomeCotistaOrigem" runat="server" CssClass="textNome" Enabled="false" Text='<%#Eval("ApelidoCotistaOrigem")%>'></asp:TextBox>                
                                    </td>
                                </tr>
                                    
                                <tr>
                                    <td class="td_Label">
                                        <asp:Label ID="label1" runat="server" CssClass="labelRequired" Text="Para:"></asp:Label>
                                    </td>
                                    
                                    <td>
                                        <dxe:ASPxSpinEdit ID="btnEditCodigoCotistaDestino" runat="server" CssClass="textButtonEdit" 
                                                            ClientInstanceName="btnEditCodigoCotistaDestino" Text='<%#Eval("IdCotistaDestino")%>'>  
                                        <Buttons>
                                            <dxe:EditButton>
                                            </dxe:EditButton>                                
                                        </Buttons>        
                                        <ClientSideEvents
                                                 KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCotistaDestino').value = '';}" 
                                                 ButtonClick="function(s, e) {cotista = 'destino'; popupCotista.ShowAtElementByID(s.name);}" 
                                                 LostFocus="function(s, e) {cotista = 'destino'; OnLostFocus(popupMensagemCotista, ASPxCallback1, btnEditCodigoCotistaDestino);}"
                                        />                            
                                        </dxe:ASPxSpinEdit>                
                                    </td>                          
                                    
                                    <td>
                                        <asp:TextBox ID="textNomeCotistaDestino" runat="server" CssClass="textNome" Enabled="false" Text='<%#Eval("ApelidoCotistaDestino")%>'></asp:TextBox>                
                                    </td>
                                </tr>                
                          
                                <tr>
                                    <td  class="td_Label">
                                        <asp:Label ID="labelData" runat="server" CssClass="labelRequired" Text="Data:" />
                                    </td> 
                                    
                                    <td colspan="2">
                                        <dxe:ASPxDateEdit ID="textData" runat="server" ClientInstanceName="textData" ClientEnabled="False" Value='<%#Eval("DataTransferencia")%>'/>					                    
				                    </td> 
				                </tr>      
				                
				                <tr>
                                    <td class="td_Label">
                                        <asp:Label ID="labelTipo" runat="server" CssClass="labelRequired" Text="Tipo:"></asp:Label>                    
                                    </td>                    
                                    <td colspan="2">
                                        <dxe:ASPxComboBox ID="dropTipo" runat="server" 
                                                            ShowShadow="false" DropDownStyle="DropDownList"
                                                            CssClass="dropDownList" Text='<%#Eval("Tipo")%>'>
                                        <Items>
                                        <dxe:ListEditItem Value="1" Text="Integral" />
                                        <dxe:ListEditItem Value="2" Text="Posições (Qtde)" />
                                        <dxe:ListEditItem Value="3" Text="Posições (%)" />
                                        </Items>                            
                                        <ClientSideEvents
                                                 SelectedIndexChanged="function(s, e) { 
                                                                            if (s.GetSelectedIndex() == 0)
                                                                                { textQuantidade.SetText(''); textQuantidade.SetEnabled(false); }
                                                                            else
                                                                                { textQuantidade.SetEnabled(true); }    
                                                                            }" />                        
                                        </dxe:ASPxComboBox>                 
                                    </td>
                                </tr>                                                      
                                
                                <tr>
                                    <td  class="td_Label">
                                        <asp:Label ID="labelQuantidade" runat="server" CssClass="labelNormal" Text="Qtde:"></asp:Label>
                                    </td>
                                    <td colspan="2">
                                        <dxe:ASPxSpinEdit ID="textQuantidade" runat="server" CssClass="textValor" ClientInstanceName="textQuantidade"
                                                                         MaxLength="20" DecimalPlaces="8" NumberType="Float"
                                                                         Text='<%#Eval("Quantidade")%>' >                                    
                                        </dxe:ASPxSpinEdit>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td class="td_Label">
                                        <asp:Label ID="label2" runat="server" CssClass="labelNormal" Text="Taxa:"></asp:Label>
                                    </td>
                                    <td colspan="2">
                                        <dxe:ASPxSpinEdit ID="textTaxa" runat="server" CssClass="textValor" ClientInstanceName="textTaxa"
                                                                         MaxLength="20" DecimalPlaces="12" NumberType="Float"
                                                                         Text='<%#Eval("Taxa")%>' >                                    
                                        </dxe:ASPxSpinEdit>
                                    </td>
                                </tr>
                                
                            </table>
                            <div class="linhaH"></div>
                                        
                            <div class="linkButton linkButtonNoBorder popupFooter">
                                <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd" OnInit="btnOKAdd_Init"
                                       OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal5" runat="server" Text="OK+"/><div></div></asp:LinkButton>
                                <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                       OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal13" runat="server" Text="OK"/><div></div></asp:LinkButton>        
                                <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel" OnClientClick="gridCadastro.CancelEdit(); return false;"><asp:Literal ID="Literal2" runat="server" Text="Cancelar"/><div></div></asp:LinkButton>
                            </div>        
                        </div>                        
                        </asp:Panel>
                    </EditForm>
                        
                    <StatusBar>
                        <div>
                            <div style="float:left">
                            <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" ></asp:Label>
                            </div>                    
                        </div>
                    </StatusBar>
                    </Templates>
                    
                    <SettingsPopup EditForm-Width="500px"  />
                    <SettingsCommandButton>
                        <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                    </SettingsCommandButton>
                    
                </dxwgv:ASPxGridView>            
            </div>        
    </div>
    </div>
    </td></tr></table>
    </div>
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" Landscape="true"
    LeftMargin = "40"
    RightMargin = "40"
    ></dxwgv:ASPxGridViewExporter>
        
    <cc1:esDataSource ID="EsDSTransferenciaCotas" runat="server" OnesSelect="EsDSTransferenciaCotas_esSelect" LowLevelBind="true"/>    
    <cc1:esDataSource ID="EsDSCotista" runat="server" OnesSelect="EsDSCotista_esSelect" />
    <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" />
    
    </form>
</body>
</html>