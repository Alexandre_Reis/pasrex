﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BloqueioCota.aspx.cs" Inherits="CadastrosBasicos_BloqueioCota" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>  
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">

    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    var popup = true;    
    document.onkeydown=onDocumentKeyDown;
    var operacao = '';

    var selectedIndex;
    
    function OnGetDataCotista(data) {
        btnEditCodigoCotista.SetValue(data);
        ASPxCallback1.SendCallback(btnEditCodigoCotista.GetValue());
        popupCotista.HideWindow();
        btnEditCodigoCotista.Focus();
    }        
    function OnGetDataPosicaoCotista(data) {
        var resultSplit = data.split('|');        
        popupPosicaoCotista.HideWindow(); 
    }  
    function OnGetDataCarteira(data) {
        btnEditCodigoCarteira.SetValue(data);        
        ASPxCallback2.SendCallback(btnEditCodigoCarteira.GetValue());
        popupCarteira.HideWindow();
        btnEditCodigoCarteira.Focus();
    }    
    function OnGetDataCarteiraFiltro(data) {
        btnEditCodigoCarteiraFiltro.SetValue(data);        
        ASPxCallback2.SendCallback(btnEditCodigoCarteiraFiltro.GetValue());
        popupCarteira.HideWindow();
        btnEditCodigoCarteiraFiltro.Focus();
    }    
    function OnGetDataCotistaFiltro(data) {
        btnEditCodigoCotistaFiltro.SetValue(data);        
        ASPxCallback1.SendCallback(btnEditCodigoCotistaFiltro.GetValue());
        popupCotista.HideWindow();
        btnEditCodigoCotistaFiltro.Focus();
    }
    
    function FechaPopupPosicaoCotista() {
        textQuantidade.SetEnabled(false); 
        textQuantidade.SetText(''); 
        popupPosicaoCotista.HideWindow();    
        gridCadastro.CancelEdit();
        return false;     
    }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '') {
                alert(e.result);             
            }
            else {
                if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new') {             
                    gridCadastro.UpdateEdit();
                }
                else {
                    callbackAdd.SendCallback();
                }    
            }
            
            operacao = '';
        }        
        "/>
    </dxcb:ASPxCallback>    

    <dxcb:ASPxCallback ID="callbackErroPopUpPosicaoCotista" runat="server" OnCallback="callbackErroPopUpPosicaoCotista_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '') {                   
                alert(e.result);                              
            }
            else {
                gridPosicaoCotista.PerformCallback(selectedIndex);                
            }
        }        
        "/>
    </dxcb:ASPxCallback>
                
    <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {                        
            if (gridCadastro.cp_EditVisibleIndex == -1) {
                var resultSplit = e.result.split('|');
                e.result = resultSplit[0];
                var textNomeCotistaFiltro = document.getElementById('popupFiltro_textNomeCotistaFiltro');
                OnCallBackCompleteCliente(s, e, popupMensagemCotista, btnEditCodigoCotistaFiltro, textNomeCotistaFiltro);
            }
            else {
                var textNomeCotista = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCotista');
                OnCallBackCompleteCliente(s, e, popupMensagemCotista, btnEditCodigoCotista, textNomeCotista);
            }
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="ASPxCallback2" runat="server" OnCallback="ASPxCallback2_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) { 
            var resultSplit = e.result.split('|');
            if (gridCadastro.cp_EditVisibleIndex == -1) {                                  
                e.result = resultSplit[0];
                var textNomeCarteiraFiltro = document.getElementById('popupFiltro_textNomeCarteiraFiltro');
                OnCallBackCompleteCliente(s, e, popupMensagemCarteira, btnEditCodigoCarteiraFiltro, textNomeCarteiraFiltro);
            }
            else {                                                                               
                var textNomeCarteira = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCarteira');                
                OnCallBackCompleteCliente(s, e, popupMensagemCarteira, btnEditCodigoCarteira, textNomeCarteira, textData);
            }
        }        
        "/>
    </dxcb:ASPxCallback>
            
    <dxpc:ASPxPopupControl ID="popupPosicaoCotista" runat="server" Width="750px" HeaderText="" ContentStyle-VerticalAlign="Top"  
                        PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" AllowDragging="True" ShowCloseButton="false">
                                                                                              
        <ContentCollection>
        <dxpc:PopupControlContentControl runat="server">     
        <div>
            <dxwgv:ASPxGridView ID="gridPosicaoCotista" runat="server" Width="100%"
                    ClientInstanceName="gridPosicaoCotista"  AutoGenerateColumns="False" 
                    DataSourceID="EsDSPosicaoCotista" KeyFieldName="IdPosicao"
                    OnCustomCallback="gridPosicaoCotista_CustomCallback"
                    OnHtmlRowCreated="gridPosicaoCotista_HtmlRowCreated"
                    OnCustomUnboundColumnData="gridPosicaoCotista_CustomUnboundColumnData">               
            <Columns>

                <dxwgv:GridViewDataSpinEditColumn FieldName="IdPosicao" Visible="false" />

                <dxwgv:GridViewDataSpinEditColumn FieldName="QuantidadeBloqueio" VisibleIndex="1" Caption="Qtd. Bloqueio" UnboundType="String" Width="15%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right" EditCellStyle-HorizontalAlign="Right">
                    <PropertiesSpinEdit DisplayFormatString="{0:#,##0.000000000000;(#,##0.000000000000);0.000000000000}" />
                </dxwgv:GridViewDataSpinEditColumn>
                
                <dxwgv:GridViewDataSpinEditColumn FieldName="QuantidadeDesbloqueio" VisibleIndex="2" Caption="Qtd. DesBloqueio" UnboundType="String" Width="15%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right" EditCellStyle-HorizontalAlign="Right">
                    <PropertiesSpinEdit DisplayFormatString="{0:#,##0.000000000000;(#,##0.000000000000);0.000000000000}" />
                </dxwgv:GridViewDataSpinEditColumn>                
                
                <dxwgv:GridViewDataTextColumn FieldName="DataConversao" Caption="Data Conversão" VisibleIndex="3" Width="15%">                    
                    <PropertiesTextEdit EncodeHtml="false" DisplayFormatString="{0:d}"  />                    
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataSpinEditColumn FieldName="QuantidadeBloqueada" VisibleIndex="4" Width="18%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                    <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}" />
                </dxwgv:GridViewDataSpinEditColumn>
                
                <dxwgv:GridViewDataSpinEditColumn FieldName="Quantidade" VisibleIndex="5" Width="15%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">                                                                    
                    <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}"></PropertiesSpinEdit>
                </dxwgv:GridViewDataSpinEditColumn>
                                
                <dxwgv:GridViewDataSpinEditColumn FieldName="ValorBruto" VisibleIndex="6" Width="15%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">                                                                    
                            <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}" />
                </dxwgv:GridViewDataSpinEditColumn>                                
            </Columns>
                        
            <SettingsBehavior ColumnResizeMode="Disabled" AllowFocusedRow="true" />
            <SettingsPager PageSize="800" />
            <Settings ShowTitlePanel="True" ShowFilterRow="True" VerticalScrollBarMode="Visible" VerticalScrollableHeight="280" />

            <SettingsDetail ShowDetailButtons="False" />
            
            <Styles AlternatingRow-Enabled="True" Cell-Wrap="False">
                <Header ImageSpacing="5px" SortingImageSpacing="5px" />
            </Styles>

            <Images>
                <PopupEditFormWindowClose Height="17px" Width="17px" />
            </Images>
            
            <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Posições de Cotista" />
                                    
            <ClientSideEvents RowDblClick="function(s, e) {textQuantidade.SetEnabled(true); textQuantidade.SetText('');
                                    textQuantidade.Focus(); selectedIndex = e.visibleIndex; }" />	       	                        
            </dxwgv:ASPxGridView>
            
            <div>
                <table>
                    <tr>
                        <td>
                            <asp:Label ID="labelQtde" runat="server" CssClass="labelNormal" Text="Qtd. Bloqueio/Desbloqueio:" />
                        </td>
                        
                        <td>    
                            <dxe:ASPxSpinEdit ID="textQuantidade" runat="server" CssClass="textValor" ClientInstanceName="textQuantidade"
                                              MaxLength="28" MinValue="0" NumberType="Float" DecimalPlaces="12" width="150">
                            </dxe:ASPxSpinEdit>
                        </td>

                        <td>
                            <asp:Label ID="labelTipoOperacao" runat="server" CssClass="labelRequired" Text="Tipo Operação:" />
                        </td>
                        <td>
                            <dxe:ASPxComboBox ID="dropTipoOperacao" runat="server" ClientInstanceName="dropTipoOperacao" SelectedIndex="0"
                                    ShowShadow="true" DropDownStyle="DropDownList" CssClass="dropDownList">
                                <Items>
                                    <dxe:ListEditItem Value="1" Text="Bloqueio" />
                                    <dxe:ListEditItem Value="2" Text="Desbloqueio" />
                                </Items>
                            </dxe:ASPxComboBox>
                        </td>                                     
                        
                        <td>
                            <div class="linkButton linkButtonNoBorder" style="text-align:right;">                                                                                   
                                <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK" OnClientClick="callbackErroPopUpPosicaoCotista.SendCallback(); return false;"><asp:Literal ID="Literal12" runat="server" Text="Salvar"/><div></div></asp:LinkButton>
                                <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel" OnClientClick="FechaPopupPosicaoCotista(); return false;"><asp:Literal ID="Literal13" runat="server" Text="Fechar"/><div></div></asp:LinkButton>
                            </div>        
                        </td>
                    </tr>
                </table>                                
            </div>                
                                               
        </div>
              
        </dxpc:PopupControlContentControl></ContentCollection>
        
        <ClientSideEvents PopUp="function(s, e) {gridPosicaoCotista.PerformCallback(); textQuantidade.SetEnabled(false);}"
                          CloseUp="function(s, e) {FechaPopupPosicaoCotista(); return false;}" />
                                  
    </dxpc:ASPxPopupControl>
            
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">    
    
    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Operações de Bloqueio/Desbloqueio de Cotas" />
    </div>
        
    <div id="mainContent">
                  
            <dxpc:ASPxPopupControl ID="popupFiltro" AllowDragging="true" PopupElementID="popupFiltro" EnableClientSideAPI="True"                    
                                    PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" CloseAction="CloseButton" 
                                    Width="400" Left="250" Top="70" HeaderText="Filtros adicionais para consulta" runat="server"
                                    HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">            
                <ContentCollection><dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                    
                    <table>        
                        <tr>
                            <td class="td_Label_Longo">
                                <asp:Label ID="labelCarteiraFiltro" runat="server" CssClass="labelNormal" Text="Carteira:"/>
                            </td>        
                            
                            <td>                                                                                                                
                                <dxe:ASPxSpinEdit ID="btnEditCodigoCarteiraFiltro" runat="server" CssClass="textButtonEdit" 
                                            ClientInstanceName="btnEditCodigoCarteiraFiltro" MaxLength="10" NumberType="Integer">            
                                <Buttons>                                           
                                    <dxe:EditButton />
                                </Buttons>       
                                <ClientSideEvents                                                           
                                         KeyPress="function(s, e) {document.getElementById('popupFiltro_textNomeCarteiraFiltro').value = '';} " 
                                         ButtonClick="function(s, e) {popupCarteira.ShowAtElementByID(s.name);}" 
                                         LostFocus="function(s, e) {popupMensagemCarteira.HideWindow();
                                     				                ASPxCallback2.SendCallback(btnEditCodigoCarteiraFiltro.GetValue());
                                                                    }"
                                        />               
                                </dxe:ASPxSpinEdit>
                            </td>
                        
                            <td  colspan="2" width="450">
                                <asp:TextBox ID="textNomeCarteiraFiltro" runat="server" CssClass="textNome" Enabled="false" />
                            </td>
                        </tr>        
                        
                        <tr>
                            <td class="td_Label_Longo">
                                <asp:Label ID="labelCotistaFiltro" runat="server" CssClass="labelNormal" Text="Cotista:"/>
                            </td>        
                            
                            <td>                                                                                                                
                                <dxe:ASPxSpinEdit ID="btnEditCodigoCotistaFiltro" runat="server" CssClass="textButtonEdit" 
                                            ClientInstanceName="btnEditCodigoCotistaFiltro" MaxLength="10" NumberType="Integer">            
                                <Buttons>                                           
                                    <dxe:EditButton/>
                                </Buttons>       
                                <ClientSideEvents                                                           
                                         KeyPress="function(s, e) {document.getElementById('popupFiltro_textNomeCotistaFiltro').value = '';} " 
                                         ButtonClick="function(s, e) {popupCotista.ShowAtElementByID(s.name);}" 
                                         LostFocus="function(s, e) {OnLostFocus(popupMensagemCotista, ASPxCallback1, btnEditCodigoCotistaFiltro);}"
                                        />               
                                </dxe:ASPxSpinEdit>
                            </td>
                        
                            <td  colspan="2" width="450">
                                <asp:TextBox ID="textNomeCotistaFiltro" runat="server" CssClass="textNome" Enabled="false" />
                            </td>
                        </tr>        
                        
                        <tr>
                            <td>                
                                <asp:Label ID="labelDataInicio" runat="server" CssClass="labelNormal" Text="Início:" />
                            </td>    
                                                
                            <td>
                                <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" />                                                            
                            </td>  
                            
                            <td colspan="2">
                            <table>
                                <tr><td><asp:Label ID="labelDataFim" runat="server" CssClass="labelNormal" Text="Fim:"/></td>
                                <td><dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim"/></td></tr>
                            </table>
                            </td>                                                                       
                        </tr>
                        
                        <tr>
                            <td>
                                <asp:Label ID="labelTipo" runat="server" CssClass="labelNormal" Text="Operação:" />
                            </td> 
                            <td colspan="3">
                                <dxe:ASPxComboBox ID="dropTipoOperacaoFiltro" runat="server" ClientInstanceName="dropTipoOperacaoFiltro" 
                                                            ShowShadow="true" DropDownStyle="DropDown" IncrementalFilteringMode="Contains"
                                                            CssClass="dropDownListCurto">
                                <Items>
                                    <dxe:ListEditItem Value="1" Text="Bloqueio" />
                                    <dxe:ListEditItem Value="2" Text="Desbloqueio" />
                                </Items>                                                                                          
                                </dxe:ASPxComboBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="labelTipoLiberacao" runat="server" CssClass="labelNormal" Text="Liberação:" />
                            </td> 
                            <td colspan="3">
                                <dxe:ASPxComboBox ID="dropTipoLiberacaoFiltro" runat="server" ClientInstanceName="dropTipoLiberacaoFiltro" 
                                                            ShowShadow="true" DropDownStyle="DropDown" IncrementalFilteringMode="Contains"
                                                            CssClass="dropDownListCurto">
                                <Items>
                                    <dxe:ListEditItem Value="1" Text="Manual" />
                                    <dxe:ListEditItem Value="2" Text="Automático" />
                                </Items>                                                                                          
                                </dxe:ASPxComboBox>
                            </td>
                        </tr>

                    </table>        
                    
                    <div class="linkButton linkButtonNoBorder" style="margin-top:20px">                         
                    <asp:LinkButton ID="btnOKFilter" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnOK" OnClientClick="popupFiltro.Hide(); gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal7" runat="server" Text="Aplicar"/><div></div></asp:LinkButton>
                    <asp:LinkButton ID="btnFilterCancel" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnFilterCancel" OnClientClick="OnClearFilterClick_FiltroDatas(); return false;"><asp:Literal ID="Literal1" runat="server" Text="Limpar"/><div></div></asp:LinkButton>
                    </div>                    
                </dxpc:PopupControlContentControl></ContentCollection>                             
            </dxpc:ASPxPopupControl>        
    
            <div class="linkButton" >               
               <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal3" runat="server" Text="Novo"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" CssClass="btnDelete" OnClientClick="if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;"><asp:Literal ID="Literal4" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnFilter" runat="server" Font-Overline="false" CssClass="btnFilter" OnClientClick="return OnButtonClick_FiltroDatas()"><asp:Literal ID="Literal5" runat="server" Text="Filtro"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal6" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal8" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal9" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
            </div>
        
            <div class="divDataGrid">
                <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                        KeyFieldName="IdBloqueio" DataSourceID="EsDSBloqueioCota"
                        OnRowUpdating="gridCadastro_RowUpdating"
                        OnCustomCallback="gridCadastro_CustomCallback"
                        OnPreRender="gridCadastro_PreRender"
                        OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData"
                        OnCustomJSProperties="gridCadastro_CustomJSProperties"
                        OnCancelRowEditing="gridCadastro_CancelRowEditing"
                        OnBeforeGetCallbackResult="gridCadastro_PreRender"
                        OnInitNewRow="OnInit">     
                    
                    <Columns>                    
                                             
                        <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                            <HeaderTemplate>
                                <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                            </HeaderTemplate>
                        </dxwgv:GridViewCommandColumn>
                        
                        <dxwgv:GridViewDataColumn FieldName="IdBloqueio" VisibleIndex="0" Width="5%" Visible="false" />
                        <dxwgv:GridViewDataColumn FieldName="Observacao" Visible="false" />
                        
                        <dxwgv:GridViewDataColumn FieldName="IdCarteira" Visible="false" Width="0%" />
                        <dxwgv:GridViewDataColumn FieldName="IdCarteiraAux" Visible="false" UnboundType="integer" />
                        
                        <dxwgv:GridViewDataColumn FieldName="IdCotista" Visible="false" Width="0%"/>
                        <dxwgv:GridViewDataColumn FieldName="IdCotistaAux" Visible="false" UnboundType="integer" />
                        
                        <dxwgv:GridViewDataColumn FieldName="ApelidoCotista" Caption="Cotista" VisibleIndex="1" Width="15%" />
                        <dxwgv:GridViewDataColumn FieldName="ApelidoCotistaAux" Visible="false" UnboundType="string" />
                        
                        <dxwgv:GridViewDataColumn FieldName="ApelidoCarteira" Caption="Carteira" VisibleIndex="2" Width="18%" />
                        <dxwgv:GridViewDataColumn FieldName="ApelidoCarteiraAux" Visible="false" UnboundType="string" />
                                                                                                
                        <dxwgv:GridViewDataDateColumn FieldName="Data" Caption="Data" VisibleIndex="3" Width="10%"/>                    
                        
                        <dxwgv:GridViewDataDateColumn FieldName="DataInicio" VisibleIndex="4" Width="10%"/>                    
                        
                        <dxwgv:GridViewDataDateColumn FieldName="DataFim" VisibleIndex="5" Width="10%"/>                    
                        
                        <dxwgv:GridViewDataComboBoxColumn Caption="Tipo" FieldName="TipoOperacao" VisibleIndex="6" Width="7%" ExportWidth="130">
                            <PropertiesComboBox EncodeHtml="false">
                                <Items>
                                    <dxe:ListEditItem Value="1" Text="<div title='Bloqueio'>Bloqueio</div>" />
                                    <dxe:ListEditItem Value="2" Text="<div title='Desbloqueio'>Desbloqueio</div>" />
                                </Items>
                            </PropertiesComboBox>
                        </dxwgv:GridViewDataComboBoxColumn>
                        
                        <dxwgv:GridViewDataComboBoxColumn Caption="Liberação" FieldName="TipoLiberacao" VisibleIndex="7" Width="7%" ExportWidth="130">
                            <PropertiesComboBox EncodeHtml="false">
                                <Items>
                                    <dxe:ListEditItem Value="1" Text="<div title='Manual'>Manual</div>" />
                                    <dxe:ListEditItem Value="2" Text="<div title='Automático'>Automático</div>" />
                                </Items>
                            </PropertiesComboBox>
                        </dxwgv:GridViewDataComboBoxColumn>
                        
                        <dxwgv:GridViewDataTextColumn FieldName="Quantidade" VisibleIndex="8" Width="14%" 
                                HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right"
                                CellStyle-HorizontalAlign="Right">                    
                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}" />
                        </dxwgv:GridViewDataTextColumn>
                        
                    </Columns>

                    <Templates>
                     <EditForm>                          
                        <asp:Label ID="labelEdicao" runat="server" CssClass="labelInformation" Text="" />
                        
                        <asp:Panel ID="panelEdicao" runat="server" OnLoad="panelEdicao_Load">
                        <div class="editForm">                            
                            
                            <table border="0">
                                <tr>
                                    <td class="td_Label">
                                        <asp:Label ID="labelCarteira" runat="server" CssClass="labelRequired" Text="Carteira:" />
                                    </td>        
                                    
                                    <td>                                        
                                        <dxe:ASPxSpinEdit ID="btnEditCodigoCarteira" runat="server" CssClass="textButtonEdit" 
                                                    ClientInstanceName="btnEditCodigoCarteira" Text='<%#Eval("IdCarteiraAux")%>'
                                                    MaxLength="10" NumberType="Integer" OnLoad="btnEditCodigoCliente_Load">
                                        <Buttons>                                           
                                            <dxe:EditButton />
                                        </Buttons>       
                                        <ClientSideEvents                                                           
                                                 KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCarteira').value = '';} " 
                                                 ButtonClick="function(s, e) {popupCarteira.ShowAtElementByID(s.name);}" 
                                                 LostFocus="function(s, e) {OnLostFocus(popupMensagemCarteira, ASPxCallback2, btnEditCodigoCarteira);}"
                                                />               
                                        </dxe:ASPxSpinEdit>
                                    </td>
                                
                                    <td colspan="2" width="450">
                                        <asp:TextBox ID="textNomeCarteira" runat="server" CssClass="textNome" Enabled="false" Text='<%#Eval("ApelidoCarteiraAux")%>' />
                                    </td>
                                </tr>

                                <tr>
                                    <td class="td_Label">
                                        <asp:Label ID="labelCotista" runat="server" CssClass="labelRequired" Text="Cotista:" />
                                    </td>        
                                    
                                    <td>
                                        <dxe:ASPxSpinEdit ID="btnEditCodigoCotista" runat="server" ClientInstanceName="btnEditCodigoCotista" CssClass="textButtonEdit"
                                                Text='<%#Eval("IdCotistaAux")%>' MaxLength="10" NumberType="Integer" OnLoad="btnEditCodigoCotista_Load">            
                                        <Buttons>
                                            <dxe:EditButton />
                                        </Buttons>
                                        <ClientSideEvents                                                           
                                                 KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCotista').value = '';} " 
                                                 ButtonClick="function(s, e) {popupCotista.ShowAtElementByID(s.name);}" 
                                                 LostFocus="function(s, e) {OnLostFocus(popupMensagemCotista, ASPxCallback1, btnEditCodigoCotista);}"
                                                />               
                                        </dxe:ASPxSpinEdit>
                                    </td>
                                
                                    <td colspan="2" width="450">
                                        <asp:TextBox ID="textNomeCotista" runat="server" CssClass="textNome" Enabled="false" Text='<%#Eval("ApelidoCotistaAux")%>' />
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td  class="td_Label">
                                        <asp:Label ID="labelData" runat="server" CssClass="labelRequired" Text="Data:" />
                                    </td> 
                                    
                                    <td class="td_Label">
                                        <dxe:ASPxDateEdit ID="textData" runat="server" ClientInstanceName="textData" Value='<%#Eval("Data")%>' ClientEnabled="false"/>
				                    </td>
				        
                                    <td class="td_Label">
                                        <asp:Label ID="labelQuantidade" runat="server" CssClass="labelRequired" Text="Quantidade:" OnInit="labelQuantidadeInit" />
                                    </td>
                                    <td>
                                        <dxe:ASPxSpinEdit ID="textQuantidade" runat="server" width="100%" CssClass="textValor" ClientInstanceName="textQuantidade"
                                                                         MaxLength="28" NumberType="Float" DecimalPlaces="12" Text="<%#Bind('Quantidade')%>" OnInit="textQuantidadeInit">                                                        
                                        </dxe:ASPxSpinEdit>
                                    </td>
				                </tr>
                                                        
                                <tr>				                				                
                                    <td  class="td_Label">
                                        <asp:Label ID="labelDataFim" runat="server" CssClass="labelRequired" Text="Data Fim:" />
                                    </td> 

                                    <td colspan="3">
    	                                <dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim" Value='<%#Eval("DataFim")%>'/>
				                    </td> 
				                </tr>
                                
                                <tr>
                                
                                    <td class="td_Label">
                                        <asp:Label ID="labelTipoLiberacao" runat="server" CssClass="labelRequired" Text="Tipo Liberação:" />                   
                                    </td>                    
                                    <td colspan="3">      
                                        <dxe:ASPxComboBox ID="dropTipoLiberacao" runat="server" ClientInstanceName="dropTipoOperacao" 
                                                            ShowShadow="true" DropDownStyle="DropDownList" 
                                                            CssClass="dropDownListCurto_2" Text='<%#Eval("TipoLiberacao")%>' >
                                        <Items>
                                        <dxe:ListEditItem Value="1" Text="Manual" />
                                        <dxe:ListEditItem Value="2" Text="Automático" />
                                        </Items>                                                                                                    
                                        </dxe:ASPxComboBox>                 
                                    </td>
                                    
                                </tr>
                                
                                <tr>
                                    <td colspan="4">
                                    <table border="0">
                                    <tr>
                                    <td style="width:80px;text-align: right;">
                                        <asp:Label ID="labelObservacao" runat="server" CssClass="labelNormal" Text="Observação:" />
                                    </td>
                                    <td>
                                        <asp:TextBox ID="textObservacao" runat="server" TextMode="MultiLine" Rows="4" CssClass="textLongo" MaxLength="255" Text='<%#Eval("Observacao")%>' />
                                    </td>
                                    
                                    <td colspan="2" class="linkButton">
                                        <asp:LinkButton ID="LinkButton1" runat="server" Font-Overline="false" CssClass="btnPopup" OnInit="btnPosicaoCotista_Init" OnClientClick="popupPosicaoCotista.ShowAtElementByID(); return false;"><asp:Literal ID="Literal10" runat="server" Text="Bloquear/Desbloquear"/><div></div></asp:LinkButton>
                                    </td>
                                    </tr>
                                    </table>
                                    </td>
                                </tr>    
                            </table>
                            
                           <div class="linhaH"></div>                                        
                                <div class="linkButton linkButtonNoBorder" style="text-align:right;">

                                    <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK" OnInit="btnOKInit"
                                           OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;" ><asp:Literal ID="Literal2" runat="server" Text="OK"/><div></div></asp:LinkButton>
                                    <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel" OnClientClick="gridCadastro.CancelEdit(); return false;" ><asp:Literal ID="Literal1" runat="server" Text="Cancelar"/><div></div></asp:LinkButton>
                                </div>        
                            </div>
                                    
                        </asp:Panel>
                    </EditForm>
                        
                    <StatusBar>
                        <div>
                            <div style="float:left">
                            <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" />
                            </div>                    
                        </div>
                    </StatusBar>
                                        
                    </Templates>
                                                          
                    <SettingsPopup EditForm-Width="500px"  />
                    <SettingsCommandButton>
                        <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                    </SettingsCommandButton>
                    
                </dxwgv:ASPxGridView>            
            </div>       
    </div>
    </div>
    </td></tr></table>
    </div>
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" Landscape="true" 
    LeftMargin = "50" RightMargin = "50"
    />
        
    <cc1:esDataSource ID="EsDSBloqueioCota" runat="server"  OnesSelect="EsDSBloqueioCotista_esSelect" />    
    <cc1:esDataSource ID="EsDSCotista" runat="server" OnesSelect="EsDSCotista_esSelect" />
    <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" />    
    <cc1:esDataSource ID="EsDSPosicaoCotista" runat="server" OnesSelect="EsDSPosicaoCotista_esSelect" />    
    
    </form>
</body>
</html>