﻿using System;
using System.Data;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;

using System.Collections.Generic;
using System.Text;
using System.Drawing;

using DevExpress.Web;

using Financial.Investidor;
using Financial.ContaCorrente;
using Financial.Security;
using Financial.Fundo;
using Financial.Common;
using Financial.Util;
using Financial.InvestidorCotista;
using Financial.Web.Common;

using Financial.ContaCorrente.Enums;
using Financial.InvestidorCotista.Enums;
using Financial.Investidor.Enums;
using Financial.Common.Enums;
using EntitySpaces.Interfaces;
using DevExpress.Web.Data;
using System.Threading;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_BloqueioCota : CadastroBasePage {

    // Usado no Modo Update para pegar o Indice da linha Selecionada
    private int selectedIndex;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasFiltro = true;
        this.HasPopupCarteira = true;
        this.HasPopupCotista = true;
        this.HasPanelFieldsLoading = true;
        base.Page_Load(sender, e);
        //
        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
                  new List<string>(new string[] { BloqueioCotaMetadata.ColumnNames.TipoOperacao,
                                                  BloqueioCotaMetadata.ColumnNames.TipoLiberacao
                  }));
    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSBloqueioCotista_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
        ClienteQuery clienteQuery = new ClienteQuery("L");
        //
        CotistaQuery cotistaQuery = new CotistaQuery("C");
        CarteiraQuery carteiraQuery = new CarteiraQuery("A");
        PosicaoCotistaQuery posicaoCotistaQuery = new PosicaoCotistaQuery("PC");
        //
        BloqueioCotaQuery bloqueioCotaQuery = new BloqueioCotaQuery("B");

        bloqueioCotaQuery.Select(bloqueioCotaQuery, 
                                 carteiraQuery.IdCarteira,
                                 cotistaQuery.IdCotista,
                                 cotistaQuery.Apelido.As("ApelidoCotista"),
                                 carteiraQuery.Apelido.As("ApelidoCarteira"));

        bloqueioCotaQuery.InnerJoin(posicaoCotistaQuery).On(bloqueioCotaQuery.IdPosicao == posicaoCotistaQuery.IdPosicao);
        bloqueioCotaQuery.InnerJoin(carteiraQuery).On(posicaoCotistaQuery.IdCarteira == carteiraQuery.IdCarteira);
        bloqueioCotaQuery.InnerJoin(cotistaQuery).On(posicaoCotistaQuery.IdCotista == cotistaQuery.IdCotista);        
        // Permissão
        bloqueioCotaQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == carteiraQuery.IdCarteira);
        bloqueioCotaQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        //
        bloqueioCotaQuery.Where(permissaoClienteQuery.IdUsuario == idUsuario);

        if (!String.IsNullOrEmpty(this.btnEditCodigoCarteiraFiltro.Text)) {
            bloqueioCotaQuery.Where(carteiraQuery.IdCarteira == Convert.ToInt32(btnEditCodigoCarteiraFiltro.Text));
        }

        if (!String.IsNullOrEmpty(this.btnEditCodigoCotistaFiltro.Text)) {
            bloqueioCotaQuery.Where(cotistaQuery.IdCotista == Convert.ToInt32(this.btnEditCodigoCotistaFiltro.Text));
        }

        if (!String.IsNullOrEmpty(this.textDataInicio.Text)) {
            bloqueioCotaQuery.Where(bloqueioCotaQuery.Data >= this.textDataInicio.Text);
        }

        if (!String.IsNullOrEmpty(this.textDataFim.Text)) {
            bloqueioCotaQuery.Where(bloqueioCotaQuery.Data <= this.textDataFim.Text);
        }

        if (this.dropTipoOperacaoFiltro.SelectedIndex > -1) {
            bloqueioCotaQuery.Where(bloqueioCotaQuery.TipoOperacao == Convert.ToByte(this.dropTipoOperacaoFiltro.SelectedItem.Value));
        }

        if (this.dropTipoLiberacaoFiltro.SelectedIndex > -1) {
            bloqueioCotaQuery.Where(bloqueioCotaQuery.TipoLiberacao == Convert.ToByte(this.dropTipoLiberacaoFiltro.SelectedItem.Value));
        }

        bloqueioCotaQuery.OrderBy(bloqueioCotaQuery.Data.Descending,
                                  carteiraQuery.Apelido.Ascending,
                                  cotistaQuery.Apelido.Ascending);

        BloqueioCotaCollection coll = new BloqueioCotaCollection();
        coll.Load(bloqueioCotaQuery);

        if (!coll.HasData) {            
            // Adiciona 4 Colunas Extras
            //coll.CreateColumnsForBinding();
            //coll.AddColumn(CarteiraMetadata.ColumnNames.IdCarteira, typeof(System.Int32));
            //coll.AddColumn(CotistaMetadata.ColumnNames.IdCotista, typeof(System.Int32));
            //coll.AddColumn("ApelidoCotista", typeof(System.String));
            //coll.AddColumn("ApelidoCarteira", typeof(System.String));

            //coll.LowLevelBind();
            //e.Collection = coll;

            //if(!this.gridCadastro.IsEditing) {
                //this.gridCadastro.AddNewRow();
                //return;
            //}
        }        
        //
        e.Collection = coll;
        //coll.AddNew();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSCotista_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CotistaCollection coll = new CotistaCollection();
        coll.BuscaCotistasComAcesso(Context.User.Identity.Name);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CarteiraCollection coll = new CarteiraCollection();
        coll.BuscaCarteirasComAcesso(Context.User.Identity.Name);
        
        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    /// <summary>
    /// Grid de Popup de PosicaoCotista
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSPosicaoCotista_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxSpinEdit btnEditCodigoCotista = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCotista") as ASPxSpinEdit;

        if ( (btnEditCodigoCarteira == null || btnEditCodigoCotista == null) || 
             (btnEditCodigoCarteira.Text == "" || btnEditCodigoCotista.Text == "") ) {            
                e.Collection = new PosicaoCotistaCollection();
                return;
        }
                                                      
        PosicaoCotistaCollection coll = new PosicaoCotistaCollection();

        coll.Query
            .Select(coll.Query.IdPosicao, coll.Query.IdCotista, coll.Query.IdCarteira,
                    coll.Query.DataConversao, coll.Query.Quantidade,
                    coll.Query.QuantidadeBloqueada, coll.Query.ValorBruto)
            .Where(coll.Query.IdCarteira == Convert.ToInt32(btnEditCodigoCarteira.Text) &
                   coll.Query.IdCotista == Convert.ToInt32(btnEditCodigoCotista.Text) &
                   coll.Query.Quantidade != 0)
            .OrderBy(coll.Query.DataConversao.Descending);
        //
        coll.Query.Load();
        //            
        e.Collection = coll;
    }
    #endregion

    #region CallBacks
    
    /// <summary>
    /// Tratamento p/ Cotista
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCotista = Convert.ToInt32(e.Parameter);
            Cotista cotista = new Cotista();
            cotista.LoadByPrimaryKey(idCotista);

            if (cotista.str.Apelido != "") {
                if (cotista.IsAtivo) {
                    PermissaoCotista permissaoCotista = new PermissaoCotista();
                    //                    
                    nome = permissaoCotista.RetornaAcessoCotista(idCotista, HttpContext.Current.User.Identity.Name)
                           ? cotista.str.Apelido
                           : "no_access";
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// Tratamento p/ Carteira
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e) 
    {
        string nome = "";
        string resultado = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "")
            {
                if (cliente.IsAtivo)
                {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name))
                    {
                        nome = cliente.str.Apelido;
                        DateTime dataDia = cliente.DataDia.Value;
                        byte status = cliente.Status.Value;

                        if (status == (byte)StatusCliente.Divulgado && gridCadastro.IsEditing)
                        {
                            resultado = nome + "|status_closed";
                        }
                        else
                        {
                            resultado = nome + "|" + dataDia.ToString().Substring(0, 10) + "|" + Thread.CurrentThread.CurrentCulture.Name;
                        }
                    }
                    else
                    {
                        resultado = "no_access";
                    }
                }
                else
                {
                    resultado = "no_active";
                }
            }
        }
        e.Result = resultado;
    }

    #endregion

    #region Tratamento de Erros

    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        if (!gridCadastro.IsEditing) //Testa deleção
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdCarteira");
            List<object> keyData = gridCadastro.GetSelectedFieldValues("Data");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idCarteira = Convert.ToInt32(keyValuesId[i]);
                DateTime data = Convert.ToDateTime(keyData[i]);

                Cliente cliente = new Cliente();
                if (cliente.LoadByPrimaryKey(idCarteira))
                {
                    DateTime dataDia = cliente.DataDia.Value;
                    byte status = cliente.Status.Value;
                    string clienteDescricao = cliente.Apelido + " (" + cliente.IdCliente.ToString() + ")";
                    if (status == (byte)StatusCliente.Divulgado)
                    {
                        e.Result = "Carteira " + clienteDescricao + " está fechada! Deleção cancelada.";
                        return;
                    }
                    else if (DateTime.Compare(dataDia, data) > 0)
                    {
                        e.Result = "Lançamento com data " + data.ToString().Substring(0, 10) + " anterior à data da carteira " + clienteDescricao + "! Deleção cancelada.";
                        return;
                    }
                }
            }
        }
        else
        {
            ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
            TextBox textData = gridCadastro.FindEditFormTemplateControl("textData") as TextBox;
            int idCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);

            Cliente cliente = new Cliente();
            if (cliente.LoadByPrimaryKey(idCarteira))
            {
                byte status = cliente.Status.Value;
                if (status == (byte)StatusCliente.Divulgado)
                {
                    e.Result = "Carteira está fechada! Operação não pode ser realizada.";
                    return;
                }
            }

            #region Campos obrigatórios
            List<Control> controles = new List<Control>();
            controles.Add(btnEditCodigoCarteira);
            controles.Add(textData);
            
            if (base.TestaObrigatorio(controles) != "")
            {
                e.Result = "Campos com * são obrigatórios!";
                return;
            }
            #endregion
        }
    }

    /// <summary>
    /// Trata Erros do PopUp de PosicaoCotista
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErroPopUpPosicaoCotista_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";

        ASPxSpinEdit textQuantidade = popupPosicaoCotista.FindControl("textQuantidade") as ASPxSpinEdit;
        ASPxComboBox dropTipoOperacao = popupPosicaoCotista.FindControl("dropTipoOperacao") as ASPxComboBox;

        if (textQuantidade.Text == "") {
            e.Result = "Quantidade Bloqueio/Desbloqueio deve ser preenchida.";
            return;
        }

        if (Convert.ToDecimal(textQuantidade.Text) <= 0.0M) {
            e.Result = "A Quantidade deve ser maior do que zero.";
            return;
        }
    }

    #endregion

    #region Grid PosicaoCotista
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridPosicaoCotista_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e) {
        UtilitarioGrid.SetaCoresGrid(e);
    }
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridPosicaoCotista_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "") {  //Usado apenas na hora que abre inicialmente a popup, para dar refresh nas posições
            gridPosicaoCotista.DataBind();
        }
        else {
            selectedIndex = Convert.ToInt32(e.Parameters);
            this.InsertBloqueioCota(selectedIndex);
            gridPosicaoCotista.DataBind();
        }
    }

    /// <summary>
    /// Preenche as Colunas do PopUp de PosicaoCotista com as Quantidades de BloqueioCota
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridPosicaoCotista_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) {
        const string UNBOUND_COLUMN1 = "QuantidadeBloqueio";
        const string UNBOUND_COLUMN2 = "QuantidadeDesbloqueio";

        if (e.Column.FieldName == UNBOUND_COLUMN1) {            
            int idPosicao = Convert.ToInt32(e.GetListSourceFieldValue(PosicaoCotistaMetadata.ColumnNames.IdPosicao));

            BloqueioCota b = new BloqueioCota();
            b.Query.Select(b.Query.Quantidade.Coalesce("0").Sum())
                   .Where(b.Query.IdPosicao == idPosicao &
                          b.Query.TipoOperacao == (byte)TipoOperacaoBloqueio.Bloqueio);
            //
            b.Query.Load();
            //           
            e.Value = b.Quantidade.Value;
        }
        else if (e.Column.FieldName == UNBOUND_COLUMN2) {
            int idPosicao = Convert.ToInt32(e.GetListSourceFieldValue(PosicaoCotistaMetadata.ColumnNames.IdPosicao));

            BloqueioCota b = new BloqueioCota();
            b.Query.Select(b.Query.Quantidade.Coalesce("0").Sum())
                   .Where(b.Query.IdPosicao == idPosicao &
                          b.Query.TipoOperacao == (byte)TipoOperacaoBloqueio.Desbloqueio);
            //
            b.Query.Load();
            //           
            e.Value = b.Quantidade.Value;
        }
    }
    #endregion

    #region Insert/Update/Delete Grid Bloqueio Cota

    /// <summary>
    /// Insere um  bloqueio Cota
    /// </summary>
    /// <param name="selectedIndex"></param>
    protected void InsertBloqueioCota(int selectedIndex) {
        // IdPosicao - Quantidade e Quantidade Bloqueada de PosicaoCotista
        int idPosicao = Convert.ToInt32(this.gridPosicaoCotista.GetRowValues(selectedIndex, PosicaoCotistaMetadata.ColumnNames.IdPosicao));
        decimal quantidade = Convert.ToDecimal(this.gridPosicaoCotista.GetRowValues(selectedIndex, PosicaoCotistaMetadata.ColumnNames.Quantidade));
        decimal quantidadeBloqueada = Convert.ToDecimal(this.gridPosicaoCotista.GetRowValues(selectedIndex, PosicaoCotistaMetadata.ColumnNames.QuantidadeBloqueada));
        //

        // Popup - Procura Quantidade e TipoOperacao
        ASPxSpinEdit textQuantidade = this.popupPosicaoCotista.FindControl("textQuantidade") as ASPxSpinEdit;
        ASPxComboBox dropTipoOperacao = this.popupPosicaoCotista.FindControl("dropTipoOperacao") as ASPxComboBox;
        decimal quantidadeBloqueioDesbloqueio = Convert.ToDecimal(textQuantidade.Text);
        //
       
        // Grid - Data - TipoLiberação - DataInicio - DataFim - Observação        
        ASPxDateEdit textData = this.gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxComboBox dropTipoLiberacao = this.gridCadastro.FindEditFormTemplateControl("dropTipoLiberacao") as ASPxComboBox;
        DateTime data = Convert.ToDateTime(textData.Text);
        //
        // OBS: DataInicio Não Tem
        ASPxDateEdit textDataFim = gridCadastro.FindEditFormTemplateControl("textDataFim") as ASPxDateEdit;
        TextBox textObservacao = gridCadastro.FindEditFormTemplateControl("textObservacao") as TextBox;

        // Se tipoOperacao = Bloqueio
        // Quantidade Bloqueio <= (Quantidade + Quantidade Bloqueada)
        if (Convert.ToByte(dropTipoOperacao.SelectedItem.Value) == (byte)TipoOperacaoBloqueio.Bloqueio) {
            decimal maximoValor = quantidade + quantidadeBloqueada;
            if (quantidadeBloqueioDesbloqueio > maximoValor) {
                quantidadeBloqueioDesbloqueio = maximoValor;
            }
        }
        // Quantidade Desbloqueio <= Quantidade Bloqueada
        else if (Convert.ToByte(dropTipoOperacao.SelectedItem.Value) == (byte)TipoOperacaoBloqueio.Desbloqueio) {
            if (quantidadeBloqueioDesbloqueio > quantidadeBloqueada) {
                quantidadeBloqueioDesbloqueio = quantidadeBloqueada;
            }
        }

        BloqueioCotaCollection bloqueioCotaCollection = new BloqueioCotaCollection();
        bloqueioCotaCollection.Query
                              .Select(bloqueioCotaCollection.Query.IdBloqueio)
                              .Where(bloqueioCotaCollection.Query.IdPosicao == idPosicao &&
                                     bloqueioCotaCollection.Query.Data == data);

        if (bloqueioCotaCollection.Query.Load()) {
            // Bloqueio já existe
            throw new Exception("Bloqueio Cota já existente para essa Posição na Data: " + data.ToString("d"));
        }
        else {
            // 
            if (quantidadeBloqueada == 0 && Convert.ToByte(dropTipoOperacao.SelectedItem.Value) == (byte)TipoOperacaoBloqueio.Desbloqueio) {
                throw new Exception("Impossível Desbloquear pois não há quantidade Bloqueada");
            }
            else {
                BloqueioCota b = new BloqueioCota();
                b.IdPosicao = idPosicao;
                b.Data = data;
                b.DataInicio = data;
                //
                b.Quantidade = quantidadeBloqueioDesbloqueio;
                //
                b.TipoOperacao = Convert.ToByte(dropTipoOperacao.SelectedItem.Value);
                b.TipoLiberacao = Convert.ToByte(dropTipoLiberacao.SelectedItem.Value);
                //
                b.DataFim = !String.IsNullOrEmpty(textDataFim.Text) ? Convert.ToDateTime(textDataFim.Text) : data;
                b.Observacao = !String.IsNullOrEmpty(textObservacao.Text) ? textObservacao.Text.Trim() : "";
                //
                b.Save();

                #region Log do Processo
                HistoricoLog historicoLog = new HistoricoLog();
                historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                "Cadastro de BloqueioCota - Operacao: Insert BloqueioCota: " + b.IdBloqueio + UtilitarioWeb.ToString(b),
                                                HttpContext.Current.User.Identity.Name,
                                                UtilitarioWeb.GetIP(Request),
                                                "",
                                                HistoricoLogOrigem.Outros);
                #endregion
            }
        }
    }
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        int idBloqueio = (int)e.Keys[0];
        //
        ASPxDateEdit textDataFim = gridCadastro.FindEditFormTemplateControl("textDataFim") as ASPxDateEdit;
        TextBox textObservacao = gridCadastro.FindEditFormTemplateControl("textObservacao") as TextBox;
        ASPxComboBox dropTipoLiberacao = gridCadastro.FindEditFormTemplateControl("dropTipoLiberacao") as ASPxComboBox;
        ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
        //
        BloqueioCota bloqueioCota = new BloqueioCota();

        if (bloqueioCota.LoadByPrimaryKey(idBloqueio)) {
            bloqueioCota.DataFim = Convert.ToDateTime(textDataFim.Text);
            bloqueioCota.TipoLiberacao = Convert.ToByte(dropTipoLiberacao.SelectedItem.Value);
            bloqueioCota.Quantidade = Convert.ToDecimal(textQuantidade.Text);
            bloqueioCota.Observacao = textObservacao.Text.Trim();
            //
            bloqueioCota.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de BloqueioCota - Operacao: Update BloqueioCota: " + idBloqueio + UtilitarioWeb.ToString(bloqueioCota),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// Delete Bloqueio Cota
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e) 
    {
        const string Delete = "btnDelete";

        if (e.Parameters == Delete) {
            BloqueioCotaCollection bloqueioCotaCollection = new BloqueioCotaCollection();

            List<object> keyValuesIdBloqueio = gridCadastro.GetSelectedFieldValues(BloqueioCotaMetadata.ColumnNames.IdBloqueio);

            for (int i = 0; i < keyValuesIdBloqueio.Count; i++) {
                BloqueioCota bloqueioCota = new BloqueioCota();
                //
                if (bloqueioCota.LoadByPrimaryKey(Convert.ToInt32(keyValuesIdBloqueio[i]))) {
                    bloqueioCotaCollection.AttachEntity(bloqueioCota);
                }
            }

            //
            BloqueioCotaCollection bloqueioCotaCollectionClone = (BloqueioCotaCollection)Utilitario.Clone(bloqueioCotaCollection);
            //

            // Delete
            bloqueioCotaCollection.MarkAllAsDeleted();
            bloqueioCotaCollection.Save();

            foreach (BloqueioCota b in bloqueioCotaCollectionClone) {
                #region Log do Processo
                HistoricoLog historicoLog = new HistoricoLog();
                historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                "Cadastro de BloqueioCota - Operacao: Delete BloqueioCota: " + b.IdBloqueio + UtilitarioWeb.ToString(b),
                                                HttpContext.Current.User.Identity.Name,
                                                UtilitarioWeb.GetIP(Request),
                                                "",
                                                HistoricoLogOrigem.Outros);
                #endregion    
            }            
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    #endregion

    #region Metodos Personalizados para o Grid Bloqueio
    protected void gridCadastro_CancelRowEditing(object sender, ASPxStartRowEditingEventArgs e)
    {
        gridCadastro.DataBind();
    }

    /// <summary>
    /// Copia as Colunas do Join que não estão na Tabela Principal para Colunas Auxiliares do Tipo Unbound
    /// para poder usá-las quando for Modo Update
    /// Colunas: IdCarteira, IdCotista, ApelidoCarteira, ApelidoCotista
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) {
        string[] colunasAuxiliares = new string[] { "IdCarteiraAux", "IdCotistaAux", "ApelidoCarteiraAux", "ApelidoCotistaAux" };
        //               
        if (e.Column.FieldName == colunasAuxiliares[0]) {
            int idCarteira = Convert.ToInt32(e.GetListSourceFieldValue(CarteiraMetadata.ColumnNames.IdCarteira));
            e.Value = idCarteira;
        }
        else if (e.Column.FieldName == colunasAuxiliares[1]) {
            int idCotista = Convert.ToInt32(e.GetListSourceFieldValue(CotistaMetadata.ColumnNames.IdCotista));
            e.Value = idCotista;
        }
        else if (e.Column.FieldName == colunasAuxiliares[2]) {
            string apelidoCarteira = Convert.ToString(e.GetListSourceFieldValue("ApelidoCarteira"));
            e.Value = apelidoCarteira;
        }
        else if (e.Column.FieldName == colunasAuxiliares[3]) {
            string apelidoCotista = Convert.ToString(e.GetListSourceFieldValue("ApelidoCotista"));
            e.Value = apelidoCotista;
        }      
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    //protected void gridCadastro_CancelRowEditing(object sender, ASPxStartRowEditingEventArgs e) {
    //    //gridCadastro.DataBind();
    //}

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing) {
            ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
            e.Properties["cpBtnEditCodigoCarteira"] = btnEditCodigoCarteira.ClientID;
        }
    }

    /// <summary>
    /// Filtro de Pesquisa
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("btnEditCodigoCarteira");
        base.gridCadastro_PreRender(sender, e);
                
        ASPxDateEdit textDataInicio = popupFiltro.FindControl("textDataInicio") as ASPxDateEdit;
        ASPxDateEdit textDataFim = popupFiltro.FindControl("textDataFim") as ASPxDateEdit;
        ASPxSpinEdit btnEditCodigoCarteiraFiltro = popupFiltro.FindControl("btnEditCodigoCarteiraFiltro") as ASPxSpinEdit;

        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;

        StringBuilder texto = new StringBuilder();
        texto.Append("");

        if (btnEditCodigoCarteiraFiltro != null && btnEditCodigoCarteiraFiltro.Text != "") {
            texto.Append(" Carteira: ").Append(btnEditCodigoCarteiraFiltro.Text);
        }
        if (btnEditCodigoCarteiraFiltro.Text != "") {
            if (texto.Length > 0) {
                texto.Append(" |");
            }
            texto.Append(" Cotista: ").Append(btnEditCodigoCotistaFiltro.Text);
        }
        if (textDataInicio.Text != "") {
            if (texto.Length > 0) {
                texto.Append(" |");
            }
            texto.Append(" Data Início >= ").Append(textDataInicio.Text.Substring(0, 10));
        }
        if (textDataFim.Text != "") {
            if (texto.Length > 0) {
                texto.Append(" |");
            }
            texto.Append(" Data Fim <= ").Append(textDataFim.Text.Substring(0, 10));
        }
        if (dropTipoOperacaoFiltro.SelectedIndex > -1) {
            if (texto.Length > 0) {
                texto.Append(" |");
            }
            texto.Append(" Tipo Operação = ").Append(dropTipoOperacaoFiltro.Text);
        }
        if (dropTipoLiberacaoFiltro.SelectedIndex > -1) {
            if (texto.Length > 0) {
                texto.Append(" |");
            }
            texto.Append(" Tipo Liberação = ").Append(dropTipoLiberacaoFiltro.Text);
        }

        labelFiltro.Text = texto.ToString();
    }

    #endregion

    #region Modo Edicao (Update/Insert) Do Grid Bloqueio

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditCodigoCliente_Load(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxSpinEdit).Enabled = false;
            (sender as ASPxSpinEdit).BackColor = Color.FromName("#EBEBEB");

            //(sender as ASPxSpinEdit).Text = "1";

        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditCodigoCotista_Load(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxSpinEdit).Enabled = false;
            (sender as ASPxSpinEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnPosicaoCotista_Init(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing) {
            (sender as LinkButton).Visible = false;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textData_Init(object sender, EventArgs e) {
        //  Modo Update e Insert
        (sender as ASPxDateEdit).Enabled = false;        
    }
    #endregion

    #region Modo Inserção Do Grid Bloqueio

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textQuantidadeInit(object sender, EventArgs e) {
        // Insert
        if (gridCadastro.IsNewRowEditing) {
            (sender as ASPxSpinEdit).Visible = false;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void labelQuantidadeInit(object sender, EventArgs e) {
        // Insert
        if (gridCadastro.IsNewRowEditing) {
            (sender as Label).Visible = false;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOKInit(object sender, EventArgs e) {
        // Insert
        if (gridCadastro.IsNewRowEditing) {
            (sender as LinkButton).Visible = false;
        }       
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    [Obsolete("Não mais usado")]
    protected void dropTipoLiberacaoPreRender(object sender, EventArgs e) {
        // Insert
        //if (gridCadastro.IsNewRowEditing) {
        //    // Força Primeiro item para não Ficar em Branco
        //    (sender as ASPxComboBox).SelectedIndex = 0;
        //}       
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void OnInit(object sender, ASPxDataInitNewRowEventArgs e) {
        e.NewValues[BloqueioCotaMetadata.ColumnNames.TipoLiberacao] = "1";
    }

    #endregion
}