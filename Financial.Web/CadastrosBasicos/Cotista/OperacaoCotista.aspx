﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OperacaoCotista.aspx.cs"
    Inherits="CadastrosBasicos_OperacaoCotista" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
    var popup = true;    
    document.onkeydown=onDocumentKeyDown;
    var operacao = '';
    
    //Suitability - Perfil Cotista
    var idPerfilCotista = '';
    //Suitability - Perfil Posicao
    var idPerfilPosicao = '';
            
    function OnChkCotaInformadaChanged(control, e){
        textCotaInformada.SetVisible(control.GetValue());    
    }        
            
    function OnGetDataCotista(data) {        
        btnEditCodigoCotista.SetValue(data);        
        ASPxCallback1.SendCallback(btnEditCodigoCotista.GetValue());
        popupCotista.HideWindow();
        btnEditCodigoCotista.Focus();
    }        
    function OnGetDataPosicaoCotista(data) {
        var resultSplit = data.split('|');        
        var hiddenNotaResgatada = document.getElementById('hiddenNotaResgatada');
        hiddenNotaResgatada.value = resultSplit[0];
        popupPosicaoCotista.HideWindow(); 
        var mensagem = resultSplit[1];
        var textObservacao = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textObservacao');
        textObservacao.value = mensagem; 
    }  
    function OnGetDataCarteira(data) {
        btnEditCodigoCarteira.SetValue(data);        
        ASPxCallback2.SendCallback(btnEditCodigoCarteira.GetValue());
        popupCarteira.HideWindow();
        btnEditCodigoCarteira.Focus();
    }    
    function OnGetDataCarteiraFiltro(data) {
        btnEditCodigoCarteiraFiltro.SetValue(data);        
        ASPxCallback2.SendCallback(btnEditCodigoCarteiraFiltro.GetValue());
        popupCarteira.HideWindow();
        btnEditCodigoCarteiraFiltro.Focus();
    }    
    function OnGetDataCotistaFiltro(data) {
        btnEditCodigoCotistaFiltro.SetValue(data);        
        ASPxCallback1.SendCallback(btnEditCodigoCotistaFiltro.GetValue());
        popupCotista.HideWindow();
        btnEditCodigoCotistaFiltro.Focus();
    }    
    function ClearTextObservacao()
    {
        var textObservacao = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textObservacao');
        textObservacao.value = '';                                                                                            
    }
    
    function OnCarteiraChange(carteira) {
        dropContaCorrente.PerformCallback();
    }
    
    function showMensagemTermoInadequacao(mensagem)
    {
        var dadosMensagem = mensagem.split('|');               
        var descricaoMensagem = dadosMensagem[0];
        var idMensagem = dadosMensagem[1];
        var controlaConcodancia = dadosMensagem[2];
        var idEvento = dadosMensagem[3];
        
        if(controlaConcodancia == "S")
        {
            if(confirm(descricaoMensagem))
            {
                var mensagem = {idCotista:btnEditCodigoCotista.GetValue(), idMensagem:idMensagem, mensagem:descricaoMensagem, resposta:'OK' };
                logMensagem.SendCallback(JSON.stringify(mensagem));
                termoInadequacao.SendCallback(btnEditCodigoCotista.GetValue() + "|" + btnEditCodigoCarteira.GetValue() + "|" + idPerfilCotista + "|" + idPerfilPosicao);
            }
            else
            {
                var mensagem = {idCotista:btnEditCodigoCotista.GetValue(), idMensagem:idMensagem, mensagem:descricaoMensagem, resposta:'Cancelar' };
                logMensagem.SendCallback(JSON.stringify(mensagem));
            }
        }
    }
    
    function atualizaOperacao()
    {
        if (operacao == 'deletar')
        {
            gridCadastro.PerformCallback('btnDelete');
        }
        else                
        {   
            if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new')
            {             
                gridCadastro.UpdateEdit();
            }
            else
            {
                callbackAdd.SendCallback();
            }    
        }
        
    }
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {          
            if (e.result != '')
            {
                var result = e.result.split('|');
            
                if(e.result.indexOf('CONFIRM') > -1)
                {
                    var resultSplit = e.result.split('|');
                    if(confirm(resultSplit[1]))
                    {
                        if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new')
                        {             
                            gridCadastro.UpdateEdit();
                        }
                        else
                        {
                            callbackAdd.SendCallback();
                        }    
                    }
                }
                else 
                {
                    if(result[1] == '')
                        alert(e.result);
                    else
                    {
                        var mensagemEvento = result[0];
                        var idMensagem = result[1];
                        var controlaConcordancia = result[2];
                        var idEvento = result[3];
                        var diretorioBase = result[6];
                
                        idPerfilCotista = result[4];
                        idPerfilPosicao = result[5];
                
                        if(controlaConcordancia == 'S')
                        {
                            if(confirm(mensagemEvento))
                            {
                                alert('confirm!')

                                var mensagem = {idCotista:btnEditCodigoCotista.GetValue(), idMensagem:idMensagem, mensagem:mensagemEvento, resposta:'OK' };
                                logMensagem.SendCallback(JSON.stringify(mensagem));

                                if(idEvento == 13)
                                {
                                    window.location.href = diretorioBase+'CadastrosBasicos/Cliente/SuitabilityVerificaPerfilCotista.aspx';
                                    return false;
                                }    
                                atualizaOperacao(); 
                            }
                            else
                            {
                                var mensagem = {idCotista:btnEditCodigoCotista.GetValue(), idMensagem:idMensagem, mensagem:mensagemEvento, resposta:'Cancelar' };
                                logMensagem.SendCallback(JSON.stringify(mensagem));
                            
                                if(idEvento == 13)
                                {
                                    mensagemTermoInadequacao.SendCallback(btnEditCodigoCotista.GetValue());
                                    return false;
                                }
                            
                            }
                        }
                        else
                        {
                            alert(result[0]);
                        }
                    }
                }
            }
            else
            {
                atualizaOperacao();               
            }
            
            operacao = '';
        }        
        " />
        </dxcb:ASPxCallback>
        
        <dxcb:ASPxCallback ID="callbackPenalidadeLockUp" runat="server" OnCallback="callbackPenalidadeLockUp_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            var resultSplit = e.result.split('|');
            
            if (resultSplit[0] == 'Erro')
            {
                alert(resultSplit[1]);
            }
            else
            {
                textValorPenalidadeLockUp.SetValue(resultSplit[1]);
            }
                
        }        
        " />
        </dxcb:ASPxCallback>
        
        <dxcb:ASPxCallback ID="callbackHoldBack" runat="server" OnCallback="callbackHoldBack_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            var resultSplit = e.result.split('|');
            
            if (resultSplit[0] == 'Erro')
            {
                alert(resultSplit[1]);
            }
            else
            {
                textValorHoldBack.SetValue(resultSplit[1]);
            }
                
        }        
        " />
        </dxcb:ASPxCallback> 
        
        <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            alert('Operação feita com sucesso.');
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {                        
            if (gridCadastro.cp_EditVisibleIndex == -1)
            {
                var resultSplit = e.result.split('|');
                e.result = resultSplit[0];
                var textNomeCotistaFiltro = document.getElementById('popupFiltro_textNomeCotistaFiltro');
                OnCallBackCompleteCliente(s, e, popupMensagemCotista, btnEditCodigoCotistaFiltro, textNomeCotistaFiltro);
            }
            else    
            {
                var textNomeCotista = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCotista');
                OnCallBackCompleteCliente(s, e, popupMensagemCotista, btnEditCodigoCotista, textNomeCotista);
            }
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="ASPxCallback2" runat="server" OnCallback="ASPxCallback2_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) { 
            var resultSplit = e.result.split('|');
            if (gridCadastro.cp_EditVisibleIndex == -1)
            {                                  
                e.result = resultSplit[0];
                var textNomeCarteiraFiltro = document.getElementById('popupFiltro_textNomeCarteiraFiltro');
                OnCallBackCompleteCliente(s, e, popupMensagemCarteira, btnEditCodigoCarteiraFiltro, textNomeCarteiraFiltro);
            }
            else
            {
                var textNomeCarteira = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCarteira');
                OnCallBackCompleteCliente(s, e, popupMensagemCarteira, btnEditCodigoCarteira, textNomeCarteira, textDataOperacao);
                
                if (gridCadastro.cp_EditVisibleIndex == 'new') {
                                
                    if ( resultSplit[1] != '' || resultSplit[3] != '' || resultSplit[4] != '') 
                    {
                                                                                
                        if (resultSplit[1] != null && resultSplit[1] != '') 
                        {                        
                            var newDate = LocalizedData(resultSplit[1], resultSplit[2]);
                            textDataOperacao.SetValue(newDate);                                                                                              
                            if(textDataRegistro.GetValue() == null) textDataRegistro.SetValue(newDate);                                                                                           
                        }                    
                        else 
                        {
                            textDataOperacao.SetValue(null);
                            textDataRegistro.SetValue(null);
                        }
                        
                        if (resultSplit[3] != null && resultSplit[3] != '') { 
                            var newDate = LocalizedData(resultSplit[3], resultSplit[2]);
                            textDataConversao.SetValue(newDate);          
                            
                            var newDate1 = LocalizedData(resultSplit[4], resultSplit[2]);
                            textDataLiquidacao.SetValue(newDate1);                        
                        }
                        else 
                        {
                            textDataConversao.SetValue(null);
                            textDataLiquidacao.SetValue(null);                                        
                        }
                        
                        if (resultSplit[5] != null && resultSplit[5] != '')
                        {
                            var idLocalNegociacao = resultSplit[5];
                            dropLocalNegociacao.SetValue(idLocalNegociacao); 
                    }
                }                                                                
            }
                
                if (resultSplit[6] == 'True')
                {
                    textValorDespesas.SetEnabled(true);
                    textValorTaxas.SetEnabled(true);
                    textValorTributos.SetEnabled(true);
                    dropContaCorrente.SetEnabled(true);
                    
                    if(dropTipoOperacao.GetSelectedIndex() == 1 || dropTipoOperacao.GetSelectedIndex() == 4)
                    {
                        textValorPenalidadeLockUp.SetEnabled(true);
                        textValorHoldBack.SetEnabled(true);
                    }  
                    else
                    {
                        textValorPenalidadeLockUp.SetEnabled(false);
                        textValorPenalidadeLockUp.SetValue(null);
                        textValorHoldBack.SetEnabled(false);
                        textValorHoldBack.SetValue(null);
                        
                    }
                    
                    dropFormaLiquidacao.SetEnabled(false);
                    dropTipoResgate.SetEnabled(false);
                    dropTipoResgate.SetValue(null);
                    
                    if (resultSplit[3] != null && resultSplit[3] != '') { 
                        var newDate = LocalizedData(resultSplit[3], resultSplit[2]);
                        textDataConversao.SetValue(newDate);          
                        
                        var newDate1 = LocalizedData(resultSplit[4], resultSplit[2]);
                        textDataLiquidacao.SetValue(newDate1);                        
                    }
                    
                }
                else
                {
                    textValorDespesas.SetEnabled(false);
                    textValorDespesas.SetValue(0);
                    textValorTaxas.SetEnabled(false);
                    textValorTaxas.SetValue(0);
                    textValorTributos.SetEnabled(false);
                    textValorTributos.SetValue(0);
                    dropContaCorrente.SetEnabled(false);
                    dropContaCorrente.SetValue(null);
                    textValorPenalidadeLockUp.SetEnabled(false);
                    textValorPenalidadeLockUp.SetValue(null);
                    textValorHoldBack.SetEnabled(false);
                    
                    
                    dropFormaLiquidacao.SetEnabled(true);
                    dropTipoResgate.SetEnabled(true);
                }
                
                if (dropTipoOperacao.GetSelectedIndex() == 1 ||
                    dropTipoOperacao.GetSelectedIndex() == 2 ||
                    dropTipoOperacao.GetSelectedIndex() == 3 ||
                    dropTipoOperacao.GetSelectedIndex() == 4 ||
                    dropTipoOperacao.GetSelectedIndex() == 7)
                    {
                        dropFieTabelaIr.SetEnabled(false);
                        dropFieModalidade.SetEnabled(false);
                        dropFieTabelaIr.SetValue(null);
                        dropFieModalidade.SetValue(null);
                    }
                    else
                    {
                        if (resultSplit[7] == 'S')
                        {
                            dropFieTabelaIr.SetEnabled(true);
                            dropFieModalidade.SetEnabled(true);
                        }
                        else
                        {
                            dropFieTabelaIr.SetEnabled(false);
                            dropFieModalidade.SetEnabled(false);
                            dropFieTabelaIr.SetValue(null);
                            dropFieModalidade.SetValue(null);
                        }
                    }
            }
        }        
        " />
        </dxcb:ASPxCallback>
        
        <dxcb:ASPxCallback ID="logMensagem" runat="server" OnCallback="logMensagem_callback">
        </dxcb:ASPxCallback>
        
        <dxcb:ASPxCallback ID="mensagemTermoInadequacao" runat="server" OnCallback="mensagemTermoInadequacao_callback">
            <ClientSideEvents CallbackComplete="function(s, e) {
                if(e.result == '')
                    alert('Mensagem não cadastrada para o evento - Termo de Inadequação');
                else
                    showMensagemTermoInadequacao(e.result);
            }
            "/>
        </dxcb:ASPxCallback>
        
        <dxcb:ASPxCallback ID="termoInadequacao" runat="server" OnCallback="termoInadequacao_callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
                atualizaOperacao();
            }
            "/>
        </dxcb:ASPxCallback>   
        
        <dxpc:ASPxPopupControl ID="popupPosicaoCotista" runat="server" Width="500px" HeaderText=""
            ContentStyle-VerticalAlign="Top" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
            AllowDragging="True">
            <ContentCollection>
                <dxpc:PopupControlContentControl runat="server">
                    <div>
                        <dxwgv:ASPxGridView ID="gridPosicaoCotista" runat="server" Width="100%" ClientInstanceName="gridPosicaoCotista"
                            AutoGenerateColumns="False" DataSourceID="EsDSPosicaoCotista" KeyFieldName="IdPosicao"
                            OnCustomDataCallback="gridPosicaoCotista_CustomDataCallback" OnCustomCallback="gridPosicaoCotista_CustomCallback"
                            OnHtmlRowCreated="gridPosicaoCotista_HtmlRowCreated">
                            <Columns>
                                <dxwgv:GridViewDataTextColumn FieldName="DataConversao" Caption="Data Conversão"
                                    VisibleIndex="2" Width="10%">
                                    <PropertiesTextEdit EncodeHtml="false" DisplayFormatString="{0:d}" />
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataSpinEditColumn FieldName="Quantidade" VisibleIndex="3" Width="15%"
                                    HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                                    <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}">
                                    </PropertiesSpinEdit>
                                </dxwgv:GridViewDataSpinEditColumn>
                                <dxwgv:GridViewDataSpinEditColumn FieldName="ValorBruto" VisibleIndex="4" Width="15%"
                                    HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                                    <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                    </PropertiesSpinEdit>
                                </dxwgv:GridViewDataSpinEditColumn>
                                <dxwgv:GridViewDataSpinEditColumn FieldName="ValorIR" VisibleIndex="5" Width="15%"
                                    HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                                    <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                    </PropertiesSpinEdit>
                                </dxwgv:GridViewDataSpinEditColumn>
                                <dxwgv:GridViewDataSpinEditColumn FieldName="ValorIOF" VisibleIndex="6" Width="15%"
                                    HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                                    <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                    </PropertiesSpinEdit>
                                </dxwgv:GridViewDataSpinEditColumn>
                                <dxwgv:GridViewDataSpinEditColumn FieldName="ValorLiquido" Caption="Valor Líquido"
                                    VisibleIndex="7" Width="15%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                                    <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                    </PropertiesSpinEdit>
                                </dxwgv:GridViewDataSpinEditColumn>
                                <dxwgv:GridViewDataTextColumn FieldName="Mnemonico" Caption="Série"
                                    VisibleIndex="8" Width="15%" HeaderStyle-HorizontalAlign="Center" CellStyle-HorizontalAlign="left">
                                </dxwgv:GridViewDataTextColumn>
                            </Columns>
                            <Settings ShowTitlePanel="True" />
                            <SettingsBehavior ColumnResizeMode="Disabled" />
                            <ClientSideEvents RowDblClick="function(s, e) { 
            gridPosicaoCotista.GetValuesOnCustomCallback(e.visibleIndex, OnGetDataPosicaoCotista);}" Init="function(s, e) {
	        e.cancel = true;
	        }" />
                            <SettingsDetail ShowDetailButtons="False" />
                            <Styles AlternatingRow-Enabled="True" Cell-Wrap="False">
                                <Header ImageSpacing="5px" SortingImageSpacing="5px" />
                            </Styles>
                            <Images>
                                <PopupEditFormWindowClose Height="17px" Width="17px" />
                            </Images>
                            <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Posições de Cotista" />
                        </dxwgv:ASPxGridView>
                    </div>
                </dxpc:PopupControlContentControl>
            </ContentCollection>
            <ClientSideEvents PopUp="function(s, e) {gridPosicaoCotista.PerformCallback(); }" />
        </dxpc:ASPxPopupControl>
        <asp:HiddenField ID="hiddenNotaResgatada" runat="server" Value='<%#Eval("IdCarteira")%>' />
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Operações de Cotistas (Aplicações e Resgates)"></asp:Label>
                            </div>
                            <div id="mainContent">
                                <dxpc:ASPxPopupControl ID="popupFiltro" AllowDragging="true" PopupElementID="popupFiltro"
                                    EnableClientSideAPI="True" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
                                    CloseAction="CloseButton" Width="400" Left="250" Top="70" HeaderText="Filtros adicionais para consulta"
                                    runat="server" HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
                                    <ContentCollection>
                                        <dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                                            <table>
                                                <tr>
                                                    <td class="td_Label_Longo">
                                                        <asp:Label ID="labelCarteiraFiltro" runat="server" CssClass="labelNormal" Text="Carteira:"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxSpinEdit ID="btnEditCodigoCarteiraFiltro" runat="server" CssClass="textButtonEdit"
                                                            ClientInstanceName="btnEditCodigoCarteiraFiltro" EnableClientSideAPI="true" CssFilePath="../../css/forms.css"
                                                            SpinButtons-ShowIncrementButtons="false" MaxLength="10" NumberType="Integer">
                                                            <Buttons>
                                                                <dxe:EditButton>
                                                                </dxe:EditButton>
                                                            </Buttons>
                                                            <ClientSideEvents KeyPress="function(s, e) {document.getElementById('popupFiltro_textNomeCarteiraFiltro').value = '';} "
                                                                ButtonClick="function(s, e) {popupCarteira.ShowAtElementByID(s.name);}" LostFocus="function(s, e) {OnLostFocus(popupMensagemCarteira, ASPxCallback2, btnEditCodigoCarteiraFiltro);}" />
                                                        </dxe:ASPxSpinEdit>
                                                    </td>
                                                    <td colspan="2" width="450">
                                                        <asp:TextBox ID="textNomeCarteiraFiltro" runat="server" CssClass="textNome" Enabled="false"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_Label_Longo">
                                                        <asp:Label ID="labelCotistaFiltro" runat="server" CssClass="labelNormal" Text="Cotista:"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxSpinEdit ID="btnEditCodigoCotistaFiltro" runat="server" CssClass="textButtonEdit"
                                                            ClientInstanceName="btnEditCodigoCotistaFiltro" EnableClientSideAPI="true" CssFilePath="../../css/forms.css"
                                                            SpinButtons-ShowIncrementButtons="false" MaxLength="10" NumberType="Integer">
                                                            <Buttons>
                                                                <dxe:EditButton>
                                                                </dxe:EditButton>
                                                            </Buttons>
                                                            <ClientSideEvents KeyPress="function(s, e) {document.getElementById('popupFiltro_textNomeCotistaFiltro').value = '';} "
                                                                ButtonClick="function(s, e) {popupCotista.ShowAtElementByID(s.name);}" LostFocus="function(s, e) {OnLostFocus(popupMensagemCotista, ASPxCallback1, btnEditCodigoCotistaFiltro);}" />
                                                        </dxe:ASPxSpinEdit>
                                                    </td>
                                                    <td colspan="2" width="450">
                                                        <asp:TextBox ID="textNomeCotistaFiltro" runat="server" CssClass="textNome" Enabled="false"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="labelDataInicio" runat="server" CssClass="labelNormal" Text="Início:"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" />
                                                    </td>
                                                    <td colspan="2">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="labelDataFim" runat="server" CssClass="labelNormal" Text="Fim:" /></td>
                                                                <td>
                                                                    <dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="labelTipo" runat="server" CssClass="labelNormal" Text="Tipo:"></asp:Label>
                                                    </td>
                                                    <td colspan="3">
                                                        <dxe:ASPxComboBox ID="dropTipoOperacaoFiltro" runat="server" ClientInstanceName="dropTipoOperacaoFiltro"
                                                            ShowShadow="true" DropDownStyle="DropDown" IncrementalFilteringMode="Contains"
                                                            CssClass="dropDownListCurto">
                                                            <Items>
                                                                <dxe:ListEditItem Value="1" Text="Aplicação" />
                                                                <dxe:ListEditItem Value="2" Text="Resgate Bruto" />
                                                                <dxe:ListEditItem Value="3" Text="Resgate Líquido" />
                                                                <dxe:ListEditItem Value="4" Text="Resgate Cotas" />
                                                                <dxe:ListEditItem Value="5" Text="Resgate Total" />
                                                                <dxe:ListEditItem Value="10" Text="Aplic. Especial" />
                                                                <dxe:ListEditItem Value="11" Text="Aplic. Ações" />
                                                                <dxe:ListEditItem Value="12" Text="Resg. Especial" />
                                                                <dxe:ListEditItem Value="20" Text="Come Cotas" />
                                                                <dxe:ListEditItem Value="80" Text="Amortização" />
                                                                <dxe:ListEditItem Value="82" Text="Juros" />
                                                                <dxe:ListEditItem Value="84" Text="Amort.+ Juros" />
                                                                <dxe:ListEditItem Value="102" Text="Retirada" />
                                                                <dxe:ListEditItem Value="103" Text="Depósito" />   
                                                                <dxe:ListEditItem Value="112" Text="Dividendos" />                                                             
                                                            </Items>
                                                        </dxe:ASPxComboBox>
                                                    </td>
                                                </tr>
                                                <tr>                                        
                                                    <td>
                                                        <asp:Label ID="label3" runat="server" CssClass="labelNormal" Text="Gestor:" /></td>
                                                    <td colspan="3">
                                                        <dxe:ASPxComboBox ID="dropGestor" runat="server" ClientInstanceName="dropGestor"
                                                            DataSourceID="EsDSAgenteMercado" ShowShadow="False" DropDownStyle="DropDown"
                                                            IncrementalFilteringMode="Contains" CssClass="dropDownListLongo" TextField="Nome"
                                                        ValueField="IdAgente">
                                                        </dxe:ASPxComboBox>
                                                    </td>
                                                </tr>
                                            </table>
                                            <div class="linkButton linkButtonNoBorder" style="margin-top: 20px">
                                                <asp:LinkButton ID="btnOKFilter" runat="server" Font-Overline="false" ForeColor="Black"
                                                    CssClass="btnOK" OnClientClick="popupFiltro.Hide(); gridCadastro.PerformCallback('btnRefresh'); return false;">
                                                    <asp:Literal ID="Literal7" runat="server" Text="Aplicar" /><div>
                                                    </div>
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="btnFilterCancel" runat="server" Font-Overline="false" ForeColor="Black"
                                                    CssClass="btnFilterCancel" OnClientClick="OnClearFilterClick_FiltroDatas(); return false;">
                                                    <asp:Literal ID="Literal1" runat="server" Text="Limpar" /><div>
                                                    </div>
                                                </asp:LinkButton>
                                            </div>
                                        </dxpc:PopupControlContentControl>
                                    </ContentCollection>
                                </dxpc:ASPxPopupControl>
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;">
                                        <asp:Literal ID="Literal3" runat="server" Text="Novo" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) {operacao='deletar'; callbackErro.SendCallback();} return false;">
                                        <asp:Literal ID="Literal4" runat="server" Text="Excluir" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnFilter" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnFilter" OnClientClick="return OnButtonClick_FiltroDatas()">
                                        <asp:Literal ID="Literal5" runat="server" Text="Filtro" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnPdf" OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal6" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnExcel" OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal8" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh"
                                        OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal9" runat="server" Text="Atualizar" /><div>
                                        </div>
                                    <asp:LinkButton ID="btnCustomFields" runat="server" Font-Overline="false" ValidationGroup="ATK" 
                                    CssClass="btnCustomFields" OnClientClick="gridCadastro.ShowCustomizationWindow(); return false;">
                                            <asp:Literal ID="Literal11" runat="server" Text="Mais Campos" /><div>
                                            </div>
                                        </asp:LinkButton>
                                    </asp:LinkButton>
                                </div>
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true" KeyFieldName="IdOperacao"
                                        DataSourceID="EsDSOperacaoCotista" OnRowUpdating="gridCadastro_RowUpdating" OnRowInserting="gridCadastro_RowInserting"
                                        OnCustomCallback="gridCadastro_CustomCallback" OnPreRender="gridCadastro_PreRender"
                                        OnCustomJSProperties="gridCadastro_CustomJSProperties" OnHtmlRowPrepared="gridCadastro_HtmlRowPrepared"
                                        OnBeforeGetCallbackResult="gridCadastro_PreRender">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                                                <HeaderTemplate>
                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                                </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataColumn FieldName="IdOperacao" Caption="Id Operação" VisibleIndex="1"
                                                Width="7%" ExportWidth="100" CellStyle-HorizontalAlign="Left">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="ApelidoCotista" Caption="Cotista" VisibleIndex="2"
                                                Width="17%" ExportWidth="200">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="ApelidoCarteira" Caption="Carteira" VisibleIndex="3"
                                                Width="20%" ExportWidth="200">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="MnemonicoSerie" Caption="Série" VisibleIndex="4"
                                                Width="10%" ExportWidth="100">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataDateColumn FieldName="DataOperacao" Caption="Dt Operação" VisibleIndex="4"
                                                Width="10%" />
                                            <dxwgv:GridViewDataDateColumn FieldName="DataRegistro" Caption="Dt Registro" VisibleIndex="4"
                                                Width="10%" />                                                
                                            <dxwgv:GridViewDataComboBoxColumn Caption="Tipo" FieldName="TipoOperacao" VisibleIndex="5"
                                                Width="8%" ExportWidth="110">
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="1" Text="<div title='Aplicação'>Aplicação</div>" />
                                                        <dxe:ListEditItem Value="2" Text="<div title='Resgate Bruto'>Resgate Bruto</div>" />
                                                        <dxe:ListEditItem Value="3" Text="<div title='Resgate Líquido'>Resgate Líquido</div>" />
                                                        <dxe:ListEditItem Value="4" Text="<div title='Resgate Cotas'>Resgate Cotas</div>" />
                                                        <dxe:ListEditItem Value="5" Text="<div title='Resgate Total'>Resgate Total</div>" />
                                                        <dxe:ListEditItem Value="10" Text="<div title='Aplicação Especial'>Aplic. Especial</div>" />
                                                        <dxe:ListEditItem Value="11" Text="<div title='Aplicação Ações'>Aplic. Ações</div>" />
                                                        <dxe:ListEditItem Value="12" Text="<div title='Resgate Especial'>Resg. Especial</div>" />
                                                        <dxe:ListEditItem Value="20" Text="<div title='Come Cotas'>Come Cotas</div>" />
                                                        <dxe:ListEditItem Value="40" Text="<div title='Ajuste Posição'>Ajuste Posição</div>" />
                                                        <dxe:ListEditItem Value="80" Text="<div title='Amortização'>Amortização</div>" />
                                                        <dxe:ListEditItem Value="82" Text="<div title='Juros'>Juros</div>" />
                                                        <dxe:ListEditItem Value="84" Text="<div title='Amort.+ Juros'>Amort.+ Juros</div>" />
                                                        <dxe:ListEditItem Value="102" Text="<div title='Retirada'>Retirada</div>" />
                                                        <dxe:ListEditItem Value="103" Text="<div title='Depósito'>Depósito</div>" />
                                                        <dxe:ListEditItem Value="110" Text="<div title='Aplicação Cotas'>Aplicação Cotas</div>" />
                                                        <dxe:ListEditItem Value="112" Text="<div title='Dividendos'>Dividendos</div>" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="ValorBruto" Caption="Vl Bruto" VisibleIndex="6"
                                                Width="10%" HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right"
                                                CellStyle-HorizontalAlign="Right">
                                                <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                            
                                            <dxwgv:GridViewDataTextColumn FieldName="Quantidade" VisibleIndex="7" Width="10%" 
                                                HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                                                <PropertiesTextEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="ValorLiquido" Caption="Vl Líquido" VisibleIndex="8"
                                                Width="10%" HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right"
                                                    CellStyle-HorizontalAlign="Right">                    
                                                <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>

                                            <dxwgv:GridViewDataComboBoxColumn Caption="Valores Colados" FieldName="ValoresColados"
                                                VisibleIndex="9" Width="10%">
                                                <EditFormSettings Visible="False" />
                                                <PropertiesComboBox>
                                                    <Items>
                                                        <dxe:ListEditItem Value="S" Text="Sim" />
                                                        <dxe:ListEditItem Value="N" Text="Não" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>	 
                                                                                        
                                            <dxwgv:GridViewDataColumn FieldName="IdCotista" Visible="false" Width="7%"/>
                                            <dxwgv:GridViewDataColumn FieldName="IdCarteira" Visible="false" Width="7%"/>
                                            <dxwgv:GridViewDataColumn FieldName="DataConversao" Visible="false" Caption="Dt Conversão"
                                                Width="10%" />
                                            <dxwgv:GridViewDataColumn FieldName="DataLiquidacao" Visible="false" Caption="Dt Liquidação"
                                                Width="10%" />
                                            <dxwgv:GridViewDataColumn FieldName="TipoResgate" Visible="false" ShowInCustomizationForm="false"/>
                                            <dxwgv:GridViewDataColumn FieldName="IdFormaLiquidacao" Visible="false" ShowInCustomizationForm="false"/>
                                            <dxwgv:GridViewDataColumn FieldName="IdConta" Visible="false" ShowInCustomizationForm="false"/>
                                            <dxwgv:GridViewDataColumn FieldName="Observacao" Visible="false" ShowInCustomizationForm="false"/>
                                            <dxwgv:GridViewDataColumn FieldName="IdPosicaoResgatada" Visible="false" ShowInCustomizationForm="false"/>
                                            <dxwgv:GridViewDataColumn FieldName="DadosBancarios" Visible="False" ShowInCustomizationForm="false"/>
                                            <dxwgv:GridViewDataTextColumn FieldName="IdLocalNegociacao" Visible="false" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataTextColumn FieldName="IdContaCorrenteOrigem" Visible="false" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataTextColumn FieldName="IdContaCorrenteDestino" Visible="false"
                                                ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataTextColumn FieldName="ValorDespesas" Visible="false" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataTextColumn FieldName="ValorTaxas" Visible="false" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataTextColumn FieldName="ValorTributos" Visible="false" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataTextColumn FieldName="CotaInformada" Visible="false" ShowInCustomizationForm="false" />
                                        </Columns>
                                        <Templates>
                                            <EditForm>
                                                <asp:Label ID="labelEdicao" runat="server" CssClass="labelInformation" Text=""></asp:Label>
                                                <asp:Panel ID="panelEdicao" runat="server" OnLoad="panelEdicao_Load">
                                                    <div class="editForm">
                                                        <table>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelCarteira" runat="server" CssClass="labelRequired" Text="Carteira:"></asp:Label>
                                                                </td>
                                                                <td colspan="4">
                                                                    <dxe:ASPxSpinEdit ID="btnEditCodigoCarteira" runat="server" CssClass="textButtonEdit" style="float:left"
                                                                        ClientInstanceName="btnEditCodigoCarteira" Text='<%#Eval("IdCarteira")%>' MaxLength="10"
                                                                        NumberType="Integer">
                                                                        <Buttons>
                                                                            <dxe:EditButton>
                                                                            </dxe:EditButton>
                                                                        </Buttons>
                                                                        <ClientSideEvents KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCarteira').value = '';} "
                                                                            ButtonClick="function(s, e) {popupCarteira.ShowAtElementByID(s.name);}" LostFocus="function(s, e) {OnLostFocus(popupMensagemCarteira, ASPxCallback2, btnEditCodigoCarteira);}"
                                                                            NumberChanged=" function(s,e) { OnCarteiraChange(s); }" />
                                                                    </dxe:ASPxSpinEdit>

                                                                    <asp:TextBox ID="textNomeCarteira" runat="server" CssClass="textNome" Enabled="false" style="float:left; margin: 0px 2px;"
                                                                        Text='<%#Eval("ApelidoCarteira")%>'></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelCotista" runat="server" CssClass="labelRequired" Text="Cotista:"></asp:Label>
                                                                </td>
                                                                <td colspan="4">
                                                                    <dxe:ASPxSpinEdit ID="btnEditCodigoCotista" runat="server" ClientInstanceName="btnEditCodigoCotista" style="float:left"
                                                                        CssClass="textButtonEdit" Text='<%#Eval("IdCotista")%>' MaxLength="10" NumberType="Integer">
                                                                        <Buttons>
                                                                            <dxe:EditButton>
                                                                            </dxe:EditButton>
                                                                        </Buttons>
                                                                        <ClientSideEvents KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCotista').value = '';} "
                                                                            ButtonClick="function(s, e) {popupCotista.ShowAtElementByID(s.name);}" LostFocus="function(s, e) {OnLostFocus(popupMensagemCotista, ASPxCallback1, btnEditCodigoCotista);}" 
                                                                            NumberChanged=" function(s,e) { OnCarteiraChange(s); }"/>
                                                                    </dxe:ASPxSpinEdit>

                                                                    <asp:TextBox ID="textNomeCotista" runat="server" CssClass="textNome" Enabled="false" style="float:left; margin: 0px 2px;"
                                                                        Text='<%#Eval("ApelidoCotista")%>'></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelDataOperacao" runat="server" CssClass="labelRequired" Text="Operação:" />
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxDateEdit ID="textDataOperacao" runat="server" ClientInstanceName="textDataOperacao"
                                                                        Value='<%#Eval("DataOperacao")%>' ClientSideEvents-ValueChanged="function(s, e) { ASPxCallback2.SendCallback(btnEditCodigoCarteira.GetValue());}" />
                                                                </td>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelDataRegistro" runat="server" CssClass="labelRequired" Text="Registro:" />
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxDateEdit ID="textDataRegistro" runat="server" ClientInstanceName="textDataRegistro"
                                                                        Value='<%#Eval("DataRegistro")%>'/>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelTipoOperacao" runat="server" CssClass="labelRequired" Text="Tipo:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxComboBox ID="dropTipoOperacao" runat="server" ClientInstanceName="dropTipoOperacao"
                                                                        ShowShadow="true" DropDownStyle="DropDownList" CssClass="dropDownListCurto_2"
                                                                        OnLoad="dropTipoOperacao_Load" Text='<%#Eval("TipoOperacao")%>'>
                                                                        <ClientSideEvents SelectedIndexChanged="function(s, e) { ASPxCallback2.SendCallback(btnEditCodigoCarteira.GetValue());
                                                                            
                                                                            }" 
                                                                            ValueChanged=" function(s,e) { OnCarteiraChange(s); }"/>
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelTipoResgate" runat="server" CssClass="labelNormal" Text="Resgate:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxComboBox ID="dropTipoResgate" runat="server" ClientInstanceName="dropTipoResgate"
                                                                        ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto"
                                                                        Text='<%#Eval("TipoResgate")%>'>
                                                                        <Items>
                                                                            <dxe:ListEditItem Value="1" Text="Específico" />
                                                                            <dxe:ListEditItem Value="2" Text="FIFO" />
                                                                            <dxe:ListEditItem Value="3" Text="LIFO" />
                                                                            <dxe:ListEditItem Value="4" Text="Menor Imposto" />
                                                                        </Items>
                                                                        <ClientSideEvents SelectedIndexChanged="function(s, e) { if (s.GetSelectedIndex() != 0)
                                                                                         { ClearTextObservacao(); }                                                                                         
                                                                                      }" />
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                                <td class="td_Label">
                                                                    <div class="linkButton linkButtonNoBorder">
                                                                        <asp:LinkButton ID="btnPosicaoCotista" runat="server" CssClass="btnSearch" OnClientClick="popupPosicaoCotista.ShowAtElementByID(); return false;">
                                                                            <asp:Literal ID="Literal2" runat="server" Text="Aplicações" /><div>
                                                                            </div>
                                                                        </asp:LinkButton>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelDataConversao" runat="server" CssClass="labelNormal" Text="Conversão:" />
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxDateEdit ID="textDataConversao" runat="server" ClientInstanceName="textDataConversao"
                                                                        Value='<%#Eval("DataConversao")%>' />
                                                                </td>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelDataLiquidacao" runat="server" CssClass="labelNormal" Text="Liquidação:" />
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxDateEdit ID="textDataLiquidacao" runat="server" ClientInstanceName="textDataLiquidacao"
                                                                        Value='<%#Eval("DataLiquidacao")%>' />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="label1" runat="server" CssClass="labelNormal" Text="Valor Despesas:" />
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit ID="textValorDespesas" ClientInstanceName="textValorDespesas" runat="server"
                                                                        Value='<%#Eval("ValorDespesas")%>' DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}" />
                                                                </td>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="label4" runat="server" CssClass="labelNormal" Text="Valor Taxas:" />
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit ID="textValorTaxas" ClientInstanceName="textValorTaxas" runat="server"
                                                                        Value='<%#Eval("ValorTaxas")%>' DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}" />
                                                                </td>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="label5" runat="server" CssClass="labelNormal" Text="Valor Tributos:" />
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit ID="textValorTributos" ClientInstanceName="textValorTributos" runat="server"
                                                                        Value='<%#Eval("ValorTributos")%>' DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelContaCorrenteOrigem" runat="server" CssClass="labelNormal" Text="Conta Corrente Origem:" />
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxComboBox ID="dropContaCorrente" DataSourceID="EsDSContaCorrente" CssClass="dropDownList"
                                                                        runat="server" ShowShadow="False" TextField="ContaCorrente" ValueField="IdConta"
                                                                        ValueType="System.String" ClientInstanceName="dropContaCorrente" Text='<%# Eval("IdContaCorrente") %>'
                                                                        EnableCallbackMode="True" OnCallback="contaCorrente_Callback">
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                                
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelFieModalidade" runat="server" CssClass="labelNormal" Text="Fie/Modalidade:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxComboBox ID="dropFieModalidade" runat="server" ClientInstanceName="dropFieModalidade"
                                                                        ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto"
                                                                        Text='<%#Eval("FieModalidade")%>'>
                                                                        <Items>
                                                                            <dxe:ListEditItem Value="1" Text="PGBL" />
                                                                            <dxe:ListEditItem Value="2" Text="VGBL" />
                                                                        </Items>
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                                
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelFieTabelaIr" runat="server" CssClass="labelNormal" Text="Fie/Tabela IR:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxComboBox ID="dropFieTabelaIr" runat="server" ClientInstanceName="dropFieTabelaIr"
                                                                        ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto"
                                                                        Text='<%#Eval("FieTabelaIr")%>'>
                                                                        <Items>
                                                                            <dxe:ListEditItem Value="1" Text="Progressiva" />
                                                                            <dxe:ListEditItem Value="2" Text="Regressiva" />
                                                                        </Items>
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelValor" runat="server" CssClass="labelNormal" Text="Valor/Qtde:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit ID="textValor" runat="server" CssClass="textValor_5" ClientInstanceName="textValor"
                                                                        MaxLength="28" NumberType="Float" DisplayFormatString="N" >
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelFormaLiquidacao" runat="server" CssClass="labelRequired" Text="Liquida:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxComboBox ID="dropFormaLiquidacao" runat="server" ClientInstanceName="dropFormaLiquidacao"
                                                                        DataSourceID="EsDSFormaLiquidacao" ShowShadow="false" DropDownStyle="DropDownList"
                                                                        CssClass="dropDownListCurto_5" TextField="Descricao" ValueField="IdFormaLiquidacao"
                                                                        Text='<%# Eval("IdFormaLiquidacao") %>'>
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                                <td class="td_Label">
                                                                    <dxe:ASPxCheckBox ID="chkCotaInformada" runat="server" Text="Cota informada" ToolTip="Cota informada substitui a cota do fundo" OnLoad="chkCotaInformada_Load">
                                                                    <ClientSideEvents CheckedChanged="function(control, e) {OnChkCotaInformadaChanged(control, e);}" />
                                                                    </dxe:ASPxCheckBox>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit ClientInstanceName="textCotaInformada" ClientVisible="false" ID="textCotaInformada"
                                                                        runat="server" CssClass="textValor_5" MaxLength="28" NumberType="Float" DecimalPlaces="12" Text='<%# Eval("CotaInformada") %>'>
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                            </tr>
                                        <SettingsCommandButton>
                                            <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                                        </SettingsCommandButton>

                                                            </tr>		
                                                                <td class="td_Label">
                                                                    <asp:Label ID="lblLocalNegociacao" runat="server" CssClass="labelRequired" AssociatedControlID="dropLocalNegociacao"
                                                                        Text="Local Negociação:">
                                                                    </asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxComboBox ID="dropLocalNegociacao" runat="server" DataSourceID="EsDSLocalNegociacao" ClientInstanceName="dropLocalNegociacao"
                                                                        ValueField="IdLocalNegociacao" TextField="Descricao"
                                                                        CssClass="dropDownList" Text='<%#Eval("IdLocalNegociacao")%>'> 
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                                
                                                                <td class="td_Label">
                                                                    <asp:Label ID="label6" runat="server" CssClass="labelNormal" Text="Penalidade (Lock Up):" />
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit ID="textValorPenalidadeLockUp" CssClass="textValor_5" Style="float: left;" ClientInstanceName="textValorPenalidadeLockUp" runat="server"
                                                                        Value='<%#Eval("PenalidadeLockUp")%>' DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}" ClientEnabled="false"/>
                                                                    <div class="linkButton linkButtonNoBorder" style="display: inline" id="btnLockUp">
                                                                        <asp:LinkButton ID="linkBtnLockUp" runat="server" CssClass="btnCalc iconOnly" OnClientClick="callbackPenalidadeLockUp.SendCallback(); return false;">
                                                                            <asp:Literal ID="Literal10" runat="server" Text="&nbsp"/><div>
                                                                            </div>
                                                                        </asp:LinkButton>
                                                                    </div>
                                                                </td>
                                                                
                                                                
                                                                <td class="td_Label">
                                                                    <asp:Label ID="label7" runat="server" CssClass="labelNormal" Text="Valor Hold Back:" />
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit ID="textValorHoldBack" CssClass="textValor_5" Style="float: left;" ClientInstanceName="textValorHoldBack" runat="server"
                                                                        Value='<%#Eval("ValorHoldBack")%>' DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}" ClientEnabled="false"/>
                                                                    <div class="linkButton linkButtonNoBorder" style="display: inline">
                                                                        <asp:LinkButton ID="btnHoldBack" runat="server" CssClass="btnCalc iconOnly" OnClientClick="callbackHoldBack.SendCallback(); return false;">
                                                                            <asp:Literal ID="Literal13" runat="server" Text="&nbsp" /><div>
                                                                            </div>
                                                                        </asp:LinkButton>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelTrader" runat="server" CssClass="labelNormal" Text="Trader:"></asp:Label>                        
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxComboBox ID="dropTrader" runat="server" ClientInstanceName="dropTrader"
                                                                                        DataSourceID="EsDSTrader" IncrementalFilteringMode="Contains"  
                                                                                        ShowShadow="false" DropDownStyle="DropDown" 
                                                                                        CssClass="dropDownListCurto_2" TextField="Nome" ValueField="IdTrader"
                                                                                        Text='<%#Eval("IdTrader")%>'>
                                                                      <ClientSideEvents LostFocus="function(s, e) { 
                                                                                                        if(s.GetSelectedIndex() == -1)
                                                                                                            s.SetText(null); } " />
                                                                    </dxe:ASPxComboBox>      
                                                                </td>   
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="label2" runat="server" CssClass="labelNormal" Text="Dados Conta:"></asp:Label>
                                                                </td>
                                                                <td colspan="4">
                                                                    <asp:TextBox ID="textConta" runat="server" TextMode="MultiLine" Rows="2" CssClass="textLongo5"
                                                                        Text='<%#Eval("DadosBancarios")%>' />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelObservacao" runat="server" CssClass="labelNormal" Text="Observação:"> </asp:Label>
                                                                </td>
                                                                <td colspan="4">
                                                                    <asp:TextBox ID="textObservacao" runat="server" TextMode="MultiLine" Rows="4" CssClass="textLongo5"
                                                                        Text='<%#Eval("Observacao")%>' />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <div class="linhaH">
                                                        </div>
                                                        <div class="linkButton linkButtonNoBorder popupFooter">
                                                            <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd"
                                                                OnInit="btnOKAdd_Init" OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;">
                                                                <asp:Literal ID="Literal7" runat="server" Text="OK+" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                                OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                                                                <asp:Literal ID="Literal11" runat="server" Text="OK" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel"
                                                                OnClientClick="gridCadastro.CancelEdit(); return false;">
                                                                <asp:Literal ID="Literal12" runat="server" Text="Cancelar" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                            </EditForm>
                                            <StatusBar>
                                                <div>
                                                    <div style="float: left">
                                                        <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text=""></asp:Label>
                                                    </div>
                                                </div>
                                            </StatusBar>
                                        </Templates>
                                        <SettingsPopup EditForm-Width="600px" />
                                        <SettingsBehavior EnableCustomizationWindow ="true" />
                                        <SettingsText CustomizationWindowCaption="Lista de campos" />
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro"
            Landscape="true" LeftMargin="35" RightMargin="35">
        </dxwgv:ASPxGridViewExporter>
        <cc1:esDataSource ID="EsDSOperacaoCotista" runat="server" OnesSelect="EsDSOperacaoCotista_esSelect"
            LowLevelBind="true" />
        <cc1:esDataSource ID="EsDSCotista" runat="server" OnesSelect="EsDSCotista_esSelect" />
        <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" />
        <cc1:esDataSource ID="EsDSFormaLiquidacao" runat="server" OnesSelect="EsDSFormaLiquidacao_esSelect" />
        <cc1:esDataSource ID="EsDSPosicaoCotista" runat="server" OnesSelect="EsDSPosicaoCotista_esSelect" />
        <cc1:esDataSource ID="EsDSAgenteMercado" runat="server" OnesSelect="EsDSAgenteMercado_esSelect" />
        <cc1:esDataSource ID="EsDSLocalNegociacao" runat="server" OnesSelect="EsDSLocalNegociacao_esSelect" />
        <cc1:esDataSource ID="EsDSContaCorrente" runat="server" OnesSelect="EsDSContaCorrente_esSelect" />
        <cc1:esDataSource ID="EsDSTrader" runat="server" OnesSelect="EsDSTrader_esSelect" />        
    </form>
</body>
</html>
