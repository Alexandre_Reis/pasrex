﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using Financial.Investidor;
using Financial.ContaCorrente;
using System.Drawing;
using Financial.ContaCorrente.Enums;
using Financial.Security;
using Financial.Fundo;
using Financial.InvestidorCotista;
using Financial.InvestidorCotista.Enums;
using Financial.Investidor.Enums;
using Financial.Common.Enums;
using Financial.Bolsa;
using Financial.Bolsa.Enums;
using DevExpress.Web.Data;
using Financial.Web.Common;
using EntitySpaces.Interfaces;
using Financial.Web.Util;
using Financial.Security.Enums;
using System.Threading;

public partial class CadastrosBasicos_AjustePosicaoCotista : Page {
    private int selectedIndex;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e) {
        btnEditCodigoCarteira.Focus();
    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSPosicaoCotista_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        if (btnEditCodigoCarteira.Text != "" && btnEditCodigoCotista.Text != "" && textData.Text != "") {
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(Convert.ToInt32(btnEditCodigoCarteira.Text));
            DateTime dataDia = cliente.DataDia.Value;

            if (DateTime.Compare(dataDia, Convert.ToDateTime(textData.Text)) <= 0) {
                PosicaoCotistaQuery posicaoCotistaQuery = new PosicaoCotistaQuery("PCQ");
                PosicaoCotistaAlteradaQuery posicaoCotistaAlteradaQuery = new PosicaoCotistaAlteradaQuery("PCAQ");

                posicaoCotistaQuery.LeftJoin(posicaoCotistaAlteradaQuery).On(posicaoCotistaQuery.IdPosicao.Equal(posicaoCotistaAlteradaQuery.IdPosicao) &&
                                                                             posicaoCotistaAlteradaQuery.Data.Equal("'" + dataDia.ToString("yyyyMMdd") + "'"));
                //posicaoCotistaQuery.Where(posicaoCotistaQuery.Quantidade.GreaterThan(0));
                posicaoCotistaQuery.Where(posicaoCotistaQuery.IdCarteira.Equal(Convert.ToInt32(btnEditCodigoCarteira.Text)));
                posicaoCotistaQuery.Where(posicaoCotistaQuery.IdCotista.Equal(Convert.ToInt32(btnEditCodigoCotista.Text)));
                posicaoCotistaQuery.OrderBy(posicaoCotistaQuery.DataConversao.Descending);


                PosicaoCotistaCollection coll = new PosicaoCotistaCollection();
                coll.Load(posicaoCotistaQuery);

                // Assign the esDataSourcSelectEvenArgs Collection property
                e.Collection = coll;
            }
            else {
                PosicaoCotistaHistoricoQuery posicaoCotistaHistoricoQuery = new PosicaoCotistaHistoricoQuery("PCHQ");
                PosicaoCotistaAlteradaQuery posicaoCotistaAlteradaQuery = new PosicaoCotistaAlteradaQuery("PCAQ");

                posicaoCotistaHistoricoQuery.LeftJoin(posicaoCotistaAlteradaQuery).On(posicaoCotistaHistoricoQuery.IdPosicao.Equal(posicaoCotistaAlteradaQuery.IdPosicao),
                                                                             posicaoCotistaHistoricoQuery.DataHistorico.Equal(posicaoCotistaAlteradaQuery.Data));
                //posicaoCotistaHistoricoQuery.Where(posicaoCotistaHistoricoQuery.Quantidade.GreaterThan(0));
                posicaoCotistaHistoricoQuery.Where(posicaoCotistaHistoricoQuery.IdCarteira.Equal(Convert.ToInt32(btnEditCodigoCarteira.Text)));
                posicaoCotistaHistoricoQuery.Where(posicaoCotistaHistoricoQuery.IdCotista.Equal(Convert.ToInt32(btnEditCodigoCotista.Text)));
                posicaoCotistaHistoricoQuery.Where(posicaoCotistaHistoricoQuery.DataHistorico.Equal(Convert.ToDateTime(textData.Text)));
                posicaoCotistaHistoricoQuery.OrderBy(posicaoCotistaHistoricoQuery.DataConversao.Descending);

                PosicaoCotistaHistoricoCollection coll = new PosicaoCotistaHistoricoCollection();
                coll.Load(posicaoCotistaHistoricoQuery);

                // Assign the esDataSourcSelectEvenArgs Collection property
                e.Collection = coll;
            }
        }
        else {
            e.Collection = new PosicaoCotistaCollection();
        }
    }

    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CarteiraCollection coll = new CarteiraCollection();
        coll.BuscaCarteirasComAcessoTodas(Context.User.Identity.Name);
        
        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCotista_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CotistaCollection coll = new CotistaCollection();
        coll.BuscaCotistasComAcesso(Context.User.Identity.Name);
        
        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        string resultado = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") {
                if (cliente.IsAtivo) {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoCliente(idCliente, HttpContext.Current.User.Identity.Name)) {
                        nome = cliente.str.Apelido;
                        resultado = nome + "|" + cliente.DataDia.Value.ToShortDateString() + "|" + Thread.CurrentThread.CurrentCulture.Name;
                    }
                    else {
                        resultado = "no_access";
                    }
                }
                else {
                    resultado = "no_active";
                }
            }
        }
        e.Result = resultado;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCotista = Convert.ToInt32(e.Parameter);
            Cotista cotista = new Cotista();
            cotista.LoadByPrimaryKey(idCotista);

            if (cotista.str.Apelido != "") {
                if (cotista.IsAtivo) {
                    PermissaoCotista permissaoCotista = new PermissaoCotista();
                    if (permissaoCotista.RetornaAcessoCotista(idCotista, HttpContext.Current.User.Identity.Name)) {
                        nome = cotista.str.Apelido;
                    }
                    else {
                        nome = "no_access";
                    }
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback3_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        int row = Convert.ToInt16(e.Parameter);

        ASPxButton button = (ASPxButton)popupPosicaoCotista.FindControl("btnVoltarValores");

        string quantidadeCalc = Convert.ToString(gridPosicaoCotista.GetRowValues(row, "QuantidadeAntigo"));
        string quantidadeOriginalCalc = Convert.ToString(gridPosicaoCotista.GetRowValues(row, "QuantidadeAntesCortesAntigo"));
        string valorIofCalc = Convert.ToString(gridPosicaoCotista.GetRowValues(row, "ValorIOFAntigo"));
        string valorIrCalc = Convert.ToString(gridPosicaoCotista.GetRowValues(row, "ValorIRAntigo"));

        e.Result = quantidadeCalc + '|' +
                   quantidadeOriginalCalc + '|' + 
                   valorIofCalc + '|' + 
                   valorIrCalc;

        if (e.Result.Equals("|||"))
            button.ClientEnabled = false;
        else
            button.ClientEnabled = true;

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback4_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        
        ASPxPopupControl popupPosicaoCotista = this.FindControl("popupPosicaoCotista") as ASPxPopupControl;
        ASPxGridView gridPosicaoCotista = popupPosicaoCotista.FindControl("gridPosicaoCotista") as ASPxGridView;

        int row = Convert.ToInt16(e.Parameter);
        
        decimal? quantidade = null;
        decimal? quantidadeOriginal = null;
        decimal? valorIof = null;
        decimal? valorIr = null;

        if(gridPosicaoCotista.GetRowValues(row, "QuantidadeAntigo").ToString() != "")
            quantidade = Convert.ToDecimal(gridPosicaoCotista.GetRowValues(row, "QuantidadeAntigo"));
        if (gridPosicaoCotista.GetRowValues(row, "QuantidadeAntesCortesAntigo").ToString() != "")
            quantidadeOriginal = Convert.ToDecimal(gridPosicaoCotista.GetRowValues(row, "QuantidadeAntesCortesAntigo"));
        if(gridPosicaoCotista.GetRowValues(row, "ValorIOFAntigo").ToString() != "")
            valorIof = Convert.ToDecimal(gridPosicaoCotista.GetRowValues(row, "ValorIOFAntigo"));
        if(gridPosicaoCotista.GetRowValues(row, "ValorIRAntigo").ToString() != "")
            valorIr = Convert.ToDecimal(gridPosicaoCotista.GetRowValues(row, "ValorIRAntigo"));

        int idCotista = Convert.ToInt32(btnEditCodigoCotista.Text);
        int idCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);

        Cliente cliente = new Cliente();
        cliente.LoadByPrimaryKey(idCarteira);
        DateTime dataDia = cliente.DataDia.Value;
        byte status = cliente.Status.Value;

        int idPosicao = Convert.ToInt32(gridPosicaoCotista.GetRowValues(row, "IdPosicao"));

        DateTime data = Convert.ToDateTime(textData.Text);

        PosicaoCotistaAlterada posicaoCotistaAlterada = new PosicaoCotistaAlterada();
        posicaoCotistaAlterada.LoadByPrimaryKey(dataDia, idPosicao);
        posicaoCotistaAlterada.MarkAsDeleted();
        posicaoCotistaAlterada.Save();


        if (DateTime.Compare(dataDia, data) > 0)
        {
            PosicaoCotistaHistorico posicaoCotistaHistorico = new PosicaoCotistaHistorico();
            if (posicaoCotistaHistorico.LoadByPrimaryKey(idPosicao, data))
            {

                if(quantidade.HasValue)
                    posicaoCotistaHistorico.Quantidade = quantidade;
                if(quantidadeOriginal.HasValue)
                    posicaoCotistaHistorico.QuantidadeAntesCortes = quantidadeOriginal;
                if(valorIr.HasValue)
                    posicaoCotistaHistorico.ValorIR = valorIr;
                if(valorIof.HasValue)
                    posicaoCotistaHistorico.ValorIOF = valorIof;

                posicaoCotistaHistorico.ValorLiquido = posicaoCotistaHistorico.ValorBruto.Value - posicaoCotistaHistorico.ValorIR.Value - posicaoCotistaHistorico.ValorIOF.Value;
                posicaoCotistaHistorico.Save();
            }
        }
        else
        {
            PosicaoCotista posicaoCotista = new PosicaoCotista();
            if (posicaoCotista.LoadByPrimaryKey(idPosicao))
            {

                if(quantidade.HasValue)
                    posicaoCotista.Quantidade = quantidade;
                if(quantidadeOriginal.HasValue)
                    posicaoCotista.QuantidadeAntesCortes = quantidadeOriginal;
                if(valorIr.HasValue)
                    posicaoCotista.ValorIR = valorIr;
                if(valorIof.HasValue)
                    posicaoCotista.ValorIOF = valorIof;

                posicaoCotista.ValorLiquido = posicaoCotista.ValorBruto.Value - posicaoCotista.ValorIR.Value - posicaoCotista.ValorIOF.Value;
                posicaoCotista.Save();

                #region Log do Processo
                HistoricoLog historicoLog = new HistoricoLog();
                historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                "Cadastro de AjustePosicaoCotista - Operacao: Update AjustePosicaoCotista: " + idPosicao + UtilitarioWeb.ToString(posicaoCotista),
                                                HttpContext.Current.User.Identity.Name,
                                                UtilitarioWeb.GetIP(Request),
                                                "",
                                                HistoricoLogOrigem.Outros);
                #endregion
            }

            if (status == (byte)StatusCliente.Divulgado)
            {
                PosicaoCotistaHistorico posicaoCotistaHistorico = new PosicaoCotistaHistorico();
                if (posicaoCotistaHistorico.LoadByPrimaryKey(idPosicao, data))
                {
                    if(quantidade.HasValue)
                        posicaoCotistaHistorico.Quantidade = quantidade;
                    if(quantidadeOriginal.HasValue)
                        posicaoCotistaHistorico.QuantidadeAntesCortes = quantidadeOriginal;
                    if(valorIof.HasValue)
                        posicaoCotistaHistorico.ValorIOF = valorIof;
                    if(valorIr.HasValue)
                        posicaoCotistaHistorico.ValorIR = valorIr;

                    posicaoCotistaHistorico.ValorLiquido = posicaoCotistaHistorico.ValorBruto.Value - posicaoCotistaHistorico.ValorIR.Value - posicaoCotistaHistorico.ValorIOF.Value;
                    posicaoCotistaHistorico.Save();
                }
            }
        }

        PosicaoCotistaAbertura posicaoCotistaAbertura = new PosicaoCotistaAbertura();
        if (posicaoCotistaAbertura.LoadByPrimaryKey(idPosicao, data))
        {
            if(quantidade.HasValue)
                posicaoCotistaAbertura.Quantidade = quantidade;
            if(quantidadeOriginal.HasValue)
                posicaoCotistaAbertura.QuantidadeAntesCortes = quantidadeOriginal;
            if(valorIof.HasValue)
                posicaoCotistaAbertura.ValorIOF = valorIof;
            if(valorIr.HasValue)
                posicaoCotistaAbertura.ValorIR = valorIr;

            posicaoCotistaAbertura.ValorLiquido = posicaoCotistaAbertura.ValorBruto.Value - posicaoCotistaAbertura.ValorIR.Value - posicaoCotistaAbertura.ValorIOF.Value;
            posicaoCotistaAbertura.Save();
        }

        //Altera a quantidade de cotas no HistoricoCotas
        if ((DateTime.Compare(dataDia, data) > 0) ||
            (status == (byte)StatusCliente.Fechado || status == (byte)StatusCliente.Divulgado))
        {
            decimal quantidadeCotasCarteira = 0;
            if (DateTime.Compare(dataDia, data) > 0)
            {
                PosicaoCotistaHistorico posicaoCotistaHistorico = new PosicaoCotistaHistorico();
                quantidadeCotasCarteira = posicaoCotistaHistorico.RetornaTotalCotas(idCarteira, data);
            }
            else
            {
                PosicaoCotista posicaoCotista = new PosicaoCotista();
                quantidadeCotasCarteira = posicaoCotista.RetornaTotalCotas(idCarteira);
            }

            HistoricoCota historicoCota = new HistoricoCota();
            if (historicoCota.LoadByPrimaryKey(data, idCarteira))
            {
                historicoCota.QuantidadeFechamento = quantidadeCotasCarteira;
                historicoCota.Save();
            }

        }

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCarteira_CustomDataCallback(object sender, DevExpress.Web.ASPxGridViewCustomDataCallbackEventArgs e) {
        e.Result = gridCarteira.GetRowValues(Convert.ToInt32(e.Parameters), "IdCarteira");
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCotista_CustomDataCallback(object sender, DevExpress.Web.ASPxGridViewCustomDataCallbackEventArgs e) {
        e.Result = gridCotista.GetRowValues(Convert.ToInt32(e.Parameters), "IdCotista");
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCarteira_HtmlRowCreated(object sender, DevExpress.Web.ASPxGridViewTableRowEventArgs e) {
        e.Row.Attributes.Add("onmouseover", "backgroundColor = this.style.backgroundColor; this.style.backgroundColor='#E0E9EB';");
        e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=backgroundColor;");
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCotista_HtmlRowCreated(object sender, DevExpress.Web.ASPxGridViewTableRowEventArgs e) {
        e.Row.Attributes.Add("onmouseover", "backgroundColor = this.style.backgroundColor; this.style.backgroundColor='#E0E9EB';");
        e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=backgroundColor;");
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="selectedIndex"></param>
    protected void AtualizaPosicao(int selectedIndex) {
        ASPxPopupControl popupPosicaoCotista = this.FindControl("popupPosicaoCotista") as ASPxPopupControl;
        ASPxGridView gridPosicaoCotista = popupPosicaoCotista.FindControl("gridPosicaoCotista") as ASPxGridView;
        ASPxSpinEdit textQuantidade = popupPosicaoCotista.FindControl("textQuantidade") as ASPxSpinEdit;
        ASPxSpinEdit textQuantidadeOriginal = popupPosicaoCotista.FindControl("textQuantidadeOriginal") as ASPxSpinEdit;
        ASPxSpinEdit textValorIof = popupPosicaoCotista.FindControl("textValorIof") as ASPxSpinEdit;
        ASPxSpinEdit textValorIr = popupPosicaoCotista.FindControl("textValorIr") as ASPxSpinEdit;

        decimal? QuantidadeAntigo = null;
        decimal? QuantidadeAntesCortesAntigo = null;
        decimal? ValorIOFAntigo = null;
        decimal? ValorIRAntigo = null;

        if( gridPosicaoCotista.GetRowValues(selectedIndex, "QuantidadeAntigo").ToString() != "")
            QuantidadeAntigo = Convert.ToDecimal(gridPosicaoCotista.GetRowValues(selectedIndex, "QuantidadeAntigo"));
        if( gridPosicaoCotista.GetRowValues(selectedIndex, "QuantidadeAntesCortesAntigo").ToString() != "")
            QuantidadeAntesCortesAntigo = Convert.ToDecimal(gridPosicaoCotista.GetRowValues(selectedIndex, "QuantidadeAntesCortesAntigo"));
        if (gridPosicaoCotista.GetRowValues(selectedIndex, "ValorIOFAntigo").ToString() != "")
            ValorIOFAntigo = Convert.ToDecimal(gridPosicaoCotista.GetRowValues(selectedIndex, "ValorIOFAntigo"));
        if(gridPosicaoCotista.GetRowValues(selectedIndex, "ValorIRAntigo").ToString() != "")
            ValorIRAntigo = Convert.ToDecimal(gridPosicaoCotista.GetRowValues(selectedIndex, "ValorIRAntigo"));

        int idCotista = Convert.ToInt32(btnEditCodigoCotista.Text);
        int idCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);

        Cliente cliente = new Cliente();
        cliente.LoadByPrimaryKey(idCarteira);
        DateTime dataDia = cliente.DataDia.Value;
        byte status = cliente.Status.Value;

        int idPosicao = Convert.ToInt32(gridPosicaoCotista.GetRowValues(selectedIndex, "IdPosicao"));

        decimal? quantidade = null;
        decimal? quantidadeOriginal = null;
        decimal? valorIr = null;
        decimal? valorIof = null;
        if(textQuantidade.Text != "")
            quantidade = Convert.ToDecimal(textQuantidade.Text);
        if(textQuantidadeOriginal.Text != "")
            quantidadeOriginal = Convert.ToDecimal(textQuantidadeOriginal.Text);
        if (textValorIr.Text != "")
            valorIr = Convert.ToDecimal(textValorIr.Text);
        if (textValorIof.Text != "")
            valorIof = Convert.ToDecimal(textValorIof.Text);

        DateTime data = Convert.ToDateTime(textData.Text);

        if (quantidade.HasValue || quantidadeOriginal.HasValue || valorIof.HasValue || valorIr.HasValue)
        {
            PosicaoCotistaAlterada posicaoCotistaAlterada = new PosicaoCotistaAlterada();
            posicaoCotistaAlterada.LoadByPrimaryKey(dataDia, idPosicao);
            posicaoCotistaAlterada.IdPosicao = idPosicao;
            posicaoCotistaAlterada.Data = dataDia;

            if (DateTime.Compare(dataDia, data) > 0) {
                PosicaoCotistaHistorico posicaoCotistaHistorico = new PosicaoCotistaHistorico();
                if (posicaoCotistaHistorico.LoadByPrimaryKey(idPosicao, data)) {

                    if (quantidade.HasValue)
                    {
                        if (!QuantidadeAntigo.HasValue)
                            posicaoCotistaAlterada.QuantidadeAntigo = posicaoCotistaHistorico.Quantidade.Value;

                        posicaoCotistaAlterada.QuantidadeNovo = quantidade.Value;

                        decimal diferenca = quantidade.Value - posicaoCotistaHistorico.Quantidade.Value;
                        posicaoCotistaHistorico.Quantidade += diferenca;
                    }

                    if (quantidadeOriginal.HasValue)
                    {
                        if (!QuantidadeAntesCortesAntigo.HasValue )
                            posicaoCotistaAlterada.QuantidadeAntesCortesAntigo = posicaoCotistaHistorico.QuantidadeAntesCortes.Value;

                        posicaoCotistaAlterada.QuantidadeAntesCortesNovo = quantidadeOriginal.Value;

                        decimal diferencaOriginal = quantidadeOriginal.Value - posicaoCotistaHistorico.QuantidadeAntesCortes.Value;
                        posicaoCotistaHistorico.QuantidadeAntesCortes += diferencaOriginal;
                    }

                    if (valorIr.HasValue)
                    {
                        if (!ValorIRAntigo.HasValue)
                            posicaoCotistaAlterada.ValorIRAntigo = posicaoCotistaHistorico.ValorIR.Value;

                        posicaoCotistaAlterada.ValorIRNovo = valorIr.Value;

                        posicaoCotistaHistorico.ValorIR = valorIr.Value;
                    }

                    if (valorIof.HasValue)
                    {
                        if (!ValorIOFAntigo.HasValue)
                            posicaoCotistaAlterada.ValorIOFAntigo = posicaoCotistaHistorico.ValorIOF.Value;

                        posicaoCotistaAlterada.ValorIOFNovo = valorIof.Value;

                        posicaoCotistaHistorico.ValorIOF = valorIof.Value;
                    }

                    posicaoCotistaAlterada.IdCarteira = idCarteira;
                    posicaoCotistaAlterada.Save();

                    #region ajusta valor liquido
                    posicaoCotistaHistorico.ValorLiquido = posicaoCotistaHistorico.ValorBruto.Value - posicaoCotistaHistorico.ValorIOF.Value - posicaoCotistaHistorico.ValorIR.Value;
                    #endregion

                    posicaoCotistaHistorico.Save();
                }
            }
            else {
                PosicaoCotista posicaoCotista = new PosicaoCotista();
                if (posicaoCotista.LoadByPrimaryKey(idPosicao)) {

                    if (quantidade.HasValue)
                    {
                        if (!QuantidadeAntigo.HasValue)
                            posicaoCotistaAlterada.QuantidadeAntigo = posicaoCotista.Quantidade.Value;

                        posicaoCotistaAlterada.QuantidadeNovo = quantidade;

                        decimal diferenca = quantidade.Value - posicaoCotista.Quantidade.Value;
                        posicaoCotista.Quantidade += diferenca;
                    }

                    if (quantidadeOriginal.HasValue)
                    {
                        if (!QuantidadeAntesCortesAntigo.HasValue)
                            posicaoCotistaAlterada.QuantidadeAntesCortesAntigo = posicaoCotista.QuantidadeAntesCortes.Value;

                        posicaoCotistaAlterada.QuantidadeNovo = quantidadeOriginal;

                        decimal diferencaOriginal = quantidadeOriginal.Value - posicaoCotista.QuantidadeAntesCortes.Value;
                        posicaoCotista.QuantidadeAntesCortes += diferencaOriginal;
                    }

                    if (valorIr.HasValue)
                    {
                        if (!ValorIRAntigo.HasValue)
                            posicaoCotistaAlterada.ValorIRAntigo = posicaoCotista.ValorIR.Value;

                        posicaoCotistaAlterada.ValorIRNovo = valorIr;

                        posicaoCotista.ValorIR = valorIr;
                    }

                    if (valorIof.HasValue)
                    {
                        if (!ValorIOFAntigo.HasValue)
                            posicaoCotistaAlterada.ValorIOFAntigo = posicaoCotista.ValorIOF.Value;

                        posicaoCotistaAlterada.ValorIOFNovo = valorIof;

                        posicaoCotista.ValorIOF = valorIof;
                    }

                    posicaoCotistaAlterada.IdCarteira = idCarteira;
                    posicaoCotistaAlterada.Save();

                    #region ajusta valor liquido
                    posicaoCotista.ValorLiquido = posicaoCotista.ValorBruto.Value - posicaoCotista.ValorIOF.Value - posicaoCotista.ValorIR.Value;
                    #endregion

                    posicaoCotista.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de AjustePosicaoCotista - Operacao: Update AjustePosicaoCotista: " + idPosicao + UtilitarioWeb.ToString(posicaoCotista),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }

                if (status == (byte)StatusCliente.Divulgado) {
                    PosicaoCotistaHistorico posicaoCotistaHistorico = new PosicaoCotistaHistorico();
                    if (posicaoCotistaHistorico.LoadByPrimaryKey(idPosicao, data)) {
                        decimal diferenca = quantidade.Value - posicaoCotistaHistorico.Quantidade.Value;
                        decimal diferencaOriginal = quantidadeOriginal.Value - posicaoCotistaHistorico.QuantidadeAntesCortes.Value;
                        posicaoCotistaHistorico.Quantidade += diferenca;
                        posicaoCotistaHistorico.QuantidadeAntesCortes += diferencaOriginal;
                        posicaoCotistaHistorico.ValorIR = valorIr.Value;
                        posicaoCotistaHistorico.ValorIOF = valorIof.Value;
                        posicaoCotistaHistorico.ValorLiquido = posicaoCotistaHistorico.ValorBruto.Value - posicaoCotistaHistorico.ValorIOF.Value - posicaoCotistaHistorico.ValorIR.Value;
                        posicaoCotistaHistorico.Save();
                    }
                }
            }
        }

        PosicaoCotistaAbertura posicaoCotistaAbertura = new PosicaoCotistaAbertura();
        if (posicaoCotistaAbertura.LoadByPrimaryKey(idPosicao, data)) {
            decimal diferenca = quantidade.Value - posicaoCotistaAbertura.Quantidade.Value;
            decimal diferencaOriginal = quantidadeOriginal.Value - posicaoCotistaAbertura.QuantidadeAntesCortes.Value;
            posicaoCotistaAbertura.Quantidade += diferenca;
            posicaoCotistaAbertura.QuantidadeAntesCortes += diferencaOriginal;
            posicaoCotistaAbertura.ValorIR = valorIr.Value;
            posicaoCotistaAbertura.ValorIOF = valorIof.Value;
            posicaoCotistaAbertura.ValorLiquido = posicaoCotistaAbertura.ValorBruto.Value - posicaoCotistaAbertura.ValorIOF.Value - posicaoCotistaAbertura.ValorIR.Value;
            posicaoCotistaAbertura.Save();
        }

        //Altera a quantidade de cotas no HistoricoCotas
        if ((DateTime.Compare(dataDia, data) > 0) ||
            (status == (byte)StatusCliente.Fechado || status == (byte)StatusCliente.Divulgado)) {
            decimal quantidadeCotasCarteira = 0;
            if (DateTime.Compare(dataDia, data) > 0) {
                PosicaoCotistaHistorico posicaoCotistaHistorico = new PosicaoCotistaHistorico();
                quantidadeCotasCarteira = posicaoCotistaHistorico.RetornaTotalCotas(idCarteira, data);
            }
            else {
                PosicaoCotista posicaoCotista = new PosicaoCotista();
                quantidadeCotasCarteira = posicaoCotista.RetornaTotalCotas(idCarteira);
            }

            HistoricoCota historicoCota = new HistoricoCota();
            if (historicoCota.LoadByPrimaryKey(data, idCarteira)) {
                historicoCota.QuantidadeFechamento = quantidadeCotasCarteira;
                historicoCota.Save();
            }

        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridPosicaoCotista_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "") //Usado apenas na hora q abre inicialmente a popup, para dar refresh nas posições
        {
            gridPosicaoCotista.DataBind();
        }
        else {
            selectedIndex = Convert.ToInt32(e.Parameters);
            AtualizaPosicao(selectedIndex);
            gridPosicaoCotista.DataBind();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridPosicaoCotista_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e) {
        UtilitarioGrid.SetaCoresGrid(e);
    }
}