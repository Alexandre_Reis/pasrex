﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AjustePosicaoComeCotas.aspx.cs" Inherits="CadastrosBasicos_AjustePosicaoComeCotas" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="Atatika.Web.UI" Namespace="Atatika.Web.UI" TagPrefix="atk" %>    
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>  
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    var popup = true;
    document.onkeydown=onDocumentKeyDown;
           
    function OnGetDataCarteira(data) {
        btnEditCodigoCarteira.SetValue(data);        
        ASPxCallback1.SendCallback(btnEditCodigoCarteira.GetValue());
        popupCarteira.HideWindow();
        btnEditCodigoCarteira.Focus();
    }    
    function OnGetDataCotista(data) {
        btnEditCodigoCotista.SetValue(data);        
        ASPxCallback2.SendCallback(btnEditCodigoCotista.GetValue());
        popupCotista.HideWindow();
        btnEditCodigoCotista.Focus();
    }    
    function FechaPopupCotista()
    {
        textValorIrAgendado.SetEnabled(false); 
        textValorIrAgendado.SetText(''); 
        textValorIofVirtual.SetEnabled(false); 
        textValorIofVirtual.SetText(''); 
        textResidual15.SetEnabled(false); 
        textResidual15.SetText('');
        textResidual175.SetText('');
        textResidual175.SetEnabled(false); 
        textResidual20.SetText('');
        textResidual20.SetEnabled(false); 
        textResidual225.SetText('');
        textResidual225.SetEnabled(false); 
        
        popupPosicaoCotista.HideWindow();         
    }
    
   function limpaPopupCotista()
    {
        textValorIrAgendado.SetEnabled(false); 
        textValorIrAgendado.SetText(''); 
        textValorIofVirtual.SetEnabled(false); 
        textValorIofVirtual.SetText(''); 
        textResidual15.SetEnabled(false); 
        textResidual15.SetText('');
        textResidual175.SetText('');
        textResidual175.SetEnabled(false); 
        textResidual20.SetText('');
        textResidual20.SetEnabled(false); 
        textResidual225.SetText('');
        textResidual225.SetEnabled(false); 
         
    }    
    
    function GetOriginalValues(row)
    {
        ASPxCallback3.SendCallback(row);
    }
    
     function GetOriginalValues(row)
    {
        gridPosicaoCotista.GetRowValues(row, 'ValorIrAgendadoAntigo;ValorIOFVirtualAntigo;Residual15Antigo;Residual175Antigo;Residual20Antigo;Residual225Antigo', OnGetRowValuesOld);
    }
    
    function retornaValores()
    {
        ASPxCallback4.SendCallback(selectedIndex);
        limpaPopupCotista();
    }

    function GetEditValues(row)
    {
        gridPosicaoCotista.GetRowValues(row, 'ValorIrAgendado;ValorIofVirtual;Residuo15;Residuo175;Residuo20;Residuo225', OnGetRowValues);
    }
    
    function OnGetRowValuesOld(values) {
        textValorIrAgendadoCalc.SetValue(values[0]);
        textValorIofVirtualCalc.SetValue(values[1]);
        textResidual15Calc.SetValue(values[2]);
        textResidual175Calc.SetValue(values[3]);
        textResidual20Calc.SetValue(values[4]);
        textResidual225Calc.SetValue(values[5]);
        
        if(values[0] == null && values[1] == null && values[2] == null && values[3] == null  && values[4] == null  && values[5] == null )
        {
            btnVoltarValores.SetEnabled(false);
        }
        else
        {
            btnVoltarValores.SetEnabled(true);
        }
        
    }
    
    function OnGetRowValues(values) {
        textValorIrAgendado.SetValue(values[0]);
        textValorIofVirtual.SetValue(values[1]);
        textResidual15.SetValue(values[2]);
        textResidual175.SetValue(values[3]);
        textResidual20.SetValue(values[4]);
        textResidual225.SetValue(values[5]);
    }
    
    </script>

</head>
<body>
    <form id="form1" runat="server">
    
    <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {            
            var textNomeCarteira = document.getElementById('textNomeCarteira');
            OnCallBackCompleteCliente(s, e, popupMensagemCarteira, btnEditCodigoCarteira, textNomeCarteira);
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="ASPxCallback2" runat="server" OnCallback="ASPxCallback2_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {            
            var textNomeCotista = document.getElementById('textNomeCotista');
            OnCallBackCompleteCliente(s, e, popupMensagemCotista, btnEditCodigoCotista, textNomeCotista);
        }        
        "/>
    </dxcb:ASPxCallback>
          

    <dxcb:ASPxCallback ID="ASPxCallback4" runat="server" OnCallback="ASPxCallback4_Callback">
    </dxcb:ASPxCallback>

    
    <asp:TextBox ID="textMsgNaoExiste" runat="server" style="display:none" Text="Inexistente"/>
    <asp:TextBox ID="textMsgInativo" runat="server" style="display:none" Text="Inativo"/>
    <asp:TextBox ID="textMsgUsuarioSemAcesso" runat="server" style="display:none" Text="Usuário sem acesso!"/>
    
    <dxpc:ASPxPopupControl ID="popupMensagemCarteira" ShowHeader="false" PopupElementID="btnEditCodigoCarteira" CloseAction="OuterMouseClick" 
            ForeColor="Red" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" Left="100"
            PopupAction="None" HeaderText="" runat="server">
    </dxpc:ASPxPopupControl>
    
    <dxpc:ASPxPopupControl ID="popupMensagemCotista" ShowHeader="false" PopupElementID="btnEditCodigoCotista" CloseAction="OuterMouseClick" 
            ForeColor="Red" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" Left="100"
            PopupAction="None" HeaderText="" runat="server">
    </dxpc:ASPxPopupControl>
          
    <dxpc:ASPxPopupControl ID="popupCarteira" runat="server" Width="500px" HeaderText="" ContentStyle-VerticalAlign="Top"  
                        PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" AllowDragging="True">
        <ContentCollection><dxpc:PopupControlContentControl runat="server">     
        <div>
            <dxwgv:ASPxGridView ID="gridCarteira" runat="server" Width="100%"
                    ClientInstanceName="gridCarteira"  AutoGenerateColumns="False" 
                    DataSourceID="EsDSCarteira" KeyFieldName="IdCarteira"
                    OnCustomDataCallback="gridCarteira_CustomDataCallback" 
                    OnHtmlRowCreated="gridCarteira_HtmlRowCreated">               
            <Columns>
                <dxwgv:GridViewDataTextColumn FieldName="IdCarteira" ReadOnly="True" VisibleIndex="0" Width="20%">
                </dxwgv:GridViewDataTextColumn>
                <dxwgv:GridViewDataTextColumn FieldName="Apelido" VisibleIndex="1" Width="80%">                    
                </dxwgv:GridViewDataTextColumn>                                
            </Columns>            
            <Settings ShowFilterRow="True" ShowTitlePanel="True"/>            
            <SettingsBehavior ColumnResizeMode="Disabled" />
            <ClientSideEvents RowDblClick="function(s, e) {
            gridCarteira.GetValuesOnCustomCallback(e.visibleIndex, OnGetDataCarteira);}" Init="function(s, e) {
	        e.cancel = true;
	        }"
	        />
	        
            <SettingsDetail ShowDetailButtons="False" />
            <Styles AlternatingRow-Enabled="True" Cell-Wrap="False">
                <Header ImageSpacing="5px" SortingImageSpacing="5px"/>
            </Styles>
            <Images>
                <PopupEditFormWindowClose Height="17px" Width="17px" />
            </Images>
            <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Carteira" />
            </dxwgv:ASPxGridView>    
        
        </div>      
        </dxpc:PopupControlContentControl></ContentCollection>
        <ClientSideEvents CloseUp="function(s, e) {gridCarteira.ClearFilter(); }" />
    </dxpc:ASPxPopupControl>
          
    <dxpc:ASPxPopupControl ID="popupCotista" runat="server" Width="500px" HeaderText="" ContentStyle-VerticalAlign="Top"  
                        PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" AllowDragging="True">
        <ContentCollection><dxpc:PopupControlContentControl runat="server">     
        <div>
            <dxwgv:ASPxGridView ID="gridCotista" runat="server" Width="100%"
                    ClientInstanceName="gridCotista"  AutoGenerateColumns="False" 
                    DataSourceID="EsDSCotista" KeyFieldName="IdCotista"
                    OnCustomDataCallback="gridCotista_CustomDataCallback" 
                    OnHtmlRowCreated="gridCotista_HtmlRowCreated">               
            <Columns>
                <dxwgv:GridViewDataTextColumn FieldName="IdCotista" ReadOnly="True" VisibleIndex="0" Width="20%" />
                <dxwgv:GridViewDataTextColumn FieldName="Apelido" VisibleIndex="1" Width="80%" />
            </Columns>            
            <Settings ShowFilterRow="True" ShowTitlePanel="True"/>            
            <SettingsBehavior ColumnResizeMode="Disabled" />
            <ClientSideEvents RowDblClick="function(s, e) {
            gridCotista.GetValuesOnCustomCallback(e.visibleIndex, OnGetDataCotista);}" Init="function(s, e) {
	        e.cancel = true;
	        }"
	        />
	        
            <SettingsDetail ShowDetailButtons="False" />
            <Styles AlternatingRow-Enabled="True" Cell-Wrap="False">
                <Header ImageSpacing="5px" SortingImageSpacing="5px"/>
            </Styles>
            <Images>
                <PopupEditFormWindowClose Height="17px" Width="17px" />
            </Images>
            <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Cotista" />
            </dxwgv:ASPxGridView>    
        
        </div>      
        </dxpc:PopupControlContentControl></ContentCollection>
        <ClientSideEvents CloseUp="function(s, e) {gridCotista.ClearFilter(); }" />
    </dxpc:ASPxPopupControl>
    
    <dxpc:ASPxPopupControl ID="popupPosicaoCotista" runat="server" Width="1000px" HeaderText="" ContentStyle-VerticalAlign="Middle"  
                        PopupVerticalAlign="WindowCenter" PopupHorizontalAlign="WindowCenter" AllowDragging="True">
        <ContentCollection><dxpc:PopupControlContentControl runat="server">     
        <div>
            <dxwgv:ASPxGridView ID="gridPosicaoCotista" runat="server" Width="100%"
                    ClientInstanceName="gridPosicaoCotista"  AutoGenerateColumns="False" 
                    DataSourceID="EsDSPosicaoCotista" KeyFieldName="IdAgendamentoComeCotas"                    
                    OnCustomCallback="gridPosicaoCotista_CustomCallback"
                    OnHtmlRowCreated="gridPosicaoCotista_HtmlRowCreated">               
            <Columns>
                <dxwgv:GridViewDataTextColumn FieldName="DataAplicacao" Caption="Data Aplicação" VisibleIndex="1" Width="10%" CellStyle-HorizontalAlign="Center">                    
                    <PropertiesTextEdit EncodeHtml="false" DisplayFormatString="{0:d}"/>                    
                </dxwgv:GridViewDataTextColumn>
                
                <dxwgv:GridViewDataTextColumn FieldName="DataLancamento" Caption="Data Lançamento" VisibleIndex="2" Width="10%" CellStyle-HorizontalAlign="Center">                    
                    <PropertiesTextEdit EncodeHtml="false" DisplayFormatString="{0:d}"/>                    
                </dxwgv:GridViewDataTextColumn>
                
                <dxwgv:GridViewDataTextColumn FieldName="DataVencimento" Caption="Data Vencimento" VisibleIndex="3" Width="10%" CellStyle-HorizontalAlign="Center">                    
                    <PropertiesTextEdit EncodeHtml="false" DisplayFormatString="{0:d}"  />                    
                </dxwgv:GridViewDataTextColumn>
                
                <dxwgv:GridViewDataSpinEditColumn FieldName="ValorIrAgendado" Caption="Ir Agendado" VisibleIndex="4" Width="10%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">                                                                    
                            <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesSpinEdit>
                </dxwgv:GridViewDataSpinEditColumn>                
                
                <dxwgv:GridViewDataSpinEditColumn FieldName="ValorIofVirtual" Caption="Iof Virtual" VisibleIndex="5" Width="10%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">                                                                    
                            <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesSpinEdit>
                </dxwgv:GridViewDataSpinEditColumn>
                
                <dxwgv:GridViewDataSpinEditColumn FieldName="Residuo15" Caption="Residuo 15%" VisibleIndex="6" Width="10%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">                                                                    
                            <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesSpinEdit>
                </dxwgv:GridViewDataSpinEditColumn>

                <dxwgv:GridViewDataSpinEditColumn FieldName="Residuo175" Caption="Residuo 17,5%" VisibleIndex="7" Width="10%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">                                                                    
                            <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesSpinEdit>
                </dxwgv:GridViewDataSpinEditColumn>
                
                <dxwgv:GridViewDataSpinEditColumn FieldName="Residuo20" Caption="Residuo 20%" VisibleIndex="8" Width="10%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">                                                                    
                            <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesSpinEdit>
                </dxwgv:GridViewDataSpinEditColumn>
                
                <dxwgv:GridViewDataSpinEditColumn FieldName="Residuo225" Caption="Residuo 22,5%" VisibleIndex="9" Width="10%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">                                                                    
                            <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesSpinEdit>
                </dxwgv:GridViewDataSpinEditColumn>
                
                <dxwgv:GridViewDataSpinEditColumn FieldName="QuantidadeAntesCortes" Visible="false">                                                                    
                </dxwgv:GridViewDataSpinEditColumn>
                <dxwgv:GridViewDataSpinEditColumn FieldName="ValorIrAgendadoAntigo" Visible="false">                                                                    
                </dxwgv:GridViewDataSpinEditColumn>
                <dxwgv:GridViewDataSpinEditColumn FieldName="ValorIOFVirtualAntigo" Visible="false">                                                                    
                </dxwgv:GridViewDataSpinEditColumn>
                <dxwgv:GridViewDataSpinEditColumn FieldName="Residual15Antigo" Visible="false">                                                                    
                </dxwgv:GridViewDataSpinEditColumn>
                <dxwgv:GridViewDataSpinEditColumn FieldName="Residual175Antigo" Visible="false">                                                                    
                </dxwgv:GridViewDataSpinEditColumn>
                <dxwgv:GridViewDataSpinEditColumn FieldName="Residual20Antigo" Visible="false">                                                                    
                </dxwgv:GridViewDataSpinEditColumn>
                <dxwgv:GridViewDataSpinEditColumn FieldName="Residual225Antigo" Visible="false">                                                                    
                </dxwgv:GridViewDataSpinEditColumn>
 
                
                
            </Columns>            
            <Settings ShowTitlePanel="True" ShowFilterRow="True" VerticalScrollBarMode="Visible" VerticalScrollableHeight="280" />
            <SettingsPager PageSize="1000"></SettingsPager>
            <SettingsBehavior ColumnResizeMode="Disabled" AllowFocusedRow="true" />            
	        
            <SettingsDetail ShowDetailButtons="False" />
            <Styles AlternatingRow-Enabled="True" Cell-Wrap="False">
                <Header ImageSpacing="5px" SortingImageSpacing="5px"/>
            </Styles>
            <Images>
                <PopupEditFormWindowClose Height="17px" Width="17px" />
            </Images>
            <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Posições de Cotista" />
            
            <ClientSideEvents RowDblClick="function(s, e) {textValorIrAgendado.SetEnabled(true);
                                                           textValorIofVirtual.SetEnabled(true); 
                                                           textResidual15.SetEnabled(true);
                                                           textResidual175.SetEnabled(true); 
                                                           textResidual20.SetEnabled(true);
                                                           textResidual225.SetEnabled(true);
                                                           textValorIrAgendado.Focus(); selectedIndex = e.visibleIndex; 
                                                           GetEditValues(selectedIndex);
                                                           GetOriginalValues(selectedIndex);}"/>
    
            <ClientSideEvents RowClick="function(s, e) {selectedIndex = e.visibleIndex; 
                                                       GetOriginalValues(selectedIndex);}"/>
                
            </dxwgv:ASPxGridView>            
        
            <div>
                <table>
                
                    <tr>
                        <td>
                            <asp:Label ID="labelValorIrAgendado" runat="server" CssClass="labelNormal" Text="Valor IR Agendado:"></asp:Label>
                        </td>
                        
                        <td>    
                            <dxe:ASPxSpinEdit ID="textValorIrAgendado" runat="server" CssClass="textValor" ClientInstanceName="textValorIrAgendado"
                                              CssFilePath="../../css/forms.css" SpinButtons-ShowIncrementButtons="false" 
                                              MaxLength="20" NumberType="Float" DecimalPlaces="2">
                            </dxe:ASPxSpinEdit>
                        </td>
                        
                        <td>
                            <asp:Label ID="labelValorIrAgendadoCalc" runat="server" CssClass="labelNormal" Text="Valor IR Agendado Calculado:"></asp:Label>
                        </td>
                        
                        <td>    
                            <dxe:ASPxSpinEdit ID="textValorIrAgendadoCalc" runat="server" CssClass="textValor" ClientInstanceName="textValorIrAgendadoCalc"
                                              CssFilePath="../../css/forms.css" Enabled="false" DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                            </dxe:ASPxSpinEdit>
                        </td>
                    </tr>
                
                    <tr>
                        <td>
                            <asp:Label ID="labelValorIOF" runat="server" CssClass="labelNormal" Text="Valor IOF Virtual:"></asp:Label>
                        </td>
                        
                        <td>    
                            <dxe:ASPxSpinEdit ID="textValorIofVirtual" runat="server" CssClass="textValor" ClientInstanceName="textValorIofVirtual"
                                              CssFilePath="../../css/forms.css" SpinButtons-ShowIncrementButtons="false" 
                                              MaxLength="20" NumberType="Float" DecimalPlaces="2">
                            </dxe:ASPxSpinEdit>
                        </td>
                        
                        <td>
                            <asp:Label ID="labelValorIOFCalc" runat="server" CssClass="labelNormal" Text="Valor IOF Virtual Calculado:"></asp:Label>
                        </td>
                        
                        <td>    
                            <dxe:ASPxSpinEdit ID="textValorIofVirtualCalc" runat="server" CssClass="textValor" ClientInstanceName="textValorIofVirtualCalc"
                                              CssFilePath="../../css/forms.css" Enabled="false" DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                            </dxe:ASPxSpinEdit>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="labelValorIr15" runat="server" CssClass="labelNormal" Text="Residual 15%:"></asp:Label>
                        </td>
                        
                        <td>    
                            <dxe:ASPxSpinEdit ID="textResidual15" runat="server" CssClass="textValor" ClientInstanceName="textResidual15"
                                              CssFilePath="../../css/forms.css" SpinButtons-ShowIncrementButtons="false" 
                                              MaxLength="20" NumberType="Float" DecimalPlaces="2" >
                            </dxe:ASPxSpinEdit>
                        </td>
                        
                        <td>
                            <asp:Label ID="labelValorIrCalc" runat="server" CssClass="labelNormal" Text="Residual 15% Calculado:"></asp:Label>
                        </td>
                        
                        <td>    
                            <dxe:ASPxSpinEdit ID="textResidual15Calc" runat="server" CssClass="textValor" ClientInstanceName="textResidual15Calc"
                                              CssFilePath="../../css/forms.css" Enabled="false" DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                            </dxe:ASPxSpinEdit>
                        </td>
                        
                    </tr>
             
                    <tr>
                        <td>
                            <asp:Label ID="labelValorIr175" runat="server" CssClass="labelNormal" Text="Residual 17,5%:"></asp:Label>
                        </td>
                        
                        <td>    
                            <dxe:ASPxSpinEdit ID="textResidual175" runat="server" CssClass="textValor" ClientInstanceName="textResidual175"
                                              CssFilePath="../../css/forms.css" SpinButtons-ShowIncrementButtons="false" 
                                              MaxLength="20" NumberType="Float" DecimalPlaces="2" >
                            </dxe:ASPxSpinEdit>
                        </td>
                        
                        <td>
                            <asp:Label ID="label4" runat="server" CssClass="labelNormal" Text="Residual 17,5% Calculado:"></asp:Label>
                        </td>
                        
                        <td>    
                            <dxe:ASPxSpinEdit ID="textResidual175Calc" runat="server" CssClass="textValor" ClientInstanceName="textResidual175Calc"
                                              CssFilePath="../../css/forms.css" Enabled="false" DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                            </dxe:ASPxSpinEdit>
                        </td>
                        
                    </tr>
             
                    <tr>
                        <td>
                            <asp:Label ID="labelValorIr20" runat="server" CssClass="labelNormal" Text="Residual 20%:"></asp:Label>
                        </td>
                        
                        <td>    
                            <dxe:ASPxSpinEdit ID="textResidual20" runat="server" CssClass="textValor" ClientInstanceName="textResidual20"
                                              CssFilePath="../../css/forms.css" SpinButtons-ShowIncrementButtons="false" 
                                              MaxLength="20" NumberType="Float" DecimalPlaces="2" >
                            </dxe:ASPxSpinEdit>
                        </td>
                        
                        <td>
                            <asp:Label ID="label2" runat="server" CssClass="labelNormal" Text="Residual 20% Calculado:"></asp:Label>
                        </td>
                        
                        <td>    
                            <dxe:ASPxSpinEdit ID="textResidual20Calc" runat="server" CssClass="textValor" ClientInstanceName="textResidual20Calc"
                                              CssFilePath="../../css/forms.css" Enabled="false" DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                            </dxe:ASPxSpinEdit>
                        </td>
                        
                    </tr>
                    
                    <tr>
                        <td>
                            <asp:Label ID="labelValorIr225" runat="server" CssClass="labelNormal" Text="Residual 22,5%:"></asp:Label>
                        </td>
                        
                        <td>    
                            <dxe:ASPxSpinEdit ID="textResidual225" runat="server" CssClass="textValor" ClientInstanceName="textResidual225"
                                              CssFilePath="../../css/forms.css" SpinButtons-ShowIncrementButtons="false" 
                                              MaxLength="20" NumberType="Float" DecimalPlaces="2" >
                            </dxe:ASPxSpinEdit>
                        </td>
                        
                        <td>
                            <asp:Label ID="label6" runat="server" CssClass="labelNormal" Text="Residual 22,5% Calculado:"></asp:Label>
                        </td>
                        
                        <td>    
                            <dxe:ASPxSpinEdit ID="textResidual225Calc" runat="server" CssClass="textValor" ClientInstanceName="textResidual225Calc"
                                              CssFilePath="../../css/forms.css" Enabled="false" DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                            </dxe:ASPxSpinEdit>
                        </td>
                        
                    </tr>
                    
                    <tr>
                        <td>
                            <div class="linkButton linkButtonNoBorder">
                                <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"  
                                                   OnClientClick="gridPosicaoCotista.PerformCallback(selectedIndex); limpaPopupCotista(); return false;">
                                                   <asp:Literal ID="Literal1" runat="server" Text="Salvar"/><div></div></asp:LinkButton>
                            </div>
                        </td>
                        <td>
                            <div class="linkButton linkButtonNoBorder">
                                <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false"
                                                CssClass="btnCancel" OnClientClick="limpaPopupCotista(); return false;">
                                                <asp:Literal ID="Literal9" runat="server" Text="Cancelar"/><div></div></asp:LinkButton>
                            </div>
                        </td>
                        
                        <td>
                            <dxe:ASPxButton ID="btnVoltarValores" runat="server" ClientInstanceName="btnVoltarValores"
                                Text="Retorna Valores" Width="140px">
                                <ClientSideEvents Click="retornaValores" />
                            </dxe:ASPxButton>
                        </td>
                        
                    </tr>                    
                </table>
            </div>
            
        </div>      
        </dxpc:PopupControlContentControl></ContentCollection>
        <ClientSideEvents PopUp="function(s, e) {gridPosicaoCotista.PerformCallback();
                                                 textValorIofVirtual.SetEnabled(false); 
                                                 textResidual15.SetEnabled(false);
                                                 textResidual175.SetEnabled(false); 
                                                 textResidual20.SetEnabled(false);
                                                 textResidual225.SetEnabled(false);}"
                          CloseUp="function(s, e) {FechaPopupCotista(); return false;}" />   
    </dxpc:ASPxPopupControl>
                                        
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container_small">
    
    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Ajuste de Posições de Come Cotas"></asp:Label>
    </div>
        
    <div id="mainContent">
            
        <atk:AKRequiredValidator ID="RequiredValidator1" runat="server" ValidationGroup="ATK" />            
        
        <div class="reportFilter">
        
        <div class="dataMessage">
        <asp:ValidationSummary ID="validationSummary" runat="server" 
                         HeaderText="Campos com * são obrigatórios." EnableClientScript="true"
                         DisplayMode="SingleParagraph" ShowSummary="true" ValidationGroup="ATK"/>
        </div>                                      
        
        <table cellpadding="3">
        
            <tr>
            <td class="td_Label">                
                <asp:Label ID="labelCarteira" runat="server" CssClass="labelRequired" Text="Carteira:"></asp:Label>
            </td>
            
            <td>
                <dxe:ASPxSpinEdit ID="btnEditCodigoCarteira" runat="server" CssClass="textButtonEdit" 
                                    ClientInstanceName="btnEditCodigoCarteira">  
                <Buttons>
                    <dxe:EditButton>
                    </dxe:EditButton>                                
                </Buttons>        
                <ClientSideEvents
                         KeyPress="function(s, e) {document.getElementById('textNomeCarteira').value = '';}" 
                         ButtonClick="function(s, e) {popupCarteira.ShowAtElementByID(s.name);}" 
                         LostFocus="function(s, e) {OnLostFocus(popupMensagemCarteira, ASPxCallback1, btnEditCodigoCarteira);}"
                />                            
                </dxe:ASPxSpinEdit>                
            </td>                          
            
            <td>
                <asp:TextBox ID="textNomeCarteira" runat="server" CssClass="textNome" Enabled="false"></asp:TextBox>                
            </td>
            </tr>
            
            <tr>
            <td class="td_Label">
                <asp:Label ID="labelCotista" runat="server" CssClass="labelRequired" Text="Cotista:"></asp:Label>
            </td>
            
            <td>
                <dxe:ASPxSpinEdit ID="btnEditCodigoCotista" runat="server" CssClass="textButtonEdit" 
                                    ClientInstanceName="btnEditCodigoCotista" >  
                <Buttons>
                    <dxe:EditButton>
                    </dxe:EditButton>                                
                </Buttons>        
                <ClientSideEvents
                         KeyPress="function(s, e) {document.getElementById('textNomeCotista').value = '';}" 
                         ButtonClick="function(s, e) {popupCotista.ShowAtElementByID(s.name);}" 
                         LostFocus="function(s, e) {OnLostFocus(popupMensagemCotista, ASPxCallback2, btnEditCodigoCotista);}"
                />                            
                </dxe:ASPxSpinEdit>                
            </td>                          
            
            <td>
                <asp:TextBox ID="textNomeCotista" runat="server" CssClass="textNome" Enabled="false"></asp:TextBox>                
            </td>
            </tr>
            
            <tr>
            <td class="td_Label">
                <asp:Label ID="labelData" runat="server" CssClass="labelRequired" Text="Data:"></asp:Label>
            </td>                        
            <td>
                <dxe:ASPxDateEdit ID="textData" runat="server" ClientInstanceName="textData" />              
            </td>         
            
            <td class="tdButton" width="170">
                <div class="linkButton linkButtonNoBorder">
                <asp:LinkButton ID="btnPosicaoCotista" ForeColor="black" runat="server" CssClass="btnPopup" ValidationGroup="ATK" OnClientClick="popupPosicaoCotista.ShowAtElementByID(); return false;"><asp:Literal ID="Literal5" runat="server" Text="Ver Posições"/><div></div></asp:LinkButton>
                </div>
            </td>         
                                                                                        
            </tr>            
        </table>
        
        </div>
        
    </div>
    </div>
    </td></tr></table>
    </div>
    
    <cc1:esDataSource ID="EsDSPosicaoCotista" runat="server" OnesSelect="EsDSPosicaoCotista_esSelect" LowLevelBind="true"/>    
    <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" />
    <cc1:esDataSource ID="EsDSCotista" runat="server" OnesSelect="EsDSCotista_esSelect" />        
    
    </form>
</body>
</html>