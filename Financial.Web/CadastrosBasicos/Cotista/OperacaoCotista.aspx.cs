﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using DevExpress.Web;

using Financial.Util;
using Financial.Common;
using Financial.Security;
using Financial.Fundo;
using Financial.Investidor;
using Financial.ContaCorrente;
using Financial.InvestidorCotista;
using Financial.Web.Common;
using Financial.Tributo.Enums;

using Financial.InvestidorCotista.Enums;
using Financial.Investidor.Enums;
using Financial.Common.Enums;
using Financial.ContaCorrente.Enums;
using System.Threading;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Fundo.Enums;
using EntitySpaces.Interfaces;
using Financial.CRM;

using Newtonsoft.Json;
using System.Web.Configuration;
using System.Net.Configuration;

using Financial.InvestidorCotista.Perfil;

public partial class CadastrosBasicos_OperacaoCotista : CadastroBasePage
{
    SuitabilityPerfilInvestidor suitabilityPerfilCotista = new SuitabilityPerfilInvestidor();
    SuitabilityPerfilInvestidor suitabilityPerfilPosicao = new SuitabilityPerfilInvestidor();

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasFiltro = true;
        this.HasPopupCarteira = true;
        this.HasPopupCotista = true;
        this.HasPanelFieldsLoading = true;
        base.Page_Load(sender, e);

        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
                          new List<string>(new string[] { OperacaoCotistaMetadata.ColumnNames.TipoOperacao }));
    }

    protected void dropTipoOperacao_Load(object sender, EventArgs e)
    {
        ASPxComboBox dropTipoOperacao = gridCadastro.FindEditFormTemplateControl("dropTipoOperacao") as ASPxComboBox;
        dropTipoOperacao.Items.Add("Aplicação", 1);
        dropTipoOperacao.Items.Add("Resgate Bruto", 2);
        dropTipoOperacao.Items.Add("Resgate Líquido", 3);
        dropTipoOperacao.Items.Add("Resgate Cotas", 4);
        dropTipoOperacao.Items.Add("Resgate Total", 5);
        dropTipoOperacao.Items.Add("Aplicação Especial", 10);
        dropTipoOperacao.Items.Add("Aplicação Ações", 11);
        dropTipoOperacao.Items.Add("Resgate Especial", 12);
        dropTipoOperacao.Items.Add("Come Cotas", 20);
        dropTipoOperacao.Items.Add("Retirada", (int)TipoOperacaoCotista.Retirada);
        dropTipoOperacao.Items.Add("Depósito", (int)TipoOperacaoCotista.Deposito);
        dropTipoOperacao.Items.Add("Aplicação Cotas", (int)TipoOperacaoCotista.AplicacaoCotas);
    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSOperacaoCotista_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        PermissaoCotistaQuery permissaoCotistaQuery = new PermissaoCotistaQuery("P");
        OperacaoCotistaQuery operacaoCotistaQuery = new OperacaoCotistaQuery("O");
        CotistaQuery cotistaQuery = new CotistaQuery("C");
        CarteiraQuery carteiraQuery = new CarteiraQuery("A");
        ClienteQuery clienteCarteira = new ClienteQuery("cCarteira");
        SeriesOffShoreQuery serieOffShoreQuery = new SeriesOffShoreQuery("SO");

        operacaoCotistaQuery.Select(operacaoCotistaQuery, cotistaQuery.Apelido.As("ApelidoCotista"),
                                    carteiraQuery.Apelido.As("ApelidoCarteira"),
                                    carteiraQuery.IdAgenteGestor,
                                    serieOffShoreQuery.Mnemonico.As("MnemonicoSerie"));
        operacaoCotistaQuery.InnerJoin(cotistaQuery).On(operacaoCotistaQuery.IdCotista.Equal(cotistaQuery.IdCotista));
        operacaoCotistaQuery.InnerJoin(carteiraQuery).On(operacaoCotistaQuery.IdCarteira.Equal(carteiraQuery.IdCarteira));
        operacaoCotistaQuery.InnerJoin(clienteCarteira).On(clienteCarteira.IdCliente.Equal(carteiraQuery.IdCarteira));
        operacaoCotistaQuery.InnerJoin(permissaoCotistaQuery).On(permissaoCotistaQuery.IdCotista.Equal(cotistaQuery.IdCotista));
        operacaoCotistaQuery.LeftJoin(serieOffShoreQuery).On(operacaoCotistaQuery.IdSeriesOffShore.Equal(serieOffShoreQuery.IdSeriesOffShore));
        operacaoCotistaQuery.Where(operacaoCotistaQuery.TipoOperacao.NotIn((byte)TipoOperacaoCotista.Amortizacao,
                                                                           (byte)TipoOperacaoCotista.Juros,
                                                                           (byte)TipoOperacaoCotista.IncorporacaoAplicacao,
                                                                           (byte)TipoOperacaoCotista.IncorporacaoResgate),
                                   permissaoCotistaQuery.IdUsuario.Equal(idUsuario),
                                   clienteCarteira.IdPessoa.NotEqual(cotistaQuery.IdPessoa));

        if (!String.IsNullOrEmpty(btnEditCodigoCarteiraFiltro.Text))
        {
            operacaoCotistaQuery.Where(operacaoCotistaQuery.IdCarteira.Equal(Convert.ToInt32(btnEditCodigoCarteiraFiltro.Text)));
        }

        if (!String.IsNullOrEmpty(btnEditCodigoCotistaFiltro.Text))
        {
            operacaoCotistaQuery.Where(operacaoCotistaQuery.IdCotista.Equal(Convert.ToInt32(btnEditCodigoCotistaFiltro.Text)));
        }

        if (!String.IsNullOrEmpty(textDataInicio.Text))
        {
            operacaoCotistaQuery.Where(operacaoCotistaQuery.DataOperacao.GreaterThanOrEqual(textDataInicio.Text));
        }

        if (!String.IsNullOrEmpty(textDataFim.Text))
        {
            operacaoCotistaQuery.Where(operacaoCotistaQuery.DataOperacao.LessThanOrEqual(textDataFim.Text));
        }

        if (dropTipoOperacaoFiltro.SelectedIndex > -1)
        {
            operacaoCotistaQuery.Where(operacaoCotistaQuery.TipoOperacao.Equal(Convert.ToByte(dropTipoOperacaoFiltro.SelectedItem.Value)));
        }

        if (dropGestor.SelectedIndex > -1)
        {
            operacaoCotistaQuery.Where(carteiraQuery.IdAgenteGestor.Equal(dropGestor.SelectedItem.Value));
        }

        operacaoCotistaQuery.OrderBy(operacaoCotistaQuery.DataOperacao.Descending);

        OperacaoCotistaCollection coll = new OperacaoCotistaCollection();
        coll.Load(operacaoCotistaQuery);

        e.Collection = coll;
    }

    protected void EsDSAgenteMercado_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        AgenteMercadoCollection coll = new AgenteMercadoCollection();
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.LoadAll();
        e.Collection = coll;
    }

    protected void EsDSLocalNegociacao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        LocalNegociacaoCollection coll = new LocalNegociacaoCollection();

        coll.Query.OrderBy(coll.Query.IdLocalNegociacao.Ascending);
        coll.LoadAll();

        e.Collection = coll;
    }

    protected void EsDSCotista_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CotistaCollection coll = new CotistaCollection();
        coll.BuscaCotistasComAcesso(Context.User.Identity.Name);

        e.Collection = coll;
    }

    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CarteiraCollection coll = new CarteiraCollection();
        coll.BuscaCarteirasFundosClubesBoletagem(Context.User.Identity.Name);

        e.Collection = coll;
    }

    protected void EsDSFormaLiquidacao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        FormaLiquidacaoCollection coll = new FormaLiquidacaoCollection();
        coll.LoadAll();

        e.Collection = coll;
    }

    protected void EsDSPosicaoCotista_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        if (gridCadastro.IsEditing)
        {
            ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
            ASPxSpinEdit btnEditCodigoCotista = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCotista") as ASPxSpinEdit;

            if (btnEditCodigoCarteira.Text != "" && btnEditCodigoCotista.Text != "")
            {
                SeriesOffShoreQuery seriesQuery = new SeriesOffShoreQuery("SO");
                PosicaoCotistaQuery posicaoQuery = new PosicaoCotistaQuery("PC");

                PosicaoCotistaCollection coll = new PosicaoCotistaCollection();

                posicaoQuery.Select(posicaoQuery, seriesQuery.Mnemonico);
                posicaoQuery.LeftJoin(seriesQuery).On(posicaoQuery.IdSeriesOffShore.Equal(seriesQuery.IdSeriesOffShore));
                posicaoQuery.Where(posicaoQuery.IdCarteira.Equal(Convert.ToInt32(btnEditCodigoCarteira.Text)) &
                                 posicaoQuery.IdCotista.Equal(Convert.ToInt32(btnEditCodigoCotista.Text)) &
                                 posicaoQuery.Quantidade.NotEqual(0));
                posicaoQuery.OrderBy(posicaoQuery.DataConversao.Descending);
                coll.Load(posicaoQuery);

                e.Collection = coll;
            }
            else
            {
                e.Collection = new PosicaoCotistaCollection();
            }
        }
        else
        {
            e.Collection = new PosicaoCotistaCollection();
        }
    }

    protected void EsDSContaCorrente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        int tipoOperacao = 0;
        ContaCorrenteCollection coll = new ContaCorrenteCollection();

        ASPxComboBox dropTipoOperacao = gridCadastro.FindEditFormTemplateControl("dropTipoOperacao") as ASPxComboBox;

        int idPessoa = 0;

        ASPxSpinEdit codigoPesquisa = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        if (codigoPesquisa.Text != "")
        {
            int idCarteira = Convert.ToInt32(codigoPesquisa.Text);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCarteira);
            idPessoa = cliente.IdPessoa.Value;
        }

        ContaCorrenteQuery contaCorrenteQuery = new ContaCorrenteQuery("CC");
        AgenciaQuery agenciaQuery = new AgenciaQuery("A");
        BancoQuery bancoQuery = new BancoQuery("B");

        contaCorrenteQuery.Select((bancoQuery.Nome + " - " + agenciaQuery.Codigo + " - " + contaCorrenteQuery.Numero).As("ContaCorrente"), contaCorrenteQuery.IdConta);
        contaCorrenteQuery.InnerJoin(bancoQuery).On(contaCorrenteQuery.IdBanco.Equal(bancoQuery.IdBanco));
        contaCorrenteQuery.InnerJoin(agenciaQuery).On(contaCorrenteQuery.IdAgencia.Equal(agenciaQuery.IdAgencia));
        contaCorrenteQuery.Where(contaCorrenteQuery.IdPessoa.Equal(idPessoa));


        coll.Load(contaCorrenteQuery);
        e.Collection = coll;
    }

    protected void EsDSContaCorrenteDestino_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        int tipoOperacao = 0;
        ContaCorrenteCollection coll = new ContaCorrenteCollection();

        ASPxComboBox dropTipoOperacao = gridCadastro.FindEditFormTemplateControl("dropTipoOperacao") as ASPxComboBox;

        if (dropTipoOperacao.SelectedIndex == -1)
        {
            e.Collection = coll;
            return;
        }
        else
        {
            tipoOperacao = Convert.ToByte(dropTipoOperacao.SelectedItem.Value);
        }

        ASPxSpinEdit codigoPesquisa = new ASPxSpinEdit();
        int idPessoa = 0;
        if (tipoOperacao == (byte)TipoOperacaoCotista.Aplicacao)
        {
            codigoPesquisa = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
            if (codigoPesquisa.Text != "")
            {
                int idPesquisa = Convert.ToInt32(codigoPesquisa.Text);
                Cliente cliente = new Cliente();
                cliente.LoadByPrimaryKey(idPesquisa);
                idPessoa = cliente.IdPessoa.Value;
            }
        }

        if (tipoOperacao == (byte)TipoOperacaoCotista.ResgateBruto || tipoOperacao == (byte)TipoOperacaoCotista.ResgateTotal)
        {
            codigoPesquisa = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCotista") as ASPxSpinEdit;
            if (codigoPesquisa.Text != "")
            {
                int idPesquisa = Convert.ToInt32(codigoPesquisa.Text);
                Cotista cotista = new Cotista();
                cotista.LoadByPrimaryKey(idPesquisa);
                idPessoa = cotista.IdPessoa.Value;
            }
        }

        ContaCorrenteQuery contaCorrenteQuery = new ContaCorrenteQuery("CC");
        AgenciaQuery agenciaQuery = new AgenciaQuery("A");
        BancoQuery bancoQuery = new BancoQuery("B");

        contaCorrenteQuery.Select((bancoQuery.Nome + " - " + agenciaQuery.Codigo + " - " + contaCorrenteQuery.Numero).As("ContaCorrente"), contaCorrenteQuery.IdConta);
        contaCorrenteQuery.InnerJoin(bancoQuery).On(contaCorrenteQuery.IdBanco.Equal(bancoQuery.IdBanco));
        contaCorrenteQuery.InnerJoin(agenciaQuery).On(contaCorrenteQuery.IdAgencia.Equal(agenciaQuery.IdAgencia));
        contaCorrenteQuery.Where(contaCorrenteQuery.IdPessoa.Equal(idPessoa));


        coll.Load(contaCorrenteQuery);
        e.Collection = coll;
    }

    protected void contaCorrente_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
    {
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        if (btnEditCodigoCarteira.Text != "")
        {
            int idPesquisa = Convert.ToInt32(btnEditCodigoCarteira.Text);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idPesquisa);
            if (cliente.IdTipo.Equals(TipoClienteFixo.OffShore_PF) || cliente.IdTipo.Equals(TipoClienteFixo.OffShore_PJ))
            {
                ASPxComboBox dropContaCorrente = (ASPxComboBox)sender;
                dropContaCorrente.DataBind();
            }
        }

    }

    #endregion

    protected void callbackPenalidadeLockUp_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxSpinEdit btnEditCodigoCotista = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCotista") as ASPxSpinEdit;
        ASPxComboBox dropTipoOperacao = gridCadastro.FindEditFormTemplateControl("dropTipoOperacao") as ASPxComboBox;
        ASPxDateEdit textDataOperacao = gridCadastro.FindEditFormTemplateControl("textDataOperacao") as ASPxDateEdit;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;

        ASPxSpinEdit textValorDespesas = gridCadastro.FindEditFormTemplateControl("textValorDespesas") as ASPxSpinEdit;
        ASPxSpinEdit textValorTaxas = gridCadastro.FindEditFormTemplateControl("textValorTaxas") as ASPxSpinEdit;
        ASPxSpinEdit textValorTributos = gridCadastro.FindEditFormTemplateControl("textValorTributos") as ASPxSpinEdit;

        if (btnEditCodigoCarteira.Text == "" ||
            btnEditCodigoCotista.Text == "" ||
            textDataOperacao.Text == "" ||
            textValor.Text == "" ||
            dropTipoOperacao.SelectedIndex == -1)
        {
            e.Result = "Erro|Favor informar Carteira, Cotista, Data da Operação, Valor/Qtde e Tipo.";
            return;
        }
        int idCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);
        int idCotista = Convert.ToInt32(btnEditCodigoCotista.Text);
        int tipoOperacao = Convert.ToByte(dropTipoOperacao.SelectedItem.Value);
        DateTime dataOperacao = Convert.ToDateTime(textDataOperacao.Text);
        decimal valor = Convert.ToDecimal(textValor.Text);
        decimal valorDespesas = 0;
        if (textValorDespesas.Text != "") Convert.ToDecimal(textValorDespesas.Text);
        decimal valorTaxas = 0;
        if (textValorTaxas.Text != "") Convert.ToDecimal(textValorTaxas.Text);
        decimal valorTributos = 0;
        if (textValorTributos.Text != "") Convert.ToDecimal(textValorTributos.Text);

        if (tipoOperacao == (byte)TipoOperacaoCotista.ResgateBruto ||
            tipoOperacao == (byte)TipoOperacaoCotista.ResgateTotal)
        {

            //Seleciona Classe
            ClassesOffShore classesOffShore = new ClassesOffShore();
            classesOffShore.LoadByPrimaryKey(idCarteira);

            //Seleciona posição da aplicação
            PosicaoCotista posicaoCotistaCheck = new PosicaoCotista();
            posicaoCotistaCheck.Query.Select(posicaoCotistaCheck.Query.DataConversao);
            posicaoCotistaCheck.Query.Where(posicaoCotistaCheck.Query.IdCarteira.Equal(idCarteira),
                                            posicaoCotistaCheck.Query.IdCotista.Equal(idCotista),
                                            posicaoCotistaCheck.Query.Quantidade.GreaterThan(0));

            if (hiddenNotaResgatada.Value != "")
            {
                posicaoCotistaCheck.Query.Where(posicaoCotistaCheck.Query.IdPosicao.Equal(Convert.ToInt32(hiddenNotaResgatada.Value)));
            }
            else
            {
                e.Result = "Erro|Favor especificar qual a posição de deseja resgatar.";
                return;
            }

            posicaoCotistaCheck.Query.Load();

            TimeSpan ts = dataOperacao - posicaoCotistaCheck.DataConversao.Value;

            if (classesOffShore.LockUp.Value > 0 && ts.Days < classesOffShore.LockUp.Value)
            {
                if (classesOffShore.HardSoft.Equals((int)HardSoft.Soft))
                {
                    decimal valorPenalidade = (((valor - (valorDespesas + valorTaxas + valorTributos)) * classesOffShore.PenalidadeResgate.Value) / 100);
                    e.Result = "OK|" + valorPenalidade.ToString();
                    return;
                }
            }
        }
        else
        {
            e.Result = "Erro|Somente para operações de resgate de carteiras off shore.";
        }
    }

    protected void callbackHoldBack_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxComboBox dropTipoOperacao = gridCadastro.FindEditFormTemplateControl("dropTipoOperacao") as ASPxComboBox;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;

        ASPxSpinEdit textValorPenalidadeLockUp = gridCadastro.FindEditFormTemplateControl("textValorPenalidadeLockUp") as ASPxSpinEdit;
        ASPxSpinEdit textValorDespesas = gridCadastro.FindEditFormTemplateControl("textValorDespesas") as ASPxSpinEdit;
        ASPxSpinEdit textValorTaxas = gridCadastro.FindEditFormTemplateControl("textValorTaxas") as ASPxSpinEdit;
        ASPxSpinEdit textValorTributos = gridCadastro.FindEditFormTemplateControl("textValorTributos") as ASPxSpinEdit;

        if (btnEditCodigoCarteira.Text == "" ||
            textValor.Text == "" ||
            dropTipoOperacao.SelectedIndex == -1)
        {
            e.Result = "Erro|Favor informar Carteira, Valor/Qtde e Tipo.";
            return;
        }

        int idCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);
        int tipoOperacao = Convert.ToByte(dropTipoOperacao.SelectedItem.Value);

        decimal valor = Convert.ToDecimal(textValor.Text);
        decimal valorDespesas = 0;
        if (textValorDespesas.Text != "") valorDespesas = Convert.ToDecimal(textValorDespesas.Text);
        decimal valorTaxas = 0;
        if (textValorTaxas.Text != "") valorTaxas = Convert.ToDecimal(textValorTaxas.Text);
        decimal valorTributos = 0;
        if (textValorTributos.Text != "") valorTributos = Convert.ToDecimal(textValorTributos.Text);
        decimal valorPenalidadeLockUp = 0;
        if (textValorPenalidadeLockUp.Text != "") valorPenalidadeLockUp = Convert.ToDecimal(textValorPenalidadeLockUp.Text);

        if (tipoOperacao == (byte)TipoOperacaoCotista.ResgateBruto ||
            tipoOperacao == (byte)TipoOperacaoCotista.ResgateTotal)
        {

            //Seleciona Classe
            ClassesOffShore classesOffShore = new ClassesOffShore();
            classesOffShore.LoadByPrimaryKey(idCarteira);

            if (classesOffShore.HoldBack.Value > 0)
            {
                decimal valorHoldBack = (((valor - (valorDespesas + valorTaxas + valorTributos)) - valorPenalidadeLockUp) * classesOffShore.HoldBack.Value) / 100;
                e.Result = "OK|" + valorHoldBack.ToString();
                return;
            }
        }
        else
        {
            e.Result = "Erro|Somente para operações de resgate de carteiras off shore.";
        }
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void panelEdicao_Load(object sender, EventArgs e)
    {
        base.panelEdicao_Load(sender, e);

        if (gridCadastro.IsEditing && gridCadastro.IsNewRowEditing)
        {
            ASPxComboBox dropFormaLiquidacao = gridCadastro.FindEditFormTemplateControl("dropFormaLiquidacao") as ASPxComboBox;
            dropFormaLiquidacao.SelectedIndex = 0;
        }

        bool travaPainel = false;
        if (gridCadastro.IsEditing && !gridCadastro.IsNewRowEditing)
        {
            if (gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "IdPosicaoResgatada").ToString() != "" &&
                hiddenNotaResgatada.Value == "")
            {
                hiddenNotaResgatada.Value = gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "IdPosicaoResgatada").ToString();
            }

            Label labelEdicao = gridCadastro.FindEditFormTemplateControl("labelEdicao") as Label;

            int idCarteira = Convert.ToInt32(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "IdCarteira"));
            DateTime dataConversao = Convert.ToDateTime(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "DataConversao"));

            Cliente cliente = new Cliente();
            if (cliente.LoadByPrimaryKey(idCarteira))
            {
                DateTime dataDia = cliente.DataDia.Value;
                byte status = cliente.Status.Value;
                if (status == (byte)StatusCliente.Divulgado)
                {
                    travaPainel = true;
                    labelEdicao.Text = "Carteira está fechada. Dados não podem ser alterados.";
                }
                else if (DateTime.Compare(dataDia, dataConversao) > 0)
                {
                    travaPainel = true;
                    labelEdicao.Text = "Data de conversão anterior à data da carteira. Dados não podem ser alterados.";
                }
            }
        }

        if (travaPainel)
        {
            LinkButton btnOK = gridCadastro.FindEditFormTemplateControl("btnOK") as LinkButton;
            btnOK.Visible = false;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        #region Tipos de Operação apenas para Fundo de Investimento
        List<byte> lstApenasParaFundo = new List<byte>();
        lstApenasParaFundo.Add((byte)TipoOperacaoFundo.IngressoAtivoImpactoCota);
        lstApenasParaFundo.Add((byte)TipoOperacaoFundo.IngressoAtivoImpactoQtde);
        lstApenasParaFundo.Add((byte)TipoOperacaoFundo.RetiradaAtivoImpactoCota);
        lstApenasParaFundo.Add((byte)TipoOperacaoFundo.RetiradaAtivoImpactoQtde);
        lstApenasParaFundo.Add((byte)TipoOperacaoFundo.RetiradaAtivoImpactoQtde);
        #endregion

        if (!gridCadastro.IsEditing) //Testa deleção
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdCarteira");
            List<object> keyDataLiquidacao = gridCadastro.GetSelectedFieldValues("DataLiquidacao");
            List<object> keyDataConversao = gridCadastro.GetSelectedFieldValues("DataConversao");

            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idCarteira = Convert.ToInt32(keyValuesId[i]);
                DateTime dataLiquidacao = Convert.ToDateTime(keyDataLiquidacao[i]);
                DateTime dataConversao = Convert.ToDateTime(keyDataConversao[i]);

                Cliente cliente = new Cliente();
                if (cliente.IdCliente.GetValueOrDefault(0) != idCarteira)
                    cliente.LoadByPrimaryKey(idCarteira);

                DateTime dataDia = cliente.DataDia.Value;
                byte status = cliente.Status.Value;
                string clienteDescricao = cliente.Apelido + " (" + cliente.IdCliente.ToString() + ")";
                DateTime dataAux = (DateTime.Compare(dataLiquidacao, dataConversao) > 0) ? dataConversao : dataLiquidacao;

                if (status == (byte)StatusCliente.Divulgado)
                {
                    e.Result = "Carteira " + clienteDescricao + " está fechada! Deleção cancelada.";
                    return;
                }
                else if (DateTime.Compare(dataDia, dataAux) > 0)
                {
                    e.Result = "Lançamento com data " + dataAux.ToString().Substring(0, 10) + " anterior à data da carteira " + clienteDescricao + "! Deleção cancelada.";
                    return;
                }
            }
        }
        else
        {
            ASPxDateEdit textDataOperacao = gridCadastro.FindEditFormTemplateControl("textDataOperacao") as ASPxDateEdit;
            ASPxDateEdit textDataRegistro = gridCadastro.FindEditFormTemplateControl("textDataRegistro") as ASPxDateEdit;
            ASPxDateEdit textDataConversao = gridCadastro.FindEditFormTemplateControl("textDataConversao") as ASPxDateEdit;
            ASPxDateEdit textDataLiquidacao = gridCadastro.FindEditFormTemplateControl("textDataLiquidacao") as ASPxDateEdit;
            ASPxComboBox dropTipoOperacao = gridCadastro.FindEditFormTemplateControl("dropTipoOperacao") as ASPxComboBox;
            ASPxComboBox dropTipoResgate = gridCadastro.FindEditFormTemplateControl("dropTipoResgate") as ASPxComboBox;
            ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
            ASPxSpinEdit btnEditCodigoCotista = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCotista") as ASPxSpinEdit;
            ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
            ASPxComboBox dropFormaLiquidacao = gridCadastro.FindEditFormTemplateControl("dropFormaLiquidacao") as ASPxComboBox;
            ASPxComboBox dropLocalNegociacao = gridCadastro.FindEditFormTemplateControl("dropLocalNegociacao") as ASPxComboBox;
            ASPxComboBox dropContaCorrente = gridCadastro.FindEditFormTemplateControl("dropContaCorrente") as ASPxComboBox;
            ASPxComboBox dropFieModalidade = gridCadastro.FindEditFormTemplateControl("dropFieModalidade") as ASPxComboBox;
            ASPxComboBox dropFieTabelaIr = gridCadastro.FindEditFormTemplateControl("dropFieTabelaIr") as ASPxComboBox;

            #region Campos obrigatórios
            List<Control> controles = new List<Control>();
            controles.Add(btnEditCodigoCarteira);
            controles.Add(btnEditCodigoCotista);
            controles.Add(textDataOperacao);
            controles.Add(textDataRegistro);
            controles.Add(textDataConversao);
            controles.Add(textDataLiquidacao);
            controles.Add(dropTipoOperacao);
            controles.Add(dropLocalNegociacao);

            if (base.TestaObrigatorio(controles) != "")
            {
                e.Result = "Campos com * são obrigatórios!";
                return;
            }

            #region Valida forma liquidação
            byte tipoOperacao = Convert.ToByte(dropTipoOperacao.SelectedItem.Value);
            List<byte> lstLiquidacaoNaoObrigatorio = new List<byte>();
            lstLiquidacaoNaoObrigatorio.InsertRange(0, lstApenasParaFundo);

            lstLiquidacaoNaoObrigatorio.Add((byte)TipoOperacaoCotista.Deposito);
            lstLiquidacaoNaoObrigatorio.Add((byte)TipoOperacaoCotista.Retirada);
            if (!lstLiquidacaoNaoObrigatorio.Contains(tipoOperacao))
            {
                controles = new List<Control>();
                controles.Add(dropFormaLiquidacao);

                if (base.TestaObrigatorio(controles) != "")
                {
                    e.Result = "Campos com * são obrigatórios!";
                    return;
                }
            }
            #endregion

            #endregion

            int idCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);
            int idCotista = Convert.ToInt32(btnEditCodigoCotista.Text);
            DateTime dataOperacao = Convert.ToDateTime(textDataOperacao.Text);
            DateTime dataConversao = Convert.ToDateTime(textDataConversao.Text);
            DateTime dataLiquidacao = Convert.ToDateTime(textDataLiquidacao.Text);

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            Cotista cotista = new Cotista();
            cotista.LoadByPrimaryKey(idCotista);

            if (!Calendario.IsDiaUtil(textDataRegistro.Date, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil))
            {
                e.Result = "Data de registro não é dia útil.";
                return;
            }

            if (!Calendario.IsDiaUtil(dataOperacao, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil))
            {
                e.Result = "Data da operação não é dia útil.";
                return;
            }
            if (!Calendario.IsDiaUtil(dataConversao, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil))
            {
                e.Result = "Data da conversão não é dia útil.";
                return;
            }
            if (!Calendario.IsDiaUtil(dataLiquidacao, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil))
            {
                e.Result = "Data da liquidação não é dia útil.";
                return;
            }

            if (DateTime.Compare(dataOperacao, textDataRegistro.Date) > 0)
            {
                e.Result = "Data da operação não pode ser posterior à data de registro.";
                return;
            }

            if (DateTime.Compare(dataOperacao, dataConversao) > 0)
            {
                e.Result = "Data da operação não pode ser posterior à data da conversão.";
                return;
            }

            if (DateTime.Compare(dataOperacao, dataLiquidacao) > 0)
            {
                e.Result = "Data da operação não pode ser posterior à data da liquidação.";
                return;
            }

            if (tipoOperacao == (byte)TipoOperacaoCotista.Aplicacao ||
                tipoOperacao == (byte)TipoOperacaoCotista.AplicacaoAcoesEspecial ||
                tipoOperacao == (byte)TipoOperacaoCotista.AplicacaoCotas ||
                tipoOperacao == (byte)TipoOperacaoCotista.AplicacaoCotasEspecial)
            {
                if (DateTime.Compare(dataConversao, dataLiquidacao) < 0)
                {
                    e.Result = "Data de conversão não pode ser anterior a data de liquidação.";
                    return;
                }
            }

            if (tipoOperacao == (byte)TipoOperacaoCotista.ResgateBruto ||
                tipoOperacao == (byte)TipoOperacaoCotista.ResgateCotas ||
                tipoOperacao == (byte)TipoOperacaoCotista.ResgateCotasEspecial ||
                tipoOperacao == (byte)TipoOperacaoCotista.ResgateLiquido ||
                tipoOperacao == (byte)TipoOperacaoCotista.ResgateTotal)
            {
                if (DateTime.Compare(dataConversao, dataLiquidacao) > 0)
                {
                    e.Result = "Data de conversão não pode ser posterior a data de liquidação.";
                    return;
                }
            }

            if (tipoOperacao == (byte)TipoOperacaoCotista.Aplicacao)
            {
                if (carteira.RealizaOfertaSubscricao.Equals("S"))
                {
                    OfertaDistribuicaoCotasCollection ofertaDistribuicaoCotasCollection = new OfertaDistribuicaoCotasCollection();
                    ofertaDistribuicaoCotasCollection.Query.Where(ofertaDistribuicaoCotasCollection.Query.IdCarteira.Equal(idCarteira) &
                                                                  ofertaDistribuicaoCotasCollection.Query.DataInicialOferta.LessThanOrEqual(dataOperacao));
                    ofertaDistribuicaoCotasCollection.Query.Where(ofertaDistribuicaoCotasCollection.Query.DataFinalOferta.GreaterThanOrEqual(dataOperacao) |
                                                                 ofertaDistribuicaoCotasCollection.Query.DataFinalOferta.IsNull());
                    ofertaDistribuicaoCotasCollection.Query.Load();

                    if (ofertaDistribuicaoCotasCollection.Count == 0)
                    {
                        e.Result = "Não existe oferta de distribuição de cotas vigente para o Fundo na data da operação.";
                        return;
                    }
                    else
                    {
                        OfertaDistribuicaoCotas ofertaDistribuicaoCotas = ofertaDistribuicaoCotasCollection[0];

                        if (ofertaDistribuicaoCotas.ApenasInvestidorProfissional.Equals("S"))
                        {
                            if (cotista.InvestidorProfissional.Equals("N"))
                            {
                                e.Result = "Subscrições no Fundo são aceitas apenas para investidores profissionais.";
                                return;
                            }
                        }

                        if (ofertaDistribuicaoCotas.ApenasInvestidorQualificado.Equals("S"))
                        {


                            if (cotista.InvestidorQualificado.Equals("N"))
                            {
                                e.Result = "Subscrições no Fundo são aceitas apenas para investidores qualificados.";
                                return;
                            }
                        }

                        //Verifica quantidade total da emissão
                        OperacaoCotistaQuery operacaoCotistaQuery = new OperacaoCotistaQuery("OC");
                        AplicacaoOfertaDistribuicaoCotasQuery aplicacaoOfertaDistribuicaoCotasQuery = new AplicacaoOfertaDistribuicaoCotasQuery("AODC");

                        operacaoCotistaQuery.Select(operacaoCotistaQuery.ValorBruto.Sum());
                        operacaoCotistaQuery.InnerJoin(aplicacaoOfertaDistribuicaoCotasQuery).On(operacaoCotistaQuery.IdOperacao.Equal(aplicacaoOfertaDistribuicaoCotasQuery.IdOperacao));
                        operacaoCotistaQuery.Where(operacaoCotistaQuery.IdCarteira.Equal(idCarteira));

                        OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();
                        operacaoCotistaCollection.Load(operacaoCotistaQuery);

                        decimal ValorTotalAporte = operacaoCotistaCollection[0].ValorBruto.GetValueOrDefault(0);
                        decimal valorOperacao = Convert.ToDecimal(textValor.Text);

                        if ((ValorTotalAporte + valorOperacao) > (ofertaDistribuicaoCotas.ValorCota * ofertaDistribuicaoCotas.QuantidadeTotalCotas))
                        {
                            e.Result = "O valor aplicado excede o valor total da emissão de cotas a serem distribuídas para o Fundo;";
                            return;
                        }

                    }
                }
            }

            Cliente cliente = new Cliente();
            if (cliente.LoadByPrimaryKey(idCarteira))
            {
                DateTime dataDia = cliente.DataDia.Value;
                byte status = cliente.Status.Value;
                if (status == (byte)StatusCliente.Divulgado)
                {
                    e.Result = "Carteira está fechada! Operação não pode ser realizada.";
                    return;
                }
                else if (DateTime.Compare(dataDia, Convert.ToDateTime(textDataConversao.Text)) > 0)
                {
                    e.Result = "Data informada anterior à data da carteira! Operação não pode ser realizada.";
                    return;
                }
            }

            Carteira carteiraFundo = new Carteira();
            if (carteiraFundo.LoadByPrimaryKey(idCarteira))
            {
                string resgateLiberado = carteiraFundo.LiberaResgate;
                if (resgateLiberado == "N")
                {
                    e.Result = "Fundo " + idCarteira.ToString() + "  não está liberado para receber resgates.";
                    return;
                }
            }

            if (tipoOperacao != (byte)TipoOperacaoCotista.Aplicacao &&
                tipoOperacao != (byte)TipoOperacaoCotista.AplicacaoAcoesEspecial &&
                tipoOperacao != (byte)TipoOperacaoCotista.AplicacaoCotas &&
                tipoOperacao != (byte)TipoOperacaoCotista.AplicacaoCotasEspecial)
            {
                if (dropTipoResgate.SelectedIndex != -1)
                {
                    if (Convert.ToByte(dropTipoResgate.SelectedItem.Value) == (byte)TipoResgateCotista.Especifico &&
                        hiddenNotaResgatada.Value == "")
                    {
                        e.Result = "Para resgates específicos, deve ser selecionada uma nota de aplicação.";
                        return;
                    }
                }
            }



            if (tipoOperacao != (byte)TipoOperacaoCotista.ResgateTotal)
            {
                if (textValor.Text == "")
                {
                    e.Result = "Para operação diferente de resgate total, é preciso informar o campo de Valor/Qtde.";
                    return;
                }
            }
            else
            {
                OperacaoCotistaCollection operacaoCotistaColl = new OperacaoCotistaCollection();
                operacaoCotistaColl.Query.Where(operacaoCotistaColl.Query.IdCarteira.Equal(idCarteira) &
                                                operacaoCotistaColl.Query.IdCotista.Equal(idCotista) &
                                                operacaoCotistaColl.Query.DataConversao.GreaterThanOrEqual(cliente.DataDia.Value) &
                                                operacaoCotistaColl.Query.TipoOperacao.Equal(tipoOperacao));
                operacaoCotistaColl.Query.OrderBy(operacaoCotistaColl.Query.DataConversao.Descending);

                if (operacaoCotistaColl.Query.Load())
                {
                    e.Result = "Já existe operação de resgate total agendada; ID -  " + operacaoCotistaColl[0].IdOperacao.Value;
                    return;
                }
            }

            if (tipoOperacao == (byte)TipoOperacaoCotista.ComeCotas ||
                tipoOperacao == (byte)TipoOperacaoCotista.ResgateBruto ||
                tipoOperacao == (byte)TipoOperacaoCotista.ResgateCotas ||
                tipoOperacao == (byte)TipoOperacaoCotista.ResgateCotasEspecial ||
                tipoOperacao == (byte)TipoOperacaoCotista.ResgateLiquido ||
                tipoOperacao == (byte)TipoOperacaoCotista.ResgateTotal)
            {
                PosicaoCotistaCollection posicaoCotistaCollection = new PosicaoCotistaCollection();
                posicaoCotistaCollection.Query.Where(posicaoCotistaCollection.Query.IdCarteira.Equal(idCarteira),
                                                     posicaoCotistaCollection.Query.IdCotista.Equal(idCotista));
                if (!posicaoCotistaCollection.Query.Load())
                {
                    e.Result = "O cotista não possui saldo de aplicação neste fundo para resgate.";
                    return;
                }
            }

            if (tipoOperacao == (byte)TipoOperacaoCotista.ResgateBruto ||
                tipoOperacao == (byte)TipoOperacaoCotista.ResgateLiquido ||
                tipoOperacao == (byte)TipoOperacaoCotista.ResgateCotas ||
                tipoOperacao == (byte)TipoOperacaoCotista.ResgateCotasEspecial ||
                tipoOperacao == (byte)TipoOperacaoCotista.ResgateTotal)
            {
                PosicaoCotista posicaoCotistaCheck = new PosicaoCotista();
                posicaoCotistaCheck.Query.Select(posicaoCotistaCheck.Query.Quantidade.Sum(),
                                                 posicaoCotistaCheck.Query.QuantidadeBloqueada.Sum(),
                                                 posicaoCotistaCheck.Query.CotaDia.Avg());
                posicaoCotistaCheck.Query.Where(posicaoCotistaCheck.Query.IdCarteira.Equal(idCarteira),
                                                posicaoCotistaCheck.Query.IdCotista.Equal(idCotista),
                                                posicaoCotistaCheck.Query.Quantidade.GreaterThan(0));

                if (hiddenNotaResgatada.Value != "")
                {
                    posicaoCotistaCheck.Query.Where(posicaoCotistaCheck.Query.IdPosicao.Equal(Convert.ToInt32(hiddenNotaResgatada.Value)));
                }

                posicaoCotistaCheck.Query.Load();

                if (!posicaoCotistaCheck.Quantidade.HasValue || posicaoCotistaCheck.Quantidade.Value == 0)
                {
                    e.Result = "O cotista não possui saldo de aplicação neste fundo para resgate.";
                    return;
                }
                else
                {
                    decimal quantidadeCheck = posicaoCotistaCheck.Quantidade.Value;
                    decimal quantidadeBloqueada = posicaoCotistaCheck.QuantidadeBloqueada.Value;
                    decimal valorCota = posicaoCotistaCheck.CotaDia.Value;
                    decimal valorCheck = Math.Round(quantidadeCheck * valorCota, 2) - Math.Round(quantidadeBloqueada * valorCota, 2);

                    if (tipoOperacao == (byte)TipoOperacaoCotista.ResgateBruto || tipoOperacao == (byte)TipoOperacaoCotista.ResgateLiquido)
                    {
                        if (Convert.ToDecimal(textValor.Text) > valorCheck)
                        {
                            e.Result = "O cotista possui saldo financeiro disponível insuficiente neste fundo para resgate.";
                            return;
                        }
                    }
                    else if (tipoOperacao == (byte)TipoOperacaoCotista.ResgateCotas || tipoOperacao == (byte)TipoOperacaoCotista.ResgateCotasEspecial)
                    {
                        if (Convert.ToDecimal(textValor.Text) > quantidadeCheck)
                        {
                            e.Result = "O cotista possui saldo em cotas disponível insuficiente neste fundo para resgate.";
                            return;
                        }
                    }
                    else if (tipoOperacao == (byte)TipoOperacaoCotista.ResgateTotal && quantidadeBloqueada != 0)
                    {
                        e.Result = "O cotista possui saldo bloqueado, para resgates totais é necessário estar com a posição toda disponível.";
                        return;
                    }

                }
            }

            #region Valida Tipo Operação
            if (cliente.IdTipo.Value != (int)TipoClienteFixo.Fundo && lstApenasParaFundo.Contains(Convert.ToByte(dropTipoOperacao.SelectedItem.Value)))
            {
                e.Result = "Tipo de Operação permitida apenas para Cliente do Tipo 'Fundo de Investimento'!";
                return;
            }
            #endregion

            DateTime dataCliente = cliente.DataDia.Value;

            int tipoFundo = Carteira.RetornaCondominioCarteira(idCarteira, dataOperacao);
            if (tipoFundo == (int)TipoFundo.Fechado &&
                tipoOperacao != (byte)TipoOperacaoCotista.Aplicacao)
            {
                e.Result = "Fundo fechado, permitido somente movimento de Aplicação!";
                return;
            }

            if (dataOperacao == dataCliente)
            {
                #region Avalia valores e saldo mínimo
                if (tipoOperacao == (byte)TipoOperacaoCotista.Aplicacao)
                {
                    if (carteira.ValorMinimoInicial.Value != 0 || carteira.ValorMinimoAplicacao.Value != 0)
                    {
                        decimal quantidadeCotas = 0;
                        PosicaoCotista posicaoCotista = new PosicaoCotista();
                        posicaoCotista.Query.Select(posicaoCotista.Query.Quantidade.Sum());
                        posicaoCotista.Query.Where(posicaoCotista.Query.IdCarteira.Equal(idCarteira),
                                                   posicaoCotista.Query.IdCotista.Equal(idCotista));
                        posicaoCotista.Query.Load();

                        if (posicaoCotista.Quantidade.HasValue)
                        {
                            quantidadeCotas = posicaoCotista.Quantidade.Value;
                        }

                        decimal valor = Convert.ToDecimal(textValor.Text);
                        if (quantidadeCotas == 0 && valor < carteira.ValorMinimoInicial.Value)
                        {
                            e.Result = "Operação com valor inferior ao valor mínimo inicial (" + carteira.ValorMinimoInicial.Value.ToString() + ") . Operação não pode ser realizada";
                            return;
                        }
                        else if (valor < carteira.ValorMinimoAplicacao.Value)
                        {
                            e.Result = "Operação com valor inferior ao valor de aplicação exigido (" + carteira.ValorMinimoAplicacao.Value.ToString() + ") . Operação não pode ser realizada";
                            return;
                        }

                    }

                    if (carteira.ValorMaximoAplicacao.GetValueOrDefault(0) != 0)
                    {
                        if (carteira.ValorMaximoAplicacao.Value < Convert.ToDecimal(textValor.Text))
                        {
                            e.Result = "Operação com valor superior ao valor de máximo de aplicação (" + carteira.ValorMaximoAplicacao.Value.ToString() + ") . Operação não pode ser realizada";
                            return;
                        }
                    }
                }
                else if (tipoOperacao == (byte)TipoOperacaoCotista.ResgateBruto ||
                         tipoOperacao == (byte)TipoOperacaoCotista.ResgateLiquido)
                {
                    if (carteira.ValorMinimoResgate.Value != 0 || carteira.ValorMinimoSaldo.Value != 0)
                    {
                        decimal saldoLiquido = 0;
                        PosicaoCotista posicaoCotista = new PosicaoCotista();
                        posicaoCotista.Query.Select(posicaoCotista.Query.ValorLiquido.Sum());
                        posicaoCotista.Query.Where(posicaoCotista.Query.IdCarteira.Equal(idCarteira),
                                                   posicaoCotista.Query.IdCotista.Equal(idCotista));
                        posicaoCotista.Query.Load();

                        if (posicaoCotista.ValorLiquido.HasValue)
                        {
                            saldoLiquido = posicaoCotista.ValorLiquido.Value;
                        }

                        decimal valor = Convert.ToDecimal(textValor.Text);
                        decimal saldoProjetado = saldoLiquido - valor;
                        if (saldoProjetado < carteira.ValorMinimoSaldo.Value)
                        {
                            e.Result = "Saldo projetado com valor inferior ao valor mínimo requerido (" + carteira.ValorMinimoSaldo.Value.ToString() + ") . Operação não pode ser realizada";
                            return;
                        }
                        else if (valor < carteira.ValorMinimoResgate.Value)
                        {
                            e.Result = "Operação com valor inferior ao valor de resgate exigido (" + carteira.ValorMinimoResgate.Value.ToString() + ") . Operação não pode ser realizada";
                            return;
                        }
                    }
                }
                #endregion
            }

            SuitabilityParametrosWorkflow suitabilityParametrosWorkflow = new SuitabilityParametrosWorkflow();
            suitabilityParametrosWorkflow.LoadByPrimaryKey(1);

            if (suitabilityParametrosWorkflow.AtivaWorkFlow.Equals("S"))
            {
                //Verifica Suitability
                e.Result = verificaSuitability(idCotista, idCarteira, tipoOperacao, dataOperacao);

                if (e.Result == "")
                {
                    ControllerPerfilCotista controllerPerfilCotista = new ControllerPerfilCotista();

                    //ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
                    decimal valorOperacaoParam = Convert.ToDecimal(textValor.Text);

                    SuitabilityPerfilInvestidor suitabilityPerfilCotistaOut;
                    SuitabilityPerfilInvestidor suitabilityPerfilPosicaoOut;
                    bool desenquadrado;

                    e.Result = controllerPerfilCotista.verificaEnquadramentoSuitability(idCotista, idCarteira, tipoOperacao, valorOperacaoParam, out suitabilityPerfilPosicaoOut, out suitabilityPerfilCotistaOut,
                        out desenquadrado);

                    suitabilityPerfilCotista = suitabilityPerfilCotistaOut;
                    suitabilityPerfilPosicao = suitabilityPerfilPosicaoOut;

                }
            }

            #region Valida operação Off Shore
            if (cliente.IdTipo.Equals((int)TipoClienteFixo.OffShore_PF) || cliente.IdTipo.Equals((int)TipoClienteFixo.OffShore_PJ))
            {
                if (tipoOperacao == (byte)TipoOperacaoCotista.AplicacaoAcoesEspecial ||
                   tipoOperacao == (byte)TipoOperacaoCotista.AplicacaoCotasEspecial ||
                   tipoOperacao == (byte)TipoOperacaoCotista.ResgateCotasEspecial ||
                   tipoOperacao == (byte)TipoOperacaoCotista.ResgateLiquido ||
                   tipoOperacao == (byte)TipoOperacaoCotista.ComeCotas ||
                   tipoOperacao == (byte)TipoOperacaoCotista.Retirada ||
                   tipoOperacao == (byte)TipoOperacaoCotista.Deposito)
                {
                    e.Result = "Operação não permitida para carteiras off shore.";
                }

                decimal valor = Convert.ToDecimal(textValor.Text);

                ClassesOffShore classesOffShore = new ClassesOffShore();
                classesOffShore.LoadByPrimaryKey(idCarteira);

                if (tipoOperacao == (byte)TipoOperacaoCotista.Aplicacao)
                {

                    if (valor < classesOffShore.VlMinimoAplicacao.Value)
                        e.Result = "Valor mínimo para aplicação é " + classesOffShore.VlMinimoAplicacao.Value.ToString("0.00") + ". Operação não pode ser realizada.";
                }

                if (tipoOperacao == (byte)TipoOperacaoCotista.ResgateBruto)
                {
                    if (valor < classesOffShore.VlMinimoResgate.Value)
                        e.Result = "Valor mínimo para resgate é " + classesOffShore.VlMinimoResgate.Value.ToString("0.00");

                    PosicaoCotista posicaoCotista = new PosicaoCotista();
                    decimal valorPosicao = 0;
                    valorPosicao = posicaoCotista.RetornaSumValorLiquido(idCarteira, idCotista);
                    if ((valorPosicao - valor) < classesOffShore.VlMinimoPermanencia.Value)
                        e.Result = "Saldo projetado com valor inferior ao valor mínimo requerido (" + classesOffShore.VlMinimoPermanencia.Value.ToString("0.00") + ") . Operação não pode ser realizada";
                }

                if (tipoOperacao == (byte)TipoOperacaoCotista.ResgateCotas ||
                    tipoOperacao == (byte)TipoOperacaoCotista.ResgateCotasEspecial)
                {
                    PosicaoCotista posicaoCotistaCheck = new PosicaoCotista();
                    posicaoCotistaCheck.Query.Select(posicaoCotistaCheck.Query.Quantidade.Sum(),
                                                     posicaoCotistaCheck.Query.QuantidadeBloqueada.Sum(),
                                                     posicaoCotistaCheck.Query.CotaDia.Avg());
                    posicaoCotistaCheck.Query.Where(posicaoCotistaCheck.Query.IdCarteira.Equal(idCarteira),
                                                    posicaoCotistaCheck.Query.IdCotista.Equal(idCotista),
                                                    posicaoCotistaCheck.Query.Quantidade.GreaterThan(0));

                    if (hiddenNotaResgatada.Value != "")
                    {
                        posicaoCotistaCheck.Query.Where(posicaoCotistaCheck.Query.IdPosicao.Equal(Convert.ToInt32(hiddenNotaResgatada.Value)));
                    }
                    else
                    {
                        e.Result = "Favor especificar qual a posição de deseja resgatar.";
                    }

                    posicaoCotistaCheck.Query.Load();

                    decimal quantidadeCheck = posicaoCotistaCheck.Quantidade.Value;
                    decimal quantidadeBloqueada = posicaoCotistaCheck.QuantidadeBloqueada.Value;
                    decimal valorCota = posicaoCotistaCheck.CotaDia.Value;
                    decimal valorCheck = Math.Round(quantidadeCheck * valorCota, 2) - Math.Round(quantidadeBloqueada * valorCota, 2);

                    if ((valorCheck - (valor * valorCota)) < classesOffShore.VlMinimoPermanencia.Value)
                        e.Result = "Saldo projetado com valor inferior ao valor mínimo requerido (" + classesOffShore.VlMinimoPermanencia.Value.ToString("0.00") + ") . Operação não pode ser realizada";
                }

                if (tipoOperacao == (byte)TipoOperacaoCotista.ResgateCotas ||
                    tipoOperacao == (byte)TipoOperacaoCotista.ResgateBruto ||
                    tipoOperacao == (byte)TipoOperacaoCotista.ResgateLiquido ||
                    tipoOperacao == (byte)TipoOperacaoCotista.ResgateTotal)
                {

                    PosicaoCotista posicaoCotistaCheck = new PosicaoCotista();
                    posicaoCotistaCheck.Query.Select(posicaoCotistaCheck.Query.DataConversao);
                    posicaoCotistaCheck.Query.Where(posicaoCotistaCheck.Query.IdCarteira.Equal(idCarteira),
                                                    posicaoCotistaCheck.Query.IdCotista.Equal(idCotista),
                                                    posicaoCotistaCheck.Query.Quantidade.GreaterThan(0));

                    if (hiddenNotaResgatada.Value != "")
                    {
                        posicaoCotistaCheck.Query.Where(posicaoCotistaCheck.Query.IdPosicao.Equal(Convert.ToInt32(hiddenNotaResgatada.Value)));
                    }
                    else
                    {
                        e.Result = "Favor especificar qual a posição de deseja resgatar.";
                        return;
                    }

                    posicaoCotistaCheck.Query.Load();

                    TimeSpan ts = dataOperacao - posicaoCotistaCheck.DataConversao.Value;

                    if (classesOffShore.LockUp.Value > 0 && ts.Days < classesOffShore.LockUp.Value)
                    {
                        if (classesOffShore.HardSoft.Equals(HardSoft.Hard))
                        {
                            e.Result = "Resgate não é possível. Operação ainda está no prazo de carência e a Classe está marcada como Hard.";
                            return;
                        }

                    }
                }
            }
            #endregion

            #region Valida operação FIE (Fundo de investimento em previdência)
            carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);
            if (carteira.Fie.Equals("S") && (tipoOperacao != (byte)TipoOperacaoCotista.ResgateBruto &&
                                         tipoOperacao != (byte)TipoOperacaoCotista.ResgateCotas &&
                                         tipoOperacao != (byte)TipoOperacaoCotista.ResgateCotasEspecial &&
                                         tipoOperacao != (byte)TipoOperacaoCotista.ResgateLiquido &&
                                         tipoOperacao != (byte)TipoOperacaoCotista.ResgateTotal))
            {
                if (dropFieModalidade.SelectedIndex == -1)
                    e.Result = "Para fundos do tipo FIE, favor informar Fie/Modalidade.";

                if (dropFieTabelaIr.SelectedIndex == -1)
                    e.Result = "Para fundos do tipo FIE, favor informar Fie/Tabela IR.";
            }

            if (!string.IsNullOrEmpty(e.Result))
                return;
            #endregion

            #region ValidacaoDataConversao
            //dataConversao
            #endregion

            #region Resgates Agendados
            List<byte> lstTipoResgate = new List<byte>();
            lstTipoResgate.Add((byte)TipoOperacaoCotista.ResgateLiquido);
            lstTipoResgate.Add((byte)TipoOperacaoCotista.ResgateBruto);
            lstTipoResgate.Add((byte)TipoOperacaoCotista.ResgateCotas);
            lstTipoResgate.Add((byte)TipoOperacaoCotista.ResgateCotasEspecial);

            if (lstTipoResgate.Contains(tipoOperacao))
            {
                OperacaoCotistaCollection operacaoCotistaColl = new OperacaoCotistaCollection();
                operacaoCotistaColl.Query.Where(operacaoCotistaColl.Query.IdCarteira.Equal(idCarteira) &
                                                operacaoCotistaColl.Query.IdCotista.Equal(idCotista) &
                                                operacaoCotistaColl.Query.DataConversao.GreaterThan(cliente.DataDia.Value) &
                                                operacaoCotistaColl.Query.TipoResgate.IsNotNull());
                operacaoCotistaColl.Query.OrderBy(operacaoCotistaColl.Query.DataConversao.Ascending);

                if (operacaoCotistaColl.Query.Load())
                {
                    StringBuilder strMensagem = new StringBuilder();
                    strMensagem.Append("CONFIRM|Os seguintes resgates estão agendados: \n");
                    foreach (OperacaoCotista operacaoCotista in operacaoCotistaColl)
                    {
                        bool porValorLiquido = operacaoCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.ResgateLiquido;
                        bool porValorBruto = operacaoCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.ResgateBruto;
                        bool porQtde = operacaoCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.ResgateCotas || operacaoCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.ResgateCotasEspecial;

                        strMensagem.Append("ID.Op: " + operacaoCotista.IdOperacao.Value + "; ");
                        if (porQtde)
                            strMensagem.Append("Qtde.Cotas: " + operacaoCotista.Quantidade.GetValueOrDefault(0).ToString());
                        else if (porValorLiquido)
                            strMensagem.Append("Vl.Líquido: " + operacaoCotista.ValorLiquido.GetValueOrDefault(0).ToString());
                        else if (porValorBruto)
                            strMensagem.Append("Vl.Bruto: " + operacaoCotista.ValorBruto.GetValueOrDefault(0).ToString());

                        strMensagem.Append("; Dt.Conv: " + String.Format("{0:dd/MM/yyyy}", operacaoCotista.DataConversao.Value));

                        strMensagem.Append("\n");
                    }
                    strMensagem.Append("\nDeseja criar a operação?");
                    e.Result = strMensagem.ToString();
                    return;
                }
            }
            #endregion

            if (carteira.CarteiraECotistaMesmaPessoa(carteira, cotista))
            {
                e.Result = "Id.Carteira - " + carteira.IdCarteira.Value + " e Id.Cotista - " + cotista.IdCotista.Value + " são a mesma pessoa, operação abortada";
                return;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCotista = Convert.ToInt32(e.Parameter);
            Cotista cotista = new Cotista();
            cotista.LoadByPrimaryKey(idCotista);

            if (cotista.str.Apelido != "")
            {
                if (cotista.IsAtivo)
                {
                    PermissaoCotista permissaoCotista = new PermissaoCotista();
                    if (permissaoCotista.RetornaAcessoCotista(idCotista, HttpContext.Current.User.Identity.Name))
                    {
                        nome = cotista.str.Apelido;
                    }
                    else
                    {
                        nome = "no_access";
                    }
                }
                else
                {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        ASPxComboBox dropTipoOperacao = gridCadastro.FindEditFormTemplateControl("dropTipoOperacao") as ASPxComboBox;
        ASPxComboBox dropLocalNegociacao = gridCadastro.FindEditFormTemplateControl("dropLocalNegociacao") as ASPxComboBox;
        Carteira carteira = new Carteira();

        string nome = "";
        string resultado = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCarteira = Convert.ToInt32(e.Parameter);
            if (carteira.LoadByPrimaryKey(idCarteira))
            {
                Cliente cliente = new Cliente();

                cliente.LoadByPrimaryKey(idCarteira);

                if (cliente.Status.Value != (byte)StatusCliente.Divulgado)
                {
                    if (carteira.IsAtivo)
                    {
                        int? idLocalNegociacao = null;
                        string strLocalNegociacao = string.Empty;

                        #region Busca local de Feriado
                        int? idLocalFeriado = null;
                        if (dropLocalNegociacao != null && dropLocalNegociacao.SelectedIndex != -1)
                            idLocalNegociacao = Convert.ToInt32(dropLocalNegociacao.SelectedItem.Value);
                        else
                            idLocalNegociacao = carteira.UpToCliente.IdLocalNegociacao;

                        if (idLocalNegociacao.HasValue)
                        {
                            strLocalNegociacao = idLocalNegociacao.Value.ToString();
                            LocalNegociacao localNegociacao = new LocalNegociacao();
                            localNegociacao.LoadByPrimaryKey(idLocalNegociacao.Value);

                            idLocalFeriado = localNegociacao.IdLocalFeriado.Value;
                        }

                        #endregion

                        DateTime dataConversao = new DateTime();
                        DateTime dataLiquidacao = new DateTime();
                        //O permissionamento da carteira é dado direto no cliente vinculado à carteira
                        PermissaoCliente permissaoCliente = new PermissaoCliente();
                        if (permissaoCliente.RetornaAcessoCliente(idCarteira, HttpContext.Current.User.Identity.Name))
                        {
                            nome = carteira.str.Apelido;

                            DateTime dataOperacao = new DateTime();
                            ASPxDateEdit textDataOperacao = gridCadastro.FindEditFormTemplateControl("textDataOperacao") as ASPxDateEdit;
                            if (textDataOperacao != null && textDataOperacao.Text != "")
                            {
                                dataOperacao = Convert.ToDateTime(textDataOperacao.Text);
                            }
                            else
                            {
                                dataOperacao = cliente.DataDia.Value;
                            }

                            if (dropTipoOperacao != null)
                            {
                                if (dropTipoOperacao.SelectedIndex >= 0)
                                {
                                    #region Parâmetros Avançados
                                    byte tipoOperacaoDDL = Convert.ToByte(dropTipoOperacao.SelectedItem.Value);

                                    #region populaListas
                                    List<byte> lstOperacoesResgate = new List<byte>();
                                    lstOperacoesResgate.Add((byte)TipoOperacaoFundo.ResgateBruto);
                                    lstOperacoesResgate.Add((byte)TipoOperacaoFundo.ResgateCotas);
                                    lstOperacoesResgate.Add((byte)TipoOperacaoFundo.ResgateCotasEspecial);
                                    lstOperacoesResgate.Add((byte)TipoOperacaoFundo.ResgateLiquido);
                                    lstOperacoesResgate.Add((byte)TipoOperacaoFundo.ResgateTotal);
                                    #endregion

                                    int tipoOperacao = lstOperacoesResgate.Contains(tipoOperacaoDDL) ? (int)TipoParametroLiqOperacao.Resgate : (int)TipoParametroLiqOperacao.Aplicacao;

                                    ParametroLiqAvancadoAnaliticoCollection collLiquidacao = new ParametroLiqAvancadoAnaliticoCollection();
                                    ParametroLiqAvancadoAnaliticoCollection collConvesao = new ParametroLiqAvancadoAnaliticoCollection();
                                    ParametroLiqAvancadoAnaliticoCollection collOperacao = new ParametroLiqAvancadoAnaliticoCollection();
                                    DateTime dataVigente = collOperacao.RetornaUltDataVigente(dataOperacao, idCarteira, tipoOperacao);
                                    if (dataVigente != new DateTime())
                                    {
                                        collOperacao = collLiquidacao.RetornaParametrosNaDataVigente(dataVigente, idCarteira, tipoOperacao, (int)TipoParametroLiquidacao.DataOperacao);
                                        collLiquidacao = collLiquidacao.RetornaParametrosNaDataVigente(dataVigente, idCarteira, tipoOperacao, (int)TipoParametroLiquidacao.DataLiquidacao);
                                        collConvesao = collConvesao.RetornaParametrosNaDataVigente(dataVigente, idCarteira, tipoOperacao, (int)TipoParametroLiquidacao.DataConversao);
                                    }

                                    if (collLiquidacao.Count > 0 && collConvesao.Count > 0)
                                    {
                                        int parametroDtOperacao = (int)TipoParametroLiquidacao.DataOperacao;
                                        int parametroDtConversao = (int)TipoParametroLiquidacao.DataConversao;
                                        int parametroDtLiquidacao = (int)TipoParametroLiquidacao.DataLiquidacao;

                                        if (tipoOperacao == (int)TipoParametroLiqOperacao.Aplicacao)
                                        {
                                            dataOperacao = collOperacao.RetornaDataCalculada(dataOperacao, dataOperacao, collOperacao, parametroDtOperacao, 0, idLocalFeriado);
                                            dataLiquidacao = collLiquidacao.RetornaDataCalculada(dataOperacao, dataOperacao, collLiquidacao, parametroDtLiquidacao, 0, idLocalFeriado);
                                            dataConversao = collConvesao.RetornaDataCalculada(dataOperacao, dataLiquidacao, collConvesao, parametroDtConversao, 0, idLocalFeriado);
                                        }
                                        else if (tipoOperacao == (int)TipoParametroLiqOperacao.Resgate)
                                        {
                                            dataOperacao = collOperacao.RetornaDataCalculada(dataOperacao, dataOperacao, collOperacao, parametroDtOperacao, 0, idLocalFeriado);
                                            dataConversao = collConvesao.RetornaDataCalculada(dataOperacao, dataOperacao, collConvesao, parametroDtConversao, 0, idLocalFeriado);
                                            dataLiquidacao = collLiquidacao.RetornaDataCalculada(dataOperacao, dataConversao, collLiquidacao, parametroDtLiquidacao, 0, idLocalFeriado);
                                        }
                                    }
                                    #endregion
                                    else
                                    {
                                        int contagemResgate = carteira.ContagemDiasConversaoResgate.Value;

                                        //Trata campos com dataConversao e dataLiquidacao
                                        int diasConversao = 0;
                                        int diasLiquidacao = 0;
                                        byte tipoOperacaoCombo = Convert.ToByte(dropTipoOperacao.SelectedItem.Value);
                                        if (tipoOperacaoCombo == (byte)TipoOperacaoCotista.Aplicacao ||
                                            tipoOperacaoCombo == (byte)TipoOperacaoCotista.AplicacaoAcoesEspecial)
                                        {
                                            if (carteira.DataInicioCota.Value == dataOperacao) //Para não dar problema no inicio de um fundo por ex
                                            {
                                                diasConversao = 0;
                                            }
                                            else
                                            {
                                                diasConversao = carteira.DiasCotizacaoAplicacao.Value;
                                            }

                                            diasLiquidacao = carteira.DiasLiquidacaoAplicacao.Value;
                                        }
                                        else if (tipoOperacaoCombo == (byte)TipoOperacaoCotista.ResgateBruto ||
                                                 tipoOperacaoCombo == (byte)TipoOperacaoCotista.ResgateCotas ||
                                                 tipoOperacaoCombo == (byte)TipoOperacaoCotista.ResgateLiquido ||
                                                 tipoOperacaoCombo == (byte)TipoOperacaoCotista.ResgateTotal)
                                        {
                                            diasConversao = carteira.DiasCotizacaoResgate.Value;
                                            diasLiquidacao = carteira.DiasLiquidacaoResgate.Value;
                                        }
                                        if (tipoOperacaoCombo == (byte)TipoOperacaoCotista.AplicacaoCotasEspecial ||
                                            tipoOperacaoCombo == (byte)TipoOperacaoCotista.ResgateCotasEspecial ||
                                            tipoOperacaoCombo == (byte)TipoOperacaoCotista.ComeCotas)
                                        {
                                            diasConversao = 0;
                                            diasLiquidacao = 0;
                                        }


                                        if (contagemResgate == (byte)ContagemDiasLiquidacaoResgate.DiasUteis ||
                                       (tipoOperacaoCombo == (byte)TipoOperacaoFundo.Aplicacao ||
                                        tipoOperacaoCombo == (byte)TipoOperacaoFundo.AplicacaoAcoesEspecial))
                                        {
                                            dataConversao = Calendario.AdicionaDiaUtil((DateTime)dataOperacao, diasConversao);
                                            dataLiquidacao = Calendario.AdicionaDiaUtil((DateTime)dataOperacao, diasLiquidacao);
                                        }
                                        else
                                        {
                                            //Conta por Dias Corridos
                                            dataConversao = dataOperacao.AddDays(diasConversao);

                                            if (!Calendario.IsDiaUtil(dataConversao))
                                            {
                                                dataConversao = Calendario.AdicionaDiaUtil(dataConversao, 1);
                                            }

                                            //Conta por Dias Úteis em cima da data de conversão
                                            dataLiquidacao = Calendario.AdicionaDiaUtil(dataConversao, diasLiquidacao);
                                        }

                                        //Se o cliente for off shore, utiliza a data da série para gerar data de conversão
                                        if (cliente.IdTipo.Value == (int)TipoClienteFixo.OffShore_PJ || cliente.IdTipo.Value == (int)TipoClienteFixo.OffShore_PF)
                                        {
                                            ClassesOffShore classe = new ClassesOffShore();
                                            classe.LoadByPrimaryKey(idCarteira);

                                            if (tipoOperacaoCombo == (byte)TipoOperacaoFundo.Aplicacao)
                                            {
                                                dataConversao = dataOperacao.AddDays(classe.QtdDiasConvAplicacao.Value);

                                                SeriesOffShore serie = this.verificaSerieOffShore(idCarteira, dataConversao, true);

                                                dataConversao = serie.DataFimAplicacao.Value;

                                                if (!Calendario.IsDiaUtil(dataConversao))
                                                    dataConversao = Calendario.SubtraiDiaUtil(dataConversao, 1);

                                                dataLiquidacao = Calendario.SubtraiDiaUtil(dataConversao, classe.QtdDiasLiqFinAplic.Value);
                                            }
                                            if (tipoOperacaoCombo == (byte)TipoOperacaoCotista.ResgateBruto || tipoOperacaoCombo == (byte)TipoOperacaoCotista.ResgateTotal)
                                            {
                                                dataConversao = dataOperacao.AddDays(classe.QtdDiasConvResgate.Value);

                                                SeriesOffShore serie = this.verificaSerieOffShore(idCarteira, dataConversao, true);

                                                dataConversao = serie.DataFimAplicacao.Value;

                                                if (!Calendario.IsDiaUtil(dataConversao))
                                                    dataConversao = Calendario.SubtraiDiaUtil(dataConversao, 1);

                                                dataLiquidacao = Calendario.AdicionaDiaUtil(dataConversao, classe.QtdDiasLiqFinAplic.Value);
                                            }
                                        }

                                    }
                                    resultado = nome + "|" +
                                                dataOperacao.ToString().Substring(0, 10) + "|" +
                                                Thread.CurrentThread.CurrentCulture.Name + "|" +
                                                    dataConversao.ToString().Substring(0, 10) + "|" +
                                                dataLiquidacao.ToString().Substring(0, 10) + "|" +
                                                strLocalNegociacao + "|" +
                                                (cliente.IdTipo.Value == (int)TipoClienteFixo.OffShore_PF || cliente.IdTipo.Value == (int)TipoClienteFixo.OffShore_PJ) + "|" +
                                                carteira.Fie;
                                }
                                else
                                {
                                    resultado = nome + "|" +
                                               dataOperacao.ToString().Substring(0, 10) + "|" +
                                               Thread.CurrentThread.CurrentCulture.Name + "|" +
                                               "" + "|" +
                                               "" + "|" +
                                               strLocalNegociacao + "|" +
                                               (cliente.IdTipo.Value == (int)TipoClienteFixo.OffShore_PF || cliente.IdTipo.Value == (int)TipoClienteFixo.OffShore_PJ) + "|" +
                                               carteira.Fie;
                                }
                            }
                            else
                            {
                                resultado = nome;
                            }
                        }
                        else
                        {
                            resultado = "no_access";
                        }
                    }
                    else
                    {
                        resultado = "no_active";
                    }
                }
                else
                {
                    resultado = cliente.str.Apelido + "|status_closed";
                }
            }
        }

        e.Result = resultado;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridPosicaoCotista_CustomDataCallback(object sender, ASPxGridViewCustomDataCallbackEventArgs e)
    {
        string dataConversao = Convert.ToString(gridPosicaoCotista.GetRowValues(Convert.ToInt32(e.Parameters), "DataConversao"));
        string mensagemNotaResgatada = "Nota resgatada" +
                    "\n-----------------------------" +
                    "\nData: " + dataConversao.Substring(0, 10) +
                    "\nQtde: " + gridPosicaoCotista.GetRowValues(Convert.ToInt32(e.Parameters), "Quantidade");

        e.Result = gridPosicaoCotista.GetRowValues(Convert.ToInt32(e.Parameters), "IdPosicao") + "|" + mensagemNotaResgatada;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridPosicaoCotista_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        UtilitarioGrid.SetaCoresGrid(e);
    }

    /// <summary>
    ///
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridPosicaoCotista_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        gridPosicaoCotista.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        List<byte> lstTipoOperacaoSemLiquidacao = new List<byte>();
        lstTipoOperacaoSemLiquidacao.Add((byte)TipoOperacaoCotista.Retirada);
        lstTipoOperacaoSemLiquidacao.Add((byte)TipoOperacaoCotista.Deposito);

        int idOperacao = (int)e.Keys[0];
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxSpinEdit btnEditCodigoCotista = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCotista") as ASPxSpinEdit;
        ASPxDateEdit textDataOperacao = gridCadastro.FindEditFormTemplateControl("textDataOperacao") as ASPxDateEdit;
        ASPxDateEdit textDataRegistro = gridCadastro.FindEditFormTemplateControl("textDataRegistro") as ASPxDateEdit;
        //ASPxCheckBox checkDepositoRetirada = gridCadastro.FindEditFormTemplateControl("checkDepositoRetirada") as ASPxCheckBox;
        ASPxDateEdit textDataConversao = gridCadastro.FindEditFormTemplateControl("textDataConversao") as ASPxDateEdit;
        ASPxDateEdit textDataLiquidacao = gridCadastro.FindEditFormTemplateControl("textDataLiquidacao") as ASPxDateEdit;
        ASPxComboBox dropTipoOperacao = gridCadastro.FindEditFormTemplateControl("dropTipoOperacao") as ASPxComboBox;
        ASPxComboBox dropTipoResgate = gridCadastro.FindEditFormTemplateControl("dropTipoResgate") as ASPxComboBox;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
        ASPxComboBox dropFormaLiquidacao = gridCadastro.FindEditFormTemplateControl("dropFormaLiquidacao") as ASPxComboBox;
        TextBox textConta = gridCadastro.FindEditFormTemplateControl("textConta") as TextBox;
        TextBox textObservacao = gridCadastro.FindEditFormTemplateControl("textObservacao") as TextBox;
        ASPxSpinEdit textCotaInformada = gridCadastro.FindEditFormTemplateControl("textCotaInformada") as ASPxSpinEdit;
        ASPxComboBox dropLocalNegociacao = gridCadastro.FindEditFormTemplateControl("dropLocalNegociacao") as ASPxComboBox;

        ASPxSpinEdit textValorDespesas = gridCadastro.FindEditFormTemplateControl("textValorDespesas") as ASPxSpinEdit;
        ASPxSpinEdit textValorTaxas = gridCadastro.FindEditFormTemplateControl("textValorTaxas") as ASPxSpinEdit;
        ASPxSpinEdit textValorTributos = gridCadastro.FindEditFormTemplateControl("textValorTributos") as ASPxSpinEdit;
        ASPxSpinEdit textValorPenalidadeLockUp = gridCadastro.FindEditFormTemplateControl("textValorPenalidadeLockUp") as ASPxSpinEdit;
        ASPxSpinEdit textValorHoldBack = gridCadastro.FindEditFormTemplateControl("textValorHoldBack") as ASPxSpinEdit;

        ASPxComboBox dropContaCorrente = gridCadastro.FindEditFormTemplateControl("dropContaCorrente") as ASPxComboBox;

        ASPxComboBox dropFieModalidade = gridCadastro.FindEditFormTemplateControl("dropFieModalidade") as ASPxComboBox;
        ASPxComboBox dropFieTabelaIr = gridCadastro.FindEditFormTemplateControl("dropFieTabelaIr") as ASPxComboBox;
        ASPxComboBox dropTrader = gridCadastro.FindEditFormTemplateControl("dropTrader") as ASPxComboBox;
        ASPxCheckBox chkCotaInformada = gridCadastro.FindEditFormTemplateControl("chkCotaInformada") as ASPxCheckBox;

        OperacaoCotista operacaoCotista = new OperacaoCotista();
        if (operacaoCotista.LoadByPrimaryKey(idOperacao))
        {
            int idCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);

            operacaoCotista.IdCarteira = idCarteira;
            operacaoCotista.IdCotista = Convert.ToInt32(btnEditCodigoCotista.Text);
            operacaoCotista.DataOperacao = Convert.ToDateTime(textDataOperacao.Text);
            operacaoCotista.DataRegistro = Convert.ToDateTime(textDataRegistro.Text);
            //operacaoCotista.DepositoRetirada = checkDepositoRetirada.Checked;
            operacaoCotista.DataConversao = Convert.ToDateTime(textDataConversao.Text);
            operacaoCotista.DataLiquidacao = Convert.ToDateTime(textDataLiquidacao.Text);
            operacaoCotista.TipoOperacao = Convert.ToByte(dropTipoOperacao.SelectedItem.Value);
            operacaoCotista.DataAgendamento = Convert.ToDateTime(textDataOperacao.Text); //TODO AJUSTAR DEPOIS
            operacaoCotista.IdLocalNegociacao = Convert.ToInt16(dropLocalNegociacao.SelectedItem.Value);

            if (dropTipoResgate.SelectedIndex != -1)
            {
                operacaoCotista.TipoResgate = Convert.ToByte(dropTipoResgate.SelectedItem.Value);
            }

            if (dropTrader.SelectedIndex > -1)
                operacaoCotista.IdTrader = Convert.ToInt32(dropTrader.SelectedItem.Value);
            else
                operacaoCotista.IdTrader = null;

            //Salva o valor dependendo do tipo de operação, o resto força zerado
            //Resgate total força tudo zerado!
            operacaoCotista.ValorBruto = 0;
            operacaoCotista.ValorLiquido = 0;
            operacaoCotista.Quantidade = 0;
            switch (operacaoCotista.TipoOperacao)
            {
                case (byte)TipoOperacaoCotista.Aplicacao:
                case (byte)TipoOperacaoCotista.AplicacaoAcoesEspecial:
                case (byte)TipoOperacaoCotista.ResgateBruto:
                case (byte)TipoOperacaoCotista.Deposito:
                case (byte)TipoOperacaoCotista.Retirada:
                    operacaoCotista.ValorBruto = Convert.ToDecimal(textValor.Text);
                    break;
                case (byte)TipoOperacaoCotista.AplicacaoCotasEspecial:
                case (byte)TipoOperacaoCotista.ComeCotas:
                case (byte)TipoOperacaoCotista.ResgateCotas:
                case (byte)TipoOperacaoCotista.ResgateCotasEspecial:
                case (byte)TipoOperacaoCotista.AplicacaoCotas:
                    operacaoCotista.Quantidade = Convert.ToDecimal(textValor.Text);
                    break;
                case (byte)TipoOperacaoCotista.ResgateLiquido:
                    operacaoCotista.ValorLiquido = Convert.ToDecimal(textValor.Text);
                    break;
            }

            if (operacaoCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.Aplicacao ||
            operacaoCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.AplicacaoCotasEspecial ||
            operacaoCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.AplicacaoAcoesEspecial ||
            operacaoCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.AplicacaoCotas ||
            operacaoCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.ComeCotas)
            {
                operacaoCotista.TipoResgate = null;
            }
            else
            {
                //Se não informar o tipo de resgate, joga default = FIFO
                if (dropTipoResgate.SelectedIndex == -1)
                {
                    operacaoCotista.TipoResgate = (byte)TipoResgateCotista.FIFO;
                }
            }

            if (!lstTipoOperacaoSemLiquidacao.Contains(operacaoCotista.TipoOperacao.Value))
                operacaoCotista.IdFormaLiquidacao = Convert.ToByte(dropFormaLiquidacao.SelectedItem.Value);
            else
                operacaoCotista.IdFormaLiquidacao = null;

            operacaoCotista.DadosBancarios = textConta.Text.ToString();
            operacaoCotista.Observacao = textObservacao.Text.ToString();

            if (hiddenNotaResgatada.Value != "")
            {
                operacaoCotista.IdPosicaoResgatada = Convert.ToInt32(hiddenNotaResgatada.Value);

                PosicaoCotista posicaoCotista = new PosicaoCotista();
                posicaoCotista.Query.Select(posicaoCotista.Query.IdOperacao);
                posicaoCotista.Query.Where(posicaoCotista.Query.IdPosicao.Equal(operacaoCotista.IdPosicaoResgatada.Value));
                if (posicaoCotista.Query.Load())
                {
                    if (posicaoCotista.IdOperacao.HasValue)
                    {
                        operacaoCotista.IdOperacaoResgatada = posicaoCotista.IdOperacao.Value;
                    }
                }
            }

            operacaoCotista.CotaInformada = null;
            if (chkCotaInformada.Checked)
            {
                if (!string.IsNullOrEmpty(textCotaInformada.Text))
                    operacaoCotista.CotaInformada = Convert.ToDecimal(textCotaInformada.Text);
            }

            operacaoCotista.RegistroEditado = "S";

            #region Verifica se é carteira off shore
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCarteira);
            if (cliente.IdTipo.Value == TipoClienteFixo.OffShore_PJ || cliente.IdTipo.Value == TipoClienteFixo.OffShore_PF)
            {
                if (textValorDespesas.Text == "")
                    operacaoCotista.ValorDespesas = 0;
                else
                    operacaoCotista.ValorDespesas = Convert.ToDecimal(textValorDespesas.Text);

                if (textValorTaxas.Text == "")
                    operacaoCotista.ValorTaxas = 0;
                else
                    operacaoCotista.ValorTaxas = Convert.ToDecimal(textValorTaxas.Text);

                if (textValorTributos.Text == "")
                    operacaoCotista.ValorTributos = 0;
                else
                    operacaoCotista.ValorTributos = Convert.ToDecimal(textValorTributos.Text);

                if (dropContaCorrente.Value != "") operacaoCotista.IdContaCorrente = Convert.ToInt32(dropContaCorrente.Value);

                if (operacaoCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.ResgateBruto ||
                    operacaoCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.ResgateTotal)
                {

                    //Resgate de off shore deve ser sempre de uma posição específica
                    operacaoCotista.TipoResgate = (byte)TipoResgateCotista.Especifico;

                    ClassesOffShore classesOffShore = new ClassesOffShore();
                    classesOffShore.LoadByPrimaryKey(idCarteira);

                    decimal valor = Convert.ToDecimal(textValor.Text);

                    PosicaoCotista posicaoCotistaCheck = new PosicaoCotista();
                    posicaoCotistaCheck.Query.Select(posicaoCotistaCheck.Query.DataConversao, posicaoCotistaCheck.Query.IdSeriesOffShore);
                    posicaoCotistaCheck.Query.Where(posicaoCotistaCheck.Query.IdCarteira.Equal(idCarteira),
                                                    posicaoCotistaCheck.Query.IdCotista.Equal(operacaoCotista.IdCotista.Value),
                                                    posicaoCotistaCheck.Query.Quantidade.GreaterThan(0));

                    posicaoCotistaCheck.Query.Where(posicaoCotistaCheck.Query.IdPosicao.Equal(Convert.ToInt32(hiddenNotaResgatada.Value)));
                    posicaoCotistaCheck.Query.Load();

                    TimeSpan ts = operacaoCotista.DataConversao.Value - posicaoCotistaCheck.DataConversao.Value;

                    if (classesOffShore.LockUp.Value > 0 && ts.Days < classesOffShore.LockUp.Value)
                    {
                        if (classesOffShore.HardSoft.Equals((int)HardSoft.Soft) && textValorPenalidadeLockUp.Text != "")
                            operacaoCotista.PenalidadeLockUp = Convert.ToDecimal(textValorPenalidadeLockUp.Text);

                    }

                    if (classesOffShore.HoldBack.Value > 0 && textValorHoldBack.Text != "")
                        operacaoCotista.ValorHoldBack = Convert.ToDecimal(textValorHoldBack.Text);

                    operacaoCotista.IdSeriesOffShore = posicaoCotistaCheck.IdSeriesOffShore;
                }
                else
                {
                    operacaoCotista.IdSeriesOffShore = this.verificaSerieOffShore(idCarteira, operacaoCotista.DataOperacao.Value, false).IdSeriesOffShore.Value;
                }


            }
            else
            {
                operacaoCotista.ValorDespesas = 0;
                operacaoCotista.ValorTaxas = 0;
                operacaoCotista.ValorTributos = 0;
                operacaoCotista.PenalidadeLockUp = 0;
                operacaoCotista.ValorHoldBack = 0;
            }
            #endregion

            #region Verifica se é FIE (Fundo de Investimento em Previdência)
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);
            if (carteira.Fie.Equals("S") && (operacaoCotista.TipoOperacao.Value != (byte)TipoOperacaoCotista.ResgateBruto &&
                                         operacaoCotista.TipoOperacao.Value != (byte)TipoOperacaoCotista.ResgateCotas &&
                                         operacaoCotista.TipoOperacao.Value != (byte)TipoOperacaoCotista.ResgateCotasEspecial &&
                                         operacaoCotista.TipoOperacao.Value != (byte)TipoOperacaoCotista.ResgateLiquido &&
                                         operacaoCotista.TipoOperacao.Value != (byte)TipoOperacaoCotista.ResgateTotal))
            {
                operacaoCotista.FieModalidade = Convert.ToInt16(dropFieModalidade.SelectedItem.Value);
                operacaoCotista.FieTabelaIr = Convert.ToInt16(dropFieTabelaIr.SelectedItem.Value);

                //Se for VGBL, grava tipo de tabela (Progressiva/Regressiva) para controle de histórico
                if (operacaoCotista.FieModalidade.Equals(FieModalidade.VGBL.GetHashCode()))
                {
                    TributacaoCautelaFie tributacaoCautelaFie = new TributacaoCautelaFie();
                    if (tributacaoCautelaFie.LoadByPrimaryKey(operacaoCotista.DataRegistro.Value, operacaoCotista.IdOperacao.Value))
                    {
                        tributacaoCautelaFie.FieTabelaIr = Convert.ToInt16(dropFieTabelaIr.SelectedItem.Value);
                    }
                    else
                    {
                        tributacaoCautelaFie.Data = operacaoCotista.DataRegistro.Value;
                        tributacaoCautelaFie.IdOperacao = operacaoCotista.IdOperacao.Value;
                        tributacaoCautelaFie.FieTabelaIr = Convert.ToInt16(dropFieTabelaIr.SelectedItem.Value);
                    }

                    tributacaoCautelaFie.Save();
                }
            }
            #endregion

            operacaoCotista.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de OperacaoCotista - Operacao: Update OperacaoCotista: " + operacaoCotista.IdOperacao + UtilitarioWeb.ToString(operacaoCotista),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion

            //Atualiza StatusRealTime para executar***********
            cliente = new Cliente();
            cliente.SetaFlagRealTime(idCarteira, (byte)StatusRealTimeCliente.Executar);
            //************************************************
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// Verifica se a carteira possui oferta de subscricao e atualiza valores
    /// </summary>
    /// <param name="operacaoCotista"></param>
    private void AtualizaOfertaSubscricao(OperacaoCotista operacaoCotista, Boolean incluir)
    {
        Carteira carteira = new Carteira();
        carteira.LoadByPrimaryKey(operacaoCotista.IdCarteira.Value);

        if (carteira.RealizaOfertaSubscricao.Equals("S"))
        {
            //Verifica se possui oferta de subscricao
            OfertaDistribuicaoCotasCollection ofertaDistribuicaoCotasCollection = new OfertaDistribuicaoCotasCollection();
            ofertaDistribuicaoCotasCollection.Query.Where(ofertaDistribuicaoCotasCollection.Query.IdCarteira.Equal(operacaoCotista.IdCarteira.Value) &
                                                          ofertaDistribuicaoCotasCollection.Query.DataInicialOferta.LessThanOrEqual(operacaoCotista.DataOperacao.Value));
            ofertaDistribuicaoCotasCollection.Query.Where(ofertaDistribuicaoCotasCollection.Query.DataFinalOferta.GreaterThanOrEqual(operacaoCotista.DataOperacao.Value) |
                                                          ofertaDistribuicaoCotasCollection.Query.DataFinalOferta.IsNull());
            ofertaDistribuicaoCotasCollection.Query.Load();
            if (ofertaDistribuicaoCotasCollection.Count > 0)
            {
                OfertaDistribuicaoCotas ofertaDistribuicaoCotas = ofertaDistribuicaoCotasCollection[0];

                AplicacaoOfertaDistribuicaoCotas aplicacaoOfertaDistribuicaoCotas = new AplicacaoOfertaDistribuicaoCotas();

                if (incluir)
                {
                    if (!aplicacaoOfertaDistribuicaoCotas.LoadByPrimaryKey(ofertaDistribuicaoCotas.IdOfertaDistribuicaoCotas.Value, operacaoCotista.IdOperacao.Value))
                    {
                        aplicacaoOfertaDistribuicaoCotas.IdOfertaDistribuicaoCotas = ofertaDistribuicaoCotas.IdOfertaDistribuicaoCotas;
                        aplicacaoOfertaDistribuicaoCotas.IdOperacao = operacaoCotista.IdOperacao;
                        aplicacaoOfertaDistribuicaoCotas.Save();
                    }
                }
                else
                {
                    if (aplicacaoOfertaDistribuicaoCotas.LoadByPrimaryKey(ofertaDistribuicaoCotas.IdOfertaDistribuicaoCotas.Value, operacaoCotista.IdOperacao.Value))
                    {
                        aplicacaoOfertaDistribuicaoCotas.MarkAsDeleted();
                        aplicacaoOfertaDistribuicaoCotas.Save();
                    }
                }
            }
        }
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();
    }

    /// <summary>
    /// 
    /// </summary>
    private void SalvarNovo()
    {
        List<byte> lstTipoOperacaoSemLiquidacao = new List<byte>();
        lstTipoOperacaoSemLiquidacao.Add((byte)TipoOperacaoCotista.Retirada);
        lstTipoOperacaoSemLiquidacao.Add((byte)TipoOperacaoCotista.Deposito);

        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxSpinEdit btnEditCodigoCotista = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCotista") as ASPxSpinEdit;
        ASPxDateEdit textDataOperacao = gridCadastro.FindEditFormTemplateControl("textDataOperacao") as ASPxDateEdit;
        ASPxDateEdit textDataRegistro = gridCadastro.FindEditFormTemplateControl("textDataRegistro") as ASPxDateEdit;
        //ASPxCheckBox checkDepositoRetirada = gridCadastro.FindEditFormTemplateControl("checkDepositoRetirada") as ASPxCheckBox;
        ASPxDateEdit textDataConversao = gridCadastro.FindEditFormTemplateControl("textDataConversao") as ASPxDateEdit;
        ASPxDateEdit textDataLiquidacao = gridCadastro.FindEditFormTemplateControl("textDataLiquidacao") as ASPxDateEdit;
        ASPxComboBox dropTipoOperacao = gridCadastro.FindEditFormTemplateControl("dropTipoOperacao") as ASPxComboBox;
        ASPxComboBox dropTipoResgate = gridCadastro.FindEditFormTemplateControl("dropTipoResgate") as ASPxComboBox;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
        ASPxComboBox dropFormaLiquidacao = gridCadastro.FindEditFormTemplateControl("dropFormaLiquidacao") as ASPxComboBox;
        TextBox textConta = gridCadastro.FindEditFormTemplateControl("textConta") as TextBox;
        TextBox textObservacao = gridCadastro.FindEditFormTemplateControl("textObservacao") as TextBox;
        ASPxSpinEdit textCotaInformada = gridCadastro.FindEditFormTemplateControl("textCotaInformada") as ASPxSpinEdit;
        ASPxComboBox dropLocalNegociacao = gridCadastro.FindEditFormTemplateControl("dropLocalNegociacao") as ASPxComboBox;

        ASPxSpinEdit textValorDespesas = gridCadastro.FindEditFormTemplateControl("textValorDespesas") as ASPxSpinEdit;
        ASPxSpinEdit textValorTaxas = gridCadastro.FindEditFormTemplateControl("textValorTaxas") as ASPxSpinEdit;
        ASPxSpinEdit textValorTributos = gridCadastro.FindEditFormTemplateControl("textValorTributos") as ASPxSpinEdit;
        ASPxSpinEdit textValorPenalidadeLockUp = gridCadastro.FindEditFormTemplateControl("textValorPenalidadeLockUp") as ASPxSpinEdit;
        ASPxSpinEdit textValorHoldBack = gridCadastro.FindEditFormTemplateControl("textValorHoldBack") as ASPxSpinEdit;

        ASPxComboBox dropContaCorrente = gridCadastro.FindEditFormTemplateControl("dropContaCorrente") as ASPxComboBox;

        ASPxComboBox dropFieModalidade = gridCadastro.FindEditFormTemplateControl("dropFieModalidade") as ASPxComboBox;
        ASPxComboBox dropFieTabelaIr = gridCadastro.FindEditFormTemplateControl("dropFieTabelaIr") as ASPxComboBox;
        ASPxComboBox dropTrader = gridCadastro.FindEditFormTemplateControl("dropTrader") as ASPxComboBox;
        ASPxCheckBox chkCotaInformada = gridCadastro.FindEditFormTemplateControl("chkCotaInformada") as ASPxCheckBox;

        OperacaoCotista operacaoCotista = new OperacaoCotista();

        int idCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);

        operacaoCotista.IdCarteira = idCarteira;
        operacaoCotista.IdCotista = Convert.ToInt32(btnEditCodigoCotista.Text);
        operacaoCotista.DataOperacao = Convert.ToDateTime(textDataOperacao.Text);
        operacaoCotista.DataRegistro = Convert.ToDateTime(textDataRegistro.Text);
        //operacaoCotista.DepositoRetirada = checkDepositoRetirada.Checked;
        operacaoCotista.DataConversao = Convert.ToDateTime(textDataConversao.Text);
        operacaoCotista.DataLiquidacao = Convert.ToDateTime(textDataLiquidacao.Text);
        operacaoCotista.TipoOperacao = Convert.ToByte(dropTipoOperacao.SelectedItem.Value);
        operacaoCotista.DataAgendamento = Convert.ToDateTime(textDataOperacao.Text); //TODO AJUSTAR DEPOIS
        operacaoCotista.IdLocalNegociacao = Convert.ToInt16(dropLocalNegociacao.SelectedItem.Value);

        if (dropTipoResgate.SelectedIndex != -1)
        {
            operacaoCotista.TipoResgate = Convert.ToByte(dropTipoResgate.SelectedItem.Value);
        }

        if (dropTrader.SelectedIndex > -1)
            operacaoCotista.IdTrader = Convert.ToInt32(dropTrader.SelectedItem.Value);
        else
            operacaoCotista.IdTrader = null;

        //Salva o valor dependendo do tipo de operação, o resto força zerado
        //Resgate total força tudo zerado!
        operacaoCotista.ValorBruto = 0;
        operacaoCotista.ValorLiquido = 0;
        operacaoCotista.Quantidade = 0;

        switch (operacaoCotista.TipoOperacao)
        {
            case (byte)TipoOperacaoCotista.Aplicacao:
            case (byte)TipoOperacaoCotista.AplicacaoAcoesEspecial:
            case (byte)TipoOperacaoCotista.ResgateBruto:
                operacaoCotista.ValorBruto = Convert.ToDecimal(textValor.Text);
                break;
            case (byte)TipoOperacaoCotista.AplicacaoCotasEspecial:
            case (byte)TipoOperacaoCotista.ComeCotas:
            case (byte)TipoOperacaoCotista.ResgateCotas:
            case (byte)TipoOperacaoCotista.ResgateCotasEspecial:
            case (byte)TipoOperacaoCotista.AplicacaoCotas:
                operacaoCotista.Quantidade = Convert.ToDecimal(textValor.Text);
                break;
            case (byte)TipoOperacaoCotista.ResgateLiquido:
                operacaoCotista.ValorLiquido = Convert.ToDecimal(textValor.Text);
                break;
        }

        if (operacaoCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.Aplicacao ||
            operacaoCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.AplicacaoCotasEspecial ||
            operacaoCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.AplicacaoAcoesEspecial ||
            operacaoCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.AplicacaoCotas ||
            operacaoCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.ComeCotas)
        {
            operacaoCotista.TipoResgate = null;
        }
        else
        {
            //Se não informar o tipo de resgate, joga default = FIFO
            if (dropTipoResgate.SelectedIndex == -1)
            {
                operacaoCotista.TipoResgate = (byte)TipoResgateCotista.FIFO;
            }
        }

        if (!lstTipoOperacaoSemLiquidacao.Contains(operacaoCotista.TipoOperacao.Value))
            operacaoCotista.IdFormaLiquidacao = Convert.ToByte(dropFormaLiquidacao.SelectedItem.Value);
        else
            operacaoCotista.IdFormaLiquidacao = null;

        operacaoCotista.DadosBancarios = textConta.Text.ToString();
        operacaoCotista.Observacao = textObservacao.Text.ToString();

        if (hiddenNotaResgatada.Value != "")
        {
            operacaoCotista.IdPosicaoResgatada = Convert.ToInt32(hiddenNotaResgatada.Value);

            PosicaoCotista posicaoCotista = new PosicaoCotista();
            posicaoCotista.Query.Select(posicaoCotista.Query.IdOperacao);
            posicaoCotista.Query.Where(posicaoCotista.Query.IdPosicao.Equal(operacaoCotista.IdPosicaoResgatada.Value));
            if (posicaoCotista.Query.Load())
            {
                if (posicaoCotista.IdOperacao.HasValue)
                {
                    operacaoCotista.IdOperacaoResgatada = posicaoCotista.IdOperacao.Value;
                }
            }
        }

        operacaoCotista.CotaInformada = null;
        if (chkCotaInformada.Checked)
        {
            if (!string.IsNullOrEmpty(textCotaInformada.Text))
                operacaoCotista.CotaInformada = Convert.ToDecimal(textCotaInformada.Text);
        }

        operacaoCotista.Fonte = (byte)FonteOperacaoCotista.Manual;
        operacaoCotista.RegistroEditado = "N";
        operacaoCotista.ValoresColados = "N";

        #region Verifica se é carteira off shore
        Cliente cliente = new Cliente();
        cliente.LoadByPrimaryKey(idCarteira);
        if (cliente.IdTipo.Value == TipoClienteFixo.OffShore_PJ || cliente.IdTipo.Value == TipoClienteFixo.OffShore_PF)
        {
            decimal valor = Convert.ToDecimal(textValor.Text);

            if (textValorDespesas.Text == "")
                operacaoCotista.ValorDespesas = 0;
            else
                operacaoCotista.ValorDespesas = Convert.ToDecimal(textValorDespesas.Text);

            if (textValorTaxas.Text == "")
                operacaoCotista.ValorTaxas = 0;
            else
                operacaoCotista.ValorTaxas = Convert.ToDecimal(textValorTaxas.Text);

            if (textValorTributos.Text == "")
                operacaoCotista.ValorTributos = 0;
            else
                operacaoCotista.ValorTributos = Convert.ToDecimal(textValorTributos.Text);

            operacaoCotista.ValorLiquido = valor - (operacaoCotista.ValorDespesas + operacaoCotista.ValorTaxas + operacaoCotista.ValorTributos);

            if (dropContaCorrente.Value != "") operacaoCotista.IdContaCorrente = Convert.ToInt32(dropContaCorrente.Value);

            if (operacaoCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.ResgateBruto ||
                operacaoCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.ResgateTotal)
            {

                //Resgate de off shore deve ser sempre de uma posição específica
                operacaoCotista.TipoResgate = (byte)TipoResgateCotista.Especifico;

                ClassesOffShore classesOffShore = new ClassesOffShore();
                classesOffShore.LoadByPrimaryKey(idCarteira);

                PosicaoCotista posicaoCotistaCheck = new PosicaoCotista();
                posicaoCotistaCheck.Query.Select(posicaoCotistaCheck.Query.DataConversao, posicaoCotistaCheck.Query.IdSeriesOffShore);
                posicaoCotistaCheck.Query.Where(posicaoCotistaCheck.Query.IdCarteira.Equal(idCarteira),
                                                posicaoCotistaCheck.Query.IdCotista.Equal(operacaoCotista.IdCotista.Value),
                                                posicaoCotistaCheck.Query.Quantidade.GreaterThan(0));

                posicaoCotistaCheck.Query.Where(posicaoCotistaCheck.Query.IdPosicao.Equal(Convert.ToInt32(hiddenNotaResgatada.Value)));
                posicaoCotistaCheck.Query.Load();

                TimeSpan ts = operacaoCotista.DataConversao.Value - posicaoCotistaCheck.DataConversao.Value;

                if (classesOffShore.LockUp.Value > 0 && ts.Days < classesOffShore.LockUp.Value)
                {
                    if (classesOffShore.HardSoft.Equals((int)HardSoft.Soft) && textValorPenalidadeLockUp.Text != "")
                        operacaoCotista.PenalidadeLockUp = Convert.ToDecimal(textValorPenalidadeLockUp.Text);

                }

                if (classesOffShore.HoldBack.Value > 0 && textValorHoldBack.Text != "")
                    operacaoCotista.ValorHoldBack = Convert.ToDecimal(textValorHoldBack.Text);

                operacaoCotista.IdSeriesOffShore = posicaoCotistaCheck.IdSeriesOffShore;
            }
            else
            {
                operacaoCotista.IdSeriesOffShore = this.verificaSerieOffShore(idCarteira, operacaoCotista.DataConversao.Value, false).IdSeriesOffShore.Value;
            }

        }
        else
        {
            operacaoCotista.ValorDespesas = 0;
            operacaoCotista.ValorTaxas = 0;
            operacaoCotista.ValorTributos = 0;
            operacaoCotista.PenalidadeLockUp = 0;
            operacaoCotista.ValorHoldBack = 0;
        }

        #endregion

        operacaoCotista.Save();

        #region Verifica se é FIE (Fundo de Investimento em Previdência)
        Carteira carteira = new Carteira();
        carteira.LoadByPrimaryKey(idCarteira);
        if (carteira.Fie.Equals("S") && (operacaoCotista.TipoOperacao.Value != (byte)TipoOperacaoCotista.ResgateBruto &&
                                         operacaoCotista.TipoOperacao.Value != (byte)TipoOperacaoCotista.ResgateCotas &&
                                         operacaoCotista.TipoOperacao.Value != (byte)TipoOperacaoCotista.ResgateCotasEspecial &&
                                         operacaoCotista.TipoOperacao.Value != (byte)TipoOperacaoCotista.ResgateLiquido &&
                                         operacaoCotista.TipoOperacao.Value != (byte)TipoOperacaoCotista.ResgateTotal))
        {
            operacaoCotista.FieModalidade = Convert.ToInt16(dropFieModalidade.SelectedItem.Value);
            operacaoCotista.FieTabelaIr = Convert.ToInt16(dropFieTabelaIr.SelectedItem.Value);
            operacaoCotista.Save();

            //Se for VGBL, grava tipo de tabela (Progressiva/Regressiva) para controle de histórico
            if (operacaoCotista.FieModalidade.Equals(FieModalidade.VGBL.GetHashCode()))
            {
                TributacaoCautelaFie tributacaoCautelaFie = new TributacaoCautelaFie();
                if (tributacaoCautelaFie.LoadByPrimaryKey(operacaoCotista.DataRegistro.Value, operacaoCotista.IdOperacao.Value))
                {
                    tributacaoCautelaFie.FieTabelaIr = Convert.ToInt16(dropFieTabelaIr.SelectedItem.Value);
                }
                else
                {
                    tributacaoCautelaFie.Data = operacaoCotista.DataRegistro.Value;
                    tributacaoCautelaFie.IdOperacao = operacaoCotista.IdOperacao.Value;
                    tributacaoCautelaFie.FieTabelaIr = Convert.ToInt16(dropFieTabelaIr.SelectedItem.Value);
                }

                tributacaoCautelaFie.Save();
            }

        }
        #endregion

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de OperacaoCotista - Operacao: Insert OperacaoCotista: " + operacaoCotista.IdOperacao + UtilitarioWeb.ToString(operacaoCotista),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        #region Faz uma inserção tb em OperacaoFundo se for o caso

        //Operacoes de aplic/resgate em carteira adm não pode entrar!
        if (operacaoCotista.IdCarteira.Value != operacaoCotista.IdCotista.Value)
        {

            Cotista cotista = new Cotista();
            cotista.LoadByPrimaryKey(operacaoCotista.IdCotista.Value);

            if (cotista.IdClienteEspelho.HasValue)
            {
                int idCliente = 0;
                Cliente clienteFundo = new Cliente();
                if (clienteFundo.LoadByPrimaryKey(cotista.IdClienteEspelho.Value))
                {
                    if (Convert.ToDateTime(textDataOperacao.Text) > clienteFundo.DataDia.Value ||
                   (Convert.ToDateTime(textDataOperacao.Text) == clienteFundo.DataDia.Value && clienteFundo.Status != (byte)StatusCliente.Divulgado))
                    {
                        idCliente = clienteFundo.IdCliente.Value;
                        OperacaoFundo operacaoFundo = new OperacaoFundo();
                        operacaoFundo.DataAgendamento = Convert.ToDateTime(textDataOperacao.Text);
                        operacaoFundo.DataConversao = Convert.ToDateTime(textDataConversao.Text);
                        operacaoFundo.DataRegistro = Convert.ToDateTime(textDataRegistro.Text);
                        //operacaoCotista.DepositoRetirada = checkDepositoRetirada.Checked;
                        operacaoFundo.DataLiquidacao = Convert.ToDateTime(textDataLiquidacao.Text);
                        operacaoFundo.DataOperacao = Convert.ToDateTime(textDataOperacao.Text);
                        operacaoFundo.TipoOperacao = Convert.ToByte(dropTipoOperacao.SelectedItem.Value);
                        operacaoFundo.IdCarteira = idCarteira;
                        operacaoFundo.IdCliente = idCliente;
                        operacaoFundo.IdFormaLiquidacao = Convert.ToByte(dropFormaLiquidacao.SelectedItem.Value);
                        operacaoFundo.Observacao = textObservacao.Text.ToString();

                        if (dropTipoResgate.SelectedIndex != -1)
                        {
                            operacaoFundo.TipoResgate = Convert.ToByte(dropTipoResgate.SelectedItem.Value);
                        }

                        //Salva o valor dependendo do tipo de operação, o resto força zerado
                        //Resgate total força tudo zerado!
                        operacaoFundo.ValorBruto = 0;
                        operacaoFundo.ValorLiquido = 0;
                        operacaoFundo.Quantidade = 0;
                        switch (operacaoCotista.TipoOperacao)
                        {
                            case (byte)TipoOperacaoCotista.Aplicacao:
                            case (byte)TipoOperacaoCotista.AplicacaoAcoesEspecial:
                            case (byte)TipoOperacaoCotista.ResgateBruto:
                                operacaoFundo.ValorBruto = Convert.ToDecimal(textValor.Text);
                                break;
                            case (byte)TipoOperacaoCotista.AplicacaoCotasEspecial:
                            case (byte)TipoOperacaoCotista.ComeCotas:
                            case (byte)TipoOperacaoCotista.ResgateCotas:
                            case (byte)TipoOperacaoCotista.ResgateCotasEspecial:
                                operacaoFundo.Quantidade = Convert.ToDecimal(textValor.Text);
                                break;
                            case (byte)TipoOperacaoCotista.ResgateLiquido:
                                operacaoFundo.ValorLiquido = Convert.ToDecimal(textValor.Text);
                                break;
                        }

                        if (operacaoCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.Aplicacao ||
                            operacaoCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.AplicacaoCotasEspecial ||
                            operacaoCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.AplicacaoAcoesEspecial ||
                            operacaoCotista.TipoOperacao.Value == (byte)TipoOperacaoCotista.ComeCotas)
                        {
                            operacaoFundo.TipoResgate = null;
                        }
                        else
                        {
                            //Se não informar o tipo de resgate, joga default = FIFO
                            if (dropTipoResgate.SelectedIndex == -1)
                            {
                                operacaoFundo.TipoResgate = (byte)TipoResgateCotista.FIFO;
                            }
                        }

                        if (operacaoCotista.IdPosicaoResgatada.HasValue)
                        {
                            PosicaoCotista posicaoCotista = new PosicaoCotista();
                            posicaoCotista.LoadByPrimaryKey(operacaoCotista.IdPosicaoResgatada.Value);

                            DateTime dataConversao = posicaoCotista.DataConversao.Value;

                            PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
                            posicaoFundoCollection.Query.Where(posicaoFundoCollection.Query.IdCarteira.Equal(idCarteira),
                                                               posicaoFundoCollection.Query.IdCliente.Equal(idCliente),
                                                               posicaoFundoCollection.Query.DataConversao.Equal(dataConversao));
                            posicaoFundoCollection.Query.Load();

                            if (posicaoFundoCollection.Count > 0)
                            {
                                operacaoFundo.IdPosicaoResgatada = posicaoFundoCollection[0].IdPosicao.Value;
                            }
                        }

                        operacaoFundo.Fonte = (byte)FonteOperacaoFundo.Manual;

                        operacaoFundo.Save();
                    }
                }
            }
        }
        #endregion

        #region Verifica se a carteira possui oferta de subscricao
        this.AtualizaOfertaSubscricao(operacaoCotista, true);
        #endregion

        //Atualiza StatusRealTime para executar***********
        cliente = new Cliente();
        cliente.SetaFlagRealTime(idCarteira, (byte)StatusRealTimeCliente.Executar);
        //************************************************
    }

    /// <summary>
    /// Retorno serie do Fundo/Classe off shore. Se não existir, cria a serie.
    /// </summary>
    /// <param name="idCarteira"></param>
    /// <param name="dataOperacao"></param>
    /// <returns></returns>
    protected SeriesOffShore verificaSerieOffShore(int idCarteira, DateTime dataOperacao, bool previa)
    {
        SeriesOffShore serieOffShore = null;

        ClassesOffShore classesOffShore = new ClassesOffShore();
        classesOffShore.LoadByPrimaryKey(idCarteira);

        SeriesOffShoreCollection seriesOffShoreCollection = new SeriesOffShoreCollection();
        seriesOffShoreCollection.Query.Where(seriesOffShoreCollection.Query.IdClassesOffShore.Equal(idCarteira));
        seriesOffShoreCollection.Query.Where(seriesOffShoreCollection.Query.DataFimAplicacao.GreaterThanOrEqual(dataOperacao));
        seriesOffShoreCollection.Query.Where(seriesOffShoreCollection.Query.DataInicioAplicacao.LessThanOrEqual(dataOperacao));
        seriesOffShoreCollection.Query.Load();

        if (seriesOffShoreCollection.Count > 0)
            serieOffShore = seriesOffShoreCollection[0];
        else
            serieOffShore = this.criaSerieOffShore(classesOffShore, dataOperacao, previa);

        return serieOffShore;
    }

    protected SeriesOffShore criaSerieOffShore(ClassesOffShore classeOffShore, DateTime dataOperacao, bool previa)
    {
        //Seleciona serie base para utilizar valor da cota na nova serie
        SeriesOffShore serieBase = new SeriesOffShore();
        serieBase.Query.Where(serieBase.Query.SerieBase.Equal("S"));
        serieBase.Query.Where(serieBase.Query.IdClassesOffShore.Equal(classeOffShore.IdClassesOffShore));
        serieBase.Query.Load();

        int numeroMeses = (dataOperacao.Month - classeOffShore.DataInicio.Value.Month) + 12 * (dataOperacao.Year - classeOffShore.DataInicio.Value.Year);
        int totalMeses = ((int)(numeroMeses / classeOffShore.FrequenciaSerie.Value)) * classeOffShore.FrequenciaSerie.Value;


        SeriesOffShore serieOffShore = new SeriesOffShore();
        serieOffShore.IdClassesOffShore = classeOffShore.IdClassesOffShore;
        serieOffShore.DataInicioAplicacao = Calendario.RetornaPrimeiroDiaCorridoMes(classeOffShore.DataInicio.Value, totalMeses);
        serieOffShore.DataFimAplicacao = Calendario.RetornaUltimoDiaCorridoMes(classeOffShore.DataInicio.Value, totalMeses);
        serieOffShore.Mnemonico = classeOffShore.Nome + " - " + serieOffShore.DataInicioAplicacao.Value.ToString("MM/yyyy");
        serieOffShore.Descricao = classeOffShore.Nome + " - " + serieOffShore.DataInicioAplicacao.Value.ToString("MM/yyyy");
        serieOffShore.VlCotaInicial = 1;

        if (!previa)
            serieOffShore.Save();

        return serieOffShore;

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (gridCadastro.IsEditing && !gridCadastro.IsNewRowEditing)
        {
            //Preenche o campo de valor, de acordo com o tipo de operação
            ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;

            byte tipoOperacao = Convert.ToByte(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, new string[] { "TipoOperacao" }));
            if (!gridCadastro.IsNewRowEditing)
            {
                switch (tipoOperacao)
                {
                    case (byte)TipoOperacaoCotista.Aplicacao:
                    case (byte)TipoOperacaoCotista.AplicacaoAcoesEspecial:
                    case (byte)TipoOperacaoCotista.ResgateBruto:
                    case (byte)TipoOperacaoCotista.ResgateTotal:
                    case (byte)TipoOperacaoCotista.Deposito:
                    case (byte)TipoOperacaoCotista.Retirada:
                        decimal valorBruto = Convert.ToDecimal(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, new string[] { "ValorBruto" }));
                        textValor.Text = valorBruto.ToString();
                        break;
                    case (byte)TipoOperacaoCotista.AplicacaoCotasEspecial:
                    case (byte)TipoOperacaoCotista.ComeCotas:
                    case (byte)TipoOperacaoCotista.ResgateCotas:
                    case (byte)TipoOperacaoCotista.ResgateCotasEspecial:
                        decimal quantidade = Convert.ToDecimal(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, new string[] { "Quantidade" }));
                        textValor.Text = quantidade.ToString();
                        break;
                    case (byte)TipoOperacaoCotista.ResgateLiquido:
                        decimal valorLiquido = Convert.ToDecimal(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, new string[] { "ValorLiquido" }));
                        textValor.Text = valorLiquido.ToString();
                        break;
                }
            }

            //Verifica se é carteira off shore
            ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
            ASPxSpinEdit textValorDespesas = gridCadastro.FindEditFormTemplateControl("textValorDespesas") as ASPxSpinEdit;
            ASPxSpinEdit textValorTaxas = gridCadastro.FindEditFormTemplateControl("textValorTaxas") as ASPxSpinEdit;
            ASPxSpinEdit textValorTributos = gridCadastro.FindEditFormTemplateControl("textValorTributos") as ASPxSpinEdit;

            ASPxComboBox dropContaCorrente = gridCadastro.FindEditFormTemplateControl("dropContaCorrente") as ASPxComboBox;
            ASPxComboBox dropFormaLiquidacao = gridCadastro.FindEditFormTemplateControl("dropFormaLiquidacao") as ASPxComboBox;
            ASPxComboBox dropTipoResgate = gridCadastro.FindEditFormTemplateControl("dropTipoResgate") as ASPxComboBox;

            int idPessoa = 0;
            int idCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCarteira);
            if (cliente.IdTipo.Equals(TipoClienteFixo.OffShore_PF) || cliente.IdTipo.Equals(TipoClienteFixo.OffShore_PJ))
            {
                textValorDespesas.ClientEnabled = true;
                textValorTaxas.ClientEnabled = true;
                textValorTributos.ClientEnabled = true;
                dropContaCorrente.ClientEnabled = true;
                dropFormaLiquidacao.ClientEnabled = false;
                dropTipoResgate.ClientEnabled = false;
            }
            else
            {
                textValorDespesas.ClientEnabled = false;
                textValorTaxas.ClientEnabled = false;
                textValorTributos.ClientEnabled = false;
                dropContaCorrente.ClientEnabled = false;
                dropFormaLiquidacao.ClientEnabled = true;
                dropTipoResgate.ClientEnabled = true;
            }


            #region Verifica se é Fundo de Previdencia
            ASPxComboBox dropFieModalidade = gridCadastro.FindEditFormTemplateControl("dropFieModalidade") as ASPxComboBox;
            ASPxComboBox dropFieTabelaIr = gridCadastro.FindEditFormTemplateControl("dropFieTabelaIr") as ASPxComboBox;

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            if (carteira.Fie.Equals("S"))
            {
                dropFieModalidade.ClientEnabled = true;
                dropFieTabelaIr.ClientEnabled = true;
            }
            else
            {
                dropFieModalidade.ClientEnabled = false;
                dropFieTabelaIr.ClientEnabled = false;
            }
            #endregion


        }

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdOperacao");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idOperacao = Convert.ToInt32(keyValuesId[i]);

                OperacaoCotista operacaoCotista = new OperacaoCotista();
                if (operacaoCotista.LoadByPrimaryKey(idOperacao))
                {
                    int idCarteira = operacaoCotista.IdCarteira.Value;
                    DateTime dataAplicacao = operacaoCotista.DataOperacao.Value;
                    int idPosicao = 0;

                    DetalheResgateCotistaCollection detalheResgateCotistaCollection = new DetalheResgateCotistaCollection();
                    detalheResgateCotistaCollection.Query.Select(detalheResgateCotistaCollection.Query.IdOperacao,
                                                                 detalheResgateCotistaCollection.Query.IdPosicaoResgatada);
                    detalheResgateCotistaCollection.Query.Where(detalheResgateCotistaCollection.Query.IdOperacao.Equal(idOperacao));
                    detalheResgateCotistaCollection.Query.Load();
                    detalheResgateCotistaCollection.MarkAllAsDeleted();
                    detalheResgateCotistaCollection.Save();

                    PosicaoCotistaCollection posicaoCotistaColl = new PosicaoCotistaCollection();
                    posicaoCotistaColl.Query.Where(posicaoCotistaColl.Query.IdOperacao.Equal(idOperacao));

                    if (posicaoCotistaColl.Query.Load())
                    {
                        idPosicao = posicaoCotistaColl[0].IdPosicao.Value;
                        posicaoCotistaColl.MarkAllAsDeleted();
                        posicaoCotistaColl.Save();
                    }

                    PosicaoCotistaHistoricoCollection posicaoCotistaHistoricoColl = new PosicaoCotistaHistoricoCollection();
                    posicaoCotistaHistoricoColl.Query.Where(posicaoCotistaHistoricoColl.Query.DataHistorico.GreaterThanOrEqual(dataAplicacao) &
                                                            posicaoCotistaHistoricoColl.Query.IdCarteira.Equal(idCarteira) & 
                                                            posicaoCotistaHistoricoColl.Query.IdOperacao.Equal(idOperacao));

                    if (posicaoCotistaHistoricoColl.Query.Load())
                    {
                        idPosicao = posicaoCotistaHistoricoColl[0].IdPosicao.Value;
                        posicaoCotistaHistoricoColl.MarkAllAsDeleted();
                        posicaoCotistaHistoricoColl.Save();
                    }

                    PosicaoCotistaAberturaCollection posicaoCotistaAberturaColl = new PosicaoCotistaAberturaCollection();
                    posicaoCotistaAberturaColl.Query.Where(posicaoCotistaAberturaColl.Query.DataHistorico.GreaterThanOrEqual(dataAplicacao) &
                                                           posicaoCotistaAberturaColl.Query.IdCarteira.Equal(idCarteira) & 
                                                           posicaoCotistaAberturaColl.Query.IdOperacao.Equal(idOperacao));

                    if (posicaoCotistaAberturaColl.Query.Load())
                    {
                        idPosicao = posicaoCotistaAberturaColl[0].IdPosicao.Value;
                        posicaoCotistaAberturaColl.MarkAllAsDeleted();
                        posicaoCotistaAberturaColl.Save();
                    }

                    TributacaoCautelaFieCollection tributacaoCautelaFieCollection = new TributacaoCautelaFieCollection();
                    tributacaoCautelaFieCollection.Query.Where(tributacaoCautelaFieCollection.Query.IdOperacao.Equal(idOperacao));

                    if (tributacaoCautelaFieCollection.Query.Load())
                    {
                        tributacaoCautelaFieCollection.MarkAllAsDeleted();
                        tributacaoCautelaFieCollection.Save();
                    }

                    if (idPosicao > 0)
                    {
                        PosicaoCotistaAlteradaCollection posicaoCotistaAlteradaColl = new PosicaoCotistaAlteradaCollection();
                        posicaoCotistaAlteradaColl.Query.Where(posicaoCotistaAlteradaColl.Query.IdPosicao.Equal(idPosicao));

                        if (posicaoCotistaAlteradaColl.Query.Load())
                        {
                            posicaoCotistaAlteradaColl.MarkAllAsDeleted();
                            posicaoCotistaAlteradaColl.Save();
                        }
                    }

                    //
                    OperacaoCotista operacaoCotistaClone = (OperacaoCotista)Utilitario.Clone(operacaoCotista);
                    //

                    #region Atualiza oferta de subscricao
                    this.AtualizaOfertaSubscricao(operacaoCotista, false);
                    #endregion

                    operacaoCotista.MarkAsDeleted();
                    operacaoCotista.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de OperacaoCotista - Operacao: Delete OperacaoCotista: " + idOperacao + UtilitarioWeb.ToString(operacaoCotistaClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion

                    //Atualiza StatusRealTime para executar***********
                    Cliente cliente = new Cliente();
                    cliente.SetaFlagRealTime(idCarteira, (byte)StatusRealTimeCliente.Executar);
                    //************************************************
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e)
    {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing)
        {
            ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
            e.Properties["cpBtnEditCodigoCarteira"] = btnEditCodigoCarteira.ClientID;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e)
    {
        this.FocaCampoGrid("btnEditCodigoCarteira");
        base.gridCadastro_PreRender(sender, e);

        ASPxSpinEdit btnEditCodigoCarteiraFiltro = popupFiltro.FindControl("btnEditCodigoCarteiraFiltro") as ASPxSpinEdit;

        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        Label labelFiltroHeader = gridCadastro.FindStatusBarTemplateControl("labelFiltroHeader") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");

        if (btnEditCodigoCarteiraFiltro != null && btnEditCodigoCarteiraFiltro.Text != "")
        {
            texto.Append(" Carteira: ").Append(btnEditCodigoCarteiraFiltro.Text);
        }
        if (btnEditCodigoCarteiraFiltro.Text != "")
        {
            if (texto.Length > 0)
            {
                texto.Append(" |");
            }
            texto.Append(" Cotista: ").Append(btnEditCodigoCotistaFiltro.Text);
        }
        if (textDataInicio.Text != "")
        {
            if (texto.Length > 0)
            {
                texto.Append(" |");
            }
            texto.Append(" Data Início >= ").Append(textDataInicio.Text.Substring(0, 10));
        }
        if (textDataFim.Text != "")
        {
            if (texto.Length > 0)
            {
                texto.Append(" |");
            }
            texto.Append(" Data Fim <= ").Append(textDataFim.Text.Substring(0, 10));
        }
        if (dropTipoOperacaoFiltro.SelectedIndex > -1)
        {
            if (texto.Length > 0)
            {
                texto.Append(" |");
            }
            texto.Append(" Tipo = ").Append(dropTipoOperacaoFiltro.Text);
        }

        labelFiltro.Text = texto.ToString();
    }

    protected void EsDSTrader_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        TraderCollection coll = new TraderCollection();
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void chkCotaInformada_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
        {
            int editingRowVisibleIndex = gridCadastro.EditingRowVisibleIndex;
            string CotaInformada = gridCadastro.GetRowValues(editingRowVisibleIndex, "CotaInformada").ToString();

            ASPxCheckBox chkCotaInformada = gridCadastro.FindEditFormTemplateControl("chkCotaInformada") as ASPxCheckBox;
            chkCotaInformada.Checked = !string.IsNullOrEmpty(CotaInformada) ? true : false;

            ASPxSpinEdit textCotaInformada = gridCadastro.FindEditFormTemplateControl("textCotaInformada") as ASPxSpinEdit;
            textCotaInformada.ClientVisible = chkCotaInformada.Checked;
        }
    }


    protected string verificaSuitability(int idCotista, int idCarteira, byte tipoOperacao, DateTime dataOperacao)
    {
        ControllerPerfilCotista controllerPerfilCotista = new ControllerPerfilCotista();

        //TODO: Na migracao para versão 1.1.N (GPS), seleciona o ID do cliente pelo atributo cliente espelho do cadastro do cotista 
        //para selecao das informações de suitability do cadastro de cliente
        Cotista cotista = new Cotista();
        cotista.LoadByPrimaryKey(idCotista);

        PessoaSuitability pessoaSuitability = new PessoaSuitability();
        if (pessoaSuitability.LoadByPrimaryKey(cotista.IdPessoa.Value))
        {
            //Se cliente dispensado, não faz nada
            if (pessoaSuitability.Dispensado.Equals("S") || pessoaSuitability.Recusa.Equals("S"))
                return "";

            //Verifica se perfil do cliente foi informado ou se foi recusado a definição do perfil
            if (!pessoaSuitability.Perfil.HasValue)
                return "Perfil não informado. Não é possível continuar a operação.";

            //Verifica se perfil ainda está válido
            if (!pessoaSuitability.UltimaAlteracao.HasValue || dataOperacao > controllerPerfilCotista.retornaValidade(pessoaSuitability.UltimaAlteracao.Value))
                return "Perfil desatualizado. Não é possível continuar a operação.";

            //Verifica se é primeira aplicação no fundo
            OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();
            operacaoCotistaCollection.Query.Where(operacaoCotistaCollection.Query.IdCarteira.Equal(idCarteira));
            operacaoCotistaCollection.Query.Where(operacaoCotistaCollection.Query.IdCotista.Equal(idCotista));
            operacaoCotistaCollection.Query.Load();

            if (operacaoCotistaCollection.Count == 0)
            {
                return controllerPerfilCotista.selecionaMensagemEvento(16, idCotista);
            }
        }
        return "";
    }

    protected void logMensagem_callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        ControllerPerfilCotista.LogMensagem logMensagem = JsonConvert.DeserializeObject<ControllerPerfilCotista.LogMensagem>(e.Parameter);

        SuitabilityLogMensagens.createSuitabilityLogMensagens(HttpContext.Current.User.Identity.Name,
                                                              Convert.ToInt32(logMensagem.IdCotista),
                                                              Convert.ToInt32(logMensagem.IdMensagem),
                                                              logMensagem.Mensagem,
                                                              logMensagem.Resposta);

    }

    protected void mensagemTermoInadequacao_callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        ControllerPerfilCotista controllerPerfilCotista = new ControllerPerfilCotista();

        e.Result = controllerPerfilCotista.selecionaMensagemEvento(18, e.Parameter[0]);
    }

    protected void termoInadequacao_callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        ControllerPerfilCotista controllerPerfilCotista = new ControllerPerfilCotista();

        int idCotista = Convert.ToInt32(e.Parameter.Split('|')[0]);
        int idCarteira = Convert.ToInt32(e.Parameter.Split('|')[1]);
        int idPerfilCotista = Convert.ToInt32(e.Parameter.Split('|')[2]);
        int idPerfilPosicao = Convert.ToInt32(e.Parameter.Split('|')[3]);

        suitabilityPerfilCotista.LoadByPrimaryKey(idPerfilCotista);
        suitabilityPerfilPosicao.LoadByPrimaryKey(idPerfilPosicao);

        controllerPerfilCotista.enviaTermoInadequacao(idCotista, idCarteira, suitabilityPerfilCotista, suitabilityPerfilPosicao);
    }


}