﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ContaCorrenteCotista.aspx.cs"
    Inherits="CadastrosBasicos_ContaCorrenteCotista" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
    var popup = false;
    var operacao = '';
    document.onkeydown=onDocumentKeyDown;
    
    function OnGetDataCotista(data) 
    {
        btnEditCodigoCotistaFiltro.SetValue(data);        
        callbackCotista.SendCallback(btnEditCodigoCotistaFiltro.GetValue());
        popupCotista.HideWindow();
        btnEditCodigoCotistaFiltro.Focus();
        gridCadastro.PerformCallback('btnRefresh');
    } 
    
    function CheckContaDefault(ContaDefault)
    {
    
    
    }  
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true"
            EnableScriptLocalization="true" />
        <dxcb:ASPxCallback ID="callbackCotista" runat="server" OnCallback="callbackCotista_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
            {
                var textNomeCotista = document.getElementById('textNomeCotistaFiltro');                              
                OnCallBackCompleteCliente(s, e, popupMensagemCotista, btnEditCodigoCotistaFiltro, textNomeCotistaFiltro);  
            }        
        " />
        </dxcb:ASPxCallback>
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Associação Conta Corrente X Cotista"></asp:Label>
                            </div>
                            <div id="mainContent">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Label ID="labelCotistaFiltro" runat="server" CssClass="labelNormal" Text="Cotista:"></asp:Label>
                                        </td>
                                        <td>
                                            <dxe:ASPxSpinEdit ID="btnEditCodigoCotistaFiltro" runat="server" CssClass="textButtonEdit"
                                                ClientInstanceName="btnEditCodigoCotistaFiltro" MaxLength="10" NumberType="Integer">
                                                <Buttons>
                                                    <dxe:EditButton>
                                                    </dxe:EditButton>
                                                </Buttons>
                                                <ClientSideEvents KeyPress="function(s, e) {document.getElementById('textNomeCotistaFiltro').value = '';} "
                                                    ButtonClick="function(s, e) {popupCotista.ShowAtElementByID(s.name);}" LostFocus="function(s, e) {OnLostFocus(popupMensagemCotista, callbackCotista, btnEditCodigoCotistaFiltro); gridCadastro.PerformCallback('btnRefresh');}" />
                                            </dxe:ASPxSpinEdit>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="textNomeCotistaFiltro" runat="server" CssClass="textNome" Enabled="false"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnVincular" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnAdd" OnClientClick="operacao='btnVincular'; gridCadastro.PerformCallback(operacao); return false;">
                                        <asp:Literal ID="Literal1" runat="server" Text="Vincular" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnDesvincular" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnDelete" OnClientClick="operacao='btnDesvincular'; gridCadastro.PerformCallback(operacao); return false;">
                                        <asp:Literal ID="Literal2" runat="server" Text="Desvincular" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnPdf" OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal4" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnExcel" OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal5" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton>
                                </div>
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true" KeyFieldName="IdConta"
                                        DataSourceID="EsDsContaCorrente" OnCustomCallback="gridCadastro_CustomCallback"
                                        OnPreRender="gridCadastro_PreRender" OnCustomJSProperties="gridCadastro_CustomJSProperties"
                                        OnBeforeGetCallbackResult="gridCadastro_PreRender">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn VisibleIndex="0" ShowClearFilterButton="True">
                                                <HeaderTemplate>
                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                                </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="IdConta" Caption="Id.Conta" Width="15%"
                                                VisibleIndex="1">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="Banco" Width="20%" VisibleIndex="2" UnboundType="String">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="Agencia" Caption="Agência" Width="20%" VisibleIndex="3"
                                                UnboundType="String">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="Numero"  Caption="Número" Width="10%" VisibleIndex="4">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="DigitoConta" Caption="Dígito Conta" Width="10%"
                                                VisibleIndex="5">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="Moeda" Width="15%" VisibleIndex="6" UnboundType="String">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="IdCotista" Caption="Cotista" Width="10%"
                                                VisibleIndex="7" UnboundType="String">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="ContaDefaultCotista" Width="15%" Caption="Conta Default?" VisibleIndex="8">
                                                <DataItemTemplate>
                                                    <dxe:ASPxCheckBox ID="cbContaDefault" ClientInstanceName="cbContaDefault" 
                                                    ValueChecked="S" ValueUnchecked="N" ValueType="System.String" runat="server" Value='<%#Eval("ContaDefaultCotista")%>'> 
                                                    </dxe:ASPxCheckBox>
                                                </DataItemTemplate>
                                            </dxwgv:GridViewDataTextColumn>                                            
                                        </Columns>
                                        <ClientSideEvents CallbackError="function(s, e) { operacao=''; alert(e.message); gridCadastro.PerformCallback('btnRefresh'); }" EndCallback="function(s, e) { if(operacao == 'btnVincular' || operacao == 'btnDesvincular') alert('Procedimento executado com sucesso!'); operacao = ''}" />
                                        <SettingsCommandButton>
                                            <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                                        </SettingsCommandButton>
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
        <cc1:esDataSource ID="EsDSContaCorrente" runat="server" OnesSelect="EsDSContaCorrente_esSelect" />
        <cc1:esDataSource ID="EsDSCotista" runat="server" OnesSelect="EsDSCotista_esSelect" />
    </form>
</body>
</html>
