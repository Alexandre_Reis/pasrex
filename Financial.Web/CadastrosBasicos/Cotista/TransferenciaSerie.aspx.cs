﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using DevExpress.Web;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Util;
using System.Drawing;
using Financial.Web.Common;
using Financial.Common;
using Financial.InvestidorCotista;
using Financial.Fundo;
using Financial.Investidor;
using Financial.Investidor.Enums;
using System.Threading;

public partial class CadastrosBasicos_TransferenciaSerie : CadastroBasePage
{
    static public int idCarteiraPosicao = 0;
    static public int idSerieOrigem = 0;
    static public int idCotistaPosicao = 0;
    static public DateTime dataExecucao = new DateTime();
    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasPopupCarteira = true;
        this.HasPopupCotista = true;
        base.Page_Load(sender, e);
    }

    #region EsSelect
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSTransferenciaSerie_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        SeriesOffShoreQuery serieOrigem = new SeriesOffShoreQuery("serieOrigem");
        PosicaoCotistaAberturaQuery posicaoAbertura = new PosicaoCotistaAberturaQuery("posicaoAbertura");
        PosicaoCotistaQuery posicao = new PosicaoCotistaQuery("posicao");
        SeriesOffShoreQuery serieDestino = new SeriesOffShoreQuery("serieDestino");
        CarteiraQuery carteiraQuery = new CarteiraQuery("carteira");
        CotistaQuery cotistaQuery = new CotistaQuery("cotista");
        TransferenciaSerieQuery transQuery = new TransferenciaSerieQuery("transferencia");

        TransferenciaSerieCollection coll = new TransferenciaSerieCollection();

        transQuery.Select(transQuery.IdTransferenciaSerie, 
                          carteiraQuery.Nome.As("DescricaoCarteira"),
                          cotistaQuery.Apelido.As("DescricaoCotista"),
                          serieOrigem.Descricao.As("NomeSerieOrigem"),
                          serieDestino.Descricao.As("NomeSerieDestino"),
                          transQuery.IdPosicao,
                          transQuery.DataExecucao,
                          transQuery.IdCarteira,
                          cotistaQuery.IdCotista,
                          transQuery.IdSerieOrigem,
                          transQuery.IdSerieDestino);
        transQuery.InnerJoin(serieOrigem).On(transQuery.IdSerieOrigem.Equal(serieOrigem.IdSeriesOffShore));
        transQuery.InnerJoin(serieDestino).On(transQuery.IdSerieDestino.Equal(serieDestino.IdSeriesOffShore));
        transQuery.InnerJoin(carteiraQuery).On(transQuery.IdCarteira.Equal(carteiraQuery.IdCarteira));
        transQuery.LeftJoin(posicao).On(transQuery.IdPosicao.Equal(posicao.IdPosicao));
        transQuery.LeftJoin(posicaoAbertura).On(transQuery.IdPosicao.Equal(posicaoAbertura.IdPosicao) & transQuery.DataPosicao.Equal(posicaoAbertura.DataHistorico));
        transQuery.InnerJoin(cotistaQuery).On(cotistaQuery.IdCotista.Equal(posicao.IdCotista.Coalesce(posicaoAbertura.IdCotista)));
        transQuery.Where(transQuery.IdEventoRollUp.IsNull());
        transQuery.OrderBy(transQuery.DataPosicao.Descending);

        coll.Load(transQuery);

        e.Collection = coll;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CarteiraCollection coll = new CarteiraCollection();
        ClienteQuery clienteQuery = new ClienteQuery("cliente");
        CarteiraQuery carteiraQuery = new CarteiraQuery("carteira");

        carteiraQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente.Equal(carteiraQuery.IdCarteira));
        carteiraQuery.Where(clienteQuery.IdTipo.In((int)TipoClienteFixo.OffShore_PJ, (int)TipoClienteFixo.OffShore_PF));
        carteiraQuery.OrderBy(carteiraQuery.IdCarteira.Descending);

        coll.Load(carteiraQuery);
        e.Collection = coll;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSPosicaoCotista_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        PosicaoCotistaAberturaCollection coll = new PosicaoCotistaAberturaCollection();

        if (idCarteiraPosicao != 0 && idSerieOrigem != 0)
        {
            coll = RetornaPosicao(idCarteiraPosicao, idCotistaPosicao, idSerieOrigem, dataExecucao);
        }

        e.Collection = coll;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSSerieOffShore_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ASPxTextBox hiddenCarteira = gridCadastro.FindEditFormTemplateControl("hiddenCarteira") as ASPxTextBox;
        ASPxTextBox hiddenSerieOrigem = gridCadastro.FindEditFormTemplateControl("hiddenSerieOrigem") as ASPxTextBox;
        SeriesOffShoreCollection coll = new SeriesOffShoreCollection();
        if (hiddenCarteira != null && !string.IsNullOrEmpty(hiddenCarteira.Text))
        {
            int idCarteira = Convert.ToInt32(hiddenCarteira.Text);
            ClassesOffShoreQuery classeOffShoreQuery = new ClassesOffShoreQuery("classeOffShore");
            SeriesOffShoreQuery seriesOffShoreQuery = new SeriesOffShoreQuery("seriesOffShore");
            
            seriesOffShoreQuery.InnerJoin(classeOffShoreQuery).On(classeOffShoreQuery.IdClassesOffShore.Equal(seriesOffShoreQuery.IdClassesOffShore));
            seriesOffShoreQuery.Where(classeOffShoreQuery.IdClassesOffShore.Equal(idCarteira));
            seriesOffShoreQuery.OrderBy(seriesOffShoreQuery.IdSeriesOffShore.Descending);

            coll.Load(seriesOffShoreQuery);            
        }

        e.Collection = coll;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSCotista_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CotistaCollection coll = new CotistaCollection();
        coll.BuscaCotistasComAcesso(Context.User.Identity.Name);

        e.Collection = coll;
    }
    #endregion

    #region GridCadastro
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        TransferenciaSerie transferenciaSerie = new TransferenciaSerie();

        int IdTransferenciaSerie = (int)e.Keys[0];

        if (transferenciaSerie.LoadByPrimaryKey(IdTransferenciaSerie))
        {
            ASPxTextBox hiddenSerieDestino = gridCadastro.FindEditFormTemplateControl("hiddenSerieDestino") as ASPxTextBox;

            transferenciaSerie.IdSerieDestino = Convert.ToInt32(hiddenSerieDestino.Text);
            transferenciaSerie.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de TransferenciaSerie - Operacao: Update TransferenciaSerie: " + IdTransferenciaSerie + UtilitarioWeb.ToString(transferenciaSerie),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdTransferenciaSerie");
            for (int i = 0; i < keyValuesId.Count; i++)
            {                
                TransferenciaSerie transferenciaSerie = new TransferenciaSerie();
                int IdTransferenciaSerie = Convert.ToInt32(keyValuesId[i]);
                
                if (transferenciaSerie.LoadByPrimaryKey(IdTransferenciaSerie))
                {
                    TransferenciaSerie transferenciaSerieClone = (TransferenciaSerie)Utilitario.Clone(transferenciaSerie);
                    //
                    transferenciaSerie.MarkAsDeleted();
                    transferenciaSerie.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de TransferenciaSerie - Operacao: Delete TransferenciaSerie: " + IdTransferenciaSerie + UtilitarioWeb.ToString(transferenciaSerieClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        base.gridCadastro_CellEditorInitialize(sender, e);

        if (!gridCadastro.IsNewRowEditing)
        {
            if (e.Column.FieldName == TransferenciaSerieMetadata.ColumnNames.IdTransferenciaSerie)
            {
                e.Editor.BackColor = Color.FromName("#EBEBEB");
                e.Editor.ReadOnly = true;
            }
        }
        else
        { // Modo Novo
            if (e.Column.FieldName == TransferenciaSerieMetadata.ColumnNames.IdTransferenciaSerie)
            {
                e.Editor.BackColor = Color.FromName("#EBEBEB");
                e.Editor.ReadOnly = true;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e)
    {
        this.FocaCampoGrid("textDescricao");
        base.gridCadastro_PreRender(sender, e);
    }
    #endregion

    #region GridSerie
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridSerieOffShore_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        this.gridSerieOffShore.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridSerieOffShore_CustomDataCallback(object sender, ASPxGridViewCustomDataCallbackEventArgs e)
    {
        ASPxGridView gridView = sender as ASPxGridView;
        int visibleIndex = Convert.ToInt32(e.Parameters);
        int idSeriesOffShore = (int)gridView.GetRowValues(visibleIndex, SeriesOffShoreMetadata.ColumnNames.IdSeriesOffShore);
        e.Result = idSeriesOffShore.ToString();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridSerieOffShore_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        UtilitarioGrid.SetaCoresGrid(e);
    }
    #endregion

    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    private void SalvarNovo()
    {
        ASPxTextBox hiddenCarteira = gridCadastro.FindEditFormTemplateControl("hiddenCarteira") as ASPxTextBox;
        ASPxTextBox hiddenSerieOrigem = gridCadastro.FindEditFormTemplateControl("hiddenSerieOrigem") as ASPxTextBox;
        ASPxTextBox hiddenSerieDestino = gridCadastro.FindEditFormTemplateControl("hiddenSerieDestino") as ASPxTextBox;
        ASPxGridLookup dropPosicaoCotista = gridCadastro.FindEditFormTemplateControl("dropPosicaoCotista") as ASPxGridLookup;
        ASPxDateEdit textDataExecucao = gridCadastro.FindEditFormTemplateControl("textDataExecucao") as ASPxDateEdit;
        TransferenciaSerieCollection transferenciaSerieColl = new TransferenciaSerieCollection();

        string[] strPosicoes = dropPosicaoCotista.Text.Split(',');
        foreach (string strPosicao in strPosicoes)
        {
            TransferenciaSerie transferenciaSerie = transferenciaSerieColl.AddNew();
            transferenciaSerie.DataPosicao = Convert.ToDateTime(textDataExecucao.Text);
            transferenciaSerie.IdCarteira = Convert.ToInt32(hiddenCarteira.Text);
            transferenciaSerie.IdPosicao = Convert.ToInt32(strPosicao.Trim());
            transferenciaSerie.IdSerieDestino = Convert.ToInt32(hiddenSerieDestino.Text);
            transferenciaSerie.IdSerieOrigem = Convert.ToInt32(hiddenSerieOrigem.Text);
            transferenciaSerie.DataPosicao = Convert.ToDateTime(textDataExecucao.Text);
            transferenciaSerie.DataExecucao = transferenciaSerie.DataPosicao;
        }

        transferenciaSerieColl.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de TransferenciaSerie - Operacao: Insert TransferenciaSerie, Lote Posições" + dropPosicaoCotista.Text,
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    #region CallBacks
    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
        ASPxTextBox hiddenCarteira = gridCadastro.FindEditFormTemplateControl("hiddenCarteira") as ASPxTextBox;
        ASPxTextBox hiddenSerieOrigem = gridCadastro.FindEditFormTemplateControl("hiddenSerieOrigem") as ASPxTextBox;
        ASPxTextBox hiddenSerieDestino = gridCadastro.FindEditFormTemplateControl("hiddenSerieDestino") as ASPxTextBox;
        ASPxGridLookup dropPosicaoCotista = gridCadastro.FindEditFormTemplateControl("dropPosicaoCotista") as ASPxGridLookup;
        ASPxDateEdit textDataExecucao = gridCadastro.FindEditFormTemplateControl("textDataExecucao") as ASPxDateEdit;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(hiddenCarteira);
        controles.Add(hiddenSerieOrigem);
        controles.Add(hiddenSerieDestino);        
        controles.Add(textDataExecucao);

        if (gridCadastro.IsNewRowEditing)
            controles.Add(dropPosicaoCotista);

        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        #region Valida Séries
        if (Convert.ToInt32(hiddenSerieOrigem.Text) == Convert.ToInt32(hiddenSerieDestino.Text))
        {
            e.Result = "Série Destino não pode ser igual a série Origem!";
            return;
        }
        #endregion 

        if (gridCadastro.IsNewRowEditing)
        {
            #region Valida transacoes x posicoes do mesmo dia
            string[] strPosicoes = dropPosicaoCotista.Text.Split(',');
            List<int> idPosicoes = new List<int>();

            foreach(string idPosicao in strPosicoes)
                idPosicoes.Add(Convert.ToInt32(idPosicao.Trim()));

            TransferenciaSerieCollection coll = new TransferenciaSerieCollection();
            coll.Query.Where(coll.Query.IdPosicao.In(idPosicoes.ToArray()) & coll.Query.DataPosicao.Equal(Convert.ToDateTime(textDataExecucao.Text)));

            if(coll.Query.Load())
            {
                StringBuilder strRetorno = new StringBuilder();
                strRetorno.Append("Já existe cadastro de transferência de série do dia "+ Convert.ToDateTime(textDataExecucao.Text) + " para as posições: ");

                foreach(TransferenciaSerie transferencia in coll)
                {
                    strRetorno.Append(transferencia.IdPosicao.Value).Append(",");
                }
                e.Result = strRetorno.ToString().Remove(strRetorno.Length - 1) + " !";
                return;
            }
            #endregion

        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();

        e.Result = "";
        if (e.Parameter == "clone")
        {
            //e.Result = 0;
        }
        else
        {
            e.Result = "Cadastro feito com sucesso.";
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callBackCarteira_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        Carteira carteira = new Carteira();
        Cliente cliente = new Cliente();

        string nome = "";
        string resultado = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {            
            int idCarteira = Convert.ToInt32(e.Parameter);
            if (cliente.LoadByPrimaryKey(idCarteira) && (cliente.IdTipo.Equals((int)TipoClienteFixo.OffShore_PF) || cliente.IdTipo.Equals((int)TipoClienteFixo.OffShore_PJ)))
            {
                carteira.LoadByPrimaryKey(idCarteira);    
                if (cliente.Status.Value != (byte)StatusCliente.Divulgado)
                {
                    if (carteira.IsAtivo)
                    {
                        //O permissionamento da carteira é dado direto no cliente vinculado à carteira
                        PermissaoCliente permissaoCliente = new PermissaoCliente();
                        if (permissaoCliente.RetornaAcessoCliente(idCarteira, HttpContext.Current.User.Identity.Name))
                        {
                            nome = carteira.str.Apelido;
                            resultado = nome + "|" + cliente.DataDia.ToString().Substring(0, 10) + "|" + Thread.CurrentThread.CurrentCulture.Name + "|" + idCarteira;
                        }
                        else
                        {
                            resultado = "no_access";
                        }
                    }
                    else
                    {
                        resultado = "no_active";
                    }
                }
                else
                {
                    resultado = cliente.str.Apelido + "|status_closed";
                }
            }
        }

        e.Result = resultado;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callBackCotista_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCotista = Convert.ToInt32(e.Parameter);
            Cotista cotista = new Cotista();
            cotista.LoadByPrimaryKey(idCotista);

            if (cotista.str.Apelido != "")
            {
                if (cotista.IsAtivo)
                {
                    PermissaoCotista permissaoCotista = new PermissaoCotista();
                    if (permissaoCotista.RetornaAcessoCotista(idCotista, HttpContext.Current.User.Identity.Name))
                    {
                        nome = cotista.str.Apelido + "|" + cotista.IdCotista.Value;
                    }
                    else
                    {
                        nome = "no_access";
                    }
                }
                else
                {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callBackAtualizaFiltro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        ASPxTextBox hiddenCarteira = gridCadastro.FindEditFormTemplateControl("hiddenCarteira") as ASPxTextBox;
        ASPxTextBox hiddenSerieOrigem = gridCadastro.FindEditFormTemplateControl("hiddenSerieOrigem") as ASPxTextBox;
        ASPxTextBox hiddenCotista = gridCadastro.FindEditFormTemplateControl("hiddenCotista") as ASPxTextBox;
        ASPxDateEdit textDataExecucao = gridCadastro.FindEditFormTemplateControl("textDataExecucao") as ASPxDateEdit;
        
        if (!string.IsNullOrEmpty(hiddenCarteira.Text))
            idCarteiraPosicao = Convert.ToInt32(hiddenCarteira.Text);
        else
            idCarteiraPosicao = 0;

        if (!string.IsNullOrEmpty(hiddenSerieOrigem.Text))
            idSerieOrigem = Convert.ToInt32(hiddenSerieOrigem.Text);
        else
            idSerieOrigem = 0;

        if (!string.IsNullOrEmpty(hiddenCotista.Text))
            idCotistaPosicao = Convert.ToInt32(hiddenCotista.Text);
        else
            idCotistaPosicao = 0;

        if (!string.IsNullOrEmpty(textDataExecucao.Text))
            dataExecucao = Convert.ToDateTime(textDataExecucao.Text);
        else
            dataExecucao = new DateTime();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callBackPopupSerieOffShore_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = string.Empty;
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {             
            SeriesOffShore seriesOffShore = new SeriesOffShore();
            if (seriesOffShore.LoadByPrimaryKey(Convert.ToInt32(e.Parameter)))
                e.Result = e.Parameter + " - " + seriesOffShore.Descricao;
        }
    }
    #endregion

    #region Load
    protected void dropPosicaoCotista_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
        {
            (sender as ASPxGridLookup).ClientVisible = false;
        }
    }

    protected void labelPosicoes_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
        {
            (sender as Label).Visible = false;
        }
    }

    protected void labelPosicao_Load(object sender, EventArgs e)
    {
        if (gridCadastro.IsNewRowEditing)
        {
            (sender as Label).Visible = false;
        }
        else
        {
            (sender as Label).Visible = true;
        }
    }

    protected void textPosicao_Load(object sender, EventArgs e)
    {
        if (gridCadastro.IsNewRowEditing)
        {
            (sender as TextBox).Visible = false;
        }
        else
        {
            (sender as TextBox).Visible = true;
        }
    }

    protected void btnEditCodigoCarteira_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
        {
            (sender as ASPxSpinEdit).Enabled = false;
        }
    }

    protected void btnEditCodigoCotista_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
        {
            (sender as ASPxSpinEdit).Enabled = false;
        }
    }

    protected void textDataExecucao_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
        {
            (sender as ASPxDateEdit).Enabled = false;
        }
    }

    protected void btnEditSerieOrigem_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
        {
            (sender as ASPxButtonEdit).ClientEnabled = false;
        }
    }
    #endregion

    #region Outros
    private PosicaoCotistaAberturaCollection RetornaPosicao(int idCarteira, int idCotista, int idSerie, DateTime dataExecucao)
    { 
        PosicaoCotistaAberturaQuery posicaoQuery = new PosicaoCotistaAberturaQuery("posicao");
        CotistaQuery cotistaQuery = new CotistaQuery("cotista");

        PosicaoCotistaAberturaCollection coll = new PosicaoCotistaAberturaCollection();
        posicaoQuery.Select(posicaoQuery.IdPosicao,
                            posicaoQuery.DataAplicacao,
                            posicaoQuery.DataConversao,
                            posicaoQuery.ValorLiquido,
                            posicaoQuery.IdCotista,
                            posicaoQuery.CotaAplicacao,
                            posicaoQuery.Quantidade,
                            cotistaQuery.Nome);
        posicaoQuery.InnerJoin(cotistaQuery).On(cotistaQuery.IdCotista.Equal(posicaoQuery.IdCotista));
        posicaoQuery.Where(posicaoQuery.IdCarteira.Equal(idCarteiraPosicao)
                         & posicaoQuery.IdSeriesOffShore.Equal(idSerieOrigem)
                         & posicaoQuery.DataHistorico.Equal(dataExecucao));

        if (idCotistaPosicao != 0)
            posicaoQuery.Where(posicaoQuery.IdCotista.Equal(idCotistaPosicao));

        posicaoQuery.OrderBy(posicaoQuery.IdSeriesOffShore.Descending);
        coll.Load(posicaoQuery);

        return coll;
    }
    #endregion
}