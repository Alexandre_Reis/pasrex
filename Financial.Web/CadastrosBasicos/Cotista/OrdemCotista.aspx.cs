﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using Financial.Investidor;
using Financial.ContaCorrente;
using System.Drawing;
using Financial.ContaCorrente.Enums;
using Financial.Security;
using Financial.InvestidorCotista;
using Financial.Investidor.Enums;
using Financial.Common.Enums;
using Financial.InvestidorCotista.Enums;
using Financial.Web.Common;
using System.Threading;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Fundo;
using Financial.Fundo.Enums;
using DevExpress.Web.Data;
using System.Web.Configuration;
using System.Net.Configuration;
using System.Net.Mail;
using Financial.Export;
using System.Xml;
using Financial.Interfaces.Export;
using Financial.InvestidorCotista.Perfil;
using Financial.CRM;

using Newtonsoft.Json;
using System.Web.Configuration;
using System.Net.Configuration;

public partial class CadastrosBasicos_OrdemCotista : Financial.Web.Common.CadastroBasePage
{
    SuitabilityPerfilInvestidor suitabilityPerfilCotista = new SuitabilityPerfilInvestidor();
    SuitabilityPerfilInvestidor suitabilityPerfilPosicao = new SuitabilityPerfilInvestidor();

    private class Datas
    {
        public DateTime dataOperacao;
        public DateTime dataConversao;
        public DateTime dataLiquidacao;
    }

    bool aprovacao = false;

    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasFiltro = true;
        this.HasPopupCotista = true;
        this.HasPopupCarteira = true;
        this.HasPanelFieldsLoading = true;
        base.Page_Load(sender, e);

        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;
        GrupoUsuario grupoUsuario = new GrupoUsuario();
        grupoUsuario.LoadByPrimaryKey(usuario.IdGrupo.Value);

        aprovacao = grupoUsuario.Front == (byte)PermissaoFront.Aprovacao;

        if (!aprovacao)
        {
            btnAprova.Visible = false;
            btnCancela.Visible = false;
            btnFilter.Visible = false;
        }

        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
                  new List<string>(new string[] { OrdemCotistaMetadata.ColumnNames.TipoOperacao }));
    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSOrdemCotista_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        PermissaoCotistaQuery permissaoCotistaQuery = new PermissaoCotistaQuery("P2");
        OrdemCotistaQuery ordemCotistaQuery = new OrdemCotistaQuery("O");
        CotistaQuery cotistaQuery = new CotistaQuery("C");
        CarteiraQuery carteiraQuery = new CarteiraQuery("A");

        ordemCotistaQuery.Select(ordemCotistaQuery, cotistaQuery.Apelido.As("ApelidoCotista"),
                                 carteiraQuery.Apelido.As("ApelidoCarteira"));
        ordemCotistaQuery.InnerJoin(cotistaQuery).On(ordemCotistaQuery.IdCotista == cotistaQuery.IdCotista);
        ordemCotistaQuery.LeftJoin(carteiraQuery).On(ordemCotistaQuery.IdCarteira == carteiraQuery.IdCarteira);
        ordemCotistaQuery.InnerJoin(permissaoCotistaQuery).On(permissaoCotistaQuery.IdCotista == cotistaQuery.IdCotista);
        ordemCotistaQuery.Where(permissaoCotistaQuery.IdUsuario.Equal(idUsuario));

        if (!String.IsNullOrEmpty(btnEditCodigoCarteiraFiltro.Text))
        {
            ordemCotistaQuery.Where(ordemCotistaQuery.IdCarteira.Equal(Convert.ToInt32(btnEditCodigoCarteiraFiltro.Text)));
        }

        if (!String.IsNullOrEmpty(textDataInicio.Text))
        {
            ordemCotistaQuery.Where(ordemCotistaQuery.DataOperacao.GreaterThanOrEqual(textDataInicio.Text));
        }

        if (!String.IsNullOrEmpty(textDataFim.Text))
        {
            ordemCotistaQuery.Where(ordemCotistaQuery.DataOperacao.LessThanOrEqual(textDataFim.Text));
        }

        ordemCotistaQuery.OrderBy(ordemCotistaQuery.IdOperacao.Descending);

        OrdemCotistaCollection coll = new OrdemCotistaCollection();
        coll.Load(ordemCotistaQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCotista_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CotistaCollection coll = new CotistaCollection();
        coll.BuscaCotistasComAcesso(Context.User.Identity.Name);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CarteiraCollection coll = new CarteiraCollection();
        coll.BuscaCarteirasFundosClubesBoletagem(Context.User.Identity.Name);
        
        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSFormaLiquidacao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        FormaLiquidacaoCollection coll = new FormaLiquidacaoCollection();
        coll.Query.OrderBy(coll.Query.IdFormaLiquidacao.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSContaCorrente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ASPxSpinEdit btnEditCodigoCotista = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCotista") as ASPxSpinEdit;

        if (btnEditCodigoCotista != null && btnEditCodigoCotista.Text != "")
        {
            int idCotista = Convert.ToInt32(btnEditCodigoCotista.Text);
            Cotista cotista = new Cotista();
            cotista.LoadByPrimaryKey(idCotista);
            ContaCorrenteCollection coll = new ContaCorrenteCollection();
            coll.Query.Where(coll.Query.IdPessoa.Equal(cotista.IdPessoa));
            coll.Query.OrderBy(coll.Query.Numero.Ascending);
            coll.Query.Load();

            // Assign the esDataSourcSelectEvenArgs Collection property
            e.Collection = coll;
        }
        else
        {
            // Assign the esDataSourcSelectEvenArgs Collection property
            e.Collection = new ContaCorrenteCollection();
        }
    }
    #endregion

    new protected void panelEdicao_Load(object sender, EventArgs e)
    {
        base.panelEdicao_Load(sender, e);

        bool travaPainel = false;
        if (gridCadastro.IsEditing && !gridCadastro.IsNewRowEditing)
        {
            Label labelEdicao = gridCadastro.FindEditFormTemplateControl("labelEdicao") as Label;

            byte status = Convert.ToByte(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "Status"));

            if (!aprovacao)
            {
                if ((byte)status == (byte)StatusOrdemFundo.Aprovado)
                {
                    labelEdicao.Text = "Ordem está aprovada! Dados não podem ser alterados.";
                    travaPainel = true;
                }
                else if ((byte)status == (byte)StatusOrdemFundo.Cancelado)
                {
                    labelEdicao.Text = "Ordem está cancelada! Dados não podem ser alterados.";
                    travaPainel = true;
                }
            }

            else if ((byte)status == (byte)StatusOrdemFundo.Processado)
            {
                labelEdicao.Text = "Ordem está processada! Dados não podem ser alterados.";
                travaPainel = true;
            }
        }

        if (travaPainel)
        {
            LinkButton btnOK = gridCadastro.FindEditFormTemplateControl("btnOK") as LinkButton;
            btnOK.Visible = false;
        }
    }

    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        TimeSpan horaLimite = DateTime.Now.TimeOfDay;

        TabelaParametrosFrontCollection tabelaParametrosFrontCollection = new TabelaParametrosFrontCollection();
        tabelaParametrosFrontCollection.LoadAll();

        if (tabelaParametrosFrontCollection.Count > 0)
        {
            if (tabelaParametrosFrontCollection[0].HorarioFimCotista.HasValue)
            {
                if (horaLimite > tabelaParametrosFrontCollection[0].HorarioFimCotista.Value.TimeOfDay)
                {
                    e.Result = "Fora do Horário permitido. Operação não pode ser realizada.";
                    return;
                }
            }
        }

        if (!gridCadastro.IsEditing) //Testa deleção
        {
            List<object> keyId = gridCadastro.GetSelectedFieldValues("IdOperacao");
            List<object> keyStatus = gridCadastro.GetSelectedFieldValues("Status");

            for (int i = 0; i < keyId.Count; i++)
            {
                int idOperacao = Convert.ToInt32(keyId[i]);
                byte status = Convert.ToByte(keyStatus[i]);

                if (e.Parameter == "deletar")
                {
                    if (!aprovacao)
                    {
                        if ((byte)status == (byte)StatusOrdemFundo.Aprovado)
                        {
                            e.Result = "Ordem " + idOperacao + " está aprovada! Operação não pode ser realizada.";
                            return;
                        }
                        else if ((byte)status == (byte)StatusOrdemFundo.Cancelado)
                        {
                            e.Result = "Ordem " + idOperacao + " está cancelada! Operação não pode ser realizada.";
                            return;
                        }
                    }
                }
                else if (e.Parameter == "aprovar" || e.Parameter == "cancelar")
                {
                    if ((byte)status == (byte)StatusOrdemFundo.Aprovado)
                    {
                        e.Result = "Ordem " + idOperacao + " está aprovada! Operação não pode ser realizada.";
                        return;
                    }
                    else if ((byte)status == (byte)StatusOrdemFundo.Cancelado)
                    {
                        e.Result = "Ordem " + idOperacao + " está cancelada! Operação não pode ser realizada.";
                        return;
                    }
                }

                if ((byte)status == (byte)StatusOrdemFundo.Processado)
                {
                    e.Result = "Ordem " + idOperacao + " está processada! Operação não pode ser realizada.";
                    return;
                }
            }
        }
        else
        {
            ASPxComboBox dropTipoOperacao = gridCadastro.FindEditFormTemplateControl("dropTipoOperacao") as ASPxComboBox;
            ASPxSpinEdit btnEditCodigoCotista = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCotista") as ASPxSpinEdit;
            ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
            ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
            ASPxComboBox dropConta = gridCadastro.FindEditFormTemplateControl("dropConta") as ASPxComboBox;

            #region Campos obrigatórios
            List<Control> controles = new List<Control>();
            controles.Add(btnEditCodigoCotista);
            controles.Add(btnEditCodigoCarteira);
            controles.Add(dropTipoOperacao);
            
            if (base.TestaObrigatorio(controles) != "")
            {
                e.Result = "Campos com * são obrigatórios!";
                return;
            }
            #endregion

            byte tipoOperacao = Convert.ToByte(dropTipoOperacao.SelectedItem.Value);

            if (tipoOperacao != (byte)TipoOperacaoCotista.ResgateTotal && textValor.Text == "")
            {
                e.Result = "Para operação diferente de resgate total, é preciso informar o campo de Valor/Qtde.";
                return;
            }

            //if (tipoOperacao != (byte)TipoOperacaoCotista.Aplicacao && dropConta.SelectedIndex == -1)
            //{
            //    e.Result = "Para qualquer tipo de resgate, é preciso informar o campo de conta corrente.";
            //    return;
            //}

            int idCotista = Convert.ToInt32(btnEditCodigoCotista.Text);
                        
            int idCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);

            PosicaoCotista posicaoCotista = new PosicaoCotista();
            posicaoCotista.Query.Select(posicaoCotista.Query.ValorBruto.Sum(), posicaoCotista.Query.Quantidade.Sum());
            posicaoCotista.Query.Where(posicaoCotista.Query.IdCotista.Equal(idCotista),
                                       posicaoCotista.Query.IdCarteira.Equal(idCarteira));
            posicaoCotista.Query.Load();

            decimal saldoBruto = 0;
            decimal quantidadeCotas = 0;
            if (posicaoCotista.ValorBruto.HasValue)
            {
                saldoBruto = posicaoCotista.ValorBruto.Value;
                quantidadeCotas = posicaoCotista.Quantidade.Value;
            }

            if (tipoOperacao == (byte)TipoOperacaoCotista.ResgateBruto ||
            tipoOperacao == (byte)TipoOperacaoCotista.ResgateCotas ||
            tipoOperacao == (byte)TipoOperacaoCotista.ResgateLiquido ||
            tipoOperacao == (byte)TipoOperacaoCotista.ResgateTotal)
            {
                if (quantidadeCotas <= 0)
                {
                    e.Result = "O cliente não possui saldo de aplicação neste fundo para resgate.";
                    return;
                }
            }

            if (tipoOperacao == (byte)TipoOperacaoCotista.Aplicacao)
            {
                Carteira carteira = new Carteira();
                carteira.LoadByPrimaryKey(idCarteira);

                decimal valor = Convert.ToDecimal(textValor.Text);
                if (quantidadeCotas == 0 && valor < carteira.ValorMinimoInicial.Value)
                {
                    e.Result = "Operação com valor inferior ao valor mínimo inicial (" + carteira.ValorMinimoInicial.Value.ToString() + ") . Operação não pode ser realizada";
                    return;
                }
                else if (quantidadeCotas != 0 && valor < carteira.ValorMinimoAplicacao.Value)
                {
                    e.Result = "Operação com valor inferior ao valor de aplicação exigido (" + carteira.ValorMinimoAplicacao.Value.ToString() + ") . Operação não pode ser realizada";
                    return;
                }
            }

            SuitabilityParametrosWorkflow suitabilityParametrosWorkflow = new SuitabilityParametrosWorkflow();
            suitabilityParametrosWorkflow.LoadByPrimaryKey(1);

            if (suitabilityParametrosWorkflow.AtivaWorkFlow.Equals("S"))
            {
                //Verifica Suitability
                e.Result = verificaSuitability(idCotista, idCarteira, tipoOperacao, DateTime.Now);

                if (e.Result == "")
                {
                    ControllerPerfilCotista controllerPerfilCotista = new ControllerPerfilCotista();

                    //ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
                    decimal valorOperacaoParam = Convert.ToDecimal(textValor.Text);

                    SuitabilityPerfilInvestidor suitabilityPerfilCotistaOut;
                    SuitabilityPerfilInvestidor suitabilityPerfilPosicaoOut;
                    bool desenquadrado;

                    e.Result = controllerPerfilCotista.verificaEnquadramentoSuitability(idCotista, idCarteira, tipoOperacao, valorOperacaoParam, out suitabilityPerfilPosicaoOut, out suitabilityPerfilCotistaOut,
                        out desenquadrado);

                    suitabilityPerfilCotista = suitabilityPerfilCotistaOut;
                    suitabilityPerfilPosicaoOut = suitabilityPerfilPosicaoOut;

                }
            }

            
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dropConta_Callback(object source, CallbackEventArgsBase e)
    {
        ASPxComboBox dropConta = source as ASPxComboBox;
        dropConta.DataBind();
    }

    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCotista = Convert.ToInt32(e.Parameter);
            Cotista cotista = new Cotista();
            cotista.LoadByPrimaryKey(idCotista);

            if (cotista.str.Apelido != "")
            {
                if (cotista.IsAtivo)
                {
                    PermissaoCotista permissaoCotista = new PermissaoCotista();
                    if (permissaoCotista.RetornaAcessoCotista(idCotista, HttpContext.Current.User.Identity.Name))
                    {
                        nome = cotista.str.Apelido;
                    }
                    else
                    {
                        nome = "no_access";
                    }
                }
                else
                {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string nome = "";
        string resultado = "";
        DateTime dataOperacao = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            Carteira carteira = new Carteira();

            int idCarteira = Convert.ToInt32(e.Parameter);
            if (carteira.LoadByPrimaryKey(idCarteira))
            {
                if (carteira.IsAtivo)
                {
                    //O permissionamento da carteira é dado direto no cliente vinculado à carteira
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoCliente(idCarteira, HttpContext.Current.User.Identity.Name))
                    {
                        nome = carteira.str.Apelido;
                        resultado = nome;
                    }
                    else
                    {
                        resultado = "no_access";
                    }
                }
                else
                {
                    resultado = "no_active";
                }
            }
        }

        e.Result = resultado;
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        int idOperacao = (int)e.Keys[0];
        ASPxSpinEdit btnEditCodigoCotista = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCotista") as ASPxSpinEdit;
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxComboBox dropTipoOperacao = gridCadastro.FindEditFormTemplateControl("dropTipoOperacao") as ASPxComboBox;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
        ASPxComboBox dropConta = gridCadastro.FindEditFormTemplateControl("dropConta") as ASPxComboBox;
        ASPxComboBox dropTrader = gridCadastro.FindEditFormTemplateControl("dropTrader") as ASPxComboBox;
        TextBox textObservacao = gridCadastro.FindEditFormTemplateControl("textObservacao") as TextBox;

        OrdemCotista ordemCotista = new OrdemCotista();
        if (ordemCotista.LoadByPrimaryKey(idOperacao))
        {
            int idCotista = Convert.ToInt32(btnEditCodigoCotista.Text);

            ordemCotista.IdCotista = idCotista;
            ordemCotista.IdCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);
            ordemCotista.TipoOperacao = Convert.ToByte(dropTipoOperacao.SelectedItem.Value);

            ordemCotista.DataOperacao = this.RetornaDatas(ordemCotista.IdCarteira.Value, ordemCotista.TipoOperacao.Value).dataOperacao;
            ordemCotista.DataConversao = this.RetornaDatas(ordemCotista.IdCarteira.Value, ordemCotista.TipoOperacao.Value).dataConversao;
            ordemCotista.DataLiquidacao = this.RetornaDatas(ordemCotista.IdCarteira.Value, ordemCotista.TipoOperacao.Value).dataLiquidacao;

            ordemCotista.DataAgendamento = ordemCotista.DataOperacao;
            ordemCotista.TipoResgate =  ConfigurationManager.AppSettings["Cliente"] == "PORTOPAR" ? (byte)TipoResgateCotista.FIFO :
                (byte)TipoResgateCotista.MenorImposto;

            if (dropTrader.SelectedIndex > -1)
                ordemCotista.IdTrader = Convert.ToInt32(dropTrader.SelectedItem.Value);
            else
                ordemCotista.IdTrader = null;

            //Salva o valor dependendo do tipo de operação, o resto força zerado
            //Resgate total força tudo zerado!
            ordemCotista.ValorBruto = 0;
            ordemCotista.ValorLiquido = 0;
            ordemCotista.Quantidade = 0;
            switch (ordemCotista.TipoOperacao)
            {
                case (byte)TipoOperacaoCotista.Aplicacao:
                case (byte)TipoOperacaoCotista.ResgateBruto:
                    ordemCotista.ValorBruto = Convert.ToDecimal(textValor.Text);
                    break;
                case (byte)TipoOperacaoCotista.ResgateCotas:
                    ordemCotista.Quantidade = Convert.ToDecimal(textValor.Text);
                    break;
                case (byte)TipoOperacaoCotista.ResgateLiquido:
                    ordemCotista.ValorLiquido = Convert.ToDecimal(textValor.Text);
                    break;
            }

            ordemCotista.Observacao = textObservacao.Text.ToString();

            if (dropConta.SelectedIndex != -1)
            {
                ordemCotista.IdConta = Convert.ToInt32(dropConta.SelectedItem.Value);
            }

            ordemCotista.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de OrdemCotista - Operacao: Update OrdemCotista: " + idOperacao + UtilitarioWeb.ToString(ordemCotista),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    private void SalvarNovo()
    {
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxSpinEdit btnEditCodigoCotista = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCotista") as ASPxSpinEdit;
        ASPxComboBox dropTipoOperacao = gridCadastro.FindEditFormTemplateControl("dropTipoOperacao") as ASPxComboBox;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
        ASPxComboBox dropConta = gridCadastro.FindEditFormTemplateControl("dropConta") as ASPxComboBox;
        ASPxComboBox dropTrader = gridCadastro.FindEditFormTemplateControl("dropTrader") as ASPxComboBox;
        TextBox textObservacao = gridCadastro.FindEditFormTemplateControl("textObservacao") as TextBox;

        OrdemCotista ordemCotista = new OrdemCotista();

        int idCotista = Convert.ToInt32(btnEditCodigoCotista.Text);

        ordemCotista.IdCotista = idCotista;
        ordemCotista.IdCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);
        ordemCotista.TipoOperacao = Convert.ToByte(dropTipoOperacao.SelectedItem.Value);

        ordemCotista.DataOperacao = this.RetornaDatas(ordemCotista.IdCarteira.Value, ordemCotista.TipoOperacao.Value).dataOperacao;
        ordemCotista.DataConversao = this.RetornaDatas(ordemCotista.IdCarteira.Value, ordemCotista.TipoOperacao.Value).dataConversao;
        ordemCotista.DataLiquidacao = this.RetornaDatas(ordemCotista.IdCarteira.Value, ordemCotista.TipoOperacao.Value).dataLiquidacao;

        ordemCotista.DataAgendamento = ordemCotista.DataOperacao;

        ordemCotista.TipoResgate = ConfigurationManager.AppSettings["Cliente"] == "PORTOPAR" ? (byte)TipoResgateCotista.FIFO :
                (byte)TipoResgateCotista.MenorImposto;

        if (dropTrader.SelectedIndex > -1)
            ordemCotista.IdTrader = Convert.ToInt32(dropTrader.SelectedItem.Value);
        else
            ordemCotista.IdTrader = null;

        //Salva o valor dependendo do tipo de operação, o resto força zerado
        //Resgate total força tudo zerado!
        ordemCotista.ValorBruto = 0;
        ordemCotista.ValorLiquido = 0;
        ordemCotista.Quantidade = 0;
        switch (ordemCotista.TipoOperacao)
        {
            case (byte)TipoOperacaoCotista.Aplicacao:
            case (byte)TipoOperacaoCotista.ResgateBruto:
                ordemCotista.ValorBruto = Convert.ToDecimal(textValor.Text);
                break;
            case (byte)TipoOperacaoCotista.ResgateCotas:
                ordemCotista.Quantidade = Convert.ToDecimal(textValor.Text);
                break;
            case (byte)TipoOperacaoCotista.ResgateLiquido:
                ordemCotista.ValorLiquido = Convert.ToDecimal(textValor.Text);
                break;
        }

        ordemCotista.IdFormaLiquidacao = 1; //TODO HOJE FIXO

        ordemCotista.Observacao = textObservacao.Text.ToString();

        if (aprovacao)
        {
            ordemCotista.Status = (byte)StatusOrdemFundo.Aprovado;
        }
        else
        {
            ordemCotista.Status = (byte)StatusOrdemFundo.Digitado;
        }
        
        if (dropConta.SelectedIndex != -1)
        {
            ordemCotista.IdConta = Convert.ToInt32(dropConta.SelectedItem.Value);
        }

        ordemCotista.Save();

        #region ARQEOP_XML
        bool integraCotistaItau = WebConfigurationManager.AppSettings["IntegraCotistaItau"] != null ?
            Convert.ToBoolean(WebConfigurationManager.AppSettings["IntegraCotistaItau"]) : false;
        if (integraCotistaItau)
        {
            /*ordemCotista.StatusIntegracao = (byte)StatusOrdemIntegracao.Agendado;
            ordemCotista.Save();

            Itau itau = new Itau();
            itau.EnviaEop_XML(ordemCotista);*/

            ordemCotista.IntegraItau(0);
        }
        #endregion

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de OrdemCotista - Operacao: Insert OrdemCotista: " + ordemCotista.IdOperacao + UtilitarioWeb.ToString(ordemCotista),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        this.EnviaEmail(ordemCotista, true);
    }

    private void EnviaEmail(OrdemCotista ordemCotista, bool emailGeral)
    {
        string body = "";

        Configuration config = WebConfigurationManager.OpenWebConfiguration(HttpContext.Current.Request.ApplicationPath);
        MailSettingsSectionGroup settings = (MailSettingsSectionGroup)config.GetSectionGroup("system.net/mailSettings");

        string from = settings.Smtp.From.ToString();

        Usuario usuario = new Usuario();
        if (!usuario.BuscaUsuario(Context.User.Identity.Name))
        {
            return;
        }

        
        TabelaParametrosFrontCollection tabelaParametrosFrontCollection = new TabelaParametrosFrontCollection();
        tabelaParametrosFrontCollection.LoadAll();

        Cliente clienteFundo = new Cliente();
        clienteFundo.LoadByPrimaryKey(ordemCotista.IdCarteira.Value);

        string to = "";
        if (emailGeral)
        {
            if (tabelaParametrosFrontCollection.Count > 0 && !String.IsNullOrEmpty(tabelaParametrosFrontCollection[0].EmailCotista))
            {
                if (!String.IsNullOrEmpty(to))
                {
                    to = to + ",";
                }

                to = to + tabelaParametrosFrontCollection[0].EmailCotista;
            }
        }
        else
        {
            #region Pega as informações de BookEmail
            string listaEmail = !String.IsNullOrEmpty(clienteFundo.BookEmail) ? clienteFundo.BookEmail.ToString() : "";
            if (listaEmail != "")
            {
                to = to + listaEmail;
            }
            #endregion
        }

        if (String.IsNullOrEmpty(to.Trim()))
            return;

        Cotista cotista = new Cotista();
        cotista.LoadByPrimaryKey(ordemCotista.IdCotista.Value);

        body += "Data: " + ordemCotista.DataOperacao.Value.ToShortDateString() + "\n";

        if (ParametrosConfiguracaoSistema.Fundo.DistribuidorPadrao.HasValue)
        {
            int idDistribuidor = Convert.ToInt32(ParametrosConfiguracaoSistema.Fundo.DistribuidorPadrao.Value);
            AgenteMercado agenteMercado = new AgenteMercado();
            agenteMercado.LoadByPrimaryKey(idDistribuidor);

            body += "Cotista: " + agenteMercado.Nome;
        }
        else
        {
            body += "Cotista: " + cotista.Apelido;
        }

        if (!String.IsNullOrEmpty(cotista.CodigoInterface))
        {
            body += " (" + cotista.CodigoInterface.Trim() + ")";
        }
                
        body += "\n";

        body += "Fundo: " + clienteFundo.Apelido + "\n";

        //FormaLiquidacao formaLiquidacao = new FormaLiquidacao();
        //formaLiquidacao.LoadByPrimaryKey(ordemCotista.IdFormaLiquidacao.Value);
        //body += "Liquida: " + formaLiquidacao.Descricao + "\n\n";

        string valor = "";
        if (ordemCotista.TipoOperacao == (byte)TipoOperacaoCotista.Aplicacao)
        {
            valor = "Valor: " + String.Format("{0:C}", ordemCotista.ValorBruto.Value) + "\n\n";
        }
        else
        {
            if (ordemCotista.TipoOperacao == (byte)TipoOperacaoCotista.ResgateBruto)
            {
                valor = "Valor Bruto: " + String.Format("{0:C}", ordemCotista.ValorBruto.Value) + "\n\n";
            }
            else if (ordemCotista.TipoOperacao == (byte)TipoOperacaoCotista.ResgateCotas)
            {
                valor = "Resgate por Cotas (Quantidade): " + String.Format("{0:N}", ordemCotista.Quantidade.Value) + "\n\n";
            }
            else if (ordemCotista.TipoOperacao == (byte)TipoOperacaoCotista.ResgateLiquido)
            {
                valor = "Valor Líquido: " + String.Format("{0:C}", ordemCotista.ValorLiquido.Value) + "\n\n";
            }
            else if (ordemCotista.TipoOperacao == (byte)TipoOperacaoCotista.ResgateTotal)
            {
                valor = "Tipo: Resgate Total" + "\n\n";
            }
        }
        body += valor;

        body += tabelaParametrosFrontCollection[0].ObservacaoCotista + "\n\n";

        if (!String.IsNullOrEmpty(ordemCotista.Observacao))
        {
            body += ordemCotista.Observacao;
        }
        
        string subject = "";
        if (ordemCotista.TipoOperacao == (byte)TipoOperacaoCotista.Aplicacao)
        {
            subject = "Boleto de Aplicação em Fundos (Cotista)";
        }
        else
        {
            subject = "Boleto de Resgate em Fundos (Cotista)";
        }

        EmailUtil e = new EmailUtil();        
        e.SendMail(body, subject, from, to);
    }
    
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();
    }

    protected void gridCadastro_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (gridCadastro.IsEditing && !gridCadastro.IsNewRowEditing)
        {
            //Preenche o campo de valor, de acordo com o tipo de operação
            ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;

            byte tipoOperacao = Convert.ToByte(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, new string[] { "TipoOperacao" }));
            if (!gridCadastro.IsNewRowEditing)
            {
                switch (tipoOperacao)
                {
                    case (byte)TipoOperacaoCotista.Aplicacao:
                    case (byte)TipoOperacaoCotista.ResgateBruto:
                    case (byte)TipoOperacaoCotista.ResgateTotal:
                        decimal valorBruto = Convert.ToDecimal(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, new string[] { "ValorBruto" }));
                        textValor.Text = valorBruto.ToString();
                        break;
                    case (byte)TipoOperacaoCotista.ResgateCotas:
                        decimal quantidade = Convert.ToDecimal(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, new string[] { "Quantidade" }));
                        textValor.Text = quantidade.ToString();
                        break;
                    case (byte)TipoOperacaoCotista.ResgateLiquido:
                        decimal valorLiquido = Convert.ToDecimal(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, new string[] { "ValorLiquido" }));
                        textValor.Text = valorLiquido.ToString();
                        break;
                }
            }
        }

    }

    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdOperacao");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idOperacao = Convert.ToInt32(keyValuesId[i]);

                OrdemCotista ordemCotista = new OrdemCotista();
                if (ordemCotista.LoadByPrimaryKey(idOperacao))
                {
                    int idCotista = ordemCotista.IdCotista.Value;

                    //
                    OrdemCotista ordemCotistaClone = (OrdemCotista)Utilitario.Clone(ordemCotista);
                    //

                    ordemCotista.MarkAsDeleted();
                    ordemCotista.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de OrdemCotista - Operacao: Delete OrdemCotista: " + idOperacao + UtilitarioWeb.ToString(ordemCotistaClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }
        else if (e.Parameters == "btnAprova" || e.Parameters == "btnCancela")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdOperacao");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idOperacao = Convert.ToInt32(keyValuesId[i]);

                OrdemCotista ordemCotista = new OrdemCotista();
                if (ordemCotista.LoadByPrimaryKey(idOperacao))
                {
                    if (e.Parameters == "btnAprova")
                    {
                        ordemCotista.Status = (byte)StatusOrdemCotista.Aprovado;
                        ordemCotista.Save();
                    }
                    else if (e.Parameters == "btnCancela")
                    {
                        ordemCotista.Status = (byte)StatusOrdemCotista.Cancelado;
                        ordemCotista.Save();
                    }

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Aprovação de Ordem Cotista: " + idOperacao,
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }
        else if (e.Parameters == "btnEmail")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdOperacao");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idOperacao = Convert.ToInt32(keyValuesId[i]);

                OrdemCotista ordemCotista = new OrdemCotista();
                if (ordemCotista.LoadByPrimaryKey(idOperacao))
                {
                    this.EnviaEmail(ordemCotista, false);
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e)
    {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing)
        {
            ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
            e.Properties["cpBtnEditCodigoCarteira"] = btnEditCodigoCarteira.ClientID;
        }
    }

    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "StatusDescricao")
        {
            int status = Convert.ToInt32(e.GetListSourceFieldValue(OrdemCotistaMetadata.ColumnNames.Status));

            string statusDescricao = "";
            if (status == (byte)StatusOrdemCotista.Digitado)
            {
                statusDescricao = "Digitado";
            }
            else if (status == (byte)StatusOrdemCotista.Aprovado)
            {
                statusDescricao = "Aprovado";
            }
            else if (status == (byte)StatusOrdemCotista.Processado)
            {
                statusDescricao = "Processado";
            }

            e.Value = statusDescricao;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_InitNewRow(object sender, ASPxDataInitNewRowEventArgs e)
    {
        if (!aprovacao)
        {
            //Valores default do form
            TabelaParametrosFrontCollection tabelaParametrosFrontCollection = new TabelaParametrosFrontCollection();
            tabelaParametrosFrontCollection.LoadAll();

            if (tabelaParametrosFrontCollection.Count > 0 && !String.IsNullOrEmpty(tabelaParametrosFrontCollection[0].ObservacaoCotista))
            {
                e.NewValues["Observacao"] = tabelaParametrosFrontCollection[0].ObservacaoCotista;
            }
        }
    }

    private Datas RetornaDatas(int idCarteira, byte tipoOperacao)
    {
        Datas datas = new Datas();

        Carteira carteira = new Carteira();
        carteira.LoadByPrimaryKey(idCarteira);
        
        DateTime hoje;
        bool usaDataCliente = ConfigurationManager.AppSettings["Cliente"] == "PORTOPAR";
        if (usaDataCliente)
        {
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCarteira);
            hoje = cliente.DataDia.Value;
        }else{
            hoje = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
        }

        int contagemResgate = carteira.ContagemDiasConversaoResgate.Value;

        //Trata campos com dataConversao e dataLiquidacao
        DateTime dataConversao;
        DateTime dataLiquidacao;
        int diasConversao = 0;
        int diasLiquidacao = 0;
        if (tipoOperacao == (byte)TipoOperacaoCotista.Aplicacao)
        {
            diasConversao = carteira.DiasCotizacaoAplicacao.Value;
            diasLiquidacao = carteira.DiasLiquidacaoAplicacao.Value;
        }
        else
        {
            diasConversao = carteira.DiasCotizacaoResgate.Value;
            diasLiquidacao = carteira.DiasLiquidacaoResgate.Value;
        }

        if (contagemResgate == (byte)ContagemDiasLiquidacaoResgate.DiasUteis || tipoOperacao == (byte)TipoOperacaoCotista.Aplicacao)
        {
            dataConversao = Calendario.AdicionaDiaUtil(hoje, diasConversao);
            dataLiquidacao = Calendario.AdicionaDiaUtil(hoje, diasLiquidacao);
        }
        else
        {
            //Conta por Dias Corridos
            dataConversao = hoje.AddDays(diasConversao);

            if (!Calendario.IsDiaUtil(dataConversao))
            {
                dataConversao = Calendario.AdicionaDiaUtil(dataConversao, 1);
            }

            //Conta por Dias Úteis em cima da data de conversão
            dataLiquidacao = Calendario.AdicionaDiaUtil(dataConversao, diasLiquidacao);
        }

        datas.dataOperacao = hoje;
        datas.dataConversao = dataConversao;
        datas.dataLiquidacao = dataLiquidacao;

        return datas;
    }

    protected string verificaSuitability(int idCotista, int idCarteira, byte tipoOperacao, DateTime dataOperacao)
    {
        ControllerPerfilCotista controllerPerfilCotista = new ControllerPerfilCotista();

        //TODO: Na migracao para versão 1.1.N (GPS), seleciona o ID do cliente pelo atributo cliente espelho do cadastro do cotista 
        //para selecao das informações de suitability do cadastro de cliente
        Cotista cotista = new Cotista();
        cotista.LoadByPrimaryKey(idCotista);

        PessoaSuitability pessoaSuitability = new PessoaSuitability();
        if (pessoaSuitability.LoadByPrimaryKey(cotista.IdPessoa.Value))
        {
            //Se cliente dispensado, não faz nada
            if (pessoaSuitability.Dispensado.Equals("S") || pessoaSuitability.Recusa.Equals("S"))
                return "";

            //Verifica se perfil do cliente foi informado ou se foi recusado a definição do perfil
            if (!pessoaSuitability.Perfil.HasValue)
                return "Perfil não informado. Não é possível continuar a operação.";

            //Verifica se perfil ainda está válido
            if (!pessoaSuitability.UltimaAlteracao.HasValue || dataOperacao > controllerPerfilCotista.retornaValidade(pessoaSuitability.UltimaAlteracao.Value))
                return "Perfil desatualizado. Não é possível continuar a operação.";

            //Verifica se é primeira aplicação no fundo
            OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();
            operacaoCotistaCollection.Query.Where(operacaoCotistaCollection.Query.IdCarteira.Equal(idCarteira));
            operacaoCotistaCollection.Query.Where(operacaoCotistaCollection.Query.IdCotista.Equal(idCotista));
            operacaoCotistaCollection.Query.Load();

            if (operacaoCotistaCollection.Count == 0)
            {
                return controllerPerfilCotista.selecionaMensagemEvento(16, idCotista);
            }
        }
        return "";
    }

    protected void logMensagem_callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        ControllerPerfilCotista.LogMensagem logMensagem = JsonConvert.DeserializeObject<ControllerPerfilCotista.LogMensagem>(e.Parameter);

        SuitabilityLogMensagens.createSuitabilityLogMensagens(HttpContext.Current.User.Identity.Name,
                                                              Convert.ToInt32(logMensagem.IdCotista),
                                                              Convert.ToInt32(logMensagem.IdMensagem),
                                                              logMensagem.Mensagem,
                                                              logMensagem.Resposta);

    }

    protected void mensagemTermoInadequacao_callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        ControllerPerfilCotista controllerPerfilCotista = new ControllerPerfilCotista();

        e.Result = controllerPerfilCotista.selecionaMensagemEvento(18, e.Parameter[0]);
    }

    protected void termoInadequacao_callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        ControllerPerfilCotista controllerPerfilCotista = new ControllerPerfilCotista();

        int idCotista = Convert.ToInt32(e.Parameter.Split('|')[0]);
        int idCarteira = Convert.ToInt32(e.Parameter.Split('|')[1]);

        controllerPerfilCotista.enviaTermoInadequacao(idCotista, idCarteira, suitabilityPerfilCotista, suitabilityPerfilPosicao);
    }
    
    protected void EsDSTrader_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        TraderCollection coll = new TraderCollection();
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
}