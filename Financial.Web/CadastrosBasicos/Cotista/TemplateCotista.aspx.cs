﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.CRM;
using Financial.InvestidorCotista;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_TemplateCotista : Financial.Web.Common.CadastroBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasPopupCotista = true;
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSTemplateCotista_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        TemplateCotistaQuery templateCotistaQuery = new TemplateCotistaQuery("T");
        CotistaQuery cotistaQuery = new CotistaQuery("C");

        templateCotistaQuery.Select(templateCotistaQuery, cotistaQuery.Apelido.As("Apelido"));
        templateCotistaQuery.InnerJoin(cotistaQuery).On(templateCotistaQuery.IdCotista == cotistaQuery.IdCotista);

        templateCotistaQuery.OrderBy(templateCotistaQuery.Descricao.Ascending);

        TemplateCotistaCollection coll = new TemplateCotistaCollection();
        coll.Load(templateCotistaQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCotista_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CotistaCollection coll = new CotistaCollection();
        coll.BuscaCotistasComAcesso(Context.User.Identity.Name);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    protected void btnEditCodigoCotista_Load(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxSpinEdit).Enabled = false;
            (sender as ASPxSpinEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCotista = Convert.ToInt32(e.Parameter);
            Cotista cotista = new Cotista();
            cotista.LoadByPrimaryKey(idCotista);

            if (cotista.str.Apelido != "") {
                if (cotista.IsAtivo) {
                    PermissaoCotista permissaoCotista = new PermissaoCotista();
                    if (permissaoCotista.RetornaAcessoCotista(idCotista, HttpContext.Current.User.Identity.Name)) {
                        nome = cotista.str.Apelido;
                    }
                    else {
                        nome = "no_access";
                    }
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";
        ASPxSpinEdit btnEditCodigoCotista = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCotista") as ASPxSpinEdit;
        ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(btnEditCodigoCotista);
        controles.Add(textDescricao);

        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        if (gridCadastro.IsNewRowEditing) {
            int idCotista = Convert.ToInt32(btnEditCodigoCotista.Text);

            TemplateCotista templateCotista = new TemplateCotista();
            if (templateCotista.LoadByPrimaryKey(idCotista)) {
                e.Result = "Registro já existente";
            }
        }
    }

    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();

        e.Result = "";
        if (e.Parameter == "clone")
        {
            //e.Result = 0;
        }
        else
        {
            e.Result = "Operação feita com sucesso.";
        }
    }

    private void SalvarNovo()
    {
        ASPxSpinEdit btnEditCodigoCotista = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCotista") as ASPxSpinEdit;
        ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;

        TemplateCotista templateCotista = new TemplateCotista();

        templateCotista.Descricao = Convert.ToString(textDescricao.Text);
        templateCotista.IdCotista = Convert.ToInt32(btnEditCodigoCotista.Text);
        templateCotista.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de TemplateCotista - Operacao: Insert TemplateCotista: " + templateCotista.IdCotista + UtilitarioWeb.ToString(templateCotista),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        int idCotista = (int)e.Keys[0];

        ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;

        TemplateCotista templateCotista = new TemplateCotista();

        if (templateCotista.LoadByPrimaryKey(idCotista)) {
            templateCotista.Descricao = Convert.ToString(textDescricao.Text);
            templateCotista.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de TemplateCotista - Operacao: Update TemplateCotista: " + idCotista + UtilitarioWeb.ToString(templateCotista),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdCotista");
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idCotista = Convert.ToInt32(keyValuesId[i]);

                TemplateCotista templateCotista = new TemplateCotista();
                if (templateCotista.LoadByPrimaryKey(idCotista)) {
                    //
                    TemplateCotista templateCotistaClone = (TemplateCotista)Utilitario.Clone(templateCotista);
                    //
                    
                    templateCotista.MarkAsDeleted();
                    templateCotista.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de TemplateCotista - Operacao: Delete TemplateCotista: " + idCotista + UtilitarioWeb.ToString(templateCotistaClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing) {
            ASPxSpinEdit btnEditCodigoCotista = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCotista") as ASPxSpinEdit;
            e.Properties["cpBtnEditCodigoCotista"] = btnEditCodigoCotista.ClientID;

            //TextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as TextBox;
            //e.Properties["cpTextDescricao"] = textDescricao.ClientID;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("btnEditCodigoCotista", "textDescricao");
        base.gridCadastro_PreRender(sender, e);
    }
}