﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using DevExpress.Web;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Util;
using System.Drawing;
using Financial.Web.Common;
using Financial.Common;
using Financial.InvestidorCotista;
using Financial.Fundo;
using Financial.Investidor;
using Financial.Investidor.Enums;
using System.Threading;
using Financial.CRM;

public partial class CadastrosBasicos_TransferenciaEntreCotista : CadastroBasePage
{
    static public int idCotistaOrigem = 0;
    static public int idCotistaDestino = 0;
    new protected void Page_Load(object sender, EventArgs e)
    {
        this.AllowUpdate = false; // Não permite Update
        base.Page_Load(sender, e);
    }

    #region PopUp CotistaOrigem
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSCotistaOrigem_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CotistaCollection coll = new CotistaCollection();
        CotistaQuery cotistaQuery = new CotistaQuery("cotista");
        PosicaoCotistaQuery posicaoCotistaQuery = new PosicaoCotistaQuery("PosicaoCotista");

        cotistaQuery.es.Distinct = true;
        cotistaQuery.Select(cotistaQuery);
        cotistaQuery.InnerJoin(posicaoCotistaQuery).On(cotistaQuery.IdCotista.Equal(posicaoCotistaQuery.IdCotista));

        coll.Load(cotistaQuery);
        e.Collection = coll;
    }

    protected void gridCotistaOrigem_CustomDataCallback(object sender, ASPxGridViewCustomDataCallbackEventArgs e)
    {
        ASPxGridView gridView = sender as ASPxGridView;
        int visibleIndex = Convert.ToInt32(e.Parameters);
        int idCotista = Convert.ToInt32(gridView.GetRowValues(visibleIndex, CotistaMetadata.ColumnNames.IdCotista));
        e.Result = idCotista.ToString();
    }

    protected void gridCotistaOrigem_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        UtilitarioGrid.SetaCoresGrid(e);
    }

    protected void gridCotistaOrigem_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        this.gridCotistaOrigem.DataBind();
    }

    protected void callBackPopupCotistaOrigem_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            string nome = "";
            int idCotista = Convert.ToInt32(e.Parameter);
            Cotista cotista = new Cotista();
            cotista.LoadByPrimaryKey(idCotista);

            if (cotista.str.Apelido != "")
            {
                if (cotista.IsAtivo)
                {
                    PermissaoCotista permissaoCotista = new PermissaoCotista();
                    if (permissaoCotista.RetornaAcessoCotista(idCotista, HttpContext.Current.User.Identity.Name))
                    {
                        nome = idCotista.ToString() + " - " + cotista.str.Apelido;
                    }
                    else
                    {
                        nome = "no_access";
                    }
                }
                else
                {
                    nome = "no_active";
                }
            }
            e.Result = nome;
        }
    }
    #endregion

    #region PopUp CotistaDestino
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSCotistaDestino_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ASPxTextBox hiddenCotistaOrigem = gridCadastro.FindEditFormTemplateControl("hiddenCotistaOrigem") as ASPxTextBox;
        CotistaCollection coll = new CotistaCollection();
        if (hiddenCotistaOrigem != null && !string.IsNullOrEmpty(hiddenCotistaOrigem.Text))
        {
            int idCotistaOrigem = Convert.ToInt32(hiddenCotistaOrigem.Text);
            CotistaQuery cotistaOrigemQuery = new CotistaQuery("CotistaOrigem");
            CotistaQuery cotistaDestinoQuery = new CotistaQuery("CotistaDestino");
            PessoaQuery pessoaOrigemQuery = new PessoaQuery("PessoaOrigem");
            PessoaQuery pessoaDestinoQuery = new PessoaQuery("PessoaDestino");

            cotistaOrigemQuery.Select(cotistaDestinoQuery);
            cotistaOrigemQuery.InnerJoin(pessoaOrigemQuery).On(cotistaOrigemQuery.IdPessoa.Equal(pessoaOrigemQuery.IdPessoa));
            cotistaOrigemQuery.InnerJoin(pessoaDestinoQuery).On(pessoaDestinoQuery.Cpfcnpj.Equal(pessoaOrigemQuery.Cpfcnpj));
            cotistaOrigemQuery.InnerJoin(cotistaDestinoQuery).On(cotistaDestinoQuery.IdPessoa.Equal(pessoaDestinoQuery.IdPessoa) & cotistaDestinoQuery.IdCotista.NotEqual(idCotistaOrigem));
            cotistaOrigemQuery.Where(cotistaOrigemQuery.IdCotista.Equal(idCotistaOrigem));

            coll.Load(cotistaOrigemQuery);
        }

        e.Collection = coll;
    }

    protected void gridCotistaDestino_CustomDataCallback(object sender, ASPxGridViewCustomDataCallbackEventArgs e)
    {
        ASPxGridView gridView = sender as ASPxGridView;
        int visibleIndex = Convert.ToInt32(e.Parameters);
        int idCotista = Convert.ToInt32(gridView.GetRowValues(visibleIndex, CotistaMetadata.ColumnNames.IdCotista));
        e.Result = idCotista.ToString();
    }

    protected void gridCotistaDestino_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        UtilitarioGrid.SetaCoresGrid(e);
    }

    protected void gridCotistaDestino_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        this.gridCotistaDestino.DataBind();
    }

    protected void callBackPopupCotistaDestino_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            string nome = "";
            int idCotista = Convert.ToInt32(e.Parameter);
            Cotista cotista = new Cotista();
            cotista.LoadByPrimaryKey(idCotista);

            if (cotista.str.Apelido != "")
            {
                if (cotista.IsAtivo)
                {
                    PermissaoCotista permissaoCotista = new PermissaoCotista();
                    if (permissaoCotista.RetornaAcessoCotista(idCotista, HttpContext.Current.User.Identity.Name))
                    {
                        nome = idCotista.ToString() + " - " + cotista.str.Apelido;
                    }
                    else
                    {
                        nome = "no_access";
                    }
                }
                else
                {
                    nome = "no_active";
                }
            }
            e.Result = nome;
        }
    }
    #endregion

    #region EsSelect
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSTransferenciaEntreCotista_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        SeriesOffShoreQuery serieOrigem = new SeriesOffShoreQuery("serieOrigem");
        PosicaoCotistaAberturaQuery posicaoAbertura = new PosicaoCotistaAberturaQuery("posicaoAbertura");
        PosicaoCotistaQuery posicao = new PosicaoCotistaQuery("posicao");
        SeriesOffShoreQuery serieDestino = new SeriesOffShoreQuery("serieDestino");
        CarteiraQuery carteiraQuery = new CarteiraQuery("carteira");
        CotistaQuery cotistaDestinoQuery = new CotistaQuery("cotistaDestino");
        CotistaQuery cotistaOrigemQuery = new CotistaQuery("cotistaOrigem");
        TransferenciaEntreCotistaQuery transQuery = new TransferenciaEntreCotistaQuery("transferencia");

        TransferenciaEntreCotistaCollection coll = new TransferenciaEntreCotistaCollection();

        transQuery.Select(transQuery,
                          carteiraQuery.Apelido.As("ApelidoCarteira"),
                          cotistaDestinoQuery.Apelido.As("ApelidoDestino"),
                          cotistaOrigemQuery.Apelido.As("ApelidoOrigem"));
        transQuery.InnerJoin(carteiraQuery).On(transQuery.IdCarteira.Equal(carteiraQuery.IdCarteira));
        transQuery.InnerJoin(cotistaDestinoQuery).On(transQuery.IdCotistaDestino.Equal(cotistaDestinoQuery.IdCotista));
        transQuery.InnerJoin(cotistaOrigemQuery).On(transQuery.IdCotistaOrigem.Equal(cotistaOrigemQuery.IdCotista));
        transQuery.OrderBy(transQuery.DataProcessamento.Descending);

        coll.Load(transQuery);

        e.Collection = coll;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSPosicaoCotista_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        PosicaoCotistaAberturaCollection coll = new PosicaoCotistaAberturaCollection();
        PosicaoCotistaAberturaQuery posicaoQuery = new PosicaoCotistaAberturaQuery("posicao");
        CarteiraQuery carteiraQuery = new CarteiraQuery("carteira");
        ClienteQuery clienteQuery = new ClienteQuery("Cliente");

        if (idCotistaOrigem != 0)
        {
            posicaoQuery.Select(posicaoQuery, carteiraQuery.Apelido, clienteQuery.DataDia.As("DataProcessamento"));
            posicaoQuery.InnerJoin(carteiraQuery).On(posicaoQuery.IdCarteira.Equal(carteiraQuery.IdCarteira));
            posicaoQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente.Equal(carteiraQuery.IdCarteira));
            posicaoQuery.Where(posicaoQuery.IdCotista.Equal(idCotistaOrigem) & posicaoQuery.DataHistorico.Equal(clienteQuery.DataDia));
            coll.Load(posicaoQuery);
        }
        e.Collection = coll;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    #endregion

    #region GridCadastro

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdTransferenciaEntreCotista");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                TransferenciaEntreCotista transferenciaEntreCotista = new TransferenciaEntreCotista();
                int IdTransferenciaEntreCotista = Convert.ToInt32(keyValuesId[i]);

                if (transferenciaEntreCotista.LoadByPrimaryKey(IdTransferenciaEntreCotista))
                {
                    TransferenciaEntreCotista transferenciaEntreCotistaClone = (TransferenciaEntreCotista)Utilitario.Clone(transferenciaEntreCotista);
                    //
                    transferenciaEntreCotista.MarkAsDeleted();
                    transferenciaEntreCotista.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de TransferenciaEntreCotista - Operacao: Delete TransferenciaEntreCotista: " + IdTransferenciaEntreCotista + UtilitarioWeb.ToString(transferenciaEntreCotistaClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        base.gridCadastro_CellEditorInitialize(sender, e);

        if (!gridCadastro.IsNewRowEditing)
        {
            if (e.Column.FieldName == TransferenciaEntreCotistaMetadata.ColumnNames.IdTransferenciaEntreCotista)
            {
                e.Editor.BackColor = Color.FromName("#EBEBEB");
                e.Editor.ReadOnly = true;
            }
        }
        else
        { // Modo Novo
            if (e.Column.FieldName == TransferenciaEntreCotistaMetadata.ColumnNames.IdTransferenciaEntreCotista)
            {
                e.Editor.BackColor = Color.FromName("#EBEBEB");
                e.Editor.ReadOnly = true;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e)
    {
        this.FocaCampoGrid("textDescricao");
        base.gridCadastro_PreRender(sender, e);
    }
    #endregion

    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    private void SalvarNovo()
    {
        ASPxTextBox hiddenCotistaDestino = gridCadastro.FindEditFormTemplateControl("hiddenCotistaDestino") as ASPxTextBox;
        ASPxTextBox hiddenCotistaOrigem = gridCadastro.FindEditFormTemplateControl("hiddenCotistaOrigem") as ASPxTextBox;
        ASPxGridLookup dropPosicaoCotista = gridCadastro.FindEditFormTemplateControl("dropPosicaoCotista") as ASPxGridLookup;
        TransferenciaEntreCotistaCollection transferenciaEntreCotistaColl = new TransferenciaEntreCotistaCollection();
        Hashtable hsClientes = new Hashtable();

        string[] strPosicoes = dropPosicaoCotista.Text.Split(',');
        foreach (string strPosicao in strPosicoes)
        {
            PosicaoCotista posicao = new PosicaoCotista();
            posicao.LoadByPrimaryKey(Convert.ToInt32(strPosicao.Trim()));

            DateTime dataDia;
            int idCarteira = posicao.IdCarteira.Value;
            if (hsClientes.Contains(idCarteira))
                dataDia = (DateTime)hsClientes[idCarteira];
            else
            {
                Cliente cliente = new Cliente();
                cliente.LoadByPrimaryKey(idCarteira);
                dataDia = cliente.DataDia.Value;
                hsClientes.Add(idCarteira, dataDia);
            }

            TransferenciaEntreCotista transferenciaEntreCotista = transferenciaEntreCotistaColl.AddNew();
            transferenciaEntreCotista.DataProcessamento = dataDia;
            transferenciaEntreCotista.IdCarteira = idCarteira;
            transferenciaEntreCotista.IdPosicao = Convert.ToInt32(strPosicao.Trim());
            transferenciaEntreCotista.IdCotistaOrigem = Convert.ToInt32(hiddenCotistaOrigem.Text);
            transferenciaEntreCotista.IdCotistaDestino = Convert.ToInt32(hiddenCotistaDestino.Text);
            transferenciaEntreCotista.IdOperacaoDeposito = null;
            transferenciaEntreCotista.IdOperacaoRetirada = null;
            transferenciaEntreCotista.IdAplicacao = posicao.IdOperacao.Value;
        }

        transferenciaEntreCotistaColl.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de TransferenciaEntreCotista - Operacao: Insert TransferenciaEntreCotista, Lote Posições" + dropPosicaoCotista.Text,
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    #region CallBacks
    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
        ASPxTextBox hiddenCotistaDestino = gridCadastro.FindEditFormTemplateControl("hiddenCotistaDestino") as ASPxTextBox;
        ASPxTextBox hiddenCotistaOrigem = gridCadastro.FindEditFormTemplateControl("hiddenCotistaOrigem") as ASPxTextBox;
        ASPxGridLookup dropPosicaoCotista = gridCadastro.FindEditFormTemplateControl("dropPosicaoCotista") as ASPxGridLookup;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(hiddenCotistaDestino);
        controles.Add(hiddenCotistaOrigem);

        if (gridCadastro.IsNewRowEditing)
            controles.Add(dropPosicaoCotista);

        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        if (gridCadastro.IsNewRowEditing)
        {
            #region Valida transacoes x posicoes do mesmo dia
            string[] strPosicoes = dropPosicaoCotista.Text.Split(',');
            List<int> idPosicoes = new List<int>();

            foreach (string idPosicao in strPosicoes)
                idPosicoes.Add(Convert.ToInt32(idPosicao.Trim()));

            TransferenciaEntreCotistaCollection coll = new TransferenciaEntreCotistaCollection();
            coll.Query.Where(coll.Query.IdPosicao.In(idPosicoes.ToArray()));

            if (coll.Query.Load())
            {
                StringBuilder strRetorno = new StringBuilder();

                strRetorno.Append("Já existe cadastro de transferência");

                if (coll.Count == 1)
                    strRetorno.Append(" para posição ");
                else
                    strRetorno.Append(" para as posições: ");

                foreach (TransferenciaEntreCotista transferencia in coll)                
                    strRetorno.Append(transferencia.IdPosicao.Value).Append(",");
                
                e.Result = strRetorno.ToString().Remove(strRetorno.Length - 1) + " !";
                return;
            }
            #endregion

        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();

        e.Result = "";
        if (e.Parameter == "clone")
        {
            //e.Result = 0;
        }
        else
        {
            e.Result = "Cadastro feito com sucesso.";
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callBackAtualizaFiltro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        ASPxTextBox hiddenCotistaDestino = gridCadastro.FindEditFormTemplateControl("hiddenCotistaDestino") as ASPxTextBox;
        ASPxTextBox hiddenCotistaOrigem = gridCadastro.FindEditFormTemplateControl("hiddenCotistaOrigem") as ASPxTextBox;

        if (!string.IsNullOrEmpty(hiddenCotistaDestino.Text))
            idCotistaDestino = Convert.ToInt32(hiddenCotistaDestino.Text);
        else
            idCotistaDestino = 0;

        if (!string.IsNullOrEmpty(hiddenCotistaOrigem.Text))
            idCotistaOrigem = Convert.ToInt32(hiddenCotistaOrigem.Text);
        else
            idCotistaOrigem = 0;

    }
    #endregion
}