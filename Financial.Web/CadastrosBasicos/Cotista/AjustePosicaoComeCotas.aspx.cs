﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using Financial.Investidor;
using Financial.ContaCorrente;
using System.Drawing;
using Financial.ContaCorrente.Enums;
using Financial.Security;
using Financial.Fundo;
using Financial.InvestidorCotista;
using Financial.InvestidorCotista.Enums;
using Financial.Investidor.Enums;
using Financial.Common.Enums;
using Financial.Bolsa;
using Financial.Bolsa.Enums;
using DevExpress.Web.Data;
using Financial.Web.Common;
using EntitySpaces.Interfaces;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Util.Enums;

public partial class CadastrosBasicos_AjustePosicaoComeCotas : Page
{
    private int selectedIndex;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e) {
        btnEditCodigoCarteira.Focus();
    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSPosicaoCotista_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        if (btnEditCodigoCarteira.Text != "" && btnEditCodigoCotista.Text != "" && textData.Text != "") {

            AgendaComeCotasQuery agendaComeCotasQuery = new AgendaComeCotasQuery("ACC");
            PosicaoCotistaQuery posicaoCotistaQuery = new PosicaoCotistaQuery("PC");
            AgendaComeCotasAlteradaQuery agendaComeCotasAlteradaQuery = new AgendaComeCotasAlteradaQuery("ACCA");

            agendaComeCotasQuery.Select(agendaComeCotasQuery.IdAgendamentoComeCotas,
                                        agendaComeCotasQuery.DataLancamento,
                                        agendaComeCotasQuery.DataVencimento,
                                        posicaoCotistaQuery.DataAplicacao,
                                        agendaComeCotasQuery.QuantidadeAntesCortes,
                                       (agendaComeCotasQuery.ValorIRAgendado * agendaComeCotasQuery.QuantidadeAntesCortes).As("ValorIrAgendado"),
                                       (agendaComeCotasQuery.ValorIOFVirtual * agendaComeCotasQuery.QuantidadeAntesCortes).As("ValorIofVirtual"),
                                       (agendaComeCotasQuery.Residuo15 * agendaComeCotasQuery.QuantidadeAntesCortes).As("Residuo15"),
                                       (agendaComeCotasQuery.Residuo175 * agendaComeCotasQuery.QuantidadeAntesCortes).As("Residuo175"),
                                       (agendaComeCotasQuery.Residuo20 * agendaComeCotasQuery.QuantidadeAntesCortes).As("Residuo20"),
                                       (agendaComeCotasQuery.Residuo225 * agendaComeCotasQuery.QuantidadeAntesCortes).As("Residuo225"),
                                       (agendaComeCotasAlteradaQuery.ValorIrAgendadoAntigo * agendaComeCotasQuery.QuantidadeAntesCortes).As("ValorIrAgendadoAntigo"),
                                       (agendaComeCotasAlteradaQuery.ValorIOFVirtualAntigo * agendaComeCotasQuery.QuantidadeAntesCortes).As("ValorIOFVirtualAntigo"),
                                       (agendaComeCotasAlteradaQuery.Residual15Antigo * agendaComeCotasQuery.QuantidadeAntesCortes).As("Residual15Antigo"),
                                       (agendaComeCotasAlteradaQuery.Residual175Antigo * agendaComeCotasQuery.QuantidadeAntesCortes).As("Residual175Antigo"),
                                       (agendaComeCotasAlteradaQuery.Residual20Antigo * agendaComeCotasQuery.QuantidadeAntesCortes).As("Residual20Antigo"),
                                       (agendaComeCotasAlteradaQuery.Residual225Antigo * agendaComeCotasQuery.QuantidadeAntesCortes).As("Residual225Antigo"));
            agendaComeCotasQuery.InnerJoin(posicaoCotistaQuery).On(posicaoCotistaQuery.IdPosicao.Equal(agendaComeCotasQuery.IdPosicao));
            agendaComeCotasQuery.LeftJoin(agendaComeCotasAlteradaQuery).On(agendaComeCotasQuery.IdAgendamentoComeCotas.Equal(agendaComeCotasAlteradaQuery.IdAgendaComeCotas));
            agendaComeCotasQuery.Where(agendaComeCotasQuery.TipoPosicao.Equal(TipoComeCotas.Cotista));
            agendaComeCotasQuery.Where(posicaoCotistaQuery.IdCarteira.Equal(Convert.ToInt32(btnEditCodigoCarteira.Text)));
            agendaComeCotasQuery.Where(posicaoCotistaQuery.IdCotista.Equal(Convert.ToInt32(btnEditCodigoCotista.Text)));
            agendaComeCotasQuery.Where(agendaComeCotasQuery.DataLancamento.LessThanOrEqual(Convert.ToDateTime(textData.Text)));         
            
            AgendaComeCotasCollection coll = new AgendaComeCotasCollection();
            coll.Load(agendaComeCotasQuery);

            // Assign the esDataSourcSelectEvenArgs Collection property
            e.Collection = coll;
        }
        else {
            e.Collection = new AgendaComeCotasCollection();
        }
    }

    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CarteiraCollection coll = new CarteiraCollection();
        coll.BuscaCarteirasComAcessoTodas(Context.User.Identity.Name);
        
        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCotista_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CotistaCollection coll = new CotistaCollection();
        coll.BuscaCotistasComAcesso(Context.User.Identity.Name);
        
        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        string resultado = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") {
                if (cliente.IsAtivo) {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoCliente(idCliente, HttpContext.Current.User.Identity.Name)) {
                        nome = cliente.str.Apelido;
                        resultado = nome;
                    }
                    else {
                        resultado = "no_access";
                    }
                }
                else {
                    resultado = "no_active";
                }
            }
        }
        e.Result = resultado;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCotista = Convert.ToInt32(e.Parameter);
            Cotista cotista = new Cotista();
            cotista.LoadByPrimaryKey(idCotista);

            if (cotista.str.Apelido != "") {
                if (cotista.IsAtivo) {
                    PermissaoCotista permissaoCotista = new PermissaoCotista();
                    if (permissaoCotista.RetornaAcessoCotista(idCotista, HttpContext.Current.User.Identity.Name)) {
                        nome = cotista.str.Apelido;
                    }
                    else {
                        nome = "no_access";
                    }
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback4_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        
        ASPxPopupControl popupPosicaoCotista = this.FindControl("popupPosicaoCotista") as ASPxPopupControl;
        ASPxGridView gridPosicaoCotista = popupPosicaoCotista.FindControl("gridPosicaoCotista") as ASPxGridView;

        int row = Convert.ToInt16(e.Parameter);

        decimal? ValorIrAgendadoAntigo = null;
        decimal? ValorIOFVirtualAntigo = null;
        decimal? Residual15Antigo = null;
        decimal? Residual175Antigo = null;
        decimal? Residual20Antigo = null;
        decimal? Residual225Antigo = null;

        if (gridPosicaoCotista.GetRowValues(row, "ValorIrAgendadoAntigo").ToString() != "")
            ValorIrAgendadoAntigo = Convert.ToDecimal(gridPosicaoCotista.GetRowValues(row, "ValorIrAgendadoAntigo"));

        if (gridPosicaoCotista.GetRowValues(row, "ValorIOFVirtualAntigo").ToString() != "")
            ValorIOFVirtualAntigo = Convert.ToDecimal(gridPosicaoCotista.GetRowValues(row, "ValorIOFVirtualAntigo"));

        if (gridPosicaoCotista.GetRowValues(row, "Residual15Antigo").ToString() != "")
            Residual15Antigo = Convert.ToDecimal(gridPosicaoCotista.GetRowValues(row, "Residual15Antigo"));

        if (gridPosicaoCotista.GetRowValues(row, "Residual175Antigo").ToString() != "")
            Residual175Antigo = Convert.ToDecimal(gridPosicaoCotista.GetRowValues(row, "Residual175Antigo"));

        if (gridPosicaoCotista.GetRowValues(row, "Residual20Antigo").ToString() != "")
            Residual20Antigo = Convert.ToDecimal(gridPosicaoCotista.GetRowValues(row, "Residual20Antigo"));

        if (gridPosicaoCotista.GetRowValues(row, "Residual225Antigo").ToString() != "")
            Residual225Antigo = Convert.ToDecimal(gridPosicaoCotista.GetRowValues(row, "Residual225Antigo"));

        int idAgendaComeCotas = Convert.ToInt32(gridPosicaoCotista.GetRowValues(row, "IdAgendamentoComeCotas"));
        decimal quantidadeAntesCortes = Convert.ToDecimal(gridPosicaoCotista.GetRowValues(row, "QuantidadeAntesCortes"));

        DateTime data = Convert.ToDateTime(textData.Text);

        AgendaComeCotasAlterada agendaComeCotasAlterada = new AgendaComeCotasAlterada();
        agendaComeCotasAlterada.LoadByPrimaryKey(idAgendaComeCotas);
        agendaComeCotasAlterada.MarkAsDeleted();
        agendaComeCotasAlterada.Save();


        AgendaComeCotas agendaComeCotas = new AgendaComeCotas();
        if (agendaComeCotas.LoadByPrimaryKey(idAgendaComeCotas))
        {

            if (ValorIrAgendadoAntigo.HasValue)
                agendaComeCotas.ValorIRAgendado = ValorIrAgendadoAntigo.Value / quantidadeAntesCortes;
            if (ValorIOFVirtualAntigo.HasValue)
                agendaComeCotas.ValorIOFVirtual = ValorIOFVirtualAntigo.Value / quantidadeAntesCortes;
            if (Residual15Antigo.HasValue)
                agendaComeCotas.Residuo15 = Residual15Antigo.Value / quantidadeAntesCortes;
            if (Residual175Antigo.HasValue)
                agendaComeCotas.Residuo175 = Residual175Antigo.Value / quantidadeAntesCortes;
            if (Residual20Antigo.HasValue)
                agendaComeCotas.Residuo20 = Residual20Antigo.Value / quantidadeAntesCortes;
            if (Residual225Antigo.HasValue)
                agendaComeCotas.Residuo225 = Residual225Antigo.Value / quantidadeAntesCortes;

            agendaComeCotas.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de AjustePosicaoComeCotas - Operacao: Update AjustePosicaoComeCotas: " + idAgendaComeCotas + UtilitarioWeb.ToString(agendaComeCotas),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }
        
        gridPosicaoCotista.DataBind();

     }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCarteira_CustomDataCallback(object sender, DevExpress.Web.ASPxGridViewCustomDataCallbackEventArgs e) {
        e.Result = gridCarteira.GetRowValues(Convert.ToInt32(e.Parameters), "IdCarteira");
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCotista_CustomDataCallback(object sender, DevExpress.Web.ASPxGridViewCustomDataCallbackEventArgs e) {
        e.Result = gridCotista.GetRowValues(Convert.ToInt32(e.Parameters), "IdCotista");
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCarteira_HtmlRowCreated(object sender, DevExpress.Web.ASPxGridViewTableRowEventArgs e) {
        e.Row.Attributes.Add("onmouseover", "backgroundColor = this.style.backgroundColor; this.style.backgroundColor='#E0E9EB';");
        e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=backgroundColor;");
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCotista_HtmlRowCreated(object sender, DevExpress.Web.ASPxGridViewTableRowEventArgs e) {
        e.Row.Attributes.Add("onmouseover", "backgroundColor = this.style.backgroundColor; this.style.backgroundColor='#E0E9EB';");
        e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=backgroundColor;");
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="selectedIndex"></param>
    protected void AtualizaPosicao(int selectedIndex) {
        ASPxPopupControl popupPosicaoCotista = this.FindControl("popupPosicaoCotista") as ASPxPopupControl;
        ASPxGridView gridPosicaoCotista = popupPosicaoCotista.FindControl("gridPosicaoCotista") as ASPxGridView;
        ASPxSpinEdit textValorIrAgendado = popupPosicaoCotista.FindControl("textValorIrAgendado") as ASPxSpinEdit;
        ASPxSpinEdit textValorIofVirtual = popupPosicaoCotista.FindControl("textValorIofVirtual") as ASPxSpinEdit;
        ASPxSpinEdit textResidual15 = popupPosicaoCotista.FindControl("textResidual15") as ASPxSpinEdit;
        ASPxSpinEdit textResidual175 = popupPosicaoCotista.FindControl("textResidual175") as ASPxSpinEdit;
        ASPxSpinEdit textResidual20 = popupPosicaoCotista.FindControl("textResidual20") as ASPxSpinEdit;
        ASPxSpinEdit textResidual225 = popupPosicaoCotista.FindControl("textResidual225") as ASPxSpinEdit;

        decimal? ValorIrAgendadoAntigo = null;
        decimal? ValorIOFVirtualAntigo = null;
        decimal? Residual15Antigo = null;
        decimal? Residual175Antigo = null;
        decimal? Residual20Antigo = null;
        decimal? Residual225Antigo = null;

        if (gridPosicaoCotista.GetRowValues(selectedIndex, "ValorIrAgendadoAntigo").ToString() != "")
            ValorIrAgendadoAntigo = Convert.ToDecimal(gridPosicaoCotista.GetRowValues(selectedIndex, "ValorIrAgendadoAntigo"));

        if (gridPosicaoCotista.GetRowValues(selectedIndex, "ValorIOFVirtualAntigo").ToString() != "")
            ValorIOFVirtualAntigo = Convert.ToDecimal(gridPosicaoCotista.GetRowValues(selectedIndex, "ValorIOFVirtualAntigo"));

        if (gridPosicaoCotista.GetRowValues(selectedIndex, "Residual15Antigo").ToString() != "")
            Residual15Antigo = Convert.ToDecimal(gridPosicaoCotista.GetRowValues(selectedIndex, "Residual15Antigo"));

        if (gridPosicaoCotista.GetRowValues(selectedIndex, "Residual175Antigo").ToString() != "")
            Residual175Antigo = Convert.ToDecimal(gridPosicaoCotista.GetRowValues(selectedIndex, "Residual175Antigo"));

        if (gridPosicaoCotista.GetRowValues(selectedIndex, "Residual20Antigo").ToString() != "")
            Residual20Antigo = Convert.ToDecimal(gridPosicaoCotista.GetRowValues(selectedIndex, "Residual20Antigo"));

        if (gridPosicaoCotista.GetRowValues(selectedIndex, "Residual225Antigo").ToString() != "")
            Residual225Antigo = Convert.ToDecimal(gridPosicaoCotista.GetRowValues(selectedIndex, "Residual225Antigo"));

        int idAgendaComeCotas = Convert.ToInt32(gridPosicaoCotista.GetRowValues(selectedIndex, "IdAgendamentoComeCotas"));
        decimal quantidadeAntesCortes = Convert.ToDecimal(gridPosicaoCotista.GetRowValues(selectedIndex, "QuantidadeAntesCortes"));

        decimal? valorIrAgendado = null;
        decimal? valorIofVirtual = null;
        decimal? residual15 = null;
        decimal? residual175 = null;
        decimal? residual20 = null;
        decimal? residual225 = null;

        if (textValorIrAgendado.Text != "")
            valorIrAgendado = Convert.ToDecimal(textValorIrAgendado.Text);

        if (textValorIofVirtual.Text != "")
            valorIofVirtual = Convert.ToDecimal(textValorIofVirtual.Text);

        if (textResidual15.Text != "")
            residual15 = Convert.ToDecimal(textResidual15.Text);

        if (textResidual175.Text != "")
            residual175 = Convert.ToDecimal(textResidual175.Text);

        if (textResidual20.Text != "")
            residual20 = Convert.ToDecimal(textResidual20.Text);

        if (textResidual225.Text != "")
            residual225 = Convert.ToDecimal(textResidual225.Text);

        if (valorIrAgendado.HasValue || valorIofVirtual.HasValue || residual15.HasValue || residual175.HasValue || residual20.HasValue || residual225.HasValue)
        {
            AgendaComeCotasAlterada agendaComeCotasAlterada = new AgendaComeCotasAlterada();
            agendaComeCotasAlterada.LoadByPrimaryKey(idAgendaComeCotas);
            agendaComeCotasAlterada.IdAgendaComeCotas = idAgendaComeCotas;

            AgendaComeCotas agendaComeCotas = new AgendaComeCotas();
            if (agendaComeCotas.LoadByPrimaryKey(idAgendaComeCotas))
            {

                if (valorIrAgendado.HasValue)
                {
                    if (!ValorIrAgendadoAntigo.HasValue)
                        agendaComeCotasAlterada.ValorIrAgendadoAntigo = agendaComeCotas.ValorIRAgendado.Value;

                    agendaComeCotas.ValorIRAgendado = valorIrAgendado.Value / quantidadeAntesCortes;
                }

                if (valorIofVirtual.HasValue)
                {
                    if (!ValorIOFVirtualAntigo.HasValue)
                        agendaComeCotasAlterada.ValorIOFVirtualAntigo = agendaComeCotas.ValorIOFVirtual.Value;

                    agendaComeCotas.ValorIOFVirtual = valorIofVirtual.Value / quantidadeAntesCortes;
                }

                if (residual15.HasValue)
                {
                    if (!Residual15Antigo.HasValue )
                        agendaComeCotasAlterada.Residual15Antigo = agendaComeCotas.Residuo15.Value;

                    agendaComeCotas.Residuo15 = residual15.Value / quantidadeAntesCortes;
                }

                if (residual175.HasValue)
                {
                    if (!Residual175Antigo.HasValue)
                        agendaComeCotasAlterada.Residual175Antigo = agendaComeCotas.Residuo175.Value;

                    agendaComeCotas.Residuo175 = residual175.Value / quantidadeAntesCortes;
                }

                if (residual20.HasValue)
                {
                    if (!Residual20Antigo.HasValue)
                        agendaComeCotasAlterada.Residual20Antigo = agendaComeCotas.Residuo20.Value;

                    agendaComeCotas.Residuo20 = residual20.Value / quantidadeAntesCortes;
                }

                if (residual225.HasValue)
                {
                    if (!Residual225Antigo.HasValue)
                        agendaComeCotasAlterada.Residual225Antigo = agendaComeCotas.Residuo225.Value;

                    agendaComeCotas.Residuo225 = residual225.Value / quantidadeAntesCortes;
                }


                agendaComeCotasAlterada.Save();
                agendaComeCotas.Save();

                #region Log do Processo
                HistoricoLog historicoLog = new HistoricoLog();
                historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                "Cadastro de AjustePosicaoComeCotas - Operacao: Update AjustePosicaoComeCotas: " + idAgendaComeCotas + UtilitarioWeb.ToString(agendaComeCotas),
                                                HttpContext.Current.User.Identity.Name,
                                                UtilitarioWeb.GetIP(Request),
                                                "",
                                                HistoricoLogOrigem.Outros);
                #endregion

            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridPosicaoCotista_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "") //Usado apenas na hora q abre inicialmente a popup, para dar refresh nas posições
        {
            gridPosicaoCotista.DataBind();
        }
        else {
            selectedIndex = Convert.ToInt32(e.Parameters);
            AtualizaPosicao(selectedIndex);
            gridPosicaoCotista.DataBind();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridPosicaoCotista_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e) {
        UtilitarioGrid.SetaCoresGrid(e);
    }
}