﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using Financial.Investidor;
using Financial.ContaCorrente;
using System.Drawing;
using Financial.ContaCorrente.Enums;
using Financial.Security;
using Financial.Fundo;
using Financial.Bolsa;
using Financial.BMF;
using Financial.RendaFixa;
using Financial.Investidor.Enums;
using Financial.InvestidorCotista;
using Financial.CRM;
using Financial.WebConfigConfiguration;
using Financial.Security.Enums;
using System.Collections.Specialized;
using EntitySpaces.Interfaces;
using Financial.Web.Util;
using Financial.InvestidorCotista.Enums;
using Financial.Web.Common;
using System.Web.Configuration;
using System.Xml;
using Financial.Export;
using Financial.Interfaces.Export;
using Financial.Util.Enums;

public partial class CadastrosBasicos_AjustePosicaoComeCotasAvancado : Financial.Web.Common.CadastroBasePage
{
    #region Campos
    ASPxSpinEdit btnEditCodigoCliente;
    ASPxDateEdit textDataLancamento;
    ASPxDateEdit textDataVencimento;
    ASPxDateEdit textDataUltimoIR;
    ASPxComboBox dropTipoPosicao;
    ASPxComboBox dropTipoAliquotaIR;
    ASPxComboBox dropExecucaoRecolhimento;
    ASPxComboBox dropTipoEvento;
    ASPxTextBox hiddenIdPosicao;
    ASPxSpinEdit textQuantidade;
    ASPxSpinEdit textQuantidadeAntesCortes;
    ASPxSpinEdit textValorCotaAplic;
    ASPxSpinEdit textValorCota;
    ASPxSpinEdit textValorCotaUltimoPagamentoIR;
    ASPxSpinEdit textAliquotaCC;
    ASPxSpinEdit textRendimentoBrutoDesdeAplicacao;
    ASPxSpinEdit textRendimentoDesdeUltimoPagamentoIR;
    ASPxSpinEdit textPrazoIOF;
    ASPxSpinEdit textAliquotaIOF;
    ASPxSpinEdit textValorIOF;
    ASPxSpinEdit textValorIOFVirtual;
    ASPxSpinEdit textNumDiasCorridosDesdeAquisicao;
    ASPxSpinEdit textPrejuizoUsado;
    ASPxSpinEdit textRendimentoCompensado;
    ASPxSpinEdit textValorIRAgendado;
    ASPxSpinEdit textResiduo15;
    ASPxSpinEdit textResiduo175;
    ASPxSpinEdit textResiduo20;
    ASPxSpinEdit textResiduo225;
    ASPxSpinEdit textValorIRPago;
    ASPxSpinEdit textQuantidadeComida;
    ASPxSpinEdit textQuantidadeFinal;
    ASPxSpinEdit textAliquotaIR;
    #endregion

    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasPopupCliente = true;
        base.Page_Load(sender, e);
    }

    #region Grid de Posição
    protected void gridPosicao_CustomDataCallback(object sender, DevExpress.Web.ASPxGridViewCustomDataCallbackEventArgs e)
    {
        e.Result = gridPosicao.GetRowValues(Convert.ToInt32(e.Parameters), "IdPosicao");
    }

    public void gridPosicao_CustomCallBack(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        ASPxGridView gridView = sender as ASPxGridView;
        gridView.DataBind();
    }
    #endregion

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ClienteCollection coll = new ClienteCollection();
        coll.BuscaClientesComAcesso(Context.User.Identity.Name);

        e.Collection = coll;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSPosicao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ASPxComboBox dropTipoPosicao = gridCadastro.FindEditFormTemplateControl("dropTipoPosicao") as ASPxComboBox;
        ASPxDateEdit textDataLancamento = gridCadastro.FindEditFormTemplateControl("textDataLancamento") as ASPxDateEdit;
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;

        int tipoPosicao = -1;
        int idCarteira = 0;
        DateTime dataHistorico = new DateTime();

        if (textDataLancamento != null && !string.IsNullOrEmpty(textDataLancamento.Text))
            dataHistorico = Convert.ToDateTime(textDataLancamento.Text);

        if (btnEditCodigoCliente != null && !string.IsNullOrEmpty(btnEditCodigoCliente.Text))
            idCarteira = Convert.ToInt32(btnEditCodigoCliente.Text);

        if (dropTipoPosicao != null && dropTipoPosicao.SelectedIndex != -1)
            tipoPosicao = Convert.ToInt32(dropTipoPosicao.SelectedItem.Value);

        if (dataHistorico != new DateTime() && tipoPosicao != -1 && idCarteira != 0) //todos devem estar preenchidos
        {
            if (tipoPosicao == (int)TipoComeCotas.Cotista)
            {
                PosicaoCotistaHistoricoCollection posicaoCotistaColl = new PosicaoCotistaHistoricoCollection();
                PosicaoCotistaHistoricoQuery posicaoCotistaQuery = new PosicaoCotistaHistoricoQuery("posicaoCotista");
                CotistaQuery cotistaQuery = new CotistaQuery("cotista");

                posicaoCotistaQuery.Select(posicaoCotistaQuery.IdPosicao,
                                           posicaoCotistaQuery.IdCotista.As("IdContraParte"),
                                           posicaoCotistaQuery.Quantidade,
                                           posicaoCotistaQuery.CotaDia,
                                           cotistaQuery.Apelido.As("Apelido"));
                posicaoCotistaQuery.InnerJoin(cotistaQuery).On(posicaoCotistaQuery.IdCotista.Equal(cotistaQuery.IdCotista));
                posicaoCotistaQuery.Where(posicaoCotistaQuery.DataHistorico.Equal(dataHistorico) & posicaoCotistaQuery.IdCarteira.Equal(idCarteira));

                posicaoCotistaColl.Load(posicaoCotistaQuery);

                e.Collection = posicaoCotistaColl;

                gridPosicao.Columns["IdContraParte"].Caption = "Id.Cotista";
            }
            else if (tipoPosicao == (int)TipoComeCotas.Fundo)
            {
                PosicaoFundoHistoricoCollection posicaoFundoColl = new PosicaoFundoHistoricoCollection();
                PosicaoFundoHistoricoQuery posicaoFundoQuery = new PosicaoFundoHistoricoQuery("posicaoFundo");
                CarteiraQuery carteiraQuery = new CarteiraQuery("cliente");

                posicaoFundoQuery.Select(posicaoFundoQuery.IdPosicao,
                                         posicaoFundoQuery.IdCarteira.As("IdContraParte"),
                                         posicaoFundoQuery.Quantidade,
                                         posicaoFundoQuery.CotaDia,
                                         carteiraQuery.Apelido.As("Apelido"));
                posicaoFundoQuery.InnerJoin(carteiraQuery).On(posicaoFundoQuery.IdCarteira.Equal(carteiraQuery.IdCarteira));
                posicaoFundoQuery.Where(posicaoFundoQuery.DataHistorico.Equal(dataHistorico) & posicaoFundoQuery.IdCliente.Equal(idCarteira));

                posicaoFundoColl.Load(posicaoFundoQuery);

                e.Collection = posicaoFundoColl;

                gridPosicao.Columns["IdContraParte"].Caption = "Id.Carteira";
            }
        }
        else
        {
            e.Collection = new PosicaoFundoHistoricoCollection();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSAgendaComeCotas_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        AgendaComeCotasCollection coll = new AgendaComeCotasCollection();
        AgendaComeCotasQuery agendaComeCotasQuery = new AgendaComeCotasQuery("agenda");
        ClienteQuery clienteQuery = new ClienteQuery("cliente");
        agendaComeCotasQuery.Select(agendaComeCotasQuery,
                                    clienteQuery.Apelido.As("Apelido"),
                                    (agendaComeCotasQuery.QuantidadeAntesCortes/agendaComeCotasQuery.QuantidadeFinal).As("Fator"));
        agendaComeCotasQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente.Equal(agendaComeCotasQuery.IdCarteira));

        coll.Load(agendaComeCotasQuery);

        e.Collection = coll;
    }
    #endregion

    #region Callback
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
        btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        textDataLancamento = gridCadastro.FindEditFormTemplateControl("textDataLancamento") as ASPxDateEdit;
        textDataVencimento = gridCadastro.FindEditFormTemplateControl("textDataVencimento") as ASPxDateEdit;
        textDataUltimoIR = gridCadastro.FindEditFormTemplateControl("textDataUltimoIR") as ASPxDateEdit;
        dropTipoPosicao = gridCadastro.FindEditFormTemplateControl("dropTipoPosicao") as ASPxComboBox;
        dropTipoAliquotaIR = gridCadastro.FindEditFormTemplateControl("dropTipoAliquotaIR") as ASPxComboBox;
        dropExecucaoRecolhimento = gridCadastro.FindEditFormTemplateControl("dropExecucaoRecolhimento") as ASPxComboBox;
        dropTipoEvento = gridCadastro.FindEditFormTemplateControl("dropTipoEvento") as ASPxComboBox;
        hiddenIdPosicao = gridCadastro.FindEditFormTemplateControl("hiddenIdPosicao") as ASPxTextBox;
        textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
        textQuantidadeAntesCortes = gridCadastro.FindEditFormTemplateControl("textQuantidadeAntesCortes") as ASPxSpinEdit;
        textValorCotaAplic = gridCadastro.FindEditFormTemplateControl("textValorCotaAplic") as ASPxSpinEdit;
        textValorCota = gridCadastro.FindEditFormTemplateControl("textValorCota") as ASPxSpinEdit;
        textValorCotaUltimoPagamentoIR = gridCadastro.FindEditFormTemplateControl("textValorCotaUltimoPagamentoIR") as ASPxSpinEdit;
        textAliquotaCC = gridCadastro.FindEditFormTemplateControl("textAliquotaCC") as ASPxSpinEdit;
        textRendimentoBrutoDesdeAplicacao = gridCadastro.FindEditFormTemplateControl("textRendimentoBrutoDesdeAplicacao") as ASPxSpinEdit;
        textRendimentoDesdeUltimoPagamentoIR = gridCadastro.FindEditFormTemplateControl("textRendimentoDesdeUltimoPagamentoIR") as ASPxSpinEdit;
        textPrazoIOF = gridCadastro.FindEditFormTemplateControl("textPrazoIOF") as ASPxSpinEdit;
        textAliquotaIOF = gridCadastro.FindEditFormTemplateControl("textAliquotaIOF") as ASPxSpinEdit;
        textValorIOF = gridCadastro.FindEditFormTemplateControl("textValorIOF") as ASPxSpinEdit;
        textValorIOFVirtual = gridCadastro.FindEditFormTemplateControl("textValorIOFVirtual") as ASPxSpinEdit;
        textNumDiasCorridosDesdeAquisicao = gridCadastro.FindEditFormTemplateControl("textNumDiasCorridosDesdeAquisicao") as ASPxSpinEdit;
        textPrejuizoUsado = gridCadastro.FindEditFormTemplateControl("textPrejuizoUsado") as ASPxSpinEdit;
        textRendimentoCompensado = gridCadastro.FindEditFormTemplateControl("textRendimentoCompensado") as ASPxSpinEdit;
        textValorIRAgendado = gridCadastro.FindEditFormTemplateControl("textValorIRAgendado") as ASPxSpinEdit;
        textResiduo15 = gridCadastro.FindEditFormTemplateControl("textResiduo15") as ASPxSpinEdit;
        textResiduo175 = gridCadastro.FindEditFormTemplateControl("textResiduo175") as ASPxSpinEdit;
        textResiduo20 = gridCadastro.FindEditFormTemplateControl("textResiduo20") as ASPxSpinEdit;
        textResiduo225 = gridCadastro.FindEditFormTemplateControl("textResiduo225") as ASPxSpinEdit;
        textValorIRPago = gridCadastro.FindEditFormTemplateControl("textValorIRPago") as ASPxSpinEdit;
        textQuantidadeComida = gridCadastro.FindEditFormTemplateControl("textQuantidadeComida") as ASPxSpinEdit;
        textQuantidadeFinal = gridCadastro.FindEditFormTemplateControl("textQuantidadeFinal") as ASPxSpinEdit;
        textAliquotaIR = gridCadastro.FindEditFormTemplateControl("textAliquotaIR") as ASPxSpinEdit;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(btnEditCodigoCliente);
        controles.Add(textDataLancamento);
        controles.Add(textDataVencimento);
        controles.Add(textDataUltimoIR);
        controles.Add(dropTipoPosicao);
        controles.Add(dropTipoAliquotaIR);
        controles.Add(dropExecucaoRecolhimento);
        controles.Add(dropTipoEvento);
        controles.Add(hiddenIdPosicao);
        controles.Add(textQuantidade);
        controles.Add(textQuantidadeAntesCortes);
        controles.Add(textValorCotaAplic);
        controles.Add(textValorCota);
        controles.Add(textValorCotaUltimoPagamentoIR);
        controles.Add(textAliquotaCC);
        controles.Add(textRendimentoBrutoDesdeAplicacao);
        controles.Add(textRendimentoDesdeUltimoPagamentoIR);
        controles.Add(textPrazoIOF);
        controles.Add(textAliquotaIOF);
        controles.Add(textValorIOF);
        controles.Add(textValorIOFVirtual);
        controles.Add(textNumDiasCorridosDesdeAquisicao);
        controles.Add(textPrejuizoUsado);
        controles.Add(textRendimentoCompensado);
        controles.Add(textValorIRAgendado);
        controles.Add(textResiduo15);
        controles.Add(textResiduo175);
        controles.Add(textResiduo20);
        controles.Add(textResiduo225);
        controles.Add(textValorIRPago);
        controles.Add(textQuantidadeComida);
        controles.Add(textQuantidadeFinal);
        controles.Add(textAliquotaIR);

        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackCliente_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            if (cliente.LoadByPrimaryKey(idCliente))
            {
                nome = cliente.Apelido;
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackPosicao_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        ASPxDateEdit textDataLancamento = gridCadastro.FindEditFormTemplateControl("textDataLancamento") as ASPxDateEdit;
        ASPxTextBox hiddenIdPosicao = gridCadastro.FindEditFormTemplateControl("hiddenIdPosicao") as ASPxTextBox;
        ASPxComboBox dropTipoPosicao = gridCadastro.FindEditFormTemplateControl("dropTipoPosicao") as ASPxComboBox;

        int tipoPosicao = -1;
        int idPosicao = 0;
        DateTime dataHistorico = new DateTime();

        if (textDataLancamento != null && !string.IsNullOrEmpty(textDataLancamento.Text))
            dataHistorico = Convert.ToDateTime(textDataLancamento.Text);

        if (hiddenIdPosicao != null && !string.IsNullOrEmpty(hiddenIdPosicao.Text))
            idPosicao = Convert.ToInt32(hiddenIdPosicao.Text);

        if (dropTipoPosicao != null && dropTipoPosicao.SelectedIndex != -1)
            tipoPosicao = Convert.ToInt32(dropTipoPosicao.SelectedItem.Value);

        if (tipoPosicao != -1 && idPosicao != 0 && dataHistorico != new DateTime())
        {
            decimal cotaDia = 0, cotaAplicacao = 0, quantidade = 0, quantidadeAntesCortes = 0, rendimentoBrutoAplicacao = 0;

            if (tipoPosicao == (int)TipoComeCotas.Cotista)
            {
                PosicaoCotistaHistorico posicaoCotistaHistorico = new PosicaoCotistaHistorico();
                posicaoCotistaHistorico.Query.Select(posicaoCotistaHistorico.Query.Quantidade, posicaoCotistaHistorico.Query.QuantidadeAntesCortes, posicaoCotistaHistorico.Query.CotaAplicacao, posicaoCotistaHistorico.Query.CotaDia);
                posicaoCotistaHistorico.Query.Where(posicaoCotistaHistorico.Query.IdPosicao.Equal(idPosicao) &
                                                    posicaoCotistaHistorico.Query.DataHistorico.Equal(dataHistorico));

                posicaoCotistaHistorico.Query.Load();

                cotaDia = posicaoCotistaHistorico.CotaDia.GetValueOrDefault(0);
                cotaAplicacao = posicaoCotistaHistorico.CotaAplicacao.GetValueOrDefault(0);
                quantidade = posicaoCotistaHistorico.Quantidade.GetValueOrDefault(0);
                quantidadeAntesCortes = posicaoCotistaHistorico.QuantidadeAntesCortes.GetValueOrDefault(0);
                rendimentoBrutoAplicacao = quantidade * (cotaDia - cotaAplicacao);

            }
            else if (tipoPosicao == (int)TipoComeCotas.Fundo)
            {
                PosicaoFundoHistorico posicaoFundoHistorico = new PosicaoFundoHistorico();
                posicaoFundoHistorico.Query.Select(posicaoFundoHistorico.Query.Quantidade, posicaoFundoHistorico.Query.QuantidadeAntesCortes, posicaoFundoHistorico.Query.CotaAplicacao, posicaoFundoHistorico.Query.CotaDia);
                posicaoFundoHistorico.Query.Where(posicaoFundoHistorico.Query.IdPosicao.Equal(idPosicao) &
                                                  posicaoFundoHistorico.Query.DataHistorico.Equal(dataHistorico));

                posicaoFundoHistorico.Query.Load();

                cotaDia = posicaoFundoHistorico.CotaDia.GetValueOrDefault(0);
                cotaAplicacao = posicaoFundoHistorico.CotaAplicacao.GetValueOrDefault(0);
                quantidade = posicaoFundoHistorico.Quantidade.GetValueOrDefault(0);
                quantidadeAntesCortes = posicaoFundoHistorico.QuantidadeAntesCortes.GetValueOrDefault(0);
                rendimentoBrutoAplicacao = quantidade * (cotaDia - cotaAplicacao);
            }

            e.Result = idPosicao + "|" + cotaDia + "|" + cotaAplicacao + "|" + quantidade + "|" + quantidadeAntesCortes + "|" + rendimentoBrutoAplicacao;

        }
        else
        {
            e.Result = 0 + "|" + 0 + "|" + 0 + "|" + 0 + "|" + 0 + "|" + 0;
        }
    }
    #endregion

    #region Grid Cadastro
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        int idAgendaComeCotas = (int)e.Keys[0];

        #region Campos
        btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        textDataLancamento = gridCadastro.FindEditFormTemplateControl("textDataLancamento") as ASPxDateEdit;
        textDataVencimento = gridCadastro.FindEditFormTemplateControl("textDataVencimento") as ASPxDateEdit;
        textDataUltimoIR = gridCadastro.FindEditFormTemplateControl("textDataUltimoIR") as ASPxDateEdit;
        dropTipoPosicao = gridCadastro.FindEditFormTemplateControl("dropTipoPosicao") as ASPxComboBox;
        dropTipoAliquotaIR = gridCadastro.FindEditFormTemplateControl("dropTipoAliquotaIR") as ASPxComboBox;
        dropExecucaoRecolhimento = gridCadastro.FindEditFormTemplateControl("dropExecucaoRecolhimento") as ASPxComboBox;
        dropTipoEvento = gridCadastro.FindEditFormTemplateControl("dropTipoEvento") as ASPxComboBox;
        hiddenIdPosicao = gridCadastro.FindEditFormTemplateControl("hiddenIdPosicao") as ASPxTextBox;
        textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
        textQuantidadeAntesCortes = gridCadastro.FindEditFormTemplateControl("textQuantidadeAntesCortes") as ASPxSpinEdit;
        textValorCotaAplic = gridCadastro.FindEditFormTemplateControl("textValorCotaAplic") as ASPxSpinEdit;
        textValorCota = gridCadastro.FindEditFormTemplateControl("textValorCota") as ASPxSpinEdit;
        textValorCotaUltimoPagamentoIR = gridCadastro.FindEditFormTemplateControl("textValorCotaUltimoPagamentoIR") as ASPxSpinEdit;
        textAliquotaCC = gridCadastro.FindEditFormTemplateControl("textAliquotaCC") as ASPxSpinEdit;
        textRendimentoBrutoDesdeAplicacao = gridCadastro.FindEditFormTemplateControl("textRendimentoBrutoDesdeAplicacao") as ASPxSpinEdit;
        textRendimentoDesdeUltimoPagamentoIR = gridCadastro.FindEditFormTemplateControl("textRendimentoDesdeUltimoPagamentoIR") as ASPxSpinEdit;
        textPrazoIOF = gridCadastro.FindEditFormTemplateControl("textPrazoIOF") as ASPxSpinEdit;
        textAliquotaIOF = gridCadastro.FindEditFormTemplateControl("textAliquotaIOF") as ASPxSpinEdit;
        textValorIOF = gridCadastro.FindEditFormTemplateControl("textValorIOF") as ASPxSpinEdit;
        textValorIOFVirtual = gridCadastro.FindEditFormTemplateControl("textValorIOFVirtual") as ASPxSpinEdit;
        textNumDiasCorridosDesdeAquisicao = gridCadastro.FindEditFormTemplateControl("textNumDiasCorridosDesdeAquisicao") as ASPxSpinEdit;
        textPrejuizoUsado = gridCadastro.FindEditFormTemplateControl("textPrejuizoUsado") as ASPxSpinEdit;
        textRendimentoCompensado = gridCadastro.FindEditFormTemplateControl("textRendimentoCompensado") as ASPxSpinEdit;
        textValorIRAgendado = gridCadastro.FindEditFormTemplateControl("textValorIRAgendado") as ASPxSpinEdit;
        textResiduo15 = gridCadastro.FindEditFormTemplateControl("textResiduo15") as ASPxSpinEdit;
        textResiduo175 = gridCadastro.FindEditFormTemplateControl("textResiduo175") as ASPxSpinEdit;
        textResiduo20 = gridCadastro.FindEditFormTemplateControl("textResiduo20") as ASPxSpinEdit;
        textResiduo225 = gridCadastro.FindEditFormTemplateControl("textResiduo225") as ASPxSpinEdit;
        textValorIRPago = gridCadastro.FindEditFormTemplateControl("textValorIRPago") as ASPxSpinEdit;
        textQuantidadeComida = gridCadastro.FindEditFormTemplateControl("textQuantidadeComida") as ASPxSpinEdit;
        textQuantidadeFinal = gridCadastro.FindEditFormTemplateControl("textQuantidadeFinal") as ASPxSpinEdit;
        textAliquotaIR = gridCadastro.FindEditFormTemplateControl("textAliquotaIR") as ASPxSpinEdit;
        #endregion

        AgendaComeCotas agendaComeCotas = new AgendaComeCotas();

        if (agendaComeCotas.LoadByPrimaryKey(idAgendaComeCotas))
        {
            agendaComeCotas.IdCarteira = Convert.ToInt32(btnEditCodigoCliente.Text);
            agendaComeCotas.DataLancamento = Convert.ToDateTime(textDataLancamento.Text);
            agendaComeCotas.DataVencimento = Convert.ToDateTime(textDataVencimento.Text);
            agendaComeCotas.TipoEvento = Convert.ToInt32(dropTipoEvento.SelectedItem.Value);
            agendaComeCotas.IdPosicao = Convert.ToInt32(hiddenIdPosicao.Text);
            agendaComeCotas.DataUltimoIR = Convert.ToDateTime(textDataUltimoIR.Text);
            agendaComeCotas.Quantidade = Convert.ToDecimal(textQuantidade.Text);
            agendaComeCotas.QuantidadeAntesCortes = Convert.ToDecimal(textQuantidadeAntesCortes.Text);
            agendaComeCotas.ValorCotaAplic = Convert.ToDecimal(textValorCotaAplic.Text);
            agendaComeCotas.ValorCotaUltimoPagamentoIR = Convert.ToDecimal(textValorCotaUltimoPagamentoIR.Text);
            agendaComeCotas.ValorCota = Convert.ToDecimal(textValorCota.Text);
            agendaComeCotas.AliquotaCC = Convert.ToDecimal(textAliquotaCC.Text);
            agendaComeCotas.RendimentoBrutoDesdeAplicacao = Convert.ToDecimal(textRendimentoBrutoDesdeAplicacao.Text);
            agendaComeCotas.RendimentoDesdeUltimoPagamentoIR = Convert.ToDecimal(textRendimentoDesdeUltimoPagamentoIR.Text);
            agendaComeCotas.NumDiasCorridosDesdeAquisicao = Convert.ToInt32(textNumDiasCorridosDesdeAquisicao.Text);
            agendaComeCotas.PrazoIOF = Convert.ToInt32(textPrazoIOF.Text);
            agendaComeCotas.AliquotaIOF = Convert.ToDecimal(textAliquotaIOF.Text);
            agendaComeCotas.ValorIOF = Convert.ToDecimal(textValorIOF.Text);
            agendaComeCotas.ValorIOFVirtual = Convert.ToDecimal(textValorIOFVirtual.Text);
            agendaComeCotas.PrejuizoUsado = Convert.ToDecimal(textPrejuizoUsado.Text);
            agendaComeCotas.RendimentoCompensado = Convert.ToDecimal(textRendimentoCompensado.Text);
            agendaComeCotas.ValorIRAgendado = Convert.ToDecimal(textValorIRAgendado.Text);
            agendaComeCotas.ValorIRPago = Convert.ToDecimal(textValorIRPago.Text);
            agendaComeCotas.Residuo15 = Convert.ToDecimal(textResiduo15.Text);
            agendaComeCotas.Residuo175 = Convert.ToDecimal(textResiduo175.Text);
            agendaComeCotas.Residuo20 = Convert.ToDecimal(textResiduo20.Text);
            agendaComeCotas.Residuo225 = Convert.ToDecimal(textResiduo225.Text);
            agendaComeCotas.QuantidadeComida = Convert.ToDecimal(textQuantidadeComida.Text);
            agendaComeCotas.QuantidadeFinal = Convert.ToDecimal(textQuantidadeFinal.Text);
            agendaComeCotas.ExecucaoRecolhimento = Convert.ToInt32(dropExecucaoRecolhimento.SelectedItem.Value);
            agendaComeCotas.TipoAliquotaIR = Convert.ToInt32(dropTipoAliquotaIR.SelectedItem.Value);
            agendaComeCotas.AliquotaIR = Convert.ToInt32(textAliquotaIR.Text);
            agendaComeCotas.TipoPosicao = Convert.ToInt32(dropTipoPosicao.SelectedItem.Value);

            agendaComeCotas.Save();
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {

        #region Campos
        btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        textDataLancamento = gridCadastro.FindEditFormTemplateControl("textDataLancamento") as ASPxDateEdit;
        textDataVencimento = gridCadastro.FindEditFormTemplateControl("textDataVencimento") as ASPxDateEdit;
        textDataUltimoIR = gridCadastro.FindEditFormTemplateControl("textDataUltimoIR") as ASPxDateEdit;
        dropTipoPosicao = gridCadastro.FindEditFormTemplateControl("dropTipoPosicao") as ASPxComboBox;
        dropTipoAliquotaIR = gridCadastro.FindEditFormTemplateControl("dropTipoAliquotaIR") as ASPxComboBox;
        dropExecucaoRecolhimento = gridCadastro.FindEditFormTemplateControl("dropExecucaoRecolhimento") as ASPxComboBox;
        dropTipoEvento = gridCadastro.FindEditFormTemplateControl("dropTipoEvento") as ASPxComboBox;
        hiddenIdPosicao = gridCadastro.FindEditFormTemplateControl("hiddenIdPosicao") as ASPxTextBox;
        textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
        textQuantidadeAntesCortes = gridCadastro.FindEditFormTemplateControl("textQuantidadeAntesCortes") as ASPxSpinEdit;
        textValorCotaAplic = gridCadastro.FindEditFormTemplateControl("textValorCotaAplic") as ASPxSpinEdit;
        textValorCota = gridCadastro.FindEditFormTemplateControl("textValorCota") as ASPxSpinEdit;
        textValorCotaUltimoPagamentoIR = gridCadastro.FindEditFormTemplateControl("textValorCotaUltimoPagamentoIR") as ASPxSpinEdit;
        textAliquotaCC = gridCadastro.FindEditFormTemplateControl("textAliquotaCC") as ASPxSpinEdit;
        textRendimentoBrutoDesdeAplicacao = gridCadastro.FindEditFormTemplateControl("textRendimentoBrutoDesdeAplicacao") as ASPxSpinEdit;
        textRendimentoDesdeUltimoPagamentoIR = gridCadastro.FindEditFormTemplateControl("textRendimentoDesdeUltimoPagamentoIR") as ASPxSpinEdit;
        textPrazoIOF = gridCadastro.FindEditFormTemplateControl("textPrazoIOF") as ASPxSpinEdit;
        textAliquotaIOF = gridCadastro.FindEditFormTemplateControl("textAliquotaIOF") as ASPxSpinEdit;
        textValorIOF = gridCadastro.FindEditFormTemplateControl("textValorIOF") as ASPxSpinEdit;
        textValorIOFVirtual = gridCadastro.FindEditFormTemplateControl("textValorIOFVirtual") as ASPxSpinEdit;
        textNumDiasCorridosDesdeAquisicao = gridCadastro.FindEditFormTemplateControl("textNumDiasCorridosDesdeAquisicao") as ASPxSpinEdit;
        textPrejuizoUsado = gridCadastro.FindEditFormTemplateControl("textPrejuizoUsado") as ASPxSpinEdit;
        textRendimentoCompensado = gridCadastro.FindEditFormTemplateControl("textRendimentoCompensado") as ASPxSpinEdit;
        textValorIRAgendado = gridCadastro.FindEditFormTemplateControl("textValorIRAgendado") as ASPxSpinEdit;
        textResiduo15 = gridCadastro.FindEditFormTemplateControl("textResiduo15") as ASPxSpinEdit;
        textResiduo175 = gridCadastro.FindEditFormTemplateControl("textResiduo175") as ASPxSpinEdit;
        textResiduo20 = gridCadastro.FindEditFormTemplateControl("textResiduo20") as ASPxSpinEdit;
        textResiduo225 = gridCadastro.FindEditFormTemplateControl("textResiduo225") as ASPxSpinEdit;
        textValorIRPago = gridCadastro.FindEditFormTemplateControl("textValorIRPago") as ASPxSpinEdit;
        textQuantidadeComida = gridCadastro.FindEditFormTemplateControl("textQuantidadeComida") as ASPxSpinEdit;
        textQuantidadeFinal = gridCadastro.FindEditFormTemplateControl("textQuantidadeFinal") as ASPxSpinEdit;
        textAliquotaIR = gridCadastro.FindEditFormTemplateControl("textAliquotaIR") as ASPxSpinEdit;
        #endregion

        AgendaComeCotas agendaComeCotas = new AgendaComeCotas();
        agendaComeCotas.IdCarteira = Convert.ToInt32(btnEditCodigoCliente.Text);
        agendaComeCotas.DataLancamento = Convert.ToDateTime(textDataLancamento.Text);
        agendaComeCotas.DataVencimento = Convert.ToDateTime(textDataVencimento.Text);
        agendaComeCotas.TipoEvento = Convert.ToInt32(dropTipoEvento.SelectedItem.Value);
        agendaComeCotas.IdPosicao = Convert.ToInt32(hiddenIdPosicao.Text);
        agendaComeCotas.DataUltimoIR = Convert.ToDateTime(textDataUltimoIR.Text);
        agendaComeCotas.Quantidade = Convert.ToDecimal(textQuantidade.Text);
        agendaComeCotas.QuantidadeAntesCortes = Convert.ToDecimal(textQuantidadeAntesCortes.Text);
        agendaComeCotas.ValorCotaAplic = Convert.ToDecimal(textValorCotaAplic.Text);
        agendaComeCotas.ValorCotaUltimoPagamentoIR = Convert.ToDecimal(textValorCotaUltimoPagamentoIR.Text);
        agendaComeCotas.ValorCota = Convert.ToDecimal(textValorCota.Text);
        agendaComeCotas.AliquotaCC = Convert.ToDecimal(textAliquotaCC.Text);
        agendaComeCotas.RendimentoBrutoDesdeAplicacao = Convert.ToDecimal(textRendimentoBrutoDesdeAplicacao.Text);
        agendaComeCotas.RendimentoDesdeUltimoPagamentoIR = Convert.ToDecimal(textRendimentoDesdeUltimoPagamentoIR.Text);
        agendaComeCotas.NumDiasCorridosDesdeAquisicao = Convert.ToInt32(textNumDiasCorridosDesdeAquisicao.Text);
        agendaComeCotas.PrazoIOF = Convert.ToInt32(textPrazoIOF.Text);
        agendaComeCotas.AliquotaIOF = Convert.ToDecimal(textAliquotaIOF.Text);
        agendaComeCotas.ValorIOF = Convert.ToDecimal(textValorIOF.Text);
        agendaComeCotas.ValorIOFVirtual = Convert.ToDecimal(textValorIOFVirtual.Text);
        agendaComeCotas.PrejuizoUsado = Convert.ToDecimal(textPrejuizoUsado.Text);
        agendaComeCotas.RendimentoCompensado = Convert.ToDecimal(textRendimentoCompensado.Text);
        agendaComeCotas.ValorIRAgendado = Convert.ToDecimal(textValorIRAgendado.Text);
        agendaComeCotas.ValorIRPago = Convert.ToDecimal(textValorIRPago.Text);
        agendaComeCotas.Residuo15 = Convert.ToDecimal(textResiduo15.Text);
        agendaComeCotas.Residuo175 = Convert.ToDecimal(textResiduo175.Text);
        agendaComeCotas.Residuo20 = Convert.ToDecimal(textResiduo20.Text);
        agendaComeCotas.Residuo225 = Convert.ToDecimal(textResiduo225.Text);
        agendaComeCotas.QuantidadeComida = Convert.ToDecimal(textQuantidadeComida.Text);
        agendaComeCotas.QuantidadeFinal = Convert.ToDecimal(textQuantidadeFinal.Text);
        agendaComeCotas.ExecucaoRecolhimento = Convert.ToInt32(dropExecucaoRecolhimento.SelectedItem.Value);
        agendaComeCotas.TipoAliquotaIR = Convert.ToInt32(dropTipoAliquotaIR.SelectedItem.Value);
        agendaComeCotas.AliquotaIR = Convert.ToInt32(textAliquotaIR.Text);
        agendaComeCotas.TipoPosicao = Convert.ToInt32(dropTipoPosicao.SelectedItem.Value);

        agendaComeCotas.Save();

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            #region Delete
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdAgendamentoComeCotas");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idAgendamentoComeCotas = Convert.ToInt32(keyValuesId[i]);

                AgendaComeCotas agendaComeCotas = new AgendaComeCotas();
                if (agendaComeCotas.LoadByPrimaryKey(idAgendamentoComeCotas))
                {
                    //
                    AgendaComeCotas agendaComeCotasClone = (AgendaComeCotas)Utilitario.Clone(agendaComeCotas);
                    //

                    agendaComeCotas.MarkAsDeleted();
                    agendaComeCotas.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de AgendaComeCotas - Operacao: Delete AgendaComeCotas: " + idAgendamentoComeCotas + UtilitarioWeb.ToString(agendaComeCotasClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
            #endregion
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }
    #endregion

    /// <summary>
    /// Seleciona dados Cotista/Fundo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        int tipoPosicao = Convert.ToInt32(e.GetListSourceFieldValue(AgendaComeCotasMetadata.ColumnNames.TipoPosicao));
        int idPosicao = Convert.ToInt32(e.GetListSourceFieldValue(AgendaComeCotasMetadata.ColumnNames.IdPosicao));

        if (e.Column.FieldName == "IdCotistaFundo")
        {
            if (tipoPosicao.Equals((int)TipoComeCotas.Cotista))
            {
                PosicaoCotista posicaoCotista = new PosicaoCotista();
                posicaoCotista.LoadByPrimaryKey(idPosicao);
                e.Value = posicaoCotista.IdCotista.ToString();
            }
            else
            {
                PosicaoFundo posicaoFundo = new PosicaoFundo();
                posicaoFundo.LoadByPrimaryKey(idPosicao);
                e.Value = posicaoFundo.IdCarteira.ToString();
            }
        }

        if (e.Column.FieldName == "NomeCotistaFundo")
        {
            if (tipoPosicao.Equals((int)TipoComeCotas.Cotista))
            {
                PosicaoCotista posicaoCotista = new PosicaoCotista();
                int idCotista = 0;
                if (posicaoCotista.LoadByPrimaryKey(idPosicao))
                    idCotista = (int)posicaoCotista.IdCotista;
                Cotista cotista = new Cotista();
                cotista.LoadByPrimaryKey(idCotista);
                e.Value = cotista.Nome;
            }
            else
            {
                PosicaoFundo posicaoFundo = new PosicaoFundo();
                int idCarteira = 0;
                if(posicaoFundo.LoadByPrimaryKey(idPosicao))
                    idCarteira = (int)posicaoFundo.IdCarteira;
                Carteira carteira = new Carteira();
                carteira.LoadByPrimaryKey(idCarteira);
                e.Value = carteira.Nome;
            }
        }

        if (e.Column.FieldName == "DataAplicacao")
        {
            if (tipoPosicao.Equals((int)TipoComeCotas.Cotista))
            {
                PosicaoCotista posicaoCotista = new PosicaoCotista();
                posicaoCotista.LoadByPrimaryKey(idPosicao);
                e.Value = posicaoCotista.DataAplicacao;
            }
            else
            {
                PosicaoFundo posicaoFundo = new PosicaoFundo();
                posicaoFundo.LoadByPrimaryKey(idPosicao);
                e.Value = posicaoFundo.DataAplicacao;
            }
        }

        
                
    }


    #region Load
    new protected void btnEditPosicao_Load(object sender, EventArgs e)
    {
        (sender as ASPxButtonEdit).ClientEnabled = gridCadastro.IsNewRowEditing;
    }

    new protected void dropTipoPosicao_Load(object sender, EventArgs e)
    {
        (sender as ASPxComboBox).ClientEnabled = gridCadastro.IsNewRowEditing;
    }

    new protected void btnEditCodigoCliente_Load(object sender, EventArgs e)
    {
        (sender as ASPxSpinEdit).ClientEnabled = gridCadastro.IsNewRowEditing;
    }

    new protected void labelDataLancamento_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as ASPxLabel).Text = "Data Lançamento:";
    }
    #endregion
}