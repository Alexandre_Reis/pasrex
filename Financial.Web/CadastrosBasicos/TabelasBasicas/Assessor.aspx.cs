using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Web.Common;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_Assessor : CadastroBasePage {

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSAssessor_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        AssessorCollection coll = new AssessorCollection();

        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        Assessor assessor = new Assessor();
        int idAssessor = (int)e.Keys[0];

        if (assessor.LoadByPrimaryKey(idAssessor)) {
            assessor.Apelido = Convert.ToString(e.NewValues[AssessorMetadata.ColumnNames.Apelido]);
            assessor.Nome = Convert.ToString(e.NewValues[AssessorMetadata.ColumnNames.Nome]);

            assessor.Save();
           
            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de Assessor - Operacao: Update Assessor: " + idAssessor + UtilitarioWeb.ToString(assessor),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }
       
        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        Assessor assessor = new Assessor();

        assessor.Apelido = Convert.ToString(e.NewValues[AssessorMetadata.ColumnNames.Apelido]);
        assessor.Nome = Convert.ToString(e.NewValues[AssessorMetadata.ColumnNames.Nome]);
        assessor.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de Assessor - Operacao: Insert Assessor: " + assessor.IdAssessor + UtilitarioWeb.ToString(assessor),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(AssessorMetadata.ColumnNames.IdAssessor);
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idAssessor = Convert.ToInt32(keyValuesId[i]);

                Assessor assessor = new Assessor();
                if (assessor.LoadByPrimaryKey(idAssessor)) {

                    Assessor assessorClone = (Assessor)Utilitario.Clone(assessor);
                    //
                    assessor.MarkAsDeleted();
                    assessor.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de Assessor - Operacao: Delete Assessor: " + idAssessor + UtilitarioWeb.ToString(assessorClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }
}