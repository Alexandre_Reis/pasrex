﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using DevExpress.Web;
using Financial.RendaFixa;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Util;
using System.Drawing;
using Financial.Web.Common;
using Financial.Common;
using Financial.Fundo;

public partial class CadastrosBasicos_PerfilClienteFaixaRating : CadastroBasePage
{
    #region Instância
    //Collection
    PerfilClienteFaixaRatingCollection perfilClienteFaixaRatingCollection = new PerfilClienteFaixaRatingCollection();
    PerfilClienteCollection perfilClienteCollection = new PerfilClienteCollection();
    PerfilFaixaRatingCollection perfilFaixaRatingCollection = new PerfilFaixaRatingCollection();
    CarteiraCollection carteiraCollection = new CarteiraCollection();
    //Entidade
    PerfilClienteFaixaRating perfilClienteFaixaRating = new PerfilClienteFaixaRating();
    #endregion

    #region EntitySpace - Base de Dados
    /// <summary>
    /// Carrega grid do perfil cliente
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSPerfilCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        perfilClienteFaixaRatingCollection = new PerfilClienteFaixaRatingCollection();
        perfilClienteFaixaRatingCollection.Query.OrderBy(perfilClienteFaixaRatingCollection.Query.IdPerfilClienteFaixaRating.Ascending);
        perfilClienteFaixaRatingCollection.LoadAll();

        e.Collection = perfilClienteFaixaRatingCollection;
    }

    /// <summary>
    /// Carrega grid do perfil cliente
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        carteiraCollection = new CarteiraCollection();
        carteiraCollection.Query.Select(carteiraCollection.Query.IdCarteira, carteiraCollection.Query.Nome);
        carteiraCollection.Query.OrderBy(carteiraCollection.Query.IdCarteira.Ascending);
        carteiraCollection.LoadAll();

        e.Collection = carteiraCollection;
    }

    /// <summary>
    /// Carrega grid do perfil cliente
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSPerfilFaixaRating_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        perfilFaixaRatingCollection = new PerfilFaixaRatingCollection();
        perfilFaixaRatingCollection.Query.Select(perfilFaixaRatingCollection.Query.IdPerfilFaixaRating,
                                                (perfilFaixaRatingCollection.Query.CodigoPerfil.Cast(EntitySpaces.Interfaces.esCastType.String) + " - " + perfilFaixaRatingCollection.Query.DescricaoPerfil).As("DescricaoPerfil"));
        perfilFaixaRatingCollection.Query.OrderBy(perfilFaixaRatingCollection.Query.IdPerfilFaixaRating.Ascending);
        perfilFaixaRatingCollection.LoadAll();

        e.Collection = perfilFaixaRatingCollection;
    }
    #endregion

    #region GridView Evento
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        perfilClienteFaixaRating = new PerfilClienteFaixaRating();

        int IdPerfilClienteFaixaRating = (int)e.Keys[0];
        if (perfilClienteFaixaRating.LoadByPrimaryKey(IdPerfilClienteFaixaRating))
        {
            perfilClienteFaixaRating = Financial.Util.CadastroDinamico.PreencheEntidade<PerfilClienteFaixaRating>(gridCadastro.FindEditFormTemplateControl("panelEdicao").Controls, perfilClienteFaixaRating);
            perfilClienteFaixaRating.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de Perfil Cliente Faixa Rating - Operacao: Update Perfil Cliente Faixa Rating: " + IdPerfilClienteFaixaRating + UtilitarioWeb.ToString(perfilClienteFaixaRating),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        //Salva as alterações da carteira
        SalvaCliente(IdPerfilClienteFaixaRating);

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        //Armazena o id do perfil cadastrado
        int idPerfilClienteFaixaRating = Cadastra();
        //Salva as carteiras correspondentes ao perfil criado
        SalvaCliente(idPerfilClienteFaixaRating);

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdPerfilClienteFaixaRating");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                perfilClienteFaixaRating = new PerfilClienteFaixaRating();
                int idPerfilClienteFaixaRating = Convert.ToInt32(keyValuesId[i]);

                if (perfilClienteFaixaRating.LoadByPrimaryKey(idPerfilClienteFaixaRating))
                {
                    #region Deleta Clientes
                    PerfilClienteCollection coll = new PerfilClienteCollection();
                    coll.Query.Where(coll.Query.IdPerfilClienteFaixaRating.Equal(idPerfilClienteFaixaRating));
                    coll.LoadAll();
                    coll.MarkAllAsDeleted();
                    coll.Save();
                    #endregion
                    
                    PerfilClienteFaixaRating perfilClienteFaixaRatingClone = (PerfilClienteFaixaRating)Utilitario.Clone(perfilClienteFaixaRating);

                    perfilClienteFaixaRating.MarkAsDeleted();
                    perfilClienteFaixaRating.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de Perfil Cliente Faixa Rating - Operacao: Delete Perfil Cliente Faixa Rating: " + idPerfilClienteFaixaRating + UtilitarioWeb.ToString(perfilClienteFaixaRatingClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }
       

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// Exibe formatação de cnpj na grid
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {   
        if (e.Column.FieldName == "IdPerfilFaixaRating")
        {
            if (e.Value != null)
            {
                perfilFaixaRatingCollection = new PerfilFaixaRatingCollection();

                perfilFaixaRatingCollection.Query.Where(perfilFaixaRatingCollection.Query.IdPerfilFaixaRating == Convert.ToInt16(e.Value));
                perfilFaixaRatingCollection.Query.Load();

                e.DisplayText = perfilFaixaRatingCollection[0].CodigoPerfil + " - " + perfilFaixaRatingCollection[0].DescricaoPerfil;
            }
        }
    }
    #endregion

    #region Callback Evento
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
    }

    /// <summary>
    /// Efetua o cadastro - Não fecha o EditForm
    /// </summary>
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        int idPerfilClienteFaixaRating = Cadastra();
        SalvaCliente(idPerfilClienteFaixaRating);

        e.Result = "Cadastro feito com sucesso.";
    }
    #endregion

    #region Button Evento
    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }
    #endregion

    #region Suporte
    /// <summary>
    /// Efetua o cadastro dos dados da tela
    /// </summary>
    private int Cadastra()
    {
        //Instancia
        perfilClienteFaixaRating = new PerfilClienteFaixaRating();

        //Realiza o tratamento para o cnpj
        perfilClienteFaixaRating = Financial.Util.CadastroDinamico.PreencheEntidade<PerfilClienteFaixaRating>(gridCadastro.FindEditFormTemplateControl("panelEdicao").Controls, null);
        perfilClienteFaixaRating.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de Agencia Classificadora - Operacao: Insert Agencia Classificadora: " + perfilClienteFaixaRating.IdPerfilClienteFaixaRating + UtilitarioWeb.ToString(perfilClienteFaixaRating),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        return perfilClienteFaixaRating.IdPerfilClienteFaixaRating.Value;
    }

    /// <summary>
    /// Salva as Clientes vinculados ao perfil cadastrado
    /// </summary>
    private void SalvaCliente(int idPerfilClienteFaixaRating)
    {
        #region Deleta Clientes
        PerfilClienteCollection coll = new PerfilClienteCollection();
        coll.Query.Where(coll.Query.IdPerfilClienteFaixaRating.Equal(idPerfilClienteFaixaRating));
        coll.LoadAll();
        coll.MarkAllAsDeleted();
        coll.Save();
        #endregion

        #region Inclui Clientes
        PerfilCliente perfilCliente = new PerfilCliente();
        ASPxGridLookup gridLookupCliente = gridCadastro.FindEditFormTemplateControl("gridLookupCliente") as ASPxGridLookup;
        List<object> clientesSelecionados = new List<object>();
        clientesSelecionados = gridLookupCliente.GridView.GetSelectedFieldValues(CarteiraMetadata.ColumnNames.IdCarteira);

        foreach (object item in clientesSelecionados)
        {
            perfilCliente = new PerfilCliente();
            perfilCliente.IdPerfilClienteFaixaRating = idPerfilClienteFaixaRating;
            perfilCliente.IdCarteira = Convert.ToInt32(item);
            perfilCliente.Save();
        }
        #endregion
    }
    #endregion

    #region OnDataBound
    /// <summary>
    /// Carrega os items selecionados para a empresa securitizada
    /// </summary>
    protected void gridLookupCliente_OnDataBound(object source, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
        {
            ASPxTextBox hiddenIdPerfilClienteFaixaRating = gridCadastro.FindEditFormTemplateControl("hiddenIdPerfilClienteFaixaRating") as ASPxTextBox;
            int IdPerfilFaixaRating = Convert.ToInt32(hiddenIdPerfilClienteFaixaRating.Text);
            
            ASPxGridLookup gridLookupCliente = gridCadastro.FindEditFormTemplateControl("gridLookupCliente") as ASPxGridLookup;

            if (string.IsNullOrEmpty(gridLookupCliente.Text))
            {
                perfilClienteCollection = new PerfilClienteCollection();
                perfilClienteCollection.Query.Where(perfilClienteCollection.Query.IdPerfilClienteFaixaRating.Equal(IdPerfilFaixaRating));
                perfilClienteCollection.Query.Load();

                foreach (PerfilCliente item in perfilClienteCollection)
                {
                    gridLookupCliente.GridView.Selection.SelectRowByKey(item.IdCarteira);
                }
            }
        }
    }
    #endregion
}