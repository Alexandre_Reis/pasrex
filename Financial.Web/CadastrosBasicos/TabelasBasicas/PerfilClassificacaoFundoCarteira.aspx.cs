﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using Financial.Investidor;
using Financial.Investidor.Enums;
using System.Text;
using DevExpress.Web;
using DevExpress.Utils;
using System.Drawing;
using Financial.Fundo;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_PerfilClassificacaoFundoCarteira : Financial.Web.Common.CadastroBasePage
{
    new protected void Page_Load(object sender, EventArgs e)
    {
        this.AllowUpdate = false;
        base.Page_Load(sender, e);

        if (IsPostBack)
            AtualizarGridCarteiras();
    }

    protected void EsDSPerfilClassificacaoFundo_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        PerfilClassificacaoFundoCollection coll = new PerfilClassificacaoFundoCollection();

        coll.Query.OrderBy(coll.Query.Codigo.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSPerfilClassificacaoFundoCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        int idPerfil = 0;

        if (ddlPerfil.SelectedIndex >= 0)
            idPerfil = Convert.ToInt32(ddlPerfil.SelectedItem.Value);

        PerfilClassificacaoFundoCarteiraCollection coll = new PerfilClassificacaoFundoCarteiraCollection();

        PerfilClassificacaoFundoCarteiraQuery perfilClassificacaoFundoCarteira = new PerfilClassificacaoFundoCarteiraQuery("PC");
        CarteiraQuery carteira = new CarteiraQuery("C");

        perfilClassificacaoFundoCarteira.Select(perfilClassificacaoFundoCarteira.IdPerfilClassificacaoFundoCarteira, perfilClassificacaoFundoCarteira.InicioVigenciaPerfil.As("InicioVigenciaNoPerfil"), carteira.Apelido.As("Fundo/Carteira"));
        perfilClassificacaoFundoCarteira.InnerJoin(carteira).On(perfilClassificacaoFundoCarteira.IdCarteira == carteira.IdCarteira);
        perfilClassificacaoFundoCarteira.Where(perfilClassificacaoFundoCarteira.IdPerfilClassificacao == idPerfil);
        perfilClassificacaoFundoCarteira.OrderBy(carteira.Apelido.Ascending);

        coll.Load(perfilClassificacaoFundoCarteira);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdPerfilClassificacaoFundoCarteira");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idPerfil = Convert.ToInt32(keyValuesId[i]);

                PerfilClassificacaoFundoCarteira perfilClassificacaoFundoCarteira = new PerfilClassificacaoFundoCarteira();
                if (perfilClassificacaoFundoCarteira.LoadByPrimaryKey(idPerfil))
                {
                    PerfilClassificacaoFundoCarteira perfilClassificacaoFundoCarteiraClone = (PerfilClassificacaoFundoCarteira)Utilitario.Clone(perfilClassificacaoFundoCarteira);

                    perfilClassificacaoFundoCarteiraClone.MarkAsDeleted();
                    perfilClassificacaoFundoCarteiraClone.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de PerfilClassificacaoFundoCarteira - Operacao: Delete PerfilClassificacaoFundoCarteira: " + idPerfil + UtilitarioWeb.ToString(perfilClassificacaoFundoCarteira),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    protected void ddlCarteira_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlCarteira.SelectedIndex >= 0 && ddlPerfil.SelectedIndex >= 0)
        {
            int idCarteira = Convert.ToInt32(ddlCarteira.SelectedItem.Value);
            DateTime dtInicioPerfil = txtInicioVigencia.Date;

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            txtInicioFundo.Date = carteira.DataInicioCota.Value;

            if (dtInicioPerfil > carteira.DataInicioCota)
                txtInicioPerfil.Date = dtInicioPerfil;
            else
                txtInicioPerfil.Date = carteira.DataInicioCota.Value;
        }
    }

    protected void btnIncluir_Click(object sender, EventArgs e)
    {
        if (ddlPerfil.SelectedIndex < 0 || ddlCarteira.SelectedIndex < 0 || txtInicioPerfil.Text == string.Empty)
            throw new Exception("Campos com * são obrigatórios!");

        if (txtInicioPerfil.Date < txtInicioFundo.Date)
            throw new Exception("A data de início de vigência no perfil deve ser posterior à data de início do fundo/carteira!");

        if (txtInicioPerfil.Date < txtInicioVigencia.Date)
            throw new Exception("A data de início de vigência no perfil deve ser posterior à data de início do perfil!");

        PerfilClassificacaoFundoCarteira perfilClassificacaoFundoCarteira = new PerfilClassificacaoFundoCarteira();
        perfilClassificacaoFundoCarteira.IdCarteira = Convert.ToInt32(ddlCarteira.SelectedItem.Value);
        perfilClassificacaoFundoCarteira.IdPerfilClassificacao = Convert.ToInt32(ddlPerfil.SelectedItem.Value);
        perfilClassificacaoFundoCarteira.InicioVigenciaPerfil = txtInicioPerfil.Date;

        perfilClassificacaoFundoCarteira.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de PerfilClassificacaoFundoCarteira - Operacao: Insert PerfilClassificacaoFundoCarteira: " + ddlPerfil.SelectedItem.Value + UtilitarioWeb.ToString(perfilClassificacaoFundoCarteira),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        AtualizarGridCarteiras();
        CarregarCarteiras();
    }

    protected void ddlPerfil_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlPerfil.SelectedIndex >= 0)
        {
            int idPerfil = Convert.ToInt32(ddlPerfil.SelectedItem.Value);

            PerfilClassificacaoFundo perfilClassificacaoFundo = new PerfilClassificacaoFundo();
            perfilClassificacaoFundo.LoadByPrimaryKey(idPerfil);

            txtDescricaoPerfil.Text = perfilClassificacaoFundo.Descricao;
            txtInicioVigencia.Text = perfilClassificacaoFundo.InicioVigencia.Value.ToString("dd/MM/yyyy");

            CarregarCarteiras();
        }
        else
        {
            txtDescricaoPerfil.Text = string.Empty;
            txtInicioVigencia.Text = string.Empty;
        }

        AtualizarGridCarteiras();
        ddlCarteira.SelectedIndex = -1;
        txtInicioFundo.Text = string.Empty;
        txtInicioPerfil.Text = string.Empty;
    }

    private void AtualizarGridCarteiras()
    {
        gridCadastro.DataBind();
    }

    private void CarregarCarteiras()
    {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        CarteiraQuery carteiraQuery = new CarteiraQuery("C");
        ClienteQuery clienteQuery = new ClienteQuery("L");
        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
        ClassesOffShoreQuery classesOffShoreQuery = new ClassesOffShoreQuery("CO");
        CarteiraComplementoQuery carteiraComplementoQuery = new CarteiraComplementoQuery("CC");
        PerfilClassificacaoFundoCarteiraQuery perfilClassificacaoFundoCarteiraQuery = new PerfilClassificacaoFundoCarteiraQuery("PC");

        carteiraQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == carteiraQuery.IdCarteira);
        carteiraQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        carteiraQuery.LeftJoin(classesOffShoreQuery).On(carteiraQuery.IdCarteira.Equal(classesOffShoreQuery.IdClassesOffShore));
        carteiraQuery.LeftJoin(carteiraComplementoQuery).On(carteiraQuery.IdCarteira.Equal(carteiraComplementoQuery.IdCarteira));

        carteiraQuery.LeftJoin(perfilClassificacaoFundoCarteiraQuery).On(carteiraQuery.IdCarteira.Equal(perfilClassificacaoFundoCarteiraQuery.IdCarteira));

        carteiraQuery.Where(permissaoClienteQuery.IdUsuario.Equal(idUsuario),
                            clienteQuery.TipoControle.NotEqual((byte)TipoControleCliente.ApenasCotacao),
                            perfilClassificacaoFundoCarteiraQuery.IdCarteira.IsNull());

        CarteiraCollection coll = new CarteiraCollection();
        coll.Load(carteiraQuery);

        carteiraQuery = new CarteiraQuery("C");
        clienteQuery = new ClienteQuery("L");
        classesOffShoreQuery = new ClassesOffShoreQuery("CO");
        carteiraComplementoQuery = new CarteiraComplementoQuery("CC");
        carteiraComplementoQuery = new CarteiraComplementoQuery("CC");
        perfilClassificacaoFundoCarteiraQuery = new PerfilClassificacaoFundoCarteiraQuery("PC");

        carteiraQuery.Select(carteiraQuery, clienteQuery.IdTipo, classesOffShoreQuery, carteiraComplementoQuery);
        carteiraQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == carteiraQuery.IdCarteira);
        carteiraQuery.LeftJoin(classesOffShoreQuery).On(carteiraQuery.IdCarteira.Equal(classesOffShoreQuery.IdClassesOffShore));
        carteiraQuery.LeftJoin(carteiraComplementoQuery).On(carteiraQuery.IdCarteira.Equal(carteiraComplementoQuery.IdCarteira));

        carteiraQuery.LeftJoin(perfilClassificacaoFundoCarteiraQuery).On(carteiraQuery.IdCarteira.Equal(perfilClassificacaoFundoCarteiraQuery.IdCarteira));

        carteiraQuery.Where(clienteQuery.TipoControle.Equal((byte)TipoControleCliente.ApenasCotacao),
                            perfilClassificacaoFundoCarteiraQuery.IdCarteira.IsNull());

        CarteiraCollection coll2 = new CarteiraCollection();
        coll2.Load(carteiraQuery);

        coll.Combine(coll2);
        coll.Sort = "Apelido ASC";

        ddlCarteira.Items.Clear();
        ddlCarteira.DataSource = coll;
        ddlCarteira.TextField = "Apelido";
        ddlCarteira.ValueField = "IdCarteira";
        ddlCarteira.DataBind();

        ddlCarteira.SelectedIndex = -1;
        txtInicioFundo.Text = string.Empty;
        txtInicioPerfil.Text = string.Empty;
    }
}