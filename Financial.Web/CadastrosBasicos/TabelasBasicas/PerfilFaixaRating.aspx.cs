﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using DevExpress.Web;
using Financial.RendaFixa;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Util;
using System.Drawing;
using Financial.Web.Common;
using Financial.Common;
using DevExpress.Web.Data;

public partial class CadastrosBasicos_PerfilFaixaRating : CadastroBasePage
{
    #region Instância
    //Collection
    PerfilFaixaRatingCollection perfilFaixaRatingCollection = new PerfilFaixaRatingCollection();
    //Entidade
    PerfilFaixaRating perfilFaixaRating = new PerfilFaixaRating();
    #endregion

    #region EntitySpace - Base de Dados
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSPerfilFaixaRating_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        perfilFaixaRatingCollection = new PerfilFaixaRatingCollection();
        perfilFaixaRatingCollection.Query.OrderBy(perfilFaixaRatingCollection.Query.IdPerfilFaixaRating.Ascending);
        perfilFaixaRatingCollection.LoadAll();

        e.Collection = perfilFaixaRatingCollection;
    }
    #endregion

    #region GridView Evento
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        perfilFaixaRating = new PerfilFaixaRating();

        int IdPerfilFaixaRating = (int)e.Keys[0];
        if (perfilFaixaRating.LoadByPrimaryKey(IdPerfilFaixaRating))
        {
            perfilFaixaRating = Financial.Util.CadastroDinamico.PreencheEntidade<PerfilFaixaRating>(gridCadastro.FindEditFormTemplateControl("panelEdicao").Controls, perfilFaixaRating);
            perfilFaixaRating.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de Perfil Faixa Rating - Operacao: Update Perfil Faixa Rating: " + IdPerfilFaixaRating + UtilitarioWeb.ToString(perfilFaixaRating),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        Cadastra();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdPerfilFaixaRating");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                perfilFaixaRating = new PerfilFaixaRating();
                int IdPerfilFaixaRating = Convert.ToInt32(keyValuesId[i]);

                if (perfilFaixaRating.LoadByPrimaryKey(IdPerfilFaixaRating))
                {
                    PerfilFaixaRating perfilFaixaRatingClone = (PerfilFaixaRating)Utilitario.Clone(perfilFaixaRating);

                    perfilFaixaRating.MarkAsDeleted();
                    perfilFaixaRating.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de Perfil Faixa Rating - Operacao: Delete Perfil Faixa Rating: " + IdPerfilFaixaRating + UtilitarioWeb.ToString(perfilFaixaRatingClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// Fecha o EditForm e atualiza a grid
    /// Evento é chamado quando o usuário clica no botão para fechar a grid
    /// </summary>
    protected void gridCadastro_CancelRowEditing(object sender, ASPxStartRowEditingEventArgs e)
    {
        e.Cancel = false;
        gridCadastro.DataBind();
    }
    #endregion

    #region Callback Evento
    /// <summary>
    /// Realiza validação dos campos
    /// </summary>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
    }

    /// <summary>
    /// Efetua o cadastro - Não fecha o EditForm
    /// </summary>
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        Cadastra();

        e.Result = "Cadastro feito com sucesso.";
    }
    #endregion

    #region Button Evento
    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }
    #endregion

    #region Suporte
    /// <summary>
    /// Efetua o cadastro dos dados da tela
    /// </summary>
    private void Cadastra()
    {
        //Instancia
        perfilFaixaRating = new PerfilFaixaRating();

        //Realiza o tratamento para o cnpj
        perfilFaixaRating = Financial.Util.CadastroDinamico.PreencheEntidade<PerfilFaixaRating>(gridCadastro.FindEditFormTemplateControl("panelEdicao").Controls, null);
        perfilFaixaRating.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de Perfil Faixa Rating - Operacao: Insert Perfil Faixa Rating: " + perfilFaixaRating.IdPerfilFaixaRating + UtilitarioWeb.ToString(perfilFaixaRating),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }
    #endregion
}