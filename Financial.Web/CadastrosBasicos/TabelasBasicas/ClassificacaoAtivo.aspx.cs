﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using Financial.Investidor;
using Financial.ContaCorrente;
using System.Drawing;
using Financial.ContaCorrente.Enums;
using Financial.Security;
using Financial.Fundo;
using Financial.Bolsa;
using Financial.BMF;
using Financial.RendaFixa;
using Financial.Investidor.Enums;
using Financial.InvestidorCotista;
using Financial.CRM;
using Financial.WebConfigConfiguration;
using Financial.Security.Enums;
using System.Collections.Specialized;
using EntitySpaces.Interfaces;
using Financial.Web.Util;
using Financial.InvestidorCotista.Enums;
using Financial.Web.Common;
using System.Web.Configuration;
using System.Xml;
using Financial.Export;
using Financial.Interfaces.Export;
using DevExpress.Web.Data;
using System.ComponentModel;
using Financial.Fundo.Enums;
using DevExpress.XtraGrid;
using Financial.Common.Enums;

public partial class CadastrosBasicos_ClassificacaoAtivo : CadastroBasePage
{
    #region Instância
    //Collection
    ClassificacaoAtivoCollection classificacaoAtivoCollection = new ClassificacaoAtivoCollection();
    PadraoNotasCollection padraoNotasCollection = new PadraoNotasCollection();
    AgenciaClassificadoraCollection agenciaClassificadoraCollection = new AgenciaClassificadoraCollection();
    PerfilFaixaRatingCollection perfilFaixaRatingCollection = new PerfilFaixaRatingCollection();
    //QUERY
    ClassificacaoAtivoQuery classificacaoAtivoQuery = new ClassificacaoAtivoQuery();
    PadraoNotasQuery padraoNotasQuery = new PadraoNotasQuery();
    //Entidade
    ClassificacaoAtivo classificacaoAtivo = new ClassificacaoAtivo();
    #endregion

    #region esDataSources
    protected void EsDSClassificacaoAtivo_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        classificacaoAtivoCollection = new ClassificacaoAtivoCollection();
        classificacaoAtivoCollection.Query.Select();
        classificacaoAtivoCollection.Query.OrderBy(classificacaoAtivoCollection.Query.IdClassificacaoAtivo.Ascending);

        classificacaoAtivoCollection.Query.Load();

        e.Collection = classificacaoAtivoCollection;
    }
    protected void EsDSAgenciaClassificadora_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        agenciaClassificadoraCollection = new AgenciaClassificadoraCollection();

        agenciaClassificadoraCollection.Query.Select(agenciaClassificadoraCollection.Query.IdAgenciaClassificadora, (agenciaClassificadoraCollection.Query.CodigoAgencia.Cast(esCastType.String) + " - " + agenciaClassificadoraCollection.Query.DescricaoAgencia).As("DescricaoAgencia"));
        agenciaClassificadoraCollection.Query.OrderBy(agenciaClassificadoraCollection.Query.IdAgenciaClassificadora.Ascending);
        agenciaClassificadoraCollection.Query.Load();

        e.Collection = agenciaClassificadoraCollection;
    }
    protected void EsDSPerfilFaixaRating_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        perfilFaixaRatingCollection = new PerfilFaixaRatingCollection();

        perfilFaixaRatingCollection.Query.Select(perfilFaixaRatingCollection.Query.IdPerfilFaixaRating,
                                                (perfilFaixaRatingCollection.Query.CodigoPerfil.Cast(EntitySpaces.Interfaces.esCastType.String) + " - " + perfilFaixaRatingCollection.Query.DescricaoPerfil).As("DescricaoPerfil"));
        perfilFaixaRatingCollection.Query.OrderBy(perfilFaixaRatingCollection.Query.IdPerfilFaixaRating.Ascending);
        perfilFaixaRatingCollection.Query.Load();

        e.Collection = perfilFaixaRatingCollection;
    }
    protected void EsDSPadraoNota_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        padraoNotasCollection = new PadraoNotasCollection();

        padraoNotasCollection.Query.OrderBy(padraoNotasCollection.Query.IdPadraoNotas.Ascending);
        padraoNotasCollection.Query.Load();

        e.Collection = padraoNotasCollection;
    }
    #endregion

    #region gridCadastro
    /// <summary>
    /// Carrega os filtros da grid
    /// </summary>
    protected void gridCadastro_OnPreRender(object sender, EventArgs e)
    {
        #region Item Avaliado
        //Busca o controle de filtro da coluna CADASTRO da grid
        GridViewDataComboBoxColumn itemAvaliado = gridCadastro.Columns["ItemAvaliado"] as GridViewDataComboBoxColumn;
        //Percorre o ENUM para preencher o controle do combobox
        foreach (int r in Enum.GetValues(typeof(ListaItemAvaliado)))
        {
            itemAvaliado.PropertiesComboBox.Items.Add(ListaItemAvaliadoDescricao.RetornaStringValue(r), r.ToString());
        }
        #endregion

        #region Ativo
        //Busca o controle de filtro da coluna CADASTRO da grid
        GridViewDataComboBoxColumn ativo = gridCadastro.Columns["Ativo"] as GridViewDataComboBoxColumn;
        //Esconde o filtro
        ativo.Settings.AllowHeaderFilter = DevExpress.Utils.DefaultBoolean.False;
        #endregion
    }

    /// <summary>
    /// Efetua a alteração dos registros
    /// </summary>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        int idClassificacaoAtivo = (int)e.Keys[0];
        if (classificacaoAtivo.LoadByPrimaryKey(idClassificacaoAtivo))
        {
            //Preenche dinamicamente a entidade
            classificacaoAtivo = Financial.Util.CadastroDinamico.PreencheEntidade<ClassificacaoAtivo>(gridCadastro.FindEditFormTemplateControl("panelEdicao").Controls, classificacaoAtivo);
            classificacaoAtivo.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de Classificação Ativo - Operacao: Update Classificação Ativo: " + classificacaoAtivo.IdClassificacaoAtivo + UtilitarioWeb.ToString(classificacaoAtivo),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// Efetua o cadastro 
    /// </summary>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        this.Cadastra();

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// Fecha o EditForm e atualiza a grid
    /// Evento é chamado quando o usuário clica no botão para fechar a grid
    /// </summary>
    protected void gridCadastro_CancelRowEditing(object sender, ASPxStartRowEditingEventArgs e)
    {
        e.Cancel = false;
        gridCadastro.DataBind();
    }

    /// <summary>
    /// Altera o nome da coluna 
    /// </summary>
    protected void gridCadastro_OnCustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "Ativo")
        {
            int itemAvaliado = Convert.ToInt32(gridCadastro.GetRowValues(e.VisibleRowIndex, "ItemAvaliado"));

            if (itemAvaliado.Equals(1))
            {
                TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
                tituloRendaFixa.LoadByPrimaryKey(Convert.ToInt32(e.Value));
                e.DisplayText = tituloRendaFixa.IdTitulo + " - " + tituloRendaFixa.Descricao;
            }
            else if (itemAvaliado.Equals(2))
            {
                Carteira carteira = new Carteira();
                carteira.LoadByPrimaryKey(Convert.ToInt32(e.Value));
                e.DisplayText = carteira.Nome;
            }
            else if (itemAvaliado.Equals(3))
            {
                Emissor emissor = new Emissor();
                emissor.LoadByPrimaryKey(Convert.ToInt32(e.Value));
                e.DisplayText = emissor.Nome;
            }
        }
    }
    #endregion

    #region CallBack
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdClassificacaoAtivo");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                classificacaoAtivo = new ClassificacaoAtivo();
                int IdClassificacaoAtivo = Convert.ToInt32(keyValuesId[i]);

                if (classificacaoAtivo.LoadByPrimaryKey(IdClassificacaoAtivo))
                {
                    ClassificacaoAtivo classificacaoAtivoClone = (ClassificacaoAtivo)Utilitario.Clone(classificacaoAtivo);

                    classificacaoAtivo.MarkAsDeleted();
                    classificacaoAtivo.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de Classificação Ativo - Operacao: Delete Classificação Ativo: " + IdClassificacaoAtivo + UtilitarioWeb.ToString(classificacaoAtivoClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
    }

    /// <summary>
    /// Carregao dropdown de acordo com o valor de filtro da agencia
    /// </summary>
    protected void dropIdPadraoNotas_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
    {
        ASPxComboBox dropIdPadraoNotas = (ASPxComboBox)sender;
        ASPxComboBox dropItemAvaliado = gridCadastro.FindEditFormTemplateControl("dropItemAvaliado") as ASPxComboBox;

        if (dropItemAvaliado != null && dropItemAvaliado.SelectedItem != null)
        {
            padraoNotasCollection = new PadraoNotasCollection();
            padraoNotasCollection.Query.Where(padraoNotasCollection.Query.IdAgencia == Convert.ToInt32(e.Parameter), padraoNotasCollection.Query.ItemAvaliado == Convert.ToInt32(dropItemAvaliado.SelectedItem.Value));
            padraoNotasCollection.Query.Load();

            //Limpa lista para evitar dados duplicados
            dropIdPadraoNotas.Items.Clear();
            foreach (PadraoNotas item in padraoNotasCollection)
            {
                dropIdPadraoNotas.Items.Add(new ListEditItem(item.CodigoNota, item.IdPadraoNotas));
            }
        }
    }

    /// <summary>
    /// Carregao dropdown de acordo com o valor de filtro da agencia
    /// </summary>
    protected void dropAtivo_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
    {
        ASPxComboBox dropAtivo = (ASPxComboBox)sender;

        //Titulos
        if (e.Parameter.Equals("1"))
        {
            TituloRendaFixaCollection tituloRendaFixaCollection = new TituloRendaFixaCollection();

            tituloRendaFixaCollection.Query.OrderBy(tituloRendaFixaCollection.Query.IdTitulo.Ascending);
            tituloRendaFixaCollection.Query.Load();

            //Limpa lista para evitar dados duplicados
            dropAtivo.Items.Clear();
            foreach (TituloRendaFixa item in tituloRendaFixaCollection)
            {
                dropAtivo.Items.Add(new ListEditItem(item.IdTitulo + " - " + item.Descricao, item.IdTitulo));
            }
        }

        //Fundo de Investimento
        if (e.Parameter.Equals("2"))
        {
            CarteiraCollection carteiraCollection = new CarteiraCollection();

            carteiraCollection.Query.OrderBy(carteiraCollection.Query.IdCarteira.Ascending);
            carteiraCollection.Query.Load();

            //Limpa lista para evitar dados duplicados
            dropAtivo.Items.Clear();
            foreach (Carteira item in carteiraCollection)
            {
                dropAtivo.Items.Add(new ListEditItem(item.Nome, item.IdCarteira));
            }
        }

        //Emissor
        if (e.Parameter.Equals("3"))
        {
            EmissorCollection emissorCollection = new EmissorCollection();

            emissorCollection.Query.OrderBy(emissorCollection.Query.IdEmissor.Ascending);
            emissorCollection.Query.Load();

            //Limpa lista para evitar dados duplicados
            dropAtivo.Items.Clear();
            foreach (Emissor item in emissorCollection)
            {
                dropAtivo.Items.Add(new ListEditItem(item.Nome, item.IdEmissor));
            }
        }
    }
    #endregion

    #region Button
    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }
    #endregion

    #region OnLoad
    /// <summary>
    /// Carrega o controle com a lista de ENUM
    /// </summary>
    protected void dropItemAvaliado_OnLoad(object sender, EventArgs e)
    {
        //Busca o controle de filtro da coluna CADASTRO da grid
        ASPxComboBox dropItemAvaliado = (ASPxComboBox)sender;
        //Limpa controles para evitar items duplicados
        dropItemAvaliado.Items.Clear();
        //Percorre o ENUM para preencher o controle do combobox
        foreach (int r in Enum.GetValues(typeof(ListaItemAvaliado)))
        {
            dropItemAvaliado.Items.Add(ListaItemAvaliadoDescricao.RetornaStringValue(r), r.ToString());
        }
    }
    #endregion

    #region OnDataBound
    /// <summary>
    /// Carrega o controle quando solicitar o update
    /// </summary>
    protected void dropIdPadraoNotas_OnDataBound(object sender, EventArgs e)
    {
        ASPxComboBox dropIdPadraoNotas = (ASPxComboBox)sender;
        ASPxComboBox dropIdAgenciaClassificadora = gridCadastro.FindEditFormTemplateControl("dropIdAgenciaClassificadora") as ASPxComboBox;

        if (dropIdPadraoNotas != null && dropIdPadraoNotas.Items.Count == 0)
        {
            if (dropIdAgenciaClassificadora != null && dropIdAgenciaClassificadora.SelectedItem != null)
            {
                padraoNotasCollection = new PadraoNotasCollection();
                padraoNotasCollection.Query.OrderBy(padraoNotasCollection.Query.IdPadraoNotas.Ascending);
                padraoNotasCollection.Query.Where(padraoNotasCollection.Query.IdAgencia == Convert.ToInt32(dropIdAgenciaClassificadora.SelectedItem.Value));
                padraoNotasCollection.Query.Load();

                //Limpa lista para evitar dados duplicados
                dropIdPadraoNotas.Items.Clear();
                foreach (PadraoNotas item in padraoNotasCollection)
                {
                    dropIdPadraoNotas.Items.Add(new ListEditItem(item.CodigoNota, item.IdPadraoNotas));
                }
            }
        }
    }

    /// <summary>
    /// Quando usuário solicitar update do item
    /// </summary>
    protected void dropAtivo_OnDataBound(object sender, EventArgs e)
    {
        ASPxComboBox dropAtivo = (ASPxComboBox)sender;
        ASPxComboBox dropItemAvaliado = gridCadastro.FindEditFormTemplateControl("dropItemAvaliado") as ASPxComboBox;
        ASPxComboBox dropIdAgenciaClassificadora = gridCadastro.FindEditFormTemplateControl("dropIdAgenciaClassificadora") as ASPxComboBox;

        if (dropAtivo != null && dropAtivo.Items.Count == 0)
        {
            if (dropItemAvaliado != null &&
                dropItemAvaliado.SelectedItem != null &&
                dropIdAgenciaClassificadora != null &&
                dropIdAgenciaClassificadora.SelectedItem != null)
            {
                if (dropItemAvaliado.SelectedItem.Value.Equals("1"))
                {
                    TituloRendaFixaCollection tituloRendaFixaCollection = new TituloRendaFixaCollection();
                    tituloRendaFixaCollection.Query.OrderBy(tituloRendaFixaCollection.Query.IdTitulo.Ascending);
                    tituloRendaFixaCollection.Query.Load();

                    //Limpa lista para evitar dados duplicados
                    dropAtivo.Items.Clear();
                    foreach (TituloRendaFixa item in tituloRendaFixaCollection)
                    {
                        dropAtivo.Items.Add(new ListEditItem(item.IdTitulo + " - " + item.Descricao, item.IdTitulo));
                    }
                }
                else if (dropItemAvaliado.SelectedItem.Value.Equals("2"))
                {
                    CarteiraCollection carteiraCollection = new CarteiraCollection();
                    carteiraCollection.Query.OrderBy(carteiraCollection.Query.IdCarteira.Ascending);
                    carteiraCollection.Query.Load();

                    //Limpa lista para evitar dados duplicados
                    dropAtivo.Items.Clear();
                    foreach (Carteira item in carteiraCollection)
                    {
                        dropAtivo.Items.Add(new ListEditItem(item.Nome, item.IdCarteira));
                    }
                }
                else if (dropItemAvaliado.SelectedItem.Value.Equals("3"))
                {
                    EmissorCollection emissorCollection = new EmissorCollection();
                    emissorCollection.Query.OrderBy(emissorCollection.Query.IdEmissor.Ascending);
                    emissorCollection.Query.Load();

                    //Limpa lista para evitar dados duplicados
                    dropAtivo.Items.Clear();
                    foreach (Emissor item in emissorCollection)
                    {
                        dropAtivo.Items.Add(new ListEditItem(item.Nome, item.IdEmissor));
                    }
                }
            }
        }
    }
    #endregion

    #region Suporte
    private void Cadastra()
    {
        classificacaoAtivo = Financial.Util.CadastroDinamico.PreencheEntidade<ClassificacaoAtivo>(gridCadastro.FindEditFormTemplateControl("panelEdicao").Controls, null);
        classificacaoAtivo.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro Classificação Ativo - Operacao: Insert Classificação Ativo: " + classificacaoAtivo.IdClassificacaoAtivo + UtilitarioWeb.ToString(classificacaoAtivo),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }
    #endregion
}