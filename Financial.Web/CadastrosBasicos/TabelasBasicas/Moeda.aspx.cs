﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using EntitySpaces.Interfaces;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_Moeda : Financial.Web.Common.CadastroBasePage {
    protected void EsDSMoeda_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        MoedaCollection coll = new MoedaCollection();

        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        Moeda moeda = new Moeda();
        short idMoeda = (short)e.Keys[0];

        if (moeda.LoadByPrimaryKey(idMoeda)) {
            moeda.Nome = Convert.ToString(e.NewValues["Nome"]);
            moeda.CodigoISO = Convert.ToString(e.NewValues["CodigoISO"]);
            moeda.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de Moeda - Operacao: Update Moeda: " + idMoeda + UtilitarioWeb.ToString(moeda),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        Moeda moeda = new Moeda();

        moeda.Nome = Convert.ToString(e.NewValues["Nome"]);
        moeda.CodigoISO = Convert.ToString(e.NewValues["CodigoISO"]);
        moeda.Save();
        
        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de Moeda - Operacao: Insert Moeda: " + moeda.IdMoeda + UtilitarioWeb.ToString(moeda),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(MoedaMetadata.ColumnNames.IdMoeda);

            MoedaCollection moedaCollectionDeletar = new MoedaCollection();
            ConversaoMoedaCollection conversaoMoedaCollectionDeletar = new ConversaoMoedaCollection();

            for (int i = 0; i < keyValuesId.Count; i++) {
                short idMoeda = Convert.ToInt16(keyValuesId[i]);

                // Acha todas as Conversões moedas que possuem a moeda a ser deletada
                /* Conversão Moeda tem 2 chaves estrangeiras: idMoedaDe e idMoedaPara
                 * idMoedaDe possue Delete Cascade == true - É deletado quando a Moeda for deletado
                 * e portanto não precisa ser deletado Manualmente
                 *
                 * idMoedaPara possue Delete Cascade == false por questões de limitação do banco.
                 * Deve ser Deletado Manualmente                 
                 */

                #region ConversaoMoeda
                ConversaoMoedaCollection conversaoMoedaCollection = new ConversaoMoedaCollection();
                conversaoMoedaCollection.Query.Select(conversaoMoedaCollection.Query.IdMoedaDe, conversaoMoedaCollection.Query.IdMoedaPara)
                                               .Where(conversaoMoedaCollection.Query.IdMoedaPara == idMoeda);

                if (conversaoMoedaCollection.Query.Load()) {
                    conversaoMoedaCollectionDeletar.Combine(conversaoMoedaCollection);
                }
                #endregion

                Moeda moeda = new Moeda();
                if (moeda.LoadByPrimaryKey(idMoeda)) {
                    moedaCollectionDeletar.AttachEntity(moeda);
                }
            }

            // Deleta Tudo de uma vez só 
            using (esTransactionScope scope = new esTransactionScope()) {
                //
                conversaoMoedaCollectionDeletar.MarkAllAsDeleted();

                //
                MoedaCollection moedaCollectionClone = (MoedaCollection)Utilitario.Clone(moedaCollectionDeletar);
                //

                moedaCollectionDeletar.MarkAllAsDeleted();

                #region Delete ConversaoMoeda e Moeda em Cascata
                conversaoMoedaCollectionDeletar.Save();
                
                moedaCollectionDeletar.Save();

                foreach (Moeda m in moedaCollectionClone) {
                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de Moeda - Operacao: Delete Moeda: " + m.IdMoeda + UtilitarioWeb.ToString(m),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion    
                }
               
                #endregion

                scope.Complete();
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }
}