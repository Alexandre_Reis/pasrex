﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EventoFinanceiro.aspx.cs"
    Inherits="CadastrosBasicos_EventoFinanceiro" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
    var popup = true;
    document.onkeydown=onDocumentKeyDownWithCallback;
    var operacao = '';
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                if (operacao == 'salvar')
                {             
                    gridCadastro.UpdateEdit();
                }
                else
                {
                    callbackAdd.SendCallback();
                }   
            }
            operacao = '';
        }        
        " />
        </dxcb:ASPxCallback>
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Eventos Financeiros"></asp:Label>
                            </div>
                            <div id="mainContent">
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;">
                                        <asp:Literal ID="Literal2" runat="server" Text="Excluir" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnFilter" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnFilter" OnClientClick="return OnButtonClick_FiltroDatas()">
                                        <asp:Literal ID="Literal3" runat="server" Text="Filtro" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnCopy" runat="server" Font-Overline="false" CssClass="btnCopy"
                                        OnClientClick=" if (confirm('As cotações marcadas serão copiadas para o dia seguinte. Confirma?')==true) gridCadastro.PerformCallback('btnCopy');return false;">
                                        <asp:Literal ID="Literal11" runat="server" Text="Copiar" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnPdf" OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal4" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnExcel" OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal5" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh"
                                        OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal6" runat="server" Text="Atualizar" /><div>
                                        </div>
                                    </asp:LinkButton>
                                </div>
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true" KeyFieldName="IdEventoFinanceiro"
                                        DataSourceID="EsDSEventoFinanceiro" OnRowUpdating="gridCadastro_RowUpdating"
                                        OnCustomCallback="gridCadastro_CustomCallback"
                                        OnPreRender="gridCadastro_PreRender" OnBeforeGetCallbackResult="gridCadastro_PreRender">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                                                <HeaderTemplate>
                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                                </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataColumn FieldName="IdEventoFinanceiro" Visible="true" VisibleIndex="1" />
                                            <dxwgv:GridViewDataColumn FieldName="Descricao" Visible="true" Width="20%" VisibleIndex="2" />
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="ClassificacaoAnbima" VisibleIndex="3"
                                                ExportWidth="110">
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="1" Text="<div title='Advogados'>Advogados</div>" />
                                                        <dxe:ListEditItem Value="2" Text="<div title='Auditoria'>Auditoria</div>" />
                                                        <dxe:ListEditItem Value="3" Text="<div title='Bancárias'>Bancárias</div>" />
                                                        <dxe:ListEditItem Value="4" Text="<div title='Cartório'>Cartório</div>" />
                                                        <dxe:ListEditItem Value="5" Text="<div title='Correspondências'>Correspondências</div>" />
                                                        <dxe:ListEditItem Value="6" Text="<div title='Impressos'>Impressos</div>" />
                                                        <dxe:ListEditItem Value="7" Text="<div title='Jurídicas'>Jurídicas</div>" />
                                                        <dxe:ListEditItem Value="8" Text="<div title='Outras Despesas Administrativas'>Outras Despesas Administrativas</div>" />
                                                        <dxe:ListEditItem Value="9" Text="<div title='Outras Despesas Exterior'>Outras Despesas Exterior</div>" />
                                                        <dxe:ListEditItem Value="10" Text="<div title='Publicação de Atas'>Publicação de Atas</div>" />
                                                        <dxe:ListEditItem Value="11" Text="<div title='Publicidade'>Publicidade</div>" />
                                                        <dxe:ListEditItem Value="12" Text="<div title='Taxa Anbima'>Taxa Anbima</div>" />
                                                        <dxe:ListEditItem Value="13" Text="<div title='Taxa CETIP'>Taxa CETIP</div>" />
                                                        <dxe:ListEditItem Value="14" Text="<div title='Taxa CVM'>Taxa CVM</div>" />
                                                        <dxe:ListEditItem Value="15" Text="<div title='Taxa Custódia'>Taxa Custódia</div>" />
                                                        <dxe:ListEditItem Value="16" Text="<div title='Taxa SELIC'>Taxa SELIC</div>" />
                                                        <dxe:ListEditItem Value="17" Text="<div title='Taxa SISBACEN'>Taxa SISBACEN</div>" />
                                                        <dxe:ListEditItem Value="18" Text="<div title='Títulos Públicos'>Títulos Públicos</div>" />
                                                        <dxe:ListEditItem Value="19" Text="<div title='Títulos Privados'>Títulos Privados</div>" />
                                                        <dxe:ListEditItem Value="20" Text="<div title='Debêntures'>Debêntures</div>" />
                                                        <dxe:ListEditItem Value="21" Text="<div title='Ações ou Opções Ações'>Ações ou Opções Ações</div>" />
                                                        <dxe:ListEditItem Value="22" Text="<div title='Derivativos (Opções Deriv. Ou Flexíveis ou Futuros)'>Derivativos (Opções Deriv. Ou Flexíveis ou Futuros)</div>" />
                                                        <dxe:ListEditItem Value="23" Text="<div title='Termo Ações'>Termo Ações</div>" />
                                                        <dxe:ListEditItem Value="24" Text="<div title='Termo SELIC'>Termo SELIC</div>" />
                                                        <dxe:ListEditItem Value="25" Text="<div title='Termo Swap'>Termo Swap</div>" />
                                                        <dxe:ListEditItem Value="26" Text="<div title='Swap'>Swap</div>" />
                                                        <dxe:ListEditItem Value="27" Text="<div title='Dividendos'>Dividendos</div>" />
                                                        <dxe:ListEditItem Value="28" Text="<div title='Juros s/ Capital Próprio'>Juros s/ Capital Próprio</div>" />
                                                        <dxe:ListEditItem Value="29" Text="<div title='Subscrições'>Subscrições</div>" />
                                                        <dxe:ListEditItem Value="30" Text="<div title='Juros (RF)'>Juros (RF)</div>" />
                                                        <dxe:ListEditItem Value="31" Text="<div title='Empréstimo Ações'>Empréstimo Ações</div>" />
                                                        <dxe:ListEditItem Value="32" Text="<div title='Empréstimo Título Público'>Empréstimo Título Público</div>" />
                                                        <dxe:ListEditItem Value="33" Text="<div title='Aluguel Imóvel'>Aluguel Imóvel</div>" />
                                                        <dxe:ListEditItem Value="34" Text="<div title='Taxa Administração'>Taxa Administração</div>" />
                                                        <dxe:ListEditItem Value="35" Text="<div title='Taxa Performance'>Taxa Performance</div>" />
                                                        <dxe:ListEditItem Value="36" Text="<div title='Despesa Corretagem Bovespa'>Despesa Corretagem Bovespa</div>" />
                                                        <dxe:ListEditItem Value="37" Text="<div title='Despesa Corretagem BM&F'>Despesa Corretagem BM&F</div>" />
                                                        <dxe:ListEditItem Value="38" Text="<div title='Emolumentos'>Emolumentos</div>" />
                                                        <dxe:ListEditItem Value="39" Text="<div title='Valor Bovespa'>Valor Bovespa</div>" />
                                                        <dxe:ListEditItem Value="40" Text="<div title='Valor Repasse Bovespa'>Valor Repasse Bovespa</div>" />
                                                        <dxe:ListEditItem Value="41" Text="<div title='Valor BM&F'>Valor BM&F</div>" />
                                                        <dxe:ListEditItem Value="42" Text="<div title='Valor Repasse BM&F'>Valor Repasse BM&F</div>" />
                                                        <dxe:ListEditItem Value="43" Text="<div title='Valor Outras Bolsas'>Valor Outras Bolsas</div>" />
                                                        <dxe:ListEditItem Value="44" Text="<div title='Valor Repasse Outras Bolsas'>Valor Repasse Outras Bolsas</div>" />
                                                        <dxe:ListEditItem Value="45" Text="<div title='Aplicação a Converter'>Aplicação a Converter</div>" />
                                                        <dxe:ListEditItem Value="46" Text="<div title='Resgate a Converter'>Resgate a Converter</div>" />
                                                        <dxe:ListEditItem Value="47" Text="<div title='Resgate a Liquidar'>Resgate a Liquidar</div>" />
                                                        <dxe:ListEditItem Value="999" Text="<div title='Outros'>Outros</div>" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataComboBoxColumn Caption="Evento Provisão" FieldName="IdEventoProvisao"
                                                VisibleIndex="4">
                                                <PropertiesComboBox DataSourceID="EsDSEventoContabil" TextField="Descricao" ValueField="IdEvento"
                                                    IncrementalFilteringMode="startswith" />
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataComboBoxColumn Caption="Evento Pagamento" FieldName="IdEventoPagamento"
                                                VisibleIndex="4">
                                                <PropertiesComboBox DataSourceID="EsDSEventoContabil" TextField="Descricao" ValueField="IdEvento"
                                                    IncrementalFilteringMode="startswith" />
                                            </dxwgv:GridViewDataComboBoxColumn>
                                        </Columns>
                                        <Templates>
                                            <EditForm>
                                                <div class="editForm">
                                                    <table>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelDescricao" runat="server" CssClass="labelRequired" Text="Descrição:"> </asp:Label>
                                                            </td>
                                                            <td colspan="3">
                                                                <asp:TextBox ID="textDescricao" runat="server" CssClass="textDescricao" Text='<%#Eval("Descricao")%>' />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelClassificacaoAnbima" runat="server" CssClass="labelRequired"
                                                                    Text="Classificação Anbima:"></asp:Label>
                                                            </td>
                                                            <td colspan="3">
                                                                <dxe:ASPxComboBox ID="dropClassificacaoAnbima" runat="server" ShowShadow="false"
                                                                    DropDownStyle="DropDownList" CssClass="dropDownList" Text='<%#Eval("ClassificacaoAnbima")%>'>
                                                                    <ClientSideEvents LostFocus="function(s, e) {if(s.GetSelectedIndex() == -1) 
                                                                                    s.SetText(null);}">
                                                                    </ClientSideEvents>
                                                                    <Items>
                                                                        <dxe:ListEditItem Value="1" Text="Advogados" />
                                                                        <dxe:ListEditItem Value="2" Text="Auditoria" />
                                                                        <dxe:ListEditItem Value="3" Text="Bancárias" />
                                                                        <dxe:ListEditItem Value="4" Text="Cartório" />
                                                                        <dxe:ListEditItem Value="5" Text="Correspondências" />
                                                                        <dxe:ListEditItem Value="6" Text="Impressos" />
                                                                        <dxe:ListEditItem Value="7" Text="Jurídicas" />
                                                                        <dxe:ListEditItem Value="8" Text="Outras Despesas Administrativas" />
                                                                        <dxe:ListEditItem Value="9" Text="Outras Despesas Exterior" />
                                                                        <dxe:ListEditItem Value="10" Text="Publicação de Atas" />
                                                                        <dxe:ListEditItem Value="11" Text="Publicidade" />
                                                                        <dxe:ListEditItem Value="12" Text="Taxa Anbima" />
                                                                        <dxe:ListEditItem Value="13" Text="Taxa CETIP" />
                                                                        <dxe:ListEditItem Value="14" Text="Taxa CVM" />
                                                                        <dxe:ListEditItem Value="15" Text="Taxa Custódia" />
                                                                        <dxe:ListEditItem Value="16" Text="Taxa SELIC" />
                                                                        <dxe:ListEditItem Value="17" Text="Taxa SISBACEN" />
                                                                        <dxe:ListEditItem Value="18" Text="Títulos Públicos" />
                                                                        <dxe:ListEditItem Value="19" Text="Títulos Privados" />
                                                                        <dxe:ListEditItem Value="20" Text="Debêntures" />
                                                                        <dxe:ListEditItem Value="21" Text="Ações ou Opções Ações" />
                                                                        <dxe:ListEditItem Value="22" Text="Derivativos (Opções Deriv. Ou Flexíveis ou Futuros)" />
                                                                        <dxe:ListEditItem Value="23" Text="Termo Ações" />
                                                                        <dxe:ListEditItem Value="24" Text="Termo SELIC" />
                                                                        <dxe:ListEditItem Value="25" Text="Termo Swap" />
                                                                        <dxe:ListEditItem Value="26" Text="Swap" />
                                                                        <dxe:ListEditItem Value="27" Text="Dividendos" />
                                                                        <dxe:ListEditItem Value="28" Text="Juros s/ Capital Próprio" />
                                                                        <dxe:ListEditItem Value="29" Text="Subscrições" />
                                                                        <dxe:ListEditItem Value="30" Text="Juros (RF)" />
                                                                        <dxe:ListEditItem Value="31" Text="Empréstimo Ações" />
                                                                        <dxe:ListEditItem Value="32" Text="Empréstimo Título Público" />
                                                                        <dxe:ListEditItem Value="33" Text="Aluguel Imóvel" />
                                                                        <dxe:ListEditItem Value="34" Text="Taxa Administração" />
                                                                        <dxe:ListEditItem Value="35" Text="Taxa Performance" />
                                                                        <dxe:ListEditItem Value="36" Text="Despesa Corretagem Bovespa" />
                                                                        <dxe:ListEditItem Value="37" Text="Despesa Corretagem BM&F" />
                                                                        <dxe:ListEditItem Value="38" Text="Emolumentos" />
                                                                        <dxe:ListEditItem Value="39" Text="Valor Bovespa" />
                                                                        <dxe:ListEditItem Value="40" Text="Valor Repasse Bovespa" />
                                                                        <dxe:ListEditItem Value="41" Text="Valor BM&F" />
                                                                        <dxe:ListEditItem Value="42" Text="Valor Repasse BM&F" />
                                                                        <dxe:ListEditItem Value="43" Text="Valor Outras Bolsas" />
                                                                        <dxe:ListEditItem Value="44" Text="Valor Repasse Outras Bolsas" />
                                                                        <dxe:ListEditItem Value="45" Text="Aplicação a Converter" />
                                                                        <dxe:ListEditItem Value="46" Text="Resgate a Converter" />
                                                                        <dxe:ListEditItem Value="47" Text="Resgate a Liquidar" />
                                                                        <dxe:ListEditItem Value="999" Text="Outros" />
                                                                    </Items>
                                                                </dxe:ASPxComboBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelEventoProvisao" runat="server" CssClass="labelNormal" Text="Evento Provisão:"></asp:Label>
                                                            </td>
                                                            <td colspan="3">
                                                                <dxe:ASPxComboBox ID="dropEventoProvisao" runat="server" ClientInstanceName="dropEventoProvisao"
                                                                    DataSourceID="EsDSEventoContabil" ShowShadow="false" DropDownStyle="DropDownList"
                                                                    CssClass="dropDownListLongo" TextField="Descricao" ValueField="IdEvento" Text='<%#Eval("IdEventoProvisao")%>'>
                                                                </dxe:ASPxComboBox>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelEventoPagamento" runat="server" CssClass="labelNormal" Text="Evento Pagamento:"></asp:Label>
                                                            </td>
                                                            <td colspan="3">
                                                                <dxe:ASPxComboBox ID="dropEventoPagamento" runat="server" ClientInstanceName="dropEventoPagamento"
                                                                    DataSourceID="EsDSEventoContabil" ShowShadow="false" DropDownStyle="DropDownList"
                                                                    CssClass="dropDownListLongo" TextField="Descricao" ValueField="IdEvento" Text='<%#Eval("IdEventoPagamento")%>'>
                                                                </dxe:ASPxComboBox>
                                                        </tr>
                                                    </table>
                                                    <div class="linhaH">
                                                    </div>
                                                    <div class="linkButton linkButtonNoBorder popupFooter">
                                                        <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd"
                                                            OnInit="btnOKAdd_Init" OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;">
                                                            <asp:Literal ID="Literal10" runat="server" Text="OK+" /><div>
                                                            </div>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                            OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                                                            <asp:Literal ID="Literal5" runat="server" Text="OK" /><div>
                                                            </div>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel"
                                                            OnClientClick="gridCadastro.CancelEdit(); return false;">
                                                            <asp:Literal ID="Literal9" runat="server" Text="Cancelar" /><div>
                                                            </div>
                                                        </asp:LinkButton>
                                                    </div>
                                                </div>
                                            </EditForm>
                                            <StatusBar>
                                                <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" />
                                            </StatusBar>
                                        </Templates>
                                        <SettingsEditing PopupEditFormWidth="250px" />
                                        <Templates>
                                            <StatusBar>
                                                <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" />
                                            </StatusBar>
                                        </Templates>
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
        <cc1:esDataSource ID="EsDSEventoFinanceiro" runat="server" OnesSelect="EsDSEventoFinanceiro_esSelect" />
        <cc1:esDataSource ID="EsDSEventoContabil" runat="server" OnesSelect="EsDSEventoContabil_esSelect" />
    </form>
</body>
</html>
