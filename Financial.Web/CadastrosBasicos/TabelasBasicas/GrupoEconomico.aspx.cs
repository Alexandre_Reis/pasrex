using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Investidor;
using Financial.RendaFixa;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_GrupoEconomico : Financial.Web.Common.CadastroBasePage
{
    protected void EsDSGrupoEconomico_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        GrupoEconomicoCollection coll = new GrupoEconomicoCollection();

        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        GrupoEconomico grupoEconomico = new GrupoEconomico();
        int idGrupo = (int)e.Keys[0];

        if (grupoEconomico.LoadByPrimaryKey(idGrupo))
        {
            grupoEconomico.Nome = Convert.ToString(e.NewValues["Nome"]);
            grupoEconomico.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de GrupoEconomico - Operacao: Update GrupoEconomico: " + idGrupo + UtilitarioWeb.ToString(grupoEconomico),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        GrupoEconomico grupoEconomico = new GrupoEconomico();

        grupoEconomico.Nome = Convert.ToString(e.NewValues["Nome"]);
        grupoEconomico.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de GrupoEconomico - Operacao: Insert Setor: " + grupoEconomico.IdGrupo + UtilitarioWeb.ToString(grupoEconomico),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdGrupo");
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idGrupo = Convert.ToInt16(keyValuesId[i]);

                GrupoEconomico grupoEconomico = new GrupoEconomico();
                if (grupoEconomico.LoadByPrimaryKey(idGrupo))
                {
                    //
                    GrupoEconomico grupoEconomicoClone = (GrupoEconomico)Utilitario.Clone(grupoEconomico);
                    //

                    grupoEconomico.MarkAsDeleted();
                    grupoEconomico.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de GrupoEconomico - Operacao: Delete GrupoEconomico: " + idGrupo + UtilitarioWeb.ToString(grupoEconomicoClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }
}