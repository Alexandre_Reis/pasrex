﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.RendaFixa;
using Financial.Web.Common;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_CategoriaMovimentacao : CadastroBasePage
{
    new protected void Page_Load(object sender, EventArgs e)
    {
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSCategoriaMovimentacao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CategoriaMovimentacaoCollection coll = new CategoriaMovimentacaoCollection();
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
        ASPxTextBox textCodigoCategoria = gridCadastro.FindEditFormTemplateControl("textCodigoCategoria") as ASPxTextBox;
        TextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as TextBox;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(textCodigoCategoria);


        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        if (gridCadastro.IsNewRowEditing)
        {
            CategoriaMovimentacaoCollection categoriaMovimentacaoColl = new CategoriaMovimentacaoCollection();
            categoriaMovimentacaoColl.Query.Where(categoriaMovimentacaoColl.Query.CodigoCategoria.Trim().Equal(textCodigoCategoria.Text.Trim()));

            if (categoriaMovimentacaoColl.Query.Load())
            {
                e.Result = "Código de categoria já cadastrado!";
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        CategoriaMovimentacao categoriaMovimentacao = new CategoriaMovimentacao();

        ASPxTextBox textCodigoCategoria = gridCadastro.FindEditFormTemplateControl("textCodigoCategoria") as ASPxTextBox;
        TextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as TextBox;
        int idOperacao = (int)e.Keys[0];

        if (categoriaMovimentacao.LoadByPrimaryKey(idOperacao))
        {
            categoriaMovimentacao.CodigoCategoria = textCodigoCategoria.Text;
            categoriaMovimentacao.Descricao = string.IsNullOrEmpty(textDescricao.Text) ? null : textDescricao.Text;
            categoriaMovimentacao.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de CategoriaMovimentacao - Operacao: Update CategoriaMovimentacao: " + categoriaMovimentacao.IdCategoriaMovimentacao + "; " + UtilitarioWeb.ToString(categoriaMovimentacao),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    private void SalvarNovo()
    {
        CategoriaMovimentacao categoriaMovimentacao = new CategoriaMovimentacao();

        ASPxTextBox textCodigoCategoria = gridCadastro.FindEditFormTemplateControl("textCodigoCategoria") as ASPxTextBox;
        TextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as TextBox;

        categoriaMovimentacao.CodigoCategoria = textCodigoCategoria.Text;
        categoriaMovimentacao.Descricao = string.IsNullOrEmpty(textDescricao.Text) ? null : textDescricao.Text;
        categoriaMovimentacao.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de CategoriaMovimentacao - Operacao: Insert CategoriaMovimentacao: " + categoriaMovimentacao.IdCategoriaMovimentacao + "; " + UtilitarioWeb.ToString(categoriaMovimentacao),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(CategoriaMovimentacaoMetadata.ColumnNames.IdCategoriaMovimentacao);
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idCategoria = Convert.ToInt32(keyValuesId[i]);

                CategoriaMovimentacao categoriaMovimentacao = new CategoriaMovimentacao();
                if (categoriaMovimentacao.LoadByPrimaryKey(idCategoria))
                {
                    CategoriaMovimentacao categoriaMovimentacaoClone = (CategoriaMovimentacao)Utilitario.Clone(categoriaMovimentacao);
                    //

                    categoriaMovimentacao.MarkAsDeleted();
                    categoriaMovimentacao.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de CategoriaMovimentacao - Operacao: Delete CategoriaMovimentacao: " + idCategoria + ";" + UtilitarioWeb.ToString(categoriaMovimentacaoClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }
}