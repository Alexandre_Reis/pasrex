﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TabelaIOF.aspx.cs" Inherits="CadastrosBasicos_TabelaIOF" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
           
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />
</head>

<body>
    <form id="form1" runat="server">
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">
        
    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Tabela de IOF"></asp:Label>
    </div>
        
    <div id="mainContent">
     
            <div class="linkButton linkButtonNoBorder" >               
               <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf" OnClick="btnPDF_Click" ><asp:Literal ID="Literal1" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel" OnClick="btnExcel_Click" ><asp:Literal ID="Literal2" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>              
            </div>
    
            <div class="divDataGrid">            
            <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true" KeyFieldName="Prazo" DataSourceID="EsDSTabelaIOF" >
                    
            <Columns>           
                <dxwgv:GridViewDataTextColumn FieldName="Prazo" Caption="Prazo (em Dias)" Width="20%" VisibleIndex="1"
                        HeaderStyle-HorizontalAlign="Center" CellStyle-HorizontalAlign="Center">                    
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn FieldName="AliquotaIOF" Caption="Alíquota IOF" Width="75%" VisibleIndex="1" CellStyle-HorizontalAlign="left"/>                    
            </Columns>
        </dxwgv:ASPxGridView>            
        </div>
    
    </div>
    </div>
    </td></tr></table>
    </div>        
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
        
    <cc1:esDataSource ID="EsDSTabelaIOF" runat="server" OnesSelect="EsDSTabelaIOF_esSelect" />
        
    </form>
</body>
</html>