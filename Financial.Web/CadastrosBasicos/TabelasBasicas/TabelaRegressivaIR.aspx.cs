﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Collections.Generic;
using System.Drawing;

using Financial.Fundo;
using Financial.Security;
using Financial.Web.Util;
using Financial.Util;
using Financial.Tributo;
using Financial.Web.Common;

using EntitySpaces.Interfaces;

using DevExpress.Web.Data;
using DevExpress.Web;


public partial class TabelasBasicas_TabelaRegressivaIR : CadastroBasePage
{
    new protected void Page_Load(object sender, EventArgs e) {

    }

    #region DataSources
    protected void EsDSTabelaRegressivaVigencia_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        TabelaRegressivaVigenciaCollection tabelaRegressivaVigenciaCollection = new TabelaRegressivaVigenciaCollection();
        tabelaRegressivaVigenciaCollection.Query.OrderBy(tabelaRegressivaVigenciaCollection.Query.Data.Descending);

        tabelaRegressivaVigenciaCollection.Query.Load();

        e.Collection = tabelaRegressivaVigenciaCollection;

    }

    

    /// <summary>
    /// PopUp Cliente
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSTabelaRegressivaValores_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {

        TabelaRegressivaValoresCollection tabelaRegressivaValoresCollection = new TabelaRegressivaValoresCollection();

        if (Session["idTabela"] != "")
        {
            int idTabela = (int)Session["idTabela"];
            tabelaRegressivaValoresCollection.Query.Where(tabelaRegressivaValoresCollection.Query.IdTabelaRegressiva.Equal(idTabela));
            tabelaRegressivaValoresCollection.Query.Load();
        }

        e.Collection = tabelaRegressivaValoresCollection;

    }

    #endregion

    protected void detailGrid_DataSelect(object sender, EventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;

        Session["idTabela"] = grid.GetMasterRowKeyValue();
    }
 
    protected void chkSingleExpanded_CheckedChanged(object sender, EventArgs e)
    {
        if (gridCadastro.SettingsDetail.AllowOnlyOneMasterRowExpanded)
        {
            gridCadastro.DetailRows.CollapseAllRows();
        }
    }

    protected void gridCadastro_RowValidating(object sender, DevExpress.Web.Data.ASPxDataValidationEventArgs e)
    {
       
        if (e.NewValues["Data"] == null)
            e.RowError = "Favor informar a data de vigência.";

        DateTime data = Convert.ToDateTime(e.NewValues["Data"]);

        TabelaRegressivaVigenciaCollection tabelaRegressivaVigenciaCollection = new TabelaRegressivaVigenciaCollection();
        tabelaRegressivaVigenciaCollection.Query.Where(tabelaRegressivaVigenciaCollection.Query.Data.Equal(data));
        if (!e.IsNewRow)
        {
            int idTabelaRegressiva = Convert.ToInt32(e.Keys[0]);
            tabelaRegressivaVigenciaCollection.Query.Where(tabelaRegressivaVigenciaCollection.Query.IdTabelaRegressiva.NotEqual(idTabelaRegressiva));
        }
        tabelaRegressivaVigenciaCollection.Query.Load();
        if (tabelaRegressivaVigenciaCollection.Count > 0)
            e.RowError = "Data de vigência já existe.";

    }

    protected void gridCadastro_RowInserting(object sender, ASPxDataInsertingEventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;

        TabelaRegressivaVigencia tabelaRegressivaVigencia = new TabelaRegressivaVigencia();

        this.AtualizaDataVigencia(tabelaRegressivaVigencia, e.NewValues);

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;

        TabelaRegressivaVigencia tabelaRegressivaVigencia = new TabelaRegressivaVigencia();

        if (tabelaRegressivaVigencia.LoadByPrimaryKey(Convert.ToInt32(e.Keys[0])))
        {
            this.AtualizaDataVigencia(tabelaRegressivaVigencia, e.NewValues);
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_RowDeleting(object sender, ASPxDataDeletingEventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;

        TabelaRegressivaVigencia tabelaRegressivaVigencia = new TabelaRegressivaVigencia();

        if (tabelaRegressivaVigencia.LoadByPrimaryKey(Convert.ToInt32(e.Keys[0])))
        {
            tabelaRegressivaVigencia.MarkAsDeleted();
            tabelaRegressivaVigencia.Save();
        }

        e.Cancel = true;
        grid.CancelEdit();
        grid.DataBind();
    }

    private void AtualizaDataVigencia(TabelaRegressivaVigencia tabelaRegressivaVigencia, System.Collections.Specialized.OrderedDictionary newValues)
    {
        tabelaRegressivaVigencia.Data = Convert.ToDateTime(newValues["Data"]);

        tabelaRegressivaVigencia.Save();
    }


    protected void detailGrid_RowValidating(object sender, DevExpress.Web.Data.ASPxDataValidationEventArgs e)
    {
        if (e.NewValues["Periodo"] == null)
            e.RowError = "Favor informar o periodo.";

        int periodo = Convert.ToInt32(e.NewValues["Periodo"]);

        if (periodo < 0)
            e.RowError = "Período inválido.";


        if (e.NewValues["Aliquota"] == null)
            e.RowError = "Favor informar a aliquota.";

        Decimal aliquota = Convert.ToDecimal(e.NewValues["Aliquota"]);

        if (aliquota < 0)
            e.RowError = "Aliquota inválida.";

        if (!e.IsNewRow)
        {
            if (Convert.ToInt32(e.Keys[1]).CompareTo(periodo) != 0)
            {
                int idTabelaRegressiva = Convert.ToInt32(e.Keys[0]);

                TabelaRegressivaValoresCollection tabelaRegressivaValoresCollection = new TabelaRegressivaValoresCollection();
                tabelaRegressivaValoresCollection.Query.Where(tabelaRegressivaValoresCollection.Query.Periodo.Equal(periodo));
                tabelaRegressivaValoresCollection.Query.Where(tabelaRegressivaValoresCollection.Query.IdTabelaRegressiva.Equal(idTabelaRegressiva));
                tabelaRegressivaValoresCollection.Query.Load();
                if (tabelaRegressivaValoresCollection.Count > 0)
                    e.RowError = "Período de IR já existe.";
            }
        }
    }

    protected void detailGrid_RowInserting(object sender, ASPxDataInsertingEventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;

        TabelaRegressivaValores tabelaRegressivaValores = new TabelaRegressivaValores();

        this.AtualizaValores(tabelaRegressivaValores, e.NewValues);

        e.Cancel = true;
        grid.CancelEdit();
        grid.DataBind();
    }

    protected void detailGrid_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;

        TabelaRegressivaValores tabelaRegressivaValores = new TabelaRegressivaValores();

        if (tabelaRegressivaValores.LoadByPrimaryKey(Convert.ToInt32(e.Keys[0]), Convert.ToInt32(e.Keys[1])))
        {
            tabelaRegressivaValores.MarkAsDeleted();
            tabelaRegressivaValores.Save();
            tabelaRegressivaValores = new TabelaRegressivaValores();
            this.AtualizaValores(tabelaRegressivaValores, e.NewValues);
        }

        e.Cancel = true;
        grid.CancelEdit();
        grid.DataBind();
    }

    protected void detailGrid_RowDeleting(object sender, ASPxDataDeletingEventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;

        TabelaRegressivaValores tabelaRegressivaValores = new TabelaRegressivaValores();

        if (tabelaRegressivaValores.LoadByPrimaryKey(Convert.ToInt32(e.Keys[0]), Convert.ToInt32(e.Keys[1])))
        {
            tabelaRegressivaValores.MarkAsDeleted();
            tabelaRegressivaValores.Save();
        }

        e.Cancel = true;
        grid.CancelEdit();
        grid.DataBind();
    }

    private void AtualizaValores(TabelaRegressivaValores tabelaRegressivaValores, System.Collections.Specialized.OrderedDictionary newValues)
    {
        tabelaRegressivaValores.IdTabelaRegressiva = Convert.ToInt32(Session["idTabela"]);
        tabelaRegressivaValores.Periodo = Convert.ToInt32(newValues["Periodo"]);
        tabelaRegressivaValores.Aliquota = Convert.ToDecimal(newValues["Aliquota"]);

        tabelaRegressivaValores.Save();
    }

}