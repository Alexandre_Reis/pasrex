﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Agencia.aspx.cs" Inherits="CadastrosBasicos_Agencia" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />    
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>    
    <script type="text/javascript" language="Javascript">
    var popup = false;
    document.onkeydown=onDocumentKeyDown;
    </script>
    
    <script type="text/javascript" language="Javascript">    
		var isCustomCallback = false;   // usada para controlar o refresh após um delete feito no grid
    </script>

</head>

<body>
    <form id="form1" runat="server">
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">
    
    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Agências" />
    </div>
        
    <div id="mainContent">
            
        <div class="linkButton" >               
           <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal1" runat="server" Text="Novo"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;"><asp:Literal ID="Literal2" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal3" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal4" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal5" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
        </div>
    
        <div class="divDataGrid">            
            <dxwgv:ASPxGridView ID="gridCadastro" ClientInstanceName="gridCadastro" runat="server" EnableCallBacks="true"
            KeyFieldName="IdAgencia" DataSourceID="EsDSAgencia"
            OnHtmlRowCreated="gridCadastro_HtmlRowCreated"  
            OnCustomCallback="gridCadastro_CustomCallback"
            OnRowInserting="gridCadastro_RowInserting"
            OnRowUpdating="gridCadastro_RowUpdating"
            OnCellEditorInitialize="gridCadastro_CellEditorInitialize">        
                    
            <Columns>                           
                <dxwgv:GridViewCommandColumn VisibleIndex="0" ShowClearFilterButton="True">
                    <HeaderTemplate>
                        <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" Width="5%" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                    </HeaderTemplate>
                </dxwgv:GridViewCommandColumn>
                
                <dxwgv:GridViewDataComboBoxColumn FieldName="IdBanco" Caption="Banco" VisibleIndex="1" Width="30%">
                <PropertiesComboBox DataSourceID="EsDSBanco" TextField="Nome" ValueField="IdBanco">
                </PropertiesComboBox>
                </dxwgv:GridViewDataComboBoxColumn>
                
                <dxwgv:GridViewDataTextColumn FieldName="Codigo" Width="15%" VisibleIndex="2">
                <PropertiesTextEdit MaxLength="10"></PropertiesTextEdit>
                </dxwgv:GridViewDataTextColumn>

                <dxwgv:GridViewDataTextColumn FieldName="DigitoAgencia" Caption="DV" Width="5%" VisibleIndex="3">
                <PropertiesTextEdit MaxLength="1">
                    <ValidationSettings RequiredField-ErrorText=""></ValidationSettings>
                    <ValidationSettings ErrorText=""></ValidationSettings>                
                </PropertiesTextEdit>
                </dxwgv:GridViewDataTextColumn>
                
                <dxwgv:GridViewDataTextColumn FieldName="Nome" Width="30%" VisibleIndex="4">
                <PropertiesTextEdit MaxLength="100">
                    <ValidationSettings RequiredField-ErrorText=""></ValidationSettings>
                    <ValidationSettings ErrorText=""></ValidationSettings>              
                </PropertiesTextEdit>                
                </dxwgv:GridViewDataTextColumn>
                
                <dxwgv:GridViewDataComboBoxColumn Caption="Local" FieldName="IdLocal" VisibleIndex="5" Width="15%">
                    <PropertiesComboBox DataSourceID="EsDSLocalFeriado" TextField="Nome" ValueField="IdLocal" />                                
                </dxwgv:GridViewDataComboBoxColumn>
                
            </Columns>            

            <ClientSideEvents

                BeginCallback="function(s, e) {
		            if (e.command == 'CUSTOMCALLBACK') {
                        isCustomCallback = true;
                    }						
                }"

                EndCallback="function(s, e) {
			        if (isCustomCallback) {
                        isCustomCallback = false;
                        s.Refresh();
                    }
                }"
            />
            <SettingsCommandButton>
                <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                <UpdateButton Image-Url="../../imagens/ico_form_ok_inline.gif"/>
                <CancelButton Image-Url="../../imagens/ico_form_back_inline.gif"/>
            </SettingsCommandButton>
                                    
            </dxwgv:ASPxGridView>            
        </div>
          
    </div>
    
    </div>
    </td></tr></table>
    </div>        
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
        
    <cc1:esDataSource ID="EsDSBanco" runat="server" OnesSelect="EsDSBanco_esSelect" />
    <cc1:esDataSource ID="EsDSAgencia" runat="server" OnesSelect="EsDSAgencia_esSelect" />
    <cc1:esDataSource ID="EsDSLocalFeriado" runat="server" OnesSelect="EsDSLocalFeriado_esSelect" />    
        
    </form>
</body>
</html>