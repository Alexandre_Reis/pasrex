﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using System.Drawing;
using Financial.Web.Common;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Common.Enums;
using Financial.Fundo;
using Financial.ContaCorrente;
using Financial.Contabil;
using DevExpress.Web;

public partial class CadastrosBasicos_EventoFinanceiro : CadastroBasePage
{
    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasSummary = false;
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSEventoFinanceiro_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        EventoFinanceiroCollection coll = new EventoFinanceiroCollection();

        coll.Query.OrderBy(coll.Query.IdEventoFinanceiro.Ascending, coll.Query.Descricao.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSEventoContabil_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ContabRoteiroCollection coll = new ContabRoteiroCollection();
        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
        TextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as TextBox;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(textDescricao);

        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion
    }

    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        TextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as TextBox;
        ASPxComboBox dropEventoProvisao = gridCadastro.FindEditFormTemplateControl("dropEventoProvisao") as ASPxComboBox;
        ASPxComboBox dropEventoPagamento = gridCadastro.FindEditFormTemplateControl("dropEventoPagamento") as ASPxComboBox;

        int idEventoFinanceiro = (int)e.Keys[0];

        EventoFinanceiro eventoFinanceiro = new EventoFinanceiro();
        eventoFinanceiro.LoadByPrimaryKey(idEventoFinanceiro);

        eventoFinanceiro.Descricao = textDescricao.Text;
        eventoFinanceiro.IdEventoPagamento = Convert.ToInt32(dropEventoPagamento.Value);
        eventoFinanceiro.IdEventoProvisao = Convert.ToInt32(dropEventoProvisao.Value);

        eventoFinanceiro.Save();

        /*ASPxComboBox dropIndice = gridCadastro.FindEditFormTemplateControl("dropIndice") as ASPxComboBox;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;*/

        /*short idIndice = Convert.ToInt16(dropIndice.SelectedItem.Value);
        DateTime data = Convert.ToDateTime(textData.Text);
        decimal valor = Convert.ToDecimal(textValor.Text);
        if (cotacaoIndice.LoadByPrimaryKey(data, idIndice))
        {
            cotacaoIndice.Valor = valor;
            cotacaoIndice.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de CotacaoIndice - Operacao: Update CotacaoIndice: " + data + "; " + idIndice + UtilitarioWeb.ToString(cotacaoIndice),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion

            if (ParametrosConfiguracaoSistema.Outras.CalculaFatorIndice)
            {
                cotacaoIndice.AtualizaFatorIndice(idIndice);
            }
        }*/

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e)
    {
        this.FocaCampoGrid("textData", "textValor");
        base.gridCadastro_PreRender(sender, e);
    }

}