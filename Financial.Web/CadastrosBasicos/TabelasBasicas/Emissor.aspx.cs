﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.CRM;
using Financial.CRM.Enums;

public partial class CadastrosBasicos_Emissor : Financial.Web.Common.CadastroBasePage 
{
    bool EstendeConceitoPessoaAgente = ParametrosConfiguracaoSistema.Outras.EstendeConceitoPessoaAgente.Equals("S");

    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasPopupPessoa = true;
        this.HasPanelFieldsLoading = true;

        base.Page_Load(sender, e);
    }



    #region DataSources
    protected void EsDSEmissor_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        EmissorCollection coll = new EmissorCollection();
        EmissorQuery EmissorQuery = new EmissorQuery("emissor");
        PessoaQuery pessoaQuery = new PessoaQuery("pessoa");

        EmissorQuery.Select(EmissorQuery, pessoaQuery.IdPessoa, pessoaQuery.Apelido.As("ApelidoPessoa"));
        EmissorQuery.LeftJoin(pessoaQuery).On(EmissorQuery.IdPessoa.Equal(pessoaQuery.IdPessoa));
        EmissorQuery.OrderBy(EmissorQuery.Nome.Ascending);

        coll.Load(EmissorQuery);

        for (int i = 0; i < coll.Count; i++)
        {
            coll[i].Cnpj = Utilitario.MascaraCNPJ(coll[i].Cnpj);
        }

        e.Collection = coll;
    }

    protected void EsDSSetor_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        SetorCollection coll = new SetorCollection();

        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSAgente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        AgenteMercadoCollection coll = new AgenteMercadoCollection();

        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) 
    {
        Emissor emissor = new Emissor();
        int idEmissor = (int)e.Keys[0];

        ASPxTextBox textNome = gridCadastro.FindEditFormTemplateControl("textNome") as ASPxTextBox;
        ASPxComboBox dropSetor = gridCadastro.FindEditFormTemplateControl("dropSetor") as ASPxComboBox;
        ASPxComboBox dropTipoEmissor = gridCadastro.FindEditFormTemplateControl("dropTipoEmissor") as ASPxComboBox;
        ASPxComboBox dropAgenteMercado = gridCadastro.FindEditFormTemplateControl("dropAgenteMercado") as ASPxComboBox;
        ASPxTextBox textCNPJ = gridCadastro.FindEditFormTemplateControl("textCNPJ") as ASPxTextBox;
        ASPxTextBox textCodigoInterface = gridCadastro.FindEditFormTemplateControl("textCodigoInterface") as ASPxTextBox;
        ASPxSpinEdit btnEditCodigoPessoa = gridCadastro.FindEditFormTemplateControl("btnEditCodigoPessoa") as ASPxSpinEdit;

        if (emissor.LoadByPrimaryKey(idEmissor))
        
        {
            emissor.Nome = textNome.Text;
            emissor.TipoEmissor = Convert.ToByte(dropTipoEmissor.SelectedItem.Value);
            emissor.IdSetor = Convert.ToInt16(dropSetor.SelectedItem.Value);        

            if (!string.IsNullOrEmpty(textCNPJ.Text))
                emissor.Cnpj = textCNPJ.Text.Replace(".", "").Replace("-", "").Replace("/", "").Replace("\\", "");
            else
                emissor.Cnpj = null;

            if (!string.IsNullOrEmpty(btnEditCodigoPessoa.Text))
                emissor.IdPessoa = Convert.ToInt32(btnEditCodigoPessoa.Text);
            else
                emissor.IdPessoa = null;

            if (!string.IsNullOrEmpty(textCodigoInterface.Text))
                emissor.CodigoInterface = textCodigoInterface.Text;
            else
                emissor.CodigoInterface = null;

            if (dropAgenteMercado.SelectedIndex != -1)
                emissor.IdAgente = Convert.ToInt32(dropAgenteMercado.SelectedItem.Value);
            else
                emissor.IdAgente = null;

            emissor.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de Emissor - Operacao: Update Emissor: " + idEmissor + UtilitarioWeb.ToString(emissor),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        ASPxTextBox textNome = gridCadastro.FindEditFormTemplateControl("textNome") as ASPxTextBox;
        ASPxComboBox dropSetor = gridCadastro.FindEditFormTemplateControl("dropSetor") as ASPxComboBox;
        ASPxComboBox dropTipoEmissor = gridCadastro.FindEditFormTemplateControl("dropTipoEmissor") as ASPxComboBox;
        ASPxTextBox textCNPJ = gridCadastro.FindEditFormTemplateControl("textCNPJ") as ASPxTextBox;
        ASPxTextBox textCodigoInterface = gridCadastro.FindEditFormTemplateControl("textCodigoInterface") as ASPxTextBox;
        ASPxSpinEdit btnEditCodigoPessoa = gridCadastro.FindEditFormTemplateControl("btnEditCodigoPessoa") as ASPxSpinEdit;
        ASPxComboBox dropAgenteMercado = gridCadastro.FindEditFormTemplateControl("dropAgenteMercado") as ASPxComboBox;

        Emissor emissor = new Emissor();
        emissor.Nome = textNome.Text;
        emissor.TipoEmissor = Convert.ToByte(dropTipoEmissor.SelectedItem.Value);
        emissor.IdSetor = Convert.ToInt16(dropSetor.SelectedItem.Value);

        if (!string.IsNullOrEmpty(textCNPJ.Text))
            emissor.Cnpj = textCNPJ.Text.Replace(".", "").Replace("-", "").Replace("/", "").Replace("\\", "");
        else
            emissor.Cnpj = null;

        if (!string.IsNullOrEmpty(btnEditCodigoPessoa.Text))
            emissor.IdPessoa = Convert.ToInt32(btnEditCodigoPessoa.Text);
        else
            emissor.IdPessoa = null;

        if (!string.IsNullOrEmpty(textCodigoInterface.Text))
            emissor.CodigoInterface = textCodigoInterface.Text;
        else
            emissor.CodigoInterface = null;

        if (dropAgenteMercado.SelectedIndex != -1)
            emissor.IdAgente = Convert.ToInt32(dropAgenteMercado.SelectedItem.Value);
        else
            emissor.IdAgente = null;

        emissor.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de Emissor - Operacao: Insert Emissor: " + emissor.IdEmissor + UtilitarioWeb.ToString(emissor),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdEmissor");
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idEmissor = Convert.ToInt32(keyValuesId[i]);

                Emissor emissor = new Emissor();
                if (emissor.LoadByPrimaryKey(idEmissor)) {
                    //
                    Emissor emissorClone = (Emissor)Utilitario.Clone(emissor);
                    //

                    emissor.MarkAsDeleted();
                    emissor.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de Emissor - Operacao: Delete Emissor: " + idEmissor + UtilitarioWeb.ToString(emissorClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "Cnpj")
        {
            string cnpj = Utilitario.MascaraCNPJ(Convert.ToString(e.GetListSourceFieldValue(EmissorMetadata.ColumnNames.Cnpj)));

            e.Value = cnpj;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        ASPxTextBox textNome = gridCadastro.FindEditFormTemplateControl("textNome") as ASPxTextBox;
        ASPxComboBox dropSetor = gridCadastro.FindEditFormTemplateControl("dropSetor") as ASPxComboBox;
        ASPxComboBox dropTipoEmissor = gridCadastro.FindEditFormTemplateControl("dropTipoEmissor") as ASPxComboBox;
        ASPxTextBox textCNPJ = gridCadastro.FindEditFormTemplateControl("textCNPJ") as ASPxTextBox;
        ASPxSpinEdit btnEditCodigoPessoa = gridCadastro.FindEditFormTemplateControl("btnEditCodigoPessoa") as ASPxSpinEdit;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(textNome);
        controles.Add(dropSetor);
        controles.Add(dropTipoEmissor);

        if (EstendeConceitoPessoaAgente)
            controles.Add(btnEditCodigoPessoa);

        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        if (!string.IsNullOrEmpty(btnEditCodigoPessoa.Text))
        {
            EmissorCollection emissorColl = new EmissorCollection();
            emissorColl.Query.Where(emissorColl.Query.IdPessoa.Equal(Convert.ToInt32(btnEditCodigoPessoa.Text)));

            if (!this.gridCadastro.IsNewRowEditing)
            {
                int idEmissor = Convert.ToInt32(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, EmissorMetadata.ColumnNames.IdEmissor));
                emissorColl.Query.Where(emissorColl.Query.IdEmissor.NotEqual(idEmissor));
            }

            if (emissorColl.Query.Load())
            {
                e.Result = "Já existe Emissor cadastrado com a Pessoa informada!!";
                return;
            }
        }

        string documento = textCNPJ.Text.Replace(".", "").Replace("/", "").Replace("-", "");
        if (!string.IsNullOrEmpty(documento))
        {
            if (!string.IsNullOrEmpty(textCNPJ.Text))
            {
                if (!Utilitario.ValidaCNPJ(textCNPJ.Text))
                {
                    e.Result = "CNPJ inválido!";
                    return;
                }
            }

            EmissorCollection emissorColl = new EmissorCollection();
            emissorColl.Query.Where(emissorColl.Query.Cnpj.Equal(documento));

            if (!this.gridCadastro.IsNewRowEditing)
            {
                int idEmissor = Convert.ToInt32(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, EmissorMetadata.ColumnNames.IdEmissor));
                emissorColl.Query.Where(emissorColl.Query.IdEmissor.NotEqual(idEmissor));
            }

            if (emissorColl.Query.Load())
            {
                e.Result = "Já existe Emissor cadastrado com o CNPJ informado!!";
                return;
            }

        }
    }

    protected void EsDSPessoa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        PessoaCollection coll = new PessoaCollection();

        coll.Query.Where(coll.Query.Tipo.Equal((int)TipoPessoa.Juridica));
        coll.Query.OrderBy(coll.Query.Apelido.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void labelPessoa_Load(object sender, EventArgs e)
    {
        if (EstendeConceitoPessoaAgente)
            (sender as Label).CssClass = "labelRequired";
        else
            (sender as Label).CssClass = "labelNormal";
    }

    protected void btnEditCodigoPessoa_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as ASPxSpinEdit).ClientEnabled = false;
        else
            (sender as ASPxSpinEdit).ClientEnabled = true;
    }

    

    protected void pessoa_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string resultado = string.Empty;
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idPessoa = Convert.ToInt32(e.Parameter);
            Pessoa pessoa = new Pessoa();
            if (pessoa.LoadByPrimaryKey(idPessoa))
            {
                resultado = pessoa.Nome;
                resultado += "|" + pessoa.Nome;
                resultado += "|" + pessoa.Cpfcnpj;                
            }
        }
        e.Result = resultado;
    }

    protected void CamposPessoa_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string habilita = "S";
        ASPxSpinEdit btnEditCodigoPessoa = gridCadastro.FindEditFormTemplateControl("btnEditCodigoPessoa") as ASPxSpinEdit;

        if (btnEditCodigoPessoa != null && !string.IsNullOrEmpty(btnEditCodigoPessoa.Text))
            habilita = "N";

        e.Result = habilita;
    }
}