using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Web.Common;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_GrupoAfinidade : CadastroBasePage
{

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSGrupoAfinidade_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        GrupoAfinidadeCollection coll = new GrupoAfinidadeCollection();

        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        GrupoAfinidade grupoAfinidade = new GrupoAfinidade();
        int idGrupoAfinidade = (int)e.Keys[0];

        if (grupoAfinidade.LoadByPrimaryKey(idGrupoAfinidade))
        {
            grupoAfinidade.Descricao = Convert.ToString(e.NewValues[GrupoAfinidadeMetadata.ColumnNames.Descricao]);

            grupoAfinidade.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de GrupoAfinidade - Operacao: Update GrupoAfinidade: " + idGrupoAfinidade + UtilitarioWeb.ToString(grupoAfinidade),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        GrupoAfinidade grupoAfinidade = new GrupoAfinidade();

        grupoAfinidade.Descricao = Convert.ToString(e.NewValues[GrupoAfinidadeMetadata.ColumnNames.Descricao]);
        grupoAfinidade.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de GrupoAfinidade - Operacao: Insert GrupoAfinidade: " + grupoAfinidade.IdGrupo + UtilitarioWeb.ToString(grupoAfinidade),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(GrupoAfinidadeMetadata.ColumnNames.IdGrupo);
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idGrupoAfinidade = Convert.ToInt32(keyValuesId[i]);

                GrupoAfinidade grupoAfinidade = new GrupoAfinidade();
                if (grupoAfinidade.LoadByPrimaryKey(idGrupoAfinidade))
                {

                    GrupoAfinidade grupoAfinidadeClone = (GrupoAfinidade)Utilitario.Clone(grupoAfinidade);
                    //
                    grupoAfinidade.MarkAsDeleted();
                    grupoAfinidade.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de GrupoAfinidade - Operacao: Delete GrupoAfinidade: " + idGrupoAfinidade + UtilitarioWeb.ToString(grupoAfinidadeClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }
}