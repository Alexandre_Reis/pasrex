﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Common.Enums;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_ParidadeCambial : Financial.Web.Common.CadastroBasePage
{
    #region DataSources
    protected void EsDSParidadeCambial_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ParidadeCambialCollection coll = new ParidadeCambialCollection();

        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSMoeda_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        MoedaCollection coll = new MoedaCollection();

        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSIndice_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        IndiceCollection coll = new IndiceCollection();

        coll.Query.Where(coll.Query.Tipo == (byte)TipoIndice.Decimal);
        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        ParidadeCambial paridadeCambial = new ParidadeCambial();

        int idParidade = (int)e.Keys[0];

        if (paridadeCambial.LoadByPrimaryKey(idParidade))
        {
            paridadeCambial.IdIndice = Convert.ToInt16(e.NewValues[ParidadeCambialMetadata.ColumnNames.IdIndice]);
            paridadeCambial.Descricao = Convert.ToString(e.NewValues[ParidadeCambialMetadata.ColumnNames.Descricao]);
            
            paridadeCambial.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de ParidadeCambial - Operacao: Update ParidadeCambial: " + idParidade + UtilitarioWeb.ToString(paridadeCambial),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        ParidadeCambial paridadeCambial = new ParidadeCambial();

        paridadeCambial.IdMoedaDenominador = Convert.ToInt16(e.NewValues[ParidadeCambialMetadata.ColumnNames.IdMoedaDenominador]);
        paridadeCambial.IdMoedaNumerador = Convert.ToInt16(e.NewValues[ParidadeCambialMetadata.ColumnNames.IdMoedaNumerador]);
        paridadeCambial.IdIndice = Convert.ToInt16(e.NewValues[ParidadeCambialMetadata.ColumnNames.IdIndice]);
        paridadeCambial.Descricao = Convert.ToString(e.NewValues[ParidadeCambialMetadata.ColumnNames.Descricao]);

        if (paridadeCambial.IdMoedaNumerador.Value == paridadeCambial.IdMoedaDenominador.Value)
        {
            throw new Exception("Moeda Numerador igual à Moeda Denominador");
        }

        ParidadeCambialCollection paridadeCambialCollection = new ParidadeCambialCollection();
        paridadeCambialCollection.Query.Where(paridadeCambialCollection.Query.IdMoedaNumerador.Equal(paridadeCambial.IdMoedaDenominador.Value) &
                                              paridadeCambialCollection.Query.IdMoedaDenominador.Equal(paridadeCambial.IdMoedaNumerador.Value));
        paridadeCambialCollection.Query.Load();

        if (paridadeCambialCollection.Count > 0)
        {
            throw new Exception("Paridade com as mesmas moedas já existente, com posições invertidas");
        }

        paridadeCambialCollection = new ParidadeCambialCollection();
        paridadeCambialCollection.Query.Where(paridadeCambialCollection.Query.IdMoedaNumerador.Equal(paridadeCambial.IdMoedaNumerador.Value) &
                                              paridadeCambialCollection.Query.IdMoedaDenominador.Equal(paridadeCambial.IdMoedaDenominador.Value) &
                                              (paridadeCambialCollection.Query.Descricao.Equal(paridadeCambial.Descricao) |
                                               paridadeCambialCollection.Query.IdIndice.Equal(paridadeCambial.IdIndice))
                                             );
        paridadeCambialCollection.Query.Load();

        if (paridadeCambialCollection.Count > 0)
        {
            throw new Exception("Paridade já existente com mesma descrição ou índice");
        }


        paridadeCambial.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de ParidadeCambial - Operacao: Insert ParidadeCambial: " + paridadeCambial.IdMoedaNumerador + "; " + paridadeCambial.IdMoedaDenominador + UtilitarioWeb.ToString(paridadeCambial),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(ParidadeCambialMetadata.ColumnNames.IdParidade);
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idParidade = Convert.ToInt16(keyValuesId[i]);
                
                ParidadeCambial paridadeCambial = new ParidadeCambial();
                if (paridadeCambial.LoadByPrimaryKey(idParidade))
                {
                    //
                    ParidadeCambial paridadeCambialClone = (ParidadeCambial)Utilitario.Clone(paridadeCambial);
                    //
                    paridadeCambial.MarkAsDeleted();
                    paridadeCambial.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de ParidadeCambial - Operacao: Delete ParidadeCambial: " + idParidade + UtilitarioWeb.ToString(paridadeCambialClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    new protected void gridCadastro_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        base.gridCadastro_CellEditorInitialize(sender, e);

        if (!gridCadastro.IsNewRowEditing)
        {
            if (e.Column.FieldName == ParidadeCambialMetadata.ColumnNames.IdMoedaDenominador || e.Column.FieldName == ParidadeCambialMetadata.ColumnNames.IdMoedaNumerador)
            {
                //e.Editor.Enabled = false;
                e.Editor.BackColor = Color.FromName("#EBEBEB");
                e.Editor.ReadOnly = true;
            }
        }
    }
}