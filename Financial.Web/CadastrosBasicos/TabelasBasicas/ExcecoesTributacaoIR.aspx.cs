﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using Financial.Common;
using Financial.Util;
using Financial.Investidor;
using Financial.ContaCorrente;
using Financial.Security;
using Financial.Fundo;
using Financial.InvestidorCotista;
using Financial.Bolsa;
using Financial.RendaFixa;
using Financial.Web.Common;

using DevExpress.Web;
using DevExpress.Web.Data;

using Financial.ContaCorrente.Enums;
using Financial.InvestidorCotista.Enums;
using Financial.Investidor.Enums;
using Financial.Common.Enums;
using Financial.Bolsa.Enums;
using Financial.RendaFixa.Enums;
using System.Threading;
using EntitySpaces.Interfaces;
using Financial.Security.Enums;
using Financial.Web.Util;
using System.ComponentModel;
using System.Reflection;
using Financial.Util.Enums;
using EntitySpaces.Core;

public partial class CadastrosBasicos_ExcecoesTributacaoIR : CadastroBasePage
{
    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasPopupAtivoBolsa = true;
        this.HasPopupCarteira = true;
        this.HasPopupTituloRF = true;
        

        base.Page_Load(sender, e);
    }
    #region Instância de Classes
    ExcecoesTributacaoIRCollection excecoesTributacaoIRCollection = new ExcecoesTributacaoIRCollection();
    ExcecoesTributacaoIR excecoesTributacaoIR = new ExcecoesTributacaoIR();
    PapelRendaFixaCollection papelRendaFixaCollection = new PapelRendaFixaCollection();
    TituloRendaFixaCollection tituloRendaFixaCollection = new TituloRendaFixaCollection();
    AtivoBolsaCollection ativoBolsaCollection = new AtivoBolsaCollection();
    CarteiraCollection carteiraCollection = new CarteiraCollection();
    #endregion

    #region EntitySpace - Base de Dados

    protected void EsDSAtivoBolsa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        AtivoBolsaCollection coll = new AtivoBolsaCollection();
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSTituloRF_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        TituloRendaFixaCollection coll = new TituloRendaFixaCollection();

        ASPxComboBox dropPapelRF = gridCadastro.FindEditFormTemplateControl("dropPapelRF") as ASPxComboBox;

        if (dropPapelRF != null && dropPapelRF.SelectedIndex > -1)
        {
            int idPapel = Convert.ToInt32(dropPapelRF.SelectedItem.Value);

            TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("P");

            tituloRendaFixaQuery.Select(tituloRendaFixaQuery, (tituloRendaFixaQuery.IdTitulo.Cast(esCastType.String) + " - " + tituloRendaFixaQuery.Descricao).As("DescricaoPapel"));
            tituloRendaFixaQuery.Where(tituloRendaFixaQuery.IdPapel.Equal(idPapel));
            tituloRendaFixaQuery.OrderBy(tituloRendaFixaQuery.DataVencimento.Ascending);

            
            coll.Load(tituloRendaFixaQuery);          
        }

        e.Collection = coll;
    }

    protected void EsDsPapelRF_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery();
        PapelRendaFixaCollection coll = new PapelRendaFixaCollection();

        papelRendaFixaQuery.Select(papelRendaFixaQuery.IdPapel, (papelRendaFixaQuery.IdPapel.Cast(esCastType.String) + " - " + papelRendaFixaQuery.Descricao).As("DescricaoCompleta"));
        coll.Load(papelRendaFixaQuery);

        e.Collection = coll;
    }

    protected void EsDSCategoriaFundo_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CategoriaFundoQuery categoriaFundoQuery = new CategoriaFundoQuery();
        CategoriaFundoCollection coll = new CategoriaFundoCollection();

        categoriaFundoQuery.Select(categoriaFundoQuery.IdCategoria, (categoriaFundoQuery.IdCategoria.Cast(esCastType.String) + " - " + categoriaFundoQuery.Descricao).As("DescricaoCompleta"));
        categoriaFundoQuery.OrderBy(categoriaFundoQuery.Descricao.Ascending);
       
        coll.Load(categoriaFundoQuery);        
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CarteiraCollection coll = new CarteiraCollection();

        CarteiraQuery carteiraQuery = new CarteiraQuery("A");
        ClienteQuery clienteQuery = new ClienteQuery("C");
        UsuarioQuery usuarioQuery = new UsuarioQuery("U");
        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");

        ASPxComboBox dropCategoriaFundo = gridCadastro.FindEditFormTemplateControl("dropCategoriaFundo") as ASPxComboBox;        

        if (dropCategoriaFundo != null && dropCategoriaFundo.SelectedIndex > -1)
        {
            int idCategoria = Convert.ToInt32(dropCategoriaFundo.SelectedItem.Value);

            carteiraQuery.Select(carteiraQuery.IdCarteira, carteiraQuery.Apelido);
            //
            carteiraQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == carteiraQuery.IdCarteira);
            carteiraQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
            carteiraQuery.InnerJoin(usuarioQuery).On(permissaoClienteQuery.IdUsuario == usuarioQuery.IdUsuario);
            //
            carteiraQuery.Where(usuarioQuery.Login == Context.User.Identity.Name,
                                carteiraQuery.IdCategoria.Equal(idCategoria),
                                clienteQuery.StatusAtivo.Equal(StatusAtivoCliente.Ativo),
                                clienteQuery.IdTipo.In(TipoClienteFixo.Clube, TipoClienteFixo.Fundo, TipoClienteFixo.FDIC),
                                clienteQuery.TipoControle.NotEqual((byte)TipoControleCliente.ApenasCotacao));

            coll.Load(carteiraQuery);

            carteiraQuery = new CarteiraQuery("A");
            clienteQuery = new ClienteQuery("C");
            //
            carteiraQuery.Select(carteiraQuery.IdCarteira, carteiraQuery.Apelido);
            carteiraQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == carteiraQuery.IdCarteira);
            carteiraQuery.Where(clienteQuery.TipoControle.Equal((byte)TipoControleCliente.ApenasCotacao),
                                carteiraQuery.IdCategoria.Equal(idCategoria),
                                clienteQuery.IdTipo.In(TipoClienteFixo.Clube, TipoClienteFixo.Fundo, TipoClienteFixo.FDIC),
                                clienteQuery.StatusAtivo.Equal(StatusAtivoCliente.Ativo));
            //
            CarteiraCollection carteiraCollection2 = new CarteiraCollection();
            carteiraCollection2.Load(carteiraQuery);
            //
            coll.Combine(carteiraCollection2);
        }

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    /// <summary>
    /// Acesso a base de dados
    /// </summary>
    protected void EsDSExcecoesTributacaoIR_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ExcecoesTributacaoIRQuery excecoesTributacaoIRQuery = new ExcecoesTributacaoIRQuery("ETIR");
        CarteiraQuery carteiraQuery = new CarteiraQuery("C");
        TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("TR");

        excecoesTributacaoIRQuery.Select(excecoesTributacaoIRQuery, carteiraQuery.Apelido, tituloRendaFixaQuery.DescricaoCompleta);
        excecoesTributacaoIRQuery.LeftJoin(carteiraQuery).On(excecoesTributacaoIRQuery.IdCarteira.Equal(carteiraQuery.IdCarteira));
        excecoesTributacaoIRQuery.LeftJoin(tituloRendaFixaQuery).On(excecoesTributacaoIRQuery.IdTituloRendaFixa.Equal(tituloRendaFixaQuery.IdTitulo));
        excecoesTributacaoIRQuery.OrderBy(excecoesTributacaoIRQuery.IdExcecoesTributacaoIR.Ascending);

        excecoesTributacaoIRCollection = new ExcecoesTributacaoIRCollection();

        excecoesTributacaoIRCollection.Load(excecoesTributacaoIRQuery);
        e.Collection = excecoesTributacaoIRCollection;
    }

    /// <summary>
    /// Acesso a base de dados
    /// </summary>
    protected void EsDSAtivos_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ASPxComboBox dropTipoClasseAtivo = gridCadastro.FindEditFormTemplateControl("dropTipoClasseAtivo") as ASPxComboBox;
        ASPxComboBox dropMercado = gridCadastro.FindEditFormTemplateControl("dropMercado") as ASPxComboBox;

        int idPapel = 0;
        if (dropTipoClasseAtivo.Value != "")
            idPapel = Convert.ToInt32(dropTipoClasseAtivo.Value);

        //Busca a lista de ativos
        tituloRendaFixaCollection = new TituloRendaFixaCollection();
        tituloRendaFixaCollection.Query.Select(tituloRendaFixaCollection.Query.IdTitulo.As("Id"), tituloRendaFixaCollection.Query.DescricaoCompleta).As("Descricao");
        tituloRendaFixaCollection.Query.Where(tituloRendaFixaCollection.Query.IdPapel.Equal(idPapel));
        tituloRendaFixaCollection.Query.Load();

        e.Collection = tituloRendaFixaCollection;

        if (dropMercado.Value == "1")
        {
            ativoBolsaCollection = new AtivoBolsaCollection();
            ativoBolsaCollection.Query.Select(ativoBolsaCollection.Query.CdAtivoBolsa.As("Id"), ativoBolsaCollection.Query.Descricao).As("Descricao");
            ativoBolsaCollection.Query.Load();

            e.Collection = ativoBolsaCollection;
        }
        else if (dropMercado.Value == "5")
        {
            carteiraCollection = new CarteiraCollection();
            carteiraCollection.Query.Select(ativoBolsaCollection.Query.CdAtivoBolsa.As("Id"), ativoBolsaCollection.Query.Descricao).As("Descricao");
            carteiraCollection.Query.Load();

            e.Collection = carteiraCollection;
        }
        else //Se e.Collection não for preenchido, a tela acusa object reference
        {
            carteiraCollection = new CarteiraCollection();
            carteiraCollection.Query.Select(ativoBolsaCollection.Query.CdAtivoBolsa.As("Id"), ativoBolsaCollection.Query.Descricao).As("Descricao");            

            e.Collection = carteiraCollection;
        }
    }

    private class Lista
    {
        public int Id = 0;
        string Descricao = "";
    }

    #endregion

    #region CallBack

    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string texto = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idTitulo = Convert.ToInt32(e.Parameter);
            TituloRendaFixaCollection tituloRendaFixaCollection = new TituloRendaFixaCollection();
            tituloRendaFixaCollection.Query.Select(tituloRendaFixaCollection.Query.DescricaoCompleta);
            tituloRendaFixaCollection.Query.Where(tituloRendaFixaCollection.Query.IdTitulo == idTitulo);

            tituloRendaFixaCollection.Query.Load();

            if (tituloRendaFixaCollection.HasData)
            {
                string descricaoCompleta = (string)tituloRendaFixaCollection[0].GetColumn(TituloRendaFixaMetadata.ColumnNames.DescricaoCompleta).ToString();

                texto = idTitulo + "|" + descricaoCompleta.ToString();

            }
        }
        e.Result = texto;
    }

    protected void ASPxCallbackCarteira_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {        
        
        Carteira carteira = new Carteira();

        string nome = "";
        string resultado = "";
        DateTime? dataDia = null;
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCarteira = Convert.ToInt32(e.Parameter);
            if (carteira.LoadByPrimaryKey(idCarteira))
            {
                if (carteira.IsAtivo)
                {
                    nome = idCarteira + "|" +carteira.str.Apelido;

                    //O permissionamento da carteira é dado direto no cliente vinculado à carteira
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoCliente(idCarteira, HttpContext.Current.User.Identity.Name))
                    {
                        resultado = nome;
                    }
                    else
                    {
                        resultado = "no_access";
                    }
                }
                else
                {
                    resultado = "no_active";
                }
            }
        }

        e.Result = resultado;
    }


    protected void ASPxCallback3_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string nome = "";
        string resultado = "";
        //
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "")
            {
                if (cliente.IsAtivo)
                {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoCliente(idCliente, HttpContext.Current.User.Identity.Name))
                    {
                        nome = cliente.str.Apelido;
                        DateTime dataDia = cliente.DataDia.Value;
                        byte status = cliente.Status.Value;

                        if (status == (byte)StatusCliente.Divulgado && gridCadastro.IsEditing)
                        {
                            resultado = "status_closed";
                        }
                        else 
                        {
                            resultado = nome + "|" + dataDia.ToString().Substring(0, 10) + "|" + Thread.CurrentThread.CurrentCulture.Name;
                        }
                    }
                    else
                    {
                        resultado = "no_access";
                    }
                }
                else
                {
                    resultado = "no_active";
                }
            }
        }
        e.Result = resultado;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        this.Cadastra();
    }

    /// <summary>
    /// Efetua a validação da tela
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        if (!e.Parameter.Equals("deletar"))
        {
            ASPxDateEdit textDataInicio = gridCadastro.FindEditFormTemplateControl("textDataInicio") as ASPxDateEdit;
            ASPxDateEdit textDataFim = gridCadastro.FindEditFormTemplateControl("textDataFim") as ASPxDateEdit;
            ASPxComboBox dropMercado = gridCadastro.FindEditFormTemplateControl("dropMercado") as ASPxComboBox;
            ASPxComboBox dropTipoInvestidor = gridCadastro.FindEditFormTemplateControl("dropTipoInvestidor") as ASPxComboBox;
            ASPxComboBox dropIsencaoIR = gridCadastro.FindEditFormTemplateControl("dropIsencaoIR") as ASPxComboBox;
            ASPxSpinEdit textAliquotaIR = gridCadastro.FindEditFormTemplateControl("textAliquotaIR") as ASPxSpinEdit;
            ASPxComboBox dropRegimeEspecialTributacao = gridCadastro.FindEditFormTemplateControl("dropRegimeEspecialTributacao") as ASPxComboBox;

            ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
            ASPxButtonEdit btnEditAtivoBolsa = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa") as ASPxButtonEdit;
            ASPxSpinEdit hiddenIdTitulo = gridCadastro.FindEditFormTemplateControl("hiddenIdTitulo") as ASPxSpinEdit;

            List<Control> controles = new List<Control>();
            controles.Add(dropMercado);
            controles.Add(dropTipoInvestidor);
            controles.Add(dropIsencaoIR);
            controles.Add(textDataInicio);
            controles.Add(textDataFim);
            controles.Add(dropRegimeEspecialTributacao);

            if (base.TestaObrigatorio(controles) != "")
            {
                e.Result = "Campos com * são obrigatórios!";
                return;
            }

            if (Convert.ToInt32(dropIsencaoIR.SelectedItem.Value) != (int)ListaIsencaoIR.Isento)
            {
                if (string.IsNullOrEmpty(textAliquotaIR.Text))
                {
                    e.Result = "Para 'Tributado' favor preencher uma Alíquota!";
                    return;
                }
            }

            if (Convert.ToInt32(dropMercado.SelectedItem.Value) == (int)TipoMercado.Bolsa)
            {
                if (string.IsNullOrEmpty(btnEditAtivoBolsa.Text))
                {
                    e.Result = "Favor fornecer o código do ativo";
                    return;
                }
            }

            DateTime dataInicio = Convert.ToDateTime(textDataInicio.Text);
            DateTime dataFim = Convert.ToDateTime(textDataFim.Text);

            if (dataInicio.CompareTo(dataFim) > 0)
            {
                e.Result = "Data inicial maior que data final.";
                return;
            }
        }

        e.Result = "";
    }

    /// <summary>
    /// Carrega os dados do ativo da classe de ativos
    /// </summary>
    protected void dropTipoClasseAtivo_OnCallback(object source, DevExpress.Web.CallbackEventArgsBase e)
    {
        //Instancia
        ASPxComboBox dropTipoClasseAtivo = (ASPxComboBox)source;
        papelRendaFixaCollection = new PapelRendaFixaCollection();

        //Busca lista de classe de ativos
        papelRendaFixaCollection.Query.Select();
        papelRendaFixaCollection.Query.Load();

        //Limpa lista para evitar dados duplicados
        dropTipoClasseAtivo.Items.Clear();
        dropTipoClasseAtivo.Items.Add(new ListEditItem(string.Empty, 0));
        foreach (PapelRendaFixa item in papelRendaFixaCollection)
        {
            dropTipoClasseAtivo.Items.Add(new ListEditItem(item.Descricao, item.IdPapel));
        }
    }

    #endregion

    #region gridCadastro
    /// <summary>
    /// Efetua a alteração dos dados dos registros
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        int idExcecoesTributacaoIR = (int)e.Keys[0];
        if (excecoesTributacaoIR.LoadByPrimaryKey(idExcecoesTributacaoIR))
        {
            //Preenche a entidade dinamicamente
            excecoesTributacaoIR = PreencheEntidade<ExcecoesTributacaoIR>(gridCadastro.FindEditFormTemplateControl("panelEdicao").Controls, excecoesTributacaoIR);
            excecoesTributacaoIR.IdExcecoesTributacaoIR = idExcecoesTributacaoIR;

            if (string.IsNullOrEmpty(excecoesTributacaoIR.Ativo))
                excecoesTributacaoIR.Ativo = null;

            excecoesTributacaoIR.IsencaoGanhoCapital = excecoesTributacaoIR.IsencaoIR;
            excecoesTributacaoIR.AliquotaIRGanhoCapital = excecoesTributacaoIR.AliquotaIRGanhoCapital;

            ASPxComboBox dropMercado = gridCadastro.FindEditFormTemplateControl("dropMercado") as ASPxComboBox;
            ASPxComboBox dropCategoriaFundo = gridCadastro.FindEditFormTemplateControl("dropCategoriaFundo") as ASPxComboBox;
            ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
            ASPxButtonEdit btnEditAtivoBolsa = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa") as ASPxButtonEdit;
            ASPxSpinEdit hiddenIdTitulo = gridCadastro.FindEditFormTemplateControl("hiddenIdTitulo") as ASPxSpinEdit;
            ASPxComboBox dropPapelRF = gridCadastro.FindEditFormTemplateControl("dropPapelRF") as ASPxComboBox;

            excecoesTributacaoIR.IdCarteira = null;
            excecoesTributacaoIR.TipoClasseAtivo = null;
            excecoesTributacaoIR.IdTituloRendaFixa = null;
            excecoesTributacaoIR.CdAtivoBolsa = null;
            excecoesTributacaoIR.TipoClasseAtivo = null;
   
            if (dropMercado.Value.Equals("5"))
            {
                if (dropCategoriaFundo.SelectedIndex > -1)
                    excecoesTributacaoIR.TipoClasseAtivo = Convert.ToInt32(dropCategoriaFundo.SelectedItem.Value);

                if (!string.IsNullOrEmpty(btnEditCodigoCarteira.Text))
                    excecoesTributacaoIR.IdCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);   
            }
            else if (dropMercado.Value.Equals("3"))
            {
                if (dropPapelRF.SelectedIndex > -1)
                    excecoesTributacaoIR.TipoClasseAtivo = Convert.ToInt32(dropPapelRF.SelectedItem.Value);

                if (!string.IsNullOrEmpty(hiddenIdTitulo.Text))
                   excecoesTributacaoIR.IdTituloRendaFixa = Convert.ToInt32(hiddenIdTitulo.Text);
            }
            else if (dropMercado.Value.Equals("1"))
            {
                excecoesTributacaoIR.CdAtivoBolsa = btnEditAtivoBolsa.Text;
            }

            excecoesTributacaoIR.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Excecoes Tributacao IR - Operacao: Update ExcecoesTributacaoIR: " + idExcecoesTributacaoIR + UtilitarioWeb.ToString(excecoesTributacaoIR),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// Efetua o cadastro
    /// </summary>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        this.Cadastra();

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// Deleta os registros selecionados
    /// </summary>
    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdExcecoesTributacaoIR");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idExcecoesTributacaoIR = Convert.ToInt32(keyValuesId[i]);
                excecoesTributacaoIR = new ExcecoesTributacaoIR();

                if (excecoesTributacaoIR.LoadByPrimaryKey(idExcecoesTributacaoIR))
                {
                    ExcecoesTributacaoIR excecoesTributacaoIRClone = (ExcecoesTributacaoIR)Utilitario.Clone(excecoesTributacaoIR);

                    excecoesTributacaoIR.MarkAsDeleted();
                    excecoesTributacaoIR.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Excecoes Tributacao IR - Operacao: Delete ExcecoesTributacaoIR: " + idExcecoesTributacaoIR + "; " + UtilitarioWeb.ToString(excecoesTributacaoIRClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// Efetua o DePara dos valores da coluna de cadastro
    /// </summary>
    protected void gridCadastro_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "Mercado")
        {
            if (e.Value != null && !e.Value.Equals(string.Empty))
                e.DisplayText = TipoMercadoDescricao.RetornaStringValue(Convert.ToInt32(e.Value));
        }
        if (e.Column.FieldName == "TipoClasseAtivo")
        {
            if (e.Value != null && !e.Value.Equals(0))
            {
                papelRendaFixaCollection = new PapelRendaFixaCollection();
                papelRendaFixaCollection.Query.Select();
                papelRendaFixaCollection.Query.Load();

                e.DisplayText = papelRendaFixaCollection.FindByPrimaryKey(Convert.ToInt32(e.Value)).Descricao;
            }
        }
        if (e.Column.FieldName == "Ativo")
        {
            if (e.Value != null && !e.Value.Equals(string.Empty))
            {
                tituloRendaFixaCollection = new TituloRendaFixaCollection();
                tituloRendaFixaCollection.Query.Select();
                tituloRendaFixaCollection.Query.Load();

                e.DisplayText = tituloRendaFixaCollection.FindByPrimaryKey(Convert.ToInt32(e.Value)).DescricaoCompleta;
            }
        }
        if (e.Column.FieldName == "TipoInvestidor")
        {
            if (!e.Value.Equals(0))
                e.DisplayText = ListaTipoInvestidorDescricao.RetornaStringValue(Convert.ToInt32(e.Value));
            else
                e.DisplayText = string.Empty;
        }
        if (e.Column.FieldName == "IsencaoIR")
        {
            if (!e.Value.Equals(0))
                e.DisplayText = ListaIsencaoIRDescricao.RetornaStringValue(Convert.ToInt32(e.Value));
            else
                e.DisplayText = string.Empty;
        }
        if (e.Column.FieldName == "IsencaoGanhoCapital")
        {
            if (!e.Value.Equals(0))
                e.DisplayText = ListaIsencaoIRDescricao.RetornaStringValue(Convert.ToInt32(e.Value));
            else
                e.DisplayText = string.Empty;
        }
        if (e.Column.FieldName == "AliquotaIR")
        {
            if (e.Value != null && !e.Value.Equals(string.Empty))
                e.DisplayText = e.Value + "%";
        }
        if (e.Column.FieldName == "AliquotaIRGanhoCapital")
        {
            if (e.Value != null && !e.Value.Equals(string.Empty))
                e.DisplayText = e.Value + "%";
        }

    }

    /// <summary>
    /// Fecha o EditForm e atualiza a grid
    /// Evento é chamado quando o usuário clica no botão para fechar a grid
    /// </summary>
    protected void gridCadastro_CancelRowEditing(object sender, ASPxStartRowEditingEventArgs e)
    {
        e.Cancel = false;
        gridCadastro.DataBind();
    }

    /// <summary>
    /// Carrega a grid cadastro com as respectivas colunas de filtro
    /// </summary>
    protected void gridCadastro_OnLoad(object sender, EventArgs e)
    {
        #region Mercado
        GridViewDataComboBoxColumn columnMercado = (gridCadastro.Columns["Mercado"] as GridViewDataComboBoxColumn);

        columnMercado.PropertiesComboBox.Items.Clear();
        foreach (int r in Enum.GetValues(typeof(TipoMercado)))
        {
            if (r.Equals(3) || r.Equals(5) || r.Equals(1))
                columnMercado.PropertiesComboBox.Items.Add(TipoMercadoDescricao.RetornaStringValue(r), r);
        }
        #endregion

        #region Tipo/Classe Ativo
        GridViewDataComboBoxColumn columnTipoClasseAtivo = (gridCadastro.Columns["Tipo/Classe Ativo"] as GridViewDataComboBoxColumn);

        papelRendaFixaCollection = new PapelRendaFixaCollection();
        papelRendaFixaCollection.Query.Select();
        papelRendaFixaCollection.Query.Load();

        //columnTipoClasseAtivo.PropertiesComboBox.Items.Clear();
        //columnTipoClasseAtivo.PropertiesComboBox.Items.Add(string.Empty);
        //foreach (PapelRendaFixa item in papelRendaFixaCollection)
        //{
        //    columnTipoClasseAtivo.PropertiesComboBox.Items.Add(new ListEditItem(item.Descricao, item.IdPapel));
        //}
        #endregion

        //#region Ativo
        //GridViewDataComboBoxColumn columnAtivo = (gridCadastro.Columns["Ativo"] as GridViewDataComboBoxColumn);

        //tituloRendaFixaCollection = new TituloRendaFixaCollection();
        //tituloRendaFixaCollection.Query.Select();
        //tituloRendaFixaCollection.Query.Where(tituloRendaFixaCollection.Query.DescricaoCompleta.NotEqual(string.Empty));
        //tituloRendaFixaCollection.Query.Load();

        //columnAtivo.PropertiesComboBox.Items.Clear();
        //columnAtivo.PropertiesComboBox.Items.Add(string.Empty);
        //foreach (TituloRendaFixa item in tituloRendaFixaCollection)
        //{
        //    columnAtivo.PropertiesComboBox.Items.Add(new ListEditItem(item.DescricaoCompleta, item.IdTitulo));
        //}
        //#endregion

        #region Tipo Investidor
        GridViewDataComboBoxColumn columnTipoInvestidor = (gridCadastro.Columns["Tipo de Investidor"] as GridViewDataComboBoxColumn);

        columnTipoInvestidor.PropertiesComboBox.Items.Clear();
        columnTipoInvestidor.PropertiesComboBox.Items.Add(string.Empty);
        foreach (int r in Enum.GetValues(typeof(ListaTipoInvestidor)))
        {
            if ((int)r == (int)ListaTipoInvestidor.OffShore)
                continue;

            columnTipoInvestidor.PropertiesComboBox.Items.Add(ListaTipoInvestidorDescricao.RetornaStringValue(r), r);
        }
        #endregion

        #region Isencao IR
        GridViewDataComboBoxColumn columnIsencaoIR = (gridCadastro.Columns["Isenção IR"] as GridViewDataComboBoxColumn);

        columnIsencaoIR.PropertiesComboBox.Items.Clear();
        columnIsencaoIR.PropertiesComboBox.Items.Add(string.Empty);
        foreach (int r in Enum.GetValues(typeof(ListaIsencaoIR)))
        {
            columnIsencaoIR.PropertiesComboBox.Items.Add(ListaIsencaoIRDescricao.RetornaStringValue(r), r);
        }
        #endregion

        #region Isencao Ganho Capital
        GridViewDataComboBoxColumn columnIsencaoGanhoCapital = (gridCadastro.Columns["Isenção Ganho Capital"] as GridViewDataComboBoxColumn);

        if (columnIsencaoGanhoCapital != null)
        {
            columnIsencaoGanhoCapital.PropertiesComboBox.Items.Clear();
            columnIsencaoGanhoCapital.PropertiesComboBox.Items.Add(string.Empty);
            foreach (int r in Enum.GetValues(typeof(ListaIsencaoIR)))
            {
                columnIsencaoGanhoCapital.PropertiesComboBox.Items.Add(ListaIsencaoIRDescricao.RetornaStringValue(r), r);
            }
        }
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "NomeAtivo")
        {
            string nomeAtivo = "";

            //Compose a primary key value
            string tipoMercado = Convert.ToString(e.GetListSourceFieldValue(ExcecoesTributacaoIRMetadata.ColumnNames.Mercado));

            if(tipoMercado == "1")
                nomeAtivo = Convert.ToString(e.GetListSourceFieldValue(ExcecoesTributacaoIRMetadata.ColumnNames.CdAtivoBolsa));

            if(tipoMercado == "3")
            {
                int idTituloRendaFixa = Convert.ToInt32(e.GetListSourceFieldValue(ExcecoesTributacaoIRMetadata.ColumnNames.IdTituloRendaFixa));
                TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
                tituloRendaFixa.LoadByPrimaryKey(idTituloRendaFixa);
                nomeAtivo = tituloRendaFixa.DescricaoCompleta;
            }

            if(tipoMercado == "5")
            {
                int idCarteira = Convert.ToInt32(e.GetListSourceFieldValue(ExcecoesTributacaoIRMetadata.ColumnNames.IdCarteira));
                Carteira carteira = new Carteira();
                carteira.LoadByPrimaryKey(idCarteira);
                nomeAtivo = carteira.Apelido;
            }

            e.Value = nomeAtivo;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e)
    {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing)
        {
            ASPxComboBox dropMercado = gridCadastro.FindEditFormTemplateControl("dropMercado") as ASPxComboBox;

            if (dropMercado.SelectedIndex != -1)
            {
                if (dropMercado.Value.Equals("1")) 
                {
                    gridView.FindControl("trAtivoBolsa");
                }
                if (dropMercado.Value.Equals("3")) 
                {
                    gridView.FindControl("trAtivoRendaFixa");
                }
                if (dropMercado.Value.Equals("5"))
                {
                    gridView.FindControl("trAtivoFundo");
                }
            }
        }
    }

    #endregion

    #region OnLoad
    ///// <summary>
    ///// Carrega a lista da classe ativos da renda fixa
    ///// </summary>
    //protected void dropTipoClasseAtivo_OnLoad(object sender, EventArgs e)
    //{
    //    //Instancia
    //    ASPxComboBox dropTipoClasseAtivo = (ASPxComboBox)sender;
    //    papelRendaFixaCollection = new PapelRendaFixaCollection();

    //    //Busca lista de classe de ativos
    //    papelRendaFixaCollection.Query.Select();
    //    papelRendaFixaCollection.Query.Load();

    //    //Limpa lista para evitar dados duplicados
    //    dropTipoClasseAtivo.Items.Clear();
    //    dropTipoClasseAtivo.Items.Add(new ListEditItem(string.Empty, 0));
    //    foreach (PapelRendaFixa item in papelRendaFixaCollection)
    //    {
    //        dropTipoClasseAtivo.Items.Add(new ListEditItem(item.Descricao, item.IdPapel));
    //    }
    //}

    /// <summary>
    /// Carrega os tipos de mercados
    /// </summary>
    protected void dropMercado_OnLoad(object sender, EventArgs e)
    {
        //Instancia
        ASPxComboBox dropMercado = (ASPxComboBox)sender;

        //Limpa items para evitar dados duplicados
        dropMercado.Items.Clear();
        foreach (int r in Enum.GetValues(typeof(TipoMercado)))
        {
            if (r.Equals(3) || r.Equals(5) || r.Equals(1))
                dropMercado.Items.Add(TipoMercadoDescricao.RetornaStringValue(r), r);
        }
    }

    /// <summary>
    /// Carrega o Enum
    /// </summary>
    protected void dropTipoInvestidor_OnLoad(object sender, EventArgs e)
    {
        //Instancia
        ASPxComboBox dropTipoInvestidor = (ASPxComboBox)sender;

        //Limpa items para evitar dados duplicados
        dropTipoInvestidor.Items.Clear();
        foreach (int r in Enum.GetValues(typeof(ListaTipoInvestidor)))
        {
            if ((int)r == (int)ListaTipoInvestidor.OffShore)
                continue;

            dropTipoInvestidor.Items.Add(ListaTipoInvestidorDescricao.RetornaStringValue(r), r);
        }
    }

    /// <summary>
    /// Carrega Enum
    /// </summary>
    protected void dropIsencaoIR_OnLoad(object sender, EventArgs e)
    {
        //Instancia
        ASPxComboBox dropIsencaoIR = (ASPxComboBox)sender;

        //Limpa items para evitar dados duplicados
        dropIsencaoIR.Items.Clear();
        foreach (int r in Enum.GetValues(typeof(ListaIsencaoIR)))
        {
            dropIsencaoIR.Items.Add(ListaIsencaoIRDescricao.RetornaStringValue(r), r);
        }
    }

    /// <summary>
    /// Carrega o Enum
    /// </summary>
    protected void dropIsencaoGanhoCapital_OnLoad(object sender, EventArgs e)
    {
        //Instancia
        ASPxComboBox dropIsencaoGanhoCapital = (ASPxComboBox)sender;

        //Limpa items para evitar dados duplicados
        dropIsencaoGanhoCapital.Items.Clear();
        foreach (int r in Enum.GetValues(typeof(ListaIsencaoIR)))
        {
            dropIsencaoGanhoCapital.Items.Add(ListaIsencaoIRDescricao.RetornaStringValue(r), r);
        }
    }
    
    #endregion

    #region OnDataBound
    /// <summary>
    /// Carrega os dados dos ativos
    /// </summary>
    protected void dropAtivo_OnDataBound(object sender, EventArgs e)
    {
        //Instancia
        ASPxComboBox dropAtivo = (ASPxComboBox)sender;
        ASPxComboBox dropTipoClasseAtivo = gridCadastro.FindEditFormTemplateControl("dropTipoClasseAtivo") as ASPxComboBox;
        tituloRendaFixaCollection = new TituloRendaFixaCollection();

        //Busca lista de classe de ativos
        if (dropTipoClasseAtivo.SelectedItem != null)
        {
            tituloRendaFixaCollection.Query.Select();
            tituloRendaFixaCollection.Query.Where(tituloRendaFixaCollection.Query.IdPapel.Equal(dropTipoClasseAtivo.SelectedItem.Value));
            tituloRendaFixaCollection.Query.Load();

            //Limpa lista para evitar dados duplicados
            dropAtivo.Items.Clear();
            dropAtivo.Items.Add(string.Empty);
            foreach (TituloRendaFixa item in tituloRendaFixaCollection)
            {
                dropAtivo.Items.Add(new ListEditItem(item.DescricaoCompleta, item.IdTitulo));
            }
        }
    }

    /// <summary>
    /// Carrega os dados dos ativos
    /// </summary>
    //protected void dropTipoClasseAtivo_OnDataBound(object sender, GridViewRowEventArgs e)
    //{
    //    //Instancia
    //    ASPxComboBox dropTipoClasseAtivo = (ASPxComboBox)sender;
    //    ASPxComboBox dropMercado = gridCadastro.FindEditFormTemplateControl("dropMercado") as ASPxComboBox;
    //    tituloRendaFixaCollection = new TituloRendaFixaCollection();

    //    if (dropMercado.SelectedIndex == -1) return;

    //    if (dropMercado.SelectedItem.Value.Equals(3))
    //    {
    //        //Busca lista de classe de ativos
    //        papelRendaFixaCollection.Query.Select();
    //        papelRendaFixaCollection.Query.Load();

    //        //Limpa lista para evitar dados duplicados
    //        dropTipoClasseAtivo.Items.Clear();
    //        dropTipoClasseAtivo.Items.Add(new ListEditItem(string.Empty, 0));
    //        foreach (PapelRendaFixa item in papelRendaFixaCollection)
    //        {
    //            dropTipoClasseAtivo.Items.Add(new ListEditItem(item.Descricao, item.IdPapel));
    //        }
    //    }
    //    else
    //    {
    //        dropTipoClasseAtivo.Visible = false;
    //    }

    //}
    #endregion

    #region Suporte
    /// <summary>
    /// Configura Visibilidade do botão Ok++
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        // Se for Update Não Aparece botão de Ok++
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// Inserção de um novo registro
    /// </summary>
    private void Cadastra()
    {
        excecoesTributacaoIR = PreencheEntidade<ExcecoesTributacaoIR>(gridCadastro.FindEditFormTemplateControl("panelEdicao").Controls, null);

        if (string.IsNullOrEmpty(excecoesTributacaoIR.Ativo))
            excecoesTributacaoIR.Ativo = null;

        if (excecoesTributacaoIR.TipoClasseAtivo == 0)
            excecoesTributacaoIR.TipoClasseAtivo = null;

        ASPxComboBox dropPapelRF = gridCadastro.FindEditFormTemplateControl("dropPapelRF") as ASPxComboBox;
        ASPxComboBox dropCategoriaFundo = gridCadastro.FindEditFormTemplateControl("dropCategoriaFundo") as ASPxComboBox;
        ASPxComboBox dropMercado = gridCadastro.FindEditFormTemplateControl("dropMercado") as ASPxComboBox;
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxButtonEdit btnEditAtivoBolsa = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa") as ASPxButtonEdit;
        ASPxSpinEdit hiddenIdTitulo = gridCadastro.FindEditFormTemplateControl("hiddenIdTitulo") as ASPxSpinEdit;
    
        excecoesTributacaoIR.IdTituloRendaFixa = null;
        excecoesTributacaoIR.CdAtivoBolsa = null;
        excecoesTributacaoIR.IdCarteira = null;
        excecoesTributacaoIR.TipoClasseAtivo = null;

        if (dropMercado.Value.Equals("5"))
        {
            if (dropCategoriaFundo.SelectedIndex > -1)
                excecoesTributacaoIR.TipoClasseAtivo = Convert.ToInt32(dropCategoriaFundo.SelectedItem.Value);

            if (!string.IsNullOrEmpty(btnEditCodigoCarteira.Text))
                excecoesTributacaoIR.IdCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);             
        }
        else if (dropMercado.Value.Equals("3"))
        {
            if (dropPapelRF.SelectedIndex > -1)
                excecoesTributacaoIR.TipoClasseAtivo = Convert.ToInt32(dropPapelRF.SelectedItem.Value);

            if (!string.IsNullOrEmpty(hiddenIdTitulo.Text))
                excecoesTributacaoIR.IdTituloRendaFixa = Convert.ToInt32(hiddenIdTitulo.Text);
        }
        else if (dropMercado.Value.Equals("1"))
        {
            excecoesTributacaoIR.CdAtivoBolsa = btnEditAtivoBolsa.Text;
            
        }

        excecoesTributacaoIR.IsencaoGanhoCapital = excecoesTributacaoIR.IsencaoIR;
        excecoesTributacaoIR.AliquotaIRGanhoCapital = excecoesTributacaoIR.AliquotaIRGanhoCapital;
        excecoesTributacaoIR.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro Excecoes Tributacao IR - Operacao: Insert ExcecoesTributacaoIR: " + excecoesTributacaoIR.IdExcecoesTributacaoIR + UtilitarioWeb.ToString(excecoesTributacaoIR),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    /// <summary>
    /// Converte o valor para o tipo especificado
    /// </summary>
    /// <param name="value">O valor a ser convertido</param>
    /// <param name="conversionType">O tipo a ser convertido</param>
    /// <returns>Retorna o valor convertido</returns>
    public static object ChangeType(object value, Type conversionType)
    {
        try
        {
            if (conversionType == null)
            {
                throw new ArgumentNullException("conversionType");
            }
            if (conversionType.IsGenericType && conversionType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
            {
                if (value == null)
                {
                    return null;
                }
                NullableConverter nullableConverter = new NullableConverter(conversionType);
                conversionType = nullableConverter.UnderlyingType;
            }
            return Convert.ChangeType(value, conversionType);
        }
        catch
        {
            //Caso não consiga converter o valor, uma nova instância é gerada para setar um valor default
            return value = Activator.CreateInstance(conversionType);
        }
    }

    /// <summary>
    /// Preenche a entidade com os valores dos controles 
    /// OBS: É necessário que os controles da tela possuam seu ID correspondente as colunas do Banco de Dados
    /// </summary>
    /// <typeparam name="T">Tipo de classe a ser preenchida</typeparam>
    /// <param name="controlCollection">Lista de controles a ser percorrida</param>
    /// <param name="Entidade">Entidade do update</param>
    /// <returns>Entidade preenchida</returns>
    public T PreencheEntidade<T>(ControlCollection controlCollection, T Entidade) where T : new()
    {
        if (Entidade == null)
            Entidade = new T();

        //Instancia lista de controles obrigatórios
        List<string> listaCamposObrigatorios = new List<string>();

        #region Campos Obrigatórios
        foreach (Control control in controlCollection)
        {
            if (control is Label)
            {
                //Verifica se o campo tem preenchimento obrigatório
                if (control.GetType().GetProperty("CssClass").GetValue(control, null).Equals("labelRequired"))
                {
                    //Devolve o nome da propriedade
                    string controle = control.ClientID.Substring(control.ClientID.IndexOf("label")).Replace("label", "");
                    //Adiciona os campos obrigatórios
                    listaCamposObrigatorios.Add(controle);
                }
            }
        }
        #endregion

        #region Preenche Entidade
        //Percorre a lista de controles
        foreach (Control control in controlCollection)
        {
            //Verifica somente os controles que possuem valor para preencher a entidade
            if (!(control is Label))
            {
                //Instância valor que irá receber o valor dos controles
                object valor = new object();

                //Retira os prefixos dos controles para encontrar as propriedades da entidade(WebControls e Controles do DevExpress)
                string propriedade = control.ClientID.Substring(control.ClientID.LastIndexOf("_")).Replace("text", "").Replace("drop", "").Replace("_", "");

                //Verifica se o controle é um dropdown
                if (control.GetType().GetProperty("Value") != null && control.GetType().GetProperty("Value").GetValue(control, null) != null)
                    //DropDown
                    valor = control.GetType().GetProperty("Value").GetValue(control, null);
                else if (control.GetType().GetProperty("Text") != null && control.GetType().GetProperty("Text").GetValue(control, null) != null)
                    //TextBox
                    valor = control.GetType().GetProperty("Text").GetValue(control, null);

                //Verifica se há campos obrigatórios
                if (listaCamposObrigatorios.Contains(propriedade) && valor.Equals(string.Empty))
                    throw new Exception("Campos com * são obrigatórios!");

                //Verifica se foi encontrado essa propriedade na entidade
                if (Entidade.GetType().GetProperty(propriedade) != null)
                    //Preenche a entidade com o valor encontrado
                    Entidade.GetType().GetProperty(propriedade).SetValue(Entidade,
                                                                         ChangeType(valor, Entidade.GetType().GetProperty(propriedade).PropertyType),
                                                                         null);
            }
        }
        #endregion

        //Retorna a entidade preenchida
        return Entidade;
    }

    /// <summary>
    /// Cria dinamicamente os controle na tela
    /// </summary>
    /// <typeparam name="T">Tipo do controle a ser criado</typeparam>
    /// <param name="idControle">ID do controle a ser criado</param>
    /// <param name="textoControle">Texto do controle a ser exibido na tela</param>
    /// <returns>Controle com as propriedades principais criadas</returns>
    private T CriaControleTela<T>(string idControle, string textoControle) where T : new()
    {
        //Instancia o tipo de controle a ser criado
        T controleCriado = new T();
        //Pega a propriedade ID do controle
        PropertyInfo id = controleCriado.GetType().GetProperty("ID");
        //Seta o valor ID para o controle
        id.SetValue(controleCriado, idControle, null);
        //Pega a propriedade TEXT do controle
        PropertyInfo texto = controleCriado.GetType().GetProperty("Text");
        //Seta o valor TEXT para o controle
        texto.SetValue(controleCriado, textoControle, null);
        //Retorna o controle com as propriedades preenchidas
        return controleCriado;
    }
    #endregion

    protected void callbackCategoria_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
        if (!gridCadastro.IsNewRowEditing)
        {
            int editingRowVisibleIndex = gridCadastro.EditingRowVisibleIndex;
            e.Result = gridCadastro.GetRowValues(editingRowVisibleIndex, ExcecoesTributacaoIRMetadata.ColumnNames.TipoClasseAtivo).ToString();            
        }
    }

}