﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Web.Common;
using Financial.Common;
using DevExpress.Web;
using System.Collections.Generic;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Security;
using Financial.Util;
using EntitySpaces.Interfaces;
using Financial.Common.Enums;
using System.Reflection;

public partial class CadastrosBasicos_LocalCustodia : CadastroBasePage
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSLocalCustodia_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        LocalCustodiaCollection coll = new LocalCustodiaCollection();

        coll.Query.OrderBy(coll.Query.Codigo.Ascending);
        coll.LoadAll();

        //Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        LocalCustodia localCustodia = new LocalCustodia();

        int IdLocalCustodia = (int)e.Keys[0];

        if (localCustodia.LoadByPrimaryKey(IdLocalCustodia))
        {
            ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
            ASPxTextBox textCnpj = gridCadastro.FindEditFormTemplateControl("textCnpj") as ASPxTextBox;
            ASPxTextBox textCodigo = gridCadastro.FindEditFormTemplateControl("textCodigo") as ASPxTextBox;

            localCustodia.Descricao = textDescricao.Text;
            localCustodia.Cnpj = textCnpj.Text;
            localCustodia.Codigo = textCodigo.Text;
            localCustodia.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de Local de Custódia - Operacao: Update Local de Custódia: " + IdLocalCustodia + UtilitarioWeb.ToString(localCustodia),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        LocalCustodia localCustodiaClone;
        LocalCustodia localCustodia;

        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(LocalCustodiaMetadata.ColumnNames.IdLocalCustodia);
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idLocalCustodia = Convert.ToInt32(keyValuesId[i]);

                localCustodia = new LocalCustodia();
                if (localCustodia.LoadByPrimaryKey(idLocalCustodia))
                {
                    localCustodiaClone = (LocalCustodia)Utilitario.Clone(localCustodia);
                    localCustodia = new LocalCustodia();
                    localCustodia.Query.Where(localCustodia.Query.IdLocalCustodia == idLocalCustodia);
                    if (localCustodia.Query.Load())
                    {
                        foreach (FieldInfo fieldInfo in typeof(LocalCustodiaFixo).GetFields())
                        {
                            int idEnum = Convert.ToInt32(fieldInfo.GetValue(fieldInfo));

                            if (idLocalCustodia == idEnum)
                            {
                                throw new Exception("Local de Custóida " + localCustodia.Descricao + " não pode ser excluído por ser primário do sistema.");
                            }
                        }

                        localCustodia.MarkAsDeleted();
                        localCustodia.Save();

                        #region Log do Processo
                        HistoricoLog historicoLog = new HistoricoLog();
                        historicoLog.InsereHistoricoLog(DateTime.Now,
                                                        DateTime.Now,
                                                        "Cadastro de Local de Custódia - Operacao: Delete Local de Custódia: " + idLocalCustodia + UtilitarioWeb.ToString(localCustodiaClone),
                                                        HttpContext.Current.User.Identity.Name,
                                                        UtilitarioWeb.GetIP(Request),
                                                        "",
                                                        HistoricoLogOrigem.Outros);
                        #endregion
                    }
                }
            }
        }


        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// Verifica quais campos possuem preenchimento obrigatório
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
        ASPxTextBox textCodigo = gridCadastro.FindEditFormTemplateControl("textCodigo") as ASPxTextBox;
        ASPxTextBox textCnpj = gridCadastro.FindEditFormTemplateControl("textCnpj") as ASPxTextBox;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(textDescricao);
        controles.Add(textCodigo);
        controles.Add(textCnpj);

        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        #region Verifica CNPJ
        if (!ValidaCNPJ(textCnpj.Text))
        {
            e.Result = "CNPJ inválido!";
            return;
        }
        #endregion
    }

    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();

        e.Result = "";
        if (e.Parameter == "clone")
        {
            //e.Result = 0;
        }
        else
        {
            e.Result = "Cadastro feito com sucesso.";
        }
    }

    /// <summary>
    /// Aplica formatação de CNPJ na grid
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs e)
    {
        //Verifica se a coluna percorrida é a de CNPJ
        if (e.Column.FieldName.Equals("Cnpj"))
        {
            //Converte o valor para inteiro e aplica mascara de CNPJ para o valor
            Int64 resultado = 0;
            if (Int64.TryParse(e.Value.ToString(), out resultado))
            {
                e.DisplayText = Convert.ToUInt64(e.Value).ToString(@"00\.000\.000\/0000\-00");
            }
        }
    }

    /// <summary>
    /// Verifica se o CNPJ é válido
    /// </summary>
    /// <param name="cnpj">Valor a ser verificado</param>
    /// <returns>Condição de validação</returns>
    private static bool ValidaCNPJ(string vrCnpj)
    {
        string CNPJ = vrCnpj.Replace(".", "");
        CNPJ = CNPJ.Replace("/", "");
        CNPJ = CNPJ.Replace("-", "");
        int[] digitos, soma, resultado;
        int nrDig;
        string ftmt;
        bool[] CNPJOk;
        ftmt = "6543298765432";
        digitos = new int[14];
        soma = new int[2];
        soma[0] = 0;
        soma[1] = 0;
        resultado = new int[2];
        resultado[0] = 0;
        resultado[1] = 0;
        CNPJOk = new bool[2];
        CNPJOk[0] = false;
        CNPJOk[1] = false;
        try
        {
            for (nrDig = 0; nrDig < 14; nrDig++)
            {
                digitos[nrDig] = int.Parse(CNPJ.Substring(nrDig, 1));
                if (nrDig <= 11) soma[0] += (digitos[nrDig] * int.Parse(ftmt.Substring(nrDig + 1, 1)));
                if (nrDig <= 12) soma[1] += (digitos[nrDig] * int.Parse(ftmt.Substring(nrDig, 1)));
            }
            for (nrDig = 0; nrDig < 2; nrDig++)
            {
                resultado[nrDig] = (soma[nrDig] % 11);
                if ((resultado[nrDig] == 0) || (resultado[nrDig] == 1))
                    CNPJOk[nrDig] = (digitos[12 + nrDig] == 0);
                else
                    CNPJOk[nrDig] = (digitos[12 + nrDig] == (11 - resultado[nrDig]));
            }
            return (CNPJOk[0] && CNPJOk[1]);
        }
        catch
        {
            return false;
        }
    }

    /// <summary>
    /// Busca os dados preenchidos do formulario e preenche o objeto para ser salvo no banco de dados
    /// </summary>
    private void SalvarNovo()
    {
        ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
        ASPxTextBox textCodigo = gridCadastro.FindEditFormTemplateControl("textCodigo") as ASPxTextBox;
        ASPxTextBox textCnpj = gridCadastro.FindEditFormTemplateControl("textCNPJ") as ASPxTextBox;

        LocalCustodia localCustodia = new LocalCustodia();

        localCustodia.Descricao = textDescricao.Text;
        localCustodia.Codigo = textCodigo.Text;
        localCustodia.Cnpj = textCnpj.Text.Replace("/", "").Replace("-", "").Replace(".", "");

        localCustodia.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de Local de Custódia - Operacao: Insert Local de Custódia: " + localCustodia.IdLocalCustodia + UtilitarioWeb.ToString(localCustodia),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }
}
