﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using Financial.Common;
using Financial.Util;
using Financial.Fundo;
using Financial.InvestidorCotista;
using Financial.Investidor;
using Financial.Security;
using Financial.Web.Common;
using Financial.InvestidorCotista.Enums;
using Financial.Util.Enums;
using Financial.Tributo.Enums;

using DevExpress.Web;
using DevExpress.Web.Data;

using System.Threading;
using EntitySpaces.Interfaces;
using Financial.Security.Enums;
using Financial.Web.Util;
using DevExpress.XtraGrid;
using Newtonsoft.Json;


public partial class TabelasBasicas_AlteracaoTributacaoFie : CadastroBasePage
{
    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasFiltro = true;
        this.HasPopupCarteira = true;
        this.HasPopupCotista = true;
        this.HasPanelFieldsLoading = true;
        base.Page_Load(sender, e);
    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSPosicaoCotista_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CarteiraQuery carteiraQuery = new CarteiraQuery("C");
        CotistaQuery cotistaQuery = new CotistaQuery("CT");
        PosicaoCotistaHistoricoQuery posicaoCotistaHistoricoQuery = new PosicaoCotistaHistoricoQuery("PCH");
        OperacaoCotistaQuery operacaoCotistaQuery = new OperacaoCotistaQuery("OC");
        posicaoCotistaHistoricoQuery.Select(posicaoCotistaHistoricoQuery.IdPosicao,
                                            posicaoCotistaHistoricoQuery.IdCarteira,
                                            carteiraQuery.Apelido.As("ApelidoCarteira"),
                                            posicaoCotistaHistoricoQuery.IdCotista,
                                            cotistaQuery.Apelido.As("ApelidoCotista"),
                                            posicaoCotistaHistoricoQuery.DataHistorico,
                                            posicaoCotistaHistoricoQuery.FieTabelaIr,
                                            operacaoCotistaQuery.IdOperacao);
        posicaoCotistaHistoricoQuery.InnerJoin(cotistaQuery).On(posicaoCotistaHistoricoQuery.IdCotista == cotistaQuery.IdCotista);
        posicaoCotistaHistoricoQuery.InnerJoin(carteiraQuery).On(posicaoCotistaHistoricoQuery.IdCarteira == carteiraQuery.IdCarteira);
        posicaoCotistaHistoricoQuery.InnerJoin(operacaoCotistaQuery).On(posicaoCotistaHistoricoQuery.IdOperacao == operacaoCotistaQuery.IdOperacao);
        posicaoCotistaHistoricoQuery.Where(posicaoCotistaHistoricoQuery.FieTabelaIr.IsNotNull());
        posicaoCotistaHistoricoQuery.Where(operacaoCotistaQuery.FieModalidade.Equal(FieModalidade.VGBL.GetHashCode()));
        posicaoCotistaHistoricoQuery.OrderBy(posicaoCotistaHistoricoQuery.DataHistorico.Ascending, cotistaQuery.Apelido.Ascending, operacaoCotistaQuery.IdOperacao.Ascending);

        if (!String.IsNullOrEmpty(btnEditCodigoCarteiraFiltro.Text))
        {
            posicaoCotistaHistoricoQuery.Where(posicaoCotistaHistoricoQuery.IdCarteira.Equal(Convert.ToInt32(btnEditCodigoCarteiraFiltro.Text)));
        }

        if (!String.IsNullOrEmpty(btnEditCodigoCotistaFiltro.Text))
        {
            posicaoCotistaHistoricoQuery.Where(posicaoCotistaHistoricoQuery.IdCotista.Equal(Convert.ToInt32(btnEditCodigoCotistaFiltro.Text)));
        }

        if (!String.IsNullOrEmpty(textDataInicio.Text))
        {
            posicaoCotistaHistoricoQuery.Where(posicaoCotistaHistoricoQuery.DataHistorico.GreaterThanOrEqual(textDataInicio.Text));
        }

        if (!String.IsNullOrEmpty(textDataFim.Text))
        {
            posicaoCotistaHistoricoQuery.Where(posicaoCotistaHistoricoQuery.DataHistorico.LessThanOrEqual(textDataFim.Text));
        }

        PosicaoCotistaHistoricoCollection posicaoCotistaHistoricoCollection = new PosicaoCotistaHistoricoCollection();
        posicaoCotistaHistoricoCollection.Load(posicaoCotistaHistoricoQuery);

        e.Collection = posicaoCotistaHistoricoCollection;

    }

    protected void EsDSCotista_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CotistaCollection coll = new CotistaCollection();
        coll.BuscaCotistasComAcesso(Context.User.Identity.Name);

        e.Collection = coll;
    }

    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CarteiraCollection coll = new CarteiraCollection();
        coll.BuscaCarteirasFundosClubesBoletagem(Context.User.Identity.Name);

        e.Collection = coll;
    }

    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        Carteira carteira = new Carteira();

        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCarteira = Convert.ToInt32(e.Parameter);
            if (carteira.LoadByPrimaryKey(idCarteira))
            {
                if (carteira.IsAtivo)
                {
                    e.Result = carteira.str.Nome;
                }
                else
                {
                    e.Result = "no_active";
                }
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCotista = Convert.ToInt32(e.Parameter);
            Cotista cotista = new Cotista();
            cotista.LoadByPrimaryKey(idCotista);

            if (cotista.str.Apelido != "")
            {
                if (cotista.IsAtivo)
                {
                    PermissaoCotista permissaoCotista = new PermissaoCotista();
                    if (permissaoCotista.RetornaAcessoCotista(idCotista, HttpContext.Current.User.Identity.Name))
                    {
                        nome = cotista.str.Apelido;
                    }
                    else
                    {
                        nome = "no_access";
                    }
                }
                else
                {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e)
    {
        this.FocaCampoGrid("btnEditCodigoCarteira");
        base.gridCadastro_PreRender(sender, e);

        ASPxSpinEdit btnEditCodigoCarteiraFiltro = popupFiltro.FindControl("btnEditCodigoCarteiraFiltro") as ASPxSpinEdit;

        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        Label labelFiltroHeader = gridCadastro.FindStatusBarTemplateControl("labelFiltroHeader") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");

        if (btnEditCodigoCarteiraFiltro != null && btnEditCodigoCarteiraFiltro.Text != "")
        {
            texto.Append(" Carteira: ").Append(btnEditCodigoCarteiraFiltro.Text);
        }
        if (btnEditCodigoCarteiraFiltro.Text != "")
        {
            if (texto.Length > 0)
            {
                texto.Append(" |");
            }
            texto.Append(" Cotista: ").Append(btnEditCodigoCotistaFiltro.Text);
        }
        if (textDataInicio.Text != "")
        {
            if (texto.Length > 0)
            {
                texto.Append(" |");
            }
            texto.Append(" Data Início >= ").Append(textDataInicio.Text.Substring(0, 10));
        }
        if (textDataFim.Text != "")
        {
            if (texto.Length > 0)
            {
                texto.Append(" |");
            }
            texto.Append(" Data Fim <= ").Append(textDataFim.Text.Substring(0, 10));
        }

        labelFiltro.Text = texto.ToString();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        ASPxComboBox dropFieTabelaIr = gridCadastro.FindEditFormTemplateControl("dropFieTabelaIr") as ASPxComboBox;
        ASPxSpinEdit btnEditOperacao = gridCadastro.FindEditFormTemplateControl("btnEditOperacao") as ASPxSpinEdit;
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxSpinEdit btnEditPosicao = gridCadastro.FindEditFormTemplateControl("btnEditPosicao") as ASPxSpinEdit;

        int idCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);
        int fieTabelaIr = Convert.ToInt32(dropFieTabelaIr.SelectedItem.Value);
        int idPosicao = Convert.ToInt32(btnEditPosicao.Text);

        int idOperacao = (int)e.Keys[1];
        DateTime dataPosicaoHistorico = (DateTime)e.Keys[0];

        PosicaoCotistaHistorico posicaoCotistaHistorico = new PosicaoCotistaHistorico();
        posicaoCotistaHistorico.LoadByPrimaryKey(idPosicao, dataPosicaoHistorico);

        posicaoCotistaHistorico.FieTabelaIr = fieTabelaIr;

        posicaoCotistaHistorico.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Tabela de IR - FIE - Operacao: Update PosicaoCotistaHistorico: " + idPosicao + ", " + dataPosicaoHistorico + " - " + UtilitarioWeb.ToString(posicaoCotistaHistorico),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        TributacaoCautelaFie tributacaoCautelaFie = new TributacaoCautelaFie();
        if (tributacaoCautelaFie.LoadByPrimaryKey(dataPosicaoHistorico, idOperacao))
        {
            tributacaoCautelaFie.FieTabelaIr = fieTabelaIr;
        }
        else
        {
            tributacaoCautelaFie.Data = dataPosicaoHistorico;
            tributacaoCautelaFie.IdOperacao = idOperacao;
            tributacaoCautelaFie.FieTabelaIr = fieTabelaIr;
        }

        tributacaoCautelaFie.Save();

        #region Operação retroativa - Verifica se deve marcar o cliente para efetuar o reprocessamento
        Cliente cliente = new Cliente();
        cliente.LoadByPrimaryKey(idCarteira);
        if (DateTime.Compare(cliente.DataDia.Value, dataPosicaoHistorico) > 0)
        {
            BoletoRetroativo boletoRetroativo = new BoletoRetroativo();
            if (boletoRetroativo.LoadByPrimaryKey(idCarteira))
            {
                if (DateTime.Compare(boletoRetroativo.DataBoleto.Value, dataPosicaoHistorico) > 0)
                {
                    boletoRetroativo.DataBoleto = dataPosicaoHistorico;
                }
            }
            else
            {
                boletoRetroativo.IdCliente = idCarteira;
                boletoRetroativo.DataBoleto = dataPosicaoHistorico;
                boletoRetroativo.TipoMercado = (int)TipoMercado.Cotista;
            }

            boletoRetroativo.Save();
        }
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();

    }


}