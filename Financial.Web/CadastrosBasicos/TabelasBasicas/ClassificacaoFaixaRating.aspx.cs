﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using Financial.Investidor;
using Financial.ContaCorrente;
using System.Drawing;
using Financial.ContaCorrente.Enums;
using Financial.Security;
using Financial.Fundo;
using Financial.Bolsa;
using Financial.BMF;
using Financial.RendaFixa;
using Financial.Investidor.Enums;
using Financial.InvestidorCotista;
using Financial.CRM;
using Financial.WebConfigConfiguration;
using Financial.Security.Enums;
using System.Collections.Specialized;
using EntitySpaces.Interfaces;
using Financial.Web.Util;
using Financial.InvestidorCotista.Enums;
using Financial.Web.Common;
using System.Web.Configuration;
using System.Xml;
using Financial.Export;
using Financial.Interfaces.Export;
using DevExpress.Web.Data;
using System.ComponentModel;
using Financial.Fundo.Enums;
using DevExpress.XtraGrid;
using Financial.Common.Enums;

public partial class CadastrosBasicos_ClassificacaoFaixaRating : CadastroBasePage
{
    #region Instância
    //Collection
    ClassificacaoFaixaRatingCollection classificacaoFaixaRatingCollection = new ClassificacaoFaixaRatingCollection();
    PadraoNotasCollection padraoNotasCollection = new PadraoNotasCollection();
    AgenciaClassificadoraCollection agenciaClassificadoraCollection = new AgenciaClassificadoraCollection();
    PerfilFaixaRatingCollection perfilFaixaRatingCollection = new PerfilFaixaRatingCollection();
    //QUERY
    ClassificacaoFaixaRatingQuery classificacaoFaixaRatingQuery = new ClassificacaoFaixaRatingQuery();
    PadraoNotasQuery padraoNotasQuery = new PadraoNotasQuery();
    //Entidade
    ClassificacaoFaixaRating classificacaoFaixaRating = new ClassificacaoFaixaRating();
    PadraoNotas padraoNotas = new PadraoNotas();
    #endregion

    #region esDataSources
    protected void EsDSClassificacaoFaixaRating_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        classificacaoFaixaRatingCollection = new ClassificacaoFaixaRatingCollection();

        classificacaoFaixaRatingCollection.Query.Select();
        classificacaoFaixaRatingCollection.Query.OrderBy(classificacaoFaixaRatingCollection.Query.IdPerfilFaixaRating.Ascending);
        classificacaoFaixaRatingCollection.Query.Load();

        e.Collection = classificacaoFaixaRatingCollection;
    }
    protected void EsDSAgenciaClassificadora_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        agenciaClassificadoraCollection = new AgenciaClassificadoraCollection();

        agenciaClassificadoraCollection.Query.Select(agenciaClassificadoraCollection.Query.IdAgenciaClassificadora, (agenciaClassificadoraCollection.Query.CodigoAgencia.Cast(esCastType.String) + " - " + agenciaClassificadoraCollection.Query.DescricaoAgencia).As("DescricaoAgencia"));
        agenciaClassificadoraCollection.Query.OrderBy(agenciaClassificadoraCollection.Query.IdAgenciaClassificadora.Ascending);
        agenciaClassificadoraCollection.Query.Load();

        e.Collection = agenciaClassificadoraCollection;
    }
    protected void EsDSPerfilFaixaRating_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        perfilFaixaRatingCollection = new PerfilFaixaRatingCollection();

        perfilFaixaRatingCollection.Query.Select(perfilFaixaRatingCollection.Query.IdPerfilFaixaRating,
                                                (perfilFaixaRatingCollection.Query.CodigoPerfil.Cast(EntitySpaces.Interfaces.esCastType.String) + " - " + perfilFaixaRatingCollection.Query.DescricaoPerfil).As("DescricaoPerfil"));
        perfilFaixaRatingCollection.Query.OrderBy(perfilFaixaRatingCollection.Query.IdPerfilFaixaRating.Ascending);
        perfilFaixaRatingCollection.Query.Load();

        e.Collection = perfilFaixaRatingCollection;
    }
    protected void EsDSPadraoNota_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        padraoNotasCollection = new PadraoNotasCollection();

        padraoNotasCollection.Query.OrderBy(padraoNotasCollection.Query.IdPadraoNotas.Ascending);
        padraoNotasCollection.Query.Load();

        e.Collection = padraoNotasCollection;
    }
    #endregion

    #region gridCadastro
    /// <summary>
    /// Carrega os filtros da grid
    /// </summary>
    protected void gridCadastro_OnPreRender(object sender, EventArgs e)
    {
        #region Classificação
        //Busca o controle de filtro da coluna CADASTRO da grid
        GridViewDataComboBoxColumn classificacao = gridCadastro.Columns["Classificacao"] as GridViewDataComboBoxColumn;
        //Limpa a lista de items para remover possiveis items duplicados
        classificacao.PropertiesComboBox.Items.Clear();
        //Percorre o ENUM para preencher o controle do combobox
        foreach (int r in Enum.GetValues(typeof(ListaClassificaoFaixaRating)))
        {
            classificacao.PropertiesComboBox.Items.Add(ListaClassificaoFaixaRatingDescricao.RetornaStringValue(r), r.ToString());
        }
        #endregion
    }

    /// <summary>
    /// Efetua a alteração dos registros
    /// </summary>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        int idClassificacaoFaixaRating = (int)e.Keys[0];
        if (classificacaoFaixaRating.LoadByPrimaryKey(idClassificacaoFaixaRating))
        {
            //Preenche dinamicamente a entidade
            classificacaoFaixaRating = Financial.Util.CadastroDinamico.PreencheEntidade<ClassificacaoFaixaRating>(gridCadastro.FindEditFormTemplateControl("panelEdicao").Controls, classificacaoFaixaRating);
            classificacaoFaixaRating.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de Classificação Faixa Ratings - Operacao: Update Classificação Faixa Rating: " + classificacaoFaixaRating.IdClassificacaoFaixaRating + UtilitarioWeb.ToString(classificacaoFaixaRating),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// Efetua o cadastro 
    /// </summary>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        this.Cadastra();

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// Fecha o EditForm e atualiza a grid
    /// Evento é chamado quando o usuário clica no botão para fechar a grid
    /// </summary>
    protected void gridCadastro_CancelRowEditing(object sender, ASPxStartRowEditingEventArgs e)
    {
        e.Cancel = false;
        gridCadastro.DataBind();
    }
    #endregion

    #region CallBack
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdClassificacaoFaixaRating");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                classificacaoFaixaRating = new ClassificacaoFaixaRating();
                int IdClassificacaoFaixaRating = Convert.ToInt32(keyValuesId[i]);

                if (classificacaoFaixaRating.LoadByPrimaryKey(IdClassificacaoFaixaRating))
                {
                    ClassificacaoFaixaRating classificacaoFaixaRatingClone = (ClassificacaoFaixaRating)Utilitario.Clone(classificacaoFaixaRating);

                    classificacaoFaixaRating.MarkAsDeleted();
                    classificacaoFaixaRating.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de Classificação Faixa Rating - Operacao: Delete Classificação Faixa Rating: " + IdClassificacaoFaixaRating + UtilitarioWeb.ToString(classificacaoFaixaRatingClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
    }

    /// <summary>
    /// Carregao dropdown de acordo com o valor de filtro da agencia
    /// </summary>
    protected void dropIdPadraoNotas_Callback(object sender, DevExpress.Web.CallbackEventArgsBase e)
    {
        ASPxComboBox dropIdPadraoNotas = (ASPxComboBox)sender;

        padraoNotasCollection = new PadraoNotasCollection();
        padraoNotasCollection.Query.Where(padraoNotasCollection.Query.IdAgencia == Convert.ToInt32(e.Parameter));
        padraoNotasCollection.Query.Load();

        //Limpa lista para evitar dados duplicados
        dropIdPadraoNotas.Items.Clear();
        foreach (PadraoNotas item in padraoNotasCollection)
        {
            dropIdPadraoNotas.Items.Add(new ListEditItem(item.CodigoNota, item.IdPadraoNotas));
        }
    }
    #endregion

    #region Button
    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }
    #endregion

    #region OnDataBound
    /// <summary>
    /// Carrega o controle quando solicitar o update
    /// </summary>
    protected void dropIdPadraoNotas_OnDataBound(object sender, EventArgs e)
    {
        ASPxComboBox dropIdAgenciaClassificadora = gridCadastro.FindEditFormTemplateControl("dropIdAgenciaClassificadora") as ASPxComboBox;
        ASPxComboBox dropIdPadraoNotas = (ASPxComboBox)sender;

        if (dropIdPadraoNotas != null && dropIdPadraoNotas.Items.Count == 0)
        {
            if (dropIdAgenciaClassificadora != null && dropIdAgenciaClassificadora.SelectedItem != null)
            {
                padraoNotasCollection = new PadraoNotasCollection();
                padraoNotasCollection.Query.OrderBy(padraoNotasCollection.Query.IdPadraoNotas.Ascending);
                padraoNotasCollection.Query.Where(padraoNotasCollection.Query.IdAgencia == Convert.ToInt32(dropIdAgenciaClassificadora.SelectedItem.Value));
                padraoNotasCollection.Query.Load();

                //Limpa lista para evitar dados duplicados
                dropIdPadraoNotas.Items.Clear();
                foreach (PadraoNotas item in padraoNotasCollection)
                {
                    dropIdPadraoNotas.Items.Add(new ListEditItem(item.CodigoNota, item.IdPadraoNotas));
                }
            }
        }
    }    
    #endregion

    #region OnLoad
    /// <summary>
    /// Carrega drop com os valores do ENUM
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropClassificacao_OnLoad(object sender, EventArgs e)
    {
        ASPxComboBox dropClassificacao = (ASPxComboBox)sender;

        //Limpa controle para evitar items duplicados
        dropClassificacao.Items.Clear();
        //Percorre o ENUM para preencher o controle do combobox
        foreach (int r in Enum.GetValues(typeof(ListaClassificaoFaixaRating)))
        {
            dropClassificacao.Items.Add(ListaClassificaoFaixaRatingDescricao.RetornaStringValue(r), r.ToString());
        }
    }
    #endregion

    #region Suporte
    private void Cadastra()
    {
        classificacaoFaixaRating = Financial.Util.CadastroDinamico.PreencheEntidade<ClassificacaoFaixaRating>(gridCadastro.FindEditFormTemplateControl("panelEdicao").Controls, null);
        classificacaoFaixaRating.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro Classificação Faixa Rating - Operacao: Insert Classificação Faixa Rating: " + classificacaoFaixaRating.IdClassificacaoFaixaRating + UtilitarioWeb.ToString(classificacaoFaixaRating),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }
    #endregion
}