﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Web.Common;
using Financial.Common;
using DevExpress.Web;
using System.Collections.Generic;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Security;
using Financial.Util;
using EntitySpaces.Interfaces;
using Financial.ContaCorrente;
using System.Reflection;
using Financial.Common.Enums;

public partial class CadastrosBasicos_Clearing : CadastroBasePage
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSClearing_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ClearingCollection coll = new ClearingCollection();
        coll.Query.OrderBy(coll.Query.Codigo.Ascending);
        coll.LoadAll();

        //Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSFormaLiquidacao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        FormaLiquidacaoCollection coll = new FormaLiquidacaoCollection();

        coll.Query.OrderBy(coll.Query.IdFormaLiquidacao.Ascending);
        coll.LoadAll();

        //Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSLocalFeriado_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        LocalFeriadoCollection coll = new LocalFeriadoCollection();

        coll.Query.OrderBy(coll.Query.IdLocal.Ascending);
        coll.LoadAll();

        //Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        Clearing clearing = new Clearing();

        int IdClearing = (int)e.Keys[0];

        if (clearing.LoadByPrimaryKey(IdClearing))
        {
            ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
            ASPxTextBox textCodigo = gridCadastro.FindEditFormTemplateControl("textCodigo") as ASPxTextBox;
            ASPxTextBox textCodigoSELIC = gridCadastro.FindEditFormTemplateControl("textCodigoSELIC") as ASPxTextBox;
            ASPxTextBox textCodigoCETIP = gridCadastro.FindEditFormTemplateControl("textCodigoCETIP") as ASPxTextBox;
            ASPxTextBox textCnpj = gridCadastro.FindEditFormTemplateControl("textCNPJ") as ASPxTextBox;
            ASPxComboBox dropFormaLiquidacao = gridCadastro.FindEditFormTemplateControl("dropFormaLiquidacao") as ASPxComboBox;
            ASPxComboBox dropLocalFeriado = gridCadastro.FindEditFormTemplateControl("dropLocalFeriado") as ASPxComboBox;

            clearing.Descricao = textDescricao.Text;
            clearing.Cnpj = textCnpj.Text;
            clearing.Codigo = textCodigo.Text;
            if (!string.IsNullOrEmpty(textCodigoCETIP.Text))
                clearing.CodigoCETIP = textCodigoCETIP.Text;
            else
                clearing.CodigoCETIP = null;

            if (!string.IsNullOrEmpty(textCodigoSELIC.Text))
                clearing.CodigoSELIC = textCodigoSELIC.Text;
            else
                clearing.CodigoSELIC = null;

            clearing.IdFormaLiquidacao = Convert.ToByte(dropFormaLiquidacao.SelectedItem.Value);
            clearing.IdLocalFeriado = Convert.ToInt16(dropLocalFeriado.SelectedItem.Value);

            clearing.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de Local de Custódia - Operacao: Update Local de Custódia: " + IdClearing + UtilitarioWeb.ToString(clearing),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        Clearing clearingClone;
        Clearing clearing;

        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(ClearingMetadata.ColumnNames.IdClearing);
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idClearing = Convert.ToInt32(keyValuesId[i]);

                clearing = new Clearing();
                if (clearing.LoadByPrimaryKey(idClearing))
                {
                    clearingClone = (Clearing)Utilitario.Clone(clearing);
                    clearing = new Clearing();
                    clearing.Query.Where(clearing.Query.IdClearing == idClearing);

                    if (clearing.Query.Load())
                    {
                        foreach (FieldInfo fieldInfo in typeof(ClearingFixo).GetFields())
                        {
                            int idEnum = Convert.ToInt32(fieldInfo.GetValue(fieldInfo));

                            if (idClearing == idEnum)
                            {
                                throw new Exception("Local de Clearing " + clearing.Descricao + " não pode ser excluído por ser primário do sistema.");
                            }
                        }

                        clearing.MarkAsDeleted();
                        clearing.Save();
                    }
                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de Clearing - Operacao: Delete Local de Custódia: " + idClearing + UtilitarioWeb.ToString(clearingClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }


        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// Verifica quais campos possuem preenchimento obrigatório
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
        ASPxTextBox textCodigo = gridCadastro.FindEditFormTemplateControl("textCodigo") as ASPxTextBox;
        ASPxTextBox textCnpj = gridCadastro.FindEditFormTemplateControl("textCNPJ") as ASPxTextBox;
        ASPxComboBox dropFormaLiquidacao = gridCadastro.FindEditFormTemplateControl("dropFormaLiquidacao") as ASPxComboBox;
        ASPxComboBox dropLocalFeriado = gridCadastro.FindEditFormTemplateControl("dropLocalFeriado") as ASPxComboBox;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(textDescricao);
        controles.Add(textCodigo);
        controles.Add(textCnpj);
        controles.Add(dropFormaLiquidacao);
        controles.Add(dropLocalFeriado);

        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        #region Verifica CNPJ
        if (!ValidaCNPJ(textCnpj.Text))
        {
            e.Result = "CNPJ inválido!";
            return;
        }
        #endregion
    }

    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();

        e.Result = "";
        if (e.Parameter == "clone")
        {
            //e.Result = 0;
        }
        else
        {
            e.Result = "Cadastro feito com sucesso.";
        }
    }

    /// <summary>
    /// Aplica formatação de CNPJ na grid
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomColumnDisplayText(object sender, DevExpress.Web.ASPxGridViewColumnDisplayTextEventArgs e)
    {
        //Verifica se a coluna percorrida é a de CNPJ
        if (e.Column.FieldName.Equals("Cnpj"))
        {
            //Converte o valor para inteiro e aplica mascara de CNPJ para o valor
            Int64 resultado = 0;
            if (Int64.TryParse(e.Value.ToString(), out resultado))
            {
                e.DisplayText = Convert.ToUInt64(e.Value).ToString(@"00\.000\.000\/0000\-00");
            }
        }
    }

    /// <summary>
    /// Verifica se o CNPJ é válido
    /// </summary>
    /// <param name="cnpj">Valor a ser verificado</param>
    /// <returns>Condição de validação</returns>
    private static bool ValidaCNPJ(string vrCnpj)
    {
        string CNPJ = vrCnpj.Replace(".", "");
        CNPJ = CNPJ.Replace("/", "");
        CNPJ = CNPJ.Replace("-", "");
        int[] digitos, soma, resultado;
        int nrDig;
        string ftmt;
        bool[] CNPJOk;
        ftmt = "6543298765432";
        digitos = new int[14];
        soma = new int[2];
        soma[0] = 0;
        soma[1] = 0;
        resultado = new int[2];
        resultado[0] = 0;
        resultado[1] = 0;
        CNPJOk = new bool[2];
        CNPJOk[0] = false;
        CNPJOk[1] = false;
        try
        {
            for (nrDig = 0; nrDig < 14; nrDig++)
            {
                digitos[nrDig] = int.Parse(CNPJ.Substring(nrDig, 1));
                if (nrDig <= 11) soma[0] += (digitos[nrDig] * int.Parse(ftmt.Substring(nrDig + 1, 1)));
                if (nrDig <= 12) soma[1] += (digitos[nrDig] * int.Parse(ftmt.Substring(nrDig, 1)));
            }
            for (nrDig = 0; nrDig < 2; nrDig++)
            {
                resultado[nrDig] = (soma[nrDig] % 11);
                if ((resultado[nrDig] == 0) || (resultado[nrDig] == 1))
                    CNPJOk[nrDig] = (digitos[12 + nrDig] == 0);
                else
                    CNPJOk[nrDig] = (digitos[12 + nrDig] == (11 - resultado[nrDig]));
            }
            return (CNPJOk[0] && CNPJOk[1]);
        }
        catch
        {
            return false;
        }
    }

    /// <summary>
    /// Busca os dados preenchidos do formulario e preenche o objeto para ser salvo no banco de dados
    /// </summary>
    private void SalvarNovo()
    {
        ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
        ASPxTextBox textCodigo = gridCadastro.FindEditFormTemplateControl("textCodigo") as ASPxTextBox;
        ASPxTextBox textCodigoSELIC = gridCadastro.FindEditFormTemplateControl("textCodigoSELIC") as ASPxTextBox;
        ASPxTextBox textCodigoCETIP = gridCadastro.FindEditFormTemplateControl("textCodigoCETIP") as ASPxTextBox;
        ASPxTextBox textCnpj = gridCadastro.FindEditFormTemplateControl("textCNPJ") as ASPxTextBox;
        ASPxComboBox dropFormaLiquidacao = gridCadastro.FindEditFormTemplateControl("dropFormaLiquidacao") as ASPxComboBox;
        ASPxComboBox dropLocalFeriado = gridCadastro.FindEditFormTemplateControl("dropLocalFeriado") as ASPxComboBox;

        Clearing clearing = new Clearing();

        clearing.Descricao = textDescricao.Text;
        clearing.Codigo = textCodigo.Text;
        clearing.Cnpj = textCnpj.Text.Replace("/", "").Replace("-", "").Replace(".", "");
        clearing.Codigo = textCodigo.Text;
        if (!string.IsNullOrEmpty(textCodigoCETIP.Text))
            clearing.CodigoCETIP = textCodigoCETIP.Text;
        else
            clearing.CodigoCETIP = null;

        if (!string.IsNullOrEmpty(textCodigoSELIC.Text))
            clearing.CodigoSELIC = textCodigoSELIC.Text;
        else
            clearing.CodigoSELIC = null;
        clearing.IdFormaLiquidacao = Convert.ToByte(dropFormaLiquidacao.SelectedItem.Value);
        clearing.IdLocalFeriado = Convert.ToInt16(dropLocalFeriado.SelectedItem.Value);


        clearing.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de Local de Custódia - Operacao: Insert Local de Custódia: " + clearing.IdClearing + UtilitarioWeb.ToString(clearing),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }
}
