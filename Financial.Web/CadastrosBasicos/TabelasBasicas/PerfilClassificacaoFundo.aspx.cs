﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Fundo;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_PerfilClassificacaoFundo : Financial.Web.Common.CadastroBasePage
{
    protected void EsDSPerfilClassificacaoFundo_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        PerfilClassificacaoFundoCollection coll = new PerfilClassificacaoFundoCollection();

        coll.Query.OrderBy(coll.Query.Codigo.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        PerfilClassificacaoFundo perfilClassificacaoFundo = new PerfilClassificacaoFundo();
        int idPerfil = (int)e.Keys[0];

        if (perfilClassificacaoFundo.LoadByPrimaryKey(idPerfil))
        {
            perfilClassificacaoFundo.Codigo = Convert.ToString(e.NewValues["Codigo"]);
            perfilClassificacaoFundo.Descricao = Convert.ToString(e.NewValues["Descricao"]);
            perfilClassificacaoFundo.InicioVigencia = Convert.ToDateTime(e.NewValues["InicioVigencia"]);
            perfilClassificacaoFundo.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de PerfilClassificacaoFundo - Operacao: Update PerfilClassificacaoFundo: " + idPerfil + UtilitarioWeb.ToString(perfilClassificacaoFundo),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        PerfilClassificacaoFundo perfilClassificacaoFundo = new PerfilClassificacaoFundo();

        perfilClassificacaoFundo.Codigo = Convert.ToString(e.NewValues["Codigo"]);
        perfilClassificacaoFundo.Descricao = Convert.ToString(e.NewValues["Descricao"]);
        perfilClassificacaoFundo.InicioVigencia = Convert.ToDateTime(e.NewValues["InicioVigencia"]);
        perfilClassificacaoFundo.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de PerfilClassificacaoFundo - Operacao: Insert PerfilClassificacaoFundo: " + perfilClassificacaoFundo.IdPerfil + UtilitarioWeb.ToString(perfilClassificacaoFundo),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdPerfil");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idPerfil = Convert.ToInt32(keyValuesId[i]);

                PerfilClassificacaoFundo perfilClassificacaoFundo = new PerfilClassificacaoFundo();
                if (perfilClassificacaoFundo.LoadByPrimaryKey(idPerfil))
                {
                    PerfilClassificacaoFundoEntidadeCollection perfilClassificacaoFundoEntidadeCollection = new PerfilClassificacaoFundoEntidadeCollection();
                    perfilClassificacaoFundoEntidadeCollection.Query.Where(perfilClassificacaoFundoEntidadeCollection.Query.IdPerfilClassificacao.Equal(idPerfil));
                    if (perfilClassificacaoFundoEntidadeCollection.Query.Load())
                    {
                        throw new Exception("Lista " + perfilClassificacaoFundo.Codigo + " não pode ser excluído por possuir entidades/classificações de tipos de fundos relacionadas.");
                    }

                    PerfilClassificacaoFundo perfilClassificacaoFundoClone = (PerfilClassificacaoFundo)Utilitario.Clone(perfilClassificacaoFundo);

                    perfilClassificacaoFundoClone.MarkAsDeleted();
                    perfilClassificacaoFundoClone.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de PerfilClassificacaoFundo - Operacao: Delete PerfilClassificacaoFundo: " + idPerfil + UtilitarioWeb.ToString(perfilClassificacaoFundo),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }
}