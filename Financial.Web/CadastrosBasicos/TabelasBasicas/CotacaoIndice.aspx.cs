﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Web.Common;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Common.Enums;
using Financial.Fundo;

public partial class CadastrosBasicos_CotacaoIndice : CadastroBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasFiltro = true;
        this.HasSummary = false;
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSCotacaoIndice_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CotacaoIndiceCollection coll = new CotacaoIndiceCollection();

        ASPxDateEdit textDataInicio = popupFiltro.FindControl("textDataInicio") as ASPxDateEdit;
        ASPxDateEdit textDataFim = popupFiltro.FindControl("textDataFim") as ASPxDateEdit;

        if (textDataInicio.Text != "") {
            coll.Query.Where(coll.Query.Data.GreaterThanOrEqual(textDataInicio.Text));
        }

        if (textDataFim.Text != "") {
            coll.Query.Where(coll.Query.Data.LessThanOrEqual(textDataFim.Text));
        }

        if (dropIndiceFiltro.SelectedIndex > -1) {
            coll.Query.Where(coll.Query.IdIndice.Equal(Convert.ToInt16(dropIndiceFiltro.SelectedItem.Value)));
        }

        coll.Query.OrderBy(coll.Query.Data.Descending, coll.Query.IdIndice.Ascending);
        coll.LoadAll();
        
        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSIndice_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        IndiceCollection coll = new IndiceCollection();
        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";
        ASPxComboBox dropIndice = gridCadastro.FindEditFormTemplateControl("dropIndice") as ASPxComboBox;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(dropIndice);
        controles.Add(textData);
        controles.Add(textValor);

        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        if (gridCadastro.IsNewRowEditing) {
            short idIndice = Convert.ToInt16(dropIndice.SelectedItem.Value);
            DateTime data = Convert.ToDateTime(textData.Text);

            CotacaoIndice cotacaoIndice = new CotacaoIndice();
            if (cotacaoIndice.LoadByPrimaryKey(data, idIndice)) {
                e.Result = "Registro já existente";
            }
        }
    }

    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();
    }

    private void SalvarNovo()
    {
        ASPxComboBox dropIndice = gridCadastro.FindEditFormTemplateControl("dropIndice") as ASPxComboBox;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;

        CotacaoIndice cotacaoIndice = new CotacaoIndice();

        cotacaoIndice.IdIndice = Convert.ToInt16(dropIndice.Value);
        cotacaoIndice.Data = Convert.ToDateTime(textData.Value);
        cotacaoIndice.Valor  = Convert.ToDecimal(textValor.Value);

        cotacaoIndice.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de Cotação Indice - Operacao: Insert IdIndice: " + dropIndice.Value,
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        if (ParametrosConfiguracaoSistema.Outras.CalculaFatorIndice)
        {            
            cotacaoIndice.AtualizaFatorIndice((short)cotacaoIndice.IdIndice);
        }
    }

    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textData_Init(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing)
            (sender as ASPxDateEdit).Enabled = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropIndice_Load(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing)
            (sender as ASPxComboBox).Enabled = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) {
        if (e.Column.FieldName == "CompositeKey") {
            //Compose a primary key value
            string data = Convert.ToString(e.GetListSourceFieldValue(CotacaoIndiceMetadata.ColumnNames.Data));
            string idIndice = Convert.ToString(e.GetListSourceFieldValue(CotacaoIndiceMetadata.ColumnNames.IdIndice));
            e.Value = data + idIndice;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        CotacaoIndice cotacaoIndice = new CotacaoIndice();

        ASPxComboBox dropIndice = gridCadastro.FindEditFormTemplateControl("dropIndice") as ASPxComboBox;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;

        short idIndice = Convert.ToInt16(dropIndice.SelectedItem.Value);
        DateTime data = Convert.ToDateTime(textData.Text);
        decimal valor = Convert.ToDecimal(textValor.Text);
        if (cotacaoIndice.LoadByPrimaryKey(data, idIndice)) {
            cotacaoIndice.Valor = valor;
            cotacaoIndice.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de CotacaoIndice - Operacao: Update CotacaoIndice: " + data + "; " + idIndice + UtilitarioWeb.ToString(cotacaoIndice),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion

            if (ParametrosConfiguracaoSistema.Outras.CalculaFatorIndice)
            {                
                cotacaoIndice.AtualizaFatorIndice(idIndice);
            }
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) 
    {
        if (e.Parameters == "btnDelete") 
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(CotacaoIndiceMetadata.ColumnNames.IdIndice);
            List<object> keyValuesData = gridCadastro.GetSelectedFieldValues(CotacaoIndiceMetadata.ColumnNames.Data);
            for (int i = 0; i < keyValuesData.Count; i++) {
                int idIndice = Convert.ToInt32(keyValuesId[i]);
                DateTime data = Convert.ToDateTime(keyValuesData[i]);

                CotacaoIndice cotacaoIndice = new CotacaoIndice();
                if (cotacaoIndice.LoadByPrimaryKey(data, (short)idIndice)) {

                    //
                    CotacaoIndice cotacaoIndiceClone = (CotacaoIndice)Utilitario.Clone(cotacaoIndice);
                    
                    cotacaoIndice.MarkAsDeleted();
                    cotacaoIndice.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de CotacaoIndice - Operacao: Delete CotacaoIndice: " + data + "; " + idIndice + UtilitarioWeb.ToString(cotacaoIndiceClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion

                    if (ParametrosConfiguracaoSistema.Outras.CalculaFatorIndice)
                    {                        
                        cotacaoIndice.AtualizaFatorIndice((short)idIndice);
                    }
                }
            }
        }
        else if (e.Parameters == "btnCopy")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(CotacaoIndiceMetadata.ColumnNames.IdIndice);
            List<object> keyValuesData = gridCadastro.GetSelectedFieldValues(CotacaoIndiceMetadata.ColumnNames.Data);
            List<object> keyValuesValor = gridCadastro.GetSelectedFieldValues(CotacaoIndiceMetadata.ColumnNames.Valor);
            for (int i = 0; i < keyValuesData.Count; i++)
            {
                short idIndice = Convert.ToInt16(keyValuesId[i]);
                DateTime data = Convert.ToDateTime(keyValuesData[i]);
                decimal valor = Convert.ToDecimal(keyValuesValor[i]);

                Indice indice = new Indice();
                indice.LoadByPrimaryKey(idIndice);

                DateTime dataProxima = new DateTime();
                if (indice.TipoDivulgacao.Value == (byte)TipoDivulgacaoIndice.Mensal)
                {
                    dataProxima = Calendario.RetornaPrimeiroDiaCorridoMes(data, 1);
                }
                else
                {
                    dataProxima = Calendario.AdicionaDiaUtil(data, 1);
                }

                

                CotacaoIndice cotacaoIndice = new CotacaoIndice();
                if (!cotacaoIndice.LoadByPrimaryKey(dataProxima, idIndice))
                {


                    cotacaoIndice = new CotacaoIndice();
                    cotacaoIndice.Data = dataProxima;
                    cotacaoIndice.IdIndice = idIndice;
                    cotacaoIndice.Valor = valor;

                    cotacaoIndice.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de CotacaoIndice - Operacao: Copy CotacaoIndice: " + data + "; " + idIndice + UtilitarioWeb.ToString(cotacaoIndice),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
                else
                {
                    throw new Exception("Índice " + indice.Descricao + " já tem cotação definida na data " + dataProxima.ToShortDateString() + ".");
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("textData", "textValor");
        base.gridCadastro_PreRender(sender, e);

        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        Label labelFiltroHeader = gridCadastro.FindStatusBarTemplateControl("labelFiltroHeader") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");
        if (textDataInicio.Text != "") {
            texto.Append(" Data Início >= ").Append(textDataInicio.Text.Substring(0, 10));
        }
        if (textDataFim.Text != "") {
            if (texto.Length > 0) {
                texto.Append(" |");
            }

            texto.Append(" Data Fim <= ").Append(textDataFim.Text.Substring(0, 10));
        }
        if (dropIndiceFiltro.SelectedIndex > -1) {
            if (texto.Length > 0) {
                texto.Append(" |");
            }

            texto.Append(" Índice = ").Append(dropIndiceFiltro.Text);
        }

        labelFiltro.Text = texto.ToString();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing) {
            ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
            e.Properties["cpTextData"] = textData.ClientID;

            ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
            e.Properties["cpTextValor"] = textValor.ClientID;
        }
    }
}