﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TabelaProgressivaIR.aspx.cs" Inherits="TabelasBasicas_TabelaProgressivaIR" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>  
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">

    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />    
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    var popup = false;
    document.onkeydown=onDocumentKeyDown;

    </script>
</head>

<body>
    <form id="form1" runat="server">
    
    <div class="divPanel">
        <table width="100%">
            <tr>
                <td>
                    <div id="container">

                        <div id="header">
                            <asp:Label ID="lblHeader" runat="server" Text="Tabela Progressiva de IR"/>
                        </div>
            
                        <div id="mainContent">
                                      
                            <div class="linkButton" >               
                               <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick="gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal4" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
                            </div>

                            <dxwgv:ASPxGridView ID="gridCadastro" runat="server" DataSourceID="EsDSTabelaProgressivaVigencia"
                                KeyFieldName="IdTabelaProgressiva" Width="50%" SettingsEditing-NewItemRowPosition="top" style="margin: auto;"
                                OnRowUpdating="gridCadastro_RowUpdating" OnRowInserting="gridCadastro_RowInserting" OnRowDeleting="gridCadastro_RowDeleting"  
                                OnRowValidating="gridCadastro_RowValidating" SettingsText-CommandUpdate="Ok" SettingsText-CommandCancel="Cancelar">
                                <Columns>
                                    <dxwgv:GridViewCommandColumn VisibleIndex="0" Width="10%" Caption=" " ShowDeleteButton="true" ShowEditButton="true" ShowNewButton="true"/>
                                        
                                    <dxwgv:GridViewDataColumn FieldName="Data" VisibleIndex="1" Width="90%" Caption="Início da Vigência"/>
                                </Columns>

                                <Templates>
                                    <DetailRow>
                                        Tabela IR com data de início de vigência em: <b><%# Eval("Data", "{0:d}")%></b>
                                        <br />
                                        <br />
                                        <dxwgv:ASPxGridView ID="detailGrid" runat="server" DataSourceID="EsDSTabelaProgressivaValores" KeyFieldName="IdTabelaProgressiva;Valor"
                                            Width="100%" OnBeforePerformDataSelect="detailGrid_DataSelect"
                                            OnRowUpdating="detailGrid_RowUpdating" OnRowInserting="detailGrid_RowInserting" OnRowDeleting="detailGrid_RowDeleting" 
                                            OnRowValidating="detailGrid_RowValidating" SettingsText-CommandUpdate="Ok" SettingsText-CommandCancel="Cancelar">
                                            <Columns>
                                                <dxwgv:GridViewCommandColumn VisibleIndex="0" Width="10%" Caption=" " ShowDeleteButton="true" ShowEditButton="true" ShowNewButton="true"/>
                                                <dxwgv:GridViewDataColumn FieldName="Valor" VisibleIndex="1" />
                                                <dxwgv:GridViewDataColumn FieldName="Aliquota" VisibleIndex="2" />
                                            </Columns>
                                            <SettingsCommandButton>
                                                <DeleteButton Text="Excluir"/>
                                                <EditButton Text="Alterar"/>
                                                <NewButton Text="Nova"/>
                                            </SettingsCommandButton>
                                        </dxwgv:ASPxGridView>
                                    </DetailRow>
                                </Templates>
                                <SettingsDetail ShowDetailRow="true" />
                                <SettingsCommandButton>
                                    <DeleteButton Text="Excluir"/>
                                    <EditButton Text="Alterar"/>
                                    <NewButton Text="Nova"/>
                                </SettingsCommandButton>
                            </dxwgv:ASPxGridView>
                                        
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </div>        
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
        
    <cc1:esDataSource ID="EsDSTabelaProgressivaVigencia" runat="server" OnesSelect="EsDSTabelaProgressivaVigencia_esSelect" />
    <cc1:esDataSource ID="EsDSTabelaProgressivaValores" runat="server" OnesSelect="EsDSTabelaProgressivaValores_esSelect" />
    </form>
</body>
</html>