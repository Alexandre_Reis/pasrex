﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Web.Common;
using Financial.Common;
using DevExpress.Web;
using System.Collections.Generic;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Security;
using Financial.Util;
using EntitySpaces.Interfaces;
using Financial.ContaCorrente;
using Financial.Common.Enums;
using System.Reflection;

public partial class CadastrosBasicos_LocalNegociacao : CadastroBasePage
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSLocalNegociacao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        LocalNegociacaoCollection coll = new LocalNegociacaoCollection();
        coll.Query.OrderBy(coll.Query.Codigo.Ascending);
        coll.LoadAll();

        //Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSLocalFeriado_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        LocalFeriadoCollection coll = new LocalFeriadoCollection();

        coll.Query.OrderBy(coll.Query.IdLocal.Ascending);
        coll.LoadAll();

        //Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSMoeda_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        MoedaCollection coll = new MoedaCollection();

        coll.Query.OrderBy(coll.Query.IdMoeda.Ascending);
        coll.LoadAll();

        //Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        LocalNegociacao localNegociacao = new LocalNegociacao();

        int IdLocalNegociacao = (int)e.Keys[0];

        if (localNegociacao.LoadByPrimaryKey(IdLocalNegociacao))
        {
            ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
            ASPxTextBox textCodigo = gridCadastro.FindEditFormTemplateControl("textCodigo") as ASPxTextBox;
            ASPxTextBox textPais = gridCadastro.FindEditFormTemplateControl("textPais") as ASPxTextBox;
            ASPxComboBox dropMoeda = gridCadastro.FindEditFormTemplateControl("dropMoeda") as ASPxComboBox;
            ASPxComboBox dropLocalFeriado = gridCadastro.FindEditFormTemplateControl("dropLocalFeriado") as ASPxComboBox;
            ASPxComboBox dropParaisoFiscal = gridCadastro.FindEditFormTemplateControl("dropParaisoFiscal") as ASPxComboBox;

            localNegociacao.Descricao = textDescricao.Text;
            localNegociacao.Codigo = textCodigo.Text;
            localNegociacao.Pais = textPais.Text;
            localNegociacao.Codigo = textCodigo.Text;
            localNegociacao.IdMoeda = Convert.ToInt16(dropMoeda.SelectedItem.Value);
            localNegociacao.IdLocalFeriado = Convert.ToInt16(dropLocalFeriado.SelectedItem.Value);
            localNegociacao.ParaisoFiscal = Convert.ToString(dropParaisoFiscal.SelectedItem.Value);

            localNegociacao.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de Local de Custódia - Operacao: Update Local de Custódia: " + IdLocalNegociacao + UtilitarioWeb.ToString(localNegociacao),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        LocalNegociacao localNegociacaoClone;
        LocalNegociacao localNegociacao;

        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(LocalNegociacaoMetadata.ColumnNames.IdLocalNegociacao);
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idLocalNegociacao = Convert.ToInt32(keyValuesId[i]);

                localNegociacao = new LocalNegociacao();
               if (localNegociacao.LoadByPrimaryKey(idLocalNegociacao))
                {
                    localNegociacaoClone = (LocalNegociacao)Utilitario.Clone(localNegociacao);
                    localNegociacao = new LocalNegociacao();
                    localNegociacao.Query.Where(localNegociacao.Query.IdLocalNegociacao == idLocalNegociacao);

                    if (localNegociacao.Query.Load())
                    {
                        foreach (FieldInfo fieldInfo in typeof(LocalNegociacaoFixo).GetFields())
                        {
                            int idEnum = Convert.ToInt32(fieldInfo.GetValue(fieldInfo));

                            if (idLocalNegociacao == idEnum)
                            {
                                throw new Exception("Local de Negociação " + localNegociacao.Descricao + " não pode ser excluído por ser primário do sistema.");
                            }
                        }

                        localNegociacao.MarkAsDeleted();
                        localNegociacao.Save();
                    }

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de LocalNegociacao - Operacao: Delete Local de Custódia: " + idLocalNegociacao + UtilitarioWeb.ToString(localNegociacaoClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }


        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// Verifica quais campos possuem preenchimento obrigatório
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
        ASPxTextBox textCodigo = gridCadastro.FindEditFormTemplateControl("textCodigo") as ASPxTextBox;
        ASPxTextBox textPais = gridCadastro.FindEditFormTemplateControl("textPais") as ASPxTextBox;
        ASPxComboBox dropMoeda = gridCadastro.FindEditFormTemplateControl("dropMoeda") as ASPxComboBox;
        ASPxComboBox dropLocalFeriado = gridCadastro.FindEditFormTemplateControl("dropLocalFeriado") as ASPxComboBox;
        ASPxComboBox dropParaisoFiscal = gridCadastro.FindEditFormTemplateControl("dropParaisoFiscal") as ASPxComboBox;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(textDescricao);
        controles.Add(textCodigo);
        controles.Add(textPais);
        controles.Add(dropMoeda);
        controles.Add(dropLocalFeriado);
        controles.Add(dropParaisoFiscal);

        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion
    }

    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();

        e.Result = "";
        if (e.Parameter == "clone")
        {
            //e.Result = 0;
        }
        else
        {
            e.Result = "Cadastro feito com sucesso.";
        }
    }

    /// <summary>
    /// Busca os dados preenchidos do formulario e preenche o objeto para ser salvo no banco de dados
    /// </summary>
    private void SalvarNovo()
    {
        ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
        ASPxTextBox textCodigo = gridCadastro.FindEditFormTemplateControl("textCodigo") as ASPxTextBox;
        ASPxTextBox textPais = gridCadastro.FindEditFormTemplateControl("textPais") as ASPxTextBox;
        ASPxComboBox dropMoeda = gridCadastro.FindEditFormTemplateControl("dropMoeda") as ASPxComboBox;
        ASPxComboBox dropLocalFeriado = gridCadastro.FindEditFormTemplateControl("dropLocalFeriado") as ASPxComboBox;
        ASPxComboBox dropParaisoFiscal = gridCadastro.FindEditFormTemplateControl("dropParaisoFiscal") as ASPxComboBox;

        LocalNegociacao localNegociacao = new LocalNegociacao();
        localNegociacao.Descricao = textDescricao.Text;
        localNegociacao.Codigo = textCodigo.Text;
        localNegociacao.Pais = textPais.Text;
        localNegociacao.Codigo = textCodigo.Text;
        localNegociacao.IdMoeda = Convert.ToInt16(dropMoeda.SelectedItem.Value);
        localNegociacao.IdLocalFeriado = Convert.ToInt16(dropLocalFeriado.SelectedItem.Value);
        localNegociacao.ParaisoFiscal = Convert.ToString(dropParaisoFiscal.SelectedItem.Value);

        localNegociacao.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de Local de Custódia - Operacao: Insert Local de Custódia: " + localNegociacao.IdLocalNegociacao + UtilitarioWeb.ToString(localNegociacao),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }
}
