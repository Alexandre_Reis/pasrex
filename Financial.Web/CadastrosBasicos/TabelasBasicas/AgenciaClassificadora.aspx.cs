﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using DevExpress.Web;
using Financial.RendaFixa;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Util;
using System.Drawing;
using Financial.Web.Common;
using Financial.Common;

public partial class CadastrosBasicos_AgenciaClassificadora : CadastroBasePage
{
    #region Instância
    //Collection
    AgenciaClassificadoraCollection agenciaClassificadoraCollection = new AgenciaClassificadoraCollection();
    //Entidade
    AgenciaClassificadora agenciaClassificadora = new AgenciaClassificadora();
    #endregion

    #region EntitySpace - Base de Dados
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSAgenciaClassificadora_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        agenciaClassificadoraCollection = new AgenciaClassificadoraCollection();
        agenciaClassificadoraCollection.Query.OrderBy(agenciaClassificadoraCollection.Query.IdAgenciaClassificadora.Ascending);
        agenciaClassificadoraCollection.LoadAll();

        e.Collection = agenciaClassificadoraCollection;
    }
    #endregion

    #region GridView Evento
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        agenciaClassificadora = new AgenciaClassificadora();

        int IdAgenciaClassificadora = (int)e.Keys[0];
        if (agenciaClassificadora.LoadByPrimaryKey(IdAgenciaClassificadora))
        {
            agenciaClassificadora = Financial.Util.CadastroDinamico.PreencheEntidade<AgenciaClassificadora>(gridCadastro.FindEditFormTemplateControl("panelEdicao").Controls, agenciaClassificadora);
            agenciaClassificadora.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de Agencia Classificadoras - Operacao: Update Agencia Classificadora: " + IdAgenciaClassificadora + UtilitarioWeb.ToString(agenciaClassificadora),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdAgenciaClassificadora");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                agenciaClassificadora = new AgenciaClassificadora();
                int IdAgenciaClassificadora = Convert.ToInt32(keyValuesId[i]);

                if (agenciaClassificadora.LoadByPrimaryKey(IdAgenciaClassificadora))
                {
                    AgenciaClassificadora agenciaClassificadoraClone = (AgenciaClassificadora)Utilitario.Clone(agenciaClassificadora);

                    agenciaClassificadora.MarkAsDeleted();
                    agenciaClassificadora.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de Agencia Classificadora - Operacao: Delete Agencia Classificadora: " + IdAgenciaClassificadora + UtilitarioWeb.ToString(agenciaClassificadoraClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// Exibe formatação de cnpj na grid
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        //Inicia variáveis
        Int64 resultado = 0;

        //Verifica se a coluna é a de CNPJ
        if (e.Column.FieldName.Equals("Cnpj"))
        {
            if (e.Value != null)
            {
                //Verifica se o valor já está formatado
                if (Int64.TryParse(e.Value.ToString(), out resultado))
                {
                    e.DisplayText = Convert.ToUInt64(e.Value).ToString(@"00\.000\.000\/0000\-00");
                }
            }
        }
    }
    #endregion

    #region Callback Evento
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        ASPxTextBox textCNPJ = gridCadastro.FindEditFormTemplateControl("textCNPJ") as ASPxTextBox;

        if (!string.IsNullOrEmpty(textCNPJ.Text))
        {
            if (ValidaCNPJ(textCNPJ.Text).Equals(false))
                e.Result = "CNPJ Inválido!";
        }
    }

    /// <summary>
    /// Efetua o cadastro - Não fecha o EditForm
    /// </summary>
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();

        e.Result = "Cadastro feito com sucesso.";
    }
    #endregion

    #region Button Evento
    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }
    #endregion

    #region Suporte
    /// <summary>
    /// Efetua o cadastro dos dados da tela
    /// </summary>
    private void SalvarNovo()
    {
        //Instancia
        agenciaClassificadora = new AgenciaClassificadora();
        ASPxTextBox textCNPJ = gridCadastro.FindEditFormTemplateControl("textCNPJ") as ASPxTextBox;

        //Realiza o tratamento para o cnpj
        if (!string.IsNullOrEmpty(textCNPJ.Text))
            agenciaClassificadora.Cnpj = textCNPJ.Text.Replace("/", "").Replace(".", "").Replace("-", "");

        agenciaClassificadora = Financial.Util.CadastroDinamico.PreencheEntidade<AgenciaClassificadora>(gridCadastro.FindEditFormTemplateControl("panelEdicao").Controls, agenciaClassificadora);
        agenciaClassificadora.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de Agencia Classificadora - Operacao: Insert Agencia Classificadora: " + agenciaClassificadora.CodigoAgencia + "-" + agenciaClassificadora.DescricaoAgencia + UtilitarioWeb.ToString(agenciaClassificadora),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    /// <summary>
    /// Verifica se o CNPJ é válido
    /// </summary>
    /// <param name="cnpj">Valor a ser verificado</param>
    /// <returns>Condição de validação</returns>
    private static bool ValidaCNPJ(string vrCnpj)
    {
        string CNPJ = vrCnpj.Replace(".", "");
        CNPJ = CNPJ.Replace("/", "");
        CNPJ = CNPJ.Replace("-", "");
        int[] digitos, soma, resultado;
        int nrDig;
        string ftmt;
        bool[] CNPJOk;
        ftmt = "6543298765432";
        digitos = new int[14];
        soma = new int[2];
        soma[0] = 0;
        soma[1] = 0;
        resultado = new int[2];
        resultado[0] = 0;
        resultado[1] = 0;
        CNPJOk = new bool[2];
        CNPJOk[0] = false;
        CNPJOk[1] = false;
        try
        {
            for (nrDig = 0; nrDig < 14; nrDig++)
            {
                digitos[nrDig] = int.Parse(CNPJ.Substring(nrDig, 1));
                if (nrDig <= 11) soma[0] += (digitos[nrDig] * int.Parse(ftmt.Substring(nrDig + 1, 1)));
                if (nrDig <= 12) soma[1] += (digitos[nrDig] * int.Parse(ftmt.Substring(nrDig, 1)));
            }
            for (nrDig = 0; nrDig < 2; nrDig++)
            {
                resultado[nrDig] = (soma[nrDig] % 11);
                if ((resultado[nrDig] == 0) || (resultado[nrDig] == 1))
                    CNPJOk[nrDig] = (digitos[12 + nrDig] == 0);
                else
                    CNPJOk[nrDig] = (digitos[12 + nrDig] == (11 - resultado[nrDig]));
            }
            return (CNPJOk[0] && CNPJOk[1]);
        }
        catch
        {
            return false;
        }
    }
    #endregion
}