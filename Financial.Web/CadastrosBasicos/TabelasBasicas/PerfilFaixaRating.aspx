﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PerfilFaixaRating.aspx.cs"
    Inherits="CadastrosBasicos_PerfilFaixaRating" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
        var popup = true;
        document.onkeydown = onDocumentKeyDown;
        var operacao = '';
    </script>

    <script type="text/javascript" language="Javascript">
        var isCustomCallback = false;   // usada para controlar o refresh após um delete feito no grid
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                if (operacao == 'deletar')
                {
                    gridCadastro.PerformCallback('btnDelete');                    
                }
                else
                {   
                    if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new')
                    {             
                        gridCadastro.UpdateEdit();
                    }
                    else
                    {
                        callbackAdd.SendCallback();
                    }    
                }  
            } 
            operacao = '';
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            alert('Cadastro feito com sucesso.');
        }        
        " />
        </dxcb:ASPxCallback>
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Perfil Faixa de Rating"></asp:Label>
                            </div>
                            <div id="mainContent">
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;">
                                        <asp:Literal ID="Literal1" runat="server" Text="Novo" /><div>
                                        </div>
                                    </asp:LinkButton><asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false"
                                        ValidationGroup="ATK" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;">
                                        <asp:Literal ID="Literal2" runat="server" Text="Excluir" /><div>
                                        </div>
                                    </asp:LinkButton><asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false"
                                        ValidationGroup="ATK" CssClass="btnPdf" OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal4" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton><asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false"
                                        ValidationGroup="ATK" CssClass="btnExcel" OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal5" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton><asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false"
                                        CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal6" runat="server" Text="Atualizar" /><div>
                                        </div>
                                    </asp:LinkButton></div>
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridCadastro" runat="server" KeyFieldName="IdPerfilFaixaRating"
                                        DataSourceID="EsDSPerfilFaixaRating" OnCustomCallback="gridCadastro_CustomCallback"
                                        OnRowInserting="gridCadastro_RowInserting" OnRowUpdating="gridCadastro_RowUpdating"
                                        OnCancelRowEditing="gridCadastro_CancelRowEditing"
                                        AutoGenerateColumns="False">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn VisibleIndex="0" ShowClearFilterButton="True">
                                                <HeaderTemplate>
                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                                </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="CodigoPerfil" Caption="Código"
                                                Width="10%" />
                                            <dxwgv:GridViewDataTextColumn FieldName="DescricaoPerfil" Caption="Descrição"
                                                Width="20%" />
                                        </Columns>
                                        <Templates>
                                            <EditForm>
                                                <asp:Label ID="labelEdicao" runat="server" CssClass="labelInformation" Text=""></asp:Label>
                                                <asp:Panel ID="panelEdicao" runat="server">
                                                    <div class="editForm">
                                                        <table>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelCodigoPerfil" runat="server" CssClass="labelRequired" Text="Código:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxTextBox ID="textCodigoPerfil" ClientInstanceName="textCodigoPerfil" runat="server"
                                                                        CssClass="textLongo" Text='<%#Eval("CodigoPerfil")%>' />
                                                                </td>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelDescricaoPerfil" runat="server" CssClass="labelRequired" Text="Descrição:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxTextBox ID="textDescricaoPerfil" ClientInstanceName="textDescricaoPerfil"
                                                                        runat="server" CssClass="textLongo" Text='<%#Eval("DescricaoPerfil")%>' />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <div class="linhaH">
                                                        </div>
                                                        <div class="linkButton linkButtonNoBorder popupFooter">
                                                            <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd"
                                                                OnInit="btnOKAdd_Init" OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;">
                                                                <asp:Literal ID="Literal5" runat="server" Text="OK+" /><div>
                                                                </div>
                                                            </asp:LinkButton><asp:LinkButton ID="btnOK" runat="server" Font-Overline="false"
                                                                CssClass="btnOK" OnClientClick="if (operacao != '') return false; operacao='salvar';callbackErro.SendCallback(); return false;">
                                                                <asp:Literal ID="Literal1" runat="server" Text="OK" /><div>
                                                                </div>
                                                            </asp:LinkButton><asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false"
                                                                CssClass="btnCancel" OnClientClick=" gridCadastro.PerformCallback('btnCancel'); return false;">
                                                                <asp:Literal ID="Literal2" runat="server" Text="Cancelar" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                            </EditForm>
                                        </Templates>
                                        <SettingsPopup EditForm-Width="600px" />
                                        <SettingsCommandButton>
                                            <ClearFilterButton Image-Url="../../imagens/funnel--minus.png">
                                                <Image Url="../../imagens/funnel--minus.png">
                                                </Image>
                                            </ClearFilterButton>
                                            <UpdateButton Image-Url="../../imagens/ico_form_ok_inline.gif">
                                                <Image Url="../../imagens/ico_form_ok_inline.gif">
                                                </Image>
                                            </UpdateButton>
                                            <CancelButton Image-Url="../../imagens/ico_form_back_inline.gif">
                                                <Image Url="../../imagens/ico_form_back_inline.gif">
                                                </Image>
                                            </CancelButton>
                                        </SettingsCommandButton>
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro"
            LeftMargin="50" RightMargin="50" />
        <cc1:esDataSource ID="EsDSPerfilFaixaRating" runat="server" OnesSelect="EsDSPerfilFaixaRating_esSelect" />
    </form>
</body>
</html>
