﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Fundo;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_EntidadeClassificacaoPerfilFundo : Financial.Web.Common.CadastroBasePage
{
    protected void EsDSEntidadeClassificacaoPerfilFundo_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        EntidadeClassificacaoPerfilFundoCollection coll = new EntidadeClassificacaoPerfilFundoCollection();

        coll.Query.OrderBy(new EntitySpaces.Interfaces.esOrderByItem[] { coll.Query.IdEntidadePerfilFundo.Ascending, coll.Query.InicioVigencia.Ascending });
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSListaEntidadePerfilFundo_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        EntidadePerfilFundoCollection coll = new EntidadePerfilFundoCollection();

        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        EntidadeClassificacaoPerfilFundo entidadeClassificacaoPerfilFundo = new EntidadeClassificacaoPerfilFundo();
        int idEntidadeClassificacao = (int)e.Keys[0];

        if (entidadeClassificacaoPerfilFundo.LoadByPrimaryKey(idEntidadeClassificacao))
        {
            entidadeClassificacaoPerfilFundo.TipoFundoCarteira = Convert.ToString(e.NewValues["TipoFundoCarteira"]);
            entidadeClassificacaoPerfilFundo.DescricaoTipoFundoCarteira = Convert.ToString(e.NewValues["DescricaoTipoFundoCarteira"]);
            entidadeClassificacaoPerfilFundo.InicioVigencia = Convert.ToDateTime(e.NewValues["InicioVigencia"]);

            if (e.NewValues["IdEntidadePerfilFundo"] != null)
                entidadeClassificacaoPerfilFundo.IdEntidadePerfilFundo = Convert.ToInt32(e.NewValues["IdEntidadePerfilFundo"]);
            else
                entidadeClassificacaoPerfilFundo.IdEntidadePerfilFundo = null;

            entidadeClassificacaoPerfilFundo.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de EntidadeClassificacaoPerfilFundo - Operacao: Update EntidadeClassificacaoPerfilFundo: " + idEntidadeClassificacao + UtilitarioWeb.ToString(entidadeClassificacaoPerfilFundo),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        EntidadeClassificacaoPerfilFundo entidadeClassificacaoPerfilFundo = new EntidadeClassificacaoPerfilFundo();

        entidadeClassificacaoPerfilFundo.TipoFundoCarteira = Convert.ToString(e.NewValues["TipoFundoCarteira"]);
        entidadeClassificacaoPerfilFundo.DescricaoTipoFundoCarteira = Convert.ToString(e.NewValues["DescricaoTipoFundoCarteira"]);
        entidadeClassificacaoPerfilFundo.InicioVigencia = Convert.ToDateTime(e.NewValues["InicioVigencia"]);

        if (e.NewValues["IdEntidadePerfilFundo"] != null)
            entidadeClassificacaoPerfilFundo.IdEntidadePerfilFundo = Convert.ToInt32(e.NewValues["IdEntidadePerfilFundo"]);
        else
            entidadeClassificacaoPerfilFundo.IdEntidadePerfilFundo = null;

        entidadeClassificacaoPerfilFundo.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de EntidadeClassificacaoPerfilFundo - Operacao: Insert EntidadeClassificacaoPerfilFundo: " + entidadeClassificacaoPerfilFundo.IdEntidadeClassificacao + UtilitarioWeb.ToString(entidadeClassificacaoPerfilFundo),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdEntidadeClassificacao");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idEntidadeClassificacao = Convert.ToInt32(keyValuesId[i]);

                EntidadeClassificacaoPerfilFundo entidadeClassificacaoPerfilFundo = new EntidadeClassificacaoPerfilFundo();
                if (entidadeClassificacaoPerfilFundo.LoadByPrimaryKey(idEntidadeClassificacao))
                {
                    PerfilClassificacaoFundoEntidadeCollection perfilClassificacaoFundoEntidadeCollection = new PerfilClassificacaoFundoEntidadeCollection();
                    perfilClassificacaoFundoEntidadeCollection.Query.Where(perfilClassificacaoFundoEntidadeCollection.Query.IdEntidadeClassificacao.Equal(idEntidadeClassificacao));
                    if (perfilClassificacaoFundoEntidadeCollection.Query.Load())
                    {
                        throw new Exception("Tipo Fundo Carteira " + entidadeClassificacaoPerfilFundo.TipoFundoCarteira + " não pode ser excluído por possuir perfis de classificação de fundos relacionados.");
                    }

                    EntidadeClassificacaoPerfilFundo entidadeClassificacaoPerfilFundoClone = (EntidadeClassificacaoPerfilFundo)Utilitario.Clone(entidadeClassificacaoPerfilFundo);

                    entidadeClassificacaoPerfilFundoClone.MarkAsDeleted();
                    entidadeClassificacaoPerfilFundoClone.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de EntidadePerfilFundo - Operacao: Delete EntidadeClassificacaoPerfilFundo: " + idEntidadeClassificacao + UtilitarioWeb.ToString(entidadeClassificacaoPerfilFundo),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }
}