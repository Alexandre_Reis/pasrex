﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Collections.Generic;
using System.Drawing;
using Financial.Common;
using Financial.Util;
using Financial.Investidor;
using DevExpress.Web;
using EntitySpaces.Interfaces;
using Financial.Web.Common;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.CRM;
using Financial.CRM.Enums;

public partial class CadastrosBasicos_Diretor : CadastroBasePage
{
    bool EstendeConceitoPessoaAgente = ParametrosConfiguracaoSistema.Outras.EstendeConceitoPessoaAgente.Equals("S");

    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasPopupPessoa = true;
        this.HasPanelFieldsLoading = true;

        base.Page_Load(sender, e);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSDiretor_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        DiretorCollection coll = new DiretorCollection();
        DiretorQuery diretorQuery = new DiretorQuery("Diretor");
        PessoaQuery pessoaQuery = new PessoaQuery("pessoa");

        diretorQuery.Select(diretorQuery, pessoaQuery.IdPessoa, pessoaQuery.Apelido.As("ApelidoPessoa"));
        diretorQuery.LeftJoin(pessoaQuery).On(diretorQuery.IdPessoa.Equal(pessoaQuery.IdPessoa));
        diretorQuery.OrderBy(diretorQuery.Nome.Ascending);

        coll.Load(diretorQuery);

        e.Collection = coll;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        ASPxTextBox textCodigoCompensacao = gridCadastro.FindEditFormTemplateControl("textCodigoCompensacao") as ASPxTextBox;
        ASPxTextBox textNome = gridCadastro.FindEditFormTemplateControl("textNome") as ASPxTextBox;
        ASPxTextBox textCodigoExterno = gridCadastro.FindEditFormTemplateControl("textCodigoExterno") as ASPxTextBox;
        ASPxTextBox textDescricaoCodigoExterno = gridCadastro.FindEditFormTemplateControl("textDescricaoCodigoExterno") as ASPxTextBox;
        ASPxSpinEdit btnEditCodigoPessoa = gridCadastro.FindEditFormTemplateControl("btnEditCodigoPessoa") as ASPxSpinEdit;

        Diretor diretor = new Diretor();
        int idDiretor = (int)e.Keys[0];

        if (diretor.LoadByPrimaryKey(idDiretor))
        {
            diretor.Nome = textNome.Text;

            if (!string.IsNullOrEmpty(btnEditCodigoPessoa.Text))
                diretor.IdPessoa = Convert.ToInt32(btnEditCodigoPessoa.Text);
            else
                diretor.IdPessoa = null;

            diretor.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de Diretor - Operacao: Update Diretor: " + idDiretor + UtilitarioWeb.ToString(diretor),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        ASPxTextBox textNome = gridCadastro.FindEditFormTemplateControl("textNome") as ASPxTextBox;
        ASPxSpinEdit btnEditCodigoPessoa = gridCadastro.FindEditFormTemplateControl("btnEditCodigoPessoa") as ASPxSpinEdit;

        Diretor diretor = new Diretor();

        diretor.Nome = textNome.Text;

        if (!string.IsNullOrEmpty(btnEditCodigoPessoa.Text))
            diretor.IdPessoa = Convert.ToInt32(btnEditCodigoPessoa.Text);
        else
            diretor.IdPessoa = null;

        diretor.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de Diretor - Operacao: Insert Diretor: " + diretor.IdDiretor + UtilitarioWeb.ToString(diretor),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(DiretorMetadata.ColumnNames.IdDiretor);
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idDiretor = Convert.ToInt32(keyValuesId[i]);

                Diretor diretor = new Diretor();
                if (diretor.LoadByPrimaryKey(idDiretor))
                {
                    Diretor diretorClone = (Diretor)Utilitario.Clone(diretor);
                    //
                    diretor.MarkAsDeleted();
                    //
                    using (esTransactionScope scope = new esTransactionScope())
                    {
                        diretor.Save();

                        #region Log do Processo
                        HistoricoLog historicoLog = new HistoricoLog();
                        historicoLog.InsereHistoricoLog(DateTime.Now,
                                                        DateTime.Now,
                                                        "Cadastro de Diretor - Operacao: Delete Diretor: " + idDiretor + UtilitarioWeb.ToString(diretorClone),
                                                        HttpContext.Current.User.Identity.Name,
                                                        UtilitarioWeb.GetIP(Request),
                                                        "",
                                                        HistoricoLogOrigem.Outros);
                        #endregion

                        //
                        scope.Complete();
                    }
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        ASPxTextBox textNome = gridCadastro.FindEditFormTemplateControl("textNome") as ASPxTextBox;
        ASPxSpinEdit btnEditCodigoPessoa = gridCadastro.FindEditFormTemplateControl("btnEditCodigoPessoa") as ASPxSpinEdit;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();        
        controles.Add(textNome);

        if (EstendeConceitoPessoaAgente)
            controles.Add(btnEditCodigoPessoa);

        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        if (!string.IsNullOrEmpty(btnEditCodigoPessoa.Text))
        {
            DiretorCollection diretorColl = new DiretorCollection();
            diretorColl.Query.Where(diretorColl.Query.IdPessoa.Equal(Convert.ToInt32(btnEditCodigoPessoa.Text)));

            if (!this.gridCadastro.IsNewRowEditing)
            {
                int idDiretor = Convert.ToInt32(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, DiretorMetadata.ColumnNames.IdDiretor));
                diretorColl.Query.Where(diretorColl.Query.IdDiretor.NotEqual(idDiretor));
            }

            if (diretorColl.Query.Load())
            {
                e.Result = "Já existe Diretor cadastrado com a Pessoa informada!!";
                return;
            }
        }
    }

    protected void EsDSPessoa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        PessoaCollection coll = new PessoaCollection();

        coll.Query.Where(coll.Query.Tipo.Equal((int)TipoPessoa.Juridica));
        coll.Query.OrderBy(coll.Query.Apelido.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void labelPessoa_Load(object sender, EventArgs e)
    {
        if (EstendeConceitoPessoaAgente)
            (sender as Label).CssClass = "labelRequired";
        else
            (sender as Label).CssClass = "labelNormal";
    }

    protected void btnEditCodigoPessoa_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as ASPxSpinEdit).ClientEnabled = false;
        else
            (sender as ASPxSpinEdit).ClientEnabled = true;
    }

    protected void pessoa_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string resultado = string.Empty;
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idPessoa = Convert.ToInt32(e.Parameter);
            Pessoa pessoa = new Pessoa();
            if (pessoa.LoadByPrimaryKey(idPessoa))
            {
                resultado = pessoa.Nome;
                resultado += "|" + pessoa.Nome;
                resultado += "|" + pessoa.Cpfcnpj;
            }
        }
        e.Result = resultado;
    }

    protected void CamposPessoa_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string habilita = "S";
        ASPxSpinEdit btnEditCodigoPessoa = gridCadastro.FindEditFormTemplateControl("btnEditCodigoPessoa") as ASPxSpinEdit;

        if (btnEditCodigoPessoa != null && !string.IsNullOrEmpty(btnEditCodigoPessoa.Text))
            habilita = "N";

        e.Result = habilita;
    }
}
