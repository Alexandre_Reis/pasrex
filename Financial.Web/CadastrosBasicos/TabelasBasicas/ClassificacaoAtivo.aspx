﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ClassificacaoAtivo.aspx.cs"
    Inherits="CadastrosBasicos_ClassificacaoAtivo" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
    popup=true;
    document.onkeydown=onDocumentKeyDown;
    var operacao = '';
    </script>

    <script type="text/javascript" language="Javascript">    
		var isCustomCallback = false;   // usada para controlar o refresh após um delete feito no grid
    </script>

</head>
<body>
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) 
        {
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                if (operacao == 'deletar')
                {
                    gridCadastro.PerformCallback('btnDelete');                    
                }
                else
                {   
                    if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new')
                    {            
                        gridCadastro.UpdateEdit();
                    }
                    else
                    {
                        callbackAdd.SendCallback();
                    }    
                }  
            } 
            operacao = '';
        }" />
    </dxcb:ASPxCallback>
    <form id="form1" runat="server">
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Classificação de Fundos, Títulos e Emissores"></asp:Label>
                            </div>
                            <div id="mainContent">
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;">
                                        <asp:Literal ID="Literal3" runat="server" Text="Novo" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" CssClass="btnDelete"
                                        OnClientClick="if (confirm('Tem certeza que quer excluir?')==true) {operacao='deletar'; callbackErro.SendCallback();} return false;">
                                        <asp:Literal ID="Literal2" runat="server" Text="Excluir" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnPdf" OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal4" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnExcel" OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal5" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton>
                                </div>
                                <dxwgv:ASPxGridView ID="gridCadastro" runat="server" OnCustomCallback="gridCadastro_CustomCallback"
                                    OnPreRender="gridCadastro_OnPreRender" OnRowInserting="gridCadastro_RowInserting"
                                    OnCustomColumnDisplayText="gridCadastro_OnCustomColumnDisplayText" OnRowUpdating="gridCadastro_RowUpdating"
                                    OnCancelRowEditing="gridCadastro_CancelRowEditing" DataSourceID="EsDSClassificacaoAtivo"
                                    KeyFieldName="IdClassificacaoAtivo">
                                    <Columns>
                                        <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                                            <HeaderTemplate>
                                                <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                            </HeaderTemplate>
                                        </dxwgv:GridViewCommandColumn>

                                        <dxwgv:GridViewDataComboBoxColumn Caption="Item Avaliado" FieldName="ItemAvaliado" Width="15%">
                                            <PropertiesComboBox>
                                            </PropertiesComboBox>
                                        </dxwgv:GridViewDataComboBoxColumn>

                                        <dxwgv:GridViewDataComboBoxColumn Caption="Ativo" FieldName="Ativo" Width="15%" Settings-AllowAutoFilter="False">
                                            <PropertiesComboBox></PropertiesComboBox>
                                        </dxwgv:GridViewDataComboBoxColumn>

                                        <dxwgv:GridViewDataComboBoxColumn Caption="Agencia" FieldName="IdAgenciaClassificadora" Width="15%">
                                            <PropertiesComboBox DataSourceID="EsDSAgenciaClassificadora" TextField="DescricaoAgencia"
                                                ValueField="IdAgenciaClassificadora" ValueType="System.String" />
                                        </dxwgv:GridViewDataComboBoxColumn>
                                        
                                        <dxwgv:GridViewDataComboBoxColumn Caption="Código Nota" FieldName="IdPadraoNotas" Width="15%">
                                            <PropertiesComboBox DataSourceID="EsDSPadraoNota" TextField="CodigoNota" ValueField="IdPadraoNotas" />
                                        </dxwgv:GridViewDataComboBoxColumn>

                                    </Columns>
                                    <Templates>
                                        <EditForm>
                                            <asp:Label ID="labelEdicao" runat="server" CssClass="labelInformation" Text=""></asp:Label>
                                            <asp:Panel ID="panelEdicao" runat="server">
                                                <div class="editForm">
                                                    <table>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelItemAvaliado" runat="server" CssClass="labelRequired" Text="Item Avaliado:" />
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxComboBox ID="dropItemAvaliado" runat="server" ClientInstanceName="dropItemAvaliado"
                                                                    ShowShadow="false" DropDownStyle="DropDown" OnLoad="dropItemAvaliado_OnLoad"
                                                                    CssClass="dropDownListLongo" Text='<%#Eval("ItemAvaliado")%>'>
                                                                    <ClientSideEvents SelectedIndexChanged="function(s,e) 
                                                                    {
                                                                         dropAtivo.PerformCallback(s.GetValue());
                                                                    }" />
                                                                </dxe:ASPxComboBox> 
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelAtivo" runat="server" CssClass="labelRequired" Text="Ativo:" />
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxComboBox ID="dropAtivo" runat="server" ClientInstanceName="dropAtivo" ShowShadow="false"
                                                                    DropDownStyle="DropDown" CssClass="dropDownListLongo" OnCallback="dropAtivo_Callback"
                                                                    OnDataBound="dropAtivo_OnDataBound" Text='<%#Eval("Ativo")%>'>
                                                                </dxe:ASPxComboBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelAgenciaClassificadora" runat="server" CssClass="labelRequired" Text="Agência:" />
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxComboBox ID="dropIdAgenciaClassificadora" runat="server" ClientInstanceName="dropIdAgenciaClassificadora"
                                                                    ShowShadow="false" DropDownStyle="DropDown" CssClass="dropDownListLongo" DataSourceID="EsDSAgenciaClassificadora"
                                                                    TextField="DescricaoAgencia" ValueField="IdAgenciaClassificadora" Text='<%#Eval("IdAgenciaClassificadora")%>'>
                                                                    <ClientSideEvents SelectedIndexChanged="function(s,e) 
                                                                    {
                                                                        if(dropItemAvaliado.GetValue() == null)
                                                                        {
                                                                            dropIdAgenciaClassificadora.SetValue(null);
                                                                            alert('Item Avaliado deve ser preenchido');                              
                                                                        }
                                                                        else
                                                                        {
                                                                            dropIdPadraoNotas.PerformCallback(s.GetValue());
                                                                        }
                                                                    }" />
                                                                </dxe:ASPxComboBox>
                                                            </td>
                                                        </tr>                                                        
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelIdPadraoNotas" runat="server" CssClass="labelRequired" Text="Código Nota:" />
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxComboBox ID="dropIdPadraoNotas" runat="server" ClientInstanceName="dropIdPadraoNotas"
                                                                    OnCallback="dropIdPadraoNotas_Callback" ShowShadow="false" DropDownStyle="DropDown"
                                                                    OnDataBound="dropIdPadraoNotas_OnDataBound" CssClass="dropDownListLongo" Text='<%#Eval("IdPadraoNotas")%>'>
                                                                </dxe:ASPxComboBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div class="linhaH">
                                                    </div>
                                                    <div class="linkButton linkButtonNoBorder popupFooter">
                                                        <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd"
                                                            OnInit="btnOKAdd_Init" OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback();callbackAgenda.SendCallback(); return false;">
                                                            <asp:Literal ID="Literal5" runat="server" Text="OK+" /><div>
                                                            </div>
                                                        </asp:LinkButton><asp:LinkButton ID="btnOK" runat="server" Font-Overline="false"
                                                            CssClass="btnOK" OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback();return false; ">
                                                            <asp:Literal ID="Literal1" runat="server" Text="OK" /><div>
                                                            </div>
                                                        </asp:LinkButton><asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false"
                                                            CssClass="btnCancel" OnClientClick="gridCadastro.CancelEdit(); return false;">
                                                            <asp:Literal ID="Literal2" runat="server" Text="Cancelar" /><div>
                                                            </div>
                                                        </asp:LinkButton>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </EditForm>
                                    </Templates>
                                    <SettingsPopup EditForm-Width="20%" />
                                    <SettingsCommandButton>
                                        <ClearFilterButton>
                                            <Image Url="../../imagens/funnel--minus.png">
                                            </Image>
                                        </ClearFilterButton>
                                    </SettingsCommandButton>
                                </dxwgv:ASPxGridView>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro"
            Landscape="true">
        </dxwgv:ASPxGridViewExporter>
        <cc1:esDataSource ID="EsDSClassificacaoAtivo" runat="server" OnesSelect="EsDSClassificacaoAtivo_esSelect" />
        <cc1:esDataSource ID="EsDSAgenciaClassificadora" runat="server" OnesSelect="EsDSAgenciaClassificadora_esSelect" />
        <cc1:esDataSource ID="EsDSPerfilFaixaRating" runat="server" OnesSelect="EsDSPerfilFaixaRating_esSelect" />
        <cc1:esDataSource ID="EsDSPadraoNota" runat="server" OnesSelect="EsDSPadraoNota_esSelect" />
    </form>
</body>
</html>
