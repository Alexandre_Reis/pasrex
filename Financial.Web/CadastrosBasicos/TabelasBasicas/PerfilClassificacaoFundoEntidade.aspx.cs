﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using DevExpress.Utils;
using System.Drawing;
using Financial.Fundo;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_PerfilClassificacaoFundoEntidade : Financial.Web.Common.CadastroBasePage
{
    new protected void Page_Load(object sender, EventArgs e)
    {
        this.AllowUpdate = false;
        base.Page_Load(sender, e);

        if (IsPostBack)
            AtualizarGridPerfil();
    }

    protected void EsDSPerfilClassificacaoFundo_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        PerfilClassificacaoFundoCollection coll = new PerfilClassificacaoFundoCollection();

        coll.Query.OrderBy(coll.Query.Codigo.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSEntidadePerfilFundo_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        EntidadePerfilFundoCollection coll = new EntidadePerfilFundoCollection();

        coll.Query.OrderBy(coll.Query.Codigo.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSPerfilClassificacaoFundoEntidade_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        int idPerfil = 0;
        PerfilClassificacaoFundoEntidadeCollection coll = new PerfilClassificacaoFundoEntidadeCollection();

        if (ddlPerfil.SelectedIndex >= 0)
            idPerfil = Convert.ToInt32(ddlPerfil.SelectedItem.Value);

        PerfilClassificacaoFundoEntidadeQuery perfilClassificacaoFundoEntidade = new PerfilClassificacaoFundoEntidadeQuery("PCF");
        EntidadePerfilFundoQuery entidadePerfilFundo = new EntidadePerfilFundoQuery("EPF");
        EntidadeClassificacaoPerfilFundoQuery entidadeClassificacaoPerfilFundo = new EntidadeClassificacaoPerfilFundoQuery("ECP");

        perfilClassificacaoFundoEntidade.Select(perfilClassificacaoFundoEntidade.IdPerfilClassificacaoFundoEntidade, entidadePerfilFundo.Codigo.As("Entidade"), entidadeClassificacaoPerfilFundo.DescricaoTipoFundoCarteira.As("ClassificacaoTipoFundoCarteira"));

        perfilClassificacaoFundoEntidade.InnerJoin(entidadePerfilFundo).On(perfilClassificacaoFundoEntidade.IdEntidade == entidadePerfilFundo.IdEntidade);
        perfilClassificacaoFundoEntidade.InnerJoin(entidadeClassificacaoPerfilFundo).On(perfilClassificacaoFundoEntidade.IdEntidadeClassificacao == entidadeClassificacaoPerfilFundo.IdEntidadeClassificacao);

        perfilClassificacaoFundoEntidade.Where(perfilClassificacaoFundoEntidade.IdPerfilClassificacao == idPerfil);

        perfilClassificacaoFundoEntidade.OrderBy(entidadePerfilFundo.Codigo.Ascending, entidadeClassificacaoPerfilFundo.DescricaoTipoFundoCarteira.Ascending);
        coll.Load(perfilClassificacaoFundoEntidade);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdPerfilClassificacaoFundoEntidade");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idPerfil = Convert.ToInt32(keyValuesId[i]);

                PerfilClassificacaoFundoEntidade perfilClassificacaoFundoEntidade = new PerfilClassificacaoFundoEntidade();
                if (perfilClassificacaoFundoEntidade.LoadByPrimaryKey(idPerfil))
                {
                    PerfilClassificacaoFundoEntidade perfilClassificacaoFundoEntidadeClone = (PerfilClassificacaoFundoEntidade)Utilitario.Clone(perfilClassificacaoFundoEntidade);

                    perfilClassificacaoFundoEntidadeClone.MarkAsDeleted();
                    perfilClassificacaoFundoEntidadeClone.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de PerfilClassificacaoFundoEntidade - Operacao: Delete PerfilClassificacaoFundoEntidade: " + idPerfil + UtilitarioWeb.ToString(perfilClassificacaoFundoEntidade),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    protected void ddlEntidade_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlPerfil.SelectedIndex >= 0 && ddlEntidade.SelectedIndex >= 0)
        {
            int idEntidade = Convert.ToInt32(ddlEntidade.SelectedItem.Value);
            EntidadeClassificacaoPerfilFundoCollection listaClassificacoes = new EntidadeClassificacaoPerfilFundoCollection();

            listaClassificacoes.Query.Select(listaClassificacoes.Query.DescricaoTipoFundoCarteira,
                                             listaClassificacoes.Query.IdEntidadeClassificacao);

            listaClassificacoes.Query.Where(listaClassificacoes.Query.IdEntidadePerfilFundo == idEntidade, listaClassificacoes.Query.InicioVigencia >= Convert.ToDateTime(txtInicioVigencia.Text));

            listaClassificacoes.Query.OrderBy(listaClassificacoes.Query.DescricaoTipoFundoCarteira.Ascending);

            listaClassificacoes.Query.Load();

            ddlClassificacao.TextField = "DescricaoTipoFundoCarteira";
            ddlClassificacao.ValueField = "IdEntidadeClassificacao";
            ddlClassificacao.DataSource = listaClassificacoes;
            ddlClassificacao.DataBind();

            ddlClassificacao.SelectedIndex = -1;
        }
        else
        {
            ddlClassificacao.Items.Clear();
            ddlClassificacao.SelectedIndex = -1;
        }
    }

    protected void btnIncluir_Click(object sender, EventArgs e)
    {
        if (ddlPerfil.SelectedIndex < 0 || ddlEntidade.SelectedIndex < 0 || ddlClassificacao.SelectedIndex < 0)
            throw new Exception("Campos com * são obrigatórios!");

        PerfilClassificacaoFundoEntidade perfilClassificacaoFundoEntidade = new PerfilClassificacaoFundoEntidade();
        perfilClassificacaoFundoEntidade.IdEntidade = Convert.ToInt32(ddlEntidade.SelectedItem.Value);
        perfilClassificacaoFundoEntidade.IdEntidadeClassificacao = Convert.ToInt32(ddlClassificacao.SelectedItem.Value);
        perfilClassificacaoFundoEntidade.IdPerfilClassificacao = Convert.ToInt32(ddlPerfil.SelectedItem.Value);

        perfilClassificacaoFundoEntidade.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de PerfilClassificacaoFundoEntidade - Operacao: Insert PerfilClassificacaoFundoEntidade: " + ddlPerfil.SelectedItem.Value + UtilitarioWeb.ToString(perfilClassificacaoFundoEntidade),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        AtualizarGridPerfil();

        ddlEntidade.SelectedIndex = -1;
        ddlClassificacao.Items.Clear();
        ddlClassificacao.SelectedIndex = -1;
    }

    protected void ddlPerfil_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlPerfil.SelectedIndex >= 0)
        {
            int idPerfil = Convert.ToInt32(ddlPerfil.SelectedItem.Value);

            PerfilClassificacaoFundo perfilClassificacaoFundo = new PerfilClassificacaoFundo();
            perfilClassificacaoFundo.LoadByPrimaryKey(idPerfil);

            txtDescricaoPerfil.Text = perfilClassificacaoFundo.Descricao;
            txtInicioVigencia.Date = perfilClassificacaoFundo.InicioVigencia.Value;

            ddlEntidade.SelectedIndex = -1;
            ddlClassificacao.Items.Clear();
            ddlClassificacao.SelectedIndex = -1;
        }
        else
        {
            txtDescricaoPerfil.Text = string.Empty;
            txtInicioVigencia.Text = string.Empty;

            ddlEntidade.SelectedIndex = -1;
            ddlClassificacao.Items.Clear();
            ddlClassificacao.SelectedIndex = -1;
        }

        AtualizarGridPerfil();
    }

    private void AtualizarGridPerfil()
    {
        gridCadastro.DataBind();
    }
}