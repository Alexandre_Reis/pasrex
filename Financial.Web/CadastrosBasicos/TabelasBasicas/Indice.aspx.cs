﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Fundo;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Common.Enums;
using System.Reflection;
using Financial.Web.Common;
using Financial.RendaFixa;

public partial class CadastrosBasicos_Indice : Financial.Web.Common.CadastroBasePage {


    #region Popup Feeder
    protected void callBackPopupFeeder_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = string.Empty;
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idFeeder = Convert.ToInt32(e.Parameter);

            FeederQuery feederQuery = new FeederQuery();
            Feeder feeder = new Feeder();

            feederQuery.Select(feederQuery.IdFeeder, feederQuery.Descricao);
            feederQuery.Where(feederQuery.IdFeeder == idFeeder);

            if (feeder.Load(feederQuery))
                e.Result = feeder.IdFeeder.ToString() + " - " + feeder.Descricao.ToString();
            //                            
        }
    }

    protected void EsDSFeeder_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        FeederCollection feederColl = new FeederCollection();

        feederColl.Query.Select(feederColl.Query.IdFeeder,
                                feederColl.Query.Descricao);
        feederColl.Query.OrderBy(feederColl.Query.IdFeeder.Ascending);
        feederColl.LoadAll();
        e.Collection = feederColl;
    }

    protected void gridFeeder_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        UtilitarioGrid.SetaCoresGrid(e);
    }

    protected void gridFeeder_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        this.gridFeeder.DataBind();
    }

    protected void gridFeeder_CustomDataCallback(object sender, ASPxGridViewCustomDataCallbackEventArgs e)
    {
        ASPxGridView gridView = sender as ASPxGridView;
        int visibleIndex = Convert.ToInt32(e.Parameters);
        int idFeeder = (int)gridView.GetRowValues(visibleIndex, FeederMetadata.ColumnNames.IdFeeder);
        e.Result = idFeeder.ToString();
    }
    #endregion

    #region Curva
    protected void EsDSCurva_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CurvaRendaFixaCollection coll = new CurvaRendaFixaCollection();
        coll.LoadAll();
        e.Collection = coll;
    }

    protected void callBackPopupCurva_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = string.Empty;
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCurvaRendaFixa = Convert.ToInt32(e.Parameter);

            CurvaRendaFixaQuery curvaRendaFixaQuery = new CurvaRendaFixaQuery();
            CurvaRendaFixa curvaRendaFixa = new CurvaRendaFixa();

            curvaRendaFixaQuery.Select(curvaRendaFixaQuery.IdCurvaRendaFixa, curvaRendaFixaQuery.Descricao);
            curvaRendaFixaQuery.Where(curvaRendaFixaQuery.IdCurvaRendaFixa == idCurvaRendaFixa);

            if (curvaRendaFixa.Load(curvaRendaFixaQuery))
                e.Result = curvaRendaFixa.IdCurvaRendaFixa.ToString() + " - " + curvaRendaFixa.Descricao.ToString();
            //                            
        }
    }

    protected void gridCurva_CustomDataCallback(object sender, ASPxGridViewCustomDataCallbackEventArgs e)
    {
        ASPxGridView gridView = sender as ASPxGridView;
        int visibleIndex = Convert.ToInt32(e.Parameters);
        int idCurvaRendaFixa = (int)gridView.GetRowValues(visibleIndex, CurvaRendaFixaMetadata.ColumnNames.IdCurvaRendaFixa);
        e.Result = idCurvaRendaFixa.ToString();
    }

    protected void gridCurva_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        UtilitarioGrid.SetaCoresGrid(e);
    }

    protected void gridCurva_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        this.gridCurva.DataBind();
    }
    #endregion

    #region PopUp Indice
    protected void gridIndice_CustomDataCallback(object sender, ASPxGridViewCustomDataCallbackEventArgs e)
    {
        ASPxGridView gridView = sender as ASPxGridView;
        int visibleIndex = Convert.ToInt32(e.Parameters);
        int idIndice = Convert.ToInt32(gridView.GetRowValues(visibleIndex, IndiceMetadata.ColumnNames.IdIndice));
        e.Result = idIndice.ToString();
    }

    protected void gridIndice_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        UtilitarioGrid.SetaCoresGrid(e);
    }

    protected void gridIndice_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        this.gridIndice.DataBind();
    }

    protected void btnEditIndice_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
        {
            (sender as ASPxButtonEdit).Enabled = false;
            (sender as ASPxButtonEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    protected void callBackPopupIndice_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = string.Empty;
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            short idIndice = Convert.ToInt16(e.Parameter);

            Indice indice = new Indice();
            if (indice.LoadByPrimaryKey(idIndice))
            {
                e.Result = indice.IdIndice + " - " + indice.Descricao + " - " + indice.UpToFeederByIdFeeder.Descricao;
            }
        }
    }
    #endregion

    protected void EsDSIndice_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        IndiceCollection coll = new IndiceCollection();
        FeederQuery feederQuery = new FeederQuery("feeder");
        CurvaRendaFixaQuery curvaQuery = new CurvaRendaFixaQuery("curva");
        IndiceQuery indiceQuery = new IndiceQuery("indice");

        indiceQuery.Select(indiceQuery, feederQuery.Descricao.As("DescricaoFeeder"), curvaQuery.Descricao.As("DescricaoCurva"));
        indiceQuery.InnerJoin(feederQuery).On(indiceQuery.IdFeeder.Equal(feederQuery.IdFeeder));
        indiceQuery.LeftJoin(curvaQuery).On(indiceQuery.IdCurva.Equal(curvaQuery.IdCurvaRendaFixa));
        indiceQuery.OrderBy(coll.Query.Descricao.Ascending);

        coll.Load(indiceQuery);        

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
        ASPxComboBox dropTipo = gridCadastro.FindEditFormTemplateControl("dropTipo") as ASPxComboBox;
        ASPxComboBox dropTipoDivulgacao = gridCadastro.FindEditFormTemplateControl("dropTipoDivulgacao") as ASPxComboBox;
        ASPxTextBox hiddenIdFeeder = gridCadastro.FindEditFormTemplateControl("hiddenIdFeeder") as ASPxTextBox;
        ASPxTextBox textIdIndice = gridCadastro.FindEditFormTemplateControl("textIdIndice") as ASPxTextBox;
        ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(dropTipo);
        controles.Add(dropTipoDivulgacao);
        controles.Add(hiddenIdFeeder);
        controles.Add(textIdIndice);
        controles.Add(textDescricao);

        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        if (gridCadastro.IsNewRowEditing)
        {
            Indice indice = new Indice();
            if (indice.LoadByPrimaryKey(Convert.ToInt16(textIdIndice.Text)))
            {
                e.Result = "Id do índice já existente!";
                return;
            }
        }
    }

    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        //SalvarNovo();

        gridCadastro.DataBind();

        e.Result = "";
        if (e.Parameter == "clone")
        {
            //e.Result = 0;
        }
        else
        {
            e.Result = "Cadastro feito com sucesso.";
        }
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        Indice indice = new Indice();

        short idIndice = (short)e.Keys[0];

        if (indice.LoadByPrimaryKey((short)idIndice))
        {
            ASPxSpinEdit textPercentual = gridCadastro.FindEditFormTemplateControl("textPercentual") as ASPxSpinEdit;
            ASPxSpinEdit textTaxa = gridCadastro.FindEditFormTemplateControl("textTaxa") as ASPxSpinEdit;
            ASPxComboBox dropTipo = gridCadastro.FindEditFormTemplateControl("dropTipo") as ASPxComboBox;
            ASPxComboBox dropTipoDivulgacao = gridCadastro.FindEditFormTemplateControl("dropTipoDivulgacao") as ASPxComboBox;
            ASPxComboBox dropIndiceBase = gridCadastro.FindEditFormTemplateControl("dropIndiceBase") as ASPxComboBox;
            ASPxTextBox hiddenIdCurva = gridCadastro.FindEditFormTemplateControl("hiddenIdCurva") as ASPxTextBox;
            ASPxTextBox hiddenIdFeeder = gridCadastro.FindEditFormTemplateControl("hiddenIdFeeder") as ASPxTextBox;
            ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
            ASPxTextBox textIdIndice = gridCadastro.FindEditFormTemplateControl("textIdIndice") as ASPxTextBox;
            ASPxSpinEdit textCodigoBDS = gridCadastro.FindEditFormTemplateControl("textCodigoBDS") as ASPxSpinEdit;
            ASPxTextBox textTabelaBDS = gridCadastro.FindEditFormTemplateControl("textTabelaBDS") as ASPxTextBox;
            ASPxSpinEdit textIntervaloBDS = gridCadastro.FindEditFormTemplateControl("textIntervaloBDS") as ASPxSpinEdit;
            ASPxTextBox textAtributoBDS = gridCadastro.FindEditFormTemplateControl("textAtributoBDS") as ASPxTextBox;

            indice.Descricao = textDescricao.Text;
            indice.Tipo = Convert.ToByte(dropTipo.SelectedItem.Value);
            indice.TipoDivulgacao = Convert.ToByte(dropTipoDivulgacao.SelectedItem.Value);
            indice.IdFeeder = Convert.ToInt32(hiddenIdFeeder.Text);

            if (!string.IsNullOrEmpty(textCodigoBDS.Text))
                indice.CodigoBDS = Convert.ToInt32(textCodigoBDS.Text);
            else
                indice.CodigoBDS = null;

            if (!string.IsNullOrEmpty(hiddenIdCurva.Text))
                indice.IdCurva = Convert.ToInt32(hiddenIdCurva.Text);
            else
                indice.IdCurva = null;

            if (!string.IsNullOrEmpty(textTaxa.Text))
                indice.Taxa = Convert.ToDecimal(textTaxa.Text);
            else
                indice.Taxa = null;

            if (dropIndiceBase.SelectedIndex > -1)
                indice.IdIndiceBase = Convert.ToInt32(dropIndiceBase.SelectedItem.Value);
            else
                indice.IdIndiceBase = null;

            if (!string.IsNullOrEmpty(textPercentual.Text))
                indice.Percentual = Convert.ToDecimal(textPercentual.Text);
            else
                indice.Percentual = null;

            if (!string.IsNullOrEmpty(textAtributoBDS.Text))
                indice.AtributoBDS = textAtributoBDS.Text;
            else
                indice.AtributoBDS = null;

            if (!string.IsNullOrEmpty(textIntervaloBDS.Text))
                indice.IntervaloBDS = Convert.ToByte(textIntervaloBDS.Text);
            else
                indice.IntervaloBDS = null;

            if (!string.IsNullOrEmpty(textTabelaBDS.Text))
                indice.TabelaBDS = textTabelaBDS.Text;
            else
                indice.TabelaBDS = null;

            indice.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de Indice - Operacao: Update Indice: " + idIndice + UtilitarioWeb.ToString(indice),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {        
        ASPxSpinEdit textPercentual = gridCadastro.FindEditFormTemplateControl("textPercentual") as ASPxSpinEdit;
        ASPxSpinEdit textTaxa = gridCadastro.FindEditFormTemplateControl("textTaxa") as ASPxSpinEdit;
        ASPxComboBox dropTipo = gridCadastro.FindEditFormTemplateControl("dropTipo") as ASPxComboBox;
        ASPxComboBox dropTipoDivulgacao = gridCadastro.FindEditFormTemplateControl("dropTipoDivulgacao") as ASPxComboBox;
        ASPxComboBox dropIndiceBase = gridCadastro.FindEditFormTemplateControl("dropIndiceBase") as ASPxComboBox;
        ASPxTextBox hiddenIdCurva = gridCadastro.FindEditFormTemplateControl("hiddenIdCurva") as ASPxTextBox;
        ASPxTextBox hiddenIdFeeder = gridCadastro.FindEditFormTemplateControl("hiddenIdFeeder") as ASPxTextBox;
        ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
        ASPxTextBox textIdIndice = gridCadastro.FindEditFormTemplateControl("textIdIndice") as ASPxTextBox;
        ASPxSpinEdit textCodigoBDS = gridCadastro.FindEditFormTemplateControl("textCodigoBDS") as ASPxSpinEdit;
        ASPxTextBox textTabelaBDS = gridCadastro.FindEditFormTemplateControl("textTabelaBDS") as ASPxTextBox;
        ASPxSpinEdit textIntervaloBDS = gridCadastro.FindEditFormTemplateControl("textIntervaloBDS") as ASPxSpinEdit;
        ASPxTextBox textAtributoBDS = gridCadastro.FindEditFormTemplateControl("textAtributoBDS") as ASPxTextBox;
         
                 
        Indice indice = new Indice();
        indice.Descricao = textDescricao.Text;
        indice.IdIndice = Convert.ToInt16(textIdIndice.Text);
        indice.Tipo = Convert.ToByte(dropTipo.SelectedItem.Value);
        indice.TipoDivulgacao = Convert.ToByte(dropTipoDivulgacao.SelectedItem.Value);
        indice.IdFeeder = Convert.ToInt32(hiddenIdFeeder.Text);

        if (!string.IsNullOrEmpty(textCodigoBDS.Text))
            indice.CodigoBDS = Convert.ToInt32(textCodigoBDS.Text);
        else
            indice.CodigoBDS = null;

        if (!string.IsNullOrEmpty(hiddenIdCurva.Text))
            indice.IdCurva = Convert.ToInt32(hiddenIdCurva.Text);
        else
            indice.IdCurva = null;

        if (!string.IsNullOrEmpty(textTaxa.Text))
            indice.Taxa = Convert.ToDecimal(textTaxa.Text);
        else
            indice.Taxa = null;

        if (dropIndiceBase.SelectedIndex > -1)
            indice.IdIndiceBase = Convert.ToInt32(dropIndiceBase.SelectedItem.Value);
        else
            indice.IdIndiceBase = null;

        if (!string.IsNullOrEmpty(textPercentual.Text))
            indice.Percentual = Convert.ToDecimal(textPercentual.Text);
        else
            indice.Percentual = null;

        if (!string.IsNullOrEmpty(textAtributoBDS.Text))
            indice.AtributoBDS = textAtributoBDS.Text;
        else
            indice.AtributoBDS = null;

        if (!string.IsNullOrEmpty(textIntervaloBDS.Text))
            indice.IntervaloBDS = Convert.ToByte(textIntervaloBDS.Text);
        else
            indice.IntervaloBDS = null;

        if (!string.IsNullOrEmpty(textTabelaBDS.Text))
            indice.TabelaBDS = textTabelaBDS.Text;
        else
            indice.TabelaBDS = null;

        indice.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de Indice - Operacao: Insert Indice: " + indice.IdIndice + UtilitarioWeb.ToString(indice),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdIndice");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idIndice = Convert.ToInt32(keyValuesId[i]);

                Indice indice = new Indice();
                if (indice.LoadByPrimaryKey((short)idIndice))
                {
                    foreach (FieldInfo fieldInfo in typeof(ListaIndiceFixo).GetFields())
                    {
                        int idEnum = Convert.ToInt32(fieldInfo.GetValue(fieldInfo));

                        if (idIndice == idEnum)
                        {
                            throw new Exception("Índice " + indice.Descricao + " não pode ser excluído por ser primário do sistema.");
                        }
                    }

                    bool achou = false;
                    CarteiraCollection carteiraCollection = new CarteiraCollection();
                    carteiraCollection.Query.Where(carteiraCollection.Query.IdIndiceBenchmark.Equal((short)idIndice));
                    if (carteiraCollection.Query.Load())
                    {
                        achou = true;
                    }
                    else
                    {
                        TabelaTaxaPerformanceCollection tabelaTaxaPerformanceCollection = new TabelaTaxaPerformanceCollection();
                        tabelaTaxaPerformanceCollection.Query.Where(tabelaTaxaPerformanceCollection.Query.IdIndice.Equal((short)idIndice));
                        if (tabelaTaxaPerformanceCollection.Query.Load())
                        {
                            achou = true;
                        }
                    }

                    if (achou)
                    {
                        throw new Exception("Índice " + indice.Descricao + " não pode ser excluído por ter cadastros (carteira e/ou Tabela de Pfee) relacionados.");
                    }

                    Indice indiceClone = (Indice)Utilitario.Clone(indice);
                    //

                    indice.MarkAsDeleted();
                    indice.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de Indice - Operacao: Delete Indice: " + idIndice + UtilitarioWeb.ToString(indiceClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    new protected void gridCadastro_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        (e.Editor as ASPxTextEdit).ValidationSettings.RequiredField.IsRequired = true;
        (e.Editor as ASPxTextEdit).ValidationSettings.RequiredField.ErrorText = this.MsgCampoObrigatorio;

        if (!gridCadastro.IsNewRowEditing)
        {
            if (e.Column.FieldName == "IdIndice")
            {
                e.Editor.Enabled = false;
                e.Editor.BackColor = Color.FromName("#EBEBEB");
                e.Editor.ReadOnly = true;
            }
        }
    }

    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    protected void textIdIndice_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
        {
            (sender as ASPxTextBox).Enabled = false;
            (sender as ASPxTextBox).BackColor = Color.FromName("#EBEBEB");
        }
    }
}