﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using Financial.Common;
using Financial.Util;
using Financial.Investidor;
using Financial.ContaCorrente;
using Financial.Fundo;
using Financial.InvestidorCotista;
using Financial.Bolsa;
using Financial.BMF;

using Financial.Investidor.Enums;

using DevExpress.Web;
using Financial.Web.Common;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.CRM;
using System.Text.RegularExpressions;
using Financial.CRM.Enums;

public partial class CadastrosBasicos_AgenteMercado : CadastroBasePage
{
    bool EstendeConceitoPessoaAgente = ParametrosConfiguracaoSistema.Outras.EstendeConceitoPessoaAgente.Equals("S");

    new protected void Page_Load(object sender, EventArgs e) {

        this.HasPopupPessoa = true;
        this.HasPanelFieldsLoading = true;

        base.Page_Load(sender, e);

        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
                                  new List<string>(new string[] { AgenteMercadoMetadata.ColumnNames.FuncaoCorretora,
                                                                  AgenteMercadoMetadata.ColumnNames.FuncaoAdministrador,
                                                                  AgenteMercadoMetadata.ColumnNames.FuncaoGestor,
                                                                  AgenteMercadoMetadata.ColumnNames.FuncaoLiquidante,
                                                                  AgenteMercadoMetadata.ColumnNames.FuncaoCustodiante,
                                                                  AgenteMercadoMetadata.ColumnNames.FuncaoDistribuidor
                                                                }));
    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSAgenteMercado_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) 
    {
        AgenteMercadoCollection coll = new AgenteMercadoCollection();
        AgenteMercadoQuery agenteMercadoQuery = new AgenteMercadoQuery("agente");
        PessoaQuery pessoaQuery = new PessoaQuery("pessoa");

        agenteMercadoQuery.Select(agenteMercadoQuery, pessoaQuery.IdPessoa, pessoaQuery.Apelido.As("ApelidoPessoa"));
        agenteMercadoQuery.LeftJoin(pessoaQuery).On(agenteMercadoQuery.IdPessoa.Equal(pessoaQuery.IdPessoa));               
        agenteMercadoQuery.OrderBy(agenteMercadoQuery.Nome.Ascending);

        coll.Load(agenteMercadoQuery);

        for (int i = 0; i < coll.Count; i++) {
            coll[i].Cnpj = Utilitario.MascaraCNPJ(coll[i].Cnpj);
        }
        
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;
        ASPxTextBox textNome = pageControl.FindControl("textNome") as ASPxTextBox;
        ASPxComboBox dropFuncaoCorretora = pageControl.FindControl("dropFuncaoCorretora") as ASPxComboBox;
        ASPxComboBox dropFuncaoAdministrador = pageControl.FindControl("dropFuncaoAdministrador") as ASPxComboBox;
        ASPxComboBox dropFuncaoGestor = pageControl.FindControl("dropFuncaoGestor") as ASPxComboBox;
        ASPxComboBox dropFuncaoLiquidante = pageControl.FindControl("dropFuncaoLiquidante") as ASPxComboBox;
        ASPxComboBox dropFuncaoDistribuidor = pageControl.FindControl("dropFuncaoDistribuidor") as ASPxComboBox;
        ASPxComboBox dropFuncaoCustodiante = pageControl.FindControl("dropFuncaoCustodiante") as ASPxComboBox;
        ASPxComboBox dropCorretagemAdicional = pageControl.FindControl("dropCorretagemAdicional") as ASPxComboBox;
        ASPxTextBox textCodigoBovespa = pageControl.FindControl("textCodigoBovespa") as ASPxTextBox;
        ASPxTextBox textCodigoBMF = pageControl.FindControl("textCodigoBMF") as ASPxTextBox;
        ASPxTextBox textCNPJ = pageControl.FindControl("textCNPJ") as ASPxTextBox;
        ASPxTextBox textCPFResponsavel = pageControl.FindControl("textCPFResponsavel") as ASPxTextBox;
        ASPxTextBox textCPFResponsavel2 = pageControl.FindControl("textCPFResponsavel2") as ASPxTextBox;
        ASPxComboBox dropEmpSecuritizada = pageControl.FindControl("dropEmpSecuritizada") as ASPxComboBox;
        ASPxSpinEdit btnEditCodigoPessoa = pageControl.FindControl("btnEditCodigoPessoa") as ASPxSpinEdit;
        
        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(textNome);
        controles.Add(dropFuncaoCorretora);
        controles.Add(dropFuncaoAdministrador);
        controles.Add(dropFuncaoGestor);
        controles.Add(dropFuncaoLiquidante);
        controles.Add(dropFuncaoDistribuidor);
        controles.Add(dropFuncaoCustodiante);
        controles.Add(dropEmpSecuritizada);

        if(EstendeConceitoPessoaAgente)
            controles.Add(btnEditCodigoPessoa);

        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        if (dropFuncaoCorretora.SelectedItem.Value.ToString() == "S")
        {
            if (dropCorretagemAdicional.SelectedIndex == -1)
            {
                e.Result = "Para agentes do tipo Corretora o tipo de corretagem adicional precisa ser informada.";
                return;
            }
        }
                
        if (textCodigoBovespa.Text.Trim() != "" && !Utilitario.IsInteger(textCodigoBovespa.Text.Replace("-", "")))
        {
            e.Result = "Código Bovespa deve ter apenas números e/ou um separador - com o DV.";
            return;
        }

        if (textCodigoBMF.Text.Trim() != "" && !Utilitario.IsInteger(textCodigoBMF.Text.Replace("-", "")))
        {
            e.Result = "Código BMF deve ter apenas números e/ou um separador - com o DV.";
            return;
        }

        if (textCodigoBovespa.Text.Contains("-"))
        {
            string[] lista = textCodigoBovespa.Text.Split(new Char[] { '-' });

            if (lista[1] == "")
            {
                e.Result = "DV não informado para Código Bovespa.";
                return;
            }
            int digitoCodigoBovespa = Convert.ToInt32(lista[1]);

            if (digitoCodigoBovespa >= 10)
            {
                e.Result = "DV do Código Bovespa deve conter apenas 1 número.";
                return;
            }
        }

        if (textCodigoBMF.Text.Contains("-"))
        {
            string[] lista = textCodigoBMF.Text.Split(new Char[] { '-' });

            if (lista[1] == "")
            {
                e.Result = "DV não informado para Código BMF.";
                return;
            }
            int digitoCodigoBMF = Convert.ToInt32(lista[1]);

            if (digitoCodigoBMF >= 10)
            {
                e.Result = "DV do Código BMF deve conter apenas 1 número.";
                return;
            }
        }


        if (!string.IsNullOrEmpty(btnEditCodigoPessoa.Text))
        {
            AgenteMercadoCollection agenteMercadoColl = new AgenteMercadoCollection();
            agenteMercadoColl.Query.Where(agenteMercadoColl.Query.IdPessoa.Equal(Convert.ToInt32(btnEditCodigoPessoa.Text)));

            if (!this.gridCadastro.IsNewRowEditing)
            {
                int idAgenteMercado = Convert.ToInt32(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, AgenteMercadoMetadata.ColumnNames.IdAgente));
                agenteMercadoColl.Query.Where(agenteMercadoColl.Query.IdAgente.NotEqual(idAgenteMercado));
            }

            if (agenteMercadoColl.Query.Load())
            {
                e.Result = "Já existe Agente de Mercado cadastrado com a Pessoa informada!!";
                return;
            }

        }


        string documento = textCNPJ.Text.Replace(".", "").Replace("/", "").Replace("-", "");
        if (!string.IsNullOrEmpty(documento))
        {
            AgenteMercadoCollection agenteMercadoColl = new AgenteMercadoCollection();
            agenteMercadoColl.Query.Where(agenteMercadoColl.Query.Cnpj.Equal(documento));

            if (!this.gridCadastro.IsNewRowEditing)
            {
                int idAgenteMercado = Convert.ToInt32(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, AgenteMercadoMetadata.ColumnNames.IdAgente));
                agenteMercadoColl.Query.Where(agenteMercadoColl.Query.IdAgente.NotEqual(idAgenteMercado));
            }

            if (agenteMercadoColl.Query.Load())
            {
                e.Result = "Já existe Agente de Mercado cadastrado com o CNPJ informado!!";
                return;
            }

            if (!string.IsNullOrEmpty(textCNPJ.Text))
            {
                if (!ValidaCNPJ(textCNPJ.Text))
                {
                    e.Result = "CNPJ inválido!";
                    return;
                }
            }
        }
       
        if (!this.gridCadastro.IsNewRowEditing)
        {
            int idAgenteMercado = Convert.ToInt32(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, AgenteMercadoMetadata.ColumnNames.IdAgente));

            CarteiraCollection carteiraCollection = new CarteiraCollection();
            PessoaCollection pessoaCollection = new PessoaCollection();
            PerfilCorretagemBolsaCollection perfilCorretagemBolsaCollection = new PerfilCorretagemBolsaCollection();
            PerfilCorretagemBMFCollection perfilCorretagemBMFCollection = new PerfilCorretagemBMFCollection();
            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
            PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();
            PosicaoBMFCollection posicaoBMFCollection = new PosicaoBMFCollection();

            OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
            OperacaoBMFCollection operacaoBMFCollection = new OperacaoBMFCollection();

            #region Verifica Documento
            if (!string.IsNullOrEmpty(textCPFResponsavel.Text))
            {
                if (!ValidaCPF(textCPFResponsavel.Text))
                {
                    e.Result = "CPF Resp.: CPF inválido!";
                    return;
                }
            }
            if (!string.IsNullOrEmpty(textCPFResponsavel2.Text))
            {
                if (!ValidaCPF(textCPFResponsavel2.Text))
                {
                    e.Result = "CPF Resp2.: CPF inválido!";
                    return;
                }
            }
            #endregion

            # region Carteira
            if (dropFuncaoGestor.SelectedItem.Value.ToString() == "N")
            {
                carteiraCollection.Query.Select(carteiraCollection.Query.IdCarteira)
                                        .Where(carteiraCollection.Query.IdAgenteGestor.Equal(idAgenteMercado));
                if (carteiraCollection.Query.Load())
                {
                    e.Result = "Função Gestor não pode ser desmarcada, pois existem vínculos com a carteira.";
                    return;
                }
            }

            if (dropFuncaoAdministrador.SelectedItem.Value.ToString() == "N")
            {
                carteiraCollection = new CarteiraCollection();
                carteiraCollection.Query.Select(carteiraCollection.Query.IdCarteira)
                                        .Where(carteiraCollection.Query.IdAgenteAdministrador.Equal(idAgenteMercado));
                if (carteiraCollection.Query.Load())
                {
                    e.Result = "Função Administrador não pode ser desmarcada, pois existem vínculos com a carteira.";
                    return;
                }
            }
            #endregion

            #region Cotista
            if (dropFuncaoDistribuidor.SelectedItem.Value.ToString() == "N")
            {
                pessoaCollection.Query.Select(pessoaCollection.Query.IdPessoa)
                                        .Where(pessoaCollection.Query.IdAgenteDistribuidor.Equal(idAgenteMercado));
                pessoaCollection.Query.Load();

                if (pessoaCollection.Count > 0)
                {
                    e.Result = "Função Distribuidor não pode ser desmarcada, pois existem vínculos com o cadastro de pessoa.";
                    return;
                }
            }
            //

            if (dropFuncaoCorretora.SelectedItem.Value.ToString() == "N")
            {
                //PerfilCorretagemBolsa, PerfilCorretagemBMF
                perfilCorretagemBolsaCollection.Query.Select(perfilCorretagemBolsaCollection.Query.IdTemplate)
                                        .Where(perfilCorretagemBolsaCollection.Query.IdAgente.Equal(idAgenteMercado));
                perfilCorretagemBolsaCollection.Query.Load();

                if (perfilCorretagemBolsaCollection.Count > 0)
                {
                    e.Result = "Função Corretora não pode ser desmarcada, pois existem vínculos com o perfil de corretagem Bolsa.";
                    return;
                }

                perfilCorretagemBMFCollection.Query.Select(perfilCorretagemBMFCollection.Query.IdPerfilCorretagem)
                                        .Where(perfilCorretagemBMFCollection.Query.IdAgente.Equal(idAgenteMercado));
                perfilCorretagemBMFCollection.Query.Load();

                if (perfilCorretagemBMFCollection.Count > 0)
                {
                    e.Result = "Função Corretora não pode ser desmarcada, pois existem vínculos com o perfil de corretagem BMF.";
                    return;
                }
                //

                //OperacaoBolsa, OperacaoBMF
                operacaoBolsaCollection.Query.Select(operacaoBolsaCollection.Query.IdOperacao)
                                        .Where(operacaoBolsaCollection.Query.IdAgenteCorretora.Equal(idAgenteMercado));
                operacaoBolsaCollection.Query.Load();

                if (operacaoBolsaCollection.Count > 0)
                {
                    e.Result = "Função Corretora não pode ser desmarcada, pois existem vínculos com operações de bolsa.";
                    return;
                }

                operacaoBMFCollection.Query.Select(operacaoBMFCollection.Query.IdOperacao)
                                        .Where(operacaoBMFCollection.Query.IdAgenteCorretora.Equal(idAgenteMercado));
                operacaoBMFCollection.Query.Load();

                if (operacaoBMFCollection.Count > 0)
                {
                    e.Result = "Função Corretora não pode ser desmarcada, pois existem vínculos com operações de BMF.";
                    return;
                }
                //
            }
            #endregion

            #region Liquidacao
            if (dropFuncaoLiquidante.SelectedItem.Value.ToString() == "N")
            {
                liquidacaoCollection.Query.Select(liquidacaoCollection.Query.IdLiquidacao)
                                        .Where(liquidacaoCollection.Query.IdAgente.Equal(idAgenteMercado));
                if (liquidacaoCollection.Query.Load())
                {
                    e.Result = "Função Liquidante não pode ser desmarcada, pois existem vínculos com lançamentos financeiros.";
                    return;
                }
            }
            //

            //PosicaoBolsa, PosicaoBMF
            if (dropFuncaoCustodiante.SelectedItem.Value.ToString() == "N")
            {
                posicaoBolsaCollection.Query.Select(posicaoBolsaCollection.Query.IdPosicao)
                                        .Where(posicaoBolsaCollection.Query.IdAgente.Equal(idAgenteMercado));
                if (posicaoBolsaCollection.Query.Load())
                {
                    e.Result = "Função Custodiante não pode ser desmarcada, pois existem vínculos com posições de bolsa.";
                    return;
                }

                posicaoBMFCollection.Query.Select(posicaoBMFCollection.Query.IdPosicao)
                                        .Where(posicaoBMFCollection.Query.IdAgente.Equal(idAgenteMercado));
                if (posicaoBMFCollection.Query.Load())
                {
                    e.Result = "Função Custodiante não pode ser desmarcada, pois existem vínculos com posições de BMF.";
                    return;
                }
            }
            #endregion
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        int idAgenteMercado = (int)e.Keys[0];

        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;

        ASPxTextBox textNome = pageControl.FindControl("textNome") as ASPxTextBox;
        ASPxTextBox textApelidoAgente = pageControl.FindControl("textApelidoAgente") as ASPxTextBox;
        ASPxTextBox textCNPJ = pageControl.FindControl("textCNPJ") as ASPxTextBox;
        ASPxTextBox textCodigoBovespa = pageControl.FindControl("textCodigoBovespa") as ASPxTextBox;
        ASPxTextBox textCodigoBMF = pageControl.FindControl("textCodigoBMF") as ASPxTextBox;
        ASPxComboBox dropFuncaoCorretora = pageControl.FindControl("dropFuncaoCorretora") as ASPxComboBox;
        ASPxComboBox dropFuncaoAdministrador = pageControl.FindControl("dropFuncaoAdministrador") as ASPxComboBox;
        ASPxComboBox dropFuncaoGestor = pageControl.FindControl("dropFuncaoGestor") as ASPxComboBox;
        ASPxComboBox dropFuncaoLiquidante = pageControl.FindControl("dropFuncaoLiquidante") as ASPxComboBox;
        ASPxComboBox dropFuncaoCustodiante = pageControl.FindControl("dropFuncaoCustodiante") as ASPxComboBox;
        ASPxComboBox dropFuncaoDistribuidor = pageControl.FindControl("dropFuncaoDistribuidor") as ASPxComboBox;
        ASPxTextBox textEndereco = pageControl.FindControl("textEndereco") as ASPxTextBox;
        ASPxTextBox textBairro = pageControl.FindControl("textBairro") as ASPxTextBox;
        ASPxTextBox textCidade = pageControl.FindControl("textCidade") as ASPxTextBox;
        ASPxTextBox textCEP = pageControl.FindControl("textCEP") as ASPxTextBox;
        ASPxTextBox textUF = pageControl.FindControl("textUF") as ASPxTextBox;
        ASPxTextBox textDDD = pageControl.FindControl("textDDD") as ASPxTextBox;
        ASPxTextBox textTelefone = pageControl.FindControl("textTelefone") as ASPxTextBox;
        ASPxTextBox textRamal = pageControl.FindControl("textRamal") as ASPxTextBox;
        ASPxTextBox textFax = pageControl.FindControl("textFax") as ASPxTextBox;
        ASPxTextBox textEmail = pageControl.FindControl("textEmail") as ASPxTextBox;
        ASPxTextBox textCPFResponsavel = pageControl.FindControl("textCPFResponsavel") as ASPxTextBox;
        ASPxTextBox textCPFResponsavel2 = pageControl.FindControl("textCPFResponsavel2") as ASPxTextBox;
        TextBox textNomeResponsavel = pageControl.FindControl("textNomeResponsavel") as TextBox;
        TextBox textWebsite = pageControl.FindControl("textWebsite") as TextBox;
        TextBox textTelOuvidoria = pageControl.FindControl("textTelOuvidoria") as TextBox;
        TextBox textEmailOuvidoria = pageControl.FindControl("textEmailOuvidoria") as TextBox;
        ASPxComboBox dropCorretagemAdicional = pageControl.FindControl("dropCorretagemAdicional") as ASPxComboBox;
        TextBox textCartaPatente = pageControl.FindControl("textCartaPatente") as TextBox;
        ASPxTextBox textCodigoCetip = pageControl.FindControl("textCodigoCetip") as ASPxTextBox;
        ASPxComboBox dropEmpSecuritizada = pageControl.FindControl("dropEmpSecuritizada") as ASPxComboBox;
        ASPxSpinEdit btnEditCodigoPessoa = pageControl.FindControl("btnEditCodigoPessoa") as ASPxSpinEdit;
        ASPxTextBox textCodigoAnbid = pageControl.FindControl("textCodigoAnbid") as ASPxTextBox;

        AgenteMercado agenteMercado = new AgenteMercado();
        if (agenteMercado.LoadByPrimaryKey(idAgenteMercado))
        {
            agenteMercado.Nome = textNome.Text.ToString();
            agenteMercado.Apelido = textApelidoAgente.Text.ToString();
            agenteMercado.Cnpj = Utilitario.RemoveCaracteresEspeciais(textCNPJ.Text.ToString());

            int? codigoBovespa = null;
            byte? digitoCodigoBovespa = null;
            if (textCodigoBovespa.Text != "" && !textCodigoBovespa.Text.Contains("-"))
            {
                codigoBovespa = Convert.ToInt32(textCodigoBovespa.Text);
            }
            else if (textCodigoBovespa.Text != "")
            {
                string[] lista = textCodigoBovespa.Text.Split(new Char[] { '-' });

                codigoBovespa = Convert.ToInt32(lista[0]);
                digitoCodigoBovespa = Convert.ToByte(lista[1]);                
            }
            agenteMercado.CodigoBovespa = codigoBovespa;
            agenteMercado.DigitoCodigoBovespa = digitoCodigoBovespa;

            int? codigoBMF = null;
            byte? digitoCodigoBMF = null;
            if (textCodigoBMF.Text != "" && !textCodigoBMF.Text.Contains("-"))
            {
                codigoBMF = Convert.ToInt32(textCodigoBMF.Text);
            }
            else if (textCodigoBMF.Text != "")
            {
                string[] lista = textCodigoBMF.Text.Split(new Char[] { '-' });

                codigoBMF = Convert.ToInt32(lista[0]);
                digitoCodigoBMF = Convert.ToByte(lista[1]);
            }
            agenteMercado.CodigoBMF = codigoBMF;
            agenteMercado.DigitoCodigoBMF = digitoCodigoBMF;

            agenteMercado.CodigoCetip = textCodigoCetip.Text;

            agenteMercado.FuncaoCorretora = Convert.ToString(dropFuncaoCorretora.SelectedItem.Value);
            agenteMercado.FuncaoAdministrador = Convert.ToString(dropFuncaoAdministrador.SelectedItem.Value);
            agenteMercado.FuncaoGestor = Convert.ToString(dropFuncaoGestor.SelectedItem.Value);
            agenteMercado.FuncaoLiquidante = Convert.ToString(dropFuncaoLiquidante.SelectedItem.Value);
            agenteMercado.FuncaoCustodiante = Convert.ToString(dropFuncaoCustodiante.SelectedItem.Value);
            agenteMercado.FuncaoDistribuidor = Convert.ToString(dropFuncaoDistribuidor.SelectedItem.Value);
            agenteMercado.Endereco = textEndereco.Text.ToString();
            agenteMercado.Bairro = textBairro.Text.ToString();
            agenteMercado.Cidade = textCidade.Text.ToString();
            agenteMercado.Cep = textCEP.Text.ToString();
            agenteMercado.Uf = textUF.Text.ToString();
            agenteMercado.Ddd = textDDD.Text.ToString();
            agenteMercado.Telefone = textTelefone.Text.ToString();
            agenteMercado.Ramal = textRamal.Text.ToString();
            agenteMercado.Fax = textFax.Text.ToString();
            agenteMercado.Email = textEmail.Text.ToString();
            agenteMercado.CPFResponsavel = textCPFResponsavel.Text.Replace("-", "").Replace(".", "").Trim();
            agenteMercado.CPFResponsavel2 = textCPFResponsavel2.Text.Replace("-", "").Replace(".", "").Trim();
            agenteMercado.NomeResponsavel = textNomeResponsavel.Text.ToString();
            agenteMercado.Website = textWebsite.Text.ToString();
            agenteMercado.TelOuvidoria = textTelOuvidoria.Text.ToString();
            agenteMercado.EmailOuvidoria = textEmailOuvidoria.Text.ToString();
            agenteMercado.CartaPatente = textCartaPatente.Text.ToString();
            agenteMercado.EmpSecuritizada = dropEmpSecuritizada.SelectedItem != null ? dropEmpSecuritizada.SelectedItem.Value.ToString() : string.Empty;

            if (dropCorretagemAdicional.SelectedIndex != -1)
            {
                agenteMercado.CorretagemAdicional = Convert.ToByte(dropCorretagemAdicional.SelectedItem.Value);
            }

            if (!string.IsNullOrEmpty(btnEditCodigoPessoa.Text))
                agenteMercado.IdPessoa = Convert.ToInt32(btnEditCodigoPessoa.Text);
            else
                agenteMercado.IdPessoa = null;

            if (!string.IsNullOrEmpty(textCodigoAnbid.Text))
            {
                agenteMercado.CodigoAnbid = textCodigoAnbid.Text;
            }
            else
            {
                agenteMercado.CodigoAnbid = null;
            }

            agenteMercado.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de AgenteMercado - Operacao: Update AgenteMercado: " + idAgenteMercado + UtilitarioWeb.ToString(agenteMercado),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;

        ASPxTextBox textNome = pageControl.FindControl("textNome") as ASPxTextBox;
        ASPxTextBox textApelidoAgente = pageControl.FindControl("textApelidoAgente") as ASPxTextBox;
        ASPxTextBox textCNPJ = pageControl.FindControl("textCNPJ") as ASPxTextBox;
        ASPxTextBox textCodigoBovespa = pageControl.FindControl("textCodigoBovespa") as ASPxTextBox;
        ASPxTextBox textCodigoBMF = pageControl.FindControl("textCodigoBMF") as ASPxTextBox;
        ASPxComboBox dropFuncaoCorretora = pageControl.FindControl("dropFuncaoCorretora") as ASPxComboBox;
        ASPxComboBox dropFuncaoAdministrador = pageControl.FindControl("dropFuncaoAdministrador") as ASPxComboBox;
        ASPxComboBox dropFuncaoGestor = pageControl.FindControl("dropFuncaoGestor") as ASPxComboBox;
        ASPxComboBox dropFuncaoLiquidante = pageControl.FindControl("dropFuncaoLiquidante") as ASPxComboBox;
        ASPxComboBox dropFuncaoCustodiante = pageControl.FindControl("dropFuncaoCustodiante") as ASPxComboBox;
        ASPxComboBox dropFuncaoDistribuidor = pageControl.FindControl("dropFuncaoDistribuidor") as ASPxComboBox;
        ASPxTextBox textEndereco = pageControl.FindControl("textEndereco") as ASPxTextBox;
        ASPxTextBox textBairro = pageControl.FindControl("textBairro") as ASPxTextBox;
        ASPxTextBox textCidade = pageControl.FindControl("textCidade") as ASPxTextBox;
        ASPxTextBox textCEP = pageControl.FindControl("textCEP") as ASPxTextBox;
        ASPxTextBox textUF = pageControl.FindControl("textUF") as ASPxTextBox;
        ASPxTextBox textDDD = pageControl.FindControl("textDDD") as ASPxTextBox;
        ASPxTextBox textTelefone = pageControl.FindControl("textTelefone") as ASPxTextBox;
        ASPxTextBox textRamal = pageControl.FindControl("textRamal") as ASPxTextBox;
        ASPxTextBox textFax = pageControl.FindControl("textFax") as ASPxTextBox;
        ASPxTextBox textEmail = pageControl.FindControl("textEmail") as ASPxTextBox;
        ASPxTextBox textCPFResponsavel = pageControl.FindControl("textCPFResponsavel") as ASPxTextBox;
        ASPxTextBox textCPFResponsavel2 = pageControl.FindControl("textCPFResponsavel2") as ASPxTextBox;
        TextBox textNomeResponsavel = pageControl.FindControl("textNomeResponsavel") as TextBox;
        TextBox textWebsite = pageControl.FindControl("textWebsite") as TextBox;
        TextBox textTelOuvidoria = pageControl.FindControl("textTelOuvidoria") as TextBox;
        TextBox textEmailOuvidoria = pageControl.FindControl("textEmailOuvidoria") as TextBox;
        ASPxComboBox dropCorretagemAdicional = pageControl.FindControl("dropCorretagemAdicional") as ASPxComboBox;
        TextBox textCartaPatente = pageControl.FindControl("textCartaPatente") as TextBox;
        ASPxTextBox textCodigoCetip = pageControl.FindControl("textCodigoCetip") as ASPxTextBox;
        ASPxComboBox dropEmpSecuritizada = pageControl.FindControl("dropEmpSecuritizada") as ASPxComboBox;
        ASPxSpinEdit btnEditCodigoPessoa = pageControl.FindControl("btnEditCodigoPessoa") as ASPxSpinEdit;
        ASPxTextBox textCodigoAnbid = pageControl.FindControl("textCodigoAnbid") as ASPxTextBox;
        
        AgenteMercado agenteMercado = new AgenteMercado();
        //
        agenteMercado.Nome = textNome.Text.ToString();
        agenteMercado.Apelido = textApelidoAgente.Text.ToString();
        agenteMercado.Cnpj = Utilitario.RemoveCaracteresEspeciais(textCNPJ.Text.ToString());
        //
        int? codigoBovespa = null;
        byte? digitoCodigoBovespa = null;
        if (textCodigoBovespa.Text != "" && !textCodigoBovespa.Text.Contains("-"))
        {
            codigoBovespa = Convert.ToInt32(textCodigoBovespa.Text);
        }
        else if (textCodigoBovespa.Text != "")
        {
            string[] lista = textCodigoBovespa.Text.Split(new Char[] { '-' });

            codigoBovespa = Convert.ToInt32(lista[0]);
            digitoCodigoBovespa = Convert.ToByte(lista[1]);
        }
        agenteMercado.CodigoBovespa = codigoBovespa;
        agenteMercado.DigitoCodigoBovespa = digitoCodigoBovespa;

        int? codigoBMF = null;
        byte? digitoCodigoBMF = null;
        if (textCodigoBMF.Text != "" && !textCodigoBMF.Text.Contains("-"))
        {
            codigoBMF = Convert.ToInt32(textCodigoBMF.Text);
        }
        else if (textCodigoBMF.Text != "")
        {
            string[] lista = textCodigoBMF.Text.Split(new Char[] { '-' });

            codigoBMF = Convert.ToInt32(lista[0]);
            digitoCodigoBMF = Convert.ToByte(lista[1]);
        }
        agenteMercado.CodigoBMF = codigoBMF;
        agenteMercado.DigitoCodigoBMF = digitoCodigoBMF;

        agenteMercado.CodigoCetip = textCodigoCetip.Text;

        agenteMercado.FuncaoCorretora = Convert.ToString(dropFuncaoCorretora.SelectedItem.Value);
        agenteMercado.FuncaoAdministrador = Convert.ToString(dropFuncaoAdministrador.SelectedItem.Value);
        agenteMercado.FuncaoGestor = Convert.ToString(dropFuncaoGestor.SelectedItem.Value);
        agenteMercado.FuncaoLiquidante = Convert.ToString(dropFuncaoLiquidante.SelectedItem.Value);
        agenteMercado.FuncaoCustodiante = Convert.ToString(dropFuncaoCustodiante.SelectedItem.Value);
        agenteMercado.FuncaoDistribuidor = Convert.ToString(dropFuncaoDistribuidor.SelectedItem.Value);
        agenteMercado.Endereco = textEndereco.Text.ToString();
        agenteMercado.Bairro = textBairro.Text.ToString();
        agenteMercado.Cidade = textCidade.Text.ToString();
        agenteMercado.Cep = textCEP.Text.ToString();
        agenteMercado.Uf = textUF.Text.ToString();
        agenteMercado.Ddd = textDDD.Text.ToString();
        agenteMercado.Telefone = textTelefone.Text.ToString();
        agenteMercado.Ramal = textRamal.Text.ToString();
        agenteMercado.Fax = textFax.Text.ToString();
        agenteMercado.Email = textEmail.Text.ToString();
        agenteMercado.CPFResponsavel = textCPFResponsavel.Text.Replace("-", "").Replace(".", "").Trim();
        agenteMercado.CPFResponsavel2 = textCPFResponsavel2.Text.Replace("-", "").Replace(".", "").Trim();
        agenteMercado.NomeResponsavel = textNomeResponsavel.Text.ToString();
        agenteMercado.Website = textWebsite.Text.ToString();
        agenteMercado.TelOuvidoria = textTelOuvidoria.Text.ToString();
        agenteMercado.EmailOuvidoria = textEmailOuvidoria.Text.ToString();
        agenteMercado.CartaPatente = textCartaPatente.Text.ToString();
        agenteMercado.EmpSecuritizada = dropEmpSecuritizada.SelectedItem != null ? dropEmpSecuritizada.SelectedItem.Value.ToString() : string.Empty;

        if (dropCorretagemAdicional.SelectedIndex != -1)
        {
            agenteMercado.CorretagemAdicional = Convert.ToByte(dropCorretagemAdicional.SelectedItem.Value);
        }

        if (!string.IsNullOrEmpty(btnEditCodigoPessoa.Text))
            agenteMercado.IdPessoa = Convert.ToInt32(btnEditCodigoPessoa.Text);
        else
            agenteMercado.IdPessoa = null;

        if (!string.IsNullOrEmpty(textCodigoAnbid.Text))
        {
            agenteMercado.CodigoAnbid = textCodigoAnbid.Text;
        }
        else
        {
            agenteMercado.CodigoAnbid = null;
        }

        agenteMercado.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de AgenteMercado - Operacao: Insert AgenteMercado: " + agenteMercado.IdAgente + UtilitarioWeb.ToString(agenteMercado),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) {
        if (e.Column.FieldName == "CodigoBovespaCompleto") {
            int? cod = null;
            if (!Convert.IsDBNull(e.GetListSourceFieldValue(AgenteMercadoMetadata.ColumnNames.CodigoBovespa)) &&
                e.GetListSourceFieldValue(AgenteMercadoMetadata.ColumnNames.CodigoBovespa) != null) {
                cod = Convert.ToInt32(e.GetListSourceFieldValue(AgenteMercadoMetadata.ColumnNames.CodigoBovespa));
            }
            
            int? digito = null;
            if (!Convert.IsDBNull(e.GetListSourceFieldValue(AgenteMercadoMetadata.ColumnNames.DigitoCodigoBovespa)) &&
                e.GetListSourceFieldValue(AgenteMercadoMetadata.ColumnNames.DigitoCodigoBovespa) != null) {
                digito = Convert.ToInt32(e.GetListSourceFieldValue(AgenteMercadoMetadata.ColumnNames.DigitoCodigoBovespa));
            }

            string codigoCompleto = "";
            if(cod.HasValue) {
                codigoCompleto = cod.Value.ToString();
            }

            if (digito.HasValue) {
                if (cod.HasValue) {
                    codigoCompleto += "-" + digito.Value.ToString();
                }
                else {
                    codigoCompleto += digito.Value;
                }
            }

            e.Value = codigoCompleto;
        }

        if (e.Column.FieldName == "CodigoBMFCompleto") {
            int? cod = null;
            if (!Convert.IsDBNull(e.GetListSourceFieldValue(AgenteMercadoMetadata.ColumnNames.CodigoBMF)) &&
                e.GetListSourceFieldValue(AgenteMercadoMetadata.ColumnNames.CodigoBMF) != null) {
                cod = Convert.ToInt32(e.GetListSourceFieldValue(AgenteMercadoMetadata.ColumnNames.CodigoBMF));
            }

            int? digito = null;
            if (!Convert.IsDBNull(e.GetListSourceFieldValue(AgenteMercadoMetadata.ColumnNames.DigitoCodigoBMF)) &&
                    e.GetListSourceFieldValue(AgenteMercadoMetadata.ColumnNames.DigitoCodigoBMF)!= null ) {
                digito = Convert.ToInt32(e.GetListSourceFieldValue(AgenteMercadoMetadata.ColumnNames.DigitoCodigoBMF));
            }

            string codigoCompleto = "";
            if (cod.HasValue) {
                codigoCompleto = cod.Value.ToString();
            }

            if (digito.HasValue) {
                if (cod.HasValue) {
                    codigoCompleto += "-" + digito.Value.ToString();
                }
                else {
                    codigoCompleto += digito.Value;
                }
            }

            e.Value = codigoCompleto;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(AgenteMercadoMetadata.ColumnNames.IdAgente);
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idAgente = Convert.ToInt32(keyValuesId[i]);

                AgenteMercado agenteMercado = new AgenteMercado();
                if (agenteMercado.LoadByPrimaryKey(idAgente))
                {
                    //
                    AgenteMercado agenteMercadoClone = (AgenteMercado)Utilitario.Clone(agenteMercado);
                    //
                    agenteMercado.MarkAsDeleted();
                    agenteMercado.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de AgenteMercado - Operacao: Delete AgenteMercado: " + idAgente + UtilitarioWeb.ToString(agenteMercadoClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),

                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e)
    {
        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing)
        {
            ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;
            ASPxTextBox textNome = pageControl.FindControl("textNome") as ASPxTextBox;
            e.Properties["cpTextNome"] = textNome.ClientID;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e)
    {
        bool isOnTabPage = true;
        this.FocaCampoGrid("textNome", isOnTabPage);
        base.gridCadastro_PreRender(sender, e);
    }

    /// <summary>
    /// Verifica se o CNPJ é válido
    /// </summary>
    /// <param name="cnpj">Valor a ser verificado</param>
    /// <returns>Condição de validação</returns>
    private static bool ValidaCNPJ(string vrCnpj)
    {
        string CNPJ = vrCnpj.Replace(".", "");
        CNPJ = CNPJ.Replace("/", "");
        CNPJ = CNPJ.Replace("-", "");
        int[] digitos, soma, resultado;
        int nrDig;
        string ftmt;
        bool[] CNPJOk;
        ftmt = "6543298765432";
        digitos = new int[14];
        soma = new int[2];
        soma[0] = 0;
        soma[1] = 0;
        resultado = new int[2];
        resultado[0] = 0;
        resultado[1] = 0;
        CNPJOk = new bool[2];
        CNPJOk[0] = false;
        CNPJOk[1] = false;
        try
        {
            for (nrDig = 0; nrDig < 14; nrDig++)
            {
                digitos[nrDig] = int.Parse(CNPJ.Substring(nrDig, 1));
                if (nrDig <= 11) soma[0] += (digitos[nrDig] * int.Parse(ftmt.Substring(nrDig + 1, 1)));
                if (nrDig <= 12) soma[1] += (digitos[nrDig] * int.Parse(ftmt.Substring(nrDig, 1)));
            }
            for (nrDig = 0; nrDig < 2; nrDig++)
            {
                resultado[nrDig] = (soma[nrDig] % 11);
                if ((resultado[nrDig] == 0) || (resultado[nrDig] == 1))
                    CNPJOk[nrDig] = (digitos[12 + nrDig] == 0);
                else
                    CNPJOk[nrDig] = (digitos[12 + nrDig] == (11 - resultado[nrDig]));
            }
            return (CNPJOk[0] && CNPJOk[1]);
        }
        catch
        {
            return false;
        }
    }

    /// <summary>
    /// Verifica se o CPF é válido
    /// </summary>
    /// <param name="cpf"></param>
    /// <returns></returns>
    public static bool ValidaCPF(string vrCPF)
    {
        string valor = vrCPF.Replace(".", "");
        valor = valor.Replace("-", "");

        if (valor.Length != 11)
            return false;

        bool igual = true;
        for (int i = 1; i < 11 && igual; i++)
            if (valor[i] != valor[0])
                igual = false;

        if (igual || valor == "12345678909")
            return false;

        int[] numeros = new int[11];

        for (int i = 0; i < 11; i++)
            numeros[i] = int.Parse(valor[i].ToString());

        int soma = 0;
        for (int i = 0; i < 9; i++)
            soma += (10 - i) * numeros[i];

        int resultado = soma % 11;
        if (resultado == 1 || resultado == 0)
        {
            if (numeros[9] != 0) return false;
        }
        else if (numeros[9] != 11 - resultado) return false;

        soma = 0;
        for (int i = 0; i < 10; i++)
            soma += (11 - i) * numeros[i];

        resultado = soma % 11;
        if (resultado == 1 || resultado == 0)
        {
            if (numeros[10] != 0) return false;
        }
        else if (numeros[10] != 11 - resultado) return false;

        return true;
    }


    protected void pessoa_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string resultado = string.Empty;
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idPessoa = Convert.ToInt32(e.Parameter);
            Pessoa pessoa = new Pessoa();
            if (pessoa.LoadByPrimaryKey(idPessoa))
            {
                resultado = pessoa.Nome;
                resultado += "|" + pessoa.Nome;
                resultado += "|" + pessoa.Apelido;
                resultado += "|" + pessoa.Cpfcnpj;

                if (pessoa.PessoaEndereco != null)
                {
                    resultado += "|" + pessoa.PessoaEndereco.Endereco;
                    resultado += "|" + pessoa.PessoaEndereco.Bairro;
                    resultado += "|" + pessoa.PessoaEndereco.Cidade;
                    resultado += "|" + pessoa.PessoaEndereco.Cep;
                    resultado += "|" + pessoa.PessoaEndereco.Uf;
                }
                else
                {
                    resultado += "|" + string.Empty;
                    resultado += "|" + string.Empty;
                    resultado += "|" + string.Empty;
                    resultado += "|" + string.Empty;
                    resultado += "|" + string.Empty;
                }
                if (pessoa.PessoaTelefone != null)
                {
                    resultado += "|" + pessoa.PessoaTelefone.Ddd;
                    resultado += "|" + pessoa.PessoaTelefone.Numero;
                    resultado += "|" + pessoa.PessoaTelefone.Ramal;
                }
                else
                {
                    resultado += "|" + string.Empty;
                    resultado += "|" + string.Empty;
                    resultado += "|" + string.Empty;
                }

                resultado += "|" + pessoa.Email; 
            }
        }
        e.Result = resultado;
    }

    protected void EsDSPessoa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        PessoaCollection coll = new PessoaCollection();

        coll.Query.Where(coll.Query.Tipo.Equal((int)TipoPessoa.Juridica));
        coll.Query.OrderBy(coll.Query.Apelido.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void labelPessoa_Load(object sender, EventArgs e)
    {
        if (EstendeConceitoPessoaAgente)
            (sender as Label).CssClass = "labelRequired";
        else
            (sender as Label).CssClass = "labelNormal";
    }

    protected void textCNPJ_Load(object sender, EventArgs e)
    {
        if (EstendeConceitoPessoaAgente)
            (sender as ASPxTextBox).ClientEnabled = false;
        else
            (sender as ASPxTextBox).ClientEnabled = true;
    }


    protected void btnEditCodigoPessoa_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as ASPxSpinEdit).ClientEnabled = false;
        else
            (sender as ASPxSpinEdit).ClientEnabled = true;
    }

    protected void CamposPessoa_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;
        string habilita = "S";
        if (pageControl != null)
        {
            ASPxSpinEdit btnEditCodigoPessoa = pageControl.FindControl("btnEditCodigoPessoa") as ASPxSpinEdit;

            if (btnEditCodigoPessoa != null && !string.IsNullOrEmpty(btnEditCodigoPessoa.Text))
                habilita = "N";            
        }

        e.Result = habilita;
    }
}

