﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PerfilClassificacaoFundoEntidade.aspx.cs" Inherits="CadastrosBasicos_PerfilClassificacaoFundoEntidade" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">

    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
        var popup = false;
        document.onkeydown = onDocumentKeyDown;
    </script>

    <script type="text/javascript" language="Javascript">
        popup = true;
        document.onkeydown = onDocumentKeyDownWithCallback;
        var operacao = '';
    </script>

    <script type="text/javascript" language="Javascript">
        var isCustomCallback = false;   // usada para controlar o refresh após um delete feito no grid
    </script>
</head>

<body>
    <form id="form1" runat="server">

        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" />

        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div class="divPanel" style="width: 100%">

                    <table>
                        <tr>
                            <td>
                                <div id="container">

                                    <div id="header">
                                        <asp:Label ID="lblHeader" runat="server" Text="Perfil de Classificação de Fundos - Associação de Entidades"></asp:Label>
                                    </div>

                                    <div id="mainContent">
                                        <table>
                                            <tr>
                                                <td width="190px">
                                                    <asp:Label ID="lblPerfil" runat="server" CssClass="labelRequired" Text="Perfil de Classificação:"></asp:Label>
                                                </td>
                                                <td>
                                                    <dxe:ASPxComboBox ID="ddlPerfil" ClientInstanceName="ddlPerfil" runat="server" DataSourceID="EsDSPerfilClassificacaoFundo" DropDownStyle="DropDown" Width="150px"
                                                        ValueField="IdPerfil" TextField="Codigo" CssClass="dropDownList" AutoPostBack="true" OnSelectedIndexChanged="ddlPerfil_SelectedIndexChanged">
                                                    </dxe:ASPxComboBox>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td width="190px">
                                                    <asp:Label ID="lblDescricaoPerfil" runat="server" CssClass="label" Text="Descrição do Perfil:"></asp:Label>
                                                </td>
                                                <td>
                                                    <dxe:ASPxTextBox ID="txtDescricaoPerfil" ClientInstanceName="txtDescricaoPerfil" runat="server" Enabled="false" Width="500px">
                                                    </dxe:ASPxTextBox>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td width="190px">
                                                    <asp:Label ID="lblInicioVigencia" runat="server" CssClass="label" Text="Início Vigência:"></asp:Label>
                                                </td>
                                                <td>
                                                    <dxe:ASPxDateEdit ID="txtInicioVigencia" ClientInstanceName="txtInicioVigencia" runat="server" Enabled="false">
                                                    </dxe:ASPxDateEdit>
                                                </td>
                                            </tr>
                                        </table>
                                        <fieldset>
                                            <legend>Associação de Entidades e Classificações de Tipos de Fundo/Carteira</legend>
                                            <table>
                                                <tr>
                                                    <td width="180px">
                                                        <asp:Label ID="lblEntidade" runat="server" CssClass="labelRequired" Text="Entidade:"></asp:Label>
                                                    </td>
                                                    <td colspan="2">
                                                        <dxe:ASPxComboBox ID="ddlEntidade" ClientInstanceName="ddlEntidade" runat="server" DataSourceID="EsDSEntidadePerfilFundo" DropDownStyle="DropDown"
                                                            ValueField="IdEntidade" TextField="Codigo" CssClass="dropDownList" AutoPostBack="true" OnSelectedIndexChanged="ddlEntidade_SelectedIndexChanged">
                                                        </dxe:ASPxComboBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="180px">
                                                        <asp:Label ID="lblClassificacao" runat="server" CssClass="labelRequired" Text="Classificação Tipo Fundo/Carteira:"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxComboBox ID="ddlClassificacao" ClientInstanceName="ddlClassificacao" runat="server" DropDownStyle="DropDown"
                                                            CssClass="dropDownList" Width="350px">
                                                        </dxe:ASPxComboBox>

                                                    </td>
                                                    <td>
                                                        <div class="linkButton linkButtonNoBorder" style="width: 78px">
                                                            <asp:LinkButton ID="btnIncluir" runat="server" OnClick="btnIncluir_Click" CssClass="btnAdd" ValidationGroup="ATK">
                                                                <asp:Literal ID="Literal5" runat="server" Text="Associar" /><div></div>
                                                            </asp:LinkButton>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>

                                        <div class="linkButton">
                                            <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;">
                                                <asp:Literal ID="Literal2" runat="server" Text="Excluir" /><div></div>
                                            </asp:LinkButton>
                                        </div>

                                        <div class="divDataGrid">
                                            <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                                                OnCustomCallback="gridCadastro_CustomCallback" DataSourceID="EsDSPerfilClassificacaoFundoEntidade" KeyFieldName="IdPerfilClassificacaoFundoEntidade">

                                                <Columns>
                                                    <dxwgv:GridViewCommandColumn VisibleIndex="0" ShowClearFilterButton="True">
                                                        <HeaderTemplate>
                                                            <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll" />
                                                        </HeaderTemplate>
                                                    </dxwgv:GridViewCommandColumn>

                                                    <dxwgv:GridViewDataTextColumn FieldName="Entidade" Width="20%" VisibleIndex="2">
                                                        <PropertiesTextEdit MaxLength="100"></PropertiesTextEdit>
                                                    </dxwgv:GridViewDataTextColumn>

                                                    <dxwgv:GridViewDataTextColumn FieldName="ClassificacaoTipoFundoCarteira" Width="70%" VisibleIndex="3">
                                                        <PropertiesTextEdit MaxLength="255"></PropertiesTextEdit>
                                                    </dxwgv:GridViewDataTextColumn>
                                                </Columns>
                                                <SettingsCommandButton>
                                                    <ClearFilterButton Image-Url="../../imagens/funnel--minus.png" />
                                                </SettingsCommandButton>

                                                <ClientSideEvents
                                                    BeginCallback="function(s, e) {
		            if (e.command == 'CUSTOMCALLBACK') {
                        isCustomCallback = true;
                    }						
                }"
                                                    EndCallback="function(s, e) {
			        if (isCustomCallback) {
                        isCustomCallback = false;
                        s.Refresh();
                    }
                }" />
                                                <SettingsCommandButton>
                                                    <ClearFilterButton Image-Url="../../imagens/funnel--minus.png" />
                                                    <UpdateButton Image-Url="../../imagens/ico_form_ok_inline.gif" />
                                                    <CancelButton Image-Url="../../imagens/ico_form_back_inline.gif" />
                                                </SettingsCommandButton>

                                            </dxwgv:ASPxGridView>
                                        </div>

                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>

            </ContentTemplate>
        </asp:UpdatePanel>

        <cc1:esDataSource ID="EsDSPerfilClassificacaoFundo" runat="server" OnesSelect="EsDSPerfilClassificacaoFundo_esSelect" />
        <cc1:esDataSource ID="EsDSEntidadePerfilFundo" runat="server" OnesSelect="EsDSEntidadePerfilFundo_esSelect" />
        <cc1:esDataSource ID="EsDSPerfilClassificacaoFundoEntidade" runat="server" OnesSelect="EsDSPerfilClassificacaoFundoEntidade_esSelect" />
    </form>
</body>
</html>