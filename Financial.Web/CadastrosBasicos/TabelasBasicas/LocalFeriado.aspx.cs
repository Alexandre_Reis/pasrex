using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_LocalFeriado : Financial.Web.Common.CadastroBasePage
{
    protected void EsDSLocalFeriado_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        LocalFeriadoCollection coll = new LocalFeriadoCollection();

        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        LocalFeriado localFeriado = new LocalFeriado();
        int idLocal = Convert.ToInt32(e.Keys[0]);

        if (localFeriado.LoadByPrimaryKey((short)idLocal))
        {
            localFeriado.Nome = Convert.ToString(e.NewValues["Nome"]);
            localFeriado.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de LocalFeriado - Operacao: Update LocalFeriado: " + idLocal + UtilitarioWeb.ToString(localFeriado),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        LocalFeriado localFeriado = new LocalFeriado();

        localFeriado.Nome = Convert.ToString(e.NewValues["Nome"]);
        localFeriado.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de LocalFeriado - Operacao: Insert LocalFeriado: " + localFeriado.IdLocal + UtilitarioWeb.ToString(localFeriado),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    
        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();    
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdLocal");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idLocal = Convert.ToInt32(keyValuesId[i]);

                LocalFeriado localFeriado = new LocalFeriado();
                if (localFeriado.LoadByPrimaryKey((short)idLocal))
                {
                    LocalFeriado localFeriadoClone = (LocalFeriado)Utilitario.Clone(localFeriado);
                    //

                    localFeriado.MarkAsDeleted();
                    localFeriado.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de LocalFeriado - Operacao: Delete LocalFeriado: " + idLocal + UtilitarioWeb.ToString(localFeriadoClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }
}