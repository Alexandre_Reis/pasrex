﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Collections.Generic;
using System.Drawing;

using Financial.Fundo;
using Financial.Security;
using Financial.Web.Util;
using Financial.Util;
using Financial.Tributo;
using Financial.Web.Common;

using EntitySpaces.Interfaces;

using DevExpress.Web.Data;
using DevExpress.Web;


public partial class TabelasBasicas_TabelaProgressivaIR : CadastroBasePage
{
    new protected void Page_Load(object sender, EventArgs e) {

    }

    #region DataSources
    protected void EsDSTabelaProgressivaVigencia_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        TabelaProgressivaVigenciaCollection tabelaProgressivaVigenciaCollection = new TabelaProgressivaVigenciaCollection();
        tabelaProgressivaVigenciaCollection.Query.OrderBy(tabelaProgressivaVigenciaCollection.Query.Data.Descending);

        tabelaProgressivaVigenciaCollection.Query.Load();

        e.Collection = tabelaProgressivaVigenciaCollection;

    }

    

    /// <summary>
    /// PopUp Cliente
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSTabelaProgressivaValores_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {

        TabelaProgressivaValoresCollection tabelaProgressivaValoresCollection = new TabelaProgressivaValoresCollection();

        if (Session["idTabela"] != "")
        {
            int idTabela = (int)Session["idTabela"];
            tabelaProgressivaValoresCollection.Query.Where(tabelaProgressivaValoresCollection.Query.IdTabelaProgressiva.Equal(idTabela));
            tabelaProgressivaValoresCollection.Query.Load();
        }

        e.Collection = tabelaProgressivaValoresCollection;

    }

    #endregion

    protected void detailGrid_DataSelect(object sender, EventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;

        Session["idTabela"] = grid.GetMasterRowKeyValue();
    }
 
    protected void chkSingleExpanded_CheckedChanged(object sender, EventArgs e)
    {
        if (gridCadastro.SettingsDetail.AllowOnlyOneMasterRowExpanded)
        {
            gridCadastro.DetailRows.CollapseAllRows();
        }
    }

    protected void gridCadastro_RowValidating(object sender, DevExpress.Web.Data.ASPxDataValidationEventArgs e)
    {
       
        if (e.NewValues["Data"] == null)
            e.RowError = "Favor informar a data de vigência.";

        DateTime data = Convert.ToDateTime(e.NewValues["Data"]);

        TabelaProgressivaVigenciaCollection tabelaProgressivaVigenciaCollection = new TabelaProgressivaVigenciaCollection();
        tabelaProgressivaVigenciaCollection.Query.Where(tabelaProgressivaVigenciaCollection.Query.Data.Equal(data));
        if (!e.IsNewRow)
        {
            int idTabelaProgressiva = Convert.ToInt32(e.Keys[0]);
            tabelaProgressivaVigenciaCollection.Query.Where(tabelaProgressivaVigenciaCollection.Query.IdTabelaProgressiva.NotEqual(idTabelaProgressiva));
        }
        tabelaProgressivaVigenciaCollection.Query.Load();
        if (tabelaProgressivaVigenciaCollection.Count > 0)
            e.RowError = "Data de vigência já existe.";

    }

    protected void gridCadastro_RowInserting(object sender, ASPxDataInsertingEventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;

        TabelaProgressivaVigencia tabelaProgressivaVigencia = new TabelaProgressivaVigencia();

        this.AtualizaDataVigencia(tabelaProgressivaVigencia, e.NewValues);

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;

        TabelaProgressivaVigencia tabelaProgressivaVigencia = new TabelaProgressivaVigencia();

        if (tabelaProgressivaVigencia.LoadByPrimaryKey(Convert.ToInt32(e.Keys[0])))
        {
            this.AtualizaDataVigencia(tabelaProgressivaVigencia, e.NewValues);
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_RowDeleting(object sender, ASPxDataDeletingEventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;

        TabelaProgressivaVigencia tabelaProgressivaVigencia = new TabelaProgressivaVigencia();

        if (tabelaProgressivaVigencia.LoadByPrimaryKey(Convert.ToInt32(e.Keys[0])))
        {
            tabelaProgressivaVigencia.MarkAsDeleted();
            tabelaProgressivaVigencia.Save();
        }

        e.Cancel = true;
        grid.CancelEdit();
        grid.DataBind();
    }

    private void AtualizaDataVigencia(TabelaProgressivaVigencia tabelaProgressivaVigencia, System.Collections.Specialized.OrderedDictionary newValues)
    {
        tabelaProgressivaVigencia.Data = Convert.ToDateTime(newValues["Data"]);

        tabelaProgressivaVigencia.Save();
    }


    protected void detailGrid_RowValidating(object sender, DevExpress.Web.Data.ASPxDataValidationEventArgs e)
    {
        if (e.NewValues["Valor"] == null)
            e.RowError = "Favor informar o valor.";

        Decimal valor = Convert.ToDecimal(e.NewValues["Valor"]);

        if (valor < 0)
            e.RowError = "Valor inválido.";


        if (e.NewValues["Aliquota"] == null)
            e.RowError = "Favor informar a aliquota.";

        Decimal aliquota = Convert.ToDecimal(e.NewValues["Aliquota"]);

        if (aliquota < 0)
            e.RowError = "Aliquota inválida.";

        if (!e.IsNewRow)
        {
            if (Convert.ToDecimal(e.Keys[1]).CompareTo(valor) != 0)
            {
                int idTabelaProgressiva = Convert.ToInt32(e.Keys[0]);

                TabelaProgressivaValoresCollection tabelaProgressivaValoresCollection = new TabelaProgressivaValoresCollection();
                tabelaProgressivaValoresCollection.Query.Where(tabelaProgressivaValoresCollection.Query.Valor.Equal(valor));
                tabelaProgressivaValoresCollection.Query.Where(tabelaProgressivaValoresCollection.Query.IdTabelaProgressiva.Equal(idTabelaProgressiva));
                tabelaProgressivaValoresCollection.Query.Load();
                if (tabelaProgressivaValoresCollection.Count > 0)
                    e.RowError = "Valor de IR já existe.";
            }
        }
    }

    protected void detailGrid_RowInserting(object sender, ASPxDataInsertingEventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;

        TabelaProgressivaValores tabelaProgressivaValores = new TabelaProgressivaValores();

        this.AtualizaValores(tabelaProgressivaValores, e.NewValues);

        e.Cancel = true;
        grid.CancelEdit();
        grid.DataBind();
    }

    protected void detailGrid_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;

        TabelaProgressivaValores tabelaProgressivaValores = new TabelaProgressivaValores();

        if (tabelaProgressivaValores.LoadByPrimaryKey(Convert.ToInt32(e.Keys[0]), Convert.ToDecimal(e.Keys[1])))
        {
            tabelaProgressivaValores.MarkAsDeleted();
            tabelaProgressivaValores.Save();
            tabelaProgressivaValores = new TabelaProgressivaValores();
            this.AtualizaValores(tabelaProgressivaValores, e.NewValues);
        }

        e.Cancel = true;
        grid.CancelEdit();
        grid.DataBind();
    }

    protected void detailGrid_RowDeleting(object sender, ASPxDataDeletingEventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;

        TabelaProgressivaValores tabelaProgressivaValores = new TabelaProgressivaValores();

        if (tabelaProgressivaValores.LoadByPrimaryKey(Convert.ToInt32(e.Keys[0]), Convert.ToDecimal(e.Keys[1])))
        {
            tabelaProgressivaValores.MarkAsDeleted();
            tabelaProgressivaValores.Save();
        }

        e.Cancel = true;
        grid.CancelEdit();
        grid.DataBind();
    }

    private void AtualizaValores(TabelaProgressivaValores tabelaProgressivaValores, System.Collections.Specialized.OrderedDictionary newValues)
    {
        tabelaProgressivaValores.IdTabelaProgressiva = Convert.ToInt32(Session["idTabela"]);
        tabelaProgressivaValores.Valor = Convert.ToDecimal(newValues["Valor"]);
        tabelaProgressivaValores.Aliquota = Convert.ToDecimal(newValues["Aliquota"]);

        tabelaProgressivaValores.Save();
    }

}