﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using Financial.Common;
using Financial.Util;
using Financial.Fundo;
using Financial.InvestidorCotista;
using Financial.Investidor;
using Financial.Security;
using Financial.Web.Common;
using Financial.InvestidorCotista.Enums;
using Financial.Util.Enums;
using Financial.Tributo.Enums;
using Financial.Investidor.Enums;

using DevExpress.Web;
using DevExpress.Web.Data;

using System.Threading;
using EntitySpaces.Interfaces;
using Financial.Security.Enums;
using Financial.Web.Util;
using DevExpress.XtraGrid;
using Newtonsoft.Json;


public partial class TabelasBasicas_AlteracaoTributacaoFieFundo : CadastroBasePage
{
    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasFiltro = true;
        this.HasPopupCarteira = true;
        this.HasPopupCliente = true;
        this.HasPanelFieldsLoading = true;
        base.Page_Load(sender, e);
    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSPosicaoFundo_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CarteiraQuery carteiraQuery = new CarteiraQuery("C");
        ClienteQuery clienteQuery = new ClienteQuery("CT");
        PosicaoFundoHistoricoQuery posicaoFundoHistoricoQuery = new PosicaoFundoHistoricoQuery("PFH");
        OperacaoFundoQuery operacaoFundoQuery = new OperacaoFundoQuery("O");
        posicaoFundoHistoricoQuery.Select(posicaoFundoHistoricoQuery.IdPosicao,
                                            posicaoFundoHistoricoQuery.IdCarteira,
                                            carteiraQuery.Apelido.As("ApelidoCarteira"),
                                            posicaoFundoHistoricoQuery.IdCliente,
                                            clienteQuery.Apelido.As("ApelidoCliente"),
                                            posicaoFundoHistoricoQuery.DataHistorico,
                                            posicaoFundoHistoricoQuery.FieTabelaIr,
                                            operacaoFundoQuery.IdOperacao);
        posicaoFundoHistoricoQuery.InnerJoin(carteiraQuery).On(posicaoFundoHistoricoQuery.IdCarteira == carteiraQuery.IdCarteira);
        posicaoFundoHistoricoQuery.InnerJoin(clienteQuery).On(posicaoFundoHistoricoQuery.IdCliente == clienteQuery.IdCliente);
        posicaoFundoHistoricoQuery.InnerJoin(operacaoFundoQuery).On(posicaoFundoHistoricoQuery.IdOperacao == operacaoFundoQuery.IdOperacao);
        posicaoFundoHistoricoQuery.Where(posicaoFundoHistoricoQuery.FieTabelaIr.IsNotNull());
        posicaoFundoHistoricoQuery.Where(operacaoFundoQuery.FieModalidade.Equal(FieModalidade.VGBL.GetHashCode()));
        posicaoFundoHistoricoQuery.OrderBy(posicaoFundoHistoricoQuery.DataHistorico.Ascending, clienteQuery.Apelido.Ascending, operacaoFundoQuery.IdOperacao.Ascending);

        if (!String.IsNullOrEmpty(btnEditCodigoCarteiraFiltro.Text))
        {
            posicaoFundoHistoricoQuery.Where(posicaoFundoHistoricoQuery.IdCarteira.Equal(Convert.ToInt32(btnEditCodigoCarteiraFiltro.Text)));
        }

        if (!String.IsNullOrEmpty(btnEditCodigoClienteFiltro.Text))
        {
            posicaoFundoHistoricoQuery.Where(posicaoFundoHistoricoQuery.IdCliente.Equal(Convert.ToInt32(btnEditCodigoClienteFiltro.Text)));
        }

        if (!String.IsNullOrEmpty(textDataInicio.Text))
        {
            posicaoFundoHistoricoQuery.Where(posicaoFundoHistoricoQuery.DataHistorico.GreaterThanOrEqual(textDataInicio.Text));
        }

        if (!String.IsNullOrEmpty(textDataFim.Text))
        {
            posicaoFundoHistoricoQuery.Where(posicaoFundoHistoricoQuery.DataHistorico.LessThanOrEqual(textDataFim.Text));
        }

        PosicaoFundoHistoricoCollection posicaoFundoHistoricoCollection = new PosicaoFundoHistoricoCollection();
        posicaoFundoHistoricoCollection.Load(posicaoFundoHistoricoQuery);

        e.Collection = posicaoFundoHistoricoCollection;

    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ClienteCollection coll = new ClienteCollection();
        coll.BuscaClientesComAcesso(Context.User.Identity.Name);

        e.Collection = coll;
    }

    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CarteiraCollection coll = new CarteiraCollection();
        coll.BuscaCarteirasFundosClubesBoletagem(Context.User.Identity.Name);

        e.Collection = coll;
    }

    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        Carteira carteira = new Carteira();

        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCarteira = Convert.ToInt32(e.Parameter);
            if (carteira.LoadByPrimaryKey(idCarteira))
            {
                if (carteira.IsAtivo)
                {
                    e.Result = carteira.str.Nome;
                }
                else
                {
                    e.Result = "no_active";
                }
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "")
            {
                if (cliente.Status.Value != (byte)StatusCliente.Divulgado)
                {

                    if (cliente.IsAtivo)
                    {
                        PermissaoCliente permissaoCliente = new PermissaoCliente();
                        if (permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name))
                        {
                            nome = cliente.str.Apelido;
                        }
                        else
                        {
                            nome = "no_access";
                        }
                    }
                    else
                    {
                        nome = "no_active";
                    }
                }
                else
                {
                    nome = cliente.str.Apelido + "|status_closed";
                }
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e)
    {
        this.FocaCampoGrid("btnEditCodigoCarteira");
        base.gridCadastro_PreRender(sender, e);

        ASPxSpinEdit btnEditCodigoCarteiraFiltro = popupFiltro.FindControl("btnEditCodigoCarteiraFiltro") as ASPxSpinEdit;

        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        Label labelFiltroHeader = gridCadastro.FindStatusBarTemplateControl("labelFiltroHeader") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");

        if (btnEditCodigoCarteiraFiltro != null && btnEditCodigoCarteiraFiltro.Text != "")
        {
            texto.Append(" Carteira: ").Append(btnEditCodigoCarteiraFiltro.Text);
        }
        if (btnEditCodigoClienteFiltro.Text != "")
        {
            if (texto.Length > 0)
            {
                texto.Append(" |");
            }
            texto.Append(" Cliente: ").Append(btnEditCodigoClienteFiltro.Text);
        }
        if (textDataInicio.Text != "")
        {
            if (texto.Length > 0)
            {
                texto.Append(" |");
            }
            texto.Append(" Data Início >= ").Append(textDataInicio.Text.Substring(0, 10));
        }
        if (textDataFim.Text != "")
        {
            if (texto.Length > 0)
            {
                texto.Append(" |");
            }
            texto.Append(" Data Fim <= ").Append(textDataFim.Text.Substring(0, 10));
        }

        labelFiltro.Text = texto.ToString();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        ASPxComboBox dropFieTabelaIr = gridCadastro.FindEditFormTemplateControl("dropFieTabelaIr") as ASPxComboBox;
        ASPxSpinEdit btnEditOperacao = gridCadastro.FindEditFormTemplateControl("btnEditOperacao") as ASPxSpinEdit;
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxSpinEdit btnEditPosicao = gridCadastro.FindEditFormTemplateControl("btnEditPosicao") as ASPxSpinEdit;

        int idCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);
        int fieTabelaIr = Convert.ToInt32(dropFieTabelaIr.SelectedItem.Value);
        int idPosicao = Convert.ToInt32(btnEditPosicao.Text);

        int idOperacao = (int)e.Keys[1];
        DateTime dataPosicaoHistorico = (DateTime)e.Keys[0];

        PosicaoFundoHistorico posicaoFundoHistorico = new PosicaoFundoHistorico();
        posicaoFundoHistorico.LoadByPrimaryKey(idPosicao, dataPosicaoHistorico);

        posicaoFundoHistorico.FieTabelaIr = fieTabelaIr;

        posicaoFundoHistorico.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Tabela de IR - FIE - Operacao: Update PosicaoCotistaHistorico: " + idPosicao + ", " + dataPosicaoHistorico + " - " + UtilitarioWeb.ToString(posicaoFundoHistorico),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        TributacaoFundoFie tributacaoFundoFie = new TributacaoFundoFie();
        if (tributacaoFundoFie.LoadByPrimaryKey(dataPosicaoHistorico, idOperacao))
        {
            tributacaoFundoFie.FieTabelaIr = fieTabelaIr;
        }
        else
        {
            tributacaoFundoFie.Data = dataPosicaoHistorico;
            tributacaoFundoFie.IdOperacao = idOperacao;
            tributacaoFundoFie.FieTabelaIr = fieTabelaIr;
        }

        tributacaoFundoFie.Save();

        #region Operação retroativa - Verifica se deve marcar o cliente para efetuar o reprocessamento
        Cliente cliente = new Cliente();
        cliente.LoadByPrimaryKey(idCarteira);
        if (DateTime.Compare(cliente.DataDia.Value, dataPosicaoHistorico) > 0)
        {
            BoletoRetroativo boletoRetroativo = new BoletoRetroativo();
            if (boletoRetroativo.LoadByPrimaryKey(idCarteira))
            {
                if (DateTime.Compare(boletoRetroativo.DataBoleto.Value, dataPosicaoHistorico) > 0)
                {
                    boletoRetroativo.DataBoleto = dataPosicaoHistorico;
                }
            }
            else
            {
                boletoRetroativo.IdCliente = idCarteira;
                boletoRetroativo.DataBoleto = dataPosicaoHistorico;
                boletoRetroativo.TipoMercado = (int)TipoMercado.Cotista;
            }

            boletoRetroativo.Save();
        }
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();

    }


}