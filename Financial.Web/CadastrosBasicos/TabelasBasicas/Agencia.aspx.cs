﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using DevExpress.Web;

using Financial.Investidor;
using Financial.Fundo;
using Financial.Common;
using Financial.Util;
using Financial.Web.Common;
using EntitySpaces.Interfaces;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_Agencia : CadastroBasePage 
{
    #region DataSources
    protected void EsDSBanco_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        BancoCollection coll = new BancoCollection();

        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSAgencia_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        AgenciaCollection coll = new AgenciaCollection();

        coll.Query.OrderBy(coll.Query.IdBanco.Ascending, coll.Query.Nome.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSLocalFeriado_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        LocalFeriadoCollection coll = new LocalFeriadoCollection();
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) {
        if (e.Column.FieldName == "CompositeKey") {
            //Compose a primary key value
            string idBanco = Convert.ToString(e.GetListSourceFieldValue(AgenciaMetadata.ColumnNames.IdBanco));
            string idAgencia = Convert.ToString(e.GetListSourceFieldValue(AgenciaMetadata.ColumnNames.IdAgencia));
            e.Value = idBanco + "|" + idAgencia;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        Agencia agencia = new Agencia();
        int idAgencia = (int)e.Keys[0];

        if (agencia.LoadByPrimaryKey(idAgencia)) {
            agencia.Codigo = Convert.ToString(e.NewValues[AgenciaMetadata.ColumnNames.Codigo]);
            agencia.Nome = Convert.ToString(e.NewValues[AgenciaMetadata.ColumnNames.Nome]);
            agencia.IdBanco = Convert.ToInt32(e.NewValues[AgenciaMetadata.ColumnNames.IdBanco]);

            agencia.IdLocal = null;
            if (e.NewValues[AgenciaMetadata.ColumnNames.IdLocal] != null)
            {
                agencia.IdLocal = Convert.ToInt16(e.NewValues[AgenciaMetadata.ColumnNames.IdLocal]);
            }

            agencia.DigitoAgencia = null;
            if (e.NewValues[AgenciaMetadata.ColumnNames.DigitoAgencia] != null) { 
                agencia.DigitoAgencia = Convert.ToString(e.NewValues[AgenciaMetadata.ColumnNames.DigitoAgencia]);
            }
            
            agencia.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de Agência - Operacao: Update Agência: " + idAgencia + UtilitarioWeb.ToString(agencia),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        Agencia agencia = new Agencia();

        agencia.Codigo = Convert.ToString(e.NewValues[AgenciaMetadata.ColumnNames.Codigo]);
        agencia.Nome = Convert.ToString(e.NewValues[AgenciaMetadata.ColumnNames.Nome]);
        agencia.IdBanco = Convert.ToInt32(e.NewValues[AgenciaMetadata.ColumnNames.IdBanco]);

        agencia.IdLocal = null;
        if (e.NewValues[AgenciaMetadata.ColumnNames.IdLocal] != null)
        {
            agencia.IdLocal = Convert.ToInt16(e.NewValues[AgenciaMetadata.ColumnNames.IdLocal]);
        }

        agencia.DigitoAgencia = null;
        if (e.NewValues[AgenciaMetadata.ColumnNames.DigitoAgencia] != null) {
            agencia.DigitoAgencia = Convert.ToString(e.NewValues[AgenciaMetadata.ColumnNames.DigitoAgencia]);
        }

        agencia.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de Agência - Operacao: Insert Agência: " + agencia.IdAgencia + UtilitarioWeb.ToString(agencia),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) 
    {
        if (e.Parameters == "btnDelete") {
            List<object> keyValues = gridCadastro.GetSelectedFieldValues(AgenciaMetadata.ColumnNames.IdAgencia);
            for (int i = 0; i < keyValues.Count; i++) {
                int idAgencia = Convert.ToInt32(keyValues[i]);

                Agencia agencia = new Agencia();
                if (agencia.LoadByPrimaryKey(idAgencia)) {
                    
                    Agencia agenciaClone = (Agencia)Utilitario.Clone(agencia);

                    agencia.MarkAsDeleted();
                    try {
                        agencia.Save();

                        #region Log do Processo
                        HistoricoLog historicoLog = new HistoricoLog();
                        historicoLog.InsereHistoricoLog(DateTime.Now,
                                                        DateTime.Now,
                                                        "Cadastro de Agencia - Operacao: Delete Agencia: " + idAgencia + UtilitarioWeb.ToString(agenciaClone),
                                                        HttpContext.Current.User.Identity.Name,
                                                        UtilitarioWeb.GetIP(Request),
                                                        "",
                                                        HistoricoLogOrigem.Outros);
                        #endregion

                    }
                    catch (Exception) {
                        throw new Exception("Impossível Deletar Agência. Conta Corrente Associada.");
                    }                                       
                }
            }
        }


        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }
}