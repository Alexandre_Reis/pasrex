﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.ContaCorrente;
using Financial.InvestidorCotista;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_FormaLiquidacao : Financial.Web.Common.CadastroBasePage {
    #region DataSources
    protected void EsDSFormaLiquidacao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        FormaLiquidacaoCollection coll = new FormaLiquidacaoCollection();

        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        FormaLiquidacao formaLiquidacao = new FormaLiquidacao();
        byte idFormaLiquidacao = (byte)e.Keys[0];

        if (formaLiquidacao.LoadByPrimaryKey(idFormaLiquidacao)) {
            formaLiquidacao.Descricao = Convert.ToString(e.NewValues["Descricao"]);
            formaLiquidacao.TipoLiquidacao = Convert.ToByte(e.NewValues["TipoLiquidacao"]);
            formaLiquidacao.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de FormaLiquidacao - Operacao: Update FormaLiquidacao: " + idFormaLiquidacao + UtilitarioWeb.ToString(formaLiquidacao),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        FormaLiquidacao formaLiquidacao = new FormaLiquidacao();

        formaLiquidacao.Descricao = Convert.ToString(e.NewValues["Descricao"]);
        formaLiquidacao.TipoLiquidacao = Convert.ToByte(e.NewValues["TipoLiquidacao"]);
        formaLiquidacao.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de FormaLiquidacao - Operacao: Insert FormaLiquidacao: " + formaLiquidacao.IdFormaLiquidacao + UtilitarioWeb.ToString(formaLiquidacao),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdFormaLiquidacao");
            for (int i = 0; i < keyValuesId.Count; i++) {
                byte idFormaLiquidacao = Convert.ToByte(keyValuesId[i]);

                FormaLiquidacao formaLiquidacao = new FormaLiquidacao();
                if (formaLiquidacao.LoadByPrimaryKey(idFormaLiquidacao)) {
                    OperacaoCotistaCollection operacaoCotistaCollection = new OperacaoCotistaCollection();
                    operacaoCotistaCollection.Query.Where(operacaoCotistaCollection.Query.IdFormaLiquidacao.Equal(idFormaLiquidacao));
                    if (operacaoCotistaCollection.Query.Load()) {
                        throw new Exception("Forma de liquidação " + formaLiquidacao.Descricao + " não pode ser excluída por ter movimentos relacionados.");
                    }

                    //
                    FormaLiquidacao formaLiquidacaoClone = (FormaLiquidacao)Utilitario.Clone(formaLiquidacao);
                    //

                    formaLiquidacao.MarkAsDeleted();
                    formaLiquidacao.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de FormaLiquidacao - Operacao: Delete FormaLiquidacao: " + idFormaLiquidacao + UtilitarioWeb.ToString(formaLiquidacaoClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }
}