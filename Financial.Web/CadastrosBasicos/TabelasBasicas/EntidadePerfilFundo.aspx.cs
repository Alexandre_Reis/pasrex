﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Fundo;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_EntidadePerfilFundo : Financial.Web.Common.CadastroBasePage
{
    protected void EsDSListaEntidadePerfilFundo_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        EntidadePerfilFundoCollection coll = new EntidadePerfilFundoCollection();

        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        EntidadePerfilFundo entidadePerfilFundo = new EntidadePerfilFundo();
        int idEntidade = (int)e.Keys[0];

        if (entidadePerfilFundo.LoadByPrimaryKey(idEntidade))
        {
            entidadePerfilFundo.Codigo = Convert.ToString(e.NewValues["Codigo"]);
            entidadePerfilFundo.Descricao = Convert.ToString(e.NewValues["Descricao"]);
            entidadePerfilFundo.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de EntidadePerfilFundo - Operacao: Update EntidadePerfilFundo: " + idEntidade + UtilitarioWeb.ToString(entidadePerfilFundo),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        EntidadePerfilFundo entidadePerfilFundo = new EntidadePerfilFundo();

        entidadePerfilFundo.Codigo = Convert.ToString(e.NewValues["Codigo"]);
        entidadePerfilFundo.Descricao = Convert.ToString(e.NewValues["Descricao"]);
        entidadePerfilFundo.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de EntidadePerfilFundo - Operacao: Insert EntidadePerfilFundo: " + entidadePerfilFundo.IdEntidade + UtilitarioWeb.ToString(entidadePerfilFundo),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdEntidade");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idEntidade = Convert.ToInt32(keyValuesId[i]);

                EntidadePerfilFundo entidadePerfilFundo = new EntidadePerfilFundo();
                if (entidadePerfilFundo.LoadByPrimaryKey(idEntidade))
                {
                    EntidadeClassificacaoPerfilFundoCollection entidadeClassificacaoPerfilFundoCollection = new EntidadeClassificacaoPerfilFundoCollection();
                    entidadeClassificacaoPerfilFundoCollection.Query.Where(entidadeClassificacaoPerfilFundoCollection.Query.IdEntidadePerfilFundo.Equal(idEntidade));
                    if (entidadeClassificacaoPerfilFundoCollection.Query.Load())
                    {
                        throw new Exception("Entidade " + entidadePerfilFundo.Codigo + " não pode ser excluída por possuir classificações de fundos associadas.");
                    }

                    PerfilClassificacaoFundoEntidadeCollection perfilClassificacaoFundoEntidadeCollection = new PerfilClassificacaoFundoEntidadeCollection();
                    perfilClassificacaoFundoEntidadeCollection.Query.Where(perfilClassificacaoFundoEntidadeCollection.Query.IdEntidade.Equal(idEntidade));
                    if (perfilClassificacaoFundoEntidadeCollection.Query.Load())
                    {
                        throw new Exception("Entidade " + entidadePerfilFundo.Codigo + " não pode ser excluída por possuir perfis de classificações de fundos associadas.");
                    }

                    EntidadePerfilFundo entidadePerfilFundoClone = (EntidadePerfilFundo)Utilitario.Clone(entidadePerfilFundo);

                    entidadePerfilFundoClone.MarkAsDeleted();
                    entidadePerfilFundoClone.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de EntidadePerfilFundo - Operacao: Delete EntidadePerfilFundo: " + idEntidade + UtilitarioWeb.ToString(entidadePerfilFundo),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }
}