﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using Financial.Bolsa;
using Financial.RendaFixa;
using Financial.Bolsa.Enums;
using Financial.Common.Enums;
using Financial.Web.Common;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_Feriado : CadastroBasePage
{

    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasFiltro = true;
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSFeriado_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        FeriadoCollection coll = new FeriadoCollection();

        ASPxDateEdit textDataInicio = popupFiltro.FindControl("textDataInicio") as ASPxDateEdit;
        ASPxDateEdit textDataFim = popupFiltro.FindControl("textDataFim") as ASPxDateEdit;

        if (textDataInicio.Text != "")
        {
            coll.Query.Where(coll.Query.Data.GreaterThanOrEqual(textDataInicio.Text));
        }

        if (textDataFim.Text != "")
        {
            coll.Query.Where(coll.Query.Data.LessThanOrEqual(textDataFim.Text));
        }

        coll.Query.OrderBy(coll.Query.Data.Descending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSLocalFeriado_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        LocalFeriadoCollection coll = new LocalFeriadoCollection();
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropAno_Load(object sender, EventArgs e)
    {
        int ano = 1990;

        for (int i = 0; i < 100; i++)
        {
            (sender as ASPxComboBox).Items.Add(ano.ToString());
            ano += 1;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textData_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as ASPxDateEdit).Enabled = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropLocal_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as ASPxComboBox).Enabled = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackLista_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        if (dropAno.SelectedIndex != -1)
        {
            int ano = Convert.ToInt32(dropAno.Text);

            //Feriados móveis nacionais
            DateTime dtPascoa = Calendario.DataPascoa(ano);
            DateTime dtCarnaval = Calendario.DataCarnaval(ano);
            DateTime dtCorpusChristi = Calendario.DataCorpusChristi(ano);
            DateTime dtSextaSanta = Calendario.DataSextaSanta(ano);

            Feriado feriado = new Feriado();
            if (!feriado.LoadByPrimaryKey(dtPascoa, (short)LocalFeriadoFixo.Brasil))
            {
                feriado.Data = dtPascoa;
                feriado.IdLocal = (short)LocalFeriadoFixo.Brasil;
                feriado.Descricao = "Páscoa";
                feriado.Save();
            }

            feriado = new Feriado();
            if (!feriado.LoadByPrimaryKey(dtCarnaval, (short)LocalFeriadoFixo.Brasil))
            {
                feriado.Data = dtCarnaval;
                feriado.IdLocal = (short)LocalFeriadoFixo.Brasil;
                feriado.Descricao = "Carnaval";
                feriado.Save();
            }

            feriado = new Feriado();
            DateTime dtCarnavalAnterior = dtCarnaval.AddDays(-1);
            if (!Calendario.IsFinalSemana(dtCarnavalAnterior) && !feriado.LoadByPrimaryKey(dtCarnavalAnterior, (short)LocalFeriadoFixo.Brasil))
            {
                feriado.Data = dtCarnavalAnterior;
                feriado.IdLocal = (short)LocalFeriadoFixo.Brasil;
                feriado.Descricao = "Carnaval";
                feriado.Save();
            }

            feriado = new Feriado();
            if (!feriado.LoadByPrimaryKey(dtCorpusChristi, (short)LocalFeriadoFixo.Brasil))
            {
                feriado.Data = dtCorpusChristi;
                feriado.IdLocal = (short)LocalFeriadoFixo.Brasil;
                feriado.Descricao = "Corpus Christi";
                feriado.Save();
            }

            feriado = new Feriado();
            if (!feriado.LoadByPrimaryKey(dtSextaSanta, (short)LocalFeriadoFixo.Brasil))
            {
                feriado.Data = dtSextaSanta;
                feriado.IdLocal = (short)LocalFeriadoFixo.Brasil;
                feriado.Descricao = "Sexta Feira Santa";
                feriado.Save();
            }
            //

            //Feriados Fixos Nacionais
            DateTime dataFeriado = new DateTime(ano, 01, 01);
            feriado = new Feriado();
            if (!feriado.LoadByPrimaryKey(dataFeriado, (short)LocalFeriadoFixo.Brasil))
            {
                feriado.Data = dataFeriado;
                feriado.IdLocal = (short)LocalFeriadoFixo.Brasil;
                feriado.Descricao = "Ano Novo";
                feriado.Save();
            }

            dataFeriado = new DateTime(ano, 04, 21);
            feriado = new Feriado();
            if (!feriado.LoadByPrimaryKey(dataFeriado, (short)LocalFeriadoFixo.Brasil))
            {
                feriado.Data = dataFeriado;
                feriado.IdLocal = (short)LocalFeriadoFixo.Brasil;
                feriado.Descricao = "Tiradentes";
                feriado.Save();
            }

            dataFeriado = new DateTime(ano, 05, 01);
            feriado = new Feriado();
            if (!feriado.LoadByPrimaryKey(dataFeriado, (short)LocalFeriadoFixo.Brasil))
            {
                feriado.Data = dataFeriado;
                feriado.IdLocal = (short)LocalFeriadoFixo.Brasil;
                feriado.Descricao = "Dia do Trabalho";
                feriado.Save();
            }

            dataFeriado = new DateTime(ano, 09, 07);
            feriado = new Feriado();
            if (!feriado.LoadByPrimaryKey(dataFeriado, (short)LocalFeriadoFixo.Brasil))
            {
                feriado.Data = dataFeriado;
                feriado.IdLocal = (short)LocalFeriadoFixo.Brasil;
                feriado.Descricao = "Dia da Independência";
                feriado.Save();
            }

            dataFeriado = new DateTime(ano, 10, 12);
            feriado = new Feriado();
            if (!feriado.LoadByPrimaryKey(dataFeriado, (short)LocalFeriadoFixo.Brasil))
            {
                feriado.Data = dataFeriado;
                feriado.IdLocal = (short)LocalFeriadoFixo.Brasil;
                feriado.Descricao = "Nossa Senhora Aparecida";
                feriado.Save();
            }

            dataFeriado = new DateTime(ano, 11, 02);
            feriado = new Feriado();
            if (!feriado.LoadByPrimaryKey(dataFeriado, (short)LocalFeriadoFixo.Brasil))
            {
                feriado.Data = dataFeriado;
                feriado.IdLocal = (short)LocalFeriadoFixo.Brasil;
                feriado.Descricao = "Finados";
                feriado.Save();
            }

            dataFeriado = new DateTime(ano, 11, 15);
            feriado = new Feriado();
            if (!feriado.LoadByPrimaryKey(dataFeriado, (short)LocalFeriadoFixo.Brasil))
            {
                feriado.Data = dataFeriado;
                feriado.IdLocal = (short)LocalFeriadoFixo.Brasil;
                feriado.Descricao = "Proclamação da República";
                feriado.Save();
            }

            dataFeriado = new DateTime(ano, 12, 25);
            feriado = new Feriado();
            if (!feriado.LoadByPrimaryKey(dataFeriado, (short)LocalFeriadoFixo.Brasil))
            {
                feriado.Data = dataFeriado;
                feriado.IdLocal = (short)LocalFeriadoFixo.Brasil;
                feriado.Descricao = "Natal";
                feriado.Save();
            }
            //

            //Feriados fixos Regionais
            dataFeriado = new DateTime(ano, 12, 31);
            feriado = new Feriado();
            if (!feriado.LoadByPrimaryKey(dataFeriado, (short)LocalFeriadoFixo.Bovespa))
            {
                feriado.Data = dataFeriado;
                feriado.IdLocal = (short)LocalFeriadoFixo.Bovespa;
                feriado.Descricao = "Ano Novo - SP";
                feriado.Save();
            }


            if (ano >= 2004)
            {
                dataFeriado = new DateTime(ano, 11, 20);
                feriado = new Feriado();
                if (!feriado.LoadByPrimaryKey(dataFeriado, (short)LocalFeriadoFixo.Bovespa))
                {
                    feriado.Data = dataFeriado;
                    feriado.IdLocal = (short)LocalFeriadoFixo.Bovespa;
                    feriado.Descricao = "Dia da Consciência Negra";
                    feriado.Save();
                }
            }

            dataFeriado = new DateTime(ano, 12, 24);
            feriado = new Feriado();
            if (!feriado.LoadByPrimaryKey(dataFeriado, (short)LocalFeriadoFixo.Bovespa))
            {
                feriado.Data = dataFeriado;
                feriado.IdLocal = (short)LocalFeriadoFixo.Bovespa;
                feriado.Descricao = "Natal - SP";
                feriado.Save();
            }

            dataFeriado = new DateTime(ano, 01, 25);
            feriado = new Feriado();
            if (!feriado.LoadByPrimaryKey(dataFeriado, (short)LocalFeriadoFixo.Bovespa))
            {
                feriado.Data = dataFeriado;
                feriado.IdLocal = (short)LocalFeriadoFixo.Bovespa;
                feriado.Descricao = "Aniversário de SP";
                feriado.Save();
            }

            dataFeriado = new DateTime(ano, 07, 09);
            feriado = new Feriado();
            if (!feriado.LoadByPrimaryKey(dataFeriado, (short)LocalFeriadoFixo.Bovespa))
            {
                feriado.Data = dataFeriado;
                feriado.IdLocal = (short)LocalFeriadoFixo.Bovespa;
                feriado.Descricao = "Revolução de 1932";
                feriado.Save();
            }

            //

            e.Result = "Lista gerada com sucesso.";
        }
        else
        {
            e.Result = "Ano precisa ser escolhido!";
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
        ASPxComboBox dropLocal = gridCadastro.FindEditFormTemplateControl("dropLocal") as ASPxComboBox;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(dropLocal);
        controles.Add(textData);
        controles.Add(textDescricao);

        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        if (gridCadastro.IsNewRowEditing)
        {
            short idLocal = Convert.ToInt16(dropLocal.SelectedItem.Value);
            DateTime data = Convert.ToDateTime(textData.Text);

            Feriado feriado = new Feriado();
            if (feriado.LoadByPrimaryKey(data, idLocal))
            {
                e.Result = "Registro já existente";
            }
        }
    }
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();
    }
    private void SalvarNovo()
    {
        Feriado feriado = new Feriado();

        ASPxComboBox dropLocal = gridCadastro.FindEditFormTemplateControl("dropLocal") as ASPxComboBox;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;

        feriado.Data = Convert.ToDateTime(textData.Text);
        feriado.IdLocal = Convert.ToInt16(dropLocal.SelectedItem.Value);
        feriado.Descricao = textDescricao.Text;
        feriado.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de Feriado - Operacao: Insert Feriado: " + feriado.Data + "; " + feriado.IdLocal + UtilitarioWeb.ToString(feriado),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        if (feriado.IdLocal.Value == (short)LocalFeriadoFixo.Bovespa)
        {
            Feriado feriadoExiste = new Feriado();
            if (!feriadoExiste.LoadByPrimaryKey(Convert.ToDateTime(textData.Text), (short)LocalFeriadoFixo.Bovespa))
            {
                Feriado feriadoInsert = new Feriado();
                feriadoInsert.Data = Convert.ToDateTime(textData.Text);
                feriadoInsert.IdLocal = (short)LocalFeriadoFixo.Bovespa;
                feriadoInsert.Descricao = textDescricao.Text;
                feriadoInsert.Save();
            }
        }
        else if (feriado.IdLocal.Value == (short)LocalFeriadoFixo.Bovespa)
        {
            Feriado feriadoExiste = new Feriado();
            if (!feriadoExiste.LoadByPrimaryKey(Convert.ToDateTime(textData.Text), (short)LocalFeriadoFixo.Bovespa))
            {
                Feriado feriadoInsert = new Feriado();
                feriadoInsert.Data = Convert.ToDateTime(textData.Text);
                feriadoInsert.IdLocal = (short)LocalFeriadoFixo.Bovespa;
                feriadoInsert.Descricao = textDescricao.Text;
                feriadoInsert.Save();
            }
        }

        Feriado.limpaCache();
    }

    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "CompositeKey")
        {
            //Compose a primary key value
            string data = Convert.ToString(e.GetListSourceFieldValue(FeriadoMetadata.ColumnNames.Data));
            string idLocal = Convert.ToString(e.GetListSourceFieldValue(FeriadoMetadata.ColumnNames.IdLocal));
            e.Value = data + idLocal;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();

        Financial.Web.Util.ErrorHandler.RestartWebApplication();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        Feriado feriado = new Feriado();

        ASPxComboBox dropLocal = gridCadastro.FindEditFormTemplateControl("dropLocal") as ASPxComboBox;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;

        short idLocal = Convert.ToInt16(dropLocal.SelectedItem.Value);
        DateTime data = Convert.ToDateTime(textData.Text);
        string descricao = Convert.ToString(textDescricao.Text);
        if (feriado.LoadByPrimaryKey(data, idLocal))
        {
            feriado.Descricao = descricao;
            feriado.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de Feriado - Operacao: Update Feriado: " + feriado.Data + "; " + feriado.IdLocal + UtilitarioWeb.ToString(feriado),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        Feriado.limpaCache();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        bool restart = false;
        if (e.Parameters == "btnDelete")
        {
            //
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(FeriadoMetadata.ColumnNames.IdLocal);
            List<object> keyValuesData = gridCadastro.GetSelectedFieldValues(FeriadoMetadata.ColumnNames.Data);
            //
            for (int i = 0; i < keyValuesData.Count; i++)
            {
                int idLocal = Convert.ToInt32(keyValuesId[i]);
                DateTime data = Convert.ToDateTime(keyValuesData[i]);

                Feriado feriado = new Feriado();
                if (feriado.LoadByPrimaryKey(data, (short)idLocal))
                {
                    //
                    Feriado feriadoClone = (Feriado)Utilitario.Clone(feriado);
                    //
                    feriado.MarkAsDeleted();
                    feriado.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de Feriado - Operacao: Delete Feriado: " + feriadoClone.Data + "; " + feriadoClone.IdLocal + UtilitarioWeb.ToString(feriadoClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion

                    restart = true;
                }
            }
        }
        Feriado.limpaCache();
        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();

        if (restart)
        {
            Financial.Web.Util.ErrorHandler.RestartWebApplication();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e)
    {
        this.FocaCampoGrid("textData", "textDescricao");
        base.gridCadastro_PreRender(sender, e);

        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        Label labelFiltroHeader = gridCadastro.FindStatusBarTemplateControl("labelFiltroHeader") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");
        if (textDataInicio.Text != "")
        {
            texto.Append(" Data Início >= ").Append(textDataInicio.Text.Substring(0, 10).Trim());
        }
        if (textDataFim.Text != "")
        {
            if (textDataInicio.Text != "")
                texto.Append(" |");

            texto.Append(" Data Fim <= ").Append(textDataFim.Text.Substring(0, 10).Trim());
        }

        int numeroDias = 0;
        if (textDataInicio.Text != "" && textDataFim.Text != "")
        {
            if (Convert.ToDateTime(textDataInicio.Text) <= Convert.ToDateTime(textDataFim.Text))
            {
                numeroDias = Calendario.NumeroDias(Convert.ToDateTime(textDataInicio.Text), Convert.ToDateTime(textDataFim.Text), (byte)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            }
        }


        string textoDias = "";
        if (numeroDias != 0)
        {
            textoDias = " Número de dias úteis entre as datas: " + numeroDias.ToString();
        }
        labelFiltro.Text = texto.ToString() + "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" + textoDias;

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e)
    {
        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing)
        {
            bool newRow = false;
            if (gridView.IsNewRowEditing)
            {
                newRow = true;
            }
            e.Properties["cpNewRow"] = newRow;

            ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
            e.Properties["cpTextData"] = textData.ClientID;
        }
    }
}