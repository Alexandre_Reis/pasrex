﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using Financial.Investidor;
using Financial.ContaCorrente;
using System.Drawing;
using Financial.ContaCorrente.Enums;
using Financial.Security;
using Financial.Fundo;
using Financial.Bolsa;
using Financial.BMF;
using Financial.RendaFixa;
using Financial.Investidor.Enums;
using Financial.InvestidorCotista;
using Financial.CRM;
using Financial.WebConfigConfiguration;
using Financial.Security.Enums;
using System.Collections.Specialized;
using EntitySpaces.Interfaces;
using Financial.Web.Util;
using Financial.InvestidorCotista.Enums;
using Financial.Web.Common;
using System.Web.Configuration;
using System.Xml;
using Financial.Export;
using Financial.Interfaces.Export;
using DevExpress.Web.Data;
using System.ComponentModel;
using Financial.Fundo.Enums;
using DevExpress.XtraGrid;
using Financial.Common.Enums;

public partial class CadastrosBasicos_PadraoNotas : CadastroBasePage
{
    #region Instância
    //Collection
    AgenciaClassificadoraCollection agenciaClassificadoraCollection = new AgenciaClassificadoraCollection();
    PadraoNotasCollection padraoNotasCollection = new PadraoNotasCollection();
    //Entidade
    PadraoNotas padraoNotas = new PadraoNotas();
    #endregion

    #region esDataSources
    protected void EsDSAgenciaClassificadora_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        agenciaClassificadoraCollection = new AgenciaClassificadoraCollection();

        agenciaClassificadoraCollection.Query.Select(agenciaClassificadoraCollection.Query.IdAgenciaClassificadora, (agenciaClassificadoraCollection.Query.CodigoAgencia.Cast(esCastType.String) + "-" + agenciaClassificadoraCollection.Query.DescricaoAgencia).As("DescricaoAgencia"));
        agenciaClassificadoraCollection.Query.OrderBy(agenciaClassificadoraCollection.Query.IdAgenciaClassificadora.Ascending);
        agenciaClassificadoraCollection.Query.Load();

        e.Collection = agenciaClassificadoraCollection;
    }
    protected void EsDSPadraoNotas_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        padraoNotasCollection = new PadraoNotasCollection();

        padraoNotasCollection.Query.OrderBy(padraoNotasCollection.Query.IdAgencia.Ascending);
        padraoNotasCollection.Query.OrderBy(padraoNotasCollection.Query.Sequencia.Ascending);
        padraoNotasCollection.Query.Load();

        e.Collection = padraoNotasCollection;
    }
    #endregion

    #region gridCadastro
    /// <summary>
    /// Carrega os filtros da grid
    /// </summary>
    protected void gridCadastro_OnPreRender(object sender, EventArgs e)
    {
        #region Item Avaliado
        //Busca o controle de filtro da coluna CADASTRO da grid
        GridViewDataComboBoxColumn itemAvaliado = gridCadastro.Columns["ItemAvaliado"] as GridViewDataComboBoxColumn;
        //Adiciona registro vazio para limpar o filtro
        itemAvaliado.PropertiesComboBox.Items.Add(string.Empty);
        //Percorre o ENUM para preencher o controle do combobox
        foreach (int r in Enum.GetValues(typeof(ListaItemAvaliado)))
        {
            itemAvaliado.PropertiesComboBox.Items.Add(ListaItemAvaliadoDescricao.RetornaStringValue(r), r.ToString());
        }
        #endregion
    }

    /// <summary>
    /// Efetua a alteração dos registros
    /// </summary>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        int idPadraoNotas = (int)e.Keys[0];
        if (padraoNotas.LoadByPrimaryKey(idPadraoNotas))
        {
            //Preenche dinamicamente a entidade
            padraoNotas = Financial.Util.CadastroDinamico.PreencheEntidade<PadraoNotas>(gridCadastro.FindEditFormTemplateControl("panelEdicao").Controls, padraoNotas);
            padraoNotas.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de Padrão Notas - Operacao: Update Padrão Notas: " + padraoNotas.IdPadraoNotas + UtilitarioWeb.ToString(padraoNotas),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// Efetua o cadastro 
    /// </summary>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        this.Cadastra();

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// Fecha o EditForm e atualiza a grid
    /// Evento é chamado quando o usuário clica no botão para fechar a grid
    /// </summary>
    protected void gridCadastro_CancelRowEditing(object sender, ASPxStartRowEditingEventArgs e)
    {
        e.Cancel = false;
        gridCadastro.DataBind();
    }
    #endregion

    #region CallBack
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdPadraoNotas");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                padraoNotas = new PadraoNotas();
                int idPadraoNotas = Convert.ToInt32(keyValuesId[i]);

                if (padraoNotas.LoadByPrimaryKey(idPadraoNotas))
                {
                    PadraoNotas padraoNotasClone = (PadraoNotas)Utilitario.Clone(padraoNotas);

                    padraoNotas.MarkAsDeleted();
                    padraoNotas.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de Padrão Notas - Operacao: Delete Padrão Notas: " + idPadraoNotas + UtilitarioWeb.ToString(padraoNotasClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
        
        //ASPxComboBox dropIdAgencia = gridCadastro.FindEditFormTemplateControl("dropIdAgencia") as ASPxComboBox;
        //ASPxSpinEdit textSequencia = gridCadastro.FindEditFormTemplateControl("textSequencia") as ASPxSpinEdit;

        ////Verifica se o registro já existe
        //if (dropIdAgencia != null && dropIdAgencia.SelectedItem != null)
        //{
        //    padraoNotasCollection = new PadraoNotasCollection();
        //    padraoNotasCollection.Query.Select(padraoNotasCollection.Query.Sequencia);
        //    padraoNotasCollection.Query.Where(padraoNotasCollection.Query.IdAgencia == Convert.ToInt32(dropIdAgencia.SelectedItem.Value));
        //    padraoNotasCollection.LoadAll();

        //    foreach (PadraoNotas item in padraoNotasCollection)
        //    {
        //        if (!string.IsNullOrEmpty(textSequencia.Text))
        //        {
        //            if (item.Sequencia.Equals(Convert.ToInt32(textSequencia.Text)))
        //            {
        //                e.Result = "cadastroSequencia";
        //            }
        //        }
        //    }   
        //}
    }

    /// <summary>
    /// 
    /// </summary>
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        this.Cadastra();
    }
    #endregion

    #region Button
    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }
    #endregion

    #region OnLoad
    /// <summary>
    /// Carrega os valores do drop
    /// </summary>
    protected void dropItemAvaliado_OnLoad(object sender, EventArgs e)
    {
        //Busca o controle de filtro da coluna CADASTRO da grid
        ASPxComboBox dropItemAvaliado = (ASPxComboBox)sender;
        //Percorre o ENUM para preencher o controle do combobox
        foreach (int r in Enum.GetValues(typeof(ListaItemAvaliado)))
        {
            dropItemAvaliado.Items.Add(ListaItemAvaliadoDescricao.RetornaStringValue(r), r.ToString());
        }
    }
    #endregion

    #region Suporte
    private void Cadastra()
    {
        //Preenche dinamicamente a entidade
        padraoNotas = Financial.Util.CadastroDinamico.PreencheEntidade<PadraoNotas>(gridCadastro.FindEditFormTemplateControl("panelEdicao").Controls, null);
        padraoNotas.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro Padrão Nota - Operacao: Insert Padrão Notas: " + padraoNotas.IdPadraoNotas + UtilitarioWeb.ToString(padraoNotas),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }
    #endregion

}