﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using DevExpress.Web;
using Financial.RendaFixa;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Util;
using System.Drawing;
using Financial.Web.Common;
using Financial.Common;

public partial class CadastrosBasicos_Feeder : CadastroBasePage
{
    new protected void Page_Load(object sender, EventArgs e)
    {
        base.Page_Load(sender, e);
    }

    new protected void gridCadastro_PreRender(object sender, EventArgs e)
    {
        this.FocaCampoGrid("textDescricao");
        base.gridCadastro_PreRender(sender, e);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSFeeder_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        FeederCollection coll = new FeederCollection();

        coll.Query.OrderBy(coll.Query.IdFeeder.Ascending);
        coll.LoadAll();

        e.Collection = coll;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        Feeder feeder = new Feeder();

        int IdFeeder = (int)e.Keys[0];

        if (feeder.LoadByPrimaryKey(IdFeeder))
        {
            ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
            ASPxTextBox textURL = gridCadastro.FindEditFormTemplateControl("textURL") as ASPxTextBox;
            ASPxTextBox textLocalJarFile = gridCadastro.FindEditFormTemplateControl("textLocalJarFile") as ASPxTextBox;
            ASPxTextBox textLocalDados = gridCadastro.FindEditFormTemplateControl("textLocalDados") as ASPxTextBox;

            feeder.Descricao = textDescricao.Text;
            feeder.Url = textURL.Text;
            feeder.LocalJarFile = textLocalJarFile.Text;
            feeder.LocalDados = textLocalDados.Text;
            feeder.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de Feeder - Operacao: Update Feeder: " + IdFeeder + UtilitarioWeb.ToString(feeder),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdFeeder");
            for (int i = 0; i < keyValuesId.Count; i++)
            {                
                Feeder feeder = new Feeder();
                int IdFeeder = Convert.ToInt32(keyValuesId[i]);
                
                if (feeder.LoadByPrimaryKey(IdFeeder))
                {

                    #region Verifica se Existe registro relacionado
                    SerieRendaFixaCollection serieRendaFixaCollection = new SerieRendaFixaCollection();
                    bool achouRegistro = false;

                    serieRendaFixaCollection.Query.Where(serieRendaFixaCollection.Query.IdFeeder == IdFeeder);
                    serieRendaFixaCollection.Load(serieRendaFixaCollection.Query);
                    achouRegistro = (serieRendaFixaCollection.Count > 0);

                    if (achouRegistro)
                    {
                        throw new Exception("Feeder " + feeder.Descricao.Trim() + " não pode ser excluído por ter cadastros relacionados.");
                    }

                    #endregion
                    Feeder feederClone = (Feeder)Utilitario.Clone(feeder);
                    //
                    feeder.MarkAsDeleted();
                    feeder.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de Feeder - Operacao: Delete Feeder: " + IdFeeder + UtilitarioWeb.ToString(feederClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        base.gridCadastro_CellEditorInitialize(sender, e);

        if (!gridCadastro.IsNewRowEditing)
        {
            if (e.Column.FieldName == FeederMetadata.ColumnNames.IdFeeder)
            {
                e.Editor.BackColor = Color.FromName("#EBEBEB");
                e.Editor.ReadOnly = true;
            }
        }
        else
        { // Modo Novo
            if (e.Column.FieldName == FeederMetadata.ColumnNames.IdFeeder)
            {
                e.Editor.BackColor = Color.FromName("#EBEBEB");
                e.Editor.ReadOnly = true;
            }
        }
    }

    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    private void SalvarNovo()
    {
        ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
        ASPxTextBox textURL = gridCadastro.FindEditFormTemplateControl("textURL") as ASPxTextBox;
        ASPxTextBox textLocalJarFile = gridCadastro.FindEditFormTemplateControl("textLocalJarFile") as ASPxTextBox;
        ASPxTextBox textLocalDados = gridCadastro.FindEditFormTemplateControl("textLocalDados") as ASPxTextBox;

        Feeder feeder = new Feeder();

        feeder.Descricao = textDescricao.Text;
        feeder.Url = textURL.Text;
        feeder.LocalJarFile = textLocalJarFile.Text;
        feeder.LocalDados = textLocalDados.Text;

        feeder.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de Feeder - Operacao: Insert Feeder: " + feeder.IdFeeder + UtilitarioWeb.ToString(feeder),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
        ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
        ASPxTextBox textURL = gridCadastro.FindEditFormTemplateControl("textURL") as ASPxTextBox;
        ASPxTextBox textLocalJarFile = gridCadastro.FindEditFormTemplateControl("textLocalJarFile") as ASPxTextBox;
        ASPxTextBox textLocalDados = gridCadastro.FindEditFormTemplateControl("textLocalDados") as ASPxTextBox;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(textDescricao);

        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion
    }

    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();

        e.Result = "";
        if (e.Parameter == "clone")
        {
            //e.Result = 0;
        }
        else
        {
            e.Result = "Cadastro feito com sucesso.";
        }
    }
}