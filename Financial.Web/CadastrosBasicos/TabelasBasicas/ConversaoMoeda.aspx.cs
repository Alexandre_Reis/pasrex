﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Common.Enums;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_ConversaoMoeda : Financial.Web.Common.CadastroBasePage {
    #region DataSources
    protected void EsDSConversaoMoeda_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ConversaoMoedaCollection coll = new ConversaoMoedaCollection();

        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSMoeda_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        MoedaCollection coll = new MoedaCollection();

        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSIndice_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        IndiceCollection coll = new IndiceCollection();

        coll.Query.Where(coll.Query.Tipo == (byte)TipoIndice.Decimal);
        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) {
        if (e.Column.FieldName == "CompositeKey") {
            //Compose a primary key value
            string idMoedaDe = Convert.ToString(e.GetListSourceFieldValue(ConversaoMoedaMetadata.ColumnNames.IdMoedaDe));
            string idMoedaPara = Convert.ToString(e.GetListSourceFieldValue(ConversaoMoedaMetadata.ColumnNames.IdMoedaPara));
            e.Value = idMoedaDe + "|" + idMoedaPara;
        }
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        ConversaoMoeda conversaoMoeda = new ConversaoMoeda();

        string chave = (string)e.Keys[0];
        int idMoedaDe = Convert.ToInt32(chave.Split('|')[0]);
        int idMoedaPara = Convert.ToInt32(chave.Split('|')[1]);

        if (conversaoMoeda.LoadByPrimaryKey((short)idMoedaDe, (short)idMoedaPara)) {
            //
            conversaoMoeda.IdIndice = Convert.ToInt16(e.NewValues[ConversaoMoedaMetadata.ColumnNames.IdIndice]);
            conversaoMoeda.Tipo = Convert.ToByte(e.NewValues[ConversaoMoedaMetadata.ColumnNames.Tipo]);
            //
            conversaoMoeda.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de ConversaoMoeda - Operacao: Update ConversaoMoeda: " + idMoedaDe + "; " + idMoedaPara + UtilitarioWeb.ToString(conversaoMoeda),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        ConversaoMoeda conversaoMoeda = new ConversaoMoeda();

        conversaoMoeda.IdMoedaDe = Convert.ToInt16(e.NewValues[ConversaoMoedaMetadata.ColumnNames.IdMoedaDe]);
        conversaoMoeda.IdMoedaPara = Convert.ToInt16(e.NewValues[ConversaoMoedaMetadata.ColumnNames.IdMoedaPara]);
        conversaoMoeda.IdIndice = Convert.ToInt16(e.NewValues[ConversaoMoedaMetadata.ColumnNames.IdIndice]);
        conversaoMoeda.Tipo = Convert.ToByte(e.NewValues[ConversaoMoedaMetadata.ColumnNames.Tipo]);

        ConversaoMoeda conversaoMoedaAux = new ConversaoMoeda();
        if (!conversaoMoedaAux.LoadByPrimaryKey(conversaoMoeda.IdMoedaDe.Value, conversaoMoeda.IdMoedaPara.Value)) {
            conversaoMoeda.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de ConversaoMoeda - Operacao: Insert ConversaoMoeda: " + conversaoMoeda.IdMoedaDe + "; " + conversaoMoeda.IdMoedaPara + UtilitarioWeb.ToString(conversaoMoeda),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }
        else {
            throw new Exception("Registro já Existente");
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesIdMoedaDe = gridCadastro.GetSelectedFieldValues(ConversaoMoedaMetadata.ColumnNames.IdMoedaDe);
            List<object> keyValuesIdMoedaPara = gridCadastro.GetSelectedFieldValues(ConversaoMoedaMetadata.ColumnNames.IdMoedaPara);
            for (int i = 0; i < keyValuesIdMoedaDe.Count; i++) {
                short idMoedaDe = Convert.ToInt16(keyValuesIdMoedaDe[i]);
                short idMoedaPara = Convert.ToInt16(keyValuesIdMoedaPara[i]);

                ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
                if (conversaoMoeda.LoadByPrimaryKey(idMoedaDe, idMoedaPara)) {
                    //
                    ConversaoMoeda conversaoMoedaClone = (ConversaoMoeda)Utilitario.Clone(conversaoMoeda);
                    //
                    conversaoMoeda.MarkAsDeleted();
                    conversaoMoeda.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de ConversaoMoeda - Operacao: Delete ConversaoMoeda: " + idMoedaDe + "; " + idMoedaPara + UtilitarioWeb.ToString(conversaoMoedaClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    new protected void gridCadastro_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e) {
        base.gridCadastro_CellEditorInitialize(sender, e);

        if (!gridCadastro.IsNewRowEditing) {
            if (e.Column.FieldName == ConversaoMoedaMetadata.ColumnNames.IdMoedaDe || e.Column.FieldName == ConversaoMoedaMetadata.ColumnNames.IdMoedaPara) {
                //e.Editor.Enabled = false;
                e.Editor.BackColor = Color.FromName("#EBEBEB");
                e.Editor.ReadOnly = true;
            }
        }
    }
}