﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CotacaoBolsa.aspx.cs" Inherits="CadastrosBasicos_CotacaoBolsa" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>    

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    
    
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>    
    <script type="text/javascript" language="Javascript">
    var popup = true;
    document.onkeydown=onDocumentKeyDownWithCallback;
    var operacao = '';
    
    function OnGetDataAtivoBolsa(data) {
        btnEditAtivoBolsa.SetValue(data);
        popupAtivoBolsa.HideWindow();
        ASPxCallback1.SendCallback(btnEditAtivoBolsa.GetValue());
        btnEditAtivoBolsa.Focus();
    }    
    
    function OnButtonClick_Importacao() {
    popupImportacao.ShowWindow(); 
    return false;        
}
    </script>
</head>

<body>
    <form id="form1" runat="server">
    
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" />
        
    <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {
            if (e.result == '' && btnEditAtivoBolsa.GetValue() != null)
            {
                popupMensagemAtivoBolsa.SetContentHTML(document.getElementById('textMsgNaoExiste').value);
                popupMensagemAtivoBolsa.ShowAtElementByID(btnEditAtivoBolsa.name);
                btnEditAtivoBolsa.SetValue(''); 
                btnEditAtivoBolsa.Focus();                 
            }            
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new')
                {             
                    gridCadastro.UpdateEdit();
                }
                else
                {
                    callbackAdd.SendCallback();
                }                          
            }
            operacao = '';
        }        
        " />
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            alert('Operação feita com sucesso.');
        }        
        " />
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="callbackImportar" runat="server" OnCallback="callbackImportar_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            popupImportacao.Hide();
            if (e.result != '') {
                alert(e.result);
                gridCadastro.PerformCallback('btnRefresh');
            }
            else
            alert('Dados importados com sucesso.');
            gridCadastro.PerformCallback('btnRefresh');
        }        
        " />
    </dxcb:ASPxCallback>
        
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">
    
    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Cotações de Bolsa"></asp:Label>
    </div>
        
    <div id="mainContent">
                                        
        <dxpc:ASPxPopupControl ID="popupFiltro" AllowDragging="true" PopupElementID="popupFiltro" EnableClientSideAPI="True"                    
                        PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" 
                        Width="400" Left="250" Top="70" HeaderText="Filtros adicionais para consulta" runat="server"
                        HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">            
            <ContentCollection><dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                
                <table cellpadding="2" cellspacing="2">        
                <tr>
                <td>                
                    <asp:Label ID="labelDataInicio" runat="server" CssClass="labelNormal" Text="Início:"></asp:Label>
                </td>    
                                    
                <td>
                    <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" />    
                </td>  
                
                <td>
                    <asp:Label ID="labelDataFim" runat="server" CssClass="labelNormal" Text="Fim:"></asp:Label>
                </td>  
                
                <td >                
                    <dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim" />    
                </td>                                                                            
                </tr>
                
                <tr>
                <td>
                    <asp:Label ID="labelCdAtivo" runat="server" CssClass="labelNormal" Text="Ativo:"></asp:Label>
                </td>                
                <td colspan="3">
                    <asp:TextBox ID="textCdAtivo" runat="server" CssClass="textNormal"></asp:TextBox>
                </td>    
                </tr>
                </table>        
                
                <div class="linkButton linkButtonNoBorder" style="margin-top:20px">              
                <asp:LinkButton ID="btnOKFilter" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnOK" OnClientClick="popupFiltro.Hide(); gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal7" runat="server" Text="Aplicar"/><div></div></asp:LinkButton>
                <asp:LinkButton ID="btnFilterCancel" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnFilterCancel" OnClientClick="OnClearFilterClick_FiltroDatas(); return false;"><asp:Literal ID="Literal8" runat="server" Text="Limpar"/><div></div></asp:LinkButton>
                </div>                    
            </dxpc:PopupControlContentControl></ContentCollection>                             
        </dxpc:ASPxPopupControl>        
        
        <dxpc:ASPxPopupControl ID="popupImportacao" AllowDragging="true" PopupElementID="popupImportacao" EnableClientSideAPI="True"                    
                        PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" 
                        Width="400" Left="550" Top="70" HeaderText="Importa Cotação de Ativos" runat="server"
                        HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">            
            <ContentCollection><dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                
                <table cellpadding="2" cellspacing="2">
                <tr>
                <td colspan="4">
                    <asp:Label ID="label3" runat="server" CssClass="labelNormal" Text="Importa Ativos Separados por ponto e vírgula:"></asp:Label>
                </td> 
                </tr>
                <tr>
                <td colspan="4">
                    <asp:TextBox ID="textImportacao" TextMode="MultiLine" Rows="4" width="250" runat="server" CssClass="textNormal"></asp:TextBox>
                </td>    
                </tr>
                        
                <tr>
                <td>                
                    <asp:Label ID="label1" runat="server" CssClass="labelNormal" Text="Início:"></asp:Label>
                </td>    
                                    
                <td>
                    <dxe:ASPxDateEdit ID="textDataInicioImporta" runat="server" ClientInstanceName="textDataInicioImporta" />    
                </td>  
                
                <td>
                    <asp:Label ID="label2" runat="server" CssClass="labelNormal" Text="Fim:"></asp:Label>
                </td>  
                
                <td>                
                    <dxe:ASPxDateEdit ID="textDataFimImporta" runat="server" ClientInstanceName="textDataFimImporta" />    
                </td>                                                                            
                </tr>
                
                </table>        
                
                <div class="linkButton linkButtonNoBorder" style="margin-top:20px">              
                <asp:LinkButton ID="LinkButton1" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnOK" OnClientClick="callbackImportar.SendCallback(); return false;"><asp:Literal ID="Literal11" runat="server" Text="Importar"/><div></div></asp:LinkButton>
                <asp:LinkButton ID="LinkButton2" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnCancel" OnClientClick="popupImportacao.Hide(); return false;"><asp:Literal ID="Literal14" runat="server" Text="Cancelar"/><div></div></asp:LinkButton>
                </div>                    
            </dxpc:PopupControlContentControl></ContentCollection>                             
        </dxpc:ASPxPopupControl>   
        
        <div class="linkButton" >               
           <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal1" runat="server" Text="Novo"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;"><asp:Literal ID="Literal2" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnFilter" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnFilter" OnClientClick="return OnButtonClick_FiltroDatas()"><asp:Literal ID="Literal3" runat="server" Text="Filtro"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnCopy" runat="server" Font-Overline="false" CssClass="btnCopy" OnClientClick=" if (confirm('As cotações marcadas serão copiadas para o dia seguinte. Confirma?')==true) gridCadastro.PerformCallback('btnCopy');return false;"><asp:Literal ID="Literal15" runat="server" Text="Copiar"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal4" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal5" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>              
           <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal6" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnImportacao" runat="server" Font-Overline="false" CssClass="btnLote" OnClientClick="return OnButtonClick_Importacao()"><asp:Literal ID="Literal9" runat="server" Text="Importar"/><div></div></asp:LinkButton>
        </div>

        <div class="divDataGrid">            
            <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                        KeyFieldName="CompositeKey" DataSourceID="EsDSCotacaoBolsa"
                        OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData"                
                        OnRowUpdating="gridCadastro_RowUpdating"
                        OnRowInserting="gridCadastro_RowInserting"
                        OnCustomCallback="gridCadastro_CustomCallback"
                        OnPreRender="gridCadastro_PreRender"
                        OnBeforeGetCallbackResult="gridCadastro_PreRender"
                        OnCustomJSProperties="gridCadastro_CustomJSProperties"
                        >        
                            
                <Columns>           
                    <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                        <HeaderTemplate>
                            <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                        </HeaderTemplate>
                    </dxwgv:GridViewCommandColumn>
                                                    
                    <dxwgv:GridViewDataDateColumn FieldName="Data" Caption="Data Referência" VisibleIndex="1" Width="10%"/>    
                    
                    <dxwgv:GridViewDataTextColumn FieldName="CdAtivoBolsa" Caption="Código" VisibleIndex="2" Width="15%"/>
                    
                    <dxwgv:GridViewDataSpinEditColumn FieldName="PUMedio" Caption="PU Médio" VisibleIndex="3" Width="15%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">                                                                                                
                        <PropertiesSpinEdit DisplayFormatString="{0:#,##0.0000;(#,##0.0000);0.0000}"></PropertiesSpinEdit>
                    </dxwgv:GridViewDataSpinEditColumn>
                    
                    <dxwgv:GridViewDataSpinEditColumn FieldName="PUFechamento" Caption="PU Fechamento" VisibleIndex="4" Width="15%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">                                                                                                
                        <PropertiesSpinEdit DisplayFormatString="{0:#,##0.0000;(#,##0.0000);0.0000}"></PropertiesSpinEdit>
                    </dxwgv:GridViewDataSpinEditColumn>
                    
                    <dxwgv:GridViewDataSpinEditColumn FieldName="PUAbertura" Caption="PU Abertura" VisibleIndex="5" Width="15%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">                                                                                                
                        <PropertiesSpinEdit DisplayFormatString="{0:#,##0.0000;(#,##0.0000);0.0000}"></PropertiesSpinEdit>
                    </dxwgv:GridViewDataSpinEditColumn>
                    
                    <dxwgv:GridViewDataTextColumn FieldName="CompositeKey" UnboundType="String" Visible="False"/>
                </Columns>
                
                <Templates>
                    <EditForm>
                                                
                        <div class="editForm">        
                             
                             <table>                    
                                <tr>                                    
                                    <td class="td_Label_Curto">
                                        <asp:Label ID="labelData" runat="server" CssClass="labelRequired" Text="Data:"></asp:Label>
                                    </td>
                                                            
                                    <td>  
                                        <dxe:ASPxDateEdit ID="textData" runat="server" ClientInstanceName="textData" Value='<%#Eval("Data")%>' OnInit="textData_Init"/>
                                    </td>  
                                </tr>
                                    
                                <tr>                                        
                                    <td class="td_Label_Curto">
                                        <asp:Label ID="labelAtivo" runat="server" CssClass="labelRequired" Text="Ativo:"></asp:Label>                        
                                    </td>
                                    <td >
                                        <dxe:ASPxButtonEdit ID="btnEditAtivoBolsa" runat="server" Width="120%" CssClass="textAtivoCurto" MaxLength="25" 
                                                            ClientInstanceName="btnEditAtivoBolsa" Text="<%#Bind('CdAtivoBolsa')%>" OnLoad="btnEditAtivoBolsa_Load">            
                                        <Buttons>
                                            <dxe:EditButton></dxe:EditButton>                
                                        </Buttons>           
                                        <ClientSideEvents                              
                                             ButtonClick="function(s, e) {popupAtivoBolsa.ShowAtElementByID(s.name);}" 
                                             LostFocus="function(s, e) {popupMensagemAtivoBolsa.HideWindow();
                                                                        if (btnEditAtivoBolsa.GetValue() != null)
                                                                        {
                                                                            ASPxCallback1.SendCallback(btnEditAtivoBolsa.GetValue());
                                                                        }
                                                                        }"
                                        />                    
                                        </dxe:ASPxButtonEdit>
                                    </td>                                                               
                                </tr>                                        
                                
                                <tr>
                                    <td class="td_Label_Curto">
                                        <asp:Label ID="labelPUFechamento" runat="server" CssClass="labelRequired" Text="Fech:"></asp:Label>
                                    </td>
                                    <td>
                                        <dxe:ASPxSpinEdit ID="textPUFechamento" runat="server" ClientInstanceName="textPUFechamento" 
                                        Text='<%# Eval("PUFechamento") %>' NumberType="Float" MaxLength="25" DecimalPlaces="16" CssClass="textValor_4">
                                        </dxe:ASPxSpinEdit>
                                    </td>                                                        
                                </tr>
                                
                                <tr>
                                    <td class="td_Label_Curto">
                                        <asp:Label ID="labelPUMedio" runat="server" CssClass="labelNormal" Text="Médio:"></asp:Label>
                                    </td>
                                    <td>
                                        <dxe:ASPxSpinEdit ID="textPUMedio" runat="server" ClientInstanceName="textPUMedio" 
                                        Text='<%# Eval("PUMedio") %>' NumberType="Float" MaxLength="12" DecimalPlaces="4" CssClass="textValor_4">
                                        </dxe:ASPxSpinEdit>
                                    </td>                                                        
                                </tr>
                                
                                <tr>
                                    <td class="td_Label_Curto">
                                        <asp:Label ID="labelPUAbertura" runat="server" CssClass="labelNormal" Text="Abertura:"></asp:Label>
                                    </td>
                                    <td>
                                        <dxe:ASPxSpinEdit ID="textPUAbertura" runat="server" ClientInstanceName="textPUAbertura" 
                                        Text='<%# Eval("PUAbertura") %>' NumberType="Float" MaxLength="12" DecimalPlaces="4" CssClass="textValor_4">
                                        </dxe:ASPxSpinEdit>                                        
                                    </td>                                                        
                                </tr>
                                
                                <tr>
                                    <td class="td_Label_Curto">
                                        <asp:Label ID="labelPUGerencial" runat="server" CssClass="labelNormal" Text="Gerencial:"></asp:Label>
                                    </td>
                                    <td>
                                        <dxe:ASPxSpinEdit ID="textPUGerencial" runat="server" ClientInstanceName="textPUGerencial" 
                                        Text='<%# Eval("PUGerencial") %>' NumberType="Float" MaxLength="12" DecimalPlaces="4" CssClass="textValor_4"
                                        OnLoad="textPUGerencial_Load">
                                        </dxe:ASPxSpinEdit>                                        
                                    </td>                                                        
                                </tr>
                            </table>
                            
                            <div class="linhaH"></div>
                    
                            <div class="linkButton linkButtonNoBorder popupFooter">
                                <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd" OnInit="btnOKAdd_Init" 
                                                   OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal10" runat="server" Text="OK+"/><div></div></asp:LinkButton>        
                                <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                   OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal12" runat="server" Text="OK"/><div></div></asp:LinkButton>
                                <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel" OnClientClick="gridCadastro.CancelEdit(); return false;"><asp:Literal ID="Literal13" runat="server" Text="Cancelar"/><div></div></asp:LinkButton>
                            </div>    
                        </div>
                    </EditForm>
                    <StatusBar>
                        <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" />
                    </StatusBar>            
                </Templates>
                
                <SettingsPopup EditForm-Width="300px" />
                                
                <Templates>
                <StatusBar>
                    <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" />
                </StatusBar>            
                </Templates>
                <SettingsCommandButton>
                    <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                </SettingsCommandButton>                        
            </dxwgv:ASPxGridView>            
        </div>
             
    </div>
    </div>
    </td></tr></table>
    </div>
                       
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
        
    <cc1:esDataSource ID="EsDSCotacaoBolsa" runat="server" OnesSelect="EsDSCotacaoBolsa_esSelect" />
    <cc1:esDataSource ID="EsDSAtivoBolsa" runat="server" OnesSelect="EsDSAtivoBolsa_esSelect" />
        
    </form>
</body>
</html>