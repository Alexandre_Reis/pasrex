﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Bolsa;
using Financial.Web.Common;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_EspecificacaoBolsa : CadastroBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        base.Page_Load(sender, e);

        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
                                  new List<string>(new string[] { EspecificacaoBolsaMetadata.ColumnNames.Tipo }));
    }

    #region DataSources
    protected void EsDSEspecificacaoBolsa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        EspecificacaoBolsaCollection coll = new EspecificacaoBolsaCollection();

        coll.Query.OrderBy(coll.Query.Tipo.Descending, coll.Query.Especificacao.Ascending);
        coll.Query.Load();

        e.Collection = coll;
    }

    protected void EsDSEsDSEspecificacao1_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        AtivoBolsaCollection coll = new AtivoBolsaCollection();

        coll.Query.es.Distinct = true;

        coll.Query.Select(coll.Query.Especificacao);
        coll.Query.OrderBy(coll.Query.Especificacao.Ascending);
        coll.Query.Load();

        e.Collection = coll;
    }

    #endregion
   
    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";
        ASPxComboBox dropEspecificacao = gridCadastro.FindEditFormTemplateControl("dropEspecificacao") as ASPxComboBox;
        ASPxComboBox dropTipo = gridCadastro.FindEditFormTemplateControl("dropTipo") as ASPxComboBox;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(dropEspecificacao);
        controles.Add(dropTipo);

        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        if (gridCadastro.IsNewRowEditing)
        {
            EspecificacaoBolsa especificacaoBolsa = new EspecificacaoBolsa();
            if (especificacaoBolsa.LoadByPrimaryKey(dropEspecificacao.Text))
            {
                e.Result = "Registro já existente";
            }
        }
    }    

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) 
    {
        EspecificacaoBolsa especificacaoBolsa = new EspecificacaoBolsa();

        ASPxComboBox dropEspecificacao = gridCadastro.FindEditFormTemplateControl("dropEspecificacao") as ASPxComboBox;
        ASPxComboBox dropTipo = gridCadastro.FindEditFormTemplateControl("dropTipo") as ASPxComboBox;

        if (especificacaoBolsa.LoadByPrimaryKey(dropEspecificacao.Text))
        {
            especificacaoBolsa.Tipo = Convert.ToByte(dropTipo.SelectedItem.Value);
            especificacaoBolsa.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de EspecificacaoBolsa - Operacao: Update EspecificacaoBolsa: " + dropEspecificacao.Text + "; " + dropTipo.Text + UtilitarioWeb.ToString(especificacaoBolsa),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) 
    {
        EspecificacaoBolsa especificacaoBolsa = new EspecificacaoBolsa();

        ASPxComboBox dropEspecificacao = gridCadastro.FindEditFormTemplateControl("dropEspecificacao") as ASPxComboBox;
        ASPxComboBox dropTipo = gridCadastro.FindEditFormTemplateControl("dropTipo") as ASPxComboBox;

        especificacaoBolsa.Especificacao = dropEspecificacao.Text;
        especificacaoBolsa.Tipo = Convert.ToByte(dropTipo.SelectedItem.Value);
        especificacaoBolsa.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de EspecificacaoBolsa - Operacao: Insert EspecificacaoBolsa: " + especificacaoBolsa.Especificacao + "; " + especificacaoBolsa.Tipo.Value.ToString() + UtilitarioWeb.ToString(especificacaoBolsa),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) 
    {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(EspecificacaoBolsaMetadata.ColumnNames.Especificacao);

            for (int i = 0; i < keyValuesId.Count; i++) {
                string especificacao = Convert.ToString(keyValuesId[i]);

                EspecificacaoBolsa especificacaoBolsa = new EspecificacaoBolsa();
                if (especificacaoBolsa.LoadByPrimaryKey(especificacao)) {
                    //
                    EspecificacaoBolsa especificacaoBolsaClone = (EspecificacaoBolsa)Utilitario.Clone(especificacaoBolsa);
                    //                    
                    especificacaoBolsa.MarkAsDeleted();
                    especificacaoBolsa.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de EspecificacaoBolsa - Operacao: Delete EspecificacaoBolsa: " + especificacao + "; " + UtilitarioWeb.ToString(especificacaoBolsaClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }
   
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) 
    {        
        base.gridCadastro_PreRender(sender, e);
    }
}