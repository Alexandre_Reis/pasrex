﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using Financial.Investidor;
using Financial.ContaCorrente;
using System.Drawing;
using Financial.ContaCorrente.Enums;
using Financial.Security;
using Financial.Fundo;
using Financial.InvestidorCotista;
using Financial.InvestidorCotista.Enums;
using Financial.Investidor.Enums;
using Financial.Common.Enums;
using Financial.Bolsa;
using Financial.Bolsa.Enums;
using DevExpress.Web.Data;
using Financial.Web.Common;
using EntitySpaces.Interfaces;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_AjustePosicaoEmprestimoBolsa : Page {
    private int selectedIndex;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e) {
        btnEditCodigoCliente.Focus();
    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSPosicaoEmprestimoBolsa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        if (btnEditCodigoCliente.Text != "" && textData.Text != "") {
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(Convert.ToInt32(btnEditCodigoCliente.Text));
            DateTime dataDia = cliente.DataDia.Value;

            if (DateTime.Compare(dataDia, Convert.ToDateTime(textData.Text)) <= 0) {
                PosicaoEmprestimoBolsaCollection coll = new PosicaoEmprestimoBolsaCollection();

                coll.Query.Where(coll.Query.Quantidade.GreaterThan(0));
                coll.Query.Where(coll.Query.IdCliente.Equal(Convert.ToInt32(btnEditCodigoCliente.Text)));
                coll.Query.OrderBy(coll.Query.IdPosicao.Descending);

                coll.Query.Load();

                // Assign the esDataSourcSelectEvenArgs Collection property
                e.Collection = coll;
            }
            else {
                PosicaoEmprestimoBolsaHistoricoCollection coll = new PosicaoEmprestimoBolsaHistoricoCollection();

                coll.Query.Where(coll.Query.Quantidade.GreaterThan(0));
                coll.Query.Where(coll.Query.IdCliente.Equal(Convert.ToInt32(btnEditCodigoCliente.Text)));
                coll.Query.Where(coll.Query.DataHistorico.Equal(Convert.ToDateTime(textData.Text)));
                coll.Query.OrderBy(coll.Query.IdPosicao.Descending);

                coll.Query.Load();

                // Assign the esDataSourcSelectEvenArgs Collection property
                e.Collection = coll;
            }
        }
        else {
            e.Collection = new PosicaoEmprestimoBolsaCollection();
        }
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ClienteCollection coll = new ClienteCollection();
        coll.BuscaClientesComAcesso(Context.User.Identity.Name);
        
        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        string resultado = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") {
                if (cliente.IsAtivo) {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name))
                    {
                        nome = cliente.str.Apelido;
                        resultado = nome;
                    }
                    else {
                        resultado = "no_access";
                    }
                }
                else {
                    resultado = "no_active";
                }
            }
        }
        e.Result = resultado;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCliente_CustomDataCallback(object sender, DevExpress.Web.ASPxGridViewCustomDataCallbackEventArgs e) {
        e.Result = gridCliente.GetRowValues(Convert.ToInt32(e.Parameters), "IdCliente");
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCliente_HtmlRowCreated(object sender, DevExpress.Web.ASPxGridViewTableRowEventArgs e) {
        e.Row.Attributes.Add("onmouseover", "backgroundColor = this.style.backgroundColor; this.style.backgroundColor='#E0E9EB';");
        e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=backgroundColor;");
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="selectedIndex"></param>
    protected void AtualizaPosicao(int selectedIndex) 
    {
        ASPxPopupControl popupPosicaoEmprestimoBolsa = this.FindControl("popupPosicaoEmprestimoBolsa") as ASPxPopupControl;
        ASPxGridView gridPosicaoEmprestimoBolsa = popupPosicaoEmprestimoBolsa.FindControl("gridPosicaoEmprestimoBolsa") as ASPxGridView;
        ASPxSpinEdit textQuantidade = popupPosicaoEmprestimoBolsa.FindControl("textQuantidade") as ASPxSpinEdit;
        ASPxSpinEdit textTaxa = popupPosicaoEmprestimoBolsa.FindControl("textTaxa") as ASPxSpinEdit;
        ASPxSpinEdit textNrContrato = popupPosicaoEmprestimoBolsa.FindControl("textNrContrato") as ASPxSpinEdit;
        ASPxDateEdit textDataRegistro = popupPosicaoEmprestimoBolsa.FindControl("textDataRegistro") as ASPxDateEdit;
        ASPxDateEdit textDataVencimento = popupPosicaoEmprestimoBolsa.FindControl("textDataVencimento") as ASPxDateEdit;

        int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);

        Cliente cliente = new Cliente();
        cliente.LoadByPrimaryKey(idCliente);
        DateTime dataDia = cliente.DataDia.Value;
        byte status = cliente.Status.Value;

        int idPosicao = Convert.ToInt32(gridPosicaoEmprestimoBolsa.GetRowValues(selectedIndex, "IdPosicao"));        
        DateTime data = Convert.ToDateTime(textData.Text);

        decimal? quantidade = null;
        if (textQuantidade.Text != "")
        {
            quantidade = Convert.ToDecimal(textQuantidade.Text);
        }
        decimal? taxa = null;
        if (textTaxa.Text != "")
        {
            taxa = Convert.ToDecimal(textTaxa.Text);
        }
        int? nrContrato = null;
        if (textNrContrato.Text != "")
        {
            nrContrato = Convert.ToInt32(textNrContrato.Text);
        }
        DateTime? dataRegistro = null;
        if (textDataRegistro.Text != "")
        {
            dataRegistro = Convert.ToDateTime(textDataRegistro.Text);
        }
        DateTime? dataVencimento = null;
        if (textDataVencimento.Text != "")
        {
            dataVencimento = Convert.ToDateTime(textDataVencimento.Text);
        }

        if (DateTime.Compare(dataDia, data) > 0)
        {
            PosicaoEmprestimoBolsaHistorico posicaoEmprestimoBolsaHistorico = new PosicaoEmprestimoBolsaHistorico();
            if (posicaoEmprestimoBolsaHistorico.LoadByPrimaryKey(idPosicao, data)) 
            {
                if (quantidade.HasValue)
                {
                    decimal diferenca = quantidade.Value - posicaoEmprestimoBolsaHistorico.Quantidade.Value;
                    posicaoEmprestimoBolsaHistorico.Quantidade += diferenca;
                }
                if (taxa.HasValue)
                {
                    posicaoEmprestimoBolsaHistorico.TaxaOperacao = taxa.Value;
                }
                if (nrContrato.HasValue)
                {
                    posicaoEmprestimoBolsaHistorico.NumeroContrato = nrContrato.Value;
                }
                if (dataRegistro.HasValue)
                {
                    posicaoEmprestimoBolsaHistorico.DataRegistro = dataRegistro.Value;
                }
                if (dataVencimento.HasValue)
                {
                    posicaoEmprestimoBolsaHistorico.DataVencimento = dataVencimento.Value;
                }
                posicaoEmprestimoBolsaHistorico.Save();
            }
        }
        else 
        {
            PosicaoEmprestimoBolsa posicaoEmprestimoBolsa = new PosicaoEmprestimoBolsa();
            if (posicaoEmprestimoBolsa.LoadByPrimaryKey(idPosicao)) 
            {
                if (quantidade.HasValue)
                {
                    decimal diferenca = quantidade.Value - posicaoEmprestimoBolsa.Quantidade.Value;
                    posicaoEmprestimoBolsa.Quantidade += diferenca;
                }
                if (taxa.HasValue)
                {
                    posicaoEmprestimoBolsa.TaxaOperacao = taxa.Value;
                }
                if (nrContrato.HasValue)
                {
                    posicaoEmprestimoBolsa.NumeroContrato = nrContrato.Value;
                }
                if (dataRegistro.HasValue)
                {
                    posicaoEmprestimoBolsa.DataRegistro = dataRegistro.Value;
                }
                if (dataVencimento.HasValue)
                {
                    posicaoEmprestimoBolsa.DataVencimento = dataVencimento.Value;
                }
                posicaoEmprestimoBolsa.Save();

                #region Log do Processo
                HistoricoLog historicoLog = new HistoricoLog();
                historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                "Cadastro de Ajuste PosicaoEmprestimoBolsa - Operacao: Update Ajuste PosicaoEmprestimoBolsa: " + idPosicao + UtilitarioWeb.ToString(posicaoEmprestimoBolsa),
                                                HttpContext.Current.User.Identity.Name,
                                                UtilitarioWeb.GetIP(Request),
                                                "",
                                                HistoricoLogOrigem.Outros);
                #endregion
            }

            if (status == (byte)StatusCliente.Divulgado) 
            {
                PosicaoEmprestimoBolsaHistorico posicaoEmprestimoBolsaHistorico = new PosicaoEmprestimoBolsaHistorico();
                if (posicaoEmprestimoBolsaHistorico.LoadByPrimaryKey(idPosicao, data)) 
                {
                    if (quantidade.HasValue)
                    {
                        decimal diferenca = quantidade.Value - posicaoEmprestimoBolsaHistorico.Quantidade.Value;
                        posicaoEmprestimoBolsaHistorico.Quantidade += diferenca;
                    }
                    if (taxa.HasValue)
                    {
                        posicaoEmprestimoBolsaHistorico.TaxaOperacao = taxa.Value;
                    }
                    if (nrContrato.HasValue)
                    {
                        posicaoEmprestimoBolsaHistorico.NumeroContrato = nrContrato.Value;
                    }
                    if (dataRegistro.HasValue)
                    {
                        posicaoEmprestimoBolsaHistorico.DataRegistro = dataRegistro.Value;
                    }
                    if (dataVencimento.HasValue)
                    {
                        posicaoEmprestimoBolsaHistorico.DataVencimento = dataVencimento.Value;
                    }
                    posicaoEmprestimoBolsaHistorico.Save();
                }
            }
        }

        PosicaoEmprestimoBolsaAbertura posicaoEmprestimoBolsaAbertura = new PosicaoEmprestimoBolsaAbertura();
        if (posicaoEmprestimoBolsaAbertura.LoadByPrimaryKey(idPosicao, data)) 
        {
            if (quantidade.HasValue)
            {
                decimal diferenca = quantidade.Value - posicaoEmprestimoBolsaAbertura.Quantidade.Value;
                posicaoEmprestimoBolsaAbertura.Quantidade += diferenca;
            }
            if (taxa.HasValue)
            {
                posicaoEmprestimoBolsaAbertura.TaxaOperacao = taxa.Value;
            }
            if (nrContrato.HasValue)
            {
                posicaoEmprestimoBolsaAbertura.NumeroContrato = nrContrato.Value;
            }
            if (dataRegistro.HasValue)
            {
                posicaoEmprestimoBolsaAbertura.DataRegistro = dataRegistro.Value;
            }
            if (dataVencimento.HasValue)
            {
                posicaoEmprestimoBolsaAbertura.DataVencimento = dataVencimento.Value;
            }
            posicaoEmprestimoBolsaAbertura.Save();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridPosicaoEmprestimoBolsa_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "") //Usado apenas na hora q abre inicialmente a popup, para dar refresh nas posições
        {
            gridPosicaoEmprestimoBolsa.DataBind();
        }
        else {
            selectedIndex = Convert.ToInt32(e.Parameters);
            AtualizaPosicao(selectedIndex);
            gridPosicaoEmprestimoBolsa.DataBind();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridPosicaoEmprestimoBolsa_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e) {
        UtilitarioGrid.SetaCoresGrid(e);
    }

    protected void gridPosicaoEmprestimoBolsa_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "PontaEmprestimoDescricao")
        {
            byte ponta = Convert.ToByte(e.GetListSourceFieldValue(PosicaoEmprestimoBolsaMetadata.ColumnNames.PontaEmprestimo));

            string pontaDescricao = ponta == (byte)PontaEmprestimoBolsa.Doador? "Doador": "Tomador";

            e.Value = pontaDescricao;
        }
    }
}