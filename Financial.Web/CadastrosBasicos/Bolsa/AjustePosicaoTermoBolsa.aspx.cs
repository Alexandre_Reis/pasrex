﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using Financial.Investidor;
using Financial.ContaCorrente;
using System.Drawing;
using Financial.ContaCorrente.Enums;
using Financial.Security;
using Financial.Fundo;
using Financial.InvestidorCotista;
using Financial.InvestidorCotista.Enums;
using Financial.Investidor.Enums;
using Financial.Common.Enums;
using Financial.Bolsa;
using Financial.Bolsa.Enums;
using DevExpress.Web.Data;
using Financial.Web.Common;
using EntitySpaces.Interfaces;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_AjustePosicaoTermoBolsa : Page
{
    private int selectedIndex;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e) {
        btnEditCodigoCliente.Focus();
    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSPosicaoTermoBolsa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        if (btnEditCodigoCliente.Text != "" && textData.Text != "") {
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(Convert.ToInt32(btnEditCodigoCliente.Text));
            DateTime dataDia = cliente.DataDia.Value;

            if (DateTime.Compare(dataDia, Convert.ToDateTime(textData.Text)) <= 0) {
                PosicaoTermoBolsaCollection coll = new PosicaoTermoBolsaCollection();

                coll.Query.Where(coll.Query.IdCliente.Equal(Convert.ToInt32(btnEditCodigoCliente.Text)));
                coll.Query.OrderBy(coll.Query.IdPosicao.Descending);

                coll.Query.Load();

                // Assign the esDataSourcSelectEvenArgs Collection property
                e.Collection = coll;
            }
            else {
                PosicaoTermoBolsaHistoricoCollection coll = new PosicaoTermoBolsaHistoricoCollection();

                coll.Query.Where(coll.Query.IdCliente.Equal(Convert.ToInt32(btnEditCodigoCliente.Text)));
                coll.Query.Where(coll.Query.DataHistorico.Equal(Convert.ToDateTime(textData.Text)));
                coll.Query.OrderBy(coll.Query.IdPosicao.Descending);

                coll.Query.Load();

                // Assign the esDataSourcSelectEvenArgs Collection property
                e.Collection = coll;
            }
        }
        else {
            e.Collection = new PosicaoTermoBolsaCollection();
        }
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ClienteCollection coll = new ClienteCollection();
        coll.BuscaClientesComAcesso(Context.User.Identity.Name);
        
        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        string resultado = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") {
                if (cliente.IsAtivo) {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name))
                    {
                        nome = cliente.str.Apelido;
                        resultado = nome;
                    }
                    else {
                        resultado = "no_access";
                    }
                }
                else {
                    resultado = "no_active";
                }
            }
        }
        e.Result = resultado;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCliente_CustomDataCallback(object sender, DevExpress.Web.ASPxGridViewCustomDataCallbackEventArgs e) {
        e.Result = gridCliente.GetRowValues(Convert.ToInt32(e.Parameters), "IdCliente");
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCliente_HtmlRowCreated(object sender, DevExpress.Web.ASPxGridViewTableRowEventArgs e) {
        e.Row.Attributes.Add("onmouseover", "backgroundColor = this.style.backgroundColor; this.style.backgroundColor='#E0E9EB';");
        e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=backgroundColor;");
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="selectedIndex"></param>
    protected void AtualizaPosicao(int selectedIndex) 
    {
        ASPxPopupControl popupPosicaoTermoBolsa = this.FindControl("popupPosicaoTermoBolsa") as ASPxPopupControl;
        ASPxGridView gridPosicaoTermoBolsa = popupPosicaoTermoBolsa.FindControl("gridPosicaoTermoBolsa") as ASPxGridView;
        ASPxSpinEdit textQuantidade = popupPosicaoTermoBolsa.FindControl("textQuantidade") as ASPxSpinEdit;
        ASPxSpinEdit textNrContrato = popupPosicaoTermoBolsa.FindControl("textNrContrato") as ASPxSpinEdit;
        ASPxDateEdit textDataOperacao = popupPosicaoTermoBolsa.FindControl("textDataOperacao") as ASPxDateEdit;
        ASPxDateEdit textDataVencimento = popupPosicaoTermoBolsa.FindControl("textDataVencimento") as ASPxDateEdit;
        ASPxComboBox dropTipoTermo = popupPosicaoTermoBolsa.FindControl("dropTipoTermo") as ASPxComboBox;

        int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);

        Cliente cliente = new Cliente();
        cliente.LoadByPrimaryKey(idCliente);
        DateTime dataDia = cliente.DataDia.Value;
        byte status = cliente.Status.Value;

        int idPosicao = Convert.ToInt32(gridPosicaoTermoBolsa.GetRowValues(selectedIndex, "IdPosicao"));        
        DateTime data = Convert.ToDateTime(textData.Text);

        decimal? quantidade = null;
        if (textQuantidade.Text != "")
        {
            quantidade = Convert.ToDecimal(textQuantidade.Text);
        }
        decimal? nrContrato = null;
        if (textNrContrato.Text != "")
        {
            nrContrato = Convert.ToDecimal(textNrContrato.Text);
        }
        DateTime? dataOperacao = null;
        if (textDataOperacao.Text != "")
        {
            dataOperacao = Convert.ToDateTime(textDataOperacao.Text);
        }
        DateTime? dataVencimento = null;
        if (textDataVencimento.Text != "")
        {
            dataVencimento = Convert.ToDateTime(textDataVencimento.Text);
        }

        byte? tipoTermo = null;
        if (dropTipoTermo.SelectedIndex > -1)
        {
            tipoTermo = Convert.ToByte(dropTipoTermo.SelectedItem.Value);
        }

        if (DateTime.Compare(dataDia, data) > 0)
        {
            PosicaoTermoBolsaHistorico posicaoTermoBolsaHistorico = new PosicaoTermoBolsaHistorico();
            if (posicaoTermoBolsaHistorico.LoadByPrimaryKey(idPosicao, data)) 
            {
                if (quantidade.HasValue)
                {
                    decimal diferenca = quantidade.Value - posicaoTermoBolsaHistorico.Quantidade.Value;
                    posicaoTermoBolsaHistorico.Quantidade += diferenca;
                }
                if (nrContrato.HasValue)
                {
                    posicaoTermoBolsaHistorico.NumeroContrato = nrContrato.Value.ToString();
                }
                if (dataOperacao.HasValue)
                {
                    posicaoTermoBolsaHistorico.DataOperacao = dataOperacao.Value;
                }
                if (dataVencimento.HasValue)
                {
                    posicaoTermoBolsaHistorico.DataVencimento = dataVencimento.Value;
                }
                if (tipoTermo.HasValue)
                {
                    posicaoTermoBolsaHistorico.TipoTermo = tipoTermo.Value;
                }
                posicaoTermoBolsaHistorico.Save();
            }
        }
        else 
        {
            PosicaoTermoBolsa posicaoTermoBolsa = new PosicaoTermoBolsa();
            if (posicaoTermoBolsa.LoadByPrimaryKey(idPosicao)) 
            {
                if (quantidade.HasValue)
                {
                    decimal diferenca = quantidade.Value - posicaoTermoBolsa.Quantidade.Value;
                    posicaoTermoBolsa.Quantidade += diferenca;
                }
                if (nrContrato.HasValue)
                {
                    posicaoTermoBolsa.NumeroContrato = nrContrato.Value.ToString();
                }
                if (dataOperacao.HasValue)
                {
                    posicaoTermoBolsa.DataOperacao = dataOperacao.Value;
                }
                if (dataVencimento.HasValue)
                {
                    posicaoTermoBolsa.DataVencimento = dataVencimento.Value;
                }
                if (tipoTermo.HasValue)
                {
                    posicaoTermoBolsa.TipoTermo = tipoTermo.Value;
                }
                posicaoTermoBolsa.Save();

                #region Log do Processo
                HistoricoLog historicoLog = new HistoricoLog();
                historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                "Cadastro de Ajuste PosicaoTermoBolsa - Operacao: Update Ajuste PosicaoTermoBolsa: " + idPosicao + UtilitarioWeb.ToString(posicaoTermoBolsa),
                                                HttpContext.Current.User.Identity.Name,
                                                UtilitarioWeb.GetIP(Request),
                                                "",
                                                HistoricoLogOrigem.Outros);
                #endregion
            }

            if (status == (byte)StatusCliente.Divulgado) 
            {
                PosicaoTermoBolsaHistorico posicaoTermoBolsaHistorico = new PosicaoTermoBolsaHistorico();
                if (posicaoTermoBolsaHistorico.LoadByPrimaryKey(idPosicao, data)) 
                {
                    if (quantidade.HasValue)
                    {
                        decimal diferenca = quantidade.Value - posicaoTermoBolsaHistorico.Quantidade.Value;
                        posicaoTermoBolsaHistorico.Quantidade += diferenca;
                    }
                    if (nrContrato.HasValue)
                    {
                        posicaoTermoBolsaHistorico.NumeroContrato = nrContrato.Value.ToString();
                    }
                    if (dataOperacao.HasValue)
                    {
                        posicaoTermoBolsaHistorico.DataOperacao = dataOperacao.Value;
                    }
                    if (dataVencimento.HasValue)
                    {
                        posicaoTermoBolsaHistorico.DataVencimento = dataVencimento.Value;
                    }
                    if (tipoTermo.HasValue)
                    {
                        posicaoTermoBolsaHistorico.TipoTermo = tipoTermo.Value;
                    }
                    posicaoTermoBolsaHistorico.Save();
                }
            }
        }

        PosicaoTermoBolsaAbertura posicaoTermoBolsaAbertura = new PosicaoTermoBolsaAbertura();
        if (posicaoTermoBolsaAbertura.LoadByPrimaryKey(idPosicao, data)) 
        {
            if (quantidade.HasValue)
            {
                decimal diferenca = quantidade.Value - posicaoTermoBolsaAbertura.Quantidade.Value;
                posicaoTermoBolsaAbertura.Quantidade += diferenca;
            }
            if (nrContrato.HasValue)
            {
                posicaoTermoBolsaAbertura.NumeroContrato = nrContrato.Value.ToString();
            }
            if (dataOperacao.HasValue)
            {
                posicaoTermoBolsaAbertura.DataOperacao = dataOperacao.Value;
            }
            if (dataVencimento.HasValue)
            {
                posicaoTermoBolsaAbertura.DataVencimento = dataVencimento.Value;
            }
            if (tipoTermo.HasValue)
            {
                posicaoTermoBolsaAbertura.TipoTermo = tipoTermo.Value;
            }
            posicaoTermoBolsaAbertura.Save();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridPosicaoTermoBolsa_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "") //Usado apenas na hora q abre inicialmente a popup, para dar refresh nas posições
        {
            gridPosicaoTermoBolsa.DataBind();
        }
        else {
            selectedIndex = Convert.ToInt32(e.Parameters);
            AtualizaPosicao(selectedIndex);
            gridPosicaoTermoBolsa.DataBind();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridPosicaoTermoBolsa_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e) {
        UtilitarioGrid.SetaCoresGrid(e);
    }

    protected void gridPosicaoTermoBolsa_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "TipoTermoDescricao")
        {
            byte tipoTermo = Convert.ToByte(e.GetListSourceFieldValue(PosicaoTermoBolsaMetadata.ColumnNames.TipoTermo));

            string tipoTermoDescricao = "";
            if (tipoTermo == (byte)TipoTermoBolsa.Normal)
            {
                tipoTermoDescricao = "Normal";
            }
            else if (tipoTermo == (byte)TipoTermoBolsa.Pontos)
            {
                tipoTermoDescricao = "Pontos";
            }
            else if (tipoTermo == (byte)TipoTermoBolsa.Dolar)
            {
                tipoTermoDescricao = "Dólar";
            }
            else if (tipoTermo == (byte)TipoTermoBolsa.Flexivel)
            {
                tipoTermoDescricao = "Flexível";
            }

            e.Value = tipoTermoDescricao;
        }
    }
}