﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LiquidacaoTermoBolsa.aspx.cs" Inherits="CadastrosBasicos_LiquidacaoTermoBolsa" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>  
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">

    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    var popup = true;    
    document.onkeydown=onDocumentKeyDown; 
    var operacao = '';
           
    function OnGetDataCliente(data) {
        btnEditCodigoCliente.SetValue(data);        
        ASPxCallback1.SendCallback(btnEditCodigoCliente.GetValue());
        popupCliente.HideWindow();
        btnEditCodigoCliente.Focus();
    }   
    function OnGetDataClienteFiltro(data) {
        btnEditCodigoClienteFiltro.SetValue(data);        
        ASPxCallback1.SendCallback(btnEditCodigoClienteFiltro.GetValue());
        popupCliente.HideWindow();
        btnEditCodigoClienteFiltro.Focus();
    }    
    function OpenPopup(value)
    {
        if (value != 3) /* != Vencimento*/
        {
            gridCadastro.StartEditRow(selectedIndex);
        }
        else
        {
            alert('Vencimentos não podem ser alterados!');
        }
    }
    function FechaPopupPosicaoTermoBolsa()
    {
        textQuantidade.SetEnabled(false); 
        textQuantidade.SetText(''); 
        textDiasLiquidacao.SetEnabled(false); 
        textDiasLiquidacao.SetText(''); 
        popupPosicaoTermoBolsa.HideWindow();
        gridCadastro.CancelEdit();
        return false;              
    }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                if (operacao == 'deletar')
                {
                    gridCadastro.PerformCallback('btnDelete');
                }
                else
                {                
                    gridCadastro.UpdateEdit();
                }                      
            }
            
            operacao = '';
        }        
        "/>
    </dxcb:ASPxCallback>    
    
    <dxcb:ASPxCallback ID="callbackErroPosicao" runat="server" OnCallback="callbackErroPosicao_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                gridPosicaoTermoBolsa.PerformCallback(selectedIndex);                
            }
        }        
        "/>
    </dxcb:ASPxCallback>    
    
    <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {                
            if (gridCadastro.cp_EditVisibleIndex == -1)
            {                  
                var resultSplit = e.result.split('|');                        
                e.result = resultSplit[0];            
                var textNomeClienteFiltro = document.getElementById('popupFiltro_textNomeClienteFiltro');
                OnCallBackCompleteClienteFiltro(s, e, popupMensagemCliente, btnEditCodigoClienteFiltro, textNomeClienteFiltro);
            } 
            else
            {                                        
                var textNomeCliente = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCliente');                
                OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigoCliente, textNomeCliente, textData);                                
            }            
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxpc:ASPxPopupControl ID="popupPosicaoTermoBolsa" runat="server" Width="700px" HeaderText="" ContentStyle-VerticalAlign="Top"  
                        PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" AllowDragging="True" ShowCloseButton="false" >
        <ContentCollection><dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">     
        <div>
            <dxwgv:ASPxGridView ID="gridPosicaoTermoBolsa" runat="server" Width="100%"
                    ClientInstanceName="gridPosicaoTermoBolsa"  AutoGenerateColumns="False" 
                    DataSourceID="EsDSPosicaoTermoBolsa" KeyFieldName="IdPosicao"
                    OnHtmlRowCreated="gridPosicaoTermoBolsa_HtmlRowCreated"       
                    OnCustomCallback="gridPosicaoTermoBolsa_CustomCallback"       
                    OnCustomUnboundColumnData="gridPosicaoTermoBolsa_CustomUnboundColumnData">               
            <Columns>
                <dxwgv:GridViewDataSpinEditColumn FieldName="IdPosicao" Visible="false">
                </dxwgv:GridViewDataSpinEditColumn>
                <dxwgv:GridViewDataSpinEditColumn FieldName="IdOperacao" Visible="false">
                </dxwgv:GridViewDataSpinEditColumn>
                <dxwgv:GridViewDataSpinEditColumn FieldName="QuantidadeAntecipada" UnboundType="String" Caption="Qtde Antecipada" VisibleIndex="3" Width="12%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right" EditCellStyle-HorizontalAlign="Right">
                    <PropertiesSpinEdit DisplayFormatString="{0:#,##0.;(#,##0.);0.}" SpinButtons-ShowIncrementButtons="false"></PropertiesSpinEdit>
                </dxwgv:GridViewDataSpinEditColumn>
                <dxwgv:GridViewDataColumn FieldName="CdAtivoBolsa" ReadOnly="true" Caption="Código" VisibleIndex="5" Width="10%">                    
                </dxwgv:GridViewDataColumn>
                <dxwgv:GridViewDataDateColumn FieldName="DataOperacao" ReadOnly="true" Caption="Operação" VisibleIndex="6" Width="10%">
                </dxwgv:GridViewDataDateColumn>
                <dxwgv:GridViewDataSpinEditColumn FieldName="Quantidade" ReadOnly="true" VisibleIndex="8" Width="12%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right" EditCellStyle-HorizontalAlign="Right">
                    <PropertiesSpinEdit DisplayFormatString="{0:#,##0.;(#,##0.);0.}" SpinButtons-ShowIncrementButtons="false" ></PropertiesSpinEdit>
                </dxwgv:GridViewDataSpinEditColumn>
                <dxwgv:GridViewDataSpinEditColumn FieldName="PUTermo" ReadOnly="true" Caption="PU Contratado" VisibleIndex="9" Width="8%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right" EditCellStyle-HorizontalAlign="Right">
                    <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}" SpinButtons-ShowIncrementButtons="false" ></PropertiesSpinEdit>
                </dxwgv:GridViewDataSpinEditColumn>
                <dxwgv:GridViewDataColumn FieldName="NumeroContrato" ReadOnly="true" Caption="Nr Contrato" VisibleIndex="10" Width="12%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                </dxwgv:GridViewDataColumn>
                
            </Columns>            
            
            <SettingsBehavior ColumnResizeMode="Disabled" />                        
            <SettingsPager PageSize="1000"></SettingsPager>
            <Settings ShowTitlePanel="True" ShowFilterRow="True" VerticalScrollBarMode="Visible" VerticalScrollableHeight="280" />
            
            <SettingsDetail ShowDetailButtons="False" />
            <Styles AlternatingRow-Enabled="True" Cell-Wrap="False">
                <Header ImageSpacing="5px" SortingImageSpacing="5px"/>
            </Styles>
            <Images>
                <PopupEditFormWindowClose Height="17px" Width="17px" />
            </Images>
            <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Posições de Termo de Bolsa" />
            
            <ClientSideEvents RowDblClick="function(s, e) {textQuantidade.SetEnabled(true); textQuantidade.SetText('');                                                           
                                                           textDiasLiquidacao.SetEnabled(true); textDiasLiquidacao.SetText('');
                                                           textQuantidade.Focus(); selectedIndex = e.visibleIndex; }"
                              EndCallback="function(s, e) {textQuantidade.SetText('');}" />            
            </dxwgv:ASPxGridView>                              
                    
            <div>
                <table>
                    <tr>
                        <td>
                            <asp:Label ID="labelQtde" runat="server" CssClass="labelNormal" Text="Qtde Antecipar:"></asp:Label>
                        </td>
                        
                        <td>    
                            <dxe:ASPxSpinEdit ID="textQuantidade" runat="server" CssClass="textValor" ClientInstanceName="textQuantidade"
                                              MaxLength="12" NumberType="Integer" DecimalPlaces="0"
                                              MinValue="-9999999999" MaxValue="9999999999" >
                            </dxe:ASPxSpinEdit>
                        </td>
                        
                        <td>                
                            <asp:Label ID="labelDiasLiquidacao" runat="server" CssClass="labelNormal" Text="Liquida em D+"></asp:Label>
                        </td>    
                                            
                        <td>    
                            <dxe:ASPxSpinEdit ID="textDiasLiquidacao" runat="server" CssClass="textCurto" ClientInstanceName="textDiasLiquidacao"
                                              MaxLength="1" MaxValue="3" MinValue="0" NumberType="Integer" DecimalPlaces="0" >
                            </dxe:ASPxSpinEdit>
                        </td>  
                        
                        <td >
                            <div class="linkButton linkButtonNoBorder popupFooter">
                                <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                   OnClientClick="callbackErroPosicao.SendCallback(); return false; "><asp:Literal ID="Literal10" runat="server" Text="Salvar"/><div></div></asp:LinkButton>
                                <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false"
                                                CssClass="btnCancel" OnClientClick="FechaPopupPosicaoTermoBolsa(); return false;"><asp:Literal ID="Literal11" runat="server" Text="Fechar"/><div></div></asp:LinkButton>
                            </div>        
                        </td>
                    </tr>
                </table>
                
                
            </div>                
            
        </div>    
        </dxpc:PopupControlContentControl></ContentCollection>     
        <ClientSideEvents PopUp="function(s, e) {gridPosicaoTermoBolsa.PerformCallback(); 
                                                 textQuantidade.SetEnabled(false);
                                                 textDiasLiquidacao.SetEnabled(false);}" />   
    </dxpc:ASPxPopupControl>
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">    
    
    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Liquidação de Termos de Bolsa"></asp:Label>
    </div>
        
    <div id="mainContent">
               
        <dxpc:ASPxPopupControl ID="popupFiltro" AllowDragging="true" PopupElementID="popupFiltro" EnableClientSideAPI="True"                    
                                PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" CloseAction="CloseButton" 
                                Width="400" Left="250" Top="70" HeaderText="Filtros adicionais para consulta" runat="server"
                                HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">            
            <ContentCollection><dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                
                <table cellpadding="2" cellspacing="2">        
                    <tr>
                        <td class="td_Label_Longo">
                            <asp:Label ID="labelClienteFiltro" runat="server" CssClass="labelNormal" Text="Cliente:"></asp:Label>
                        </td>        
                        
                        <td>                                                                                                                
                            <dxe:ASPxSpinEdit ID="btnEditCodigoClienteFiltro" runat="server" CssClass="textButtonEdit" 
                                        ClientInstanceName="btnEditCodigoClienteFiltro" MaxLength="10" NumberType="Integer">            
                            <Buttons>                                           
                                <dxe:EditButton>
                                </dxe:EditButton>                                
                            </Buttons>       
                            <ClientSideEvents                                                           
                                     KeyPress="function(s, e) {document.getElementById('popupFiltro_textNomeClienteFiltro').value = '';} " 
                                     ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" 
                                     LostFocus="function(s, e) {OnLostFocus(popupMensagemCliente, ASPxCallback1, btnEditCodigoClienteFiltro);}"
                                    />               
                            </dxe:ASPxSpinEdit>
                        </td>
                    
                        <td colspan="2" width="400">
                            <asp:TextBox ID="textNomeClienteFiltro" runat="server" CssClass="textNome" Enabled="false" ></asp:TextBox>
                        </td>
                    </tr>        
                    
                    <tr>
                        <td>                
                            <asp:Label ID="labelDataInicio" runat="server" CssClass="labelNormal" Text="Início:"></asp:Label>
                        </td>    
                                            
                        <td>
                            <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" />
                        </td>  
                        
                       <td colspan="2">
                            <table>
                                <tr><td><asp:Label ID="labelDataFim" runat="server" CssClass="labelNormal" Text="Fim:"/></td>
                                <td><dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim"/></td></tr>
                            </table>
                        </td>                                                                          
                    </tr>
                    
                    <tr>
                    <td>
                        <asp:Label ID="labelCdAtivo" runat="server" CssClass="labelNormal" Text="Ativo:"></asp:Label>
                    </td>                
                    <td colspan="3">
                        <asp:TextBox ID="textCdAtivo" runat="server" CssClass="textNormal_5"></asp:TextBox>
                    </td>    
                    </tr>
                </table>        
                
               <div class="linkButton linkButtonNoBorder" style="margin-top:20px">              
                <asp:LinkButton ID="btnOKFilter" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnOK" OnClientClick="popupFiltro.Hide(); gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal7" runat="server" Text="Aplicar"/><div></div></asp:LinkButton>
                <asp:LinkButton ID="btnFilterCancel" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnFilterCancel" OnClientClick="OnClearFilterClick_FiltroDatas(); return false;"><asp:Literal ID="Literal8" runat="server" Text="Limpar"/><div></div></asp:LinkButton>
               </div>                    
              
            </dxpc:PopupControlContentControl></ContentCollection>                             
        </dxpc:ASPxPopupControl>        

        <div class="linkButton" >               
           <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal1" runat="server" Text="Novo"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) {operacao='deletar'; callbackErro.SendCallback();} return false;" ><asp:Literal ID="Literal2" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnFilter" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnFilter" OnClientClick="return OnButtonClick_FiltroDatas()"><asp:Literal ID="Literal3" runat="server" Text="Filtro"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal4" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal5" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal6" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
        </div>
    
        <div class="divDataGrid">
            <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                    KeyFieldName="IdLiquidacao" DataSourceID="EsDSLiquidacaoTermoBolsa"
                    OnCustomCallback="gridCadastro_CustomCallback"
                    OnPreRender="gridCadastro_PreRender"
                    OnCustomJSProperties="gridCadastro_CustomJSProperties"
                    OnRowUpdating="gridCadastro_RowUpdating"                      
                    OnCancelRowEditing="gridCadastro_CancelRowEditing"
                    OnBeforeGetCallbackResult="gridCadastro_PreRender" >
                
                <Columns>
                    <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                        <HeaderTemplate>
                            <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                        </HeaderTemplate>
                    </dxwgv:GridViewCommandColumn>
                    
                    <dxwgv:GridViewDataColumn FieldName="IdCliente" VisibleIndex="2" Width="7%" CellStyle-HorizontalAlign="left" />                    
                    
                    <dxwgv:GridViewDataColumn FieldName="Apelido" VisibleIndex="3" Width="20%"/>                    
                    
                    <dxwgv:GridViewDataDateColumn FieldName="DataMovimento" Caption="Data" VisibleIndex="4" Width="10%"/>
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="TipoLiquidacao" Caption="Tipo" VisibleIndex="5" Width="18%">
                    <PropertiesComboBox EncodeHtml="false">
                        <Items>
                            <dxe:ListEditItem Value="1" Text="<div title='Antecipação'>Antecipação</div>" />
                            <dxe:ListEditItem Value="2" Text="<div title='Antecipação (LPD)'>Antecipação (LPD)</div>" />
                            <dxe:ListEditItem Value="3" Text="<div title='Vencimento'>Vencimento</div>" />
                            <dxe:ListEditItem Value="4" Text="<div title='Antecipação (LPDE)'>Antecipação (LPDE)</div>" />
                        </Items>                    
                    </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataComboBoxColumn Caption="Corretora" FieldName="IdAgente" VisibleIndex="6" Width="18%">
                        <PropertiesComboBox DataSourceID="EsDSAgenteMercado" TextField="Nome" ValueField="IdAgente">
                        </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataColumn FieldName="CdAtivoBolsa" Caption="Código" VisibleIndex="7" Width="10%" />
                    
                    <dxwgv:GridViewDataTextColumn FieldName="Quantidade" VisibleIndex="9" Width="8%"
                            HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right"
                            CellStyle-HorizontalAlign="Right">                    
                    <PropertiesTextEdit DisplayFormatString="{0:#,##0.;(#,##0.);0.}"></PropertiesTextEdit>        
                    </dxwgv:GridViewDataTextColumn>
                    
                    <dxwgv:GridViewDataTextColumn FieldName="Valor" VisibleIndex="11" Width="8%"
                            HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right"
                            CellStyle-HorizontalAlign="Right">                    
                    <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesTextEdit>        
                    </dxwgv:GridViewDataTextColumn>
                    
                    <dxwgv:GridViewDataColumn FieldName="NumeroContrato" Visible="false"/>
                    <dxwgv:GridViewDataColumn FieldName="IdAgente" Visible="false"/>                    
                    <dxwgv:GridViewDataColumn FieldName="IdPosicao" Visible="false"/>
                    <dxwgv:GridViewDataColumn FieldName="DataLiquidacao" Visible="false"/>
                </Columns>
                
                <Templates>            
                <EditForm>                          
                    <asp:Label ID="labelEdicao" runat="server" CssClass="labelInformation" Text=""></asp:Label>
                    
                    <asp:Panel ID="panelEdicao" runat="server" OnLoad="panelEdicao_Load">
                    
                    <div class="editForm">                            
                        
                        <asp:TextBox ID="hiddenIdPosicao" runat="server" CssClass="hiddenField" Text='<%#Eval("IdPosicao")%>'    />
                        <asp:TextBox ID="hiddenIdLiquidacao" runat="server" CssClass="hiddenField" Text='<%#Eval("IdLiquidacao")%>'    />                                                 
                        
                        <table>
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelCliente" runat="server" CssClass="labelRequired" Text="Cliente:"></asp:Label>
                                </td>        
                                
                                <td colspan="3">
                                  <table cellspacing="0" ><tr><td>
                                    <dxe:ASPxSpinEdit ID="btnEditCodigoCliente" runat="server" CssClass="textButtonEdit" 
                                                ClientInstanceName="btnEditCodigoCliente" Text='<%#Eval("IdCliente")%>'                                                    
                                                MaxLength="10" NumberType="Integer">            
                                    <Buttons>                                           
                                        <dxe:EditButton>
                                        </dxe:EditButton>                                
                                    </Buttons>       
                                    <ClientSideEvents                                                           
                                             KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCliente').value = '';} " 
                                             ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" 
                                             LostFocus="function(s, e) {OnLostFocus(popupMensagemCliente, ASPxCallback1, btnEditCodigoCliente);}"
                                            />                           
                                    </dxe:ASPxSpinEdit>
                                    </td><td>
                                    <asp:TextBox ID="textNomeCliente" runat="server" CssClass="textNome" Enabled="false" Text='<%#Eval("Apelido")%>' ></asp:TextBox>
                                    </td></tr></table>
                                </td>
                            </tr>
                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelData" runat="server" CssClass="labelRequired" Text="Data:" />
                                </td>                                     
                                <td colspan="3">                                
                                    <dxe:ASPxDateEdit ID="textData" runat="server" ClientInstanceName="textData" Value='<%#Eval("DataMovimento")%>' ClientEnabled="false"/>                                
                                </td> 
                            </tr>
                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelAgente" runat="server" CssClass="labelRequired" Text="Agente:"> </asp:Label>
                                </td>                
                                <td colspan="2">
                                    <dxe:ASPxComboBox ID="dropAgente" runat="server" ClientInstanceName="dropAgente"
                                                        DataSourceID="EsDSAgenteMercado" IncrementalFilteringMode="Contains"
                                                        ShowShadow="false" DropDownStyle="DropDown"
                                                        CssClass="dropDownListLongo" TextField="Nome" ValueField="IdAgente"
                                                        Text='<%#Eval("IdAgente")%>'>                                        
                                    </dxe:ASPxComboBox>         
                                </td>
                                
                                <td class="linkButton linkButtonNoBorder" width="290">
                                    <asp:LinkButton ID="btnPosicaoTermoBolsa" ForeColor="black" runat="server" CssClass="btnSearch" OnClientClick="popupPosicaoTermoBolsa.ShowAtElementByID(); return false;"><asp:Literal ID="Literal7" runat="server" Text="Ver Posições"/><div></div></asp:LinkButton>
                                </td>                                    
                            </tr>     
                            
                            <tr>
                                <td  class="td_Label">
                                    <asp:Label ID="labelQuantidade" runat="server" CssClass="labelRequired" Text="Quantidade:"></asp:Label>
                                </td>
                                <td colspan="4">
                                    <dxe:ASPxSpinEdit ID="textQuantidade" runat="server" CssClass="textValor" ClientInstanceName="textQuantidade"
                                                                     MaxLength="12" NumberType="Integer" Text="<%#Bind('Quantidade')%>">                                                        
                                    </dxe:ASPxSpinEdit>
                                </td>
                            </tr>    
                            
                            <tr>
                                <td  class="td_Label">
                                    <asp:Label ID="labelDataLiquidacao" runat="server" CssClass="labelRequired" Text="Liquida em:"></asp:Label>
                                </td>
                                <td>
                                    <dxe:ASPxDateEdit ID="textDataLiquidacao" runat="server" ClientInstanceName="textDataLiquidacao" Value='<%#Eval("DataLiquidacao")%>'/>
                                </td>
                            </tr>                       
                            
                        </table>
                        <div class="linhaH"></div>
                                    
                        <div class="linkButton linkButtonNoBorder popupFooter">
                            <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                               OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal5" runat="server" Text="OK"/><div></div></asp:LinkButton>
                            <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnCancel" OnClientClick="gridCadastro.CancelEdit(); return false;"><asp:Literal ID="Literal9" runat="server" Text="Cancelar"/><div></div></asp:LinkButton>
                        </div>        
                    </div>                        
                    </asp:Panel>
                </EditForm>
                    
                <StatusBar>
                    <div>
                        <div style="float:left">
                        <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" ></asp:Label>
                        </div>                    
                    </div>
                </StatusBar>
                </Templates>
                
                <SettingsPopup EditForm-Width="450px"  />
                <SettingsCommandButton>
                    <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                </SettingsCommandButton>
                
            </dxwgv:ASPxGridView>            
        </div>                           
    </div>
    </div>
    </td></tr></table>
    </div>
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" Landscape="true"
    LeftMargin = "50"
    RightMargin = "50"
    ></dxwgv:ASPxGridViewExporter>
        
    <cc1:esDataSource ID="EsDSLiquidacaoTermoBolsa" runat="server" OnesSelect="EsDSLiquidacaoTermoBolsa_esSelect" LowLevelBind="true"/>    
    <cc1:esDataSource ID="EsDSPosicaoTermoBolsa" runat="server" OnesSelect="EsDSPosicaoTermoBolsa_esSelect" LowLevelBind="true"/>    
    <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" />
    <cc1:esDataSource ID="EsDSAtivoBolsa" runat="server" OnesSelect="EsDSAtivoBolsa_esSelect" />    
    <cc1:esDataSource ID="EsDSAgenteMercado" runat="server" OnesSelect="EsDSAgenteMercado_esSelect" />            
    
    </form>
</body>
</html>