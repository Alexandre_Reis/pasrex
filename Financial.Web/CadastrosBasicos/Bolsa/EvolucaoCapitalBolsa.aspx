﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EvolucaoCapitalBolsa.aspx.cs" Inherits="CadastrosBasicos_EvolucaoCapitalBolsa" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>    
    <script type="text/javascript" language="Javascript">
    var popup = true;
    document.onkeydown=onDocumentKeyDownWithCallback;
    var operacao = '';
    
    function OnGetDataAtivoBolsa(data) {
        btnEditAtivoBolsa.SetValue(data);
        popupAtivoBolsa.HideWindow();
        ASPxCallback1.SendCallback(btnEditAtivoBolsa.GetValue());
        btnEditAtivoBolsa.Focus();
    }    
    </script>
</head>

<body>
    <form id="form1" runat="server">
    
    <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {
            if (e.result == '' && btnEditAtivoBolsa.GetValue() != null)
            {
                popupMensagemAtivoBolsa.SetContentHTML(document.getElementById('textMsgNaoExiste').value);
                popupMensagemAtivoBolsa.ShowAtElementByID(btnEditAtivoBolsa.name);
                btnEditAtivoBolsa.SetValue(''); 
                btnEditAtivoBolsa.Focus();                 
            }            
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                gridCadastro.UpdateEdit();                
            }
            operacao = '';
        }        
        "/>
    </dxcb:ASPxCallback>
        
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">
    
    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Evolução Capital (Bolsa)"></asp:Label>
    </div>
        
    <div id="mainContent">
        
            <dxpc:ASPxPopupControl ID="popupFiltro" AllowDragging="true" PopupElementID="popupFiltro" EnableClientSideAPI="True"                    
                            PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" 
                            Width="400" Left="250" Top="70" HeaderText="Filtros adicionais para consulta" runat="server"
                            HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">            
                <ContentCollection><dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                    
                    <table>        
                    <tr>
                    <td>                
                        <asp:Label ID="labelDataInicio" runat="server" CssClass="labelNormal" Text="Início:"></asp:Label>
                    </td>    
                                        
                    <td>
                        <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" />
                    </td>  
                    
                    <td>
                        <asp:Label ID="labelDataFim" runat="server" CssClass="labelNormal" Text="Fim:"/>
                    </td>
                    <td>
                        <dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim" />
                    </td>                                                                            
                    </tr>
                    
                    <tr>
                    <td>
                        <asp:Label ID="labelCdAtivo" runat="server" CssClass="labelNormal" Text="Ativo:"></asp:Label>
                    </td>                
                    <td>
                        <asp:TextBox ID="textCdAtivo" runat="server" CssClass="textNormal_5" MaxLength="15"></asp:TextBox>
                    </td>    
                    </tr>                    
                    </table>        
                    
                    <div class="linkButton linkButtonNoBorder" style="margin-top:20px">              
                    <asp:LinkButton ID="btnOKFilter" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnOK" OnClientClick="popupFiltro.Hide(); gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal7" runat="server" Text="Aplicar"/><div></div></asp:LinkButton>
                    <asp:LinkButton ID="btnFilterCancel" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnFilterCancel" OnClientClick="OnClearFilterClick_FiltroDatas(); return false;"><asp:Literal ID="Literal8" runat="server" Text="Limpar"/><div></div></asp:LinkButton>
                    </div>                    
                </dxpc:PopupControlContentControl></ContentCollection>                             
            </dxpc:ASPxPopupControl>        
            
            <div class="linkButton" >               
               <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal1" runat="server" Text="Novo"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;"><asp:Literal ID="Literal2" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnFilter" runat="server" Font-Overline="false" CssClass="btnFilter" OnClientClick="return OnButtonClick_FiltroDatas()"><asp:Literal ID="Literal3" runat="server" Text="Filtro"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal4" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal5" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>              
               <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal6" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
            </div>
    
            <div class="divDataGrid">            
                <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                            KeyFieldName="CompositeKey" DataSourceID="EsDSEvolucaoCapitalBolsa"
                            OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData"                
                            OnRowUpdating="gridCadastro_RowUpdating"
                            OnRowInserting="gridCadastro_RowInserting"
                            OnCustomCallback="gridCadastro_CustomCallback"
                            OnBeforeGetCallbackResult="gridCadastro_PreRender"                              
                             >        
                                
                    <Columns>           
                        <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                            <HeaderTemplate>
                                <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                            </HeaderTemplate>
                        </dxwgv:GridViewCommandColumn>
                        
                        <dxwgv:GridViewDataTextColumn FieldName="CompositeKey" UnboundType="String" Visible="False"/>
                                                        
                        <dxwgv:GridViewDataDateColumn FieldName="Data" Caption="Data Referência" VisibleIndex="1" Width="8%"/>
                        <dxwgv:GridViewDataTextColumn FieldName="CdAtivoBolsa" Caption="Código Ativo" VisibleIndex="2" Width="10%"/>

                        <dxwgv:GridViewDataComboBoxColumn FieldName="Tipo" VisibleIndex="3" Width="8%" ExportWidth="125">
                        <PropertiesComboBox EncodeHtml="false">
                        <Items>                                        
                        <dxe:ListEditItem Value="1" Text="Aumento Cap." />
                        <dxe:ListEditItem Value="2" Text="Redução Cap." />                                                                               
                        <dxe:ListEditItem Value="3" Text="Outros" />
                        </Items>
                        </PropertiesComboBox>
                        </dxwgv:GridViewDataComboBoxColumn>
                        <dxwgv:GridViewDataTextColumn FieldName="Descricao" Caption="Descrição" VisibleIndex="5" Width="20%"/>

                        <dxwgv:GridViewDataSpinEditColumn FieldName="Quantidade" Caption="Quantidade Evento" VisibleIndex="7" Width="12%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                        <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesSpinEdit>
                        </dxwgv:GridViewDataSpinEditColumn>
                        
                        <dxwgv:GridViewDataSpinEditColumn FieldName="Valor" Caption="Valor Evento" VisibleIndex="9" Width="10%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                        <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesSpinEdit>
                        </dxwgv:GridViewDataSpinEditColumn>
                        
                        <dxwgv:GridViewDataSpinEditColumn FieldName="QuantidadeFinal" Caption="Quantidade Final" VisibleIndex="11" Width="12%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                        <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesSpinEdit>
                        </dxwgv:GridViewDataSpinEditColumn>
                        
                        <dxwgv:GridViewDataSpinEditColumn FieldName="ValorFinal" Caption="Valor Final" VisibleIndex="13" Width="10%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                        <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}"></PropertiesSpinEdit>
                        </dxwgv:GridViewDataSpinEditColumn>
                        
                    </Columns>
                    
                    <Templates>
                        <EditForm>
                                                    
                            <div class="editForm">        
                                 
                                 <table>                    
                                    <tr>                                        
                                        <td class="td_Label_Curto">
                                            <asp:Label ID="labelData" runat="server" CssClass="labelRequired" Text="Data:"/>
                                        </td>
                                                                
                                        <td>             
                                            <dxe:ASPxDateEdit ID="textData" runat="server" ClientInstanceName="textData" Value='<%#Eval("Data")%>' OnInit="textData_Init"/>
                                        </td>  
                                    </tr>
                                        
                                    <tr>                                        
                                        <td class="td_Label_Curto">
                                            <asp:Label ID="labelAtivo" runat="server" CssClass="labelRequired" Text="Ativo:"/>                      
                                        </td>
                                        <td>
                                            <dxe:ASPxButtonEdit ID="btnEditAtivoBolsa" runat="server" Width="70%" CssClass="textAtivoCurto" MaxLength="25" 
                                                                ClientInstanceName="btnEditAtivoBolsa" Text="<%#Bind('CdAtivoBolsa')%>" 
                                                                OnLoad="btnEditAtivoBolsa_Load">            
                                            <Buttons>
                                                <dxe:EditButton></dxe:EditButton>                
                                            </Buttons>           
                                            <ClientSideEvents                              
                                                 ButtonClick="function(s, e) {popupAtivoBolsa.ShowAtElementByID(s.name);}" 
                                                 LostFocus="function(s, e) {popupMensagemAtivoBolsa.HideWindow(); 
                                                                            if (btnEditAtivoBolsa.GetValue() != null)
                                                                            {
                                                                                ASPxCallback1.SendCallback(btnEditAtivoBolsa.GetValue());
                                                                            }
                                                                            }"
                                            />                    
                                            </dxe:ASPxButtonEdit>
                                        </td>                                                               
                                    </tr> 
                                    
                                    <td class="td_Label">
                                        <asp:Label ID="labelTipoOperacao" runat="server" CssClass="labelRequired" Text="Tipo:"></asp:Label>                    
                                    </td>                    
                                    <td>      
                                        <dxe:ASPxComboBox ID="dropTipo" runat="server" 
                                                            ShowShadow="false" DropDownStyle="DropDownList"
                                                            CssClass="dropDownListCurto" Text='<%#Eval("Tipo")%>'>
                                        <Items>                                        
                                        <dxe:ListEditItem Value="1" Text="Aumento Cap." />
                                        <dxe:ListEditItem Value="2" Text="Redução Cap." />                                                                               
                                        <dxe:ListEditItem Value="3" Text="Outros" />
                                        </Items>                                        
                                        </dxe:ASPxComboBox>                 
                                    </td>                                       
                                    
                                    <tr>
                                        <td class="td_Label_Curto">
                                            <asp:Label ID="labelFator" runat="server" CssClass="labelRequired" Text="Quantidade:"/>
                                        </td>
                                        <td>
                                            <dxe:ASPxSpinEdit ID="textQuantidade" runat="server" CssClass="textValor_5" ClientInstanceName="textQuantidade"
                                    		      MaxLength="15" NumberType="Float" DecimalPlaces="2" Text="<%#Bind('Quantidade')%>">            
                                            </dxe:ASPxSpinEdit>                                        
                                        </td>                                                        
                                    </tr>
                                    
                                    <tr>
                                        <td class="td_Label_Curto">
                                            <asp:Label ID="label1" runat="server" CssClass="labelRequired" Text="Valor:"/>
                                        </td>
                                        <td>
                                            <dxe:ASPxSpinEdit ID="textValor" runat="server" CssClass="textValor_5" ClientInstanceName="textValor"
                                    		      MaxLength="15" NumberType="Float" DecimalPlaces="2" Text="<%#Bind('Valor')%>">            
                                            </dxe:ASPxSpinEdit>                                        
                                        </td>                                                        
                                    </tr>
                                    
                                    <tr>
                                        <td  class="td_Label">
                                            <asp:Label ID="labelDescricao" runat="server" CssClass="labelNormal" Text="Descrição:"> </asp:Label>
                                        </td>
                                        <td colspan="5">
                                            <asp:TextBox ID="textDescricao" runat="server" TextMode="MultiLine" Rows="2" CssClass="textLongo5" MaxLength="200" Text='<%#Eval("Descricao")%>'/>
                                        </td>
                                    
                                </table>
                                
                                <div class="linhaH"></div>
                        
                                <div class="linkButton linkButtonNoBorder popupFooter">
                                    <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK" 
                                                            OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal5" runat="server" Text="OK"/><div></div></asp:LinkButton>     
                                    <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel" OnClientClick="gridCadastro.CancelEdit(); return false;"><asp:Literal ID="Literal9" runat="server" Text="Cancelar"/><div></div></asp:LinkButton>
                                </div>    
                            </div>
                        </EditForm>
                        <StatusBar>
                            <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" />
                        </StatusBar>            
                    </Templates>
                    
                    <SettingsPopup EditForm-Width="250px" />
                                        
                    <Templates>
                    <StatusBar>
                        <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" />
                    </StatusBar>            
                    </Templates>
                    <SettingsCommandButton>
                        <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                    </SettingsCommandButton>                        
                </dxwgv:ASPxGridView>            
            </div>                          
    </div>
    </div>
    </td></tr></table>
    </div>
                       
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
        
    <cc1:esDataSource ID="EsDSEvolucaoCapitalBolsa" runat="server" OnesSelect="EsDSEvolucaoCapitalBolsa_esSelect" />
    <cc1:esDataSource ID="EsDSAtivoBolsa" runat="server" OnesSelect="EsDSAtivoBolsa_esSelect" />
        
    </form>
</body>
</html>