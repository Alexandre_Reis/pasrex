﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PosicaoBolsaDetalheCarteira.aspx.cs" Inherits="CadastrosBasicos_PosicaoBolsaDetalheCarteira" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">

    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />    
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    var popup = true;
    document.onkeydown=onDocumentKeyDown;  
    var operacao = '';
          
    function OnGetDataCliente(data) {
        btnEditCodigoCliente.SetValue(data);
        popupCliente.HideWindow();
        ASPxCallback1.SendCallback(btnEditCodigoCliente.GetValue());
        btnEditCodigoCliente.Focus();
    }
    
    function OnGetDataAtivoBolsa(data) {
        btnEditAtivoBolsa.SetValue(data);
        popupAtivoBolsa.HideWindow();
        ASPxCallback2.SendCallback(btnEditAtivoBolsa.GetValue());
        btnEditAtivoBolsa.Focus();
    }
    
    function OpenPopup(value)
    {
        if (value != 21016)
        {
            gridCadastro.StartEditRow(selectedIndex);
        }
        else
        {
            alert('Não é possível atualizações/inserções na carteira livre, em vez disto realize as alterações (ou inclusões) na(s) carteira(s) bloqueada(s).');
        }
    }
    </script>
</head>

<body>
    <form id="form1" runat="server">
    
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '') {            
                alert(e.result);                              
            }
            else {
                if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new') {             
                    gridCadastro.UpdateEdit();
                }
                else {
                    callbackAdd.SendCallback();
                }    
            }
            
            operacao = '';
        }
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {
            alert('Operação feita com sucesso.');
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {    
            var textNome = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNome');            
            OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigoCliente, textNome);                        
            }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="ASPxCallback2" runat="server" OnCallback="ASPxCallback2_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {
            if (e.result == '' && btnEditAtivoBolsa.GetValue() != null)
            {
                popupMensagemAtivoBolsa.SetContentHTML(document.getElementById('textMsgNaoExiste').value);
                popupMensagemAtivoBolsa.ShowAtElementByID(btnEditAtivoBolsa.name);
                btnEditAtivoBolsa.SetValue(''); 
                btnEditAtivoBolsa.Focus();                 
            }            
        }        
        "/>
    </dxcb:ASPxCallback>
        
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">

    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Detalhamento de Posição de Carteira (Bolsa)"></asp:Label>
    </div>
    
    <div id="mainContent">
                   
            <div class="linkButton" >               
               <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal10" runat="server" Text="Novo"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;"><asp:Literal ID="Literal1" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal2" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal3" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick="gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal4" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
            </div>
    
            <div class="divDataGrid">            
            <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true" 
                KeyFieldName="CompositeKey" DataSourceID="EsDSPosicaoBolsaDetalheCarteira"
                OnCustomCallback="gridCadastro_CustomCallback"
                OnRowInserting="gridCadastro_RowInserting"
                OnRowUpdating="gridCadastro_RowUpdating"
                OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData"
                OnBeforeGetCallbackResult="gridCadastro_PreRender"
                OnCustomJSProperties="gridCadastro_CustomJSProperties" >        
                    
            <Columns>           
                <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                    <HeaderTemplate>
                        <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                    </HeaderTemplate>
                </dxwgv:GridViewCommandColumn>            
                
                <dxwgv:GridViewDataColumn FieldName="CompositeKey" UnboundType="String" Visible="false">
                </dxwgv:GridViewDataColumn>
                
                <dxwgv:GridViewDataDateColumn FieldName="DataHistorico" Caption="Data" VisibleIndex="2" Width="10%"/>
                
                <dxwgv:GridViewDataColumn FieldName="IdCliente" VisibleIndex="4" Width="10%" CellStyle-HorizontalAlign="left">
                </dxwgv:GridViewDataColumn>
                
                <dxwgv:GridViewDataColumn FieldName="Apelido" Caption="Nome" VisibleIndex="5" Width="20%">
                </dxwgv:GridViewDataColumn>
                
                <dxwgv:GridViewDataComboBoxColumn FieldName="IdAgente" Caption="Corretora" VisibleIndex="8" Width="15%">
                    <PropertiesComboBox DataSourceID="EsDSAgenteMercado" TextField="Nome" ValueField="IdAgente">
                    </PropertiesComboBox>
                </dxwgv:GridViewDataComboBoxColumn>
                
                <dxwgv:GridViewDataColumn FieldName="CdAtivoBolsa" Caption="Código" VisibleIndex="10" Width="8%">
                </dxwgv:GridViewDataColumn>
                
                <dxwgv:GridViewDataComboBoxColumn FieldName="TipoCarteira" VisibleIndex="12" Width="15%">
                    <PropertiesComboBox DataSourceID="EsDSTipoCarteira" TextField="Descricao" ValueField="IdCarteiraBolsa"/>                       
                </dxwgv:GridViewDataComboBoxColumn>
                
                <dxwgv:GridViewDataSpinEditColumn FieldName="Quantidade" VisibleIndex="14" Width="12%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">                                                                                                
                    <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}" SpinButtons-ShowIncrementButtons="false"></PropertiesSpinEdit>
                </dxwgv:GridViewDataSpinEditColumn>                
                
                <dxwgv:GridViewDataDateColumn FieldName="QuantidadeAbertura" Visible="false"/>
                
            </Columns>
            
            <Templates>
                
                <EditForm>                                                                   
                    <div class="editForm">       
                    
                        <table>
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelDataHistorico" runat="server" CssClass="labelRequired" Text="Data:"  /></td> 
                                <td colspan="2">
                                    <dxe:ASPxDateEdit ID="textDataHistorico" runat="server" ClientInstanceName="textDataHistorico" Value='<%#Eval("DataHistorico")%>' OnInit="textDataHistorico_Init"/>
                                </td>  
                            </tr>
                                                    
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelCliente" runat="server" CssClass="labelRequired" Text="Cliente:"></asp:Label>
                                </td>        
                                                            
                                <td>                        
                                    <dxe:ASPxSpinEdit ID="btnEditCodigoCliente" runat="server" CssClass="textButtonEdit" ClientInstanceName="btnEditCodigoCliente"
                                                                Text='<%# Eval("IdCliente") %>' MaxLength="10" NumberType="Integer" 
                                                                OnLoad="btnEditCodigoCliente_Load">            
                                    <Buttons>
                                        <dxe:EditButton>
                                        </dxe:EditButton>                                
                                    </Buttons>  
                                    <ClientSideEvents                                                           
                                             KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNome').value = '';} " 
                                             ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" 
                                             LostFocus="function(s, e) {OnLostFocus(popupMensagemCliente, ASPxCallback1, btnEditCodigoCliente);}"
                                            />               
                                    </dxe:ASPxSpinEdit>
                                </td> 
                                    
                                <td>
                                    <asp:TextBox ID="textNome" runat="server" CssClass="textNome" Enabled="false" Text='<%#Eval("Apelido")%>' ></asp:TextBox>
                                </td>                        
                            </tr>
                            
                            <tr>                                        
                                <td class="td_Label">
                                    <asp:Label ID="labelAgente" runat="server" CssClass="labelRequired" AssociatedControlID="dropAgente" Text="Agente:"/>                                    
                                </td>                                                    
                                <td colspan="2">
                                    <dxe:ASPxComboBox ID="dropAgente" runat="server" DataSourceID="EsDSAgenteMercado"
                                                     ValueField="IdAgente" TextField="Nome"
                                                     CssClass="dropDownList" Text='<%#Eval("IdAgente")%>' OnLoad="dropAgente_Load">
                                    </dxe:ASPxComboBox>                                                                               
                                </td>    
                            </tr>
                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelAtivo" runat="server" CssClass="labelRequired" Text="Ativo:"></asp:Label>                        
                                </td>
                                <td colspan="2">
                                    <dxe:ASPxButtonEdit ID="btnEditAtivoBolsa" runat="server" Width="45%" CssClass="textAtivoCurto" MaxLength="25" 
                                                        ClientInstanceName="btnEditAtivoBolsa" Text='<%#Eval("CdAtivoBolsa")%>' OnLoad="btnEditAtivoBolsa_Load" >            
                                    <Buttons>
                                        <dxe:EditButton></dxe:EditButton>                
                                    </Buttons>           
                                    <ClientSideEvents    
                                         ButtonClick="function(s, e) {popupAtivoBolsa.ShowAtElementByID(s.name);}" 
                                         LostFocus="function(s, e) {popupMensagemAtivoBolsa.HideWindow();
                                                                    if (btnEditAtivoBolsa.GetValue() != null)
                                                                    {
                                                                        ASPxCallback2.SendCallback(btnEditAtivoBolsa.GetValue());
                                                                    }
                                                                    }"
                                    />                    
                                    </dxe:ASPxButtonEdit>
                                </td>    
                            </tr>
                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelTipoCarteira" runat="server" CssClass="labelRequired" Text="Carteira:"/>
                                </td>                    
                                <td colspan="2">      
                                
                                <dxe:ASPxComboBox ID="dropTipoCarteira" runat="server" DataSourceID="EsDSTipoCarteira" ClientInstanceName="dropTipoCarteira"
                                                        ShowShadow="false" IncrementalFilteringMode="Contains" DropDownStyle="DropDown" OnLoad="dropTipoCarteira_Load"
                                                        CssClass="dropDownListCurto" Text='<%#Eval("TipoCarteira")%>'
                                                        TextField="Descricao" ValueField="IdCarteiraBolsa"      
                                                        >                                              
                                            <ClientSideEvents KeyDown="function(s, e) {if (e.htmlEvent.keyCode == 46) {s.SetSelectedIndex(-1); } }" />
                                </dxe:ASPxComboBox>
                                                                                                
                                </td>
                            </tr>
                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelQuantidade" runat="server" CssClass="labelRequired" Text="Quantidade:"></asp:Label>
                                </td>
                                <td colspan="2">
                                    <dxe:ASPxSpinEdit ID="textQuantidade" runat="server" ClientInstanceName="textQuantidade"
                                        Text='<%# Eval("Quantidade") %>' NumberType="Float" MaxLength="16" DecimalPlaces="2"
                                        CssClass="textValor">
                                    </dxe:ASPxSpinEdit>
                                </td>                                                        
                            </tr>
                            
                        </table>
                        
                        <div class="linhaH"></div>
                
                        <div class="linkButton linkButtonNoBorder popupFooter">
                        
                            <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd" OnInit="btnOKAdd_Init"
                               OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal12" runat="server" Text="OK+"/><div></div></asp:LinkButton>

                            <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                            OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal5" runat="server" Text="OK"/><div></div></asp:LinkButton>     
                            <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel" OnClientClick="gridCadastro.CancelEdit(); return false;"><asp:Literal ID="Literal6" runat="server" Text="Cancelar"/><div></div></asp:LinkButton>
                        </div>
                    </div>                    
                </EditForm>
                
            </Templates>
            
            <SettingsPopup EditForm-Width="350px" />
            <SettingsCommandButton>
                <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
            </SettingsCommandButton>
            
        </dxwgv:ASPxGridView>            
        </div>
                    
    </div>
    </div>
    </td></tr></table>
    </div>        
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
        
    <cc1:esDataSource ID="EsDSPosicaoBolsaDetalheCarteira" runat="server" OnesSelect="EsDSPosicaoBolsaDetalheCarteira_esSelect" LowLevelBind="true" />
    <cc1:esDataSource ID="EsDSAgenteMercado" runat="server" OnesSelect="EsDSAgenteMercado_esSelect" />
    <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" />
    <cc1:esDataSource ID="EsDSAtivoBolsa" runat="server" OnesSelect="EsDSAtivoBolsa_esSelect" />
    <cc1:esDataSource ID="EsDSTipoCarteira" runat="server" OnesSelect="EsDSTipoCarteira_esSelect" />
        
    </form>
</body>
</html>