﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Bolsa;
using Financial.Investidor;
using Financial.Security;
using Financial.Web.Common;
using Financial.Security.Enums;
using Financial.Web.Util;
using Financial.Investidor.Enums;

public partial class CadastrosBasicos_PerfilCorretagemBolsa : CadastroBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasPopupCliente = true;
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSPerfilCorretagemBolsa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        PerfilCorretagemBolsaQuery perfilCorretagemBolsaQuery = new PerfilCorretagemBolsaQuery("P");
        ClienteQuery clienteQuery = new ClienteQuery("C");
        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("Q");

        perfilCorretagemBolsaQuery.Select(perfilCorretagemBolsaQuery, clienteQuery.Apelido.As("Apelido"));
        perfilCorretagemBolsaQuery.InnerJoin(clienteQuery).On(perfilCorretagemBolsaQuery.IdCliente == clienteQuery.IdCliente);
        perfilCorretagemBolsaQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        perfilCorretagemBolsaQuery.Where(permissaoClienteQuery.IdUsuario.Equal(idUsuario));

        perfilCorretagemBolsaQuery.OrderBy(perfilCorretagemBolsaQuery.IdCliente.Ascending, perfilCorretagemBolsaQuery.IdAgente.Ascending);

        PerfilCorretagemBolsaCollection coll = new PerfilCorretagemBolsaCollection();
        coll.Load(perfilCorretagemBolsaQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ClienteCollection coll = new ClienteCollection();
        coll.BuscaClientesComAcesso(Context.User.Identity.Name);
        
        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSTemplateCorretagemBolsa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        TemplateCorretagemBolsaCollection coll = new TemplateCorretagemBolsaCollection();

        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSAgenteMercado_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        AgenteMercadoCollection coll = new AgenteMercadoCollection();

        coll.Query.Where(coll.Query.FuncaoCorretora.Equal("S"));
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") {
                if (cliente.IsAtivo) {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name))
                    {
                        nome = cliente.str.Apelido;
                    }
                    else {
                        nome = "no_access";
                    }
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        SalvarNovo();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxComboBox dropAgente = gridCadastro.FindEditFormTemplateControl("dropAgente") as ASPxComboBox;
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        ASPxComboBox dropTemplate = gridCadastro.FindEditFormTemplateControl("dropTemplate") as ASPxComboBox;
        ASPxSpinEdit textPercentualDesconto = gridCadastro.FindEditFormTemplateControl("textPercentualDesconto") as ASPxSpinEdit;
        ASPxComboBox dropTipoMercado = gridCadastro.FindEditFormTemplateControl("dropTipoMercado") as ASPxComboBox;
        ASPxSpinEdit textISS = gridCadastro.FindEditFormTemplateControl("textISS") as ASPxSpinEdit;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(btnEditCodigoCliente);
        controles.Add(dropAgente);
        controles.Add(textDataReferencia);
        controles.Add(dropTemplate);
        controles.Add(textPercentualDesconto);
        controles.Add(textISS);
        
        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        if (gridCadastro.IsNewRowEditing) {
            int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
            int idAgente = Convert.ToInt32(dropAgente.SelectedItem.Value);
            DateTime dataReferencia = Convert.ToDateTime(textDataReferencia.Text);
            string tipoMercado = "";

            if (dropTipoMercado.SelectedIndex > -1)
            {
                Convert.ToString(dropTipoMercado.SelectedItem.Value);
            }

            PerfilCorretagemBolsa perfilCorretagemBolsa = new PerfilCorretagemBolsa();
            if (perfilCorretagemBolsa.LoadByPrimaryKey(idCliente, idAgente, dataReferencia, tipoMercado)) {
                e.Result = "Registro já existente";
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackClone_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        TextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as TextBox;
        ASPxSpinEdit textPercentualDesconto = gridCadastro.FindEditFormTemplateControl("textPercentualDesconto") as ASPxSpinEdit;
        ASPxSpinEdit textISS = gridCadastro.FindEditFormTemplateControl("textISS") as ASPxSpinEdit;
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        ASPxComboBox dropAgente = gridCadastro.FindEditFormTemplateControl("dropAgente") as ASPxComboBox;
        ASPxComboBox dropTemplate = gridCadastro.FindEditFormTemplateControl("dropTemplate") as ASPxComboBox;
        ASPxComboBox dropTipoMercado = gridCadastro.FindEditFormTemplateControl("dropTipoMercado") as ASPxComboBox;

        int idClienteBase = Convert.ToInt32(btnEditCodigoCliente.Text);
        int idAgente = Convert.ToInt32(dropAgente.SelectedItem.Value);
        DateTime dataReferencia = Convert.ToDateTime(textDataReferencia.Text);
        int idTemplate = Convert.ToInt32(dropTemplate.SelectedItem.Value);
        decimal percentualDesconto = Convert.ToDecimal(textPercentualDesconto.Text);
        decimal iss = Convert.ToDecimal(textISS.Text);

        string tipoMercado = "";        
        if (dropTipoMercado.SelectedItem != null) {
            tipoMercado = Convert.ToString(dropTipoMercado.SelectedItem.Value);
        }

        ClienteCollection clienteCollection = new ClienteCollection();
        clienteCollection.Query.Select(clienteCollection.Query.IdCliente);
        clienteCollection.Query.Where(clienteCollection.Query.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo),
                                      clienteCollection.Query.IdCliente.NotEqual(idClienteBase));
        clienteCollection.Query.Load();

        foreach (Cliente cliente in clienteCollection)
        {
            int idCliente = cliente.IdCliente.Value;

            PerfilCorretagemBolsa perfilCorretagemBolsa = new PerfilCorretagemBolsa();
            if (!perfilCorretagemBolsa.LoadByPrimaryKey(idCliente, idAgente, dataReferencia, tipoMercado))
            {
                perfilCorretagemBolsa = new PerfilCorretagemBolsa();
                perfilCorretagemBolsa.IdCliente = idCliente;
                perfilCorretagemBolsa.IdAgente = idAgente;
                perfilCorretagemBolsa.DataReferencia = dataReferencia;
                perfilCorretagemBolsa.IdTemplate = idTemplate;
                perfilCorretagemBolsa.PercentualDesconto = percentualDesconto;
                perfilCorretagemBolsa.Iss = iss;
                perfilCorretagemBolsa.TipoMercado = tipoMercado;
                perfilCorretagemBolsa.Save();
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnClone_Init(object sender, EventArgs e)
    {
        if (gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textDataReferencia_Init(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing)
            (sender as ASPxDateEdit).Enabled = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOKAdd_Init(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditCodigoCliente_Load(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxSpinEdit).Enabled = false;
            (sender as ASPxSpinEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropAgente_Load(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxComboBox).Enabled = false;
            (sender as ASPxComboBox).BackColor = Color.FromName("#EBEBEB");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropTipoMercado_Load(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxComboBox).Enabled = false;
            (sender as ASPxComboBox).BackColor = Color.FromName("#EBEBEB");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) {
        if (e.Column.FieldName == "CompositeKey") {
            //Compose a primary key value
            string idCliente = Convert.ToString(e.GetListSourceFieldValue(PerfilCorretagemBolsaMetadata.ColumnNames.IdCliente));
            string idAgente = Convert.ToString(e.GetListSourceFieldValue(PerfilCorretagemBolsaMetadata.ColumnNames.IdAgente));
            string dataReferencia = Convert.ToString(e.GetListSourceFieldValue(PerfilCorretagemBolsaMetadata.ColumnNames.DataReferencia));
            string tipoMercado = Convert.ToString(e.GetListSourceFieldValue(PerfilCorretagemBolsaMetadata.ColumnNames.TipoMercado));
            e.Value = idCliente + idAgente + dataReferencia + tipoMercado;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        PerfilCorretagemBolsa perfilCorretagemBolsa = new PerfilCorretagemBolsa();

        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        TextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as TextBox;
        ASPxSpinEdit textPercentualDesconto = gridCadastro.FindEditFormTemplateControl("textPercentualDesconto") as ASPxSpinEdit;
        ASPxSpinEdit textISS = gridCadastro.FindEditFormTemplateControl("textISS") as ASPxSpinEdit;
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        ASPxComboBox dropAgente = gridCadastro.FindEditFormTemplateControl("dropAgente") as ASPxComboBox;
        ASPxComboBox dropTemplate = gridCadastro.FindEditFormTemplateControl("dropTemplate") as ASPxComboBox;
        ASPxComboBox dropTipoMercado = gridCadastro.FindEditFormTemplateControl("dropTipoMercado") as ASPxComboBox;

        int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
        int idAgente = Convert.ToInt32(dropAgente.SelectedItem.Value);
        DateTime dataReferencia = Convert.ToDateTime(textDataReferencia.Text);
        int idTemplate = Convert.ToInt32(dropTemplate.SelectedItem.Value);
        decimal percentualDesconto = Convert.ToDecimal(textPercentualDesconto.Text);
        decimal iss = Convert.ToDecimal(textISS.Text);
        
        string tipoMercado = "";
        
        if (dropTipoMercado.SelectedItem != null) {
            tipoMercado = Convert.ToString(dropTipoMercado.SelectedItem.Value);
        }

        if (perfilCorretagemBolsa.LoadByPrimaryKey(idCliente, idAgente, dataReferencia, tipoMercado)) {
            perfilCorretagemBolsa.PercentualDesconto = percentualDesconto;
            perfilCorretagemBolsa.Iss = iss;
            perfilCorretagemBolsa.IdTemplate = idTemplate;

            perfilCorretagemBolsa.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de PerfilCorretagemBolsa - Operacao: Update PerfilCorretagemBolsa: " + idCliente + "; " + idAgente + "; " + dataReferencia + "; " + tipoMercado + UtilitarioWeb.ToString(perfilCorretagemBolsa),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    private void SalvarNovo() {
        PerfilCorretagemBolsa perfilCorretagemBolsa = new PerfilCorretagemBolsa();

        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        TextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as TextBox;
        ASPxSpinEdit textPercentualDesconto = gridCadastro.FindEditFormTemplateControl("textPercentualDesconto") as ASPxSpinEdit;
        ASPxSpinEdit textISS = gridCadastro.FindEditFormTemplateControl("textISS") as ASPxSpinEdit;
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        ASPxComboBox dropAgente = gridCadastro.FindEditFormTemplateControl("dropAgente") as ASPxComboBox;
        ASPxComboBox dropTemplate = gridCadastro.FindEditFormTemplateControl("dropTemplate") as ASPxComboBox;
        ASPxComboBox dropTipoMercado = gridCadastro.FindEditFormTemplateControl("dropTipoMercado") as ASPxComboBox;

        int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
        int idAgente = Convert.ToInt32(dropAgente.SelectedItem.Value);
        DateTime dataReferencia = Convert.ToDateTime(textDataReferencia.Text);
        int idTemplate = Convert.ToInt32(dropTemplate.SelectedItem.Value);
        decimal percentualDesconto = Convert.ToDecimal(textPercentualDesconto.Text);
        decimal iss = Convert.ToDecimal(textISS.Text);
        string tipoMercado = Convert.ToString(dropTipoMercado.SelectedItem.Value);

        perfilCorretagemBolsa.IdCliente = idCliente;
        perfilCorretagemBolsa.IdAgente = idAgente;
        perfilCorretagemBolsa.DataReferencia = dataReferencia;
        perfilCorretagemBolsa.TipoMercado = tipoMercado;
        perfilCorretagemBolsa.PercentualDesconto = percentualDesconto;
        perfilCorretagemBolsa.Iss = iss;
        perfilCorretagemBolsa.IdTemplate = idTemplate;

        perfilCorretagemBolsa.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de PerfilCorretagemBolsa - Operacao: Insert PerfilCorretagemBolsa: " + perfilCorretagemBolsa.IdCliente + "; " + perfilCorretagemBolsa.IdAgente + "; " + perfilCorretagemBolsa.DataReferencia + "; " + perfilCorretagemBolsa.TipoMercado + UtilitarioWeb.ToString(perfilCorretagemBolsa),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesIdCliente = gridCadastro.GetSelectedFieldValues(PerfilCorretagemBolsaMetadata.ColumnNames.IdCliente);
            List<object> keyValuesIdAgente = gridCadastro.GetSelectedFieldValues(PerfilCorretagemBolsaMetadata.ColumnNames.IdAgente);
            List<object> keyValuesDataReferencia = gridCadastro.GetSelectedFieldValues(PerfilCorretagemBolsaMetadata.ColumnNames.DataReferencia);
            List<object> keyValuesTipoMercado = gridCadastro.GetSelectedFieldValues(PerfilCorretagemBolsaMetadata.ColumnNames.TipoMercado);
            
            for (int i = 0; i < keyValuesIdCliente.Count; i++) {
                int idCliente = Convert.ToInt32(keyValuesIdCliente[i]);
                int idAgente = Convert.ToInt32(keyValuesIdAgente[i]);
                DateTime dataReferencia = Convert.ToDateTime(keyValuesDataReferencia[i]);
                string tipoMercado = Convert.ToString(keyValuesTipoMercado[i]);

                PerfilCorretagemBolsa perfilCorretagemBolsa = new PerfilCorretagemBolsa();
                if (perfilCorretagemBolsa.LoadByPrimaryKey(idCliente, idAgente, dataReferencia, tipoMercado)) {

                    //
                    PerfilCorretagemBolsa perfilCorretagemBolsaClone = (PerfilCorretagemBolsa)Utilitario.Clone(perfilCorretagemBolsa);
                    //
                    
                    perfilCorretagemBolsa.MarkAsDeleted();
                    perfilCorretagemBolsa.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de PerfilCorretagemBolsa - Operacao: Delete PerfilCorretagemBolsa: " + perfilCorretagemBolsaClone.IdCliente + "; " + perfilCorretagemBolsaClone.IdAgente + "; " + perfilCorretagemBolsaClone.DataReferencia + "; " + perfilCorretagemBolsaClone.TipoMercado + UtilitarioWeb.ToString(perfilCorretagemBolsaClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing) {
            ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
            e.Properties["cpBtnEditCodigoCliente"] = btnEditCodigoCliente.ClientID;

            ASPxSpinEdit textPercentualDesconto = gridCadastro.FindEditFormTemplateControl("textPercentualDesconto") as ASPxSpinEdit;
            e.Properties["cpTextPercentualDesconto"] = textPercentualDesconto.ClientID;

            ASPxSpinEdit textISS = gridCadastro.FindEditFormTemplateControl("textISS") as ASPxSpinEdit;
            e.Properties["cpTextISS"] = textISS.ClientID;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("btnEditCodigoCliente", "textPercentualDesconto");
        base.gridCadastro_PreRender(sender, e);
    }
}