﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using Financial.Investidor;
using Financial.ContaCorrente;
using System.Drawing;
using Financial.ContaCorrente.Enums;
using Financial.Security;
using Financial.Fundo;
using Financial.InvestidorCotista;
using Financial.InvestidorCotista.Enums;
using Financial.Investidor.Enums;
using Financial.Common.Enums;
using Financial.Bolsa;
using Financial.Bolsa.Enums;
using DevExpress.Web.Data;
using Financial.Web.Common;
using System.Threading;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_OperacaoEmprestimoBolsa : Financial.Web.Common.CadastroBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasFiltro = true;
        this.HasPopupCliente = true;
        this.HasPopupAtivoBolsa = true;
        this.HasPanelFieldsLoading = true;
        base.Page_Load(sender, e);

        hiddenFrontOffice.Value = Convert.ToString(Request["param"] == "Frontoffice");

        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
                  new List<string>(new string[] { OperacaoEmprestimoBolsaMetadata.ColumnNames.PontaEmprestimo }));
    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSOperacaoEmprestimoBolsa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
        OperacaoEmprestimoBolsaQuery operacaoEmprestimoBolsaQuery = new OperacaoEmprestimoBolsaQuery("O");
        ClienteQuery clienteQuery = new ClienteQuery("C");

        operacaoEmprestimoBolsaQuery.Select(operacaoEmprestimoBolsaQuery, clienteQuery.Apelido.As("Apelido"));
        operacaoEmprestimoBolsaQuery.InnerJoin(clienteQuery).On(operacaoEmprestimoBolsaQuery.IdCliente == clienteQuery.IdCliente);
        operacaoEmprestimoBolsaQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        operacaoEmprestimoBolsaQuery.Where(permissaoClienteQuery.IdUsuario.Equal(idUsuario));

        if (!String.IsNullOrEmpty(btnEditCodigoClienteFiltro.Text)) {
            operacaoEmprestimoBolsaQuery.Where(operacaoEmprestimoBolsaQuery.IdCliente.Equal(Convert.ToInt32(btnEditCodigoClienteFiltro.Text)));
        }

        if (!String.IsNullOrEmpty(textDataInicio.Text)) {
            operacaoEmprestimoBolsaQuery.Where(operacaoEmprestimoBolsaQuery.DataRegistro.GreaterThanOrEqual(textDataInicio.Text));
        }

        if (!String.IsNullOrEmpty(textDataFim.Text)) {
            operacaoEmprestimoBolsaQuery.Where(operacaoEmprestimoBolsaQuery.DataRegistro.LessThanOrEqual(textDataFim.Text));
        }

        if (!String.IsNullOrEmpty(textCdAtivo.Text)) {
            operacaoEmprestimoBolsaQuery.Where(operacaoEmprestimoBolsaQuery.CdAtivoBolsa.Like("%" + textCdAtivo.Text + "%"));
        }

        if (dropPontaEmprestimoFiltro.SelectedIndex > -1) {
            operacaoEmprestimoBolsaQuery.Where(operacaoEmprestimoBolsaQuery.PontaEmprestimo.Equal(Convert.ToString(dropPontaEmprestimoFiltro.SelectedItem.Value)));
        }

        operacaoEmprestimoBolsaQuery.OrderBy(operacaoEmprestimoBolsaQuery.DataRegistro.Descending);

        OperacaoEmprestimoBolsaCollection coll = new OperacaoEmprestimoBolsaCollection();
        coll.Load(operacaoEmprestimoBolsaQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ClienteCollection coll = new ClienteCollection();
        coll.BuscaClientesComAcesso(Context.User.Identity.Name);
        
        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSAgenteMercado_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        AgenteMercadoCollection coll = new AgenteMercadoCollection();
        coll.Query.Where(coll.Query.FuncaoCorretora.Equal("S"));
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSAtivoBolsa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        AtivoBolsaCollection coll = new AtivoBolsaCollection();
        coll.Query.Where(coll.Query.TipoMercado.Equal(Convert.ToString(TipoMercadoBolsa.MercadoVista)));
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSTrader_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        TraderCollection coll = new TraderCollection();
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void panelEdicao_Load(object sender, EventArgs e) {
        UtilitarioGrid.SetaCorCombosPopup(sender as Control);
        //base.panelEdicao_Load(sender, e);

        if (Convert.ToBoolean(hiddenFrontOffice.Value) == false) {
            Label labelTrader = gridCadastro.FindEditFormTemplateControl("labelTrader") as Label;
            ASPxComboBox dropTrader = gridCadastro.FindEditFormTemplateControl("dropTrader") as ASPxComboBox;
            labelTrader.Visible = false;
            dropTrader.Visible = false;
        }

        bool travaPainel = false;
        if (gridCadastro.IsEditing && !gridCadastro.IsNewRowEditing && Convert.ToString(Session["FormLoad"]) != "S") {
            Session["FormLoad"] = "S";
            //Especificacao, FatorCotacao
            string cdAtivoBolsa = gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "CdAtivoBolsa").ToString();
            DateTime dataRegistro = Convert.ToDateTime(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "DataRegistro"));

            TextBox textEspecificacao = gridCadastro.FindEditFormTemplateControl("textEspecificacao") as TextBox;
            ASPxSpinEdit textFatorCotacao = gridCadastro.FindEditFormTemplateControl("textFatorCotacao") as ASPxSpinEdit;

            AtivoBolsa ativoBolsa = new AtivoBolsa();
            ativoBolsa.LoadByPrimaryKey(cdAtivoBolsa);

            string especificacao = ativoBolsa.Especificacao;

            string fatorCotacao = "";
            FatorCotacaoBolsa fatorCotacaoBolsa = new FatorCotacaoBolsa();
            if (fatorCotacaoBolsa.BuscaFatorCotacaoBolsa(cdAtivoBolsa, dataRegistro)) {
                fatorCotacao = fatorCotacaoBolsa.Fator.Value.ToString();
            }
            textEspecificacao.Text = especificacao;
            textFatorCotacao.Text = fatorCotacao;
            //

            Label labelEdicao = gridCadastro.FindEditFormTemplateControl("labelEdicao") as Label;

            int idCliente = Convert.ToInt32(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "IdCliente"));

            Cliente cliente = new Cliente();
            if (cliente.LoadByPrimaryKey(idCliente)) {
                DateTime dataDia = cliente.DataDia.Value;
                byte status = cliente.Status.Value;
                if (status == (byte)StatusCliente.Divulgado) {
                    travaPainel = true;
                    labelEdicao.Text = "Cliente está fechado. Dados não podem ser alterados.";
                }
                else if (DateTime.Compare(dataDia, dataRegistro) > 0) {
                    travaPainel = true;
                    labelEdicao.Text = "Data informada anterior à data do cliente. Dados não podem ser alterados.";
                }
            }
        }

        if (travaPainel) {
            LinkButton btnOK = gridCadastro.FindEditFormTemplateControl("btnOK") as LinkButton;
            btnOK.Visible = false;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";

        if (!gridCadastro.IsEditing) //Testa deleção
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdCliente");
            List<object> keyData = gridCadastro.GetSelectedFieldValues("DataRegistro");
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idCliente = Convert.ToInt32(keyValuesId[i]);
                DateTime data = Convert.ToDateTime(keyData[i]);

                Cliente cliente = new Cliente();
                if (cliente.LoadByPrimaryKey(idCliente)) {
                    DateTime dataDia = cliente.DataDia.Value;
                    byte status = cliente.Status.Value;
                    string clienteDescricao = cliente.Apelido + " (" + cliente.IdCliente.ToString() + ")";
                    if (status == (byte)StatusCliente.Divulgado) {
                        e.Result = "Cliente " + clienteDescricao + " está fechado! Deleção cancelada.";
                        return;
                    }
                    else if (DateTime.Compare(dataDia, data) > 0) {
                        e.Result = "Lançamento com data " + data.ToString().Substring(0, 10) + " anterior à data do cliente " + clienteDescricao + "! Deleção cancelada.";
                        return;
                    }
                }
            }
        }
        else {
            ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
            ASPxComboBox dropAgente = gridCadastro.FindEditFormTemplateControl("dropAgente") as ASPxComboBox;
            ASPxDateEdit textDataRegistro = gridCadastro.FindEditFormTemplateControl("textDataRegistro") as ASPxDateEdit;
            ASPxDateEdit textDataVencimento = gridCadastro.FindEditFormTemplateControl("textDataVencimento") as ASPxDateEdit;
            ASPxButtonEdit btnEditAtivoBolsa = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa") as ASPxButtonEdit;
            ASPxComboBox dropPontaEmprestimo = gridCadastro.FindEditFormTemplateControl("dropPontaEmprestimo") as ASPxComboBox;
            ASPxSpinEdit textNumeroContrato = gridCadastro.FindEditFormTemplateControl("textNumeroContrato") as ASPxSpinEdit;
            ASPxSpinEdit textTaxaOperacao = gridCadastro.FindEditFormTemplateControl("textTaxaOperacao") as ASPxSpinEdit;
            ASPxSpinEdit textTaxaComissao = gridCadastro.FindEditFormTemplateControl("textTaxaComissao") as ASPxSpinEdit;
            ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
            ASPxSpinEdit textPU = gridCadastro.FindEditFormTemplateControl("textPU") as ASPxSpinEdit;
            ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
            ASPxComboBox dropDevolucaoAntecipada = gridCadastro.FindEditFormTemplateControl("dropDevolucaoAntecipada") as ASPxComboBox;

            #region Campos obrigatórios
            List<Control> controles = new List<Control>();
            controles.Add(btnEditCodigoCliente);
            controles.Add(dropAgente);
            controles.Add(textDataVencimento);
            controles.Add(btnEditAtivoBolsa);
            controles.Add(dropPontaEmprestimo);
            controles.Add(textNumeroContrato);
            controles.Add(textTaxaOperacao);
            controles.Add(textTaxaComissao);
            controles.Add(textQuantidade);
            controles.Add(textPU);
            controles.Add(textValor);
            controles.Add(dropDevolucaoAntecipada);

            if (base.TestaObrigatorio(controles) != "") {
                e.Result = "Campos com * são obrigatórios!";
                return;
            }
            #endregion

            #region Testa Ativo Não Existente
            AtivoBolsa a = new AtivoBolsa();
            string cdAtivoBolsaAux = btnEditAtivoBolsa.Text.ToString().Trim().ToUpper();
            if (!a.LoadByPrimaryKey(cdAtivoBolsaAux)) {
                e.Result = "Ativo não Existente.";
                return;
            }
            #endregion

            int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
            DateTime dataRegistro = Convert.ToDateTime(textDataRegistro.Text);
            DateTime dataVencimento = Convert.ToDateTime(textDataVencimento.Text);
            string cdAtivoBolsa = btnEditAtivoBolsa.Text.ToString().Trim().ToUpper();
            int idAgente = Convert.ToInt32(dropAgente.SelectedItem.Value);

            if (!Calendario.IsDiaUtil(dataVencimento, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil)) {
                e.Result = "Data de vencimento não é dia útil.";
                return;
            }
            else if (DateTime.Compare(dataRegistro, dataVencimento) > 0) {
                e.Result = "Data vencimento deve ser maior que a data de registro! Operação não pode ser realizada.";
                return;
            }

            if (dropDevolucaoAntecipada.SelectedItem.Value.Equals("S"))
            {
                ASPxDateEdit textDataInicialDevolucaoAntecipada = gridCadastro.FindEditFormTemplateControl("textDataInicialDevolucaoAntecipada") as ASPxDateEdit;
                controles.Clear();
                controles.Add(textDataInicialDevolucaoAntecipada);

                if (base.TestaObrigatorio(controles) != "")
                {
                    e.Result = "Campos com * são obrigatórios!";
                    return;
                }

                DateTime dataInicial = Convert.ToDateTime(textDataInicialDevolucaoAntecipada.Text);
                if (DateTime.Compare(dataRegistro, dataInicial) > 0)
                {
                    e.Result = "Data Ínicial deve ser maior ou ígual a data de registro! Operação não pode ser realizada.";
                    return;
                }
            }           

            Cliente cliente = new Cliente();
            if (cliente.LoadByPrimaryKey(idCliente)) {
                DateTime dataDia = cliente.DataDia.Value;
                byte status = cliente.Status.Value;
                if (status == (byte)StatusCliente.Divulgado) {
                    e.Result = "Cliente está fechado! Operação não pode ser realizada.";
                    return;
                }
                else if (DateTime.Compare(dataDia, Convert.ToDateTime(textDataRegistro.Text)) > 0) {
                    e.Result = "Data informada anterior à data do cliente! Operação não pode ser realizada.";
                    return;
                }
            }
            PosicaoBolsaCollection posicaoBolsaCollection;
            int pontaEmprestimo = Convert.ToInt32(dropPontaEmprestimo.SelectedItem.Value);
            if (pontaEmprestimo == (int)PontaEmprestimoBolsa.Doador) //Testa apenas para doador!
            {
                posicaoBolsaCollection = new PosicaoBolsaCollection();
                posicaoBolsaCollection.Query.Where(posicaoBolsaCollection.Query.IdCliente == idCliente,
                                                   posicaoBolsaCollection.Query.CdAtivoBolsa == cdAtivoBolsa);

                if (!posicaoBolsaCollection.Query.Load())
                {
                    e.Result = "O cliente não possui posição neste ativo para ser emprestada.";
                    return;
                }

                posicaoBolsaCollection = new PosicaoBolsaCollection();

                int? idAgenteAux = null;
                ClienteBolsa clienteBolsa = new ClienteBolsa();
                clienteBolsa.LoadByPrimaryKey(idCliente);

                if (!clienteBolsa.IdCustodianteAcoes.HasValue)
                    idAgenteAux = idAgente;

                decimal qtdeDisponivel = posicaoBolsaCollection.RetornaQuantidadeDisponivel(dataRegistro, cdAtivoBolsa, idAgenteAux, idCliente, true, true);

                if (qtdeDisponivel < Convert.ToDecimal(textQuantidade.Text))
                {
                    e.Result = "Quantidade insuficiente para cobrir o empréstimo!!! (Qtde Disponível - " + qtdeDisponivel + ")";
                    return;
                }
            }

        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        string resultado = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") {
                if (cliente.Status.Value != (byte)StatusCliente.Divulgado) {

                    if (cliente.IsAtivo) {
                        PermissaoCliente permissaoCliente = new PermissaoCliente();
                        if (permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name))
                        {
                            nome = cliente.str.Apelido;
                            DateTime dataDia = cliente.DataDia.Value;

                            resultado = this.gridCadastro.IsNewRowEditing
                                        ? nome + "|" + dataDia.ToString().Substring(0, 10) + "|" + Thread.CurrentThread.CurrentCulture.Name
                                        : nome;
                        }
                        else {
                            resultado = "no_access";
                        }
                    }
                    else {
                        resultado = "no_active";
                    }
                }
                else {
                    resultado = cliente.str.Apelido + "|status_closed";
                }
            }
        }
        e.Result = resultado;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string resultado = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            AtivoBolsa ativoBolsa = new AtivoBolsa();

            if (!ativoBolsa.LoadByPrimaryKey(Convert.ToString(e.Parameter))) {
                e.Result = "";
                return;
            }           
            else if (ativoBolsa.str.CdAtivoBolsa != "" || ativoBolsa.TipoMercado != Convert.ToString(TipoMercadoBolsa.MercadoVista)) {
                ASPxDateEdit textDataRegistro = gridCadastro.FindEditFormTemplateControl("textDataRegistro") as ASPxDateEdit;

                string cdAtivoBolsa = ativoBolsa.str.CdAtivoBolsa;
                string especificacao = ativoBolsa.Especificacao;

                //Fator de cotação
                string fatorCotacao = "";
                if (textDataRegistro.Text != "") {
                    FatorCotacaoBolsa fatorCotacaoBolsa = new FatorCotacaoBolsa();
                    if (fatorCotacaoBolsa.BuscaFatorCotacaoBolsa(cdAtivoBolsa, Convert.ToDateTime(textDataRegistro.Text))) {
                        fatorCotacao = fatorCotacaoBolsa.Fator.Value.ToString();
                    }
                    else {
                        e.Result = "noquote";
                        return;
                    }
                }

                resultado = cdAtivoBolsa + "|" + especificacao + "|" + fatorCotacao;
            }
        }
        e.Result = resultado;
    }

    /// <summary>
    /// Configura Visibilidade do botão Ok++
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOKAdd_Init(object sender, EventArgs e) {
        // Se for Update Não Aparece botão de Ok++
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        this.SalvarNovo();
    }

    /// <summary>
    /// Inserção de um novo registro
    /// </summary>
    private void SalvarNovo() {
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxButtonEdit btnEditCodigoAtivoBolsa = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa") as ASPxButtonEdit;
        ASPxComboBox dropAgente = gridCadastro.FindEditFormTemplateControl("dropAgente") as ASPxComboBox;
        ASPxDateEdit textDataRegistro = gridCadastro.FindEditFormTemplateControl("textDataRegistro") as ASPxDateEdit;
        ASPxDateEdit textDataVencimento = gridCadastro.FindEditFormTemplateControl("textDataVencimento") as ASPxDateEdit;
        ASPxComboBox dropPontaEmprestimo = gridCadastro.FindEditFormTemplateControl("dropPontaEmprestimo") as ASPxComboBox;
        ASPxComboBox dropTipoEmprestimo = gridCadastro.FindEditFormTemplateControl("dropTipoEmprestimo") as ASPxComboBox;
        ASPxSpinEdit textTaxaOperacao = gridCadastro.FindEditFormTemplateControl("textTaxaOperacao") as ASPxSpinEdit;
        ASPxSpinEdit textTaxaComissao = gridCadastro.FindEditFormTemplateControl("textTaxaComissao") as ASPxSpinEdit;
        ASPxSpinEdit textNumeroContrato = gridCadastro.FindEditFormTemplateControl("textNumeroContrato") as ASPxSpinEdit;
        ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
        ASPxSpinEdit textPU = gridCadastro.FindEditFormTemplateControl("textPU") as ASPxSpinEdit;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
        ASPxComboBox dropTrader = gridCadastro.FindEditFormTemplateControl("dropTrader") as ASPxComboBox;
        ASPxComboBox dropDevolucaoAntecipada = gridCadastro.FindEditFormTemplateControl("dropDevolucaoAntecipada") as ASPxComboBox;
        ASPxDateEdit textDataInicialDevolucaoAntecipada = gridCadastro.FindEditFormTemplateControl("textDataInicialDevolucaoAntecipada") as ASPxDateEdit;
        //
        OperacaoEmprestimoBolsa operacaoEmprestimoBolsa = new OperacaoEmprestimoBolsa();
        //
        int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
        //
        operacaoEmprestimoBolsa.IdCliente = idCliente;
        operacaoEmprestimoBolsa.CdAtivoBolsa = Convert.ToString(btnEditCodigoAtivoBolsa.Text).ToUpper().Trim();
        operacaoEmprestimoBolsa.IdAgente = Convert.ToInt32(dropAgente.SelectedItem.Value);
        operacaoEmprestimoBolsa.DataRegistro = Convert.ToDateTime(textDataRegistro.Text);
        operacaoEmprestimoBolsa.DataVencimento = Convert.ToDateTime(textDataVencimento.Text);
        operacaoEmprestimoBolsa.PontaEmprestimo = Convert.ToByte(dropPontaEmprestimo.SelectedItem.Value);
        operacaoEmprestimoBolsa.TipoEmprestimo = Convert.ToByte(dropTipoEmprestimo.SelectedItem.Value);
        operacaoEmprestimoBolsa.TaxaOperacao = Convert.ToDecimal(textTaxaOperacao.Text);
        operacaoEmprestimoBolsa.TaxaComissao = Convert.ToDecimal(textTaxaComissao.Text);

        operacaoEmprestimoBolsa.NumeroContrato = 0;
        if (textNumeroContrato.Text.ToString() != "") {
            operacaoEmprestimoBolsa.NumeroContrato = Convert.ToInt32(textNumeroContrato.Text);
        }

        operacaoEmprestimoBolsa.Pu = Convert.ToDecimal(textPU.Text);
        operacaoEmprestimoBolsa.Valor = Convert.ToDecimal(textValor.Text);
        operacaoEmprestimoBolsa.Quantidade = Convert.ToDecimal(textQuantidade.Text);
        operacaoEmprestimoBolsa.Fonte = (byte)FonteEmprestimoBolsa.Manual;
        operacaoEmprestimoBolsa.IdLocal = Convert.ToInt16(LocalFeriadoFixo.Bovespa);

        operacaoEmprestimoBolsa.PermiteDevolucaoAntecipada = dropDevolucaoAntecipada.SelectedItem.Value.ToString();
        if (dropDevolucaoAntecipada.SelectedItem.Value.Equals("S") && !string.IsNullOrEmpty(textDataInicialDevolucaoAntecipada.Text))
            operacaoEmprestimoBolsa.DataInicialDevolucaoAntecipada = Convert.ToDateTime(textDataInicialDevolucaoAntecipada.Text);
        else
            operacaoEmprestimoBolsa.DataInicialDevolucaoAntecipada = null;

        if (Convert.ToBoolean(hiddenFrontOffice.Value) == true && dropTrader.SelectedIndex > 0) {
            operacaoEmprestimoBolsa.IdTrader = Convert.ToInt32(dropTrader.SelectedItem.Value); ;
        }

        operacaoEmprestimoBolsa.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de OperacaoEmprestimoBolsa - Operacao: Insert OperacaoEmprestimoBolsa: " + operacaoEmprestimoBolsa.IdOperacao + UtilitarioWeb.ToString(operacaoEmprestimoBolsa),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        /* Atualiza StatusRealTime para executar */
        Cliente cliente = new Cliente();
        cliente.SetaFlagRealTime(idCliente, (byte)StatusRealTimeCliente.Executar);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        int idOperacao = (int)e.Keys[0];
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxButtonEdit btnEditCodigoAtivoBolsa = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa") as ASPxButtonEdit;
        ASPxComboBox dropAgente = gridCadastro.FindEditFormTemplateControl("dropAgente") as ASPxComboBox;
        ASPxDateEdit textDataRegistro = gridCadastro.FindEditFormTemplateControl("textDataRegistro") as ASPxDateEdit;
        ASPxDateEdit textDataVencimento = gridCadastro.FindEditFormTemplateControl("textDataVencimento") as ASPxDateEdit;
        ASPxComboBox dropPontaEmprestimo = gridCadastro.FindEditFormTemplateControl("dropPontaEmprestimo") as ASPxComboBox;
        ASPxComboBox dropTipoEmprestimo = gridCadastro.FindEditFormTemplateControl("dropTipoEmprestimo") as ASPxComboBox;
        ASPxSpinEdit textTaxaOperacao = gridCadastro.FindEditFormTemplateControl("textTaxaOperacao") as ASPxSpinEdit;
        ASPxSpinEdit textTaxaComissao = gridCadastro.FindEditFormTemplateControl("textTaxaComissao") as ASPxSpinEdit;
        ASPxSpinEdit textNumeroContrato = gridCadastro.FindEditFormTemplateControl("textNumeroContrato") as ASPxSpinEdit;
        ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
        ASPxSpinEdit textPU = gridCadastro.FindEditFormTemplateControl("textPU") as ASPxSpinEdit;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
        ASPxComboBox dropTrader = gridCadastro.FindEditFormTemplateControl("dropTrader") as ASPxComboBox;
        ASPxComboBox dropDevolucaoAntecipada = gridCadastro.FindEditFormTemplateControl("dropDevolucaoAntecipada") as ASPxComboBox;
        ASPxDateEdit textDataInicialDevolucaoAntecipada = gridCadastro.FindEditFormTemplateControl("textDataInicialDevolucaoAntecipada") as ASPxDateEdit;

        //#region Testa Ativo Não Existente
        //AtivoBolsa a = new AtivoBolsa();
        //string cdAtivoBolsaAux = btnEditCodigoAtivoBolsa.Text.ToString().Trim().ToUpper();
        //if (!a.LoadByPrimaryKey(cdAtivoBolsaAux)) {
        //    e.Cancel = true;
        //    return;
        //}
        //#endregion

        OperacaoEmprestimoBolsa operacaoEmprestimoBolsa = new OperacaoEmprestimoBolsa();
        if (operacaoEmprestimoBolsa.LoadByPrimaryKey(idOperacao)) {
            int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);

            operacaoEmprestimoBolsa.IdCliente = idCliente;
            operacaoEmprestimoBolsa.CdAtivoBolsa = Convert.ToString(btnEditCodigoAtivoBolsa.Text).ToUpper().Trim();
            operacaoEmprestimoBolsa.IdAgente = Convert.ToInt32(dropAgente.SelectedItem.Value);
            operacaoEmprestimoBolsa.DataRegistro = Convert.ToDateTime(textDataRegistro.Text);
            operacaoEmprestimoBolsa.DataVencimento = Convert.ToDateTime(textDataVencimento.Text);
            operacaoEmprestimoBolsa.PontaEmprestimo = Convert.ToByte(dropPontaEmprestimo.SelectedItem.Value);
            operacaoEmprestimoBolsa.TipoEmprestimo = Convert.ToByte(dropTipoEmprestimo.SelectedItem.Value);
            operacaoEmprestimoBolsa.TaxaOperacao = Convert.ToDecimal(textTaxaOperacao.Text);
            operacaoEmprestimoBolsa.TaxaComissao = Convert.ToDecimal(textTaxaComissao.Text);

            if (textNumeroContrato.Text.ToString() != "") {
                operacaoEmprestimoBolsa.NumeroContrato = Convert.ToInt32(textNumeroContrato.Text);
            }

            operacaoEmprestimoBolsa.Pu = Convert.ToDecimal(textPU.Text);
            operacaoEmprestimoBolsa.Valor = Convert.ToDecimal(textValor.Text);
            operacaoEmprestimoBolsa.Quantidade = Convert.ToDecimal(textQuantidade.Text);
            operacaoEmprestimoBolsa.Fonte = (byte)FonteEmprestimoBolsa.Manual;
            operacaoEmprestimoBolsa.IdLocal = Convert.ToInt16(LocalFeriadoFixo.Bovespa);

            operacaoEmprestimoBolsa.PermiteDevolucaoAntecipada = dropDevolucaoAntecipada.SelectedItem.Value.ToString();
            if (dropDevolucaoAntecipada.SelectedItem.Value.Equals("S") && !string.IsNullOrEmpty(textDataInicialDevolucaoAntecipada.Text))
                operacaoEmprestimoBolsa.DataInicialDevolucaoAntecipada = Convert.ToDateTime(textDataInicialDevolucaoAntecipada.Text);
            else
                operacaoEmprestimoBolsa.DataInicialDevolucaoAntecipada = null;

            if (Convert.ToBoolean(hiddenFrontOffice.Value) == true && dropTrader.SelectedIndex > 0) {
                operacaoEmprestimoBolsa.IdTrader = Convert.ToInt32(dropTrader.SelectedItem.Value); ;
            }

            operacaoEmprestimoBolsa.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de OperacaoEmprestimoBolsa - Operacao: Update OperacaoEmprestimoBolsa: " + idOperacao + UtilitarioWeb.ToString(operacaoEmprestimoBolsa),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion

            /* Atualiza StatusRealTime para executar */
            Cliente cliente = new Cliente();
            cliente.SetaFlagRealTime(idCliente, (byte)StatusRealTimeCliente.Executar);
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        this.SalvarNovo();
        //
        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdOperacao");
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idOperacao = Convert.ToInt32(keyValuesId[i]);

                OperacaoEmprestimoBolsa operacaoEmprestimoBolsa = new OperacaoEmprestimoBolsa();
                if (operacaoEmprestimoBolsa.LoadByPrimaryKey(idOperacao)) {
                    int idCliente = operacaoEmprestimoBolsa.IdCliente.Value;

                    //
                    OperacaoEmprestimoBolsa operacaoEmprestimoBolsaClone = (OperacaoEmprestimoBolsa)Utilitario.Clone(operacaoEmprestimoBolsa);
                    //

                    operacaoEmprestimoBolsa.MarkAsDeleted();
                    operacaoEmprestimoBolsa.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de OperacaoEmprestimoBolsa - Operacao: Delete OperacaoEmprestimoBolsa: " + idOperacao + UtilitarioWeb.ToString(operacaoEmprestimoBolsaClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion

                    /* Atualiza StatusRealTime para executar */
                    Cliente cliente = new Cliente();
                    cliente.SetaFlagRealTime(idCliente, (byte)StatusRealTimeCliente.Executar);
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing) {
            ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
            e.Properties["cpBtnEditCodigoCliente"] = btnEditCodigoCliente.ClientID;

            ASPxSpinEdit textFatorCotacao = gridCadastro.FindEditFormTemplateControl("textFatorCotacao") as ASPxSpinEdit;
            e.Properties["cpTextFatorCotacao"] = textFatorCotacao.ClientID;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_InitNewRow(object sender, ASPxDataInitNewRowEventArgs e) {
        //Valores default do form
        e.NewValues["TipoEmprestimo"] = "Voluntário";
        e.NewValues["TaxaComissao"] = "0";
        e.NewValues["NumeroContrato"] = "0";
        e.NewValues[OperacaoEmprestimoBolsaMetadata.ColumnNames.PermiteDevolucaoAntecipada] = "S";
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("btnEditCodigoCliente");
        base.gridCadastro_PreRender(sender, e);
        
        ASPxDateEdit textDataInicio = popupFiltro.FindControl("textDataInicio") as ASPxDateEdit;
        ASPxDateEdit textDataFim = popupFiltro.FindControl("textDataFim") as ASPxDateEdit;

        ASPxSpinEdit btnEditCodigoClienteFiltro = popupFiltro.FindControl("btnEditCodigoClienteFiltro") as ASPxSpinEdit;

        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        Label labelFiltroHeader = gridCadastro.FindStatusBarTemplateControl("labelFiltroHeader") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");
        if (btnEditCodigoClienteFiltro != null && btnEditCodigoClienteFiltro.Text != "") {
            texto.Append(" Cliente: ").Append(btnEditCodigoClienteFiltro.Text);
        }
        if (textDataInicio.Text != "") {
            if (texto.Length > 0) {
                texto.Append(" |");
            }
            texto.Append(" Data Início >= ").Append(textDataInicio.Text.Substring(0, 10));
        }
        if (textDataFim.Text != "") {
            if (texto.Length > 0) {
                texto.Append(" |");
            }
            texto.Append(" Data Fim <= ").Append(textDataFim.Text.Substring(0, 10));
        }
        if (textCdAtivo.Text != "") {
            if (texto.Length > 0) {
                texto.Append(" |");
            }
            texto.Append(" Ativo Like %").Append(textCdAtivo.Text).Append("%");
        }
        if (dropPontaEmprestimoFiltro.SelectedIndex > -1) {
            if (texto.Length > 0) {
                texto.Append(" |");
            }

            texto.Append(" Ponta = ").Append(dropPontaEmprestimoFiltro.Text);
        }

        labelFiltro.Text = texto.ToString();
    }
}