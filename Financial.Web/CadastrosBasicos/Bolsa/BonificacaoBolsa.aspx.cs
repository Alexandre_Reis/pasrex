﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.HtmlControls;

using Financial.Common;
using Financial.Util;

using DevExpress.Web;

using Financial.Bolsa;
using Financial.RendaFixa;
using Financial.Bolsa.Enums;
using Financial.Web.Common;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Security;

public partial class CadastrosBasicos_BonificacaoBolsa : CadastroBasePage {

    new protected void Page_Load(object sender, EventArgs e) {
        this.HasPopupAtivoBolsa = true;
        this.HasPopupAtivoBolsa2 = true;
        this.HasFiltro = true;
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSBonificacaoBolsa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        BonificacaoBolsaCollection coll = new BonificacaoBolsaCollection();

        ASPxDateEdit textDataInicio = popupFiltro.FindControl("textDataInicio") as ASPxDateEdit;
        ASPxDateEdit textDataFim = popupFiltro.FindControl("textDataFim") as ASPxDateEdit;

        if (textDataInicio.Text != "") {
            coll.Query.Where(coll.Query.DataLancamento.GreaterThanOrEqual(textDataInicio.Text));
        }

        if (textDataFim.Text != "") {
            coll.Query.Where(coll.Query.DataLancamento.LessThanOrEqual(textDataFim.Text));
        }

        if (!String.IsNullOrEmpty(textCdAtivo.Text)) {
            coll.Query.Where(coll.Query.CdAtivoBolsa.Like("%" + textCdAtivo.Text + "%"));
        }

        if (!String.IsNullOrEmpty(textCdAtivoDestino.Text))
        {
            coll.Query.Where(coll.Query.CdAtivoBolsaDestino.Like("%" + textCdAtivoDestino.Text + "%"));
        }

        coll.Query.OrderBy(coll.Query.DataLancamento.Descending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSAtivoBolsa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        AtivoBolsaCollection coll = new AtivoBolsaCollection();
        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";
        ASPxButtonEdit btnEditAtivoBolsa = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa") as ASPxButtonEdit;
        ASPxDateEdit textDataLancamento = gridCadastro.FindEditFormTemplateControl("textDataLancamento") as ASPxDateEdit;
        ASPxDateEdit textDataEx = gridCadastro.FindEditFormTemplateControl("textDataEx") as ASPxDateEdit;
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        ASPxSpinEdit textPercentual = gridCadastro.FindEditFormTemplateControl("textPercentual") as ASPxSpinEdit;
        ASPxSpinEdit textPUBonificacao = gridCadastro.FindEditFormTemplateControl("textPUBonificacao") as ASPxSpinEdit;
        ASPxButtonEdit btnEditAtivoDestino = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa2") as ASPxButtonEdit;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(btnEditAtivoBolsa);
        controles.Add(textDataLancamento);
        controles.Add(textDataEx);
        controles.Add(textDataReferencia);
        controles.Add(textPercentual);
        controles.Add(textPUBonificacao);
        controles.Add(btnEditAtivoDestino);

        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        AtivoBolsa ativoBolsa = new AtivoBolsa();
        if (!ativoBolsa.LoadByPrimaryKey(btnEditAtivoBolsa.Text.ToString())) {
            e.Result = "Ativo não existente!";
            return;
        }
        else {
            ativoBolsa = new AtivoBolsa();
            if (!ativoBolsa.LoadByPrimaryKey(btnEditAtivoDestino.Text.ToString())) {
                e.Result = "Ativo destino não existente!";
                return;
            }
        }

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        SalvarNovo();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOKAdd_Init(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string cdAtivoBolsa = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            AtivoBolsa ativoBolsa = new AtivoBolsa();
            ativoBolsa.LoadByPrimaryKey(Convert.ToString(e.Parameter));

            if (ativoBolsa.str.CdAtivoBolsa != "") {
                cdAtivoBolsa = ativoBolsa.str.CdAtivoBolsa;
            }
        }
        e.Result = cdAtivoBolsa;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string cdAtivoBolsa = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            AtivoBolsa ativoBolsa = new AtivoBolsa();
            ativoBolsa.LoadByPrimaryKey(Convert.ToString(e.Parameter));

            if (ativoBolsa.str.CdAtivoBolsa != "") {
                cdAtivoBolsa = ativoBolsa.str.CdAtivoBolsa;
            }
        }
        e.Result = cdAtivoBolsa;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    private void SalvarNovo() {
        ASPxButtonEdit btnEditAtivoBolsa = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa") as ASPxButtonEdit;
        ASPxButtonEdit btnEditAtivoDestino = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa2") as ASPxButtonEdit;
        ASPxDateEdit textDataLancamento = gridCadastro.FindEditFormTemplateControl("textDataLancamento") as ASPxDateEdit;
        ASPxDateEdit textDataEx = gridCadastro.FindEditFormTemplateControl("textDataEx") as ASPxDateEdit;
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        ASPxSpinEdit textPercentual = gridCadastro.FindEditFormTemplateControl("textPercentual") as ASPxSpinEdit;
        ASPxSpinEdit textPUBonificacao = gridCadastro.FindEditFormTemplateControl("textPUBonificacao") as ASPxSpinEdit;

        BonificacaoBolsa bonificacaoBolsa = new BonificacaoBolsa();

        bonificacaoBolsa.CdAtivoBolsa = btnEditAtivoBolsa.Text.ToString().ToUpper();
        bonificacaoBolsa.CdAtivoBolsaDestino = btnEditAtivoDestino.Text.ToString().ToUpper();
        bonificacaoBolsa.DataLancamento = Convert.ToDateTime(textDataLancamento.Text);
        bonificacaoBolsa.DataEx = Convert.ToDateTime(textDataEx.Text);
        bonificacaoBolsa.DataReferencia = Convert.ToDateTime(textDataReferencia.Text);
        bonificacaoBolsa.Fonte = (byte)FonteProventoBolsa.Manual;
        bonificacaoBolsa.Percentual = Convert.ToDecimal(textPercentual.Text);
        bonificacaoBolsa.PUBonificacao = Convert.ToDecimal(textPUBonificacao.Text);
        bonificacaoBolsa.TipoFracao = (byte)IndicadorTratamentoFracaoEvento.Trunca; //Forçado! TODO Parametrizar depois!

        bonificacaoBolsa.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de BonificacaoBolsa - Operacao: Insert BonificacaoBolsa: " + bonificacaoBolsa.IdBonificacao + UtilitarioWeb.ToString(bonificacaoBolsa),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        int idBonificacao = (int)e.Keys[0];

        ASPxButtonEdit btnEditAtivoBolsa = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa") as ASPxButtonEdit;
        ASPxButtonEdit btnEditAtivoDestino = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa2") as ASPxButtonEdit;
        ASPxDateEdit textDataLancamento = gridCadastro.FindEditFormTemplateControl("textDataLancamento") as ASPxDateEdit;
        ASPxDateEdit textDataEx = gridCadastro.FindEditFormTemplateControl("textDataEx") as ASPxDateEdit;
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        ASPxSpinEdit textPercentual = gridCadastro.FindEditFormTemplateControl("textPercentual") as ASPxSpinEdit;
        ASPxSpinEdit textPUBonificacao = gridCadastro.FindEditFormTemplateControl("textPUBonificacao") as ASPxSpinEdit;

        BonificacaoBolsa bonificacaoBolsa = new BonificacaoBolsa();

        if (bonificacaoBolsa.LoadByPrimaryKey(idBonificacao)) {
            bonificacaoBolsa.CdAtivoBolsa = btnEditAtivoBolsa.Text.ToString().ToUpper();
            bonificacaoBolsa.CdAtivoBolsaDestino = btnEditAtivoDestino.Text.ToString().ToUpper();
            bonificacaoBolsa.DataLancamento = Convert.ToDateTime(textDataLancamento.Text);
            bonificacaoBolsa.DataEx = Convert.ToDateTime(textDataEx.Text);
            bonificacaoBolsa.DataReferencia = Convert.ToDateTime(textDataReferencia.Text);
            bonificacaoBolsa.Fonte = (byte)FonteProventoBolsa.Manual;
            bonificacaoBolsa.Percentual = Convert.ToDecimal(textPercentual.Text);
            bonificacaoBolsa.PUBonificacao = Convert.ToDecimal(textPUBonificacao.Text);
            bonificacaoBolsa.TipoFracao = (byte)IndicadorTratamentoFracaoEvento.Trunca; //Forçado! TODO Parametrizar depois!

            bonificacaoBolsa.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de BonificacaoBolsa - Operacao: Update BonificacaoBolsa: " + idBonificacao + UtilitarioWeb.ToString(bonificacaoBolsa),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValues1 = gridCadastro.GetSelectedFieldValues(BonificacaoBolsaMetadata.ColumnNames.IdBonificacao);
            for (int i = 0; i < keyValues1.Count; i++) {
                int idBonificacao = Convert.ToInt32(keyValues1[i]);

                BonificacaoBolsa bonificacaoBolsa = new BonificacaoBolsa();
                if (bonificacaoBolsa.LoadByPrimaryKey(idBonificacao)) {

                    //
                    BonificacaoBolsa bonificacaoBolsaClone = (BonificacaoBolsa)Utilitario.Clone(bonificacaoBolsa);
                    //

                    bonificacaoBolsa.MarkAsDeleted();
                    bonificacaoBolsa.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de BonificacaoBolsa - Operacao: Delete BonificacaoBolsa: " + idBonificacao + UtilitarioWeb.ToString(bonificacaoBolsaClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing) {
            ASPxButtonEdit btnEditAtivoBolsa = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa") as ASPxButtonEdit;
            e.Properties["cpBtnEditAtivoBolsa"] = btnEditAtivoBolsa.ClientID;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("btnEditAtivoBolsa");
        base.gridCadastro_PreRender(sender, e);

        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        Label labelFiltroHeader = gridCadastro.FindStatusBarTemplateControl("labelFiltroHeader") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");
        if (textDataInicio.Text != "") {
            texto.Append(" Data Início >= ").Append(textDataInicio.Text.Substring(0, 10));
        }
        if (textDataFim.Text != "") {
            if (textDataInicio.Text != "")
                texto.Append(" |");

            texto.Append(" Data Fim <= ").Append(textDataFim.Text.Substring(0, 10));
        }

        labelFiltro.Text = texto.ToString();
    }
}