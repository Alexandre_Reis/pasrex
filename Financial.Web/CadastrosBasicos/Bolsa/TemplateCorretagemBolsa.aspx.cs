using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Bolsa;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_TemplateCorretagemBolsa : Financial.Web.Common.CadastroBasePage
{
    protected void EsDSTemplateCorretagemBolsa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        TemplateCorretagemBolsaCollection coll = new TemplateCorretagemBolsaCollection();

        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        TemplateCorretagemBolsa templateCorretagemBolsa = new TemplateCorretagemBolsa();

        int idTemplate = Convert.ToInt32(e.Keys[0]);

        if (templateCorretagemBolsa.LoadByPrimaryKey(idTemplate))
        {
            templateCorretagemBolsa.Descricao = Convert.ToString(e.NewValues["Descricao"]);
            templateCorretagemBolsa.TipoCorretagem = Convert.ToByte(e.NewValues["TipoCorretagem"]);
            templateCorretagemBolsa.PercentualDesconto = Convert.ToDecimal(e.NewValues["PercentualDesconto"]);

            if (e.NewValues["ValorFixoOperacao"] != null)
            {
                templateCorretagemBolsa.ValorFixoOperacao = Convert.ToDecimal(e.NewValues["ValorFixoOperacao"]);
            }
            else
            {
                templateCorretagemBolsa.ValorFixoOperacao = null;
            }

            if (e.NewValues["Teto"] != null)
            {
                templateCorretagemBolsa.Teto = Convert.ToDecimal(e.NewValues["Teto"]);
            }
            else
            {
                templateCorretagemBolsa.Teto = null;
            }

            templateCorretagemBolsa.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de TemplateCorretagemBolsa - Operacao: Update TemplateCorretagemBolsa: " + idTemplate + UtilitarioWeb.ToString(templateCorretagemBolsa),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        TemplateCorretagemBolsa templateCorretagemBolsa = new TemplateCorretagemBolsa();

        templateCorretagemBolsa.Descricao = Convert.ToString(e.NewValues["Descricao"]);
        templateCorretagemBolsa.TipoCorretagem = Convert.ToByte(e.NewValues["TipoCorretagem"]);
        templateCorretagemBolsa.PercentualDesconto = Convert.ToDecimal(e.NewValues["PercentualDesconto"]);

        if (e.NewValues["ValorFixoOperacao"] != null)
        {
            templateCorretagemBolsa.ValorFixoOperacao = Convert.ToDecimal(e.NewValues["ValorFixoOperacao"]);
        }
        else
        {
            templateCorretagemBolsa.ValorFixoOperacao = null;
        }

        if (e.NewValues["Teto"] != null)
        {
            templateCorretagemBolsa.Teto = Convert.ToDecimal(e.NewValues["Teto"]);
        }
        else
        {
            templateCorretagemBolsa.Teto = null;
        }

        templateCorretagemBolsa.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de TemplateCorretagemBolsa - Operacao: Insert TemplateCorretagemBolsa: " + templateCorretagemBolsa.IdTemplate + UtilitarioWeb.ToString(templateCorretagemBolsa),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdTemplate");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idTemplate = Convert.ToInt32(keyValuesId[i]);

                TemplateCorretagemBolsa templateCorretagemBolsa = new TemplateCorretagemBolsa();
                if (templateCorretagemBolsa.LoadByPrimaryKey(idTemplate))
                {
                    TemplateCorretagemBolsa templateCorretagemBolsaClone = (TemplateCorretagemBolsa)Utilitario.Clone(templateCorretagemBolsa);
                    //
                    templateCorretagemBolsa.MarkAsDeleted();
                    templateCorretagemBolsa.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de TemplateCorretagemBolsa - Operacao: Delete TemplateCorretagemBolsa: " + idTemplate + UtilitarioWeb.ToString(templateCorretagemBolsaClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    new protected void gridCadastro_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        if (e.Column.FieldName != "ValorFixoOperacao" && e.Column.FieldName != "Teto")
        {
            (e.Editor as ASPxTextEdit).ValidationSettings.RequiredField.IsRequired = true;
            (e.Editor as ASPxTextEdit).ValidationSettings.RequiredField.ErrorText = this.MsgCampoObrigatorio;
        }
    }

}

