﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Bolsa;
using Financial.Web.Common;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_EvolucaoCapitalBolsa : CadastroBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasFiltro = true;
        this.HasPopupAtivoBolsa = true;
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSEvolucaoCapitalBolsa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        EvolucaoCapitalSocialCollection coll = new EvolucaoCapitalSocialCollection();

        ASPxDateEdit textDataInicio = popupFiltro.FindControl("textDataInicio") as ASPxDateEdit;
        ASPxDateEdit textDataFim = popupFiltro.FindControl("textDataFim") as ASPxDateEdit;
        TextBox textCdAtivo = popupFiltro.FindControl("textCdAtivo") as TextBox;

        if (textDataInicio.Text != "") {
            coll.Query.Where(coll.Query.Data >= textDataInicio.Text);
        }

        if (textDataFim.Text != "") {
            coll.Query.Where(coll.Query.Data <= textDataFim.Text);
        }

        if (textCdAtivo.Text != "") {
            coll.Query.Where(coll.Query.CdAtivoBolsa.Like("%" + textCdAtivo.Text + "%"));
        }

        coll.Query.OrderBy(coll.Query.Data.Descending, 
                           coll.Query.CdAtivoBolsa.Ascending);
        //
        coll.Query.Load();

        e.Collection = coll;
    }

    protected void EsDSAtivoBolsa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        AtivoBolsaCollection coll = new AtivoBolsaCollection();
        coll.Query.OrderBy(coll.Query.CdAtivoBolsa.Ascending);
        coll.LoadAll();
        //        
        e.Collection = coll;
    }
    #endregion
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string cdAtivoBolsa = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            AtivoBolsa ativoBolsa = new AtivoBolsa();
            ativoBolsa.LoadByPrimaryKey(Convert.ToString(e.Parameter));

            if (ativoBolsa.str.CdAtivoBolsa != "") {
                cdAtivoBolsa = ativoBolsa.str.CdAtivoBolsa;
            }
        }
        e.Result = cdAtivoBolsa;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";
        //
        ASPxButtonEdit btnEditAtivoBolsa = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa") as ASPxButtonEdit;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
        ASPxComboBox dropTipo = gridCadastro.FindEditFormTemplateControl("dropTipo") as ASPxComboBox;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { btnEditAtivoBolsa, textData, textQuantidade, textValor, dropTipo });

        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        if (gridCadastro.IsNewRowEditing) {
            string cdAtivoBolsa = Convert.ToString(btnEditAtivoBolsa.Text);
            DateTime data = Convert.ToDateTime(textData.Text);

            EvolucaoCapitalSocial evolucaoCapitalSocial = new EvolucaoCapitalSocial();
            if (evolucaoCapitalSocial.LoadByPrimaryKey(cdAtivoBolsa, data)) {
                e.Result = "Registro já existente";
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textData_Init(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing)
            (sender as ASPxDateEdit).Enabled = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditAtivoBolsa_Load(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxButtonEdit).Enabled = false;
            (sender as ASPxButtonEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) {
        if (e.Column.FieldName == "CompositeKey") {
            //Compose a primary key value
            string data = Convert.ToString(e.GetListSourceFieldValue(EvolucaoCapitalSocialMetadata.ColumnNames.Data));
            string cdAtivoBolsa = Convert.ToString(e.GetListSourceFieldValue(EvolucaoCapitalSocialMetadata.ColumnNames.CdAtivoBolsa));
            //
            e.Value = data + cdAtivoBolsa;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        EvolucaoCapitalSocial evolucaoCapitalSocial = new EvolucaoCapitalSocial();

        ASPxButtonEdit btnEditAtivoBolsa = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa") as ASPxButtonEdit;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
        TextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as TextBox;
        ASPxComboBox dropTipo = gridCadastro.FindEditFormTemplateControl("dropTipo") as ASPxComboBox;

        string cdAtivoBolsa = Convert.ToString(btnEditAtivoBolsa.Text);
        DateTime data = Convert.ToDateTime(textData.Text);
        //
        if (evolucaoCapitalSocial.LoadByPrimaryKey(cdAtivoBolsa, data)) {
            evolucaoCapitalSocial.Valor = Convert.ToDecimal(textValor.Text);
            evolucaoCapitalSocial.Quantidade = Convert.ToDecimal(textQuantidade.Text);
            evolucaoCapitalSocial.Descricao = textDescricao.Text;
            evolucaoCapitalSocial.QuantidadeFinal = Convert.ToDecimal(textQuantidade.Text);
            evolucaoCapitalSocial.ValorFinal = Convert.ToDecimal(textValor.Text);
            evolucaoCapitalSocial.Tipo = Convert.ToByte(dropTipo.SelectedItem.Value);
            evolucaoCapitalSocial.Save();

            this.AtualizaValoresFinais(evolucaoCapitalSocial);

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de EvolucaoCapitalSocial - Operacao: Update EvolucaoCapitalSocial: " + cdAtivoBolsa + "; " + data + UtilitarioWeb.ToString(evolucaoCapitalSocial),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }
        
        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        EvolucaoCapitalSocial evolucaoCapitalSocial = new EvolucaoCapitalSocial();

        ASPxButtonEdit btnEditAtivoBolsa = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa") as ASPxButtonEdit;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
        TextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as TextBox;
        ASPxComboBox dropTipo = gridCadastro.FindEditFormTemplateControl("dropTipo") as ASPxComboBox;
        //
        evolucaoCapitalSocial.CdAtivoBolsa = Convert.ToString(btnEditAtivoBolsa.Text).ToUpper();
        evolucaoCapitalSocial.Data = Convert.ToDateTime(textData.Text);
        evolucaoCapitalSocial.Quantidade = Convert.ToDecimal(textQuantidade.Text);
        evolucaoCapitalSocial.Valor = Convert.ToDecimal(textValor.Text);
        evolucaoCapitalSocial.Descricao = textDescricao.Text;
        evolucaoCapitalSocial.QuantidadeFinal = Convert.ToDecimal(textQuantidade.Text);
        evolucaoCapitalSocial.ValorFinal = Convert.ToDecimal(textValor.Text);
        evolucaoCapitalSocial.Tipo = Convert.ToByte(dropTipo.SelectedItem.Value);
        //
        evolucaoCapitalSocial.Save();

        this.AtualizaValoresFinais(evolucaoCapitalSocial);

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de EvolucaoCapitalSocial - Operacao: Insert EvolucaoCapitalSocial: " + evolucaoCapitalSocial.CdAtivoBolsa + "; " + evolucaoCapitalSocial.Data + UtilitarioWeb.ToString(evolucaoCapitalSocial),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(EvolucaoCapitalSocialMetadata.ColumnNames.CdAtivoBolsa);
            List<object> keyValuesData = gridCadastro.GetSelectedFieldValues(EvolucaoCapitalSocialMetadata.ColumnNames.Data);

            for (int i = 0; i < keyValuesData.Count; i++) {
                string cdAtivoBolsa = Convert.ToString(keyValuesId[i]);
                DateTime data = Convert.ToDateTime(keyValuesData[i]);

                EvolucaoCapitalSocial evolucaoCapitalSocial = new EvolucaoCapitalSocial();
                if (evolucaoCapitalSocial.LoadByPrimaryKey(cdAtivoBolsa, data)) {
                    //
                    EvolucaoCapitalSocial evolucaoCapitalSocialClone = (EvolucaoCapitalSocial)Utilitario.Clone(evolucaoCapitalSocial);
                    //                    
                    evolucaoCapitalSocial.MarkAsDeleted();
                    evolucaoCapitalSocial.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de EvolucaoCapitalSocial - Operacao: Delete EvolucaoCapitalSocial: " + cdAtivoBolsa + "; " + data + UtilitarioWeb.ToString(evolucaoCapitalSocialClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("textData", "textQuantidade");
        base.gridCadastro_PreRender(sender, e);

        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");
        if (textDataInicio.Text != "") {
            texto.Append(" Data Início >= ").Append(textDataInicio.Text.Substring(0, 10));
        }
        if (textDataFim.Text != "") {
            if (texto.Length > 0) {
                texto.Append(" |");
            }

            texto.Append(" Data Fim <= ").Append(textDataFim.Text.Substring(0, 10));
        }
        if (textCdAtivo.Text != "") {
            if (texto.Length > 0) {
                texto.Append(" |");
            }

            texto.Append(" Ativo Like %").Append(textCdAtivo.Text).Append("%");
        }

        labelFiltro.Text = texto.ToString();
    }

    private void AtualizaValoresFinais(EvolucaoCapitalSocial e)
    {
        decimal quantidadeAnterior = 0;
        decimal valorAnterior = 0;
        EvolucaoCapitalSocialCollection evolucaoCapitalSocialCollection = new EvolucaoCapitalSocialCollection();
        evolucaoCapitalSocialCollection.Query.Where(evolucaoCapitalSocialCollection.Query.CdAtivoBolsa.Equal(e.CdAtivoBolsa));
        evolucaoCapitalSocialCollection.Query.OrderBy(evolucaoCapitalSocialCollection.Query.Data.Ascending);
        evolucaoCapitalSocialCollection.Query.Load();

        foreach (EvolucaoCapitalSocial evolucaoCapitalSocial in evolucaoCapitalSocialCollection)
        {
            evolucaoCapitalSocial.QuantidadeFinal = evolucaoCapitalSocial.Quantidade.Value + quantidadeAnterior;
            evolucaoCapitalSocial.ValorFinal = evolucaoCapitalSocial.Valor.Value + valorAnterior;

            quantidadeAnterior = evolucaoCapitalSocial.QuantidadeFinal.Value;
            valorAnterior = evolucaoCapitalSocial.ValorFinal.Value;
        }

        evolucaoCapitalSocialCollection.Save();        
    }
}