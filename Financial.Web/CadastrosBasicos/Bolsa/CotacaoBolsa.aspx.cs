﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using Financial.Common;
using Financial.Util;
using Financial.Bolsa;
using Financial.Web.Common;

using DevExpress.Web;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Investidor;
using Financial.Interfaces.Import.Offshore;
using EntitySpaces.Interfaces;

public partial class CadastrosBasicos_CotacaoBolsa : CadastroBasePage {
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasPopupAtivoBolsa = true;
        this.HasFiltro = true;
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSCotacaoBolsa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CotacaoBolsaCollection coll = new CotacaoBolsaCollection();

        ASPxDateEdit textDataInicio = popupFiltro.FindControl("textDataInicio") as ASPxDateEdit;
        ASPxDateEdit textDataFim = popupFiltro.FindControl("textDataFim") as ASPxDateEdit;
        TextBox textCdAtivo = popupFiltro.FindControl("textCdAtivo") as TextBox;

        if (textDataInicio.Text != "") {
            coll.Query.Where(coll.Query.Data.GreaterThanOrEqual(textDataInicio.Text));
        }

        if (textDataFim.Text != "") {
            coll.Query.Where(coll.Query.Data.LessThanOrEqual(textDataFim.Text));
        }

        if (textCdAtivo.Text != "") {
            coll.Query.Where(coll.Query.CdAtivoBolsa.Like("%" + textCdAtivo.Text + "%"));
        }

        coll.Query.OrderBy(coll.Query.Data.Descending, coll.Query.CdAtivoBolsa.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSAtivoBolsa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        AtivoBolsaCollection coll = new AtivoBolsaCollection();
        coll.Query.OrderBy(coll.Query.CdAtivoBolsa.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string cdAtivoBolsa = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            AtivoBolsa ativoBolsa = new AtivoBolsa();
            ativoBolsa.LoadByPrimaryKey(Convert.ToString(e.Parameter));

            if (ativoBolsa.str.CdAtivoBolsa != "") {
                cdAtivoBolsa = ativoBolsa.str.CdAtivoBolsa;
            }
        }
        e.Result = cdAtivoBolsa;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";
        ASPxButtonEdit btnEditAtivoBolsa = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa") as ASPxButtonEdit;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxSpinEdit textPUFechamento = gridCadastro.FindEditFormTemplateControl("textPUFechamento") as ASPxSpinEdit;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(btnEditAtivoBolsa);
        controles.Add(textData);
        controles.Add(textPUFechamento);

        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        if (gridCadastro.IsNewRowEditing) {
            string cdAtivoBolsa = Convert.ToString(btnEditAtivoBolsa.Text);
            DateTime data = Convert.ToDateTime(textData.Text);

            CotacaoBolsa cotacaoBolsa = new CotacaoBolsa();
            if (cotacaoBolsa.LoadByPrimaryKey(data, cdAtivoBolsa)) {
                e.Result = "Registro já existente";
            }
        }
    }

    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();
    }
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textData_Init(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing)
            (sender as ASPxDateEdit).Enabled = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditAtivoBolsa_Load(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxButtonEdit).Enabled = false;
            (sender as ASPxButtonEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textPUGerencial_Load(object sender, EventArgs e)
    {
        ClienteCollection clienteCollection = new ClienteCollection();
        clienteCollection.Query.Select(clienteCollection.Query.CalculaGerencial);
        clienteCollection.Query.Where(clienteCollection.Query.CalculaGerencial.Equal("S"));
        clienteCollection.Query.Load();

        if (gridCadastro.IsNewRowEditing || clienteCollection.Count == 0)
        {
            (sender as ASPxSpinEdit).Visible = false;

            Label labelPUGerencial = gridCadastro.FindEditFormTemplateControl("labelPUGerencial") as Label;
            labelPUGerencial.Visible = false;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) {
        if (e.Column.FieldName == "CompositeKey") {
            //Compose a primary key value
            string data = Convert.ToString(e.GetListSourceFieldValue(CotacaoBolsaMetadata.ColumnNames.Data));
            string cdAtivoBolsa = Convert.ToString(e.GetListSourceFieldValue(CotacaoBolsaMetadata.ColumnNames.CdAtivoBolsa));
            e.Value = data + cdAtivoBolsa;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) 
    {
        ASPxButtonEdit btnEditAtivoBolsa = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa") as ASPxButtonEdit;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxSpinEdit textPUMedio = gridCadastro.FindEditFormTemplateControl("textPUMedio") as ASPxSpinEdit;
        ASPxSpinEdit textPUFechamento = gridCadastro.FindEditFormTemplateControl("textPUFechamento") as ASPxSpinEdit;
        ASPxSpinEdit textPUAbertura = gridCadastro.FindEditFormTemplateControl("textPUAbertura") as ASPxSpinEdit;
        ASPxSpinEdit textPUGerencial = gridCadastro.FindEditFormTemplateControl("textPUGerencial") as ASPxSpinEdit;

        CotacaoBolsa cotacaoBolsa = new CotacaoBolsa();
        //
        string cdAtivoBolsa = Convert.ToString(btnEditAtivoBolsa.Text).ToUpper().Trim();
        DateTime data = Convert.ToDateTime(textData.Text);
        //
        decimal PUMedio = !String.IsNullOrEmpty(textPUMedio.Text) ? Convert.ToDecimal(textPUMedio.Text) : 0;
        decimal PUFechamento = !String.IsNullOrEmpty(textPUFechamento.Text) ? Convert.ToDecimal(textPUFechamento.Text) : 0;
        decimal PUAbertura = !String.IsNullOrEmpty(textPUAbertura.Text) ? Convert.ToDecimal(textPUAbertura.Text) : 0;
        //
        if (cotacaoBolsa.LoadByPrimaryKey(data, cdAtivoBolsa)) {
            cotacaoBolsa.PUMedio = PUMedio;
            cotacaoBolsa.PUFechamento = PUFechamento;
            cotacaoBolsa.PUAbertura = PUAbertura;

            if (textPUGerencial != null && !String.IsNullOrEmpty(textPUGerencial.Text))
            {
                cotacaoBolsa.PUGerencial = Convert.ToDecimal(textPUGerencial.Text);
            }

            cotacaoBolsa.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de CotacaoBolsa - Operacao: Update CotacaoBolsa: " + cdAtivoBolsa + "; " + data + UtilitarioWeb.ToString(cotacaoBolsa),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void callbackImportar_Callback(object sender, DevExpress.Web.CallbackEventArgs e) 
    {
        e.Result = "";

        if (textDataInicioImporta.Value == "")
        {
            e.Result = "Preencha a Data Inicio.";
            return;
        }
        else
            if (textDataFimImporta.Value == "")
            {
            e.Result = "Preencha a Data Fim.";
            return;
        }
        else
        {            
            string[] lines = textImportacao.Text.Split(new string[] { "\n" }, StringSplitOptions.None);

            List<string> ativos = new List<string>();
            List<string> especs = new List<string>();

            for (int c = 0; c < lines.Length; c++)
            {
                string[] str = lines[c].Split(';');

                for (int d = 0; d < str.Length; d++)
                {
                    ativos.Add(str[d]);
                    especs.Add("BZ");
                }
            }

            CotacaoBloomberg cotacaoBloombergExec = new CotacaoBloomberg();
            List<CotacaoBloomberg> listaCotacao = cotacaoBloombergExec.CarregaCotacaoBloomberg(ativos, especs, Convert.ToDateTime(textDataInicioImporta.Value) , Convert.ToDateTime(textDataFimImporta.Value));
                
            CotacaoBolsaCollection cotacaoBolsaCollection = new CotacaoBolsaCollection();

            for (int c = 0; c < listaCotacao.Count; c++)
            {
                CotacaoBolsa cotBolsa = new CotacaoBolsa();
                if (!cotBolsa.LoadByPrimaryKey(listaCotacao[c].Data, listaCotacao[c].CdAtivo ))
                {
                    CotacaoBolsa cotacaoBolsa = cotacaoBolsaCollection.AddNew();

                    cotacaoBolsa.Data = listaCotacao[c].Data;
                    cotacaoBolsa.CdAtivoBolsa = listaCotacao[c].CdAtivo;
                    cotacaoBolsa.PUMedio = listaCotacao[c].Pu;
                    cotacaoBolsa.PUFechamento = listaCotacao[c].Pu;
                    cotacaoBolsa.PUAbertura = listaCotacao[c].Pu;                    
                }
            }
            cotacaoBolsaCollection.Save();
        }
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void SalvarNovo()
    {
        ASPxButtonEdit btnEditAtivoBolsa = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa") as ASPxButtonEdit;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxSpinEdit textPUMedio = gridCadastro.FindEditFormTemplateControl("textPUMedio") as ASPxSpinEdit;
        ASPxSpinEdit textPUFechamento = gridCadastro.FindEditFormTemplateControl("textPUFechamento") as ASPxSpinEdit;
        ASPxSpinEdit textPUAbertura = gridCadastro.FindEditFormTemplateControl("textPUAbertura") as ASPxSpinEdit;

        CotacaoBolsa cotacaoBolsa = new CotacaoBolsa();
        //
        string cdAtivoBolsa = Convert.ToString(btnEditAtivoBolsa.Text).ToUpper().Trim();
        DateTime data = Convert.ToDateTime(textData.Text);
        //
        decimal PUMedio = !String.IsNullOrEmpty(textPUMedio.Text) ? Convert.ToDecimal(textPUMedio.Text) : 0;
        decimal PUFechamento = !String.IsNullOrEmpty(textPUFechamento.Text) ? Convert.ToDecimal(textPUFechamento.Text) : 0;
        decimal PUAbertura = !String.IsNullOrEmpty(textPUAbertura.Text) ? Convert.ToDecimal(textPUAbertura.Text) : 0;

        cotacaoBolsa.Data = data;
        cotacaoBolsa.CdAtivoBolsa = cdAtivoBolsa;
        cotacaoBolsa.PUMedio = PUMedio;
        cotacaoBolsa.PUFechamento = PUFechamento;
        cotacaoBolsa.PUAbertura = PUAbertura;
        cotacaoBolsa.Save();
        //

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de CotacaoBolsa - Operacao: Insert CotacaoBolsa: " + cotacaoBolsa.CdAtivoBolsa + "; " + cotacaoBolsa.Data + UtilitarioWeb.ToString(cotacaoBolsa),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") 
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(CotacaoBolsaMetadata.ColumnNames.CdAtivoBolsa);
            List<object> keyValuesData = gridCadastro.GetSelectedFieldValues(CotacaoBolsaMetadata.ColumnNames.Data);
            for (int i = 0; i < keyValuesData.Count; i++) {
                string cdAtivoBolsa = Convert.ToString(keyValuesId[i]);
                DateTime data = Convert.ToDateTime(keyValuesData[i]);

                CotacaoBolsa cotacaoBolsa = new CotacaoBolsa();
                if (cotacaoBolsa.LoadByPrimaryKey(data, cdAtivoBolsa)) {

                    //
                    CotacaoBolsa cotacaoBolsaClone = (CotacaoBolsa)Utilitario.Clone(cotacaoBolsa);

                    cotacaoBolsa.MarkAsDeleted();
                    cotacaoBolsa.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de CotacaoBolsa - Operacao: Delete CotacaoBolsa: " + cotacaoBolsaClone.CdAtivoBolsa + "; " + cotacaoBolsaClone.Data + UtilitarioWeb.ToString(cotacaoBolsaClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }
        else if (e.Parameters == "btnCopy")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(CotacaoBolsaMetadata.ColumnNames.CdAtivoBolsa);
            List<object> keyValuesData = gridCadastro.GetSelectedFieldValues(CotacaoBolsaMetadata.ColumnNames.Data);
            for (int i = 0; i < keyValuesData.Count; i++)
            {
                string cdAtivoBolsa = Convert.ToString(keyValuesId[i]);
                DateTime data = Convert.ToDateTime(keyValuesData[i]);
                DateTime dataProxima = Calendario.AdicionaDiaUtil(data, 1);

                CotacaoBolsa cotacaoBolsaOrig = new CotacaoBolsa();
                cotacaoBolsaOrig.LoadByPrimaryKey(data, cdAtivoBolsa);

                CotacaoBolsa cotacaoBolsa = new CotacaoBolsa();
                if (!cotacaoBolsa.LoadByPrimaryKey(dataProxima, cdAtivoBolsa))
                {
                    cotacaoBolsa = new CotacaoBolsa();
                    foreach (esColumnMetadata colCotacaoBolsa in cotacaoBolsa.es.Meta.Columns)
                    {
                        if (colCotacaoBolsa.Name.ToUpper().Equals("DATA"))
                            cotacaoBolsa.Data = dataProxima;
                        else
                            cotacaoBolsa.SetColumn(colCotacaoBolsa.Name, cotacaoBolsaOrig.GetColumn(colCotacaoBolsa.Name));
                    }

                    cotacaoBolsa.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de CotacaoBolsa - Operacao: Copy CotacaoBolsa: " + data + "; " + UtilitarioWeb.ToString(cotacaoBolsa),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
                else
                {
                    throw new Exception("Ativo " + cotacaoBolsa.CdAtivoBolsa + " já tem cotação definida na data " + dataProxima.ToShortDateString() + ".");
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("textData", "textPUFechamento");
        base.gridCadastro_PreRender(sender, e);
        
        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");
        if (textDataInicio.Text != "") {
            texto.Append(" Data Início >= ").Append(textDataInicio.Text.Substring(0, 10));
        }
        if (textDataFim.Text != "") {
            if (texto.Length > 0) {
                texto.Append(" |");
            }

            texto.Append(" Data Fim <= ").Append(textDataFim.Text.Substring(0, 10));
        }
        if (textCdAtivo.Text != "") {
            if (texto.Length > 0) {
                texto.Append(" |");
            }

            texto.Append(" Ativo Like %").Append(textCdAtivo.Text).Append("%");
        }

        labelFiltro.Text = texto.ToString();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing) {
            ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
            e.Properties["cpTextData"] = textData.ClientID;

            ASPxSpinEdit textPUFechamento = gridCadastro.FindEditFormTemplateControl("textPUFechamento") as ASPxSpinEdit;
            e.Properties["cpTextPUFechamento"] = textPUFechamento.ClientID;
        }
    }
}