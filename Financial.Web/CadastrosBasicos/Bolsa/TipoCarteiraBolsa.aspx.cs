﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.CRM;
using Financial.InvestidorCotista;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Bolsa;
using System.Reflection;
using Financial.Bolsa.Enums; 

public partial class CadastrosBasicos_Bolsa_TipoCarteiraBolsa : Financial.Web.Common.CadastroBasePage
{
    new protected void Page_Load(object sender, EventArgs e)
    {        
        base.Page_Load(sender, e);
    }

    protected void textIdCarteiraBolsa_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
        {
            (sender as ASPxSpinEdit).Enabled = false;
            (sender as ASPxSpinEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    protected void EsDSTipoCarteiraBolsa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        TipoCarteiraBolsaCollection coll = new TipoCarteiraBolsaCollection();
        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.Query.Load();
            
        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll ;
    }
    
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
        ASPxSpinEdit textIdCarteiraBolsa = gridCadastro.FindEditFormTemplateControl("textIdCarteiraBolsa") as ASPxSpinEdit;
        ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(textIdCarteiraBolsa);
        controles.Add(textDescricao);

        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        if (gridCadastro.IsNewRowEditing)
        {
            int idCarteiraBolsa = Convert.ToInt32(textIdCarteiraBolsa.Text);

            TipoCarteiraBolsa tipoCarteiraBolsa = new TipoCarteiraBolsa();
            if (tipoCarteiraBolsa.LoadByPrimaryKey(idCarteiraBolsa))
            {
                e.Result = "Registro já existente";
            }
        }
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        int idCarteiraBolsa = (int)e.Keys[0];

        ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;

        TipoCarteiraBolsa tipoCarteiraBolsa = new TipoCarteiraBolsa();

        if (tipoCarteiraBolsa.LoadByPrimaryKey(idCarteiraBolsa))
        {
            tipoCarteiraBolsa.Descricao = Convert.ToString(textDescricao.Text);
            tipoCarteiraBolsa.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de TipoCarteiraBolsa - Operacao: Update TipoCarteiraBolsa: " + idCarteiraBolsa + UtilitarioWeb.ToString(tipoCarteiraBolsa),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        ASPxSpinEdit textIdCarteiraBolsa = gridCadastro.FindEditFormTemplateControl("textIdCarteiraBolsa") as ASPxSpinEdit;
        ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;

        TipoCarteiraBolsa tipoCarteiraBolsa = new TipoCarteiraBolsa();

        tipoCarteiraBolsa.Descricao = Convert.ToString(textDescricao.Text);
        tipoCarteiraBolsa.IdCarteiraBolsa = Convert.ToInt32(textIdCarteiraBolsa.Text);
        tipoCarteiraBolsa.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de TipoCarteiraBolsa - Operacao: Insert TipoCarteiraBolsa: " + tipoCarteiraBolsa.IdCarteiraBolsa + UtilitarioWeb.ToString(tipoCarteiraBolsa),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdCarteiraBolsa");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idCarteiraBolsa = Convert.ToInt32(keyValuesId[i]);

                TipoCarteiraBolsa tipoCarteiraBolsa = new TipoCarteiraBolsa();
                if (tipoCarteiraBolsa.LoadByPrimaryKey(idCarteiraBolsa))
                {
                    //
                    TipoCarteiraBolsa tipoCarteiraBolsaClone = (TipoCarteiraBolsa)Utilitario.Clone(tipoCarteiraBolsa);
                    //

                    int j = 0;
                    foreach (FieldInfo fieldInfo in typeof(TipoCarteiraBovespa).GetFields())
                    {
                        if (j > 0)
                        {
                            int idEnum = Convert.ToInt32(fieldInfo.GetValue(fieldInfo));

                            if (idCarteiraBolsa == idEnum)
                            {
                                throw new Exception("Código " + tipoCarteiraBolsa.IdCarteiraBolsa + " não pode ser excluído por ser primário do sistema.");
                            }
                        }

                        j++;
                    }

                    tipoCarteiraBolsa.MarkAsDeleted();
                    tipoCarteiraBolsa.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de TipoCarteiraBolsa - Operacao: Delete TipoCarteiraBolsa: " + idCarteiraBolsa + UtilitarioWeb.ToString(tipoCarteiraBolsaClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }
        

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e)
    {
        base.gridCadastro_PreRender(sender, e);
    }
}