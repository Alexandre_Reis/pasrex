﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using DevExpress.Web;
using DevExpress.Web.Data;

using Financial.Investidor;
using Financial.ContaCorrente;
using Financial.Util;
using Financial.Common;
using Financial.Security;
using Financial.Bolsa;
using Financial.Web.Common;
using Financial.WebConfigConfiguration;

using Financial.Investidor.Enums;
using Financial.Common.Enums;
using Financial.Bolsa.Enums;

using EntitySpaces.Interfaces;
using Financial.Web.Util;
using System.Threading;
using Financial.Security.Enums;

public partial class CadastrosBasicos_OrdemBolsa : CadastroBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasFiltro = true;
        this.HasPopupCliente = true;
        this.HasPopupAtivoBolsa = true;
        this.HasPanelFieldsLoading = true;

        if (!Page.IsPostBack) {

            List<Hashtable> list = Combos.carregaComboTipoMercadoBolsa(ConfiguracaoCombo.OpcaoBranco);

            for (int i = 0; i < list.Count; i++) {
                // Hashtable
                Hashtable hashtable = (Hashtable)list[i];

                foreach (DictionaryEntry item in hashtable) {
                    ListEditItem listItem = new ListEditItem();
                    //
                    listItem.Text = item.Value.ToString();
                    listItem.Value = item.Key.ToString();
                    //
                    dropTipoMercado.Items.Add(listItem);
                }
            }
        }

        base.Page_Load(sender, e);

        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
                         new List<string>(new string[] { OrdemBolsaMetadata.ColumnNames.TipoOrdem }));
    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSOrdemBolsa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
        OrdemBolsaQuery ordemBolsaQuery = new OrdemBolsaQuery("O");
        ClienteQuery clienteQuery = new ClienteQuery("C");
        //
        AgenteMercadoQuery agenteQuery = new AgenteMercadoQuery("A");
        TraderQuery traderQuery = new TraderQuery("T");

        ordemBolsaQuery.Select(ordemBolsaQuery, clienteQuery.Apelido.As("Apelido"), agenteQuery.Nome, traderQuery.Nome);
        ordemBolsaQuery.InnerJoin(clienteQuery).On(ordemBolsaQuery.IdCliente == clienteQuery.IdCliente);
        ordemBolsaQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        ordemBolsaQuery.InnerJoin(agenteQuery).On(ordemBolsaQuery.IdAgenteCorretora == agenteQuery.IdAgente);
        ordemBolsaQuery.LeftJoin(traderQuery).On(ordemBolsaQuery.IdTrader == traderQuery.IdTrader);
        //        
        ordemBolsaQuery.Where(permissaoClienteQuery.IdUsuario.Equal(idUsuario));

        if (!String.IsNullOrEmpty(btnEditCodigoClienteFiltro.Text)) {
            ordemBolsaQuery.Where(ordemBolsaQuery.IdCliente.Equal(Convert.ToInt32(btnEditCodigoClienteFiltro.Text)));
        }

        if (!String.IsNullOrEmpty(textDataInicio.Text)) {
            ordemBolsaQuery.Where(ordemBolsaQuery.Data >= textDataInicio.Text);
        }

        if (!String.IsNullOrEmpty(textDataFim.Text)) {
            ordemBolsaQuery.Where(ordemBolsaQuery.Data <= textDataFim.Text);
        }

        if (!String.IsNullOrEmpty(textCdAtivo.Text)) {
            ordemBolsaQuery.Where(ordemBolsaQuery.CdAtivoBolsa.Like("%" + textCdAtivo.Text + "%"));
        }

        if (dropTipoOperacaoFiltro.SelectedIndex > -1) {
            ordemBolsaQuery.Where(ordemBolsaQuery.TipoOrdem == Convert.ToString(dropTipoOperacaoFiltro.SelectedItem.Value));
        }

        if (dropTraderFiltro.SelectedIndex != -1) {
            ordemBolsaQuery.Where(ordemBolsaQuery.IdTrader == dropTraderFiltro.SelectedItem.Value);
        }

        if (dropTipoMercado.SelectedIndex > 0) {
            ordemBolsaQuery.Where(ordemBolsaQuery.TipoMercado == dropTipoMercado.SelectedItem.Value);
        }

        ordemBolsaQuery.OrderBy(ordemBolsaQuery.Data.Descending,
                                agenteQuery.Nome.Ascending,
                                traderQuery.Nome.Ascending);

        OrdemBolsaCollection coll = new OrdemBolsaCollection();
        coll.Load(ordemBolsaQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ClienteCollection coll = new ClienteCollection();
        coll.BuscaClientesComAcesso(Context.User.Identity.Name);
        
        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSAgenteMercadoCorretora_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        AgenteMercadoCollection coll = new AgenteMercadoCollection();
        coll.Query.Where(coll.Query.FuncaoCorretora.Equal("S"));
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSAgenteMercadoLiquidacao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        AgenteMercadoCollection coll = new AgenteMercadoCollection();
        coll.Query.Where(coll.Query.FuncaoLiquidante.Equal("S"));
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSAtivoBolsa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        AtivoBolsaCollection coll = new AtivoBolsaCollection();
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSIndice_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        IndiceCollection coll = new IndiceCollection();
        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSTrader_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        TraderCollection coll = new TraderCollection();
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSLocalCustodia_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        LocalCustodiaCollection coll = new LocalCustodiaCollection();

        coll.Query.OrderBy(coll.Query.IdLocalCustodia.Ascending);
        coll.LoadAll();

        e.Collection = coll;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSClearing_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ClearingCollection coll = new ClearingCollection();

        coll.Query.OrderBy(coll.Query.IdClearing.Ascending);
        coll.LoadAll();

        e.Collection = coll;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSLocalNegociacao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        LocalNegociacaoCollection coll = new LocalNegociacaoCollection();

        coll.Query.OrderBy(coll.Query.IdLocalNegociacao.Ascending);
        coll.LoadAll();

        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void panelEdicao_Load(object sender, EventArgs e) {
        //Busca do web.config o flag de agente liquidação exclusivo
        //bool agenteLiquidacaoExclusivo = Convert.ToBoolean(WebConfig.AppSettings.AgenteLiquidacaoExclusivo);
        bool agenteLiquidacaoExclusivo = ParametrosConfiguracaoSistema.Outras.AgenteLiquidacaoExclusivo;


        if (!agenteLiquidacaoExclusivo) {
            Label labelAgenteLiquidacao = gridCadastro.FindEditFormTemplateControl("labelAgenteLiquidacao") as Label;
            ASPxComboBox dropAgenteLiquidacao = gridCadastro.FindEditFormTemplateControl("dropAgenteLiquidacao") as ASPxComboBox;
            labelAgenteLiquidacao.Visible = false;
            dropAgenteLiquidacao.Visible = false;
        }

        base.panelEdicao_Load(sender, e);

        bool travaPainel = false;
        if (gridCadastro.IsEditing && !gridCadastro.IsNewRowEditing && Convert.ToString(Session["FormLoad"]) != "S") {
            Session["FormLoad"] = "S";

            int idOrdem = Convert.ToInt32(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "IdOrdem"));

            //Especificacao, FatorCotacao
            string cdAtivoBolsa = gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "CdAtivoBolsa").ToString();
            DateTime data = Convert.ToDateTime(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "Data"));

            ASPxSpinEdit textFatorCotacao = gridCadastro.FindEditFormTemplateControl("textFatorCotacao") as ASPxSpinEdit;

            string fatorCotacao = "";
            FatorCotacaoBolsa fatorCotacaoBolsa = new FatorCotacaoBolsa();
            if (fatorCotacaoBolsa.BuscaFatorCotacaoBolsa(cdAtivoBolsa, data)) {
                fatorCotacao = fatorCotacaoBolsa.Fator.Value.ToString();
            }
            textFatorCotacao.Text = fatorCotacao;
            //

            //CheckBox de custos informados
            CheckBox chkCustosInformados = gridCadastro.FindEditFormTemplateControl("chkCustosInformados") as CheckBox;
            string calculaDespesas = gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "CalculaDespesas").ToString();

            chkCustosInformados.Checked = !(calculaDespesas == "S");

            //CheckBox de Exercicio de Opcoes
            ASPxButtonEdit btnEditAtivoBolsa = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa") as ASPxButtonEdit;

            CheckBox chkExercicio = gridCadastro.FindEditFormTemplateControl("chkExercicio") as CheckBox;
            byte origem = Convert.ToByte(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "Origem"));
            if (origem == (byte)OrigemOperacaoBolsa.ExercicioOpcaoCompra ||
                origem == (byte)OrigemOperacaoBolsa.ExercicioOpcaoVenda) {
                chkExercicio.Checked = true;

                string cdAtivoBolsaOpcao = gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "CdAtivoBolsaOpcao").ToString();
                btnEditAtivoBolsa.Text = cdAtivoBolsaOpcao;
            }
            else {
                btnEditAtivoBolsa.Text = cdAtivoBolsa;
            }

            //Campos de Termo
            ASPxSpinEdit textTaxaTermo = gridCadastro.FindEditFormTemplateControl("textTaxaTermo") as ASPxSpinEdit;
            ASPxButtonEdit btnNumeroContratoTermo = gridCadastro.FindEditFormTemplateControl("btnNumeroContratoTermo") as ASPxButtonEdit;
            ASPxDateEdit textDataVencimentoTermo = gridCadastro.FindEditFormTemplateControl("textDataVencimentoTermo") as ASPxDateEdit;
            ASPxComboBox dropIndiceTermo = gridCadastro.FindEditFormTemplateControl("dropIndiceTermo") as ASPxComboBox;
            OrdemTermoBolsa ordemTermoBolsa = new OrdemTermoBolsa();
            if (ordemTermoBolsa.LoadByPrimaryKey(idOrdem)) {
                textTaxaTermo.Text = ordemTermoBolsa.Taxa.ToString();
                btnNumeroContratoTermo.Text = ordemTermoBolsa.NumeroContrato.ToString();

                //Preenche data de vencimento
                AtivoBolsa ativoBolsa = new AtivoBolsa();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(ativoBolsa.Query.DataVencimento);
                ativoBolsa.LoadByPrimaryKey(campos, cdAtivoBolsa);
                textDataVencimentoTermo.Text = ativoBolsa.DataVencimento.Value.ToString();
                //

                if (ordemTermoBolsa.IdIndice.HasValue) {
                    Indice indice = new Indice();
                    indice.LoadByPrimaryKey(ordemTermoBolsa.IdIndice.Value);

                    int idIndice = ordemTermoBolsa.IdIndice.Value;
                    dropIndiceTermo.Value = indice.Descricao;
                }
            }
            else {
                textTaxaTermo.Text = "";
                btnNumeroContratoTermo.Text = "";
                textDataVencimentoTermo.Text = "";
                dropIndiceTermo.SelectedIndex = -1;
            }


            Label labelEdicao = gridCadastro.FindEditFormTemplateControl("labelEdicao") as Label;

            int idCliente = Convert.ToInt32(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "IdCliente"));

            Cliente cliente = new Cliente();
            if (cliente.LoadByPrimaryKey(idCliente)) {
                DateTime dataDia = cliente.DataDia.Value;
                byte status = cliente.Status.Value;
                if (status == (byte)StatusCliente.Divulgado) {
                    travaPainel = true;
                    labelEdicao.Text = "Cliente está fechado. Dados não podem ser alterados.";
                }
                else if (DateTime.Compare(dataDia, data) > 0) {
                    travaPainel = true;
                    labelEdicao.Text = "Data informada anterior à data do cliente. Dados não podem ser alterados.";
                }
            }
        }

        if (travaPainel) {
            LinkButton btnOK = gridCadastro.FindEditFormTemplateControl("btnOK") as LinkButton;
            btnOK.Visible = false;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";

        if (!gridCadastro.IsEditing) //Testa deleção
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdCliente");
            List<object> keyData = gridCadastro.GetSelectedFieldValues("Data");
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idCliente = Convert.ToInt32(keyValuesId[i]);
                DateTime data = Convert.ToDateTime(keyData[i]);

                Cliente cliente = new Cliente();
                if (cliente.LoadByPrimaryKey(idCliente)) {
                    DateTime dataDia = cliente.DataDia.Value;
                    byte status = cliente.Status.Value;
                    string clienteDescricao = cliente.Apelido + " (" + cliente.IdCliente.ToString() + ")";
                    if (status == (byte)StatusCliente.Divulgado) {
                        e.Result = "Cliente " + clienteDescricao + " está fechado! Deleção cancelada.";
                        return;
                    }
                    else if (DateTime.Compare(dataDia, data) > 0) {
                        e.Result = "Lançamento com data " + data.ToString().Substring(0, 10) + " anterior à data do cliente " + clienteDescricao + "! Deleção cancelada.";
                        return;
                    }
                }
            }
        }
        else {
            ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
            ASPxComboBox dropAgenteCorretora = gridCadastro.FindEditFormTemplateControl("dropAgenteCorretora") as ASPxComboBox;
            ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
            ASPxComboBox dropTipoOrdem = gridCadastro.FindEditFormTemplateControl("dropTipoOrdem") as ASPxComboBox;
            ASPxButtonEdit btnEditAtivoBolsa = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa") as ASPxButtonEdit;
            ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
            ASPxSpinEdit textPU = gridCadastro.FindEditFormTemplateControl("textPU") as ASPxSpinEdit;
            ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
            ASPxComboBox dropTrader = gridCadastro.FindEditFormTemplateControl("dropTrader") as ASPxComboBox;
            CheckBox chkExercicio = gridCadastro.FindEditFormTemplateControl("chkExercicio") as CheckBox;
            ASPxComboBox dropLocalNegociacao = gridCadastro.FindEditFormTemplateControl("dropLocalNegociacao") as ASPxComboBox;
            ASPxComboBox dropClearing = gridCadastro.FindEditFormTemplateControl("dropClearing") as ASPxComboBox;
            ASPxComboBox dropLocalCustodia = gridCadastro.FindEditFormTemplateControl("dropLocalCustodia") as ASPxComboBox;

            #region Campos obrigatórios
            List<Control> controles = new List<Control>();
            controles.Add(btnEditCodigoCliente);
            controles.Add(dropAgenteCorretora);
            controles.Add(dropTipoOrdem);
            controles.Add(textData);
            controles.Add(btnEditAtivoBolsa);
            controles.Add(textQuantidade);
            controles.Add(textPU);
            controles.Add(textValor);
            controles.Add(dropLocalNegociacao);
            controles.Add(dropClearing);
            controles.Add(dropLocalCustodia);

            if (base.TestaObrigatorio(controles) != "") {
                e.Result = "Campos com * são obrigatórios!";
                return;
            }
            #endregion

            int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
            DateTime data = Convert.ToDateTime(textData.Text);

            AtivoBolsa ativoBolsa = new AtivoBolsa();
            ativoBolsa.LoadByPrimaryKey(btnEditAtivoBolsa.Text);

            if (ativoBolsa.IdMoeda == (int)ListaMoedaFixo.Real)
            {
                if (!Calendario.IsDiaUtil(data, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil))
                {
                    e.Result = "Data da operação não é dia útil.";
                    return;
                }
            }
            else
            {
                if (!Calendario.IsDiaUtil(data, LocalFeriadoFixo.NovaYork, TipoFeriado.Outros))
                {
                    e.Result = "Data da operação não é dia útil.";
                    return;
                }
            }

            Cliente cliente = new Cliente();
            if (cliente.LoadByPrimaryKey(idCliente)) {
                DateTime dataDia = cliente.DataDia.Value;
                byte status = cliente.Status.Value;
                if (status == (byte)StatusCliente.Divulgado) {
                    e.Result = "Cliente está fechado! Operação não pode ser realizada.";
                    return;
                }
                else if (DateTime.Compare(dataDia, Convert.ToDateTime(textData.Text)) > 0) {
                    e.Result = "Data informada anterior à data do cliente! Operação não pode ser realizada.";
                    return;
                }
            }

            // Se Data está Preenchida e Existe Ativo compara a Data de Vencimento
            if (ativoBolsa.TipoMercado == TipoMercadoBolsa.Termo && !String.IsNullOrEmpty(textData.Text)) {

                if (ativoBolsa.DataVencimento.HasValue && ativoBolsa.DataVencimento < Convert.ToDateTime(textData.Text)) {
                    e.Result = "Ativo Vencido em: " + ativoBolsa.DataVencimento.Value.ToString("d");
                    return;
                }
            }

            if (chkExercicio.Checked) {
                if (ativoBolsa.TipoMercado != TipoMercadoBolsa.OpcaoCompra && ativoBolsa.TipoMercado != TipoMercadoBolsa.OpcaoVenda) {
                    e.Result = "O ativo precisa ser opção de compra/venda para exercícios de opção.";
                    return;
                }
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOKAdd_Init(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        //ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        //btnEditCodigoCliente.Attributes.Add("onblur", "javascript: alert('teste'); return false;");

        string nome = "";
        string resultado = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") {
                if (cliente.Status.Value != (byte)StatusCliente.Divulgado) {

                    if (cliente.IsAtivo) {
                        PermissaoCliente permissaoCliente = new PermissaoCliente();
                        if (permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name))
                        {
                            nome = cliente.str.Apelido;
                            DateTime dataDia = cliente.DataDia.Value;
                            //
                            resultado = this.gridCadastro.IsNewRowEditing
                                       ? nome + "|" + dataDia.ToString().Substring(0, 10) + "|" + Thread.CurrentThread.CurrentCulture.Name
                                       : nome;
                        }
                        else {
                            resultado = "no_access";
                        }
                    }
                    else {
                        resultado = "no_active";
                    }
                }
                else {
                    resultado = cliente.str.Apelido + "|status_closed";
                }
            }
        }
        e.Result = resultado;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string resultado = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            AtivoBolsa ativoBolsa = new AtivoBolsa();
            ativoBolsa.LoadByPrimaryKey(Convert.ToString(e.Parameter));

            if (ativoBolsa.str.CdAtivoBolsa != "") {
                ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;

                string cdAtivoBolsa = ativoBolsa.str.CdAtivoBolsa;
                string idLocalCustodia = ativoBolsa.IdLocalCustodia.Value.ToString();
                string idLocalNegociacao = ativoBolsa.IdLocalNegociacao.Value.ToString();
                string idClearing = ativoBolsa.IdClearing.Value.ToString();

                //Fator de cotação
                string fatorCotacao = "";
                if (textData.Text != "") {
                    FatorCotacaoBolsa fatorCotacaoBolsa = new FatorCotacaoBolsa();
                    if (fatorCotacaoBolsa.BuscaFatorCotacaoBolsa(cdAtivoBolsa, Convert.ToDateTime(textData.Text))) {
                        fatorCotacao = fatorCotacaoBolsa.Fator.Value.ToString();
                    }
                    else {
                        e.Result = "noquote";
                        return;
                    }
                }

                //Vencimento do termo
                string dataVencimento = "";
                if (ativoBolsa.TipoMercado == TipoMercadoBolsa.Termo) {
                    dataVencimento = ativoBolsa.DataVencimento.Value.ToString().Substring(0, 10);
                }

                resultado = cdAtivoBolsa + "|" + fatorCotacao + "|" + dataVencimento + "|" + Thread.CurrentThread.CurrentCulture.Name + "|" + idLocalCustodia + "|" + idLocalNegociacao + "|" + idClearing;
            }
        }
        e.Result = resultado;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callBackCustos_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        if (btnEditCodigoCustos.Text == "" ||
            textCorretagem.Text == "" ||
            textTaxas.Text == "") {
            e.Result = "Os campos precisam ser preenchidos corretamente.";
            return;
        }

        int idCliente = Convert.ToInt32(btnEditCodigoCustos.Text);
        Cliente cliente = new Cliente();
        List<esQueryItem> campos = new List<esQueryItem>();
        campos.Add(cliente.Query.DataDia);
        campos.Add(cliente.Query.Status);
        if (!cliente.LoadByPrimaryKey(campos, idCliente)) {
            e.Result = "Cliente " + idCliente.ToString() + " inexistente.";
            return;
        }
        else if (cliente.Status.Value == (byte)StatusCliente.Divulgado) {
            e.Result = "Cliente " + idCliente.ToString() + " está fechado. Processo não pode ser executado.";
            return;
        }

        decimal corretagem = Convert.ToDecimal(textCorretagem.Text);
        decimal taxas = Convert.ToDecimal(textTaxas.Text);
        DateTime data = cliente.DataDia.Value;

        List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdOrdem");
        List<int> listaIds = new List<int>();
        for (int i = 0; i < keyValuesId.Count; i++) {
            int idOrdem = Convert.ToInt32(keyValuesId[i]);
            OrdemBolsa ordemBolsaCheck = new OrdemBolsa();
            ordemBolsaCheck.LoadByPrimaryKey(idOrdem);
            if (ordemBolsaCheck.Data.Value != data) {
                e.Result = "Existem ordens selecionadas com data diferente da data atual do cliente. Processo não pode ser executado.";
                return;
            }

            listaIds.Add(idOrdem);
        }

        OrdemBolsa ordemBolsa = new OrdemBolsa();
        if (listaIds.Count == 0) {
            ordemBolsa.RateiaTaxasTudo(idCliente, data, corretagem, taxas);
        }
        else {
            ordemBolsa.RateiaTaxasTudoPorIds(listaIds, corretagem, taxas);
        }


        e.Result = "Processo executado com sucesso.";
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callBackLote_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        bool ok = true;

        List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdOrdem");
        for (int i = 0; i < keyValuesId.Count; i++) {
            int idOrdem = Convert.ToInt32(keyValuesId[i]);

            OrdemBolsa ordemBolsa = new OrdemBolsa();
            if (ordemBolsa.LoadByPrimaryKey(idOrdem)) {
                int idCliente = ordemBolsa.IdCliente.Value;
                Cliente cliente = new Cliente();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(cliente.Query.DataDia);
                campos.Add(cliente.Query.Status);
                cliente.LoadByPrimaryKey(campos, idCliente);
                DateTime dataDia = cliente.DataDia.Value;
                byte status = cliente.Status.Value;

                if (DateTime.Compare(dataDia, ordemBolsa.Data.Value) > 0 || status == (byte)StatusCliente.Divulgado) {
                    ok = false;
                }
                else {
                    if (dropAgenteLote.SelectedIndex > -1) {
                        ordemBolsa.IdAgenteLiquidacao = Convert.ToInt32(dropAgenteLote.SelectedItem.Value);
                    }
                    if (dropTraderLote.SelectedIndex > -1) {
                        ordemBolsa.IdTrader = Convert.ToInt32(dropTraderLote.SelectedItem.Value);
                    }
                    if (textPercentualLote.Text != "") {
                        ordemBolsa.PercentualDesconto = Convert.ToDecimal(textPercentualLote.Text);
                    }
                    if (dropInformaCustos.SelectedIndex > -1) {
                        if (dropInformaCustos.SelectedItem.Value.ToString() == "S") {
                            ordemBolsa.CalculaDespesas = "N";
                        }
                        else {
                            ordemBolsa.CalculaDespesas = "S";
                        }
                    }
                }

                ordemBolsa.Save();
            }
        }

        if (ok) {
            e.Result = "Processo executado com sucesso.";
        }
        else {
            e.Result = "Cliente(s) com data dia posterior à data da ordem a ser alterada ou com status fechado. Processo executado parcialmente.";
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        int idOrdem = (int)e.Keys[0];
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxButtonEdit btnEditAtivoBolsa = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa") as ASPxButtonEdit;
        ASPxComboBox dropAgenteCorretora = gridCadastro.FindEditFormTemplateControl("dropAgenteCorretora") as ASPxComboBox;
        ASPxComboBox dropAgenteLiquidacao = gridCadastro.FindEditFormTemplateControl("dropAgenteLiquidacao") as ASPxComboBox;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxComboBox dropTipoOrdem = gridCadastro.FindEditFormTemplateControl("dropTipoOrdem") as ASPxComboBox;
        ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
        ASPxSpinEdit textPU = gridCadastro.FindEditFormTemplateControl("textPU") as ASPxSpinEdit;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
        CheckBox chkCustosInformados = gridCadastro.FindEditFormTemplateControl("chkCustosInformados") as CheckBox;
        ASPxComboBox dropTrader = gridCadastro.FindEditFormTemplateControl("dropTrader") as ASPxComboBox;
        ASPxSpinEdit textPercentual = gridCadastro.FindEditFormTemplateControl("textPercentual") as ASPxSpinEdit;
        CheckBox chkExercicio = gridCadastro.FindEditFormTemplateControl("chkExercicio") as CheckBox;
        ASPxComboBox dropLocalNegociacao = gridCadastro.FindEditFormTemplateControl("dropLocalNegociacao") as ASPxComboBox;
        ASPxComboBox dropCategoriaMovimentacao = gridCadastro.FindEditFormTemplateControl("dropCategoriaMovimentacao") as ASPxComboBox;
        ASPxComboBox dropClearing = gridCadastro.FindEditFormTemplateControl("dropClearing") as ASPxComboBox;
        ASPxComboBox dropLocalCustodia = gridCadastro.FindEditFormTemplateControl("dropLocalCustodia") as ASPxComboBox;

        OrdemBolsa ordemBolsa = new OrdemBolsa();
        if (ordemBolsa.LoadByPrimaryKey(idOrdem)) {
            int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);

            ordemBolsa.IdCliente = idCliente;
            ordemBolsa.CdAtivoBolsa = Convert.ToString(btnEditAtivoBolsa.Text).ToUpper();
            ordemBolsa.IdAgenteCorretora = Convert.ToInt32(dropAgenteCorretora.SelectedItem.Value);

            //Trata agente liquidacao
            if (ParametrosConfiguracaoSistema.Outras.AgenteLiquidacaoExclusivo)
            {
                ordemBolsa.IdAgenteLiquidacao = Convert.ToInt32(dropAgenteLiquidacao.SelectedItem.Value);
            }
            else {
                ordemBolsa.IdAgenteLiquidacao = Convert.ToInt32(dropAgenteCorretora.SelectedItem.Value);
            }
            //

            AtivoBolsa ativoBolsa = new AtivoBolsa();
            ativoBolsa.LoadByPrimaryKey(Convert.ToString(btnEditAtivoBolsa.Text));
            string tipoMercadoAtivo = ativoBolsa.TipoMercado;
            int idMoeda = ativoBolsa.IdMoeda.Value;

            ordemBolsa.IdMoeda = idMoeda;
            ordemBolsa.TipoMercado = tipoMercadoAtivo;
            ordemBolsa.TipoOrdem = Convert.ToString(dropTipoOrdem.SelectedItem.Value);

            //Trata exercicio!!!! ****************
            if (chkExercicio.Checked) {
                if (tipoMercadoAtivo == TipoMercadoBolsa.OpcaoCompra) {
                    ordemBolsa.Origem = (byte)OrigemOrdemBolsa.ExercicioOpcaoCompra;
                }
                else if (tipoMercadoAtivo == TipoMercadoBolsa.OpcaoVenda) {
                    ordemBolsa.Origem = (byte)OrigemOrdemBolsa.ExercicioOpcaoVenda;
                }
            }
            else {
                ordemBolsa.Origem = (byte)OrigemOrdemBolsa.Primaria;
            }

            if (ordemBolsa.Origem == (byte)OrigemOrdemBolsa.ExercicioOpcaoCompra ||
                ordemBolsa.Origem == (byte)OrigemOrdemBolsa.ExercicioOpcaoVenda) {
                //Precisa transformar a opcao lancada para a vista e usar o ativo informado como opcao (CdAtivoBolsaOpcao)
                ordemBolsa.CdAtivoBolsa = ativoBolsa.CdAtivoBolsaObjeto;
                ordemBolsa.CdAtivoBolsaOpcao = Convert.ToString(btnEditAtivoBolsa.Text).ToUpper();
                ordemBolsa.TipoMercado = TipoMercadoBolsa.MercadoVista;
            }
            //**************************************

            ordemBolsa.Pu = Convert.ToDecimal(textPU.Text);
            ordemBolsa.Valor = Convert.ToDecimal(textValor.Text);
            ordemBolsa.Quantidade = Convert.ToDecimal(textQuantidade.Text);

            DateTime data = Convert.ToDateTime(textData.Text);
            ordemBolsa.Data = Convert.ToDateTime(textData.Text.ToString());

            if (dropTrader.SelectedItem != null) {
                ordemBolsa.IdTrader = Convert.ToInt32(dropTrader.SelectedItem.Value);
            }
            else {
                ordemBolsa.IdTrader = null; // Vazio
            }

            if (dropCategoriaMovimentacao.SelectedIndex > -1)
            {
                ordemBolsa.IdCategoriaMovimentacao = Convert.ToInt32(dropCategoriaMovimentacao.SelectedItem.Value);
            }
            else
            {
                ordemBolsa.IdCategoriaMovimentacao = null;
            }

            ordemBolsa.PercentualDesconto = textPercentual.Text != "" ? Convert.ToDecimal(textPercentual.Text) : 0;

            //Trata Custos Informados
            ordemBolsa.CalculaDespesas = chkCustosInformados.Checked ? "N" : "S";
            //

            ordemBolsa.IdLocalNegociacao = Convert.ToInt32(dropLocalNegociacao.SelectedItem.Value);
            ordemBolsa.IdLocalCustodia = Convert.ToInt32(dropLocalCustodia.SelectedItem.Value);
            ordemBolsa.IdClearing = Convert.ToInt32(dropClearing.SelectedItem.Value);
            ordemBolsa.DataOperacao = Convert.ToDateTime(textData.Text.ToString());

            ordemBolsa.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de OrdemBolsa - Operacao: Update OrdemBolsa: " + idOrdem + UtilitarioWeb.ToString(ordemBolsa),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion

            //Salva a operação termo bolsa
            if (tipoMercadoAtivo == TipoMercadoBolsa.Termo) {
                ASPxSpinEdit textTaxaTermo = gridCadastro.FindEditFormTemplateControl("textTaxaTermo") as ASPxSpinEdit;
                ASPxButtonEdit btnNumeroContratoTermo = gridCadastro.FindEditFormTemplateControl("btnNumeroContratoTermo") as ASPxButtonEdit;
                ASPxComboBox dropIndiceTermo = gridCadastro.FindEditFormTemplateControl("dropIndiceTermo") as ASPxComboBox;

                short? idIndice = null;
                if (dropIndiceTermo.SelectedIndex != -1) {
                    idIndice = Convert.ToInt16(dropIndiceTermo.SelectedItem.Value);
                }
                decimal taxa = 0;
                if (textTaxaTermo.Text != "") {
                    taxa = Convert.ToDecimal(textTaxaTermo.Text);
                }
                string numeroContrato = btnNumeroContratoTermo.Text.ToString();
                OrdemTermoBolsa ordemTermoBolsa = new OrdemTermoBolsa();
                if (!ordemTermoBolsa.LoadByPrimaryKey(idOrdem)) {
                    OrdemTermoBolsa ordemTermoBolsaInsert = new OrdemTermoBolsa();
                    ordemTermoBolsaInsert.IdOrdem = idOrdem;
                    ordemTermoBolsaInsert.NumeroContrato = numeroContrato;
                    ordemTermoBolsaInsert.Taxa = taxa;
                    ordemTermoBolsaInsert.IdIndice = idIndice;
                    ordemTermoBolsaInsert.Save();
                }
                else {
                    ordemTermoBolsa.NumeroContrato = numeroContrato;
                    ordemTermoBolsa.Taxa = taxa;
                    ordemTermoBolsa.IdIndice = idIndice;
                    ordemTermoBolsa.Save();
                }
            }
            else {
                OrdemTermoBolsa ordemTermoBolsaDeletar = new OrdemTermoBolsa();
                if (ordemTermoBolsaDeletar.LoadByPrimaryKey(idOrdem)) {
                    ordemTermoBolsaDeletar.MarkAsDeleted();
                    ordemTermoBolsaDeletar.Save();
                }
            }

            //Atualiza StatusRealTime para executar***********
            Cliente cliente = new Cliente();
            cliente.SetaFlagRealTime(idCliente, (byte)StatusRealTimeCliente.Executar);
            //************************************************
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    private void SalvarNovo() {
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxButtonEdit btnEditAtivoBolsa = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa") as ASPxButtonEdit;
        ASPxComboBox dropAgenteCorretora = gridCadastro.FindEditFormTemplateControl("dropAgenteCorretora") as ASPxComboBox;
        ASPxComboBox dropAgenteLiquidacao = gridCadastro.FindEditFormTemplateControl("dropAgenteLiquidacao") as ASPxComboBox;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxComboBox dropTipoOrdem = gridCadastro.FindEditFormTemplateControl("dropTipoOrdem") as ASPxComboBox;
        ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
        ASPxSpinEdit textPU = gridCadastro.FindEditFormTemplateControl("textPU") as ASPxSpinEdit;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
        CheckBox chkCustosInformados = gridCadastro.FindEditFormTemplateControl("chkCustosInformados") as CheckBox;
        ASPxComboBox dropTrader = gridCadastro.FindEditFormTemplateControl("dropTrader") as ASPxComboBox;
        ASPxSpinEdit textPercentual = gridCadastro.FindEditFormTemplateControl("textPercentual") as ASPxSpinEdit;
        CheckBox chkExercicio = gridCadastro.FindEditFormTemplateControl("chkExercicio") as CheckBox;
        ASPxComboBox dropLocalNegociacao = gridCadastro.FindEditFormTemplateControl("dropLocalNegociacao") as ASPxComboBox;
        ASPxComboBox dropCategoriaMovimentacao = gridCadastro.FindEditFormTemplateControl("dropCategoriaMovimentacao") as ASPxComboBox;
        ASPxComboBox dropClearing = gridCadastro.FindEditFormTemplateControl("dropClearing") as ASPxComboBox;
        ASPxComboBox dropLocalCustodia = gridCadastro.FindEditFormTemplateControl("dropLocalCustodia") as ASPxComboBox;

        OrdemBolsa ordemBolsa = new OrdemBolsa();

        int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);

        ordemBolsa.IdCliente = idCliente;
        ordemBolsa.CdAtivoBolsa = Convert.ToString(btnEditAtivoBolsa.Text).ToUpper();
        ordemBolsa.IdAgenteCorretora = Convert.ToInt32(dropAgenteCorretora.SelectedItem.Value);

        //Trata agente liquidacao
        if (dropAgenteLiquidacao.SelectedIndex > -1) {
            ordemBolsa.IdAgenteLiquidacao = Convert.ToInt32(dropAgenteLiquidacao.SelectedItem.Value);
        }
        else {
            ordemBolsa.IdAgenteLiquidacao = Convert.ToInt32(dropAgenteCorretora.SelectedItem.Value);
        }
        //

        AtivoBolsa ativoBolsa = new AtivoBolsa();
        ativoBolsa.LoadByPrimaryKey(Convert.ToString(btnEditAtivoBolsa.Text));
        string tipoMercadoAtivo = ativoBolsa.TipoMercado;
        int idMoeda = ativoBolsa.IdMoeda.Value;

        ordemBolsa.IdMoeda = idMoeda;
        ordemBolsa.TipoMercado = tipoMercadoAtivo;
        ordemBolsa.TipoOrdem = Convert.ToString(dropTipoOrdem.SelectedItem.Value);

        //Trata exercicio!!!! ****************
        if (chkExercicio.Checked) {
            if (tipoMercadoAtivo == TipoMercadoBolsa.OpcaoCompra) {
                ordemBolsa.Origem = (byte)OrigemOrdemBolsa.ExercicioOpcaoCompra;
            }
            else if (tipoMercadoAtivo == TipoMercadoBolsa.OpcaoVenda) {
                ordemBolsa.Origem = (byte)OrigemOrdemBolsa.ExercicioOpcaoVenda;
            }
        }
        else {
            ordemBolsa.Origem = (byte)OrigemOrdemBolsa.Primaria;
        }

        if (ordemBolsa.Origem == (byte)OrigemOrdemBolsa.ExercicioOpcaoCompra ||
            ordemBolsa.Origem == (byte)OrigemOrdemBolsa.ExercicioOpcaoVenda) {
            //Precisa transformar a opcao lancada para a vista e usar o ativo informado como opcao (CdAtivoBolsaOpcao)
            ordemBolsa.CdAtivoBolsa = ativoBolsa.CdAtivoBolsaObjeto;
            ordemBolsa.CdAtivoBolsaOpcao = Convert.ToString(btnEditAtivoBolsa.Text).ToUpper();
            ordemBolsa.TipoMercado = TipoMercadoBolsa.MercadoVista;
        }
        //**************************************

        ordemBolsa.Pu = Convert.ToDecimal(textPU.Text);
        ordemBolsa.Valor = Convert.ToDecimal(textValor.Text);
        ordemBolsa.Quantidade = Convert.ToDecimal(textQuantidade.Text);

        DateTime data = Convert.ToDateTime(textData.Text);
        ordemBolsa.Data = Convert.ToDateTime(textData.Text.ToString());
        ordemBolsa.Fonte = (byte)FonteOrdemBolsa.Manual;
        ordemBolsa.PercentualDesconto = 0; //Por ora sem opção de informar o percentual
        ordemBolsa.Desconto = 0; //Por ora sem opção de informar o percentual
        ordemBolsa.QuantidadeDayTrade = 0;

        if (dropTrader.SelectedItem != null) {
            ordemBolsa.IdTrader = Convert.ToInt32(dropTrader.SelectedItem.Value);
        }
        else {
            ordemBolsa.IdTrader = null; // Vazio
        }

        if (textPercentual.Text != "") {
            ordemBolsa.PercentualDesconto = Convert.ToDecimal(textPercentual.Text);
        }
        else {
            ordemBolsa.PercentualDesconto = 0;
        }

        if (dropCategoriaMovimentacao.SelectedIndex > -1)
        {
            ordemBolsa.IdCategoriaMovimentacao = Convert.ToInt32(dropCategoriaMovimentacao.SelectedItem.Value);
        }
        else
        {
            ordemBolsa.IdCategoriaMovimentacao = null;
        }

        //Trata Custos Informados
        ordemBolsa.CalculaDespesas = chkCustosInformados.Checked ? "N" : "S";

        ordemBolsa.IdLocalNegociacao = Convert.ToInt32(dropLocalNegociacao.SelectedItem.Value);
        ordemBolsa.IdLocalCustodia = Convert.ToInt32(dropLocalCustodia.SelectedItem.Value);
        ordemBolsa.IdClearing = Convert.ToInt32(dropClearing.SelectedItem.Value);
        ordemBolsa.DataOperacao = Convert.ToDateTime(textData.Text.ToString());

        ordemBolsa.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de OrdemBolsa - Operacao: Insert OrdemBolsa: " + ordemBolsa.IdOrdem + UtilitarioWeb.ToString(ordemBolsa),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        //Salva a operação termo bolsa
        if (tipoMercadoAtivo == TipoMercadoBolsa.Termo) {
            ASPxSpinEdit textTaxaTermo = gridCadastro.FindEditFormTemplateControl("textTaxaTermo") as ASPxSpinEdit;
            ASPxButtonEdit btnNumeroContratoTermo = gridCadastro.FindEditFormTemplateControl("btnNumeroContratoTermo") as ASPxButtonEdit;
            ASPxDateEdit textDataVencimentoTermo = gridCadastro.FindEditFormTemplateControl("textDataVencimentoTermo") as ASPxDateEdit;
            ASPxComboBox dropIndiceTermo = gridCadastro.FindEditFormTemplateControl("dropIndiceTermo") as ASPxComboBox;

            short? idIndice = null;
            if (dropIndiceTermo.SelectedIndex != -1) {
                idIndice = Convert.ToInt16(dropIndiceTermo.SelectedItem.Value);
            }
            decimal taxa = 0;
            if (textTaxaTermo.Text != "") {
                taxa = Convert.ToDecimal(textTaxaTermo.Text);
            }
            string numeroContrato = btnNumeroContratoTermo.Text.ToString();
            DateTime dataVencimento = Convert.ToDateTime(textDataVencimentoTermo.Text);
            OrdemTermoBolsa ordemTermoBolsa = new OrdemTermoBolsa();

            OrdemTermoBolsa ordemTermoBolsaInsert = new OrdemTermoBolsa();
            ordemTermoBolsaInsert.IdOrdem = ordemBolsa.IdOrdem; //Usa o mesmo Id de OrdemBolsa
            ordemTermoBolsaInsert.NumeroContrato = numeroContrato;
            ordemTermoBolsaInsert.Taxa = taxa;
            ordemTermoBolsaInsert.IdIndice = idIndice;
            ordemTermoBolsaInsert.Save();
        }

        //Atualiza StatusRealTime para executar***********
        Cliente cliente = new Cliente();
        cliente.SetaFlagRealTime(idCliente, (byte)StatusRealTimeCliente.Executar);
        //************************************************        
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        SalvarNovo();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(OrdemBolsaMetadata.ColumnNames.IdOrdem);
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idOrdem = Convert.ToInt32(keyValuesId[i]);

                OrdemBolsa ordemBolsa = new OrdemBolsa();
                if (ordemBolsa.LoadByPrimaryKey(idOrdem)) {
                    int idCliente = ordemBolsa.IdCliente.Value;

                    //
                    OrdemBolsa ordemBolsaClone = (OrdemBolsa)Utilitario.Clone(ordemBolsa);
                    //

                    ordemBolsa.MarkAsDeleted();
                    ordemBolsa.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de OrdemBolsa - Operacao: Delete OrdemBolsa: " + idOrdem + UtilitarioWeb.ToString(ordemBolsaClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion

                    //Atualiza StatusRealTime para executar***********
                    Cliente cliente = new Cliente();
                    cliente.SetaFlagRealTime(idCliente, (byte)StatusRealTimeCliente.Executar);
                    //************************************************
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing) {
            ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
            e.Properties["cpBtnEditCodigoCliente"] = btnEditCodigoCliente.ClientID;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_InitNewRow(object sender, ASPxDataInitNewRowEventArgs e) {
        //Valores default do form
        //e.NewValues["PercentualDesconto"] = "0";
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("btnEditCodigoCliente");
        base.gridCadastro_PreRender(sender, e);

        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        Label labelFiltroHeader = gridCadastro.FindStatusBarTemplateControl("labelFiltroHeader") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");

        if (btnEditCodigoClienteFiltro != null && btnEditCodigoClienteFiltro.Text != "") {
            texto.Append(" Cliente: ").Append(btnEditCodigoClienteFiltro.Text);
        }
        if (textDataInicio.Text != "") {
            if (texto.Length > 0) {
                texto.Append(" |");
            }
            texto.Append(" Data Início >= ").Append(textDataInicio.Text.Substring(0, 10));
        }
        if (textDataFim.Text != "") {
            if (texto.Length > 0) {
                texto.Append(" |");
            }
            texto.Append(" Data Fim <= ").Append(textDataFim.Text.Substring(0, 10));
        }
        if (textCdAtivo.Text != "") {
            if (texto.Length > 0) {
                texto.Append(" |");
            }
            texto.Append(" Ativo Like %").Append(textCdAtivo.Text).Append("%");
        }
        if (dropTipoOperacaoFiltro.SelectedIndex > -1) {
            if (texto.Length > 0) {
                texto.Append(" |");
            }
            texto.Append(" Tipo = ").Append(dropTipoOperacaoFiltro.Text);
        }
        if (dropTipoMercado.SelectedIndex > 0) {
            if (texto.Length > 0) {
                texto.Append(" |");
            }
            texto.Append(" Mercado = ").Append(dropTipoMercado.SelectedItem.Text);
        }
        if (dropTraderFiltro.SelectedIndex != -1) {
            if (texto.Length > 0) {
                texto.Append(" |");
            }
            texto.Append(" Trader = ").Append(dropTraderFiltro.SelectedItem.Text);
        }

        labelFiltro.Text = texto.ToString();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e) {
        if (e.Column.FieldName == OrdemBolsaMetadata.ColumnNames.TipoOrdem) {
            if (Convert.ToByte(gridCadastro.GetRowValues(e.VisibleRowIndex, OrdemBolsaMetadata.ColumnNames.Origem)) == OrigemOperacaoBolsa.ExercicioOpcaoCompra ||
                Convert.ToByte(gridCadastro.GetRowValues(e.VisibleRowIndex, OrdemBolsaMetadata.ColumnNames.Origem)) == OrigemOperacaoBolsa.ExercicioOpcaoVenda) {

                e.DisplayText = e.Value.ToString() == TipoOrdemBolsa.Compra ? "Exercício-Compra" : "Exercício-Venda";
            }
        }
    }

    /// <summary>
    ///  Em testes para acrescentar Filtro por Exercicio-Compra e Exercicio-Venda
    ///  Acrescentar dxe:ListEditItem Value="EC" Text="Exercicio Compra"
    ///              dxe:ListEditItem Value="EV" Text="Exercicio Venda"  no combo de filtro
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_ProcessColumnAutoFilter(object sender, ASPxGridViewAutoFilterEventArgs e) {
        //if (e.Column.FieldName != OrdemBolsaMetadata.ColumnNames.TipoOrdem) {
        //    return;
        //}

        //if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria) {
        //    // Creates a new filter criterion and applies it.
        //    if (e.Value == "EC") {
        //        e.Criteria = (new OperandProperty(OrdemBolsaMetadata.ColumnNames.TipoOrdem) == TipoOrdemBolsa.Compra) &
        //                     (new OperandProperty(OrdemBolsaMetadata.ColumnNames.Origem) == OrigemOperacaoBolsa.ExercicioOpcaoCompra);
        //    }
        //    else if (e.Value == "EV") {
        //        e.Criteria = (new OperandProperty(OrdemBolsaMetadata.ColumnNames.TipoOrdem) == TipoOrdemBolsa.Venda) &
        //                     (new OperandProperty(OrdemBolsaMetadata.ColumnNames.Origem) == OrigemOperacaoBolsa.ExercicioOpcaoVenda);
        //    }
        //    else if (e.Value == TipoOrdemBolsa.Compra) {
        //        e.Criteria = new OperandProperty(OrdemBolsaMetadata.ColumnNames.TipoOrdem) == TipoOrdemBolsa.Compra;

        //    }
        //    else if (e.Value == TipoOrdemBolsa.Venda) {
        //        e.Criteria = new OperandProperty(OrdemBolsaMetadata.ColumnNames.TipoOrdem) == TipoOrdemBolsa.Venda;
        //    }
        //}
        //else {
        //    //DateTime res;
        //    //if (!DateTime.TryParse(e.Value, out res)) {
        //    //    e.Value = string.Empty;
        //    //}
        //    //else {
        //    //    // Specifies the text dosplayed within the auto-filter row cell.
        //    //    e.Value = res.Year.ToString();
        //    //}
        //}
    }

    protected void EsDSCategoriaMovimentacao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CategoriaMovimentacaoCollection coll = new CategoriaMovimentacaoCollection();
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
}