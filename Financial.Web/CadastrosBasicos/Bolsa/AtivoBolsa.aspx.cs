﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;

using DevExpress.Web;

using Financial.Bolsa;
using Financial.Common;
using Financial.Util;

using Financial.Bolsa.Enums;
using Financial.Common.Enums;
using Financial.Web.Common;
using System.Drawing;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Fundo;

public partial class CadastrosBasicos_AtivoBolsa : CadastroBasePage
{
    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasPopupEmissor = true;
        base.Page_Load(sender, e);

        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
                          new List<string>(new string[] { AtivoBolsaMetadata.ColumnNames.TipoMercado }));

    }

    #region DataSources
    protected void EsDSAtivoBolsa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        AtivoBolsaQuery ativo = new AtivoBolsaQuery("a");
        EmissorQuery emissor = new EmissorQuery("e");

        ativo.Select(ativo, emissor.Nome.As("NomeEmissor"));
        ativo.InnerJoin(emissor).On(ativo.IdEmissor == emissor.IdEmissor);
        ativo.OrderBy(ativo.CdAtivoBolsa.Ascending);

        AtivoBolsaCollection coll = new AtivoBolsaCollection();
        coll.Load(ativo);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSLocalCustodia_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        LocalCustodiaCollection coll = new LocalCustodiaCollection();

        coll.Query.OrderBy(coll.Query.IdLocalCustodia.Ascending);
        coll.LoadAll();

        e.Collection = coll;
    }

    protected void EsDSClearing_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ClearingCollection coll = new ClearingCollection();

        coll.Query.OrderBy(coll.Query.IdClearing.Ascending);
        coll.LoadAll();

        e.Collection = coll;
    }

    protected void EsDSLocalNegociacao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        LocalNegociacaoCollection coll = new LocalNegociacaoCollection();

        coll.Query.OrderBy(coll.Query.IdLocalNegociacao.Ascending);
        coll.LoadAll();

        e.Collection = coll;
    }

    protected void EsDSEmissor_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        EmissorCollection coll = new EmissorCollection();

        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSMoeda_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        MoedaCollection coll = new MoedaCollection();

        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSEstrategia_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        EstrategiaCollection coll = new EstrategiaCollection();
        EstrategiaQuery estrategiaQuery = new EstrategiaQuery("estrategia");

        StringBuilder descricaoField = new StringBuilder();
        descricaoField.Append("< cast(estrategia.[IdEstrategia] as varchar(20)) + ' - ' + estrategia.descricao as Descricao >");

        estrategiaQuery.Select(descricaoField.ToString(), estrategiaQuery.IdEstrategia);
        estrategiaQuery.OrderBy(estrategiaQuery.IdEstrategia.Ascending);
        coll.Load(estrategiaQuery);

        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string texto = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idEmissor = Convert.ToInt32(e.Parameter);
            Emissor emissor = new Emissor();

            if (emissor.LoadByPrimaryKey(idEmissor))
            {
                texto = emissor.IdEmissor + " - " + emissor.Nome;
            }
        }
        e.Result = texto;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        ASPxTextBox textCdAtivoBolsa = gridCadastro.FindEditFormTemplateControl("textCdAtivoBolsa") as ASPxTextBox;
        ASPxTextBox textEspecificacao = gridCadastro.FindEditFormTemplateControl("textEspecificacao") as ASPxTextBox;
        ASPxComboBox dropTipoMercado = gridCadastro.FindEditFormTemplateControl("dropTipoMercado") as ASPxComboBox;
        ASPxComboBox dropMoeda = gridCadastro.FindEditFormTemplateControl("dropMoeda") as ASPxComboBox;
        TextBox textCodigoIsin = gridCadastro.FindEditFormTemplateControl("textCodigoIsin") as TextBox;
        TextBox textCdAtivoBolsaObjeto = gridCadastro.FindEditFormTemplateControl("textCdAtivoBolsaObjeto") as TextBox;
        ASPxDateEdit textDataVencimento = gridCadastro.FindEditFormTemplateControl("textDataVencimento") as ASPxDateEdit;
        ASPxComboBox dropTipoPapel = gridCadastro.FindEditFormTemplateControl("dropTipoPapel") as ASPxComboBox;
        ASPxButtonEdit btnEditEmissor = gridCadastro.FindEditFormTemplateControl("btnEditEmissor") as ASPxButtonEdit;
        TextBox textCodigoCusip = gridCadastro.FindEditFormTemplateControl("textCodigoCusip") as TextBox;
        ASPxComboBox dropLocalCustodia = gridCadastro.FindEditFormTemplateControl("dropLocalCustodia") as ASPxComboBox;
        ASPxComboBox dropLocalNegociacao = gridCadastro.FindEditFormTemplateControl("dropLocalNegociacao") as ASPxComboBox;
        ASPxComboBox dropClearing = gridCadastro.FindEditFormTemplateControl("dropClearing") as ASPxComboBox;
        ASPxComboBox dropEstrategia = gridCadastro.FindEditFormTemplateControl("dropEstrategia") as ASPxComboBox;
        

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(textCdAtivoBolsa);
        controles.Add(textEspecificacao);
        controles.Add(dropTipoMercado);
        controles.Add(btnEditEmissor);
        controles.Add(dropMoeda);
        controles.Add(dropTipoPapel);
        controles.Add(dropLocalCustodia);
        controles.Add(dropLocalNegociacao);
        controles.Add(dropClearing);
        controles.Add(dropEstrategia);

        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        string cdAtivoBolsa = Convert.ToString(textCdAtivoBolsa.Text);

        if (dropTipoMercado.SelectedItem.Value != TipoMercadoBolsa.Termo && cdAtivoBolsa.Length > 25)
        {
            e.Result = "Código deve ter no máximo 10 caracteres.";
            return;
        }

        if (gridCadastro.IsNewRowEditing)
        {
            AtivoBolsa ativoBolsa = new AtivoBolsa();
            if (ativoBolsa.LoadByPrimaryKey(cdAtivoBolsa.Trim()))
            {
                e.Result = "Registro já existente";
                return;
            }
        }

        if (!String.IsNullOrEmpty(textCdAtivoBolsaObjeto.Text))
        {
            AtivoBolsa ativoBolsaObjeto = new AtivoBolsa();
            if (!ativoBolsaObjeto.LoadByPrimaryKey(textCdAtivoBolsaObjeto.Text))
            {
                e.Result = "Código Ativo objeto não cadastrado";
                return;
            }
        }

        if (!string.IsNullOrEmpty(textDataVencimento.Text))
        {
            if (!Calendario.IsDiaUtil(Convert.ToDateTime(textDataVencimento.Text), LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil))
            {
                e.Result = "Data de vencimento não é dia útil.";
                return;
            }
        }

        if (textCodigoCusip.Text.Length < 9 && !string.IsNullOrEmpty(textCodigoCusip.Text))
        {
            e.Result = "Código Cusip deve possuir 9 caracteres.";
            return;
        }

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textCdAtivoBolsa_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
        {
            (sender as ASPxTextBox).Enabled = false;
            (sender as ASPxTextBox).BackColor = Color.FromName("#EBEBEB");
        }

        if (gridCadastro.IsEditing && !gridCadastro.IsNewRowEditing)
        {
            var chkParticipacao = gridCadastro.FindEditFormTemplateControl("chkParticipacao") as ASPxCheckBox;
            if(chkParticipacao == null) return;

            var participacao = gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "Participacao");
            if (participacao == null) return;

            chkParticipacao.Checked = participacao.ToString() == "S";
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    private void SalvarNovo()
    {
        ASPxTextBox textCdAtivoBolsa = gridCadastro.FindEditFormTemplateControl("textCdAtivoBolsa") as ASPxTextBox;
        ASPxTextBox textEspecificacao = gridCadastro.FindEditFormTemplateControl("textEspecificacao") as ASPxTextBox;
        TextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as TextBox;
        ASPxComboBox dropTipoMercado = gridCadastro.FindEditFormTemplateControl("dropTipoMercado") as ASPxComboBox;
        ASPxSpinEdit textPUExercicio = gridCadastro.FindEditFormTemplateControl("textPUExercicio") as ASPxSpinEdit;
        ASPxDateEdit textDataVencimento = gridCadastro.FindEditFormTemplateControl("textDataVencimento") as ASPxDateEdit;
        TextBox textCdAtivoBolsaObjeto = gridCadastro.FindEditFormTemplateControl("textCdAtivoBolsaObjeto") as TextBox;
        TextBox textCodigoIsin = gridCadastro.FindEditFormTemplateControl("textCodigoIsin") as TextBox;
        ASPxComboBox dropMoeda = gridCadastro.FindEditFormTemplateControl("dropMoeda") as ASPxComboBox;
        ASPxComboBox dropEstrategia = gridCadastro.FindEditFormTemplateControl("dropEstrategia") as ASPxComboBox;
        TextBox textNumeroSerie = gridCadastro.FindEditFormTemplateControl("textNumeroSerie") as TextBox;
        ASPxComboBox dropTipoPapel = gridCadastro.FindEditFormTemplateControl("dropTipoPapel") as ASPxComboBox;
        TextBox textCodigoCusip = gridCadastro.FindEditFormTemplateControl("textCodigoCusip") as TextBox;
        ASPxComboBox dropLocalNegociacao = gridCadastro.FindEditFormTemplateControl("dropLocalNegociacao") as ASPxComboBox;
        ASPxComboBox dropLocalCustodia = gridCadastro.FindEditFormTemplateControl("dropLocalCustodia") as ASPxComboBox;
        ASPxComboBox dropClearing = gridCadastro.FindEditFormTemplateControl("dropClearing") as ASPxComboBox;
        ASPxSpinEdit textCodigoCDA = gridCadastro.FindEditFormTemplateControl("textCodigoCDA") as ASPxSpinEdit;
        ASPxSpinEdit textCodigoBDS = gridCadastro.FindEditFormTemplateControl("textCodigoBDS") as ASPxSpinEdit;
        ASPxComboBox dropInvestimentoColetivoCvm = gridCadastro.FindEditFormTemplateControl("dropInvestimentoColetivoCvm") as ASPxComboBox;
        ASPxCheckBox chkParticipacao = gridCadastro.FindEditFormTemplateControl("chkParticipacao") as ASPxCheckBox;

        TextBox hiddenIdEmissor = gridCadastro.FindEditFormTemplateControl("hiddenIdEmissor") as TextBox;

        //
        AtivoBolsa ativoBolsa = new AtivoBolsa();
        //
        ativoBolsa.CdAtivoBolsa = textCdAtivoBolsa.Text.Trim().ToUpper();
        ativoBolsa.Especificacao = textEspecificacao.Text.Trim().ToUpper();
        ativoBolsa.Descricao = textDescricao.Text.Trim();
        ativoBolsa.TipoMercado = Convert.ToString(dropTipoMercado.SelectedItem.Value);
        ativoBolsa.IdLocalCustodia = Convert.ToInt16(dropLocalCustodia.SelectedItem.Value);

        if (dropClearing.SelectedIndex > -1)
        {
            ativoBolsa.IdClearing = Convert.ToInt16(dropClearing.SelectedItem.Value);
        }
        else
        {
            ativoBolsa.IdClearing = null;
        }

        if (dropLocalNegociacao.SelectedIndex > -1)
        {
            ativoBolsa.IdLocalNegociacao = Convert.ToInt16(dropLocalNegociacao.SelectedItem.Value);
        }
        else
        {
            ativoBolsa.IdLocalNegociacao = null;
        }


        if (!String.IsNullOrEmpty(textPUExercicio.Text.Trim()))
        {
            ativoBolsa.PUExercicio = Convert.ToDecimal(textPUExercicio.Text);
        }

        if (!String.IsNullOrEmpty(textDataVencimento.Text.Trim()))
        {
            ativoBolsa.DataVencimento = Convert.ToDateTime(textDataVencimento.Text);
        }

        ativoBolsa.CdAtivoBolsaObjeto = textCdAtivoBolsaObjeto.Text.Trim().ToUpper();
        ativoBolsa.CodigoIsin = textCodigoIsin.Text.ToString().Trim();
        ativoBolsa.IdMoeda = Convert.ToInt16(dropMoeda.SelectedItem.Value);

        ativoBolsa.IdEmissor = Convert.ToInt32(hiddenIdEmissor.Text);

        ativoBolsa.NumeroSerie = 0;
        if (textNumeroSerie.Text != "")
        {
            ativoBolsa.NumeroSerie = Convert.ToInt32(textNumeroSerie.Text);
        }

        // HACK AtivoBolsa.OpcaoIndice: Quando for usado, deve ser ajustado
        ativoBolsa.OpcaoIndice = 0;

        if (dropEstrategia.SelectedIndex != -1)
        {
            ativoBolsa.IdEstrategia = Convert.ToInt32(dropEstrategia.SelectedItem.Value);
        }
        else
        {
            AtivoBolsaCollection ativoBolsaCollection = new AtivoBolsaCollection();
            ativoBolsaCollection.Query.Where(ativoBolsaCollection.Query.IdEstrategia.IsNotNull());
            ativoBolsaCollection.Query.Load();

            if (ativoBolsaCollection.Count > 0 && ativoBolsaCollection[0].IdEstrategia.HasValue)
            {
                ativoBolsa.IdEstrategia = ativoBolsaCollection[0].IdEstrategia.Value;
            }
        }

        ativoBolsa.TipoPapel = Convert.ToByte(dropTipoPapel.SelectedItem.Value);

        if (textCodigoCusip.Text != "")
            ativoBolsa.CodigoCusip = textCodigoCusip.Text;

        int codicoCda = 0;
        if (int.TryParse(textCodigoCDA.Text.ToString(), out codicoCda))
            ativoBolsa.CodigoCDA = codicoCda;
        else
            ativoBolsa.CodigoCDA = null;

        int codigoBDS = 0;
        if (int.TryParse(textCodigoBDS.Text.ToString(), out codigoBDS))
            ativoBolsa.CodigoBDS = codigoBDS;
        else
            ativoBolsa.CodigoBDS = null;

        if (dropInvestimentoColetivoCvm.SelectedIndex > -1)
        {
            ativoBolsa.InvestimentoColetivoCvm = dropInvestimentoColetivoCvm.SelectedItem.Value.ToString();
        }

        ativoBolsa.Participacao = chkParticipacao.Checked ? "S" : "N";

        ativoBolsa.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de AtivoBolsa - Operacao: Insert AtivoBolsa: " + ativoBolsa.CdAtivoBolsa + UtilitarioWeb.ToString(ativoBolsa),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        if (textCdAtivoBolsaObjeto.Text.Trim().ToUpper() != "")
        {
            FatorCotacaoBolsa fatorCotacaoBolsa = new FatorCotacaoBolsa();
            if (fatorCotacaoBolsa.BuscaFatorCotacaoBolsa(textCdAtivoBolsaObjeto.Text.Trim().ToUpper(), DateTime.Now))
            {
                FatorCotacaoBolsa fatorCotacaoBolsaNovo = new FatorCotacaoBolsa();
                fatorCotacaoBolsaNovo.DataReferencia = new DateTime(2000, 01, 01);
                fatorCotacaoBolsaNovo.CdAtivoBolsa = textCdAtivoBolsa.Text.Trim().ToUpper();
                fatorCotacaoBolsaNovo.Fator = fatorCotacaoBolsa.Fator.Value;
                fatorCotacaoBolsaNovo.Save();
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        ASPxTextBox textCdAtivoBolsa = gridCadastro.FindEditFormTemplateControl("textCdAtivoBolsa") as ASPxTextBox;
        ASPxTextBox textEspecificacao = gridCadastro.FindEditFormTemplateControl("textEspecificacao") as ASPxTextBox;
        TextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as TextBox;
        ASPxComboBox dropTipoMercado = gridCadastro.FindEditFormTemplateControl("dropTipoMercado") as ASPxComboBox;
        ASPxSpinEdit textPUExercicio = gridCadastro.FindEditFormTemplateControl("textPUExercicio") as ASPxSpinEdit;
        ASPxDateEdit textDataVencimento = gridCadastro.FindEditFormTemplateControl("textDataVencimento") as ASPxDateEdit;
        TextBox textCdAtivoBolsaObjeto = gridCadastro.FindEditFormTemplateControl("textCdAtivoBolsaObjeto") as TextBox;
        TextBox textCodigoIsin = gridCadastro.FindEditFormTemplateControl("textCodigoIsin") as TextBox;
        ASPxComboBox dropMoeda = gridCadastro.FindEditFormTemplateControl("dropMoeda") as ASPxComboBox;
        ASPxComboBox dropEstrategia = gridCadastro.FindEditFormTemplateControl("dropEstrategia") as ASPxComboBox;
        TextBox textNumeroSerie = gridCadastro.FindEditFormTemplateControl("textNumeroSerie") as TextBox;
        ASPxComboBox dropTipoPapel = gridCadastro.FindEditFormTemplateControl("dropTipoPapel") as ASPxComboBox;
        TextBox textCodigoCusip = gridCadastro.FindEditFormTemplateControl("textCodigoCusip") as TextBox;
        TextBox hiddenIdEmissor = gridCadastro.FindEditFormTemplateControl("hiddenIdEmissor") as TextBox;
        ASPxComboBox dropLocalNegociacao = gridCadastro.FindEditFormTemplateControl("dropLocalNegociacao") as ASPxComboBox;
        ASPxComboBox dropLocalCustodia = gridCadastro.FindEditFormTemplateControl("dropLocalCustodia") as ASPxComboBox;
        ASPxComboBox dropClearing = gridCadastro.FindEditFormTemplateControl("dropClearing") as ASPxComboBox;
        ASPxSpinEdit textCodigoCDA = gridCadastro.FindEditFormTemplateControl("textCodigoCDA") as ASPxSpinEdit;
        ASPxSpinEdit textCodigoBDS = gridCadastro.FindEditFormTemplateControl("textCodigoBDS") as ASPxSpinEdit;
        ASPxComboBox dropInvestimentoColetivoCvm = gridCadastro.FindEditFormTemplateControl("dropInvestimentoColetivoCvm") as ASPxComboBox;
        ASPxCheckBox chkParticipacao = gridCadastro.FindEditFormTemplateControl("chkParticipacao") as ASPxCheckBox;

        AtivoBolsa ativoBolsa = new AtivoBolsa();

        string cdAtivoBolsa = textCdAtivoBolsa.Text.ToString();
        if (ativoBolsa.LoadByPrimaryKey(cdAtivoBolsa))
        {
            ativoBolsa.Especificacao = textEspecificacao.Text.ToString().Trim().ToUpper();
            ativoBolsa.Descricao = textDescricao.Text.ToString().Trim();
            ativoBolsa.IdLocalCustodia = Convert.ToInt16(dropLocalCustodia.SelectedItem.Value);

            if (dropClearing.SelectedIndex > -1)
            {
                ativoBolsa.IdClearing = Convert.ToInt16(dropClearing.SelectedItem.Value);
            }
            else
            {
                ativoBolsa.IdClearing = null;
            }

            if (dropLocalNegociacao.SelectedIndex > -1)
            {
                ativoBolsa.IdLocalNegociacao = Convert.ToInt16(dropLocalNegociacao.SelectedItem.Value);
            }
            else
            {
                ativoBolsa.IdLocalNegociacao = null;
            }

            if (!String.IsNullOrEmpty(textPUExercicio.Text.Trim()))
            {
                ativoBolsa.PUExercicio = Convert.ToDecimal(textPUExercicio.Text);
            }

            if (textDataVencimento.Text != "" || Convert.ToString(dropTipoMercado.SelectedItem.Value) != ativoBolsa.TipoMercado)
            {
                if (!ativoBolsa.DataVencimento.HasValue || Convert.ToDateTime(textDataVencimento.Text) != ativoBolsa.DataVencimento.Value ||
                    Convert.ToString(dropTipoMercado.SelectedItem.Value) != ativoBolsa.TipoMercado)
                {
                    #region Posicao Bolsa
                    PosicaoBolsaCollection posicaoBolsaCollection = new PosicaoBolsaCollection();
                    posicaoBolsaCollection.Query.Where(posicaoBolsaCollection.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));

                    posicaoBolsaCollection.Query.Load();

                    foreach (PosicaoBolsa posicaoBolsa in posicaoBolsaCollection)
                    {
                        if (textDataVencimento.Text != "")
                        {
                            posicaoBolsa.DataVencimento = Convert.ToDateTime(textDataVencimento.Text);
                        }
                        else
                        {
                            posicaoBolsa.DataVencimento = null;
                        }

                        posicaoBolsa.TipoMercado = Convert.ToString(dropTipoMercado.SelectedItem.Value);
                    }
                    posicaoBolsaCollection.Save();
                    #endregion

                    #region PosicaoBolsaHistorico
                    PosicaoBolsaHistoricoCollection posicaoBolsaHistoricoCollection = new PosicaoBolsaHistoricoCollection();
                    posicaoBolsaHistoricoCollection.Query.Where(posicaoBolsaHistoricoCollection.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));

                    posicaoBolsaHistoricoCollection.Query.Load();

                    foreach (PosicaoBolsaHistorico posicaoBolsaHistorico in posicaoBolsaHistoricoCollection)
                    {
                        if (textDataVencimento.Text != "")
                        {
                            posicaoBolsaHistorico.DataVencimento = Convert.ToDateTime(textDataVencimento.Text);
                        }
                        else
                        {
                            posicaoBolsaHistorico.DataVencimento = null;
                        }

                        posicaoBolsaHistorico.TipoMercado = Convert.ToString(dropTipoMercado.SelectedItem.Value);
                    }
                    posicaoBolsaHistoricoCollection.Save();
                    #endregion

                    #region PosicaoBolsaAbertura
                    PosicaoBolsaAberturaCollection posicaoBolsaAberturaCollection = new PosicaoBolsaAberturaCollection();
                    posicaoBolsaAberturaCollection.Query.Where(posicaoBolsaAberturaCollection.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));

                    posicaoBolsaAberturaCollection.Query.Load();

                    foreach (PosicaoBolsaAbertura posicaoBolsaAbertura in posicaoBolsaAberturaCollection)
                    {
                        if (textDataVencimento.Text != "")
                        {
                            posicaoBolsaAbertura.DataVencimento = Convert.ToDateTime(textDataVencimento.Text);
                        }
                        else
                        {
                            posicaoBolsaAbertura.DataVencimento = null;
                        }

                        posicaoBolsaAbertura.TipoMercado = Convert.ToString(dropTipoMercado.SelectedItem.Value);
                    }
                    posicaoBolsaAberturaCollection.Save();

                    #endregion

                    if (Convert.ToString(dropTipoMercado.SelectedItem.Value) != ativoBolsa.TipoMercado)
                    {
                        #region OrdemBolsa
                        OrdemBolsaCollection ordemBolsaCollection = new OrdemBolsaCollection();
                        ordemBolsaCollection.Query.Where(ordemBolsaCollection.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));

                        ordemBolsaCollection.Query.Load();

                        foreach (OrdemBolsa ordemBolsa in ordemBolsaCollection)
                        {
                            ordemBolsa.TipoMercado = Convert.ToString(dropTipoMercado.SelectedItem.Value);
                        }
                        ordemBolsaCollection.Save();
                        #endregion

                        #region OperacaoBolsa
                        OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
                        operacaoBolsaCollection.Query.Where(operacaoBolsaCollection.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));

                        operacaoBolsaCollection.Query.Load();

                        foreach (OperacaoBolsa operacaoBolsa in operacaoBolsaCollection)
                        {
                            operacaoBolsa.TipoMercado = Convert.ToString(dropTipoMercado.SelectedItem.Value);
                        }
                        operacaoBolsaCollection.Save();
                        #endregion
                    }
                }
            }

            ativoBolsa.TipoMercado = Convert.ToString(dropTipoMercado.SelectedItem.Value);

            if (!String.IsNullOrEmpty(textDataVencimento.Text.Trim()))
            {
                ativoBolsa.DataVencimento = Convert.ToDateTime(textDataVencimento.Text);
            }

            ativoBolsa.CdAtivoBolsaObjeto = textCdAtivoBolsaObjeto.Text.Trim();
            ativoBolsa.CodigoIsin = textCodigoIsin.Text.Trim();
            ativoBolsa.IdMoeda = Convert.ToInt16(dropMoeda.SelectedItem.Value);
            //
            ativoBolsa.IdEmissor = Convert.ToInt32(hiddenIdEmissor.Text);

            // HACK: ativoBolsa.OpcaoIndice - Quando for usado, deve ser ajustado!
            ativoBolsa.OpcaoIndice = 0;

            ativoBolsa.NumeroSerie = 0;
            if (textNumeroSerie.Text != "")
            {
                ativoBolsa.NumeroSerie = Convert.ToInt32(textNumeroSerie.Text);
            }

            if (dropEstrategia.SelectedIndex != -1)
            {
                ativoBolsa.IdEstrategia = Convert.ToInt32(dropEstrategia.SelectedItem.Value);
            }
            else
            {
                ativoBolsa.IdEstrategia = null;
            }

            ativoBolsa.TipoPapel = Convert.ToByte(dropTipoPapel.SelectedItem.Value);

            if (textCodigoCusip.Text != "")
                ativoBolsa.CodigoCusip = textCodigoCusip.Text;

            int codicoCda = 0;
            if (int.TryParse(textCodigoCDA.Text.ToString(), out codicoCda))
                ativoBolsa.CodigoCDA = codicoCda;
            else
                ativoBolsa.CodigoCDA = null;

            int codigoBDS = 0;
            if (int.TryParse(textCodigoBDS.Text.ToString(), out codigoBDS))
                ativoBolsa.CodigoBDS = codigoBDS;
            else
                ativoBolsa.CodigoBDS = null;

            if (dropInvestimentoColetivoCvm.SelectedIndex != -1)
            {
                ativoBolsa.InvestimentoColetivoCvm = dropInvestimentoColetivoCvm.SelectedItem.Value.ToString();
            }
            ativoBolsa.Participacao = chkParticipacao.Checked ? "S" : "N";

            ativoBolsa.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de AtivoBolsa - Operacao: Update AtivoBolsa: " + cdAtivoBolsa + UtilitarioWeb.ToString(ativoBolsa),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValues1 = gridCadastro.GetSelectedFieldValues(AtivoBolsaMetadata.ColumnNames.CdAtivoBolsa);
            for (int i = 0; i < keyValues1.Count; i++)
            {
                string cdAtivoBolsa = Convert.ToString(keyValues1[i]);

                AtivoBolsa ativoBolsa = new AtivoBolsa();
                if (ativoBolsa.LoadByPrimaryKey(cdAtivoBolsa))
                {
                    bool achou = false;
                    PosicaoBolsaAberturaCollection posicaoBolsaAberturaCollection = new PosicaoBolsaAberturaCollection();
                    posicaoBolsaAberturaCollection.Query.Where(posicaoBolsaAberturaCollection.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));
                    if (posicaoBolsaAberturaCollection.Query.Load())
                    {
                        achou = true;
                    }
                    else
                    {
                        OperacaoBolsaCollection operacaoBolsaCollection = new OperacaoBolsaCollection();
                        operacaoBolsaCollection.Query.Where(operacaoBolsaCollection.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));
                        if (operacaoBolsaCollection.Query.Load())
                        {
                            achou = true;
                        }
                    }

                    if (achou)
                    {
                        throw new Exception("Ativo " + cdAtivoBolsa + " não pode ser excluído por ter posições e/ou movimentos relacionadas.");
                    }

                    AtivoBolsa ativoBolsaClone = (AtivoBolsa)Utilitario.Clone(ativoBolsa);
                    //
                    ativoBolsa.MarkAsDeleted();
                    ativoBolsa.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de AtivoBolsa - Operacao: Delete AtivoBolsa: " + cdAtivoBolsa + UtilitarioWeb.ToString(ativoBolsaClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e)
    {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing)
        {
            ASPxTextBox textCdAtivoBolsa = gridCadastro.FindEditFormTemplateControl("textCdAtivoBolsa") as ASPxTextBox;
            e.Properties["cpTextCdAtivoBolsa"] = textCdAtivoBolsa.ClientID;

            ASPxTextBox textEspecificacao = gridCadastro.FindEditFormTemplateControl("textEspecificacao") as ASPxTextBox;
            e.Properties["cpTextEspecificacao"] = textEspecificacao.ClientID;

            TextBox hiddenIdEmissor = gridCadastro.FindEditFormTemplateControl("hiddenIdEmissor") as TextBox;
            e.Properties["cpHiddenIdEmissor"] = hiddenIdEmissor.ClientID;

        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e)
    {
        this.FocaCampoGrid("textCdAtivoBolsa", "textEspecificacao");
        base.gridCadastro_PreRender(sender, e);
    }
}