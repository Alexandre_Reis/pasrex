﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

using Financial.Common;
using Financial.Util;
using Financial.Investidor;
using Financial.ContaCorrente;
using Financial.Security;
using Financial.Fundo;
using Financial.Bolsa;
using Financial.Web.Common;

using DevExpress.Web;

using Financial.ContaCorrente.Enums;
using Financial.Common.Enums;
using Financial.Investidor.Enums;
using Financial.Bolsa.Enums;
using System.Threading;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_BloqueioBolsa : CadastroBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasPopupCliente = true;
        this.HasPopupAtivoBolsa = true;
        this.HasFiltro = true;
        base.Page_Load(sender, e);
        
        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
                  new List<string>(new string[] { BloqueioBolsaMetadata.ColumnNames.TipoOperacao }));
    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSBloqueioBolsa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        Usuario usuario = new Usuario();
        if (usuario.BuscaUsuario(Context.User.Identity.Name)) {
            int idUsuario = usuario.IdUsuario.Value;

            BloqueioBolsaQuery bloqueioBolsaQuery = new BloqueioBolsaQuery("T");
            ClienteQuery clienteQuery = new ClienteQuery("C");
            PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");

            bloqueioBolsaQuery.Select(bloqueioBolsaQuery, clienteQuery.Apelido.As("Apelido"));
            bloqueioBolsaQuery.InnerJoin(clienteQuery).On(bloqueioBolsaQuery.IdCliente == clienteQuery.IdCliente);
            bloqueioBolsaQuery.InnerJoin(permissaoClienteQuery).On(bloqueioBolsaQuery.IdCliente == permissaoClienteQuery.IdCliente);
            bloqueioBolsaQuery.Where(permissaoClienteQuery.IdUsuario.Equal(idUsuario));

            if (!String.IsNullOrEmpty(btnEditCodigoClienteFiltro.Text)) {
                bloqueioBolsaQuery.Where(bloqueioBolsaQuery.IdCliente.Equal(Convert.ToInt32(btnEditCodigoClienteFiltro.Text)));
            }

            if (!String.IsNullOrEmpty(textDataInicio.Text)) {
                bloqueioBolsaQuery.Where(bloqueioBolsaQuery.DataOperacao.GreaterThanOrEqual(textDataInicio.Text));
            }

            if (!String.IsNullOrEmpty(textDataFim.Text)) {
                bloqueioBolsaQuery.Where(bloqueioBolsaQuery.DataOperacao.LessThanOrEqual(textDataFim.Text));
            }

            bloqueioBolsaQuery.OrderBy(bloqueioBolsaQuery.DataOperacao.Descending);

            BloqueioBolsaCollection coll = new BloqueioBolsaCollection();
            coll.Load(bloqueioBolsaQuery);

            // Assign the esDataSourcSelectEvenArgs Collection property
            e.Collection = coll;
        }
        else {
            e.Collection = new BloqueioBolsaCollection();
        }

    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ClienteCollection coll = new ClienteCollection();
        coll.BuscaClientesComAcesso(Context.User.Identity.Name);
        
        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSAgenteMercado_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        AgenteMercadoCollection coll = new AgenteMercadoCollection();
        coll.Query.Where(coll.Query.FuncaoCustodiante == 'S');
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSAtivoBolsa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        AtivoBolsaCollection coll = new AtivoBolsaCollection();
        coll.Query.Where(coll.Query.TipoMercado.Equal(TipoMercadoBolsa.MercadoVista));
        coll.Query.OrderBy(coll.Query.CdAtivoBolsa.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSTipoCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        TipoCarteiraBolsaCollection coll = new TipoCarteiraBolsaCollection();
        //
        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();
        //        
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void panelEdicao_Load(object sender, EventArgs e) {
        UtilitarioGrid.SetaCorCombosPopup(sender as Control);

        bool travaPainel = false;
        if (gridCadastro.IsEditing && !gridCadastro.IsNewRowEditing && Convert.ToString(Session["FormLoad"]) != "S") {
            Session["FormLoad"] = "S";
            Label labelEdicao = gridCadastro.FindEditFormTemplateControl("labelEdicao") as Label;

            int idCliente = Convert.ToInt32(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "IdCliente"));
            DateTime data = Convert.ToDateTime(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "DataOperacao"));

            Cliente cliente = new Cliente();
            if (cliente.LoadByPrimaryKey(idCliente)) {
                DateTime dataDia = cliente.DataDia.Value;
                byte status = cliente.Status.Value;
                if (status == (byte)StatusCliente.Divulgado) {
                    travaPainel = true;
                    labelEdicao.Text = "Cliente está fechado. Dados não podem ser alterados.";
                }
                else if (DateTime.Compare(dataDia, data) > 0) {
                    travaPainel = true;
                    labelEdicao.Text = "Data informada anterior à data do cliente. Dados não podem ser alterados.";
                }
            }
        }

        if (travaPainel) {
            LinkButton btnOK = gridCadastro.FindEditFormTemplateControl("btnOK") as LinkButton;
            btnOK.Visible = false;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";

        if (!gridCadastro.IsEditing) //Testa deleção
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(BloqueioBolsaMetadata.ColumnNames.IdCliente);
            List<object> keyData = gridCadastro.GetSelectedFieldValues(BloqueioBolsaMetadata.ColumnNames.DataOperacao);
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idCliente = Convert.ToInt32(keyValuesId[i]);
                DateTime data = Convert.ToDateTime(keyData[i]);

                Cliente cliente = new Cliente();
                if (cliente.LoadByPrimaryKey(idCliente)) {
                    DateTime dataDia = cliente.DataDia.Value;
                    byte status = cliente.Status.Value;
                    string clienteDescricao = cliente.Apelido + " (" + cliente.IdCliente.ToString() + ")";
                    if (status == (byte)StatusCliente.Divulgado) {
                        e.Result = "Cliente " + clienteDescricao + " está fechado! Deleção cancelada.";
                        return;
                    }
                    else if (DateTime.Compare(dataDia, data) > 0) {
                        e.Result = "Lançamento com data " + data.ToString().Substring(0, 10) + " anterior à data do cliente " + clienteDescricao + "! Deleção cancelada.";
                        return;
                    }
                }
            }
        }
        else {
            ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
            ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
            ASPxComboBox dropTipoOperacao = gridCadastro.FindEditFormTemplateControl("dropTipoOperacao") as ASPxComboBox;
            ASPxButtonEdit btnEditAtivoBolsa = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa") as ASPxButtonEdit;
            ASPxComboBox dropAgente = gridCadastro.FindEditFormTemplateControl("dropAgente") as ASPxComboBox;
            ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;

            #region Campos obrigatórios
            List<Control> controles = new List<Control>();
            controles.Add(btnEditCodigoCliente);
            controles.Add(btnEditAtivoBolsa);
            controles.Add(dropTipoOperacao);
            controles.Add(dropAgente);
            controles.Add(textQuantidade);

            if (base.TestaObrigatorio(controles) != "") {
                e.Result = "Campos com * são obrigatórios!";
                return;
            }
            #endregion

            int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
            DateTime data = Convert.ToDateTime(textData.Text);

            if (!Calendario.IsDiaUtil(data, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil)) {
                e.Result = "Data não é dia útil.";
                return;
            }

            Cliente cliente = new Cliente();
            if (cliente.LoadByPrimaryKey(idCliente)) {
                DateTime dataDia = cliente.DataDia.Value;
                byte status = cliente.Status.Value;
                if (status == (byte)StatusCliente.Divulgado) {
                    e.Result = "Cliente está fechado! Operação não pode ser realizada.";
                    return;
                }
                else if (DateTime.Compare(dataDia, Convert.ToDateTime(textData.Text)) > 0) {
                    e.Result = "Data informada anterior à data do cliente! Operação não pode ser realizada.";
                    return;
                }
            }

            PosicaoBolsa posicaoBolsa = new PosicaoBolsa();

            if (Convert.ToByte(dropTipoOperacao.SelectedItem.Value) == (byte)TipoOperacaoBloqueio.Bloqueio) {
                decimal quantidade = posicaoBolsa.RetornaQuantidadeAtivo(idCliente,
                                        Convert.ToInt32((dropAgente.SelectedItem.Value)), btnEditAtivoBolsa.Text);

                if (Convert.ToDecimal(textQuantidade.Text) > quantidade) {
                    e.Result = "Quantidade a bloquear é maior do que a quantidade em posição do ativo.";
                    return;
                }
            }
            else if (Convert.ToByte(dropTipoOperacao.SelectedItem.Value) == (byte)TipoOperacaoBloqueio.Desbloqueio) {
                decimal quantidadeBloqueada = posicaoBolsa.RetornaQuantidadeBloqueadaAtivo(idCliente,
                                              Convert.ToInt32((dropAgente.SelectedItem.Value)), btnEditAtivoBolsa.Text);

                if (quantidadeBloqueada < Convert.ToDecimal(textQuantidade.Text)) {
                    e.Result = "Quantidade a desbloquear é maior que a quantidade bloqueada do ativo.";
                    return;
                }
            }
        }
    }
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        DateTime dataDia;
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") {
                if (cliente.Status.Value != (byte)StatusCliente.Divulgado) {
                    if (cliente.IsAtivo) {
                        PermissaoCliente permissaoCliente = new PermissaoCliente();
                        if (permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name))
                        {
                            nome = cliente.str.Apelido;
                            dataDia = cliente.DataDia.Value;

                            nome = nome + "|" + dataDia.ToShortDateString() + "|" + Thread.CurrentThread.CurrentCulture.Name;
                        }
                        else {
                            nome = "no_access";
                        }
                    }
                    else {
                        nome = "no_active";
                    }
                }
                else
                {
                    nome = cliente.str.Apelido + "|status_closed";
                }
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string CdAtivoBolsa = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            AtivoBolsa ativoBolsa = new AtivoBolsa();
            ativoBolsa.LoadByPrimaryKey(Convert.ToString(e.Parameter));

            if (ativoBolsa.str.CdAtivoBolsa != "") {
                CdAtivoBolsa = ativoBolsa.str.CdAtivoBolsa;
            }
        }
        e.Result = CdAtivoBolsa;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        int idTransferencia = (int)e.Keys[0];
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxButtonEdit btnEditAtivoBolsa = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa") as ASPxButtonEdit;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxComboBox dropAgente = gridCadastro.FindEditFormTemplateControl("dropAgente") as ASPxComboBox;
        ASPxComboBox dropTipoOperacao = gridCadastro.FindEditFormTemplateControl("dropTipoOperacao") as ASPxComboBox;
        ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
        TextBox textObservacao = gridCadastro.FindEditFormTemplateControl("textObservacao") as TextBox;
        ASPxComboBox dropTipoCarteira = gridCadastro.FindEditFormTemplateControl("dropTipoCarteira") as ASPxComboBox;

        BloqueioBolsa bloqueioBolsa = new BloqueioBolsa();
        if (bloqueioBolsa.LoadByPrimaryKey(idTransferencia)) {
            bloqueioBolsa.IdCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
            bloqueioBolsa.CdAtivoBolsa = Convert.ToString(btnEditAtivoBolsa.Text).ToUpper();
            bloqueioBolsa.DataOperacao = Convert.ToDateTime(textData.Text);
            bloqueioBolsa.IdAgente = Convert.ToInt32(dropAgente.SelectedItem.Value);
            bloqueioBolsa.TipoOperacao = Convert.ToByte(dropTipoOperacao.SelectedItem.Value);
            bloqueioBolsa.Quantidade = Convert.ToDecimal(textQuantidade.Text);

            bloqueioBolsa.TipoCarteiraBloqueada = null;
            if (dropTipoCarteira.SelectedIndex != -1)
            {
                bloqueioBolsa.TipoCarteiraBloqueada = Convert.ToInt32(dropTipoCarteira.SelectedItem.Value);
            }

            if (textObservacao.Text.Trim() != "")
            {
                bloqueioBolsa.Observacao = textObservacao.Text;
            }

            bloqueioBolsa.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de BloqueioBolsa - Operacao: Update BloqueioBolsa: " + idTransferencia + UtilitarioWeb.ToString(bloqueioBolsa),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="idCliente"></param>
    /// <param name="data"></param>
    /// <param name="tipoOperacao"></param>
    /// <param name="cdAtivoBolsa"></param>
    private void DeletaOperacoes(int idCliente, DateTime data, byte tipoOperacao, string cdAtivoBolsa, int idAgente) {
        BloqueioBolsaCollection bloqueioBolsaCollection = new BloqueioBolsaCollection();
        bloqueioBolsaCollection.Query.Where(bloqueioBolsaCollection.Query.IdCliente.Equal(idCliente),
                                            bloqueioBolsaCollection.Query.DataOperacao.Equal(data),
                                            bloqueioBolsaCollection.Query.TipoOperacao.Equal(tipoOperacao),
                                            bloqueioBolsaCollection.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                            bloqueioBolsaCollection.Query.IdAgente.Equal(idAgente));
        bloqueioBolsaCollection.Query.Load();
        bloqueioBolsaCollection.MarkAllAsDeleted();
        bloqueioBolsaCollection.Save();
    }
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxButtonEdit btnEditAtivoBolsa = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa") as ASPxButtonEdit;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxComboBox dropAgente = gridCadastro.FindEditFormTemplateControl("dropAgente") as ASPxComboBox;
        ASPxComboBox dropTipoOperacao = gridCadastro.FindEditFormTemplateControl("dropTipoOperacao") as ASPxComboBox;
        ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
        TextBox textObservacao = gridCadastro.FindEditFormTemplateControl("textObservacao") as TextBox;
        ASPxComboBox dropTipoCarteira = gridCadastro.FindEditFormTemplateControl("dropTipoCarteira") as ASPxComboBox;

        //Deleta todas operações do tipo em questão na data
        DeletaOperacoes(Convert.ToInt32(btnEditCodigoCliente.Text),
                        Convert.ToDateTime(textData.Text),
                        Convert.ToByte(dropTipoOperacao.SelectedItem.Value),
                        btnEditAtivoBolsa.Text,
                        Convert.ToInt32(dropAgente.SelectedItem.Value));
        //

        BloqueioBolsa bloqueioBolsa = new BloqueioBolsa();
        bloqueioBolsa.IdCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
        bloqueioBolsa.CdAtivoBolsa = Convert.ToString(btnEditAtivoBolsa.Text).ToUpper();
        bloqueioBolsa.DataOperacao = Convert.ToDateTime(textData.Text);
        bloqueioBolsa.IdAgente = Convert.ToInt32(dropAgente.SelectedItem.Value);
        bloqueioBolsa.TipoOperacao = Convert.ToByte(dropTipoOperacao.SelectedItem.Value);
        bloqueioBolsa.Quantidade = Convert.ToDecimal(textQuantidade.Text);

        if (textObservacao.Text.Trim() != "")
        {
            bloqueioBolsa.Observacao = textObservacao.Text;
        }

        if (dropTipoCarteira.SelectedIndex != -1)
        {
            bloqueioBolsa.TipoCarteiraBloqueada = Convert.ToInt32(dropTipoCarteira.SelectedItem.Value);
        }

        bloqueioBolsa.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de BloqueioBolsa - Operacao: Insert BloqueioBolsa: " + bloqueioBolsa.IdBloqueio + UtilitarioWeb.ToString(bloqueioBolsa),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(BloqueioBolsaMetadata.ColumnNames.IdBloqueio);
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idBloqueio = Convert.ToInt32(keyValuesId[i]);

                BloqueioBolsa bloqueioBolsa = new BloqueioBolsa();
                if (bloqueioBolsa.LoadByPrimaryKey(idBloqueio)) {

                    BloqueioBolsa bloqueioBolsaClone = (BloqueioBolsa)Utilitario.Clone(bloqueioBolsa);
                    //
                    bloqueioBolsa.MarkAsDeleted();
                    bloqueioBolsa.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de BloqueioBolsa - Operacao: Delete BloqueioBolsa: " + idBloqueio + UtilitarioWeb.ToString(bloqueioBolsaClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing) {
            ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
            e.Properties["cpBtnEditCodigoCliente"] = btnEditCodigoCliente.ClientID;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("btnEditCodigoCliente");
        base.gridCadastro_PreRender(sender, e);
                
        ASPxDateEdit textDataInicio = popupFiltro.FindControl("textDataInicio") as ASPxDateEdit;
        ASPxDateEdit textDataFim = popupFiltro.FindControl("textDataFim") as ASPxDateEdit;
        ASPxSpinEdit btnEditCodigoClienteFiltro = popupFiltro.FindControl("btnEditCodigoClienteFiltro") as ASPxSpinEdit;

        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        Label labelFiltroHeader = gridCadastro.FindStatusBarTemplateControl("labelFiltroHeader") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");
        if (btnEditCodigoClienteFiltro != null && btnEditCodigoClienteFiltro.Text != "") {
            texto.Append(" Cliente: ").Append(btnEditCodigoClienteFiltro.Text);
        }
        if (textDataInicio.Text != "") {
            if (btnEditCodigoClienteFiltro != null && btnEditCodigoClienteFiltro.Text != "") {
                texto.Append(" |");
            }
            texto.Append(" Data Início >= ").Append(textDataInicio.Text.Substring(0, 10));
        }
        if (textDataFim.Text != "") {
            if (textDataInicio.Text != "" || btnEditCodigoClienteFiltro.Text != "") {
                texto.Append(" |");
            }
            texto.Append(" Data Fim <= ").Append(textDataFim.Text.Substring(0, 10));
        }

        labelFiltro.Text = texto.ToString();
    }
}