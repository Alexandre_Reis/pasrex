﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using DevExpress.Web;
using DevExpress.Web.Data;

using Financial.Common;
using Financial.Util;
using Financial.Util.Enums;
using Financial.Investidor;
using Financial.ContaCorrente;
using Financial.Security;
using Financial.Fundo;
using Financial.InvestidorCotista;
using Financial.Bolsa;
using Financial.Web.Common;
using Financial.WebConfigConfiguration;

using Financial.ContaCorrente.Enums;
using Financial.InvestidorCotista.Enums;
using Financial.Investidor.Enums;
using Financial.Common.Enums;
using Financial.Bolsa.Enums;

using EntitySpaces.Interfaces;
using System.Threading;
using Financial.Web.Util;
using Financial.Security.Enums;

using Financial.Investidor;

public partial class CadastrosBasicos_OperacaoBolsa : CadastroBasePage {

    //protected void Page_PreRender(object sender, EventArgs e) {
    //    HtmlMeta metatag = new HtmlMeta();
    //    metatag.Attributes.Add("http-equiv", "X-UA-Compatible");
    //    metatag.Attributes.Add("content", "ID=IE=EmulateIE7");
    //    Page.Header.Controls.AddAt(0, metatag);

    //}

    new protected void Page_Load(object sender, EventArgs e) {
        this.HasFiltro = true;
        this.HasPopupCliente = true;
        this.HasPopupAtivoBolsa = true;
        this.HasPanelFieldsLoading = true;
        base.Page_Load(sender, e);

        this.gridCadastro.ClientSideEvents.RowDblClick = "function(s, e) {selectedIndex = e.visibleIndex; gridCadastro.GetRowValues(e.visibleIndex, 'Origem', OpenPopup); }";

        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
          new List<string>(new string[] { OperacaoBolsaMetadata.ColumnNames.TipoOperacao }));

        if (!Page.IsPostBack)
        {
            List<Hashtable> list = Combos.carregaComboTipoMercadoBolsa(ConfiguracaoCombo.OpcaoBranco);

            for (int i = 0; i < list.Count; i++)
            {
                // Hashtable
                Hashtable hashtable = (Hashtable)list[i];

                foreach (DictionaryEntry item in hashtable)
                {
                    ListEditItem listItem = new ListEditItem();
                    //
                    listItem.Text = item.Value.ToString();
                    listItem.Value = item.Key.ToString();
                    //
                    dropTipoMercado.Items.Add(listItem);
                }
            }
        }
    }

    protected void dropTipoOperacao_Load(object sender, EventArgs e)
    {
        ASPxComboBox dropTipoOperacao = gridCadastro.FindEditFormTemplateControl("dropTipoOperacao") as ASPxComboBox;
        dropTipoOperacao.Items.Add("Compra", TipoOperacaoBolsa.Compra);
        dropTipoOperacao.Items.Add("Venda", TipoOperacaoBolsa.Venda);
        dropTipoOperacao.Items.Add("Compra DT", TipoOperacaoBolsa.CompraDaytrade);
        dropTipoOperacao.Items.Add("Venda DT", TipoOperacaoBolsa.VendaDaytrade);
        dropTipoOperacao.Items.Add("Depósito", TipoOperacaoBolsa.Deposito);
        dropTipoOperacao.Items.Add("Retirada", TipoOperacaoBolsa.Retirada);
        dropTipoOperacao.Items.Add("Ingresso em Ativos com Impacto na Quantidade", TipoOperacaoBolsa.IngressoAtivoImpactoQtde);
        dropTipoOperacao.Items.Add("Ingresso em Ativos com Impacto na Cota", TipoOperacaoBolsa.IngressoAtivoImpactoCota);
        dropTipoOperacao.Items.Add("Retirada em Ativos com Impacto na Quantidade", TipoOperacaoBolsa.RetiradaAtivoImpactoQtde);
        dropTipoOperacao.Items.Add("Retirada em Ativos com Impacto na Cota", TipoOperacaoBolsa.RetiradaAtivoImpactoCota);

    }

    //testa para saber se é deposito ou retirada unicas operações onde é possivel editar a data de operação
    protected void dropTipoOperacao_OnDataBound(object sender, EventArgs e)
    {
        ASPxComboBox dropTipoOperacao = sender as ASPxComboBox;
        ASPxDateEdit textDataOperacao = gridCadastro.FindEditFormTemplateControl("textDataOperacao") as ASPxDateEdit;

        //primeiro testa para saber se é valido e possui valor depois testa para saber se é deposito ou retirada unicas operações onde é possivel editar a data de operação.
        if (dropTipoOperacao != null && dropTipoOperacao.Value != null)
        {
            if (dropTipoOperacao.Value.ToString() == "DE" || dropTipoOperacao.Value.ToString() == "RE")
            {
                textDataOperacao.ClientEnabled = true;
            }
            else
            {
                textDataOperacao.ClientEnabled = false;
            }
        }
    }

    protected void callBackDiasLiq_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
        {
            ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
            ASPxTextBox hiddenDataLiquidacao = gridCadastro.FindEditFormTemplateControl("hiddenDataLiquidacao") as ASPxTextBox;

            if (textData != null && hiddenDataLiquidacao != null && !string.IsNullOrEmpty(textData.Text) && !string.IsNullOrEmpty(hiddenDataLiquidacao.Text))
            {
                DateTime data = Convert.ToDateTime(textData.Text);
                DateTime dataLiquidacao = Convert.ToDateTime(hiddenDataLiquidacao.Text);
                int diasLiquidacao = Calendario.NumeroDias(data, dataLiquidacao, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);

                e.Result = diasLiquidacao.ToString();
            }
        }
    }

    protected void callBackQtdeDisponivel_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        ASPxDateEdit textDataOperacao = gridCadastro.FindEditFormTemplateControl("textDataOperacao") as ASPxDateEdit;
        ASPxButtonEdit btnEditAtivoBolsa = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa") as ASPxButtonEdit;
        ASPxComboBox dropAgenteCorretora = gridCadastro.FindEditFormTemplateControl("dropAgenteCorretora") as ASPxComboBox;
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;

        if (!string.IsNullOrEmpty(textDataOperacao.Text) &&
            !string.IsNullOrEmpty(btnEditAtivoBolsa.Text) && 
            !string.IsNullOrEmpty(btnEditCodigoCliente.Text) && 
            dropAgenteCorretora.SelectedIndex != -1)
        {
            DateTime dataOperacao = Convert.ToDateTime(textDataOperacao.Text);
            string cdAtivoBolsa = btnEditAtivoBolsa.Text;
            int? idAgente = null;
            int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);

            ClienteBolsa cliente = new ClienteBolsa();
            cliente.LoadByPrimaryKey(idCliente);

            //Se existe custodiante central, não usa o da operação para validar a quantidade 
            if (!cliente.IdCustodianteAcoes.HasValue)
                idAgente = Convert.ToInt32(dropAgenteCorretora.SelectedItem.Value);            

            PosicaoBolsaCollection posicaoColl = new PosicaoBolsaCollection();
            decimal qtdeDisponivel = posicaoColl.RetornaQuantidadeDisponivel(dataOperacao, cdAtivoBolsa, idAgente, idCliente, true, false);
            e.Result = qtdeDisponivel.ToString();
        }        
    }

    protected void callBackFatorCot_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {        
        if (!gridCadastro.IsNewRowEditing)
        {
            ASPxButtonEdit btnEditAtivoBolsa = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa") as ASPxButtonEdit;
            ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
            string cdAtivoBolsa = btnEditAtivoBolsa.Text;
            if (!string.IsNullOrEmpty(textData.Text))
            {
                FatorCotacaoBolsa fatorCotacaoBolsa = new FatorCotacaoBolsa();
                if (fatorCotacaoBolsa.BuscaFatorCotacaoBolsa(cdAtivoBolsa, Convert.ToDateTime(textData.Text)))
                {
                    e.Result = fatorCotacaoBolsa.Fator.Value.ToString();
                }
                else
                {
                    e.Result = "noquote";
                }
            }
        }
    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSOperacaoBolsa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
        OperacaoBolsaQuery operacaoBolsaQuery = new OperacaoBolsaQuery("O");
        ClienteQuery clienteQuery = new ClienteQuery("C");
        AtivoBolsaQuery ativoBolsaQuery = new AtivoBolsaQuery("Ativo");
        //
        AgenteMercadoQuery agenteQuery = new AgenteMercadoQuery("A");

        operacaoBolsaQuery.Select(operacaoBolsaQuery, clienteQuery.Apelido.As("Apelido"), agenteQuery.Nome, ativoBolsaQuery.TipoMercado);
        operacaoBolsaQuery.InnerJoin(clienteQuery).On(operacaoBolsaQuery.IdCliente == clienteQuery.IdCliente);
        operacaoBolsaQuery.InnerJoin(ativoBolsaQuery).On(operacaoBolsaQuery.CdAtivoBolsa == ativoBolsaQuery.CdAtivoBolsa);
        operacaoBolsaQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        operacaoBolsaQuery.InnerJoin(agenteQuery).On(operacaoBolsaQuery.IdAgenteCorretora == agenteQuery.IdAgente);
        //
        operacaoBolsaQuery.Where(permissaoClienteQuery.IdUsuario.Equal(idUsuario));

        if (!String.IsNullOrEmpty(btnEditCodigoClienteFiltro.Text)) {
            operacaoBolsaQuery.Where(operacaoBolsaQuery.IdCliente.Equal(Convert.ToInt32(btnEditCodigoClienteFiltro.Text)));
        }

        if (!String.IsNullOrEmpty(textDataInicio.Text)) {
            operacaoBolsaQuery.Where(operacaoBolsaQuery.Data.GreaterThanOrEqual(textDataInicio.Text));
        }

        if (!String.IsNullOrEmpty(textDataFim.Text)) {
            operacaoBolsaQuery.Where(operacaoBolsaQuery.Data.LessThanOrEqual(textDataFim.Text));
        }

        if (!String.IsNullOrEmpty(textCdAtivo.Text)) {
            operacaoBolsaQuery.Where(operacaoBolsaQuery.CdAtivoBolsa.Like("%" + textCdAtivo.Text + "%"));
        }

        if (dropTipoOperacaoFiltro.SelectedIndex > -1) {
            operacaoBolsaQuery.Where(operacaoBolsaQuery.TipoOperacao.Equal(Convert.ToString(dropTipoOperacaoFiltro.SelectedItem.Value)));
        }

        if (dropTipoMercado.SelectedIndex > 0)
        {
            operacaoBolsaQuery.Where(operacaoBolsaQuery.TipoMercado == dropTipoMercado.SelectedItem.Value);
        }

        operacaoBolsaQuery.OrderBy(operacaoBolsaQuery.Data.Descending,
                                   agenteQuery.Nome.Ascending);

        OperacaoBolsaCollection coll = new OperacaoBolsaCollection();
        coll.Load(operacaoBolsaQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ClienteCollection coll = new ClienteCollection();
        coll.BuscaClientesComAcesso(Context.User.Identity.Name);
        
        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCategoriaMovimentacao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CategoriaMovimentacaoCollection coll = new CategoriaMovimentacaoCollection();
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSAgenteMercadoCorretora_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        AgenteMercadoCollection coll = new AgenteMercadoCollection();
        coll.Query.Where(coll.Query.FuncaoCorretora.Equal("S"));
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSAgenteMercadoLiquidacao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        AgenteMercadoCollection coll = new AgenteMercadoCollection();
        coll.Query.Where(coll.Query.FuncaoLiquidante.Equal("S"));
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSAtivoBolsa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        AtivoBolsaCollection coll = new AtivoBolsaCollection();
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSIndice_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        IndiceCollection coll = new IndiceCollection();
        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSLocalCustodia_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        LocalCustodiaCollection coll = new LocalCustodiaCollection();

        coll.Query.OrderBy(coll.Query.IdLocalCustodia.Ascending);
        coll.LoadAll();

        e.Collection = coll;
    }

    protected void EsDSClearing_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ClearingCollection coll = new ClearingCollection();

        coll.Query.OrderBy(coll.Query.IdClearing.Ascending);
        coll.LoadAll();

        e.Collection = coll;
    }

    protected void EsDSLocalNegociacao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        LocalNegociacaoCollection coll = new LocalNegociacaoCollection();

        coll.Query.OrderBy(coll.Query.IdLocalNegociacao.Ascending);
        coll.LoadAll();

        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void panelEdicao_Load(object sender, EventArgs e) 
    {
        if (ParametrosConfiguracaoSistema.Outras.PermitirOperacoesRetroativas)
        {
            ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;            
            textData.ClientEnabled = true;
        } 

        bool agenteLiquidacaoExclusivo = ParametrosConfiguracaoSistema.Outras.AgenteLiquidacaoExclusivo;

        if (!agenteLiquidacaoExclusivo) {
            Label labelAgenteLiquidacao = gridCadastro.FindEditFormTemplateControl("labelAgenteLiquidacao") as Label;
            ASPxComboBox dropAgenteLiquidacao = gridCadastro.FindEditFormTemplateControl("dropAgenteLiquidacao") as ASPxComboBox;
            labelAgenteLiquidacao.Visible = false;
            dropAgenteLiquidacao.Visible = false;
        }

        base.panelEdicao_Load(sender, e);

        bool multiConta = ParametrosConfiguracaoSistema.Outras.MultiConta;
        Label labelConta = gridCadastro.FindEditFormTemplateControl("labelConta") as Label;
        ASPxComboBox dropConta = gridCadastro.FindEditFormTemplateControl("dropConta") as ASPxComboBox;

        if (!multiConta)
        {
            labelConta.Visible = false;
            dropConta.Visible = false;
        }

        bool travaPainel = false;
        if (gridCadastro.IsEditing && !gridCadastro.IsNewRowEditing && Convert.ToString(Session["FormLoad"]) != "S") {
            Session["FormLoad"] = "S";

            int idOperacao = Convert.ToInt32(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "IdOperacao"));

            //Especificacao, TipoMercado, FatorCotacao
            string cdAtivoBolsa = gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "CdAtivoBolsa").ToString();
            DateTime data = Convert.ToDateTime(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "Data"));

            ASPxSpinEdit textFatorCotacao = gridCadastro.FindEditFormTemplateControl("textFatorCotacao") as ASPxSpinEdit;

            string fatorCotacao = "";
            FatorCotacaoBolsa fatorCotacaoBolsa = new FatorCotacaoBolsa();
            if (fatorCotacaoBolsa.BuscaFatorCotacaoBolsa(cdAtivoBolsa, data)) {
                fatorCotacao = fatorCotacaoBolsa.Fator.Value.ToString();
            }
            textFatorCotacao.Text = fatorCotacao;
            //

            //Numero de dias liquidação
            DateTime dataLiquidacao = Convert.ToDateTime(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "DataLiquidacao"));

            int numeroDias = Calendario.NumeroDias(data, dataLiquidacao, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
            ASPxSpinEdit textDiasLiquidacao = gridCadastro.FindEditFormTemplateControl("textDiasLiquidacao") as ASPxSpinEdit;
            textDiasLiquidacao.Text = numeroDias.ToString();
            //

            //CheckBox de custos informados
            CheckBox chkCustosInformados = gridCadastro.FindEditFormTemplateControl("chkCustosInformados") as CheckBox;
            string calculaDespesas = gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "CalculaDespesas").ToString();

            if (calculaDespesas == "S") {
                chkCustosInformados.Checked = false;
            }
            else {
                chkCustosInformados.Checked = true;
            }

            //CheckBox de Exercicio de Opcoes
            ASPxButtonEdit btnEditAtivoBolsa = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa") as ASPxButtonEdit;

            CheckBox chkExercicio = gridCadastro.FindEditFormTemplateControl("chkExercicio") as CheckBox;
            byte origem = Convert.ToByte(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "Origem"));
            if (origem == (byte)OrigemOperacaoBolsa.ExercicioOpcaoCompra ||
                origem == (byte)OrigemOperacaoBolsa.ExercicioOpcaoVenda) {
                chkExercicio.Checked = true;

                string cdAtivoBolsaOpcao = gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "CdAtivoBolsaOpcao").ToString();
                btnEditAtivoBolsa.Text = cdAtivoBolsaOpcao;
            }
            else {
                btnEditAtivoBolsa.Text = cdAtivoBolsa;
            }

            //Campos de Termo
            ASPxSpinEdit textTaxaTermo = gridCadastro.FindEditFormTemplateControl("textTaxaTermo") as ASPxSpinEdit;
            ASPxButtonEdit btnNumeroContratoTermo = gridCadastro.FindEditFormTemplateControl("btnNumeroContratoTermo") as ASPxButtonEdit;
            ASPxDateEdit textDataVencimentoTermo = gridCadastro.FindEditFormTemplateControl("textDataVencimentoTermo") as ASPxDateEdit;
            ASPxComboBox dropIndiceTermo = gridCadastro.FindEditFormTemplateControl("dropIndiceTermo") as ASPxComboBox;
            OperacaoTermoBolsa operacaoTermoBolsa = new OperacaoTermoBolsa();
            if (operacaoTermoBolsa.LoadByPrimaryKey(idOperacao)) {
                textTaxaTermo.Text = operacaoTermoBolsa.Taxa.ToString();
                btnNumeroContratoTermo.Text = operacaoTermoBolsa.NumeroContrato.ToString();
                textDataVencimentoTermo.Text = operacaoTermoBolsa.DataVencimento.ToString();

                if (operacaoTermoBolsa.IdIndice.HasValue) {
                    Indice indice = new Indice();
                    indice.LoadByPrimaryKey(operacaoTermoBolsa.IdIndice.Value);

                    int idIndice = operacaoTermoBolsa.IdIndice.Value;
                    dropIndiceTermo.Value = indice.Descricao;
                }
            }
            else {
                textTaxaTermo.Text = "";
                btnNumeroContratoTermo.Text = "";
                textDataVencimentoTermo.Text = "";
                dropIndiceTermo.SelectedIndex = -1;
            }

            Label labelEdicao = gridCadastro.FindEditFormTemplateControl("labelEdicao") as Label;

            int idCliente = Convert.ToInt32(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "IdCliente"));

            Cliente cliente = new Cliente();
            if (cliente.LoadByPrimaryKey(idCliente)) {
                DateTime dataDia = cliente.DataDia.Value;
                byte status = cliente.Status.Value;
                if (status == (byte)StatusCliente.Divulgado) {
                    travaPainel = true;
                    labelEdicao.Text = "Cliente está fechado. Dados não podem ser alterados.";
                }
                else if (DateTime.Compare(dataDia, data) > 0) {
                    travaPainel = true;
                    labelEdicao.Text = "Data informada anterior à data do cliente. Dados não podem ser alterados.";
                }
            }
        }

        if (travaPainel) 
        {
            LinkButton btnOK = gridCadastro.FindEditFormTemplateControl("btnOK") as LinkButton;
            btnOK.Visible = false;

            LinkButton btnObservacao = gridCadastro.FindEditFormTemplateControl("btnObservacao") as LinkButton;
            btnObservacao.Visible = true;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";

        if (!gridCadastro.IsEditing) //Testa deleção
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdCliente");
            List<object> keyData = gridCadastro.GetSelectedFieldValues("Data");
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idCliente = Convert.ToInt32(keyValuesId[i]);
                DateTime data = Convert.ToDateTime(keyData[i]);

                Cliente cliente = new Cliente();
                if (cliente.LoadByPrimaryKey(idCliente)) {
                    DateTime dataDia = cliente.DataDia.Value;
                    byte status = cliente.Status.Value;
                    string clienteDescricao = cliente.Apelido + " (" + cliente.IdCliente.ToString() + ")";
                    if (status == (byte)StatusCliente.Divulgado) {
                        e.Result = "Cliente " + clienteDescricao + " está fechado! Deleção cancelada.";
                        return;
                    }
                    else if (DateTime.Compare(dataDia, data) > 0) {
                        e.Result = "Lançamento com data " + data.ToString().Substring(0, 10) + " anterior à data do cliente " + clienteDescricao + "! Deleção cancelada.";
                        return;
                    }
                }
            }
        }
        else {
            ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
            ASPxComboBox dropAgenteCorretora = gridCadastro.FindEditFormTemplateControl("dropAgenteCorretora") as ASPxComboBox;
            ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
            ASPxComboBox dropTipoOperacao = gridCadastro.FindEditFormTemplateControl("dropTipoOperacao") as ASPxComboBox;
            ASPxButtonEdit btnEditAtivoBolsa = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa") as ASPxButtonEdit;
            ASPxSpinEdit textPercentualDesconto = gridCadastro.FindEditFormTemplateControl("textPercentualDesconto") as ASPxSpinEdit;
            ASPxSpinEdit textDiasLiquidacao = gridCadastro.FindEditFormTemplateControl("textDiasLiquidacao") as ASPxSpinEdit;
            ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
            ASPxSpinEdit textPU = gridCadastro.FindEditFormTemplateControl("textPU") as ASPxSpinEdit;
            ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
            CheckBox chkExercicio = gridCadastro.FindEditFormTemplateControl("chkExercicio") as CheckBox;
            ASPxComboBox dropLocalNegociacao = gridCadastro.FindEditFormTemplateControl("dropLocalNegociacao") as ASPxComboBox;
            ASPxComboBox dropLocalCustodia = gridCadastro.FindEditFormTemplateControl("dropLocalCustodia") as ASPxComboBox;
            ASPxComboBox dropClearing = gridCadastro.FindEditFormTemplateControl("dropClearing") as ASPxComboBox;
            ASPxDateEdit textDataOperacao = gridCadastro.FindEditFormTemplateControl("textDataOperacao") as ASPxDateEdit;

            #region Campos obrigatórios
            List<Control> controles = new List<Control>();            
            controles.Add(btnEditCodigoCliente);
            controles.Add(dropAgenteCorretora);
            controles.Add(dropTipoOperacao);
            controles.Add(btnEditAtivoBolsa);
            controles.Add(textPercentualDesconto);
            controles.Add(textDiasLiquidacao);
            controles.Add(textQuantidade);
            controles.Add(textPU);
            controles.Add(textValor);
            controles.Add(dropLocalNegociacao);
            controles.Add(dropLocalCustodia);
            controles.Add(dropClearing);

            if (base.TestaObrigatorio(controles) != "") {
                e.Result = "Campos com * são obrigatórios!";
                return;
            }
            #endregion

            int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
            DateTime data = Convert.ToDateTime(textData.Text);

            AtivoBolsa ativoBolsa = new AtivoBolsa();
            ativoBolsa.LoadByPrimaryKey(btnEditAtivoBolsa.Text);

            
            if (textDataOperacao.Date < textData.Date)
            {
                e.Result = "Data de operação não pode ser anterior a data de registro";
                return;
            }




            if (ativoBolsa.IdMoeda == (int)ListaMoedaFixo.Real)
            {
                if (!Calendario.IsDiaUtil(data, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil))
                {
                    e.Result = "Data de registro não é dia útil.";
                    return;
                }

                if (!Calendario.IsDiaUtil(textDataOperacao.Date, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil))
                {
                    e.Result = "Data de operação não é dia útil.";
                    return;
                }

            }
            else
            {
                if (!Calendario.IsDiaUtil(data, LocalFeriadoFixo.NovaYork, TipoFeriado.Outros))
                {
                    e.Result = "Data de registro não é dia útil.";
                    return;
                }

                if (!Calendario.IsDiaUtil(textDataOperacao.Date, LocalFeriadoFixo.NovaYork, TipoFeriado.Outros))
                {
                    e.Result = "Data de operação não é dia útil.";
                    return;
                }
            }

            Cliente cliente = new Cliente();
            if (cliente.LoadByPrimaryKey(idCliente)) {
                DateTime dataDia = cliente.DataDia.Value;
                DateTime dataInicio = cliente.DataInicio.Value;
                byte status = cliente.Status.Value;
                if (status == (byte)StatusCliente.Divulgado) {
                    e.Result = "Cliente está fechado! Operação não pode ser realizada.";
                    return;
                }
                else if (dataInicio > data)
                {
                    e.Result = "Data informada não pode ser anterior a data de início do Cliente(" + dataInicio.ToString("dd/MM/yyyy") + ").";
                    return;
                }
                //else if (DateTime.Compare(dataDia, Convert.ToDateTime(textData.Text)) > 0) {
                //    e.Result = "Data informada anterior à data do cliente! Operação não pode ser realizada.";
                //    return;
                //}
            }
            
            // Se Data está Preenchida e Existe Ativo compara a Data de Vencimento
            if (!String.IsNullOrEmpty(textData.Text)) {

                if (ativoBolsa.TipoMercado == TipoMercadoBolsa.Termo && ativoBolsa.DataVencimento.HasValue && ativoBolsa.DataVencimento < Convert.ToDateTime(textData.Text))
                {
                    e.Result = "Ativo Vencido em: " + ativoBolsa.DataVencimento.Value.ToString("d");
                    return;
                }
            }

            if (chkExercicio.Checked) {
                if (Convert.ToString(dropTipoOperacao.SelectedItem.Value) == TipoOperacaoBolsa.Deposito ||
                    Convert.ToString(dropTipoOperacao.SelectedItem.Value) == TipoOperacaoBolsa.Retirada) {
                    e.Result = "Exercícios de opção só podem ter associados tipos de operação Compra, Venda, Compra DT, Venda DT.";
                    return;
                }

                if (ativoBolsa.TipoMercado != TipoMercadoBolsa.OpcaoCompra && ativoBolsa.TipoMercado != TipoMercadoBolsa.OpcaoVenda) {
                    e.Result = "O ativo precisa ser opção de compra/venda para exercícios de opção.";
                    return;
                }
            }

            #region TipoOperacao
            List<string> lstApenasParaFundo = new List<string>();
            lstApenasParaFundo.Add(TipoOperacaoBolsa.IngressoAtivoImpactoCota);
            lstApenasParaFundo.Add(TipoOperacaoBolsa.IngressoAtivoImpactoQtde);
            lstApenasParaFundo.Add(TipoOperacaoBolsa.RetiradaAtivoImpactoCota);
            lstApenasParaFundo.Add(TipoOperacaoBolsa.RetiradaAtivoImpactoQtde);

            if (cliente.IdTipo.Value != (int)TipoClienteFixo.Fundo && lstApenasParaFundo.Contains(dropTipoOperacao.SelectedItem.Value.ToString()))
            {
                e.Result = "Tipo de Operação permitida apenas para Cliente do Tipo 'Fundo de Investimento'!";
                return;
            }
            #endregion

            #region Operacao Retroativa
            if (ParametrosConfiguracaoSistema.Outras.PermitirOperacoesRetroativas)
            {
                if (DateTime.Compare(data, cliente.DataDia.Value) < 0)
                {
                    e.Result = "Data de registro anterior a data do Cliente. | Favor processar a carteira novamente a partir de " + data.ToString("dd/MM/yyyy");
                    return;
                }
            }
            #endregion
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOKAdd_Init(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        string resultado = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") {
                if (cliente.Status.Value != (byte)StatusCliente.Divulgado) {

                    if (cliente.IsAtivo) {
                        PermissaoCliente permissaoCliente = new PermissaoCliente();
                        if (permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name))
                        {
                            nome = cliente.str.Apelido;
                            DateTime dataDia = cliente.DataDia.Value;

                            resultado = this.gridCadastro.IsNewRowEditing
                                       ? nome + "|" + dataDia.ToString().Substring(0, 10) + "|" + Thread.CurrentThread.CurrentCulture.Name
                                       : nome;
                        }
                        else {
                            resultado = "no_access";
                        }
                    }
                    else {
                        resultado = "no_active";
                    }
                }
                else {
                    resultado = cliente.str.Apelido + "|status_closed";
                }
            }
        }
        e.Result = resultado;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string resultado = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            AtivoBolsa ativoBolsa = new AtivoBolsa();
            ativoBolsa.LoadByPrimaryKey(Convert.ToString(e.Parameter));

            if (ativoBolsa.str.CdAtivoBolsa != "") {
                ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;

                string cdAtivoBolsa = ativoBolsa.str.CdAtivoBolsa;
                string especificacao = ativoBolsa.Especificacao;
                string tipoMercado = ativoBolsa.TipoMercado;

                //Dias de liquidação
                ASPxSpinEdit textDiasLiquidacao = gridCadastro.FindEditFormTemplateControl("textDiasLiquidacao") as ASPxSpinEdit;
                string diasLiquidacao = "";
                if (ativoBolsa.TipoMercado == TipoMercadoBolsa.MercadoVista ||
                    ativoBolsa.TipoMercado == TipoMercadoBolsa.Termo ||
                    ativoBolsa.TipoMercado == TipoMercadoBolsa.Imobiliario)
                {
                    diasLiquidacao = LiquidacaoMercado.Acoes.ToString();
                }
                else {
                    diasLiquidacao = LiquidacaoMercado.OpcoesBolsa.ToString();
                }

                //Fator de cotação
                string fatorCotacao = "";
                if (textData.Text != "") {
                    FatorCotacaoBolsa fatorCotacaoBolsa = new FatorCotacaoBolsa();
                    if (fatorCotacaoBolsa.BuscaFatorCotacaoBolsa(cdAtivoBolsa, Convert.ToDateTime(textData.Text))) {
                        fatorCotacao = fatorCotacaoBolsa.Fator.Value.ToString();
                    }
                    else {
                        e.Result = "noquote";
                        return;
                    }
                }

                //Vencimento do termo
                string dataVencimento = "";
                if (ativoBolsa.TipoMercado == TipoMercadoBolsa.Termo) {
                    dataVencimento = ativoBolsa.DataVencimento.Value.ToString().Substring(0, 10);
                }

                int idLocalCustodia = (int)ativoBolsa.IdLocalCustodia;
                int idLocalNegociacao = (int)ativoBolsa.IdLocalNegociacao;
                int idClearing = (int)ativoBolsa.IdClearing;

                resultado = cdAtivoBolsa + "|" + fatorCotacao + "|" +
                            diasLiquidacao + "|" + dataVencimento + "|" + Thread.CurrentThread.CurrentCulture.Name + "|" + idLocalCustodia + "|" + idLocalNegociacao + "|" + idClearing;
            }
        }
        e.Result = resultado;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callBackCustos_Callback(object source, DevExpress.Web.CallbackEventArgs e) 
    {
        if (btnEditCodigoCustos.Text == "" ||
            textCorretagem.Text == "" ||
            textTaxas.Text == "") {
            e.Result = "Os campos precisam ser preenchidos corretamente.";
            return;
        }

        int idCliente = Convert.ToInt32(btnEditCodigoCustos.Text);
        Cliente cliente = new Cliente();
        List<esQueryItem> campos = new List<esQueryItem>();
        campos.Add(cliente.Query.DataDia);
        campos.Add(cliente.Query.Status);
        if (!cliente.LoadByPrimaryKey(campos, idCliente)) {
            e.Result = "Cliente " + idCliente.ToString() + " inexistente.";
            return;
        }
        else if (cliente.Status.Value == (byte)StatusCliente.Divulgado) {
            e.Result = "Cliente " + idCliente.ToString() + " está fechado. Processo não pode ser executado.";
            return;
        }

        decimal corretagem = Convert.ToDecimal(textCorretagem.Text);
        decimal taxas = Convert.ToDecimal(textTaxas.Text);
        DateTime data = cliente.DataDia.Value;

        List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdOperacao");
        List<int> listaIds = new List<int>();
        for (int i = 0; i < keyValuesId.Count; i++)
        {
            int idOperacao = Convert.ToInt32(keyValuesId[i]);
            OperacaoBolsa operacaoBolsaCheck = new OperacaoBolsa();
            operacaoBolsaCheck.LoadByPrimaryKey(idOperacao);
            if (operacaoBolsaCheck.Data.Value != data)
            {
                e.Result = "Existem operações selecionadas com data diferente da data atual do cliente. Processo não pode ser executado.";
                return;
            }

            listaIds.Add(idOperacao);
        }

        OperacaoBolsa operacaoBolsa = new OperacaoBolsa();
        if (listaIds.Count == 0)
        {
            operacaoBolsa.RateiaTaxasTudo(idCliente, data, corretagem, taxas);
        }
        else
        {
            operacaoBolsa.RateiaTaxasTudoPorIds(listaIds, corretagem, taxas);
        }

        e.Result = "Processo executado com sucesso.";
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        int idOperacao = (int)e.Keys[0];
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxButtonEdit btnEditAtivoBolsa = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa") as ASPxButtonEdit;
        ASPxComboBox dropAgenteCorretora = gridCadastro.FindEditFormTemplateControl("dropAgenteCorretora") as ASPxComboBox;
        ASPxComboBox dropAgenteLiquidacao = gridCadastro.FindEditFormTemplateControl("dropAgenteLiquidacao") as ASPxComboBox;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxDateEdit textDataOperacao = gridCadastro.FindEditFormTemplateControl("textDataOperacao") as ASPxDateEdit;
        ASPxComboBox dropTipoOperacao = gridCadastro.FindEditFormTemplateControl("dropTipoOperacao") as ASPxComboBox;
        ASPxSpinEdit textPercentualDesconto = gridCadastro.FindEditFormTemplateControl("textPercentualDesconto") as ASPxSpinEdit;
        ASPxSpinEdit textDiasLiquidacao = gridCadastro.FindEditFormTemplateControl("textDiasLiquidacao") as ASPxSpinEdit;
        ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
        ASPxSpinEdit textPU = gridCadastro.FindEditFormTemplateControl("textPU") as ASPxSpinEdit;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
        CheckBox chkCustosInformados = gridCadastro.FindEditFormTemplateControl("chkCustosInformados") as CheckBox;
        CheckBox chkExercicio = gridCadastro.FindEditFormTemplateControl("chkExercicio") as CheckBox;
        TextBox textObservacao = gridCadastro.FindEditFormTemplateControl("textObservacao") as TextBox;
        ASPxComboBox dropAgenteCustodia = gridCadastro.FindEditFormTemplateControl("dropAgenteCustodia") as ASPxComboBox;
        ASPxComboBox dropCategoriaMovimentacao = gridCadastro.FindEditFormTemplateControl("dropCategoriaMovimentacao") as ASPxComboBox;
        ASPxComboBox dropLocalNegociacao = gridCadastro.FindEditFormTemplateControl("dropLocalNegociacao") as ASPxComboBox;
        ASPxComboBox dropLocalCustodia = gridCadastro.FindEditFormTemplateControl("dropLocalCustodia") as ASPxComboBox;
        ASPxComboBox dropClearing = gridCadastro.FindEditFormTemplateControl("dropClearing") as ASPxComboBox;
        ASPxComboBox dropTrader = gridCadastro.FindEditFormTemplateControl("dropTrader") as ASPxComboBox;
        ASPxComboBox dropConta = gridCadastro.FindEditFormTemplateControl("dropConta") as ASPxComboBox;

        OperacaoBolsa operacaoBolsa = new OperacaoBolsa();
        if (operacaoBolsa.LoadByPrimaryKey(idOperacao)) {
            int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);

            operacaoBolsa.IdCliente = idCliente;
            operacaoBolsa.CdAtivoBolsa = Convert.ToString(btnEditAtivoBolsa.Text).ToUpper();
            operacaoBolsa.IdAgenteCorretora = Convert.ToInt32(dropAgenteCorretora.SelectedItem.Value);
            operacaoBolsa.IdLocalNegociacao = Convert.ToInt32(dropLocalNegociacao.SelectedItem.Value);
            operacaoBolsa.IdLocalCustodia = Convert.ToInt32(dropLocalCustodia.SelectedItem.Value);
            operacaoBolsa.IdClearing = Convert.ToInt32(dropClearing.SelectedItem.Value);
            //Trata agente liquidacao

            if (ParametrosConfiguracaoSistema.Outras.AgenteLiquidacaoExclusivo)
            {
                operacaoBolsa.IdAgenteLiquidacao = Convert.ToInt32(dropAgenteLiquidacao.SelectedItem.Value);
            }
            else {
                operacaoBolsa.IdAgenteLiquidacao = Convert.ToInt32(dropAgenteCorretora.SelectedItem.Value);
            }
            //
            AtivoBolsa ativoBolsa = new AtivoBolsa();
            ativoBolsa.LoadByPrimaryKey(Convert.ToString(btnEditAtivoBolsa.Text));
            string tipoMercadoAtivo = ativoBolsa.TipoMercado;
            int idMoeda = ativoBolsa.IdMoeda.Value;

            DateTime dataVencimento = new DateTime();
            if (ativoBolsa.TipoMercado == TipoMercadoBolsa.Termo && ativoBolsa.DataVencimento.HasValue)
            {
                dataVencimento = ativoBolsa.DataVencimento.Value;
            }

            if (dropCategoriaMovimentacao.SelectedIndex > -1)
            {
                operacaoBolsa.IdCategoriaMovimentacao = Convert.ToInt32(dropCategoriaMovimentacao.SelectedItem.Value);
            }
            else
            {
                operacaoBolsa.IdCategoriaMovimentacao = null;
            }

            operacaoBolsa.IdMoeda = idMoeda;
            operacaoBolsa.TipoMercado = tipoMercadoAtivo;

            if (dropTrader.SelectedIndex > -1)
                operacaoBolsa.IdTrader = Convert.ToInt32(dropTrader.SelectedItem.Value);
            else
                operacaoBolsa.IdTrader = null;

            //Trata exercicio!!!! ****************
            if (chkExercicio.Checked) {
                if (Convert.ToString(dropTipoOperacao.SelectedItem.Value) == TipoOperacaoBolsa.Compra ||
                    Convert.ToString(dropTipoOperacao.SelectedItem.Value) == TipoOperacaoBolsa.CompraDaytrade ||
                    Convert.ToString(dropTipoOperacao.SelectedItem.Value) == TipoOperacaoBolsa.Venda ||
                    Convert.ToString(dropTipoOperacao.SelectedItem.Value) == TipoOperacaoBolsa.VendaDaytrade) {
                    if (tipoMercadoAtivo == TipoMercadoBolsa.OpcaoCompra) {
                        operacaoBolsa.Origem = (byte)OrigemOperacaoBolsa.ExercicioOpcaoCompra;
                    }
                    else if (tipoMercadoAtivo == TipoMercadoBolsa.OpcaoVenda) {
                        operacaoBolsa.Origem = (byte)OrigemOperacaoBolsa.ExercicioOpcaoVenda;
                    }
                }
            }
            else {
                operacaoBolsa.Origem = (byte)OrigemOperacaoBolsa.Primaria;
            }

            if (operacaoBolsa.Origem == (byte)OrigemOperacaoBolsa.ExercicioOpcaoCompra ||
                operacaoBolsa.Origem == (byte)OrigemOperacaoBolsa.ExercicioOpcaoVenda) {
                //Precisa transformar a opcao lancada para a vista e usar o ativo informado como opcao (CdAtivoBolsaOpcao)
                operacaoBolsa.CdAtivoBolsa = ativoBolsa.CdAtivoBolsaObjeto;
                operacaoBolsa.CdAtivoBolsaOpcao = Convert.ToString(btnEditAtivoBolsa.Text).ToUpper();
                operacaoBolsa.TipoMercado = TipoMercadoBolsa.MercadoVista;
            }
            //**************************************

            operacaoBolsa.TipoOperacao = Convert.ToString(dropTipoOperacao.SelectedItem.Value);
            operacaoBolsa.Pu = Convert.ToDecimal(textPU.Text);
            operacaoBolsa.PULiquido = Convert.ToDecimal(textPU.Text);
            operacaoBolsa.Valor = Convert.ToDecimal(textValor.Text);
            operacaoBolsa.Quantidade = Convert.ToDecimal(textQuantidade.Text);

            DateTime data = Convert.ToDateTime(textData.Text);
            int diasLiquidacao = Convert.ToInt32(textDiasLiquidacao.Text);
            DateTime dataLiquidacao = Calendario.AdicionaDiaUtil(data, diasLiquidacao, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
            operacaoBolsa.Data = data;
            operacaoBolsa.DataOperacao = Convert.ToDateTime(textDataOperacao.Text);
            operacaoBolsa.DataLiquidacao = dataLiquidacao;
            operacaoBolsa.PercentualDesconto = Convert.ToDecimal(textPercentualDesconto.Text);

            //Trata Custos Informados
            operacaoBolsa.CalculaDespesas = chkCustosInformados.Checked ? "N" : "S";

            if (textObservacao.Text.Trim() != "")
            {
                operacaoBolsa.Observacao = textObservacao.Text;
            }

            if (ParametrosConfiguracaoSistema.Outras.MultiConta && dropConta.Value != null && !string.IsNullOrEmpty(dropConta.Value.ToString()))
            {
                operacaoBolsa.IdConta = Convert.ToInt32(dropConta.Value);
            }

            operacaoBolsa.Save();

            #region Operação retroativa - Verifica se deve marcar o cliente para efetuar o reprocessamento
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);
            if (DateTime.Compare(cliente.DataDia.Value, operacaoBolsa.Data.Value) > 0)
            {
                BoletoRetroativo boletoRetroativo = new BoletoRetroativo();
                if (boletoRetroativo.LoadByPrimaryKey(idCliente))
                {
                    if (DateTime.Compare(boletoRetroativo.DataBoleto.Value, operacaoBolsa.Data.Value) > 0)
                    {
                        boletoRetroativo.DataBoleto = operacaoBolsa.Data.Value;
                    }
                }
                else
                {
                    boletoRetroativo.IdCliente = idCliente;
                    boletoRetroativo.DataBoleto = operacaoBolsa.Data.Value;
                    boletoRetroativo.TipoMercado = (int)TipoMercado.Bolsa;
                }

                boletoRetroativo.Save();
            }
            #endregion

            //Se data da operação anterior a data do cliente, marca para Reprocessamento
            cliente.LoadByPrimaryKey(idCliente);

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de OperacaoBolsa - Operacao: Update OperacaoBolsa: " + idOperacao + UtilitarioWeb.ToString(operacaoBolsa),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion

            //Salva a operação termo bolsa
            if (tipoMercadoAtivo == TipoMercadoBolsa.Termo) {
                ASPxSpinEdit textTaxaTermo = gridCadastro.FindEditFormTemplateControl("textTaxaTermo") as ASPxSpinEdit;
                ASPxButtonEdit btnNumeroContratoTermo = gridCadastro.FindEditFormTemplateControl("btnNumeroContratoTermo") as ASPxButtonEdit;
                ASPxComboBox dropIndiceTermo = gridCadastro.FindEditFormTemplateControl("dropIndiceTermo") as ASPxComboBox;

                short? idIndice = null;
                if (dropIndiceTermo.SelectedIndex != -1) {
                    idIndice = Convert.ToInt16(dropIndiceTermo.SelectedItem.Value);
                }
                decimal taxa = 0;
                if (textTaxaTermo.Text != "") {
                    taxa = Convert.ToDecimal(textTaxaTermo.Text);
                }
                string numeroContrato = btnNumeroContratoTermo.Text.ToString();
                OperacaoTermoBolsa operacaoTermoBolsa = new OperacaoTermoBolsa();
                if (!operacaoTermoBolsa.LoadByPrimaryKey(idOperacao)) {
                    OperacaoTermoBolsa operacaoTermoBolsaInsert = new OperacaoTermoBolsa();
                    operacaoTermoBolsaInsert.IdOperacao = idOperacao;
                    operacaoTermoBolsaInsert.NumeroContrato = numeroContrato;
                    operacaoTermoBolsaInsert.Taxa = taxa;
                    operacaoTermoBolsaInsert.IdIndice = idIndice;
                    operacaoTermoBolsaInsert.DataVencimento = dataVencimento;
                    operacaoTermoBolsaInsert.Save();
                }
                else {
                    operacaoTermoBolsa.NumeroContrato = numeroContrato;
                    operacaoTermoBolsa.Taxa = taxa;
                    operacaoTermoBolsa.IdIndice = idIndice;
                    operacaoTermoBolsa.DataVencimento = dataVencimento;
                    operacaoTermoBolsa.Save();
                }
            }
            else {
                OperacaoTermoBolsa operacaoTermoBolsaDeletar = new OperacaoTermoBolsa();
                if (operacaoTermoBolsaDeletar.LoadByPrimaryKey(idOperacao)) {
                    operacaoTermoBolsaDeletar.MarkAsDeleted();
                    operacaoTermoBolsaDeletar.Save();
                }
            }

            //Atualiza StatusRealTime para executar***********
            cliente = new Cliente();
            cliente.SetaFlagRealTime(idCliente, (byte)StatusRealTimeCliente.Executar);
            //************************************************
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    private void SalvarNovo() 
    {
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxButtonEdit btnEditAtivoBolsa = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa") as ASPxButtonEdit;
        ASPxComboBox dropAgenteCorretora = gridCadastro.FindEditFormTemplateControl("dropAgenteCorretora") as ASPxComboBox;
        ASPxComboBox dropAgenteLiquidacao = gridCadastro.FindEditFormTemplateControl("dropAgenteLiquidacao") as ASPxComboBox;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxDateEdit textDataOperacao = gridCadastro.FindEditFormTemplateControl("textDataOperacao") as ASPxDateEdit;
        ASPxComboBox dropTipoOperacao = gridCadastro.FindEditFormTemplateControl("dropTipoOperacao") as ASPxComboBox;
        ASPxSpinEdit textPercentualDesconto = gridCadastro.FindEditFormTemplateControl("textPercentualDesconto") as ASPxSpinEdit;
        ASPxSpinEdit textDiasLiquidacao = gridCadastro.FindEditFormTemplateControl("textDiasLiquidacao") as ASPxSpinEdit;
        ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
        ASPxSpinEdit textPU = gridCadastro.FindEditFormTemplateControl("textPU") as ASPxSpinEdit;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
        CheckBox chkCustosInformados = gridCadastro.FindEditFormTemplateControl("chkCustosInformados") as CheckBox;
        CheckBox chkExercicio = gridCadastro.FindEditFormTemplateControl("chkExercicio") as CheckBox;
        TextBox textObservacao = gridCadastro.FindEditFormTemplateControl("textObservacao") as TextBox;
        ASPxComboBox dropAgenteCustodia = gridCadastro.FindEditFormTemplateControl("dropAgenteCustodia") as ASPxComboBox;
        ASPxComboBox dropCategoriaMovimentacao = gridCadastro.FindEditFormTemplateControl("dropCategoriaMovimentacao") as ASPxComboBox;
        ASPxComboBox dropLocalNegociacao = gridCadastro.FindEditFormTemplateControl("dropLocalNegociacao") as ASPxComboBox;
        ASPxComboBox dropLocalCustodia = gridCadastro.FindEditFormTemplateControl("dropLocalCustodia") as ASPxComboBox;
        ASPxComboBox dropClearing = gridCadastro.FindEditFormTemplateControl("dropClearing") as ASPxComboBox;
        ASPxComboBox dropTrader = gridCadastro.FindEditFormTemplateControl("dropTrader") as ASPxComboBox;
        ASPxComboBox dropConta = gridCadastro.FindEditFormTemplateControl("dropConta") as ASPxComboBox;

        OperacaoBolsa operacaoBolsa = new OperacaoBolsa();

        int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);

        operacaoBolsa.IdCliente = idCliente;
        operacaoBolsa.CdAtivoBolsa = Convert.ToString(btnEditAtivoBolsa.Text).ToUpper();
        operacaoBolsa.IdAgenteCorretora = Convert.ToInt32(dropAgenteCorretora.SelectedItem.Value);
        operacaoBolsa.IdLocalNegociacao = Convert.ToInt32(dropLocalNegociacao.SelectedItem.Value);
        operacaoBolsa.IdLocalCustodia = Convert.ToInt32(dropLocalCustodia.SelectedItem.Value);
        operacaoBolsa.IdClearing = Convert.ToInt32(dropClearing.SelectedItem.Value);
        
        DateTime data = Convert.ToDateTime(textData.Text);

        //Trata agente liquidacao
        if (dropAgenteLiquidacao.SelectedIndex > -1) 
        {
            operacaoBolsa.IdAgenteLiquidacao = Convert.ToInt32(dropAgenteLiquidacao.SelectedItem.Value);
        }
        else
        {
            operacaoBolsa.IdAgenteLiquidacao = Convert.ToInt32(dropAgenteCorretora.SelectedItem.Value);
        }
        //

        if (dropCategoriaMovimentacao.SelectedIndex > -1)
        {
            operacaoBolsa.IdCategoriaMovimentacao = Convert.ToInt32(dropCategoriaMovimentacao.SelectedItem.Value);
        }
        else
        {
            operacaoBolsa.IdCategoriaMovimentacao = null;
        }

        AtivoBolsa ativoBolsa = new AtivoBolsa();
        ativoBolsa.LoadByPrimaryKey(Convert.ToString(btnEditAtivoBolsa.Text));
        string tipoMercadoAtivo = ativoBolsa.TipoMercado;
        int idMoeda = ativoBolsa.IdMoeda.Value;

        DateTime dataVencimento = new DateTime();
        if (ativoBolsa.DataVencimento.HasValue && ativoBolsa.TipoMercado == TipoMercadoBolsa.Termo)
        {
            dataVencimento = ativoBolsa.DataVencimento.Value;
        }
        
        operacaoBolsa.IdMoeda = idMoeda;
        operacaoBolsa.TipoMercado = tipoMercadoAtivo;
        operacaoBolsa.TipoOperacao = Convert.ToString(dropTipoOperacao.SelectedItem.Value);
        


        //Trata exercicio!!!! ****************
        if (chkExercicio.Checked) {
            if (Convert.ToString(dropTipoOperacao.SelectedItem.Value) == TipoOperacaoBolsa.Compra ||
                Convert.ToString(dropTipoOperacao.SelectedItem.Value) == TipoOperacaoBolsa.CompraDaytrade ||
                Convert.ToString(dropTipoOperacao.SelectedItem.Value) == TipoOperacaoBolsa.Venda ||
                Convert.ToString(dropTipoOperacao.SelectedItem.Value) == TipoOperacaoBolsa.VendaDaytrade) {
                if (tipoMercadoAtivo == TipoMercadoBolsa.OpcaoCompra) {
                    operacaoBolsa.Origem = (byte)OrigemOperacaoBolsa.ExercicioOpcaoCompra;
                }
                else if (tipoMercadoAtivo == TipoMercadoBolsa.OpcaoVenda) {
                    operacaoBolsa.Origem = (byte)OrigemOperacaoBolsa.ExercicioOpcaoVenda;
                }
            }
        }
        else {
            operacaoBolsa.Origem = (byte)OrigemOperacaoBolsa.Primaria;
        }

        if (operacaoBolsa.Origem == (byte)OrigemOperacaoBolsa.ExercicioOpcaoCompra ||
            operacaoBolsa.Origem == (byte)OrigemOperacaoBolsa.ExercicioOpcaoVenda) {
            //Precisa transformar a opcao lancada para a vista e usar o ativo informado como opcao (CdAtivoBolsaOpcao)
            operacaoBolsa.CdAtivoBolsa = ativoBolsa.CdAtivoBolsaObjeto;
            operacaoBolsa.CdAtivoBolsaOpcao = Convert.ToString(btnEditAtivoBolsa.Text).ToUpper();
            operacaoBolsa.TipoMercado = TipoMercadoBolsa.MercadoVista;
        }
        //**************************************


        operacaoBolsa.Pu = Convert.ToDecimal(textPU.Text);
        operacaoBolsa.PULiquido = Convert.ToDecimal(textPU.Text);
        operacaoBolsa.Valor = Convert.ToDecimal(textValor.Text);
        operacaoBolsa.Quantidade = Convert.ToDecimal(textQuantidade.Text);
                
        int diasLiquidacao = Convert.ToInt32(textDiasLiquidacao.Text);
        DateTime dataLiquidacao = Calendario.AdicionaDiaUtil(data, diasLiquidacao, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
        operacaoBolsa.Data = Convert.ToDateTime(textData.Text.ToString());        
        operacaoBolsa.DataOperacao = Convert.ToDateTime(textDataOperacao.Text.ToString());
        operacaoBolsa.DataLiquidacao = dataLiquidacao;
        operacaoBolsa.Fonte = (byte)FonteOperacaoBolsa.Manual;
        operacaoBolsa.PercentualDesconto = Convert.ToDecimal(textPercentualDesconto.Text);
        operacaoBolsa.IdLocal = Convert.ToInt16(LocalFeriadoFixo.Bovespa);

        if (dropTrader.SelectedIndex > -1)
            operacaoBolsa.IdTrader = Convert.ToInt32(dropTrader.SelectedItem.Value);
        else
            operacaoBolsa.IdTrader = null;

        //Trata Custos Informados
        operacaoBolsa.CalculaDespesas = chkCustosInformados.Checked ? "N" : "S";

        if (textObservacao.Text.Trim() != "")
        {
            operacaoBolsa.Observacao = textObservacao.Text;
        }

        if (ParametrosConfiguracaoSistema.Outras.MultiConta && dropConta.Value != null && !string.IsNullOrEmpty(dropConta.Value.ToString()))
        {
            operacaoBolsa.IdConta = Convert.ToInt32(dropConta.Value);
        }

        operacaoBolsa.Save();

        #region Operação retroativa - Verifica se deve marcar o cliente para efetuar o reprocessamento
        Cliente cliente = new Cliente();
        cliente.LoadByPrimaryKey(idCliente);
        if (DateTime.Compare(cliente.DataDia.Value, operacaoBolsa.Data.Value) > 0)
        {
            BoletoRetroativo boletoRetroativo = new BoletoRetroativo();
            if (boletoRetroativo.LoadByPrimaryKey(idCliente))
            {
                if (DateTime.Compare(boletoRetroativo.DataBoleto.Value, operacaoBolsa.Data.Value) > 0)
                {
                    boletoRetroativo.DataBoleto = operacaoBolsa.Data.Value;
                }
            }
            else
            {
                boletoRetroativo.IdCliente = idCliente;
                boletoRetroativo.DataBoleto = operacaoBolsa.Data.Value;
                boletoRetroativo.TipoMercado = (int)TipoMercado.Bolsa;
            }

            boletoRetroativo.Save();
        }
        #endregion

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de OperacaoBolsa - Operacao: Insert OperacaoBolsa: " + operacaoBolsa.IdOperacao + UtilitarioWeb.ToString(operacaoBolsa),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        //Salva a operação termo bolsa
        if (tipoMercadoAtivo == TipoMercadoBolsa.Termo) {
            ASPxSpinEdit textTaxaTermo = gridCadastro.FindEditFormTemplateControl("textTaxaTermo") as ASPxSpinEdit;
            ASPxButtonEdit btnNumeroContratoTermo = gridCadastro.FindEditFormTemplateControl("btnNumeroContratoTermo") as ASPxButtonEdit;
            ASPxComboBox dropIndiceTermo = gridCadastro.FindEditFormTemplateControl("dropIndiceTermo") as ASPxComboBox;

            short? idIndice = null;
            if (dropIndiceTermo.SelectedIndex != -1) {
                idIndice = Convert.ToInt16(dropIndiceTermo.SelectedItem.Value);
            }
            decimal taxa = 0;
            if (textTaxaTermo.Text != "") {
                taxa = Convert.ToDecimal(textTaxaTermo.Text);
            }
            string numeroContrato = btnNumeroContratoTermo.Text.ToString();
            OperacaoTermoBolsa operacaoTermoBolsa = new OperacaoTermoBolsa();

            operacaoTermoBolsa.IdOperacao = operacaoBolsa.IdOperacao; //Usa o mesmo IdOperacao de OperacaoBolsa
            operacaoTermoBolsa.NumeroContrato = numeroContrato;
            operacaoTermoBolsa.Taxa = taxa;
            operacaoTermoBolsa.IdIndice = idIndice;
            operacaoTermoBolsa.DataVencimento = dataVencimento;
            operacaoTermoBolsa.Save();
        }

        //Atualiza StatusRealTime para executar***********
        cliente = new Cliente();
        cliente.SetaFlagRealTime(idCliente, (byte)StatusRealTimeCliente.Executar);
        //************************************************
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        SalvarNovo();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete") 
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdOperacao");
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idOperacao = Convert.ToInt32(keyValuesId[i]);

                OperacaoBolsa operacaoBolsa = new OperacaoBolsa();
                if (operacaoBolsa.LoadByPrimaryKey(idOperacao)) {
                    int idCliente = operacaoBolsa.IdCliente.Value;

                    //
                    OperacaoBolsa operacaoBolsaClone = (OperacaoBolsa)Utilitario.Clone(operacaoBolsa);
                    //

                    operacaoBolsa.MarkAsDeleted();
                    operacaoBolsa.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de OperacaoBolsa - Operacao: Delete OperacaoBolsa: " + idOperacao + UtilitarioWeb.ToString(operacaoBolsaClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion

                    //Atualiza StatusRealTime para executar***********
                    Cliente cliente = new Cliente();
                    cliente.SetaFlagRealTime(idCliente, (byte)StatusRealTimeCliente.Executar);
                    //************************************************
                }
            }
        }
        else if (e.Parameters == "btnObservacao")
        {
            int idOperacao = Convert.ToInt32(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "IdOperacao"));
            TextBox textObservacao = gridCadastro.FindEditFormTemplateControl("textObservacao") as TextBox;

            OperacaoBolsa operacaoBolsaUpdate = new OperacaoBolsa();
            if (operacaoBolsaUpdate.LoadByPrimaryKey(idOperacao))
            {
                operacaoBolsaUpdate.Observacao = textObservacao.Text;
                operacaoBolsaUpdate.Save();

                #region Log do Processo
                HistoricoLog historicoLog = new HistoricoLog();
                historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                "Cadastro de OperacaoBolsa - Operacao: Update OperacaoBolsa: " + idOperacao + "Nova Observação: " + textObservacao.Text,
                                                HttpContext.Current.User.Identity.Name,
                                                UtilitarioWeb.GetIP(Request),
                                                "",
                                                HistoricoLogOrigem.Outros);
                #endregion
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing) {
            ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
            e.Properties["cpBtnEditCodigoCliente"] = btnEditCodigoCliente.ClientID;

            //Busca do web.config o flag de multi-conta
            //bool multiConta = Convert.ToBoolean(WebConfig.AppSettings.MultiConta);
            bool multiConta = ParametrosConfiguracaoSistema.Outras.MultiConta;
            //
            e.Properties["cpMultiConta"] = multiConta.ToString();

        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_InitNewRow(object sender, ASPxDataInitNewRowEventArgs e) {
        //Valores default do form
        e.NewValues["PercentualDesconto"] = "0";
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("btnEditCodigoCliente");
        base.gridCadastro_PreRender(sender, e);

        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        Label labelFiltroHeader = gridCadastro.FindStatusBarTemplateControl("labelFiltroHeader") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");

        if (btnEditCodigoClienteFiltro != null && btnEditCodigoClienteFiltro.Text != "") {
            texto.Append(" Cliente: ").Append(btnEditCodigoClienteFiltro.Text);
        }
        if (textDataInicio.Text != "") {
            if (texto.Length > 0) {
                texto.Append(" |");
            }
            texto.Append(" Data Início >= ").Append(textDataInicio.Text.Substring(0, 10));
        }
        if (textDataFim.Text != "") {
            if (texto.Length > 0) {
                texto.Append(" |");
            }
            texto.Append(" Data Fim <= ").Append(textDataFim.Text.Substring(0, 10));
        }
        if (dropTipoMercado.SelectedIndex > 0)
        {
            if (texto.Length > 0)
            {
                texto.Append(" |");
            }
            texto.Append(" Mercado = ").Append(dropTipoMercado.SelectedItem.Text);
        }
        if (textCdAtivo.Text != "") {
            if (texto.Length > 0) {
                texto.Append(" |");
            }
            texto.Append(" Ativo Like %").Append(textCdAtivo.Text).Append("%");
        }
        if (dropTipoOperacaoFiltro.SelectedIndex > -1)
        {
            if (texto.Length > 0)
            {
                texto.Append(" |");
            }
            texto.Append(" Tipo = ").Append(dropTipoOperacaoFiltro.Text);
        }

        labelFiltro.Text = texto.ToString();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e) {
        if (e.Column.FieldName == "TipoOperacao") {
            if (Convert.ToByte(gridCadastro.GetRowValues(e.VisibleRowIndex, "Origem")) == OrigemOperacaoBolsa.AberturaEmprestimo) {
                if (e.Value.ToString() == "DE") {
                    e.DisplayText = "Aber.Empréstimo(+)";
                }
                else {
                    e.DisplayText = "Aber.Empréstimo(-)";
                }
            }
            else if (Convert.ToByte(gridCadastro.GetRowValues(e.VisibleRowIndex, "Origem")) == OrigemOperacaoBolsa.LiquidacaoEmprestimo) {
                if (e.Value.ToString() == "DE") {
                    e.DisplayText = "Liq.Empréstimo(+)";
                }
                else {
                    e.DisplayText = "Liq.Empréstimo(-)";
                }
            }
            else if (Convert.ToByte(gridCadastro.GetRowValues(e.VisibleRowIndex, "Origem")) == OrigemOperacaoBolsa.LiquidacaoTermo) {
                if (e.Value.ToString() == "DE") {
                    e.DisplayText = "Liq.Termo(+)";
                }
                else {
                    e.DisplayText = "Liq.Termo(-)";
                }
            }
            else if (Convert.ToByte(gridCadastro.GetRowValues(e.VisibleRowIndex, "Origem")) == OrigemOperacaoBolsa.VencimentoOpcao) {
                if (e.Value.ToString() == "DE") {
                    e.DisplayText = "Vcto Opção(+)";
                }
                else {
                    e.DisplayText = "Vcto Opção(-)";
                }
            }
            else if (Convert.ToByte(gridCadastro.GetRowValues(e.VisibleRowIndex, "Origem")) == OrigemOperacaoBolsa.TransferenciaCorretora) {
                if (e.Value.ToString() == "DE") {
                    e.DisplayText = "Transferência(+)";
                }
                else {
                    e.DisplayText = "Transferência(-)";
                }
            }
            else if (Convert.ToByte(gridCadastro.GetRowValues(e.VisibleRowIndex, "Origem")) == OrigemOperacaoBolsa.ExercicioOpcaoCompra ||
                     Convert.ToByte(gridCadastro.GetRowValues(e.VisibleRowIndex, "Origem")) == OrigemOperacaoBolsa.ExercicioOpcaoVenda) {
                if (e.Value.ToString() == "DE" || e.Value.ToString() == "CD" || e.Value.ToString() == "C") {
                    e.DisplayText = "Exercício-Compra";
                }
                else {
                    e.DisplayText = "Exercício-Venda";
                }
            }
        }
    }

    protected void EsDSTrader_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        TraderCollection coll = new TraderCollection();
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dropConta_Callback(object source, CallbackEventArgsBase e)
    {
        if (!ParametrosConfiguracaoSistema.Outras.MultiConta)
            return;

        ASPxComboBox dropConta = source as ASPxComboBox;
        dropConta.DataBind();
    }

    protected void EsDSContaCorrente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        if (!ParametrosConfiguracaoSistema.Outras.MultiConta)
        {
            e.Collection = new ContaCorrenteCollection();
            return;
        }

        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;

        if (btnEditCodigoCarteira != null && btnEditCodigoCarteira.Text != "")
        {
            int idCliente = Convert.ToInt32(btnEditCodigoCarteira.Text);

            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IdPessoa);
            if (cliente.LoadByPrimaryKey(campos, idCliente))
            {
                ContaCorrenteCollection coll = new ContaCorrenteCollection();

                coll.Query.Where(coll.Query.IdPessoa.Equal(cliente.IdPessoa.Value) | coll.Query.IdCliente.Equal(idCliente));
                coll.Query.OrderBy(coll.Query.Numero.Ascending);
                coll.Query.Load();

                // Assign the esDataSourcSelectEvenArgs Collection property
                e.Collection = coll;
            }

        }
        else
        {
            // Assign the esDataSourcSelectEvenArgs Collection property
            e.Collection = new ContaCorrenteCollection();
        }
    }
}