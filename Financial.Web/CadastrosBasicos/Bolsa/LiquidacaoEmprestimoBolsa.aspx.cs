﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using Financial.Investidor;
using Financial.ContaCorrente;
using System.Drawing;
using Financial.ContaCorrente.Enums;
using Financial.Security;
using Financial.Fundo;
using Financial.InvestidorCotista;
using Financial.InvestidorCotista.Enums;
using Financial.Investidor.Enums;
using Financial.Common.Enums;
using Financial.Bolsa;
using Financial.Bolsa.Enums;
using DevExpress.Web.Data;
using Financial.Web.Common;
using EntitySpaces.Interfaces;
using System.Threading;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_LiquidacaoEmprestimoBolsa : Financial.Web.Common.CadastroBasePage
{
    private int selectedIndex;

    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasFiltro = true;
        this.HasPopupCliente = true;
        this.HasPanelFieldsLoading = true;
        base.Page_Load(sender, e);

        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
                          new List<string>(new string[] { LiquidacaoEmprestimoBolsaMetadata.ColumnNames.TipoLiquidacao }));

        this.gridCadastro.ClientSideEvents.RowDblClick = "function(s, e) {selectedIndex = e.visibleIndex; gridCadastro.GetRowValues(e.visibleIndex, 'TipoLiquidacao', OpenPopup); }";
    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSLiquidacaoEmprestimoBolsa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
        LiquidacaoEmprestimoBolsaQuery liquidacaoEmprestimoBolsaQuery = new LiquidacaoEmprestimoBolsaQuery("L");
        ClienteQuery clienteQuery = new ClienteQuery("C");
        OperacaoEmprestimoBolsaQuery operacaoEmprestimoBolsaQuery = new OperacaoEmprestimoBolsaQuery("O");

        liquidacaoEmprestimoBolsaQuery.Select(liquidacaoEmprestimoBolsaQuery, clienteQuery.Apelido.As("Apelido"), operacaoEmprestimoBolsaQuery.DataRegistro, operacaoEmprestimoBolsaQuery.PontaEmprestimo, operacaoEmprestimoBolsaQuery.TaxaOperacao, operacaoEmprestimoBolsaQuery.Quantidade.As("QuantidadeOperacao"), operacaoEmprestimoBolsaQuery.Pu.As("PUOperacao"), operacaoEmprestimoBolsaQuery.Valor.As("ValorOperacao"));
        liquidacaoEmprestimoBolsaQuery.InnerJoin(clienteQuery).On(liquidacaoEmprestimoBolsaQuery.IdCliente == clienteQuery.IdCliente);
        liquidacaoEmprestimoBolsaQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        liquidacaoEmprestimoBolsaQuery.InnerJoin(operacaoEmprestimoBolsaQuery).On(liquidacaoEmprestimoBolsaQuery.IdOperacao == operacaoEmprestimoBolsaQuery.IdOperacao);

        liquidacaoEmprestimoBolsaQuery.Where(permissaoClienteQuery.IdUsuario.Equal(idUsuario));

        if (!String.IsNullOrEmpty(btnEditCodigoClienteFiltro.Text))
        {
            liquidacaoEmprestimoBolsaQuery.Where(liquidacaoEmprestimoBolsaQuery.IdCliente.Equal(Convert.ToInt32(btnEditCodigoClienteFiltro.Text)));
        }

        if (!String.IsNullOrEmpty(textDataInicio.Text))
        {
            liquidacaoEmprestimoBolsaQuery.Where(liquidacaoEmprestimoBolsaQuery.Data.GreaterThanOrEqual(textDataInicio.Text));
        }

        if (!String.IsNullOrEmpty(textDataFim.Text))
        {
            liquidacaoEmprestimoBolsaQuery.Where(liquidacaoEmprestimoBolsaQuery.Data.LessThanOrEqual(textDataFim.Text));
        }

        if (!String.IsNullOrEmpty(textCdAtivo.Text))
        {
            liquidacaoEmprestimoBolsaQuery.Where(liquidacaoEmprestimoBolsaQuery.CdAtivoBolsa.Like("%" + textCdAtivo.Text + "%"));
        }

        liquidacaoEmprestimoBolsaQuery.OrderBy(liquidacaoEmprestimoBolsaQuery.Data.Descending);

        LiquidacaoEmprestimoBolsaCollection coll = new LiquidacaoEmprestimoBolsaCollection();
        coll.Load(liquidacaoEmprestimoBolsaQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSPosicaoEmprestimoBolsa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxComboBox dropAgente = gridCadastro.FindEditFormTemplateControl("dropAgente") as ASPxComboBox;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;

        if (btnEditCodigoCliente != null && dropAgente != null && textData != null)
        {
            if (btnEditCodigoCliente.Text == "" || dropAgente.SelectedIndex == -1 || string.IsNullOrEmpty(textData.Text))
            {
                e.Collection = new PosicaoEmprestimoBolsaCollection();
                return;
            }
        }
        else
        {
            e.Collection = new PosicaoEmprestimoBolsaCollection();
            return;
        }

        Cliente cliente = new Cliente();
        List<esQueryItem> campos = new List<esQueryItem>();
        campos.Add(cliente.Query.DataDia);
        cliente.LoadByPrimaryKey(campos, Convert.ToInt32(btnEditCodigoCliente.Text));
        DateTime data = cliente.DataDia.Value;

        PosicaoEmprestimoBolsaCollection coll = new PosicaoEmprestimoBolsaCollection();

        coll.Query.Where(coll.Query.Quantidade.GreaterThan(0)
                         & coll.Query.IdCliente.Equal(Convert.ToInt32(btnEditCodigoCliente.Text))
                         & coll.Query.IdAgente.Equal(Convert.ToInt32(dropAgente.SelectedItem.Value))
                         & coll.Query.PermiteDevolucaoAntecipada.Equal("S")
                         & coll.Query.DataInicialDevolucaoAntecipada.LessThanOrEqual(Convert.ToDateTime(textData.Text)));

        coll.Query.OrderBy(coll.Query.PontaEmprestimo.Ascending, coll.Query.CdAtivoBolsa.Ascending, coll.Query.DataVencimento.Descending);

        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ClienteCollection coll = new ClienteCollection();
        coll.BuscaClientesComAcesso(Context.User.Identity.Name);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSAgenteMercado_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        AgenteMercadoCollection coll = new AgenteMercadoCollection();
        coll.Query.Where(coll.Query.FuncaoCustodiante.Equal("S"));
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSAtivoBolsa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        AtivoBolsaCollection coll = new AtivoBolsaCollection();
        coll.Query.Where(coll.Query.TipoMercado.Equal(Convert.ToString(TipoMercadoBolsa.MercadoVista)));
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void panelEdicao_Load(object sender, EventArgs e)
    {
        UtilitarioGrid.SetaCorCombosPopup(sender as Control);

        if (gridCadastro.IsEditing && gridCadastro.IsNewRowEditing)
        {
            Label labelQuantidade = gridCadastro.FindEditFormTemplateControl("labelQuantidade") as Label;
            ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
            labelQuantidade.Visible = false;
            textQuantidade.Visible = false;
            LinkButton btnOK = gridCadastro.FindEditFormTemplateControl("btnOK") as LinkButton;
            btnOK.Visible = false;
        }
        else if (gridCadastro.IsEditing && gridCadastro.IsNewRowEditing)
        {
            LinkButton btnPosicaoEmprestimoBolsa = gridCadastro.FindEditFormTemplateControl("btnPosicaoEmprestimoBolsa") as LinkButton;
            btnPosicaoEmprestimoBolsa.Visible = false;
        }

        bool travaPainel = false;
        if (gridCadastro.IsEditing && !gridCadastro.IsNewRowEditing && Convert.ToString(Session["FormLoad"]) != "S")
        {
            Session["FormLoad"] = "S";
            Label labelEdicao = gridCadastro.FindEditFormTemplateControl("labelEdicao") as Label;

            DateTime data = Convert.ToDateTime(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "Data"));
            int idCliente = Convert.ToInt32(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "IdCliente"));

            Cliente cliente = new Cliente();
            if (cliente.LoadByPrimaryKey(idCliente))
            {
                byte status = cliente.Status.Value;
                if (status == (byte)StatusCliente.Divulgado)
                {
                    travaPainel = true;
                    labelEdicao.Text = "Cliente está fechado. Dados não podem ser alterados.";
                }
                else if (DateTime.Compare(cliente.DataDia.Value, data) > 0)
                {
                    travaPainel = true;
                    labelEdicao.Text = "Data do lançamento da liquidação anterior à data atual do cliente. Dados não podem ser alterados.";
                }

            }
        }

        if (travaPainel)
        {
            LinkButton btnOK = gridCadastro.FindEditFormTemplateControl("btnOK") as LinkButton;
            btnOK.Visible = false;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        if (!gridCadastro.IsEditing)
        { //Testa deleção
            #region Delete

            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(LiquidacaoEmprestimoBolsaMetadata.ColumnNames.IdCliente);
            List<object> keyData = gridCadastro.GetSelectedFieldValues(LiquidacaoEmprestimoBolsaMetadata.ColumnNames.Data);
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idCliente = Convert.ToInt32(keyValuesId[i]);
                DateTime data = Convert.ToDateTime(keyData[i]);

                Cliente cliente = new Cliente();
                if (cliente.LoadByPrimaryKey(idCliente))
                {
                    DateTime dataDia = cliente.DataDia.Value;
                    byte status = cliente.Status.Value;
                    string clienteDescricao = cliente.Apelido + " (" + cliente.IdCliente.ToString() + ")";
                    if (status == (byte)StatusCliente.Divulgado)
                    {
                        e.Result = "Cliente " + clienteDescricao + " está fechado! Deleção cancelada.";
                        return;
                    }
                    else if (DateTime.Compare(dataDia, data) > 0)
                    {
                        e.Result = "Lançamento com data " + data.ToString().Substring(0, 10) + " anterior à data do cliente " + clienteDescricao + "! Deleção cancelada.";
                        return;
                    }
                }
            }
            #endregion
        }
        else
        {
            ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
            ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
            ASPxComboBox dropAgente = gridCadastro.FindEditFormTemplateControl("dropAgente") as ASPxComboBox;
            int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);

            Cliente cliente = new Cliente();
            if (cliente.LoadByPrimaryKey(idCliente))
            {
                byte status = cliente.Status.Value;
                if (status == (byte)StatusCliente.Divulgado)
                {
                    e.Result = "Cliente está fechado! Operação não pode ser realizada.";
                    return;
                }
            }

            #region Campos obrigatórios
            List<Control> controles = new List<Control>();
            controles.Add(btnEditCodigoCliente);
            controles.Add(textData);
            controles.Add(dropAgente);

            if (base.TestaObrigatorio(controles) != "")
            {
                e.Result = "Campos com * são obrigatórios!";
                return;
            }
            #endregion

            if (gridCadastro.IsEditing && !gridCadastro.IsNewRowEditing)
            {
                TextBox hiddenIdPosicao = gridCadastro.FindEditFormTemplateControl("hiddenIdPosicao") as TextBox;

                int idPosicao = Convert.ToInt32(hiddenIdPosicao.Text);
                DateTime data = Convert.ToDateTime(textData.Text);

                PosicaoEmprestimoBolsaAbertura posicaoEmprestimoBolsaAbertura = new PosicaoEmprestimoBolsaAbertura();
                if (posicaoEmprestimoBolsaAbertura.LoadByPrimaryKey(idPosicao, data))
                {
                    ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;

                    List<Control> controlesAux = new List<Control>();
                    controlesAux.Add(btnEditCodigoCliente);
                    controlesAux.Add(dropAgente);
                    controlesAux.Add(textQuantidade);

                    if (base.TestaObrigatorio(controlesAux) != "")
                    {
                        e.Result = "Campos com * são obrigatórios!";
                        return;
                    }

                    if (posicaoEmprestimoBolsaAbertura.Quantidade.Value < Convert.ToDecimal(textQuantidade.Text))
                    {
                        e.Result = "Quantidade a ser antecipada é maior que a quantidade em posição! Operação não pode ser realizada.";
                        return;
                    }
                }
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErroPosicao_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        ASPxSpinEdit textQuantidade = popupPosicaoEmprestimoBolsa.FindControl("textQuantidade") as ASPxSpinEdit;

        if (textQuantidade.Text == "")
        {
            e.Result = "A Quantidade deve ser preenchida.";
            return;
        }

        if (Convert.ToInt32(textQuantidade.Text) <= 0)
        {
            e.Result = "A Quantidade deve ser maior do que zero.";
            return;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string nome = "";
        string resultado = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "")
            {
                if (cliente.IsAtivo)
                {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name))
                    {
                        nome = cliente.str.Apelido;
                        DateTime dataDia = cliente.DataDia.Value;
                        byte status = cliente.Status.Value;

                        if (status == (byte)StatusCliente.Divulgado && gridCadastro.IsEditing) 
                        {
                            resultado = cliente.str.Apelido + "|status_closed";
                        }
                        else
                        {
                            resultado = nome + "|" + dataDia.ToString().Substring(0, 10) + "|" + Thread.CurrentThread.CurrentCulture.Name;
                        }
                    }
                    else
                    {
                        resultado = "no_access";
                    }
                }
                else
                {
                    resultado = "no_active";
                }
            }
        }
        e.Result = resultado;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="selectedIndex"></param>
    protected void InsertLiquidacao(int selectedIndex)
    {
        ASPxPopupControl popupPosicaoEmprestimoBolsa = this.FindControl("popupPosicaoEmprestimoBolsa") as ASPxPopupControl;
        ASPxGridView gridPosicaoEmprestimoBolsa = popupPosicaoEmprestimoBolsa.FindControl("gridPosicaoEmprestimoBolsa") as ASPxGridView;
        ASPxSpinEdit textQuantidade = popupPosicaoEmprestimoBolsa.FindControl("textQuantidade") as ASPxSpinEdit;
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxComboBox dropAgente = gridCadastro.FindEditFormTemplateControl("dropAgente") as ASPxComboBox;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;

        int idPosicao = Convert.ToInt32(gridPosicaoEmprestimoBolsa.GetRowValues(selectedIndex, LiquidacaoEmprestimoBolsaMetadata.ColumnNames.IdPosicao));
        string cdAtivoBolsa = Convert.ToString(gridPosicaoEmprestimoBolsa.GetRowValues(selectedIndex, LiquidacaoEmprestimoBolsaMetadata.ColumnNames.CdAtivoBolsa));
        decimal quantidade = Convert.ToDecimal(textQuantidade.Text);
        DateTime data = Convert.ToDateTime(textData.Text);

        //Checa qtde já antecipada vs quantidade de abertura
        PosicaoEmprestimoBolsaAbertura posicaoEmprestimoBolsaAbertura = new PosicaoEmprestimoBolsaAbertura();
        decimal quantidadePosicaoAbertura = posicaoEmprestimoBolsaAbertura.RetornaQuantidadePosicao(idPosicao, data);
        LiquidacaoEmprestimoBolsa liquidacaoEmprestimoBolsaChecar = new LiquidacaoEmprestimoBolsa();
        decimal quantidadeLiquidada = liquidacaoEmprestimoBolsaChecar.RetornaTotalLiquidado(idPosicao, data);

        if (quantidade > (quantidadePosicaoAbertura - quantidadeLiquidada))
        {
            quantidade = quantidadePosicaoAbertura - quantidadeLiquidada;
        }
        //

        if (quantidade > 0)
        {
            int numeroContrato = 0;
            if (gridPosicaoEmprestimoBolsa.GetRowValues(selectedIndex, LiquidacaoEmprestimoBolsaMetadata.ColumnNames.NumeroContrato).ToString() != "")
            {
                numeroContrato = Convert.ToInt32(gridPosicaoEmprestimoBolsa.GetRowValues(selectedIndex, LiquidacaoEmprestimoBolsaMetadata.ColumnNames.NumeroContrato));
            }

            int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);

            //Se em PosicaoEmprestimoBolsa tiver o IdOperacao, pega este Id
            int? idOperacao = null;
            PosicaoEmprestimoBolsa posicaoEmprestimoBolsa = new PosicaoEmprestimoBolsa();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(posicaoEmprestimoBolsa.Query.IdOperacao);
            if (posicaoEmprestimoBolsa.LoadByPrimaryKey(idPosicao))
            {
                idOperacao = posicaoEmprestimoBolsa.IdOperacao;
            }

            LiquidacaoEmprestimoBolsa liquidacaoEmprestimoBolsa = new LiquidacaoEmprestimoBolsa();
            liquidacaoEmprestimoBolsa.IdPosicao = idPosicao;
            liquidacaoEmprestimoBolsa.IdOperacao = idOperacao;
            liquidacaoEmprestimoBolsa.IdCliente = idCliente;
            liquidacaoEmprestimoBolsa.CdAtivoBolsa = cdAtivoBolsa;
            liquidacaoEmprestimoBolsa.IdAgente = Convert.ToInt32(dropAgente.SelectedItem.Value);
            liquidacaoEmprestimoBolsa.Data = data;
            liquidacaoEmprestimoBolsa.Quantidade = quantidade;
            liquidacaoEmprestimoBolsa.NumeroContrato = numeroContrato;
            liquidacaoEmprestimoBolsa.TipoLiquidacao = (byte)TipoLiquidacaoEmprestimoBolsa.Antecipacao;
            liquidacaoEmprestimoBolsa.Fonte = (byte)FonteLiquidacaoEmprestimoBolsa.Manual;
            liquidacaoEmprestimoBolsa.ValorIR = 0;
            liquidacaoEmprestimoBolsa.Valor = 0; //Ajustar depois!!!!!
            liquidacaoEmprestimoBolsa.ValorLiquidacao = 0; //Ajustar depois!!!!!

            liquidacaoEmprestimoBolsa.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de LiquidacaoEmprestimoBolsa - Operacao: Insert LiquidacaoEmprestimoBolsa: " + liquidacaoEmprestimoBolsa.IdLiquidacao + UtilitarioWeb.ToString(liquidacaoEmprestimoBolsa),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion


            //Atualiza StatusRealTime para executar***********
            Cliente cliente = new Cliente();
            cliente.SetaFlagRealTime(idCliente, (byte)StatusRealTimeCliente.Executar);
            //************************************************
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CancelRowEditing(object sender, ASPxStartRowEditingEventArgs e)
    {
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        int idLiquidacao = (int)e.Keys[0];

        ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;

        LiquidacaoEmprestimoBolsa liquidacaoEmprestimoBolsa = new LiquidacaoEmprestimoBolsa();
        if (liquidacaoEmprestimoBolsa.LoadByPrimaryKey(idLiquidacao))
        {
            liquidacaoEmprestimoBolsa.Quantidade = Convert.ToDecimal(textQuantidade.Text);
            liquidacaoEmprestimoBolsa.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de LiquidacaoEmprestimoBolsa - Operacao: Update LiquidacaoEmprestimoBolsa: " + idLiquidacao + UtilitarioWeb.ToString(liquidacaoEmprestimoBolsa),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion

            //Atualiza StatusRealTime para executar***********
            Cliente cliente = new Cliente();
            cliente.SetaFlagRealTime(liquidacaoEmprestimoBolsa.IdCliente.Value, (byte)StatusRealTimeCliente.Executar);
            //************************************************
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(LiquidacaoEmprestimoBolsaMetadata.ColumnNames.IdLiquidacao);
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idLiquidacao = Convert.ToInt32(keyValuesId[i]);

                LiquidacaoEmprestimoBolsa liquidacaoEmprestimoBolsa = new LiquidacaoEmprestimoBolsa();
                if (liquidacaoEmprestimoBolsa.LoadByPrimaryKey(idLiquidacao))
                {
                    int idCliente = liquidacaoEmprestimoBolsa.IdCliente.Value;
                    //
                    LiquidacaoEmprestimoBolsa liquidacaoEmprestimoBolsaClone = (LiquidacaoEmprestimoBolsa)Utilitario.Clone(liquidacaoEmprestimoBolsa);
                    //
                    liquidacaoEmprestimoBolsa.MarkAsDeleted();
                    liquidacaoEmprestimoBolsa.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de LiquidacaoEmprestimoBolsa - Operacao: Delete LiquidacaoEmprestimoBolsa: " + idLiquidacao + UtilitarioWeb.ToString(liquidacaoEmprestimoBolsaClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion

                    //Atualiza StatusRealTime para executar***********
                    Cliente cliente = new Cliente();
                    cliente.SetaFlagRealTime(idCliente, (byte)StatusRealTimeCliente.Executar);
                    //************************************************
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e)
    {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing)
        {
            ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
            e.Properties["cpBtnEditCodigoCliente"] = btnEditCodigoCliente.ClientID;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e)
    {
        this.FocaCampoGrid("btnEditCodigoCliente");
        base.gridCadastro_PreRender(sender, e);

        ASPxDateEdit textDataInicio = popupFiltro.FindControl("textDataInicio") as ASPxDateEdit;
        ASPxDateEdit textDataFim = popupFiltro.FindControl("textDataFim") as ASPxDateEdit;
        ASPxSpinEdit btnEditCodigoClienteFiltro = popupFiltro.FindControl("btnEditCodigoClienteFiltro") as ASPxSpinEdit;

        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        Label labelFiltroHeader = gridCadastro.FindStatusBarTemplateControl("labelFiltroHeader") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");
        if (btnEditCodigoClienteFiltro != null && btnEditCodigoClienteFiltro.Text != "")
        {
            texto.Append(" Cliente: ").Append(btnEditCodigoClienteFiltro.Text);
        }
        if (textDataInicio.Text != "")
        {
            if (texto.Length > 0)
            {
                texto.Append(" |");
            }
            texto.Append(" Data Início >= ").Append(textDataInicio.Text.Substring(0, 10));
        }
        if (textDataFim.Text != "")
        {
            if (texto.Length > 0)
            {
                texto.Append(" |");
            }
            texto.Append(" Data Fim <= ").Append(textDataFim.Text.Substring(0, 10));
        }
        if (textCdAtivo.Text != "")
        {
            if (texto.Length > 0)
            {
                texto.Append(" |");
            }
            texto.Append(" Ativo Like %").Append(textCdAtivo.Text).Append("%");
        }

        labelFiltro.Text = texto.ToString();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridPosicaoEmprestimoBolsa_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "") //Usado apenas na hora q abre inicialmente a popup, para dar refresh nas posições
        {
            gridPosicaoEmprestimoBolsa.DataBind();
        }
        else
        {
            selectedIndex = Convert.ToInt32(e.Parameters);
            InsertLiquidacao(selectedIndex);
            gridPosicaoEmprestimoBolsa.DataBind();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridPosicaoEmprestimoBolsa_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        UtilitarioGrid.SetaCoresGrid(e);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridPosicaoEmprestimoBolsa_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "QuantidadeAntecipada")
        {
            ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;

            int idPosicao = Convert.ToInt32(e.GetListSourceFieldValue(LiquidacaoEmprestimoBolsaMetadata.ColumnNames.IdPosicao));
            LiquidacaoEmprestimoBolsa liquidacaoEmprestimoBolsa = new LiquidacaoEmprestimoBolsa();
            decimal quantidadeLiquidada = liquidacaoEmprestimoBolsa.RetornaTotalLiquidado(idPosicao, Convert.ToDateTime(textData.Text));

            e.Value = quantidadeLiquidada;
        }
        else if (e.Column.FieldName == "PontaEmprestimoDescricao")
        {
            byte tipoEmprestimo = Convert.ToByte(e.GetListSourceFieldValue("PontaEmprestimo"));
            //
            e.Value = tipoEmprestimo == (byte)PontaEmprestimoBolsa.Tomador ? "Tomador" : "Doador";
        }
    }
}