using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Investidor;
using Financial.Fundo;
using Financial.Bolsa;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_TabelaCustosBolsa : Financial.Web.Common.CadastroBasePage {
    #region DataSources
    protected void EsDSTabelaCustosBolsa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        TabelaCustosBolsaCollection coll = new TabelaCustosBolsaCollection();

        coll.Query.OrderBy(coll.Query.DataReferencia.Ascending, coll.Query.IdTabela.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) {
        if (e.Column.FieldName == "CompositeKey") {
            //Compose a primary key value
            string dataReferencia = Convert.ToString(e.GetListSourceFieldValue("DataReferencia"));
            string idTabela = Convert.ToString(e.GetListSourceFieldValue("IdTabela"));
            e.Value = dataReferencia + "|" + idTabela;
        }
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        TabelaCustosBolsa tabelaCustosBolsa = new TabelaCustosBolsa();
        string compositeKey = Convert.ToString(e.Keys[0]);
        char[] delimitador = { '|' };
        string[] chaves = compositeKey.Split(delimitador);
        DateTime dataReferencia = Convert.ToDateTime(chaves[0]);
        short idTabela = Convert.ToInt16(chaves[1]);

        if (tabelaCustosBolsa.LoadByPrimaryKey(dataReferencia, idTabela)) {
            tabelaCustosBolsa.Descricao = Convert.ToString(e.NewValues["Descricao"]);
            tabelaCustosBolsa.PercentualEmolumentoBolsa = Convert.ToDecimal(e.NewValues["PercentualEmolumentoBolsa"]);
            tabelaCustosBolsa.PercentualLiquidacaoCBLC = Convert.ToDecimal(e.NewValues["PercentualLiquidacaoCBLC"]);
            tabelaCustosBolsa.PercentualRegistroBolsa = Convert.ToDecimal(e.NewValues["PercentualRegistroBolsa"]);
            tabelaCustosBolsa.PercentualRegistroCBLC = Convert.ToDecimal(e.NewValues["PercentualRegistroCBLC"]);

            if (e.NewValues["MinimoBovespa"] != null) {
                tabelaCustosBolsa.MinimoBovespa = Convert.ToDecimal(e.NewValues["MinimoBovespa"]);
            }
            else {
                tabelaCustosBolsa.MinimoBovespa = null;
            }

            if (e.NewValues["MinimoCBLC"] != null) {
                tabelaCustosBolsa.MinimoCBLC = Convert.ToDecimal(e.NewValues["MinimoCBLC"]);
            }
            else {
                tabelaCustosBolsa.MinimoCBLC = null;
            }

            tabelaCustosBolsa.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de TabelaCustosBolsa - Operacao: Update TabelaCustosBolsa: " + dataReferencia + "; " + idTabela + UtilitarioWeb.ToString(tabelaCustosBolsa),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    new protected void gridCadastro_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e) {
        if (e.Column.FieldName != "MinimoBovespa" && e.Column.FieldName != "MinimoCBLC") {
            (e.Editor as ASPxTextEdit).ValidationSettings.RequiredField.IsRequired = true;
            (e.Editor as ASPxTextEdit).ValidationSettings.RequiredField.ErrorText = this.MsgCampoObrigatorio;
        }
    }
}