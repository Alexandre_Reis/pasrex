﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="GrupamentoBolsa.aspx.cs" Inherits="CadastrosBasicos_GrupamentoBolsa" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">    
 
     <link href="~/css/forms.css" rel="stylesheet" type="text/css" />    
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">    
    var popup = true;
    document.onkeydown=onDocumentKeyDown;
    var operacao = '';
    
    function OnGetDataAtivoBolsa(data) {
        btnEditAtivoBolsa.SetValue(data);
        popupAtivoBolsa.HideWindow();
        ASPxCallback1.SendCallback(btnEditAtivoBolsa.GetValue());
        btnEditAtivoBolsa.Focus();
    }        
    </script>
</head>

<body>
    <form id="form1" runat="server">
    
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '') {
                alert(e.result);                              
            }
            else {
                if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new') {             
                    gridCadastro.UpdateEdit();
                }
                else {
                    callbackAdd.SendCallback();
                }    
            }
            
            operacao = '';
        }
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {
            alert('Operação feita com sucesso.');
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {
            if (e.result == '' && btnEditAtivoBolsa.GetValue() != null)
            {
                popupMensagemAtivoBolsa.SetContentHTML(document.getElementById('textMsgNaoExiste').value);
                popupMensagemAtivoBolsa.ShowAtElementByID(btnEditAtivoBolsa.name);
                btnEditAtivoBolsa.SetValue(''); 
                btnEditAtivoBolsa.Focus();                 
            }            
        }        
        "/>
    </dxcb:ASPxCallback>
        
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">
    
    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Lançamentos de Grupamento/Desdobramento (Bolsa)"></asp:Label>
    </div>
        
    <div id="mainContent">
                       
            <dxpc:ASPxPopupControl ID="popupFiltro" AllowDragging="true" PopupElementID="popupFiltro" EnableClientSideAPI="True"                    
                            PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" 
                            Width="400" Left="250" Top="70" HeaderText="Filtros adicionais para consulta" runat="server"
                            HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">            
                <ContentCollection><dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                    
                    <table>        
                    <tr>
                    <td>                
                        <asp:Label ID="labelDataInicio" runat="server" CssClass="labelNormal" Text="Início:"></asp:Label>
                    </td>    
                                        
                    <td>
                        <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" />                        
                    </td>  
                    
                    <td>
                        <asp:Label ID="labelDataFim" runat="server" CssClass="labelNormal" Text="Fim:"/>
                    </td>
                    <td>
                        <dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim" />
                    </td>                                                                            
                    </tr>
                    
                    <tr>
                        <td>
                            <asp:Label ID="labelCdAtivo" runat="server" CssClass="labelNormal" Text="Ativo:"></asp:Label>
                        </td>                
                        <td colspan="3">
                            <asp:TextBox ID="textCdAtivo" runat="server" CssClass="textNormal_5"/>
                        </td>    
                        </tr>
                    </table>        
                    
                    <div class="linkButton linkButtonNoBorder" style="margin-top:20px">              
                    <asp:LinkButton ID="btnOKFilter" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnOK" OnClientClick="popupFiltro.Hide(); gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal7" runat="server" Text="Aplicar"/><div></div></asp:LinkButton>
                    <asp:LinkButton ID="btnFilterCancel" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnFilterCancel" OnClientClick="OnClearFilterClick_FiltroDatas(); return false;"><asp:Literal ID="Literal8" runat="server" Text="Limpar"/><div></div></asp:LinkButton>
                    </div>                    
                </dxpc:PopupControlContentControl></ContentCollection>                             
            </dxpc:ASPxPopupControl>        
        
            <div class="linkButton" >               
               <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal1" runat="server" Text="Novo"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;"><asp:Literal ID="Literal2" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnFilter" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnFilter" OnClientClick="return OnButtonClick_FiltroDatas()"><asp:Literal ID="Literal3" runat="server" Text="Filtro"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal4" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal5" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal6" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
            </div>
                        
            <div class="divDataGrid">            
                <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                    KeyFieldName="IdGrupamento" DataSourceID="EsDSGrupamentoBolsa"
                    OnCustomCallback="gridCadastro_CustomCallback"
                    OnRowUpdating="gridCadastro_RowUpdating"
                    OnRowInserting="gridCadastro_RowInserting"
                    OnCustomJSProperties="gridCadastro_CustomJSProperties"
                    OnPreRender="gridCadastro_PreRender"
                    OnBeforeGetCallbackResult="gridCadastro_PreRender" >        
                        
                <Columns>           
                    <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                        <HeaderTemplate>
                            <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                        </HeaderTemplate>
                    </dxwgv:GridViewCommandColumn>
                    
                    <dxwgv:GridViewDataComboBoxColumn FieldName="TipoGrupamento" VisibleIndex="1" Width="15%" ExportWidth="150">
                    <PropertiesComboBox EncodeHtml="false">
                    <Items>
                        <dxe:ListEditItem Value="1" Text="<div title='Grupamento'>Grupamento</div>" />
                        <dxe:ListEditItem Value="2" Text="<div title='Desdobramento'>Desdobramento</div>" />                    
                    </Items>
                    </PropertiesComboBox>
                    </dxwgv:GridViewDataComboBoxColumn>
                    
                    <dxwgv:GridViewDataTextColumn FieldName="CdAtivoBolsa" Caption="Ativo" Width="15%" VisibleIndex="2">
                    </dxwgv:GridViewDataTextColumn>
                    
                    <dxwgv:GridViewDataDateColumn FieldName="DataLancamento" Caption="Lançamento" VisibleIndex="3" Width="10%"/>
                    
                    <dxwgv:GridViewDataDateColumn FieldName="DataEx" VisibleIndex="4" Width="10%"/>
                    
                    <dxwgv:GridViewDataDateColumn FieldName="DataReferencia" Caption="Referência" VisibleIndex="5" Width="10%"/>
                    
                    <dxwgv:GridViewDataSpinEditColumn FieldName="FatorQuantidade" VisibleIndex="6" Width="15%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                        <PropertiesSpinEdit DisplayFormatString="{0:#,##0.000000;(#,##0.0000);0.000000}"></PropertiesSpinEdit>
                    </dxwgv:GridViewDataSpinEditColumn>
                    
                    <dxwgv:GridViewDataTextColumn FieldName="FatorPU" VisibleIndex="7" Width="15%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.000000;(#,##0.0000);0.000000}"></PropertiesTextEdit>
                    </dxwgv:GridViewDataTextColumn>                                        
                </Columns>
                
                <Templates>
                <EditForm>                                        
                    <div class="editForm">
                        
                        <table>
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelTipoGrupamento" runat="server" AssociatedControlID="dropTipoGrupamento" CssClass="labelRequired" Text="Tipo:">
                                    </asp:Label>
                                </td>                                
                                <td>
                                    <dxe:ASPxComboBox ID="dropTipoGrupamento" runat="server" 
                                        ShowShadow="false" DropDownStyle="DropDownList" 
                                        CssClass="dropDownListCurto" Text='<%#Eval("TipoGrupamento")%>'>
                                    <Items>
                                    <dxe:ListEditItem Value="1" Text="Grupamento" />
                                    <dxe:ListEditItem Value="2" Text="Desdobramento" />
                                    </Items>                                                            
                                    </dxe:ASPxComboBox>           
                                </td>
                            </tr>
                            
                            <tr>                                        
                                <td  class="td_Label">
                                    <asp:Label ID="labelAtivo" runat="server" CssClass="labelRequired" Text="Ativo:"></asp:Label>                        
                                </td>
                                <td>
                                    <dxe:ASPxButtonEdit ID="btnEditAtivoBolsa" runat="server" Width="100%" CssClass="textAtivoCurto" MaxLength="25" 
                                                        ClientInstanceName="btnEditAtivoBolsa" Text="<%#Bind('CdAtivoBolsa')%>" >            
                                    <Buttons>
                                        <dxe:EditButton></dxe:EditButton>                
                                    </Buttons>           
                                    <ClientSideEvents                              
                                         ButtonClick="function(s, e) {popupAtivoBolsa.ShowAtElementByID(s.name);}" 
                                         LostFocus="function(s, e) {popupMensagemAtivoBolsa.HideWindow();
                                                                    if (btnEditAtivoBolsa.GetValue() != null)
                                                                    {                                                                                        
                                                                        ASPxCallback1.SendCallback(btnEditAtivoBolsa.GetValue());
                                                                    }
                                                                    }"
                                    />                    
                                    </dxe:ASPxButtonEdit>
                                </td>                                                               
                                
                                <td class="td_Label">
                                    <asp:Label ID="labelDataLancamento" runat="server" CssClass="labelRequired" Text="Lançamento:"></asp:Label>
                                </td>                                                        
                                <td>    
                                    <dxe:ASPxDateEdit ID="textDataLancamento" runat="server" ClientInstanceName="textDataLancamento" Value='<%#Eval("DataLancamento")%>'/>                                                    
                                </td>  
                            </tr>                                        
                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelDataEx" runat="server" CssClass="labelRequired" Text="Data Ex:"></asp:Label>
                                </td>                                                        
                                <td>             
                                    <dxe:ASPxDateEdit ID="textDataEx" runat="server" ClientInstanceName="textDataEx" Value='<%#Eval("DataEx")%>'/>           
                                </td>  
                                
                                <td class="td_Label">
                                    <asp:Label ID="labelDataReferencia" runat="server" CssClass="labelRequired" Text="Data Ref.:"></asp:Label>
                                </td>                                                        
                                <td>           
                                    <dxe:ASPxDateEdit ID="textDataReferencia" runat="server" ClientInstanceName="textDataReferencia" Value='<%#Eval("DataReferencia")%>'/>             
                                </td>  
                            </tr>
                            
                            <tr>     
                                <td class="td_Label">
                                    <asp:Label ID="labelFatorQuantidade" runat="server" AssociatedControlID="textFatorQuantidade" CssClass="labelRequired" Text="Fator Qtde:">
                                    </asp:Label>
                                </td>                                
                                <td>
                                    <dxe:ASPxSpinEdit ID="textFatorQuantidade" runat="server" ClientInstanceName="textFatorQuantidade" 
                                    Text='<%# Eval("FatorQuantidade") %>' NumberType="Float" MaxLength="20" DecimalPlaces="11" CssClass="textValor_5">
                                    </dxe:ASPxSpinEdit>                                
                                </td>
                                
                                <td class="td_Label">
                                    <asp:Label ID="labelFatorPU" runat="server" AssociatedControlID="textFatorPU" CssClass="labelRequired" Text="Fator PU:">
                                    </asp:Label>
                                </td>                                
                                <td>
                                    <dxe:ASPxSpinEdit ID="textFatorPU" runat="server" ClientInstanceName="textFatorPU" 
                                    Text='<%# Eval("FatorPU") %>' NumberType="Float" MaxLength="20" DecimalPlaces="15"
                                    CssClass="textValor_5">
                                    </dxe:ASPxSpinEdit>                                
                                </td>
                            </tr>
                                
                        </table>    
                    
                        <div class="linhaH"></div>
                        
                        <div class="linkButton linkButtonNoBorder popupFooter">
                            <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd" OnInit="btnOKAdd_Init"
                               OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal12" runat="server" Text="OK+"/><div></div></asp:LinkButton>

                            <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                            OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;" ><asp:Literal ID="Literal5" runat="server" Text="OK"/><div></div></asp:LinkButton>
                            <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel" OnClientClick="gridCadastro.CancelEdit(); return false;" ><asp:Literal ID="Literal9" runat="server" Text="Cancelar"/><div></div></asp:LinkButton>
                        </div>                        
                    </div>                                  
                </EditForm>
                
                <StatusBar>
                    <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" />
                </StatusBar>      
                
                </Templates>
                
                <SettingsPopup EditForm-Width="500px" />
                <SettingsCommandButton>
                    <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                </SettingsCommandButton>
                
            </dxwgv:ASPxGridView>            
            </div>
         
    </div>
    </div>
    </td></tr></table>
    </div>        
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" 
    LeftMargin = "50"
    RightMargin = "50"
    />
        
    <cc1:esDataSource ID="EsDSGrupamentoBolsa" runat="server" OnesSelect="EsDSGrupamentoBolsa_esSelect" />        
    <cc1:esDataSource ID="EsDSAtivoBolsa" runat="server" OnesSelect="EsDSAtivoBolsa_esSelect" />        
        
    </form>
</body>
</html>

