﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using Financial.Bolsa;
using Financial.RendaFixa;
using Financial.Bolsa.Enums;
using Financial.Web.Common;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Security;

public partial class CadastrosBasicos_ConversaoBolsa : CadastroBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasPopupAtivoBolsa = true;
        this.HasPopupAtivoBolsa2 = true;
        this.HasFiltro = true;
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSConversaoBolsa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ConversaoBolsaCollection coll = new ConversaoBolsaCollection();

        ASPxDateEdit textDataInicio = popupFiltro.FindControl("textDataInicio") as ASPxDateEdit;
        ASPxDateEdit textDataFim = popupFiltro.FindControl("textDataFim") as ASPxDateEdit;

        if (textDataInicio.Text != "") {
            coll.Query.Where(coll.Query.DataLancamento.GreaterThanOrEqual(textDataInicio.Text));
        }

        if (textDataFim.Text != "") {
            coll.Query.Where(coll.Query.DataLancamento.LessThanOrEqual(textDataFim.Text));
        }

        if (!String.IsNullOrEmpty(textCdAtivo.Text)) {
            coll.Query.Where(coll.Query.CdAtivoBolsa.Like("%" + textCdAtivo.Text + "%"));
        }

        if (!String.IsNullOrEmpty(textCdAtivoDestino.Text))
        {
            coll.Query.Where(coll.Query.CdAtivoBolsaDestino.Like("%" + textCdAtivoDestino.Text + "%"));
        }

        coll.Query.OrderBy(coll.Query.DataLancamento.Descending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSAtivoBolsa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        AtivoBolsaCollection coll = new AtivoBolsaCollection();
        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";
        ASPxButtonEdit btnEditAtivoBolsa = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa") as ASPxButtonEdit;
        ASPxDateEdit textDataLancamento = gridCadastro.FindEditFormTemplateControl("textDataLancamento") as ASPxDateEdit;
        ASPxDateEdit textDataEx = gridCadastro.FindEditFormTemplateControl("textDataEx") as ASPxDateEdit;
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        ASPxSpinEdit textFatorQuantidade = gridCadastro.FindEditFormTemplateControl("textFatorQuantidade") as ASPxSpinEdit;
        ASPxSpinEdit textFatorPU = gridCadastro.FindEditFormTemplateControl("textFatorPU") as ASPxSpinEdit;
        ASPxButtonEdit btnEditAtivoBolsa2 = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa2") as ASPxButtonEdit;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(btnEditAtivoBolsa);
        controles.Add(textDataLancamento);
        controles.Add(textDataEx);
        controles.Add(textDataReferencia);
        controles.Add(textFatorQuantidade);
        controles.Add(textFatorPU);
        controles.Add(btnEditAtivoBolsa2);

        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        AtivoBolsa ativoBolsa = new AtivoBolsa();
        if (!ativoBolsa.LoadByPrimaryKey(btnEditAtivoBolsa.Text.ToString())) {
            e.Result = "Ativo não existente!";
            return;
        }
        else {
            ativoBolsa = new AtivoBolsa();
            if (!ativoBolsa.LoadByPrimaryKey(btnEditAtivoBolsa2.Text.ToString())) {
                e.Result = "Ativo destino não existente!";
                return;
            }
        }

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string CdAtivoBolsa = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            AtivoBolsa ativoBolsa = new AtivoBolsa();
            ativoBolsa.LoadByPrimaryKey(Convert.ToString(e.Parameter));

            if (ativoBolsa.str.CdAtivoBolsa != "") {
                CdAtivoBolsa = ativoBolsa.str.CdAtivoBolsa;
            }
        }
        e.Result = CdAtivoBolsa;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string CdAtivoBolsa = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            AtivoBolsa ativoBolsa = new AtivoBolsa();
            ativoBolsa.LoadByPrimaryKey(Convert.ToString(e.Parameter));

            if (ativoBolsa.str.CdAtivoBolsa != "") {
                CdAtivoBolsa = ativoBolsa.str.CdAtivoBolsa;
            }
        }
        e.Result = CdAtivoBolsa;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        SalvarNovo();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOKAdd_Init(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    private void SalvarNovo() {
        ASPxButtonEdit btnEditAtivoBolsa = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa") as ASPxButtonEdit;
        ASPxButtonEdit btnEditAtivoDestino = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa2") as ASPxButtonEdit;
        ASPxDateEdit textDataLancamento = gridCadastro.FindEditFormTemplateControl("textDataLancamento") as ASPxDateEdit;
        ASPxDateEdit textDataEx = gridCadastro.FindEditFormTemplateControl("textDataEx") as ASPxDateEdit;
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        ASPxSpinEdit textFatorQuantidade = gridCadastro.FindEditFormTemplateControl("textFatorQuantidade") as ASPxSpinEdit;
        ASPxSpinEdit textFatorPU = gridCadastro.FindEditFormTemplateControl("textFatorPU") as ASPxSpinEdit;
        //
        ConversaoBolsa conversaoBolsa = new ConversaoBolsa();
        //
        conversaoBolsa.CdAtivoBolsa = btnEditAtivoBolsa.Text.ToString().ToUpper();
        conversaoBolsa.CdAtivoBolsaDestino = btnEditAtivoDestino.Text.ToString().ToUpper();
        conversaoBolsa.DataLancamento = Convert.ToDateTime(textDataLancamento.Text);
        conversaoBolsa.DataEx = Convert.ToDateTime(textDataEx.Text);
        conversaoBolsa.DataReferencia = Convert.ToDateTime(textDataReferencia.Text);
        conversaoBolsa.Fonte = (byte)FonteProventoBolsa.Manual;
        conversaoBolsa.FatorQuantidade = Convert.ToDecimal(textFatorQuantidade.Text);
        conversaoBolsa.FatorPU = Convert.ToDecimal(textFatorPU.Text);

        conversaoBolsa.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de ConversaoBolsa - Operacao: Insert ConversaoBolsa: " + conversaoBolsa.IdConversao + UtilitarioWeb.ToString(conversaoBolsa),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        int idConversao = (int)e.Keys[0];

        ASPxButtonEdit btnEditAtivoBolsa = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa") as ASPxButtonEdit;
        ASPxButtonEdit btnEditAtivoDestino = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa2") as ASPxButtonEdit;
        ASPxDateEdit textDataLancamento = gridCadastro.FindEditFormTemplateControl("textDataLancamento") as ASPxDateEdit;
        ASPxDateEdit textDataEx = gridCadastro.FindEditFormTemplateControl("textDataEx") as ASPxDateEdit;
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        ASPxSpinEdit textFatorQuantidade = gridCadastro.FindEditFormTemplateControl("textFatorQuantidade") as ASPxSpinEdit;
        ASPxSpinEdit textFatorPU = gridCadastro.FindEditFormTemplateControl("textFatorPU") as ASPxSpinEdit;
        //
        ConversaoBolsa conversaoBolsa = new ConversaoBolsa();
        //
        if (conversaoBolsa.LoadByPrimaryKey(idConversao)) {
            conversaoBolsa.CdAtivoBolsa = btnEditAtivoBolsa.Text.ToString().ToUpper();
            conversaoBolsa.CdAtivoBolsaDestino = btnEditAtivoDestino.Text.ToString().ToUpper();
            conversaoBolsa.DataLancamento = Convert.ToDateTime(textDataLancamento.Text);
            conversaoBolsa.DataEx = Convert.ToDateTime(textDataEx.Text);
            conversaoBolsa.DataReferencia = Convert.ToDateTime(textDataReferencia.Text);
            conversaoBolsa.Fonte = (byte)FonteProventoBolsa.Manual;
            conversaoBolsa.FatorQuantidade = Convert.ToDecimal(textFatorQuantidade.Text);
            conversaoBolsa.FatorPU = Convert.ToDecimal(textFatorPU.Text);
            //
            conversaoBolsa.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de ConversaoBolsa - Operacao: Update ConversaoBolsa: " + idConversao + UtilitarioWeb.ToString(conversaoBolsa),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValues1 = gridCadastro.GetSelectedFieldValues(ConversaoBolsaMetadata.ColumnNames.IdConversao);
            for (int i = 0; i < keyValues1.Count; i++) {
                int IdConversao = Convert.ToInt32(keyValues1[i]);

                ConversaoBolsa conversaoBolsa = new ConversaoBolsa();
                if (conversaoBolsa.LoadByPrimaryKey(IdConversao)) {

                    ConversaoBolsa conversaoBolsaClone = (ConversaoBolsa)Utilitario.Clone(conversaoBolsa);
                    //

                    conversaoBolsa.MarkAsDeleted();
                    conversaoBolsa.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de ConversaoBolsa - Operacao: Delete ConversaoBolsa: " + IdConversao + UtilitarioWeb.ToString(conversaoBolsaClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing) {
            ASPxButtonEdit btnEditAtivoBolsa = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa") as ASPxButtonEdit;
            e.Properties["cpBtnEditAtivoBolsa"] = btnEditAtivoBolsa.ClientID;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("btnEditAtivoBolsa");
        base.gridCadastro_PreRender(sender, e);

        ASPxDateEdit textDataInicio = popupFiltro.FindControl("textDataInicio") as ASPxDateEdit;
        ASPxDateEdit textDataFim = popupFiltro.FindControl("textDataFim") as ASPxDateEdit;

        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        Label labelFiltroHeader = gridCadastro.FindStatusBarTemplateControl("labelFiltroHeader") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");

        if (textDataInicio.Text != "") {
            texto.Append(" Data Início >= ").Append(textDataInicio.Text.Substring(0, 10));
        }
        if (textDataFim.Text != "") {
            if (textDataInicio.Text != "") {
                texto.Append(" |");
            }
            texto.Append(" Data Fim <= ").Append(textDataFim.Text.Substring(0, 10));
        }

        labelFiltro.Text = texto.ToString();
    }
}