﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BonificacaoBolsa.aspx.cs" Inherits="CadastrosBasicos_BonificacaoBolsa" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">

    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />    
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">    
    var popup = true;
    document.onkeydown=onDocumentKeyDown;    
    var operacao = '';
    
    var browseAtivoBolsa2 = false; //Controla o retorno do popup de AtivoBolsa (para ativoBolsa ou ativoBolsa2)
    function OnGetDataAtivoBolsa(data) {
        btnEditAtivoBolsa.SetValue(data);
        popupAtivoBolsa.HideWindow();
        ASPxCallback1.SendCallback(btnEditAtivoBolsa.GetValue());
        btnEditAtivoBolsa.Focus();
    }
    function OnGetDataAtivoBolsa2(data) {
        btnEditAtivoBolsa2.SetValue(data);
        popupAtivoBolsa.HideWindow();
        ASPxCallback2.SendCallback(btnEditAtivoBolsa2.GetValue());
        btnEditAtivoBolsa2.Focus();
    }    
    </script>
</head>

<body>
    <form id="form1" runat="server">
    
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '') {
                alert(e.result);                              
            }
            else {
                if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new') {             
                    gridCadastro.UpdateEdit();
                }
                else {
                    callbackAdd.SendCallback();
                }    
            }
            
            operacao = '';
        }
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {
            alert('Operação feita com sucesso.');
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) { 
            if (e.result == '' && btnEditAtivoBolsa.GetValue() != null)
            {
                popupMensagemAtivoBolsa.SetContentHTML(document.getElementById('textMsgNaoExiste').value);
                popupMensagemAtivoBolsa.ShowAtElementByID(btnEditAtivoBolsa.name);
                btnEditAtivoBolsa.SetValue(''); 
                btnEditAtivoBolsa.Focus();                 
            }            
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="ASPxCallback2" runat="server" OnCallback="ASPxCallback2_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {
            if (e.result == '' && btnEditAtivoBolsa2.GetValue() != null)
            {
                popupMensagemAtivoBolsa.SetContentHTML(document.getElementById('textMsgNaoExiste').value);
                popupMensagemAtivoBolsa.ShowAtElementByID(btnEditAtivoBolsa2.name);
                btnEditAtivoBolsa2.SetValue(''); 
                btnEditAtivoBolsa2.Focus();                 
            }            
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">
    
    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Lançamentos de Bonificação (Bolsa)"></asp:Label>
    </div>
        
    <div id="mainContent">
                
        <dxpc:ASPxPopupControl ID="popupFiltro" AllowDragging="true" PopupElementID="popupFiltro" EnableClientSideAPI="True"                    
                        PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" 
                        Width="400" Left="250" Top="70" HeaderText="Filtros adicionais para consulta" runat="server"
                        HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">            
            <ContentCollection><dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                
                <table>        
                <tr>
                <td>                
                    <asp:Label ID="labelDataInicio" runat="server" CssClass="labelNormal" Text="Início:"></asp:Label>
                </td>    
                                    
                <td>
                    <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" />
                </td>  
                
                <td>
                    <asp:Label ID="labelDataFim" runat="server" CssClass="labelNormal" Text="Fim:"></asp:Label>                                                                                                                                                                         
                </td>
                <td>
                    <dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim" />
                </td>
                </tr>
                
                <tr>
                <td>
                    <asp:Label ID="labelCdAtivo" runat="server" CssClass="labelNormal" Text="Ativo:"></asp:Label>
                </td>                
                <td colspan="3">
                    <asp:TextBox ID="textCdAtivo" runat="server" CssClass="textNormal_5"/>
                </td>    
                </tr>
                
                <tr>
                <td>
                    <asp:Label ID="labelCdAtivoDestino" runat="server" CssClass="labelNormal" Text="Destino:"></asp:Label>
                </td>                
                <td colspan="3">
                    <asp:TextBox ID="textCdAtivoDestino" runat="server" CssClass="textNormal_5"/>
                </td>    
                </tr>
                </table>        
                
                <div class="linkButton linkButtonNoBorder" style="margin-top:20px">              
                <asp:LinkButton ID="btnOKFilter" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnOK" OnClientClick="popupFiltro.Hide(); gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal7" runat="server" Text="Aplicar"/><div></div></asp:LinkButton>
                <asp:LinkButton ID="btnFilterCancel" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnFilterCancel" OnClientClick="OnClearFilterClick_FiltroDatas(); return false;"><asp:Literal ID="Literal8" runat="server" Text="Limpar"/><div></div></asp:LinkButton>
                </div>                    
            </dxpc:PopupControlContentControl></ContentCollection>                             
        </dxpc:ASPxPopupControl>        
    
        <div class="linkButton" >               
           <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal1" runat="server" Text="Novo"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;"><asp:Literal ID="Literal2" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnFilter" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnFilter" OnClientClick="return OnButtonClick_FiltroDatas()"><asp:Literal ID="Literal3" runat="server" Text="Filtro"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal4" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal5" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
           <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal6" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
        </div>
                    
        <div class="divDataGrid">            
            <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                KeyFieldName="IdBonificacao" DataSourceID="EsDSBonificacaoBolsa"
                OnCustomCallback="gridCadastro_CustomCallback"
                OnRowUpdating="gridCadastro_RowUpdating"
                OnRowInserting="gridCadastro_RowInserting"
                OnCustomJSProperties="gridCadastro_CustomJSProperties"
                OnPreRender="gridCadastro_PreRender"
                OnBeforeGetCallbackResult="gridCadastro_PreRender">        
                    
            <Columns>           
                <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="10%" ButtonType="Image" ShowClearFilterButton="True">
                    <HeaderTemplate>
                        <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                    </HeaderTemplate>
                </dxwgv:GridViewCommandColumn>
                
                <dxwgv:GridViewDataTextColumn FieldName="CdAtivoBolsa" Caption="Ativo" Width="15%" VisibleIndex="1" />
                
                <dxwgv:GridViewDataDateColumn FieldName="DataLancamento" Caption="Lançamento" VisibleIndex="2" Width="10%"/>
                
                <dxwgv:GridViewDataDateColumn FieldName="DataEx" VisibleIndex="3" Width="10%"/>
                
                <dxwgv:GridViewDataDateColumn FieldName="DataReferencia" Caption="Referência" VisibleIndex="4" Width="10%"/>
                
                <dxwgv:GridViewDataSpinEditColumn FieldName="Percentual" VisibleIndex="5" Width="10%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                    <PropertiesSpinEdit DisplayFormatString="{0:#,##0.0000;(#,##0.0000);0.0000}"></PropertiesSpinEdit>
                </dxwgv:GridViewDataSpinEditColumn>
                
                <dxwgv:GridViewDataTextColumn FieldName="PUBonificacao" Caption="PU Bonificação" VisibleIndex="6" Width="10%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                    <PropertiesTextEdit DisplayFormatString="{0:#,##0.0000;(#,##0.0000);0.0000}"></PropertiesTextEdit>
                </dxwgv:GridViewDataTextColumn>
                
                <dxwgv:GridViewDataTextColumn FieldName="CdAtivoBolsaDestino" Caption="Ativo Destino" Width="15%" VisibleIndex="7" Settings-AutoFilterCondition="Contains" />
                
                <dxwgv:GridViewDataTextColumn FieldName="TipoFracao" Visible="false" />                                    
            </Columns>
            
            <Templates>
            <EditForm>
                                
                <div class="editForm">
                    
                    <table> 
                        <tr>                                        
                            <td class="td_Label">
                                <asp:Label ID="labelAtivo" runat="server" CssClass="labelRequired" Text="Ativo:"></asp:Label>                        
                            </td>
                            <td >
                                <dxe:ASPxButtonEdit ID="btnEditAtivoBolsa" runat="server" Width="100%" CssClass="textAtivoCurto" MaxLength="25" 
                                                    ClientInstanceName="btnEditAtivoBolsa" Text="<%#Bind('CdAtivoBolsa')%>" >            
                                <Buttons>
                                    <dxe:EditButton></dxe:EditButton>                
                                </Buttons>           
                                <ClientSideEvents                              
                                     ButtonClick="function(s, e) {browseAtivoBolsa2=false; popupAtivoBolsa.ShowAtElementByID(s.name);}" 
                                     LostFocus="function(s, e) {popupMensagemAtivoBolsa.HideWindow(); 
                                                                if (btnEditAtivoBolsa.GetValue() != null)
                                                                {
                                                                    ASPxCallback1.SendCallback(btnEditAtivoBolsa.GetValue());
                                                                }
                                                                }"
                                />                    
                                </dxe:ASPxButtonEdit>
                            </td>
                            <td class="td_Label">
                                <asp:Label ID="labelDataLancamento" runat="server" CssClass="labelRequired" Text="Lançamento:"></asp:Label>
                            </td>                                                        
                            <td >   
                                <dxe:ASPxDateEdit ID="textDataLancamento" runat="server" ClientInstanceName="textDataVencimento" Value='<%#Eval("DataLancamento")%>'/>                                                 
                            </td>  
                        </tr>                                        
                        
                        <tr>
                            <td class="td_Label">
                                <asp:Label ID="labelDataEx" runat="server" CssClass="labelRequired" Text="Data Ex:"></asp:Label>
                            </td>                                                        
                            <td >
                                <dxe:ASPxDateEdit ID="textDataEx" runat="server" ClientInstanceName="textDataEx" Value='<%#Eval("DataEx")%>'/>                                
                            </td>
                            <td class="td_Label">
                                <asp:Label ID="labelDataReferencia" runat="server" CssClass="labelRequired" Text="Data Ref.:"></asp:Label>
                            </td>                                                        
                            <td >
                                <dxe:ASPxDateEdit ID="textDataReferencia" runat="server" Width="100%" ClientInstanceName="textDataReferencia" Value='<%#Eval("DataReferencia")%>'/>                                                    
                            </td>  
                        </tr>
                        
                        <tr>     
                            <td class="td_Label">
                                <asp:Label ID="labelPercentual" runat="server" AssociatedControlID="textPercentual" CssClass="labelRequired" Text="Percentual:" />
                            </td>                                
                            <td>                            
                                <dxe:ASPxSpinEdit ID="textPercentual" runat="server" ClientInstanceName="textPercentual" CssClass="textValor_5" 
                                Text='<%# Eval("Percentual") %>' NumberType="Float" MaxLength="20" DecimalPlaces="11">
                                </dxe:ASPxSpinEdit>                                
                            </td>                            
                            <td class="td_Label">
                                <asp:Label ID="labelPUBonificacao" runat="server" AssociatedControlID="textPUBonificacao" CssClass="labelRequired" Text="PU:" />
                            </td>                                
                            <td >
                                <dxe:ASPxSpinEdit ID="textPUBonificacao" runat="server" ClientInstanceName="textPUBonificacao" CssClass="textValor_5"
                                Text='<%# Eval("PUBonificacao") %>' NumberType="Float" MaxLength="15" DecimalPlaces="10">
                                </dxe:ASPxSpinEdit>                                
                            </td>
                        </tr>    
                        
                        <tr>
                            <td  class="td_Label">
                                <asp:Label ID="labelAtivo2" runat="server" CssClass="labelRequired" Text="Ativo Dest:"></asp:Label>                        
                            </td>
                            <td >
                                <dxe:ASPxButtonEdit ID="btnEditAtivoBolsa2" runat="server" Width="100%" CssClass="textAtivoCurto" 
                                                    MaxLength="25" ClientInstanceName="btnEditAtivoBolsa2" 
                                                    Text="<%#Bind('CdAtivoBolsaDestino')%>">
                                <Buttons>                                           
                                    <dxe:EditButton />
                                </Buttons>                    
                                <ClientSideEvents                              
                                     ButtonClick="function(s, e) {browseAtivoBolsa2=true; popupAtivoBolsa.ShowAtElementByID(s.name);}" 
                                     LostFocus="function(s, e) {popupMensagemAtivoBolsa.HideWindow();  
                                                                if (btnEditAtivoBolsa2.GetValue() != null)  
                                                                {
                                                                    ASPxCallback2.SendCallback(btnEditAtivoBolsa2.GetValue());
                                                                }
                                                                }"
                                />                    
                                </dxe:ASPxButtonEdit>
                            </td>                                                               
                        </tr>   
                    </table>    
                
                    <div class="linhaH"></div>
                    
                    <div class="linkButton linkButtonNoBorder popupFooter">
                        <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd" OnInit="btnOKAdd_Init"
                               OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal12" runat="server" Text="OK+"/><div></div></asp:LinkButton>
                        
                        <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                        OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal5" runat="server" Text="OK"/><div></div></asp:LinkButton>
                                                        
                        <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel" OnClientClick="gridCadastro.CancelEdit(); return false;"><asp:Literal ID="Literal9" runat="server" Text="Cancelar"/><div></div></asp:LinkButton>
                    </div>                        
                </div>       
                                
            </EditForm>
            
            <StatusBar>
                <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" />
            </StatusBar>      
            
            </Templates>
            
            <SettingsPopup EditForm-Width="500px" />
            <SettingsCommandButton>
                <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
            </SettingsCommandButton>
                        
        </dxwgv:ASPxGridView>            
        </div>
       
    </div>
    </div>
    </td></tr></table>
    </div>        
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
        
    <cc1:esDataSource ID="EsDSBonificacaoBolsa" runat="server" OnesSelect="EsDSBonificacaoBolsa_esSelect" />        
    <cc1:esDataSource ID="EsDSAtivoBolsa" runat="server" OnesSelect="EsDSAtivoBolsa_esSelect" />        
        
    </form>
</body>
</html>