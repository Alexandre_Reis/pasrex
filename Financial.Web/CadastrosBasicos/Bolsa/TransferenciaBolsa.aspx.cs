﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Text;
using System.Collections.Generic;

using DevExpress.Web;

using Financial.Common;
using Financial.Util;
using Financial.Investidor;
using Financial.ContaCorrente;
using Financial.Security;
using Financial.Fundo;
using Financial.Bolsa;
using Financial.Web.Common;

using Financial.ContaCorrente.Enums;
using Financial.Common.Enums;
using Financial.Investidor.Enums;
using System.Threading;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_TransferenciaBolsa : CadastroBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasPopupCliente = true;
        this.HasPopupAtivoBolsa = true;
        this.HasFiltro = true;
        base.Page_Load(sender, e);
    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSTransferenciaBolsa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        Usuario usuario = new Usuario();
        if (usuario.BuscaUsuario(Context.User.Identity.Name)) {
            int idUsuario = usuario.IdUsuario.Value;

            TransferenciaBolsaQuery transferenciaBolsaQuery = new TransferenciaBolsaQuery("T");
            ClienteQuery clienteQuery = new ClienteQuery("C");
            PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");

            transferenciaBolsaQuery.Select(transferenciaBolsaQuery, clienteQuery.Apelido.As("Apelido"));
            transferenciaBolsaQuery.InnerJoin(clienteQuery).On(transferenciaBolsaQuery.IdCliente == clienteQuery.IdCliente);
            transferenciaBolsaQuery.InnerJoin(permissaoClienteQuery).On(transferenciaBolsaQuery.IdCliente == permissaoClienteQuery.IdCliente);
            transferenciaBolsaQuery.Where(permissaoClienteQuery.IdUsuario.Equal(idUsuario));

            if (!String.IsNullOrEmpty(btnEditCodigoClienteFiltro.Text)) {
                transferenciaBolsaQuery.Where(transferenciaBolsaQuery.IdCliente.Equal(Convert.ToInt32(btnEditCodigoClienteFiltro.Text)));
            }

            if (!String.IsNullOrEmpty(textDataInicio.Text)) {
                transferenciaBolsaQuery.Where(transferenciaBolsaQuery.Data.GreaterThanOrEqual(textDataInicio.Text));
            }

            if (!String.IsNullOrEmpty(textDataFim.Text)) {
                transferenciaBolsaQuery.Where(transferenciaBolsaQuery.Data.LessThanOrEqual(textDataFim.Text));
            }

            transferenciaBolsaQuery.OrderBy(transferenciaBolsaQuery.Data.Descending);

            TransferenciaBolsaCollection coll = new TransferenciaBolsaCollection();
            coll.Load(transferenciaBolsaQuery);

            // Assign the esDataSourcSelectEvenArgs Collection property
            e.Collection = coll;
        }
        else {
            e.Collection = new TransferenciaBolsaCollection();
        }

    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ClienteCollection coll = new ClienteCollection();
        coll.BuscaClientesComAcesso(Context.User.Identity.Name);
        
        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSAgenteMercado_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        AgenteMercadoCollection coll = new AgenteMercadoCollection();
        coll.Query.Where(coll.Query.FuncaoCustodiante == 'S');
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSAtivoBolsa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        AtivoBolsaCollection coll = new AtivoBolsaCollection();
        coll.Query.OrderBy(coll.Query.CdAtivoBolsa.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void panelEdicao_Load(object sender, EventArgs e) {
        UtilitarioGrid.SetaCorCombosPopup(sender as Control);

        bool travaPainel = false;
        if (gridCadastro.IsEditing && !gridCadastro.IsNewRowEditing && Convert.ToString(Session["FormLoad"]) != "S") {
            Session["FormLoad"] = "S";
            Label labelEdicao = gridCadastro.FindEditFormTemplateControl("labelEdicao") as Label;

            int idCliente = Convert.ToInt32(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "IdCliente"));
            DateTime data = Convert.ToDateTime(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "Data"));

            Cliente cliente = new Cliente();
            if (cliente.LoadByPrimaryKey(idCliente)) {
                DateTime dataDia = cliente.DataDia.Value;
                byte status = cliente.Status.Value;
                if (status == (byte)StatusCliente.Divulgado) {
                    travaPainel = true;
                    labelEdicao.Text = "Cliente está fechado. Dados não podem ser alterados.";
                }
                else if (DateTime.Compare(dataDia, data) > 0) {
                    travaPainel = true;
                    labelEdicao.Text = "Data informada anterior à data do cliente. Dados não podem ser alterados.";
                }
            }
        }

        if (travaPainel) {
            LinkButton btnOK = gridCadastro.FindEditFormTemplateControl("btnOK") as LinkButton;
            btnOK.Visible = false;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOKAdd_Init(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";

        if (!gridCadastro.IsEditing) //Testa deleção
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdCliente");
            List<object> keyData = gridCadastro.GetSelectedFieldValues("Data");
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idCliente = Convert.ToInt32(keyValuesId[i]);
                DateTime data = Convert.ToDateTime(keyData[i]);

                Cliente cliente = new Cliente();
                if (cliente.LoadByPrimaryKey(idCliente)) {
                    DateTime dataDia = cliente.DataDia.Value;
                    byte status = cliente.Status.Value;
                    string clienteDescricao = cliente.Apelido + " (" + cliente.IdCliente.ToString() + ")";
                    if (status == (byte)StatusCliente.Divulgado) {
                        e.Result = "Cliente " + clienteDescricao + " está fechado! Deleção cancelada.";
                        return;
                    }
                    else if (DateTime.Compare(dataDia, data) > 0) {
                        e.Result = "Lançamento com data " + data.ToString().Substring(0, 10) + " anterior à data do cliente " + clienteDescricao + "! Deleção cancelada.";
                        return;
                    }
                }
            }
        }
        else {
            ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
            ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
            ASPxButtonEdit btnEditAtivoBolsa = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa") as ASPxButtonEdit;
            ASPxComboBox dropAgenteOrigem = gridCadastro.FindEditFormTemplateControl("dropAgenteOrigem") as ASPxComboBox;
            ASPxComboBox dropAgenteDestino = gridCadastro.FindEditFormTemplateControl("dropAgenteDestino") as ASPxComboBox;
            ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
            ASPxSpinEdit textPU = gridCadastro.FindEditFormTemplateControl("textPU") as ASPxSpinEdit;

            #region Campos obrigatórios
            List<Control> controles = new List<Control>();
            controles.Add(btnEditCodigoCliente);
            controles.Add(btnEditAtivoBolsa);
            controles.Add(dropAgenteOrigem);
            controles.Add(dropAgenteDestino);
            controles.Add(textQuantidade);
            controles.Add(textPU);

            if (base.TestaObrigatorio(controles) != "") {
                e.Result = "Campos com * são obrigatórios!";
                return;
            }
            #endregion

            int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
            DateTime data = Convert.ToDateTime(textData.Text);

            if (!Calendario.IsDiaUtil(data, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil)) {
                e.Result = "Data não é dia útil.";
                return;
            }

            Cliente cliente = new Cliente();
            if (cliente.LoadByPrimaryKey(idCliente)) 
            {
                DateTime dataDia = cliente.DataDia.Value;
                byte status = cliente.Status.Value;
                if (status == (byte)StatusCliente.Divulgado) {
                    e.Result = "Cliente está fechado! Operação não pode ser realizada.";
                    return;
                }
                else if (DateTime.Compare(dataDia, Convert.ToDateTime(textData.Text)) > 0) {
                    e.Result = "Data informada anterior à data do cliente! Operação não pode ser realizada.";
                    return;
                }
            }

            ClienteBolsa clienteBolsa = new ClienteBolsa();
            clienteBolsa.Query.Select(clienteBolsa.Query.IdCustodianteAcoes,
                                      clienteBolsa.Query.IdCustodianteOpcoes);
            clienteBolsa.Query.Where(clienteBolsa.Query.IdCliente.Equal(idCliente));
            clienteBolsa.Query.Load();

            if (clienteBolsa.IdCustodianteAcoes.HasValue || clienteBolsa.IdCustodianteOpcoes.HasValue)
            {
                e.Result = "Cadastro de Transferência não pode ser realizado, pois existe centralização de custodiante cadastrada para o Cliente!";
                return;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        DateTime dataDia;
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") {
                if (cliente.Status.Value != (byte)StatusCliente.Divulgado) {
                    if (cliente.IsAtivo) {
                        PermissaoCliente permissaoCliente = new PermissaoCliente();
                        if (permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name))
                        {
                            nome = cliente.str.Apelido;
                            dataDia = cliente.DataDia.Value;

                            nome = nome + "|" + dataDia.ToShortDateString() + "|" + Thread.CurrentThread.CurrentCulture.Name;
                        }
                        else {
                            nome = "no_access";
                        }
                    }
                    else {
                        nome = "no_active";
                    }
                }
                else {
                    nome = cliente.str.Apelido + "|status_closed";
                }
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string CdAtivoBolsa = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            AtivoBolsa ativoBolsa = new AtivoBolsa();
            ativoBolsa.LoadByPrimaryKey(Convert.ToString(e.Parameter));

            if (ativoBolsa.str.CdAtivoBolsa != "") {
                CdAtivoBolsa = ativoBolsa.str.CdAtivoBolsa;
            }
        }
        e.Result = CdAtivoBolsa;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        int idTransferencia = (int)e.Keys[0];
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxButtonEdit btnEditAtivoBolsa = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa") as ASPxButtonEdit;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxComboBox dropAgenteOrigem = gridCadastro.FindEditFormTemplateControl("dropAgenteOrigem") as ASPxComboBox;
        ASPxComboBox dropAgenteDestino = gridCadastro.FindEditFormTemplateControl("dropAgenteDestino") as ASPxComboBox;
        ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
        ASPxSpinEdit textPU = gridCadastro.FindEditFormTemplateControl("textPU") as ASPxSpinEdit;

        TransferenciaBolsa transferenciaBolsa = new TransferenciaBolsa();
        if (transferenciaBolsa.LoadByPrimaryKey(idTransferencia)) {
            int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
            transferenciaBolsa.IdCliente = idCliente;
            transferenciaBolsa.CdAtivoBolsa = Convert.ToString(btnEditAtivoBolsa.Text).ToUpper();
            transferenciaBolsa.Data = Convert.ToDateTime(textData.Text);
            transferenciaBolsa.IdAgenteOrigem = Convert.ToInt32(dropAgenteOrigem.SelectedItem.Value);
            transferenciaBolsa.IdAgenteDestino = Convert.ToInt32(dropAgenteDestino.SelectedItem.Value);
            transferenciaBolsa.Quantidade = Convert.ToDecimal(textQuantidade.Text);

            decimal? pu = null;
            if (!String.IsNullOrEmpty(textPU.Text)) {
                pu = Convert.ToDecimal(textPU.Text);
            }
            transferenciaBolsa.Pu = pu;

            transferenciaBolsa.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de TransferenciaBolsa - Operacao: Update TransferenciaBolsa: " + idTransferencia + UtilitarioWeb.ToString(transferenciaBolsa),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion

            //Atualiza StatusRealTime para executar***********
            Cliente cliente = new Cliente();
            cliente.SetaFlagRealTime(idCliente, (byte)StatusRealTimeCliente.Executar);
            //************************************************
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        this.SalvarNovo();
        
        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    private void SalvarNovo() {
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxButtonEdit btnEditAtivoBolsa = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa") as ASPxButtonEdit;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxComboBox dropAgenteOrigem = gridCadastro.FindEditFormTemplateControl("dropAgenteOrigem") as ASPxComboBox;
        ASPxComboBox dropAgenteDestino = gridCadastro.FindEditFormTemplateControl("dropAgenteDestino") as ASPxComboBox;
        ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
        ASPxSpinEdit textPU = gridCadastro.FindEditFormTemplateControl("textPU") as ASPxSpinEdit;

        TransferenciaBolsa transferenciaBolsa = new TransferenciaBolsa();

        int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);

        transferenciaBolsa.IdCliente = idCliente;
        transferenciaBolsa.CdAtivoBolsa = Convert.ToString(btnEditAtivoBolsa.Text).ToUpper();
        transferenciaBolsa.Data = Convert.ToDateTime(textData.Text);
        transferenciaBolsa.IdAgenteOrigem = Convert.ToInt32(dropAgenteOrigem.SelectedItem.Value);
        transferenciaBolsa.IdAgenteDestino = Convert.ToInt32(dropAgenteDestino.SelectedItem.Value);
        transferenciaBolsa.Quantidade = Convert.ToDecimal(textQuantidade.Text);

        if (!String.IsNullOrEmpty(textPU.Text)) {
            transferenciaBolsa.Pu = Convert.ToDecimal(textPU.Text);
        }

        transferenciaBolsa.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de TransferenciaBolsa - Operacao: Insert TransferenciaBolsa: " + transferenciaBolsa.IdTransferencia + UtilitarioWeb.ToString(transferenciaBolsa),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        //Atualiza StatusRealTime para executar***********
        Cliente cliente = new Cliente();
        cliente.SetaFlagRealTime(idCliente, (byte)StatusRealTimeCliente.Executar);
        //************************************************
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        SalvarNovo();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdTransferencia");
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idTranferencia = Convert.ToInt32(keyValuesId[i]);

                TransferenciaBolsa transferenciaBolsa = new TransferenciaBolsa();
                if (transferenciaBolsa.LoadByPrimaryKey(idTranferencia)) {
                    int idCliente = transferenciaBolsa.IdCliente.Value;

                    TransferenciaBolsa transferenciaBolsaClone = (TransferenciaBolsa)Utilitario.Clone(transferenciaBolsa);
                    //

                    transferenciaBolsa.MarkAsDeleted();
                    transferenciaBolsa.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de TransferenciaBolsa - Operacao: Delete TransferenciaBolsa: " + idTranferencia + UtilitarioWeb.ToString(transferenciaBolsaClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion

                    //Atualiza StatusRealTime para executar***********
                    Cliente cliente = new Cliente();
                    cliente.SetaFlagRealTime(idCliente, (byte)StatusRealTimeCliente.Executar);
                    //************************************************
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing) {
            ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
            e.Properties["cpBtnEditCodigoCliente"] = btnEditCodigoCliente.ClientID;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("btnEditCodigoCliente");
        base.gridCadastro_PreRender(sender, e);                              
        //        
        ASPxDateEdit textDataInicio = popupFiltro.FindControl("textDataInicio") as ASPxDateEdit;
        ASPxDateEdit textDataFim = popupFiltro.FindControl("textDataFim") as ASPxDateEdit;
        ASPxSpinEdit btnEditCodigoClienteFiltro = popupFiltro.FindControl("btnEditCodigoClienteFiltro") as ASPxSpinEdit;

        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        Label labelFiltroHeader = gridCadastro.FindStatusBarTemplateControl("labelFiltroHeader") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");
        if (btnEditCodigoClienteFiltro != null && btnEditCodigoClienteFiltro.Text != "") {
            texto.Append(" Cliente: ").Append(btnEditCodigoClienteFiltro.Text);
        }
        if (textDataInicio.Text != "") {
            if (btnEditCodigoClienteFiltro != null && btnEditCodigoClienteFiltro.Text != "") {
                texto.Append(" |");
            }
            texto.Append(" Data Início >= ").Append(textDataInicio.Text.Substring(0, 10));
        }
        if (textDataFim.Text != "") {
            if (textDataInicio.Text != "" || btnEditCodigoClienteFiltro.Text != "") {
                texto.Append(" |");
            }
            texto.Append(" Data Fim <= ").Append(textDataFim.Text.Substring(0, 10));
        }

        labelFiltro.Text = texto.ToString();
    }
}