﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EspecificacaoBolsa.aspx.cs" Inherits="CadastrosBasicos_EspecificacaoBolsa" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>    
    <script type="text/javascript" language="Javascript">
    document.onkeydown=onDocumentKeyDownWithCallback;
    var operacao = '';
    
    </script>
</head>

<body>
    <form id="form1" runat="server">
            
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                gridCadastro.UpdateEdit();                
            }
            operacao = '';
        }        
        "/>
    </dxcb:ASPxCallback>
        
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">
    
    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Especificação (Bolsa)"></asp:Label>
    </div>
        
    <div id="mainContent">
                                        
            <div class="linkButton" >               
               <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal1" runat="server" Text="Novo"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;"><asp:Literal ID="Literal2" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal4" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal5" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>              
               <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal6" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
            </div>
    
            <div class="divDataGrid">            
                <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                            KeyFieldName="Especificacao" DataSourceID="EsDSEspecificacaoBolsa"
                            OnRowUpdating="gridCadastro_RowUpdating"
                            OnRowInserting="gridCadastro_RowInserting"
                            OnCustomCallback="gridCadastro_CustomCallback"
                            OnBeforeGetCallbackResult="gridCadastro_PreRender"  >
                                
                    <Columns>           
                        <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                            <HeaderTemplate>
                                <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                            </HeaderTemplate>
                        </dxwgv:GridViewCommandColumn>
                                                        
                        <dxwgv:GridViewDataTextColumn FieldName="Especificacao" Caption="Especificação" VisibleIndex="1" Width="20%"/>
                    
                        <dxwgv:GridViewDataComboBoxColumn FieldName="Tipo" VisibleIndex="2" Width="75%" ExportWidth="130">
                        <PropertiesComboBox EncodeHtml="false">
                        <Items>                
                            <dxe:ListEditItem Value="1" Text="<div title='Ordinária'>Ordinária</div>" />
                            <dxe:ListEditItem Value="2" Text="<div title='Preferencial'>Preferencial</div>" />                    
                            <dxe:ListEditItem Value="3" Text="<div title='Unit'>Unit</div>" />
                        </Items>
                        </PropertiesComboBox>
                        </dxwgv:GridViewDataComboBoxColumn>
                                            
                    </Columns>
                    
                    <Templates>
                        <EditForm>
                                                    
                            <div class="editForm">        
                                 
                                 <table>                    
                                 <tr>
                                      <td class="td_Label">
                                        <asp:Label ID="labelEspecificacao" runat="server" CssClass="labelRequired" Text="Especificação:"/>
                                      </td>
                                                                                                                            
                                        <td>
                                            <dxe:ASPxComboBox ID="dropEspecificacao" runat="server" DataSourceID="EsDSEspecificacao1"
                                                                 ValueField="Especificacao" TextField="Especificacao" CssClass="dropDownListCurto" Text='<%#Eval("Especificacao")%>'>
                                            </dxe:ASPxComboBox>
                                        </td>
                                  </tr>
                                  
                                  <tr>
                                    <td class="td_Label">
                                        <asp:Label ID="labelTipo" runat="server" CssClass="labelRequired" Text="Tipo:" />
                                    </td>
                                    
                                    <td>
                                        <dxe:ASPxComboBox ID="dropTipo" runat="server" CssClass="dropDownListCurto" Text='<%#Eval("Tipo")%>'>
                                        <Items>
                                        <dxe:ListEditItem Value="1" Text="Ordinária" />
                                        <dxe:ListEditItem Value="2" Text="Preferencial" />
                                        <dxe:ListEditItem Value="3" Text="Unit" />
                                        </Items>                                                                                    
                                        </dxe:ASPxComboBox>                            
                                    </td>
                                    </tr>
                                </table>
                                
                                <div class="linhaH"></div>
                        
                                <div class="linkButton linkButtonNoBorder popupFooter">
                                    <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK" 
                                                            OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal5" runat="server" Text="OK"/><div></div></asp:LinkButton>     
                                    <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel" OnClientClick="gridCadastro.CancelEdit(); return false;"><asp:Literal ID="Literal9" runat="server" Text="Cancelar"/><div></div></asp:LinkButton>
                                </div>    
                            </div>
                        </EditForm>
                        <StatusBar>
                            <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" />
                        </StatusBar>            
                    </Templates>
                    
                    <SettingsPopup EditForm-Width="250px" />
                                        
                    <Templates>
                    <StatusBar>
                        <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" />
                    </StatusBar>            
                    </Templates>
                    <SettingsCommandButton>
                        <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                    </SettingsCommandButton>                        
                </dxwgv:ASPxGridView>            
            </div>                          
    </div>
    </div>
    </td></tr></table>
    </div>
                       
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
        
    <cc1:esDataSource ID="EsDSEspecificacaoBolsa" runat="server" OnesSelect="EsDSEspecificacaoBolsa_esSelect" />
    <cc1:esDataSource ID="EsDSEspecificacao1" runat="server" OnesSelect="EsDSEsDSEspecificacao1_esSelect" />
        
    </form>
</body>
</html>