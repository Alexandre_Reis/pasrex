﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Bolsa;
using Financial.Investidor;
using Financial.Security;
using Financial.Web.Common;
using Financial.Security.Enums;
using Financial.Web.Util;
using Financial.Investidor.Enums;
using Financial.Bolsa.Enums;

public partial class CadastrosBasicos_PosicaoBolsaDetalheCarteira : CadastroBasePage
{
    ClientePosicao clientePosicao;

    new protected void Page_Load(object sender, EventArgs e) 
    {
        this.HasPopupCliente = true;
        this.HasPopupAtivoBolsa = true;
        base.Page_Load(sender, e);

        this.gridCadastro.ClientSideEvents.RowDblClick = "function(s, e) {selectedIndex = e.visibleIndex; gridCadastro.GetRowValues(e.visibleIndex, 'TipoCarteira', OpenPopup); }";                
    }

    public class ClientePosicao
    {
        public DateTime dataImplantacao;
        public DateTime dataDia;
        public bool historico;
        public decimal quantidadePosicao;
        public decimal quantidadePosicaoBloqueada;
        public decimal quantidadePosicaoLivre;
    }

    private void CarregaDados()
    {
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxComboBox dropAgente = gridCadastro.FindEditFormTemplateControl("dropAgente") as ASPxComboBox;
        ASPxDateEdit textDataHistorico = gridCadastro.FindEditFormTemplateControl("textDataHistorico") as ASPxDateEdit;
        ASPxButtonEdit btnEditAtivoBolsa = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa") as ASPxButtonEdit;
        ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
        ASPxComboBox dropTipoCarteira = gridCadastro.FindEditFormTemplateControl("dropTipoCarteira") as ASPxComboBox;
        
        int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
        int idAgente = Convert.ToInt32(dropAgente.SelectedItem.Value);
        DateTime dataHistorico = Convert.ToDateTime(textDataHistorico.Text);
        string cdAtivoBolsa = Convert.ToString(btnEditAtivoBolsa.Text);
        int tipoCarteira = Convert.ToInt32(dropTipoCarteira.SelectedItem.Value);
        decimal quantidade = Convert.ToDecimal(textQuantidade.Text);

        Cliente cliente = new Cliente();
        cliente.Query.Select(cliente.Query.DataDia, cliente.Query.DataImplantacao);
        cliente.Query.Where(cliente.Query.IdCliente.Equal(idCliente));
        cliente.Query.Load();

        bool historico = cliente.DataDia.Value > dataHistorico;
        
        decimal quantidadePosicao = 0;
        decimal quantidadePosicaoBloqueada = 0;
        if (historico)
        {
            PosicaoBolsaHistorico posicaoBolsaHistorico = new PosicaoBolsaHistorico();
            posicaoBolsaHistorico.Query.Select(posicaoBolsaHistorico.Query.Quantidade.Sum(),
                                               posicaoBolsaHistorico.Query.QuantidadeBloqueada.Sum());
            posicaoBolsaHistorico.Query.Where(posicaoBolsaHistorico.Query.DataHistorico.Equal(dataHistorico),
                                              posicaoBolsaHistorico.Query.IdCliente.Equal(idCliente),
                                              posicaoBolsaHistorico.Query.IdAgente.Equal(idAgente),
                                              posicaoBolsaHistorico.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));
            posicaoBolsaHistorico.Query.Load();

            quantidadePosicao = posicaoBolsaHistorico.Quantidade.HasValue ? posicaoBolsaHistorico.Quantidade.Value : 0;
            quantidadePosicaoBloqueada = posicaoBolsaHistorico.QuantidadeBloqueada.HasValue ? posicaoBolsaHistorico.QuantidadeBloqueada.Value : 0;
        }
        else
        {
            PosicaoBolsa posicaoBolsa = new PosicaoBolsa();
            posicaoBolsa.Query.Select(posicaoBolsa.Query.Quantidade.Sum(),
                                      posicaoBolsa.Query.QuantidadeBloqueada.Sum());
            posicaoBolsa.Query.Where(posicaoBolsa.Query.IdCliente.Equal(idCliente),
                                     posicaoBolsa.Query.IdAgente.Equal(idAgente),
                                     posicaoBolsa.Query.CdAtivoBolsa.Equal(cdAtivoBolsa));
            posicaoBolsa.Query.Load();

            quantidadePosicao = posicaoBolsa.Quantidade.HasValue ? posicaoBolsa.Quantidade.Value : 0;
            quantidadePosicaoBloqueada = posicaoBolsa.QuantidadeBloqueada.HasValue ? posicaoBolsa.QuantidadeBloqueada.Value : 0;
        }

        decimal quantidadePosicaoLivre = quantidadePosicao - quantidadePosicaoBloqueada;

        this.clientePosicao.dataImplantacao = cliente.DataImplantacao.Value;
        this.clientePosicao.dataDia = cliente.DataDia.Value;
        this.clientePosicao.historico = cliente.DataDia.Value > dataHistorico;
        this.clientePosicao.quantidadePosicao = quantidadePosicao;
        this.clientePosicao.quantidadePosicaoBloqueada = quantidadePosicaoBloqueada;
        this.clientePosicao.quantidadePosicaoLivre = quantidadePosicaoLivre;
    }

    #region DataSources
    protected void EsDSPosicaoBolsaDetalheCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        PosicaoBolsaDetalheQuery posicaoBolsaDetalheQuery = new PosicaoBolsaDetalheQuery("P");
        ClienteQuery clienteQuery = new ClienteQuery("C");
        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("Q");

        posicaoBolsaDetalheQuery.Select(posicaoBolsaDetalheQuery, clienteQuery.Apelido.As("Apelido"));
        posicaoBolsaDetalheQuery.InnerJoin(clienteQuery).On(posicaoBolsaDetalheQuery.IdCliente == clienteQuery.IdCliente);
        posicaoBolsaDetalheQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        posicaoBolsaDetalheQuery.Where(permissaoClienteQuery.IdUsuario.Equal(idUsuario));

        posicaoBolsaDetalheQuery.OrderBy(posicaoBolsaDetalheQuery.IdCliente.Ascending, posicaoBolsaDetalheQuery.CdAtivoBolsa.Ascending);

        PosicaoBolsaDetalheCollection coll = new PosicaoBolsaDetalheCollection();
        coll.Load(posicaoBolsaDetalheQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ClienteCollection coll = new ClienteCollection();
        coll.BuscaClientesComAcesso(Context.User.Identity.Name);
        
        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSAgenteMercado_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        AgenteMercadoCollection coll = new AgenteMercadoCollection();

        coll.Query.Where(coll.Query.FuncaoCorretora.Equal("S"));
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSAtivoBolsa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        AtivoBolsaCollection coll = new AtivoBolsaCollection();
        coll.Query.TipoMercado.NotIn(TipoMercadoBolsa.Futuro, TipoMercadoBolsa.OpcaoCompra, TipoMercadoBolsa.OpcaoVenda, TipoMercadoBolsa.Termo);
        coll.Query.OrderBy(coll.Query.CdAtivoBolsa.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSTipoCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        TipoCarteiraBolsaCollection coll = new TipoCarteiraBolsaCollection();
        //
        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();
        //        
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") {
                if (cliente.IsAtivo) {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name))
                    {
                        nome = cliente.str.Apelido;
                    }
                    else {
                        nome = "no_access";
                    }
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string cdAtivoBolsa = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            AtivoBolsa ativoBolsa = new AtivoBolsa();
            ativoBolsa.LoadByPrimaryKey(Convert.ToString(e.Parameter));

            if (ativoBolsa.str.CdAtivoBolsa != "")
            {
                cdAtivoBolsa = ativoBolsa.str.CdAtivoBolsa;
            }
        }
        e.Result = cdAtivoBolsa;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        SalvarNovo();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) 
    {
        e.Result = "";
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxComboBox dropAgente = gridCadastro.FindEditFormTemplateControl("dropAgente") as ASPxComboBox;
        ASPxDateEdit textDataHistorico = gridCadastro.FindEditFormTemplateControl("textDataHistorico") as ASPxDateEdit;
        ASPxButtonEdit btnEditAtivoBolsa = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa") as ASPxButtonEdit;
        ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
        ASPxComboBox dropTipoCarteira = gridCadastro.FindEditFormTemplateControl("dropTipoCarteira") as ASPxComboBox;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(btnEditCodigoCliente);
        controles.Add(dropAgente);
        controles.Add(textDataHistorico);
        controles.Add(btnEditAtivoBolsa);
        controles.Add(textQuantidade);
        controles.Add(dropTipoCarteira);
        
        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
        int idAgente = Convert.ToInt32(dropAgente.SelectedItem.Value);
        DateTime dataHistorico = Convert.ToDateTime(textDataHistorico.Text);
        string cdAtivoBolsa = Convert.ToString(btnEditAtivoBolsa.Text);
        int tipoCarteira = Convert.ToInt32(dropTipoCarteira.SelectedItem.Value);
        decimal quantidade = Convert.ToDecimal(textQuantidade.Text);
        
        if (this.clientePosicao == null)
        {
            this.clientePosicao = new ClientePosicao();
            this.CarregaDados();
        }

        if (dataHistorico > this.clientePosicao.dataDia)
        {
            e.Result = "Data informada é maior que a data dia atual do cliente.";
            return;
        }

        if (dataHistorico < this.clientePosicao.dataImplantacao)
        {
            e.Result = "Data informada é menor que a data de implantação do cliente.";
            return;
        }

        if (gridCadastro.IsNewRowEditing) 
        {
            PosicaoBolsaDetalhe posicaoBolsaDetalhe = new PosicaoBolsaDetalhe();
            if (posicaoBolsaDetalhe.LoadByPrimaryKey(dataHistorico, idCliente, idAgente, cdAtivoBolsa, tipoCarteira))
            {
                e.Result = "Registro já existente";
                return;
            }
        }

        bool historico = this.clientePosicao.historico;
        
        decimal quantidadePosicao = this.clientePosicao.quantidadePosicao;
        decimal quantidadePosicaoBloqueada = this.clientePosicao.quantidadePosicaoBloqueada;
        
        if (quantidade > quantidadePosicao)
        {
            e.Result = "Quantidade informada maior que a quantidade total do cliente.";
            return;
        }
                
        PosicaoBolsaDetalhe posicaoBolsaDetalheBloqueada = new PosicaoBolsaDetalhe();
        posicaoBolsaDetalheBloqueada.Query.Select(posicaoBolsaDetalheBloqueada.Query.Quantidade.Sum());
        posicaoBolsaDetalheBloqueada.Query.Where(posicaoBolsaDetalheBloqueada.Query.DataHistorico.Equal(dataHistorico),
                                                 posicaoBolsaDetalheBloqueada.Query.IdCliente.Equal(idCliente),
                                                 posicaoBolsaDetalheBloqueada.Query.IdAgente.Equal(idAgente),
                                                 posicaoBolsaDetalheBloqueada.Query.CdAtivoBolsa.Equal(cdAtivoBolsa),
                                                 posicaoBolsaDetalheBloqueada.Query.TipoCarteira.NotIn((int)TipoCarteiraBovespa.Livre,
                                                                                                        tipoCarteira));
        posicaoBolsaDetalheBloqueada.Query.Load();

        decimal quantidadeCarteiraBloqueada = posicaoBolsaDetalheBloqueada.Quantidade.HasValue ? posicaoBolsaDetalheBloqueada.Quantidade.Value : 0;

        if ((quantidade + quantidadeCarteiraBloqueada) > quantidadePosicaoBloqueada)
        {
            e.Result = "Quantidade de carteira bloqueada total informada maior que a quantidade bloqueada total da posição (" + quantidadePosicaoBloqueada + ") do cliente.";
            return;
        }        

        
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textDataHistorico_Init(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing)
            (sender as ASPxDateEdit).Enabled = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditAtivoBolsa_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
        {
            (sender as ASPxButtonEdit).Enabled = false;
            (sender as ASPxButtonEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditCodigoCliente_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
        {
            (sender as ASPxSpinEdit).Enabled = false;
            (sender as ASPxSpinEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropAgente_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
        {
            (sender as ASPxComboBox).Enabled = false;
            (sender as ASPxComboBox).BackColor = Color.FromName("#EBEBEB");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropTipoCarteira_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
        {
            (sender as ASPxComboBox).Enabled = false;
            (sender as ASPxComboBox).BackColor = Color.FromName("#EBEBEB");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOKAdd_Init(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) 
    {
        if (e.Column.FieldName == "CompositeKey") {
            //Compose a primary key value
            string idCliente = Convert.ToString(e.GetListSourceFieldValue(PosicaoBolsaDetalheMetadata.ColumnNames.IdCliente));
            string idAgente = Convert.ToString(e.GetListSourceFieldValue(PosicaoBolsaDetalheMetadata.ColumnNames.IdAgente));
            string dataHistorico = Convert.ToString(e.GetListSourceFieldValue(PosicaoBolsaDetalheMetadata.ColumnNames.DataHistorico));
            string cdAtivoBolsa = Convert.ToString(e.GetListSourceFieldValue(PosicaoBolsaDetalheMetadata.ColumnNames.CdAtivoBolsa));
            string tipoCarteira = Convert.ToString(e.GetListSourceFieldValue(PosicaoBolsaDetalheMetadata.ColumnNames.TipoCarteira));

            e.Value = idCliente + idAgente + dataHistorico + cdAtivoBolsa + tipoCarteira;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) 
    {
        PosicaoBolsaDetalhe posicaoBolsaDetalhe = new PosicaoBolsaDetalhe();

        ASPxDateEdit textDataHistorico = gridCadastro.FindEditFormTemplateControl("textDataHistorico") as ASPxDateEdit;
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;                
        ASPxComboBox dropAgente = gridCadastro.FindEditFormTemplateControl("dropAgente") as ASPxComboBox;
        ASPxButtonEdit btnEditAtivoBolsa = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa") as ASPxButtonEdit;
        ASPxComboBox dropTipoCarteira = gridCadastro.FindEditFormTemplateControl("dropTipoCarteira") as ASPxComboBox;
        ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;

        DateTime dataHistorico = Convert.ToDateTime(textDataHistorico.Text);
        int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
        int idAgente = Convert.ToInt32(dropAgente.SelectedItem.Value);
        string cdAtivoBolsa = Convert.ToString(btnEditAtivoBolsa.Text);
        int tipoCarteira = Convert.ToInt32(dropTipoCarteira.SelectedItem.Value);
        decimal quantidade = Convert.ToDecimal(textQuantidade.Text);

        if (posicaoBolsaDetalhe.LoadByPrimaryKey(dataHistorico, idCliente, idAgente, cdAtivoBolsa, tipoCarteira))
        {
            if (this.clientePosicao == null)
            {
                this.clientePosicao = new ClientePosicao();
                this.CarregaDados();
            }

            posicaoBolsaDetalhe.Quantidade = quantidade;
            
            posicaoBolsaDetalhe.Save();

            this.RebalanceiaCarteiras();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de PosicaoBolsaDetalhe - Operacao: Update PosicaoBolsaDetalhe: " + UtilitarioWeb.ToString(posicaoBolsaDetalhe),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    private void SalvarNovo() 
    {
        PosicaoBolsaDetalhe posicaoBolsaDetalhe = new PosicaoBolsaDetalhe();

        ASPxDateEdit textDataHistorico = gridCadastro.FindEditFormTemplateControl("textDataHistorico") as ASPxDateEdit;
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxComboBox dropAgente = gridCadastro.FindEditFormTemplateControl("dropAgente") as ASPxComboBox;
        ASPxButtonEdit btnEditAtivoBolsa = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa") as ASPxButtonEdit;
        ASPxComboBox dropTipoCarteira = gridCadastro.FindEditFormTemplateControl("dropTipoCarteira") as ASPxComboBox;
        ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;

        DateTime dataHistorico = Convert.ToDateTime(textDataHistorico.Text);
        int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
        int idAgente = Convert.ToInt32(dropAgente.SelectedItem.Value);
        string cdAtivoBolsa = Convert.ToString(btnEditAtivoBolsa.Text);
        int tipoCarteira = Convert.ToInt32(dropTipoCarteira.SelectedItem.Value);
        decimal quantidade = Convert.ToDecimal(textQuantidade.Text);

        posicaoBolsaDetalhe.CdAtivoBolsa = cdAtivoBolsa;
        posicaoBolsaDetalhe.DataHistorico = dataHistorico;
        posicaoBolsaDetalhe.IdAgente = idAgente;
        posicaoBolsaDetalhe.IdCliente = idCliente;
        posicaoBolsaDetalhe.Quantidade = quantidade;
        posicaoBolsaDetalhe.QuantidadeAbertura = quantidade;
        posicaoBolsaDetalhe.TipoCarteira = tipoCarteira;

        posicaoBolsaDetalhe.Save();
        

        this.RebalanceiaCarteiras();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de PosicaoBolsaDetalhe - Operacao: Insert PosicaoBolsaDetalhe: " + UtilitarioWeb.ToString(posicaoBolsaDetalhe),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    private void RebalanceiaCarteiras()
    {
        PosicaoBolsaDetalhe posicaoBolsaDetalhe = new PosicaoBolsaDetalhe();

        ASPxDateEdit textDataHistorico = gridCadastro.FindEditFormTemplateControl("textDataHistorico") as ASPxDateEdit;
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxComboBox dropAgente = gridCadastro.FindEditFormTemplateControl("dropAgente") as ASPxComboBox;
        ASPxButtonEdit btnEditAtivoBolsa = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa") as ASPxButtonEdit;
        ASPxComboBox dropTipoCarteira = gridCadastro.FindEditFormTemplateControl("dropTipoCarteira") as ASPxComboBox;
        ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;

        DateTime dataHistorico = Convert.ToDateTime(textDataHistorico.Text);
        int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
        int idAgente = Convert.ToInt32(dropAgente.SelectedItem.Value);
        string cdAtivoBolsa = Convert.ToString(btnEditAtivoBolsa.Text);
        int tipoCarteira = Convert.ToInt32(dropTipoCarteira.SelectedItem.Value);
        decimal quantidade = Convert.ToDecimal(textQuantidade.Text);
                
        if (this.clientePosicao == null)
        {
            this.clientePosicao = new ClientePosicao();
            this.CarregaDados();
        }

        PosicaoBolsaDetalhe posicaoBolsaDetalheLivre = new PosicaoBolsaDetalhe();
        if (posicaoBolsaDetalheLivre.LoadByPrimaryKey(dataHistorico, idCliente, idAgente, cdAtivoBolsa, (int)TipoCarteiraBovespa.Livre))
        {
            posicaoBolsaDetalheLivre.Quantidade = this.clientePosicao.quantidadePosicaoLivre;            
        }
        else
        {
            posicaoBolsaDetalheLivre = new PosicaoBolsaDetalhe();
            posicaoBolsaDetalheLivre.CdAtivoBolsa = cdAtivoBolsa;
            posicaoBolsaDetalheLivre.DataHistorico = dataHistorico;
            posicaoBolsaDetalheLivre.IdAgente = idAgente;
            posicaoBolsaDetalheLivre.IdCliente = idCliente;
            posicaoBolsaDetalheLivre.Quantidade = this.clientePosicao.quantidadePosicaoLivre;
            posicaoBolsaDetalheLivre.QuantidadeAbertura = this.clientePosicao.quantidadePosicaoLivre;
            posicaoBolsaDetalheLivre.TipoCarteira = (int)TipoCarteiraBovespa.Livre;

        }

        posicaoBolsaDetalheLivre.Save();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) 
    {
        if (e.Parameters == "btnDelete") 
        {
            List<object> keyValuesIdCliente = gridCadastro.GetSelectedFieldValues(PosicaoBolsaDetalheMetadata.ColumnNames.IdCliente);
            List<object> keyValuesIdAgente = gridCadastro.GetSelectedFieldValues(PosicaoBolsaDetalheMetadata.ColumnNames.IdAgente);
            List<object> keyValuesDataHistorico = gridCadastro.GetSelectedFieldValues(PosicaoBolsaDetalheMetadata.ColumnNames.DataHistorico);
            List<object> keyValuesCdAtivoBolsa = gridCadastro.GetSelectedFieldValues(PosicaoBolsaDetalheMetadata.ColumnNames.CdAtivoBolsa);
            List<object> keyValuesTipoCarteira = gridCadastro.GetSelectedFieldValues(PosicaoBolsaDetalheMetadata.ColumnNames.TipoCarteira);
            
            for (int i = 0; i < keyValuesIdCliente.Count; i++) 
            {
                int idCliente = Convert.ToInt32(keyValuesIdCliente[i]);
                int idAgente = Convert.ToInt32(keyValuesIdAgente[i]);
                DateTime dataHistorico = Convert.ToDateTime(keyValuesDataHistorico[i]);
                string cdAtivoBolsa = Convert.ToString(keyValuesCdAtivoBolsa[i]);
                int tipoCarteira = Convert.ToInt32(keyValuesTipoCarteira[i]);

                PosicaoBolsaDetalhe posicaoBolsaDetalhe = new PosicaoBolsaDetalhe();
                if (posicaoBolsaDetalhe.LoadByPrimaryKey(dataHistorico, idCliente, idAgente, cdAtivoBolsa, tipoCarteira)) 
                {
                    //
                    PosicaoBolsaDetalhe posicaoBolsaDetalheClone = (PosicaoBolsaDetalhe)Utilitario.Clone(posicaoBolsaDetalhe);
                    //

                    posicaoBolsaDetalhe.MarkAsDeleted();
                    posicaoBolsaDetalhe.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de PosicaoBolsaDetalhe - Operacao: Delete PosicaoBolsaDetalhe: " + posicaoBolsaDetalheClone.IdCliente +
                                                                        "; " + posicaoBolsaDetalheClone.IdAgente + 
                                                                        "; " + posicaoBolsaDetalheClone.DataHistorico + 
                                                                        "; " + posicaoBolsaDetalheClone.TipoCarteira +
                                                                        "; " + posicaoBolsaDetalheClone.CdAtivoBolsa +
                                                                        UtilitarioWeb.ToString(posicaoBolsaDetalheClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing) {
            ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
            e.Properties["cpBtnEditCodigoCliente"] = btnEditCodigoCliente.ClientID;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("btnEditCodigoCliente");
        base.gridCadastro_PreRender(sender, e);
    }
}