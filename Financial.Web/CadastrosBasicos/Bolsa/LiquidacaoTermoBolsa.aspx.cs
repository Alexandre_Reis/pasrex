﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using Financial.Investidor;
using Financial.ContaCorrente;
using System.Drawing;
using Financial.ContaCorrente.Enums;
using Financial.Security;
using Financial.Fundo;
using Financial.InvestidorCotista;
using Financial.InvestidorCotista.Enums;
using Financial.Investidor.Enums;
using Financial.Common.Enums;
using Financial.Bolsa;
using Financial.Bolsa.Enums;
using DevExpress.Web.Data;
using Financial.Web.Common;
using EntitySpaces.Interfaces;
using System.Threading;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_LiquidacaoTermoBolsa : CadastroBasePage {
    private int selectedIndex;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasFiltro = true;
        this.HasPopupCliente = true;
        this.HasPanelFieldsLoading = true;
        base.Page_Load(sender, e);

        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
                          new List<string>(new string[] { LiquidacaoTermoBolsaMetadata.ColumnNames.TipoLiquidacao }));

        this.gridCadastro.ClientSideEvents.RowDblClick = "function(s, e) {selectedIndex = e.visibleIndex; gridCadastro.GetRowValues(e.visibleIndex, 'TipoLiquidacao', OpenPopup); }";
    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSLiquidacaoTermoBolsa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
        LiquidacaoTermoBolsaQuery liquidacaoTermoBolsaQuery = new LiquidacaoTermoBolsaQuery("L");
        ClienteQuery clienteQuery = new ClienteQuery("C");

        liquidacaoTermoBolsaQuery.Select(liquidacaoTermoBolsaQuery, clienteQuery.Apelido.As("Apelido"));
        liquidacaoTermoBolsaQuery.InnerJoin(clienteQuery).On(liquidacaoTermoBolsaQuery.IdCliente == clienteQuery.IdCliente);
        liquidacaoTermoBolsaQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        liquidacaoTermoBolsaQuery.Where(permissaoClienteQuery.IdUsuario.Equal(idUsuario));

        if (!String.IsNullOrEmpty(btnEditCodigoClienteFiltro.Text)) {
            liquidacaoTermoBolsaQuery.Where(liquidacaoTermoBolsaQuery.IdCliente.Equal(Convert.ToInt32(btnEditCodigoClienteFiltro.Text)));
        }

        if (!String.IsNullOrEmpty(textDataInicio.Text)) {
            liquidacaoTermoBolsaQuery.Where(liquidacaoTermoBolsaQuery.DataMovimento.GreaterThanOrEqual(textDataInicio.Text));
        }

        if (!String.IsNullOrEmpty(textDataFim.Text)) {
            liquidacaoTermoBolsaQuery.Where(liquidacaoTermoBolsaQuery.DataMovimento.LessThanOrEqual(textDataFim.Text));
        }

        if (!String.IsNullOrEmpty(textCdAtivo.Text)) {
            liquidacaoTermoBolsaQuery.Where(liquidacaoTermoBolsaQuery.CdAtivoBolsa.Like("%" + textCdAtivo.Text + "%"));
        }

        liquidacaoTermoBolsaQuery.OrderBy(liquidacaoTermoBolsaQuery.DataMovimento.Descending);

        LiquidacaoTermoBolsaCollection coll = new LiquidacaoTermoBolsaCollection();
        coll.Load(liquidacaoTermoBolsaQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSPosicaoTermoBolsa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxComboBox dropAgente = gridCadastro.FindEditFormTemplateControl("dropAgente") as ASPxComboBox;

        if (btnEditCodigoCliente != null && dropAgente != null) {
            if (btnEditCodigoCliente.Text == "" || dropAgente.SelectedIndex == -1) {
                e.Collection = new PosicaoTermoBolsaCollection();
                return;
            }
        }
        else {
            e.Collection = new PosicaoTermoBolsaCollection();
            return;
        }

        Cliente cliente = new Cliente();
        List<esQueryItem> campos = new List<esQueryItem>();
        campos.Add(cliente.Query.DataDia);
        cliente.LoadByPrimaryKey(campos, Convert.ToInt32(btnEditCodigoCliente.Text));
        DateTime data = cliente.DataDia.Value;

        PosicaoTermoBolsaCollection coll = new PosicaoTermoBolsaCollection();

        coll.Query.Where(coll.Query.IdCliente.Equal(Convert.ToInt32(btnEditCodigoCliente.Text)));
        coll.Query.Where(coll.Query.IdAgente.Equal(Convert.ToInt32(dropAgente.SelectedItem.Value)));

        coll.Query.OrderBy(coll.Query.CdAtivoBolsa.Ascending, coll.Query.DataVencimento.Descending);

        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ClienteCollection coll = new ClienteCollection();
        coll.BuscaClientesComAcesso(Context.User.Identity.Name);
        
        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSAgenteMercado_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        AgenteMercadoCollection coll = new AgenteMercadoCollection();
        coll.Query.Where(coll.Query.FuncaoCustodiante.Equal("S"));
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSAtivoBolsa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        AtivoBolsaCollection coll = new AtivoBolsaCollection();
        coll.Query.Where(coll.Query.TipoMercado.Equal(Convert.ToString(TipoMercadoBolsa.Termo)));
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void panelEdicao_Load(object sender, EventArgs e) {
        UtilitarioGrid.SetaCorCombosPopup(sender as Control);

        if (gridCadastro.IsEditing && gridCadastro.IsNewRowEditing) {
            Label labelQuantidade = gridCadastro.FindEditFormTemplateControl("labelQuantidade") as Label;
            ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
            Label labelDataLiquidacao = gridCadastro.FindEditFormTemplateControl("labelDataLiquidacao") as Label;
            ASPxDateEdit textDataLiquidacao = gridCadastro.FindEditFormTemplateControl("textDataLiquidacao") as ASPxDateEdit;
            //ImageButton ImgBntCalcLiquidacao = gridCadastro.FindEditFormTemplateControl("ImgBntCalcLiquidacao") as ImageButton;

            labelQuantidade.Visible = false;
            textQuantidade.Visible = false;
            labelDataLiquidacao.Visible = false;
            textDataLiquidacao.Visible = false;
            //ImgBntCalcLiquidacao.Visible = false;
            LinkButton btnOK = gridCadastro.FindEditFormTemplateControl("btnOK") as LinkButton;
            btnOK.Visible = false;
        }
        else if (gridCadastro.IsEditing && gridCadastro.IsNewRowEditing) {
            LinkButton btnPosicaoTermoBolsa = gridCadastro.FindEditFormTemplateControl("btnPosicaoTermoBolsa") as LinkButton;
            btnPosicaoTermoBolsa.Visible = false;
        }

        bool travaPainel = false;
        if (gridCadastro.IsEditing && !gridCadastro.IsNewRowEditing && Convert.ToString(Session["FormLoad"]) != "S") {
            Session["FormLoad"] = "S";
            Label labelEdicao = gridCadastro.FindEditFormTemplateControl("labelEdicao") as Label;

            DateTime dataMovimento = Convert.ToDateTime(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "DataMovimento"));
            int idCliente = Convert.ToInt32(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "IdCliente"));

            Cliente cliente = new Cliente();
            if (cliente.LoadByPrimaryKey(idCliente)) {
                byte status = cliente.Status.Value;
                if (status == (byte)StatusCliente.Divulgado) {
                    travaPainel = true;
                    labelEdicao.Text = "Cliente está fechado. Dados não podem ser alterados.";
                }
                else if (DateTime.Compare(cliente.DataDia.Value, dataMovimento) > 0) {
                    travaPainel = true;
                    labelEdicao.Text = "Data do lançamento da liquidação anterior à data atual do cliente. Dados não podem ser alterados.";
                }
            }
        }

        if (travaPainel) {
            LinkButton btnOK = gridCadastro.FindEditFormTemplateControl("btnOK") as LinkButton;
            btnOK.Visible = false;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";

        if (!gridCadastro.IsEditing) //Testa deleção
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(LiquidacaoTermoBolsaMetadata.ColumnNames.IdCliente);
            List<object> keyDataMovimento = gridCadastro.GetSelectedFieldValues(LiquidacaoTermoBolsaMetadata.ColumnNames.DataMovimento);
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idCliente = Convert.ToInt32(keyValuesId[i]);
                DateTime dataMovimento = Convert.ToDateTime(keyDataMovimento[i]);

                Cliente cliente = new Cliente();
                if (cliente.LoadByPrimaryKey(idCliente)) {
                    DateTime dataDia = cliente.DataDia.Value;
                    byte status = cliente.Status.Value;
                    string clienteDescricao = cliente.Apelido + " (" + cliente.IdCliente.ToString() + ")";
                    if (status == (byte)StatusCliente.Divulgado) {
                        e.Result = "Cliente " + clienteDescricao + " está fechado! Deleção cancelada.";
                        return;
                    }
                    else if (DateTime.Compare(dataDia, dataMovimento) > 0) {
                        e.Result = "Lançamento com data " + dataMovimento.ToString().Substring(0, 10) + " anterior à data do cliente " + clienteDescricao + "! Deleção cancelada.";
                        return;
                    }
                }
            }
        }
        else {
            ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
            ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
            ASPxComboBox dropAgente = gridCadastro.FindEditFormTemplateControl("dropAgente") as ASPxComboBox;
            ASPxDateEdit textDataLiquidacao = gridCadastro.FindEditFormTemplateControl("textDataLiquidacao") as ASPxDateEdit;

            #region Campos obrigatórios
            List<Control> controles = new List<Control>();
            controles.Add(btnEditCodigoCliente);
            controles.Add(textData);
            controles.Add(dropAgente);
            controles.Add(textDataLiquidacao);

            if (base.TestaObrigatorio(controles) != "") {
                e.Result = "Campos com * são obrigatórios!";
                return;
            }
            #endregion

            int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);

            Cliente cliente = new Cliente();
            if (cliente.LoadByPrimaryKey(idCliente)) {
                byte status = cliente.Status.Value;
                if (status == (byte)StatusCliente.Divulgado) {
                    e.Result = "Cliente está fechado! Operação não pode ser realizada.";
                    return;
                }

                DateTime dataLiquidacao = Convert.ToDateTime(textDataLiquidacao.Text);

                if (!Calendario.IsDiaUtil(dataLiquidacao, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil)) {
                    e.Result = "Data da liquidação não é dia útil.";
                    return;
                }

                if (DateTime.Compare(cliente.DataDia.Value, dataLiquidacao) > 0) {
                    e.Result = "Data da liquidação deve ser maior ou igual à data atual do cliente.";
                    return;
                }
            }

            if (gridCadastro.IsEditing && !gridCadastro.IsNewRowEditing) {
                TextBox hiddenIdPosicao = gridCadastro.FindEditFormTemplateControl("hiddenIdPosicao") as TextBox;

                int idPosicao = Convert.ToInt32(hiddenIdPosicao.Text);
                DateTime data = Convert.ToDateTime(textData.Text);

                PosicaoTermoBolsaAbertura posicaoTermoBolsaAbertura = new PosicaoTermoBolsaAbertura();
                if (posicaoTermoBolsaAbertura.LoadByPrimaryKey(idPosicao, data)) {
                    ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;

                    List<Control> controlesAux = new List<Control>();
                    controlesAux.Add(btnEditCodigoCliente);
                    controlesAux.Add(dropAgente);
                    controlesAux.Add(textQuantidade);
                    controlesAux.Add(textDataLiquidacao);

                    if (base.TestaObrigatorio(controlesAux) != "") {
                        e.Result = "Campos com * são obrigatórios!";
                        return;
                    }

                    if (posicaoTermoBolsaAbertura.Quantidade.Value < Convert.ToDecimal(textQuantidade.Text)) {
                        e.Result = "Quantidade a ser antecipada é maior que a quantidade em posição! Operação não pode ser realizada.";
                        return;
                    }
                }
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErroPosicao_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";

        ASPxSpinEdit textQuantidade = popupPosicaoTermoBolsa.FindControl("textQuantidade") as ASPxSpinEdit;

        if (textQuantidade.Text == "") {
            e.Result = "A Quantidade deve ser preenchida.";
            return;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        string resultado = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") {
                if (cliente.IsAtivo) {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name))
                    {
                        nome = cliente.str.Apelido;
                        DateTime dataDia = cliente.DataDia.Value;
                        byte status = cliente.Status.Value;

                        if (status == (byte)StatusCliente.Divulgado && gridCadastro.IsEditing) {
                            resultado = cliente.str.Apelido + "|status_closed";
                        }
                        else {
                            resultado = nome + "|" + dataDia.ToString().Substring(0, 10) + "|" + Thread.CurrentThread.CurrentCulture.Name;
                        }
                    }
                    else {
                        resultado = "no_access";
                    }
                }
                else {
                    resultado = "no_active";
                }
            }
        }
        e.Result = resultado;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="selectedIndex"></param>
    protected void InsertLiquidacao(int selectedIndex) {
        ASPxPopupControl popupPosicaoTermoBolsa = this.FindControl("popupPosicaoTermoBolsa") as ASPxPopupControl;
        ASPxGridView gridPosicaoTermoBolsa = popupPosicaoTermoBolsa.FindControl("gridPosicaoTermoBolsa") as ASPxGridView;
        ASPxSpinEdit textQuantidade = popupPosicaoTermoBolsa.FindControl("textQuantidade") as ASPxSpinEdit;
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxComboBox dropAgente = gridCadastro.FindEditFormTemplateControl("dropAgente") as ASPxComboBox;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;

        int idPosicao = Convert.ToInt32(gridPosicaoTermoBolsa.GetRowValues(selectedIndex, "IdPosicao"));
        string cdAtivoBolsa = Convert.ToString(gridPosicaoTermoBolsa.GetRowValues(selectedIndex, "CdAtivoBolsa"));
        decimal quantidade = Convert.ToDecimal(textQuantidade.Text);
        DateTime data = Convert.ToDateTime(textData.Text);

        //Checa qtde já antecipada vs quantidade de abertura
        PosicaoTermoBolsaAbertura posicaoTermoBolsaAbertura = new PosicaoTermoBolsaAbertura();
        decimal quantidadePosicaoAbertura = posicaoTermoBolsaAbertura.RetornaQuantidadePosicao(idPosicao, data);
        LiquidacaoTermoBolsa liquidacaoTermoBolsaChecar = new LiquidacaoTermoBolsa();
        decimal quantidadeLiquidada = liquidacaoTermoBolsaChecar.RetornaTotalLiquidado(idPosicao, data);

        //Se as qtdes liquidada e da posição tiverem sinais trocados, não atualiza nada (erro de digitação)
        if ((quantidade > 0 && quantidadePosicaoAbertura < 0) ||
            (quantidade < 0 && quantidadePosicaoAbertura > 0)) {
            return;
        }

        if (quantidadePosicaoAbertura > 0) {
            if (quantidade > (quantidadePosicaoAbertura - quantidadeLiquidada)) {
                quantidade = quantidadePosicaoAbertura - quantidadeLiquidada;
            }
        }
        else {
            if (Math.Abs(quantidade) > (Math.Abs(quantidadePosicaoAbertura) - Math.Abs(quantidadeLiquidada))) {
                quantidade = (Math.Abs(quantidadePosicaoAbertura) - Math.Abs(quantidadeLiquidada)) * -1;
            }
        }
        //

        if (quantidade != 0) {
            //Busca o PU da posição do termo (PU contratado). Calcula Valor da antecipação.
            decimal puTermo = 0;
            decimal valorAntecipacao = 0;
            PosicaoTermoBolsa posicaoTermoBolsa = new PosicaoTermoBolsa();
            if (posicaoTermoBolsa.LoadByPrimaryKey(idPosicao)) {
                puTermo = posicaoTermoBolsa.PUTermo.Value;
                decimal fatorCotacao = 1; //Default para o caso de não ter fator de cotação cadastrado
                FatorCotacaoBolsa fatorCotacaoBolsa = new FatorCotacaoBolsa();
                if (fatorCotacaoBolsa.BuscaFatorCotacaoBolsa(cdAtivoBolsa, data)) {
                    fatorCotacao = fatorCotacaoBolsa.Fator.Value;
                }

                valorAntecipacao = Utilitario.Truncate(puTermo * quantidade / fatorCotacao, 2);
            }
            //

            string numeroContrato = "";
            if (gridPosicaoTermoBolsa.GetRowValues(selectedIndex, "NumeroContrato").ToString() != "") {
                numeroContrato = Convert.ToString(gridPosicaoTermoBolsa.GetRowValues(selectedIndex, "NumeroContrato"));
            }

            //Calcula data de liquidação
            DateTime dataLiquidacao = data;
            int diasLiquidacao = 0;
            if (textDiasLiquidacao.Text != "") {
                diasLiquidacao = Convert.ToInt32(textDiasLiquidacao.Text);
            }

            if (diasLiquidacao > 0) {
                if (diasLiquidacao < 0) {
                    diasLiquidacao = 0;
                }

                if (diasLiquidacao > 3) {
                    diasLiquidacao = 3;
                }

                dataLiquidacao = Calendario.AdicionaDiaUtil(data, diasLiquidacao, LocalFeriadoFixo.Bovespa, TipoFeriado.Brasil);
            }
            //

            LiquidacaoTermoBolsa liquidacaoTermoBolsa = new LiquidacaoTermoBolsa();
            liquidacaoTermoBolsa.IdPosicao = idPosicao;
            liquidacaoTermoBolsa.IdCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
            liquidacaoTermoBolsa.CdAtivoBolsa = cdAtivoBolsa;
            liquidacaoTermoBolsa.IdAgente = Convert.ToInt32(dropAgente.SelectedItem.Value);
            liquidacaoTermoBolsa.DataMovimento = data;
            liquidacaoTermoBolsa.DataLiquidacao = dataLiquidacao;
            liquidacaoTermoBolsa.Quantidade = quantidade;
            liquidacaoTermoBolsa.NumeroContrato = numeroContrato;
            liquidacaoTermoBolsa.TipoLiquidacao = (byte)TipoLiquidacaoTermoBolsa.Antecipada;
            liquidacaoTermoBolsa.Fonte = (byte)FonteLiquidacaoTermoBolsa.Manual;
            liquidacaoTermoBolsa.Pu = puTermo;
            liquidacaoTermoBolsa.Valor = valorAntecipacao;

            liquidacaoTermoBolsa.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de LiquidacaoTermoBolsa - Operacao: Insert LiquidacaoTermoBolsa: " + liquidacaoTermoBolsa.IdLiquidacao + UtilitarioWeb.ToString(liquidacaoTermoBolsa),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion

            //Atualiza StatusRealTime para executar***********
            Cliente cliente = new Cliente();
            cliente.SetaFlagRealTime(liquidacaoTermoBolsa.IdCliente.Value, (byte)StatusRealTimeCliente.Executar);
            //************************************************
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        TextBox hiddenIdLiquidacao = gridCadastro.FindEditFormTemplateControl("hiddenIdLiquidacao") as TextBox;

        int idLiquidacao = Convert.ToInt32(hiddenIdLiquidacao.Text);

        ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
        ASPxDateEdit textDataLiquidacao = gridCadastro.FindEditFormTemplateControl("textDataLiquidacao") as ASPxDateEdit;

        LiquidacaoTermoBolsa liquidacaoTermoBolsa = new LiquidacaoTermoBolsa();
        if (liquidacaoTermoBolsa.LoadByPrimaryKey(idLiquidacao)) {
            liquidacaoTermoBolsa.DataLiquidacao = Convert.ToDateTime(textDataLiquidacao.Text);
            liquidacaoTermoBolsa.Quantidade = Convert.ToDecimal(textQuantidade.Text);
            liquidacaoTermoBolsa.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de liquidacaoTermoBolsa - Operacao: Update liquidacaoTermoBolsa: " + idLiquidacao + UtilitarioWeb.ToString(liquidacaoTermoBolsa),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion

            //Atualiza StatusRealTime para executar***********
            Cliente cliente = new Cliente();
            cliente.SetaFlagRealTime(liquidacaoTermoBolsa.IdCliente.Value, (byte)StatusRealTimeCliente.Executar);
            //************************************************
        }
       
        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    //protected void UpdateLiquidacao() {
    //    TextBox hiddenIdLiquidacao = gridCadastro.FindEditFormTemplateControl("hiddenIdLiquidacao") as TextBox;

    //    int idLiquidacao = Convert.ToInt32(hiddenIdLiquidacao.Text);

    //    ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
    //    ASPxDateEdit textDataLiquidacao = gridCadastro.FindEditFormTemplateControl("textDataLiquidacao") as ASPxDateEdit;

    //    LiquidacaoTermoBolsa liquidacaoTermoBolsa = new LiquidacaoTermoBolsa();
    //    if (liquidacaoTermoBolsa.LoadByPrimaryKey(idLiquidacao)) {
    //        liquidacaoTermoBolsa.DataLiquidacao = Convert.ToDateTime(textDataLiquidacao.Text);
    //        liquidacaoTermoBolsa.Quantidade = Convert.ToDecimal(textQuantidade.Text);
    //        liquidacaoTermoBolsa.Save();

    //        #region Log do Processo
    //        HistoricoLog historicoLog = new HistoricoLog();
    //        historicoLog.InsereHistoricoLog(DateTime.Now,
    //                                        DateTime.Now,
    //                                        "Cadastro de liquidacaoTermoBolsa - Operacao: Update liquidacaoTermoBolsa: " + idLiquidacao + UtilitarioWeb.ToString(liquidacaoTermoBolsa),
    //                                        HttpContext.Current.User.Identity.Name,
    //                                        UtilitarioWeb.GetIP(Request),
    //                                        "",
    //                                        HistoricoLogOrigem.Outros);
    //        #endregion

    //        //Atualiza StatusRealTime para executar***********
    //        Cliente cliente = new Cliente();
    //        cliente.SetaFlagRealTime(liquidacaoTermoBolsa.IdCliente.Value, (byte)StatusRealTimeCliente.Executar);
    //        //************************************************
    //    }

    //    gridCadastro.CancelEdit();
    //    gridCadastro.DataBind();
    //}

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(LiquidacaoTermoBolsaMetadata.ColumnNames.IdLiquidacao);
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idLiquidacao = Convert.ToInt32(keyValuesId[i]);

                LiquidacaoTermoBolsa liquidacaoTermoBolsa = new LiquidacaoTermoBolsa();
                if (liquidacaoTermoBolsa.LoadByPrimaryKey(idLiquidacao)) {
                    int idCliente = liquidacaoTermoBolsa.IdCliente.Value;

                    LiquidacaoTermoBolsa liquidacaoTermoBolsaClone = (LiquidacaoTermoBolsa)Utilitario.Clone(liquidacaoTermoBolsa);
                    //
                    
                    liquidacaoTermoBolsa.MarkAsDeleted();
                    liquidacaoTermoBolsa.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de LiquidacaoTermoBolsa - Operacao: Delete LiquidacaoTermoBolsa: " + idLiquidacao + UtilitarioWeb.ToString(liquidacaoTermoBolsaClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion

                    //Atualiza StatusRealTime para executar***********
                    Cliente cliente = new Cliente();
                    cliente.SetaFlagRealTime(idCliente, (byte)StatusRealTimeCliente.Executar);
                    //************************************************
                }
            }
        }
        //else if (e.Parameters == "btnUpdate") {
        //    UpdateLiquidacao();
        //}

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CancelRowEditing(object sender, ASPxStartRowEditingEventArgs e) {
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing) {
            ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
            e.Properties["cpBtnEditCodigoCliente"] = btnEditCodigoCliente.ClientID;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("btnEditCodigoCliente");
        base.gridCadastro_PreRender(sender, e);
        
        ASPxDateEdit textDataInicio = popupFiltro.FindControl("textDataInicio") as ASPxDateEdit;
        ASPxDateEdit textDataFim = popupFiltro.FindControl("textDataFim") as ASPxDateEdit;
        ASPxSpinEdit btnEditCodigoClienteFiltro = popupFiltro.FindControl("btnEditCodigoClienteFiltro") as ASPxSpinEdit;

        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        Label labelFiltroHeader = gridCadastro.FindStatusBarTemplateControl("labelFiltroHeader") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");
        if (btnEditCodigoClienteFiltro != null && btnEditCodigoClienteFiltro.Text != "") {
            texto.Append(" Cliente: ").Append(btnEditCodigoClienteFiltro.Text);
        }
        if (textDataInicio.Text != "") {
            if (texto.Length > 0) {
                texto.Append(" |");
            }
            texto.Append(" Data Início >= ").Append(textDataInicio.Text.Substring(0, 10));
        }
        if (textDataFim.Text != "") {
            if (texto.Length > 0) {
                texto.Append(" |");
            }
            texto.Append(" Data Fim <= ").Append(textDataFim.Text.Substring(0, 10));
        }
        if (textCdAtivo.Text != "") {
            if (texto.Length > 0) {
                texto.Append(" |");
            }
            texto.Append(" Ativo Like %").Append(textCdAtivo.Text).Append("%");
        }

        labelFiltro.Text = texto.ToString();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridPosicaoTermoBolsa_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "") //Usado apenas na hora q abre inicialmente a popup, para dar refresh nas posições
        {
            gridPosicaoTermoBolsa.DataBind();
        }
        else {
            selectedIndex = Convert.ToInt32(e.Parameters);
            InsertLiquidacao(selectedIndex);
            gridPosicaoTermoBolsa.DataBind();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridPosicaoTermoBolsa_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e) {
        UtilitarioGrid.SetaCoresGrid(e);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridPosicaoTermoBolsa_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) {
        if (e.Column.FieldName == "QuantidadeAntecipada") {
            ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;

            int idPosicao = Convert.ToInt32(e.GetListSourceFieldValue("IdPosicao"));
            LiquidacaoTermoBolsa liquidacaoTermoBolsa = new LiquidacaoTermoBolsa();
            decimal quantidadeLiquidada = liquidacaoTermoBolsa.RetornaTotalLiquidado(idPosicao, Convert.ToDateTime(textData.Text));

            e.Value = quantidadeLiquidada;
        }
    }
}