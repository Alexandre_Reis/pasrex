﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using Financial.Bolsa;
using Financial.Bolsa.Enums;
using Financial.Web.Common;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_ProventoBolsa : CadastroBasePage {

    new protected void Page_Load(object sender, EventArgs e) {
        this.HasPopupAtivoBolsa = true;
        this.HasFiltro = true;
        base.Page_Load(sender, e);


        if (!Page.IsPostBack && !Page.IsCallback)
        {
            List<string> textos = new List<string>(new string[] {
            "Dividendo",
            "Rest. Capital",
            "Bonificação em $",
            "Juros c/ Capital",
            "Rendimento",
            "Juros",
            "Amortização",
            "Rest.Capital com Ações",
            "Crédito Frações",
            "Rendimento Líquido"
        });

            List<Object> keys = new List<Object>(new Object[] { 10, 11, 12, 13, 14, 16, 17, 22, 72, 98 });

            // Carrega tooltip no grid na coluna TipoProvento
            UtilitarioGrid.CarregaTooltipListEditItem(this.gridCadastro, ProventoBolsaMetadata.ColumnNames.TipoProvento, textos, keys);
        }

        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
                          new List<string>(new string[] { ProventoBolsaMetadata.ColumnNames.TipoProvento }));
    }

    #region DataSources
    protected void EsDSProventoBolsa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ProventoBolsaCollection coll = new ProventoBolsaCollection();

        ASPxDateEdit textDataInicio = popupFiltro.FindControl("textDataInicio") as ASPxDateEdit;
        ASPxDateEdit textDataFim = popupFiltro.FindControl("textDataFim") as ASPxDateEdit;

        if (textDataInicio.Text != "") {
            coll.Query.Where(coll.Query.DataLancamento.GreaterThanOrEqual(textDataInicio.Text));
        }

        if (textDataFim.Text != "") {
            coll.Query.Where(coll.Query.DataLancamento.LessThanOrEqual(textDataFim.Text));
        }

        if (!String.IsNullOrEmpty(textCdAtivo.Text)) {
            coll.Query.Where(coll.Query.CdAtivoBolsa.Like("%" + textCdAtivo.Text + "%"));
        }

        coll.Query.OrderBy(coll.Query.DataLancamento.Descending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSAtivoBolsa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        AtivoBolsaCollection coll = new AtivoBolsaCollection();
        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";
        ASPxButtonEdit btnEditAtivoBolsa = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa") as ASPxButtonEdit;
        ASPxDateEdit textDataLancamento = gridCadastro.FindEditFormTemplateControl("textDataLancamento") as ASPxDateEdit;
        ASPxDateEdit textDataEx = gridCadastro.FindEditFormTemplateControl("textDataEx") as ASPxDateEdit;
        ASPxDateEdit textDataPagamento = gridCadastro.FindEditFormTemplateControl("textDataPagamento") as ASPxDateEdit;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
        ASPxComboBox dropTipoProvento = gridCadastro.FindEditFormTemplateControl("dropTipoProvento") as ASPxComboBox;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(btnEditAtivoBolsa);
        controles.Add(textDataLancamento);
        controles.Add(textDataEx);
        controles.Add(textDataPagamento);
        controles.Add(textValor);
        controles.Add(dropTipoProvento);

        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        AtivoBolsa ativoBolsa = new AtivoBolsa();
        if (!ativoBolsa.LoadByPrimaryKey(btnEditAtivoBolsa.Text.ToString())) {
            e.Result = "Ativo não existente!";
            return;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOKAdd_Init(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string cdAtivoBolsa = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            AtivoBolsa ativoBolsa = new AtivoBolsa();
            ativoBolsa.LoadByPrimaryKey(Convert.ToString(e.Parameter));

            if (ativoBolsa.str.CdAtivoBolsa != "") {
                cdAtivoBolsa = ativoBolsa.str.CdAtivoBolsa;
            }
        }
        e.Result = cdAtivoBolsa;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        SalvarNovo();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    private void SalvarNovo() {
        ASPxButtonEdit btnEditAtivoBolsa = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa") as ASPxButtonEdit;
        ASPxDateEdit textDataLancamento = gridCadastro.FindEditFormTemplateControl("textDataLancamento") as ASPxDateEdit;
        ASPxDateEdit textDataEx = gridCadastro.FindEditFormTemplateControl("textDataEx") as ASPxDateEdit;
        ASPxDateEdit textDataPagamento = gridCadastro.FindEditFormTemplateControl("textDataPagamento") as ASPxDateEdit;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
        ASPxComboBox dropTipoProvento = gridCadastro.FindEditFormTemplateControl("dropTipoProvento") as ASPxComboBox;

        ProventoBolsa proventoBolsa = new ProventoBolsa();

        proventoBolsa.CdAtivoBolsa = btnEditAtivoBolsa.Text.ToString().ToUpper();
        proventoBolsa.DataLancamento = Convert.ToDateTime(textDataLancamento.Text);
        proventoBolsa.DataEx = Convert.ToDateTime(textDataEx.Text);
        proventoBolsa.DataPagamento = Convert.ToDateTime(textDataPagamento.Text);
        proventoBolsa.Valor = Convert.ToDecimal(textValor.Text);
        proventoBolsa.TipoProvento = Convert.ToByte(dropTipoProvento.SelectedItem.Value);

        proventoBolsa.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de ProventoBolsa - Operacao: Insert ProventoBolsa: " + proventoBolsa.IdProvento + UtilitarioWeb.ToString(proventoBolsa),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        int idProvento = (int)e.Keys[0];

        ASPxButtonEdit btnEditAtivoBolsa = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa") as ASPxButtonEdit;
        ASPxDateEdit textDataLancamento = gridCadastro.FindEditFormTemplateControl("textDataLancamento") as ASPxDateEdit;
        ASPxDateEdit textDataEx = gridCadastro.FindEditFormTemplateControl("textDataEx") as ASPxDateEdit;
        ASPxDateEdit textDataPagamento = gridCadastro.FindEditFormTemplateControl("textDataPagamento") as ASPxDateEdit;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
        ASPxComboBox dropTipoProvento = gridCadastro.FindEditFormTemplateControl("dropTipoProvento") as ASPxComboBox;

        ProventoBolsa proventoBolsa = new ProventoBolsa();

        if (proventoBolsa.LoadByPrimaryKey(idProvento)) {
            proventoBolsa.CdAtivoBolsa = btnEditAtivoBolsa.Text.ToString().ToUpper();
            proventoBolsa.DataLancamento = Convert.ToDateTime(textDataLancamento.Text);
            proventoBolsa.DataEx = Convert.ToDateTime(textDataEx.Text);
            proventoBolsa.DataPagamento = Convert.ToDateTime(textDataPagamento.Text);
            proventoBolsa.Valor = Convert.ToDecimal(textValor.Text);
            proventoBolsa.TipoProvento = Convert.ToByte(dropTipoProvento.SelectedItem.Value);

            proventoBolsa.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de ProventoBolsa - Operacao: Update ProventoBolsa: " + idProvento + UtilitarioWeb.ToString(proventoBolsa),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValues1 = gridCadastro.GetSelectedFieldValues("IdProvento");
            for (int i = 0; i < keyValues1.Count; i++) {
                int idProvento = Convert.ToInt32(keyValues1[i]);

                ProventoBolsa proventoBolsa = new ProventoBolsa();
                if (proventoBolsa.LoadByPrimaryKey(idProvento)) {

                    ProventoBolsa proventoBolsaClone = (ProventoBolsa)Utilitario.Clone(proventoBolsa);
                    //
                    proventoBolsa.MarkAsDeleted();
                    proventoBolsa.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de ProventoBolsa - Operacao: Delete ProventoBolsa: " + idProvento + UtilitarioWeb.ToString(proventoBolsaClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing) {
            ASPxButtonEdit btnEditAtivoBolsa = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa") as ASPxButtonEdit;
            e.Properties["cpBtnEditAtivoBolsa"] = btnEditAtivoBolsa.ClientID;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("btnEditAtivoBolsa");
        base.gridCadastro_PreRender(sender, e);

        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        Label labelFiltroHeader = gridCadastro.FindStatusBarTemplateControl("labelFiltroHeader") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");
        if (textDataInicio.Text != "") {
            texto.Append(" Data Início >= ").Append(textDataInicio.Text.Substring(0, 10));
        }
        if (textDataFim.Text != "") {
            if (textDataInicio.Text != "")
                texto.Append(" |");

            texto.Append(" Data Fim <= ").Append(textDataFim.Text.Substring(0, 10));
        }

        labelFiltro.Text = texto.ToString();
    }
}