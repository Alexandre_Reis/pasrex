using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Bolsa;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_TabelaCorretagemBolsa : Financial.Web.Common.CadastroBasePage
{
    #region DataSources
    protected void EsDSTabelaCorretagemBolsa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        TabelaCorretagemBolsaCollection coll = new TabelaCorretagemBolsaCollection();

        coll.Query.OrderBy(coll.Query.Faixa.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSTemplateCorretagemBolsa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        TemplateCorretagemBolsaCollection coll = new TemplateCorretagemBolsaCollection();

        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        TabelaCorretagemBolsa tabelaCorretagemBolsa = new TabelaCorretagemBolsa();

        int idTabela = Convert.ToInt32(e.Keys[0]);

        if (tabelaCorretagemBolsa.LoadByPrimaryKey(idTabela))
        {
            tabelaCorretagemBolsa.IdTemplate = Convert.ToInt32(e.NewValues["IdTemplate"]);
            tabelaCorretagemBolsa.Faixa = Convert.ToDecimal(e.NewValues["Faixa"]);
            tabelaCorretagemBolsa.Percentual = Convert.ToDecimal(e.NewValues["Percentual"]);
            tabelaCorretagemBolsa.ValorAdicional = Convert.ToDecimal(e.NewValues["ValorAdicional"]);
            
            tabelaCorretagemBolsa.Save();
            //
            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de TabelaCorretagemBolsa - Operacao: Update TabelaCorretagemBolsa: " + idTabela + UtilitarioWeb.ToString(tabelaCorretagemBolsa),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        TabelaCorretagemBolsa tabelaCorretagemBolsa = new TabelaCorretagemBolsa();

        tabelaCorretagemBolsa.IdTemplate = Convert.ToInt32(e.NewValues["IdTemplate"]);
        tabelaCorretagemBolsa.Faixa = Convert.ToDecimal(e.NewValues["Faixa"]);
        tabelaCorretagemBolsa.Percentual = Convert.ToDecimal(e.NewValues["Percentual"]);
        tabelaCorretagemBolsa.ValorAdicional = Convert.ToDecimal(e.NewValues["ValorAdicional"]);

        tabelaCorretagemBolsa.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de TabelaCorretagemBolsa - Operacao: Insert TabelaCorretagemBolsa: " + tabelaCorretagemBolsa.IdTabela + UtilitarioWeb.ToString(tabelaCorretagemBolsa),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
                
        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdTabela");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idTabela = Convert.ToInt32(keyValuesId[i]);

                TabelaCorretagemBolsa tabelaCorretagemBolsa = new TabelaCorretagemBolsa();
                if (tabelaCorretagemBolsa.LoadByPrimaryKey(idTabela))
                {
                    TabelaCorretagemBolsa tabelaCorretagemBolsaClone = (TabelaCorretagemBolsa)Utilitario.Clone(tabelaCorretagemBolsa);
                    //

                    tabelaCorretagemBolsa.MarkAsDeleted();
                    tabelaCorretagemBolsa.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de TabelaCorretagemBolsa - Operacao: Delete TabelaCorretagemBolsa: " + idTabela + UtilitarioWeb.ToString(tabelaCorretagemBolsaClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

}

