﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Bolsa;
using Financial.Web.Common;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_DistribuicaoBolsa : CadastroBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasFiltro = true;
        this.HasPopupAtivoBolsa = true;
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSDistribuicaoBolsa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        DistribuicaoBolsaCollection coll = new DistribuicaoBolsaCollection();

        ASPxDateEdit textDataInicio = popupFiltro.FindControl("textDataInicio") as ASPxDateEdit;
        ASPxDateEdit textDataFim = popupFiltro.FindControl("textDataFim") as ASPxDateEdit;
        TextBox textCdAtivo = popupFiltro.FindControl("textCdAtivo") as TextBox;

        if (textDataInicio.Text != "") {
            coll.Query.Where(coll.Query.DataReferencia.GreaterThanOrEqual(textDataInicio.Text));
        }

        if (textDataFim.Text != "") {
            coll.Query.Where(coll.Query.DataReferencia.LessThanOrEqual(textDataFim.Text));
        }

        if (textCdAtivo.Text != "") {
            coll.Query.Where(coll.Query.CdAtivoBolsa.Like("%" + textCdAtivo.Text + "%"));
        }

        coll.Query.OrderBy(coll.Query.DataReferencia.Descending, coll.Query.CdAtivoBolsa.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSAtivoBolsa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        AtivoBolsaCollection coll = new AtivoBolsaCollection();
        coll.Query.OrderBy(coll.Query.CdAtivoBolsa.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string cdAtivoBolsa = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            AtivoBolsa ativoBolsa = new AtivoBolsa();
            ativoBolsa.LoadByPrimaryKey(Convert.ToString(e.Parameter));

            if (ativoBolsa.str.CdAtivoBolsa != "") {
                cdAtivoBolsa = ativoBolsa.str.CdAtivoBolsa;
            }
        }
        e.Result = cdAtivoBolsa;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";
        ASPxButtonEdit btnEditAtivoBolsa = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa") as ASPxButtonEdit;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxSpinEdit textDistribuicao = gridCadastro.FindEditFormTemplateControl("textDistribuicao") as ASPxSpinEdit;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(btnEditAtivoBolsa);
        controles.Add(textData);
        controles.Add(textDistribuicao);

        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        if (gridCadastro.IsNewRowEditing) {
            string cdAtivoBolsa = Convert.ToString(btnEditAtivoBolsa.Text);
            DateTime data = Convert.ToDateTime(textData.Text);

            DistribuicaoBolsa distribuicaoBolsa = new DistribuicaoBolsa();
            if (distribuicaoBolsa.LoadByPrimaryKey(data, cdAtivoBolsa)) {
                e.Result = "Registro já existente";
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textData_Init(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing)
            (sender as ASPxDateEdit).Enabled = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditAtivoBolsa_Load(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxButtonEdit).Enabled = false;
            (sender as ASPxButtonEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) {
        if (e.Column.FieldName == "CompositeKey") {
            //Compose a primary key value
            string data = Convert.ToString(e.GetListSourceFieldValue(DistribuicaoBolsaMetadata.ColumnNames.DataReferencia));
            string cdAtivoBolsa = Convert.ToString(e.GetListSourceFieldValue(DistribuicaoBolsaMetadata.ColumnNames.CdAtivoBolsa));
            e.Value = data + cdAtivoBolsa;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        DistribuicaoBolsa distribuicaoBolsa = new DistribuicaoBolsa();

        ASPxButtonEdit btnEditAtivoBolsa = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa") as ASPxButtonEdit;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxSpinEdit textDistribuicao = gridCadastro.FindEditFormTemplateControl("textDistribuicao") as ASPxSpinEdit;

        string cdAtivoBolsa = Convert.ToString(btnEditAtivoBolsa.Text);
        DateTime data = Convert.ToDateTime(textData.Text);
        int distribuicao = Convert.ToInt32(textDistribuicao.Text);
        if (distribuicaoBolsa.LoadByPrimaryKey(data, cdAtivoBolsa)) {
            distribuicaoBolsa.Distribuicao = distribuicao;
            distribuicaoBolsa.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de DistribuicaoBolsa - Operacao: Update DistribuicaoBolsa: " + cdAtivoBolsa + "; " + data + UtilitarioWeb.ToString(distribuicaoBolsa),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        DistribuicaoBolsa distribuicaoBolsa = new DistribuicaoBolsa();

        ASPxButtonEdit btnEditAtivoBolsa = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa") as ASPxButtonEdit;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxSpinEdit textDistribuicao = gridCadastro.FindEditFormTemplateControl("textDistribuicao") as ASPxSpinEdit;

        string cdAtivoBolsa = Convert.ToString(btnEditAtivoBolsa.Text);
        DateTime data = Convert.ToDateTime(textData.Text);
        int distribuicao = Convert.ToInt32(textDistribuicao.Text);

        distribuicaoBolsa.CdAtivoBolsa = cdAtivoBolsa.ToUpper();
        distribuicaoBolsa.DataReferencia = data;
        distribuicaoBolsa.Distribuicao = distribuicao;
        distribuicaoBolsa.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de DistribuicaoBolsa - Operacao: Insert DistribuicaoBolsa: " + distribuicaoBolsa.CdAtivoBolsa + "; " + distribuicaoBolsa.DataReferencia + UtilitarioWeb.ToString(distribuicaoBolsa),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(DistribuicaoBolsaMetadata.ColumnNames.CdAtivoBolsa);
            List<object> keyValuesData = gridCadastro.GetSelectedFieldValues(DistribuicaoBolsaMetadata.ColumnNames.DataReferencia);
            for (int i = 0; i < keyValuesData.Count; i++) {
                string cdAtivoBolsa = Convert.ToString(keyValuesId[i]);
                DateTime data = Convert.ToDateTime(keyValuesData[i]);

                DistribuicaoBolsa distribuicaoBolsa = new DistribuicaoBolsa();
                if (distribuicaoBolsa.LoadByPrimaryKey(data, cdAtivoBolsa)) {
                    //
                    DistribuicaoBolsa distribuicaoBolsaClone = (DistribuicaoBolsa)Utilitario.Clone(distribuicaoBolsa);
                    //                    
                    distribuicaoBolsa.MarkAsDeleted();
                    distribuicaoBolsa.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de DistribuicaoBolsa - Operacao: Delete DistribuicaoBolsa: " + cdAtivoBolsa + "; " + data + UtilitarioWeb.ToString(distribuicaoBolsaClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing) {
            ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
            e.Properties["cpTextData"] = textData.ClientID;

            ASPxSpinEdit textDistribuicao = gridCadastro.FindEditFormTemplateControl("textDistribuicao") as ASPxSpinEdit;
            e.Properties["cpTextDistribuicao"] = textDistribuicao.ClientID;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("textData", "textDistribuicao");
        base.gridCadastro_PreRender(sender, e);

        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");
        if (textDataInicio.Text != "") {
            texto.Append(" Data Início >= ").Append(textDataInicio.Text.Substring(0, 10));
        }
        if (textDataFim.Text != "") {
            if (texto.Length > 0) {
                texto.Append(" |");
            }

            texto.Append(" Data Fim <= ").Append(textDataFim.Text.Substring(0, 10));
        }
        if (textCdAtivo.Text != "") {
            if (texto.Length > 0) {
                texto.Append(" |");
            }

            texto.Append(" Ativo Like %").Append(textCdAtivo.Text).Append("%");
        }

        labelFiltro.Text = texto.ToString();
    }
}