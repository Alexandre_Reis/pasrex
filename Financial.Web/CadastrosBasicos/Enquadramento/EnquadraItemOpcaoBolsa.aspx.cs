using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using DevExpress.Web;

using Financial.Util;
using Financial.Common;
using Financial.Enquadra;

using Financial.Enquadra.Enums;

using EntitySpaces.Interfaces;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_EnquadraItemOpcaoBolsa : Financial.Web.Common.CadastroBasePage {
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSEnquadraItemOpcaoBolsa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        EnquadraItemQuery enquadraItemQuery = new EnquadraItemQuery("I");
        EnquadraItemOpcaoBolsaQuery enquadraItemOpcaoBolsaQuery = new EnquadraItemOpcaoBolsaQuery("A");

        enquadraItemQuery.Select(enquadraItemOpcaoBolsaQuery, enquadraItemQuery.Descricao);
        enquadraItemQuery.InnerJoin(enquadraItemOpcaoBolsaQuery).On(enquadraItemQuery.IdItem == enquadraItemOpcaoBolsaQuery.IdItem);
        enquadraItemQuery.OrderBy(enquadraItemQuery.Descricao.Ascending);

        EnquadraItemCollection coll = new EnquadraItemCollection();
        coll.Load(enquadraItemQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        EnquadraItem enquadraItem = new EnquadraItem();
        EnquadraItemOpcaoBolsa enquadraItemOpcaoBolsa = new EnquadraItemOpcaoBolsa();

        if (enquadraItem.LoadByPrimaryKey((int)e.Keys[0])) 
        {
            enquadraItem.Descricao = Convert.ToString(e.NewValues[EnquadraItemMetadata.ColumnNames.Descricao]);
            enquadraItem.Save();

            if (enquadraItemOpcaoBolsa.LoadByPrimaryKey((int)e.Keys[0]))
            {
                if (e.NewValues["Posicao"] != null)
                {
                    enquadraItemOpcaoBolsa.Posicao = Convert.ToString(e.NewValues["Posicao"]).Trim().ToUpper();
                }
                else
                {
                    enquadraItemOpcaoBolsa.Posicao = null;
                }

                enquadraItemOpcaoBolsa.Save();
            }

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de EnquadraItemOpcaoBolsa - Operacao: Update EnquadraItemOpcaoBolsa: " + (int)e.Keys[0] + UtilitarioWeb.ToString(enquadraItemOpcaoBolsa),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        EnquadraItem enquadraItem = new EnquadraItem();
        EnquadraItemOpcaoBolsa enquadraItemOpcaoBolsa = new EnquadraItemOpcaoBolsa();

        enquadraItem.Descricao = Convert.ToString(e.NewValues[EnquadraItemMetadata.ColumnNames.Descricao]);
        enquadraItem.Tipo = (int)TipoItemEnquadra.OpcaoBolsa;

        using (esTransactionScope scope = new esTransactionScope()) 
        {
            enquadraItem.Save();

            if (e.NewValues["Posicao"] != null)
            {
                enquadraItemOpcaoBolsa.Posicao = Convert.ToString(e.NewValues["Posicao"]).Trim().ToUpper();
            }
            else
            {
                enquadraItemOpcaoBolsa.Posicao = null;
            }

            enquadraItemOpcaoBolsa.IdItem = enquadraItem.IdItem.Value; //Usa o mesmo Id da tabela EnquadraItem!

            enquadraItemOpcaoBolsa.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de EnquadraItemOpcaoBolsa - Operacao: Insert EnquadraItemOpcaoBolsa: " + enquadraItemOpcaoBolsa.IdItem + UtilitarioWeb.ToString(enquadraItemOpcaoBolsa),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion

            scope.Complete();
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        
        if (e.Parameters == "btnDelete") 
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(EnquadraItemMetadata.ColumnNames.IdItem);
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idItem = Convert.ToInt32(keyValuesId[i]);

                using (esTransactionScope scope = new esTransactionScope()) {
                    EnquadraItemOpcaoBolsa enquadraItemOpcaoBolsa = new EnquadraItemOpcaoBolsa();
                    if (enquadraItemOpcaoBolsa.LoadByPrimaryKey(idItem)) {

                        //
                        EnquadraItemOpcaoBolsa enquadraItemOpcaoBolsaClone = (EnquadraItemOpcaoBolsa)Utilitario.Clone(enquadraItemOpcaoBolsa);
                        //
                                               
                        enquadraItemOpcaoBolsa.MarkAsDeleted();
                        enquadraItemOpcaoBolsa.Save();

                        #region Log do Processo
                        HistoricoLog historicoLog = new HistoricoLog();
                        historicoLog.InsereHistoricoLog(DateTime.Now,
                                                        DateTime.Now,
                                                        "Cadastro de EnquadraItemOpcaoBolsa - Operacao: Delete EnquadraItemOpcaoBolsa: " + idItem + UtilitarioWeb.ToString(enquadraItemOpcaoBolsaClone),
                                                        HttpContext.Current.User.Identity.Name,
                                                        UtilitarioWeb.GetIP(Request),
                                                        "",
                                                        HistoricoLogOrigem.Outros);
                        #endregion
                    }

                    EnquadraItem enquadraItem = new EnquadraItem();
                    if (enquadraItem.LoadByPrimaryKey(idItem)) {
                        enquadraItem.MarkAsDeleted();
                        enquadraItem.Save();
                    }
                    scope.Complete();
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }
}