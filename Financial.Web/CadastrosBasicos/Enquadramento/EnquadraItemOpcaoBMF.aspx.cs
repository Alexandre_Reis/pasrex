﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Enquadra;
using Financial.Enquadra.Enums;
using EntitySpaces.Interfaces;
using Financial.BMF;
using Financial.Web.Common;
using Financial.Web.Util;
using Financial.Security;
using Financial.Security.Enums;

public partial class CadastrosBasicos_EnquadraItemOpcaoBMF : CadastroBasePage {
    // TODO - Colocar no Resource
    static readonly string mensagemErroCodigoBMFInexistente = "Código BMF Não Existente";

    protected void EsDSEnquadraItemOpcaoBMF_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        EnquadraItemQuery enquadraItemQuery = new EnquadraItemQuery("I");
        EnquadraItemOpcaoBMFQuery enquadraItemOpcaoBMFQuery = new EnquadraItemOpcaoBMFQuery("A");

        enquadraItemQuery.Select(enquadraItemOpcaoBMFQuery, enquadraItemQuery.Descricao);
        enquadraItemQuery.InnerJoin(enquadraItemOpcaoBMFQuery).On(enquadraItemQuery.IdItem == enquadraItemOpcaoBMFQuery.IdItem);
        enquadraItemQuery.OrderBy(enquadraItemQuery.Descricao.Ascending);

        EnquadraItemCollection coll = new EnquadraItemCollection();
        coll.Load(enquadraItemQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) {
        if (e.Column.FieldName == "CdAtivoBMF") {
            e.Value = Convert.ToString(e.GetListSourceFieldValue("CdAtivoBMF"));
        }
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        EnquadraItem enquadraItem = new EnquadraItem();
        EnquadraItemOpcaoBMF enquadraItemOpcaoBMF = new EnquadraItemOpcaoBMF();

        using (esTransactionScope scope = new esTransactionScope()) {
            if (enquadraItem.LoadByPrimaryKey((int)e.Keys[0])) {
                enquadraItem.Descricao = Convert.ToString(e.NewValues["Descricao"]);
                enquadraItem.Save();
            }

            if (e.Keys[0] != null) {
                if (enquadraItemOpcaoBMF.LoadByPrimaryKey((int)e.Keys[0])) {
                    if (e.NewValues["CdAtivoBMF"] != null) {
                        AtivoBMFCollection ativoBMFCollection = new AtivoBMFCollection();
                        ativoBMFCollection.Query.Select(ativoBMFCollection.Query.CdAtivoBMF)
                                                .Where(ativoBMFCollection.Query.CdAtivoBMF == Convert.ToString(e.NewValues["CdAtivoBMF"]).Trim().ToUpper());

                        if (!ativoBMFCollection.Query.Load()) {
                            throw new Exception(mensagemErroCodigoBMFInexistente);
                        }

                        enquadraItemOpcaoBMF.CdAtivoBMF = Convert.ToString(e.NewValues["CdAtivoBMF"]).Trim().ToUpper();
                    }
                    else {
                        enquadraItemOpcaoBMF.CdAtivoBMF = null;
                    }

                    enquadraItemOpcaoBMF.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de EnquadraItemOpcaoBMF - Operacao: Update EnquadraItemOpcaoBMF: " + (int)e.Keys[0] + UtilitarioWeb.ToString(enquadraItemOpcaoBMF),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }

            scope.Complete();
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        EnquadraItem enquadraItem = new EnquadraItem();
        EnquadraItemOpcaoBMF enquadraItemOpcaoBMF = new EnquadraItemOpcaoBMF();

        enquadraItem.Descricao = Convert.ToString(e.NewValues["Descricao"]);
        enquadraItem.Tipo = (int)TipoItemEnquadra.OpcaoBMF;

        using (esTransactionScope scope = new esTransactionScope()) {
            enquadraItem.Save();

            enquadraItemOpcaoBMF.IdItem = enquadraItem.IdItem.Value; //Usa o mesmo Id da tabela EnquadraItem!

            if (e.NewValues["CdAtivoBMF"] != null) {
                AtivoBMFCollection ativoBMFCollection = new AtivoBMFCollection();
                ativoBMFCollection.Query.Select(ativoBMFCollection.Query.CdAtivoBMF)
                                        .Where(ativoBMFCollection.Query.CdAtivoBMF == Convert.ToString(e.NewValues["CdAtivoBMF"]).Trim().ToUpper());

                if (!ativoBMFCollection.Query.Load()) {
                    throw new Exception(mensagemErroCodigoBMFInexistente);
                }

                enquadraItemOpcaoBMF.CdAtivoBMF = Convert.ToString(e.NewValues["CdAtivoBMF"]).Trim().ToUpper();
            }
            else {
                enquadraItemOpcaoBMF.CdAtivoBMF = null;
            }

            enquadraItemOpcaoBMF.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de EnquadraItemOpcaoBMF - Operacao: Insert EnquadraItemOpcaoBMF: " + enquadraItemOpcaoBMF.IdItem + UtilitarioWeb.ToString(enquadraItemOpcaoBMF),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion

            scope.Complete();
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {

        if (e.Parameters == "btnDelete") {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdItem");
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idItem = Convert.ToInt32(keyValuesId[i]);

                using (esTransactionScope scope = new esTransactionScope()) {
                    EnquadraItemOpcaoBMF enquadraItemOpcaoBMF = new EnquadraItemOpcaoBMF();
                    if (enquadraItemOpcaoBMF.LoadByPrimaryKey(idItem)) {

                        //
                        EnquadraItemOpcaoBMF enquadraItemOpcaoBMFClone = (EnquadraItemOpcaoBMF)Utilitario.Clone(enquadraItemOpcaoBMF);
                        //

                        enquadraItemOpcaoBMF.MarkAsDeleted();
                        enquadraItemOpcaoBMF.Save();

                        #region Log do Processo
                        HistoricoLog historicoLog = new HistoricoLog();
                        historicoLog.InsereHistoricoLog(DateTime.Now,
                                                        DateTime.Now,
                                                        "Cadastro de EnquadraItemOpcaoBMF - Operacao: Delete EnquadraItemOpcaoBMF: " + idItem + UtilitarioWeb.ToString(enquadraItemOpcaoBMFClone),
                                                        HttpContext.Current.User.Identity.Name,
                                                        UtilitarioWeb.GetIP(Request),
                                                        "",
                                                        HistoricoLogOrigem.Outros);
                        #endregion
                    }

                    EnquadraItem enquadraItem = new EnquadraItem();
                    if (enquadraItem.LoadByPrimaryKey(idItem)) {
                        enquadraItem.MarkAsDeleted();
                        enquadraItem.Save();
                    }
                    scope.Complete();
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }
}