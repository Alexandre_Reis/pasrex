﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using Financial.Bolsa;
using Financial.RendaFixa;
using Financial.Enquadra;
using Financial.Enquadra.Enums;
using EntitySpaces.Interfaces;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Web.Common;

public partial class CadastrosBasicos_EnquadraItemRendaFixa : CadastroBasePage {

    new protected void Page_Load(object sender, EventArgs e) {
        base.Page_Load(sender, e);

        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
                          new List<string>(new string[] { EnquadraItemRendaFixaMetadata.ColumnNames.OperacaoCompromissada }));
    }

    #region DataSources
    protected void EsDSEnquadraItemRendaFixa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        EnquadraItemRendaFixaQuery enquadraItemRendaFixaQuery = new EnquadraItemRendaFixaQuery("E");
        EnquadraItemQuery enquadraItemQuery = new EnquadraItemQuery("I");
        EmissorQuery emissorQuery = new EmissorQuery("M");

        enquadraItemRendaFixaQuery.Select(enquadraItemRendaFixaQuery, enquadraItemQuery.Descricao, emissorQuery.TipoEmissor);
        enquadraItemRendaFixaQuery.InnerJoin(enquadraItemQuery).On(enquadraItemQuery.IdItem == enquadraItemRendaFixaQuery.IdItem);
        enquadraItemRendaFixaQuery.LeftJoin(emissorQuery).On(emissorQuery.IdEmissor == enquadraItemRendaFixaQuery.IdEmissor);
        enquadraItemRendaFixaQuery.OrderBy(enquadraItemQuery.Descricao.Ascending);

        EnquadraItemRendaFixaCollection coll = new EnquadraItemRendaFixaCollection();
        coll.Load(enquadraItemRendaFixaQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSEmissor_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        EmissorCollection coll = new EmissorCollection();

        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSIndice_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        IndiceCollection coll = new IndiceCollection();

        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSPapelRendaFixa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        PapelRendaFixaCollection coll = new PapelRendaFixaCollection();

        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }


    protected void EsDSEmissorInner_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        EmissorCollection coll = new EmissorCollection();

        EnquadraItemRendaFixaQuery enquadraItemRendaFixaQuery = new EnquadraItemRendaFixaQuery("F");
        EmissorQuery emissorQuery = new EmissorQuery("e");

        emissorQuery.Select(emissorQuery.IdEmissor, emissorQuery.Nome);
        emissorQuery.InnerJoin(enquadraItemRendaFixaQuery).On(emissorQuery.IdEmissor == enquadraItemRendaFixaQuery.IdEmissor);
        emissorQuery.es.Distinct = true;

        emissorQuery.OrderBy(emissorQuery.Nome.Ascending);
        coll.Load(emissorQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSIndiceInner_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        IndiceCollection coll = new IndiceCollection();

        EnquadraItemRendaFixaQuery enquadraItemRendaFixaQuery = new EnquadraItemRendaFixaQuery("E");
        IndiceQuery indiceQuery = new IndiceQuery("I");

        indiceQuery.Select(indiceQuery.IdIndice, indiceQuery.Descricao);
        indiceQuery.InnerJoin(enquadraItemRendaFixaQuery).On(indiceQuery.IdIndice == enquadraItemRendaFixaQuery.IdIndice);
        indiceQuery.OrderBy(indiceQuery.Descricao.Ascending);

        coll.Load(indiceQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSPapelRendaFixaInner_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        PapelRendaFixaCollection coll = new PapelRendaFixaCollection();

        EnquadraItemRendaFixaQuery enquadraItemRendaFixaQuery = new EnquadraItemRendaFixaQuery("E");
        PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("Q");

        papelRendaFixaQuery.Select(papelRendaFixaQuery.IdPapel, papelRendaFixaQuery.Descricao);
        papelRendaFixaQuery.InnerJoin(enquadraItemRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == enquadraItemRendaFixaQuery.IdPapel);
        papelRendaFixaQuery.OrderBy(papelRendaFixaQuery.Descricao.Ascending);

        coll.Load(papelRendaFixaQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    #endregion

    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) {
        if (e.Column.FieldName == "Descricao") {
            e.Value = Convert.ToString(e.GetListSourceFieldValue(EnquadraItemMetadata.ColumnNames.Descricao));
        }
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        EnquadraItem enquadraItem = new EnquadraItem();
        EnquadraItemRendaFixa enquadraItemRendaFixa = new EnquadraItemRendaFixa();

        ASPxTextBox txtDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
        ASPxComboBox dropTipoRentabilidade = gridCadastro.FindEditFormTemplateControl("dropTipoRentabilidade") as ASPxComboBox;
        ASPxComboBox dropTipoPapel = gridCadastro.FindEditFormTemplateControl("dropTipoPapel") as ASPxComboBox;
        ASPxComboBox dropOperacaoCompromissada = gridCadastro.FindEditFormTemplateControl("dropOperacaoCompromissada") as ASPxComboBox;
        ASPxComboBox dropIndice = gridCadastro.FindEditFormTemplateControl("dropIndice") as ASPxComboBox;
        ASPxComboBox dropPapel = gridCadastro.FindEditFormTemplateControl("dropPapel") as ASPxComboBox;
        ASPxComboBox dropEmissor = gridCadastro.FindEditFormTemplateControl("dropEmissor") as ASPxComboBox;
        ASPxComboBox dropTipoEmissor = gridCadastro.FindEditFormTemplateControl("dropTipoEmissor") as ASPxComboBox;
        ASPxComboBox dropBloqueado = gridCadastro.FindEditFormTemplateControl("dropBloqueado") as ASPxComboBox;
        
        using (esTransactionScope scope = new esTransactionScope()) {
            if (enquadraItem.LoadByPrimaryKey((int)e.Keys[0])) {
                enquadraItem.Descricao = Convert.ToString(txtDescricao.Text);
                enquadraItem.Save();
            }

            if (enquadraItemRendaFixa.LoadByPrimaryKey((int)e.Keys[0])) {

                //Tipo Papel
                if (dropTipoPapel.SelectedIndex > -1)
                {
                    enquadraItemRendaFixa.TipoPapel = Convert.ToByte(dropTipoPapel.Value);
                }
                else
                {
                    enquadraItemRendaFixa.TipoPapel = null;
                }

                //Tipo Rentabilidade
                if (dropTipoRentabilidade.SelectedIndex > -1)
                {
                    enquadraItemRendaFixa.TipoRentabilidade = Convert.ToByte(dropTipoRentabilidade.Value);
                }
                else
                {
                    enquadraItemRendaFixa.TipoRentabilidade = null;
                }

                //Compromissada
                if (dropOperacaoCompromissada.SelectedIndex > -1)
                {
                    enquadraItemRendaFixa.OperacaoCompromissada = Convert.ToString(dropOperacaoCompromissada.Value);
                }
                else
                {
                    enquadraItemRendaFixa.OperacaoCompromissada = null;
                }

                //Índice
                if (dropIndice.SelectedIndex > -1)
                {
                    enquadraItemRendaFixa.IdIndice = Convert.ToInt16(dropIndice.Value);
                }
                else
                {
                    enquadraItemRendaFixa.IdIndice = null;
                }

                //Papel
                if (dropPapel.SelectedIndex > -1)
                {
                    enquadraItemRendaFixa.IdPapel = Convert.ToInt32(dropPapel.Value);
                }
                else
                {
                    enquadraItemRendaFixa.IdPapel = null;
                }

                //Emissor
                if (dropEmissor.SelectedIndex > -1)
                {
                    enquadraItemRendaFixa.IdEmissor = Convert.ToInt32(dropEmissor.Value);
                }
                else
                {
                    enquadraItemRendaFixa.IdEmissor = null;
                }

                //Tipo Emissor
                if (dropTipoEmissor.SelectedIndex > -1)
                {
                    enquadraItemRendaFixa.TipoEmissor = Convert.ToByte(dropTipoEmissor.Value);
                }
                else
                {
                    enquadraItemRendaFixa.TipoEmissor = null;
                }

                //Bloqueado
                if (dropBloqueado.SelectedIndex > -1)
                {
                    enquadraItemRendaFixa.Bloqueado = Convert.ToString(dropBloqueado.Value);
                }
                else
                {
                    enquadraItemRendaFixa.Bloqueado = null;
                }

                enquadraItemRendaFixa.Save();

                #region Log do Processo
                HistoricoLog historicoLog = new HistoricoLog();
                historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                "Cadastro de EnquadraItemRendaFixa - Operacao: Update EnquadraItemRendaFixa: " + (int)e.Keys[0] + UtilitarioWeb.ToString(enquadraItemRendaFixa),
                                                HttpContext.Current.User.Identity.Name,
                                                UtilitarioWeb.GetIP(Request),
                                                "",
                                                HistoricoLogOrigem.Outros);
                #endregion
            }

            scope.Complete();
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void SalvarNovo()
    {
        EnquadraItem enquadraItem = new EnquadraItem();
        EnquadraItemRendaFixa enquadraItemRendaFixa = new EnquadraItemRendaFixa();

        ASPxTextBox txtDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
        ASPxComboBox dropTipoRentabilidade = gridCadastro.FindEditFormTemplateControl("dropTipoRentabilidade") as ASPxComboBox;
        ASPxComboBox dropTipoPapel = gridCadastro.FindEditFormTemplateControl("dropTipoPapel") as ASPxComboBox;
        ASPxComboBox dropOperacaoCompromissada = gridCadastro.FindEditFormTemplateControl("dropOperacaoCompromissada") as ASPxComboBox;
        ASPxComboBox dropIndice = gridCadastro.FindEditFormTemplateControl("dropIndice") as ASPxComboBox;
        ASPxComboBox dropPapel = gridCadastro.FindEditFormTemplateControl("dropPapel") as ASPxComboBox;
        ASPxComboBox dropEmissor = gridCadastro.FindEditFormTemplateControl("dropEmissor") as ASPxComboBox;
        ASPxComboBox dropTipoEmissor = gridCadastro.FindEditFormTemplateControl("dropTipoEmissor") as ASPxComboBox;
        ASPxComboBox dropBloqueado = gridCadastro.FindEditFormTemplateControl("dropBloqueado") as ASPxComboBox;
        
        enquadraItem.Descricao = Convert.ToString(txtDescricao.Text);
        enquadraItem.Tipo = (int)TipoItemEnquadra.RendaFixa;

        using (esTransactionScope scope = new esTransactionScope())
        {
            enquadraItem.Save();

            enquadraItemRendaFixa.IdItem = enquadraItem.IdItem.Value; //Usa o mesmo Id da tabela EnquadraItem!

            //Tipo Papel
            if (dropTipoPapel.SelectedIndex > -1)
            {
                enquadraItemRendaFixa.TipoPapel = Convert.ToByte(dropTipoPapel.Value);
            }
            else
            {
                enquadraItemRendaFixa.TipoPapel = null;
            }

            //Tipo Rentabilidade
            if (dropTipoRentabilidade.SelectedIndex > -1)
            {
                enquadraItemRendaFixa.TipoRentabilidade = Convert.ToByte(dropTipoRentabilidade.Value);
            }
            else
            {
                enquadraItemRendaFixa.TipoRentabilidade = null;
            }

            //Compromissada
            if (dropOperacaoCompromissada.SelectedIndex > -1)
            {
                enquadraItemRendaFixa.OperacaoCompromissada = Convert.ToString(dropOperacaoCompromissada.Value);
            }
            else
            {
                enquadraItemRendaFixa.OperacaoCompromissada = null;
            }

            //Índice
            if (dropIndice.SelectedIndex > -1)
            {
                enquadraItemRendaFixa.IdIndice = Convert.ToInt16(dropIndice.Value);
            }
            else
            {
                enquadraItemRendaFixa.IdIndice = null;
            }

            //Papel
            if (dropPapel.SelectedIndex > -1)
            {
                enquadraItemRendaFixa.IdPapel = Convert.ToInt32(dropPapel.Value);
            }
            else
            {
                enquadraItemRendaFixa.IdPapel = null;
            }

            //Emissor
            if (dropEmissor.SelectedIndex > -1)
            {
                enquadraItemRendaFixa.IdEmissor = Convert.ToInt32(dropEmissor.Value);
            }
            else
            {
                enquadraItemRendaFixa.IdEmissor = null;
            }

            //Tipo Emissor
            if (dropTipoEmissor.SelectedIndex > -1)
            {
                enquadraItemRendaFixa.TipoEmissor = Convert.ToByte(dropTipoEmissor.Value);
            }
            else
            {
                enquadraItemRendaFixa.TipoEmissor = null;
            }

            //Bloqueado
            if (dropBloqueado.SelectedIndex > -1)
            {
                enquadraItemRendaFixa.Bloqueado = Convert.ToString(dropBloqueado.Value);
            }
            else
            {
                enquadraItemRendaFixa.Bloqueado = null;
            }

            enquadraItemRendaFixa.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de EnquadraItemRendaFixa - Operacao: Insert EnquadraItemRendaFixa: " + enquadraItemRendaFixa.IdItem + UtilitarioWeb.ToString(enquadraItemRendaFixa),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
            //
            scope.Complete();
        }
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {

        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        
        if (e.Parameters == "btnDelete") 
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdItem");
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idItem = Convert.ToInt32(keyValuesId[i]);

                using (esTransactionScope scope = new esTransactionScope()) {
                    EnquadraItemRendaFixa enquadraItemRendaFixa = new EnquadraItemRendaFixa();
                    if (enquadraItemRendaFixa.LoadByPrimaryKey(idItem)) {

                        //
                        EnquadraItemRendaFixa enquadraItemRendaFixaClone = (EnquadraItemRendaFixa)Utilitario.Clone(enquadraItemRendaFixa);
                        //

                        enquadraItemRendaFixa.MarkAsDeleted();
                        enquadraItemRendaFixa.Save();

                        #region Log do Processo
                        HistoricoLog historicoLog = new HistoricoLog();
                        historicoLog.InsereHistoricoLog(DateTime.Now,
                                                        DateTime.Now,
                                                        "Cadastro de EnquadraItemRendaFixa - Operacao: Delete EnquadraItemRendaFixa: " + idItem + UtilitarioWeb.ToString(enquadraItemRendaFixaClone),
                                                        HttpContext.Current.User.Identity.Name,
                                                        UtilitarioWeb.GetIP(Request),
                                                        "",
                                                        HistoricoLogOrigem.Outros);
                        #endregion
                    }

                    EnquadraItem enquadraItem = new EnquadraItem();
                    if (enquadraItem.LoadByPrimaryKey(idItem)) {
                        enquadraItem.MarkAsDeleted();
                        enquadraItem.Save();
                    }

                    scope.Complete();
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }
}

