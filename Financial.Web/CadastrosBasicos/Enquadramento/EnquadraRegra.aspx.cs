﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using Financial.Investidor;
using Financial.ContaCorrente;
using System.Drawing;
using Financial.ContaCorrente.Enums;
using Financial.Security;
using Financial.Fundo;
using DevExpress.Web.Data;
using Financial.Investidor.Enums;
using Financial.Common.Enums;
using Financial.Enquadra;
using Financial.Web.Common;
using EntitySpaces.Interfaces;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_EnquadraRegra : CadastroBasePage {
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasPopupCarteira = true;
        base.Page_Load(sender, e);

        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
                          new List<string>(new string[] { EnquadraRegraMetadata.ColumnNames.TipoRegra,
                                                          EnquadraRegraMetadata.ColumnNames.SubTipoRegra
                          }));
    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSEnquadraRegra_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        EnquadraRegraQuery enquadraRegraQuery = new EnquadraRegraQuery("E");
        CarteiraQuery carteiraQuery = new CarteiraQuery("C");
        ClienteQuery clienteQuery = new ClienteQuery("L");
        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");

        enquadraRegraQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == enquadraRegraQuery.IdCarteira);
        enquadraRegraQuery.InnerJoin(clienteQuery).On(enquadraRegraQuery.IdCarteira == clienteQuery.IdCliente);
        enquadraRegraQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        enquadraRegraQuery.Where(permissaoClienteQuery.IdUsuario.Equal(idUsuario));

        EnquadraRegraCollection enquadraRegraCollection = new EnquadraRegraCollection();
        enquadraRegraCollection.Load(enquadraRegraQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = enquadraRegraCollection;
    }

    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CarteiraCollection coll = new CarteiraCollection();
        coll.BuscaCarteirasComAcesso(Context.User.Identity.Name);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSEnquadraGrupo_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        EnquadraGrupoCollection coll = new EnquadraGrupoCollection();
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropTipoEnquadramento_Load(object sender, EventArgs e) {
        if (gridCadastro.IsNewRowEditing) {
            (sender as ASPxComboBox).SelectedIndex = 0;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropRealTime_Load(object sender, EventArgs e)
    {
        if (gridCadastro.IsNewRowEditing) {
            (sender as ASPxComboBox).SelectedIndex = 0;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditCodigoCarteira_Load(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxSpinEdit).ClientEnabled = false;
            (sender as ASPxSpinEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
        ASPxDateEdit textDataFim = gridCadastro.FindEditFormTemplateControl("textDataFim") as ASPxDateEdit;
        ASPxComboBox dropTipoRegra = gridCadastro.FindEditFormTemplateControl("dropTipoRegra") as ASPxComboBox;
        ASPxComboBox dropSubTipoRegra = gridCadastro.FindEditFormTemplateControl("dropSubTipoRegra") as ASPxComboBox;
        ASPxComboBox dropGrupoBase = gridCadastro.FindEditFormTemplateControl("dropGrupoBase") as ASPxComboBox;
        ASPxComboBox dropGrupoCriterio = gridCadastro.FindEditFormTemplateControl("dropGrupoCriterio") as ASPxComboBox;
        ASPxSpinEdit textMinimo = gridCadastro.FindEditFormTemplateControl("textMinimo") as ASPxSpinEdit;
        ASPxSpinEdit textMaximo = gridCadastro.FindEditFormTemplateControl("textMaximo") as ASPxSpinEdit;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(btnEditCodigoCarteira);
        controles.Add(textDataReferencia);
        controles.Add(textDescricao);
        controles.Add(dropTipoRegra);

        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        if (textDataReferencia.Text != "")
        {
            DateTime dataReferencia = Convert.ToDateTime(textDataReferencia.Text);
            if (!Calendario.IsDiaUtil(dataReferencia, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil))
            {
                e.Result = "Data de referência não é dia útil.";
                return;
            }
        }

        if (textDataFim.Text != "")
        {
            DateTime dataFim = Convert.ToDateTime(textDataFim.Text);
            if (!Calendario.IsDiaUtil(dataFim, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil)) {
                e.Result = "Data fim não é dia útil.";
                return;
            }
        }

        if (dropTipoRegra.SelectedIndex == 3 || dropTipoRegra.SelectedIndex == 5 || dropTipoRegra.SelectedIndex == 6 || dropTipoRegra.SelectedIndex == 7 || dropTipoRegra.SelectedIndex == 8)
        {
            if (dropSubTipoRegra.Value == "")
            {
                e.Result = "Sub Tipo Regra deve ser selecionado para este Tipo Regra.";
                return;
            }
        }

        if (dropTipoRegra.SelectedIndex == 0) //Limite Absoluto
        {
            if (dropGrupoBase.SelectedIndex == -1 || dropGrupoCriterio.SelectedIndex == -1) 
            {
                e.Result = "Grupo Base e Grupo Critério devem ser escolhidos para Tipo Regra = LimiteAbsoluto.";
                return;
            }
        }

        if (dropTipoRegra.SelectedIndex == 5) //Concentração Ativo
        {
            if (dropGrupoBase.SelectedIndex == -1) 
            {
                e.Result = "Grupo Base deve ser escolhido para TipoRegra = Concentração.";
                return;
            }

            if (dropSubTipoRegra.SelectedIndex == 5 && dropGrupoCriterio.SelectedIndex == -1) //Emissor
            {
                e.Result = "Para Sub Tipo Regra 'Concentração por Emissor' ser escolhido Grupo Critério.";
                return;
            }
        }
        
    }
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        SalvarNovo();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOKAdd_Init(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCarteira = Convert.ToInt32(e.Parameter);
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            if (carteira.str.Apelido != "") {
                if (carteira.IsAtivo) {
                    //O permissionamento da carteira é dado direto no cliente vinculado à carteira
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoClienteComControle(idCarteira, HttpContext.Current.User.Identity.Name)) {
                        nome = carteira.str.Apelido;
                    }
                    else {
                        nome = "no_access";
                    }
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="enquadraRegra"></param>
    private void ReplicaRegra(EnquadraRegra enquadraRegra)
    {
        int idCarteira = enquadraRegra.IdCarteira.Value;
        Cliente cliente = new Cliente();
        List<esQueryItem> campos = new List<esQueryItem>();
        campos.Add(cliente.Query.TipoControle);
        campos.Add(cliente.Query.IdTipo);
        cliente.LoadByPrimaryKey(campos, idCarteira);
        byte tipoControle = cliente.TipoControle.Value;
        int idTipo = cliente.IdTipo.Value;

        CarteiraQuery carteiraQuery = new CarteiraQuery("CA");
        ClienteQuery clienteQuery = new ClienteQuery("CL");

        carteiraQuery.Select(carteiraQuery.IdCarteira);
        carteiraQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == carteiraQuery.IdCarteira);
        carteiraQuery.Where(clienteQuery.IdTipo.Equal(idTipo)); //Busca clientes com mesmo tipo de cliente
        carteiraQuery.Where(clienteQuery.TipoControle.Equal(tipoControle)); //Busca clientes com mesmo tipo de controle

        CarteiraCollection carteiraCollection = new CarteiraCollection();
        carteiraCollection.Load(carteiraQuery);

        foreach (Carteira carteira in carteiraCollection) 
        {
            int idCarteiraReplicar = carteira.IdCarteira.Value;

            EnquadraRegra enquadraRegraExiste = new EnquadraRegra();
            enquadraRegraExiste.Query.Where(enquadraRegraExiste.Query.IdCarteira.Equal(idCarteiraReplicar),
                                            enquadraRegraExiste.Query.DataReferencia.Equal(enquadraRegra.DataReferencia.Value),
                                            enquadraRegraExiste.Query.TipoEnquadramento.Equal(enquadraRegra.TipoEnquadramento.Value),
                                            enquadraRegraExiste.Query.TipoRegra.Equal(enquadraRegra.TipoRegra.Value));

            if (enquadraRegra.SubTipoRegra.HasValue) {
                enquadraRegraExiste.Query.Where(enquadraRegraExiste.Query.SubTipoRegra.Equal(enquadraRegra.SubTipoRegra.Value));
            }
            else {
                enquadraRegraExiste.Query.Where(enquadraRegraExiste.Query.SubTipoRegra.IsNull());
            }

            if (enquadraRegra.IdGrupoBase.HasValue) {
                enquadraRegraExiste.Query.Where(enquadraRegraExiste.Query.IdGrupoBase.Equal(enquadraRegra.IdGrupoBase.Value));
            }
            else {
                enquadraRegraExiste.Query.Where(enquadraRegraExiste.Query.IdGrupoBase.IsNull());
            }

            if (enquadraRegra.IdGrupoCriterio.HasValue) {
                enquadraRegraExiste.Query.Where(enquadraRegraExiste.Query.IdGrupoCriterio.Equal(enquadraRegra.IdGrupoCriterio.Value));
            }
            else {
                enquadraRegraExiste.Query.Where(enquadraRegraExiste.Query.IdGrupoCriterio.IsNull());
            }

            if (!enquadraRegraExiste.Query.Load()) //Só replica se a regra não existir para a carteira
            {
                EnquadraRegra enquadraRegraReplicar = new EnquadraRegra();
                enquadraRegraReplicar.IdCarteira = idCarteiraReplicar;
                enquadraRegraReplicar.DataFim = enquadraRegra.DataFim;
                enquadraRegraReplicar.DataReferencia = enquadraRegra.DataReferencia.Value;
                enquadraRegraReplicar.Descricao = enquadraRegra.Descricao;
                enquadraRegraReplicar.IdGrupoBase = enquadraRegra.IdGrupoBase;
                enquadraRegraReplicar.IdGrupoCriterio = enquadraRegra.IdGrupoCriterio;
                enquadraRegraReplicar.IdRegra = enquadraRegra.IdRegra.Value;
                enquadraRegraReplicar.Maximo = enquadraRegra.Maximo;
                enquadraRegraReplicar.Minimo = enquadraRegra.Minimo;
                enquadraRegraReplicar.SubTipoRegra = enquadraRegra.SubTipoRegra;
                enquadraRegraReplicar.TipoEnquadramento = enquadraRegra.TipoEnquadramento.Value;
                enquadraRegraReplicar.TipoRegra = enquadraRegra.TipoRegra.Value;
                enquadraRegraReplicar.RealTime = enquadraRegra.RealTime.Value;

                enquadraRegraReplicar.Save();
            }
            else
            {
                enquadraRegraExiste.Maximo = enquadraRegra.Minimo;
                enquadraRegraExiste.Maximo = enquadraRegra.Maximo;
                enquadraRegraExiste.Save();
            }

        }
    }
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        int idRegra = (int)e.Keys[0];
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        ASPxDateEdit textDataFim = gridCadastro.FindEditFormTemplateControl("textDataFim") as ASPxDateEdit;
        ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
        ASPxComboBox dropTipoEnquadramento = gridCadastro.FindEditFormTemplateControl("dropTipoEnquadramento") as ASPxComboBox;
        ASPxComboBox dropRealTime = gridCadastro.FindEditFormTemplateControl("dropRealTime") as ASPxComboBox;
        ASPxComboBox dropTipoRegra = gridCadastro.FindEditFormTemplateControl("dropTipoRegra") as ASPxComboBox;
        ASPxComboBox dropSubTipoRegra = gridCadastro.FindEditFormTemplateControl("dropSubTipoRegra") as ASPxComboBox;
        ASPxComboBox dropGrupoBase = gridCadastro.FindEditFormTemplateControl("dropGrupoBase") as ASPxComboBox;
        ASPxComboBox dropGrupoCriterio = gridCadastro.FindEditFormTemplateControl("dropGrupoCriterio") as ASPxComboBox;
        ASPxSpinEdit textMinimo = gridCadastro.FindEditFormTemplateControl("textMinimo") as ASPxSpinEdit;
        ASPxSpinEdit textMaximo = gridCadastro.FindEditFormTemplateControl("textMaximo") as ASPxSpinEdit;
        CheckBox chkReplica = gridCadastro.FindEditFormTemplateControl("chkReplica") as CheckBox;

        EnquadraRegra enquadraRegra = new EnquadraRegra();
        if (enquadraRegra.LoadByPrimaryKey(idRegra)) {
            enquadraRegra.IdCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);
            enquadraRegra.DataReferencia = Convert.ToDateTime(textDataReferencia.Text);
            enquadraRegra.Descricao = textDescricao.Text;
            enquadraRegra.TipoEnquadramento = Convert.ToByte(dropTipoEnquadramento.SelectedItem.Value);
            enquadraRegra.RealTime = Convert.ToByte(dropRealTime.SelectedItem.Value);
            enquadraRegra.TipoRegra = Convert.ToInt32(dropTipoRegra.SelectedItem.Value);

            if (textDataFim.Text != "") {
                enquadraRegra.DataFim = Convert.ToDateTime(textDataFim.Text);
            }

            if (dropSubTipoRegra.Value != "") {
                //Só tem subregra: PosicoesDescoberto, Concentração Ativo, Concentração Emissor, Limite Oscilação, Limite Desvio (mensal/anual)
                if (dropTipoRegra.SelectedIndex == 3 || dropTipoRegra.SelectedIndex == 5 ||
                    dropTipoRegra.SelectedIndex == 6 || dropTipoRegra.SelectedIndex == 7 || 
                    dropTipoRegra.SelectedIndex == 8)
                {
                    enquadraRegra.SubTipoRegra = Convert.ToInt32(dropSubTipoRegra.Value);
                }
                else {
                    enquadraRegra.SubTipoRegra = null;
                }
            }

            if (dropGrupoBase.SelectedIndex != -1) {
                //Só tem grupoBase: Limite Absoluto, Concentração Ativo, Concentração Emissor
                if (dropTipoRegra.SelectedIndex == 0 || dropTipoRegra.SelectedIndex == 5 ||
                    dropTipoRegra.SelectedIndex == 6) {
                    enquadraRegra.IdGrupoBase = Convert.ToInt32(dropGrupoBase.SelectedItem.Value);
                }
                else {
                    enquadraRegra.IdGrupoBase = null;
                }
            }

            if (dropGrupoCriterio.SelectedIndex != -1) {
                //Só tem grupoCriterio: Limite Absoluto, Concentração Ativo, Concentração Emissor
                if (dropTipoRegra.SelectedIndex == 0 || dropTipoRegra.SelectedIndex == 5 ||
                    dropTipoRegra.SelectedIndex == 6) {
                    enquadraRegra.IdGrupoCriterio = Convert.ToInt32(dropGrupoCriterio.SelectedItem.Value);
                }
                else {
                    enquadraRegra.IdGrupoCriterio = null;
                }
            }

            if (textMinimo.Text != "") {
                if (dropTipoRegra.SelectedIndex != 3 && dropTipoRegra.SelectedIndex != 4) {
                    enquadraRegra.Minimo = Convert.ToDecimal(textMinimo.Text);
                }
                else //Posicoes a descoberto; Media movel
                {
                    enquadraRegra.Minimo = null;
                }
            }

            if (textMaximo.Text != "") {
                if (dropTipoRegra.SelectedIndex != 3 && dropTipoRegra.SelectedIndex != 4) {
                    enquadraRegra.Maximo = Convert.ToDecimal(textMaximo.Text);
                }
                else //Posicoes a descoberto; Media movel
                {
                    enquadraRegra.Maximo = null;
                }
            }

            enquadraRegra.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de EnquadraRegra - Operacao: Update EnquadraRegra: " + idRegra + UtilitarioWeb.ToString(enquadraRegra),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion

            if (chkReplica.Checked) //Replica a regra para carteiras com mesmo tipo de controle e baseadas no mesmo tipo de cliente
            {
                ReplicaRegra(enquadraRegra);
            }
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    private void SalvarNovo() {
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxDateEdit textDataReferencia = gridCadastro.FindEditFormTemplateControl("textDataReferencia") as ASPxDateEdit;
        ASPxDateEdit textDataFim = gridCadastro.FindEditFormTemplateControl("textDataFim") as ASPxDateEdit;
        ASPxTextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
        ASPxComboBox dropTipoEnquadramento = gridCadastro.FindEditFormTemplateControl("dropTipoEnquadramento") as ASPxComboBox;
        ASPxComboBox dropRealTime = gridCadastro.FindEditFormTemplateControl("dropRealTime") as ASPxComboBox;
        ASPxComboBox dropTipoRegra = gridCadastro.FindEditFormTemplateControl("dropTipoRegra") as ASPxComboBox;
        ASPxComboBox dropSubTipoRegra = gridCadastro.FindEditFormTemplateControl("dropSubTipoRegra") as ASPxComboBox;
        ASPxComboBox dropGrupoBase = gridCadastro.FindEditFormTemplateControl("dropGrupoBase") as ASPxComboBox;
        ASPxComboBox dropGrupoCriterio = gridCadastro.FindEditFormTemplateControl("dropGrupoCriterio") as ASPxComboBox;
        ASPxSpinEdit textMinimo = gridCadastro.FindEditFormTemplateControl("textMinimo") as ASPxSpinEdit;
        ASPxSpinEdit textMaximo = gridCadastro.FindEditFormTemplateControl("textMaximo") as ASPxSpinEdit;
        CheckBox chkReplica = gridCadastro.FindEditFormTemplateControl("chkReplica") as CheckBox;

        EnquadraRegra enquadraRegra = new EnquadraRegra();

        enquadraRegra.IdCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);
        enquadraRegra.DataReferencia = Convert.ToDateTime(textDataReferencia.Text);
        enquadraRegra.Descricao = textDescricao.Text;
        enquadraRegra.TipoEnquadramento = Convert.ToByte(dropTipoEnquadramento.SelectedItem.Value);
        enquadraRegra.RealTime = Convert.ToByte(dropRealTime.SelectedItem.Value);
        enquadraRegra.TipoRegra = Convert.ToInt32(dropTipoRegra.SelectedItem.Value);

        if (textDataFim.Text != "") {
            enquadraRegra.DataFim = Convert.ToDateTime(textDataFim.Text);
        }

        if (dropSubTipoRegra.SelectedIndex != -1) {
            //Só tem subregra: PosicoesDescoberto, Concentração Ativo, Concentração Emissor, Limite Oscilação
            if (dropTipoRegra.SelectedIndex == 3 || dropTipoRegra.SelectedIndex == 5 ||
                dropTipoRegra.SelectedIndex == 6 || dropTipoRegra.SelectedIndex == 7 ||
                    dropTipoRegra.SelectedIndex == 8)
            {
                enquadraRegra.SubTipoRegra = Convert.ToInt32(dropSubTipoRegra.SelectedItem.Value);
            }
            else {
                enquadraRegra.SubTipoRegra = null;
            }
        }

        if (dropGrupoBase.SelectedIndex != -1) {
            //Só tem grupoBase: Limite Absoluto, Concentração Ativo, Concentração Emissor
            if (dropTipoRegra.SelectedIndex == 0 || dropTipoRegra.SelectedIndex == 5 ||
                dropTipoRegra.SelectedIndex == 6) {
                enquadraRegra.IdGrupoBase = Convert.ToInt32(dropGrupoBase.SelectedItem.Value);
            }
            else {
                enquadraRegra.IdGrupoBase = null;
            }
        }

        if (dropGrupoCriterio.SelectedIndex != -1) {
            //Só tem grupoCriterio: Limite Absoluto, Concentração Ativo, Concentração Emissor
            if (dropTipoRegra.SelectedIndex == 0 || dropTipoRegra.SelectedIndex == 5 ||
                dropTipoRegra.SelectedIndex == 6) {
                enquadraRegra.IdGrupoCriterio = Convert.ToInt32(dropGrupoCriterio.SelectedItem.Value);
            }
            else {
                enquadraRegra.IdGrupoCriterio = null;
            }
        }

        if (textMinimo.Text != "") {
            if (dropTipoRegra.SelectedIndex != 3 && dropTipoRegra.SelectedIndex != 4) {
                enquadraRegra.Minimo = Convert.ToDecimal(textMinimo.Text);
            }
            else //Posicoes a descoberto; Media movel
            {
                enquadraRegra.Minimo = null;
            }
        }

        if (textMaximo.Text != "") {
            if (dropTipoRegra.SelectedIndex != 3 && dropTipoRegra.SelectedIndex != 4) {
                enquadraRegra.Maximo = Convert.ToDecimal(textMaximo.Text);
            }
            else //Posicoes a descoberto; Media movel
            {
                enquadraRegra.Maximo = null;
            }
        }

        enquadraRegra.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de EnquadraRegra - Operacao: Insert EnquadraRegra: " + enquadraRegra.IdRegra + UtilitarioWeb.ToString(enquadraRegra),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        if (chkReplica.Checked) {
            ReplicaRegra(enquadraRegra);
        }

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(EnquadraRegraMetadata.ColumnNames.IdRegra);
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idRegra = Convert.ToInt32(keyValuesId[i]);

                EnquadraRegra enquadraRegra = new EnquadraRegra();
                if (enquadraRegra.LoadByPrimaryKey(idRegra)) {
                    EnquadraResultadoCollection enquadraResultadoCollection = new EnquadraResultadoCollection();
                    enquadraResultadoCollection.Query.Where(enquadraResultadoCollection.Query.IdRegra.Equal(idRegra));
                    enquadraResultadoCollection.Query.Load();

                    enquadraResultadoCollection.MarkAllAsDeleted();
                    enquadraResultadoCollection.Save();

                    EnquadraRegra enquadraRegraClone = (EnquadraRegra)Utilitario.Clone(enquadraRegra);
                    //

                    enquadraRegra.MarkAsDeleted();
                    enquadraRegra.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de EnquadraRegra - Operacao: Delete EnquadraRegra: " + idRegra + UtilitarioWeb.ToString(enquadraRegraClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing) {
            ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
            e.Properties["cpBtnEditCodigoCarteira"] = btnEditCodigoCarteira.ClientID;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_InitNewRow(object sender, ASPxDataInitNewRowEventArgs e) {
        e.NewValues[EnquadraRegraMetadata.ColumnNames.TipoEnquadramento] = "1";
        e.NewValues[EnquadraRegraMetadata.ColumnNames.RealTime] = "2";
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) 
    {
        this.FocaCampoGrid("btnEditCodigoCarteira", "textDataReferencia");
        base.gridCadastro_PreRender(sender, e);
                
        ASPxComboBox dropTipoRegra = gridCadastro.FindEditFormTemplateControl("dropTipoRegra") as ASPxComboBox;
        ASPxComboBox dropSubTipoRegra = gridCadastro.FindEditFormTemplateControl("dropSubTipoRegra") as ASPxComboBox;
        ASPxComboBox dropGrupoBase = gridCadastro.FindEditFormTemplateControl("dropGrupoBase") as ASPxComboBox;
        ASPxComboBox dropGrupoCriterio = gridCadastro.FindEditFormTemplateControl("dropGrupoCriterio") as ASPxComboBox;
        ASPxSpinEdit textMinimo = gridCadastro.FindEditFormTemplateControl("textMinimo") as ASPxSpinEdit;
        ASPxSpinEdit textMaximo = gridCadastro.FindEditFormTemplateControl("textMaximo") as ASPxSpinEdit;

        if (!gridCadastro.IsNewRowEditing && dropTipoRegra != null)
        {
            #region Trata dropSubTipoRegra
            if (dropTipoRegra.SelectedIndex == 3)
            {
                dropSubTipoRegra.Items.Add("Termo Ação", 401);
                dropSubTipoRegra.Items.Add("Opções", 402);
            }
            else if (dropTipoRegra.SelectedIndex == 5)
            {
                dropSubTipoRegra.Items.Add("Cotas Inv. - Ativo", 601);
                dropSubTipoRegra.Items.Add("Cotas Inv. - Administrador", 602);
                dropSubTipoRegra.Items.Add("Cotas Inv. - Gestor", 603);
                dropSubTipoRegra.Items.Add("Ação", 610);
                dropSubTipoRegra.Items.Add("Título Renda Fixa", 620);
                dropSubTipoRegra.Items.Add("Emissor", 630);
            }
            else if (dropTipoRegra.SelectedIndex == 6 || dropTipoRegra.SelectedIndex == 7 || dropTipoRegra.SelectedIndex == 8)
            {
                dropSubTipoRegra.Items.Add("Cota da Carteira", 801);
                dropSubTipoRegra.Items.Add("Cotas de Fundos", 802);
            }
            else
            {
                //dropSubTipoRegra.SelectedIndex = -1;
                dropSubTipoRegra.ClientEnabled = false;                
            }
            #endregion

            if (dropTipoRegra.SelectedIndex != 0 && !(dropTipoRegra.SelectedIndex == 5 || dropSubTipoRegra.SelectedIndex == 5))
            {
                dropGrupoCriterio.SelectedIndex = -1;
                dropGrupoCriterio.ClientEnabled = false;                
            }

            if (dropTipoRegra.SelectedIndex != 0 && dropTipoRegra.SelectedIndex != 5)
            {
                dropGrupoBase.SelectedIndex = -1;
                dropGrupoBase.ClientEnabled = false;                
            }

            if (dropTipoRegra.SelectedIndex == 3 || dropTipoRegra.SelectedIndex == 4)
            {
                textMinimo.Text = "";
                textMinimo.ClientEnabled = false;                

                textMaximo.Text = "";
                textMaximo.ClientEnabled = false;                
            }
        }
    }
}