using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Enquadra;
using Financial.Enquadra.Enums;
using Financial.Fundo;
using Financial.Investidor;
using Financial.Investidor.Enums;
using EntitySpaces.Interfaces;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_EnquadraItemFundo : Financial.Web.Common.CadastroBasePage {
    #region DataSources
    protected void EsDSEnquadraItemFundo_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        EnquadraItemQuery enquadraItemQuery = new EnquadraItemQuery("I");
        EnquadraItemFundoQuery enquadraItemFundoQuery = new EnquadraItemFundoQuery("A");

        enquadraItemFundoQuery.Select(enquadraItemFundoQuery, enquadraItemQuery.Descricao);
        enquadraItemFundoQuery.InnerJoin(enquadraItemQuery).On(enquadraItemQuery.IdItem == enquadraItemFundoQuery.IdItem);
        enquadraItemFundoQuery.OrderBy(enquadraItemQuery.Descricao.Ascending);

        EnquadraItemFundoCollection coll = new EnquadraItemFundoCollection();
        coll.Load(enquadraItemFundoQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSAgenteAdministrador_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        AgenteMercadoCollection coll = new AgenteMercadoCollection();

        coll.Query.Where(coll.Query.FuncaoAdministrador.Equal("S"));
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSAgenteGestor_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        AgenteMercadoCollection coll = new AgenteMercadoCollection();

        coll.Query.Where(coll.Query.FuncaoGestor.Equal("S"));
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) 
    {
        CarteiraQuery carteiraQuery = new CarteiraQuery("A");
        ClienteQuery clienteQuery = new ClienteQuery("I");

        carteiraQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == carteiraQuery.IdCarteira);
        carteiraQuery.Where(clienteQuery.IdTipo.In((int)TipoClienteFixo.Clube, (int)TipoClienteFixo.Fundo, (int)TipoClienteFixo.FDIC));
        carteiraQuery.OrderBy(carteiraQuery.Apelido.Ascending);

        CarteiraCollection coll = new CarteiraCollection();
        coll.Load(carteiraQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSIndice_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        IndiceCollection coll = new IndiceCollection();

        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCategoria_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CategoriaFundoCollection coll = new CategoriaFundoCollection();

        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) {
        if (e.Column.FieldName == "Descricao") {
            e.Value = Convert.ToString(e.GetListSourceFieldValue(EnquadraItemMetadata.ColumnNames.Descricao));
        }
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        EnquadraItem enquadraItem = new EnquadraItem();
        EnquadraItemFundo enquadraItemFundo = new EnquadraItemFundo();
        
        using (esTransactionScope scope = new esTransactionScope()) {
            if (enquadraItem.LoadByPrimaryKey((int)e.Keys[0])) {
                enquadraItem.Descricao = Convert.ToString(e.NewValues["Descricao"]);
                //
                enquadraItem.Save();
            }

            if (enquadraItemFundo.LoadByPrimaryKey((int)e.Keys[0])) {

                if (e.NewValues["TipoCarteira"] != null) {
                    enquadraItemFundo.TipoCarteira = Convert.ToByte(e.NewValues["TipoCarteira"]);
                }
                if (e.NewValues["IdIndice"] != null) {

                    enquadraItemFundo.IdIndice = Convert.ToInt16(e.NewValues["IdIndice"]);
                }

                if (e.NewValues["IdCarteira"] != null) {
                    enquadraItemFundo.IdCarteira = Convert.ToInt32(e.NewValues["IdCarteira"]);
                }

                if (e.NewValues["IdAgenteAdministrador"] != null) {
                    enquadraItemFundo.IdAgenteAdministrador = Convert.ToInt32(e.NewValues["IdAgenteAdministrador"]);
                }

                if (e.NewValues["IdAgenteGestor"] != null) {
                    enquadraItemFundo.IdAgenteGestor = Convert.ToInt32(e.NewValues["IdAgenteGestor"]);
                }

                if (e.NewValues["IdCategoria"] != null)
                {
                    enquadraItemFundo.IdCategoria = Convert.ToInt32(e.NewValues["IdCategoria"]);
                }
                //
                enquadraItemFundo.Save();

                #region Log do Processo
                HistoricoLog historicoLog = new HistoricoLog();
                historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                "Cadastro de EnquadraItemFundo - Operacao: Update EnquadraItemFundo: " + (int)e.Keys[0] + UtilitarioWeb.ToString(enquadraItemFundo),
                                                HttpContext.Current.User.Identity.Name,
                                                UtilitarioWeb.GetIP(Request),
                                                "",
                                                HistoricoLogOrigem.Outros);
                #endregion
            }

            scope.Complete();
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        EnquadraItem enquadraItem = new EnquadraItem();
        EnquadraItemFundo enquadraItemFundo = new EnquadraItemFundo();

        enquadraItem.Descricao = Convert.ToString(e.NewValues["Descricao"]);
        enquadraItem.Tipo = (int)TipoItemEnquadra.CotaInvestimento;

        using (esTransactionScope scope = new esTransactionScope()) {
            enquadraItem.Save();

            enquadraItemFundo.IdItem = enquadraItem.IdItem.Value; ; //Usa o mesmo Id da tabela EnquadraItem!

            if (e.NewValues["TipoCarteira"] != null) {
                enquadraItemFundo.TipoCarteira = Convert.ToByte(e.NewValues["TipoCarteira"]);
            }
            if (e.NewValues["IdIndice"] != null) {

                enquadraItemFundo.IdIndice = Convert.ToInt16(e.NewValues["IdIndice"]);
            }
            if (e.NewValues["IdCarteira"] != null) {
                enquadraItemFundo.IdCarteira = Convert.ToInt32(e.NewValues["IdCarteira"]);
            }
            if (e.NewValues["IdAgenteAdministrador"] != null) {
                enquadraItemFundo.IdAgenteAdministrador = Convert.ToInt32(e.NewValues["IdAgenteAdministrador"]);
            }
            if (e.NewValues["IdAgenteGestor"] != null) {
                enquadraItemFundo.IdAgenteGestor = Convert.ToInt32(e.NewValues["IdAgenteGestor"]);
            }
            if (e.NewValues["IdCategoria"] != null)
            {
                enquadraItemFundo.IdCategoria = Convert.ToInt32(e.NewValues["IdCategoria"]);
            }
            //              
            enquadraItemFundo.Save();
            //
            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de EnquadraItemFundo - Operacao: Insert enquadraItemFundo: " + enquadraItemFundo.IdItem + UtilitarioWeb.ToString(enquadraItemFundo),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion

            scope.Complete();
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        
        if (e.Parameters == "btnDelete") 
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdItem");
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idItem = Convert.ToInt32(keyValuesId[i]);

                using (esTransactionScope scope = new esTransactionScope()) {
                    EnquadraItemFundo enquadraItemFundo = new EnquadraItemFundo();
                    if (enquadraItemFundo.LoadByPrimaryKey(idItem)) {

                        //
                        EnquadraItemFundo enquadraItemFundoClone = (EnquadraItemFundo)Utilitario.Clone(enquadraItemFundo);
                        //

                        enquadraItemFundo.MarkAsDeleted();
                        enquadraItemFundo.Save();

                        #region Log do Processo
                        HistoricoLog historicoLog = new HistoricoLog();
                        historicoLog.InsereHistoricoLog(DateTime.Now,
                                                        DateTime.Now,
                                                        "Cadastro de EnquadraItemFundo - Operacao: Delete EnquadraItemFundo: " + idItem + UtilitarioWeb.ToString(enquadraItemFundoClone),
                                                        HttpContext.Current.User.Identity.Name,
                                                        UtilitarioWeb.GetIP(Request),
                                                        "",
                                                        HistoricoLogOrigem.Outros);
                        #endregion
                    }

                    EnquadraItem enquadraItem = new EnquadraItem();
                    if (enquadraItem.LoadByPrimaryKey(idItem)) {
                        enquadraItem.MarkAsDeleted();
                        enquadraItem.Save();
                    }
                    scope.Complete();
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }
}