using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Enquadra;
using EntitySpaces.Interfaces;
using Financial.Enquadra.Enums;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_EnquadraItemSwap : Financial.Web.Common.CadastroBasePage {

    protected void EsDSEnquadraItemSwap_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {        
        EnquadraItemQuery enquadraItemQuery = new EnquadraItemQuery("I");
        EnquadraItemSwapQuery enquadraItemSwapQuery = new EnquadraItemSwapQuery("A");

        enquadraItemQuery.Select(enquadraItemSwapQuery, enquadraItemQuery.Descricao);
        enquadraItemQuery.InnerJoin(enquadraItemSwapQuery).On(enquadraItemQuery.IdItem == enquadraItemSwapQuery.IdItem);
        enquadraItemQuery.OrderBy(enquadraItemQuery.Descricao.Ascending);

        EnquadraItemCollection coll = new EnquadraItemCollection();
        coll.Load(enquadraItemQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        EnquadraItem enquadraItem = new EnquadraItem();
        //
        if (enquadraItem.LoadByPrimaryKey((int)e.Keys[0])) {
            enquadraItem.Descricao = Convert.ToString(e.NewValues["Descricao"]);
            enquadraItem.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de EnquadraItemSwap - Operacao: Update EnquadraItemSwap: " + (int)e.Keys[0] + UtilitarioWeb.ToString(enquadraItem),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        EnquadraItem enquadraItem = new EnquadraItem();
        EnquadraItemSwap enquadraItemSwap = new EnquadraItemSwap();

        enquadraItem.Descricao = Convert.ToString(e.NewValues["Descricao"]);
        enquadraItem.Tipo = (int)TipoItemEnquadra.Swap;

        using (esTransactionScope scope = new esTransactionScope()) {
            enquadraItem.Save();

            //Usa o mesmo Id da tabela EnquadraItem!
            enquadraItemSwap.IdItem = enquadraItem.IdItem.Value; 
            //
            enquadraItemSwap.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de EnquadraItemSwap - Operacao: Insert EnquadraItemSwap: " + enquadraItemSwap.IdItem + UtilitarioWeb.ToString(enquadraItemSwap),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
                
            scope.Complete();
        }        

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        
        if (e.Parameters == "btnDelete") 
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdItem");
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idItem = Convert.ToInt32(keyValuesId[i]);

                using (esTransactionScope scope = new esTransactionScope()) {
                    EnquadraItemSwap enquadraItemSwap = new EnquadraItemSwap();
                    if (enquadraItemSwap.LoadByPrimaryKey(idItem)) {

                        EnquadraItemSwap enquadraItemSwapClone = (EnquadraItemSwap)Utilitario.Clone(enquadraItemSwap);
                        //
                        
                        enquadraItemSwap.MarkAsDeleted();
                        enquadraItemSwap.Save();

                        #region Log do Processo
                        HistoricoLog historicoLog = new HistoricoLog();
                        historicoLog.InsereHistoricoLog(DateTime.Now,
                                                        DateTime.Now,
                                                        "Cadastro de EnquadraItemSwap - Operacao: Delete EnquadraItemSwap: " + idItem + UtilitarioWeb.ToString(enquadraItemSwapClone),
                                                        HttpContext.Current.User.Identity.Name,
                                                        UtilitarioWeb.GetIP(Request),
                                                        "",
                                                        HistoricoLogOrigem.Outros);
                        #endregion
                    }

                    EnquadraItem enquadraItem = new EnquadraItem();
                    if (enquadraItem.LoadByPrimaryKey(idItem)) {
                        enquadraItem.MarkAsDeleted();
                        enquadraItem.Save();
                    }

                    scope.Complete();
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }
}