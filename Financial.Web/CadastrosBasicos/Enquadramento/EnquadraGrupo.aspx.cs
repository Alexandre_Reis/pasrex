﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Enquadra;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Security;

public partial class CadastrosBasicos_EnquadraGrupo : Financial.Web.Common.CadastroBasePage {
    protected void EsDSEnquadraGrupo_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        EnquadraGrupoCollection coll = new EnquadraGrupoCollection();

        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        EnquadraGrupo enquadraGrupo = new EnquadraGrupo();
        int idGrupo = (int)e.Keys[0];

        if (enquadraGrupo.LoadByPrimaryKey(idGrupo)) {
            enquadraGrupo.Descricao = Convert.ToString(e.NewValues["Descricao"]);
            enquadraGrupo.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de EnquadraGrupo - Operacao: Update EnquadraGrupo: " + idGrupo + UtilitarioWeb.ToString(enquadraGrupo),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        EnquadraGrupo enquadraGrupo = new EnquadraGrupo();

        enquadraGrupo.Descricao = Convert.ToString(e.NewValues["Descricao"]);
        enquadraGrupo.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de EnquadraGrupo - Operacao: Insert EnquadraGrupo: " + enquadraGrupo.IdGrupo + UtilitarioWeb.ToString(enquadraGrupo),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdGrupo");
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idGrupo = Convert.ToInt32(keyValuesId[i]);

                EnquadraGrupo enquadraGrupo = new EnquadraGrupo();
                if (enquadraGrupo.LoadByPrimaryKey(idGrupo)) {
                    EnquadraRegraCollection enquadraRegraCollection = new EnquadraRegraCollection();
                    enquadraRegraCollection.Query.Where(enquadraRegraCollection.Query.IdGrupoBase.Equal(idGrupo) |
                                                        enquadraRegraCollection.Query.IdGrupoCriterio.Equal(idGrupo));
                    if (enquadraRegraCollection.Query.Load()) {
                        throw new Exception("Grupo " + enquadraGrupo.Descricao + " não pode ser excluído por ter regras de enquadramento relacionadas.");
                    }
                    //
                    EnquadraGrupo enquadraGrupoClone = (EnquadraGrupo)Utilitario.Clone(enquadraGrupo);
                    //
                    enquadraGrupo.MarkAsDeleted();
                    enquadraGrupo.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de EnquadraGrupo - Operacao: Delete EnquadraGrupo: " + idGrupo + UtilitarioWeb.ToString(enquadraGrupoClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }
}