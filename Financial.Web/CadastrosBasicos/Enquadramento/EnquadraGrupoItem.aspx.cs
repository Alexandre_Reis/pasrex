using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Common.Enums;
using Financial.Enquadra;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_EnquadraGrupoItem : Financial.Web.Common.CadastroBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        this.AllowUpdate = false;
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSEnquadraGrupoItem_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        EnquadraGrupoItemCollection coll = new EnquadraGrupoItemCollection();

        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSEnquadraGrupo_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        EnquadraGrupoCollection coll = new EnquadraGrupoCollection();

        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSEnquadraItem_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        EnquadraItemCollection coll = new EnquadraItemCollection();

        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) {
        if (e.Column.FieldName == "CompositeKey") {
            //Compose a primary key value
            string idGrupo = Convert.ToString(e.GetListSourceFieldValue("IdGrupo"));
            string idItem = Convert.ToString(e.GetListSourceFieldValue("IdItem"));
            e.Value = idGrupo + "|" + idItem;
        }
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        EnquadraGrupoItem enquadraGrupoItem = new EnquadraGrupoItem();

        if (!enquadraGrupoItem.LoadByPrimaryKey(Convert.ToInt32(e.NewValues["IdGrupo"]),
                                                Convert.ToInt32(e.NewValues["IdItem"]))) {
            enquadraGrupoItem.IdGrupo = Convert.ToInt32(e.NewValues["IdGrupo"]);
            enquadraGrupoItem.IdItem = Convert.ToInt32(e.NewValues["IdItem"]);
            enquadraGrupoItem.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de EnquadraGrupoItem - Operacao: Insert EnquadraGrupoItem: " + enquadraGrupoItem.IdItem + "; " + enquadraGrupoItem.IdGrupo + UtilitarioWeb.ToString(enquadraGrupoItem),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesIdGrupo = gridCadastro.GetSelectedFieldValues("IdGrupo");
            List<object> keyValuesIdItem = gridCadastro.GetSelectedFieldValues("IdItem");
            for (int i = 0; i < keyValuesIdGrupo.Count; i++) {
                int idGrupo = Convert.ToInt32(keyValuesIdGrupo[i]);
                int iditem = Convert.ToInt32(keyValuesIdItem[i]);

                EnquadraGrupoItem enquadraGrupoItem = new EnquadraGrupoItem();
                if (enquadraGrupoItem.LoadByPrimaryKey(idGrupo, iditem)) {

                    EnquadraGrupoItem enquadraGrupoItemClone = (EnquadraGrupoItem)Utilitario.Clone(enquadraGrupoItem);
                    //                    
                    enquadraGrupoItem.MarkAsDeleted();
                    enquadraGrupoItem.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de EnquadraGrupoItem - Operacao: Delete EnquadraGrupoItem: " + idGrupo + "; " + iditem + UtilitarioWeb.ToString(enquadraGrupoItemClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    new protected void gridCadastro_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e) {
        (e.Editor as ASPxTextEdit).ValidationSettings.RequiredField.IsRequired = true;
        (e.Editor as ASPxTextEdit).ValidationSettings.RequiredField.ErrorText = this.MsgCampoObrigatorio;

        if (!gridCadastro.IsNewRowEditing) {
            e.Editor.Enabled = false;
            e.Editor.BackColor = Color.FromName("#EBEBEB");
            e.Editor.ReadOnly = true;
        }
    }
}