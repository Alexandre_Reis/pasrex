using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Enquadra;
using Financial.Enquadra.Enums;
using EntitySpaces.Interfaces;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_EnquadraItemTermoBolsa : Financial.Web.Common.CadastroBasePage {
    protected void EsDSEnquadraItemTermoBolsa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        EnquadraItemQuery enquadraItemQuery = new EnquadraItemQuery("I");
        EnquadraItemTermoBolsaQuery enquadraItemTermoBolsaQuery = new EnquadraItemTermoBolsaQuery("A");

        enquadraItemQuery.Select(enquadraItemTermoBolsaQuery, enquadraItemQuery.Descricao);
        enquadraItemQuery.InnerJoin(enquadraItemTermoBolsaQuery).On(enquadraItemQuery.IdItem == enquadraItemTermoBolsaQuery.IdItem);
        enquadraItemQuery.OrderBy(enquadraItemQuery.Descricao.Ascending);

        EnquadraItemCollection coll = new EnquadraItemCollection();
        coll.Load(enquadraItemQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        EnquadraItem enquadraItem = new EnquadraItem();
        EnquadraItemTermoBolsa enquadraItemTermoBolsa = new EnquadraItemTermoBolsa();

        if (enquadraItem.LoadByPrimaryKey((int)e.Keys[0])) {
            enquadraItem.Descricao = Convert.ToString(e.NewValues["Descricao"]);
            enquadraItem.Save();

            if (enquadraItemTermoBolsa.LoadByPrimaryKey((int)e.Keys[0]))
            {
                if (e.NewValues["Posicao"] != null)
                {
                    enquadraItemTermoBolsa.Posicao = Convert.ToString(e.NewValues["Posicao"]).Trim().ToUpper();
                }
                else
                {
                    enquadraItemTermoBolsa.Posicao = null;
                }

                enquadraItemTermoBolsa.Save();
            }

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de EnquadraItemTermoBolsa - Operacao: Update EnquadraItemTermoBolsa: " + (int)e.Keys[0] + UtilitarioWeb.ToString(enquadraItem),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        EnquadraItem enquadraItem = new EnquadraItem();
        EnquadraItemTermoBolsa enquadraItemTermoBolsa = new EnquadraItemTermoBolsa();

        enquadraItem.Descricao = Convert.ToString(e.NewValues["Descricao"]);
        enquadraItem.Tipo = (int)TipoItemEnquadra.TermoBolsa;

        using (esTransactionScope scope = new esTransactionScope()) {
            enquadraItem.Save();

            enquadraItemTermoBolsa.IdItem = enquadraItem.IdItem.Value; //Usa o mesmo Id da tabela EnquadraItem!

            if (e.NewValues["Posicao"] != null)
            {
                enquadraItemTermoBolsa.Posicao = Convert.ToString(e.NewValues["Posicao"]).Trim().ToUpper();
            }
            else
            {
                enquadraItemTermoBolsa.Posicao = null;
            }

            enquadraItemTermoBolsa.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de EnquadraItemTermoBolsa - Operacao: Insert EnquadraItemTermoBolsa: " + enquadraItemTermoBolsa.IdItem + UtilitarioWeb.ToString(enquadraItemTermoBolsa),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion

            scope.Complete();
        }
        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        
        if (e.Parameters == "btnDelete") 
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdItem");
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idItem = Convert.ToInt32(keyValuesId[i]);

                using (esTransactionScope scope = new esTransactionScope()) {
                    EnquadraItemTermoBolsa enquadraItemTermoBolsa = new EnquadraItemTermoBolsa();
                    if (enquadraItemTermoBolsa.LoadByPrimaryKey(idItem)) {

                        //
                        EnquadraItemTermoBolsa enquadraItemTermoBolsaClone = (EnquadraItemTermoBolsa)Utilitario.Clone(enquadraItemTermoBolsa);
                        //

                        enquadraItemTermoBolsa.MarkAsDeleted();
                        enquadraItemTermoBolsa.Save();

                        #region Log do Processo
                        HistoricoLog historicoLog = new HistoricoLog();
                        historicoLog.InsereHistoricoLog(DateTime.Now,
                                                        DateTime.Now,
                                                        "Cadastro de EnquadraItemTermoBolsa - Operacao: Delete EnquadraItemTermoBolsa: " + idItem + UtilitarioWeb.ToString(enquadraItemTermoBolsaClone),
                                                        HttpContext.Current.User.Identity.Name,
                                                        UtilitarioWeb.GetIP(Request),
                                                        "",
                                                        HistoricoLogOrigem.Outros);
                        #endregion

                    }

                    EnquadraItem enquadraItem = new EnquadraItem();
                    if (enquadraItem.LoadByPrimaryKey(idItem)) {
                        enquadraItem.MarkAsDeleted();
                        enquadraItem.Save();
                    }
                    scope.Complete();
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }
}