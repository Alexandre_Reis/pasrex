﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Enquadra;
using Financial.Enquadra.Enums;
using EntitySpaces.Interfaces;
using Financial.BMF;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_EnquadraItemFuturoBMF : Financial.Web.Common.CadastroBasePage {
    // TODO - Colocar no Resource
    static readonly string messegemErroCodigoBMFInexistente = "Código BMF Não Existente";

    protected void EsDSEnquadraItemFuturoBMF_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        EnquadraItemQuery enquadraItemQuery = new EnquadraItemQuery("I");
        EnquadraItemFuturoBMFQuery enquadraItemFuturoBMFQuery = new EnquadraItemFuturoBMFQuery("A");

        enquadraItemFuturoBMFQuery.Select(enquadraItemFuturoBMFQuery, enquadraItemQuery.Descricao);
        enquadraItemFuturoBMFQuery.InnerJoin(enquadraItemQuery).On(enquadraItemQuery.IdItem == enquadraItemFuturoBMFQuery.IdItem);
        enquadraItemFuturoBMFQuery.OrderBy(enquadraItemQuery.Descricao.Ascending);

        EnquadraItemFuturoBMFCollection coll = new EnquadraItemFuturoBMFCollection();
        coll.Load(enquadraItemFuturoBMFQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) {
        if (e.Column.FieldName == "Descricao") {
            e.Value = Convert.ToString(e.GetListSourceFieldValue(EnquadraItemMetadata.ColumnNames.Descricao));
        }
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        EnquadraItem enquadraItem = new EnquadraItem();
        EnquadraItemFuturoBMF enquadraItemFuturoBMF = new EnquadraItemFuturoBMF();

        using (esTransactionScope scope = new esTransactionScope()) {
            if (enquadraItem.LoadByPrimaryKey((int)e.Keys[0])) {
                enquadraItem.Descricao = Convert.ToString(e.NewValues["Descricao"]);
                enquadraItem.Save();
            }

            if (e.Keys[0] != null) {
                if (enquadraItemFuturoBMF.LoadByPrimaryKey((int)e.Keys[0])) {
                    if (e.NewValues["CdAtivoBMF"] != null) {
                        AtivoBMFCollection ativoBMFCollection = new AtivoBMFCollection();
                        ativoBMFCollection.Query.Select(ativoBMFCollection.Query.CdAtivoBMF)
                                                .Where(ativoBMFCollection.Query.CdAtivoBMF == Convert.ToString(e.NewValues["CdAtivoBMF"]).Trim().ToUpper());

                        if (!ativoBMFCollection.Query.Load()) {
                            throw new Exception(messegemErroCodigoBMFInexistente);
                        }

                        enquadraItemFuturoBMF.CdAtivoBMF = Convert.ToString(e.NewValues["CdAtivoBMF"]).Trim().ToUpper();
                    }
                    else {
                        enquadraItemFuturoBMF.CdAtivoBMF = null;
                    }

                    if (e.NewValues["Posicao"] != null)
                    {
                        enquadraItemFuturoBMF.Posicao = Convert.ToString(e.NewValues["Posicao"]).Trim().ToUpper();
                    }
                    else
                    {
                        enquadraItemFuturoBMF.Posicao = null;
                    }

                    enquadraItemFuturoBMF.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de EnquadraItemFuturoBMF - Operacao: Update EnquadraItemFuturoBMF: " + (int)e.Keys[0] + UtilitarioWeb.ToString(enquadraItemFuturoBMF),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }

            scope.Complete();
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        EnquadraItem enquadraItem = new EnquadraItem();
        EnquadraItemFuturoBMF enquadraItemFuturoBMF = new EnquadraItemFuturoBMF();

        enquadraItem.Descricao = Convert.ToString(e.NewValues["Descricao"]);
        enquadraItem.Tipo = (int)TipoItemEnquadra.FuturoBMF;

        using (esTransactionScope scope = new esTransactionScope()) {
            enquadraItem.Save();

            enquadraItemFuturoBMF.IdItem = enquadraItem.IdItem.Value; //Usa o mesmo Id da tabela EnquadraItem!

            if (e.NewValues["CdAtivoBMF"] != null) {
                AtivoBMFCollection ativoBMFCollection = new AtivoBMFCollection();
                ativoBMFCollection.Query.Select(ativoBMFCollection.Query.CdAtivoBMF)
                                        .Where(ativoBMFCollection.Query.CdAtivoBMF == Convert.ToString(e.NewValues["CdAtivoBMF"]).Trim().ToUpper());

                if (!ativoBMFCollection.Query.Load()) {
                    throw new Exception(messegemErroCodigoBMFInexistente);
                }

                enquadraItemFuturoBMF.CdAtivoBMF = Convert.ToString(e.NewValues["CdAtivoBMF"]).Trim().ToUpper();
            }
            else {
                enquadraItemFuturoBMF.CdAtivoBMF = null;
            }

            if (e.NewValues["Posicao"] != null)
            {
                enquadraItemFuturoBMF.Posicao = Convert.ToString(e.NewValues["Posicao"]).Trim().ToUpper();
            }
            else
            {
                enquadraItemFuturoBMF.Posicao = null;
            }

            enquadraItemFuturoBMF.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de EnquadraItemFuturoBMF - Operacao: Insert EnquadraItemFuturoBMF: " + enquadraItemFuturoBMF.IdItem + UtilitarioWeb.ToString(enquadraItemFuturoBMF),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion

            scope.Complete();
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {

        if (e.Parameters == "btnDelete") {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdItem");
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idItem = Convert.ToInt32(keyValuesId[i]);

                using (esTransactionScope scope = new esTransactionScope()) {
                    EnquadraItemFuturoBMF enquadraItemFuturoBMF = new EnquadraItemFuturoBMF();
                    if (enquadraItemFuturoBMF.LoadByPrimaryKey(idItem)) {

                        //
                        EnquadraItemFuturoBMF enquadraItemFuturoBMFClone = (EnquadraItemFuturoBMF)Utilitario.Clone(enquadraItemFuturoBMF);
                        //

                        enquadraItemFuturoBMF.MarkAsDeleted();
                        enquadraItemFuturoBMF.Save();

                        #region Log do Processo
                        HistoricoLog historicoLog = new HistoricoLog();
                        historicoLog.InsereHistoricoLog(DateTime.Now,
                                                        DateTime.Now,
                                                        "Cadastro de EnquadraItemFuturoBMF - Operacao: Delete EnquadraItemFuturoBMF: " + idItem + UtilitarioWeb.ToString(enquadraItemFuturoBMFClone),
                                                        HttpContext.Current.User.Identity.Name,
                                                        UtilitarioWeb.GetIP(Request),
                                                        "",
                                                        HistoricoLogOrigem.Outros);
                        #endregion
                    }

                    EnquadraItem enquadraItem = new EnquadraItem();
                    if (enquadraItem.LoadByPrimaryKey(idItem)) {
                        enquadraItem.MarkAsDeleted();
                        enquadraItem.Save();
                    }

                    scope.Complete();
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }
}