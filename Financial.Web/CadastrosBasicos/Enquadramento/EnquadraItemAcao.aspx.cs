using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Enquadra;
using Financial.Enquadra.Enums;
using EntitySpaces.Interfaces;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Bolsa;
using Financial.Bolsa.Enums;

public partial class CadastrosBasicos_EnquadraItemAcao : Financial.Web.Common.CadastroBasePage
{
    #region DataSources
    protected void EsDSEnquadraItemAcao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        EnquadraItemQuery enquadraItemQuery = new EnquadraItemQuery("I");
        EnquadraItemAcaoQuery enquadraItemAcaoQuery = new EnquadraItemAcaoQuery("A");

        enquadraItemAcaoQuery.Select(enquadraItemAcaoQuery, enquadraItemQuery.Descricao);
        enquadraItemAcaoQuery.InnerJoin(enquadraItemQuery).On(enquadraItemQuery.IdItem == enquadraItemAcaoQuery.IdItem);
        enquadraItemAcaoQuery.OrderBy(enquadraItemQuery.Descricao.Ascending);

        EnquadraItemAcaoCollection coll = new EnquadraItemAcaoCollection();
        coll.Load(enquadraItemAcaoQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSEmissorInner_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        EmissorCollection coll = new EmissorCollection();
        EmissorQuery emissorQuery = new EmissorQuery("e");

        EnquadraItemAcaoQuery enquadraItemAcaoQuery = new EnquadraItemAcaoQuery("A");
        emissorQuery.Select(emissorQuery.IdEmissor, emissorQuery.Nome);
        emissorQuery.InnerJoin(enquadraItemAcaoQuery).On(emissorQuery.IdEmissor == enquadraItemAcaoQuery.IdEmissor);
        emissorQuery.es.Distinct = true;

        //coll.Query.es.Top = 1;
        emissorQuery.OrderBy(emissorQuery.Nome.Ascending);
        coll.Load(emissorQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSEmissor_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        EmissorCollection coll = new EmissorCollection();

        EmissorQuery emissorQuery = new EmissorQuery("e");
        AtivoBolsaQuery ativobolsaQuery = new AtivoBolsaQuery("A");

        emissorQuery.Select(emissorQuery.IdEmissor, emissorQuery.Nome);
        emissorQuery.InnerJoin(ativobolsaQuery).On(emissorQuery.IdEmissor == ativobolsaQuery.IdEmissor);
        emissorQuery.Where(ativobolsaQuery.TipoMercado.In(TipoMercadoBolsa.MercadoVista, TipoMercadoBolsa.Imobiliario));
        emissorQuery.es.Distinct = true;
        emissorQuery.OrderBy(emissorQuery.Nome.Ascending);

        coll.Load(emissorQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSSetorInner_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        SetorCollection coll = new SetorCollection();
        
        EnquadraItemAcaoQuery enquadraItemAcaoQuery = new EnquadraItemAcaoQuery("A");
        SetorQuery setorQuery = new SetorQuery("Q");

        setorQuery.Select(setorQuery.IdSetor, setorQuery.Nome);
        setorQuery.InnerJoin(enquadraItemAcaoQuery).On(setorQuery.IdSetor == enquadraItemAcaoQuery.IdSetor);
        setorQuery.es.Distinct = true;
        setorQuery.OrderBy(setorQuery.Nome.Ascending);

        coll.Load(setorQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSSetor_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        SetorCollection coll = new SetorCollection();

        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion
    

    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "Descricao")
        {
            e.Value = Convert.ToString(e.GetListSourceFieldValue(EnquadraItemMetadata.ColumnNames.Descricao));
        }
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        EnquadraItem enquadraItem = new EnquadraItem();
        EnquadraItemAcao enquadraItemAcao = new EnquadraItemAcao();

        ASPxComboBox dropEmissor = gridCadastro.FindEditFormTemplateControl("dropEmissor") as ASPxComboBox;
        ASPxTextBox txtDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
        ASPxComboBox dropSetor = gridCadastro.FindEditFormTemplateControl("dropSetor") as ASPxComboBox;
        ASPxComboBox dropTipoPapel = gridCadastro.FindEditFormTemplateControl("dropTipoPapel") as ASPxComboBox;
        ASPxComboBox dropTipoMercado = gridCadastro.FindEditFormTemplateControl("dropTipoMercado") as ASPxComboBox;
        ASPxComboBox dropBloqueado = gridCadastro.FindEditFormTemplateControl("dropBloqueado") as ASPxComboBox;

        using (esTransactionScope scope = new esTransactionScope())
        {
            if (enquadraItem.LoadByPrimaryKey((int)e.Keys[0]))
            {
                enquadraItem.Descricao = Convert.ToString(txtDescricao.Value);
                enquadraItem.Save();
            }

            if (enquadraItemAcao.LoadByPrimaryKey((int)e.Keys[0]))
            {
                if (dropEmissor.Value != "")
                {
                    enquadraItemAcao.IdEmissor = Convert.ToInt32(dropEmissor.Value);
                }
                if (dropSetor.Value != "")
                {
                    enquadraItemAcao.IdSetor = Convert.ToInt16(dropSetor.Value);
                }
                if (dropTipoPapel.Value != "")
                {
                    enquadraItemAcao.TipoPapel = Convert.ToByte(dropTipoPapel.Value);
                }
                if (dropTipoMercado.Value != "")
                {
                    enquadraItemAcao.TipoMercado = Convert.ToString(dropTipoMercado.Value);
                }
                if (dropBloqueado.Value != "")
                {
                    enquadraItemAcao.Bloqueado = Convert.ToString(dropBloqueado.Value);
                }

                enquadraItemAcao.Save();

                #region Log do Processo
                HistoricoLog historicoLog = new HistoricoLog();
                historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                "Cadastro de EnquadraItemAcao - Operacao: Update EnquadraItemAcao: " + (int)e.Keys[0] + UtilitarioWeb.ToString(enquadraItemAcao),
                                                HttpContext.Current.User.Identity.Name,
                                                UtilitarioWeb.GetIP(Request),
                                                "",
                                                HistoricoLogOrigem.Outros);
                #endregion
            }
            scope.Complete();
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    private void SalvarNovo()
    {
        EnquadraItem enquadraItem = new EnquadraItem();
        EnquadraItemAcao enquadraItemAcao = new EnquadraItemAcao();

        ASPxComboBox dropEmissor = gridCadastro.FindEditFormTemplateControl("dropEmissor") as ASPxComboBox;
        ASPxTextBox txtDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as ASPxTextBox;
        ASPxComboBox dropSetor = gridCadastro.FindEditFormTemplateControl("dropSetor") as ASPxComboBox;
        ASPxComboBox dropTipoPapel = gridCadastro.FindEditFormTemplateControl("dropTipoPapel") as ASPxComboBox;
        ASPxComboBox dropTipoMercado = gridCadastro.FindEditFormTemplateControl("dropTipoMercado") as ASPxComboBox;
        ASPxComboBox dropBloqueado = gridCadastro.FindEditFormTemplateControl("dropBloqueado") as ASPxComboBox;
        
        enquadraItem.Descricao = txtDescricao.Text;

        if (dropTipoMercado.Text == TipoMercadoBolsa.Imobiliario)
        {
            enquadraItem.Tipo = 2;
        }
        else
        {
            enquadraItem.Tipo = 1;
        }

        enquadraItem.Descricao = Convert.ToString(txtDescricao.Value);

        using (esTransactionScope scope = new esTransactionScope())
        {
            enquadraItem.Save();

            enquadraItemAcao.IdItem = enquadraItem.IdItem.Value; //Usa o mesmo Id da tabela EnquadraItem!

            if (dropEmissor.Value != "")
            {
                enquadraItemAcao.IdEmissor = Convert.ToInt32(dropEmissor.Value);
            }
            if (dropSetor.Value != "")
            {
                enquadraItemAcao.IdSetor = Convert.ToInt16(dropSetor.Value);
            }
            if (dropTipoPapel.Value != "")
            {
                enquadraItemAcao.TipoPapel = Convert.ToByte(dropTipoPapel.Value);
            }
            if (dropTipoMercado.Value != "")
            {
                enquadraItemAcao.TipoMercado = Convert.ToString(dropTipoMercado.Value);
            }
            if (dropBloqueado.Value != "")
            {
                enquadraItemAcao.Bloqueado = Convert.ToString(dropBloqueado.Value);
            }

            enquadraItemAcao.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de EnquadraItemAcao - Operacao: Insert EnquadraItemAcao: " + enquadraItemAcao.IdItem + UtilitarioWeb.ToString(enquadraItemAcao),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion

            scope.Complete();
        }
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdItem");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idItem = Convert.ToInt32(keyValuesId[i]);

                using (esTransactionScope scope = new esTransactionScope())
                {
                    EnquadraItemAcao enquadraItemAcao = new EnquadraItemAcao();
                    if (enquadraItemAcao.LoadByPrimaryKey(idItem))
                    {
                        //
                        EnquadraItemAcao enquadraItemAcaoClone = (EnquadraItemAcao)Utilitario.Clone(enquadraItemAcao);
                        //

                        enquadraItemAcao.MarkAsDeleted();
                        enquadraItemAcao.Save();

                        #region Log do Processo
                        HistoricoLog historicoLog = new HistoricoLog();
                        historicoLog.InsereHistoricoLog(DateTime.Now,
                                                        DateTime.Now,
                                                        "Cadastro de EnquadraItemAcao - Operacao: Delete EnquadraItemAcao: " + idItem + UtilitarioWeb.ToString(enquadraItemAcaoClone),
                                                        HttpContext.Current.User.Identity.Name,
                                                        UtilitarioWeb.GetIP(Request),
                                                        "",
                                                        HistoricoLogOrigem.Outros);
                        #endregion
                    }

                    EnquadraItem enquadraItem = new EnquadraItem();
                    if (enquadraItem.LoadByPrimaryKey(idItem))
                    {
                        enquadraItem.MarkAsDeleted();
                        enquadraItem.Save();
                    }
                    scope.Complete();
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }


}