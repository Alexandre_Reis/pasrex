﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using System.Globalization;

using Financial.Util;
using Financial.Web.Common;
using Financial.Util.ConfiguracaoSistema;

using Financial.ContaCorrente.Enums;
using Financial.Bolsa.Enums;
using Financial.BMF.Enums;
using Financial.Fundo.Enums;
using Financial.Util.Enums;

using log4net;

using EntitySpaces.Interfaces;

using DevExpress.Web;
using Financial.Security;

public partial class ConfigCliente : BasePage {
    private static readonly ILog log = LogManager.GetLogger(typeof(ConfigCliente));

    new protected void Page_Load(object sender, EventArgs e) {
        if (!Page.IsPostBack) {
            this.PreencheStatusCheckBox();
        }
    }

    /// <summary>
    /// Carrega Status Incial dos CheckBox de acordo com as informações presentes no Database
    /// </summary>
    private void PreencheStatusCheckBox() {        
        bool status = ParametrosConfiguracaoSistema.Seguranca.Menu.AcessoAcoesOpcoes;
        this.chkAcoesOpcoes.Checked = status;

        status = ParametrosConfiguracaoSistema.Seguranca.Menu.AcessoTermoOpcoes;
        this.chkTermoAcoes.Checked = status;

        status = ParametrosConfiguracaoSistema.Seguranca.Menu.AcessoBTCAcoes;
        this.chkAluguelAcoes.Checked = status;

        status = ParametrosConfiguracaoSistema.Seguranca.Menu.AcessoBMF;
        this.chkBMF.Checked = status;

        status = ParametrosConfiguracaoSistema.Seguranca.Menu.AcessoRendaFixa;
        this.chkRendaFixa.Checked = status;

        status = ParametrosConfiguracaoSistema.Seguranca.Menu.AcessoFundos;
        this.chkFundos.Checked = status;

        status = ParametrosConfiguracaoSistema.Seguranca.Menu.AcessoSwap;
        this.chkSwap.Checked = status;
                      
        status = ParametrosConfiguracaoSistema.Seguranca.Menu.AcessoGerencial;
        this.chkGerencial.Checked = status;

        status = ParametrosConfiguracaoSistema.Seguranca.Menu.AcessoCalculoDespesas;
        this.chkCalculoDespesas.Checked = status;

        status = ParametrosConfiguracaoSistema.Seguranca.Menu.AcessoRebates;
        this.chkRebates.Checked = status;

        status = ParametrosConfiguracaoSistema.Seguranca.Menu.AcessoEnquadramento;
        this.chkEnquadramento.Checked = status;

        status = ParametrosConfiguracaoSistema.Seguranca.Menu.AcessoCotista;
        this.chkCotista.Checked = status;

        status = ParametrosConfiguracaoSistema.Seguranca.Menu.AcessoContabil;
        this.chkContabil.Checked = status;

        status = ParametrosConfiguracaoSistema.Seguranca.Menu.AcessoApuracaoIR;
        this.chkApuracaoIR.Checked = status;

        status = ParametrosConfiguracaoSistema.Seguranca.Menu.AcessoInformeFundos;
        this.chkInformeFundos.Checked = status;

        status = ParametrosConfiguracaoSistema.Seguranca.Menu.AcessoInformeClubes;
        this.chkInformeClubes.Checked = status;

        status = ParametrosConfiguracaoSistema.Seguranca.Menu.AcessoFrontOffice;
        this.chkFrontOffice.Checked = status;

        status = ParametrosConfiguracaoSistema.Seguranca.Menu.AcessoCarteiraSimulada;
        this.chkCarteiraSimulada.Checked = status;

        //Se todos os CheckBox = true então Todos = true        
        //if (this.chkAcoesOpcoes.Checked && this.chkTermoAcoes.Checked &&
        //    this.chkAlguelAcoes.Checked && this.chkBMF.Checked &&
        //    this.chkRendaFixa.Checked && this.chkFundos.Checked &&
        //    this.chkSwap.Checked && this.chkGerencial.Checked &&
        //    this.chkCalculoDespesas.Checked && this.chkRebates.Checked &&
        //    this.chkEnquadramento.Checked && this.chkCotista.Checked &&
        //    this.chkContabil.Checked && this.chkApuracaoIR.Checked &&
        //    this.chkInformeFundos.Checked && this.chkInformeClubes.Checked) {

        //    this.chkTodos.Checked = true;
        //}        
    }

    /// <summary>
    /// Salva os Parametros de Configuração no Banco
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>   
    protected void btnSalvar_Click(object sender, EventArgs e) {
        
        ConfiguracaoCollection configuracaoCollection = new ConfiguracaoCollection();
        configuracaoCollection.Query.Where(configuracaoCollection.Query.Id >= 3000);
        configuracaoCollection.Query.Load();
        //
        for (int i = 0; i < configuracaoCollection.Count; i++) {
            int id = configuracaoCollection[i].Id.Value;
            //                   
            switch (id) {

                #region Parametros Seguranca
                case ConfiguracaoRegrasNegocio.Seguranca.Menu.AcoesOpcoes:
                    configuracaoCollection[i].ValorTexto = (bool)this.chkAcoesOpcoes.Value ? "S" : "N";
                    break;
                case ConfiguracaoRegrasNegocio.Seguranca.Menu.TermoAcoes:
                    configuracaoCollection[i].ValorTexto = (bool)this.chkTermoAcoes.Value ? "S" : "N";
                    break;
                case ConfiguracaoRegrasNegocio.Seguranca.Menu.BTC_Aluguel:
                    configuracaoCollection[i].ValorTexto = (bool)this.chkAluguelAcoes.Value ? "S" : "N";
                    break;
                case ConfiguracaoRegrasNegocio.Seguranca.Menu.BMF:
                    configuracaoCollection[i].ValorTexto = (bool)this.chkBMF.Value ? "S" : "N";
                    break;
                case ConfiguracaoRegrasNegocio.Seguranca.Menu.RendaFixa:
                    configuracaoCollection[i].ValorTexto = (bool)this.chkRendaFixa.Value ? "S" : "N";
                    break;
                case ConfiguracaoRegrasNegocio.Seguranca.Menu.Fundos:
                    configuracaoCollection[i].ValorTexto = (bool)this.chkFundos.Value ? "S" : "N";
                    break;
                case ConfiguracaoRegrasNegocio.Seguranca.Menu.Swap:
                    configuracaoCollection[i].ValorTexto = (bool)this.chkSwap.Value ? "S" : "N";
                    break;
                case ConfiguracaoRegrasNegocio.Seguranca.Menu.Gerencial:
                    configuracaoCollection[i].ValorTexto = (bool)this.chkGerencial.Value ? "S" : "N";
                    break;
                case ConfiguracaoRegrasNegocio.Seguranca.Menu.CalculoDespesas:
                    configuracaoCollection[i].ValorTexto = (bool)this.chkCalculoDespesas.Value ? "S" : "N";
                    break;
                case ConfiguracaoRegrasNegocio.Seguranca.Menu.Rebates:
                    configuracaoCollection[i].ValorTexto = (bool)this.chkRebates.Value ? "S" : "N";
                    break;
                case ConfiguracaoRegrasNegocio.Seguranca.Menu.Enquadramento:
                    configuracaoCollection[i].ValorTexto = (bool)this.chkEnquadramento.Value ? "S" : "N";
                    break;
                case ConfiguracaoRegrasNegocio.Seguranca.Menu.Cotista:
                    configuracaoCollection[i].ValorTexto = (bool)this.chkCotista.Value ? "S" : "N";
                    break;
                case ConfiguracaoRegrasNegocio.Seguranca.Menu.Contabil:
                    configuracaoCollection[i].ValorTexto = (bool)this.chkContabil.Value ? "S" : "N";
                    break;
                case ConfiguracaoRegrasNegocio.Seguranca.Menu.ApuracaoIR:
                    configuracaoCollection[i].ValorTexto = (bool)this.chkApuracaoIR.Value ? "S" : "N";
                    break;
                case ConfiguracaoRegrasNegocio.Seguranca.Menu.InformeFundos:
                    configuracaoCollection[i].ValorTexto = (bool)this.chkInformeFundos.Value ? "S" : "N";
                    break;
                case ConfiguracaoRegrasNegocio.Seguranca.Menu.InformeClubes:
                    configuracaoCollection[i].ValorTexto = (bool)this.chkInformeClubes.Value ? "S" : "N";
                    break;
                case ConfiguracaoRegrasNegocio.Seguranca.Menu.FrontOffice:
                    configuracaoCollection[i].ValorTexto = (bool)this.chkFrontOffice.Value ? "S" : "N";
                    break;
                case ConfiguracaoRegrasNegocio.Seguranca.Menu.CarteiraSimulada:
                    configuracaoCollection[i].ValorTexto = (bool)this.chkCarteiraSimulada.Value ? "S" : "N";
                    break;
                #endregion
            }
        }

        // Salva todas as Configurações
        try {

            #region Permissões para o Grupo 0
            FinancialSiteMapProvider s = new FinancialSiteMapProvider();
            //            
            /* Acoes/Opções */
            this.SalvaPermissaoGrupo0(s.MenuAcoesOpcoes, ((bool)this.chkAcoesOpcoes.Value ? "S" : "N"));
            /* Termo Ações */
            this.SalvaPermissaoGrupo0(s.MenuTermoAcoes, ((bool)this.chkTermoAcoes.Value ? "S" : "N"));
            /* BTC/Aluguel Ações */
            this.SalvaPermissaoGrupo0(s.MenuBTCAcoes, ((bool)this.chkAluguelAcoes.Value ? "S" : "N"));
            /* BMF */
            this.SalvaPermissaoGrupo0(s.MenuBMF, ((bool)this.chkBMF.Value ? "S" : "N"));
            /* Renda Fixa */
            this.SalvaPermissaoGrupo0(s.MenuRendaFixa, ((bool)this.chkRendaFixa.Value ? "S" : "N"));
            /* Fundos */
            this.SalvaPermissaoGrupo0(s.MenuFundos, ((bool)this.chkFundos.Value ? "S" : "N"));
            /* Swap */
            this.SalvaPermissaoGrupo0(s.MenuSwap, ((bool)this.chkSwap.Value ? "S" : "N"));
            /* Gerencial */
            this.SalvaPermissaoGrupo0(s.MenuGerencial, ((bool)this.chkGerencial.Value ? "S" : "N"));
            //
            /* Cálculo Despesas */
            this.SalvaPermissaoGrupo0(s.MenuCalculoDespesas, ((bool)this.chkCalculoDespesas.Value ? "S" : "N"));
            /* Rebates */
            this.SalvaPermissaoGrupo0(s.MenuRebates, ((bool)this.chkRebates.Value ? "S" : "N"));
            /* Enquadramento */
            this.SalvaPermissaoGrupo0(s.MenuEnquadramento, ((bool)this.chkEnquadramento.Value ? "S" : "N"));
            /* Cotista */
            this.SalvaPermissaoGrupo0(s.MenuCotista, ((bool)this.chkCotista.Value ? "S" : "N"));
            /* Contábil */
            this.SalvaPermissaoGrupo0(s.MenuContabil, ((bool)this.chkContabil.Value ? "S" : "N"));

            /* Apuração IR */
            /* Informes Fundos */
            /* Informes Clubes   */
            //
            //<siteMapNode url="" title="$resources:Menu,#Clubes" menuID="25100">
            //  <siteMapNode title="$resources:Menu,#InformeMensalClubes" url="~/Relatorios/Legais/Clubes/FiltroReportInformeMensalClubes.aspx" menuID="25110"/>
            //</siteMapNode>
            //<siteMapNode url="" title="$resources:Menu,#ImpostoRenda" menuID="25200">
            //  <siteMapNode title="$resources:Menu,#InformeRendimentos" url="~/Relatorios/Legais/ImpostoRenda/FiltroReportInformeRendimentos.aspx" menuID="25220"/>
            //  <siteMapNode title="$resources:Menu,#GanhosRendaVariavel" url="~/Relatorios/Legais/ImpostoRenda/FiltroReportGanhoRendaVariavel.aspx" menuID="25230"/>
            //  <siteMapNode title="$resources:Menu,#InformeContabilPJ" url="~/Relatorios/Legais/ImpostoRenda/FiltroReportInformeContabilPJ.aspx" menuID="25240"/>          
            //</siteMapNode>    
            //
            bool apuracaoIR = (bool)this.chkApuracaoIR.Value;
            bool informeFundos = (bool)this.chkInformeFundos.Value;
            bool informeClubes = (bool)this.chkInformeClubes.Value;
            //
            List<int> idMenu = new List<int>();
            //
            idMenu.Add(25100);
            this.SalvaPermissaoGrupo0(idMenu, informeClubes ? "S" : "N");
            //

            idMenu.Clear();
            idMenu.Add(25220);
            if (!informeClubes && !informeFundos) {
                this.SalvaPermissaoGrupo0(idMenu, "N");    
            }

            if (informeClubes && informeFundos) {
                this.SalvaPermissaoGrupo0(idMenu, "S");
            }
            //

            idMenu.Clear();
            idMenu.Add(25230);
            this.SalvaPermissaoGrupo0(idMenu, apuracaoIR ? "S" : "N");
            //
                
            idMenu.Clear();
            idMenu.Add(25240);
            this.SalvaPermissaoGrupo0(idMenu, informeFundos ? "S" : "N");
            //

            List<int> idMenus = new List<int>( new int[] { 25000, 25200 }  );
            if (!informeClubes && !informeFundos && !apuracaoIR) {
                this.SalvaPermissaoGrupo0(idMenus, "N");
            }
            
            if (informeClubes && informeFundos && apuracaoIR) {
                this.SalvaPermissaoGrupo0(idMenus, "S");
            }

            #endregion

            configuracaoCollection.Save();
        }
        catch (Exception ex) {
            throw new Exception(ex.Message);
        }
        string mensagem = "Alterações Salvas com Sucesso.";
        throw new Exception(mensagem);
    }

    /// <summary>
    /// Atualiza permissões para o grupo 0 baseado num conjunto de menus
    /// </summary>
    /// <param name="idMenus">lista de id Menu</param>
    /// <param name="permissao">S para indicar permissão ou N para não permissão</param>
    private void SalvaPermissaoGrupo0(List<int> idMenus, string permissao) {
        PermissaoMenuCollection p = new PermissaoMenuCollection();
        //
        p.Query.Where(p.Query.IdGrupo == 0 && p.Query.IdMenu.In(idMenus));
        p.Query.Load();

        for (int i = 0; i < p.Count; i++) {
            p[i].PermissaoLeitura = permissao;
        }
        p.Save();
    }

    /// <summary>
    /// Funcionalidade de Clique no CheckBox Todos
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chkTodos_CheckedChanged(object sender, EventArgs e) 
    {
        this.chkAcoesOpcoes.Checked = (bool)this.chkTodos.Value;
        this.chkTermoAcoes.Checked = (bool)this.chkTodos.Value;
        this.chkAluguelAcoes.Checked = (bool)this.chkTodos.Value;
        this.chkBMF.Checked = (bool)this.chkTodos.Value;
        this.chkRendaFixa.Checked = (bool)this.chkTodos.Value;
        this.chkFundos.Checked = (bool)this.chkTodos.Value;
        this.chkSwap.Checked = (bool)this.chkTodos.Value;
        this.chkGerencial.Checked = (bool)this.chkTodos.Value;
        this.chkCalculoDespesas.Checked = (bool)this.chkTodos.Value;
        this.chkRebates.Checked = (bool)this.chkTodos.Value;
        this.chkEnquadramento.Checked = (bool)this.chkTodos.Value;
        this.chkCotista.Checked = (bool)this.chkTodos.Value;
        this.chkContabil.Checked = (bool)this.chkTodos.Value;
        this.chkApuracaoIR.Checked = (bool)this.chkTodos.Value;
        this.chkInformeFundos.Checked = (bool)this.chkTodos.Value;
        this.chkInformeClubes.Checked = (bool)this.chkTodos.Value;
        this.chkFrontOffice.Checked = (bool)this.chkTodos.Value;
        this.chkCarteiraSimulada.Checked = (bool)this.chkTodos.Value;
    }    
}