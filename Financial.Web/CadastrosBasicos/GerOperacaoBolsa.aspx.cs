﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using DevExpress.Web;
using DevExpress.Web.Data;

using Financial.Common;
using Financial.Util;
using Financial.Investidor;
using Financial.ContaCorrente;
using Financial.Security;
using Financial.Fundo;
using Financial.InvestidorCotista;
using Financial.Bolsa;
using Financial.Web.Common;
using Financial.WebConfigConfiguration;

using Financial.ContaCorrente.Enums;
using Financial.InvestidorCotista.Enums;
using Financial.Investidor.Enums;
using Financial.Common.Enums;
using Financial.Bolsa.Enums;

using EntitySpaces.Interfaces;
using System.Threading;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Gerencial;

public partial class CadastrosBasicos_GerOperacaoBolsa : CadastroBasePage {

    //protected void Page_PreRender(object sender, EventArgs e) {
    //    HtmlMeta metatag = new HtmlMeta();
    //    metatag.Attributes.Add("http-equiv", "X-UA-Compatible");
    //    metatag.Attributes.Add("content", "ID=IE=EmulateIE7");
    //    Page.Header.Controls.AddAt(0, metatag);

    //}

    new protected void Page_Load(object sender, EventArgs e) {
        this.HasFiltro = true;
        this.HasPopupCliente = true;
        this.HasPopupAtivoBolsa = true;
        this.HasPanelFieldsLoading = true;
        base.Page_Load(sender, e);

        this.gridCadastro.ClientSideEvents.RowDblClick = "function(s, e) {selectedIndex = e.visibleIndex; gridCadastro.GetRowValues(e.visibleIndex, 'Origem', OpenPopup); }";

        if (!Page.IsPostBack)
        {
            List<Hashtable> list = Combos.carregaComboTipoMercadoBolsa(ConfiguracaoCombo.OpcaoBranco);

            for (int i = 0; i < list.Count; i++)
            {
                // Hashtable
                Hashtable hashtable = (Hashtable)list[i];

                foreach (DictionaryEntry item in hashtable)
                {
                    ListEditItem listItem = new ListEditItem();
                    //
                    listItem.Text = item.Value.ToString();
                    listItem.Value = item.Key.ToString();
                    //
                    dropTipoMercado.Items.Add(listItem);
                }
            }
        }
    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSGerOperacaoBolsa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
        GerOperacaoBolsaQuery gerOperacaoBolsaQuery = new GerOperacaoBolsaQuery("O");
        ClienteQuery clienteQuery = new ClienteQuery("C");

        gerOperacaoBolsaQuery.Select(gerOperacaoBolsaQuery, clienteQuery.Apelido.As("Apelido"));
        gerOperacaoBolsaQuery.InnerJoin(clienteQuery).On(gerOperacaoBolsaQuery.IdCliente == clienteQuery.IdCliente);
        gerOperacaoBolsaQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        gerOperacaoBolsaQuery.Where(permissaoClienteQuery.IdUsuario.Equal(idUsuario));

        if (!String.IsNullOrEmpty(btnEditCodigoClienteFiltro.Text)) {
            gerOperacaoBolsaQuery.Where(gerOperacaoBolsaQuery.IdCliente.Equal(Convert.ToInt32(btnEditCodigoClienteFiltro.Text)));
        }

        if (!String.IsNullOrEmpty(textDataInicio.Text)) {
            gerOperacaoBolsaQuery.Where(gerOperacaoBolsaQuery.Data.GreaterThanOrEqual(textDataInicio.Text));
        }

        if (!String.IsNullOrEmpty(textDataFim.Text)) {
            gerOperacaoBolsaQuery.Where(gerOperacaoBolsaQuery.Data.LessThanOrEqual(textDataFim.Text));
        }

        if (!String.IsNullOrEmpty(textCdAtivo.Text)) {
            gerOperacaoBolsaQuery.Where(gerOperacaoBolsaQuery.CdAtivoBolsa.Like("%" + textCdAtivo.Text + "%"));
        }

        if (dropTipoOperacaoFiltro.SelectedIndex > -1) {
            gerOperacaoBolsaQuery.Where(gerOperacaoBolsaQuery.TipoOperacao.Equal(Convert.ToString(dropTipoOperacaoFiltro.SelectedItem.Value)));
        }

        if (dropTipoMercado.SelectedIndex > 0)
        {
            gerOperacaoBolsaQuery.Where(gerOperacaoBolsaQuery.TipoMercado == dropTipoMercado.SelectedItem.Value);
        }

        gerOperacaoBolsaQuery.OrderBy(gerOperacaoBolsaQuery.Data.Descending);

        GerOperacaoBolsaCollection coll = new GerOperacaoBolsaCollection();
        coll.Load(gerOperacaoBolsaQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ClienteCollection coll = new ClienteCollection();
        coll.BuscaClientesComAcesso(Context.User.Identity.Name);
        
        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSTrader_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        TraderCollection coll = new TraderCollection();
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSAtivoBolsa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        AtivoBolsaCollection coll = new AtivoBolsaCollection();
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void panelEdicao_Load(object sender, EventArgs e) {
        
        base.panelEdicao_Load(sender, e);

        bool travaPainel = false;
        if (gridCadastro.IsEditing && !gridCadastro.IsNewRowEditing && Convert.ToString(Session["FormLoad"]) != "S") {
            Session["FormLoad"] = "S";

            int idOperacao = Convert.ToInt32(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "IdOperacao"));

            //Especificacao, TipoMercado, FatorCotacao
            string cdAtivoBolsa = gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "CdAtivoBolsa").ToString();
            DateTime data = Convert.ToDateTime(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "Data"));

            TextBox textFatorCotacao = gridCadastro.FindEditFormTemplateControl("textFatorCotacao") as TextBox;

            string fatorCotacao = "";
            FatorCotacaoBolsa fatorCotacaoBolsa = new FatorCotacaoBolsa();
            if (fatorCotacaoBolsa.BuscaFatorCotacaoBolsa(cdAtivoBolsa, data)) {
                fatorCotacao = fatorCotacaoBolsa.Fator.Value.ToString();
            }
            textFatorCotacao.Text = fatorCotacao;
            //
        
            //CheckBox de Exercicio de Opcoes
            ASPxButtonEdit btnEditAtivoBolsa = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa") as ASPxButtonEdit;

            CheckBox chkExercicio = gridCadastro.FindEditFormTemplateControl("chkExercicio") as CheckBox;
            byte origem = Convert.ToByte(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "Origem"));
            if (origem == (byte)OrigemOperacaoBolsa.ExercicioOpcaoCompra ||
                origem == (byte)OrigemOperacaoBolsa.ExercicioOpcaoVenda) {
                chkExercicio.Checked = true;

                string cdAtivoBolsaOpcao = gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "CdAtivoBolsaOpcao").ToString();
                btnEditAtivoBolsa.Text = cdAtivoBolsaOpcao;
            }
            else {
                btnEditAtivoBolsa.Text = cdAtivoBolsa;
            }
            
            Label labelEdicao = gridCadastro.FindEditFormTemplateControl("labelEdicao") as Label;

            int idCliente = Convert.ToInt32(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "IdCliente"));

            Cliente cliente = new Cliente();
            if (cliente.LoadByPrimaryKey(idCliente)) {
                DateTime dataDia = cliente.DataDia.Value;
                byte status = cliente.Status.Value;
                if (status == (byte)StatusCliente.Divulgado) {
                    travaPainel = true;
                    labelEdicao.Text = "Cliente está fechado. Dados não podem ser alterados.";
                }
                else if (DateTime.Compare(dataDia, data) > 0) {
                    travaPainel = true;
                    labelEdicao.Text = "Data informada anterior à data do cliente. Dados não podem ser alterados.";
                }
            }
        }

        if (travaPainel) {
            LinkButton btnOK = gridCadastro.FindEditFormTemplateControl("btnOK") as LinkButton;
            btnOK.Visible = false;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";

        if (!gridCadastro.IsEditing) //Testa deleção
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdCliente");
            List<object> keyData = gridCadastro.GetSelectedFieldValues("Data");
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idCliente = Convert.ToInt32(keyValuesId[i]);
                DateTime data = Convert.ToDateTime(keyData[i]);

                Cliente cliente = new Cliente();
                if (cliente.LoadByPrimaryKey(idCliente)) {
                    DateTime dataDia = cliente.DataDia.Value;
                    byte status = cliente.Status.Value;
                    string clienteDescricao = cliente.Apelido + " (" + cliente.IdCliente.ToString() + ")";
                    if (status == (byte)StatusCliente.Divulgado) {
                        e.Result = "Cliente " + clienteDescricao + " está fechado! Deleção cancelada.";
                        return;
                    }
                    else if (DateTime.Compare(dataDia, data) > 0) {
                        e.Result = "Lançamento com data " + data.ToString().Substring(0, 10) + " anterior à data do cliente " + clienteDescricao + "! Deleção cancelada.";
                        return;
                    }
                }
            }
        }
        else {
            ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
            ASPxComboBox dropTrader = gridCadastro.FindEditFormTemplateControl("dropTrader") as ASPxComboBox;
            ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
            ASPxComboBox dropTipoOperacao = gridCadastro.FindEditFormTemplateControl("dropTipoOperacao") as ASPxComboBox;
            ASPxButtonEdit btnEditAtivoBolsa = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa") as ASPxButtonEdit;
            ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
            ASPxSpinEdit textPU = gridCadastro.FindEditFormTemplateControl("textPU") as ASPxSpinEdit;
            ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
            CheckBox chkExercicio = gridCadastro.FindEditFormTemplateControl("chkExercicio") as CheckBox;

            #region Campos obrigatórios
            List<Control> controles = new List<Control>();
            controles.Add(btnEditCodigoCliente);
            controles.Add(dropTrader);
            controles.Add(dropTipoOperacao);
            controles.Add(btnEditAtivoBolsa);
            controles.Add(textQuantidade);
            controles.Add(textPU);
            controles.Add(textValor);

            if (base.TestaObrigatorio(controles) != "") {
                e.Result = "Campos com * são obrigatórios!";
                return;
            }
            #endregion

            int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
            DateTime data = Convert.ToDateTime(textData.Text);

            if (!Calendario.IsDiaUtil(data, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil)) {
                e.Result = "Data da operação não é dia útil.";
                return;
            }

            Cliente cliente = new Cliente();
            if (cliente.LoadByPrimaryKey(idCliente)) {
                DateTime dataDia = cliente.DataDia.Value;
                byte status = cliente.Status.Value;
                if (status == (byte)StatusCliente.Divulgado) {
                    e.Result = "Cliente está fechado! Operação não pode ser realizada.";
                    return;
                }
                else if (DateTime.Compare(dataDia, Convert.ToDateTime(textData.Text)) > 0) {
                    e.Result = "Data informada anterior à data do cliente! Operação não pode ser realizada.";
                    return;
                }
            }

            AtivoBolsa ativoBolsa = new AtivoBolsa();
            ativoBolsa.LoadByPrimaryKey(btnEditAtivoBolsa.Text);

            if (chkExercicio.Checked) {
                if (Convert.ToString(dropTipoOperacao.SelectedItem.Value) == TipoOperacaoBolsa.Deposito ||
                    Convert.ToString(dropTipoOperacao.SelectedItem.Value) == TipoOperacaoBolsa.Retirada) {
                    e.Result = "Exercícios de opção só podem ter associados tipos de operação Compra, Venda, Compra DT, Venda DT.";
                    return;
                }

                if (ativoBolsa.TipoMercado != TipoMercadoBolsa.OpcaoCompra && ativoBolsa.TipoMercado != TipoMercadoBolsa.OpcaoVenda) {
                    e.Result = "O ativo precisa ser opção de compra/venda para exercícios de opção.";
                    return;
                }
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOKAdd_Init(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        string resultado = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") {
                if (cliente.IsAtivo) {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoCliente(idCliente, HttpContext.Current.User.Identity.Name)) {
                        nome = cliente.str.Apelido;
                        DateTime dataDia = cliente.DataDia.Value;

                        resultado = this.gridCadastro.IsNewRowEditing
                                   ? nome + "|" + dataDia.ToString().Substring(0, 10) + "|" + Thread.CurrentThread.CurrentCulture.Name
                                   : nome;
                    }
                    else {
                        resultado = "no_access";
                    }
                }
                else {
                    resultado = "no_active";
                }
            }
        }
        e.Result = resultado;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string resultado = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            AtivoBolsa ativoBolsa = new AtivoBolsa();
            ativoBolsa.LoadByPrimaryKey(Convert.ToString(e.Parameter));

            if (ativoBolsa.str.CdAtivoBolsa != "") {
                ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;

                string cdAtivoBolsa = ativoBolsa.str.CdAtivoBolsa;
                string especificacao = ativoBolsa.Especificacao;
                string tipoMercado = ativoBolsa.TipoMercado;

                //Fator de cotação
                string fatorCotacao = "";
                if (textData.Text != "") {
                    FatorCotacaoBolsa fatorCotacaoBolsa = new FatorCotacaoBolsa();
                    if (fatorCotacaoBolsa.BuscaFatorCotacaoBolsa(cdAtivoBolsa, Convert.ToDateTime(textData.Text))) {
                        fatorCotacao = fatorCotacaoBolsa.Fator.Value.ToString();
                    }
                    else {
                        e.Result = "noquote";
                        return;
                    }
                }

                resultado = cdAtivoBolsa + "|" + fatorCotacao + "|" + Thread.CurrentThread.CurrentCulture.Name;
            }
        }
        e.Result = resultado;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) 
    {
        int idOperacao = (int)e.Keys[0];
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxButtonEdit btnEditAtivoBolsa = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa") as ASPxButtonEdit;
        ASPxComboBox dropTrader = gridCadastro.FindEditFormTemplateControl("dropTrader") as ASPxComboBox;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxComboBox dropTipoOperacao = gridCadastro.FindEditFormTemplateControl("dropTipoOperacao") as ASPxComboBox;
        ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
        ASPxSpinEdit textPU = gridCadastro.FindEditFormTemplateControl("textPU") as ASPxSpinEdit;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
        CheckBox chkExercicio = gridCadastro.FindEditFormTemplateControl("chkExercicio") as CheckBox;

        GerOperacaoBolsa operacaoBolsa = new GerOperacaoBolsa();
        if (operacaoBolsa.LoadByPrimaryKey(idOperacao)) {
            int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);

            operacaoBolsa.IdCliente = idCliente;
            operacaoBolsa.CdAtivoBolsa = Convert.ToString(btnEditAtivoBolsa.Text).ToUpper();
            operacaoBolsa.IdTrader = Convert.ToInt32(dropTrader.SelectedItem.Value);

            AtivoBolsa ativoBolsa = new AtivoBolsa();
            ativoBolsa.LoadByPrimaryKey(Convert.ToString(btnEditAtivoBolsa.Text));
            string tipoMercadoAtivo = ativoBolsa.TipoMercado;
            
            operacaoBolsa.TipoMercado = tipoMercadoAtivo;

            //Trata exercicio!!!! ****************
            if (chkExercicio.Checked) {
                if (Convert.ToString(dropTipoOperacao.SelectedItem.Value) == TipoOperacaoBolsa.Compra ||
                    Convert.ToString(dropTipoOperacao.SelectedItem.Value) == TipoOperacaoBolsa.CompraDaytrade ||
                    Convert.ToString(dropTipoOperacao.SelectedItem.Value) == TipoOperacaoBolsa.Venda ||
                    Convert.ToString(dropTipoOperacao.SelectedItem.Value) == TipoOperacaoBolsa.VendaDaytrade) {
                    if (tipoMercadoAtivo == TipoMercadoBolsa.OpcaoCompra) {
                        operacaoBolsa.Origem = (byte)OrigemOperacaoBolsa.ExercicioOpcaoCompra;
                    }
                    else if (tipoMercadoAtivo == TipoMercadoBolsa.OpcaoVenda) {
                        operacaoBolsa.Origem = (byte)OrigemOperacaoBolsa.ExercicioOpcaoVenda;
                    }
                }
            }
            else {
                operacaoBolsa.Origem = (byte)OrigemOperacaoBolsa.Primaria;
            }

            if (operacaoBolsa.Origem == (byte)OrigemOperacaoBolsa.ExercicioOpcaoCompra ||
                operacaoBolsa.Origem == (byte)OrigemOperacaoBolsa.ExercicioOpcaoVenda) {
                //Precisa transformar a opcao lancada para a vista e usar o ativo informado como opcao (CdAtivoBolsaOpcao)
                operacaoBolsa.CdAtivoBolsa = ativoBolsa.CdAtivoBolsaObjeto;
                operacaoBolsa.CdAtivoBolsaOpcao = Convert.ToString(btnEditAtivoBolsa.Text).ToUpper();
                operacaoBolsa.TipoMercado = TipoMercadoBolsa.MercadoVista;
            }
            //**************************************

            operacaoBolsa.TipoOperacao = Convert.ToString(dropTipoOperacao.SelectedItem.Value);
            operacaoBolsa.Pu = Convert.ToDecimal(textPU.Text);
            operacaoBolsa.PULiquido = Convert.ToDecimal(textPU.Text);
            operacaoBolsa.Valor = Convert.ToDecimal(textValor.Text);
            operacaoBolsa.Quantidade = Convert.ToDecimal(textQuantidade.Text);

            DateTime data = Convert.ToDateTime(textData.Text);
            operacaoBolsa.Data = Convert.ToDateTime(textData.Text.ToString());
            
            operacaoBolsa.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de OperacaoBolsa - Operacao: Update OperacaoBolsa: " + idOperacao + UtilitarioWeb.ToString(operacaoBolsa),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion

            //Atualiza StatusRealTime para executar***********
            Cliente cliente = new Cliente();
            cliente.SetaFlagRealTime(idCliente, (byte)StatusRealTimeCliente.Executar);
            //************************************************
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    private void SalvarNovo() 
    {
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxButtonEdit btnEditAtivoBolsa = gridCadastro.FindEditFormTemplateControl("btnEditAtivoBolsa") as ASPxButtonEdit;
        ASPxComboBox dropTrader = gridCadastro.FindEditFormTemplateControl("dropTrader") as ASPxComboBox;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxComboBox dropTipoOperacao = gridCadastro.FindEditFormTemplateControl("dropTipoOperacao") as ASPxComboBox;
        ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
        ASPxSpinEdit textPU = gridCadastro.FindEditFormTemplateControl("textPU") as ASPxSpinEdit;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
        CheckBox chkExercicio = gridCadastro.FindEditFormTemplateControl("chkExercicio") as CheckBox;

        GerOperacaoBolsa operacaoBolsa = new GerOperacaoBolsa();

        int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);

        operacaoBolsa.IdCliente = idCliente;
        operacaoBolsa.CdAtivoBolsa = Convert.ToString(btnEditAtivoBolsa.Text).ToUpper();
        operacaoBolsa.IdTrader = Convert.ToInt32(dropTrader.SelectedItem.Value);
        DateTime data = Convert.ToDateTime(textData.Text);

        AtivoBolsa ativoBolsa = new AtivoBolsa();
        ativoBolsa.LoadByPrimaryKey(Convert.ToString(btnEditAtivoBolsa.Text));
        string tipoMercadoAtivo = ativoBolsa.TipoMercado;
                
        operacaoBolsa.TipoMercado = tipoMercadoAtivo;
        operacaoBolsa.TipoOperacao = Convert.ToString(dropTipoOperacao.SelectedItem.Value);
        
        //Trata exercicio!!!! ****************
        if (chkExercicio.Checked) {
            if (Convert.ToString(dropTipoOperacao.SelectedItem.Value) == TipoOperacaoBolsa.Compra ||
                Convert.ToString(dropTipoOperacao.SelectedItem.Value) == TipoOperacaoBolsa.CompraDaytrade ||
                Convert.ToString(dropTipoOperacao.SelectedItem.Value) == TipoOperacaoBolsa.Venda ||
                Convert.ToString(dropTipoOperacao.SelectedItem.Value) == TipoOperacaoBolsa.VendaDaytrade) {
                if (tipoMercadoAtivo == TipoMercadoBolsa.OpcaoCompra) {
                    operacaoBolsa.Origem = (byte)OrigemOperacaoBolsa.ExercicioOpcaoCompra;
                }
                else if (tipoMercadoAtivo == TipoMercadoBolsa.OpcaoVenda) {
                    operacaoBolsa.Origem = (byte)OrigemOperacaoBolsa.ExercicioOpcaoVenda;
                }
            }
        }
        else {
            operacaoBolsa.Origem = (byte)OrigemOperacaoBolsa.Primaria;
        }

        if (operacaoBolsa.Origem == (byte)OrigemOperacaoBolsa.ExercicioOpcaoCompra ||
            operacaoBolsa.Origem == (byte)OrigemOperacaoBolsa.ExercicioOpcaoVenda) {
            //Precisa transformar a opcao lancada para a vista e usar o ativo informado como opcao (CdAtivoBolsaOpcao)
            operacaoBolsa.CdAtivoBolsa = ativoBolsa.CdAtivoBolsaObjeto;
            operacaoBolsa.CdAtivoBolsaOpcao = Convert.ToString(btnEditAtivoBolsa.Text).ToUpper();
            operacaoBolsa.TipoMercado = TipoMercadoBolsa.MercadoVista;
        }
        //**************************************


        operacaoBolsa.Pu = Convert.ToDecimal(textPU.Text);
        operacaoBolsa.PULiquido = Convert.ToDecimal(textPU.Text);
        operacaoBolsa.Valor = Convert.ToDecimal(textValor.Text);
        operacaoBolsa.Quantidade = Convert.ToDecimal(textQuantidade.Text);
                
        operacaoBolsa.Data = Convert.ToDateTime(textData.Text.ToString());
        operacaoBolsa.Fonte = (byte)FonteOperacaoBolsa.Manual;
        
        operacaoBolsa.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de GerOperacaoBolsa - Operacao: Insert GerOperacaoBolsa: " + operacaoBolsa.IdOperacao + UtilitarioWeb.ToString(operacaoBolsa),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
                
        //Atualiza StatusRealTime para executar***********
        Cliente cliente = new Cliente();
        cliente.SetaFlagRealTime(idCliente, (byte)StatusRealTimeCliente.Executar);
        //************************************************
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        SalvarNovo();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e) 
    {
        if (e.Parameters == "btnDelete") 
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdOperacao");
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idOperacao = Convert.ToInt32(keyValuesId[i]);

                GerOperacaoBolsa gerOperacaoBolsa = new GerOperacaoBolsa();
                if (gerOperacaoBolsa.LoadByPrimaryKey(idOperacao))
                {
                    int idCliente = gerOperacaoBolsa.IdCliente.Value;

                    //
                    GerOperacaoBolsa gerOperacaoBolsaClone = (GerOperacaoBolsa)Utilitario.Clone(gerOperacaoBolsa);
                    //

                    gerOperacaoBolsa.MarkAsDeleted();
                    gerOperacaoBolsa.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de GerOperacaoBolsa - Operacao: Delete GerOperacaoBolsa: " + idOperacao + UtilitarioWeb.ToString(gerOperacaoBolsaClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion

                    //Atualiza StatusRealTime para executar***********
                    Cliente cliente = new Cliente();
                    cliente.SetaFlagRealTime(idCliente, (byte)StatusRealTimeCliente.Executar);
                    //************************************************
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing) {
            ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
            e.Properties["cpBtnEditCodigoCliente"] = btnEditCodigoCliente.ClientID;

            TextBox textFatorCotacao = gridCadastro.FindEditFormTemplateControl("textFatorCotacao") as TextBox;
            e.Properties["cpTextFatorCotacao"] = textFatorCotacao.ClientID;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("btnEditCodigoCliente");
        base.gridCadastro_PreRender(sender, e);

        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        Label labelFiltroHeader = gridCadastro.FindStatusBarTemplateControl("labelFiltroHeader") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");

        if (btnEditCodigoClienteFiltro != null && btnEditCodigoClienteFiltro.Text != "") {
            texto.Append(" Cliente: ").Append(btnEditCodigoClienteFiltro.Text);
        }
        if (textDataInicio.Text != "") {
            if (texto.Length > 0) {
                texto.Append(" |");
            }
            texto.Append(" Data Início >= ").Append(textDataInicio.Text.Substring(0, 10));
        }
        if (textDataFim.Text != "") {
            if (texto.Length > 0) {
                texto.Append(" |");
            }
            texto.Append(" Data Fim <= ").Append(textDataFim.Text.Substring(0, 10));
        }
        if (dropTipoMercado.SelectedIndex > 0)
        {
            if (texto.Length > 0)
            {
                texto.Append(" |");
            }
            texto.Append(" Mercado = ").Append(dropTipoMercado.SelectedItem.Text);
        }
        if (textCdAtivo.Text != "") {
            if (texto.Length > 0) {
                texto.Append(" |");
            }
            texto.Append(" Ativo Like %").Append(textCdAtivo.Text).Append("%");
        }
        if (dropTipoOperacaoFiltro.SelectedIndex > -1) {
            if (texto.Length > 0) {
                texto.Append(" |");
            }
            texto.Append(" Tipo = ").Append(dropTipoOperacaoFiltro.Text);
        }

        labelFiltro.Text = texto.ToString();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e) {
        if (e.Column.FieldName == "TipoOperacao") 
        {
            if (Convert.ToByte(gridCadastro.GetRowValues(e.VisibleRowIndex, "Origem")) == OrigemOperacaoBolsa.VencimentoOpcao) {
                if (e.Value.ToString() == "DE") {
                    e.DisplayText = "Vcto Opção(+)";
                }
                else {
                    e.DisplayText = "Vcto Opção(-)";
                }
            }
            else if (Convert.ToByte(gridCadastro.GetRowValues(e.VisibleRowIndex, "Origem")) == OrigemOperacaoBolsa.ExercicioOpcaoCompra ||
                     Convert.ToByte(gridCadastro.GetRowValues(e.VisibleRowIndex, "Origem")) == OrigemOperacaoBolsa.ExercicioOpcaoVenda) {
                if (e.Value.ToString() == "DE" || e.Value.ToString() == "CD" || e.Value.ToString() == "C") {
                    e.DisplayText = "Exercício-Compra";
                }
                else {
                    e.DisplayText = "Exercício-Venda";
                }
            }
        }
    }
}