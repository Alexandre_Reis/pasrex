﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ConfigCliente.aspx.cs" Inherits="ConfigCliente" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxrp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxp" %>
<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" />

        <div class="divPanel divPanelNew">
            <table width="100%" border="0">
                <tr>
                    <td>
                        <div id="container_small">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Configuração de Menu" />
                            </div>
                            <div id="mainContentSpace">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <br />
                                        <br />
                                        <table border="0" align="center">
                                            <tr>
                                                <td>
                                                    <dxrp:ASPxRoundPanel ID="pnlMenu" runat="server" HeaderText=" Configuração Menu" HeaderStyle-Font-Size="11px" Width="459px">
                                                        <PanelCollection>
                                                            <dxp:PanelContent runat="server">
                                                                <br />
                                                                <table border="0" align="center" cellpadding="2" cellspacing="2" width="350">
                                                                    <tr>
                                                                        <td>
                                                                            <dxe:ASPxCheckBox ID="chkTodos" runat="server" Width="10px" AutoPostBack="True" ClientInstanceName="chkTodos" OnCheckedChanged="chkTodos_CheckedChanged"/>
                                                                        </td>
                                                                        <td colspan=3>Todos</td>
                                                                    </tr>
                                                                    
                                                                    <tr>             
                                                                        <td colspan=5 valign=top><hr></td>
                                                                    </tr>
                                                                    
                                                                    <tr>
                                                                        <td>
                                                                            <dxe:ASPxCheckBox ID="chkAcoesOpcoes" runat="server" Width="10px"/>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="label11" runat="server" CssClass="labelNormal" Text="Acoes/Opções" />
                                                                        </td>
                                                                        <td style="width:10px">
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxCheckBox ID="chkCalculoDespesas" runat="server" Width="10px"/>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="label9" runat="server" CssClass="labelNormal" Text="Cálculo Despesas" />
                                                                        </td>
                                                                    </tr>
                                                                    
                                                                    <tr>
                                                                        <td>
                                                                            <dxe:ASPxCheckBox ID="chkTermoAcoes" runat="server" Width="10px"/>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="label1" runat="server" CssClass="labelNormal" Text="Termo Ações" />
                                                                        </td>
                                                                        <td style="width:10px">
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxCheckBox ID="chkRebates" runat="server" Width="10px"/>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="label8" runat="server" CssClass="labelNormal" Text="Rebates" />
                                                                        </td>
                                                                    </tr>
                                                                    
                                                                    <tr>
                                                                        <td>
                                                                            <dxe:ASPxCheckBox ID="chkAluguelAcoes" runat="server" Width="10px"/>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="label2" runat="server" CssClass="labelNormal" Text="BTC/Aluguel Ações" />
                                                                        </td>
                                                                        <td style="width:10px">
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxCheckBox ID="chkEnquadramento" runat="server" Width="10px"/>                                                                            
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="label10" runat="server" CssClass="labelNormal" Text="Enquadramento" />
                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td>
                                                                            <dxe:ASPxCheckBox ID="chkBMF" runat="server" Width="10px"/>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="label7" runat="server" CssClass="labelNormal" Text="BMF" />
                                                                        </td>
                                                                        <td style="width:10px">
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxCheckBox ID="chkCotista" runat="server" Width="10px"/>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="label12" runat="server" CssClass="labelNormal" Text="Cotista" />
                                                                        </td>
                                                                    </tr>                                                                    
                                                                    <tr>
                                                                        <td >
                                                                            <dxe:ASPxCheckBox ID="chkRendaFixa" runat="server" Width="10px"/>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="label3" runat="server" CssClass="labelNormal" Text="Renda Fixa" />
                                                                        </td>
                                                                        <td style="width:10px">
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxCheckBox ID="chkContabil" runat="server" Width="10px"/>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="label13" runat="server" CssClass="labelNormal" Text="Contábil" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td >
                                                                            <dxe:ASPxCheckBox ID="chkFundos" runat="server" Width="10px"/>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="label4" runat="server" CssClass="labelNormal" Text="Fundos" />
                                                                        </td>
                                                                        <td style="width:10px">
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxCheckBox ID="chkApuracaoIR" runat="server" Width="10px"/>                                                                            
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="label14" runat="server" CssClass="labelNormal" Text="Apuração IR" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td >
                                                                            <dxe:ASPxCheckBox ID="chkSwap" runat="server" Width="10px"/>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="label5" runat="server" CssClass="labelNormal" Text="Swap" />
                                                                        </td>
                                                                        <td style="width:10px">
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxCheckBox ID="chkInformeFundos" runat="server" Width="10px"/>                                                                            
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="label15" runat="server" CssClass="labelNormal" Text="Informes Fundos" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <dxe:ASPxCheckBox ID="chkGerencial" runat="server" Width="10px"/>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="label6" runat="server" CssClass="labelNormal" Text="Gerencial" />
                                                                        </td>
                                                                        <td style="width:10px">
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxCheckBox ID="chkInformeClubes" runat="server" Width="10px"/>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="label16" runat="server" CssClass="labelNormal" Text="Informes Clubes" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <dxe:ASPxCheckBox ID="chkFrontOffice" runat="server" Width="10px"/>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="label18" runat="server" CssClass="labelNormal" Text="FrontOffice" />
                                                                        </td>
                                                                        <td style="width:10px">
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxCheckBox ID="chkCarteiraSimulada" runat="server" Width="10px"/>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="label17" runat="server" CssClass="labelNormal" Text="Carteira Simulada" />
                                                                        </td>                                                                        
                                                                    </tr>
                                                                                                                                        
                                                                </table>
                                                                <div class="linkButton linkButtonTbar" style="padding-left: 50pt">
                                                                    <div class="linkButtonWrapper">
                                                                        <asp:LinkButton ID="LinkButton1" runat="server" Font-Overline="False" CssClass="btnOK"
                                                                            OnClick="btnSalvar_Click">
                                                                            <asp:Literal ID="Literal1" runat="server" Text="Salvar" /><div>
                                                                            </div>
                                                                        </asp:LinkButton>
                                                                    </div>
                                                                </div>
                                                            </dxp:PanelContent>
                                                        </PanelCollection>
                                                        <HeaderStyle Font-Size="11px" />
                                                    </dxrp:ASPxRoundPanel>
                                                </td>
                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>