﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.RendaFixa;
using Financial.Web.Common;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Swap.Enums;
using Financial.Swap;
using Financial.Investidor;

public partial class CadastrosBasicos_EventoSwap : CadastroBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasFiltro = true;
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSEventoSwap_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) 
    {
        EventoSwapCollection coll = new EventoSwapCollection();

        ASPxDateEdit textDataInicio = popupFiltro.FindControl("textDataInicio") as ASPxDateEdit;
        ASPxDateEdit textDataFim = popupFiltro.FindControl("textDataFim") as ASPxDateEdit;

        if (textDataInicio.Text != "")
        {
            coll.Query.Where(coll.Query.DataEvento.GreaterThanOrEqual(textDataInicio.Text));
        }

        if (textDataFim.Text != "")
        {
            coll.Query.Where(coll.Query.DataEvento.LessThanOrEqual(textDataFim.Text));
        }

        coll.Query.OrderBy(coll.Query.DataEvento.Descending);
        coll.Query.Load();

        e.Collection = coll;
    }

    protected void EsDSOperacaoSwap_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        OperacaoSwapCollection coll = new OperacaoSwapCollection();
        OperacaoSwapQuery operacaoSwapQuery = new OperacaoSwapQuery("operacao");
        ClienteQuery clienteQuery = new ClienteQuery("cliente");

        operacaoSwapQuery.Select(operacaoSwapQuery,
                                 clienteQuery.Apelido.As("Cliente"));
        operacaoSwapQuery.InnerJoin(clienteQuery).On(operacaoSwapQuery.IdCliente.Equal(clienteQuery.IdCliente));

        operacaoSwapQuery.OrderBy(operacaoSwapQuery.IdOperacao.Descending);

        coll.Load(operacaoSwapQuery);

        e.Collection = coll;
    }
    #endregion

    #region Grid Operação
    protected void gridOperacao_CustomDataCallback(object sender, DevExpress.Web.ASPxGridViewCustomDataCallbackEventArgs e)
    {
        e.Result = gridOperacao.GetRowValues(Convert.ToInt32(e.Parameters), "IdOperacao");
    }

    public void gridOperacao_CustomCallBack(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        ASPxGridView gridView = sender as ASPxGridView;
        gridView.DataBind();
    }

    protected void callbackOperacao_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        ASPxTextBox hiddenIdOperacao = gridCadastro.FindEditFormTemplateControl("hiddenIdOperacao") as ASPxTextBox;
        e.Result = string.Empty;
        if (hiddenIdOperacao != null && !string.IsNullOrEmpty(hiddenIdOperacao.Text))
        {
            OperacaoSwap operacaoSwap = new OperacaoSwap();

            if (operacaoSwap.LoadByPrimaryKey(Convert.ToInt32(hiddenIdOperacao.Text)))
            {
                string descricaoOperacao = "Id.Op - " + hiddenIdOperacao.Text + " - Parte - ";

                int tipoPonta = operacaoSwap.TipoPonta.Value;
                if (tipoPonta == (int)TipoPontaSwap.AtivoBolsa)
                {
                    descricaoOperacao += operacaoSwap.CdAtivoBolsa;
                }
                else if (tipoPonta == (int)TipoPontaSwap.FundoInvestimento)
                {
                    Cliente cliente = new Cliente();
                    cliente.LoadByPrimaryKey(operacaoSwap.IdAtivoCarteira.Value);

                    descricaoOperacao += (cliente.IdCliente.Value.ToString() + " - " + cliente.Apelido);
                }
                else if (tipoPonta == (int)TipoPontaSwap.PreFixado)
                {
                    descricaoOperacao += "Pré fixado";
                }
                else
                {
                    Indice indice = new Indice();

                    indice.LoadByPrimaryKey(operacaoSwap.IdIndice.Value);
                    descricaoOperacao += (indice.Descricao);
                }

                descricaoOperacao += " / Contra Parte - ";

                int tipoPontaContraParte = operacaoSwap.TipoPontaContraParte.Value;
                if (tipoPontaContraParte == (int)TipoPontaSwap.AtivoBolsa)
                {
                    descricaoOperacao += operacaoSwap.CdAtivoBolsaContraParte;
                }
                else if (tipoPontaContraParte == (int)TipoPontaSwap.FundoInvestimento)
                {
                    Cliente cliente = new Cliente();
                    cliente.LoadByPrimaryKey(operacaoSwap.IdAtivoCarteiraContraParte.Value);

                    descricaoOperacao += (cliente.IdCliente.Value.ToString() + " - " + cliente.Apelido);
                }
                else if (tipoPonta == (int)TipoPontaSwap.PreFixado)
                {
                    descricaoOperacao += "Pré fixado";
                }
                else
                {
                    Indice indice = new Indice();

                    indice.LoadByPrimaryKey(operacaoSwap.IdIndiceContraParte.Value);
                    descricaoOperacao += (indice.Descricao);
                }

                e.Result = descricaoOperacao;

            }

        }

    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {

        List<int> lstEventoSemPontaEspecifica = new List<int>();
        lstEventoSemPontaEspecifica.Add((int)TipoEventoSwap.Reset);

        e.Result = "";
        ASPxComboBox dropEventoSwap = gridCadastro.FindEditFormTemplateControl("dropEventoSwap") as ASPxComboBox;
        ASPxComboBox dropPontaSwap = gridCadastro.FindEditFormTemplateControl("dropPontaSwap") as ASPxComboBox;
        ASPxDateEdit textDataEvento = gridCadastro.FindEditFormTemplateControl("textDataEvento") as ASPxDateEdit;
        ASPxDateEdit textDataVigencia = gridCadastro.FindEditFormTemplateControl("textDataVigencia") as ASPxDateEdit;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
        ASPxTextBox hiddenIdOperacao = gridCadastro.FindEditFormTemplateControl("hiddenIdOperacao") as ASPxTextBox;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(dropEventoSwap);
        controles.Add(textDataEvento);
        controles.Add(textDataVigencia);
        controles.Add(textValor);
        controles.Add(hiddenIdOperacao);

        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }

        if (!lstEventoSemPontaEspecifica.Contains(Convert.ToInt32(dropEventoSwap.SelectedItem.Value)))
        {
            controles.Add(dropPontaSwap);
            if (base.TestaObrigatorio(controles) != "")
            {
                e.Result = "Campos com * são obrigatórios!";
                return;
            }
        }

        #endregion

        DateTime dataEvento = Convert.ToDateTime(textDataEvento.Text);
        DateTime dataVigencia = Convert.ToDateTime(textDataVigencia.Text);

        if (gridCadastro.IsNewRowEditing) {
            int idOperacao = Convert.ToInt32(hiddenIdOperacao.Text);
            int tipoEventoSwap = Convert.ToInt32(dropEventoSwap.SelectedItem.Value);

            EventoSwap eventoSwap = new EventoSwap();
            eventoSwap.Query.Where(eventoSwap.Query.TipoEvento.Equal(tipoEventoSwap) &
                                   eventoSwap.Query.IdOperacao.Equal(idOperacao) &
                                   eventoSwap.Query.DataVigencia.Equal(dataVigencia) &
                                   eventoSwap.Query.DataEvento.Equal(dataEvento));

            if (!lstEventoSemPontaEspecifica.Contains(Convert.ToInt32(dropEventoSwap.SelectedItem.Value)))            
                eventoSwap.Query.Where(eventoSwap.Query.PontaSwap.Equal(dropPontaSwap.SelectedItem.Value));            

            if (eventoSwap.Query.Load())
            {
                e.Result = "Registro já existente";
                return;
            }
        }

        OperacaoSwap operacaoSwap = new OperacaoSwap();
        operacaoSwap.LoadByPrimaryKey(Convert.ToInt32(hiddenIdOperacao.Text));

        if (dataEvento < dataVigencia)
        {
            e.Result = "Data do Evento não pode ser menor que a vigência!";
            return;
        }

        if (dataEvento >= operacaoSwap.DataVencimento.Value)
        {
            e.Result = "Data do Evento deve ser menor que vencimento!";
            return;
        }

        if (Convert.ToInt32(dropEventoSwap.SelectedItem.Value) == (int)TipoEventoSwap.Ajuste)
        {
            if (Convert.ToInt32(dropPontaSwap.SelectedItem.Value) == (int)PontaSwap.Ambas)
            {
                e.Result = "Um evento de ajuste não pode ser parametrizado para ambas as pontas, favor registrar separadamente!";
                return;
            }

        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropEventoSwap_Load(object sender, EventArgs e)
    {
        ASPxComboBox dropEventoSwap = gridCadastro.FindEditFormTemplateControl("dropEventoSwap") as ASPxComboBox;
        if (dropEventoSwap != null)
        {
            dropEventoSwap.Items.Clear();
            dropEventoSwap.Items.Add("", 0);
            foreach (int i in Enum.GetValues(typeof(TipoEventoSwap)))
            {
                dropEventoSwap.Items.Add(TipoEventoSwapDescricao.RetornaStringValue(i), i);
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropPontaSwap_Load(object sender, EventArgs e)
    {
        ASPxComboBox dropPontaSwap = gridCadastro.FindEditFormTemplateControl("dropPontaSwap") as ASPxComboBox;
        if (dropPontaSwap != null)
        {
            dropPontaSwap.Items.Clear();
            dropPontaSwap.Items.Add("", 0);
            foreach (int i in Enum.GetValues(typeof(PontaSwap)))
            {
                dropPontaSwap.Items.Add(PontaSwapDescricao.RetornaStringValue(i), i);
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        EventoSwap eventoSwap = new EventoSwap();

        ASPxComboBox dropPontaSwap = gridCadastro.FindEditFormTemplateControl("dropPontaSwap") as ASPxComboBox;
        ASPxComboBox dropEventoSwap = gridCadastro.FindEditFormTemplateControl("dropEventoSwap") as ASPxComboBox;
        ASPxDateEdit textDataEvento = gridCadastro.FindEditFormTemplateControl("textDataEvento") as ASPxDateEdit;
        ASPxDateEdit textDataVigencia = gridCadastro.FindEditFormTemplateControl("textDataVigencia") as ASPxDateEdit;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
        ASPxTextBox hiddenIdOperacao = gridCadastro.FindEditFormTemplateControl("hiddenIdOperacao") as ASPxTextBox;

        int idEventoSwap = Convert.ToInt32(e.Keys[0]);

        if (eventoSwap.LoadByPrimaryKey(idEventoSwap))
        {
            eventoSwap.IdOperacao = Convert.ToInt32(hiddenIdOperacao.Text);

            if (dropPontaSwap.SelectedIndex != -1)
                eventoSwap.PontaSwap = Convert.ToInt16(dropPontaSwap.SelectedItem.Value);
            else
                eventoSwap.PontaSwap = null;

            eventoSwap.DataEvento = Convert.ToDateTime(textDataEvento.Text);
            eventoSwap.DataVigencia = Convert.ToDateTime(textDataVigencia.Text);
            eventoSwap.TipoEvento = Convert.ToInt16(dropEventoSwap.SelectedItem.Value);
            eventoSwap.Valor = Convert.ToDecimal(textValor.Text);
            eventoSwap.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de EventoSwap - Operacao: Update EventoSwap: " + eventoSwap.DataEvento.Value + "; " + eventoSwap.IdOperacao.Value + UtilitarioWeb.ToString(eventoSwap),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) 
    {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    private void SalvarNovo()
    {
        EventoSwap eventoSwap = new EventoSwap();

        ASPxComboBox dropPontaSwap = gridCadastro.FindEditFormTemplateControl("dropPontaSwap") as ASPxComboBox;
        ASPxComboBox dropEventoSwap = gridCadastro.FindEditFormTemplateControl("dropEventoSwap") as ASPxComboBox;
        ASPxDateEdit textDataEvento = gridCadastro.FindEditFormTemplateControl("textDataEvento") as ASPxDateEdit;
        ASPxDateEdit textDataVigencia = gridCadastro.FindEditFormTemplateControl("textDataVigencia") as ASPxDateEdit;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
        ASPxTextBox hiddenIdOperacao = gridCadastro.FindEditFormTemplateControl("hiddenIdOperacao") as ASPxTextBox;

        eventoSwap.IdOperacao = Convert.ToInt32(hiddenIdOperacao.Text);
        eventoSwap.DataEvento = Convert.ToDateTime(textDataEvento.Text);
        eventoSwap.DataVigencia = Convert.ToDateTime(textDataVigencia.Text);
        eventoSwap.TipoEvento = Convert.ToInt16(dropEventoSwap.SelectedItem.Value);
        eventoSwap.Valor = Convert.ToDecimal(textValor.Text);

        if (dropPontaSwap.SelectedIndex != -1)
            eventoSwap.PontaSwap = Convert.ToInt16(dropPontaSwap.SelectedItem.Value);
        else
            eventoSwap.PontaSwap = null;

        eventoSwap.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de EventoSwap - Operacao: Insert EventoSwap: " + eventoSwap.DataEvento.Value + "; " + eventoSwap.IdOperacao.Value + UtilitarioWeb.ToString(eventoSwap),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();

        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(EventoSwapMetadata.ColumnNames.IdEventoSwap);

            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idEvento = Convert.ToInt32(keyValuesId[i]);

                EventoSwap eventoSwap = new EventoSwap();
                if (eventoSwap.LoadByPrimaryKey(idEvento))
                {

                    EventoSwap eventoSwapClone = (EventoSwap)Utilitario.Clone(eventoSwap);
                    //

                    eventoSwap.MarkAsDeleted();
                    eventoSwap.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de EventoSwap - Operacao: Delete EventoSwap: " + eventoSwapClone.DataEvento.Value + "; " + eventoSwapClone.IdOperacao.Value + UtilitarioWeb.ToString(eventoSwapClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }    
}