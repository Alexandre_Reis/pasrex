﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using Financial.Investidor;
using Financial.ContaCorrente;
using System.Drawing;
using Financial.ContaCorrente.Enums;
using Financial.Security;
using Financial.Fundo;
using Financial.InvestidorCotista;
using Financial.InvestidorCotista.Enums;
using Financial.Investidor.Enums;
using Financial.Common.Enums;
using Financial.Bolsa;
using Financial.Bolsa.Enums;
using DevExpress.Web.Data;
using Financial.Swap;
using Financial.Web.Common;
using System.Threading;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Swap.Enums;
using EntitySpaces.Interfaces;

public partial class CadastrosBasicos_OperacaoSwap : CadastroBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasFiltro = true;
        this.HasPopupCliente = true;
        this.HasPanelFieldsLoading = true;
        base.Page_Load(sender, e);
    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSOperacaoSwap_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
        OperacaoSwapQuery operacaoSwapQuery = new OperacaoSwapQuery("O");
        ClienteQuery clienteQuery = new ClienteQuery("C");

        #region Monta campo ativo
        StringBuilder ativoField = new StringBuilder();
        ativoField.Append("< CASE WHEN O.[TipoPonta] = ").Append((int)TipoPontaSwap.AtivoBolsa);
        ativoField.Append(" THEN O.[CdAtivoBolsa]");
        ativoField.Append("       WHEN O.[TipoPonta] = ").Append((int)TipoPontaSwap.FundoInvestimento);
        ativoField.Append(" THEN CONVERT(char(20),O.[IdAtivoCarteira])");
        ativoField.Append(" END as Ativo>");
        #endregion

        #region Monta campo ativo contraparte
        StringBuilder ativoContraParteField = new StringBuilder();
        ativoContraParteField.Append("< CASE WHEN O.[TipoPontaContraParte] = ").Append((int)TipoPontaSwap.AtivoBolsa);
        ativoContraParteField.Append(" THEN O.[CdAtivoBolsaContraParte]");
        ativoContraParteField.Append("       WHEN O.[TipoPontaContraParte] = ").Append((int)TipoPontaSwap.FundoInvestimento);
        ativoContraParteField.Append(" THEN CONVERT(char(20),O.[IdAtivoCarteiraContraParte])");
        ativoContraParteField.Append(" END as AtivoContraParte>");
        #endregion

        operacaoSwapQuery.Select(operacaoSwapQuery, ativoField.ToString(), ativoContraParteField.ToString(),clienteQuery.Apelido.As("Apelido"));
        operacaoSwapQuery.InnerJoin(clienteQuery).On(operacaoSwapQuery.IdCliente == clienteQuery.IdCliente);
        operacaoSwapQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        operacaoSwapQuery.Where(permissaoClienteQuery.IdUsuario.Equal(idUsuario));
        
        if (!String.IsNullOrEmpty(btnEditCodigoClienteFiltro.Text)) {
            operacaoSwapQuery.Where(operacaoSwapQuery.IdCliente.Equal(Convert.ToInt32(btnEditCodigoClienteFiltro.Text)));
        }

        if (!String.IsNullOrEmpty(textDataInicio.Text)) {
            operacaoSwapQuery.Where(operacaoSwapQuery.DataRegistro.GreaterThanOrEqual(textDataInicio.Text));
        }

        if (!String.IsNullOrEmpty(textDataFim.Text)) {
            operacaoSwapQuery.Where(operacaoSwapQuery.DataRegistro.LessThanOrEqual(textDataFim.Text));
        }

        operacaoSwapQuery.OrderBy(operacaoSwapQuery.DataEmissao.Descending);

        OperacaoSwapCollection coll = new OperacaoSwapCollection();
        coll.Load(operacaoSwapQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ClienteCollection coll = new ClienteCollection();
        coll.BuscaClientesComAcesso(Context.User.Identity.Name);
        
        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCategoriaMovimentacao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CategoriaMovimentacaoCollection coll = new CategoriaMovimentacaoCollection();
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSIndice_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        IndiceCollection coll = new IndiceCollection();
        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSAgente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        AgenteMercadoCollection coll = new AgenteMercadoCollection();
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSEstrategia_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        EstrategiaCollection coll = new EstrategiaCollection();
        coll.Query.OrderBy(coll.Query.IdEstrategia.Ascending);
        coll.Query.Load();
       
        e.Collection = coll;
    }

    protected void EsDSAtivo_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ASPxComboBox dropTipoPonta = gridCadastro.FindEditFormTemplateControl("dropTipoPonta") as ASPxComboBox;
        ASPxDateEdit textDataEmissao = gridCadastro.FindEditFormTemplateControl("textDataEmissao") as ASPxDateEdit;
        AtivoBolsaCollection ativoBolsaCollection = new AtivoBolsaCollection();
        CarteiraCollection carteiraCollection = new CarteiraCollection();

        if (dropTipoPonta != null && dropTipoPonta.SelectedIndex != -1 && textDataEmissao != null && !string.IsNullOrEmpty(textDataEmissao.Text))
        {
            int tipoPonta = Convert.ToInt32(dropTipoPonta.SelectedItem.Value);

            if (tipoPonta == (int)TipoPontaSwap.AtivoBolsa)
            {
                ativoBolsaCollection.Query.Select(ativoBolsaCollection.Query.CdAtivoBolsa.As("IdAtivo"),
                                                  ativoBolsaCollection.Query.Descricao);
                ativoBolsaCollection.Query.Where(ativoBolsaCollection.Query.DataVencimento.IsNull() | ativoBolsaCollection.Query.DataVencimento.GreaterThan(Convert.ToDateTime(textDataEmissao.Text)));

                ativoBolsaCollection.Query.Load();

                e.Collection = ativoBolsaCollection;
                return;
            }

            if (tipoPonta == (int)TipoPontaSwap.FundoInvestimento)
            {

                carteiraCollection.Query.Select(carteiraCollection.Query.IdCarteira.As("IdAtivo"),
                                                carteiraCollection.Query.Apelido.As("Descricao"));

                carteiraCollection.Query.Load();

                e.Collection = carteiraCollection;
                return;
            }
        }

        e.Collection = carteiraCollection;
    }

    protected void EsDSAtivoContraParte_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ASPxComboBox dropTipoPonta2 = gridCadastro.FindEditFormTemplateControl("dropTipoPonta2") as ASPxComboBox;
        ASPxDateEdit textDataEmissao = gridCadastro.FindEditFormTemplateControl("textDataEmissao") as ASPxDateEdit;
        AtivoBolsaCollection ativoBolsaCollection = new AtivoBolsaCollection();
        CarteiraCollection carteiraCollection = new CarteiraCollection();

        if (dropTipoPonta2 != null && dropTipoPonta2.SelectedIndex != -1 && textDataEmissao != null && !string.IsNullOrEmpty(textDataEmissao.Text))
        {
            int tipoPonta = Convert.ToInt32(dropTipoPonta2.SelectedItem.Value);

            if (tipoPonta == (int)TipoPontaSwap.AtivoBolsa)
            {

                ativoBolsaCollection.Query.Select(ativoBolsaCollection.Query.CdAtivoBolsa.As("IdAtivo"),
                                                  ativoBolsaCollection.Query.Descricao);
                ativoBolsaCollection.Query.Where(ativoBolsaCollection.Query.DataVencimento.IsNull() | ativoBolsaCollection.Query.DataVencimento.GreaterThan(Convert.ToDateTime(textDataEmissao.Text)));

                ativoBolsaCollection.Query.Load();

                e.Collection = ativoBolsaCollection;
                return;
            }

            if (tipoPonta == (int)TipoPontaSwap.FundoInvestimento)
            {

                carteiraCollection.Query.Select(carteiraCollection.Query.IdCarteira.As("IdAtivo"),
                                                carteiraCollection.Query.Apelido.As("Descricao"));

                carteiraCollection.Query.Load();

                e.Collection = carteiraCollection;
                return;
            }
        }

        e.Collection = carteiraCollection;
    }
    #endregion

    new protected void panelEdicao_Load(object sender, EventArgs e) {
        base.panelEdicao_Load(sender, e);

        bool travaPainel = false;
        if (gridCadastro.IsEditing && !gridCadastro.IsNewRowEditing && Convert.ToString(Session["FormLoad"]) != "S") {
            Session["FormLoad"] = "S";
            Label labelEdicao = gridCadastro.FindEditFormTemplateControl("labelEdicao") as Label;

            int idCliente = Convert.ToInt32(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "IdCliente"));
            DateTime data = Convert.ToDateTime(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "DataRegistro"));

            Cliente cliente = new Cliente();
            if (cliente.LoadByPrimaryKey(idCliente)) {
                DateTime dataDia = cliente.DataDia.Value;
                byte status = cliente.Status.Value;
                if (status == (byte)StatusCliente.Divulgado) {
                    travaPainel = true;
                    labelEdicao.Text = "Cliente está fechado. Dados não podem ser alterados.";
                }
                else if (DateTime.Compare(dataDia, data) > 0) {
                    travaPainel = true;
                    labelEdicao.Text = "Data informada anterior à data do cliente. Dados não podem ser alterados.";
                }
            }
        }

        if (travaPainel) {
            LinkButton btnOK = gridCadastro.FindEditFormTemplateControl("btnOK") as LinkButton;
            btnOK.Visible = false;
        }
    }

    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";

        if (!gridCadastro.IsEditing) {
            #region Testa deleção
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdCliente");
            List<object> keyData = gridCadastro.GetSelectedFieldValues("DataRegistro");
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idCliente = Convert.ToInt32(keyValuesId[i]);
                DateTime data = Convert.ToDateTime(keyData[i]);

                Cliente cliente = new Cliente();
                if (cliente.LoadByPrimaryKey(idCliente)) {
                    DateTime dataDia = cliente.DataDia.Value;
                    byte status = cliente.Status.Value;
                    string clienteDescricao = cliente.Apelido + " (" + cliente.IdCliente.ToString() + ")";
                    if (status == (byte)StatusCliente.Divulgado) {
                        e.Result = "Cliente " + clienteDescricao + " está fechado! Deleção cancelada.";
                        return;
                    }
                    else if (DateTime.Compare(dataDia, data) > 0) {
                        e.Result = "Lançamento com data " + data.ToString().Substring(0, 10) + " anterior à data do cliente " + clienteDescricao + "! Deleção cancelada.";
                        return;
                    }
                }
            } 
            #endregion
        }
        else {
            #region Campos
            ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
            ASPxComboBox dropTipoRegistro = gridCadastro.FindEditFormTemplateControl("dropTipoRegistro") as ASPxComboBox;
            TextBox textNumeroContrato = gridCadastro.FindEditFormTemplateControl("textNumeroContrato") as TextBox;
			TextBox textCodigoIsin = gridCadastro.FindEditFormTemplateControl("textCodigoIsin") as TextBox;
			TextBox textCpfcnpjContraParte = gridCadastro.FindEditFormTemplateControl("textCpfcnpjContraParte") as TextBox;
            ASPxDateEdit textDataEmissao = gridCadastro.FindEditFormTemplateControl("textDataEmissao") as ASPxDateEdit;
            ASPxDateEdit textDataRegistro = gridCadastro.FindEditFormTemplateControl("textDataRegistro") as ASPxDateEdit;
            ASPxDateEdit textDataVencimento = gridCadastro.FindEditFormTemplateControl("textDataVencimento") as ASPxDateEdit;
            ASPxSpinEdit textValorBase = gridCadastro.FindEditFormTemplateControl("textValorBase") as ASPxSpinEdit;
            //
            ASPxComboBox dropTipoPonta = gridCadastro.FindEditFormTemplateControl("dropTipoPonta") as ASPxComboBox;
            ASPxComboBox dropTipoApropriacao = gridCadastro.FindEditFormTemplateControl("dropTipoApropriacao") as ASPxComboBox;
            ASPxComboBox dropContagemDias = gridCadastro.FindEditFormTemplateControl("dropContagemDias") as ASPxComboBox;
            ASPxComboBox dropBaseAno = gridCadastro.FindEditFormTemplateControl("dropBaseAno") as ASPxComboBox;
            ASPxComboBox dropIndice = gridCadastro.FindEditFormTemplateControl("dropIndice") as ASPxComboBox;
            ASPxButtonEdit btnEditAtivo = gridCadastro.FindEditFormTemplateControl("btnEditAtivo") as ASPxButtonEdit;
            //
            ASPxComboBox dropTipoPonta2 = gridCadastro.FindEditFormTemplateControl("dropTipoPonta2") as ASPxComboBox;
            ASPxComboBox dropTipoApropriacao2 = gridCadastro.FindEditFormTemplateControl("dropTipoApropriacao2") as ASPxComboBox;
            ASPxComboBox dropContagemDias2 = gridCadastro.FindEditFormTemplateControl("dropContagemDias2") as ASPxComboBox;
            ASPxComboBox dropBaseAno2 = gridCadastro.FindEditFormTemplateControl("dropBaseAno2") as ASPxComboBox;
            ASPxComboBox dropIndice2 = gridCadastro.FindEditFormTemplateControl("dropIndice2") as ASPxComboBox;
            ASPxSpinEdit textTaxaJuros = gridCadastro.FindEditFormTemplateControl("textTaxaJuros") as ASPxSpinEdit;
            ASPxSpinEdit textTaxaJuros2 = gridCadastro.FindEditFormTemplateControl("textTaxaJuros2") as ASPxSpinEdit;
            ASPxButtonEdit btnEditAtivoContraParte = gridCadastro.FindEditFormTemplateControl("btnEditAtivoContraParte") as ASPxButtonEdit;
            //
            ASPxSpinEdit textPercentual = gridCadastro.FindEditFormTemplateControl("textPercentual") as ASPxSpinEdit;
            ASPxSpinEdit textPercentual2 = gridCadastro.FindEditFormTemplateControl("textPercentual2") as ASPxSpinEdit;
            //
            ASPxSpinEdit textValorIndicePartida2 = gridCadastro.FindEditFormTemplateControl("textValorIndicePartida2") as ASPxSpinEdit;
            ASPxSpinEdit textValorIndicePartida = gridCadastro.FindEditFormTemplateControl("textValorIndicePartida") as ASPxSpinEdit;

            ASPxComboBox dropEstrategia = gridCadastro.FindEditFormTemplateControl("dropEstrategia") as ASPxComboBox;

            //Reset 
            ASPxComboBox dropRealizaReset = gridCadastro.FindEditFormTemplateControl("dropRealizaReset") as ASPxComboBox;
            ASPxDateEdit textDataInicioReset = gridCadastro.FindEditFormTemplateControl("textDataInicioReset") as ASPxDateEdit;
            ASPxSpinEdit textQtdePeriodicidade = gridCadastro.FindEditFormTemplateControl("textQtdePeriodicidade") as ASPxSpinEdit;
            ASPxComboBox dropPeriodicidadeReset = gridCadastro.FindEditFormTemplateControl("dropPeriodicidadeReset") as ASPxComboBox;
            #endregion
            
            #region Campos obrigatórios
            List<Control> controles = new List<Control>();
            controles.Add(btnEditCodigoCliente);
            controles.Add(dropTipoRegistro);
            controles.Add(textNumeroContrato);
            controles.Add(textDataEmissao);
            controles.Add(textDataVencimento);
            controles.Add(textValorBase);
            controles.Add(dropTipoPonta);            
            controles.Add(dropEstrategia);
            controles.Add(dropTipoPonta2);
            controles.Add(dropRealizaReset);

            if (base.TestaObrigatorio(controles) != "") {
                e.Result = "Campos com * são obrigatórios!";
                return;
            }

            byte tipoPonta =Convert.ToByte(dropTipoPonta.SelectedItem.Value);            
            if (tipoPonta == (byte)TipoPontaSwap.AtivoBolsa || tipoPonta == (byte)TipoPontaSwap.FundoInvestimento)
            {
                controles.Add(btnEditAtivo);
            }
            else
            {
                controles.Add(dropTipoApropriacao);
                controles.Add(dropContagemDias);
                controles.Add(dropBaseAno);
                controles.Add(textTaxaJuros);
            }
            
            byte tipoPonta2 = Convert.ToByte(dropTipoPonta2.SelectedItem.Value);
            if (tipoPonta2 == (byte)TipoPontaSwap.AtivoBolsa || tipoPonta2 == (byte)TipoPontaSwap.FundoInvestimento)
            {
                controles.Add(btnEditAtivoContraParte);
            }
            else
            {
                controles.Add(dropTipoApropriacao2);
                controles.Add(dropContagemDias2);
                controles.Add(dropBaseAno2);
                controles.Add(textTaxaJuros2);
            }

            if (dropRealizaReset.SelectedItem.Value == "S")
            {
                controles.Add(textDataInicioReset);
                controles.Add(textQtdePeriodicidade);
                controles.Add(dropPeriodicidadeReset);
                
            }

            if (base.TestaObrigatorio(controles) != "")
            {
                e.Result = "Campos com * são obrigatórios!";
                return;
            }
            #endregion

            #region Restrições de Datas
            int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
            DateTime dataEmissao = Convert.ToDateTime(textDataEmissao.Text);
            DateTime dataVencimento = Convert.ToDateTime(textDataVencimento.Text);
            DateTime dataRegistro = Convert.ToDateTime(textDataRegistro.Text);

            if (!Calendario.IsDiaUtil(dataVencimento, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil)) {
                e.Result = "Data de vencimento não é dia útil.";
                return;
            }

            if (dataEmissao > dataRegistro) {
                e.Result = "Data de registro deve ser superior ou igual à data de emissão.";
                return;
            }

            if (dataRegistro >= dataVencimento) {
                e.Result = "Data de vencimento deve ser superior à data de registro.";
                return;
            }

            if (dataEmissao >= dataVencimento) {
                e.Result = "Data de vencimento deve ser superior à data de emissão.";
                return;
            } 
            #endregion

            #region TipoPonta

            #region PréFixado
            if (Convert.ToByte(dropTipoPonta.SelectedItem.Value) == (byte)TipoPontaSwap.PreFixado) {

                if (String.IsNullOrEmpty(textTaxaJuros.Text) || Convert.ToDecimal(textTaxaJuros.Text) == 0.0M) {
                    e.Result = "Taxa de Juros do Índice da ponta Ativo deve ser informado e não pode ser zero.";
                    return;
                }
            }

            if (Convert.ToByte(dropTipoPonta2.SelectedItem.Value) == (byte)TipoPontaSwap.PreFixado) {

                if (String.IsNullOrEmpty(textTaxaJuros2.Text) || Convert.ToDecimal(textTaxaJuros2.Text) == 0.0M) {
                    e.Result = "Taxa de Juros do Índice da ponta Passivo deve ser informado e não pode ser zero.";
                    return;
                }
            }
            #endregion

            #region Indexado
            if ( Convert.ToByte(dropTipoPonta.SelectedItem.Value) == (byte)TipoPontaSwap.Indexado ||
                 Convert.ToByte(dropTipoPonta.SelectedItem.Value) == (byte)TipoPontaSwap.IndexadoD0) {
                
                if ( String.IsNullOrEmpty(textPercentual.Text) || Convert.ToDecimal(textPercentual.Text) == 0.0M ) {
                    e.Result = "Percentual do Índice da ponta Ativo deve ser informado e não pode ser zero.";
                    return;                    
                }

                if (dropIndice.SelectedIndex == -1) {
                    e.Result = "Escolha um Índice na ponta Ativo.";
                    return;
                }
            }

            if (Convert.ToByte(dropTipoPonta2.SelectedItem.Value) == (byte)TipoPontaSwap.Indexado ||
                Convert.ToByte(dropTipoPonta2.SelectedItem.Value) == (byte)TipoPontaSwap.IndexadoD0) {

                if (String.IsNullOrEmpty(textPercentual2.Text) || Convert.ToDecimal(textPercentual2.Text) == 0.0M) {
                    e.Result = "Percentual do Índice da ponta Passivo deve ser informado e não pode ser zero.";
                    return;
                }

                if (dropIndice2.SelectedIndex == -1) {
                    e.Result = "Escolha um Índice na ponta Passivo.";
                    return;
                }
            }
            #endregion

            #endregion

            #region Cliente Fechado e Data
            Cliente cliente = new Cliente();
            if (cliente.LoadByPrimaryKey(idCliente)) {
                DateTime dataDia = cliente.DataDia.Value;
                byte status = cliente.Status.Value;
                if (status == (byte)StatusCliente.Divulgado) {
                    e.Result = "Cliente está fechado! Operação não pode ser realizada.";
                    return;
                }
                else if (dataDia > dataRegistro) {
                    e.Result = "Data informada anterior à data do cliente! Operação não pode ser realizada.";
                    return;
                }
            } 
            #endregion

            if (dropRealizaReset.SelectedItem.Value == "S")
            {
                DateTime dataInicioReset = Convert.ToDateTime(textDataInicioReset.Text);
                if (dataEmissao > dataInicioReset)
                {
                    e.Result = "Data de Inicio do Reset deve ser maior ou igual que a data de Emissão.";
                    return;
                }
            }
        }
    }

    protected void callbackAtivo_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string descricao = string.Empty;

        ASPxComboBox dropTipoPonta = gridCadastro.FindEditFormTemplateControl("dropTipoPonta") as ASPxComboBox;
        ASPxTextBox hiddenIdAtivo = gridCadastro.FindEditFormTemplateControl("hiddenIdAtivo") as ASPxTextBox;

        if (dropTipoPonta != null)
        {
            int tipoPonta = Convert.ToInt32(dropTipoPonta.SelectedItem.Value);
            if (tipoPonta == (int)TipoPontaSwap.AtivoBolsa)
            {
                AtivoBolsa ativoBolsa = new AtivoBolsa();
                ativoBolsa.LoadByPrimaryKey(hiddenIdAtivo.Text);

                descricao = ativoBolsa.Descricao;
            }

            if (tipoPonta == (int)TipoPontaSwap.FundoInvestimento)
            {
                Carteira carteira = new Carteira();
                carteira.LoadByPrimaryKey(Convert.ToInt32(hiddenIdAtivo.Text));
                descricao = carteira.IdCarteira.Value + " - " + carteira.Apelido;
            }
        }

        e.Result = descricao;
    }

    protected void callbackAtivoContraParte_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string descricao = string.Empty;

        ASPxComboBox dropTipoPonta2 = gridCadastro.FindEditFormTemplateControl("dropTipoPonta2") as ASPxComboBox;
        ASPxTextBox hiddenIdAtivoContraParte = gridCadastro.FindEditFormTemplateControl("hiddenIdAtivoContraParte") as ASPxTextBox;

        if (dropTipoPonta2 != null)
        {
            int tipoPonta = Convert.ToInt32(dropTipoPonta2.SelectedItem.Value);
            if (tipoPonta == (int)TipoPontaSwap.AtivoBolsa)
            {
                AtivoBolsa ativoBolsa = new AtivoBolsa();
                ativoBolsa.LoadByPrimaryKey(hiddenIdAtivoContraParte.Text);

                descricao = ativoBolsa.Descricao;
            }

            if (tipoPonta == (int)TipoPontaSwap.FundoInvestimento)
            {
                Carteira carteira = new Carteira();
                carteira.LoadByPrimaryKey(Convert.ToInt32(hiddenIdAtivoContraParte.Text));
                descricao = carteira.IdCarteira.Value + " - " + carteira.Apelido;
            }
        }

        e.Result = descricao;
    }

    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        string resultado = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") {
                if (cliente.Status.Value != (byte)StatusCliente.Divulgado) {

                    if (cliente.IsAtivo) {
                        PermissaoCliente permissaoCliente = new PermissaoCliente();
                        if (permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name)) {
                            nome = cliente.str.Apelido;
                            DateTime dataDia = cliente.DataDia.Value;

                            resultado = this.gridCadastro.IsNewRowEditing
                                      ? nome + "|" + dataDia.ToString().Substring(0, 10) + "|" + Thread.CurrentThread.CurrentCulture.Name
                                      : nome;
                        }
                        else {
                            resultado = "no_access";
                        }
                    }
                    else {
                        resultado = "no_active";
                    }
                }
                else {
                    resultado = cliente.str.Apelido + "|status_closed";
                }
            }
        }
        e.Result = resultado;
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        int idOperacao = (int)e.Keys[0];
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxComboBox dropTipoRegistro = gridCadastro.FindEditFormTemplateControl("dropTipoRegistro") as ASPxComboBox;
        ASPxComboBox dropAgente = gridCadastro.FindEditFormTemplateControl("dropAgente") as ASPxComboBox;
        TextBox textNumeroContrato = gridCadastro.FindEditFormTemplateControl("textNumeroContrato") as TextBox;
		TextBox textCodigoIsin = gridCadastro.FindEditFormTemplateControl("textCodigoIsin") as TextBox;
		TextBox textCpfcnpjContraParte = gridCadastro.FindEditFormTemplateControl("textCpfcnpjContraParte") as TextBox;

        ASPxComboBox dropComGarantia = gridCadastro.FindEditFormTemplateControl("dropComGarantia") as ASPxComboBox; 
        ASPxDateEdit textDataRegistro = gridCadastro.FindEditFormTemplateControl("textDataRegistro") as ASPxDateEdit;
        ASPxDateEdit textDataEmissao = gridCadastro.FindEditFormTemplateControl("textDataEmissao") as ASPxDateEdit;
        ASPxDateEdit textDataVencimento = gridCadastro.FindEditFormTemplateControl("textDataVencimento") as ASPxDateEdit;
        ASPxSpinEdit textValorBase = gridCadastro.FindEditFormTemplateControl("textValorBase") as ASPxSpinEdit;
        ASPxComboBox dropEstrategia = gridCadastro.FindEditFormTemplateControl("dropEstrategia") as ASPxComboBox;
        ASPxComboBox dropCategoriaMovimentacao = gridCadastro.FindEditFormTemplateControl("dropCategoriaMovimentacao") as ASPxComboBox;
        //Ponta 1
        ASPxComboBox dropTipoPonta = gridCadastro.FindEditFormTemplateControl("dropTipoPonta") as ASPxComboBox;
        ASPxComboBox dropTipoApropriacao = gridCadastro.FindEditFormTemplateControl("dropTipoApropriacao") as ASPxComboBox;
        ASPxComboBox dropContagemDias = gridCadastro.FindEditFormTemplateControl("dropContagemDias") as ASPxComboBox;
        ASPxComboBox dropBaseAno = gridCadastro.FindEditFormTemplateControl("dropBaseAno") as ASPxComboBox;
        ASPxSpinEdit textPercentual = gridCadastro.FindEditFormTemplateControl("textPercentual") as ASPxSpinEdit;
        ASPxComboBox dropIndice = gridCadastro.FindEditFormTemplateControl("dropIndice") as ASPxComboBox;
        ASPxSpinEdit textTaxaJuros = gridCadastro.FindEditFormTemplateControl("textTaxaJuros") as ASPxSpinEdit;
        ASPxSpinEdit textValorIndicePartida = gridCadastro.FindEditFormTemplateControl("textValorIndicePartida") as ASPxSpinEdit;
        ASPxTextBox hiddenIdAtivo = gridCadastro.FindEditFormTemplateControl("hiddenIdAtivo") as ASPxTextBox;
        //Ponta 2
        ASPxComboBox dropTipoPonta2 = gridCadastro.FindEditFormTemplateControl("dropTipoPonta2") as ASPxComboBox;
        ASPxComboBox dropTipoApropriacao2 = gridCadastro.FindEditFormTemplateControl("dropTipoApropriacao2") as ASPxComboBox;
        ASPxComboBox dropContagemDias2 = gridCadastro.FindEditFormTemplateControl("dropContagemDias2") as ASPxComboBox;
        ASPxComboBox dropBaseAno2 = gridCadastro.FindEditFormTemplateControl("dropBaseAno2") as ASPxComboBox;
        ASPxSpinEdit textPercentual2 = gridCadastro.FindEditFormTemplateControl("textPercentual2") as ASPxSpinEdit;
        ASPxComboBox dropIndice2 = gridCadastro.FindEditFormTemplateControl("dropIndice2") as ASPxComboBox;
        ASPxSpinEdit textTaxaJuros2 = gridCadastro.FindEditFormTemplateControl("textTaxaJuros2") as ASPxSpinEdit;
        ASPxSpinEdit textValorIndicePartida2 = gridCadastro.FindEditFormTemplateControl("textValorIndicePartida2") as ASPxSpinEdit;
        ASPxTextBox hiddenIdAtivoContraParte = gridCadastro.FindEditFormTemplateControl("hiddenIdAtivoContraParte") as ASPxTextBox;

        //Reset 
        ASPxComboBox dropRealizaReset = gridCadastro.FindEditFormTemplateControl("dropRealizaReset") as ASPxComboBox;
        ASPxDateEdit textDataInicioReset = gridCadastro.FindEditFormTemplateControl("textDataInicioReset") as ASPxDateEdit;
        ASPxSpinEdit textQtdePeriodicidade = gridCadastro.FindEditFormTemplateControl("textQtdePeriodicidade") as ASPxSpinEdit;
        ASPxComboBox dropPeriodicidadeReset = gridCadastro.FindEditFormTemplateControl("dropPeriodicidadeReset") as ASPxComboBox;

        OperacaoSwap operacaoSwap = new OperacaoSwap();
        if (operacaoSwap.LoadByPrimaryKey(idOperacao)) {
            int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);

            operacaoSwap.IdCliente = idCliente;
            operacaoSwap.TipoRegistro = Convert.ToByte(dropTipoRegistro.SelectedItem.Value);

            if (dropAgente.SelectedIndex != -1) {
                operacaoSwap.IdAgente = Convert.ToInt32(dropAgente.SelectedItem.Value);
            }

            if (dropCategoriaMovimentacao.SelectedIndex != -1)
            {
                operacaoSwap.IdCategoriaMovimentacao = Convert.ToInt32(dropCategoriaMovimentacao.SelectedItem.Value);
            }
            else
            {
                operacaoSwap.IdCategoriaMovimentacao = null;
            }

            operacaoSwap.NumeroContrato = textNumeroContrato.Text.ToString();
			operacaoSwap.CodigoIsin = textCodigoIsin.Text.ToString();
			operacaoSwap.CpfcnpjContraParte = textCpfcnpjContraParte.Text.ToString();

            operacaoSwap.ComGarantia = Convert.ToString(dropComGarantia.SelectedItem.Value);
            operacaoSwap.DataEmissao = Convert.ToDateTime(textDataEmissao.Text);
            operacaoSwap.DataRegistro = Convert.ToDateTime(textDataRegistro.Text);
            operacaoSwap.DataVencimento = Convert.ToDateTime(textDataVencimento.Text);
            
            operacaoSwap.ValorBase = Convert.ToDecimal(textValorBase.Text);           
            operacaoSwap.IdEstrategia = Convert.ToInt32(dropEstrategia.SelectedItem.Value);

            #region Ponta 1
            operacaoSwap.TipoPonta = Convert.ToByte(dropTipoPonta.SelectedItem.Value);

            operacaoSwap.Percentual = null;
            operacaoSwap.IdIndice = null;
            operacaoSwap.TipoApropriacao = null;
            operacaoSwap.ContagemDias = null;
            operacaoSwap.BaseAno = null;
            operacaoSwap.ValorIndicePartida = null;
            operacaoSwap.CdAtivoBolsa = null;
            operacaoSwap.IdAtivoCarteira = null;
            operacaoSwap.TaxaJuros = null;
            if (operacaoSwap.TipoPonta.Value == (byte)TipoPontaSwap.FundoInvestimento)
            {
                operacaoSwap.IdAtivoCarteira = Convert.ToInt32(hiddenIdAtivo.Text);
            }
            else if (operacaoSwap.TipoPonta.Value == (byte)TipoPontaSwap.AtivoBolsa)
            {
                operacaoSwap.CdAtivoBolsa = hiddenIdAtivo.Text.Trim();
            }
            else
            {
                if (Convert.ToByte(dropTipoPonta.SelectedItem.Value) == (byte)TipoPontaSwap.PreFixado)
                {
                    operacaoSwap.Percentual = 0;
                    operacaoSwap.IdIndice = null;
                }
                else
                {
                    #region Percentual
                    if (textPercentual.Text != "")
                    {
                        operacaoSwap.Percentual = Convert.ToDecimal(textPercentual.Text);
                    }
                    else
                    {
                        operacaoSwap.Percentual = 0;
                    }
                    #endregion

                    if (dropIndice.SelectedIndex != -1)
                    {
                        operacaoSwap.IdIndice = Convert.ToInt16(dropIndice.SelectedItem.Value);
                    }
                }

                operacaoSwap.TipoApropriacao = Convert.ToByte(dropTipoApropriacao.SelectedItem.Value);
                operacaoSwap.ContagemDias = Convert.ToByte(dropContagemDias.SelectedItem.Value);
                operacaoSwap.BaseAno = Convert.ToInt16(dropBaseAno.SelectedItem.Value);

                #region Taxa Juros
                if (textTaxaJuros.Text != "")
                {
                    operacaoSwap.TaxaJuros = Convert.ToDecimal(textTaxaJuros.Text);
                }
                else
                {
                    operacaoSwap.TaxaJuros = 0;
                }
                #endregion

                #region ValorIndicePartida
                if (!String.IsNullOrEmpty(textValorIndicePartida.Text))
                {
                    operacaoSwap.ValorIndicePartida = Convert.ToDecimal(textValorIndicePartida.Text);
                }
                else
                {
                    operacaoSwap.ValorIndicePartida = null;
                }
                #endregion

            }
            #endregion

            #region Ponta Contra Parte
            operacaoSwap.TipoPontaContraParte = Convert.ToByte(dropTipoPonta2.SelectedItem.Value);
            
            operacaoSwap.PercentualContraParte = null;
            operacaoSwap.IdIndiceContraParte = null;
            operacaoSwap.TipoApropriacaoContraParte = null;
            operacaoSwap.ContagemDiasContraParte = null;
            operacaoSwap.BaseAnoContraParte = null;
            operacaoSwap.ValorIndicePartidaContraParte = null;
            operacaoSwap.CdAtivoBolsaContraParte = null;
            operacaoSwap.IdAtivoCarteiraContraParte = null;
            operacaoSwap.TaxaJurosContraParte = null;
            if (operacaoSwap.TipoPontaContraParte.Value == (byte)TipoPontaSwap.FundoInvestimento)
            {
                operacaoSwap.IdAtivoCarteiraContraParte = Convert.ToInt32(hiddenIdAtivoContraParte.Text);
            }
            else if (operacaoSwap.TipoPontaContraParte.Value == (byte)TipoPontaSwap.AtivoBolsa)
            {
                operacaoSwap.CdAtivoBolsaContraParte = hiddenIdAtivoContraParte.Text.Trim();
            }
            else
            {
                if (Convert.ToByte(dropTipoPonta2.SelectedItem.Value) == (byte)TipoPontaSwap.PreFixado)
                {
                    operacaoSwap.PercentualContraParte = 0;
                    operacaoSwap.IdIndiceContraParte = null;
                }
                else
                {
                    #region Percentual ContraParte
                    if (textPercentual2.Text != "")
                    {
                        operacaoSwap.PercentualContraParte = Convert.ToDecimal(textPercentual2.Text);
                    }
                    else
                    {
                        operacaoSwap.PercentualContraParte = 0;
                    }
                    #endregion

                    if (dropIndice2.SelectedIndex != -1)
                    {
                        operacaoSwap.IdIndiceContraParte = Convert.ToInt16(dropIndice2.SelectedItem.Value);
                    }
                }

                operacaoSwap.TipoApropriacaoContraParte = Convert.ToByte(dropTipoApropriacao2.SelectedItem.Value);
                operacaoSwap.ContagemDiasContraParte = Convert.ToByte(dropContagemDias2.SelectedItem.Value);
                operacaoSwap.BaseAnoContraParte = Convert.ToInt16(dropBaseAno2.SelectedItem.Value);

                #region Juros ContraParte
                if (textTaxaJuros2.Text != "")
                {
                    operacaoSwap.TaxaJurosContraParte = Convert.ToDecimal(textTaxaJuros2.Text);
                }
                else
                {
                    operacaoSwap.TaxaJurosContraParte = 0;
                }
                #endregion


                if (!String.IsNullOrEmpty(textValorIndicePartida2.Text))
                {
                    operacaoSwap.ValorIndicePartidaContraParte = Convert.ToDecimal(textValorIndicePartida2.Text);
                }
                else
                {
                    operacaoSwap.ValorIndicePartidaContraParte = null;
                }

            }
            #endregion

            #region reset
            if (string.IsNullOrEmpty(textDataInicioReset.Text))
                operacaoSwap.DataInicioReset = operacaoSwap.DataRegistro;
            else
                operacaoSwap.DataInicioReset = Convert.ToDateTime(textDataInicioReset.Text);

            if (dropRealizaReset.SelectedItem.Value == "S")
            {
                operacaoSwap.RealizaReset = "S";
                operacaoSwap.PeriodicidadeReset = Convert.ToInt32(dropPeriodicidadeReset.SelectedItem.Value);
                operacaoSwap.QtdePeriodicidade = Convert.ToInt32(textQtdePeriodicidade.Text);
            }
            else
            {
                operacaoSwap.RealizaReset = "N";
                operacaoSwap.PeriodicidadeReset = null;
                operacaoSwap.QtdePeriodicidade = null;
            }
            #endregion

            int diasCorridos = Calendario.NumeroDias(Convert.ToDateTime(textDataEmissao.Text),
                                                     Convert.ToDateTime(textDataVencimento.Text));
            //
            int diasUteis = Calendario.NumeroDias(Convert.ToDateTime(textDataEmissao.Text),
                                                  Convert.ToDateTime(textDataVencimento.Text),
                                                  LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            //
            operacaoSwap.DiasCorridos = diasCorridos;
            operacaoSwap.DiasUteis = diasUteis;

            operacaoSwap.Save();

            EventoSwap eventoSwap = new EventoSwap();
            idOperacao = operacaoSwap.IdOperacao.Value;
            if (operacaoSwap.RealizaReset == "S")
            {
                decimal valorBase = operacaoSwap.ValorBase.Value;
                idOperacao = operacaoSwap.IdOperacao.Value;
                int qtdePeriodicidade = operacaoSwap.QtdePeriodicidade.Value;
                int periodicidadeReset = operacaoSwap.PeriodicidadeReset.Value;
                DateTime dataEmissao = operacaoSwap.DataEmissao.Value;
                DateTime dataInicioReset = operacaoSwap.DataInicioReset.Value;
                DateTime dataVencimento = operacaoSwap.DataVencimento.Value;
                
                eventoSwap.GeraFluxoReset(idOperacao, dataEmissao, valorBase, dataInicioReset, dataVencimento, qtdePeriodicidade, periodicidadeReset);

            }
            else
            {
                eventoSwap.DeletaEvento(idOperacao, (int)Financial.Swap.Enums.TipoEventoSwap.Reset);
            }

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de OperacaoSwap - Operacao: Update OperacaoSwap: " + idOperacao + UtilitarioWeb.ToString(operacaoSwap),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion

            //Atualiza StatusRealTime para executar***********
            Cliente cliente = new Cliente();
            cliente.SetaFlagRealTime(idCliente, (byte)StatusRealTimeCliente.Executar);
            //************************************************
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxComboBox dropTipoRegistro = gridCadastro.FindEditFormTemplateControl("dropTipoRegistro") as ASPxComboBox;
        ASPxComboBox dropAgente = gridCadastro.FindEditFormTemplateControl("dropAgente") as ASPxComboBox;
        TextBox textNumeroContrato = gridCadastro.FindEditFormTemplateControl("textNumeroContrato") as TextBox;
		TextBox textCodigoIsin = gridCadastro.FindEditFormTemplateControl("textCodigoIsin") as TextBox;
		TextBox textCpfcnpjContraParte = gridCadastro.FindEditFormTemplateControl("textCpfcnpjContraParte") as TextBox;

        ASPxComboBox dropComGarantia = gridCadastro.FindEditFormTemplateControl("dropComGarantia") as ASPxComboBox;
        ASPxDateEdit textDataRegistro = gridCadastro.FindEditFormTemplateControl("textDataRegistro") as ASPxDateEdit;
        ASPxDateEdit textDataEmissao = gridCadastro.FindEditFormTemplateControl("textDataEmissao") as ASPxDateEdit;
        ASPxDateEdit textDataVencimento = gridCadastro.FindEditFormTemplateControl("textDataVencimento") as ASPxDateEdit;
        ASPxSpinEdit textValorBase = gridCadastro.FindEditFormTemplateControl("textValorBase") as ASPxSpinEdit;
        ASPxComboBox dropEstrategia = gridCadastro.FindEditFormTemplateControl("dropEstrategia") as ASPxComboBox;
        ASPxComboBox dropCategoriaMovimentacao = gridCadastro.FindEditFormTemplateControl("dropCategoriaMovimentacao") as ASPxComboBox;
        //Ponta 1
        ASPxComboBox dropTipoPonta = gridCadastro.FindEditFormTemplateControl("dropTipoPonta") as ASPxComboBox;
        ASPxComboBox dropTipoApropriacao = gridCadastro.FindEditFormTemplateControl("dropTipoApropriacao") as ASPxComboBox;
        ASPxComboBox dropContagemDias = gridCadastro.FindEditFormTemplateControl("dropContagemDias") as ASPxComboBox;
        ASPxComboBox dropBaseAno = gridCadastro.FindEditFormTemplateControl("dropBaseAno") as ASPxComboBox;
        ASPxSpinEdit textPercentual = gridCadastro.FindEditFormTemplateControl("textPercentual") as ASPxSpinEdit;
        ASPxComboBox dropIndice = gridCadastro.FindEditFormTemplateControl("dropIndice") as ASPxComboBox;
        ASPxSpinEdit textTaxaJuros = gridCadastro.FindEditFormTemplateControl("textTaxaJuros") as ASPxSpinEdit;
        ASPxSpinEdit textValorIndicePartida = gridCadastro.FindEditFormTemplateControl("textValorIndicePartida") as ASPxSpinEdit;
        ASPxTextBox hiddenIdAtivo = gridCadastro.FindEditFormTemplateControl("hiddenIdAtivo") as ASPxTextBox;
        //Ponta 2
        ASPxComboBox dropTipoPonta2 = gridCadastro.FindEditFormTemplateControl("dropTipoPonta2") as ASPxComboBox;
        ASPxComboBox dropTipoApropriacao2 = gridCadastro.FindEditFormTemplateControl("dropTipoApropriacao2") as ASPxComboBox;
        ASPxComboBox dropContagemDias2 = gridCadastro.FindEditFormTemplateControl("dropContagemDias2") as ASPxComboBox;
        ASPxComboBox dropBaseAno2 = gridCadastro.FindEditFormTemplateControl("dropBaseAno2") as ASPxComboBox;
        ASPxSpinEdit textPercentual2 = gridCadastro.FindEditFormTemplateControl("textPercentual2") as ASPxSpinEdit;
        ASPxComboBox dropIndice2 = gridCadastro.FindEditFormTemplateControl("dropIndice2") as ASPxComboBox;
        ASPxSpinEdit textTaxaJuros2 = gridCadastro.FindEditFormTemplateControl("textTaxaJuros2") as ASPxSpinEdit;
        ASPxSpinEdit textValorIndicePartida2 = gridCadastro.FindEditFormTemplateControl("textValorIndicePartida2") as ASPxSpinEdit;
        ASPxTextBox hiddenIdAtivoContraParte = gridCadastro.FindEditFormTemplateControl("hiddenIdAtivoContraParte") as ASPxTextBox;

        //Reset 
        ASPxComboBox dropRealizaReset = gridCadastro.FindEditFormTemplateControl("dropRealizaReset") as ASPxComboBox;
        ASPxDateEdit textDataInicioReset = gridCadastro.FindEditFormTemplateControl("textDataInicioReset") as ASPxDateEdit;
        ASPxSpinEdit textQtdePeriodicidade = gridCadastro.FindEditFormTemplateControl("textQtdePeriodicidade") as ASPxSpinEdit;
        ASPxComboBox dropPeriodicidadeReset = gridCadastro.FindEditFormTemplateControl("dropPeriodicidadeReset") as ASPxComboBox;

        OperacaoSwap operacaoSwap = new OperacaoSwap();

        int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);

        operacaoSwap.IdCliente = idCliente;
        operacaoSwap.TipoRegistro = Convert.ToByte(dropTipoRegistro.SelectedItem.Value);

        if (dropCategoriaMovimentacao.SelectedIndex != -1)
        {
            operacaoSwap.IdCategoriaMovimentacao = Convert.ToInt32(dropCategoriaMovimentacao.SelectedItem.Value);
        }
        else
        {
            operacaoSwap.IdCategoriaMovimentacao = null;
        }

        if (dropAgente.SelectedIndex != -1) {
            operacaoSwap.IdAgente = Convert.ToInt32(dropAgente.SelectedItem.Value);
        }
       
        operacaoSwap.IdEstrategia = Convert.ToInt32(dropEstrategia.SelectedItem.Value);
        

        operacaoSwap.NumeroContrato = textNumeroContrato.Text.ToString();
		operacaoSwap.CodigoIsin = textCodigoIsin.Text.ToString();
		operacaoSwap.CpfcnpjContraParte = textCpfcnpjContraParte.Text.ToString();

        operacaoSwap.ComGarantia = Convert.ToString(dropComGarantia.SelectedItem.Value);
        operacaoSwap.DataEmissao = Convert.ToDateTime(textDataEmissao.Text);
        operacaoSwap.DataRegistro = Convert.ToDateTime(textDataRegistro.Text);
        operacaoSwap.DataVencimento = Convert.ToDateTime(textDataVencimento.Text);
        operacaoSwap.ValorBase = Convert.ToDecimal(textValorBase.Text);

        #region Ponta 1
        operacaoSwap.TipoPonta = Convert.ToByte(dropTipoPonta.SelectedItem.Value);

        operacaoSwap.Percentual = null;
        operacaoSwap.IdIndice = null;
        operacaoSwap.TipoApropriacao = null;
        operacaoSwap.ContagemDias = null;
        operacaoSwap.BaseAno = null;
        operacaoSwap.ValorIndicePartida = null;
        operacaoSwap.CdAtivoBolsa = null;
        operacaoSwap.IdAtivoCarteira = null;
        operacaoSwap.TaxaJuros = null;
        if (operacaoSwap.TipoPonta.Value == (byte)TipoPontaSwap.FundoInvestimento)
        {
            operacaoSwap.IdAtivoCarteira = Convert.ToInt32(hiddenIdAtivo.Text);
        }
        else if (operacaoSwap.TipoPonta.Value == (byte)TipoPontaSwap.AtivoBolsa)
        {
            operacaoSwap.CdAtivoBolsa = hiddenIdAtivo.Text.Trim();
        }
        else
        {
            if (Convert.ToByte(dropTipoPonta.SelectedItem.Value) == (byte)TipoPontaSwap.PreFixado)
            {
                operacaoSwap.Percentual = 0;
                operacaoSwap.IdIndice = null;
            }
            else
            {
                #region Percentual
                if (textPercentual.Text != "")
                {
                    operacaoSwap.Percentual = Convert.ToDecimal(textPercentual.Text);
                }
                else
                {
                    operacaoSwap.Percentual = 0;
                }
                #endregion

                if (dropIndice.SelectedIndex != -1)
                {
                    operacaoSwap.IdIndice = Convert.ToInt16(dropIndice.SelectedItem.Value);
                }
            }

            operacaoSwap.TipoApropriacao = Convert.ToByte(dropTipoApropriacao.SelectedItem.Value);
            operacaoSwap.ContagemDias = Convert.ToByte(dropContagemDias.SelectedItem.Value);
            operacaoSwap.BaseAno = Convert.ToInt16(dropBaseAno.SelectedItem.Value);

            if (textTaxaJuros.Text != "")
            {
                operacaoSwap.TaxaJuros = Convert.ToDecimal(textTaxaJuros.Text);
            }
            else
            {
                operacaoSwap.TaxaJuros = 0;
            }

            if (!String.IsNullOrEmpty(textValorIndicePartida.Text))
            {
                operacaoSwap.ValorIndicePartida = Convert.ToDecimal(textValorIndicePartida.Text);
            }
        }
        #endregion

        #region Ponta ContraParte
        operacaoSwap.TipoPontaContraParte = Convert.ToByte(dropTipoPonta2.SelectedItem.Value);

        operacaoSwap.PercentualContraParte = null;
        operacaoSwap.IdIndiceContraParte = null;
        operacaoSwap.TipoApropriacaoContraParte = null;
        operacaoSwap.ContagemDiasContraParte = null;
        operacaoSwap.BaseAnoContraParte = null;
        operacaoSwap.ValorIndicePartidaContraParte = null;
        operacaoSwap.CdAtivoBolsaContraParte = null;
        operacaoSwap.IdAtivoCarteiraContraParte = null;
        operacaoSwap.TaxaJurosContraParte = null;
        if (operacaoSwap.TipoPontaContraParte.Value == (byte)TipoPontaSwap.FundoInvestimento)
        {
            operacaoSwap.IdAtivoCarteiraContraParte = Convert.ToInt32(hiddenIdAtivoContraParte.Text);
        }
        else if (operacaoSwap.TipoPontaContraParte.Value == (byte)TipoPontaSwap.AtivoBolsa)
        {
            operacaoSwap.CdAtivoBolsaContraParte = hiddenIdAtivoContraParte.Text.Trim();
        }
        else
        {
            if (Convert.ToByte(dropTipoPonta2.SelectedItem.Value) == (byte)TipoPontaSwap.PreFixado)
            {
                operacaoSwap.PercentualContraParte = 0;
                operacaoSwap.IdIndiceContraParte = null;
            }
            else
            {
                #region Percentual ContraParte
                if (textPercentual2.Text != "")
                {
                    operacaoSwap.PercentualContraParte = Convert.ToDecimal(textPercentual2.Text);
                }
                else
                {
                    operacaoSwap.PercentualContraParte = 0;
                }
                #endregion

                if (dropIndice2.SelectedIndex != -1)
                {
                    operacaoSwap.IdIndiceContraParte = Convert.ToInt16(dropIndice2.SelectedItem.Value);
                }
            }

            operacaoSwap.TipoApropriacaoContraParte = Convert.ToByte(dropTipoApropriacao2.SelectedItem.Value);
            operacaoSwap.ContagemDiasContraParte = Convert.ToByte(dropContagemDias2.SelectedItem.Value);
            operacaoSwap.BaseAnoContraParte = Convert.ToInt16(dropBaseAno2.SelectedItem.Value);

            if (textTaxaJuros2.Text != "")
            {
                operacaoSwap.TaxaJurosContraParte = Convert.ToDecimal(textTaxaJuros2.Text);
            }
            else
            {
                operacaoSwap.TaxaJurosContraParte = 0;
            }

            if (!String.IsNullOrEmpty(textValorIndicePartida2.Text))
            {
                operacaoSwap.ValorIndicePartidaContraParte = Convert.ToDecimal(textValorIndicePartida2.Text);
            }
        }
        #endregion

        #region reset
        if (string.IsNullOrEmpty(textDataInicioReset.Text))
            operacaoSwap.DataInicioReset = operacaoSwap.DataRegistro;
        else
            operacaoSwap.DataInicioReset = Convert.ToDateTime(textDataInicioReset.Text);

        if (dropRealizaReset.SelectedItem.Value == "S")
        {
            operacaoSwap.RealizaReset = "S";
            operacaoSwap.PeriodicidadeReset = Convert.ToInt32(dropPeriodicidadeReset.SelectedItem.Value);
            operacaoSwap.QtdePeriodicidade = Convert.ToInt32(textQtdePeriodicidade.Text);            
        }
        else
        {
            operacaoSwap.RealizaReset = "N";
            operacaoSwap.PeriodicidadeReset = null;
            operacaoSwap.QtdePeriodicidade = null;
        }
        #endregion

        int diasCorridos = Calendario.NumeroDias(Convert.ToDateTime(textDataEmissao.Text),
                                                 Convert.ToDateTime(textDataVencimento.Text));
        //
        int diasUteis = Calendario.NumeroDias(Convert.ToDateTime(textDataEmissao.Text),
                                              Convert.ToDateTime(textDataVencimento.Text),
                                              LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
        //
        operacaoSwap.DiasCorridos = diasCorridos;
        operacaoSwap.DiasUteis = diasUteis;

        operacaoSwap.Save();

        if(operacaoSwap.RealizaReset == "S")
        {
            decimal valorBase = operacaoSwap.ValorBase.Value;
            int idOperacao = operacaoSwap.IdOperacao.Value;
            int qtdePeriodicidade = operacaoSwap.QtdePeriodicidade.Value;
            int periodicidadeReset = operacaoSwap.PeriodicidadeReset.Value;
            DateTime dataEmissao = operacaoSwap.DataEmissao.Value;
            DateTime dataInicioReset = operacaoSwap.DataInicioReset.Value;
            DateTime dataVencimento = operacaoSwap.DataVencimento.Value;

            EventoSwap eventoSwap = new EventoSwap();
            eventoSwap.GeraFluxoReset(idOperacao, dataEmissao, valorBase, dataInicioReset, dataVencimento, qtdePeriodicidade, periodicidadeReset);

        }

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de OperacaoSwap - Operacao: Insert OperacaoSwap: "  + UtilitarioWeb.ToString(operacaoSwap),
                                        HttpContext.Current.User.Identity.Name, 
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        //Atualiza StatusRealTime para executar***********
        Cliente cliente = new Cliente();
        cliente.SetaFlagRealTime(idCliente, (byte)StatusRealTimeCliente.Executar);
        //************************************************        

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdOperacao");
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idOperacao = Convert.ToInt32(keyValuesId[i]);

                OperacaoSwap operacaoSwap = new OperacaoSwap();
                if (operacaoSwap.LoadByPrimaryKey(idOperacao)) {
                    int idCliente = operacaoSwap.IdCliente.Value;

                    #region Deleta posição
                    EventoSwapCollection eventoSwapColl = new EventoSwapCollection();
                    PosicaoSwapCollection posicaoSwapColl = new PosicaoSwapCollection();
                    PosicaoSwapHistoricoCollection posicaoSwapHisColl = new PosicaoSwapHistoricoCollection();
                    PosicaoSwapAberturaCollection posicaoSwapAbeColl = new PosicaoSwapAberturaCollection();

                    using (esTransactionScope scope = new esTransactionScope())
                    {
                        eventoSwapColl.Query.Where(eventoSwapColl.Query.IdOperacao.Equal(idOperacao));
                        if (eventoSwapColl.Query.Load())
                        {
                            eventoSwapColl.MarkAllAsDeleted();
                            eventoSwapColl.Save();
                        }

                        posicaoSwapColl.Query.Where(posicaoSwapColl.Query.IdOperacao.Equal(idOperacao));
                        if (posicaoSwapColl.Query.Load())
                        {
                            posicaoSwapColl.MarkAllAsDeleted();
                            posicaoSwapColl.Save();
                        }

                        posicaoSwapHisColl.Query.Where(posicaoSwapHisColl.Query.IdOperacao.Equal(idOperacao));
                        if (posicaoSwapHisColl.Query.Load())
                        {
                            posicaoSwapHisColl.MarkAllAsDeleted();
                            posicaoSwapHisColl.Save();
                        }

                        posicaoSwapAbeColl.Query.Where(posicaoSwapAbeColl.Query.IdOperacao.Equal(idOperacao));
                        if (posicaoSwapAbeColl.Query.Load())
                        {
                            posicaoSwapAbeColl.MarkAllAsDeleted();
                            posicaoSwapAbeColl.Save();
                        }

                        scope.Complete();
                    }
                    #endregion

                    //
                    OperacaoSwap operacaoSwapClone = (OperacaoSwap)Utilitario.Clone(operacaoSwap);
                    //

                    operacaoSwap.MarkAsDeleted();
                    operacaoSwap.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de OperacaoSwap - Operacao: Delete OperacaoSwap: " + idOperacao + UtilitarioWeb.ToString(operacaoSwapClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion

                    //Atualiza StatusRealTime para executar***********
                    Cliente cliente = new Cliente();
                    cliente.SetaFlagRealTime(idCliente, (byte)StatusRealTimeCliente.Executar);
                    //************************************************
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing) {
            ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
            e.Properties["cpBtnEditCodigoCliente"] = btnEditCodigoCliente.ClientID;
        }
    }

    protected void gridCadastro_InitNewRow(object sender, ASPxDataInitNewRowEventArgs e) {
        //Valores default do form
        e.NewValues[OperacaoSwapMetadata.ColumnNames.NumeroContrato] = "0";
        e.NewValues[OperacaoSwapMetadata.ColumnNames.ComGarantia] = "N";
        e.NewValues[OperacaoSwapMetadata.ColumnNames.Percentual] = "0";
        e.NewValues[OperacaoSwapMetadata.ColumnNames.TaxaJuros] = "0";
        e.NewValues[OperacaoSwapMetadata.ColumnNames.PercentualContraParte] = "0";
        e.NewValues[OperacaoSwapMetadata.ColumnNames.TaxaJurosContraParte] = "0";
        e.NewValues[OperacaoSwapMetadata.ColumnNames.ValorIndicePartida] = "0";
        e.NewValues[OperacaoSwapMetadata.ColumnNames.ValorIndicePartidaContraParte] = "0";
        e.NewValues[OperacaoSwapMetadata.ColumnNames.RealizaReset] = "N";
    }

    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("btnEditCodigoCliente");
        base.gridCadastro_PreRender(sender, e);

        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        Label labelFiltroHeader = gridCadastro.FindStatusBarTemplateControl("labelFiltroHeader") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");

        if (btnEditCodigoClienteFiltro != null && btnEditCodigoClienteFiltro.Text != "") {
            texto.Append(" Cliente: ").Append(btnEditCodigoClienteFiltro.Text);
        }
        if (textDataInicio.Text != "") {
            if (btnEditCodigoClienteFiltro != null && btnEditCodigoClienteFiltro.Text != "") {
                texto.Append(" |");
            }
            texto.Append(" Data Início >= ").Append(textDataInicio.Text.Substring(0, 10));
        }
        if (textDataFim.Text != "") {
            if (textDataInicio.Text != "" || btnEditCodigoClienteFiltro.Text != "") {
                texto.Append(" |");
            }
            texto.Append(" Data Fim <= ").Append(textDataFim.Text.Substring(0, 10));
        }

        labelFiltro.Text = texto.ToString();
    }

    #region Grid Ativo/ContraParte
    protected void gridAtivo_CustomDataCallback(object sender, DevExpress.Web.ASPxGridViewCustomDataCallbackEventArgs e)
    {
        ASPxGridView gridView = sender as ASPxGridView;
        int visibleIndex = Convert.ToInt32(e.Parameters);
        e.Result = gridView.GetRowValues(visibleIndex, "IdAtivo").ToString();
    }

    protected void gridAtivoContraParte_CustomDataCallback(object sender, DevExpress.Web.ASPxGridViewCustomDataCallbackEventArgs e)
    {
        ASPxGridView gridView = sender as ASPxGridView;
        int visibleIndex = Convert.ToInt32(e.Parameters);
        e.Result = gridView.GetRowValues(visibleIndex, "IdAtivo").ToString();
    }

    public void gridAtivo_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        gridAtivo.DataBind();
    }

    public void gridAtivoContraParte_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        gridAtivoContraParte.DataBind();
    }

    protected void gridAtivo_HtmlRowCreated(object sender, DevExpress.Web.ASPxGridViewTableRowEventArgs e)
    {
        e.Row.Attributes.Add("onmouseover", "backgroundColor = this.style.backgroundColor; this.style.backgroundColor='#E0E9EB';");
        e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=backgroundColor;");
    }

    protected void gridAtivoContraParte_HtmlRowCreated(object sender, DevExpress.Web.ASPxGridViewTableRowEventArgs e)
    {
        e.Row.Attributes.Add("onmouseover", "backgroundColor = this.style.backgroundColor; this.style.backgroundColor='#E0E9EB';");
        e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=backgroundColor;");
    }
    #endregion
}