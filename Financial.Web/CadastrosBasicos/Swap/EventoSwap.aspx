﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EventoSwap.aspx.cs" Inherits="CadastrosBasicos_EventoSwap" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
        var popup = true;
        document.onkeydown = onDocumentKeyDownWithCallback;
        var operacao = '';
        
        function DesabilitaCampos()
        {                          
            if(dropEventoSwap.GetSelectedItem() != null)
            {
                var evento = dropEventoSwap.GetSelectedItem().value;
                var cssPontaSwap = 'labelNormal';
                if(evento == 1) //Para reset não é necessário setar a ponta do Swap
                {
                    dropPontaSwap.SetEnabled(false);
                    dropPontaSwap.SetValue(3); //Ambas
                    
                } 
                else
                {
                    dropPontaSwap.SetEnabled(true);   
                    cssPontaSwap = 'labelRequired';              
                    dropPontaSwap.SetValue(null); //Ambas
                }
                
                lblPontaSwap.GetMainElement().className = cssPontaSwap;
            }       
        }
        
        function OnGetDataOperacao(data) 
        {
            hiddenIdOperacao.SetValue(data);
            callbackOperacao.SendCallback(data);
            popupOperacao.HideWindow();
            btnEditOperacao.Focus();
        }            
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <dxcb:ASPxCallback ID="callbackOperacao" runat="server" OnCallback="callbackOperacao_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) { if (e.result != '') btnEditOperacao.SetValue(e.result); }" />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new')
                {             
                    gridCadastro.UpdateEdit();
                }
                else
                {
                    callbackAdd.SendCallback();
                }                          
            }
            operacao = '';
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            alert('Operação feita com sucesso.');
        }        
        " />
        </dxcb:ASPxCallback>
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Eventos de Swap"></asp:Label>
                            </div>
                            <div id="mainContent">
                                <dxpc:ASPxPopupControl ID="popupOperacao" runat="server" HeaderText="" Width="500px"
                                    ContentStyle-VerticalAlign="Top" EnableClientSideAPI="True" PopupVerticalAlign="Middle"
                                    PopupHorizontalAlign="OutsideRight" AllowDragging="True">
                                    <ContentCollection>
                                        <dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                                            <div>
                                                <dxwgv:ASPxGridView ID="gridOperacao" runat="server" Width="100%" ClientInstanceName="gridOperacao"
                                                    AutoGenerateColumns="False" DataSourceID="EsDSOperacaoSwap" KeyFieldName="IdOperacao"
                                                    OnCustomDataCallback="gridOperacao_CustomDataCallback" OnCustomCallback="gridOperacao_CustomCallBack">
                                                    <Columns>
                                                        <dxwgv:GridViewDataTextColumn FieldName="IdOperacao" Caption="Id" VisibleIndex="1" />
                                                        <dxwgv:GridViewDataTextColumn FieldName="Cliente" Caption="Cliente" VisibleIndex="2" />
                                                        <dxwgv:GridViewDataComboBoxColumn FieldName="TipoPonta" Caption="Tipo Ponta" VisibleIndex="3">
                                                            <PropertiesComboBox EncodeHtml="false">
                                                                <Items>
                                                                    <dxe:ListEditItem Value="1" Text="<div title='Pré Fixado'>Pré Fixado</div>" />
                                                                    <dxe:ListEditItem Value="2" Text="<div title='Indexado'>Indexado</div>" />
                                                                    <dxe:ListEditItem Value="3" Text="<div title='Indexado-D0'>Indexado-D0</div>" />
                                                                    <dxe:ListEditItem Value="4" Text="<div title='Ativo Bolsa'>Ativo Bolsa</div>" />
                                                                    <dxe:ListEditItem Value="5" Text="<div title='Fundo de Investimento'>Fundo de Investimento</div>" />
                                                                </Items>
                                                            </PropertiesComboBox>
                                                        </dxwgv:GridViewDataComboBoxColumn>
                                                        <dxwgv:GridViewDataComboBoxColumn FieldName="TipoPontaContraParte" Caption="Tipo Ponta Contra Parte"
                                                            VisibleIndex="4">
                                                            <PropertiesComboBox EncodeHtml="false">
                                                                <Items>
                                                                    <dxe:ListEditItem Value="1" Text="<div title='Pré Fixado'>Pré Fixado</div>" />
                                                                    <dxe:ListEditItem Value="2" Text="<div title='Indexado'>Indexado</div>" />
                                                                    <dxe:ListEditItem Value="3" Text="<div title='Indexado-D0'>Indexado-D0</div>" />
                                                                    <dxe:ListEditItem Value="4" Text="<div title='Ativo Bolsa'>Ativo Bolsa</div>" />
                                                                    <dxe:ListEditItem Value="5" Text="<div title='Fundo de Investimento'>Fundo de Investimento</div>" />
                                                                </Items>
                                                            </PropertiesComboBox>
                                                        </dxwgv:GridViewDataComboBoxColumn>
                                                        <dxwgv:GridViewDataDateColumn FieldName="DataEmissao" Caption="Data Emissão" VisibleIndex="5" />
                                                        <dxwgv:GridViewDataDateColumn FieldName="DataVencimento" Caption="Data Vencimento"
                                                            VisibleIndex="6" />
                                                        <dxwgv:GridViewDataTextColumn FieldName="ValorBase" Caption="Valor Inicial" VisibleIndex="7">
                                                            <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                            </PropertiesTextEdit>
                                                        </dxwgv:GridViewDataTextColumn>
                                                    </Columns>
                                                    <Settings ShowFilterRow="True" ShowTitlePanel="True" />
                                                    <SettingsBehavior ColumnResizeMode="Disabled" />
                                                    <ClientSideEvents RowDblClick="function(s, e) { gridOperacao.GetValuesOnCustomCallback(e.visibleIndex, OnGetDataOperacao);}"
                                                        Init="function(s, e) {e.cancel = true; }" />
                                                    <SettingsDetail ShowDetailButtons="False" />
                                                    <Styles Cell-Wrap="False" AlternatingRow-CssClass="MyClass">
                                                        <Header ImageSpacing="5px" SortingImageSpacing="5px" />
                                                    </Styles>
                                                    <Images>
                                                    </Images>
                                                    <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Operação" />
                                                </dxwgv:ASPxGridView>
                                            </div>
                                        </dxpc:PopupControlContentControl>
                                    </ContentCollection>
                                    <ClientSideEvents CloseUp="function(s, e) {gridOperacao.ClearFilter();}" PopUp="function(s, e) {gridOperacao.PerformCallback();}" />
                                </dxpc:ASPxPopupControl>
                                <dxpc:ASPxPopupControl ID="popupFiltro" AllowDragging="true" PopupElementID="popupFiltro"
                                    EnableClientSideAPI="True" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
                                    Width="400" Left="250" Top="70" HeaderText="Filtros adicionais para consulta"
                                    runat="server" HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
                                    <HeaderStyle BackColor="#EBECEE" Font-Bold="True" Font-Size="11px"></HeaderStyle>
                                    <ContentCollection>
                                        <dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="labelDataInicio" runat="server" CssClass="labelNormal" Text="Início:"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="labelDataFim" runat="server" CssClass="labelNormal" Text="Fim:" />
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim" />
                                                    </td>
                                                </tr>
                                            </table>
                                            <div class="linkButton linkButtonNoBorder" style="margin-top: 20px">
                                                <asp:LinkButton ID="btnOKFilter" runat="server" Font-Overline="false" ForeColor="Black"
                                                    CssClass="btnOK" OnClientClick="popupFiltro.Hide(); gridCadastro.PerformCallback('btnRefresh'); return false;">
                                                    <asp:Literal ID="Literal7" runat="server" Text="Aplicar" />
                                                    <div>
                                                    </div>
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="btnFilterCancel" runat="server" Font-Overline="false" ForeColor="Black"
                                                    CssClass="btnFilterCancel" OnClientClick="OnClearFilterClick_FiltroDatas(); return false;">
                                                    <asp:Literal ID="Literal8" runat="server" Text="Limpar" />
                                                    <div>
                                                    </div>
                                                </asp:LinkButton>
                                            </div>
                                        </dxpc:PopupControlContentControl>
                                    </ContentCollection>
                                </dxpc:ASPxPopupControl>
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;">
                                        <asp:Literal ID="Literal1" runat="server" Text="Novo" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;">
                                        <asp:Literal ID="Literal2" runat="server" Text="Excluir" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnFilter" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnFilter" OnClientClick="return OnButtonClick_FiltroDatas()">
                                        <asp:Literal ID="Literal3" runat="server" Text="Filtro" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnPdf" OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal4" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnExcel" OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal5" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh"
                                        OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal6" runat="server" Text="Atualizar" /><div>
                                        </div>
                                    </asp:LinkButton>
                                </div>
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridCadastro" runat="server" KeyFieldName="IdEventoSwap"
                                        DataSourceID="EsDSEventoSwap" OnRowUpdating="gridCadastro_RowUpdating" OnRowInserting="gridCadastro_RowInserting"
                                        OnCustomCallback="gridCadastro_CustomCallback" OnPreRender="gridCadastro_PreRender"
                                        OnCustomJSProperties="gridCadastro_CustomJSProperties" OnBeforeGetCallbackResult="gridCadastro_PreRender"
                                        AutoGenerateColumns="False">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                                                <HeaderTemplate>
                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                                </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="IdEventoSwap" VisibleIndex="1" Width="10%" />
                                            <dxwgv:GridViewDataTextColumn FieldName="IdOperacao" Caption="Id.Operação" VisibleIndex="2"
                                                Width="10%" />
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="TipoEvento" Caption="Tipo Evento" VisibleIndex="3">
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="1" Text="<div title='Reset'>Reset</div>" />
                                                        <dxe:ListEditItem Value="2" Text="<div title='Ajuste'>Ajuste</div>" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="PontaSwap" Caption="Ponta" VisibleIndex="4">
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="1" Text="<div title='Parte'>Parte</div>" />
                                                        <dxe:ListEditItem Value="2" Text="<div title='Contra Parte'>Contra Parte</div>" />
                                                        <dxe:ListEditItem Value="3" Text="<div title='Ambas'>Ambas</div>" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>                                            
                                            <dxwgv:GridViewDataDateColumn FieldName="DataVigencia" Caption="Data Vigência" VisibleIndex="5">
                                            </dxwgv:GridViewDataDateColumn>
                                            <dxwgv:GridViewDataDateColumn FieldName="DataEvento" Caption="Data Evento" VisibleIndex="6">
                                            </dxwgv:GridViewDataDateColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="Valor" VisibleIndex="7" Width="12%" HeaderStyle-HorizontalAlign="Right"
                                                UnboundType="string" FooterCellStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                                                <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                        </Columns>
                                        <Templates>
                                            <StatusBar>
                                                <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" />
                                            </StatusBar>
                                            <EditForm>
                                                <div class="editForm">
                                                    <dxe:ASPxTextBox ID="hiddenIdOperacao" ClientInstanceName="hiddenIdOperacao" runat="server"
                                                        CssClass="hiddenField" Text='<%#Eval("IdOperacao")%>' />
                                                    <table>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="lblOperação" runat="server" CssClass="labelRequired" Text="Id Operação:" />
                                                            </td>
                                                            <td colspan="3">
                                                                <dxe:ASPxButtonEdit ID="btnEditOperacao" runat="server" CssClass="textButtonEdit"
                                                                    ClientInstanceName="btnEditOperacao" ReadOnly="true" Width="380px">
                                                                    <Buttons>
                                                                        <dxe:EditButton>
                                                                        </dxe:EditButton>
                                                                    </Buttons>
                                                                    <ClientSideEvents ButtonClick="function(s, e) {popupOperacao.ShowAtElementByID(s.name);}"
                                                                        Init="function(s, e) { callbackOperacao.SendCallback(); }" />
                                                                </dxe:ASPxButtonEdit>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label_Curto">
                                                                <asp:Label ID="lblEventoSwap" runat="server" CssClass="labelRequired" Text="Tipo Evento:">
                                                                </asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxComboBox ID="dropEventoSwap" ClientInstanceName="dropEventoSwap" runat="server"
                                                                    CssClass="dropDownListCurto" Text='<%#Eval("TipoEvento")%>' OnLoad="dropEventoSwap_Load">
                                                                    <ClientSideEvents Init="function(s, e) { DesabilitaCampos(); }" SelectedIndexChanged="function(s, e) { DesabilitaCampos(); }" />
                                                                </dxe:ASPxComboBox>
                                                            </td>
                                                            <td class="td_Label">
                                                               <dxe:ASPxLabel ID="lblPontaSwap" ClientInstanceName="lblPontaSwap" runat="server" CssClass="labelNormal" Text="Ponta Swap:"></dxe:ASPxLabel>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxComboBox ID="dropPontaSwap" ClientInstanceName="dropPontaSwap" runat="server"
                                                                    ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto_2"
                                                                    Text='<%#Eval("PontaSwap")%>' OnLoad="dropPontaSwap_Load">
                                                                </dxe:ASPxComboBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label_Curto">
                                                                <asp:Label ID="lblDataEvento" runat="server" CssClass="labelRequired" Text="Data Evento:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxDateEdit ID="textDataEvento" runat="server" ClientInstanceName="textDataEvento"
                                                                    Value='<%#Eval("DataEvento")%>'>
                                                                </dxe:ASPxDateEdit>
                                                            </td>
                                                            <td class="td_Label_Curto">
                                                                <asp:Label ID="labelDataVigencia" runat="server" CssClass="labelRequired" Text="Data Vigência:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxDateEdit ID="textDataVigencia" runat="server" ClientInstanceName="textDataVigencia"
                                                                    Value='<%#Eval("DataVigencia")%>' />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label_Curto">
                                                                <asp:Label ID="labelValor" runat="server" CssClass="labelRequired" Text="Valor:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="textValor" runat="server" ClientInstanceName="textValor" CssClass="textValor_5"
                                                                    Text='<%# Eval("Valor") %>' NumberType="Float" MaxLength="16" DecimalPlaces="8">
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div class="linhaH">
                                                    </div>
                                                    <div class="linkButton linkButtonNoBorder popupFooter">
                                                        <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd"
                                                            OnInit="btnOKAdd_Init" OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;">
                                                            <asp:Literal ID="Literal7" runat="server" Text="OK+" /><div>
                                                            </div>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                            OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                                                            <asp:Literal ID="Literal5" runat="server" Text="OK" /><div>
                                                            </div>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel"
                                                            OnClientClick="gridCadastro.CancelEdit(); return false;">
                                                            <asp:Literal ID="Literal6" runat="server" Text="Cancelar" /><div>
                                                            </div>
                                                        </asp:LinkButton>
                                                    </div>
                                                </div>
                                            </EditForm>
                                        </Templates>
                                        <SettingsPopup EditForm-Width="250px" />
                                        <Templates>
                                            <StatusBar>
                                                <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" />
                                            </StatusBar>
                                        </Templates>
                                        <SettingsCommandButton>
                                            <ClearFilterButton Image-Url="../../imagens/funnel--minus.png">
                                                <Image Url="../../imagens/funnel--minus.png">
                                                </Image>
                                            </ClearFilterButton>
                                        </SettingsCommandButton>
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
        <cc1:esDataSource ID="EsDSEventoSwap" runat="server" OnesSelect="EsDSEventoSwap_esSelect" />
        <cc1:esDataSource ID="EsDSOperacaoSwap" runat="server" OnesSelect="EsDSOperacaoSwap_esSelect" />
    </form>
</body>
</html>
