﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using Financial.Investidor;
using Financial.ContaCorrente;
using System.Drawing;
using Financial.ContaCorrente.Enums;
using Financial.Security;
using Financial.Investidor.Enums;
using Financial.Common.Enums;
using Financial.Swap;
using Financial.Swap.Enums;
using DevExpress.Web.Data;
using Financial.Web.Common;
using EntitySpaces.Interfaces;
using System.Threading;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class CadastrosBasicos_LiquidacaoSwap : CadastroBasePage {
    private int selectedIndex;

    new protected void Page_Load(object sender, EventArgs e) {
        this.HasFiltro = true;
        this.HasPopupCliente = true;
        this.HasPanelFieldsLoading = true;
        base.Page_Load(sender, e);

        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
                          new List<string>(new string[] { LiquidacaoSwapMetadata.ColumnNames.TipoLiquidacao }));

        this.gridCadastro.ClientSideEvents.RowDblClick = "function(s, e) {selectedIndex = e.visibleIndex; gridCadastro.GetRowValues(e.visibleIndex, 'TipoLiquidacao', OpenPopup); }";
    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSLiquidacaoSwap_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
        LiquidacaoSwapQuery liquidacaoSwapQuery = new LiquidacaoSwapQuery("L");
        ClienteQuery clienteQuery = new ClienteQuery("C");

        liquidacaoSwapQuery.Select(liquidacaoSwapQuery, clienteQuery.Apelido.As("Apelido"));
        liquidacaoSwapQuery.InnerJoin(clienteQuery).On(liquidacaoSwapQuery.IdCliente == clienteQuery.IdCliente);
        liquidacaoSwapQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        liquidacaoSwapQuery.Where(permissaoClienteQuery.IdUsuario.Equal(idUsuario));

        if (!String.IsNullOrEmpty(btnEditCodigoClienteFiltro.Text)) {
            liquidacaoSwapQuery.Where(liquidacaoSwapQuery.IdCliente.Equal(Convert.ToInt32(btnEditCodigoClienteFiltro.Text)));
        }

        if (!String.IsNullOrEmpty(textDataInicio.Text)) {
            liquidacaoSwapQuery.Where(liquidacaoSwapQuery.Data.GreaterThanOrEqual(textDataInicio.Text));
        }

        if (!String.IsNullOrEmpty(textDataFim.Text)) {
            liquidacaoSwapQuery.Where(liquidacaoSwapQuery.Data.LessThanOrEqual(textDataFim.Text));
        }

        liquidacaoSwapQuery.OrderBy(liquidacaoSwapQuery.Data.Descending);

        LiquidacaoSwapCollection coll = new LiquidacaoSwapCollection();
        coll.Load(liquidacaoSwapQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSPosicaoSwap_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;

        if (btnEditCodigoCliente != null) {
            if (btnEditCodigoCliente.Text == "") {
                e.Collection = new PosicaoSwapCollection();
                return;
            }
        }
        else {
            e.Collection = new PosicaoSwapCollection();
            return;
        }

        Cliente cliente = new Cliente();
        List<esQueryItem> campos = new List<esQueryItem>();
        campos.Add(cliente.Query.DataDia);
        cliente.LoadByPrimaryKey(campos, Convert.ToInt32(btnEditCodigoCliente.Text));
        DateTime data = cliente.DataDia.Value;

        PosicaoSwapCollection coll = new PosicaoSwapCollection();

        coll.Query.Where(coll.Query.ValorBase.GreaterThan(0));
        coll.Query.Where(coll.Query.IdCliente.Equal(Convert.ToInt32(btnEditCodigoCliente.Text)));

        coll.Query.OrderBy(coll.Query.DataVencimento.Descending);

        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ClienteCollection coll = new ClienteCollection();
        coll.BuscaClientesComAcesso(Context.User.Identity.Name);
        
        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void panelEdicao_Load(object sender, EventArgs e) {
        UtilitarioGrid.SetaCorCombosPopup(sender as Control);

        if (gridCadastro.IsEditing && gridCadastro.IsNewRowEditing) {
            Label labelValor = gridCadastro.FindEditFormTemplateControl("labelValor") as Label;
            ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
            labelValor.Visible = false;
            textValor.Visible = false;
            LinkButton btnOK = gridCadastro.FindEditFormTemplateControl("btnOK") as LinkButton;
            btnOK.Visible = false;
        }
        else if (gridCadastro.IsEditing && gridCadastro.IsNewRowEditing) {
            LinkButton btnPosicaoSwap = gridCadastro.FindEditFormTemplateControl("btnPosicaoSwap") as LinkButton;
            btnPosicaoSwap.Visible = false;
        }

        bool travaPainel = false;
        if (gridCadastro.IsEditing && !gridCadastro.IsNewRowEditing && Convert.ToString(Session["FormLoad"]) != "S") {
            Session["FormLoad"] = "S";
            Label labelEdicao = gridCadastro.FindEditFormTemplateControl("labelEdicao") as Label;

            DateTime data = Convert.ToDateTime(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "Data"));
            int idCliente = Convert.ToInt32(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "IdCliente"));

            Cliente cliente = new Cliente();
            if (cliente.LoadByPrimaryKey(idCliente)) {
                byte status = cliente.Status.Value;
                if (status == (byte)StatusCliente.Divulgado) {
                    travaPainel = true;
                    labelEdicao.Text = "Cliente está fechado. Dados não podem ser alterados.";
                }
                else if (DateTime.Compare(cliente.DataDia.Value, data) > 0) {
                    travaPainel = true;
                    labelEdicao.Text = "Data do lançamento da liquidação anterior à data atual do cliente. Dados não podem ser alterados.";
                }

            }
        }

        if (travaPainel) {
            LinkButton btnOK = gridCadastro.FindEditFormTemplateControl("btnOK") as LinkButton;
            btnOK.Visible = false;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";

        if (!gridCadastro.IsEditing) //Testa deleção
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdCliente");
            List<object> keyData = gridCadastro.GetSelectedFieldValues("Data");
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idCliente = Convert.ToInt32(keyValuesId[i]);
                DateTime data = Convert.ToDateTime(keyData[i]);

                Cliente cliente = new Cliente();
                if (cliente.LoadByPrimaryKey(idCliente)) {
                    DateTime dataDia = cliente.DataDia.Value;
                    byte status = cliente.Status.Value;
                    string clienteDescricao = cliente.Apelido + " (" + cliente.IdCliente.ToString() + ")";
                    if (status == (byte)StatusCliente.Divulgado) {
                        e.Result = "Cliente " + clienteDescricao + " está fechado! Deleção cancelada.";
                        return;
                    }
                    else if (DateTime.Compare(dataDia, data) > 0) {
                        e.Result = "Lançamento com data " + data.ToString().Substring(0, 10) + " anterior à data do cliente " + clienteDescricao + "! Deleção cancelada.";
                        return;
                    }
                }
            }
        }
        else {
            ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
            ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
            int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);

            Cliente cliente = new Cliente();
            if (cliente.LoadByPrimaryKey(idCliente)) {
                byte status = cliente.Status.Value;
                if (status == (byte)StatusCliente.Divulgado) {
                    e.Result = "Cliente está fechado! Operação não pode ser realizada.";
                    return;
                }
            }

            #region Campos obrigatórios
            List<Control> controles = new List<Control>();
            controles.Add(btnEditCodigoCliente);
            controles.Add(textData);

            if (base.TestaObrigatorio(controles) != "") {
                e.Result = "Campos com * são obrigatórios!";
                return;
            }
            #endregion

            if (gridCadastro.IsEditing && !gridCadastro.IsNewRowEditing) {
                TextBox hiddenIdPosicao = gridCadastro.FindEditFormTemplateControl("hiddenIdPosicao") as TextBox;

                int idPosicao = Convert.ToInt32(hiddenIdPosicao.Text);
                DateTime data = Convert.ToDateTime(textData.Text);

                PosicaoSwapAbertura posicaoSwapAbertura = new PosicaoSwapAbertura();
                if (posicaoSwapAbertura.LoadByPrimaryKey(idPosicao, data)) {
                    ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;

                    if (posicaoSwapAbertura.ValorBase.Value < Convert.ToDecimal(textValor.Text)) {
                        e.Result = "Valor a ser antecipado é maior que o valor base da posição! Operação não pode ser realizada.";
                        return;
                    }
                }
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErroPosicao_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";

        ASPxSpinEdit textValor = popupPosicaoSwap.FindControl("textValor") as ASPxSpinEdit;

        if (textValor.Text == "") {
            e.Result = "O valor deve ser preenchido.";
            return;
        }

        if (Convert.ToDecimal(textValor.Text) <= 0) {
            e.Result = "O valor deve ser maior do que zero.";
            return;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        string resultado = "";

        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") {
                if (cliente.Status.Value != (byte)StatusCliente.Divulgado) {

                    if (cliente.IsAtivo) {
                        PermissaoCliente permissaoCliente = new PermissaoCliente();
                        if (permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name)) {
                            nome = cliente.str.Apelido;
                            DateTime dataDia = cliente.DataDia.Value;
                            byte status = cliente.Status.Value;

                            if (status == (byte)StatusCliente.Divulgado && gridCadastro.IsEditing) {
                                resultado = cliente.str.Apelido + "|status_closed";
                            }
                            else {
                                resultado = nome + "|" + dataDia.ToString().Substring(0, 10) + "|" + Thread.CurrentThread.CurrentCulture.Name;
                            }
                        }
                        else {
                            resultado = "no_access";
                        }
                    }
                    else {
                        resultado = "no_active";
                    }
                }
                else {
                    resultado = cliente.str.Apelido + "|status_closed";
                }
            }
        }
        e.Result = resultado;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="selectedIndex"></param>
    protected void InsertLiquidacao(int selectedIndex) {
        ASPxPopupControl popupPosicaoSwap = this.FindControl("popupPosicaoSwap") as ASPxPopupControl;
        ASPxGridView gridPosicaoSwap = popupPosicaoSwap.FindControl("gridPosicaoSwap") as ASPxGridView;
        ASPxSpinEdit textValor = popupPosicaoSwap.FindControl("textValor") as ASPxSpinEdit;
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;

        int idPosicao = Convert.ToInt32(gridPosicaoSwap.GetRowValues(selectedIndex, "IdPosicao"));
        decimal valorAntecipacao = Convert.ToDecimal(textValor.Text);
        DateTime data = Convert.ToDateTime(textData.Text);

        //Checa valor já antecipado vs valor base de abertura
        PosicaoSwapAbertura posicaoSwapAbertura = new PosicaoSwapAbertura();
        decimal valorPosicaoAbertura = posicaoSwapAbertura.RetornaValorBasePosicao(idPosicao, data);
        LiquidacaoSwap liquidacaoSwapChecar = new LiquidacaoSwap();
        decimal valorLiquidado = liquidacaoSwapChecar.RetornaTotalLiquidado(idPosicao, data);

        if (valorAntecipacao > (valorPosicaoAbertura - valorLiquidado)) {
            valorAntecipacao = valorPosicaoAbertura - valorLiquidado;
        }
        //

        if (valorAntecipacao > 0) {
            LiquidacaoSwap liquidacaoSwap = new LiquidacaoSwap();
            liquidacaoSwap.IdPosicao = idPosicao;
            liquidacaoSwap.IdCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
            liquidacaoSwap.Data = data;
            liquidacaoSwap.ValorAntecipacao = valorAntecipacao;
            liquidacaoSwap.TipoLiquidacao = (byte)TipoLiquidacaoSwap.Antecipacao;

            liquidacaoSwap.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de LiquidacaoSwap - Operacao: Insert LiquidacaoSwap: " + liquidacaoSwap.IdLiquidacao + UtilitarioWeb.ToString(liquidacaoSwap),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion

            //Atualiza StatusRealTime para executar***********
            Cliente cliente = new Cliente();
            cliente.SetaFlagRealTime(liquidacaoSwap.IdCliente.Value, (byte)StatusRealTimeCliente.Executar);
            //************************************************
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CancelRowEditing(object sender, ASPxStartRowEditingEventArgs e) {
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        int idLiquidacao = (int)e.Keys[0];

        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;

        LiquidacaoSwap liquidacaoSwap = new LiquidacaoSwap();
        if (liquidacaoSwap.LoadByPrimaryKey(idLiquidacao)) {
            liquidacaoSwap.ValorAntecipacao = Convert.ToDecimal(textValor.Text);
            liquidacaoSwap.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de LiquidacaoSwap - Operacao: Update LiquidacaoSwap: " + idLiquidacao + UtilitarioWeb.ToString(liquidacaoSwap),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion


            //Atualiza StatusRealTime para executar***********
            Cliente cliente = new Cliente();
            cliente.SetaFlagRealTime(liquidacaoSwap.IdCliente.Value, (byte)StatusRealTimeCliente.Executar);
            //************************************************
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(LiquidacaoSwapMetadata.ColumnNames.IdLiquidacao);
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idLiquidacao = Convert.ToInt32(keyValuesId[i]);

                LiquidacaoSwap liquidacaoSwap = new LiquidacaoSwap();
                if (liquidacaoSwap.LoadByPrimaryKey(idLiquidacao)) {

                    LiquidacaoSwap liquidacaoSwapClone = (LiquidacaoSwap)Utilitario.Clone(liquidacaoSwap);
                    //
                    
                    liquidacaoSwap.MarkAsDeleted();
                    liquidacaoSwap.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de LiquidacaoSwap - Operacao: Delete LiquidacaoSwap: " + idLiquidacao + UtilitarioWeb.ToString(liquidacaoSwapClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion

                    //Atualiza StatusRealTime para executar***********
                    Cliente cliente = new Cliente();
                    cliente.SetaFlagRealTime(liquidacaoSwap.IdCliente.Value, (byte)StatusRealTimeCliente.Executar);
                    //************************************************
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing) {
            ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
            e.Properties["cpBtnEditCodigoCliente"] = btnEditCodigoCliente.ClientID;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("btnEditCodigoCliente");
        base.gridCadastro_PreRender(sender, e);

        ASPxDateEdit textDataInicio = popupFiltro.FindControl("textDataInicio") as ASPxDateEdit;
        ASPxDateEdit textDataFim = popupFiltro.FindControl("textDataFim") as ASPxDateEdit;
        ASPxSpinEdit btnEditCodigoClienteFiltro = popupFiltro.FindControl("btnEditCodigoClienteFiltro") as ASPxSpinEdit;

        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        Label labelFiltroHeader = gridCadastro.FindStatusBarTemplateControl("labelFiltroHeader") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");
        if (btnEditCodigoClienteFiltro != null && btnEditCodigoClienteFiltro.Text != "") {
            texto.Append(" Cliente: ").Append(btnEditCodigoClienteFiltro.Text);
        }
        if (textDataInicio.Text != "") {
            if (texto.Length > 0) {
                texto.Append(" |");
            }
            texto.Append(" Data Início >= ").Append(textDataInicio.Text.Substring(0, 10));
        }
        if (textDataFim.Text != "") {
            if (texto.Length > 0) {
                texto.Append(" |");
            }
            texto.Append(" Data Fim <= ").Append(textDataFim.Text.Substring(0, 10));
        }

        labelFiltro.Text = texto.ToString();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridPosicaoSwap_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "") //Usado apenas na hora q abre inicialmente a popup, para dar refresh nas posições
        {
            gridPosicaoSwap.DataBind();
        }
        else {
            selectedIndex = Convert.ToInt32(e.Parameters);
            InsertLiquidacao(selectedIndex);
            gridPosicaoSwap.DataBind();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridPosicaoSwap_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e) {
        UtilitarioGrid.SetaCoresGrid(e);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridPosicaoSwap_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) {
        if (e.Column.FieldName == "ValorAntecipacao") {
            ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;

            int idPosicao = Convert.ToInt32(e.GetListSourceFieldValue(LiquidacaoSwapMetadata.ColumnNames.IdPosicao));
            LiquidacaoSwap liquidacaoSwap = new LiquidacaoSwap();
            decimal valorAntecipado = liquidacaoSwap.RetornaTotalLiquidado(idPosicao, Convert.ToDateTime(textData.Text));

            e.Value = valorAntecipado;
        }
    }
}