﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Investidor;
using EntitySpaces.Interfaces;
using Financial.Security;
using System.Collections.Specialized;
using DevExpress.Web.ASPxTreeList;
using Financial.Web.Common;
using Financial.Web.Util;
using Financial.Security.Enums;
using System.Threading;
using DevExpress.XtraPrinting;
using System.IO;

public partial class CadastrosBasicos_PermissaoMenu : BasePage {

    #region Atributos Private
    private class ExportPermissaoMenu {
        public string nomeGrupo;

        public string NomeGrupo {
            get { return nomeGrupo; }
            set { nomeGrupo = value; }
        }

        public string nomeMenu;

        public string NomeMenu
        {
            get { return nomeMenu; }
            set { nomeMenu = value; }
        }

        public string descricaoMenu;

        public string DescricaoMenu {
            get { return descricaoMenu; }
            set { descricaoMenu = value; }
        }

        public string nivel;

        public string Nivel
        {
            get { return nivel; }
            set { nivel = value; }
        }

        public string permissao;

        public string Permissao {
            get { return permissao; }
            set { permissao = value; }
        }
    }

    private bool temAcesso;

    /// <summary>
    /// Indica se o treeview está expandido ou não
    /// </summary>
    private bool statusExpandido = true; // Inicialmente está expandido
    
    #endregion

    new protected void Page_Load(object sender, EventArgs e) {
        NameValueCollection nameValueCollection = new NameValueCollection();
        nameValueCollection.Add("securityTrimmingEnabled", "false");
        nameValueCollection.Add("siteMapFile", "Web.sitemap");

        FinancialSiteMapProvider financialSiteMapProvider = new FinancialSiteMapProvider();
        financialSiteMapProvider.Initialize("customProvider", nameValueCollection);
        financialSiteMapProvider.TestaAcesso = false;

        SiteMapDataSource1.Provider = financialSiteMapProvider;

        treeView1.Attributes.Add("onclick", "OnTreeClick(event);");

        // Seta Carregamento do Panel para a Table da TreeView
        LoadingPanel.ContainerElementID = "PanelForLoading";
        LoadingPanelCarregar.ContainerElementID = "PanelForLoading";

        //LoadingPanelCarregar.ContainerElementID = "treeView1";
        //LoadingPanelCarregar.ContainerElementID = "PanelForLoadingDiv";        
        
        if (Page.IsPostBack) {
            if (Session["StatusExpandido"] != null) {
                this.statusExpandido = Convert.ToBoolean(Session["StatusExpandido"]);
            }
        }
        else {
            Session.Remove("StatusExpandido");
        }
    }

    protected void EsDSGrupoUsuario_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        GrupoUsuarioCollection coll = new GrupoUsuarioCollection();

        // Se login != Master não mostra o Grupo
        //if (!new FinancialMembershipProvider().IsUserMaster(HttpContext.Current.User.Identity.Name)) {
        if (Convert.ToBoolean(Session["IsUserMaster"]) == false) {
            coll.Query.Where(coll.Query.IdGrupo != 0);        
        }
               
        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
       
        coll.LoadAll();
        
        e.Collection = coll;

        if (e.Collection.Count > 0) {
            dropGrupoUsuario.SelectedIndex = 0;
        }
    }
  
    protected void treeView1_TreeNodeDataBound(object sender, TreeNodeEventArgs e) {
        if (dropGrupoUsuario.SelectedIndex != -1) {
            SiteMapNode siteMapNode = (SiteMapNode)e.Node.DataItem;

            int idGrupo = Convert.ToInt32(dropGrupoUsuario.SelectedItem.Value);
            int idMenu = Convert.ToInt32(siteMapNode["menuID"]);

            e.Node.Value = idMenu.ToString();

            e.Node.NavigateUrl = "javascript:void(0);";

            PermissaoMenu permissaoMenu = new PermissaoMenu();
            if (permissaoMenu.LoadByPrimaryKey(idGrupo, idMenu)) {
                e.Node.Checked = permissaoMenu.PermissaoLeitura == "S";
            }
        }
    }

    protected void btnOK_Click(object sender, EventArgs e) {
        if (dropGrupoUsuario.SelectedIndex != -1) {
            SalvarAlteracoes(treeView1.Nodes);
        }
    }

    private void SalvarAlteracoes(TreeNodeCollection treeNodeCollection) {
        int idGrupo = Convert.ToInt32(dropGrupoUsuario.SelectedItem.Value);

        foreach (TreeNode treeNode in treeNodeCollection) {
            int idMenu = Convert.ToInt32(treeNode.Value);
            PermissaoMenu permissaoMenu = new PermissaoMenu();

            if (treeNode.ChildNodes.Count == 0) 
            {
                if (permissaoMenu.LoadByPrimaryKey(idGrupo, idMenu)) {
                    permissaoMenu.PermissaoLeitura = treeNode.Checked ? "S" : "N";
                    permissaoMenu.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de PermissaoMenu - Operacao: Update PermissaoMenu: " + idGrupo + "; " + idMenu + UtilitarioWeb.ToString(permissaoMenu),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
                else
                {
                    this.SalvaNovo("N", "N", "N", treeNode.Checked ? "S" : "N", idMenu, idGrupo);
                }
            }
            else {
                this.temAcesso = false;
                TestaAcessoParent(treeNode.ChildNodes);
                bool temPermissao = this.temAcesso;

                if (permissaoMenu.LoadByPrimaryKey(idGrupo, idMenu))
                {
                    permissaoMenu.PermissaoLeitura = temPermissao ? "S" : "N";
                    permissaoMenu.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de PermissaoMenu - Operacao: Update PermissaoMenu: " + idGrupo + "; " + idMenu + UtilitarioWeb.ToString(permissaoMenu),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
                else
                {
                    this.SalvaNovo("N", "N", "N", temPermissao ? "S" : "N", idMenu, idGrupo);                    
                }

                SalvarAlteracoes(treeNode.ChildNodes);
            }
        }

    }

    private void SalvaNovo(string permissaoAlteracao, string permissaoExclusao, string permissaoInclusao, string permissaoLeitura, int idMenu, int idGrupo)
    {
        PermissaoMenu permissaoMenu = new PermissaoMenu();
        permissaoMenu.PermissaoAlteracao = permissaoAlteracao;
        permissaoMenu.PermissaoExclusao = permissaoExclusao;
        permissaoMenu.PermissaoInclusao = permissaoInclusao;
        permissaoMenu.IdGrupo = idGrupo;
        permissaoMenu.IdMenu = idMenu;
        permissaoMenu.PermissaoLeitura = permissaoLeitura;
        permissaoMenu.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de PermissaoMenu - Operacao: Insert PermissaoMenu: " + idGrupo + "; " + idMenu + UtilitarioWeb.ToString(permissaoMenu),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    private void TestaAcessoParent(TreeNodeCollection treeNodeCollection) {
        foreach (TreeNode treeNode in treeNodeCollection) {
            if (treeNode.ChildNodes.Count == 0) {
                if (treeNode.Checked) {
                    this.temAcesso = true;
                    break;
                }
            }
            else {
                TestaAcessoParent(treeNode.ChildNodes);
            }
        }
    }

    protected void dropGrupoUsuario_SelectedIndexChanged(object sender, EventArgs e) {
        treeView1.DataBind();        
        // quando mudar o grupo a treeview fica expandida
        Session["StatusExpandido"] = true;
    }

    protected void btnCancel_Click(object sender, EventArgs e) {
        treeView1.DataBind();
    }

    /// <summary>
    /// Lista de Exportação do Menu
    /// </summary>
    /// <returns></returns>
    private List<ExportPermissaoMenu> ExportaPermissaoMenu()
    {
        List<ExportPermissaoMenu> lista = new List<ExportPermissaoMenu>();

        SiteMapNodeCollection pages = SiteMapDataSource1.Provider.RootNode.GetAllNodes();

        PermissaoMenuCollection permissaoMenuCollection = new PermissaoMenuCollection();
        permissaoMenuCollection.Query.Where(permissaoMenuCollection.Query.IdGrupo != 0);
        permissaoMenuCollection.Query.OrderBy(permissaoMenuCollection.Query.IdGrupo.Ascending, permissaoMenuCollection.Query.IdMenu.Ascending);
        //
        permissaoMenuCollection.Query.Load();

        //essas variáveis servem para montar a hierarquia de item das telas
        Dictionary<int,int> ContaNiveis = new Dictionary<int,int>();
        int nivelAtual = 0;
        int itemAtual = 0;
        Dictionary<string,int> NivelMenu = new Dictionary<string,int>();
        bool primeiroItem = true;
        bool sobeNivel = false;
        int idGrupoAnterior = 0;

        int i = 0;
        while (i < permissaoMenuCollection.Count)
        {
            PermissaoMenu permissaoMenu = permissaoMenuCollection[i];

            int idMenuPermissao = permissaoMenu.IdMenu.Value;
            int idGrupo = permissaoMenu.IdGrupo.Value;
            if (idGrupo != idGrupoAnterior)
            {
                primeiroItem = true;
                idGrupoAnterior = idGrupo;
            }

            GrupoUsuario grupoUsuario = new GrupoUsuario();
            grupoUsuario.LoadByPrimaryKey(idGrupo);

            int j = 0;
            while (j < pages.Count)
            {                
                while (idGrupoAnterior == idGrupo && j < pages.Count && i < permissaoMenuCollection.Count)
                {
                    SiteMapNode siteMapNode = pages[j];

                    int idMenu = Convert.ToInt32(siteMapNode["menuid"]);
                    string title = siteMapNode.Title;
                    string description = siteMapNode.Description;

                    if (idMenu == permissaoMenu.IdMenu.Value)
                    {
                        if (primeiroItem)
                        {
                            ContaNiveis.Clear();
                            NivelMenu.Clear();
                            itemAtual = 0;
                            nivelAtual = 0;                        
                            ContaNiveis.Add(nivelAtual, itemAtual);
                            NivelMenu.Add(siteMapNode.ParentNode.Title, nivelAtual);
                            primeiroItem = false;
                        }

                        if (NivelMenu.ContainsKey(siteMapNode.ParentNode.Title))
                        {
                            nivelAtual = NivelMenu[siteMapNode.ParentNode.Title];
                        }
                        else
                        {
                            if (NivelMenu.ContainsKey(siteMapNode.ParentNode.ParentNode.Title))
                            {
                                int nivelPai = NivelMenu[siteMapNode.ParentNode.ParentNode.Title] + 1;
                                NivelMenu.Add(siteMapNode.ParentNode.Title, nivelPai + 1);
                                nivelAtual = nivelPai + 1;
                                ContaNiveis[nivelAtual] = 0;
                            }
                        }
                        
                        if (!ContaNiveis.ContainsKey(nivelAtual))
                            ContaNiveis.Add(nivelAtual, 0);
                        else
                        {
                            itemAtual = ContaNiveis[nivelAtual] + 1;
                            ContaNiveis[nivelAtual] = itemAtual;
                        }

                        if (siteMapNode.HasChildNodes)
                        {
                            if (nivelAtual == 0)
                                description = "Ítem de Menu";
                            else
                                description = "Ítem de Submenu";
                            if (!NivelMenu.ContainsKey(siteMapNode.Title))
                            {
                                NivelMenu.Add(siteMapNode.Title, nivelAtual + 1);
                            }
                            if (ContaNiveis.ContainsKey(nivelAtual + 1))
                                ContaNiveis[nivelAtual + 1] = 0;
                            else
                            {
                                ContaNiveis.Add(nivelAtual + 1, 0);
                            }
                            sobeNivel = true;
                        }

                        string nivelItem = "";
                        for (int c = 0; c <= nivelAtual; c++)
                        {
                            nivelItem += ContaNiveis[c].ToString() + "."; 
                        }
                        nivelItem = nivelItem.Substring(0, nivelItem.Length - 1);
                        
                        if (sobeNivel)
                        {
                            sobeNivel = false;
                            nivelAtual += 1;
                        }

                        ExportPermissaoMenu exportPermissaoMenu = new ExportPermissaoMenu();
                        exportPermissaoMenu.nomeMenu = title;
                        exportPermissaoMenu.descricaoMenu = description;
                        exportPermissaoMenu.nomeGrupo = grupoUsuario.Descricao;
                        exportPermissaoMenu.permissao = permissaoMenu.PermissaoLeitura;
                        exportPermissaoMenu.nivel = nivelItem;

                        lista.Add(exportPermissaoMenu);                        
                    }

                    j++;
                }
                permissaoMenu = permissaoMenuCollection[i];
                idGrupo = permissaoMenu.IdGrupo.Value;
                i++;
            }
        }

        return lista;
    }
    
    protected void callbackExporta_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";

        MemoryStream ms = new MemoryStream();
        //
        List<ExportPermissaoMenu> l = this.ExportaPermissaoMenu();
        //
        XlsExportOptions a = new XlsExportOptions();
        a.Suppress256ColumnsWarning = true;
        this.gridExportacao1.DataSource = l;
        this.gridExport.WriteXls(ms, a);
        
        // Salva Memory Stream do Arquivo Excel na Session                
        Session["streamArquivo"] = ms;

        string newLocation = HttpContext.Current.Request.Url.AbsolutePath.Replace("PermissaoMenu.aspx", "ExportacaoPermissaoMenu.aspx");
        e.Result = "redirect:" + newLocation;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnColapsaExpande_Click(object sender, EventArgs e) {
        if (this.statusExpandido) {
            this.treeView1.CollapseAll();
        }
        else {
            this.treeView1.ExpandAll();
        }
        this.statusExpandido = !this.statusExpandido;

        // Salva na sessão para posterior uso
        Session["StatusExpandido"] = this.statusExpandido;
    }
}