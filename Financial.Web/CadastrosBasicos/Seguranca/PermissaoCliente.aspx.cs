﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;

using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;

using Financial.Security;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Common.Enums;

using DevExpress.Web;
using Financial.Web.Common;
using Financial.InvestidorCotista;

using System.Collections.Generic;
using Financial.Util;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Fundo;
using Financial.CRM;

public partial class CadastrosBasicos_PermissaoCliente : ConsultaBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        base.Page_Load(sender, e);
        //
        this.PersonalizaGridConsulta();
    }

    /// <summary>
    /// Configura Grid de Consulta
    /// </summary>
    private void PersonalizaGridConsulta() {
        // Enable ViewState
        this.gridConsulta.EnableViewState = true;
        
        // Tamanho da barra Lateral
        this.gridConsulta.Settings.VerticalScrollableHeight = 340;
    }

    #region DataSources

    /// <summary>
    /// Carrega o Grid de Cliente
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ClienteCollection coll = new ClienteCollection();

        // Se tem Usuario
        if (this.dropUsuario.SelectedIndex > -1) 
        { // Se escolheu Usuario            
            coll.Query.Select(coll.Query.IdCliente, coll.Query.Apelido, "<'N' as Permissao >");
            coll.Query.Where(coll.Query.TipoControle.NotEqual((byte)TipoControleCliente.ApenasCotacao));
            coll.Query.Load();

            // Para cada Cotista/Usuario verifica se tem permissão                                  
            for (int i = 0; i < coll.Count; i++) {
                PermissaoCliente p = new PermissaoCliente();
                if (p.LoadByPrimaryKey(Convert.ToInt32(dropUsuario.SelectedItem.Value), coll[i].IdCliente.Value)) {
                    coll[i].SetColumn("Permissao", "Sim");
                }
                else {
                    coll[i].SetColumn("Permissao", "Não");
                }
            }

            coll.Sort = "Permissao DESC, Apelido ASC";
        }
        //               
        e.Collection = coll;
    }

    /// <summary>
    /// Carrega o Combo de Usuário
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSUsuario_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        UsuarioCollection coll = new UsuarioCollection();
        coll.Query.Select(coll.Query.IdUsuario, coll.Query.Login.Trim());
        if (Financial.WebConfigConfiguration.WebConfig.AppSettings.Cliente.ToUpper() == "PORTOPAR")
        {
            coll.Query.Where(coll.Query.Email.NotEqual("usuario@hide.com"));
        }
        coll.Query.OrderBy(coll.Query.Login.Ascending);
        coll.Query.Load();

        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";

        // Se Usuario for vazio
        if (this.dropUsuario.SelectedIndex == -1) {
            e.Result = "Escolha o Usuário";
            return;
        }
        else {
            List<object> keyValuesId = this.gridConsulta.GetSelectedFieldValues(ClienteMetadata.ColumnNames.IdCliente);
            if (keyValuesId.Count == 0) {
                e.Result = "Escolha um Cliente";
                return;
            }
        }
    }

    private bool IsUsuarioTemplate(string login)
    {
        TemplateCotistaCollection templates = new TemplateCotistaCollection();
        templates.LoadAll();

        if (templates.Count == 0)
        {
            return false;
        }

        Cotista template = new Cotista();
        template.LoadByPrimaryKey(templates[0].IdCotista.Value);
        
        //Encontrar usuario associado ao cotista template
        string loginTemplate = RetornaLoginAssociado(template.IdCotista.Value);
        bool isUsuarioPadrao = login == loginTemplate;
        return isUsuarioPadrao;
    }

    private bool IsPortoPar()
    {
        return Financial.WebConfigConfiguration.WebConfig.AppSettings.Cliente.ToUpper() == "PORTOPAR";
    }

    private string RetornaLoginAssociado(int idCotista)
    {
        PermissaoCotistaQuery permissaoCotistaQuery = new PermissaoCotistaQuery("P");
        UsuarioQuery usuarioQuery = new UsuarioQuery("U");
        usuarioQuery.InnerJoin(permissaoCotistaQuery).On(permissaoCotistaQuery.IdUsuario == usuarioQuery.IdUsuario);
        usuarioQuery.Where(permissaoCotistaQuery.IdCotista.Equal(idCotista));
        usuarioQuery.Where(usuarioQuery.TipoTrava.Equal((byte)TipoTravaUsuario.TravaCotista));

        UsuarioCollection usuarioCollection = new UsuarioCollection();
        usuarioCollection.Load(usuarioQuery);

        string login = "";
        if (usuarioCollection.Count > 0)
        {
            login = usuarioCollection[0].Login.ToString();
        }

        return login;
    }

    private void AdicionaPermissaoUsuarioPadrao(UsuarioCollection usuarioPadraoCol, int idCliente, PermissaoClienteCollection permissaoClienteCol)
    {
        foreach (Usuario usuario in usuarioPadraoCol)
        {
            PermissaoCliente p = permissaoClienteCol.AddNew();
            p.IdUsuario = usuario.IdUsuario.Value;
            p.IdCliente = idCliente;
        }
    }

    private void RemovePermissaoUsuarioPadrao(UsuarioCollection usuarioPadraoCol, int idCliente, PermissaoClienteCollection permissaoClienteCol)
    {
        foreach (Usuario usuario in usuarioPadraoCol)
        {
            PermissaoCliente p = permissaoClienteCol.AddNew();
            p.IdUsuario = usuario.IdUsuario.Value;
            p.IdCliente = idCliente;

            p.AcceptChanges();
        }
    }

    private UsuarioCollection BuscaUsuarioPadraoCol(int idUsuarioIgnorar)
    {
        UsuarioCollection usuarioPadraoCol = new UsuarioCollection();
        usuarioPadraoCol.Query.Where(usuarioPadraoCol.Query.IdUsuario.NotEqual(idUsuarioIgnorar),
            usuarioPadraoCol.Query.Email == "usuario@hide.com");
        usuarioPadraoCol.Load(usuarioPadraoCol.Query);
        return usuarioPadraoCol;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// 
    protected void gridConsulta_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e) {

        UsuarioCollection usuarioPadraoCol = new UsuarioCollection();

        if (e.Parameters == "btnAtivar") {           
            #region Adiciona Permissao

            List<object> keyValuesId = this.gridConsulta.GetSelectedFieldValues(ClienteMetadata.ColumnNames.IdCliente);
            
            int idUsuario = Convert.ToInt32(this.dropUsuario.SelectedItem.Value);
            PermissaoClienteCollection adicionaPermissao = new PermissaoClienteCollection();
            PermissaoClienteCollection deletaPermissao = new PermissaoClienteCollection();

            Usuario usuario = new Usuario();
            usuario.LoadByPrimaryKey(idUsuario);

            bool propagaPermissaoUsuarioPadrao = false;
            
            if (IsPortoPar())
            {
                propagaPermissaoUsuarioPadrao = IsUsuarioTemplate(usuario.Login);
                if (propagaPermissaoUsuarioPadrao)
                {
                    usuarioPadraoCol = BuscaUsuarioPadraoCol(idUsuario);
                }
            }

            for (int i = 0; i < keyValuesId.Count; i++) {
                int idCliente = Convert.ToInt32(keyValuesId[i]);
                //
                string permissao = (string)this.gridConsulta.GetRowValuesByKeyValue(idCliente, "Permissao");

                // Adiciona na permissão
                if (permissao.Substring(0, 1) == "N") {
                    PermissaoCliente p = adicionaPermissao.AddNew();
                    p.IdUsuario = idUsuario;
                    p.IdCliente = idCliente;

                    //Propagar se tiver usuario padrao
                    if (propagaPermissaoUsuarioPadrao)
                    {
                        AdicionaPermissaoUsuarioPadrao(usuarioPadraoCol, idCliente, adicionaPermissao);
                    }

                    //Se existe carteira e cotistas, adiciona permissoes para este usuario
                    if (chkPropagaCotista.Checked)
                    {
                        usuario.AdicionaPermissaoCotistas(idCliente);
                    }
                }
            }
            
            //
            PermissaoClienteCollection adicionaPermissaoClone = (PermissaoClienteCollection)Utilitario.Clone(adicionaPermissao);
            //

            adicionaPermissao.Save();

           foreach (PermissaoCliente p in adicionaPermissaoClone) {

                #region Log do Processo
                HistoricoLog historicoLog = new HistoricoLog();
                historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                "Cadastro de PermissaoCliente - Operacao: Update PermissaoCliente: " + p.IdCliente + " ;" + p.IdUsuario + UtilitarioWeb.ToString(p),
                                                HttpContext.Current.User.Identity.Name,
                                                UtilitarioWeb.GetIP(Request),
                                                "",
                                                HistoricoLogOrigem.Outros);
                #endregion
            }

            #endregion
        }
        else if (e.Parameters == "btnDesativar") {
            #region Deleta Permissao

            List<object> keyValuesId = this.gridConsulta.GetSelectedFieldValues(ClienteMetadata.ColumnNames.IdCliente);
            
            int idUsuario = Convert.ToInt32(this.dropUsuario.SelectedItem.Value);
            PermissaoClienteCollection adicionaPermissao = new PermissaoClienteCollection();
            PermissaoClienteCollection deletaPermissao = new PermissaoClienteCollection();

            Usuario usuario = new Usuario();
            usuario.LoadByPrimaryKey(idUsuario);

            bool propagaPermissaoUsuarioPadrao = false;
            if (IsPortoPar())
            {
                propagaPermissaoUsuarioPadrao = IsUsuarioTemplate(usuario.Login);
                if (propagaPermissaoUsuarioPadrao)
                {
                    usuarioPadraoCol = BuscaUsuarioPadraoCol(idUsuario);
                }
            }

            for (int i = 0; i < keyValuesId.Count; i++) {
                int idCliente = Convert.ToInt32(keyValuesId[i]);
                //
                string permissao = (string)this.gridConsulta.GetRowValuesByKeyValue(idCliente, "Permissao");

                // Deleta na permissão
                if (permissao.Substring(0, 1) == "S") {
                    PermissaoCliente p = deletaPermissao.AddNew();
                    p.IdUsuario = idUsuario;
                    p.IdCliente = idCliente;
                    p.AcceptChanges();
                }

                //Propagar se tiver usuario padrao
                if (propagaPermissaoUsuarioPadrao)
                {
                    RemovePermissaoUsuarioPadrao(usuarioPadraoCol, idCliente, deletaPermissao);
                }
            }

            //
            PermissaoClienteCollection deletaPermissaoClone = (PermissaoClienteCollection)Utilitario.Clone(deletaPermissao);
            //
            deletaPermissao.MarkAllAsDeleted();
            deletaPermissao.Save();

            foreach (PermissaoCliente p in deletaPermissaoClone) {

                #region Log do Processo
                HistoricoLog historicoLog = new HistoricoLog();
                historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                "Cadastro de PermissaoCliente - Operacao: Delete PermissaoCliente: " + p.IdCliente + " ;" + p.IdUsuario + UtilitarioWeb.ToString(p),
                                                HttpContext.Current.User.Identity.Name,
                                                UtilitarioWeb.GetIP(Request),
                                                "",
                                                HistoricoLogOrigem.Outros);
                #endregion
            }

            #endregion
        }
        
        gridConsulta.DataBind();        
        gridConsulta.Selection.UnselectAll();
    }
}