﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Investidor;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Web.Common;

public partial class CadastrosBasicos_GrupoUsuario : CadastroBasePage {

    protected void EsDSGrupoUsuario_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        GrupoUsuarioCollection coll = new GrupoUsuarioCollection();

        coll.Query.Where(coll.Query.IdGrupo != 0);
        //
        coll.Query.OrderBy(coll.Query.TipoPerfil.Ascending, coll.Query.Descricao.Ascending);

        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        GrupoUsuario grupoUsuario = new GrupoUsuario();
        int idGrupo = (Int32)e.Keys[0];

        if (grupoUsuario.LoadByPrimaryKey(idGrupo)) {
            grupoUsuario.Descricao = Convert.ToString(e.NewValues["Descricao"]);
            grupoUsuario.TipoPerfil = Convert.ToByte(e.NewValues["TipoPerfil"]);
            grupoUsuario.Front = Convert.ToByte(e.NewValues["Front"]);
            grupoUsuario.CarteiraSimulada = Convert.ToString(e.NewValues["CarteiraSimulada"]);
            
            grupoUsuario.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de GrupoUsuario - Operacao: Update GrupoUsuario: " + idGrupo + UtilitarioWeb.ToString(grupoUsuario),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        //Só salva se existir algum outro grupo já cadastrado de onde se possa extrair os ids de menus
        GrupoUsuarioCollection grupoUsuarioCollection = new GrupoUsuarioCollection();
        //
        grupoUsuarioCollection.Query.Where(grupoUsuarioCollection.Query.IdGrupo != 0);
        
        if (grupoUsuarioCollection.LoadAll()) {
            int idGrupoTemplate = grupoUsuarioCollection[0].IdGrupo.Value;

            GrupoUsuario grupoUsuario = new GrupoUsuario();

            grupoUsuario.Descricao = Convert.ToString(e.NewValues["Descricao"]);
            grupoUsuario.TipoPerfil = Convert.ToByte(e.NewValues["TipoPerfil"]);
            grupoUsuario.Front = Convert.ToByte(e.NewValues["Front"]);
            grupoUsuario.CarteiraSimulada = Convert.ToString(e.NewValues["CarteiraSimulada"]);
            grupoUsuario.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de GrupoUsuario - Operacao: Insert GrupoUsuario: " + grupoUsuario.IdGrupo + UtilitarioWeb.ToString(grupoUsuario),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion

            //Salva o default de permissionamento de menu (tudo desmarcado) para o novo grupo
            PermissaoMenuCollection permissaoMenuCollection = new PermissaoMenuCollection();
            permissaoMenuCollection.Query.Where(permissaoMenuCollection.Query.IdGrupo.Equal(idGrupoTemplate));
            permissaoMenuCollection.Query.Load();

            foreach (PermissaoMenu permissaoMenu in permissaoMenuCollection) {
                int idMenu = permissaoMenu.IdMenu.Value;

                PermissaoMenu permissaoMenuNovo = new PermissaoMenu();
                permissaoMenuNovo.PermissaoAlteracao = "N";
                permissaoMenuNovo.PermissaoExclusao = "N";
                permissaoMenuNovo.PermissaoInclusao = "N";
                permissaoMenuNovo.PermissaoLeitura = grupoUsuario.TipoPerfil.Value == (byte)TipoPerfilGrupo.Administrador ? "S" : "N";
                //
                permissaoMenuNovo.IdGrupo = grupoUsuario.IdGrupo.Value;
                permissaoMenuNovo.IdMenu = idMenu;
                permissaoMenuNovo.Save();
            }
        }
        //

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdGrupo");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idGrupo = Convert.ToInt32(keyValuesId[i]);

                GrupoUsuario grupoUsuario = new GrupoUsuario();
                if (grupoUsuario.LoadByPrimaryKey(idGrupo))
                {
                    UsuarioCollection usuarioCollection = new UsuarioCollection();
                    usuarioCollection.Query.Where(usuarioCollection.Query.IdGrupo.Equal(idGrupo));
                    if (usuarioCollection.Query.Load())
                    {
                        throw new Exception("Grupo de usuário " + grupoUsuario.Descricao + " não pode ser excluído por ter usuários relacionados.");
                    }

                    //
                    GrupoUsuario grupoUsuarioClone = (GrupoUsuario)Utilitario.Clone(grupoUsuario);
                    //

                    grupoUsuario.MarkAsDeleted();
                    grupoUsuario.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de GrupoUsuario - Operacao: Delete GrupoUsuario: " + idGrupo + UtilitarioWeb.ToString(grupoUsuarioClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }
}