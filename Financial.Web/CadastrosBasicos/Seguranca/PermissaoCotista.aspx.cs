﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;

using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;

using Financial.Security;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Common.Enums;

using DevExpress.Web;
using Financial.Web.Common;
using Financial.InvestidorCotista;
using System.Collections.Generic;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Util;

public partial class CadastrosBasicos_PermissaoCotista : ConsultaBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        base.Page_Load(sender, e);
        //
        this.PersonalizaGridConsulta();           
    }

    /// <summary>
    /// Configura Grid de Consulta
    /// </summary>
    private void PersonalizaGridConsulta() {
        // Enable ViewState
        this.gridConsulta.EnableViewState = true; // Necessario para Check Multiplo do grid

        // Tamanho da barra Lateral
        this.gridConsulta.Settings.VerticalScrollableHeight = 340;
    }

    #region DataSources
    
    /// <summary>
    /// Carrega o Grid de Cotista 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSCotista_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CotistaCollection coll = new CotistaCollection();

        // Se tem Usuario
        if (this.dropUsuario.SelectedIndex > -1) { // Se escolheu Usuario            
            coll.Query.Select(coll.Query.IdCotista, coll.Query.Apelido, "<'N' as Status >");
            coll.Query.Load();

            // Para cada Cotista/Usuario verifica se tem permissão                                  
            for (int i = 0; i < coll.Count; i++) {
                PermissaoCotista p = new PermissaoCotista();
                if (p.LoadByPrimaryKey(Convert.ToInt32(dropUsuario.SelectedItem.Value), coll[i].IdCotista.Value)) {
                    coll[i].SetColumn("Status", "Sim");
                }
                else {
                    coll[i].SetColumn("Status", "Não");
                }
            }

            coll.Sort = "Status DESC, Apelido ASC";
        }        
        //               
        e.Collection = coll;
    }
    
    /// <summary>
    /// Carrega o Combo de Usuário
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSUsuario_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        UsuarioCollection coll = new UsuarioCollection();
        coll.Query.Select(coll.Query.IdUsuario, coll.Query.Login.Trim());
        coll.Query.OrderBy(coll.Query.Login.Ascending);
        coll.Query.Load();

        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";

        // Se Usuario for vazio
        if (this.dropUsuario.SelectedIndex == -1) {
            e.Result = "Escolha o Usuário";
            return;
        }
        else {
            List<object> keyValuesId = this.gridConsulta.GetSelectedFieldValues(CotistaMetadata.ColumnNames.IdCotista);
            if (keyValuesId.Count == 0) {
                e.Result = "Escolha um Cotista";
                return;                
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridConsulta_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e) {

        if (e.Parameters == "btnAtivar") {
            List<object> keyValuesId = this.gridConsulta.GetSelectedFieldValues(CotistaMetadata.ColumnNames.IdCotista);
            
            int idUsuario = Convert.ToInt32(this.dropUsuario.SelectedItem.Value);
            PermissaoCotistaCollection adicionaPermissao = new PermissaoCotistaCollection();
            PermissaoCotistaCollection deletaPermissao = new PermissaoCotistaCollection();

            #region Adiciona Permissao
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idCotista = Convert.ToInt32(keyValuesId[i]);

                string permissao = (string)this.gridConsulta.GetRowValuesByKeyValue(idCotista, "Status");
                
                // Adiciona na permissão
                if(permissao.Substring(0,1) == "N") {
                    PermissaoCotista p = adicionaPermissao.AddNew();
                    p.IdUsuario = idUsuario;
                    p.IdCotista = idCotista;
                }

                //Adiciona permissão para as carteiras associadas a este cotista
                PermissaoCliente permissaoCliente = new PermissaoCliente();
                permissaoCliente.GeraPermissaoCarteira(idUsuario, idCotista);
            }

            //
            PermissaoCotistaCollection adicionaPermissaoClone = (PermissaoCotistaCollection)Utilitario.Clone(adicionaPermissao);
            //
            //
            adicionaPermissao.Save();

            foreach (PermissaoCotista p in adicionaPermissaoClone) {

                #region Log do Processo
                HistoricoLog historicoLog = new HistoricoLog();
                historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                "Cadastro de PermissaoCotista - Operacao: Update PermissaoCotista: " + p.IdCotista + " ;" + p.IdUsuario + UtilitarioWeb.ToString(p),
                                                HttpContext.Current.User.Identity.Name,
                                                UtilitarioWeb.GetIP(Request),
                                                "",
                                                HistoricoLogOrigem.Outros);
                #endregion
            }
           
            #endregion

        }
        else if (e.Parameters == "btnDesativar") {

            List<object> keyValuesId = this.gridConsulta.GetSelectedFieldValues(CotistaMetadata.ColumnNames.IdCotista);
            
            int idUsuario = Convert.ToInt32(this.dropUsuario.SelectedItem.Value);
            PermissaoCotistaCollection adicionaPermissao = new PermissaoCotistaCollection();
            PermissaoCotistaCollection deletaPermissao = new PermissaoCotistaCollection();

            #region Deleta Permissao
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idCotista = Convert.ToInt32(keyValuesId[i]);

                string permissao = (string)this.gridConsulta.GetRowValuesByKeyValue(idCotista, "Status");

                // Deleta na permissão                
                if (permissao.Substring(0, 1) == "S") {
                    PermissaoCotista p = deletaPermissao.AddNew();
                    p.IdUsuario = idUsuario;
                    p.IdCotista = idCotista;
                    p.AcceptChanges();
                }
            }
            //
            PermissaoCotistaCollection deletaPermissaoClone = (PermissaoCotistaCollection)Utilitario.Clone(deletaPermissao);
            //

            deletaPermissao.MarkAllAsDeleted();
            deletaPermissao.Save();

            foreach (PermissaoCotista p in deletaPermissaoClone) {

                #region Log do Processo
                HistoricoLog historicoLog = new HistoricoLog();
                historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                "Cadastro de PermissaoCotista - Operacao: Delete PermissaoCotista: " + p.IdCotista + " ;" + p.IdUsuario + UtilitarioWeb.ToString(p),
                                                HttpContext.Current.User.Identity.Name,
                                                UtilitarioWeb.GetIP(Request),
                                                "",
                                                HistoricoLogOrigem.Outros);
                #endregion
            }

            #endregion
        }

        gridConsulta.DataBind();
        gridConsulta.Selection.UnselectAll();
    }
}