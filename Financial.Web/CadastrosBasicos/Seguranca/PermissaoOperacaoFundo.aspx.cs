﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;

using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;

using Financial.Security;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Common.Enums;

using DevExpress.Web;
using Financial.Web.Common;
using Financial.InvestidorCotista;
using System.Collections.Generic;
using Financial.Util;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Fundo;

public partial class CadastrosBasicos_PermissaoOperacaoFundo : ConsultaBasePage
{
    new protected void Page_Load(object sender, EventArgs e)
    {
        base.Page_Load(sender, e);
        //
        this.PersonalizaGridConsulta();
    }

    /// <summary>
    /// Configura Grid de Consulta
    /// </summary>
    private void PersonalizaGridConsulta()
    {
        // Enable ViewState
        this.gridConsulta.EnableViewState = true;

        // Tamanho da barra Lateral
        this.gridConsulta.Settings.VerticalScrollableHeight = 340;
    }

    #region DataSources

    /// <summary>
    /// Carrega o Grid de Cliente
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSFundo_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {

        CarteiraCollection coll = new CarteiraCollection();
        CarteiraQuery carteiraQuery = new CarteiraQuery("A");
        ClienteQuery clienteQuery = new ClienteQuery("C");


        // Se tem Cotista
        if (this.dropCotista.SelectedIndex > -1)
        { // Se escolheu Cotista            
            carteiraQuery.Select(carteiraQuery.IdCarteira.As("IdFundo"), carteiraQuery.Apelido.As("ApelidoFundo"),
                carteiraQuery.Nome.As("NomeFundo"), "<'N' as Permissao >", "<'' as Email >", "<'' as SituacaoEnvio >", carteiraQuery);

            carteiraQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == carteiraQuery.IdCarteira);

            carteiraQuery.Where(clienteQuery.StatusAtivo.Equal(StatusAtivoCliente.Ativo),
                clienteQuery.IdTipo.In(TipoClienteFixo.Clube, TipoClienteFixo.Fundo, TipoClienteFixo.FDIC),
                clienteQuery.TipoControle.NotEqual((byte)TipoControleCliente.ApenasCotacao));

            coll.Load(carteiraQuery);

            // Para cada Cotista/Cotista verifica se tem permissão                                  
            for (int i = 0; i < coll.Count; i++)
            {
                PermissaoOperacaoFundo p = new PermissaoOperacaoFundo();
                if (p.LoadByPrimaryKey(Convert.ToInt32(dropCotista.SelectedItem.Value), Convert.ToInt32(coll[i].IdCarteira.Value)))
                {
                    coll[i].SetColumn("Permissao", "Sim");
                    coll[i].SetColumn("Email", p.Email);
                    coll[i].SetColumn("SituacaoEnvio", p.SituacaoEnvio);
                }
                else
                {
                    coll[i].SetColumn("Permissao", "Não");
                }
            }

            coll.Sort = "Permissao DESC, Apelido ASC";
        }
        //               
        e.Collection = coll;
    }

    /// <summary>
    /// Carrega o Combo de Usuário
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSCotista_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CotistaCollection coll = new CotistaCollection();
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.Query.Load();

        e.Collection = coll;
    }
    #endregion

    protected void callBackAlteraEmail_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        List<object> keyValuesId = this.gridConsulta.GetSelectedFieldValues("IdFundo");

        if (keyValuesId.Count == 0 || this.dropCotista.SelectedItem == null)
        {
            e.Result = "Nenhum fundo selecionado";
            return;
        }

        int idCotista = Convert.ToInt32(this.dropCotista.SelectedItem.Value);


        for (int i = 0; i < keyValuesId.Count; i++)
        {
            int idFundo = Convert.ToInt32(keyValuesId[i]);
            //
            string flagPermissao = (string)this.gridConsulta.GetRowValuesByKeyValue(idFundo, "Permissao");

            // Adiciona na permissão
            if (flagPermissao.Substring(0, 1) == "S")
            {
                PermissaoOperacaoFundo permissao = new PermissaoOperacaoFundo();
                permissao.LoadByPrimaryKey(idCotista, idFundo);
                if (textEmail.Text == "-")
                {
                    permissao.Email = "";
                }
                else if (!String.IsNullOrEmpty(textEmail.Text))
                {
                    permissao.Email = textEmail.Text;
                }
                
                if (dropSituacaoEnvio.SelectedIndex >= 0)
                {
                    permissao.SituacaoEnvio = Convert.ToByte(dropSituacaoEnvio.SelectedItem.Value);
                }

                permissao.Save();
            }
        }


        e.Result = "Processo executado com sucesso.";
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        // Se Cotista for vazio
        if (this.dropCotista.SelectedIndex == -1)
        {
            e.Result = "Escolha o Cotista";
            return;
        }
        else
        {
            List<object> keyValuesId = this.gridConsulta.GetSelectedFieldValues("IdFundo");
            if (keyValuesId.Count == 0)
            {
                e.Result = "Escolha um Fundo";
                return;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridConsulta_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {

        if (e.Parameters == "btnAtivar")
        {
            #region Adiciona Permissao

            List<object> keyValuesId = this.gridConsulta.GetSelectedFieldValues("IdFundo");

            int idCotista = Convert.ToInt32(this.dropCotista.SelectedItem.Value);
            PermissaoOperacaoFundoCollection adicionaPermissao = new PermissaoOperacaoFundoCollection();
            PermissaoOperacaoFundoCollection deletaPermissao = new PermissaoOperacaoFundoCollection();

            Cotista cotista = new Cotista();
            cotista.LoadByPrimaryKey(idCotista);

            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idFundo = Convert.ToInt32(keyValuesId[i]);
                //
                string permissao = (string)this.gridConsulta.GetRowValuesByKeyValue(idFundo, "Permissao");

                // Adiciona na permissão
                if (permissao.Substring(0, 1) == "N")
                {
                    PermissaoOperacaoFundo p = adicionaPermissao.AddNew();
                    p.IdCotista = idCotista;
                    p.IdFundo = idFundo;
                }
            }

            //
            PermissaoOperacaoFundoCollection adicionaPermissaoClone = (PermissaoOperacaoFundoCollection)Utilitario.Clone(adicionaPermissao);
            //

            adicionaPermissao.Save();

            foreach (PermissaoOperacaoFundo p in adicionaPermissaoClone)
            {

                #region Log do Processo
                HistoricoLog historicoLog = new HistoricoLog();
                historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                "Cadastro de PermissaoOperacaoFundo - Operacao: Update PermissaoOperacaoFundo: " + p.IdFundo + " ;" + p.IdCotista + UtilitarioWeb.ToString(p),
                                                HttpContext.Current.User.Identity.Name,
                                                UtilitarioWeb.GetIP(Request),
                                                "",
                                                HistoricoLogOrigem.Outros);
                #endregion
            }

            #endregion
        }
        else if (e.Parameters == "btnDesativar")
        {
            #region Deleta Permissao

            List<object> keyValuesId = this.gridConsulta.GetSelectedFieldValues("IdFundo");

            int idCotista = Convert.ToInt32(this.dropCotista.SelectedItem.Value);
            PermissaoOperacaoFundoCollection adicionaPermissao = new PermissaoOperacaoFundoCollection();
            PermissaoOperacaoFundoCollection deletaPermissao = new PermissaoOperacaoFundoCollection();

            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idFundo = Convert.ToInt32(keyValuesId[i]);
                //
                string permissao = (string)this.gridConsulta.GetRowValuesByKeyValue(idFundo, "Permissao");

                // Deleta na permissão
                if (permissao.Substring(0, 1) == "S")
                {
                    PermissaoOperacaoFundo p = deletaPermissao.AddNew();
                    p.IdCotista = idCotista;
                    p.IdFundo = idFundo;
                    p.AcceptChanges();
                }
            }

            //
            PermissaoOperacaoFundoCollection deletaPermissaoClone = (PermissaoOperacaoFundoCollection)Utilitario.Clone(deletaPermissao);
            //
            deletaPermissao.MarkAllAsDeleted();
            deletaPermissao.Save();

            foreach (PermissaoOperacaoFundo p in deletaPermissaoClone)
            {

                #region Log do Processo
                HistoricoLog historicoLog = new HistoricoLog();
                historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                "Cadastro de PermissaoOperacaoFundo - Operacao: Delete PermissaoOperacaoFundo: " + p.IdFundo + " ;" + p.IdCotista + UtilitarioWeb.ToString(p),
                                                HttpContext.Current.User.Identity.Name,
                                                UtilitarioWeb.GetIP(Request),
                                                "",
                                                HistoricoLogOrigem.Outros);
                #endregion
            }

            #endregion
        }

        gridConsulta.DataBind();
        gridConsulta.Selection.UnselectAll();
    }
}