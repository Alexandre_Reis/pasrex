﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PermissaoOperacaoFundo.aspx.cs"
    Inherits="CadastrosBasicos_PermissaoOperacaoFundo" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxlp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Permissão Operação Fundo</title>
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript"> 
document.onkeydown=onDocumentKeyDown;
var operacao = '';
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
        <dxcb:ASPxCallback ID="callBackAlteraEmail" runat="server" OnCallback="callBackAlteraEmail_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {            
            LoadingPanel.Hide();
            
            if (e.result != '') {
                alert(e.result);

                if (e.result == 'Processo executado com sucesso.') {                     
                    popupAlteraEmail.Hide();
                    gridConsulta.PerformCallback('btnRefresh');
                }                               
            }                                             
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {                         
            if (e.result != '') {            
                alert(e.result);                              
            }
            else {
                if (operacao == 'ativar') {
                    gridConsulta.PerformCallback('btnAtivar');
                }
                else if (operacao == 'desativar') {
                    gridConsulta.PerformCallback('btnDesativar');
                }
            }            
            operacao = '';
        }        
        " />
        </dxcb:ASPxCallback>
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <dxpc:ASPxPopupControl ID="popupAlteraEmail" AllowDragging="true" PopupElementID="popupAlteraEmail"
                                EnableClientSideAPI="True" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
                                CloseAction="CloseButton" Width="400" Left="250" Top="70" HeaderText="Alteração de Email"
                                runat="server" HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
                                <ContentCollection>
                                    <dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="labelEmail" runat="server" Text="Email:" />
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="textEmail" runat="server" CssClass="textNormal" MaxLength="200"
                                                        Text='<%#Eval("Email")%>'></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="labelSituacaoEnvio" runat="server" CssClass="labelNormal" Text="Evento Envio:"></asp:Label>
                                                </td>
                                                <td>
                                                    <dxe:ASPxComboBox ID="dropSituacaoEnvio" runat="server" ClientInstanceName="dropSituacaoEnvio"
                                                        ShowShadow="False" CssClass="dropDownListLongo" ValueType="System.String">
                                                        <items>
                                                                <dxe:ListEditItem Value="1" Text="Todos" />
                                                                <dxe:ListEditItem Value="2" Text="Distribuição" />
                                                                <dxe:ListEditItem Value="3" Text="Aprovação" />
                                                            </items>
                                                    </dxe:ASPxComboBox>
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="linkButton linkButtonNoBorder" style="margin-top: 20px">
                                            <asp:LinkButton ID="btnProcessaAlteracao" runat="server" Font-Overline="false" ForeColor="Black"
                                                CssClass="btnOK" OnClientClick="if (confirm('Tem certeza que quer realizar a atualização em lote?')==true) {
                                                                        LoadingPanel.Show();
                                                                        callBackAlteraEmail.SendCallback();
                                                                        
                                                                   }
                                                                   return false;
                                                                  ">
                                                <asp:Literal ID="Literal12" runat="server" Text="Processa Atualização" /><div>
                                                </div>
                                            </asp:LinkButton>
                                        </div>
                                    </dxpc:PopupControlContentControl>
                                </ContentCollection>
                            </dxpc:ASPxPopupControl>
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Permissão para Operação de Fundos" />
                            </div>
                            <div id="mainContent">
                                <table cellspacing="2" cellpadding="2" width="450">
                                    <tr>
                                        <td>
                                            <asp:Label ID="labelCotista" runat="server" CssClass="labelNormal" Text="Cotista:" />
                                        </td>
                                        <td width="200">
                                            <dxe:ASPxComboBox ID="dropCotista" runat="server" AutoPostBack="false" DataSourceID="EsDSCotista"
                                                ClientInstanceName="dropCotista" Width="200px" IncrementalFilteringMode="Contains"
                                                ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListLongo"
                                                TextField="Apelido" ValueField="IdCotista">
                                                <clientsideevents selectedindexchanged="function(s, e) { 
                        gridConsulta.PerformCallback('btnRefresh'); return false;  }" />
                                            </dxe:ASPxComboBox>
                                        </td>
                                        <td>
                                            <div class="linkButton linkButtonNoBorder" style="margin-top: 5px; margin-bottom: 5px;
                                                width: 300px">
                                                <asp:LinkButton ID="btnAtiva" runat="server" Font-Overline="false" CssClass="btnEdit"
                                                    OnClientClick="operacao='ativar'; if(!confirm('Atenção: Este usuário poderá acessar TODOS os fundos/carteiras selecionados. Deseja realmente prosseguir ?')){alert('Operação cancelada'); return false;} callbackErro.SendCallback(); return false;">
                                                    <asp:Literal ID="Literal10" runat="server" Text="Ativa" /><div>
                                                    </div>
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="btnDesativa" runat="server" Font-Overline="false" CssClass="btnPageDelete"
                                                    OnClientClick="operacao='desativar'; callbackErro.SendCallback(); return false;">
                                                    <asp:Literal ID="Literal1" runat="server" Text="Desativa" /><div>
                                                    </div>
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="btnAlteraEmail" runat="server" Font-Overline="false" CssClass="btnCard"
                                                    OnClientClick="popupAlteraEmail.ShowWindow(); return false;">
                                                    <asp:Literal ID="Literal11" runat="server" Text="Altera em Lote" /><div>
                                                    </div>
                                                </asp:LinkButton>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridConsulta" runat="server" EnableCallBacks="true" ClientInstanceName="gridConsulta"
                                        DataSourceID="EsDSFundo" KeyFieldName="IdFundo" Width="100%" OnCustomCallback="gridConsulta_CustomCallback">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="4%" ButtonType="Image" Visible="true" ShowClearFilterButton="True">
                                                <HeaderStyle HorizontalAlign="Center"/>
                                                <HeaderTemplate>
                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridConsulta);}" OnInit="CheckBoxSelectAll"/>
                                                </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="IdFundo" VisibleIndex="1" Width="15%" CellStyle-HorizontalAlign="left" />
                                            <dxwgv:GridViewDataTextColumn FieldName="ApelidoFundo" VisibleIndex="2" Width="40%" />
                                            <dxwgv:GridViewDataTextColumn FieldName="Permissao" Caption="Permissão" VisibleIndex="3"
                                                Width="10%" CellStyle-HorizontalAlign="center" />
                                            <dxwgv:GridViewDataTextColumn FieldName="Email" VisibleIndex="4" />
                                            <dxwgv:GridViewDataComboBoxColumn Caption="Evento Envio" FieldName="SituacaoEnvio"
                                                VisibleIndex="5">
                                                <EditFormSettings Visible="True" />
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="1" Text="Todos" />
                                                        <dxe:ListEditItem Value="2" Text="Distribuição" />
                                                        <dxe:ListEditItem Value="3" Text="Aprovação" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                        </Columns>
                                        <Settings VerticalScrollBarStyle="Virtual" />
                                        <Styles>
                                            <Header ImageSpacing="5px" SortingImageSpacing="5px" />
                                        </Styles>
                                        <SettingsCommandButton>
                                            <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                                        </SettingsCommandButton>
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <cc1:esDataSource ID="EsDSCotista" runat="server" OnesSelect="EsDSCotista_esSelect" />
        <cc1:esDataSource ID="EsDSFundo" runat="server" OnesSelect="EsDSFundo_esSelect" />
        <dxlp:ASPxLoadingPanel ID="LoadingPanel" runat="server" Text="Processando, aguarde..."
            ClientInstanceName="LoadingPanel" Modal="True" />
    </form>
</body>
</html>
