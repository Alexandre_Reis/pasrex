﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;

using System.Drawing;
using Financial.Investidor;
using Financial.Security;

using System.Collections.Specialized;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Web.Common;
using System.Text.RegularExpressions;

using System.IO;

public partial class CadastrosBasicos_Usuario : CadastroBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasPopupCliente = true;
        base.Page_Load(sender, e);

        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
                          new List<string>(new string[] { UsuarioMetadata.ColumnNames.StatusAtivo,
                                                          UsuarioMetadata.ColumnNames.StatusBloqueado
                                                        }));

    }

    #region DataSources
    protected void EsDSUsuario_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        UsuarioCollection coll = new UsuarioCollection();
        UsuarioQuery usuarioQuery = new UsuarioQuery("U");
        ClienteQuery clienteQuery = new ClienteQuery("C");
        LimiteOperacaoUsuarioQuery limiteOperacaoUsuarioQuery = new LimiteOperacaoUsuarioQuery("L");
        //
        usuarioQuery.Select(usuarioQuery, clienteQuery.Apelido.As("Apelido"), 
            limiteOperacaoUsuarioQuery.MaximoAplicacao, limiteOperacaoUsuarioQuery.MaximoResgate);

        usuarioQuery.LeftJoin(clienteQuery).On(usuarioQuery.IdCliente == clienteQuery.IdCliente);
        usuarioQuery.LeftJoin(limiteOperacaoUsuarioQuery).On(usuarioQuery.IdUsuario == limiteOperacaoUsuarioQuery.IdUsuario);
        //
        usuarioQuery.Where(usuarioQuery.IdGrupo != 0);

        if (Financial.WebConfigConfiguration.WebConfig.AppSettings.Cliente.ToUpper() == "PORTOPAR")
        {
            usuarioQuery.Where(usuarioQuery.Email.NotEqual("usuario@hide.com"));
        }
        //
        usuarioQuery.OrderBy(coll.Query.Nome.Ascending);
        //
        coll.Load(usuarioQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSGrupoUsuario_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        GrupoUsuarioCollection coll = new GrupoUsuarioCollection();

        coll.Query.Where(coll.Query.IdGrupo != 0);

        coll.Query.OrderBy(coll.Query.Descricao.Ascending);
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        DateTime dataDia;
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") {
                if (cliente.IsAtivo) {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoCliente(idCliente, HttpContext.Current.User.Identity.Name)) {
                        nome = cliente.str.Apelido;
                        dataDia = cliente.DataDia.Value;
                        //

                        /*nome = this.gridCadastro.IsNewRowEditing
                                   ? nome + "|" + dataDia.ToString().Substring(0, 10) + "|" + Thread.CurrentThread.CurrentCulture.Name
                                   : nome;*/
                    }
                    else {
                        nome = "no_access";
                    }
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ClienteCollection clienteCollection = new ClienteCollection();
        clienteCollection.BuscaClientesComAcesso(HttpContext.Current.User.Identity.Name);

        e.Collection = clienteCollection;
    }

    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";

        ASPxTextBox textNome = gridCadastro.FindEditFormTemplateControl("textNome") as ASPxTextBox;
        TextBox textLogin = gridCadastro.FindEditFormTemplateControl("textLogin") as TextBox;
        ASPxComboBox dropAtivo = gridCadastro.FindEditFormTemplateControl("dropAtivo") as ASPxComboBox;
        TextBox textEmail = gridCadastro.FindEditFormTemplateControl("textEmail") as TextBox;
        ASPxComboBox dropTipoTrava = gridCadastro.FindEditFormTemplateControl("dropTipoTrava") as ASPxComboBox;
        ASPxComboBox dropGrupoUsuario = gridCadastro.FindEditFormTemplateControl("dropGrupoUsuario") as ASPxComboBox;
        ASPxComboBox dropEnviaEmailAcesso = gridCadastro.FindEditFormTemplateControl("dropEnviaEmailAcesso") as ASPxComboBox;

        TextBox textSenha = gridCadastro.FindEditFormTemplateControl("textSenha") as TextBox;
        TextBox textConfirmaSenha = gridCadastro.FindEditFormTemplateControl("textConfirmaSenha") as TextBox;


        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { 
            textNome, textLogin, dropAtivo, dropTipoTrava, dropGrupoUsuario 
        });

        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        textEmail.Text = textEmail.Text.Trim();
        textLogin.Text = textLogin.Text.Trim();

        bool insert = gridCadastro.IsNewRowEditing;

        #region Validação de senha: Senha por enquanto é sempre obrigatória em um insert
        bool temSenha = !String.IsNullOrEmpty(textSenha.Text);
        if (insert && !temSenha) {
            e.Result = "Senha não foi preenchida!";
            return;
        }

        if (temSenha) {
            if (String.IsNullOrEmpty(textConfirmaSenha.Text)) {
                e.Result = "Confirmação Senha não foi preenchida!";
                return;
            }

            if (textSenha.Text != textConfirmaSenha.Text) {
                e.Result = "Senha e Confirmação de Senha devem ser iguais";
                return;
            }

            FinancialMembershipProvider financialMembershipProvider = new FinancialMembershipProvider();
            try {
                financialMembershipProvider.ValidatePassword(null, textSenha.Text);
            }
            catch (Exception exception) {
                e.Result = exception.Message;
                return;
            }
        }
        #endregion

        string login = "";
        string email = "";
        if (!insert) {
            int idUsuario = Convert.ToInt32(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "IdUsuario"));
            Usuario usuario = new Usuario();
            usuario.LoadByPrimaryKey(idUsuario);
            login = usuario.Login.Trim();
            email = usuario.Email.Trim();
        }


        #region Valida Login Existente no Insert ou se Update para um novo login
        if (gridCadastro.IsNewRowEditing || login != textLogin.Text.Trim()) {
            UsuarioCollection usuarioCollection = new UsuarioCollection();
            usuarioCollection.Query.Where(usuarioCollection.Query.Login.Equal(textLogin.Text));
            if (usuarioCollection.Query.Load()) {
                e.Result = "Login já existente!";
                return;
            }
        }
        #endregion

        #region Valida Email
        bool textEmailPreenchido = (textEmail.Text.ToString() != "");
        if (textEmailPreenchido) {
            if (insert || email != textEmail.Text) { // Insert
                UsuarioCollection usuarioCollection = new UsuarioCollection();
                usuarioCollection.Query.Where(usuarioCollection.Query.Email.Equal(textEmail.Text));
                if (usuarioCollection.Query.Load()) {
                    e.Result = "Email já existente!";
                    return;
                }
            }

            if (!Utilitario.IsEmailValid(textEmail.Text)) {
                e.Result = "Email Inválido: " + textEmail.Text;
                return;
            }
        }
        #endregion

        #region Valida Envio Senha
        //Se email nao foi preenchido, o drop de enviaemail nao pode conter SIM
        if (Convert.ToString(dropEnviaEmailAcesso.SelectedItem.Value) == "S" && !textEmailPreenchido) {
            e.Result = "O e-mail deve ser informado quando a opção de envio de senha estiver ativa";
            return;
        }

        #endregion

    }

    protected void btnOKAdd_Init(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    protected void labelInfoSenha_Init(object sender, EventArgs e) {
        if (gridCadastro.IsNewRowEditing)
            (sender as Label).Visible = false;
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        Salvar(null);

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    private void Salvar(int? idUsuario) {
        bool insert = (idUsuario == null);

        Usuario usuario = new Usuario();
        if (!insert) {
            if (!usuario.LoadByPrimaryKey(idUsuario.Value)) {
                throw new Exception("Não foi possível acessar o usuário selecionado");
            }
        }

        ASPxComboBox dropGrupoUsuario = gridCadastro.FindEditFormTemplateControl("dropGrupoUsuario") as ASPxComboBox;
        ASPxTextBox textNome = gridCadastro.FindEditFormTemplateControl("textNome") as ASPxTextBox;
        TextBox textSenha = gridCadastro.FindEditFormTemplateControl("textSenha") as TextBox;
        TextBox textLogin = gridCadastro.FindEditFormTemplateControl("textLogin") as TextBox;
        TextBox textEmail = gridCadastro.FindEditFormTemplateControl("textEmail") as TextBox;
        ASPxComboBox dropAtivo = gridCadastro.FindEditFormTemplateControl("dropAtivo") as ASPxComboBox;
        ASPxComboBox dropTipoTrava = gridCadastro.FindEditFormTemplateControl("dropTipoTrava") as ASPxComboBox;
        ASPxComboBox dropEnviaEmailAcesso = gridCadastro.FindEditFormTemplateControl("dropEnviaEmailAcesso") as ASPxComboBox;
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;


        int? idGrupo = null;
        if (dropGrupoUsuario.SelectedIndex != -1) {
            idGrupo = Convert.ToInt32(dropGrupoUsuario.SelectedItem.Value);
        }

        string oldPassword = usuario.Senha;

        int? idClienteDefault = null;
        if (btnEditCodigoCliente.Value != null) {
            idClienteDefault = Convert.ToInt32(btnEditCodigoCliente.Value);
        }

        usuario.Save(idGrupo, textNome.Text, textLogin.Text,
            textSenha.Text, textEmail.Text.Trim(), Convert.ToString(dropAtivo.SelectedItem.Value),
            Convert.ToByte(dropTipoTrava.SelectedItem.Value), idClienteDefault);

        #region Codigo provisorio de save limiteoperacao
        ASPxSpinEdit textMaximoAplicacao = gridCadastro.FindEditFormTemplateControl("textMaximoAplicacao") as ASPxSpinEdit;
        ASPxSpinEdit textMaximoResgate = gridCadastro.FindEditFormTemplateControl("textMaximoResgate") as ASPxSpinEdit;
        decimal? maximoAplicacao = textMaximoAplicacao.Text != "" ? Convert.ToDecimal(textMaximoAplicacao.Text) : (decimal?)null;
        decimal? maximoResgate = textMaximoResgate.Text != "" ? Convert.ToDecimal(textMaximoResgate.Text) : (decimal?)null;
            
        LimiteOperacaoUsuario limiteOperacaoUsuario = new LimiteOperacaoUsuario();
        bool possuiRegistroLimite = limiteOperacaoUsuario.LoadByPrimaryKey(usuario.IdUsuario.Value);
        if (!possuiRegistroLimite)
        {
            limiteOperacaoUsuario.IdUsuario = usuario.IdUsuario.Value;
        }
        limiteOperacaoUsuario.MaximoAplicacao = maximoAplicacao;
        limiteOperacaoUsuario.MaximoResgate = maximoResgate;
        limiteOperacaoUsuario.Save();

        #endregion


        string newPassword = usuario.Senha;

        string descricaoOperacao = String.Format(
            "Cadastro de Usuario - Operacao: {0} Usuario: {1}",
            insert ? "Insert" : "Update",
            usuario.IdUsuario + UtilitarioWeb.ToString(usuario));


        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        descricaoOperacao,
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);


        bool senhaAlterada = (oldPassword != newPassword);
        bool enviaEmailAcesso = (Convert.ToString(dropEnviaEmailAcesso.SelectedItem.Value) == "S");
        //Se a senha foi alterada e o checkbox de envia email marcado, enviar email com senha
        if (senhaAlterada && enviaEmailAcesso) {
            usuario.EnviaEmailAcesso(insert);
        }

    }

    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        Salvar(null);
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {

        this.Salvar((int)e.Keys[0]);
        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdUsuario");
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idUsuario = Convert.ToInt32(keyValuesId[i]);

                Usuario usuario = new Usuario();
                if (usuario.LoadByPrimaryKey(idUsuario)) {
                    GrupoUsuario grupoUsuario = new GrupoUsuario();
                    grupoUsuario.LoadByPrimaryKey(usuario.IdGrupo.Value);
                    if (grupoUsuario.TipoPerfil.Value == (byte)TipoPerfilGrupo.Administrador) {
                        UsuarioCollection usuarioCollection = new UsuarioCollection();
                        UsuarioQuery usuarioQuery = new UsuarioQuery("U");
                        GrupoUsuarioQuery grupoUsuarioQuery = new GrupoUsuarioQuery("G");
                        usuarioQuery.Select(usuarioQuery.IdUsuario);
                        usuarioQuery.InnerJoin(grupoUsuarioQuery).On(grupoUsuarioQuery.IdGrupo == usuarioQuery.IdGrupo);
                        usuarioQuery.Where(grupoUsuarioQuery.TipoPerfil.Equal((byte)TipoPerfilGrupo.Administrador));

                        usuarioCollection.Load(usuarioQuery);

                        if (usuarioCollection.Count <= 1) {
                            throw new Exception("Deve existir ao menos 1 administrador! Deleção do usuário " + idUsuario.ToString() + " cancelada.");
                        }
                    }

                    //
                    Usuario usuarioClone = (Usuario)Utilitario.Clone(usuario);
                    //

                    usuario.MarkAsDeleted();
                    usuario.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de Usuario - Operacao: Delete Usuario: " + idUsuario + UtilitarioWeb.ToString(usuarioClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }
        else if (e.Parameters == "btnDesbloqueia") {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdUsuario");
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idUsuario = Convert.ToInt32(keyValuesId[i]);

                Usuario usuario = new Usuario();
                if (usuario.LoadByPrimaryKey(idUsuario)) {
                    usuario.Desbloqueia();
                    usuario.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Usuario - Operacao: Desbloqueia Usuario: " + idUsuario + UtilitarioWeb.ToString(usuario),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }
        else if (e.Parameters == "btnAtivar") {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdUsuario");
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idUsuario = Convert.ToInt32(keyValuesId[i]);

                Usuario usuario = new Usuario();
                if (usuario.LoadByPrimaryKey(idUsuario)) {
                    usuario.StatusAtivo = "S";
                    usuario.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Usuario - Operacao: Ativa Usuario: " + idUsuario + UtilitarioWeb.ToString(usuario),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }
        else if (e.Parameters == "btnDesativar") {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdUsuario");
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idUsuario = Convert.ToInt32(keyValuesId[i]);

                Usuario usuario = new Usuario();
                if (usuario.LoadByPrimaryKey(idUsuario)) {
                    usuario.StatusAtivo = "N";
                    usuario.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Usuario - Operacao: Desativa Usuario: " + idUsuario + UtilitarioWeb.ToString(usuario),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }
        else if (e.Parameters == "btnLogout") {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdUsuario");
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idUsuario = Convert.ToInt32(keyValuesId[i]);

                Usuario usuario = new Usuario();
                if (usuario.LoadByPrimaryKey(idUsuario)) {
                    usuario.DataLogout = DateTime.Now;
                    usuario.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Usuario - Operacao: Logoff forçado Usuario: " + idUsuario + UtilitarioWeb.ToString(usuario),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }


        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing) {
            ASPxTextBox textNome = gridCadastro.FindEditFormTemplateControl("textNome") as ASPxTextBox;
            e.Properties["cpTextNome"] = textNome.ClientID;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("textNome");
        base.gridCadastro_PreRender(sender, e);
    }

    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) {
        if (e.Column.FieldName == "Logado") {
            DateTime dataUltimoLogin = Convert.ToDateTime(e.GetListSourceFieldValue(UsuarioMetadata.ColumnNames.DataUltimoLogin));
            DateTime dataLogout = Convert.ToDateTime(e.GetListSourceFieldValue(UsuarioMetadata.ColumnNames.DataLogout));

            if (dataLogout < dataUltimoLogin) {
                e.Value = "Sim";
            }
            else {
                e.Value = "Não";
            }
        }
    }

    #region Funções relativas ao Logotipo Usuário

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackUsuarioLogotipo_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";

        // Limpa a Sessão
        Session.Remove("idUsuarios");

        // Pega os Elementos Selecionados
        List<object> idUsuariosSelecionados = gridCadastro.GetSelectedFieldValues(UsuarioMetadata.ColumnNames.IdUsuario);

        if (idUsuariosSelecionados.Count == 0) {
            e.Result = "Selecione um ou mais Id Usuários.";
        }
        else {
            // Salva os idUsuarios Selecionados na Sessão para posterior uso pelo Metodo de Upload
            Session["idUsuarios"] = idUsuariosSelecionados;
        }
    }

    /// <summary>
    /// Reseta O logotipo do Usuário
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackUsuarioResetaLogotipo_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";

        // Pega os Elementos Selecionados
        List<object> idUsuariosSelecionados = gridCadastro.GetSelectedFieldValues(UsuarioMetadata.ColumnNames.IdUsuario);

        if (idUsuariosSelecionados.Count == 0) {
            e.Result = "Selecione um ou mais Id Usuários.";
            return;
        }
        else {
            UsuarioCollection usuarioCollection = new UsuarioCollection();
            usuarioCollection.Query.Select(usuarioCollection.Query.IdUsuario,
                                           usuarioCollection.Query.Logotipo)
                                   .Where(usuarioCollection.Query.IdUsuario.In(idUsuariosSelecionados));
            //
            usuarioCollection.Query.Load();
            //
            for (int j = 0; j < usuarioCollection.Count; j++) {
                usuarioCollection[j].Logotipo = null;
            }

            usuarioCollection.Save();
        }

        e.Result = "Logotipo resetado com sucesso.";
    }

    /// <summary>
    /// Processa o salvamento do Logotipo após ter acabado o Upload
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplLogotipoUsuario_FileUploadComplete(object sender, FileUploadCompleteEventArgs e) {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!this.isExtensaoPNG(e.UploadedFile.FileName.Trim())) {
            e.CallbackData = "Formato de Arquivo Inválido. Formato permitido: .png\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo BMP
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento imagem Logotipo

        try {
            // Salva Imagem do Logotipo no Diretório de Imagens Personalizadas e atualiza o campo Logotipo 
            // para os usuários escolhidos.
            this.SalvaLogotipo(sr);
        }
        catch (Exception e2) {
            e.CallbackData = "Logotipo usuário - " + e2.Message;
            return;
        }
        #endregion

        // Limpa a Sessão
        Session.Remove("idUsuarios");
    }

    public bool isExtensaoPNG(string arquivo) {
        string extensao = new FileInfo(arquivo).Extension;
        if (extensao != ".png") {
            return false;
        }
        return true;
    }

    private void SalvaLogotipo(Stream streamImagem) {
        string path = AppDomain.CurrentDomain.BaseDirectory + "\\imagensPersonalizadas\\";

        int i = 1;
        string imagemUsuario = path + "logotipoUsuario_1.png";

        // Não repete nome da imagem
        while (File.Exists(imagemUsuario)) {
            i++;
            imagemUsuario = path + "logotipoUsuario_" + i.ToString() + ".png";
        }

        this.SaveStreamToFile(imagemUsuario, streamImagem);

        // Atualiza o campo Logotipo para todos os Usuários Escolhidos
        // Pega os idsUsuarios Selecionados
        List<Object> idUsuarios = (List<object>)Session["idUsuarios"];
        //
        UsuarioCollection usuarioCollection = new UsuarioCollection();
        usuarioCollection.Query.Select(usuarioCollection.Query.IdUsuario,
                                       usuarioCollection.Query.Logotipo)
                               .Where(usuarioCollection.Query.IdUsuario.In(idUsuarios));
        //
        usuarioCollection.Query.Load();
        //
        for (int j = 0; j < usuarioCollection.Count; j++) {
            usuarioCollection[j].Logotipo = imagemUsuario.Substring(imagemUsuario.LastIndexOf("\\") + 1);
        }

        usuarioCollection.Save();
    }

    /// <summary>
    /// Salva Stream como imagem no disco
    /// </summary>
    /// <param name="fileFullPath"></param>
    /// <param name="stream"></param>
    private void SaveStreamToFile(string fileFullPath, Stream stream) {
        if (stream.Length == 0) return;

        // Create a FileStream object to write a stream to a file
        using (FileStream fileStream = System.IO.File.Create(fileFullPath, (int)stream.Length)) {
            // Fill the bytes[] array with the stream data
            byte[] bytesInStream = new byte[stream.Length];
            stream.Read(bytesInStream, 0, (int)bytesInStream.Length);

            // Use FileStream object to write to the specified file
            fileStream.Write(bytesInStream, 0, bytesInStream.Length);
        }
    }

    #endregion
}