﻿using Financial.Web.Common;
using System;
using System.IO;
using System.Web;
using System.Globalization;

public partial class _ExibeExportacaoPermissaoMenu : BasePage {
   
    new protected void Page_Load(object sender, EventArgs e) {        
        MemoryStream ms = (MemoryStream)(Session["streamArquivo"]);
        //
        string nomeArquivo = "Grid.xls";
        //
        ms.Seek(0, SeekOrigin.Begin);
        Response.ClearContent();
        Response.ClearHeaders();
        Response.Buffer = true;
        Response.Cache.SetCacheability(HttpCacheability.Private);
        Response.ContentType = "application/vnd.ms-excel";
        Response.AddHeader("Content-Length", ms.Length.ToString(CultureInfo.CurrentCulture));
        Response.AddHeader("Content-Disposition", String.Format("attachment;filename=" + nomeArquivo));
        //
        Response.BinaryWrite(ms.ToArray());
        //
        ms.Close();
        HttpContext.Current.ApplicationInstance.CompleteRequest();
        Response.End();        
   }
}