﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PermissaoCotista.aspx.cs" Inherits="CadastrosBasicos_PermissaoCotista" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">

<title>Untitled Page</title>
<link href="~/css/forms.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="Javascript" src="../../js/global.js"></script>     

<script type="text/javascript" language="Javascript"> 
document.onkeydown=onDocumentKeyDown;
var operacao = '';
</script>
            
</head>
<body>    
    <form id="form1" runat="server">

    <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="360000" EnableScriptGlobalization="true" EnableScriptLocalization="true"/>
    
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {                         
            if (e.result != '') {            
                alert(e.result);                              
            }
            else {
                if (operacao == 'ativar') {
                    gridConsulta.PerformCallback('btnAtivar');
                }
                else if (operacao == 'desativar') {
                    gridConsulta.PerformCallback('btnDesativar');
                }
            }            
            operacao = '';
        }        
        "/>
    </dxcb:ASPxCallback>
            
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">
    
    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Controle de Permissão de Usuário (Cotista/Carteira)"/>
    </div>
        
    <div id="mainContent">    
                                
       <table cellspacing="2" cellpadding="2" width="450">
            <tr><td>
                    <asp:Label id="labelUsuario" runat="server" CssClass="labelNormal" Text="Usuário:"/>                
                </td>
                <td width="200">
                <dxe:ASPxComboBox ID="dropUsuario" runat="server" AutoPostBack="false" DataSourceID="EsDSUsuario"
                                                        ClientInstanceName="dropUsuario" Width="200px" IncrementalFilteringMode="Contains"
                                                        ShowShadow="false" DropDownStyle="DropDownList"
                                                        CssClass="dropDownListLongo" TextField="Login" ValueField="IdUsuario">
                
                <ClientSideEvents SelectedIndexChanged="function(s, e) { 
                        gridConsulta.PerformCallback('btnRefresh'); return false;  }"   />
                </dxe:ASPxComboBox>
                </td>
                <td>
                    <div class="linkButton linkButtonNoBorder" style="margin-top:5px;margin-bottom:5px">                                                    
                        <asp:LinkButton ID="btnAtiva" runat="server" Font-Overline="false" CssClass="btnEdit" OnClientClick="operacao='ativar'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal10" runat="server" Text="Ativa"/><div></div></asp:LinkButton>
                        <asp:LinkButton ID="btnDesativa" runat="server" Font-Overline="false" CssClass="btnPageDelete" OnClientClick="operacao='desativar'; callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal1" runat="server" Text="Desativa"/><div></div></asp:LinkButton>
                    </div>
                </td></tr>
            </table>
                                                                                                                                       
       <div class="divDataGrid">
            
                <dxwgv:ASPxGridView ID="gridConsulta" runat="server" EnableCallBacks="true" 
                        ClientInstanceName="gridConsulta" DataSourceID="EsDSCotista" 
                        KeyFieldName="IdCotista" Width="100%"
                        OnCustomCallback="gridConsulta_CustomCallback">             
                <Columns>                                                   
                                                            
                    <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="4%" ButtonType="Image" Visible="true" ShowClearFilterButton="True">
                        <HeaderStyle HorizontalAlign="Center"/>
                        <HeaderTemplate>
                            <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridConsulta);}" OnInit="CheckBoxSelectAll"/>
                        </HeaderTemplate>
                    </dxwgv:GridViewCommandColumn>
                                 
                    <dxwgv:GridViewDataTextColumn FieldName="IdCotista" VisibleIndex="1" Width="15%" CellStyle-HorizontalAlign="left"/>
                    <dxwgv:GridViewDataTextColumn FieldName="Apelido" VisibleIndex="2" Width="40%"/>
                    <dxwgv:GridViewDataTextColumn FieldName="Status" Caption="Permissão" VisibleIndex="3" Width="10%" CellStyle-HorizontalAlign="center"/>
                </Columns>            
                                
                <Settings VerticalScrollBarStyle="Virtual" />

                <Styles><Header ImageSpacing="5px" SortingImageSpacing="5px" /></Styles>
                <SettingsCommandButton>
                    <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                </SettingsCommandButton>                    
                              
                </dxwgv:ASPxGridView>                                                    
            </div>                      
    </div>
    </div>
    </td></tr></table>
    </div>   
        
    <cc1:esDataSource ID="EsDSUsuario" runat="server" OnesSelect="EsDSUsuario_esSelect" />
    <cc1:esDataSource ID="EsDSCotista" runat="server" OnesSelect="EsDSCotista_esSelect" />
    
    </form>
        
</body>
</html>