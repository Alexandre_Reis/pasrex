﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TabelaFundoCategoria.aspx.cs" Inherits="CadastrosBasicos_TabelaFundoCategoria" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">

    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />    
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    var popup = true;
    document.onkeydown=onDocumentKeyDownWithCallback;
    var operacao = '';
    
    function OnGetDataCarteira(data) {
        btnEditCodigoCarteiraOrigem.SetValue(data);        
        ASPxCallback1.SendCallback(btnEditCodigoCarteiraOrigem.GetValue());
        popupCarteira.HideWindow();
        btnEditCodigoCarteiraOrigem.Focus();    
    }    
    </script>
    
</head>

<body>
    <form id="form1" runat="server">
    
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"/>
    
    <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {    
            var textNome = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNome');
            OnCallBackCompleteCliente(s, e, popupMensagemCarteira, btnEditCodigoCarteiraOrigem, textNome);
        }        
        "/>
    </dxcb:ASPxCallback>
            
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '') {                   
                alert(e.result);                              
            }
            else {
                if (operacao == 'salvar')
                {             
                    gridCadastro.UpdateEdit();
                }
                else
                {
                    callbackAdd.SendCallback();
                }                 
            }
            operacao = '';
        }        
        "/>
    </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            alert('Operação feita com sucesso.');
        }        
        " />
        </dxcb:ASPxCallback>
        
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">

    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Tabela Fundo Categoria"></asp:Label>
    </div>
           
    <div id="mainContent">
        
            <div class="linkButton" >               
               <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal1" runat="server" Text="Novo"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;"><asp:Literal ID="Literal2" runat="server" Text="Excluir"/><div></div></asp:LinkButton>                          
               <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal4" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal5" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal6" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
            </div>
        
            <div class="divDataGrid">            
                <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                    KeyFieldName="CompositeKey" DataSourceID="EsDSTabelaFundoCategoria"
                    OnCustomCallback="gridCadastro_CustomCallback"
                    OnRowInserting="gridCadastro_RowInserting"
                    OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData"
                    OnPreRender="gridCadastro_PreRender"
                    >
                                                                                                                                                           
                <Columns>           
                    <dxwgv:GridViewCommandColumn VisibleIndex="0" Width="13%" ShowClearFilterButton="True">
                        <HeaderTemplate>
                            <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                        </HeaderTemplate>
                    </dxwgv:GridViewCommandColumn>
                    
                    <dxwgv:GridViewDataColumn FieldName="Apelido" Caption="Carteira" VisibleIndex="1" Width="35%" CellStyle-HorizontalAlign="left"/>

                    <dxwgv:GridViewDataColumn FieldName="DescricaoLista" Caption="Descrição Lista" VisibleIndex="2" Width="30%" CellStyle-HorizontalAlign="left" Settings-AutoFilterCondition="Contains" />
                    
                    <dxwgv:GridViewDataColumn FieldName="DescricaoCategoria" Caption="Descrição Categoria" VisibleIndex="3" Width="35%" CellStyle-HorizontalAlign="left" Settings-AutoFilterCondition="Contains" />

<%--
                    <dxwgv:GridViewDataComboBoxColumn Caption="Descrição Lista" FieldName="DescricaoLista" VisibleIndex="2" Width="30%">
                            <PropertiesComboBox DataSourceID="EsDSListaCategoriaFundo" TextField="DescricaoLista" ValueField="IdLista"/>
                    </dxwgv:GridViewDataComboBoxColumn>
                                       
                    <dxwgv:GridViewDataComboBoxColumn Caption="Descrição Categoria" FieldName="DescricaoCategoria" VisibleIndex="3" Width="35%">                            
                            <PropertiesComboBox DataSourceID="EsDSCategoriaFundo" TextField="Descricao" ValueField="IdCategoria"/>
                    </dxwgv:GridViewDataComboBoxColumn>
--%>                    
                    <dxwgv:GridViewDataTextColumn FieldName="IdCarteira" UnboundType="String" Visible="False"/>
                    <dxwgv:GridViewDataTextColumn FieldName="IdLista" UnboundType="String" Visible="False"/>                    
                    <dxwgv:GridViewDataTextColumn FieldName="CompositeKey" UnboundType="String" Visible="False"/>
                </Columns>
                
                <Templates>
                <EditForm>                    
                    <div class="editForm">    
                            
                        <table border="0">
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelCarteira" runat="server" CssClass="labelRequired" Text="Carteira:"/>
                                </td>
                                <td>
                                    <dxe:ASPxSpinEdit ID="btnEditCodigoCarteiraOrigem" runat="server" CssClass="textButtonEdit" 
                                            ClientInstanceName="btnEditCodigoCarteiraOrigem" MaxLength="10" NumberType="Integer">
                                    <Buttons>                                           
                                        <dxe:EditButton></dxe:EditButton>                                
                                    </Buttons>         
                                    <ClientSideEvents                                                           
                                             KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNome').value = '';} " 
                                             ButtonClick="function(s, e) {popupCarteira.ShowAtElementByID(s.name);}" 
                                             LostFocus="function(s, e) {popupMensagemCarteira.HideWindow();                                                                                        
                                                                        ASPxCallback1.SendCallback(btnEditCodigoCarteiraOrigem.GetValue());
                                                                      }"
                                            />               
                                    </dxe:ASPxSpinEdit>
                                </td>                                        
                                
                                <td colspan="2">
                                    <asp:TextBox ID="textNome" runat="server" CssClass="textLongo5" Enabled="False"/>   
                                </td>
                            </tr>
                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelLista" runat="server" CssClass="labelRequired" Text="Id Lista:"/>
                                </td>                
                                <td colspan="3">
                                    <dxe:ASPxComboBox ID="dropLista" runat="server" ClientInstanceName="dropLista"
                                                        DataSourceID="EsDSListaCategoriaFundo"
                                                        ShowShadow="false" DropDownStyle="DropDownList"
                                                        CssClass="dropDownList" TextField="Descricao" ValueField="idLista">
                                                        
                                     <ClientSideEvents SelectedIndexChanged="function(s, e) { dropCategoriaFundo.PerformCallback(); }" />
                                                            
                                    </dxe:ASPxComboBox>         
                                </td>
                            </tr>  
                                
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="label1" runat="server" CssClass="labelRequired" Text="Id Categoria Fundo:"/>
                                </td>                
                                <td colspan="3">
                                    <dxe:ASPxComboBox ID="dropCategoriaFundo" runat="server" ClientInstanceName="dropCategoriaFundo"
                                                        DataSourceID="EsDSCategoriaFundo" 
                                                        ShowShadow="false" DropDownStyle="DropDownList"
                                                        CssClass="dropDownList" TextField="Descricao" ValueField="IdCategoria"
                                                        OnCallback="dropCategoriaFundo_Callback"
                                                        >
                                    </dxe:ASPxComboBox>         
                                </td>
                            </tr>  
                                
                                                                                    
                        </table>
                        
                        <div class="linhaH"></div>    
                                                
                        <div class="linkButton linkButtonNoBorder popupFooter">
                        
                        <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd"
                            OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;">
                            <asp:Literal ID="Literal3" runat="server" Text="OK+" /><div>
                            </div>
                        </asp:LinkButton>
                        
                        <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                            OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                            <asp:Literal ID="Literal12" runat="server" Text="OK" /><div>
                            </div>
                        </asp:LinkButton>
                        
                        <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel"
                            OnClientClick="gridCadastro.CancelEdit(); return false;">
                            <asp:Literal ID="Literal13" runat="server" Text="Cancelar" /><div>
                            </div>
                        </asp:LinkButton>
                    </div>
                        
                    </div>                                              
                </EditForm>
                
                 <StatusBar>
                        <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" />
                 </StatusBar> 
                    
                </Templates>
                
                <SettingsPopup EditForm-Width="500px" />
                <SettingsCommandButton>
                    <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                </SettingsCommandButton>
                                                
            </dxwgv:ASPxGridView>            
            </div>
    </div>
    </div>
    </td></tr></table>
    </div>        
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" LeftMargin = "30" RightMargin = "30"/>
        
    <cc1:esDataSource ID="EsDSTabelaFundoCategoria" runat="server" OnesSelect="EsDSTabelaFundoCategoria_esSelect" LowLevelBind="true" />
    <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" />
    <cc1:esDataSource ID="EsDSListaCategoriaFundo" runat="server" OnesSelect="EsDSListaCategoriaFundo_esSelect" />
    <cc1:esDataSource ID="EsDSCategoriaFundo" runat="server" OnesSelect="EsDSCategoriaFundo_esSelect" />
                
    </form>
</body>
</html>