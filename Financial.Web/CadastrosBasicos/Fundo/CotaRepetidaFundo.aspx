﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CotaRepetidaFundo.aspx.cs" Inherits="CadastrosBasicos_CotaRepetidaFundo" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxlp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
    var popup = true;
    document.onkeydown=onDocumentKeyDownWithCallback;
    var operacao = '';
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '' && e.result != 'IdItau')
            {                   
                alert(e.result);
            }
            else
            {
                if (e.result == 'IdItau')
                {
                    if (confirm('Cota com variação acima de 2%, deseja continuar?')==false) 
                    { 
                        operacao = '';
                        return false;
                    }
                }                
                
                if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new')
                {             
                    gridCadastro.UpdateEdit();
                }
                else
                {
                    callbackAdd.SendCallback();
                }                
            }
            operacao = '';
        }        
        " />
        </dxcb:ASPxCallback>
        
        <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            alert('Operação feita com sucesso.');
        }        
        " />
        </dxcb:ASPxCallback>
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Cotas Importadas Pendentes de Inclusão."></asp:Label>
                            </div>
                            <div id="mainContent">
                                <dxpc:ASPxPopupControl ID="popupFiltro" 
                                                       AllowDragging="true" 
                                                       PopupElementID="popupFiltro"
                                                       EnableClientSideAPI="True" 
                                                       PopupVerticalAlign="Middle" 
                                                       PopupHorizontalAlign="OutsideRight"
                                                       Width="400" 
                                                       Left="250" 
                                                       Top="70" 
                                                       HeaderText="Filtros adicionais para consulta"
                                                       runat="server" 
                                                       HeaderStyle-BackColor="#EBECEE" 
                                                       HeaderStyle-Font-Bold="true" 
                                                       HeaderStyle-Font-Size="11px">
                                    <ContentCollection>
                                        <dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="labelDataInicio" 
                                                                   runat="server" 
                                                                   CssClass="labelNormal" 
                                                                   Text="Início:">
                                                         </asp:Label>
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxDateEdit ID="textDataInicio" 
                                                                          runat="server" 
                                                                          ClientInstanceName="textDataInicio" />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="labelDataFim" 
                                                                   runat="server" 
                                                                   CssClass="labelNormal" 
                                                                   Text="Fim:" />
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxDateEdit ID="textDataFim" 
                                                                          runat="server" 
                                                                          ClientInstanceName="textDataFim" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="labelCarteira" 
                                                                   runat="server" 
                                                                   CssClass="labelNormal" 
                                                                   Text="Fundo:" />
                                                    </td>
                                                    <td colspan="3">
                                                        <dxe:ASPxComboBox ID="dropCarteiraFiltro" 
                                                                          runat="server" 
                                                                          DropDownStyle="DropDown" 
                                                                          IncrementalFilteringMode="Contains"
                                                                          DataSourceID="EsDSCarteira" 
                                                                          ValueField="IdCarteira" 
                                                                          TextField="Apelido" 
                                                                          CssClass="dropDownListLongo">
                                                        </dxe:ASPxComboBox>
                                                    </td>
                                                </tr>
                                            </table>
                                            <div class="linkButton linkButtonNoBorder" 
                                                 style="margin-top: 20px">
                                                <asp:LinkButton ID="btnOKFilter" 
                                                                runat="server" 
                                                                Font-Overline="false" 
                                                                ForeColor="Black"
                                                                CssClass="btnOK" 
                                                                OnClientClick="popupFiltro.Hide(); gridCadastro.PerformCallback('btnRefresh'); return false;">
                                                    <asp:Literal ID="Literal7" 
                                                                 runat="server" 
                                                                 Text="Aplicar" /><div>
                                                    </div>
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="btnFilterCancel" 
                                                                runat="server" 
                                                                Font-Overline="false" 
                                                                ForeColor="Black"
                                                                CssClass="btnFilterCancel" 
                                                                OnClientClick="OnClearFilterClick_FiltroDatas(); return false;">
                                                    <asp:Literal ID="Literal8" 
                                                                 runat="server" 
                                                                 Text="Limpar" />
                                                    <div>
                                                    </div>
                                                </asp:LinkButton>
                                            </div>
                                        </dxpc:PopupControlContentControl>
                                    </ContentCollection>
                                </dxpc:ASPxPopupControl>
                                <div class="linkButton"> 
                                    <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" CssClass="btnOK"
                                        OnClientClick="if (confirm('Aprovar ?')==true) gridCadastro.PerformCallback('btnAprovar');return false;">
                                        <asp:Literal ID="Literal1" runat="server" Text="Aprovar" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" CssClass="btnDelete"
                                        OnClientClick="if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;">
                                        <asp:Literal ID="Literal2" runat="server" Text="Excluir" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnFilter" runat="server" Font-Overline="false" CssClass="btnFilter"
                                        OnClientClick="return OnButtonClick_FiltroDatas()">
                                        <asp:Literal ID="Literal3" runat="server" Text="Filtro" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf"
                                        OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal4" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel"
                                        OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal5" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh"
                                        OnClientClick="gridCadastro.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal6" runat="server" Text="Atualizar" /><div>
                                        </div>
                                    </asp:LinkButton>
                                </div>
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridCadastro" 
                                                        runat="server" 
                                                        EnableCallBacks="true" 
                                                        KeyFieldName="CompositeKey"
                                                        DataSourceID="EsDSHistoricoCota" 
                                                        OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData"
                                                        OnRowUpdating="gridCadastro_RowUpdating" 
                                                        OnCustomCallback="gridCadastro_CustomCallback" 
                                                        OnPreRender="gridCadastro_PreRender"
                                                        OnCustomJSProperties="gridCadastro_CustomJSProperties" 
                                                        OnBeforeGetCallbackResult="gridCadastro_PreRender">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="10%" ButtonType="Image" ShowClearFilterButton="True">
                                                <HeaderTemplate>
                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                                </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataDateColumn FieldName="Data" VisibleIndex="1" Width="10%" />
                                            <dxwgv:GridViewDataTextColumn FieldName="Apelido" Caption="Fundo" Width="45%" VisibleIndex="2"/>
                                            <dxwgv:GridViewDataSpinEditColumn FieldName="Cota" Caption="Valor Cota"
                                                VisibleIndex="3" Width="30%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                                                <PropertiesSpinEdit DisplayFormatString="{0:#,##0.0000000000;(#,##0.00);0.0000000000}" />
                                            </dxwgv:GridViewDataSpinEditColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="CompositeKey" UnboundType="String" Visible="False" />
                                            <dxwgv:GridViewDataTextColumn FieldName="IdCarteira" Visible="false" />
                                        </Columns>
                                        <Templates>
                                            <StatusBar>
                                                <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" />
                                            </StatusBar>
                                        </Templates>
                                        <SettingsEditing PopupEditFormWidth="350px" />
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
        <cc1:esDataSource ID="EsDSHistoricoCota" runat="server" OnesSelect="EsDSHistoricoCota_esSelect" />
        <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" />
        <dxlp:ASPxLoadingPanel ID="LoadingPanel" runat="server" Text="Importando cotas, aguarde..."
            ClientInstanceName="LoadingPanel" Modal="True" />
    </form>
</body>
</html>
