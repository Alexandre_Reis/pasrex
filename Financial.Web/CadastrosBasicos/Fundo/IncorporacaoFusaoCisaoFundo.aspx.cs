﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using Financial.Util.Enums;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.CRM;
using Financial.Security;
using Financial.Fundo;
using Financial.Fundo.Enums;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Web.Common;
using EntitySpaces.Interfaces;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.InvestidorCotista;
using Financial.BMF;
using Financial.Bolsa;
using Financial.RendaFixa;
using Financial.Swap;
using Financial.ContaCorrente;
using Financial.InvestidorCotista.Enums;

public partial class CadastrosBasicos_IncorporacaoFusaoCisaoFundo : CadastroBasePage
{
    public bool VerificaCarteiraParametro
    {
        get
        {
            if (Session["VerificaCarteiraParametro"] == null)
                Session["VerificaCarteiraParametro"] = false;

            return (bool)Session["VerificaCarteiraParametro"];
        }
        set
        {
            Session["VerificaCarteiraParametro"] = value;
        }
    }

    new protected void Page_Load(object sender, EventArgs e)
    {

        this.AllowUpdate = false; // Não permite Update

        this.HasPopupCarteira = true;   // PopUp Carteira Origem
        this.HasPopupCarteira1 = true;  // PopUp Carteira Destino
        base.Page_Load(sender, e);
    }

    #region DataSources

    /// <summary>
    /// Consulta Principal
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSIncorporacaoFundo_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        EventoFundoQuery eventoFundoQuery = new EventoFundoQuery("EVQ");
        CarteiraQuery carteiraOrigemQuery = new CarteiraQuery("C1");
        CarteiraQuery carteiraDestinoQuery = new CarteiraQuery("C2");
        //
        eventoFundoQuery.Select(eventoFundoQuery,
                                carteiraOrigemQuery.IdCarteira,
                                carteiraOrigemQuery.Apelido.As("Apelido"),
                                carteiraDestinoQuery.IdCarteira,
                                carteiraDestinoQuery.Apelido.As("Apelido1"));
        //
        eventoFundoQuery.InnerJoin(carteiraOrigemQuery).On(eventoFundoQuery.IdCarteiraOrigem.Equal(carteiraOrigemQuery.IdCarteira));
        eventoFundoQuery.InnerJoin(carteiraDestinoQuery).On(eventoFundoQuery.IdCarteiraDestino.Equal(carteiraDestinoQuery.IdCarteira));

        //
        eventoFundoQuery.OrderBy(eventoFundoQuery.IdCarteiraOrigem.Ascending);
        //

        EventoFundoCollection coll = new EventoFundoCollection();
        coll.Load(eventoFundoQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    /// <summary>
    /// Popup Carteira Origem e Destino
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {

        CarteiraQuery carteiraQuery = new CarteiraQuery("A");
        ClienteQuery clienteQuery = new ClienteQuery("C");

        carteiraQuery.Select(carteiraQuery.IdCarteira, carteiraQuery.Apelido);
        carteiraQuery.Where(clienteQuery.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo));
        carteiraQuery.InnerJoin(clienteQuery).On(carteiraQuery.IdCarteira == clienteQuery.IdCliente);
        carteiraQuery.OrderBy(carteiraQuery.Apelido.Ascending);

        CarteiraCollection coll = new CarteiraCollection();
        coll.Load(carteiraQuery);

        e.Collection = coll;
    }

    #endregion

    #region Popups Carteira
    /// <summary>
    /// Pop Up Carteira Origem
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCarteira = Convert.ToInt32(e.Parameter);
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            if (carteira.str.Apelido != "")
            {
                if (carteira.IsAtivo)
                {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    nome = permissaoCliente.RetornaAcessoCliente(idCarteira, HttpContext.Current.User.Identity.Name)
                           ? carteira.str.Apelido
                           : "no_access";

                }
                else
                {
                    nome = "no_active";
                }
            }
        }

        e.Result = e.Parameter + " - " + nome;
    }

    /// <summary>
    /// Seleciona data da carteira
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback3_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);
            e.Result = cliente.DataDia.Value.Year + "|" + (cliente.DataDia.Value.Month - 1) + "|" + cliente.DataDia.Value.Day;
        }
    }


    /// <summary>
    /// Pop Up Carteira Destino
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCarteira = Convert.ToInt32(e.Parameter);
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            if (carteira.str.Apelido != "")
            {
                if (carteira.IsAtivo)
                {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    nome = permissaoCliente.RetornaAcessoCliente(idCarteira, HttpContext.Current.User.Identity.Name)
                           ? carteira.str.Apelido : "no_access";
                }
                else
                {
                    nome = "no_active";
                }
            }
        }

        e.Result = e.Parameter + " - " + nome;
    }
    #endregion

    protected void callbackStatus_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        List<object> keyValuesIdEventoFundo = gridCadastro.GetSelectedFieldValues(EventoFundoMetadata.ColumnNames.IdEventoFundo);

        for (int i = 0; i < keyValuesIdEventoFundo.Count; i++)
        {
            int idEventoFundo = Convert.ToInt32(keyValuesIdEventoFundo[i]);

            EventoFundo eventoFundo = new EventoFundo();
            eventoFundo.LoadByPrimaryKey(idEventoFundo);
            if (eventoFundo.Status.Equals("Ativo"))
            {
                eventoFundo.Status = "Desligado";
            }
            else
            {
                eventoFundo.Status = "Ativo";
            }

            eventoFundo.Save();
        }
        e.Result = "Alteração efetuada com sucesso.";
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// Tratamento Erro
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        ASPxButtonEdit btnEditCodigoCarteiraOrigem = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteiraOrigem") as ASPxButtonEdit;
        ASPxButtonEdit btnEditCodigoCarteiraDestino = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteiraDestino") as ASPxButtonEdit;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        //ASPxComboBox dropMetodo = gridCadastro.FindEditFormTemplateControl("dropMetodo") as ASPxComboBox;
        ASPxComboBox dropEvento = gridCadastro.FindEditFormTemplateControl("dropEvento") as ASPxComboBox;
        ASPxTextBox hiddenIdCarteiraOrigem = gridCadastro.FindEditFormTemplateControl("hiddenIdCarteiraOrigem") as ASPxTextBox;
        ASPxTextBox hiddenIdCarteiraDestino = gridCadastro.FindEditFormTemplateControl("hiddenIdCarteiraDestino") as ASPxTextBox;
        ASPxCallbackPanel cbPanel = gridCadastro.FindEditFormTemplateControl("cbPanel") as ASPxCallbackPanel;
        ASPxRadioButtonList rblExecucaoRecolhimento = cbPanel.FindControl("rblExecucaoRecolhimento") as ASPxRadioButtonList;
        ASPxRadioButtonList rblAliquotaIR = cbPanel.FindControl("rblAliquotaIR") as ASPxRadioButtonList;
        ASPxDateEdit textDataRecolhimento = cbPanel.FindControl("textDataRecolhimento") as ASPxDateEdit;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(btnEditCodigoCarteiraOrigem);
        controles.Add(btnEditCodigoCarteiraDestino);
        controles.Add(textData);
        //controles.Add(dropMetodo);
        controles.Add(dropEvento);

        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        if (Convert.ToInt32(hiddenIdCarteiraOrigem.Value) == Convert.ToInt32(hiddenIdCarteiraDestino.Value))
        {
            e.Result = "Carteira Origem não pode ser Igual Carteira Destino.";
            return;
        }
        else if (rblExecucaoRecolhimento.SelectedItem == null)
            e.Result = "Campo 'Execução do Recolhimento' deve ser preenchido!";
        else if (rblExecucaoRecolhimento.SelectedItem != null)
        {
            if (rblExecucaoRecolhimento.SelectedItem.Value.Equals(3))
            {
                int comparaData = DateTime.Compare(Convert.ToDateTime(textDataRecolhimento.Text), Convert.ToDateTime(textData.Text));
                if (textDataRecolhimento.Text == string.Empty)
                    e.Result = "Campo 'Data Recolhimento' deve ser preenchido!";
                else if (comparaData < 0)
                    e.Result = "'Data Recolhimento' deve ser maior que a 'Data Posição'";
            }
            if (rblExecucaoRecolhimento.SelectedItem.Value.Equals(3) || rblExecucaoRecolhimento.SelectedItem.Value.Equals(2))
            {
                if (rblAliquotaIR.SelectedItem == null)
                    e.Result = "Campo 'Alíquota IR' deve ser preenchido!";
            }
        }

        //Verifica se possui parâmetro cadastrado
        if (!VerificaCarteiraParametro)
        {
            e.Result = "Parâmetros do Administrador devem ser previamente cadastrados para possibilitar o registro do evento corporativo";
            return;
        }

        #region Verifica Evento Cadastrado
        //Verifica se já existe um evento para a carteira a ser cadastrada
        DateTime? Data = Financial.Util.ConverteTipo.TryParseDateTime(textData.Text);
        int? Carteira = Financial.Util.ConverteTipo.TryParseInt(hiddenIdCarteiraOrigem.Text);
        if (Carteira != null && Data != null)
        {
            string existeEvento = VerificaEventoExistente(Carteira, Data);
            if (!string.IsNullOrEmpty(existeEvento))
            {
                e.Result = existeEvento;
                return;
            }
        }
        #endregion
    }

    /// <summary>
    /// Inserção
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        Salvar();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// Insere Registro
    /// </summary>
    private void Salvar()
    {
        ASPxTextBox hiddenIdCarteiraOrigem = gridCadastro.FindEditFormTemplateControl("hiddenIdCarteiraOrigem") as ASPxTextBox;
        ASPxTextBox hiddenIdCarteiraDestino = gridCadastro.FindEditFormTemplateControl("hiddenIdCarteiraDestino") as ASPxTextBox;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxSpinEdit textPercentual = gridCadastro.FindEditFormTemplateControl("textPercentual") as ASPxSpinEdit;
        //ASPxComboBox dropMetodo = gridCadastro.FindEditFormTemplateControl("dropMetodo") as ASPxComboBox;
        ASPxComboBox dropEvento = gridCadastro.FindEditFormTemplateControl("dropEvento") as ASPxComboBox;
        ASPxCallbackPanel cbPanel = gridCadastro.FindEditFormTemplateControl("cbPanel") as ASPxCallbackPanel;
        ASPxRadioButtonList rblExecucaoRecolhimento = cbPanel.FindControl("rblExecucaoRecolhimento") as ASPxRadioButtonList;
        ASPxRadioButtonList rblAliquotaIR = cbPanel.FindControl("rblAliquotaIR") as ASPxRadioButtonList;
        ASPxDateEdit textDataRecolhimento = cbPanel.FindControl("textDataRecolhimento") as ASPxDateEdit;

        EventoFundo eventoFundo = new EventoFundo();

        eventoFundo.IdCarteiraOrigem = Convert.ToInt32(hiddenIdCarteiraOrigem.Value);
        eventoFundo.IdCarteiraDestino = Convert.ToInt32(hiddenIdCarteiraDestino.Value);
        eventoFundo.TipoEvento = Convert.ToInt32(dropEvento.SelectedItem.Value);
        //eventoFundo.TipoMetodo = Convert.ToInt32(dropMetodo.SelectedItem.Value);
        eventoFundo.DataPosicao = Convert.ToDateTime(textData.Text);
        eventoFundo.Status = "Ativo";

        if (textPercentual.Text != "")
        {
            eventoFundo.Percentual = Convert.ToDecimal(textPercentual.Text);
        }
        else
        {
            eventoFundo.Percentual = 0;
        }
        eventoFundo.ExecucaoRecolhimento = Convert.ToInt32(rblExecucaoRecolhimento.SelectedItem.Value);
        if(eventoFundo.ExecucaoRecolhimento > 1) eventoFundo.AliquotaIR = Convert.ToInt32(rblAliquotaIR.SelectedItem.Value);
        eventoFundo.DataRecolhimento = Financial.Util.ConverteTipo.TryParseDateTime(textData.Text);
        eventoFundo.Save();

        this.geraMovimentosCautela(eventoFundo.IdEventoFundo.Value, eventoFundo.IdCarteiraOrigem.Value, eventoFundo.DataPosicao.Value, eventoFundo.Percentual.Value);
        this.geraMovimentosAtivos(eventoFundo.IdEventoFundo.Value, eventoFundo.IdCarteiraOrigem.Value, eventoFundo.DataPosicao.Value, eventoFundo.Percentual.Value);

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de Incorporação/Fusão/Cisão Fundo - Operacao: Insert EventoFundo: " + eventoFundo.IdEventoFundo.Value + "; " + UtilitarioWeb.ToString(eventoFundo),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    /// <summary>
    /// Gera posições de cautelas para tela de acertos de evento de Cisão
    /// </summary>
    /// <param name="idEventoFundo"></param>
    /// <param name="idCarteiraOrigem"></param>
    /// <param name="dataPosicao"></param>
    /// <param name="percentual"></param>
    private void geraMovimentosCautela(int idEventoFundo, int idCarteiraOrigem, DateTime dataPosicao, decimal percentual)
    {
        PosicaoCotistaHistoricoQuery posicaoCotistaHistoricoQuery = new PosicaoCotistaHistoricoQuery();

        posicaoCotistaHistoricoQuery.Select(posicaoCotistaHistoricoQuery.IdCotista,
                                             posicaoCotistaHistoricoQuery.DataAplicacao,
                                             posicaoCotistaHistoricoQuery.IdOperacao.Coalesce("0"),
                                             posicaoCotistaHistoricoQuery.Quantidade.Sum());
        posicaoCotistaHistoricoQuery.Where(posicaoCotistaHistoricoQuery.IdCarteira == idCarteiraOrigem);
        posicaoCotistaHistoricoQuery.Where(posicaoCotistaHistoricoQuery.DataHistorico.Equal(dataPosicao));
        posicaoCotistaHistoricoQuery.Where(posicaoCotistaHistoricoQuery.ValorBruto.NotEqual(0)); 
        posicaoCotistaHistoricoQuery.GroupBy(posicaoCotistaHistoricoQuery.IdCotista,
                                             posicaoCotistaHistoricoQuery.DataAplicacao,
                                             posicaoCotistaHistoricoQuery.IdOperacao.Coalesce("0"),
                                             posicaoCotistaHistoricoQuery.Quantidade);

        PosicaoCotistaHistoricoCollection posicaoCotistaHistoricoCollection = new PosicaoCotistaHistoricoCollection();
        posicaoCotistaHistoricoCollection.Load(posicaoCotistaHistoricoQuery);

        foreach (PosicaoCotistaHistorico posicaoCotistaHistorico in posicaoCotistaHistoricoCollection)
        {
            EventoFundoCautela eventoFundoCautela = new EventoFundoCautela();

            eventoFundoCautela.IdEventoFundo = idEventoFundo;
            eventoFundoCautela.IdCotista = posicaoCotistaHistorico.IdCotista;
            eventoFundoCautela.DataAplicacao = posicaoCotistaHistorico.DataAplicacao;
            eventoFundoCautela.Quantidade = (posicaoCotistaHistorico.Quantidade * percentual) / 100;
            eventoFundoCautela.IdOperacao = posicaoCotistaHistorico.IdOperacao.Value;
            eventoFundoCautela.Save();
        }
    }

    /// <summary>
    /// Gera posições de ativos para tela de acertos de evento de Cisão
    /// </summary>
    /// <param name="idEventoFundo"></param>
    /// <param name="idCarteiraOrigem"></param>
    /// <param name="dataPosicao"></param>
    /// <param name="percentual"></param>
    private void geraMovimentosAtivos(int idEventoFundo, int idCarteiraOrigem, DateTime dataPosicao, decimal percentual)
    {
        //Posicao BMF
        PosicaoBMFHistoricoQuery posicaoBMFHistoricoQuery = new PosicaoBMFHistoricoQuery();

        posicaoBMFHistoricoQuery.Select(posicaoBMFHistoricoQuery.CdAtivoBMF, posicaoBMFHistoricoQuery.Serie, posicaoBMFHistoricoQuery.Quantidade.Sum());
        posicaoBMFHistoricoQuery.Where(posicaoBMFHistoricoQuery.IdCliente == idCarteiraOrigem);
        posicaoBMFHistoricoQuery.Where(posicaoBMFHistoricoQuery.DataHistorico.Equal(dataPosicao));
        posicaoBMFHistoricoQuery.GroupBy(posicaoBMFHistoricoQuery.CdAtivoBMF, posicaoBMFHistoricoQuery.Serie);

        PosicaoBMFHistoricoCollection posicaoBMFHistoricoCollection = new PosicaoBMFHistoricoCollection();
        posicaoBMFHistoricoCollection.Load(posicaoBMFHistoricoQuery);

        foreach (PosicaoBMFHistorico posicaoBMFHistorico in posicaoBMFHistoricoCollection)
        {
            EventoFundoBMF eventoFundoBMF = new EventoFundoBMF();

            eventoFundoBMF.IdEventoFundo = idEventoFundo;
            eventoFundoBMF.CdAtivoBMF = posicaoBMFHistorico.CdAtivoBMF;
            eventoFundoBMF.Serie = posicaoBMFHistorico.Serie;
            eventoFundoBMF.IdAgente = posicaoBMFHistorico.IdAgente;
            eventoFundoBMF.Quantidade = (posicaoBMFHistorico.Quantidade * percentual) / 100;
            eventoFundoBMF.TipoMercado = (int)TipoMercado.BMF;
            eventoFundoBMF.ValorLiquido = 0;
            eventoFundoBMF.Save();
        }

        //Posicao Bolsa
        PosicaoBolsaHistoricoQuery posicaoBolsaHistoricoQuery = new PosicaoBolsaHistoricoQuery();

        posicaoBolsaHistoricoQuery.Where(posicaoBolsaHistoricoQuery.IdCliente == idCarteiraOrigem);
        posicaoBolsaHistoricoQuery.Where(posicaoBolsaHistoricoQuery.DataHistorico.Equal(dataPosicao));

        PosicaoBolsaHistoricoCollection posicaoBolsaHistoricoCollection = new PosicaoBolsaHistoricoCollection();
        posicaoBolsaHistoricoCollection.Load(posicaoBolsaHistoricoQuery);

        foreach (PosicaoBolsaHistorico posicaoBolsaHistorico in posicaoBolsaHistoricoCollection)
        {
            EventoFundoBolsa eventoFundoBolsa = new EventoFundoBolsa();

            eventoFundoBolsa.IdEventoFundo = idEventoFundo;
            eventoFundoBolsa.CdAtivoBolsa = posicaoBolsaHistorico.CdAtivoBolsa;
            eventoFundoBolsa.IdAgente = posicaoBolsaHistorico.IdAgente;
            eventoFundoBolsa.Quantidade = Utilitario.RoundMax(Convert.ToDecimal(((posicaoBolsaHistorico.Quantidade * percentual) / 100)), 0);
            eventoFundoBolsa.TipoMercado = (int)TipoMercado.Bolsa;
            eventoFundoBolsa.Save();
        }

        //Posicao Renda Fixa
        PosicaoRendaFixaHistoricoQuery posicaoRendaFixaHistoricoQuery = new PosicaoRendaFixaHistoricoQuery();

        posicaoRendaFixaHistoricoQuery.Select(posicaoRendaFixaHistoricoQuery.IdTitulo,
                                              posicaoRendaFixaHistoricoQuery.TipoOperacao,
                                              posicaoRendaFixaHistoricoQuery.DataOperacao,
                                              posicaoRendaFixaHistoricoQuery.DataLiquidacao,
                                              posicaoRendaFixaHistoricoQuery.Quantidade.Sum());
        posicaoRendaFixaHistoricoQuery.Where(posicaoRendaFixaHistoricoQuery.IdCliente == idCarteiraOrigem);
        posicaoRendaFixaHistoricoQuery.Where(posicaoRendaFixaHistoricoQuery.DataHistorico.Equal(dataPosicao));
        posicaoRendaFixaHistoricoQuery.GroupBy(posicaoRendaFixaHistoricoQuery.IdTitulo,
                                              posicaoRendaFixaHistoricoQuery.TipoOperacao,
                                              posicaoRendaFixaHistoricoQuery.DataOperacao,
                                              posicaoRendaFixaHistoricoQuery.DataLiquidacao);

        PosicaoRendaFixaHistoricoCollection posicaoRendaFixaHistoricoCollection = new PosicaoRendaFixaHistoricoCollection();
        posicaoRendaFixaHistoricoCollection.Load(posicaoRendaFixaHistoricoQuery);

        foreach (PosicaoRendaFixaHistorico posicaoRendaFixaHistorico in posicaoRendaFixaHistoricoCollection)
        {
            EventoFundoRendaFixa eventoFundoRendaFixa = new EventoFundoRendaFixa();

            eventoFundoRendaFixa.IdEventoFundo = idEventoFundo;
            eventoFundoRendaFixa.IdTitulo = posicaoRendaFixaHistorico.IdTitulo;
            eventoFundoRendaFixa.TipoOperacao = posicaoRendaFixaHistorico.TipoOperacao;
            eventoFundoRendaFixa.DataOperacao = posicaoRendaFixaHistorico.DataOperacao;
            eventoFundoRendaFixa.DataLiquidacao = posicaoRendaFixaHistorico.DataLiquidacao;
            eventoFundoRendaFixa.Quantidade = Utilitario.RoundMax(Convert.ToDecimal(((posicaoRendaFixaHistorico.Quantidade * percentual) / 100)), 2);
            eventoFundoRendaFixa.TipoMercado = (int)TipoMercado.RendaFixa;
            eventoFundoRendaFixa.Save();
        }

        //Posicao Fundos
        PosicaoFundoHistoricoQuery posicaoFundoHistoricoQuery = new PosicaoFundoHistoricoQuery();

        posicaoFundoHistoricoQuery.Select(posicaoFundoHistoricoQuery.IdCarteira,
                                          posicaoFundoHistoricoQuery.Quantidade.Sum());
        posicaoFundoHistoricoQuery.Where(posicaoFundoHistoricoQuery.IdCliente == idCarteiraOrigem);
        posicaoFundoHistoricoQuery.Where(posicaoFundoHistoricoQuery.DataHistorico.Equal(dataPosicao));
        posicaoFundoHistoricoQuery.GroupBy(posicaoFundoHistoricoQuery.IdCarteira);

        PosicaoFundoHistoricoCollection posicaoFundoHistoricoCollection = new PosicaoFundoHistoricoCollection();
        posicaoFundoHistoricoCollection.Load(posicaoFundoHistoricoQuery);

        foreach (PosicaoFundoHistorico posicaoFundoHistorico in posicaoFundoHistoricoCollection)
        {
            EventoFundoFundo eventoFundoFundo = new EventoFundoFundo();

            eventoFundoFundo.IdEventoFundo = idEventoFundo;
            eventoFundoFundo.IdCarteira = posicaoFundoHistorico.IdCarteira;
            eventoFundoFundo.Quantidade = (posicaoFundoHistorico.Quantidade * percentual) / 100;
            eventoFundoFundo.TipoMercado = (int)TipoMercado.Fundos;
            eventoFundoFundo.Save();
        }

        //Posicao Swap
        PosicaoSwapHistoricoQuery posicaoSwapHistoricoQuery = new PosicaoSwapHistoricoQuery();

        posicaoSwapHistoricoQuery.Where(posicaoSwapHistoricoQuery.IdCliente == idCarteiraOrigem);
        posicaoSwapHistoricoQuery.Where(posicaoSwapHistoricoQuery.DataHistorico.Equal(dataPosicao));

        PosicaoSwapHistoricoCollection posicaoSwapHistoricoCollection = new PosicaoSwapHistoricoCollection();
        posicaoSwapHistoricoCollection.Load(posicaoSwapHistoricoQuery);

        foreach (PosicaoSwapHistorico posicaoSwapHistorico in posicaoSwapHistoricoCollection)
        {
            EventoFundoSwap eventoFundoSwap = new EventoFundoSwap();

            eventoFundoSwap.IdEventoFundo = idEventoFundo;
            eventoFundoSwap.NumeroContrato = posicaoSwapHistorico.NumeroContrato;
            eventoFundoSwap.DataEmissao = posicaoSwapHistorico.DataEmissao;
            eventoFundoSwap.DataVencimento = posicaoSwapHistorico.DataVencimento;
            eventoFundoSwap.TipoPonta = posicaoSwapHistorico.TipoPonta;
            eventoFundoSwap.IdIndice = posicaoSwapHistorico.IdIndice;
            eventoFundoSwap.TipoPontaContraParte = posicaoSwapHistorico.TipoPontaContraParte;
            eventoFundoSwap.IdIndiceContraParte = posicaoSwapHistorico.IdIndiceContraParte;
            eventoFundoSwap.Quantidade = 1;
            eventoFundoSwap.QuantidadeOriginal = 1;
            eventoFundoSwap.TipoMercado = (int)TipoMercado.Swap;
            eventoFundoSwap.Save();
        }

        //Posicao Liquidacao
        LiquidacaoHistoricoCollection liquidacaoHistoricoCollection = new LiquidacaoHistoricoCollection();
        liquidacaoHistoricoCollection.Query.Select(liquidacaoHistoricoCollection.Query.Descricao,
                                                   liquidacaoHistoricoCollection.Query.DataLancamento,
                                                   liquidacaoHistoricoCollection.Query.DataVencimento,
                                                   liquidacaoHistoricoCollection.Query.Valor.Sum(),
                                                   liquidacaoHistoricoCollection.Query.Valor.Count().As("Contador"));
        liquidacaoHistoricoCollection.Query.Where(liquidacaoHistoricoCollection.Query.DataHistorico.Equal(dataPosicao));
        liquidacaoHistoricoCollection.Query.Where(liquidacaoHistoricoCollection.Query.IdCliente.Equal(idCarteiraOrigem));
        liquidacaoHistoricoCollection.Query.Where(liquidacaoHistoricoCollection.Query.Origem.NotIn(801, 802, 810, 811, 820, 821, 830, 840, 841, 890, 891));
        liquidacaoHistoricoCollection.Query.Where(liquidacaoHistoricoCollection.Query.DataVencimento.GreaterThan(dataPosicao));
        liquidacaoHistoricoCollection.Query.Where(liquidacaoHistoricoCollection.Query.Valor.NotEqual(0));
        liquidacaoHistoricoCollection.Query.GroupBy(liquidacaoHistoricoCollection.Query.Descricao,
                                                   liquidacaoHistoricoCollection.Query.DataLancamento,
                                                   liquidacaoHistoricoCollection.Query.DataVencimento);
        liquidacaoHistoricoCollection.Query.Load();

        foreach (LiquidacaoHistorico liquidacaoHistorico in liquidacaoHistoricoCollection)
        {
            EventoFundoLiquidacao eventoFundoLiquidacao = new EventoFundoLiquidacao();

            eventoFundoLiquidacao.IdEventoFundo = idEventoFundo;
            eventoFundoLiquidacao.TipoMercado = (int)TipoMercado.Liquidacao;
            eventoFundoLiquidacao.DataLancamento = liquidacaoHistorico.DataLancamento;
            eventoFundoLiquidacao.DataVencimento = liquidacaoHistorico.DataVencimento;
            eventoFundoLiquidacao.Descricao = liquidacaoHistorico.Descricao;
            eventoFundoLiquidacao.Valor = liquidacaoHistorico.Valor;
            eventoFundoLiquidacao.Quantidade = (liquidacaoHistorico.Valor * percentual) / 100;
            eventoFundoLiquidacao.QuantidadeConstante = 1;
            eventoFundoLiquidacao.Contador = Convert.ToInt32(liquidacaoHistorico.GetColumn("Contador"));

            eventoFundoLiquidacao.Save();
        }

        //Posicao Caixa
        SaldoCaixaCollection saldoCaixaCollection = new SaldoCaixaCollection();
        saldoCaixaCollection.Query.Where(saldoCaixaCollection.Query.Data.Equal(dataPosicao));
        saldoCaixaCollection.Query.Where(saldoCaixaCollection.Query.IdCliente.Equal(idCarteiraOrigem));
        saldoCaixaCollection.Query.Load();

        foreach (SaldoCaixa saldoCaixa in saldoCaixaCollection)
        {
            EventoFundoCaixa eventoFundoCaixa = new EventoFundoCaixa();

            eventoFundoCaixa.IdEventoFundo = idEventoFundo;
            eventoFundoCaixa.TipoMercado = (int)TipoMercado.Caixa;
            eventoFundoCaixa.Data = saldoCaixa.Data;
            eventoFundoCaixa.IdConta = saldoCaixa.IdConta;
            eventoFundoCaixa.SaldoAbertura = saldoCaixa.SaldoAbertura;
            eventoFundoCaixa.Valor = (saldoCaixa.SaldoFechamento.GetValueOrDefault(0) * percentual) / 100;
            eventoFundoCaixa.QuantidadeConstante = 1;
            eventoFundoCaixa.Descricao = "Valor em Caixa";

            eventoFundoCaixa.Save();
        }

    }

    /// <summary>
    /// Tratamento Delete
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            #region Delete
            List<object> keyValuesIdEventoFundo = gridCadastro.GetSelectedFieldValues(EventoFundoMetadata.ColumnNames.IdEventoFundo);

            for (int i = 0; i < keyValuesIdEventoFundo.Count; i++)
            {
                int idEventoFundo = Convert.ToInt32(keyValuesIdEventoFundo[i]);

                EventoFundo eventoFundo = new EventoFundo();
                if (eventoFundo.LoadByPrimaryKey(idEventoFundo))
                {
                    EventoFundo eventoFundoClone = (EventoFundo)Utilitario.Clone(eventoFundo);
                    //

                    this.excluiDetalheEventos(eventoFundo);

                    eventoFundo.MarkAsDeleted();
                    eventoFundo.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de Incorporação/Fusão/Cisão Fundo - Operacao: Delete Evento Fundo: " + idEventoFundo + "; " + UtilitarioWeb.ToString(eventoFundoClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
            #endregion
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    private void excluiDetalheEventos(EventoFundo eventoFundo)
    {
        HistoricoCota historicoCota = new HistoricoCota();
        historicoCota.ExcluiPosicoesGeradas(eventoFundo, 0);
        historicoCota.ExcluiMovimentosGerados(eventoFundo, 0);

        EventoFundoBMFCollection eventoFundoBMFCollection = new EventoFundoBMFCollection();
        eventoFundoBMFCollection.Query.Where(eventoFundoBMFCollection.Query.IdEventoFundo.Equal(eventoFundo.IdEventoFundo));
        eventoFundoBMFCollection.Query.Load();
        eventoFundoBMFCollection.MarkAllAsDeleted();
        eventoFundoBMFCollection.Save();

        EventoFundoBolsaCollection eventoFundoBolsaCollection = new EventoFundoBolsaCollection();
        eventoFundoBolsaCollection.Query.Where(eventoFundoBolsaCollection.Query.IdEventoFundo.Equal(eventoFundo.IdEventoFundo));
        eventoFundoBolsaCollection.Query.Load();
        eventoFundoBolsaCollection.MarkAllAsDeleted();
        eventoFundoBolsaCollection.Save();

        EventoFundoRendaFixaCollection eventoFundoRendaFixaCollection = new EventoFundoRendaFixaCollection();
        eventoFundoRendaFixaCollection.Query.Where(eventoFundoRendaFixaCollection.Query.IdEventoFundo.Equal(eventoFundo.IdEventoFundo));
        eventoFundoRendaFixaCollection.Query.Load();
        eventoFundoRendaFixaCollection.MarkAllAsDeleted();
        eventoFundoRendaFixaCollection.Save();

        EventoFundoSwapCollection eventoFundoSwapCollection = new EventoFundoSwapCollection();
        eventoFundoSwapCollection.Query.Where(eventoFundoSwapCollection.Query.IdEventoFundo.Equal(eventoFundo.IdEventoFundo));
        eventoFundoSwapCollection.Query.Load();
        eventoFundoSwapCollection.MarkAllAsDeleted();
        eventoFundoSwapCollection.Save();

        EventoFundoFundoCollection eventoFundoFundoCollection = new EventoFundoFundoCollection();
        eventoFundoFundoCollection.Query.Where(eventoFundoBolsaCollection.Query.IdEventoFundo.Equal(eventoFundo.IdEventoFundo));
        eventoFundoFundoCollection.Query.Load();
        eventoFundoFundoCollection.MarkAllAsDeleted();
        eventoFundoFundoCollection.Save();

        EventoFundoCautelaCollection eventoFundoCautelaCollection = new EventoFundoCautelaCollection();
        eventoFundoCautelaCollection.Query.Where(eventoFundoCautelaCollection.Query.IdEventoFundo.Equal(eventoFundo.IdEventoFundo));
        eventoFundoCautelaCollection.Query.Load();
        eventoFundoCautelaCollection.MarkAllAsDeleted();
        eventoFundoCautelaCollection.Save();

        EventoFundoLiquidacaoCollection eventoFundoLiquidacaoCollection = new EventoFundoLiquidacaoCollection();
        eventoFundoLiquidacaoCollection.Query.Where(eventoFundoLiquidacaoCollection.Query.IdEventoFundo.Equal(eventoFundo.IdEventoFundo));
        eventoFundoLiquidacaoCollection.Query.Load();
        eventoFundoLiquidacaoCollection.MarkAllAsDeleted();
        eventoFundoLiquidacaoCollection.Save();

        EventoFundoCaixaCollection eventoFundoCaixaCollection = new EventoFundoCaixaCollection();
        eventoFundoCaixaCollection.Query.Where(eventoFundoCaixaCollection.Query.IdEventoFundo.Equal(eventoFundo.IdEventoFundo));
        eventoFundoCaixaCollection.Query.Load();
        eventoFundoCaixaCollection.MarkAllAsDeleted();
        eventoFundoCaixaCollection.Save();

        AgendaComeCotasCollection agendaComeCotasCollection = new AgendaComeCotasCollection();
        agendaComeCotasCollection.Query.Where(agendaComeCotasCollection.Query.DataLancamento.Equal(eventoFundo.DataPosicao),
                                              agendaComeCotasCollection.Query.TipoEvento.Equal(FonteOperacaoCotista.CisaoIncorporacaoFusao));
        agendaComeCotasCollection.Query.Load();
        agendaComeCotasCollection.MarkAllAsDeleted();
        agendaComeCotasCollection.Save();

    }

    /// <summary>
    /// Foco Inicial e Mensagem de Data
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e)
    {

        base.gridCadastro_PreRender(sender, e);

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditCarteira_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
        {
            (sender as ASPxButtonEdit).Enabled = false;
            (sender as ASPxButtonEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "CarteiraOrigem")
        {
            string idCarteira = Convert.ToString(e.GetListSourceFieldValue("IdCarteira"));
            string apelido = Convert.ToString(e.GetListSourceFieldValue("Apelido"));
            e.Value = idCarteira + " - " + apelido;
        }

        if (e.Column.FieldName == "CarteiraDestino")
        {
            string idCarteira = Convert.ToString(e.GetListSourceFieldValue("IdCarteira1"));
            string apelido = Convert.ToString(e.GetListSourceFieldValue("Apelido1"));
            e.Value = idCarteira + " - " + apelido;
        }

    }

    /// <summary>
    /// Atribui mascara dinamicamente para os campos dependendo do tipo de campo
    /// OBS: O método é um AspxCallBackPanel, ele funciona como o UpdatePanel.
    /// Sua utilização é necessária para que não ocorra o postback na página
    /// e a mascara perca o seu valor.
    /// </summary>
    protected void cbPanel_Callback(object source, CallbackEventArgsBase e)
    {
        ParametroAdministradorFundoInvestimentoCollection parametroCollection = new ParametroAdministradorFundoInvestimentoCollection();
        CarteiraCollection carteiraCollection = new CarteiraCollection();

        ASPxCallbackPanel cbPanel = gridCadastro.FindEditFormTemplateControl("cbPanel") as ASPxCallbackPanel;
        ASPxRadioButtonList rblAliquotaIR = cbPanel.FindControl("rblAliquotaIR") as ASPxRadioButtonList;
        ASPxRadioButtonList rblExecucaoRecolhimento = cbPanel.FindControl("rblExecucaoRecolhimento") as ASPxRadioButtonList;
        ASPxDateEdit textDataRecolhimento = cbPanel.FindControl("textDataRecolhimento") as ASPxDateEdit;

        carteiraCollection.Query.Select(carteiraCollection.Query.IdAgenteAdministrador);
        carteiraCollection.Query.Where(carteiraCollection.Query.IdCarteira.Equal(Convert.ToInt32(e.Parameter)));
        carteiraCollection.Query.Load();

        if (carteiraCollection.Count > 0)
        {
            parametroCollection.Query.Select();
            parametroCollection.Query.Where(parametroCollection.Query.Administrador.Equal(carteiraCollection[0].IdAgenteAdministrador));
            parametroCollection.Query.Load();
        }

        if (parametroCollection.Count > 0)
        {
            //Carteira possui parâmetros cadastrados
            VerificaCarteiraParametro = true;
            rblAliquotaIR.Value = parametroCollection[0].AliquotaIR;
            //Solicitação de fixar valor para não recolhimento
            rblExecucaoRecolhimento.Value = 1;
            textDataRecolhimento.Value = parametroCollection[0].DataRecolhimento;
        }
        else
        {
            //Carteira não possui parâmetros cadastrados
            VerificaCarteiraParametro = false;
            rblAliquotaIR.Value = -1;
            rblExecucaoRecolhimento.Value = -1;
            textDataRecolhimento.Value = null;
        }
    }

    /// <summary>
    /// Verifica se o evento a ser cadastrado já possui alguem evento cadastrado na data definida
    /// </summary>
    /// <param name="idCarteira">Codigo da carteira a ser analisada</param>
    /// <param name="data">data do evento </param>
    /// <returns></returns>
    private string VerificaEventoExistente(int? idCarteira, DateTime? data)
    {
        string mensagem = string.Empty;

        #region Desenquadramento Tributário
        DesenquadramentoTributarioCollection dtc = new DesenquadramentoTributarioCollection();
        dtc.Query.Where(dtc.Query.IdCarteira == idCarteira, dtc.Query.Data == data);
        dtc.Query.Load();

        if (dtc.Count > 0)
            mensagem = "Fundo já cadastrado para o evento de 'Desenquadramento Tributário'";
        #endregion

        #region Forma Condominio
        FundoInvestimentoFormaCondominioCollection fifc = new FundoInvestimentoFormaCondominioCollection();
        fifc.Query.Where(fifc.Query.IdCarteira == idCarteira, fifc.Query.DataInicioVigencia == data);
        fifc.Query.Load();

        if (fifc.Count > 0)
            mensagem = "Fundo já cadastrado para o evento de 'Fundo Investimento Forma Condomínio'";
        #endregion

        return mensagem;
    }

}