﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using Financial.Investidor;
using Financial.ContaCorrente;
using System.Drawing;
using Financial.ContaCorrente.Enums;
using Financial.Security;
using Financial.Fundo;
using Financial.Investidor.Enums;
using Financial.Common.Enums;
using Financial.Fundo.Enums;
using Financial.Web.Common;
using Financial.InvestidorCotista;
using Financial.InvestidorCotista.Enums;
using System.Threading;
using Financial.Web.Util;
using Financial.Security.Enums;
using EntitySpaces.Interfaces;

public partial class CadastrosBasicos_DetalheResgateFundo : CadastroBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        this.HasFiltro = true;
        this.HasPopupCliente = true;
        this.HasPopupCarteira = true;
        
        if (IsCallback) {
            //ASPxComboBox dropPosicao = gridCadastro.FindEditFormTemplateControl("dropPosicao") as ASPxComboBox;
            //dropPosicao.SelectedIndex = -1;
        }

        if (IsPostBack) {
            //ASPxComboBox dropPosicao = gridCadastro.FindEditFormTemplateControl("dropPosicao") as ASPxComboBox;
            //dropPosicao.DataBind();
        }

        base.Page_Load(sender, e);
    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ClienteCollection coll = new ClienteCollection();
        coll.BuscaClientesComAcesso(Context.User.Identity.Name);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CarteiraCollection coll = new CarteiraCollection();

        CarteiraQuery carteiraQuery = new CarteiraQuery("A");
        ClienteQuery clienteQuery = new ClienteQuery("C");
        UsuarioQuery usuarioQuery = new UsuarioQuery("U");
        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");

        carteiraQuery.Select(carteiraQuery.IdCarteira, carteiraQuery.Nome, carteiraQuery.Apelido);
        //
        carteiraQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == carteiraQuery.IdCarteira);
        carteiraQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        carteiraQuery.InnerJoin(usuarioQuery).On(permissaoClienteQuery.IdUsuario == usuarioQuery.IdUsuario);
        //
        carteiraQuery.Where(usuarioQuery.Login == Context.User.Identity.Name,
                            clienteQuery.StatusAtivo.Equal(StatusAtivoCliente.Ativo),
                            clienteQuery.IdTipo.In(TipoClienteFixo.Clube, TipoClienteFixo.Fundo, TipoClienteFixo.FDIC),
                            clienteQuery.TipoControle.NotEqual((byte)TipoControleCliente.ApenasCotacao));

        coll.Load(carteiraQuery);

        carteiraQuery = new CarteiraQuery("A");
        clienteQuery = new ClienteQuery("C");
        //
        carteiraQuery.Select(carteiraQuery.IdCarteira, carteiraQuery.Nome, carteiraQuery.Apelido);
        carteiraQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == carteiraQuery.IdCarteira);
        carteiraQuery.Where(clienteQuery.TipoControle.Equal((byte)TipoControleCliente.ApenasCotacao),
                            clienteQuery.IdTipo.In(TipoClienteFixo.Clube, TipoClienteFixo.Fundo, TipoClienteFixo.FDIC),
                            clienteQuery.StatusAtivo.Equal(StatusAtivoCliente.Ativo));
        //
        CarteiraCollection carteiraCollection2 = new CarteiraCollection();
        carteiraCollection2.Load(carteiraQuery);
        //
        coll.Combine(carteiraCollection2);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }        

    protected void EsDSDetalheResgateFundo_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
        DetalheResgateFundoQuery detalheResgateFundoQuery = new DetalheResgateFundoQuery("D");
        OperacaoFundoQuery operacaoFundoQuery = new OperacaoFundoQuery("O");
        ClienteQuery clienteQuery = new ClienteQuery("C");
        CarteiraQuery carteiraQuery = new CarteiraQuery("A");

        detalheResgateFundoQuery.Select(detalheResgateFundoQuery,
                                  clienteQuery.Apelido.As("ApelidoCliente"),
                                  carteiraQuery.Apelido.As("ApelidoCarteira"),
                                  operacaoFundoQuery.DataOperacao,
                                  operacaoFundoQuery.Fonte);

        detalheResgateFundoQuery.InnerJoin(clienteQuery).On(detalheResgateFundoQuery.IdCliente == clienteQuery.IdCliente);
        detalheResgateFundoQuery.InnerJoin(carteiraQuery).On(detalheResgateFundoQuery.IdCarteira == carteiraQuery.IdCarteira);
        detalheResgateFundoQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        detalheResgateFundoQuery.InnerJoin(operacaoFundoQuery).On(detalheResgateFundoQuery.IdOperacao == operacaoFundoQuery.IdOperacao);
        //
        detalheResgateFundoQuery.Where(permissaoClienteQuery.IdUsuario == idUsuario);

        if (!String.IsNullOrEmpty(btnEditCodigoClienteFiltro.Text)) {
            detalheResgateFundoQuery.Where(detalheResgateFundoQuery.IdCliente == Convert.ToInt32(btnEditCodigoClienteFiltro.Text));
        }
        
        if (!String.IsNullOrEmpty(btnEditCodigoCarteiraFiltro.Text)) {
            detalheResgateFundoQuery.Where(detalheResgateFundoQuery.IdCarteira == Convert.ToInt32(btnEditCodigoCarteiraFiltro.Text));
        }

        if (!String.IsNullOrEmpty(textIdOperacaoFiltro.Text)) {
            detalheResgateFundoQuery.Where(detalheResgateFundoQuery.IdOperacao == Convert.ToInt32(textIdOperacaoFiltro.Text));
        }

        if (!String.IsNullOrEmpty(textIdPosicaoResgatadaFiltro.Text)) {
            detalheResgateFundoQuery.Where(detalheResgateFundoQuery.IdPosicaoResgatada == Convert.ToInt32(textIdPosicaoResgatadaFiltro.Text));
        }
        //
        detalheResgateFundoQuery.OrderBy(detalheResgateFundoQuery.IdCarteira.Descending,
                                         detalheResgateFundoQuery.IdCliente.Descending,
                                         detalheResgateFundoQuery.IdOperacao.Descending,
                                         detalheResgateFundoQuery.IdPosicaoResgatada.Descending);
        //
        DetalheResgateFundoCollection coll = new DetalheResgateFundoCollection();
        coll.Load(detalheResgateFundoQuery);
        //        
        e.Collection = coll;
    }

    protected void EsDSOperacaoFundo_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
        OperacaoFundoQuery operacaoFundoQuery = new OperacaoFundoQuery("O");
        ClienteQuery clienteQuery = new ClienteQuery("C");
        CarteiraQuery carteiraQuery = new CarteiraQuery("A");

        operacaoFundoQuery.Select(operacaoFundoQuery,
                                  clienteQuery.Apelido.As("ApelidoCliente"),
                                  carteiraQuery.Apelido.As("ApelidoCarteira"));
        //
        operacaoFundoQuery.InnerJoin(clienteQuery).On(operacaoFundoQuery.IdCliente == clienteQuery.IdCliente);
        operacaoFundoQuery.InnerJoin(carteiraQuery).On(operacaoFundoQuery.IdCarteira == carteiraQuery.IdCarteira);
        operacaoFundoQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        //
        operacaoFundoQuery.Where(permissaoClienteQuery.IdUsuario == idUsuario);

        operacaoFundoQuery.OrderBy(operacaoFundoQuery.DataOperacao.Descending);

        OperacaoFundoCollection coll = new OperacaoFundoCollection();
        coll.Load(operacaoFundoQuery);

        e.Collection = coll;
    }

    protected void EsDSPosicaoFundo_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {

        #region Pega IdCliente e IdCarteira direto da tela
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        //

        int? idCliente = null;
        if (!String.IsNullOrEmpty(btnEditCodigoCliente.Text.Trim())) {
            idCliente = Convert.ToInt32(btnEditCodigoCliente.Text.Trim());
        }
        //
        int? idCarteira = null;
        if (!String.IsNullOrEmpty(btnEditCodigoCarteira.Text.Trim())) {
            idCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text.Trim());
        }
        #endregion
        
        if (idCliente.HasValue && idCarteira.HasValue) {
            Usuario usuario = new Usuario();
            usuario.BuscaUsuario(Context.User.Identity.Name);
            int idUsuario = usuario.IdUsuario.Value;

            #region PosicaoFundo
            PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
            PosicaoFundoQuery posicaoFundoQuery = new PosicaoFundoQuery("O");
            ClienteQuery clienteQuery = new ClienteQuery("C");
            CarteiraQuery carteiraQuery = new CarteiraQuery("A");

            posicaoFundoQuery.Select(posicaoFundoQuery
                                    , "<'1' as DataAplicacaoFormatada>"                                     
                                    );
            //
            posicaoFundoQuery.InnerJoin(clienteQuery).On(posicaoFundoQuery.IdCliente == clienteQuery.IdCliente);
            posicaoFundoQuery.InnerJoin(carteiraQuery).On(posicaoFundoQuery.IdCarteira == carteiraQuery.IdCarteira);
            posicaoFundoQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
            //
            posicaoFundoQuery.Where(permissaoClienteQuery.IdUsuario == idUsuario,
                                    posicaoFundoQuery.Quantidade != 0,
                                    posicaoFundoQuery.IdCliente == idCliente.Value,
                                    posicaoFundoQuery.IdCarteira == idCarteira.Value
                                   );

            posicaoFundoQuery.OrderBy(posicaoFundoQuery.IdPosicao.Ascending);

            PosicaoFundoCollection coll = new PosicaoFundoCollection();
            coll.Load(posicaoFundoQuery);
            //

            //PosicaoFundo p = new PosicaoFundo();
            //p.IdPosicao = -1;
            //p.ValorAplicacao = 0;
            //p.DataAplicacao = DateTime.Now;
            //p.CotaAplicacao = 0;
            //p.ValorLiquido = 0;
            //p.Quantidade = 0;
            ////
            //coll.AttachEntity(p);
            ////
            //coll.Sort = PosicaoFundoMetadata.ColumnNames.IdPosicao + " DESC";
            //

            // Formato de Data pois AspxCombox Não faz
            for (int i = 0; i < coll.Count; i++) {
                string dataAplicacao = coll[i].DataAplicacao.Value.ToShortDateString();
                coll[i].SetColumn("DataAplicacaoFormatada", dataAplicacao);
            }

            e.Collection = coll;
            #endregion
        }
        else {

            PosicaoFundoCollection p = new PosicaoFundoCollection();
            //           
            p.CreateColumnsForBinding();
            p.AddColumn("IdPosicao", typeof(System.Int32));
            p.AddColumn("ValorAplicacao", typeof(System.Decimal));
            p.AddColumn("DataAplicacao", typeof(System.DateTime));            
            p.AddColumn("CotaAplicacao", typeof(System.Decimal));
            p.AddColumn("ValorLiquido", typeof(System.Decimal));
            p.AddColumn("Quantidade", typeof(System.Decimal));
            //                        
            e.Collection = p;
        }
    }
    #endregion

    /// <summary>
    /// Ocorre quando escolhe a OperaçãoFundo
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dropPosicao_Callback(object source, CallbackEventArgsBase e) {
        string[] parameter = e.Parameter.Split(';');
        //        
        int? idCliente = null;
        if (!String.IsNullOrEmpty(parameter[0]) && parameter[0] != "null") {
            idCliente = Convert.ToInt32(parameter[0]);
        }
        //
        int? idCarteira = null;
        if (!String.IsNullOrEmpty(parameter[1]) && parameter[1] != "null") {
            idCarteira = Convert.ToInt32(parameter[1]);
        }

        FillComboPosicaoFundo(idCliente, idCarteira);
    }

    /// <summary>
    /// Faz O Binding do Combo de PosicaoFundo
    /// Não precisou dos parametros
    /// </summary>
    /// <param name="idCliente"></param>
    /// <param name="idcarteira"></param>
    protected void FillComboPosicaoFundo(int? idCliente, int? idCarteira) {

        ASPxComboBox dropPosicao = gridCadastro.FindEditFormTemplateControl("dropPosicao") as ASPxComboBox;
        
        //OBS: Passar parametro para EsDataSource - não precisou
        //Session["idCliente"] = idCliente;
        //Session["idCarteira"] = idCarteira;

        dropPosicao.DataBind();

        //dropPosicao.Text = "10305";
//        dropPosicao.SelectedIndex = -1;
        //dropPosicao.SelectedIndex = 4;

        //dropPosicao.Text = "";
        //dropPosicao.Value = "";
        //dropPosicao.TextField = "";
    }

    #region CallBack Filtro
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") {
                if (cliente.IsAtivo) {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    nome = permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name)
                           ? cliente.str.Apelido
                           : "no_access";
                }
                else {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        string resultado = "";
        //
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "") {
                if (cliente.IsAtivo) {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoCliente(idCliente, HttpContext.Current.User.Identity.Name)) {
                        nome = cliente.str.Apelido;
                        DateTime dataDia = cliente.DataDia.Value;
                        byte status = cliente.Status.Value;

                        if (status == (byte)StatusCliente.Divulgado && gridCadastro.IsEditing) {
                            resultado = "status_closed";
                        }
                        else {
                            resultado = nome + "|" + dataDia.ToString().Substring(0, 10) + "|" + Thread.CurrentThread.CurrentCulture.Name;
                        }
                    }
                    else {
                        resultado = "no_access";
                    }
                }
                else {
                    resultado = "no_active";
                }
            }
        }
        e.Result = resultado;
    }

    #endregion

    /// <summary>
    /// Controla Visibilidade dos Campos no modo Insert e Update
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void panelEdicao_Load(object sender, EventArgs e) {

        Control c1 = this.gridCadastro.FindEditFormTemplateControl("divInsert");
        Control c2 = this.gridCadastro.FindEditFormTemplateControl("divUpdate");

        //Insert
        if (this.gridCadastro.IsNewRowEditing) {
            if (c1 != null) { c1.Visible = true; }
            if (c2 != null) { c2.Visible = false; }
        }

        // Update
        else if (this.gridCadastro.IsEditing) {
            if (c1 != null) { c1.Visible = false; }            
            if (c2 != null) { c2.Visible = true; }
        }

        base.panelEdicao_Load(sender, e);
    }
    
    /// <summary>
    /// Update
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) 
    {
        ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
        ASPxSpinEdit textPerformance = gridCadastro.FindEditFormTemplateControl("textPerformance") as ASPxSpinEdit;
        ASPxSpinEdit textValorBruto = gridCadastro.FindEditFormTemplateControl("textValorBruto") as ASPxSpinEdit;
        ASPxSpinEdit textValorLiquido = gridCadastro.FindEditFormTemplateControl("textValorLiquido") as ASPxSpinEdit;
        ASPxSpinEdit textValorIR = gridCadastro.FindEditFormTemplateControl("textValorIR") as ASPxSpinEdit;
        ASPxSpinEdit textValorIOF = gridCadastro.FindEditFormTemplateControl("textValorIOF") as ASPxSpinEdit;
        ASPxSpinEdit textValorPerformance = gridCadastro.FindEditFormTemplateControl("textValorPerformance") as ASPxSpinEdit;

        DetalheResgateFundo dr = new DetalheResgateFundo();
        //
        ASPxSpinEdit textIdOperacao = gridCadastro.FindEditFormTemplateControl("textIdOperacao") as ASPxSpinEdit;
        ASPxSpinEdit textIdPosicao = gridCadastro.FindEditFormTemplateControl("textIdPosicao") as ASPxSpinEdit;

        int chaveIdOperacao = Convert.ToInt32(textIdOperacao.Text);
        int chaveIdPosicaoResgatada = Convert.ToInt32(textIdPosicao.Text);

        if (dr.LoadByPrimaryKey(chaveIdOperacao, chaveIdPosicaoResgatada)) 
        {
            dr.Quantidade = Convert.ToDecimal(textQuantidade.Text);
            dr.ValorPerformance = Convert.ToDecimal(textPerformance.Text);
            dr.ValorBruto = Convert.ToDecimal(textValorBruto.Text);
            dr.ValorLiquido = Convert.ToDecimal(textValorLiquido.Text);
            dr.ValorIR = Convert.ToDecimal(textValorIR.Text);
            dr.ValorIOF = Convert.ToDecimal(textValorIOF.Text);
            
            dr.Save();

            OperacaoFundo operacaoFundo = new OperacaoFundo();
            operacaoFundo.LoadByPrimaryKey(chaveIdOperacao);

            DetalheResgateFundo detalheResgateFundo = new DetalheResgateFundo();
            detalheResgateFundo.Query.Select(detalheResgateFundo.Query.Quantidade.Sum(),
                                             detalheResgateFundo.Query.ValorBruto.Sum(),
                                             detalheResgateFundo.Query.ValorIOF.Sum(),
                                             detalheResgateFundo.Query.ValorIR.Sum(),
                                             detalheResgateFundo.Query.ValorLiquido.Sum(),
                                             detalheResgateFundo.Query.ValorPerformance.Sum());
            detalheResgateFundo.Query.Where(detalheResgateFundo.Query.IdOperacao.Equal(chaveIdOperacao));
            detalheResgateFundo.Query.Load();

            if (detalheResgateFundo.ValorBruto.HasValue)
            {
                operacaoFundo.Quantidade = detalheResgateFundo.Quantidade.Value;
                operacaoFundo.ValorBruto = detalheResgateFundo.ValorBruto.Value;
                operacaoFundo.ValorIOF = detalheResgateFundo.ValorIOF.Value;
                operacaoFundo.ValorIR = detalheResgateFundo.ValorIR.Value;
                operacaoFundo.ValorLiquido = detalheResgateFundo.ValorLiquido.Value;
                operacaoFundo.ValorPerformance = detalheResgateFundo.ValorPerformance.Value;
            }

            operacaoFundo.Fonte = (byte)FonteOperacaoFundo.Ajustado;

            operacaoFundo.Save();

            if (operacaoFundo.TipoOperacao.Value == (byte)TipoOperacaoFundo.ComeCotas)
            {
                OperacaoFundoCollection operacaoFundoCollection = new OperacaoFundoCollection();
                operacaoFundoCollection.Query.Where(operacaoFundoCollection.Query.TipoOperacao.Equal((byte)TipoOperacaoFundo.ComeCotas),
                                                    operacaoFundoCollection.Query.IdCliente.Equal(operacaoFundo.IdCliente.Value),
                                                    operacaoFundoCollection.Query.IdCarteira.Equal(operacaoFundo.IdCarteira.Value));
                operacaoFundoCollection.Query.Load();

                foreach (OperacaoFundo operacaoFundoAjuste in operacaoFundoCollection)
                {
                    operacaoFundoAjuste.Fonte = (byte)FonteOperacaoFundo.Ajustado;
                }

                operacaoFundoCollection.Save();
            }

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de DetalheResgateFundo - Operacao: Update DetalheResgateFundo: " + textIdOperacao.Text + "; " + textIdPosicao.Text + UtilitarioWeb.ToString(dr),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// Inserção
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) 
    {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }
  
    /// <summary>
    /// Erro
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";

        if (!gridCadastro.IsEditing) {
            #region Delete - Confere Deleção Não Permitida
            //
            List<object> keyValuesIdCliente = gridCadastro.GetSelectedFieldValues(DetalheResgateFundoMetadata.ColumnNames.IdCliente);
            List<object> keyValuesDataOperacao = gridCadastro.GetSelectedFieldValues(OperacaoFundoMetadata.ColumnNames.DataOperacao);
            //
            for (int i = 0; i < keyValuesIdCliente.Count; i++) {
                int idCliente = Convert.ToInt32(keyValuesIdCliente[i]);
                DateTime dataOperacao = Convert.ToDateTime(keyValuesDataOperacao[i]);

                Cliente cliente = new Cliente();
                if (cliente.LoadByPrimaryKey(idCliente)) {
                    DateTime dataDia = cliente.DataDia.Value;
                    byte status = cliente.Status.Value;

                    string clienteDescricao = cliente.Apelido + " (" + cliente.IdCliente.ToString() + ")";
                    
                    if (status == (byte)StatusCliente.Divulgado) {
                        e.Result = "Cliente " + clienteDescricao + " está fechado! Deleção cancelada.";
                        return;
                    }
                    else if (dataDia > dataOperacao) {
                        e.Result = "Data Operação: " + dataOperacao.ToString().Substring(0, 10) + " anterior à data do cliente " + clienteDescricao + "! Deleção cancelada.";
                        return;
                    }
                }
            }
            #endregion
        }
        else {
            #region Campos Obrigatórios
            ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
            ASPxSpinEdit textPerformance = gridCadastro.FindEditFormTemplateControl("textPerformance") as ASPxSpinEdit;
            ASPxSpinEdit textValorBruto = gridCadastro.FindEditFormTemplateControl("textValorBruto") as ASPxSpinEdit;
            ASPxSpinEdit textValorLiquido = gridCadastro.FindEditFormTemplateControl("textValorLiquido") as ASPxSpinEdit;
            ASPxSpinEdit textValorIR = gridCadastro.FindEditFormTemplateControl("textValorIR") as ASPxSpinEdit;
            ASPxSpinEdit textValorIOF = gridCadastro.FindEditFormTemplateControl("textValorIOF") as ASPxSpinEdit;
            
            // Modo Insert 
            ASPxGridLookup dropOperacaoFundo = gridCadastro.FindEditFormTemplateControl("dropOperacaoFundo") as ASPxGridLookup;
            ASPxComboBox dropPosicaoFundo = gridCadastro.FindEditFormTemplateControl("dropPosicao") as ASPxComboBox;

            #region Campos obrigatórios
            List<Control> controles = new List<Control>(new Control[] {
                        textQuantidade, textPerformance, textValorBruto, textValorLiquido, textValorIR, textValorIOF
            });

            // Se for modo Update
            if (!gridCadastro.IsNewRowEditing) {
                if (base.TestaObrigatorio(controles) != "") {
                    e.Result = "Campos com * são obrigatórios!";
                    return;
                }
            }

            // Se for modo Insert
            if (this.gridCadastro.IsNewRowEditing) {
                controles.Add(dropOperacaoFundo);
                //controles.Add(dropPosicaoFundo);  // SelectIndex = -1

                // Necessario comparar dropPosicaoFundo separadamente
                if (base.TestaObrigatorio(controles) != "" || dropPosicaoFundo.Text.Trim() == "") {
                    e.Result = "Campos com * são obrigatórios!";
                    return;
                }

                DetalheResgateFundo detalheResgateFundo = new DetalheResgateFundo();
                string textoOperacaoFundo = dropOperacaoFundo.Text.Split('-')[0].Trim();
                int idOperacao = Convert.ToInt32(textoOperacaoFundo);

                string textoDropPosicao = dropPosicaoFundo.Text.Split('-')[0].Trim();
                int idPosicaoResgatada = Convert.ToInt32(textoDropPosicao);
                //
                if (detalheResgateFundo.LoadByPrimaryKey(idOperacao, idPosicaoResgatada)) {
                    e.Result = "Registro já existente.";
                    return;
                }
            }
            #endregion

            #endregion
        }
    }


    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();

        e.Result = "";
        if (e.Parameter == "clone")
        {
            //e.Result = 0;
        }
        else
        {
            e.Result = "Operação feita com sucesso.";
        }
    }

    private void SalvarNovo()
    {
        ASPxSpinEdit textQuantidade = gridCadastro.FindEditFormTemplateControl("textQuantidade") as ASPxSpinEdit;
        ASPxSpinEdit textPerformance = gridCadastro.FindEditFormTemplateControl("textPerformance") as ASPxSpinEdit;
        ASPxSpinEdit textValorBruto = gridCadastro.FindEditFormTemplateControl("textValorBruto") as ASPxSpinEdit;
        ASPxSpinEdit textValorLiquido = gridCadastro.FindEditFormTemplateControl("textValorLiquido") as ASPxSpinEdit;
        ASPxSpinEdit textValorIR = gridCadastro.FindEditFormTemplateControl("textValorIR") as ASPxSpinEdit;
        ASPxSpinEdit textValorIOF = gridCadastro.FindEditFormTemplateControl("textValorIOF") as ASPxSpinEdit;
        ASPxSpinEdit textValorPerformance = gridCadastro.FindEditFormTemplateControl("textValorPerformance") as ASPxSpinEdit;
        //
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        //
        ASPxGridLookup dropOperacaoFundo = gridCadastro.FindEditFormTemplateControl("dropOperacaoFundo") as ASPxGridLookup;
        ASPxComboBox dropPosicao = gridCadastro.FindEditFormTemplateControl("dropPosicao") as ASPxComboBox;

        DetalheResgateFundo dr = new DetalheResgateFundo();
        //
        string textoDropOperacao = dropOperacaoFundo.Text.Split('-')[0].Trim();
        dr.IdOperacao = Convert.ToInt32(textoDropOperacao);
        //
        string textoDropPosicao = dropPosicao.Text.Split('-')[0].Trim();
        dr.IdPosicaoResgatada = Convert.ToInt32(textoDropPosicao);
        //
        dr.IdCliente = Convert.ToInt32(btnEditCodigoCliente.Text.Trim());
        dr.IdCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text.Trim());
        //
        dr.Quantidade = Convert.ToDecimal(textQuantidade.Text);
        dr.ValorPerformance = Convert.ToDecimal(textPerformance.Text);
        dr.ValorBruto = Convert.ToDecimal(textValorBruto.Text);
        dr.ValorLiquido = Convert.ToDecimal(textValorLiquido.Text);
        dr.ValorIR = Convert.ToDecimal(textValorIR.Text);
        dr.ValorIOF = Convert.ToDecimal(textValorIOF.Text);
        dr.Save();

        OperacaoFundo operacaoFundo = new OperacaoFundo();
        operacaoFundo.LoadByPrimaryKey(dr.IdOperacao.Value);

        DetalheResgateFundo detalheResgateFundo = new DetalheResgateFundo();
        detalheResgateFundo.Query.Select(detalheResgateFundo.Query.Quantidade.Sum(),
                                         detalheResgateFundo.Query.ValorBruto.Sum(),
                                         detalheResgateFundo.Query.ValorIOF.Sum(),
                                         detalheResgateFundo.Query.ValorIR.Sum(),
                                         detalheResgateFundo.Query.ValorLiquido.Sum(),
                                         detalheResgateFundo.Query.ValorPerformance.Sum());
        detalheResgateFundo.Query.Where(detalheResgateFundo.Query.IdOperacao.Equal(dr.IdOperacao.Value));

        if (detalheResgateFundo.ValorBruto.HasValue)
        {
            operacaoFundo.Quantidade = detalheResgateFundo.Quantidade.Value;
            operacaoFundo.ValorBruto = detalheResgateFundo.ValorBruto.Value;
            operacaoFundo.ValorIOF = detalheResgateFundo.ValorIOF.Value;
            operacaoFundo.ValorIR = detalheResgateFundo.ValorIR.Value;
            operacaoFundo.ValorLiquido = detalheResgateFundo.ValorLiquido.Value;
            operacaoFundo.ValorPerformance = detalheResgateFundo.ValorPerformance.Value;
        }

        operacaoFundo.Fonte = (byte)FonteOperacaoFundo.Ajustado;

        operacaoFundo.Save();

        if (operacaoFundo.TipoOperacao.Value == (byte)TipoOperacaoFundo.ComeCotas)
        {
            OperacaoFundoCollection operacaoFundoCollection = new OperacaoFundoCollection();
            operacaoFundoCollection.Query.Where(operacaoFundoCollection.Query.TipoOperacao.Equal((byte)TipoOperacaoFundo.ComeCotas),
                                                operacaoFundoCollection.Query.IdCliente.Equal(operacaoFundo.IdCliente.Value),
                                                operacaoFundoCollection.Query.IdCarteira.Equal(operacaoFundo.IdCarteira.Value));
            operacaoFundoCollection.Query.Load();

            foreach (OperacaoFundo operacaoFundoAjuste in operacaoFundoCollection)
            {
                operacaoFundoAjuste.Fonte = (byte)FonteOperacaoFundo.Ajustado;
            }

            operacaoFundoCollection.Save();
        }

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de DetalheResgateFundo - Operacao: Insert DetalheResgateFundo: " + dr.IdOperacao + ";" + dr.IdPosicaoResgatada + ";" + UtilitarioWeb.ToString(dr),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }


    /// <summary>
    /// Compõe Chave Primaria
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) {
        if (e.Column.FieldName == "CompositeKey") {
            //Compose a primary key value
            string idOperacao = Convert.ToString(e.GetListSourceFieldValue(DetalheResgateFundoMetadata.ColumnNames.IdOperacao));
            string idposicaoResgatada = Convert.ToString(e.GetListSourceFieldValue(DetalheResgateFundoMetadata.ColumnNames.IdPosicaoResgatada));
            e.Value = idOperacao + idposicaoResgatada;
        }
    }
    
    /// <summary>
    /// Exclusão
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValues1 = gridCadastro.GetSelectedFieldValues(DetalheResgateFundoMetadata.ColumnNames.IdOperacao);
            List<object> keyValues2 = gridCadastro.GetSelectedFieldValues(DetalheResgateFundoMetadata.ColumnNames.IdPosicaoResgatada);

            for (int i = 0; i < keyValues1.Count; i++) {
                int idOperacao = Convert.ToInt32(keyValues1[i]);
                int idPosicaoResgatada = Convert.ToInt32(keyValues2[i]);

                DetalheResgateFundo detalheResgateFundo = new DetalheResgateFundo();
                if (detalheResgateFundo.LoadByPrimaryKey(idOperacao, idPosicaoResgatada)) {
                    //
                    DetalheResgateFundo detalheResgateFundoClone = (DetalheResgateFundo)Utilitario.Clone(detalheResgateFundo);
                    //

                    detalheResgateFundo.MarkAsDeleted();
                    detalheResgateFundo.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de DetalheResgateFundo - Operacao: Delete DetalheResgateFundo: " + idOperacao + idPosicaoResgatada + UtilitarioWeb.ToString(detalheResgateFundoClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        //ASPxComboBox dropPosicao = gridCadastro.FindEditFormTemplateControl("dropPosicao") as ASPxComboBox;
        //dropPosicao.DataBind();

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// Chamado antes de Iniciar Grid
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("textQuantidade");
        base.gridCadastro_PreRender(sender, e);
       
        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        //
        StringBuilder texto = new StringBuilder();
        texto.Append("");
        //
        if (btnEditCodigoClienteFiltro != null && btnEditCodigoClienteFiltro.Text != "") {
            texto.Append(" Cliente: ").Append(btnEditCodigoClienteFiltro.Text);
        }
        if (btnEditCodigoCarteiraFiltro != null && btnEditCodigoCarteiraFiltro.Text != "") {
            if (texto.Length > 0) {
                texto.Append(" |");
            }
            texto.Append(" Carteira: ").Append(btnEditCodigoCarteiraFiltro.Text);
        }
        if (textIdOperacaoFiltro != null && textIdOperacaoFiltro.Text != "") {
            if (texto.Length > 0) {
                texto.Append(" |");
            }
            texto.Append(" Id Operação: ").Append(textIdOperacaoFiltro.Text);
        }
        if (textIdPosicaoResgatadaFiltro != null && textIdPosicaoResgatadaFiltro.Text != "") {
            if (texto.Length > 0) {
                texto.Append(" |");
            }
            texto.Append(" Id Posição Resgatada: ").Append(textIdPosicaoResgatadaFiltro.Text);
        }

        labelFiltro.Text = texto.ToString();
    }   
}