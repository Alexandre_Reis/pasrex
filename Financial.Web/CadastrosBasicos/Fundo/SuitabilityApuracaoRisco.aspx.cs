﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Financial.Web.Common;
using Financial.Investidor;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using System.Collections.Generic;
using Financial.Util;
using Financial.CRM;
using Financial.Common.Enums;

using DevExpress.Web;

public partial class CadastrosBasicos_Fundo_SuitabilityApuracaoRisco : CadastroBasePage
{
    protected void EsDSSuitabilityApruracaoRisco_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        SuitabilityApuracaoRiscoCollection coll = new SuitabilityApuracaoRiscoCollection();

        coll.Query.OrderBy(coll.Query.IdApuracaoRisco.Ascending);

        coll.LoadAll();

        e.Collection = coll;
    }

    protected void EsDSSuitabilityPerfilInvestidor_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        SuitabilityPerfilInvestidorCollection coll = new SuitabilityPerfilInvestidorCollection();

        coll.LoadAll();

        e.Collection = coll;
    }

    protected void EsDSValidacao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        SuitabilityValidacaoCollection coll = new SuitabilityValidacaoCollection();

        coll.Query.OrderBy(coll.Query.IdValidacao.Ascending);
        coll.LoadAll();

        e.Collection = coll;
    }

    protected void EsDSSuitabilityHistorico_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        SuitabilityApuracaoRiscoHistoricoCollection coll = new SuitabilityApuracaoRiscoHistoricoCollection();

        coll.Query.OrderBy(coll.Query.DataHistorico.Descending);
        coll.LoadAll();

        e.Collection = coll;
    }

    protected void gridCadastro_Init(object sender, EventArgs e)
    {
        this.AllowUpdate = false;
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        SuitabilityApuracaoRisco suitabilityApuracaoRisco = new SuitabilityApuracaoRisco();

        suitabilityApuracaoRisco.IdValidacao = Convert.ToInt16(e.NewValues["IdValidacao"]);
        suitabilityApuracaoRisco.PerfilCarteira = Convert.ToInt16(e.NewValues["PerfilCarteira"]);
        suitabilityApuracaoRisco.PerfilRiscoFundo = Convert.ToString(e.NewValues["PerfilRiscoFundo"]);
        suitabilityApuracaoRisco.Criterio = Convert.ToInt16(e.NewValues["Criterio"]);
        suitabilityApuracaoRisco.Percentual = Convert.ToInt16(e.NewValues["Percentual"]);

        suitabilityApuracaoRisco.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de suitabilityApuracaoRisco - Operacao: Insert: " + suitabilityApuracaoRisco.IdApuracaoRisco + UtilitarioWeb.ToString(suitabilityApuracaoRisco),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        #region Atualiza Historico
        SuitabilityApuracaoRiscoHistorico.createSuitabilityOpcaoHistorico(suitabilityApuracaoRisco, TipoOperacaoBanco.Inclusão);
        #endregion

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }


    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdApuracaoRisco");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                short IdApuracaoRisco = Convert.ToInt16(keyValuesId[i]);

                SuitabilityApuracaoRisco suitabilityApuracaoRisco = new SuitabilityApuracaoRisco();
                if (suitabilityApuracaoRisco.LoadByPrimaryKey(IdApuracaoRisco))
                {
                    //
                    SuitabilityApuracaoRisco suitabilityApuracaoRiscoClone = (SuitabilityApuracaoRisco)Utilitario.Clone(suitabilityApuracaoRisco);
                    //

                    suitabilityApuracaoRisco.MarkAsDeleted();
                    suitabilityApuracaoRisco.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de suitabilityApuracaoRisco - Operacao: Delete suitabilityApuracaoRisco: " + IdApuracaoRisco + UtilitarioWeb.ToString(suitabilityApuracaoRiscoClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion

                    #region Atualiza Historico
                    SuitabilityApuracaoRiscoHistorico.createSuitabilityOpcaoHistorico(suitabilityApuracaoRiscoClone, TipoOperacaoBanco.Exclusão);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        SuitabilityApuracaoRisco suitabilityApuracaoRisco = new SuitabilityApuracaoRisco();
        short IdApuracaoRisco = Convert.ToInt16(e.Keys[0]);

        if (suitabilityApuracaoRisco.LoadByPrimaryKey(IdApuracaoRisco))
        {
            suitabilityApuracaoRisco.IdValidacao = Convert.ToInt16(e.NewValues["IdValidacao"]);
            suitabilityApuracaoRisco.PerfilCarteira = Convert.ToInt16(e.NewValues["PerfilCarteira"]);
            suitabilityApuracaoRisco.PerfilRiscoFundo = Convert.ToString(e.NewValues["PerfilRiscoFundo"]);
            suitabilityApuracaoRisco.Criterio = Convert.ToInt16(e.NewValues["Criterio"]);
            suitabilityApuracaoRisco.Percentual = Convert.ToInt16(e.NewValues["Percentual"]);
            suitabilityApuracaoRisco.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de Setor - Operacao: Update suitabilityApuracaoRisco: " + IdApuracaoRisco + UtilitarioWeb.ToString(suitabilityApuracaoRisco),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion

            #region Atualiza Historico
            SuitabilityApuracaoRiscoHistorico.createSuitabilityOpcaoHistorico(suitabilityApuracaoRisco, TipoOperacaoBanco.Alteração);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    ///
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridHistorico_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        gridHistorico.DataBind();
    }

}
