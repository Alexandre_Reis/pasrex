﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using Financial.Investidor;
using Financial.ContaCorrente;
using System.Drawing;
using Financial.ContaCorrente.Enums;
using Financial.Security;
using Financial.Fundo;
using Financial.Bolsa;
using Financial.BMF;
using Financial.RendaFixa;
using Financial.Investidor.Enums;
using Financial.InvestidorCotista;
using Financial.CRM;
using Financial.WebConfigConfiguration;
using Financial.Security.Enums;
using System.Collections.Specialized;
using EntitySpaces.Interfaces;
using Financial.Web.Util;
using Financial.InvestidorCotista.Enums;
using Financial.Web.Common;
using System.Web.Configuration;
using System.Xml;
using Financial.Export;
using Financial.Interfaces.Export;
using DevExpress.Web.Data;
using System.ComponentModel;
using Financial.Fundo.Enums;

public partial class CadastrosBasicos_DesenquadramentoTributario : CadastroBasePage
{
    new protected void Page_Load(object sender, EventArgs e)
    {
        this.AllowUpdate = true;
        this.HasPopupCarteira = true;
        base.Page_Load(sender, e);
    }

    #region Instância
    //Query
    DesenquadramentoTributarioQuery desenquadramentoTributarioQuery = new DesenquadramentoTributarioQuery();
    CarteiraQuery carteiraQuery = new CarteiraQuery();
    //Collection
    DesenquadramentoTributarioCollection desenquadramentoTributarioCollection = new DesenquadramentoTributarioCollection();
    CarteiraCollection carteiraCollection = new CarteiraCollection();
    ParametroAdministradorFundoInvestimentoCollection parametroCollection = new ParametroAdministradorFundoInvestimentoCollection();
    //Entidade
    DesenquadramentoTributario desenquadramentoTributario = new DesenquadramentoTributario();
    //Session
    public bool VerificaCarteiraParametro
    {
        get
        {
            if (Session["VerificaCarteiraParametro"] == null)
                Session["VerificaCarteiraParametro"] = false;

            return (bool)Session["VerificaCarteiraParametro"];
        }
        set
        {
            Session["VerificaCarteiraParametro"] = value;
        }
    }
    #endregion

    #region esDataSources
    protected void EsDSDesenquadramentoTributario_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        desenquadramentoTributarioQuery = new DesenquadramentoTributarioQuery("d");
        carteiraQuery = new CarteiraQuery("c");

        desenquadramentoTributarioQuery.Select(desenquadramentoTributarioQuery, carteiraQuery.Apelido, carteiraQuery.TipoTributacao.As("ClassificacaoTributariaCarteira"));
        desenquadramentoTributarioQuery.OrderBy(desenquadramentoTributarioQuery.IdDesenquadramentoTributario.Ascending);
        desenquadramentoTributarioQuery.LeftJoin(carteiraQuery).On(desenquadramentoTributarioQuery.IdCarteira == carteiraQuery.IdCarteira);
        desenquadramentoTributarioCollection.Load(desenquadramentoTributarioQuery);

        e.Collection = desenquadramentoTributarioCollection;
    }
    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        carteiraCollection = new CarteiraCollection();

        carteiraCollection.Query.Select();
        carteiraCollection.Query.OrderBy(carteiraCollection.Query.IdCarteira.Ascending);
        carteiraCollection.Query.Load();

        e.Collection = carteiraCollection;
    }
    #endregion

    #region CallBack
    /// <summary>
    /// Verifica se os campos obrigatorios foram devidamente preenchidos
    /// </summary>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
        //Instância
        ASPxRadioButtonList rblTipoOcorrencia = gridCadastro.FindEditFormTemplateControl("rblTipoOcorrencia") as ASPxRadioButtonList;
        ASPxCallbackPanel cbPanel = gridCadastro.FindEditFormTemplateControl("cbPanel") as ASPxCallbackPanel;
        ASPxRadioButtonList rblExecucaoRecolhimento = cbPanel.FindControl("rblExecucaoRecolhimento") as ASPxRadioButtonList;
        ASPxDateEdit textDataRecolhimento = cbPanel.FindControl("textDataRecolhimento") as ASPxDateEdit;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxComboBox dropIdCarteira = gridCadastro.FindEditFormTemplateControl("dropIdCarteira") as ASPxComboBox;
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;

        //Verifica se o controle foi encontrado
        if (rblTipoOcorrencia.SelectedItem != null)
        {
            //Verifica se o Tipo de Ocorrencia possui Recolhimento de IR
            if (rblTipoOcorrencia.SelectedItem.Value.Equals(2))
            {
                //Verifica se a carteira possui Parâmetro já cadastrado
                if (!VerificaCarteiraParametro)
                {
                    e.Result = "Parâmetros do Administrador devem ser previamente cadastrados para possibilitar o registro do evento corporativo";
                    return;
                }
            }
            else
            {
                if (rblExecucaoRecolhimento.SelectedItem != null && rblExecucaoRecolhimento.SelectedItem.Value.Equals(3))
                {
                    if (textDataRecolhimento.Text.Equals(string.Empty))
                    {
                        e.Result = "Data Recolhimento deve ser preenchido";
                        return;
                    }
                }
            }
        }

        if (!string.IsNullOrEmpty(btnEditCodigoCarteira.Text))
        {
            //Verifica se já existe um evento para a carteira a ser cadastrada
            DateTime? Data = Financial.Util.ConverteTipo.TryParseDateTime(textData.Text);
            int? Carteira = Financial.Util.ConverteTipo.TryParseInt(btnEditCodigoCarteira.Text.ToString());
            if (Carteira != null && Data != null)
            {
                string existeEvento = VerificaEventoExistente(Carteira, Data);
                if (!string.IsNullOrEmpty(existeEvento))
                {
                    e.Result = existeEvento;
                    return;
                }
            }
        }
    }

    /// <summary>
    /// Deleta os registros selecionados na gridCadastro
    /// </summary>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdDesenquadramentoTributario");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idDesenquadramentoTributario = Convert.ToInt32(keyValuesId[i]);
                desenquadramentoTributario = new DesenquadramentoTributario();

                if (desenquadramentoTributario.LoadByPrimaryKey(idDesenquadramentoTributario))
                {
                    DesenquadramentoTributario desenquadramentoTributarioClone = (DesenquadramentoTributario)Utilitario.Clone(desenquadramentoTributario);

                    desenquadramentoTributario.MarkAsDeleted();
                    desenquadramentoTributario.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de Desenquadramento Tributario - Operacao: Delete Desenquadramento Tributario: " + idDesenquadramentoTributario + "; " + UtilitarioWeb.ToString(desenquadramentoTributarioClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// Atribui mascara dinamicamente para os campos dependendo do tipo de campo
    /// OBS: O método é um AspxCallBackPanel, ele funciona como o UpdatePanel.
    /// Sua utilização é necessária para que não ocorra o postback na página
    /// e a mascara perca o seu valor.
    /// </summary>
    protected void cbPanel_Callback(object source, CallbackEventArgsBase e)
    {
        if (!string.IsNullOrEmpty(e.Parameter) && !e.Parameter.Equals("null"))
        {
            ASPxCallbackPanel cbPanel = gridCadastro.FindEditFormTemplateControl("cbPanel") as ASPxCallbackPanel;
            ASPxRadioButtonList rblAliquotaIR = cbPanel.FindControl("rblAliquotaIR") as ASPxRadioButtonList;
            ASPxRadioButtonList rblExecucaoRecolhimento = cbPanel.FindControl("rblExecucaoRecolhimento") as ASPxRadioButtonList;
            ASPxDateEdit textDataRecolhimento = cbPanel.FindControl("textDataRecolhimento") as ASPxDateEdit;

            carteiraCollection = new CarteiraCollection();
            carteiraCollection.Query.Select(carteiraCollection.Query.IdAgenteAdministrador);
            carteiraCollection.Query.Where(carteiraCollection.Query.IdCarteira.Equal(Convert.ToInt32(e.Parameter)));
            carteiraCollection.Query.Load();

            if (carteiraCollection.Count > 0)
            {
                parametroCollection = new ParametroAdministradorFundoInvestimentoCollection();
                parametroCollection.Query.Select();
                parametroCollection.Query.Where(parametroCollection.Query.Administrador.Equal(carteiraCollection[0].IdAgenteAdministrador));
                parametroCollection.Query.Load();
            }
            if (parametroCollection.Count > 0)
            {
                VerificaCarteiraParametro = true;
                rblAliquotaIR.Value = parametroCollection[0].AliquotaIR;
                rblExecucaoRecolhimento.Value = parametroCollection[0].ExecucaoRecolhimento;
                textDataRecolhimento.Value = parametroCollection[0].DataRecolhimento;
            }
            else
            {
                VerificaCarteiraParametro = false;
                rblAliquotaIR.Value = -1;
                rblExecucaoRecolhimento.Value = -1;
                textDataRecolhimento.Value = null;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCarteira = Convert.ToInt32(e.Parameter);
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            if (carteira.str.Apelido != "")
            {
                if (carteira.IsAtivo)
                {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoCliente(idCarteira, HttpContext.Current.User.Identity.Name))
                    {
                        nome = carteira.str.Apelido;
                    }
                    else
                    {
                        nome = "no_access";
                    }
                }
                else
                {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }
    #endregion

    #region gridCadastro
    /// <summary>
    /// Efetua a alteração dos registros
    /// </summary>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        int idDesenquandramentoTributario = (int)e.Keys[0];
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        if (desenquadramentoTributario.LoadByPrimaryKey(idDesenquandramentoTributario))
        {
            //Preenche dinamicamente a entidade
            desenquadramentoTributario.IdCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);
            desenquadramentoTributario = Financial.Util.CadastroDinamico.PreencheEntidade<DesenquadramentoTributario>(gridCadastro.FindEditFormTemplateControl("panelEdicao").Controls, desenquadramentoTributario);

            //Recolhimento IR
            PreencheRecolhimentoIR(desenquadramentoTributario);

            //Preenche a entidade com as informações do Recolhimento de IR
            desenquadramentoTributario.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de Forma Condominio - Operacao: Update Forma Condominio: " + desenquadramentoTributario.IdDesenquadramentoTributario + UtilitarioWeb.ToString(desenquadramentoTributario),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// Efetua o cadastro 
    /// </summary>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        this.Cadastra();

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// Atualiza o gridCadastro quando o usuário fechar o EditForm
    /// </summary>
    protected void gridCadastro_CancelRowEditing(object sender, ASPxStartRowEditingEventArgs e)
    {
        e.Cancel = false;
        gridCadastro.DataBind();
    }

    /// <summary>
    /// Efetua o tratamento dos campos
    /// </summary>
    protected void gridCadastro_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "IdCarteira")
        {
            carteiraCollection = new CarteiraCollection();

            carteiraCollection.Query.Select(carteiraCollection.Query.Nome);
            carteiraCollection.Query.Where(carteiraCollection.Query.IdCarteira.Equal(Convert.ToInt32(e.Value)));
            carteiraCollection.Query.Load();

            e.DisplayText = carteiraCollection[0].Nome;
        }
        if (e.Column.FieldName == "DataRecolhimento")
        {
            string data = e.Value.ToString();
            if (data.Equals(null))
                e.DisplayText = string.Empty;
        }
    }

    /// <summary>
    /// Carrega os filtros da grid 
    /// </summary>
    protected void gridCadastro_PreRender(object sender, EventArgs e)
    {
        #region Tipo Ocorrencia
        //Busca o controle de filtro da coluna CADASTRO da grid
        GridViewDataComboBoxColumn TipoOcorrencia = gridCadastro.Columns["TipoOcorrencia"] as GridViewDataComboBoxColumn;
        //Limpa a lista de items para remover possiveis items duplicados
        TipoOcorrencia.PropertiesComboBox.Items.Clear();
        //Adiciona registro vazio para limpar o filtro
        TipoOcorrencia.PropertiesComboBox.Items.Add(string.Empty);
        //Percorre o ENUM para preencher o controle do combobox
        foreach (int r in Enum.GetValues(typeof(TipoOcorrencia)))
        {
            TipoOcorrencia.PropertiesComboBox.Items.Add(TipoOcorrenciaDescricao.RetornaStringValue(r), r.ToString());
        }
        #endregion

        #region Classificação Tributária
        //Busca o controle de filtro da coluna CADASTRO da grid
        GridViewDataComboBoxColumn ClassificacaoTributariaAtual = gridCadastro.Columns["ClassificacaoTributariaAtual"] as GridViewDataComboBoxColumn;
        //Limpa a lista de items para remover possiveis items duplicados
        ClassificacaoTributariaAtual.PropertiesComboBox.Items.Clear();
        //Adiciona registro vazio para limpar o filtro
        ClassificacaoTributariaAtual.PropertiesComboBox.Items.Add(string.Empty);
        //Percorre o ENUM para preencher o controle do combobox
        foreach (int r in Enum.GetValues(typeof(ClassificacaoTributaria)))
        {
            ClassificacaoTributariaAtual.PropertiesComboBox.Items.Add(ClassificacaoTributariaDescricao.RetornaStringValue(r), r.ToString());
        }
        #endregion
    }
    #endregion

    #region OnLoad
    /// <summary>
    /// Carrega a lista de Classificação Tributária
    /// </summary>
    protected void dropClassificacaoTributariaAtual_OnLoad(object sender, EventArgs e)
    {
        ASPxComboBox dropClassificacaoTributariaAtual = (ASPxComboBox)sender;
        dropClassificacaoTributariaAtual.Items.Clear();
        foreach (int r in Enum.GetValues(typeof(ClassificacaoTributaria)))
        {
            dropClassificacaoTributariaAtual.Items.Add(ClassificacaoTributariaDescricao.RetornaStringValue(r), r.ToString());
        }
    }

    /// <summary>
    /// Carrega a lista de Classificação Tributária
    /// </summary>
    protected void rblClassificacaoTributariaAtual_OnLoad(object sender, EventArgs e)
    {
        ASPxRadioButtonList rblNovaClassificacaoTributaria = (ASPxRadioButtonList)sender;

        rblNovaClassificacaoTributaria.Items.Clear();
        foreach (int r in Enum.GetValues(typeof(ClassificacaoTributaria)))
        {
            rblNovaClassificacaoTributaria.Items.Add(ClassificacaoTributariaDescricao.RetornaStringValue(r), r.ToString());
        }
    }

    /// <summary>
    /// Carrega a Classificação do Tributo Cadastrado 
    /// </summary>
    protected void textClassificacaoTributariaBaseDados_OnDataBound(object sender, EventArgs e)
    {
        ASPxTextBox textClassificacaoTributariaBaseDados = (ASPxTextBox)sender;
        int classificacao = !string.IsNullOrEmpty(textClassificacaoTributariaBaseDados.Text) ? Convert.ToInt32(textClassificacaoTributariaBaseDados.Text) : 0;

        if (classificacao != 0)
            textClassificacaoTributariaBaseDados.Text = ClassificacaoTributariaDescricao.RetornaStringValue(classificacao);
    }

    /// <summary>
    /// Carrega o controle de acordo com a lista(Enum) do Tipo de Ocorrencia
    /// </summary>
    protected void rblTipoOcorrencia_OnLoad(object sender, EventArgs e)
    {
        ASPxRadioButtonList rblTipoOcorrencia = (ASPxRadioButtonList)sender;

        rblTipoOcorrencia.Items.Clear();
        foreach (int r in Enum.GetValues(typeof(TipoOcorrencia)))
        {
            rblTipoOcorrencia.Items.Add(TipoOcorrenciaDescricao.RetornaStringValue(r), r.ToString());
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditCodigo_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
        {
            (sender as ASPxSpinEdit).Enabled = false;
            (sender as ASPxSpinEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }
    #endregion

    #region Suporte
    /// <summary>
    /// Inserção de um novo registro
    /// </summary>
    private void Cadastra()
    {
        //Instancia
        desenquadramentoTributario = new DesenquadramentoTributario();
        ASPxRadioButtonList rblTipoOcorrencia = gridCadastro.FindEditFormTemplateControl("rblTipoOcorrencia") as ASPxRadioButtonList;
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;

        //Preenche dinamicamente a entidade
        
        desenquadramentoTributario = Financial.Util.CadastroDinamico.PreencheEntidade<DesenquadramentoTributario>(gridCadastro.FindEditFormTemplateControl("panelEdicao").Controls, null);

        //Preenche a entidade com os dados do Recolhimento IR
        if (rblTipoOcorrencia.SelectedItem.Value.Equals(2))
        {
            PreencheRecolhimentoIR(desenquadramentoTributario);
        }
        desenquadramentoTributario.IdCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);
        desenquadramentoTributario.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro Fundo - Operacao: Insert Desenquadramento Tributario: " + desenquadramentoTributario.IdDesenquadramentoTributario + UtilitarioWeb.ToString(desenquadramentoTributario),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }
   
    /// <summary>
    /// Preenche os registros do Recolhimento do IR
    /// </summary>
    private void PreencheRecolhimentoIR(DesenquadramentoTributario desenquadramentoTributario)
    {
        ASPxCallbackPanel cbPanel = gridCadastro.FindEditFormTemplateControl("cbPanel") as ASPxCallbackPanel;
        ASPxRadioButtonList rblExecucaoRecolhimento = cbPanel.FindControl("rblExecucaoRecolhimento") as ASPxRadioButtonList;
        ASPxRadioButtonList rblAliquotaIR = cbPanel.FindControl("rblAliquotaIR") as ASPxRadioButtonList;
        ASPxDateEdit textDataRecolhimento = cbPanel.FindControl("textDataRecolhimento") as ASPxDateEdit;

        int? ExecucaoRecolhimento = null;
        int? AliquotaIR = null;

        if (rblExecucaoRecolhimento.SelectedItem != null)
            ExecucaoRecolhimento = Convert.ToInt32(rblExecucaoRecolhimento.SelectedItem.Value);
        if (rblAliquotaIR.SelectedItem != null)
            AliquotaIR = Convert.ToInt32(rblAliquotaIR.SelectedItem.Value);

        if (desenquadramentoTributario.TipoOcorrencia.Equals(2))
        {
            desenquadramentoTributario.ExecucaoRecolhimento = ExecucaoRecolhimento;
            desenquadramentoTributario.AliquotaIR = AliquotaIR;
            desenquadramentoTributario.DataRecolhimento = Financial.Util.ConverteTipo.TryParseDateTime(textDataRecolhimento.Text);
        }
        else
        {
            desenquadramentoTributario.ExecucaoRecolhimento = null;
            desenquadramentoTributario.AliquotaIR = null;
            desenquadramentoTributario.DataRecolhimento = null;
        }
    }

    /// <summary>
    /// Verifica se o evento a ser cadastrado já possui alguem evento cadastrado na data definida
    /// </summary>
    /// <param name="idCarteira">Codigo da carteira a ser analisada</param>
    /// <param name="data">data do evento </param>
    /// <returns></returns>
    private string VerificaEventoExistente(int? idCarteira, DateTime? data)
    {
        string mensagem = string.Empty;

        if (gridCadastro.IsNewRowEditing)
        {
            #region Desenquadramento Tributário
            EventoFundoCollection efc = new EventoFundoCollection();
            efc.Query.Where(efc.Query.IdCarteiraOrigem == idCarteira, efc.Query.DataPosicao == data);
            efc.Query.Load();

            if (efc.Count > 0)
                mensagem = "Fundo já cadastrado para o evento de 'Evento Fundo'";
            #endregion

            #region Forma Condominio
            FundoInvestimentoFormaCondominioCollection fifc = new FundoInvestimentoFormaCondominioCollection();
            fifc.Query.Where(fifc.Query.IdCarteira == idCarteira, fifc.Query.DataInicioVigencia == data);
            fifc.Query.Load();

            if (fifc.Count > 0)
                mensagem = "Fundo já cadastrado para o evento de 'Fundo Investimento Forma Condomínio'";
            #endregion
        }
        return mensagem;
    }
    #endregion
}