﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using Financial.Investidor;
using Financial.ContaCorrente;
using System.Drawing;
using Financial.ContaCorrente.Enums;
using Financial.Security;
using Financial.Fundo;
using Financial.Investidor.Enums;
using Financial.Common.Enums;
using Financial.Fundo.Enums;
using Financial.Web.Common;
using Financial.InvestidorCotista;
using Financial.InvestidorCotista.Enums;
using System.Threading;
using Financial.Web.Util;
using Financial.Security.Enums;
using DevExpress.Web.Data;
using System.Net.Configuration;
using System.Web.Configuration;

public partial class CadastrosBasicos_OrdemFundo : Financial.Web.Common.CadastroBasePage
{
    private class Datas
    {
        public DateTime dataOperacao;
        public DateTime dataConversao;
        public DateTime dataLiquidacao;
    }

    bool aprovacao = false;

    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasFiltro = true;
        this.HasPopupCliente = true;
        this.HasPopupCarteira = true;
        this.HasPanelFieldsLoading = true;
        base.Page_Load(sender, e);

        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;
        GrupoUsuario grupoUsuario = new GrupoUsuario();
        grupoUsuario.LoadByPrimaryKey(usuario.IdGrupo.Value);

        aprovacao = grupoUsuario.Front == (byte)PermissaoFront.Aprovacao;

        if (!aprovacao)
        {
            btnAprova.Visible = false;
            btnCancela.Visible = false;
            btnFilter.Visible = false;
        }

        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
                         new List<string>(new string[] { OrdemFundoMetadata.ColumnNames.TipoOperacao }));
    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSOrdemFundo_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        DateTime hoje = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);

        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
        OrdemFundoQuery ordemFundoQuery = new OrdemFundoQuery("O");
        ClienteQuery clienteQuery = new ClienteQuery("C");
        CarteiraQuery carteiraQuery = new CarteiraQuery("A");

        ordemFundoQuery.Select(ordemFundoQuery, clienteQuery.Apelido.As("ApelidoCliente"),
                               carteiraQuery.Apelido.As("ApelidoCarteira"));
        ordemFundoQuery.InnerJoin(clienteQuery).On(ordemFundoQuery.IdCliente == clienteQuery.IdCliente);
        ordemFundoQuery.LeftJoin(carteiraQuery).On(ordemFundoQuery.IdCarteira == carteiraQuery.IdCarteira);
        ordemFundoQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        ordemFundoQuery.Where(permissaoClienteQuery.IdUsuario.Equal(idUsuario));

        if (!String.IsNullOrEmpty(btnEditCodigoClienteFiltro.Text))
        {
            ordemFundoQuery.Where(ordemFundoQuery.IdCliente.Equal(Convert.ToInt32(btnEditCodigoClienteFiltro.Text)));
        }

        if (!String.IsNullOrEmpty(textDataInicio.Text))
        {
            ordemFundoQuery.Where(ordemFundoQuery.DataOperacao.GreaterThanOrEqual(textDataInicio.Text));
        }

        if (!String.IsNullOrEmpty(textDataFim.Text))
        {
            ordemFundoQuery.Where(ordemFundoQuery.DataOperacao.LessThanOrEqual(textDataFim.Text));
        }

        ordemFundoQuery.OrderBy(ordemFundoQuery.IdOperacao.Descending);

        OrdemFundoCollection coll = new OrdemFundoCollection();
        coll.Load(ordemFundoQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ClienteCollection coll = new ClienteCollection();
        coll.BuscaClientesComAcesso(Context.User.Identity.Name);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CarteiraCollection coll = new CarteiraCollection();

        CarteiraQuery carteiraQuery = new CarteiraQuery("A");
        ClienteQuery clienteQuery = new ClienteQuery("C");
        UsuarioQuery usuarioQuery = new UsuarioQuery("U");
        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");

        carteiraQuery.Select(carteiraQuery.IdCarteira, carteiraQuery.Nome, carteiraQuery.Apelido);
        //
        carteiraQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == carteiraQuery.IdCarteira);
        carteiraQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        carteiraQuery.InnerJoin(usuarioQuery).On(permissaoClienteQuery.IdUsuario == usuarioQuery.IdUsuario);
        //
        carteiraQuery.Where(usuarioQuery.Login == Context.User.Identity.Name,
                            clienteQuery.StatusAtivo.Equal(StatusAtivoCliente.Ativo),
                            clienteQuery.IdTipo.In(TipoClienteFixo.Clube, TipoClienteFixo.Fundo, TipoClienteFixo.FDIC),
                            clienteQuery.TipoControle.NotEqual((byte)TipoControleCliente.ApenasCotacao));

        coll.Load(carteiraQuery);

        carteiraQuery = new CarteiraQuery("A");
        clienteQuery = new ClienteQuery("C");
        //
        carteiraQuery.Select(carteiraQuery.IdCarteira, carteiraQuery.Nome, carteiraQuery.Apelido);
        carteiraQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == carteiraQuery.IdCarteira);
        carteiraQuery.Where(clienteQuery.TipoControle.Equal((byte)TipoControleCliente.ApenasCotacao),
                            clienteQuery.IdTipo.In(TipoClienteFixo.Clube, TipoClienteFixo.Fundo, TipoClienteFixo.FDIC),
                            clienteQuery.StatusAtivo.Equal(StatusAtivoCliente.Ativo));
        //
        CarteiraCollection carteiraCollection2 = new CarteiraCollection();
        carteiraCollection2.Load(carteiraQuery);
        //
        coll.Combine(carteiraCollection2);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSFormaLiquidacao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        FormaLiquidacaoCollection coll = new FormaLiquidacaoCollection();
        coll.Query.OrderBy(coll.Query.IdFormaLiquidacao.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    new protected void panelEdicao_Load(object sender, EventArgs e)
    {
        base.panelEdicao_Load(sender, e);

        if (gridCadastro.IsEditing && gridCadastro.IsNewRowEditing)
        {
            ASPxComboBox dropFormaLiquidacao = gridCadastro.FindEditFormTemplateControl("dropFormaLiquidacao") as ASPxComboBox;
            dropFormaLiquidacao.SelectedIndex = 0;
        }

        bool travaPainel = false;
        if (gridCadastro.IsEditing && !gridCadastro.IsNewRowEditing)
        {
            Label labelEdicao = gridCadastro.FindEditFormTemplateControl("labelEdicao") as Label;

            byte status = Convert.ToByte(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "Status"));

            if (!aprovacao)
            {
                if ((byte)status == (byte)StatusOrdemFundo.Aprovado)
                {
                    labelEdicao.Text = "Ordem está aprovada! Dados não podem ser alterados.";
                    travaPainel = true;
                }
                else if ((byte)status == (byte)StatusOrdemFundo.Cancelado)
                {
                    labelEdicao.Text = "Ordem está cancelada! Dados não podem ser alterados.";
                    travaPainel = true;
                }
            }

            else if ((byte)status == (byte)StatusOrdemFundo.Processado)
            {
                labelEdicao.Text = "Ordem está processada! Dados não podem ser alterados.";
                travaPainel = true;
            }
        }

        if (travaPainel)
        {
            LinkButton btnOK = gridCadastro.FindEditFormTemplateControl("btnOK") as LinkButton;
            btnOK.Visible = false;
        }
    }

    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        TimeSpan horaLimite = DateTime.Now.TimeOfDay;

        TabelaParametrosFrontCollection tabelaParametrosFrontCollection = new TabelaParametrosFrontCollection();
        tabelaParametrosFrontCollection.LoadAll();

        if (tabelaParametrosFrontCollection.Count > 0)
        {
            if (tabelaParametrosFrontCollection[0].HorarioFimFundo.HasValue)
            {
                if (horaLimite > tabelaParametrosFrontCollection[0].HorarioFimFundo.Value.TimeOfDay)
                {
                    e.Result = "Fora do Horário permitido. Operação não pode ser realizada.";
                    return;
                }
            }
        }

        if (!gridCadastro.IsEditing) //Testa deleção
        {
            List<object> keyId = gridCadastro.GetSelectedFieldValues("IdOperacao");
            List<object> keyStatus = gridCadastro.GetSelectedFieldValues("Status");

            for (int i = 0; i < keyId.Count; i++)
            {
                int idOperacao = Convert.ToInt32(keyId[i]);
                byte status = Convert.ToByte(keyStatus[i]);

                if (e.Parameter == "deletar")
                {
                    if (!aprovacao)
                    {
                        if ((byte)status == (byte)StatusOrdemFundo.Aprovado)
                        {
                            e.Result = "Ordem " + idOperacao + " está aprovada! Operação não pode ser realizada.";
                            return;
                        }
                        else if ((byte)status == (byte)StatusOrdemFundo.Cancelado)
                        {
                            e.Result = "Ordem " + idOperacao + " está cancelada! Operação não pode ser realizada.";
                            return;
                        }
                    }
                }
                else if (e.Parameter == "aprovar" || e.Parameter == "cancelar")
                {
                    if ((byte)status == (byte)StatusOrdemFundo.Aprovado)
                    {
                        e.Result = "Ordem " + idOperacao + " está aprovada! Operação não pode ser realizada.";
                        return;
                    }
                    else if ((byte)status == (byte)StatusOrdemFundo.Cancelado)
                    {
                        e.Result = "Ordem " + idOperacao + " está cancelada! Operação não pode ser realizada.";
                        return;
                    }
                }

                if ((byte)status == (byte)StatusOrdemFundo.Processado)
                {
                    e.Result = "Ordem " + idOperacao + " está processada! Operação não pode ser realizada.";
                    return;
                }
            }
        }
        else
        {
            ASPxComboBox dropTipoOperacao = gridCadastro.FindEditFormTemplateControl("dropTipoOperacao") as ASPxComboBox;
            ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
            ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
            ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
            ASPxComboBox dropFormaLiquidacao = gridCadastro.FindEditFormTemplateControl("dropFormaLiquidacao") as ASPxComboBox;

            #region Campos obrigatórios
            List<Control> controles = new List<Control>();
            controles.Add(btnEditCodigoCliente);
            controles.Add(btnEditCodigoCarteira);
            controles.Add(dropTipoOperacao);
            controles.Add(dropFormaLiquidacao);

            if (base.TestaObrigatorio(controles) != "")
            {
                e.Result = "Campos com * são obrigatórios!";
                return;
            }
            #endregion

            byte tipoOperacao = Convert.ToByte(dropTipoOperacao.SelectedItem.Value);

            if (tipoOperacao != (byte)TipoOperacaoFundo.ResgateTotal && textValor.Text == "")
            {
                e.Result = "Para operação diferente de resgate total, é preciso informar o campo de Valor/Qtde.";
                return;
            }

            int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
            
            int idCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);

            PosicaoFundo posicaoFundo = new PosicaoFundo();
            posicaoFundo.Query.Select(posicaoFundo.Query.ValorBruto.Sum(), posicaoFundo.Query.Quantidade.Sum());
            posicaoFundo.Query.Where(posicaoFundo.Query.IdCliente.Equal(idCliente),
                                     posicaoFundo.Query.IdCarteira.Equal(idCarteira));
            posicaoFundo.Query.Load();

            decimal saldoBruto = 0;
            decimal quantidadeCotas = 0;
            if (posicaoFundo.ValorBruto.HasValue)
            {
                saldoBruto = posicaoFundo.ValorBruto.Value;
                quantidadeCotas = posicaoFundo.Quantidade.Value;
            }

            if (tipoOperacao == (byte)TipoOperacaoFundo.ResgateBruto ||
                tipoOperacao == (byte)TipoOperacaoFundo.ResgateCotas ||
                tipoOperacao == (byte)TipoOperacaoFundo.ResgateLiquido ||
                tipoOperacao == (byte)TipoOperacaoFundo.ResgateTotal)
            {
                if (quantidadeCotas <= 0)
                {
                    e.Result = "O cliente não possui saldo de aplicação neste fundo para resgate.";
                    return;
                }
            }

            if (tipoOperacao == (byte)TipoOperacaoFundo.Aplicacao)
            {
                Carteira carteira = new Carteira();
                carteira.LoadByPrimaryKey(idCarteira);

                decimal valor = Convert.ToDecimal(textValor.Text);
                if (quantidadeCotas == 0 && valor < carteira.ValorMinimoInicial.Value)
                {
                    e.Result = "Operação com valor inferior ao valor mínimo inicial (" + carteira.ValorMinimoInicial.Value.ToString() + ") . Operação não pode ser realizada";
                    return;
                }
                else if (quantidadeCotas != 0 && valor < carteira.ValorMinimoAplicacao.Value)
                {
                    e.Result = "Operação com valor inferior ao valor de aplicação exigido (" + carteira.ValorMinimoAplicacao.Value.ToString() + ") . Operação não pode ser realizada";
                    return;
                }
            }
        }
    }

    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "")
            {
                if (cliente.IsAtivo)
                {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name))
                    {
                        nome = cliente.str.Apelido;
                    }
                    else
                    {
                        nome = "no_access";
                    }
                }
                else
                {
                    nome = "no_active";
                }
            }
        }
        e.Result = nome;
    }

    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string nome = "";
        string resultado = "";
        DateTime dataOperacao = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            Carteira carteira = new Carteira();

            int idCarteira = Convert.ToInt32(e.Parameter);
            if (carteira.LoadByPrimaryKey(idCarteira))
            {
                if (carteira.IsAtivo)
                {
                    //O permissionamento da carteira é dado direto no cliente vinculado à carteira
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoCliente(idCarteira, HttpContext.Current.User.Identity.Name))
                    {
                        nome = carteira.str.Apelido;
                        resultado = nome;
                    }
                    else
                    {
                        resultado = "no_access";
                    }
                }
                else
                {
                    resultado = "no_active";
                }
            }
        }

        e.Result = resultado;
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        int idOperacao = (int)e.Keys[0];
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxComboBox dropTipoOperacao = gridCadastro.FindEditFormTemplateControl("dropTipoOperacao") as ASPxComboBox;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
        ASPxComboBox dropFormaLiquidacao = gridCadastro.FindEditFormTemplateControl("dropFormaLiquidacao") as ASPxComboBox;
        ASPxComboBox dropTrader = gridCadastro.FindEditFormTemplateControl("dropTrader") as ASPxComboBox;
        ASPxComboBox dropCategoriaMovimentacao = gridCadastro.FindEditFormTemplateControl("dropCategoriaMovimentacao") as ASPxComboBox;
        TextBox textObservacao = gridCadastro.FindEditFormTemplateControl("textObservacao") as TextBox;

        OrdemFundo ordemFundo = new OrdemFundo();
        if (ordemFundo.LoadByPrimaryKey(idOperacao))
        {
            int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);

            ordemFundo.IdCliente = idCliente;
            ordemFundo.TipoOperacao = Convert.ToByte(dropTipoOperacao.SelectedItem.Value);

            ordemFundo.IdCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);
            ordemFundo.DataOperacao = this.RetornaDatas(ordemFundo.IdCarteira.Value, ordemFundo.TipoOperacao.Value).dataOperacao;
            ordemFundo.DataConversao = this.RetornaDatas(ordemFundo.IdCarteira.Value, ordemFundo.TipoOperacao.Value).dataConversao;
            ordemFundo.DataLiquidacao = this.RetornaDatas(ordemFundo.IdCarteira.Value, ordemFundo.TipoOperacao.Value).dataLiquidacao;

            ordemFundo.DataAgendamento = ordemFundo.DataOperacao;
            ordemFundo.TipoResgate = (byte)TipoResgateFundo.MenorImposto;

            if (dropTrader.SelectedIndex > -1)
                ordemFundo.IdTrader = Convert.ToInt32(dropTrader.SelectedItem.Value);
            else
                ordemFundo.IdTrader = null;


            //Salva o valor dependendo do tipo de operação, o resto força zerado
            //Resgate total força tudo zerado!
            ordemFundo.ValorBruto = 0;
            ordemFundo.ValorLiquido = 0;
            ordemFundo.Quantidade = 0;
            switch (ordemFundo.TipoOperacao)
            {
                case (byte)TipoOperacaoFundo.Aplicacao:
                case (byte)TipoOperacaoFundo.ResgateBruto:
                    ordemFundo.ValorBruto = Convert.ToDecimal(textValor.Text);
                    break;
                case (byte)TipoOperacaoFundo.ResgateCotas:
                    ordemFundo.Quantidade = Convert.ToDecimal(textValor.Text);
                    break;
                case (byte)TipoOperacaoFundo.ResgateLiquido:
                    ordemFundo.ValorLiquido = Convert.ToDecimal(textValor.Text);
                    break;
            }

            ordemFundo.IdFormaLiquidacao = Convert.ToByte(dropFormaLiquidacao.SelectedItem.Value);

            if (dropCategoriaMovimentacao.SelectedIndex > -1)
                ordemFundo.IdCategoriaMovimentacao = Convert.ToInt32(dropCategoriaMovimentacao.SelectedItem.Value);            
            else
                ordemFundo.IdCategoriaMovimentacao = null;            

            ordemFundo.Observacao = textObservacao.Text.ToString();

            ordemFundo.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de OrdemFundo - Operacao: Update OrdemFundo: " + idOperacao + UtilitarioWeb.ToString(ordemFundo),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    private void SalvarNovo()
    {
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxComboBox dropTipoOperacao = gridCadastro.FindEditFormTemplateControl("dropTipoOperacao") as ASPxComboBox;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
        ASPxComboBox dropFormaLiquidacao = gridCadastro.FindEditFormTemplateControl("dropFormaLiquidacao") as ASPxComboBox;
        ASPxComboBox dropCategoriaMovimentacao = gridCadastro.FindEditFormTemplateControl("dropCategoriaMovimentacao") as ASPxComboBox;
        TextBox textObservacao = gridCadastro.FindEditFormTemplateControl("textObservacao") as TextBox;
        ASPxComboBox dropTrader = gridCadastro.FindEditFormTemplateControl("dropTrader") as ASPxComboBox;

        OrdemFundo ordemFundo = new OrdemFundo();

        int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);

        ordemFundo.IdCliente = idCliente;
        ordemFundo.TipoOperacao = Convert.ToByte(dropTipoOperacao.SelectedItem.Value);

        ordemFundo.IdCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);
        ordemFundo.DataOperacao = this.RetornaDatas(ordemFundo.IdCarteira.Value, ordemFundo.TipoOperacao.Value).dataOperacao;
        ordemFundo.DataConversao = this.RetornaDatas(ordemFundo.IdCarteira.Value, ordemFundo.TipoOperacao.Value).dataConversao;
        ordemFundo.DataLiquidacao = this.RetornaDatas(ordemFundo.IdCarteira.Value, ordemFundo.TipoOperacao.Value).dataLiquidacao;

        ordemFundo.DataAgendamento = ordemFundo.DataOperacao;

        ordemFundo.TipoResgate = (byte)TipoResgateFundo.MenorImposto;

        if (dropTrader.SelectedIndex > -1)
            ordemFundo.IdTrader = Convert.ToInt32(dropTrader.SelectedItem.Value);
        else
            ordemFundo.IdTrader = null;

        //Salva o valor dependendo do tipo de operação, o resto força zerado
        //Resgate total força tudo zerado!
        ordemFundo.ValorBruto = 0;
        ordemFundo.ValorLiquido = 0;
        ordemFundo.Quantidade = 0;
        switch (ordemFundo.TipoOperacao)
        {
            case (byte)TipoOperacaoFundo.Aplicacao:
            case (byte)TipoOperacaoFundo.ResgateBruto:
                ordemFundo.ValorBruto = Convert.ToDecimal(textValor.Text);
                break;
            case (byte)TipoOperacaoFundo.ResgateCotas:
                ordemFundo.Quantidade = Convert.ToDecimal(textValor.Text);
                break;
            case (byte)TipoOperacaoFundo.ResgateLiquido:
                ordemFundo.ValorLiquido = Convert.ToDecimal(textValor.Text);
                break;
        }

        ordemFundo.IdFormaLiquidacao = Convert.ToByte(dropFormaLiquidacao.SelectedItem.Value);

        if (dropCategoriaMovimentacao.SelectedIndex > -1)
            ordemFundo.IdCategoriaMovimentacao = Convert.ToInt32(dropCategoriaMovimentacao.SelectedItem.Value);
        else
            ordemFundo.IdCategoriaMovimentacao = null;           

        ordemFundo.Observacao = textObservacao.Text.ToString();

        if (aprovacao)
        {
            ordemFundo.Status = (byte)StatusOrdemFundo.Aprovado;
        }
        else
        {
            ordemFundo.Status = (byte)StatusOrdemFundo.Digitado;
        }

        ordemFundo.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de OrdemFundo - Operacao: Insert OrdemFundo: " + ordemFundo.IdOperacao + UtilitarioWeb.ToString(ordemFundo),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        this.EnviaEmail(ordemFundo);
    }

    private void EnviaEmail(OrdemFundo ordemFundo)
    {
        Configuration config = WebConfigurationManager.OpenWebConfiguration(HttpContext.Current.Request.ApplicationPath);
        MailSettingsSectionGroup settings = (MailSettingsSectionGroup)config.GetSectionGroup("system.net/mailSettings");

        string from = settings.Smtp.From.ToString();

        Usuario usuario = new Usuario();
        if (!usuario.BuscaUsuario(Context.User.Identity.Name))
        {
            return;
        }

        string to = "";
        if (!String.IsNullOrEmpty(usuario.Email))
        {
            to = usuario.Email;
        }
        TabelaParametrosFrontCollection tabelaParametrosFrontCollection = new TabelaParametrosFrontCollection();
        tabelaParametrosFrontCollection.LoadAll();

        if (tabelaParametrosFrontCollection.Count > 0 && !String.IsNullOrEmpty(tabelaParametrosFrontCollection[0].EmailFundo))
        {
            if (!String.IsNullOrEmpty(to))
            {
                to = to + ",";
            }

            to = to + tabelaParametrosFrontCollection[0].EmailFundo;
        }

        if (String.IsNullOrEmpty(to.Trim()))
            return;

        Cliente cliente = new Cliente();
        cliente.LoadByPrimaryKey(ordemFundo.IdCliente.Value);

        FormaLiquidacao formaLiquidacao = new FormaLiquidacao();
        formaLiquidacao.LoadByPrimaryKey(ordemFundo.IdFormaLiquidacao.Value);

        string body = "";
        body = "Data: " + ordemFundo.DataOperacao.Value.ToShortDateString() + "\n\n";
        body += "Cliente: " + cliente.Apelido + "\n\n";
        body += "Liquida: " + formaLiquidacao.Descricao + "\n\n";

        string valor = "";
        if (ordemFundo.TipoOperacao == (byte)TipoOperacaoFundo.Aplicacao)
        {
            valor = "Valor: " + String.Format("{0:C}", ordemFundo.ValorBruto.Value) + "\n\n";
        }
        else
        {
            if (ordemFundo.TipoOperacao == (byte)TipoOperacaoFundo.ResgateBruto)
            {
                valor = "Valor Bruto: " + String.Format("{0:C}", ordemFundo.ValorBruto.Value) + "\n\n";
            }
            else if (ordemFundo.TipoOperacao == (byte)TipoOperacaoFundo.ResgateCotas)
            {
                valor = "Resgate por Cotas (Quantidade): " + String.Format("{0:N}", ordemFundo.Quantidade.Value) + "\n\n";
            }
            else if (ordemFundo.TipoOperacao == (byte)TipoOperacaoFundo.ResgateLiquido)
            {
                valor = "Valor Líquido: " + String.Format("{0:C}", ordemFundo.ValorLiquido.Value) + "\n\n";
            }
            else if (ordemFundo.TipoOperacao == (byte)TipoOperacaoFundo.ResgateTotal)
            {
                valor = "Tipo: Resgate Total" + "\n\n";
            }
        }
        body += valor;

        if (!String.IsNullOrEmpty(ordemFundo.Observacao))
        {
            body += ordemFundo.Observacao;
        }

        string subject = "";
        if (ordemFundo.TipoOperacao == (byte)TipoOperacaoFundo.Aplicacao)
        {
            subject = "Boleto de Aplicação em Fundos - " + cliente.Apelido;
        }
        else
        {
            subject = "Boleto de Resgate em Fundos - " + cliente.Apelido;
        }

        EmailUtil e = new EmailUtil();
        e.SendMail(body, subject, from, to);
    }

    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();
    }

    protected void gridCadastro_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (gridCadastro.IsEditing && !gridCadastro.IsNewRowEditing)
        {
            //Preenche o campo de valor, de acordo com o tipo de operação
            ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;

            byte tipoOperacao = Convert.ToByte(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, new string[] { "TipoOperacao" }));
            if (!gridCadastro.IsNewRowEditing)
            {
                switch (tipoOperacao)
                {
                    case (byte)TipoOperacaoFundo.Aplicacao:
                    case (byte)TipoOperacaoFundo.ResgateBruto:
                    case (byte)TipoOperacaoFundo.ResgateTotal:
                        decimal valorBruto = Convert.ToDecimal(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, new string[] { "ValorBruto" }));
                        textValor.Text = valorBruto.ToString();
                        break;
                    case (byte)TipoOperacaoFundo.ResgateCotas:
                        decimal quantidade = Convert.ToDecimal(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, new string[] { "Quantidade" }));
                        textValor.Text = quantidade.ToString();
                        break;
                    case (byte)TipoOperacaoFundo.ResgateLiquido:
                        decimal valorLiquido = Convert.ToDecimal(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, new string[] { "ValorLiquido" }));
                        textValor.Text = valorLiquido.ToString();
                        break;
                }
            }
        }

    }

    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdOperacao");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idOperacao = Convert.ToInt32(keyValuesId[i]);

                OrdemFundo ordemFundo = new OrdemFundo();
                if (ordemFundo.LoadByPrimaryKey(idOperacao))
                {
                    int idCliente = ordemFundo.IdCliente.Value;

                    //
                    OrdemFundo ordemFundoClone = (OrdemFundo)Utilitario.Clone(ordemFundo);
                    //

                    ordemFundo.MarkAsDeleted();
                    ordemFundo.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de OrdemFundo - Operacao: Delete OrdemFundo: " + idOperacao + UtilitarioWeb.ToString(ordemFundoClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }
        else if (e.Parameters == "btnAprova" || e.Parameters == "btnCancela")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdOperacao");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idOperacao = Convert.ToInt32(keyValuesId[i]);

                OrdemFundo ordemFundo = new OrdemFundo();
                if (ordemFundo.LoadByPrimaryKey(idOperacao))
                {
                    if (e.Parameters == "btnAprova")
                    {
                        ordemFundo.Status = (byte)StatusOrdemFundo.Aprovado;
                        ordemFundo.Save();
                    }
                    else if (e.Parameters == "btnCancela")
                    {
                        ordemFundo.Status = (byte)StatusOrdemFundo.Cancelado;
                        ordemFundo.Save();
                    }

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Aprovação de Ordem Fundo: " + idOperacao,
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e)
    {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing)
        {
            ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
            e.Properties["cpBtnEditCodigoCliente"] = btnEditCodigoCliente.ClientID;
        }
    }

    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "StatusDescricao")
        {
            int status = Convert.ToInt32(e.GetListSourceFieldValue(OrdemFundoMetadata.ColumnNames.Status));

            string statusDescricao = "";
            if (status == (byte)StatusOrdemFundo.Digitado)
            {
                statusDescricao = "Digitado";
            }
            else if (status == (byte)StatusOrdemFundo.Aprovado)
            {
                statusDescricao = "Aprovado";
            }
            else if (status == (byte)StatusOrdemFundo.Processado)
            {
                statusDescricao = "Processado";
            }
            else if (status == (byte)StatusOrdemFundo.Cancelado)
            {
                statusDescricao = "Cancelado";
            }

            e.Value = statusDescricao;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_InitNewRow(object sender, ASPxDataInitNewRowEventArgs e)
    {
        if (!aprovacao)
        {
            //Valores default do form
            TabelaParametrosFrontCollection tabelaParametrosFrontCollection = new TabelaParametrosFrontCollection();
            tabelaParametrosFrontCollection.LoadAll();

            if (tabelaParametrosFrontCollection.Count > 0 && !String.IsNullOrEmpty(tabelaParametrosFrontCollection[0].ObservacaoFundo))
            {
                e.NewValues["Observacao"] = tabelaParametrosFrontCollection[0].ObservacaoFundo;
            }
        }
    }

    private Datas RetornaDatas(int idCarteira, byte tipoOperacao)
    {
        Datas datas = new Datas();

        DateTime hoje = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);

        Carteira carteira = new Carteira();
        carteira.LoadByPrimaryKey(idCarteira);

        int contagemResgate = carteira.ContagemDiasConversaoResgate.Value;

        //Trata campos com dataConversao e dataLiquidacao
        DateTime dataConversao;
        DateTime dataLiquidacao;
        int diasConversao = 0;
        int diasLiquidacao = 0;
        if (tipoOperacao == (byte)TipoOperacaoFundo.Aplicacao)
        {
            diasConversao = carteira.DiasCotizacaoAplicacao.Value;
            diasLiquidacao = carteira.DiasLiquidacaoAplicacao.Value;
        }
        else
        {
            diasConversao = carteira.DiasCotizacaoResgate.Value;
            diasLiquidacao = carteira.DiasLiquidacaoResgate.Value;
        }

        if (contagemResgate == (byte)ContagemDiasLiquidacaoResgate.DiasUteis || tipoOperacao == (byte)TipoOperacaoFundo.Aplicacao)
        {
            dataConversao = Calendario.AdicionaDiaUtil(hoje, diasConversao);
            dataLiquidacao = Calendario.AdicionaDiaUtil(hoje, diasLiquidacao);
        }
        else
        {
            //Conta por Dias Corridos
            dataConversao = hoje.AddDays(diasConversao);

            if (!Calendario.IsDiaUtil(dataConversao))
            {
                dataConversao = Calendario.AdicionaDiaUtil(dataConversao, 1);
            }

            //Conta por Dias Úteis em cima da data de conversão
            dataLiquidacao = Calendario.AdicionaDiaUtil(dataConversao, diasLiquidacao);
        }

        datas.dataOperacao = hoje;
        datas.dataConversao = dataConversao;
        datas.dataLiquidacao = dataLiquidacao;

        return datas;
    }

    protected void EsDSTrader_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        TraderCollection coll = new TraderCollection();
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.Query.Load();
    }
    protected void EsDSCategoriaMovimentacao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CategoriaMovimentacaoCollection coll = new CategoriaMovimentacaoCollection();
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
}