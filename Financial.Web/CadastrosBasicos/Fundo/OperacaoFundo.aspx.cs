﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using Financial.Util.Enums;
using System.Text;
using DevExpress.Web;
using Financial.Investidor;
using Financial.ContaCorrente;
using System.Drawing;
using Financial.ContaCorrente.Enums;
using Financial.Security;
using Financial.Fundo;
using Financial.Investidor.Enums;
using Financial.Common.Enums;
using Financial.Fundo.Enums;
using Financial.Web.Common;
using Financial.InvestidorCotista;
using Financial.InvestidorCotista.Enums;
using System.Threading;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.CRM;
using Financial.Tributo.Enums;
using EntitySpaces.Interfaces;

public partial class CadastrosBasicos_OperacaoFundo : Financial.Web.Common.CadastroBasePage
{
    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasFiltro = true;
        this.HasPopupCliente = true;
        this.HasPopupCarteira = true;
        this.HasPanelFieldsLoading = true;
        base.Page_Load(sender, e);

        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
                         new List<string>(new string[] { OperacaoFundoMetadata.ColumnNames.TipoOperacao }));
    }

    protected void dropTipoOperacao_Load(object sender, EventArgs e)
    {
        ASPxComboBox dropTipoOperacao = gridCadastro.FindEditFormTemplateControl("dropTipoOperacao") as ASPxComboBox;
        dropTipoOperacao.Items.Add("Aplicação", 1);
        dropTipoOperacao.Items.Add("Resgate Bruto", 2);
        dropTipoOperacao.Items.Add("Resgate Líquido", 3);
        dropTipoOperacao.Items.Add("Resgate Cotas", 4);
        dropTipoOperacao.Items.Add("Resgate Total", 5);
        dropTipoOperacao.Items.Add("Aplicação Especial", 10);
        dropTipoOperacao.Items.Add("Aplicação Ações", 11);
        dropTipoOperacao.Items.Add("Resgate Especial", 12);
        dropTipoOperacao.Items.Add("Ajuste Posição", 40);
        dropTipoOperacao.Items.Add("Ingresso em Ativos com Impacto na Quantidade", (int)TipoOperacaoFundo.IngressoAtivoImpactoQtde);
        dropTipoOperacao.Items.Add("Ingresso em Ativos com Impacto na Cota", (int)TipoOperacaoFundo.IngressoAtivoImpactoCota);
        dropTipoOperacao.Items.Add("Retirada em Ativos com Impacto na Quantidade", (int)TipoOperacaoFundo.RetiradaAtivoImpactoQtde);
        dropTipoOperacao.Items.Add("Retirada em Ativos com Impacto na Cota", (int)TipoOperacaoFundo.RetiradaAtivoImpactoCota);
        dropTipoOperacao.Items.Add("Aplicação Cotas", (int)TipoOperacaoCotista.AplicacaoCotas);
    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSOperacaoFundo_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
        OperacaoFundoQuery operacaoFundoQuery = new OperacaoFundoQuery("O");
        ClienteQuery clienteQuery = new ClienteQuery("C");
        CarteiraQuery carteiraQuery = new CarteiraQuery("A");

        operacaoFundoQuery.Select(operacaoFundoQuery,
                                  clienteQuery.Apelido.As("ApelidoCliente"),
                                  carteiraQuery.Apelido.As("ApelidoCarteira"));

        operacaoFundoQuery.InnerJoin(clienteQuery).On(operacaoFundoQuery.IdCliente == clienteQuery.IdCliente);
        operacaoFundoQuery.InnerJoin(carteiraQuery).On(operacaoFundoQuery.IdCarteira == carteiraQuery.IdCarteira);
        operacaoFundoQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);

        operacaoFundoQuery.Where(operacaoFundoQuery.TipoOperacao.NotIn((byte)TipoOperacaoFundo.Amortizacao, (byte)TipoOperacaoFundo.Juros),
                                 permissaoClienteQuery.IdUsuario == idUsuario);

        if (!String.IsNullOrEmpty(btnEditCodigoClienteFiltro.Text))
        {
            operacaoFundoQuery.Where(operacaoFundoQuery.IdCliente == Convert.ToInt32(btnEditCodigoClienteFiltro.Text));
        }

        if (!String.IsNullOrEmpty(btnEditCodigoCarteiraFiltro.Text))
        {
            operacaoFundoQuery.Where(operacaoFundoQuery.IdCarteira == Convert.ToInt32(btnEditCodigoCarteiraFiltro.Text));
        }

        if (!String.IsNullOrEmpty(textDataInicio.Text))
        {
            operacaoFundoQuery.Where(operacaoFundoQuery.DataOperacao.GreaterThanOrEqual(textDataInicio.Text));
        }

        if (!String.IsNullOrEmpty(textDataFim.Text))
        {
            operacaoFundoQuery.Where(operacaoFundoQuery.DataOperacao.LessThanOrEqual(textDataFim.Text));
        }

        operacaoFundoQuery.OrderBy(operacaoFundoQuery.DataOperacao.Descending);

        OperacaoFundoCollection coll = new OperacaoFundoCollection();
        coll.Load(operacaoFundoQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        ClienteCollection coll = new ClienteCollection();
        coll.BuscaClientesComAcesso(Context.User.Identity.Name);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSLocalNegociacao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        LocalNegociacaoCollection coll = new LocalNegociacaoCollection();

        coll.Query.OrderBy(coll.Query.IdLocalNegociacao.Ascending);
        coll.LoadAll();

        e.Collection = coll;
    }

    protected void EsDSCategoriaMovimentacao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CategoriaMovimentacaoCollection coll = new CategoriaMovimentacaoCollection();
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CarteiraCollection coll = new CarteiraCollection();

        CarteiraQuery carteiraQuery = new CarteiraQuery("A");
        ClienteQuery clienteQuery = new ClienteQuery("C");
        UsuarioQuery usuarioQuery = new UsuarioQuery("U");
        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");

        carteiraQuery.Select(carteiraQuery.IdCarteira, carteiraQuery.Nome, carteiraQuery.Apelido);
        //
        carteiraQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == carteiraQuery.IdCarteira);
        carteiraQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        carteiraQuery.InnerJoin(usuarioQuery).On(permissaoClienteQuery.IdUsuario == usuarioQuery.IdUsuario);
        //
        carteiraQuery.Where(usuarioQuery.Login == Context.User.Identity.Name,
                            clienteQuery.StatusAtivo.Equal(StatusAtivoCliente.Ativo),
                            clienteQuery.IdTipo.In(TipoClienteFixo.Clube, TipoClienteFixo.Fundo, TipoClienteFixo.FDIC),
                            clienteQuery.TipoControle.NotEqual((byte)TipoControleCliente.ApenasCotacao));

        coll.Load(carteiraQuery);

        carteiraQuery = new CarteiraQuery("A");
        clienteQuery = new ClienteQuery("C");
        //
        carteiraQuery.Select(carteiraQuery.IdCarteira, carteiraQuery.Nome, carteiraQuery.Apelido);
        carteiraQuery.InnerJoin(clienteQuery).On(clienteQuery.IdCliente == carteiraQuery.IdCarteira);
        carteiraQuery.Where(clienteQuery.TipoControle.Equal((byte)TipoControleCliente.ApenasCotacao),
                            clienteQuery.IdTipo.In(TipoClienteFixo.Clube, TipoClienteFixo.Fundo, TipoClienteFixo.FDIC),
                            clienteQuery.StatusAtivo.Equal(StatusAtivoCliente.Ativo));
        //
        CarteiraCollection carteiraCollection2 = new CarteiraCollection();
        carteiraCollection2.Load(carteiraQuery);
        //
        coll.Combine(carteiraCollection2);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSFormaLiquidacao_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        FormaLiquidacaoCollection coll = new FormaLiquidacaoCollection();
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSPosicaoFundo_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        if (gridCadastro.IsEditing)
        {
            ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
            ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;

            if (btnEditCodigoCarteira.Text != "" && btnEditCodigoCliente.Text != "")
            {
                PosicaoFundoCollection coll = new PosicaoFundoCollection();
                coll.Query.Where(coll.Query.IdCliente.Equal(Convert.ToInt32(btnEditCodigoCliente.Text)) &
                                 coll.Query.IdCarteira.Equal(Convert.ToInt32(btnEditCodigoCarteira.Text)) &
                                 coll.Query.Quantidade.NotEqual(0));
                coll.Query.OrderBy(coll.Query.DataConversao.Descending);
                coll.Query.Load();

                // Assign the esDataSourcSelectEvenArgs Collection property
                e.Collection = coll;
            }
            else
            {
                e.Collection = new PosicaoFundoCollection();
            }
        }
        else
        {
            e.Collection = new PosicaoFundoCollection();
        }
    }
    #endregion

    new protected void panelEdicao_Load(object sender, EventArgs e)
    {
        base.panelEdicao_Load(sender, e);

        if (gridCadastro.IsEditing && gridCadastro.IsNewRowEditing)
        {
            ASPxComboBox dropFormaLiquidacao = gridCadastro.FindEditFormTemplateControl("dropFormaLiquidacao") as ASPxComboBox;
            dropFormaLiquidacao.SelectedIndex = 0;
        }

        bool travaPainel = false;

        bool multiConta = ParametrosConfiguracaoSistema.Outras.MultiConta;
        Label labelConta = gridCadastro.FindEditFormTemplateControl("labelConta") as Label;
        ASPxComboBox dropConta = gridCadastro.FindEditFormTemplateControl("dropConta") as ASPxComboBox;

        if (!multiConta)
        {
            labelConta.Visible = false;
            dropConta.Visible = false;
        }

        if (gridCadastro.IsEditing && !gridCadastro.IsNewRowEditing)
        {
            if (gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "IdPosicaoResgatada").ToString() != "" &&
                hiddenNotaResgatada.Value == "")
            {
                hiddenNotaResgatada.Value = gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "IdPosicaoResgatada").ToString();
            }

            Label labelEdicao = gridCadastro.FindEditFormTemplateControl("labelEdicao") as Label;

            int idCliente = Convert.ToInt32(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "IdCliente"));
            DateTime dataOperacao = Convert.ToDateTime(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "DataOperacao"));

            Cliente cliente = new Cliente();
            if (cliente.LoadByPrimaryKey(idCliente))
            {
                DateTime dataDia = cliente.DataDia.Value;
                byte status = cliente.Status.Value;
                if (status == (byte)StatusCliente.Divulgado)
                {
                    travaPainel = true;
                    labelEdicao.Text = "Cliente está fechado. Dados não podem ser alterados.";
                }
                //else if (DateTime.Compare(dataDia, dataOperacao) > 0) {
                //    travaPainel = true;
                //    labelEdicao.Text = "Data informada anterior à data do cliente. Dados não podem ser alterados.";
                //}
            }
        }

        if (travaPainel)
        {
            LinkButton btnOK = gridCadastro.FindEditFormTemplateControl("btnOK") as LinkButton;
            btnOK.Visible = false;

            LinkButton btnObservacao = gridCadastro.FindEditFormTemplateControl("btnObservacao") as LinkButton;
            btnObservacao.Visible = true;
        }
    }

    protected void checkDepositoRetirada_OnDataBound(object sender, EventArgs e)
    {
        ASPxCheckBox checkDepositoRetirada = sender as ASPxCheckBox;
        ASPxDateEdit textDataRegistro = gridCadastro.FindEditFormTemplateControl("textDataRegistro") as ASPxDateEdit;
        if (Convert.ToBoolean(checkDepositoRetirada.Value))
        {
            textDataRegistro.ClientEnabled = true;
        }
        else
        {
            textDataRegistro.ClientEnabled = false;
        }
    }

    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        #region Tipos de Operação apenas para Fundo de Investimento
        List<byte> lstApenasParaFundo = new List<byte>();
        lstApenasParaFundo.Add((byte)TipoOperacaoFundo.IngressoAtivoImpactoCota);
        lstApenasParaFundo.Add((byte)TipoOperacaoFundo.IngressoAtivoImpactoQtde);
        lstApenasParaFundo.Add((byte)TipoOperacaoFundo.RetiradaAtivoImpactoCota);
        lstApenasParaFundo.Add((byte)TipoOperacaoFundo.RetiradaAtivoImpactoQtde);
        #endregion

        if (!gridCadastro.IsEditing) //Testa deleção
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdCliente");
            List<object> keyDataLiquidacao = gridCadastro.GetSelectedFieldValues("DataLiquidacao");
            List<object> keyDataConversao = gridCadastro.GetSelectedFieldValues("DataConversao");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idCliente = Convert.ToInt32(keyValuesId[i]);
                DateTime dataLiquidacao = Convert.ToDateTime(keyDataLiquidacao[i]);
                DateTime dataConversao = Convert.ToDateTime(keyDataConversao[i]);

                Cliente cliente = new Cliente();
                if (cliente.LoadByPrimaryKey(idCliente))
                {
                    DateTime dataDia = cliente.DataDia.Value;
                    byte status = cliente.Status.Value;
                    string clienteDescricao = cliente.Apelido + " (" + cliente.IdCliente.ToString() + ")";
                    DateTime dataAux = (DateTime.Compare(dataLiquidacao, dataConversao) > 0) ? dataConversao : dataLiquidacao;

                    if (status == (byte)StatusCliente.Divulgado)
                    {
                        e.Result = "Cliente " + clienteDescricao + " está fechado! Deleção cancelada.";
                        return;
                    }
                    else if (DateTime.Compare(dataDia, dataAux) > 0)
                    {
                        e.Result = "Lançamento com data " + dataAux.ToString().Substring(0, 10) + " anterior à data do cliente " + clienteDescricao + "! Deleção cancelada.";
                        return;
                    }
                }
            }
        }
        else
        {
            ASPxDateEdit textDataOperacao = gridCadastro.FindEditFormTemplateControl("textDataOperacao") as ASPxDateEdit;
            ASPxDateEdit textDataRegistro = gridCadastro.FindEditFormTemplateControl("textDataRegistro") as ASPxDateEdit;
            ASPxCheckBox checkDepositoRetirada = gridCadastro.FindEditFormTemplateControl("checkDepositoRetirada") as ASPxCheckBox;
            ASPxDateEdit textDataConversao = gridCadastro.FindEditFormTemplateControl("textDataConversao") as ASPxDateEdit;
            ASPxDateEdit textDataLiquidacao = gridCadastro.FindEditFormTemplateControl("textDataLiquidacao") as ASPxDateEdit;
            ASPxComboBox dropTipoOperacao = gridCadastro.FindEditFormTemplateControl("dropTipoOperacao") as ASPxComboBox;
            ASPxComboBox dropTipoResgate = gridCadastro.FindEditFormTemplateControl("dropTipoResgate") as ASPxComboBox;
            ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
            ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
            ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
            ASPxComboBox dropFormaLiquidacao = gridCadastro.FindEditFormTemplateControl("dropFormaLiquidacao") as ASPxComboBox;
            ASPxComboBox dropLocalNegociacao = gridCadastro.FindEditFormTemplateControl("dropLocalNegociacao") as ASPxComboBox;
            ASPxComboBox dropFieModalidade = gridCadastro.FindEditFormTemplateControl("dropFieModalidade") as ASPxComboBox;
            ASPxComboBox dropFieTabelaIr = gridCadastro.FindEditFormTemplateControl("dropFieTabelaIr") as ASPxComboBox;
            ASPxComboBox dropConta = gridCadastro.FindEditFormTemplateControl("dropConta") as ASPxComboBox;

            #region Campos obrigatórios
            List<Control> controles = new List<Control>();
            controles.Add(btnEditCodigoCliente);
            controles.Add(btnEditCodigoCarteira);
            controles.Add(textDataOperacao);
            controles.Add(dropTipoOperacao);
            controles.Add(textDataRegistro);
            controles.Add(dropFormaLiquidacao);
            controles.Add(textDataOperacao);
            controles.Add(textDataConversao);
            controles.Add(textDataLiquidacao);
            controles.Add(dropLocalNegociacao);

            if (base.TestaObrigatorio(controles) != "")
            {
                e.Result = "Campos com * são obrigatórios!";
                return;
            }

            #region Valida forma liquidação
            byte tipoOperacao = Convert.ToByte(dropTipoOperacao.SelectedItem.Value);
            if (!lstApenasParaFundo.Contains(tipoOperacao) && tipoOperacao != (byte)TipoOperacaoCotista.Deposito && tipoOperacao != (byte)TipoOperacaoCotista.Retirada)
            {
                controles = new List<Control>();
                controles.Add(dropFormaLiquidacao);

                if (base.TestaObrigatorio(controles) != "")
                {
                    e.Result = "Campos com * são obrigatórios!";
                    return;
                }
            }
            #endregion

            #endregion

            int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
            int idCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);
            DateTime dataOperacao = Convert.ToDateTime(textDataOperacao.Text);
            DateTime dataRegistro = Convert.ToDateTime(textDataRegistro.Text);

            Cliente clienteFundo = new Cliente();
            clienteFundo.Query.Select(clienteFundo.Query.IdMoeda);
            clienteFundo.Query.Where(clienteFundo.Query.IdCliente.Equal(idCarteira));
            clienteFundo.Query.Load();

            int idMoeda = clienteFundo.IdMoeda.Value;

            Cliente cliente = new Cliente();
            if (cliente.LoadByPrimaryKey(idCliente))
            {
                DateTime dataDia = cliente.DataDia.Value;
                DateTime dataInicio = cliente.DataInicio.Value;
                byte status = cliente.Status.Value;
                if (status == (byte)StatusCliente.Divulgado)
                {
                    e.Result = "Cliente está fechado! Operação não pode ser realizada.";
                    return;
                }
                else if (dataInicio > dataOperacao)
                {
                    e.Result = "Data informada não pode ser anterior a data de início do Cliente(" + dataInicio.ToString("dd/MM/yyyy") + ").";
                    return;
                }
                //else if (DateTime.Compare(dataDia, Convert.ToDateTime(textDataOperacao.Text)) > 0) {
                //    e.Result = "Data informada anterior à data do cliente! Operação não pode ser realizada.";
                //    return;
                //}
            }

            Carteira carteira = new Carteira();
            if (carteira.LoadByPrimaryKey(idCarteira))
            {
                string resgateLiberado = carteira.LiberaResgate;
                if (resgateLiberado == "N")
                {
                    e.Result = "Fundo " + idCarteira.ToString() + "  não está liberado para receber resgates.";
                    return;
                }
            }

            if (tipoOperacao != (byte)TipoOperacaoFundo.Aplicacao &&
                tipoOperacao != (byte)TipoOperacaoFundo.AplicacaoAcoesEspecial &&
                tipoOperacao != (byte)TipoOperacaoFundo.AplicacaoCotas &&
                tipoOperacao != (byte)TipoOperacaoFundo.AplicacaoCotasEspecial)
            {
                if (dropTipoResgate.SelectedIndex != -1)
                {
                    if (Convert.ToByte(dropTipoResgate.SelectedItem.Value) == (byte)TipoResgateFundo.Especifico &&
                        hiddenNotaResgatada.Value == "")
                    {
                        e.Result = "Para resgates específicos, deve ser selecionada uma nota de aplicação.";
                        return;
                    }
                }
            }

            if (tipoOperacao != (byte)TipoOperacaoFundo.ResgateTotal)
            {
                if (textValor.Text == "")
                {
                    e.Result = "Para operação diferente de resgate total, é preciso informar o campo de Valor/Qtde.";
                    return;
                }
            }
            else
            {
                OperacaoFundoCollection operacaoFundoColl = new OperacaoFundoCollection();
                operacaoFundoColl.Query.Where(operacaoFundoColl.Query.IdCarteira.Equal(idCarteira) &
                                                operacaoFundoColl.Query.IdCliente.Equal(idCliente) &
                                                operacaoFundoColl.Query.DataConversao.GreaterThanOrEqual(cliente.DataDia.Value) &
                                                operacaoFundoColl.Query.TipoOperacao.Equal(tipoOperacao));
                operacaoFundoColl.Query.OrderBy(operacaoFundoColl.Query.DataConversao.Descending);

                if (operacaoFundoColl.Query.Load())
                {
                    e.Result = "Já existe operação de resgate total agendada; ID -  " + operacaoFundoColl[0].IdOperacao.Value;
                    return;
                }
            }

            if (tipoOperacao == (byte)TipoOperacaoFundo.ComeCotas ||
                tipoOperacao == (byte)TipoOperacaoFundo.ResgateBruto ||
                tipoOperacao == (byte)TipoOperacaoFundo.ResgateCotas ||
                tipoOperacao == (byte)TipoOperacaoFundo.ResgateCotasEspecial ||
                tipoOperacao == (byte)TipoOperacaoFundo.ResgateLiquido ||
                tipoOperacao == (byte)TipoOperacaoFundo.ResgateTotal)
            {
                PosicaoFundoCollection posicaoFundoCollection = new PosicaoFundoCollection();
                posicaoFundoCollection.Query.Where(posicaoFundoCollection.Query.IdCliente.Equal(idCliente),
                                                   posicaoFundoCollection.Query.IdCarteira.Equal(idCarteira));
                if (!posicaoFundoCollection.Query.Load())
                {
                    e.Result = "O cliente não possui saldo de aplicação nesta carteira para resgate.";
                    return;
                }
            }

            DateTime dataConversao = Convert.ToDateTime(textDataConversao.Text);
            DateTime dataLiquidacao = Convert.ToDateTime(textDataLiquidacao.Text);

            if (idMoeda == (int)ListaMoedaFixo.Real)
            {
                if (!Calendario.IsDiaUtil(dataOperacao, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil))
                {
                    e.Result = "Data da operação não é dia útil.";
                    return;
                }
                if (!Calendario.IsDiaUtil(dataRegistro, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil))
                {
                    e.Result = "Data de registro não é dia útil.";
                    return;
                }
                if (!Calendario.IsDiaUtil(dataConversao, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil))
                {
                    e.Result = "Data da conversão não é dia útil.";
                    return;
                }
                if (!Calendario.IsDiaUtil(dataLiquidacao, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil))
                {
                    e.Result = "Data da liquidação não é dia útil.";
                    return;
                }
            }
            else
            {
                if (!Calendario.IsDiaUtil(dataOperacao, LocalFeriadoFixo.NovaYork, TipoFeriado.Outros))
                {
                    e.Result = "Data da operação não é dia útil.";
                    return;
                }
                if (!Calendario.IsDiaUtil(dataRegistro, LocalFeriadoFixo.NovaYork, TipoFeriado.Outros))
                {
                    e.Result = "Data de registro não é dia útil.";
                    return;
                }
                if (!Calendario.IsDiaUtil(dataConversao, LocalFeriadoFixo.NovaYork, TipoFeriado.Outros))
                {
                    e.Result = "Data da conversão não é dia útil.";
                    return;
                }
                if (!Calendario.IsDiaUtil(dataLiquidacao, LocalFeriadoFixo.NovaYork, TipoFeriado.Outros))
                {
                    e.Result = "Data da liquidação não é dia útil.";
                    return;
                }
            }

            if (DateTime.Compare(dataOperacao, dataConversao) > 0)
            {
                e.Result = "Data da operação não pode ser posterior à data da conversão.";
                return;
            }

            if (DateTime.Compare(dataOperacao, dataLiquidacao) > 0)
            {
                e.Result = "Data da operação não pode ser posterior à data da liquidação.";
                return;
            }

            #region Valida operação FIE (Fundo de investimento em previdência)
            carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);
            if (carteira.Fie.Equals("S") && (tipoOperacao != (byte)TipoOperacaoFundo.ResgateBruto &&
                                             tipoOperacao != (byte)TipoOperacaoFundo.ResgateCotas &&
                                             tipoOperacao != (byte)TipoOperacaoFundo.ResgateCotasEspecial &&
                                             tipoOperacao != (byte)TipoOperacaoFundo.ResgateLiquido &&
                                             tipoOperacao != (byte)TipoOperacaoFundo.ResgateTotal))
            {
                if (dropFieModalidade.SelectedIndex == -1)
                    e.Result = "Para fundos do tipo FIE, favor informar Fie/Modalidade.";

                if (dropFieTabelaIr.SelectedIndex == -1)
                    e.Result = "Para fundos do tipo FIE, favor informar Fie/Tabela IR.";
            }
            #endregion

            #region Valida Tipo Operação
            if (cliente.IdTipo.Value != (int)TipoClienteFixo.Fundo && lstApenasParaFundo.Contains(Convert.ToByte(dropTipoOperacao.SelectedItem.Value)))
            {
                e.Result = "Tipo de Operação permitida apenas para Cliente do Tipo 'Fundo de Investimento'!";
                return;
            }
            #endregion

            DateTime dataCliente = cliente.DataDia.Value;

            carteira = new Carteira();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(carteira.Query.ValorMinimoAplicacao);
            campos.Add(carteira.Query.ValorMinimoInicial);
            campos.Add(carteira.Query.ValorMinimoSaldo);
            campos.Add(carteira.Query.ValorMinimoResgate);
            campos.Add(carteira.Query.ValorMaximoAplicacao);
            campos.Add(carteira.Query.TipoFundo);
            carteira.LoadByPrimaryKey(campos, idCarteira);

            int tipoFundo = Carteira.RetornaCondominioCarteira(idCarteira, dataOperacao);
            if (tipoFundo == (int)TipoFundo.Fechado &&
                tipoOperacao != (byte)TipoOperacaoCotista.Aplicacao)
            {
                e.Result = "Fundo fechado, não é possível movimentar o mesmo!";
                return;
            }

            if (dataOperacao == dataCliente)
            {
                #region Avalia valores e saldo mínimo
                if (tipoOperacao == (byte)TipoOperacaoCotista.Aplicacao)
                {
                    if (carteira.ValorMinimoInicial.Value != 0 || carteira.ValorMinimoAplicacao.Value != 0)
                    {
                        decimal quantidadeCotas = 0;
                        PosicaoFundo posicaoFundo = new PosicaoFundo();
                        posicaoFundo.Query.Select(posicaoFundo.Query.Quantidade.Sum());
                        posicaoFundo.Query.Where(posicaoFundo.Query.IdCarteira.Equal(idCarteira),
                                                   posicaoFundo.Query.IdCliente.Equal(idCliente));
                        posicaoFundo.Query.Load();

                        if (posicaoFundo.Quantidade.HasValue)
                        {
                            quantidadeCotas = posicaoFundo.Quantidade.Value;
                        }

                        decimal valor = Convert.ToDecimal(textValor.Text);
                        if (quantidadeCotas == 0 && valor < carteira.ValorMinimoInicial.Value)
                        {
                            e.Result = "Operação com valor inferior ao valor mínimo inicial (" + carteira.ValorMinimoInicial.Value.ToString() + ") . Operação não pode ser realizada";
                            return;
                        }
                        else if (valor < carteira.ValorMinimoAplicacao.Value)
                        {
                            e.Result = "Operação com valor inferior ao valor de aplicação exigido (" + carteira.ValorMinimoAplicacao.Value.ToString() + ") . Operação não pode ser realizada";
                            return;
                        }
                    }

                    if (carteira.ValorMaximoAplicacao.GetValueOrDefault(0) != 0)
                    {
                        if (carteira.ValorMaximoAplicacao.Value < Convert.ToDecimal(textValor.Text))
                        {
                            e.Result = "Operação com valor superior ao valor de máximo de aplicação (" + carteira.ValorMaximoAplicacao.Value.ToString() + ") . Operação não pode ser realizada";
                            return;
                        }
                    }

                }
                else if (tipoOperacao == (byte)TipoOperacaoCotista.ResgateBruto ||
                         tipoOperacao == (byte)TipoOperacaoCotista.ResgateLiquido)
                {
                    if (carteira.ValorMinimoResgate.Value != 0 || carteira.ValorMinimoSaldo.Value != 0)
                    {
                        decimal saldoLiquido = 0;
                        PosicaoFundo posicaoFundo = new PosicaoFundo();
                        posicaoFundo.Query.Select(posicaoFundo.Query.ValorLiquido.Sum());
                        posicaoFundo.Query.Where(posicaoFundo.Query.IdCarteira.Equal(idCarteira),
                                                   posicaoFundo.Query.IdCliente.Equal(idCliente));
                        posicaoFundo.Query.Load();

                        if (posicaoFundo.ValorLiquido.HasValue)
                        {
                            saldoLiquido = posicaoFundo.ValorLiquido.Value;
                        }

                        decimal valor = Convert.ToDecimal(textValor.Text);
                        decimal saldoProjetado = saldoLiquido - valor;
                        if (saldoProjetado < carteira.ValorMinimoSaldo.Value)
                        {
                            e.Result = "Saldo projetado com valor inferior ao valor mínimo requerido (" + carteira.ValorMinimoSaldo.Value.ToString() + ") . Operação não pode ser realizada";
                            return;
                        }
                        else if (valor < carteira.ValorMinimoResgate.Value)
                        {
                            e.Result = "Operação com valor inferior ao valor de resgate exigido (" + carteira.ValorMinimoResgate.Value.ToString() + ") . Operação não pode ser realizada";
                            return;
                        }
                    }
                }
                #endregion
            }

            #region Operação de Resgate Agendada
            List<byte> lstTipoResgate = new List<byte>();
            lstTipoResgate.Add((byte)TipoOperacaoFundo.ResgateLiquido);
            lstTipoResgate.Add((byte)TipoOperacaoFundo.ResgateBruto);
            lstTipoResgate.Add((byte)TipoOperacaoFundo.ResgateCotas);
            lstTipoResgate.Add((byte)TipoOperacaoFundo.ResgateCotasEspecial);

            if (lstTipoResgate.Contains(tipoOperacao))
            {
                OperacaoFundoCollection operacaoFundoColl = new OperacaoFundoCollection();
                operacaoFundoColl.Query.Where(operacaoFundoColl.Query.IdCarteira.Equal(idCarteira) &
                                                operacaoFundoColl.Query.IdCliente.Equal(idCliente) &
                                                operacaoFundoColl.Query.DataConversao.GreaterThan(cliente.DataDia.Value) &
                                                operacaoFundoColl.Query.TipoResgate.IsNotNull());
                operacaoFundoColl.Query.OrderBy(operacaoFundoColl.Query.DataConversao.Ascending);

                if (operacaoFundoColl.Query.Load())
                {
                    StringBuilder strMensagem = new StringBuilder();
                    strMensagem.Append("CONFIRM|Os seguintes resgates estão agendados: \n");
                    foreach (OperacaoFundo operacaoFundo in operacaoFundoColl)
                    {
                        bool porValorLiquido = operacaoFundo.TipoOperacao.Value == (byte)TipoOperacaoFundo.ResgateLiquido;
                        bool porValorBruto = operacaoFundo.TipoOperacao.Value == (byte)TipoOperacaoFundo.ResgateBruto;
                        bool porQtde = operacaoFundo.TipoOperacao.Value == (byte)TipoOperacaoFundo.ResgateCotas || operacaoFundo.TipoOperacao.Value == (byte)TipoOperacaoFundo.ResgateCotasEspecial;

                        strMensagem.Append("ID.Op: " + operacaoFundo.IdOperacao.Value + "; ");
                        if (porQtde)
                            strMensagem.Append("Qtde.Cotas: " + operacaoFundo.Quantidade.GetValueOrDefault(0).ToString());
                        else if (porValorLiquido)
                            strMensagem.Append("Vl.Líquido: " + operacaoFundo.ValorLiquido.GetValueOrDefault(0).ToString());
                        else if (porValorBruto)
                            strMensagem.Append("Vl.Bruto: " + operacaoFundo.ValorBruto.GetValueOrDefault(0).ToString());

                        strMensagem.Append("; Dt.Conv: " + String.Format("{0:dd/MM/yyyy}", operacaoFundo.DataConversao.Value));

                        strMensagem.Append("\n");
                    }
                    strMensagem.Append("\nDeseja criar a operação?");
                    e.Result = strMensagem.ToString();
                    return;
                }
            }
            #endregion

            #region Operacao Retroativa
            if (ParametrosConfiguracaoSistema.Outras.PermitirOperacoesRetroativas)
            {
                if (DateTime.Compare(dataConversao, cliente.DataDia.Value) < 0)
                {
                    e.Result = "CONFIRM|Data de conversão anterior a data do Cliente. Favor processar a carteira novamente a partir de " + dataConversao.ToString("dd/MM/yyyy");
                    return;
                }
            }
            #endregion

        }
    }

    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "")
            {
                if (cliente.Status.Value != (byte)StatusCliente.Divulgado)
                {

                    if (cliente.IsAtivo)
                    {
                        PermissaoCliente permissaoCliente = new PermissaoCliente();
                        if (permissaoCliente.RetornaAcessoClienteComControle(idCliente, HttpContext.Current.User.Identity.Name))
                        {
                            nome = cliente.str.Apelido;
                        }
                        else
                        {
                            nome = "no_access";
                        }
                    }
                    else
                    {
                        nome = "no_active";
                    }
                }
                else
                {
                    nome = cliente.str.Apelido + "|status_closed";
                }
            }
        }
        e.Result = nome;
    }

    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxComboBox dropTipoOperacao = gridCadastro.FindEditFormTemplateControl("dropTipoOperacao") as ASPxComboBox;
        ASPxComboBox dropLocalNegociacao = gridCadastro.FindEditFormTemplateControl("dropLocalNegociacao") as ASPxComboBox;
        Carteira carteira = new Carteira();

        string nome = "";
        string resultado = "";
        DateTime? dataDia = null;
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCarteira = Convert.ToInt32(e.Parameter);
            if (carteira.LoadByPrimaryKey(idCarteira))
            {
                if (carteira.IsAtivo)
                {

                    int? idLocalNegociacao = null;
                    string strLocalNegociacao = string.Empty;

                    #region Busca local de Feriado
                    int? idLocalFeriado = null;
                    if (dropLocalNegociacao.SelectedIndex != -1)
                        idLocalNegociacao = Convert.ToInt32(dropLocalNegociacao.SelectedItem.Value);
                    else
                        idLocalNegociacao = carteira.UpToCliente.IdLocalNegociacao;

                    if (idLocalNegociacao.HasValue)
                    {
                        strLocalNegociacao = idLocalNegociacao.Value.ToString();
                        LocalNegociacao localNegociacao = new LocalNegociacao();
                        localNegociacao.LoadByPrimaryKey(idLocalNegociacao.Value);

                        idLocalFeriado = localNegociacao.IdLocalFeriado.Value;
                    }

                    #endregion


                    //O permissionamento da carteira é dado direto no cliente vinculado à carteira
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoCliente(idCarteira, HttpContext.Current.User.Identity.Name))
                    {
                        nome = carteira.str.Apelido;

                        //Se tiver o cliente preenchido, busco a data dia                        
                        if (btnEditCodigoCliente.Text != "")
                        {
                            int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);
                            Cliente cliente = new Cliente();
                            cliente.LoadByPrimaryKey(idCliente);

                            ASPxDateEdit textDataOperacao = gridCadastro.FindEditFormTemplateControl("textDataOperacao") as ASPxDateEdit;
                            if (textDataOperacao != null && textDataOperacao.Text != "")
                            {
                                dataDia = Convert.ToDateTime(textDataOperacao.Text);
                            }
                            else
                            {
                                dataDia = cliente.DataDia.Value;
                            }
                        }

                        if (dropTipoOperacao.SelectedIndex >= 0 && dataDia.HasValue)
                        {
                            DateTime dataConversao = new DateTime();
                            DateTime dataLiquidacao = new DateTime();

                            #region Parâmetros Avançados
                            byte tipoOperacaoCombo = Convert.ToByte(dropTipoOperacao.SelectedItem.Value);

                            #region populaListas
                            List<byte> lstOperacoesResgate = new List<byte>();
                            lstOperacoesResgate.Add((byte)TipoOperacaoFundo.ResgateBruto);
                            lstOperacoesResgate.Add((byte)TipoOperacaoFundo.ResgateCotas);
                            lstOperacoesResgate.Add((byte)TipoOperacaoFundo.ResgateCotasEspecial);
                            lstOperacoesResgate.Add((byte)TipoOperacaoFundo.ResgateLiquido);
                            lstOperacoesResgate.Add((byte)TipoOperacaoFundo.ResgateTotal);
                            #endregion

                            int tipoOperacao = lstOperacoesResgate.Contains(tipoOperacaoCombo) ? (int)TipoParametroLiqOperacao.Resgate : (int)TipoParametroLiqOperacao.Aplicacao;

                            ParametroLiqAvancadoAnaliticoCollection collLiquidacao = new ParametroLiqAvancadoAnaliticoCollection();
                            ParametroLiqAvancadoAnaliticoCollection collConvesao = new ParametroLiqAvancadoAnaliticoCollection();
                            ParametroLiqAvancadoAnaliticoCollection collOperacao = new ParametroLiqAvancadoAnaliticoCollection();
                            DateTime dataVigente = collOperacao.RetornaUltDataVigente(dataDia.Value, idCarteira, tipoOperacao);
                            if (dataVigente != new DateTime())
                            {
                                collOperacao = collLiquidacao.RetornaParametrosNaDataVigente(dataVigente, idCarteira, tipoOperacao, (int)TipoParametroLiquidacao.DataOperacao);
                                collLiquidacao = collLiquidacao.RetornaParametrosNaDataVigente(dataVigente, idCarteira, tipoOperacao, (int)TipoParametroLiquidacao.DataLiquidacao);
                                collConvesao = collConvesao.RetornaParametrosNaDataVigente(dataVigente, idCarteira, tipoOperacao, (int)TipoParametroLiquidacao.DataConversao);
                            }

                            if (collLiquidacao.Count > 0 && collConvesao.Count > 0)
                            {
                                int parametroDtOperacao = (int)TipoParametroLiquidacao.DataOperacao;
                                int parametroDtConversao = (int)TipoParametroLiquidacao.DataConversao;
                                int parametroDtLiquidacao = (int)TipoParametroLiquidacao.DataLiquidacao;

                                if (tipoOperacao == (int)TipoParametroLiqOperacao.Aplicacao)
                                {
                                    dataDia = collOperacao.RetornaDataCalculada(dataDia.Value, dataDia.Value, collOperacao, parametroDtOperacao, 0, idLocalFeriado);
                                    dataLiquidacao = collLiquidacao.RetornaDataCalculada(dataDia.Value, dataDia.Value, collLiquidacao, parametroDtLiquidacao, 0, idLocalFeriado);
                                    dataConversao = collConvesao.RetornaDataCalculada(dataDia.Value, dataLiquidacao, collConvesao, parametroDtConversao, 0, idLocalFeriado);
                                }
                                else if (tipoOperacao == (int)TipoParametroLiqOperacao.Resgate)
                                {
                                    dataDia = collOperacao.RetornaDataCalculada(dataDia.Value, dataDia.Value, collOperacao, parametroDtOperacao, 0, idLocalFeriado);
                                    dataConversao = collConvesao.RetornaDataCalculada(dataDia.Value, dataDia.Value, collConvesao, parametroDtConversao, 0, idLocalFeriado);
                                    dataLiquidacao = collLiquidacao.RetornaDataCalculada(dataDia.Value, dataConversao, collLiquidacao, parametroDtLiquidacao, 0, idLocalFeriado);
                                }
                            }
                            #endregion
                            #region Parâmetros Básicos
                            else
                            {

                                int contagemResgate = carteira.ContagemDiasConversaoResgate.Value;

                                //Trata campos com dataConversao e dataLiquidacao
                                int diasConversao = 0;
                                int diasLiquidacao = 0;
                                if (tipoOperacaoCombo == (byte)TipoOperacaoFundo.Aplicacao ||
                                    tipoOperacaoCombo == (byte)TipoOperacaoFundo.AplicacaoAcoesEspecial)
                                {
                                    diasConversao = carteira.DiasCotizacaoAplicacao.Value;
                                    diasLiquidacao = carteira.DiasLiquidacaoAplicacao.Value;
                                }
                                else if (tipoOperacaoCombo == (byte)TipoOperacaoFundo.ResgateBruto ||
                                         tipoOperacaoCombo == (byte)TipoOperacaoFundo.ResgateCotas ||
                                         tipoOperacaoCombo == (byte)TipoOperacaoFundo.ResgateLiquido ||
                                         tipoOperacaoCombo == (byte)TipoOperacaoFundo.ResgateTotal)
                                {
                                    diasConversao = carteira.DiasCotizacaoResgate.Value;
                                    diasLiquidacao = carteira.DiasLiquidacaoResgate.Value;
                                }
                                if (tipoOperacaoCombo == (byte)TipoOperacaoFundo.AplicacaoCotasEspecial ||
                                    tipoOperacaoCombo == (byte)TipoOperacaoFundo.ResgateCotasEspecial ||
                                    tipoOperacaoCombo == (byte)TipoOperacaoFundo.ComeCotas)
                                {
                                    diasConversao = 0;
                                    diasLiquidacao = 0;
                                }

                                if (contagemResgate == (byte)ContagemDiasLiquidacaoResgate.DiasUteis ||
                                    (tipoOperacaoCombo == (byte)TipoOperacaoFundo.Aplicacao ||
                                     tipoOperacaoCombo == (byte)TipoOperacaoFundo.AplicacaoAcoesEspecial))
                                {
                                    dataConversao = Calendario.AdicionaDiaUtil((DateTime)dataDia, diasConversao);
                                    dataLiquidacao = Calendario.AdicionaDiaUtil((DateTime)dataDia, diasLiquidacao);
                                }
                                else
                                {
                                    //Conta por Dias Corridos
                                    dataConversao = dataDia.Value.AddDays(diasConversao);

                                    if (!Calendario.IsDiaUtil(dataConversao))
                                    {
                                        dataConversao = Calendario.AdicionaDiaUtil(dataConversao, 1);
                                    }

                                    //Conta por Dias Úteis em cima da data de conversão
                                    dataLiquidacao = Calendario.AdicionaDiaUtil(dataConversao, diasLiquidacao);
                                }
                            }
                            //
                            #endregion
                            resultado = nome + "|" + dataDia.ToString().Substring(0, 10) + "|" +
                                            dataConversao.ToString().Substring(0, 10) + "|" +
                                            dataLiquidacao.ToString().Substring(0, 10) + "|" + Thread.CurrentThread.CurrentCulture.Name + "|" + strLocalNegociacao + "|" +
                                            carteira.Fie;
                        }
                        else
                        {
                            if (dataDia.HasValue)
                            {
                                resultado = nome + "|" + dataDia.ToString().Substring(0, 10) + "|" +
                                            "" + "|" +
                                            "" + "|" + Thread.CurrentThread.CurrentCulture.Name + "|" + strLocalNegociacao + "|" +
                                            carteira.Fie;
                            }
                            else
                            {
                                resultado = nome;
                            }
                        }
                    }
                    else
                    {
                        resultado = "no_access";
                    }
                }
                else
                {
                    resultado = "no_active";
                }
            }
        }

        e.Result = resultado;
    }

    /// <summary>
    /// Tratamento p/ Carteira Filtro
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback3_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string nome = "";
        string resultado = "";
        //
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCliente = Convert.ToInt32(e.Parameter);
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            if (cliente.str.Apelido != "")
            {
                if (cliente.IsAtivo)
                {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    if (permissaoCliente.RetornaAcessoCliente(idCliente, HttpContext.Current.User.Identity.Name))
                    {
                        nome = cliente.str.Apelido;
                        DateTime dataDia = cliente.DataDia.Value;
                        byte status = cliente.Status.Value;

                        if (status == (byte)StatusCliente.Divulgado && gridCadastro.IsEditing)
                        {
                            resultado = "status_closed";
                        }
                        else
                        {
                            resultado = nome + "|" + dataDia.ToString().Substring(0, 10) + "|" + Thread.CurrentThread.CurrentCulture.Name;
                        }
                    }
                    else
                    {
                        resultado = "no_access";
                    }
                }
                else
                {
                    resultado = "no_active";
                }
            }
        }
        e.Result = resultado;
    }

    protected void gridPosicaoFundo_CustomDataCallback(object sender, ASPxGridViewCustomDataCallbackEventArgs e)
    {
        string dataConversao = Convert.ToString(gridPosicaoFundo.GetRowValues(Convert.ToInt32(e.Parameters), "DataConversao"));
        string mensagemNotaResgatada = "Nota resgatada" +
                    "\n-----------------------------" +
                    "\nData: " + dataConversao.Substring(0, 10) +
                    "\nQtde: " + gridPosicaoFundo.GetRowValues(Convert.ToInt32(e.Parameters), "Quantidade");

        e.Result = gridPosicaoFundo.GetRowValues(Convert.ToInt32(e.Parameters), "IdPosicao") + "|" + mensagemNotaResgatada;
    }

    protected void gridPosicaoFundo_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        UtilitarioGrid.SetaCoresGrid(e);
    }

    protected void gridPosicaoFundo_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        gridPosicaoFundo.DataBind();
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        List<byte> lstTipoOperacaoSemLiquidacao = new List<byte>();
        lstTipoOperacaoSemLiquidacao.Add((byte)TipoOperacaoFundo.IngressoAtivoImpactoCota);
        lstTipoOperacaoSemLiquidacao.Add((byte)TipoOperacaoFundo.IngressoAtivoImpactoQtde);
        lstTipoOperacaoSemLiquidacao.Add((byte)TipoOperacaoFundo.RetiradaAtivoImpactoCota);
        lstTipoOperacaoSemLiquidacao.Add((byte)TipoOperacaoFundo.RetiradaAtivoImpactoQtde);

        int idOperacao = (int)e.Keys[0];
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxDateEdit textDataOperacao = gridCadastro.FindEditFormTemplateControl("textDataOperacao") as ASPxDateEdit;
        ASPxDateEdit textDataRegistro = gridCadastro.FindEditFormTemplateControl("textDataRegistro") as ASPxDateEdit;
        ASPxCheckBox checkDepositoRetirada = gridCadastro.FindEditFormTemplateControl("checkDepositoRetirada") as ASPxCheckBox;
        ASPxDateEdit textDataConversao = gridCadastro.FindEditFormTemplateControl("textDataConversao") as ASPxDateEdit;
        ASPxDateEdit textDataLiquidacao = gridCadastro.FindEditFormTemplateControl("textDataLiquidacao") as ASPxDateEdit;
        ASPxComboBox dropTipoOperacao = gridCadastro.FindEditFormTemplateControl("dropTipoOperacao") as ASPxComboBox;
        ASPxComboBox dropTipoResgate = gridCadastro.FindEditFormTemplateControl("dropTipoResgate") as ASPxComboBox;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
        ASPxComboBox dropFormaLiquidacao = gridCadastro.FindEditFormTemplateControl("dropFormaLiquidacao") as ASPxComboBox;
        TextBox textObservacao = gridCadastro.FindEditFormTemplateControl("textObservacao") as TextBox;
        ASPxComboBox dropLocalNegociacao = gridCadastro.FindEditFormTemplateControl("dropLocalNegociacao") as ASPxComboBox;
        ASPxComboBox dropFieModalidade = gridCadastro.FindEditFormTemplateControl("dropFieModalidade") as ASPxComboBox;
        ASPxComboBox dropFieTabelaIr = gridCadastro.FindEditFormTemplateControl("dropFieTabelaIr") as ASPxComboBox;
        ASPxComboBox dropTrader = gridCadastro.FindEditFormTemplateControl("dropTrader") as ASPxComboBox;
        ASPxComboBox dropConta = gridCadastro.FindEditFormTemplateControl("dropConta") as ASPxComboBox;
        ASPxComboBox dropCategoriaMovimentacao = gridCadastro.FindEditFormTemplateControl("dropCategoriaMovimentacao") as ASPxComboBox;

        OperacaoFundo operacaoFundo = new OperacaoFundo();
        if (operacaoFundo.LoadByPrimaryKey(idOperacao))
        {
            int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);

            operacaoFundo.IdCliente = idCliente;
            operacaoFundo.IdCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);
            operacaoFundo.DataOperacao = Convert.ToDateTime(textDataOperacao.Text);
            operacaoFundo.DataRegistro = Convert.ToDateTime(textDataRegistro.Text);
            operacaoFundo.DepositoRetirada = checkDepositoRetirada.Checked;
            operacaoFundo.DataConversao = Convert.ToDateTime(textDataConversao.Text);
            operacaoFundo.DataLiquidacao = Convert.ToDateTime(textDataLiquidacao.Text);
            operacaoFundo.TipoOperacao = Convert.ToByte(dropTipoOperacao.SelectedItem.Value);
            operacaoFundo.DataAgendamento = Convert.ToDateTime(textDataOperacao.Text); //TODO AJUSTAR DEPOIS
            operacaoFundo.IdLocalNegociacao = Convert.ToInt16(dropLocalNegociacao.SelectedItem.Value);

            if (dropTrader.SelectedIndex > -1)
                operacaoFundo.IdTrader = Convert.ToInt32(dropTrader.SelectedItem.Value);
            else
                operacaoFundo.IdTrader = null;


            if (dropCategoriaMovimentacao.SelectedIndex > -1)
            {
                operacaoFundo.IdCategoriaMovimentacao = Convert.ToInt32(dropCategoriaMovimentacao.SelectedItem.Value);
            }
            else
            {
                operacaoFundo.IdCategoriaMovimentacao = null;
            }

            if (dropTipoResgate.SelectedIndex != -1)
            {
                operacaoFundo.TipoResgate = Convert.ToByte(dropTipoResgate.SelectedItem.Value);
            }

            //Salva o valor dependendo do tipo de operação, o resto força zerado
            //Resgate total força tudo zerado!
            operacaoFundo.ValorBruto = 0;
            operacaoFundo.ValorLiquido = 0;
            operacaoFundo.Quantidade = 0;
            switch (operacaoFundo.TipoOperacao)
            {
                case (byte)TipoOperacaoFundo.Aplicacao:
                case (byte)TipoOperacaoFundo.AplicacaoAcoesEspecial:
                case (byte)TipoOperacaoFundo.ResgateBruto:
                    operacaoFundo.ValorBruto = Convert.ToDecimal(textValor.Text);
                    break;
                case (byte)TipoOperacaoFundo.AplicacaoCotasEspecial:
                case (byte)TipoOperacaoFundo.ComeCotas:
                case (byte)TipoOperacaoFundo.ResgateCotas:
                case (byte)TipoOperacaoFundo.ResgateCotasEspecial:
                case (byte)TipoOperacaoFundo.IngressoAtivoImpactoCota:
                case (byte)TipoOperacaoFundo.IngressoAtivoImpactoQtde:
                case (byte)TipoOperacaoFundo.RetiradaAtivoImpactoCota:
                case (byte)TipoOperacaoFundo.RetiradaAtivoImpactoQtde:
                case (byte)TipoOperacaoFundo.AplicacaoCotas:
                    operacaoFundo.Quantidade = Convert.ToDecimal(textValor.Text);
                    break;
                case (byte)TipoOperacaoFundo.ResgateLiquido:
                    operacaoFundo.ValorLiquido = Convert.ToDecimal(textValor.Text);
                    break;
            }

            if (operacaoFundo.TipoOperacao.Value == (byte)TipoOperacaoFundo.Aplicacao ||
            operacaoFundo.TipoOperacao.Value == (byte)TipoOperacaoFundo.AplicacaoCotasEspecial ||
            operacaoFundo.TipoOperacao.Value == (byte)TipoOperacaoFundo.AplicacaoAcoesEspecial ||
            operacaoFundo.TipoOperacao.Value == (byte)TipoOperacaoFundo.ComeCotas ||
            operacaoFundo.TipoOperacao.Value == (byte)TipoOperacaoFundo.IngressoAtivoImpactoCota ||
            operacaoFundo.TipoOperacao.Value == (byte)TipoOperacaoFundo.AplicacaoCotas ||
            operacaoFundo.TipoOperacao.Value == (byte)TipoOperacaoFundo.IngressoAtivoImpactoQtde)
            {
                operacaoFundo.TipoResgate = null;
            }
            else
            {
                //Se não informar o tipo de resgate, joga default = FIFO
                if (dropTipoResgate.SelectedIndex == -1)
                {
                    operacaoFundo.TipoResgate = (byte)TipoResgateFundo.FIFO;
                }
            }

            if (!lstTipoOperacaoSemLiquidacao.Contains(operacaoFundo.TipoOperacao.Value))
                operacaoFundo.IdFormaLiquidacao = Convert.ToByte(dropFormaLiquidacao.SelectedItem.Value);
            else
                operacaoFundo.IdFormaLiquidacao = null;

            operacaoFundo.Observacao = textObservacao.Text.ToString();

            if (hiddenNotaResgatada.Value != "")
            {
                operacaoFundo.IdPosicaoResgatada = Convert.ToInt32(hiddenNotaResgatada.Value);

                PosicaoFundo posicaoFundo = new PosicaoFundo();
                posicaoFundo.Query.Select(posicaoFundo.Query.IdOperacao);
                posicaoFundo.Query.Where(posicaoFundo.Query.IdPosicao.Equal(operacaoFundo.IdPosicaoResgatada.Value));
                if (posicaoFundo.Query.Load())
                {
                    if (posicaoFundo.IdOperacao.HasValue)
                    {
                        operacaoFundo.IdOperacaoResgatada = posicaoFundo.IdOperacao.Value;
                    }
                }
            }

            #region Verifica se é FIE (Fundo de Investimento em Previdência)
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(operacaoFundo.IdCarteira.Value);
            if (carteira.Fie.Equals("S") && (operacaoFundo.TipoOperacao.Value != (byte)TipoOperacaoFundo.ResgateBruto &&
                                             operacaoFundo.TipoOperacao.Value != (byte)TipoOperacaoFundo.ResgateCotas &&
                                             operacaoFundo.TipoOperacao.Value != (byte)TipoOperacaoFundo.ResgateCotasEspecial &&
                                             operacaoFundo.TipoOperacao.Value != (byte)TipoOperacaoFundo.ResgateLiquido &&
                                             operacaoFundo.TipoOperacao.Value != (byte)TipoOperacaoFundo.ResgateTotal))
            {
                operacaoFundo.FieModalidade = Convert.ToInt16(dropFieModalidade.SelectedItem.Value);
                operacaoFundo.FieTabelaIr = Convert.ToInt16(dropFieTabelaIr.SelectedItem.Value);

                //Se for VGBL, grava tipo de tabela (Progressiva/Regressiva) para controle de histórico
                if (operacaoFundo.FieModalidade.Equals(FieModalidade.VGBL.GetHashCode()))
                {
                    TributacaoFundoFie tributacaoFundoFie = new TributacaoFundoFie();
                    if (tributacaoFundoFie.LoadByPrimaryKey(operacaoFundo.DataRegistro.Value, operacaoFundo.IdOperacao.Value))
                    {
                        tributacaoFundoFie.FieTabelaIr = Convert.ToInt16(dropFieTabelaIr.SelectedItem.Value);
                    }
                    else
                    {
                        tributacaoFundoFie.Data = operacaoFundo.DataRegistro.Value;
                        tributacaoFundoFie.IdOperacao = operacaoFundo.IdOperacao.Value;
                        tributacaoFundoFie.FieTabelaIr = Convert.ToInt16(dropFieTabelaIr.SelectedItem.Value);
                    }

                    tributacaoFundoFie.Save();
                }
            }
            #endregion

            operacaoFundo.RegistroEditado = "S";

            if (ParametrosConfiguracaoSistema.Outras.MultiConta && dropConta.Value != null && !string.IsNullOrEmpty(dropConta.Value.ToString()))
            {
                operacaoFundo.IdConta = Convert.ToInt32(dropConta.Value);
            }

            operacaoFundo.Save();

            #region Operação retroativa - Verifica se deve marcar o cliente para efetuar o reprocessamento
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);
            if (DateTime.Compare(cliente.DataDia.Value, operacaoFundo.DataOperacao.Value) > 0)
            {
                BoletoRetroativo boletoRetroativo = new BoletoRetroativo();
                if (boletoRetroativo.LoadByPrimaryKey(idCliente))
                {
                    if (DateTime.Compare(boletoRetroativo.DataBoleto.Value, operacaoFundo.DataOperacao.Value) > 0)
                    {
                        boletoRetroativo.DataBoleto = operacaoFundo.DataOperacao.Value;
                    }
                }
                else
                {
                    boletoRetroativo.IdCliente = idCliente;
                    boletoRetroativo.DataBoleto = operacaoFundo.DataOperacao.Value;
                    boletoRetroativo.TipoMercado = (int)TipoMercado.Fundos;
                }

                boletoRetroativo.Save();
            }
            #endregion

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de OperacaoFundo - Operacao: Update OperacaoFundo: " + idOperacao + UtilitarioWeb.ToString(operacaoFundo),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion

            //Atualiza StatusRealTime para executar***********
            cliente.SetaFlagRealTime(idCliente, (byte)StatusRealTimeCliente.Executar);
            //************************************************
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    private void SalvarNovo()
    {
        List<byte> lstTipoOperacaoSemLiquidacao = new List<byte>();
        lstTipoOperacaoSemLiquidacao.Add((byte)TipoOperacaoFundo.IngressoAtivoImpactoCota);
        lstTipoOperacaoSemLiquidacao.Add((byte)TipoOperacaoFundo.IngressoAtivoImpactoQtde);
        lstTipoOperacaoSemLiquidacao.Add((byte)TipoOperacaoFundo.RetiradaAtivoImpactoCota);
        lstTipoOperacaoSemLiquidacao.Add((byte)TipoOperacaoFundo.RetiradaAtivoImpactoQtde);

        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
        ASPxDateEdit textDataOperacao = gridCadastro.FindEditFormTemplateControl("textDataOperacao") as ASPxDateEdit;
        ASPxDateEdit textDataRegistro = gridCadastro.FindEditFormTemplateControl("textDataRegistro") as ASPxDateEdit;
        ASPxCheckBox checkDepositoRetirada = gridCadastro.FindEditFormTemplateControl("checkDepositoRetirada") as ASPxCheckBox;
        ASPxDateEdit textDataConversao = gridCadastro.FindEditFormTemplateControl("textDataConversao") as ASPxDateEdit;
        ASPxDateEdit textDataLiquidacao = gridCadastro.FindEditFormTemplateControl("textDataLiquidacao") as ASPxDateEdit;
        ASPxComboBox dropTipoOperacao = gridCadastro.FindEditFormTemplateControl("dropTipoOperacao") as ASPxComboBox;
        ASPxComboBox dropTipoResgate = gridCadastro.FindEditFormTemplateControl("dropTipoResgate") as ASPxComboBox;
        ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;
        ASPxComboBox dropFormaLiquidacao = gridCadastro.FindEditFormTemplateControl("dropFormaLiquidacao") as ASPxComboBox;
        TextBox textObservacao = gridCadastro.FindEditFormTemplateControl("textObservacao") as TextBox;
        ASPxComboBox dropLocalNegociacao = gridCadastro.FindEditFormTemplateControl("dropLocalNegociacao") as ASPxComboBox;
        ASPxComboBox dropFieModalidade = gridCadastro.FindEditFormTemplateControl("dropFieModalidade") as ASPxComboBox;
        ASPxComboBox dropFieTabelaIr = gridCadastro.FindEditFormTemplateControl("dropFieTabelaIr") as ASPxComboBox;
        ASPxComboBox dropTrader = gridCadastro.FindEditFormTemplateControl("dropTrader") as ASPxComboBox;
        ASPxComboBox dropConta = gridCadastro.FindEditFormTemplateControl("dropConta") as ASPxComboBox;
        ASPxComboBox dropCategoriaMovimentacao = gridCadastro.FindEditFormTemplateControl("dropCategoriaMovimentacao") as ASPxComboBox;

        OperacaoFundo operacaoFundo = new OperacaoFundo();

        int idCliente = Convert.ToInt32(btnEditCodigoCliente.Text);

        operacaoFundo.IdCliente = idCliente;
        operacaoFundo.IdCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);
        operacaoFundo.DataOperacao = Convert.ToDateTime(textDataOperacao.Text);
        operacaoFundo.DataRegistro = Convert.ToDateTime(textDataRegistro.Text);
        operacaoFundo.DepositoRetirada = checkDepositoRetirada.Checked;
        operacaoFundo.DataConversao = Convert.ToDateTime(textDataConversao.Text);
        operacaoFundo.DataLiquidacao = Convert.ToDateTime(textDataLiquidacao.Text);
        operacaoFundo.TipoOperacao = Convert.ToByte(dropTipoOperacao.SelectedItem.Value);
        operacaoFundo.DataAgendamento = Convert.ToDateTime(textDataOperacao.Text); //TODO AJUSTAR DEPOIS
        operacaoFundo.IdLocalNegociacao = Convert.ToInt16(dropLocalNegociacao.SelectedItem.Value);

        if (dropTrader.SelectedIndex > -1)
            operacaoFundo.IdTrader = Convert.ToInt32(dropTrader.SelectedItem.Value);
        else
            operacaoFundo.IdTrader = null;

        if (dropTipoResgate.SelectedIndex != -1)
        {
            operacaoFundo.TipoResgate = Convert.ToByte(dropTipoResgate.SelectedItem.Value);
        }

        if (dropCategoriaMovimentacao.SelectedIndex > -1)
        {
            operacaoFundo.IdCategoriaMovimentacao = Convert.ToInt32(dropCategoriaMovimentacao.SelectedItem.Value);
        }
        else
        {
            operacaoFundo.IdCategoriaMovimentacao = null;
        }

        //Salva o valor dependendo do tipo de operação, o resto força zerado        
        //Resgate total força tudo zerado!
        operacaoFundo.ValorBruto = 0;
        operacaoFundo.ValorLiquido = 0;
        operacaoFundo.Quantidade = 0;
        switch (operacaoFundo.TipoOperacao)
        {
            case (byte)TipoOperacaoFundo.Aplicacao:
            case (byte)TipoOperacaoFundo.AplicacaoAcoesEspecial:
            case (byte)TipoOperacaoFundo.ResgateBruto:
                operacaoFundo.ValorBruto = Convert.ToDecimal(textValor.Text);
                break;
            case (byte)TipoOperacaoFundo.AplicacaoCotasEspecial:
            case (byte)TipoOperacaoFundo.ComeCotas:
            case (byte)TipoOperacaoFundo.ResgateCotas:
            case (byte)TipoOperacaoFundo.ResgateCotasEspecial:
            case (byte)TipoOperacaoFundo.AjustePosicao:
            case (byte)TipoOperacaoFundo.IngressoAtivoImpactoCota:
            case (byte)TipoOperacaoFundo.IngressoAtivoImpactoQtde:
            case (byte)TipoOperacaoFundo.RetiradaAtivoImpactoCota:
            case (byte)TipoOperacaoFundo.RetiradaAtivoImpactoQtde:
            case (byte)TipoOperacaoFundo.AplicacaoCotas:
                operacaoFundo.Quantidade = Convert.ToDecimal(textValor.Text);
                break;
            case (byte)TipoOperacaoFundo.ResgateLiquido:
                operacaoFundo.ValorLiquido = Convert.ToDecimal(textValor.Text);
                break;
        }

        if (operacaoFundo.TipoOperacao.Value == (byte)TipoOperacaoFundo.Aplicacao ||
            operacaoFundo.TipoOperacao.Value == (byte)TipoOperacaoFundo.AplicacaoCotasEspecial ||
            operacaoFundo.TipoOperacao.Value == (byte)TipoOperacaoFundo.AplicacaoAcoesEspecial ||
            operacaoFundo.TipoOperacao.Value == (byte)TipoOperacaoFundo.ComeCotas ||
            operacaoFundo.TipoOperacao.Value == (byte)TipoOperacaoFundo.AjustePosicao ||
            operacaoFundo.TipoOperacao.Value == (byte)TipoOperacaoFundo.IngressoAtivoImpactoCota ||
            operacaoFundo.TipoOperacao.Value == (byte)TipoOperacaoFundo.AplicacaoCotas ||
            operacaoFundo.TipoOperacao.Value == (byte)TipoOperacaoFundo.IngressoAtivoImpactoQtde)
        {
            operacaoFundo.TipoResgate = null;
        }
        else
        {
            //Se não informar o tipo de resgate, joga default = FIFO
            if (dropTipoResgate.SelectedIndex == -1)
            {
                operacaoFundo.TipoResgate = (byte)TipoResgateFundo.FIFO;
            }
        }

        if (!lstTipoOperacaoSemLiquidacao.Contains(operacaoFundo.TipoOperacao.Value))
            operacaoFundo.IdFormaLiquidacao = Convert.ToByte(dropFormaLiquidacao.SelectedItem.Value);
        else
            operacaoFundo.IdFormaLiquidacao = null;

        operacaoFundo.Observacao = textObservacao.Text.ToString();

        if (hiddenNotaResgatada.Value != "")
        {
            operacaoFundo.IdPosicaoResgatada = Convert.ToInt32(hiddenNotaResgatada.Value);

            PosicaoFundo posicaoFundo = new PosicaoFundo();
            posicaoFundo.Query.Select(posicaoFundo.Query.IdOperacao);
            posicaoFundo.Query.Where(posicaoFundo.Query.IdPosicao.Equal(operacaoFundo.IdPosicaoResgatada.Value));
            if (posicaoFundo.Query.Load())
            {
                if (posicaoFundo.IdOperacao.HasValue)
                {
                    operacaoFundo.IdOperacaoResgatada = posicaoFundo.IdOperacao.Value;
                }
            }
        }

        if (ParametrosConfiguracaoSistema.Outras.MultiConta && dropConta.Value != null && !string.IsNullOrEmpty(dropConta.Value.ToString()))
        {
            operacaoFundo.IdConta = Convert.ToInt32(dropConta.Value);
        }

        operacaoFundo.Fonte = (byte)FonteOperacaoFundo.Manual;
        operacaoFundo.RegistroEditado = "N";
        operacaoFundo.ValoresColados = "N";
        operacaoFundo.Save();

        #region Verifica se é FIE (Fundo de Investimento em Previdência)
        Carteira carteira = new Carteira();
        carteira.LoadByPrimaryKey(operacaoFundo.IdCarteira.Value);
        if (carteira.Fie.Equals("S") && (operacaoFundo.TipoOperacao.Value != (byte)TipoOperacaoFundo.ResgateBruto &&
                                         operacaoFundo.TipoOperacao.Value != (byte)TipoOperacaoFundo.ResgateCotas &&
                                         operacaoFundo.TipoOperacao.Value != (byte)TipoOperacaoFundo.ResgateCotasEspecial &&
                                         operacaoFundo.TipoOperacao.Value != (byte)TipoOperacaoFundo.ResgateLiquido &&
                                         operacaoFundo.TipoOperacao.Value != (byte)TipoOperacaoFundo.ResgateTotal))
        {
            operacaoFundo.FieModalidade = Convert.ToInt16(dropFieModalidade.SelectedItem.Value);
            operacaoFundo.FieTabelaIr = Convert.ToInt16(dropFieTabelaIr.SelectedItem.Value);
            operacaoFundo.Save();

            //Se for VGBL, grava tipo de tabela (Progressiva/Regressiva) para controle de histórico
            if (operacaoFundo.FieModalidade.Equals(FieModalidade.VGBL.GetHashCode()))
            {
                TributacaoFundoFie tributacaoFundoFie = new TributacaoFundoFie();
                if (tributacaoFundoFie.LoadByPrimaryKey(operacaoFundo.DataRegistro.Value, operacaoFundo.IdOperacao.Value))
                {
                    tributacaoFundoFie.FieTabelaIr = Convert.ToInt16(dropFieTabelaIr.SelectedItem.Value);
                }
                else
                {
                    tributacaoFundoFie.Data = operacaoFundo.DataRegistro.Value;
                    tributacaoFundoFie.IdOperacao = operacaoFundo.IdOperacao.Value;
                    tributacaoFundoFie.FieTabelaIr = Convert.ToInt16(dropFieTabelaIr.SelectedItem.Value);
                }

                tributacaoFundoFie.Save();
            }

        }
        #endregion

        #region Operação retroativa - Verifica se deve marcar o cliente para efetuar o reprocessamento
        Cliente cliente = new Cliente();
        cliente.LoadByPrimaryKey(idCliente);
        if (DateTime.Compare(cliente.DataDia.Value, operacaoFundo.DataOperacao.Value) > 0)
        {
            BoletoRetroativo boletoRetroativo = new BoletoRetroativo();
            if (boletoRetroativo.LoadByPrimaryKey(idCliente))
            {
                if (DateTime.Compare(boletoRetroativo.DataBoleto.Value, operacaoFundo.DataOperacao.Value) > 0)
                {
                    boletoRetroativo.DataBoleto = operacaoFundo.DataOperacao.Value;
                }
            }
            else
            {
                boletoRetroativo.IdCliente = idCliente;
                boletoRetroativo.DataBoleto = operacaoFundo.DataOperacao.Value;
                boletoRetroativo.TipoMercado = (int)TipoMercado.Fundos;
            }

            boletoRetroativo.Save();
        }
        #endregion

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de OperacaoFundo - Operacao: Insert OperacaoFundo: " + operacaoFundo.IdOperacao + UtilitarioWeb.ToString(operacaoFundo),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion

        #region Faz uma inserção tb em OperacaoCotista se for o caso
        int idCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);
        int idCotista = 0;
        Cotista cotista = new Cotista();

        cotista.Query.Where(cotista.Query.IdClienteEspelho.Equal(idCliente));
        cotista.Query.Load();

        Cliente clienteFundo = new Cliente();
        clienteFundo.LoadByPrimaryKey(idCarteira);

        if (cotista.IdCotista.HasValue && operacaoFundo.IdCliente.Value != operacaoFundo.IdCarteira.Value &&
           (clienteFundo.TipoControle.Value == (byte)TipoControleCliente.Completo || clienteFundo.TipoControle.Value == (byte)TipoControleCliente.Cotista))
        {
            idCotista = cotista.IdCotista.Value;

            Pessoa pessoa = new Pessoa();
            pessoa.LoadByPrimaryKey(idCotista);

            bool copia = true;
            if (pessoa.Cpfcnpj != null && pessoa.Cpfcnpj != "")
            {
                List<int> listaCheck = new List<int>();

                PessoaCollection pessoaCollection = new PessoaCollection();
                pessoaCollection.Query.Select(pessoaCollection.Query.IdPessoa);
                pessoaCollection.Query.Where(pessoaCollection.Query.Cpfcnpj.Equal(Utilitario.RemoveCaracteresEspeciais(pessoa.Cpfcnpj)),
                                             pessoaCollection.Query.IdPessoa.NotEqual(idCotista));
                pessoaCollection.Query.Load();

                foreach (Pessoa pessoaCPF in pessoaCollection)
                {
                    listaCheck.Add(pessoaCPF.IdPessoa.Value);
                }

                if (listaCheck.Count > 0)
                {
                    PosicaoCotistaCollection collPos = new PosicaoCotistaCollection();
                    collPos.Query.Select(collPos.Query.IdPosicao);
                    collPos.Query.Where(collPos.Query.IdCotista.In(listaCheck),
                                        collPos.Query.IdCarteira.Equal(operacaoFundo.IdCarteira.Value));
                    collPos.Query.Load();

                    OperacaoCotistaCollection collOper = new OperacaoCotistaCollection();
                    collOper.Query.Select(collOper.Query.IdOperacao);
                    collOper.Query.Where(collOper.Query.IdCotista.In(listaCheck),
                                         collOper.Query.IdCarteira.Equal(operacaoFundo.IdCarteira.Value));
                    collOper.Query.Load();

                    copia = collPos.Count == 0 && collOper.Count == 0;
                }
            }

            if (copia)
            {
                OperacaoCotista operacaoCotista = new OperacaoCotista();
                operacaoCotista.DataAgendamento = Convert.ToDateTime(textDataOperacao.Text);
                operacaoCotista.DataConversao = Convert.ToDateTime(textDataConversao.Text);
                operacaoCotista.DataLiquidacao = Convert.ToDateTime(textDataLiquidacao.Text);
                operacaoCotista.DataOperacao = Convert.ToDateTime(textDataOperacao.Text);
                operacaoCotista.DataRegistro = Convert.ToDateTime(textDataRegistro.Text);
                operacaoCotista.DepositoRetirada = checkDepositoRetirada.Checked;
                operacaoCotista.TipoOperacao = Convert.ToByte(dropTipoOperacao.SelectedItem.Value);
                operacaoCotista.IdCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);
                operacaoCotista.IdCotista = idCotista;

                if (ParametrosConfiguracaoSistema.Outras.MultiConta && dropConta.Value != null && !string.IsNullOrEmpty(dropConta.Value.ToString()))
                {
                    operacaoCotista.IdConta = Convert.ToInt32(dropConta.Value);
                }

                operacaoCotista.IdFormaLiquidacao = Convert.ToByte(dropFormaLiquidacao.SelectedItem.Value);
                operacaoCotista.Observacao = textObservacao.Text.ToString();

                if (dropTipoResgate.SelectedIndex != -1)
                {
                    operacaoCotista.TipoResgate = Convert.ToByte(dropTipoResgate.SelectedItem.Value);
                }

                //Salva o valor dependendo do tipo de operação, o resto força zerado
                //Resgate total força tudo zerado!
                operacaoCotista.ValorBruto = 0;
                operacaoCotista.ValorLiquido = 0;
                operacaoCotista.Quantidade = 0;
                switch (operacaoFundo.TipoOperacao)
                {
                    case (byte)TipoOperacaoFundo.Aplicacao:
                    case (byte)TipoOperacaoFundo.AplicacaoAcoesEspecial:
                    case (byte)TipoOperacaoFundo.ResgateBruto:
                        operacaoCotista.ValorBruto = Convert.ToDecimal(textValor.Text);
                        break;
                    case (byte)TipoOperacaoFundo.AplicacaoCotasEspecial:
                    case (byte)TipoOperacaoFundo.ComeCotas:
                    case (byte)TipoOperacaoFundo.ResgateCotas:
                    case (byte)TipoOperacaoFundo.ResgateCotasEspecial:
                        operacaoCotista.Quantidade = Convert.ToDecimal(textValor.Text);
                        break;
                    case (byte)TipoOperacaoFundo.ResgateLiquido:
                        operacaoCotista.ValorLiquido = Convert.ToDecimal(textValor.Text);
                        break;
                }

                if (operacaoFundo.TipoOperacao.Value == (byte)TipoOperacaoFundo.Aplicacao ||
                    operacaoFundo.TipoOperacao.Value == (byte)TipoOperacaoFundo.AplicacaoCotasEspecial ||
                    operacaoFundo.TipoOperacao.Value == (byte)TipoOperacaoFundo.AplicacaoAcoesEspecial ||
                    operacaoFundo.TipoOperacao.Value == (byte)TipoOperacaoFundo.ComeCotas)
                {
                    operacaoCotista.TipoResgate = null;
                }
                else
                {
                    //Se não informar o tipo de resgate, joga default = FIFO
                    if (dropTipoResgate.SelectedIndex == -1)
                    {
                        operacaoCotista.TipoResgate = (byte)TipoResgateCotista.FIFO;
                    }
                }

                if (operacaoFundo.IdPosicaoResgatada.HasValue)
                {
                    PosicaoFundo posicaoFundo = new PosicaoFundo();
                    posicaoFundo.LoadByPrimaryKey(operacaoFundo.IdPosicaoResgatada.Value);

                    DateTime dataConversao = posicaoFundo.DataConversao.Value;

                    PosicaoCotistaCollection posicaoCotistaCollection = new PosicaoCotistaCollection();
                    posicaoCotistaCollection.Query.Where(posicaoCotistaCollection.Query.IdCarteira.Equal(Convert.ToInt32(btnEditCodigoCarteira.Text)),
                                                       posicaoCotistaCollection.Query.IdCotista.Equal(idCotista),
                                                       posicaoCotistaCollection.Query.DataConversao.Equal(dataConversao));
                    posicaoCotistaCollection.Query.Load();

                    if (posicaoCotistaCollection.Count > 0)
                    {
                        operacaoCotista.IdPosicaoResgatada = posicaoCotistaCollection[0].IdPosicao.Value;
                    }

                }

                operacaoCotista.Fonte = (byte)FonteOperacaoCotista.Manual;

                operacaoCotista.Save();
            }
        }
        #endregion

        //Atualiza StatusRealTime para executar***********
        cliente.SetaFlagRealTime(idCliente, (byte)StatusRealTimeCliente.Executar);
        //************************************************
    }

    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();
    }

    protected void gridCadastro_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (gridCadastro.IsEditing && !gridCadastro.IsNewRowEditing)
        {
            //Preenche o campo de valor, de acordo com o tipo de operação
            ASPxSpinEdit textValor = gridCadastro.FindEditFormTemplateControl("textValor") as ASPxSpinEdit;

            byte tipoOperacao = Convert.ToByte(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, new string[] { "TipoOperacao" }));
            if (!gridCadastro.IsNewRowEditing)
            {
                switch (tipoOperacao)
                {
                    case (byte)TipoOperacaoFundo.Aplicacao:
                    case (byte)TipoOperacaoFundo.AplicacaoAcoesEspecial:
                    case (byte)TipoOperacaoFundo.ResgateBruto:
                    case (byte)TipoOperacaoFundo.ResgateTotal:
                        decimal valorBruto = Convert.ToDecimal(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, new string[] { "ValorBruto" }));
                        textValor.Text = valorBruto.ToString();
                        break;
                    case (byte)TipoOperacaoFundo.AplicacaoCotasEspecial:
                    case (byte)TipoOperacaoFundo.ComeCotas:
                    case (byte)TipoOperacaoFundo.ResgateCotas:
                    case (byte)TipoOperacaoFundo.ResgateCotasEspecial:
                        decimal quantidade = Convert.ToDecimal(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, new string[] { "Quantidade" }));
                        textValor.Text = quantidade.ToString();
                        break;
                    case (byte)TipoOperacaoFundo.ResgateLiquido:
                        decimal valorLiquido = Convert.ToDecimal(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, new string[] { "ValorLiquido" }));
                        textValor.Text = valorLiquido.ToString();
                        break;
                }
            }

            #region Verifica se é Fundo de Previdencia
            ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
            ASPxComboBox dropFieModalidade = gridCadastro.FindEditFormTemplateControl("dropFieModalidade") as ASPxComboBox;
            ASPxComboBox dropFieTabelaIr = gridCadastro.FindEditFormTemplateControl("dropFieTabelaIr") as ASPxComboBox;

            int idCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            if (carteira.Fie.Equals("S") && (tipoOperacao != (byte)TipoOperacaoFundo.ResgateBruto &&
                                             tipoOperacao != (byte)TipoOperacaoFundo.ResgateCotas &&
                                             tipoOperacao != (byte)TipoOperacaoFundo.ResgateCotasEspecial &&
                                             tipoOperacao != (byte)TipoOperacaoFundo.ResgateLiquido &&
                                             tipoOperacao != (byte)TipoOperacaoFundo.ResgateTotal))
            {
                dropFieModalidade.ClientEnabled = true;
                dropFieTabelaIr.ClientEnabled = true;
            }
            else
            {
                dropFieModalidade.ClientEnabled = false;
                dropFieTabelaIr.ClientEnabled = false;
            }
            #endregion

        }

    }

    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdOperacao");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idOperacao = Convert.ToInt32(keyValuesId[i]);
                
                OperacaoFundo operacaoFundo = new OperacaoFundo();

                if (operacaoFundo.LoadByPrimaryKey(idOperacao)) {
                    int idCliente = operacaoFundo.IdCliente.Value;
                    DateTime dataAplicacao = operacaoFundo.DataOperacao.Value;

                    PosicaoFundoCollection posicaoFundoColl = new PosicaoFundoCollection();
                    posicaoFundoColl.Query.Where(posicaoFundoColl.Query.IdCliente.Equal(idCliente) & 
                                                 posicaoFundoColl.Query.IdOperacao.Equal(idOperacao));

                    if (posicaoFundoColl.Query.Load())
                    {
                        posicaoFundoColl.MarkAllAsDeleted();
                        posicaoFundoColl.Save();
                    }

                    PosicaoFundoHistoricoCollection posicaoFundoHistoricoColl = new PosicaoFundoHistoricoCollection();
                    posicaoFundoHistoricoColl.Query.Where(posicaoFundoHistoricoColl.Query.DataHistorico.GreaterThanOrEqual(dataAplicacao) &
                                                            posicaoFundoHistoricoColl.Query.IdCliente.Equal(idCliente) &
                                                            posicaoFundoHistoricoColl.Query.IdOperacao.Equal(idOperacao));

                    if (posicaoFundoHistoricoColl.Query.Load())
                    {
                        posicaoFundoHistoricoColl.MarkAllAsDeleted();
                        posicaoFundoHistoricoColl.Save();
                    }

                    PosicaoFundoAberturaCollection posicaoFundoAberturaColl = new PosicaoFundoAberturaCollection();
                    posicaoFundoAberturaColl.Query.Where(posicaoFundoAberturaColl.Query.DataHistorico.GreaterThanOrEqual(dataAplicacao) &
                                                           posicaoFundoAberturaColl.Query.IdCliente.Equal(idCliente) &
                                                           posicaoFundoAberturaColl.Query.IdOperacao.Equal(idOperacao));

                    if (posicaoFundoAberturaColl.Query.Load())
                    {                        
                        posicaoFundoAberturaColl.MarkAllAsDeleted();
                        posicaoFundoAberturaColl.Save();
                    }
                    
                    OperacaoFundo operacaoFundoClone = (OperacaoFundo)Utilitario.Clone(operacaoFundo);
                    operacaoFundo.MarkAsDeleted();
                    operacaoFundo.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de OperacaoFundo - Operacao: Delete OperacaoFundo: " + idOperacao + UtilitarioWeb.ToString(operacaoFundoClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion

                    //Atualiza StatusRealTime para executar***********
                    Cliente cliente = new Cliente();
                    cliente.SetaFlagRealTime(idCliente, (byte)StatusRealTimeCliente.Executar);
                    //************************************************
                }
            }
        }
        else if (e.Parameters == "btnObservacao")
        {
            int idOperacao = Convert.ToInt32(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, "IdOperacao"));
            TextBox textObservacao = gridCadastro.FindEditFormTemplateControl("textObservacao") as TextBox;

            OperacaoFundo operacaoFundoUpdate = new OperacaoFundo();
            if (operacaoFundoUpdate.LoadByPrimaryKey(idOperacao))
            {
                operacaoFundoUpdate.Observacao = textObservacao.Text;
                operacaoFundoUpdate.Save();

                #region Log do Processo
                HistoricoLog historicoLog = new HistoricoLog();
                historicoLog.InsereHistoricoLog(DateTime.Now,
                                                DateTime.Now,
                                                "Cadastro de OperacaoFundo - Operacao: Update OperacaoFundo: " + idOperacao + "Nova Observação: " + textObservacao.Text,
                                                HttpContext.Current.User.Identity.Name,
                                                UtilitarioWeb.GetIP(Request),
                                                "",
                                                HistoricoLogOrigem.Outros);
                #endregion
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e)
    {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing)
        {
            ASPxSpinEdit btnEditCodigoCliente = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;
            e.Properties["cpBtnEditCodigoCliente"] = btnEditCodigoCliente.ClientID;

            //Busca do web.config o flag de multi-conta
            //bool multiConta = Convert.ToBoolean(WebConfig.AppSettings.MultiConta);
            bool multiConta = ParametrosConfiguracaoSistema.Outras.MultiConta;
            //
            e.Properties["cpMultiConta"] = multiConta.ToString();
        }
    }

    new protected void gridCadastro_PreRender(object sender, EventArgs e)
    {
        this.FocaCampoGrid("btnEditCodigoCliente");
        base.gridCadastro_PreRender(sender, e);

        ASPxSpinEdit btnEditCodigoClienteFiltro = popupFiltro.FindControl("btnEditCodigoClienteFiltro") as ASPxSpinEdit;

        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        Label labelFiltroHeader = gridCadastro.FindStatusBarTemplateControl("labelFiltroHeader") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");
        //
        if (btnEditCodigoCarteiraFiltro != null && btnEditCodigoCarteiraFiltro.Text != "")
        {
            texto.Append(" Carteira: ").Append(btnEditCodigoCarteiraFiltro.Text);
        }
        if (btnEditCodigoClienteFiltro != null && btnEditCodigoClienteFiltro.Text != "")
        {
            if (texto.Length > 0)
            {
                texto.Append(" |");
            }
            texto.Append(" Cliente: ").Append(btnEditCodigoClienteFiltro.Text);
        }
        if (textDataInicio.Text != "")
        {
            if (texto.Length > 0)
            {
                texto.Append(" |");
            }
            texto.Append(" Data Início >= ").Append(textDataInicio.Text.Substring(0, 10));
        }
        if (textDataFim.Text != "")
        {
            if (texto.Length > 0)
            {
                texto.Append(" |");
            }
            texto.Append(" Data Fim <= ").Append(textDataFim.Text.Substring(0, 10));
        }

        labelFiltro.Text = texto.ToString();
    }

    protected void EsDSTrader_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        TraderCollection coll = new TraderCollection();
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dropConta_Callback(object source, CallbackEventArgsBase e)
    {
        if (!ParametrosConfiguracaoSistema.Outras.MultiConta)
            return;

        ASPxComboBox dropConta = source as ASPxComboBox;
        dropConta.DataBind();
    }

    protected void EsDSContaCorrente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        if (!ParametrosConfiguracaoSistema.Outras.MultiConta)
        {
            e.Collection = new ContaCorrenteCollection();
            return;
        }

        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCliente") as ASPxSpinEdit;

        if (btnEditCodigoCarteira != null && btnEditCodigoCarteira.Text != "")
        {
            int idCliente = Convert.ToInt32(btnEditCodigoCarteira.Text);

            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.IdPessoa);
            if (cliente.LoadByPrimaryKey(campos, idCliente))
            {
                ContaCorrenteCollection coll = new ContaCorrenteCollection();

                coll.Query.Where(coll.Query.IdPessoa.Equal(cliente.IdPessoa.Value) | coll.Query.IdCliente.Equal(idCliente));
                coll.Query.OrderBy(coll.Query.Numero.Ascending);
                coll.Query.Load();

                // Assign the esDataSourcSelectEvenArgs Collection property
                e.Collection = coll;
            }

        }
        else
        {
            // Assign the esDataSourcSelectEvenArgs Collection property
            e.Collection = new ContaCorrenteCollection();
        }
    }
}