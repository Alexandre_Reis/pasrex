﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="IncorporacaoFusaoCisaoFundo.aspx.cs"
    Inherits="CadastrosBasicos_IncorporacaoFusaoCisaoFundo" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxw" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxlp" %>
<%@ Register Assembly="DevExpress.Web.v15.2" Namespace="DevExpress.Web"
    TagPrefix="dxuc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
    var popup = true;
    document.onkeydown=onDocumentKeyDownWithCallback;
    var operacao = '';
    
    function OnGetDataCarteira(data) {
        debugger;
        hiddenIdCarteiraOrigem.SetValue(data);
        ASPxCallback1.SendCallback(data);
        ASPxCallback3.SendCallback(data);
        popupCarteira.HideWindow();
        btnEditCodigoCarteiraOrigem.Focus();
        cbPanel.PerformCallback(data);
    }
    
    function OnGetDataCarteira1(data) {
        hiddenIdCarteiraDestino.SetValue(data);
        ASPxCallback2.SendCallback(data);
        popupCarteira1.HideWindow();
        btnEditCodigoCarteiraDestino.Focus();    
    }
    function HideData() {
        labelDataRecolhimento.SetVisible(false);
        textDataRecolhimento.SetVisible(false);
    }
    function ShowData() {
        labelDataRecolhimento.SetVisible(true);
        textDataRecolhimento.SetVisible(true);
    }
    
    function DesabilitarControleRecolhimentoIR(){
        rblExecucaoRecolhimento.SetEnabled(false);
        rblAliquotaIR.SetEnabled(false);
        textDataRecolhimento.SetEnabled(false);
    }
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true"
            EnableScriptLocalization="true" />
        <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {if (e.result != null) btnEditCodigoCarteiraOrigem.SetValue(e.result); } " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="ASPxCallback2" runat="server" OnCallback="ASPxCallback2_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {if (e.result != null) btnEditCodigoCarteiraDestino.SetValue(e.result); } " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="ASPxCallback3" runat="server" OnCallback="ASPxCallback3_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {
                var resultDataSplit = e.result.split('|');
                textData.SetValue(new Date(resultDataSplit[0], resultDataSplit[1], resultDataSplit[2])); 
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackStatus" runat="server" OnCallback="callbackStatus_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            if (e.result != '')
            {                   
                alert(e.result);
            }
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '') {                   
                alert(e.result);                              
            }
            else {
                gridCadastro.UpdateEdit();                
            }
            operacao = '';
        }        
        " />
        </dxcb:ASPxCallback>
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Incorporação/Fusão/Cisão de Fundo"></asp:Label>
                            </div>
                            <div id="mainContent">
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" CssClass="btnAdd"
                                        OnClientClick="gridCadastro.AddNewRow(); return false;">
                                        <asp:Literal ID="Literal1" runat="server" Text="Novo" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" CssClass="btnDelete"
                                        OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;">
                                        <asp:Literal ID="Literal2" runat="server" Text="Excluir" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf"
                                        OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal4" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel"
                                        OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal5" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh"
                                        OnClientClick="gridCadastro.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal6" runat="server" Text="Atualizar" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnCheck" runat="server" Font-Overline="false" CssClass="btnTick"
                                        OnClientClick="callbackStatus.SendCallback();gridCadastro.PerformCallback('btnRefresh');return false;">
                                        <asp:Literal ID="Literal8" runat="server" Text="Ativar/Desativar" /><div>
                                        </div>
                                    </asp:LinkButton>
                                </div>
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridCadastro" runat="server" KeyFieldName="IdEventoFundo"
                                        DataSourceID="EsDSIncorporacaoFusaoCisaoFundo" OnCustomCallback="gridCadastro_CustomCallback"
                                        OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData" OnRowInserting="gridCadastro_RowInserting"
                                        OnBeforeGetCallbackResult="gridCadastro_PreRender" AutoGenerateColumns="False">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="7%" ButtonType="Image" ShowClearFilterButton="True">
                                                <HeaderTemplate>
                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                                </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataColumn FieldName="CarteiraOrigem" Caption="Carteira Origem" VisibleIndex="1"
                                                UnboundType="String" Width="30%" />
                                            <dxwgv:GridViewDataColumn FieldName="CarteiraDestino" Caption="Carteira Destino"
                                                VisibleIndex="2" UnboundType="String" Width="30%" />
                                                
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="TipoEvento" Caption="Tipo" VisibleIndex="3"
                                                Width="15%">
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="1" Text="Incorporação" />
                                                        <dxe:ListEditItem Value="2" Text="Fusão" />
                                                        <dxe:ListEditItem Value="3" Text="Cisão" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            
                                            <dxwgv:GridViewDataTextColumn FieldName="Percentual" Caption="Percentual" VisibleIndex="4"
                                                UnboundType="String" Width="10%">
                                                <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataDateColumn FieldName="DataPosicao" Caption="Data Posicao" VisibleIndex="5" Width="10%"/>
                                            
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="ExecucaoRecolhimento" Caption="IR" VisibleIndex="6"
                                                Width="15%">
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="1" Text="Não Recolher" />
                                                        <dxe:ListEditItem Value="2" Text="Recolher no evento" />
                                                        <dxe:ListEditItem Value="3" Text="Agendar o Recolhimento" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="AliquotaIR" Caption="Alíquota" VisibleIndex="7"
                                                Width="15%">
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="1" Text="Come-Cota" />
                                                        <dxe:ListEditItem Value="2" Text="Cautela" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            
                                            <dxwgv:GridViewDataDateColumn FieldName="DataRecolhimento" Caption="Data Recolhimento" VisibleIndex="8" Width="10%"/>
                                                                                        
                                            <dxwgv:GridViewDataTextColumn FieldName="Status" Caption="Status" VisibleIndex="8"
                                                UnboundType="String" Width="5%">
                                                <EditFormSettings Visible="False" />
                                            </dxwgv:GridViewDataTextColumn>
                                        </Columns>
                                        <Templates>
                                            <EditForm>
                                                <div class="editForm">
                                                    <dxe:ASPxTextBox ID="hiddenIdCarteiraOrigem" runat="server" CssClass="hiddenField"
                                                        Text='<%#Eval("IdCarteiraOrigem")%>' ClientInstanceName="hiddenIdCarteiraOrigem" />
                                                    <dxe:ASPxTextBox ID="hiddenIdCarteiraDestino" runat="server" CssClass="hiddenField"
                                                        Text='<%#Eval("IdCarteiraDestino")%>' ClientInstanceName="hiddenIdCarteiraDestino" />
                                                    <table>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelCarteiraOrigem" runat="server" CssClass="labelRequired" Text="Carteira Origem:" />
                                                            </td>
                                                            <td colspan="2">
                                                                <dxe:ASPxButtonEdit ID="btnEditCodigoCarteiraOrigem" runat="server" CssClass="textButtonEdit"
                                                                    EnableClientSideAPI="True" ClientInstanceName="btnEditCodigoCarteiraOrigem" ReadOnly="true"
                                                                    Width="380px" Text='<%#Eval("CarteiraOrigem")%>' OnLoad="btnEditCarteira_Load">
                                                                    <Buttons>
                                                                        <dxe:EditButton />
                                                                    </Buttons>
                                                                    <ClientSideEvents ButtonClick="function(s, e) {popupCarteira.ShowAtElementByID(s.name); DesabilitarControleRecolhimentoIR();}" />
                                                                </dxe:ASPxButtonEdit>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelCarteiraDestino" runat="server" CssClass="labelRequired" Text="Carteira Destino:" />
                                                            </td>
                                                            <td colspan="2">
                                                                <dxe:ASPxButtonEdit ID="btnEditCodigoCarteiraDestino" runat="server" CssClass="textButtonEdit"
                                                                    EnableClientSideAPI="True" ClientInstanceName="btnEditCodigoCarteiraDestino"
                                                                    ReadOnly="true" Width="380px" Text='<%#Eval("CarteiraDestino")%>' OnLoad="btnEditCarteira_Load">
                                                                    <Buttons>
                                                                        <dxe:EditButton />
                                                                    </Buttons>
                                                                    <ClientSideEvents ButtonClick="function(s, e) {popupCarteira1.ShowAtElementByID(s.name);}" />
                                                                </dxe:ASPxButtonEdit>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelEvento" runat="server" CssClass="labelRequired" Text="Evento:" />
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxComboBox ID="dropEvento" runat="server" ClientInstanceName="dropEvento"
                                                                    ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto"
                                                                    Text='<%#Eval("TipoEvento")%>'>
                                                                    <Items>
                                                                        <dxe:ListEditItem Value="1" Text="Incorporação" />
                                                                        <dxe:ListEditItem Value="2" Text="Fusão" />
                                                                        <dxe:ListEditItem Value="3" Text="Cisão" />
                                                                    </Items>
                                                                </dxe:ASPxComboBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label">
                                                                <asp:Label ID="labelTaxa" runat="server" AssociatedControlID="labelTaxa" CssClass="labelNormal"
                                                                    Text="Percentual:" />
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxSpinEdit ID="textPercentual" runat="server" ClientInstanceName="textTaxa"
                                                                    CssClass="textValor_6" NumberType="Float" MaxLength="5" DecimalPlaces="2" Text='<%#Eval("Percentual")%>'>
                                                                </dxe:ASPxSpinEdit>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td_Label_Curto">
                                                                <asp:Label ID="labelData" runat="server" CssClass="labelRequired" Text="Data Posição:" />
                                                            </td>
                                                            <td>
                                                                <dxe:ASPxDateEdit ID="textData" runat="server" ClientInstanceName="textData" Value='<%#Eval("DataPosicao")%>' />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <dxcp:ASPxCallbackPanel runat="server" ClientInstanceName="cbPanel" ID="cbPanel"
                                                        OnCallback="cbPanel_Callback">
                                                        <PanelCollection>
                                                            <dxp:PanelContent runat="server">
                                                                <fieldset>
                                                                    <legend>Padrão de Recolhimento de IR(Come-Cotas) para Eventos Corporativos, Transformações
                                                                        e Desenquadramentos de Fundos</legend>
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <fieldset>
                                                                                    <legend style="text-align: left">Execução do Recolhimento</legend>
                                                                                    <dxe:ASPxRadioButtonList ID="rblExecucaoRecolhimento" runat="server" ClientInstanceName="rblExecucaoRecolhimento"
                                                                                        ValueType="System.Int32" Value='<%#Eval("ExecucaoRecolhimento")%>' ClientEnabled="False">
                                                                                        <Items>
                                                                                            <dxe:ListEditItem Value="1" Text="Não Recolher" />
                                                                                            <dxe:ListEditItem Value="2" Text="Recolher no momento do evento/transformação/desenquadramento" />
                                                                                            <dxe:ListEditItem Value="3" Text="Agendar o Recolhimento para uma data pré-definida" />
                                                                                        </Items>
                                                                                        <ClientSideEvents Init="function(s, e) 
                                                                                        {
                                                                                            if(rblExecucaoRecolhimento.GetSelectedItem() != null){
                                                                                                if(rblExecucaoRecolhimento.GetSelectedItem().value != '3')
                                                                                                { 
                                                                                                    HideData();
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    ShowData();
                                                                                                }
                                                                                            }
                                                                                            else
                                                                                             {
                                                                                                HideData();
                                                                                            }
                                                                                        }" />
                                                                                        <ClientSideEvents SelectedIndexChanged="function(s, e) 
                                                                                        {
                                                                                            if(rblExecucaoRecolhimento.GetSelectedItem().value == '3')
                                                                                            {
                                                                                                ShowData();
                                                                                            } 
                                                                                            else 
                                                                                            {
                                                                                                HideData();
                                                                                                textDataRecolhimento.SetDate(null);
                                                                                            }
                                                                                        }" />
                                                                                    </dxe:ASPxRadioButtonList>
                                                                                </fieldset>
                                                                            </td>
                                                                            <td>
                                                                                <fieldset>
                                                                                    <legend style="text-align: left">Alíquota IR</legend>
                                                                                    <dxe:ASPxRadioButtonList ID="rblAliquotaIR" ClientInstanceName="rblAliquotaIR" runat="server"
                                                                                    ValueType="System.Int32" Value='<%#Eval("AliquotaIR")%>' ClientEnabled="False">
                                                                                        <Items>
                                                                                            <dxe:ListEditItem Value="1" Text="Utilizar alíquota IR padrão do Come-Cota" />
                                                                                            <dxe:ListEditItem Value="2" Text="Utilizar alíquota IR vigente de cada cautela" />
                                                                                        </Items>
                                                                                    </dxe:ASPxRadioButtonList>
                                                                                </fieldset>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <dxe:ASPxLabel ID="labelDataRecolhimento" runat="server" ClientInstanceName="labelDataRecolhimento"
                                                                                    CssClass="labelNormal" Text="Data Recolhimento:" />
                                                                                <dxe:ASPxDateEdit ID="textDataRecolhimento" runat="server" ClientInstanceName="textDataRecolhimento"
                                                                                    Value='<%#Eval("DataRecolhimento")%>' ClientEnabled="False">
                                                                                </dxe:ASPxDateEdit>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </fieldset>
                                                            </dxp:PanelContent>
                                                        </PanelCollection>
                                                        <ClientSideEvents EndCallback="function(s, e) {DesabilitarControleRecolhimentoIR();}" />
                                                    </dxcp:ASPxCallbackPanel>
                                                    <div class="linhaH">
                                                    </div>
                                                    <div class="linkButton linkButtonNoBorder popupFooter">
                                                        <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                            OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                                                            <asp:Literal ID="Literal12" runat="server" Text="OK" /><div>
                                                            </div>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel"
                                                            OnClientClick="gridCadastro.CancelEdit(); return false;">
                                                            <asp:Literal ID="Literal13" runat="server" Text="Cancelar" /><div>
                                                            </div>
                                                        </asp:LinkButton>
                                                    </div>
                                                </div>                                                
                                            </EditForm>
                                            <StatusBar>
                                                <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" />
                                            </StatusBar>
                                        </Templates>
                                        <SettingsPopup EditForm-Width="800px" />
                                        <SettingsCommandButton>
                                            <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                                        </SettingsCommandButton>
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
        <cc1:esDataSource ID="EsDSIncorporacaoFusaoCisaoFundo" runat="server" OnesSelect="EsDSIncorporacaoFundo_esSelect" />
        <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" />
        <cc1:esDataSource ID="EsDSCarteira1" runat="server" OnesSelect="EsDSCarteira_esSelect" />
    </form>
</body>
</html>
