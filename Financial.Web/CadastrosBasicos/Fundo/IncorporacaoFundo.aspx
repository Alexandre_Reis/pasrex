﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="IncorporacaoFundo.aspx.cs" Inherits="CadastrosBasicos_IncorporacaoFundo" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">

    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />    
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    var popup = true;
    document.onkeydown=onDocumentKeyDownWithCallback;
    var operacao = '';
    
    function OnGetDataCarteira(data) {
        btnEditCodigoCarteiraOrigem.SetValue(data);        
        ASPxCallback1.SendCallback(btnEditCodigoCarteiraOrigem.GetValue());
        popupCarteira.HideWindow();
        btnEditCodigoCarteiraOrigem.Focus();    
    }
    
    function OnGetDataCarteira1(data) {
        btnEditCodigoCarteiraDestino.SetValue(data);        
        ASPxCallback2.SendCallback(btnEditCodigoCarteiraDestino.GetValue());
        popupCarteira1.HideWindow();
        btnEditCodigoCarteiraDestino.Focus();    
    }
    </script>
</head>

<body>
    <form id="form1" runat="server">
    
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"/>
    
    <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {    
            var textNome = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNome');
            OnCallBackCompleteCliente(s, e, popupMensagemCarteira, btnEditCodigoCarteiraOrigem, textNome);
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="ASPxCallback2" runat="server" OnCallback="ASPxCallback2_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {
            var textNome = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNome1');
            OnCallBackCompleteCliente(s, e, popupMensagemCarteira1, btnEditCodigoCarteiraDestino, textNome);
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '') {                   
                alert(e.result);                              
            }
            else {
                gridCadastro.UpdateEdit();                
            }
            operacao = '';
        }        
        "/>
    </dxcb:ASPxCallback>
        
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">

    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Cadastro de Incorporação de Fundo"></asp:Label>
    </div>
           
    <div id="mainContent">

        <dxpc:ASPxPopupControl ID="popupFiltro" AllowDragging="true" PopupElementID="popupFiltro" EnableClientSideAPI="True"                    
                        PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight" 
                        Width="400" Left="250" Top="70" HeaderText="Filtros adicionais para consulta" runat="server"
                        HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">            
            <ContentCollection><dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                
                <table>
                <tr>
                <td>
                    <asp:Label ID="labelDataInicio" runat="server" CssClass="labelNormal" Text="Início:"/>
                </td>
                                    
                <td>
                    <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" />
                </td>  
                
                <td>                
                    <asp:Label ID="labelDataFim" runat="server" CssClass="labelNormal" Text="Fim:" />
                </td>                                        
                <td>
                    <dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim" />
                </td>                                                                            
                </tr>
                </table>        
                
                <div class="linkButton linkButtonNoBorder" style="margin-top:20px">              
                <asp:LinkButton ID="btnOKFilter" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnOK" OnClientClick="popupFiltro.Hide(); gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal7" runat="server" Text="Aplicar"/><div></div></asp:LinkButton>
                <asp:LinkButton ID="btnFilterCancel" runat="server" Font-Overline="false" ForeColor="Black" CssClass="btnFilterCancel" OnClientClick="OnClearFilterClick_FiltroDatas(); return false;"><asp:Literal ID="Literal8" runat="server" Text="Limpar"/><div></div></asp:LinkButton>
                </div>                    
            </dxpc:PopupControlContentControl></ContentCollection>                             
        </dxpc:ASPxPopupControl>

            <div class="linkButton" >               
               <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal1" runat="server" Text="Novo"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;"><asp:Literal ID="Literal2" runat="server" Text="Excluir"/><div></div></asp:LinkButton>           
               <asp:LinkButton ID="btnFilter" runat="server" Font-Overline="false" CssClass="btnFilter" OnClientClick="return OnButtonClick_FiltroDatas()"><asp:Literal ID="Literal3" runat="server" Text="Filtro"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal4" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal5" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal6" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
            </div>
        
            <div class="divDataGrid">            
                <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                    KeyFieldName="CompositeKey" DataSourceID="EsDSIncorporacaoFundo"
                    OnCustomCallback="gridCadastro_CustomCallback"
                    OnRowInserting="gridCadastro_RowInserting"
                    OnCustomUnboundColumnData="gridCadastro_CustomUnboundColumnData"
                    OnPreRender="gridCadastro_PreRender"
                    OnBeforeGetCallbackResult="gridCadastro_PreRender"
                    >
                                                                                                                                                           
                <Columns>           
                    <dxwgv:GridViewCommandColumn VisibleIndex="0" Width="5%" ShowClearFilterButton="True">
                        <HeaderTemplate>
                            <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                        </HeaderTemplate>
                    </dxwgv:GridViewCommandColumn>
                    
                    <dxwgv:GridViewDataDateColumn FieldName="Data" VisibleIndex="1" Width="10%"/>
                    <dxwgv:GridViewDataColumn FieldName="ApelidoOrigem" Caption="Carteira Origem" VisibleIndex="2" Width="30%" CellStyle-HorizontalAlign="left" Settings-AutoFilterCondition="Contains" />
                    <dxwgv:GridViewDataColumn FieldName="ApelidoDestino" Caption="Carteira Destino" VisibleIndex="3" Width="30%" CellStyle-HorizontalAlign="left" Settings-AutoFilterCondition="Contains" />
                    <dxwgv:GridViewDataTextColumn FieldName="Taxa" VisibleIndex="7" Width="20%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                        <PropertiesTextEdit DisplayFormatString="{0:#,##0.000000000000000000;(#,##0.000000000000000000);0.000000000000000000}"></PropertiesTextEdit>
                    </dxwgv:GridViewDataTextColumn>
                                        
                    <dxwgv:GridViewDataColumn FieldName="IdCarteiraOrigem" Visible="false" />
                    <dxwgv:GridViewDataColumn FieldName="IdCarteiraDestino" Visible="false"  />
                    <dxwgv:GridViewDataColumn FieldName="Data" Visible="false" />
                    
                    <dxwgv:GridViewDataTextColumn FieldName="CompositeKey" UnboundType="String" Visible="False"/>
                </Columns>
                
                <Templates>
                <EditForm>                    
                    <div class="editForm">    
                            
                        <table border="0">
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelCarteira" runat="server" CssClass="labelRequired" Text="Carteira Origem:"/>
                                </td>
                                <td>
                                    <dxe:ASPxSpinEdit ID="btnEditCodigoCarteiraOrigem" runat="server" CssClass="textButtonEdit" 
                                            ClientInstanceName="btnEditCodigoCarteiraOrigem" MaxLength="10" NumberType="Integer">
                                    <Buttons>                                           
                                        <dxe:EditButton>
                                        </dxe:EditButton>                                
                                    </Buttons>         
                                    <ClientSideEvents                                                           
                                             KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNome').value = '';} " 
                                             ButtonClick="function(s, e) {popupCarteira.ShowAtElementByID(s.name);}" 
                                             LostFocus="function(s, e) {popupMensagemCarteira.HideWindow();                                                                                        
                                                                        ASPxCallback1.SendCallback(btnEditCodigoCarteiraOrigem.GetValue());
                                                                      }"
                                            />               
                                    </dxe:ASPxSpinEdit>
                                </td>                                        
                                
                                <td colspan="2">
                                    <asp:TextBox ID="textNome" runat="server" CssClass="textLongo5" Enabled="False"/>   
                                </td>
                            </tr>

                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="label1" runat="server" CssClass="labelRequired" Text="Carteira Destino:"/>
                                </td>
                                <td>
                                    <dxe:ASPxSpinEdit ID="btnEditCodigoCarteiraDestino" runat="server" CssClass="textButtonEdit" 
                                            ClientInstanceName="btnEditCodigoCarteiraDestino" MaxLength="10" NumberType="Integer">   
                                    <Buttons>                                           
                                        <dxe:EditButton>
                                        </dxe:EditButton>                                
                                    </Buttons>         
                                    <ClientSideEvents                                                           
                                             KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNome1').value = '';} " 
                                             ButtonClick="function(s, e) {popupCarteira1.ShowAtElementByID(s.name);}" 
                                             LostFocus="function(s, e) {popupMensagemCarteira1.HideWindow();                                                                                        
                                                                        ASPxCallback2.SendCallback(btnEditCodigoCarteiraDestino.GetValue());
                                                                         }"
                                            />               
                                    </dxe:ASPxSpinEdit>
                                </td>                                        
                                
                                <td colspan="2">
                                    <asp:TextBox ID="textNome1" runat="server" CssClass="textLongo5" Enabled="False"/>   
                                </td>
                            </tr>
                            
                            <tr>                                        
                                <td class="td_Label_Curto">
                                    <asp:Label ID="labelData" runat="server" CssClass="labelRequired" Text="Data:"/>
                                </td>
                                                        
                                <td>     
                                    <dxe:ASPxDateEdit ID="textData" runat="server" ClientInstanceName="textData" />                                                           
                                </td>  
                            </tr>
                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelTaxa" runat="server" AssociatedControlID="labelTaxa" CssClass="labelNormal" Text="Percentual:" />
                                </td>                                
                                <td>                            
                                    <dxe:ASPxSpinEdit ID="textTaxa" runat="server" ClientInstanceName="textTaxa" CssClass="textValor_6" 
                                    Text='<%# Eval("Taxa") %>' NumberType="Float" MaxLength="25" DecimalPlaces="18">
                                    </dxe:ASPxSpinEdit>                                
                                </td>
                            </tr>
                            
                        </table>
                        
                        <div class="linhaH"></div>    
                                                
                        <div class="linkButton linkButtonNoBorder popupFooter">
                        
                        <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                            OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                            <asp:Literal ID="Literal12" runat="server" Text="OK" /><div>
                            </div>
                        </asp:LinkButton>
                        
                        <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel"
                            OnClientClick="gridCadastro.CancelEdit(); return false;">
                            <asp:Literal ID="Literal13" runat="server" Text="Cancelar" /><div>
                            </div>
                        </asp:LinkButton>
                    </div>
                        
                    </div>                                              
                </EditForm>
                
                 <StatusBar>
                        <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" />
                    </StatusBar>    
                    
                </Templates>
                
                <SettingsPopup EditForm-Width="500px" />
                <SettingsCommandButton>
                    <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                </SettingsCommandButton>
                                                
            </dxwgv:ASPxGridView>            
            </div>
    </div>
    </div>
    </td></tr></table>
    </div>        
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
        
    <cc1:esDataSource ID="EsDSIncorporacaoFundo" runat="server" OnesSelect="EsDSIncorporacaoFundo_esSelect" LowLevelBind="true" />
    <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" />
    <cc1:esDataSource ID="EsDSCarteira1" runat="server" OnesSelect="EsDSCarteira_esSelect" />
            
    </form>
</body>
</html>