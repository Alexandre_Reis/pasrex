﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AgendaFundo.aspx.cs" Inherits="CadastrosBasicos_AgendaFundo" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
    var popup = true;
    document.onkeydown=onDocumentKeyDownWithCallback;
    var operacao = '';
    
    function OnGetDataCarteira(data) 
    {
        btnEditCodigoCarteira.SetValue(data);        
        ASPxCallback1.SendCallback(btnEditCodigoCarteira.GetValue());
        popupCarteira.HideWindow();
        btnEditCodigoCarteira.Focus();    
    }    
    
    function SetaTipoValorDefault(tipoEvento)
    {
        /*Tipo Evento
            <dxe:ListEditItem Value="1" Text="Juros" />
            <dxe:ListEditItem Value="2" Text="Amortização" />
            <dxe:ListEditItem Value="3" Text="Amort.+ Juros" />
            <dxe:ListEditItem Value="4" Text="Dividendo" />
            <dxe:ListEditItem Value="20" Text="ComeCotas" />
        */
        
        /* Tipo Valor        
            <dxe:ListEditItem Value="1" Text="Valor por Cota" />
            <dxe:ListEditItem Value="2" Text="Taxa Cota-Ex" />
            <dxe:ListEditItem Value="3" Text="Taxa Fixa" />
        */
        
        if(tipoEvento == 3)
        {
            dropTipoValorInput.SetEnabled(false);
            dropTipoValorInput.SetValue(3);
        }
        else
        {
            dropTipoValorInput.SetEnabled(true);
        }
    
    }
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true"
            EnableScriptLocalization="true" />
        <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {  
                var resultSplit = e.result.split('|');
                e.result = resultSplit[0];
                
                var textNome = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNome');                
                OnCallBackCompleteCliente(s, e, popupMensagemCarteira, btnEditCodigoCarteira, textNome);  
                
                var habilitaImpactaCota = resultSplit[1] == '1'                
                dropImpactarCota.SetEnabled(habilitaImpactaCota);  
                dropImpactarCota.SetValue('S');                                          
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                                              
            }
            else
            {
                if (operacao == 'deletar')
                {
                    gridCadastro.PerformCallback('btnDelete');                    
                }
                else
                {   
                    if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new')
                    {             
                        gridCadastro.UpdateEdit();
                    }
                    else
                    {
                        callbackAdd.SendCallback();
                    }    
                }
            }
            
            operacao = '';
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {
            alert('Operação feita com sucesso.');
        }        
        " />
        </dxcb:ASPxCallback>
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Agenda de Amortizações/Juros em Fundos"></asp:Label>
                            </div>
                            <div id="mainContent">
                                <dxpc:ASPxPopupControl ID="popupFiltro" AllowDragging="true" PopupElementID="popupFiltro"
                                    EnableClientSideAPI="True" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
                                    Width="440" Left="250" Top="70" HeaderText="Filtros adicionais para consulta"
                                    runat="server" HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
                                    <ContentCollection>
                                        <dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                                            <table cellpadding="2" cellspacing="2">
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="labelDataInicio" runat="server" CssClass="labelNormal" Text="Início:"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="labelDataFim" runat="server" CssClass="labelNormal" Text="Fim:"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim" />
                                                    </td>
                                                </tr>
                                            </table>
                                            <div class="linkButton linkButtonNoBorder" style="margin-top: 20px">
                                                <asp:LinkButton ID="btnOKFilter" runat="server" Font-Overline="false" ForeColor="Black"
                                                    CssClass="btnOK" OnClientClick="popupFiltro.Hide(); gridCadastro.PerformCallback('btnRefresh'); return false;">
                                                    <asp:Literal ID="Literal7" runat="server" Text="Aplicar" /><div>
                                                    </div>
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="btnFilterCancel" runat="server" Font-Overline="false" ForeColor="Black"
                                                    CssClass="btnFilterCancel" OnClientClick="OnClearFilterClick_FiltroDatas(); return false;">
                                                    <asp:Literal ID="Literal8" runat="server" Text="Limpar" /><div>
                                                    </div>
                                                </asp:LinkButton>
                                            </div>
                                        </dxpc:PopupControlContentControl>
                                    </ContentCollection>
                                </dxpc:ASPxPopupControl>
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" CssClass="btnAdd"
                                        OnClientClick="gridCadastro.AddNewRow(); return false;">
                                        <asp:Literal ID="Literal1" runat="server" Text="Novo" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" CssClass="btnDelete"
                                        OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) {operacao='deletar'; callbackErro.SendCallback();} return false;">
                                        <asp:Literal ID="Literal2" runat="server" Text="Excluir" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnFilter" runat="server" Font-Overline="false" CssClass="btnFilter"
                                        OnClientClick="return OnButtonClick_FiltroDatas()">
                                        <asp:Literal ID="Literal3" runat="server" Text="Filtro" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf"
                                        OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal4" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel"
                                        OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal5" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh"
                                        OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal6" runat="server" Text="Atualizar" /><div>
                                        </div>
                                    </asp:LinkButton>
                                </div>
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true" KeyFieldName="IdAgenda"
                                        DataSourceID="EsDSAgendaFundo" OnRowUpdating="gridCadastro_RowUpdating" OnRowInserting="gridCadastro_RowInserting"
                                        OnStartRowEditing="gridCadastro_StartRowEditing" OnCustomCallback="gridCadastro_CustomCallback" OnCustomJSProperties="gridCadastro_CustomJSProperties"
                                        OnBeforeGetCallbackResult="gridCadastro_PreRender" OnPreRender="gridCadastro_PreRender">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                                                <HeaderTemplate>
                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                                </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataColumn FieldName="IdCarteira" VisibleIndex="1" Width="10%" CellStyle-HorizontalAlign="left" />
                                            <dxwgv:GridViewDataTextColumn FieldName="Apelido" Caption="Carteira" Width="45%"
                                                VisibleIndex="2">
                                                <PropertiesTextEdit MaxLength="100">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="TipoEvento" Caption="Tipo" VisibleIndex="2"
                                                Width="10%">
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="1" Text="Juros" />
                                                        <dxe:ListEditItem Value="2" Text="Amortização" />
                                                        <dxe:ListEditItem Value="3" Text="Amort.+ Juros" />
                                                        <dxe:ListEditItem Value="4" Text="Dividendo" />
                                                        <dxe:ListEditItem Value="20" Text="ComeCotas" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataDateColumn FieldName="DataEvento" VisibleIndex="3" Width="8%" />
                                            <dxwgv:GridViewDataTextColumn FieldName="Taxa" Caption="Taxa" VisibleIndex="14" Width="10%"
                                                HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                                                <PropertiesTextEdit DisplayFormatString="{0:#,##0.0000000000;(#,##0.0000000000);0.0000000000}">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="Valor" Caption="Valor Por Cota" VisibleIndex="4"
                                                Width="10%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                                                <PropertiesTextEdit DisplayFormatString="{0:#,##0.0000000000;(#,##0.0000000000);0.0000000000}">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="ImpactarCota" Caption="Impactar Cota" VisibleIndex="5" Width="5%">
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="S" Text="Sim" />
                                                        <dxe:ListEditItem Value="N" Text="Não" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataColumn FieldName="IdAgenda" Visible="false">
                                            </dxwgv:GridViewDataColumn>
                                        </Columns>
                                        <Templates>
                                            <EditForm>
                                                <asp:Label ID="labelEdicao" runat="server" CssClass="labelInformation" Text=""></asp:Label>
                                                <asp:Panel ID="panelEdicao" runat="server">
                                                    <div class="editForm">
                                                        <table border="0">
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelCarteira" runat="server" CssClass="labelRequired" Text="Carteira:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit ID="btnEditCodigoCarteira" runat="server" CssClass="textButtonEdit"
                                                                        ClientInstanceName="btnEditCodigoCarteira" Text='<%# Eval("IdCarteira") %>' OnLoad="btnEditCodigo_Load"
                                                                        MaxLength="10" NumberType="Integer">
                                                                        <Buttons>
                                                                            <dxe:EditButton />
                                                                        </Buttons>
                                                                        <ClientSideEvents KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNome').value = '';} "
                                                                            ButtonClick="function(s, e) {popupCarteira.ShowAtElementByID(s.name);}" LostFocus="function(s, e) {popupMensagemCarteira.HideWindow();                                                                                        
                                                                            ASPxCallback1.SendCallback(btnEditCodigoCarteira.GetValue());
                                                                             }" />
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                <td colspan="3">
                                                                    <asp:TextBox ID="textNome" runat="server" CssClass="textLongo5" Enabled="False" Text='<%# Eval("Apelido") %>'></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelTipoOperacao" runat="server" CssClass="labelRequired" Text="Tipo:"></asp:Label>
                                                                </td>
                                                                <td colspan="2">
                                                                    <dxe:ASPxComboBox ID="dropTipoEvento" runat="server" ShowShadow="false" DropDownStyle="DropDownList"
                                                                        CssClass="dropDownListCurto_2" Text='<%#Eval("TipoEvento")%>'>
                                                                        <Items>
                                                                            <dxe:ListEditItem Value="1" Text="Juros" />
                                                                            <dxe:ListEditItem Value="2" Text="Amortização" />
                                                                            <dxe:ListEditItem Value="3" Text="Amort.+ Juros" />
                                                                            <dxe:ListEditItem Value="4" Text="Dividendo" />
                                                                            <dxe:ListEditItem Value="20" Text="ComeCotas" />
                                                                        </Items>
                                                                        <ClientSideEvents SelectedIndexChanged="function(s, e){ SetaTipoValorDefault(s.GetSelectedItem().value);}" />
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelDataEvento" runat="server" CssClass="labelRequired" Text="Data:" />
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxDateEdit ID="textDataEvento" runat="server" ClientInstanceName="textDataEvento"
                                                                        Value='<%#Eval("DataEvento")%>' />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="label3" runat="server" CssClass="labelRequired" Text="Lançar por:" />
                                                                </td>
                                                                <td colspan="2">
                                                                    <dxe:ASPxComboBox ID="dropTipoValorInput" ClientInstanceName="dropTipoValorInput" runat="server"
                                                                     OnInit="dropTipoValorInput_Init" ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto_2" 
                                                                     Value='<%#Eval("TipoValorInput")%>' ValueType="System.Int32">
                                                                        <Items>
                                                                            <dxe:ListEditItem Value="1" Text="Valor por Cota" />
                                                                            <dxe:ListEditItem Value="2" Text="Taxa Cota-Ex" />
                                                                            <dxe:ListEditItem Value="3" Text="Taxa Fixa" />
                                                                        </Items>
                                                                    </dxe:ASPxComboBox>
                                                                </td>                                                                
                                                                <td class="td_Label">
                                                                    <asp:Label ID="label1" runat="server" CssClass="labelRequired" Text="Impactar Cota:" />
                                                                </td>
                                                                <td >
                                                                    <dxe:ASPxComboBox ID="dropImpactarCota" ClientInstanceName="dropImpactarCota" runat="server"
                                                                     ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto_2"
                                                                     Text='<%#Eval("ImpactarCota")%>' OnInit="dropImpactarCota_Init">
                                                                        <Items>
                                                                            <dxe:ListEditItem Value="S" Text="Sim" />
                                                                            <dxe:ListEditItem Value="N" Text="Não" />
                                                                        </Items>
                                                                    </dxe:ASPxComboBox>
                                                                </td>                                                                
                                                            </tr>                                                            
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="label4" runat="server" CssClass="labelRequired" Text="Taxa/Valor:" />
                                                                </td>
                                                                <td colspan="3">
                                                                    <dxe:ASPxSpinEdit ID="textValorInput" runat="server" CssClass="textValor" ClientInstanceName="textValorInput"
                                                                        OnInit="textValorInput_Init" MaxLength="30" NumberType="Float" DecimalPlaces="20" />
                                                                </td>
                                                            </tr>
                                                            
                                                           
                                                        </table>
                                                        <div class="linhaH">
                                                        </div>
                                                        <div class="linkButton linkButtonNoBorder popupFooter">
                                                            <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd"
                                                                OnInit="btnOKAdd_Init" OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;">
                                                                <asp:Literal ID="Literal5" runat="server" Text="OK+" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                                OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                                                                <asp:Literal ID="Literal3" runat="server" Text="OK" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel"
                                                                OnClientClick="gridCadastro.CancelEdit(); return false;">
                                                                <asp:Literal ID="Literal7" runat="server" Text="Cancelar" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                            </EditForm>
                                            <StatusBar>
                                                <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text="" />
                                            </StatusBar>
                                        </Templates>
                                        <SettingsPopup EditForm-Width="600px" />
                                        <SettingsCommandButton>
                                            <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                                        </SettingsCommandButton>
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro"
            Landscape="true" />
        <cc1:esDataSource ID="EsDSAgendaFundo" runat="server" OnesSelect="EsDSAgendaFundo_esSelect"
            LowLevelBind="true" />
        <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" />
    </form>
</body>
</html>
