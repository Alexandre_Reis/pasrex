﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.Fundo;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Common.Enums;
using Financial.Web.Common;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;
using System.Net.Configuration;
using System.Web.Configuration;

public partial class CadastrosBasicos_CotaRepetidaFundo : CadastroBasePage
{
    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasFiltro = true;
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSHistoricoCota_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        HistoricoCotaGalgoQuery historicoCotaQuery = new HistoricoCotaGalgoQuery("H");
        CarteiraQuery carteiraQuery = new CarteiraQuery("A");
        ClienteQuery clienteQuery = new ClienteQuery("C");

        historicoCotaQuery.Select(historicoCotaQuery.Data, historicoCotaQuery.Cota,
                                  carteiraQuery.Apelido, carteiraQuery.IdCarteira);
        historicoCotaQuery.Where(clienteQuery.TipoControle.Equal(TipoControleCliente.ApenasCotacao) |
                            clienteQuery.TipoControle.Equal(TipoControleCliente.CarteiraImportada) |
                            clienteQuery.TipoControle.Equal(TipoControleCliente.Cotista),
                            clienteQuery.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo));
        historicoCotaQuery.InnerJoin(clienteQuery).On(historicoCotaQuery.IdCarteira == clienteQuery.IdCliente);
        historicoCotaQuery.InnerJoin(carteiraQuery).On(historicoCotaQuery.IdCarteira == carteiraQuery.IdCarteira);
        historicoCotaQuery.Where(historicoCotaQuery.Aprovar == (int)Financial.Util.Enums.AprovarCotaGalgo.Aprovar);


        ASPxDateEdit textDataInicio = popupFiltro.FindControl("textDataInicio") as ASPxDateEdit;
        ASPxDateEdit textDataFim = popupFiltro.FindControl("textDataFim") as ASPxDateEdit;

        if (textDataInicio.Text != "")
        {
            historicoCotaQuery.Where(historicoCotaQuery.Data.GreaterThanOrEqual(textDataInicio.Text));
        }

        if (textDataFim.Text != "")
        {
            historicoCotaQuery.Where(historicoCotaQuery.Data.LessThanOrEqual(textDataFim.Text));
        }

        if (dropCarteiraFiltro.SelectedIndex > -1)
        {
            historicoCotaQuery.Where(historicoCotaQuery.IdCarteira.Equal(Convert.ToInt32(dropCarteiraFiltro.SelectedItem.Value)));
        }

        historicoCotaQuery.OrderBy(historicoCotaQuery.Data.Descending);

        HistoricoCotaGalgoCollection coll = new HistoricoCotaGalgoCollection();
        coll.Load(historicoCotaQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CarteiraCollection coll = new CarteiraCollection();

        CarteiraQuery carteiraQuery = new CarteiraQuery("A");
        ClienteQuery clienteQuery = new ClienteQuery("C");

        carteiraQuery.Select(carteiraQuery.IdCarteira, carteiraQuery.Apelido);
        carteiraQuery.Where((clienteQuery.TipoControle.Equal(TipoControleCliente.ApenasCotacao) |
                            clienteQuery.TipoControle.Equal(TipoControleCliente.Cotista) |
                            clienteQuery.TipoControle.Equal(TipoControleCliente.CarteiraImportada)),
                            clienteQuery.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo),
                            clienteQuery.IdTipo.In((int)TipoClienteFixo.Fundo, (int)TipoClienteFixo.FDIC, (int)TipoClienteFixo.Clube));

        carteiraQuery.InnerJoin(clienteQuery).On(carteiraQuery.IdCarteira == clienteQuery.IdCliente);
        carteiraQuery.OrderBy(carteiraQuery.Apelido.Ascending);

        coll.Load(carteiraQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
        ASPxComboBox dropCarteira = gridCadastro.FindEditFormTemplateControl("dropCarteira") as ASPxComboBox;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxSpinEdit textCota = gridCadastro.FindEditFormTemplateControl("textCota") as ASPxSpinEdit;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(dropCarteira);
        controles.Add(textData);
        controles.Add(textCota);

        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        int idCarteira = Convert.ToInt32(dropCarteira.SelectedItem.Value);
        DateTime data = Convert.ToDateTime(textData.Text);

        HistoricoCota historicoCota = new HistoricoCota();
        if (gridCadastro.IsNewRowEditing)
        {
            if (historicoCota.LoadByPrimaryKey(data, idCarteira))
            {
                e.Result = "Registro já existente";
                return;
            }
        }

        if (System.Configuration.ConfigurationManager.AppSettings["Cliente"].ToUpper() == "ITAU")
        {
            Cliente cliente = new Cliente();
            cliente.Query.Select(cliente.Query.Status,
                                 cliente.Query.DataDia);
            cliente.Query.Where(cliente.Query.IdCliente.Equal(idCarteira));
            cliente.Query.Load();

            if (cliente.DataDia.Value > data ||
                    (cliente.DataDia.Value == data &&
                        (cliente.Status.Value == (byte)StatusCliente.Fechado || cliente.Status.Value == (byte)StatusCliente.Divulgado)
                    )
                )
            {
                e.Result = "Fundo fechado/processado na data, ou com data posterior à da cota informada, operação cancelada.";
                return;
            }

            DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1);
            historicoCota = new HistoricoCota();
            if (historicoCota.LoadByPrimaryKey(dataAnterior, idCarteira))
            {
                if (historicoCota.CotaFechamento.HasValue)
                {
                    decimal valorCota = Convert.ToDecimal(textCota.Text);
                    decimal valorCotaAnterior = historicoCota.CotaFechamento.Value;

                    decimal variacao = ((valorCota / valorCotaAnterior) - 1M) * 100M;

                    if (Math.Abs(variacao) > 2)
                    {
                        e.Result = "IdItau";
                    }
                }
            }

        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textData_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as ASPxDateEdit).Enabled = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropCarteira_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as ASPxComboBox).Enabled = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "CompositeKey")
        {
            //Compose a primary key value
            string data = Convert.ToString(e.GetListSourceFieldValue(HistoricoCotaMetadata.ColumnNames.Data));
            string idCarteira = Convert.ToString(e.GetListSourceFieldValue(HistoricoCotaMetadata.ColumnNames.IdCarteira));
            e.Value = data + idCarteira;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        this.Salvar(false);
        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    private void Salvar(bool insert)
    {
        HistoricoCota historicoCota = new HistoricoCota();

        ASPxComboBox dropCarteira = gridCadastro.FindEditFormTemplateControl("dropCarteira") as ASPxComboBox;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxSpinEdit textCota = gridCadastro.FindEditFormTemplateControl("textCota") as ASPxSpinEdit;

        int idCarteira = Convert.ToInt32(dropCarteira.SelectedItem.Value);
        DateTime data = Convert.ToDateTime(textData.Text);
        decimal cota = Convert.ToDecimal(textCota.Text);
        decimal pl = 0;

        if (!insert)
        {
            if (!historicoCota.LoadByPrimaryKey(data, idCarteira))
            {
                return;
            }
            //Se estou fazendo update, manter PL com o mesmo valor da abertura, pois cotas importadas trazem PL
            pl = historicoCota.PLFechamento.Value;
        }

        historicoCota.Save(idCarteira, data, cota, pl, false);

        #region Log do Processo


        string descricaoOperacao = String.Format(
            "Cadastro de HistoricoCota - Operacao: {0} HistoricoCota: {1}; {2}",
            insert ? "Insert" : "Update",
            data, idCarteira + UtilitarioWeb.ToString(historicoCota));


        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        descricaoOperacao,
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);

        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        Salvar(true);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(HistoricoCotaGalgoMetadata.ColumnNames.IdCarteira);
            List<object> keyValuesData = gridCadastro.GetSelectedFieldValues(HistoricoCotaGalgoMetadata.ColumnNames.Data);
            for (int i = 0; i < keyValuesData.Count; i++)
            {
                int idCarteira = Convert.ToInt32(keyValuesId[i]);
                DateTime data = Convert.ToDateTime(keyValuesData[i]);

                HistoricoCotaGalgo historicoCota = new HistoricoCotaGalgo();
                if (historicoCota.LoadByPrimaryKey(data, idCarteira))
                {
                    HistoricoCotaGalgo historicoCotaClone = (HistoricoCotaGalgo)Utilitario.Clone(historicoCota);

                    historicoCota.MarkAsDeleted();
                    historicoCota.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cotas Pendentes de Inclusão (Galgo) - Operacao: Delete HistoricoCotaGalgo: " + data + "; " + idCarteira + UtilitarioWeb.ToString(historicoCotaClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        if (e.Parameters == "btnAprovar")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(HistoricoCotaGalgoMetadata.ColumnNames.IdCarteira);
            List<object> keyValuesData = gridCadastro.GetSelectedFieldValues(HistoricoCotaGalgoMetadata.ColumnNames.Data);
            for (int i = 0; i < keyValuesData.Count; i++)
            {
                int idCarteira = Convert.ToInt32(keyValuesId[i]);
                DateTime data = Convert.ToDateTime(keyValuesData[i]);

                HistoricoCotaGalgo historicoCota = new HistoricoCotaGalgo();
                if (historicoCota.LoadByPrimaryKey(data, idCarteira))
                {
                    HistoricoCota hCota = new HistoricoCota();

                    if (!hCota.LoadByPrimaryKey(data, idCarteira))
                    {
                        hCota = new HistoricoCota();
                        hCota.QuantidadeFechamento = 0;
                        hCota.AjustePL = 0;
                    }

                    hCota.Data = historicoCota.Data;
                    hCota.IdCarteira = historicoCota.IdCarteira;

                    hCota.CotaAbertura = historicoCota.Cota;
                    hCota.CotaFechamento = historicoCota.Cota;
                    hCota.CotaBruta = historicoCota.Cota;
                    //
                    hCota.PLAbertura = historicoCota.Pl;
                    hCota.PLFechamento = historicoCota.Pl;
                    hCota.PatrimonioBruto = historicoCota.Pl;
                    hCota.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cotas Pendentes de Inclusão (Galgo) - Operacao: Aprovacao HistoricoCotaGalgo: " + data + "; " + idCarteira + UtilitarioWeb.ToString(hCota),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                    historicoCota.Aprovar = (int)Financial.Util.Enums.AprovarCotaGalgo.Aprovada;
                    historicoCota.DataAprovacao = DateTime.Now;
                    historicoCota.UsuarioAprovacao = HttpContext.Current.User.Identity.Name;
                    historicoCota.Save();
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e)
    {
        this.FocaCampoGrid("textData", "textCota");
        base.gridCadastro_PreRender(sender, e);

        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        Label labelFiltroHeader = gridCadastro.FindStatusBarTemplateControl("labelFiltroHeader") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");
        if (textDataInicio.Text != "")
        {
            texto.Append(" Data Início >= ").Append(textDataInicio.Text.Substring(0, 10));
        }
        if (textDataFim.Text != "")
        {
            if (texto.Length > 0)
            {
                texto.Append(" |");
            }

            texto.Append(" Data Fim <= ").Append(textDataFim.Text.Substring(0, 10));
        }
        if (dropCarteiraFiltro.SelectedIndex > -1)
        {
            if (texto.Length > 0)
            {
                texto.Append(" |");
            }

            texto.Append(" Fundo = ").Append(dropCarteiraFiltro.Text);
        }

        labelFiltro.Text = texto.ToString();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e)
    {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing)
        {
            ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
            e.Properties["cpTextData"] = textData.ClientID;

            ASPxSpinEdit textCota = gridCadastro.FindEditFormTemplateControl("textCota") as ASPxSpinEdit;
            e.Properties["cpTextCota"] = textCota.ClientID;
        }
    }
}