﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.CRM;
using Financial.Security;
using Financial.Fundo;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Web.Common;
using EntitySpaces.Interfaces;

public partial class CadastrosBasicos_IncorporacaoFundo : CadastroBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        
        //this.gridCadastro.ClientSideEvents.RowDblClick = "function(s, e) { alert('teste'); }";

        this.AllowUpdate = false; // Não permite Update
        //
        this.HasPopupCarteira = true;   // PopUp Carteira Origem
        this.HasPopupCarteira1 = true;  // PopUp Carteira Destino
        //
        this.HasFiltro = true; // Indica que tem Filtro
        //
        base.Page_Load(sender, e);
    }

    #region DataSources

    /// <summary>
    /// Consulta Principal
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSIncorporacaoFundo_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        IncorporacaoFundoQuery incorporacaoFundoQuery = new IncorporacaoFundoQuery("I");
        CarteiraQuery carteiraOrigemQuery = new CarteiraQuery("C1");
        CarteiraQuery carteiraDestinoQuery = new CarteiraQuery("C2");
        //
        incorporacaoFundoQuery.Select(incorporacaoFundoQuery, 
                                           ( carteiraOrigemQuery.IdCarteira.Cast(esCastType.String) + " - " + carteiraOrigemQuery.Apelido ).As("ApelidoOrigem"),
                                           ( carteiraDestinoQuery.IdCarteira.Cast(esCastType.String) + " - " +  carteiraDestinoQuery.Apelido ).As("ApelidoDestino") );
        //
        incorporacaoFundoQuery.InnerJoin(carteiraOrigemQuery).On(incorporacaoFundoQuery.IdCarteiraOrigem == carteiraOrigemQuery.IdCarteira);
        incorporacaoFundoQuery.InnerJoin(carteiraDestinoQuery).On(incorporacaoFundoQuery.IdCarteiraDestino == carteiraDestinoQuery.IdCarteira);

        //
        incorporacaoFundoQuery.OrderBy(incorporacaoFundoQuery.Data.Descending, incorporacaoFundoQuery.IdCarteiraOrigem.Ascending);
        //

        ASPxDateEdit textDataInicio = popupFiltro.FindControl("textDataInicio") as ASPxDateEdit;
        ASPxDateEdit textDataFim = popupFiltro.FindControl("textDataFim") as ASPxDateEdit;

        if (textDataInicio.Text != "") {
            incorporacaoFundoQuery.Where(incorporacaoFundoQuery.Data >= textDataInicio.Text);
        }

        if (textDataFim.Text != "") {
            incorporacaoFundoQuery.Where(incorporacaoFundoQuery.Data <= textDataFim.Text);
        }

        IncorporacaoFundoCollection coll = new IncorporacaoFundoCollection();
        coll.Load(incorporacaoFundoQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    /// <summary>
    /// Popup Carteira Origem e Destino
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CarteiraCollection coll = new CarteiraCollection();
        coll.BuscaCarteirasFundosClubes();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    #endregion

    #region Popups Carteira
    /// <summary>
    /// Pop Up Carteira Origem
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCarteira = Convert.ToInt32(e.Parameter);
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            if (carteira.str.Apelido != "") {
                if (carteira.IsAtivo) {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    nome = permissaoCliente.RetornaAcessoCliente(idCarteira, HttpContext.Current.User.Identity.Name)
                           ? carteira.str.Apelido
                           : "no_access";

                }
                else {
                    nome = "no_active";
                }
            }
        }

        e.Result = nome;
    }

    /// <summary>
    /// Pop Up Carteira Destino
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback2_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCarteira = Convert.ToInt32(e.Parameter);
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            if (carteira.str.Apelido != "") {
                if (carteira.IsAtivo) {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    nome = permissaoCliente.RetornaAcessoCliente(idCarteira, HttpContext.Current.User.Identity.Name)
                           ? carteira.str.Apelido : "no_access";
                }
                else {
                    nome = "no_active";
                }
            }
        }

        e.Result = nome;
    }
    #endregion

    /// <summary>
    /// Tratamento Erro
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";

        ASPxSpinEdit btnEditCodigoCarteiraOrigem = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteiraOrigem") as ASPxSpinEdit;
        ASPxSpinEdit btnEditCodigoCarteiraDestino = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteiraDestino") as ASPxSpinEdit;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(btnEditCodigoCarteiraOrigem);
        controles.Add(btnEditCodigoCarteiraDestino);
        controles.Add(textData);

        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion
       
        if( Convert.ToInt32(btnEditCodigoCarteiraOrigem.Text.Trim()) == Convert.ToInt32(btnEditCodigoCarteiraDestino.Text.Trim()) ) {
            e.Result = "Carteira Origem não pode ser Igual Carteira Destino.";
            return;
        } 

        if (gridCadastro.IsNewRowEditing) {
            int idCarteiraOrigem = Convert.ToInt32(btnEditCodigoCarteiraOrigem.Text);
            int idCarteiraDestino = Convert.ToInt32(btnEditCodigoCarteiraDestino.Text);
            DateTime data = Convert.ToDateTime(textData.Text);

            IncorporacaoFundo incorporacaoFundo = new IncorporacaoFundo();
            if (incorporacaoFundo.LoadByPrimaryKey(idCarteiraOrigem, idCarteiraDestino, data)){
                e.Result = "Registro já existente.";
            }
        }
    }

    /// <summary>
    /// Define Chave Primária
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) {
        if (e.Column.FieldName == "CompositeKey") {
            //Compose a primary key value
            string idCarteiraOrigem = Convert.ToString(e.GetListSourceFieldValue(IncorporacaoFundoMetadata.ColumnNames.IdCarteiraOrigem));
            string idCarteiraDestino = Convert.ToString(e.GetListSourceFieldValue(IncorporacaoFundoMetadata.ColumnNames.IdCarteiraDestino));
            DateTime data = Convert.ToDateTime(e.GetListSourceFieldValue(IncorporacaoFundoMetadata.ColumnNames.Data));

            e.Value = idCarteiraOrigem + idCarteiraDestino + data;
        }
    }

    /// <summary>
    /// Inserção
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        Salvar();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// Insere Registro
    /// </summary>
    private void Salvar() {
        ASPxSpinEdit btnEditCodigoCarteiraOrigem = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteiraOrigem") as ASPxSpinEdit;
        ASPxSpinEdit btnEditCodigoCarteiraDestino = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteiraDestino") as ASPxSpinEdit;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxSpinEdit textTaxa = gridCadastro.FindEditFormTemplateControl("textTaxa") as ASPxSpinEdit;

        IncorporacaoFundo incorporacaoFundo = new IncorporacaoFundo();

        incorporacaoFundo.IdCarteiraOrigem = Convert.ToInt32(btnEditCodigoCarteiraOrigem.Text);
        incorporacaoFundo.IdCarteiraDestino = Convert.ToInt32(btnEditCodigoCarteiraDestino.Text);
        incorporacaoFundo.Data = Convert.ToDateTime(textData.Text);

        if (textTaxa.Text != "")
        {
            incorporacaoFundo.Taxa = Convert.ToDecimal(textTaxa.Text);
        }
        incorporacaoFundo.Save();
        
        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de Incorporação Fundo - Operacao: Insert IncorporacaoFundo: " + incorporacaoFundo.IdCarteiraOrigem.Value + "; " + incorporacaoFundo.IdCarteiraDestino.Value + "; " + incorporacaoFundo.Data.Value + "; " + UtilitarioWeb.ToString(incorporacaoFundo),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    /// <summary>
    /// Tratamento Delete
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            #region Delete
            List<object> keyValuesIdCarteiraOrigem = gridCadastro.GetSelectedFieldValues(IncorporacaoFundoMetadata.ColumnNames.IdCarteiraOrigem);
            List<object> keyValuesIdCarteiraDestino = gridCadastro.GetSelectedFieldValues(IncorporacaoFundoMetadata.ColumnNames.IdCarteiraDestino);
            List<object> keyValuesData = gridCadastro.GetSelectedFieldValues(IncorporacaoFundoMetadata.ColumnNames.Data);

            for (int i = 0; i < keyValuesIdCarteiraOrigem.Count; i++) {
                int idCarteiraOrigem = Convert.ToInt32(keyValuesIdCarteiraOrigem[i]);
                int idCarteiraDestino = Convert.ToInt32(keyValuesIdCarteiraDestino[i]);
                DateTime data = Convert.ToDateTime(keyValuesData[i]);

                IncorporacaoFundo incorporacaoFundo = new IncorporacaoFundo();
                if (incorporacaoFundo.LoadByPrimaryKey(idCarteiraOrigem, idCarteiraDestino, data)) {
                    IncorporacaoFundo incorporacaoFundoClone = (IncorporacaoFundo)Utilitario.Clone(incorporacaoFundo);
                    //

                    incorporacaoFundo.MarkAsDeleted();
                    incorporacaoFundo.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de Incorporação Fundo - Operacao: Delete Incorporação Fundo: " + idCarteiraOrigem + "; " + idCarteiraDestino + "; " + data + "; " + UtilitarioWeb.ToString(incorporacaoFundoClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
            #endregion
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// Foco Inicial e Mensagem de Data
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("btnEditCodigoCarteiraOrigem");
        base.gridCadastro_PreRender(sender, e);
        
        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        Label labelFiltroHeader = gridCadastro.FindStatusBarTemplateControl("labelFiltroHeader") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");
        if (textDataInicio.Text != "") {
            texto.Append(" Data Início >= ").Append(textDataInicio.Text.Substring(0, 10));
        }
        if (textDataFim.Text != "") {
            if (textDataInicio.Text != "")
                texto.Append(" |");

            texto.Append(" Data Fim <= ").Append(textDataFim.Text.Substring(0, 10));
        }

        labelFiltro.Text = texto.ToString();
    }
}