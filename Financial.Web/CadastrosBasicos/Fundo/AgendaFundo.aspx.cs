﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

using Financial.Util;
using Financial.Security;
using Financial.Fundo;
using Financial.Web.Common;

using DevExpress.Web;

using Financial.Common.Enums;
using Financial.Security.Enums;
using Financial.Web.Util;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Fundo.Enums;

public partial class CadastrosBasicos_AgendaFundo : CadastroBasePage {
    const int TIPO_INPUT_POR_VALOR = (int)TipoValorProvento.ValorporCota;
    const int TIPO_INPUT_POR_TAXA_COTA_EX = (int)TipoValorProvento.TaxaCotaEx;
    const int TIPO_INPUT_POR_TAXA_FIXA = (int)TipoValorProvento.TaxaFixa;
    
    new protected void Page_Load(object sender, EventArgs e) {
            
        this.HasPopupCarteira = true;
        this.HasFiltro = true;
        base.Page_Load(sender, e);
       
        UtilitarioGrid.ExportacaoPDFExcel(this.gridExport,
                          new List<string>(new string[] { AgendaFundoMetadata.ColumnNames.TipoEvento }));        
    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSAgendaFundo_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {

        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");        
        AgendaFundoQuery agendaFundoQuery = new AgendaFundoQuery("A");
        CarteiraQuery carteiraQuery = new CarteiraQuery("C");
        ClienteQuery clienteQuery = new ClienteQuery("Cliente");

        agendaFundoQuery.Select(agendaFundoQuery, carteiraQuery.IdCarteira, carteiraQuery.Apelido);
        //
        agendaFundoQuery.InnerJoin(carteiraQuery).On(agendaFundoQuery.IdCarteira == carteiraQuery.IdCarteira);
        agendaFundoQuery.InnerJoin(clienteQuery).On(agendaFundoQuery.IdCarteira == clienteQuery.IdCliente);        
        agendaFundoQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        agendaFundoQuery.Where(permissaoClienteQuery.IdUsuario == idUsuario &
                               clienteQuery.TipoControle.NotEqual((byte)TipoControleCliente.ApenasCotacao));

        if (this.textDataInicio.Text != "") {
            agendaFundoQuery.Where(agendaFundoQuery.DataEvento >= this.textDataInicio.Text);
        }

        if (this.textDataFim.Text != "") {
            agendaFundoQuery.Where(agendaFundoQuery.DataEvento <= this.textDataFim.Text);
        }

        agendaFundoQuery.OrderBy(agendaFundoQuery.IdCarteira.Ascending, 
                                 agendaFundoQuery.DataEvento.Descending, 
                                 agendaFundoQuery.TipoEvento.Descending);

        AgendaFundoCollection coll1 = new AgendaFundoCollection();
        coll1.Load(agendaFundoQuery);


        AgendaFundoQuery agendaFundoQuery2 = new AgendaFundoQuery("A1");
        CarteiraQuery carteiraQuery2 = new CarteiraQuery("C1");
        ClienteQuery clienteQuery2 = new ClienteQuery("Cliente1");

        agendaFundoQuery2.Select(agendaFundoQuery2, carteiraQuery2.IdCarteira, carteiraQuery2.Apelido);
        //
        agendaFundoQuery2.InnerJoin(carteiraQuery2).On(agendaFundoQuery2.IdCarteira == carteiraQuery2.IdCarteira);
        agendaFundoQuery2.InnerJoin(clienteQuery2).On(agendaFundoQuery2.IdCarteira == clienteQuery2.IdCliente);        
        agendaFundoQuery2.Where(clienteQuery2.TipoControle.Equal((byte)TipoControleCliente.ApenasCotacao));

        if (this.textDataInicio.Text != "")
        {
            agendaFundoQuery2.Where(agendaFundoQuery2.DataEvento >= this.textDataInicio.Text);
        }

        if (this.textDataFim.Text != "")
        {
            agendaFundoQuery2.Where(agendaFundoQuery2.DataEvento <= this.textDataFim.Text);
        }

        agendaFundoQuery2.OrderBy(agendaFundoQuery2.IdCarteira.Ascending,
                                 agendaFundoQuery2.DataEvento.Descending,
                                 agendaFundoQuery2.TipoEvento.Descending);

        AgendaFundoCollection coll2 = new AgendaFundoCollection();
        coll2.Load(agendaFundoQuery2);

        coll1.Combine(coll2);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll1;
    }

    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        CarteiraCollection coll = new CarteiraCollection();
        coll.BuscaCarteirasComAcessoTodas(Context.User.Identity.Name);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    protected void btnEditCodigo_Load(object sender, EventArgs e) {
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxSpinEdit).Enabled = false;
            (sender as ASPxSpinEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string result = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCarteira = Convert.ToInt32(e.Parameter);
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            if (carteira.str.Apelido != "")
            {
                if (carteira.IsAtivo)
                {
                    Cliente cliente = new Cliente();
                    cliente.LoadByPrimaryKey(idCarteira);

                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    result = permissaoCliente.RetornaAcessoCliente(idCarteira, HttpContext.Current.User.Identity.Name) ? carteira.str.Apelido : "no_access";

                    result += "|";        
                    result += cliente.TipoControle.Value;
                }
                else
                {
                    result = "no_active";
                }
            }
        }
        e.Result = result;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        if (gridCadastro.IsEditing)
        {
            ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
            ASPxComboBox dropTipoEvento = gridCadastro.FindEditFormTemplateControl("dropTipoEvento") as ASPxComboBox;
            ASPxDateEdit textDataEvento = gridCadastro.FindEditFormTemplateControl("textDataEvento") as ASPxDateEdit;
            ASPxSpinEdit textValorInput = gridCadastro.FindEditFormTemplateControl("textValorInput") as ASPxSpinEdit;
            ASPxComboBox dropTipoValorInput = gridCadastro.FindEditFormTemplateControl("dropTipoValorInput") as ASPxComboBox;

            #region Campos obrigatórios
            List<Control> controles = new List<Control>(new Control[] { 
                btnEditCodigoCarteira, dropTipoEvento, textDataEvento, dropTipoValorInput, textValorInput
            });

            if (base.TestaObrigatorio(controles) != "")
            {
                e.Result = "Campos com * são obrigatórios!";
                return;
            }
            #endregion

            if (!Calendario.IsDiaUtil(Convert.ToDateTime(textDataEvento.Text), LocalFeriadoFixo.Brasil, TipoFeriado.Brasil))
            {
                e.Result = "Data do evento não é dia útil.";
                return;
            }

            DateTime dataEvento = Convert.ToDateTime(textDataEvento.Text);
            int tipoEvento = Convert.ToInt32(dropTipoEvento.SelectedItem.Value);
            int idCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);

            List<int> lstEventos = new List<int>();
            if (tipoEvento == (int)TipoEventoFundo.AmortizacaoJuros)
            {
                lstEventos.Add((int)TipoEventoFundo.AmortizacaoJuros);
                lstEventos.Add((int)TipoEventoFundo.Amortizacao);
                lstEventos.Add((int)TipoEventoFundo.ComeCotas);
                lstEventos.Add((int)TipoEventoFundo.Dividendo);
                lstEventos.Add((int)TipoEventoFundo.Juros);                
            }
            else
            {
                lstEventos.Add(tipoEvento);
                lstEventos.Add((int)TipoEventoFundo.AmortizacaoJuros);
            }

            AgendaFundoCollection agendaFundoColl = new AgendaFundoCollection();
            agendaFundoColl.Query.Where(agendaFundoColl.Query.DataEvento.Equal(dataEvento) &
                                        agendaFundoColl.Query.IdCarteira.Equal(idCarteira) &
                                        agendaFundoColl.Query.TipoEvento.In(lstEventos.ToArray()));

            if (!gridCadastro.IsNewRowEditing)
            {
                int idAgenda = Convert.ToInt32(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, AgendaFundoMetadata.ColumnNames.IdAgenda));
                agendaFundoColl.Query.Where(agendaFundoColl.Query.IdAgenda.NotEqual(idAgenda));
            }

            if (agendaFundoColl.Query.Load())
            {
                string mensagem = "Já existe evento(s) cadastrado(s) para a data/cliente informado: \n";

                for (int i = 0; i < agendaFundoColl.Count; i++)
                {
                    mensagem += (i + 1) + " - " + TipoEventoFundoDescricao.RetornaDescricao(agendaFundoColl[i].TipoEvento.Value);

                    if ((i + 1) < agendaFundoColl.Count)
                        mensagem += "\n";
                }

                e.Result = mensagem;
                return;
            }
        }
    }


    /// <summary>
    /// Configura Visibilidade do botão Ok++
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnOKAdd_Init(object sender, EventArgs e) {
        // Se for Update Não Aparece botão de Ok++
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        this.SalvarNovo();
    }

    /// <summary>
    /// Inserção de um novo registro
    /// </summary>
    private void SalvarNovo() {
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxComboBox dropTipoEvento = gridCadastro.FindEditFormTemplateControl("dropTipoEvento") as ASPxComboBox;
        ASPxDateEdit textDataEvento = gridCadastro.FindEditFormTemplateControl("textDataEvento") as ASPxDateEdit;
        ASPxSpinEdit textValorInput = gridCadastro.FindEditFormTemplateControl("textValorInput") as ASPxSpinEdit;
        ASPxComboBox dropTipoValorInput = gridCadastro.FindEditFormTemplateControl("dropTipoValorInput") as ASPxComboBox;
        ASPxComboBox dropImpactarCota = gridCadastro.FindEditFormTemplateControl("dropImpactarCota") as ASPxComboBox;

        int idCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);
        DateTime dataEvento = Convert.ToDateTime(textDataEvento.Text);
        decimal taxa = 0, valor = 0;
        decimal valorInput = Convert.ToDecimal(textValorInput.Text);
        string impactarCota = "S";
        if(dropImpactarCota.SelectedIndex > -1)
            impactarCota = Convert.ToString(dropImpactarCota.Value);

        int tipoInput = Convert.ToInt32(dropTipoValorInput.SelectedItem.Value);
        if (tipoInput == TIPO_INPUT_POR_VALOR)
        {
            valor = valorInput;
        }

        if (tipoInput == TIPO_INPUT_POR_TAXA_COTA_EX || tipoInput == TIPO_INPUT_POR_TAXA_FIXA)
        {
            taxa = valorInput;
            valor = 0;
        }

        AgendaFundo agendaFundo = new AgendaFundo();
        //
        agendaFundo.IdCarteira = idCarteira;
        agendaFundo.TipoEvento = Convert.ToByte(dropTipoEvento.SelectedItem.Value);
        agendaFundo.DataEvento = dataEvento;
        agendaFundo.Valor = valor;
        agendaFundo.Taxa = taxa;
        agendaFundo.TipoValorInput = tipoInput;
        agendaFundo.ImpactarCota = impactarCota;

        agendaFundo.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de AgendaFundo - Operacao: Insert AgendaFundo: " + agendaFundo.IdAgenda + UtilitarioWeb.ToString(agendaFundo),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e){
        int idAgenda = (int)e.Keys[0];
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
        ASPxComboBox dropTipoEvento = gridCadastro.FindEditFormTemplateControl("dropTipoEvento") as ASPxComboBox;
        ASPxDateEdit textDataEvento = gridCadastro.FindEditFormTemplateControl("textDataEvento") as ASPxDateEdit;
        ASPxSpinEdit textValorInput = gridCadastro.FindEditFormTemplateControl("textValorInput") as ASPxSpinEdit;
        ASPxComboBox dropTipoValorInput = gridCadastro.FindEditFormTemplateControl("dropTipoValorInput") as ASPxComboBox;
        ASPxComboBox dropImpactarCota = gridCadastro.FindEditFormTemplateControl("dropImpactarCota") as ASPxComboBox;

        int idCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);
        DateTime dataEvento = Convert.ToDateTime(textDataEvento.Text);
        decimal taxa = 0, valor = 0;
        decimal valorInput = Convert.ToDecimal(textValorInput.Text);
        string impactarCota = Convert.ToString(dropImpactarCota.Value);

        AgendaFundo agendaFundo = new AgendaFundo();
        if (agendaFundo.LoadByPrimaryKey(idAgenda))
        {
            int tipoInput = Convert.ToInt32(dropTipoValorInput.SelectedItem.Value);

            if (tipoInput == TIPO_INPUT_POR_VALOR)
            {
                valor = valorInput;
            }

            if (tipoInput == TIPO_INPUT_POR_TAXA_COTA_EX || tipoInput == TIPO_INPUT_POR_TAXA_FIXA)
            {
                taxa = valorInput;
                valor = 0;
            }

            agendaFundo.IdCarteira = idCarteira;
            agendaFundo.TipoEvento = Convert.ToByte(dropTipoEvento.SelectedItem.Value);
            agendaFundo.DataEvento = dataEvento;
            agendaFundo.Valor = valor;
            agendaFundo.Taxa = taxa;
            agendaFundo.TipoValorInput = tipoInput;
            agendaFundo.ImpactarCota = impactarCota;

            agendaFundo.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de AgendaFundo - Operacao: Update AgendaFundo: " + idAgenda + UtilitarioWeb.ToString(agendaFundo),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        this.SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues(AgendaFundoMetadata.ColumnNames.IdAgenda);
            for (int i = 0; i < keyValuesId.Count; i++) {
                int idAgenda = Convert.ToInt32(keyValuesId[i]);

                AgendaFundo agendaFundo = new AgendaFundo();
                if (agendaFundo.LoadByPrimaryKey(idAgenda)) {

                    AgendaFundo agendaFundoClone = (AgendaFundo)Utilitario.Clone(agendaFundo);

                    agendaFundo.MarkAsDeleted();
                    agendaFundo.Save();

                    #region Log do Processo
                        HistoricoLog historicoLog = new HistoricoLog();
                        historicoLog.InsereHistoricoLog(DateTime.Now,
                                                        DateTime.Now,
                                                        "Cadastro de AgendaFundo - Operacao: Delete AgendaFundo: " + idAgenda + UtilitarioWeb.ToString(agendaFundoClone),
                                                        HttpContext.Current.User.Identity.Name,
                                                        UtilitarioWeb.GetIP(Request),
                                                        "",
                                                        HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e) {
        base.gridCadastro_CustomJSProperties(sender, e);

        ASPxGridView gridView = sender as ASPxGridView;
        if (gridView.IsEditing)
        {
            ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteira") as ASPxSpinEdit;
            e.Properties["cpBtnEditCodigoCarteira"] = btnEditCodigoCarteira.UniqueID;
        }
    }

    new protected void dropTipoValorInput_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
        {
            int tipoEvento = Convert.ToInt32(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, AgendaFundoMetadata.ColumnNames.TipoEvento));
            (sender as ASPxComboBox).ClientEnabled = (tipoEvento != (int)TipoEventoFundo.AmortizacaoJuros);
        }
    }

    new protected void dropImpactarCota_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
        {
            int idCarteira = Convert.ToInt32(gridCadastro.GetRowValues(gridCadastro.EditingRowVisibleIndex, AgendaFundoMetadata.ColumnNames.IdCarteira));

            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCarteira);

            (sender as ASPxComboBox).ClientEnabled = (cliente.TipoControle.Value == (int)TipoControleCliente.ApenasCotacao);       
        }
    }

    new protected void textValorInput_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
        {
            (sender as ASPxSpinEdit).Value = Session["textValorInput"];

        }
    }
    
    protected void gridCadastro_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
        {
            int idAgendaFundo = (int)e.EditingKeyValue;
            AgendaFundo agendaFundo = new AgendaFundo();
            agendaFundo.LoadByPrimaryKey(idAgendaFundo);

            Session["dropTipoValorInput"] = agendaFundo.TipoValorInput.Value.ToString();

            if (agendaFundo.TipoValorInput.Value.Equals(Convert.ToInt16(TIPO_INPUT_POR_VALOR)))
            {
                Session["textValorInput"] = agendaFundo.Valor.Value;
            }
            else
            {
                Session["textValorInput"] = agendaFundo.Taxa.Value;
            }
            
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        
        this.FocaCampoGrid("btnEditCodigoCarteira");
        base.gridCadastro_PreRender(sender, e);
               
        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");
        if (textDataInicio.Text != "") {
            texto.Append(" Data Evento >= ").Append(textDataInicio.Text.Substring(0, 10));
        }
        if (textDataFim.Text != "") {
            if (texto.Length > 0) {
                texto.Append(" |");
            }

            texto.Append(" Data Fim <= ").Append(textDataFim.Text.Substring(0, 10));
        }

        labelFiltro.Text = texto.ToString();
    }
}