﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using Financial.Investidor;
using Financial.ContaCorrente;
using System.Drawing;
using Financial.ContaCorrente.Enums;
using Financial.Security;
using Financial.Fundo;
using Financial.Bolsa;
using Financial.BMF;
using Financial.RendaFixa;
using Financial.Investidor.Enums;
using Financial.InvestidorCotista;
using Financial.CRM;
using Financial.WebConfigConfiguration;
using Financial.Security.Enums;
using System.Collections.Specialized;
using EntitySpaces.Interfaces;
using Financial.Web.Util;
using Financial.InvestidorCotista.Enums;
using Financial.Web.Common;
using System.Web.Configuration;
using System.Xml;
using Financial.Export;
using Financial.Interfaces.Export;
using DevExpress.Web.Data;
using System.ComponentModel;
using Financial.Fundo.Enums;
using System.Web.Services;

public partial class CadastrosBasicos_FormaCondominio : CadastroBasePage
{
    public bool VerificaCarteiraParametro
    {
        get
        {
            if (Session["VerificaCarteiraParametro"] == null)
                Session["VerificaCarteiraParametro"] = false;

            return (bool)Session["VerificaCarteiraParametro"];
        }
        set
        {
            Session["VerificaCarteiraParametro"] = value;
        }
    }


    protected void btnEditCarteira_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
        {
            (sender as ASPxButtonEdit).Enabled = false;
            (sender as ASPxButtonEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }


    new protected void Page_Load(object sender, EventArgs e)
    {
        this.HasPopupCarteira = true;
        base.Page_Load(sender, e);
    }

    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCarteira = Convert.ToInt32(e.Parameter);
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            if (carteira.str.Apelido != "")
            {
                if (carteira.IsAtivo)
                {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    nome = permissaoCliente.RetornaAcessoCliente(idCarteira, HttpContext.Current.User.Identity.Name)
                           ? carteira.str.Apelido
                           : "no_access";

                }
                else
                {
                    nome = "no_active";
                }
            }
        }

        e.Result = e.Parameter + " - " + nome;
    }

    #region Instância
    //Collection
    FundoInvestimentoFormaCondominioCollection formaCondominioCollection = new FundoInvestimentoFormaCondominioCollection();
    CarteiraCollection carteiraCollection = new CarteiraCollection();
    ParametroAdministradorFundoInvestimentoCollection parametroCollection = new ParametroAdministradorFundoInvestimentoCollection();
    //Query
    FundoInvestimentoFormaCondominioQuery formaCondominioQuery = new FundoInvestimentoFormaCondominioQuery();
    CarteiraQuery carteiraQuery = new CarteiraQuery();
    //Entidade
    FundoInvestimentoFormaCondominio formaCondominio = new FundoInvestimentoFormaCondominio();
    #endregion

    #region esDataSources
    protected void EsDSFundoInvestimentoFormaCondominio_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        formaCondominioCollection = new FundoInvestimentoFormaCondominioCollection();

        formaCondominioCollection.Query.Select();
        formaCondominioCollection.Query.OrderBy(formaCondominioCollection.Query.IdFormaCondominio.Ascending);
        formaCondominioCollection.Query.Load();

        e.Collection = formaCondominioCollection;
    }
    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        carteiraCollection = new CarteiraCollection();

        carteiraCollection.Query.Select();
        carteiraCollection.Query.OrderBy(carteiraCollection.Query.IdCarteira.Ascending);
        carteiraCollection.Query.Load();

        e.Collection = carteiraCollection;
    }
    #endregion

    #region CallBack
    /// <summary>
    /// Verifica se os campos obrigatorios foram devidamente preenchidos
    /// </summary>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";
        ASPxDateEdit textDataInicioVigencia = gridCadastro.FindEditFormTemplateControl("textDataInicioVigencia") as ASPxDateEdit;
        
        ASPxTextBox hiddenIdCarteira = gridCadastro.FindEditFormTemplateControl("hiddenIdCarteira") as ASPxTextBox;
        ASPxCallbackPanel cbPanel = gridCadastro.FindEditFormTemplateControl("cbPanel") as ASPxCallbackPanel;
        ASPxRadioButtonList rblFormaCondominio = cbPanel.FindControl("rblFormaCondominio") as ASPxRadioButtonList;
        ASPxRadioButtonList rblExecucaoRecolhimento = cbPanel.FindControl("rblExecucaoRecolhimento") as ASPxRadioButtonList;
        ASPxRadioButtonList rblAliquotaIR = cbPanel.FindControl("rblAliquotaIR") as ASPxRadioButtonList;
        ASPxDateEdit textDataRecolhimento = cbPanel.FindControl("textDataRecolhimento") as ASPxDateEdit;

        if (textDataInicioVigencia.Text == string.Empty)
            e.Result = "Campo 'Data Inicio Vigência' deve ser preenchido!";
        else if (hiddenIdCarteira.Text == string.Empty)
            e.Result = "Campo 'Fundo Investimento' deve ser preenchido!";
        else if (rblFormaCondominio.SelectedItem == null)
            e.Result = "Campo 'Forma Condomínio' deve ser preenchido!";
        else if (rblExecucaoRecolhimento.SelectedItem == null)
            e.Result = "Campo 'Execução do Recolhimento' deve ser preenchido!";
        else if (rblAliquotaIR.SelectedItem == null)
            e.Result = "Campo 'Alíquota IR' deve ser preenchido!";
        else if (rblExecucaoRecolhimento.SelectedItem != null)
        {
            if (rblExecucaoRecolhimento.SelectedItem.Value.Equals(3))
            {
                if (textDataRecolhimento.Text == string.Empty)
                    e.Result = "Campo 'Data Recolhimento' deve ser preenchido!";
            }
        }

        //Verifica se possui parâmetro quando for alteração
        if (!gridCadastro.IsNewRowEditing)
        {
            carteiraCollection = new CarteiraCollection();
            carteiraCollection.Query.Select(carteiraCollection.Query.IdAgenteAdministrador);
            carteiraCollection.Query.Where(carteiraCollection.Query.IdCarteira.Equal(Convert.ToInt32(hiddenIdCarteira.Text)));
            carteiraCollection.Query.Load();

            if (carteiraCollection.Count > 0)
            {
                parametroCollection = new ParametroAdministradorFundoInvestimentoCollection();
                parametroCollection.Query.Select();
                parametroCollection.Query.Where(parametroCollection.Query.Administrador.Equal(carteiraCollection[0].IdAgenteAdministrador));
                parametroCollection.Query.Load();
                if (parametroCollection.Count == 0)
                {
                    e.Result = "Parâmetros do Administrador devem ser previamente cadastrados para possibilitar o registro do evento corporativo";
                    return;
                }
            }
        }
        //Verifica se possui parâmetro quando for Inclusão
        else
        {
            if (!VerificaCarteiraParametro)
            {
                e.Result = "Parâmetros do Administrador devem ser previamente cadastrados para possibilitar o registro do evento corporativo";
                return;
            }

            //Verifica se já existe um evento para a carteira a ser cadastrada
            DateTime? Data = Financial.Util.ConverteTipo.TryParseDateTime(textDataInicioVigencia.Text);
            int? Carteira = Financial.Util.ConverteTipo.TryParseInt(hiddenIdCarteira.Text);
            if (Carteira != null && Data != null)
            {
                string existeEvento = VerificaEventoExistente(Carteira, Data);
                if (!string.IsNullOrEmpty(existeEvento))
                {
                    e.Result = existeEvento;
                    return;
                }

                //Verifica se há eventos cadastrados em um período posterior a 'Data Inicio Vigencia'
                if (VerificaDataEvento(Convert.ToDateTime(textDataInicioVigencia.Text), Carteira.Value))
                    e.Result = "Já existe um evento cadastrado para uma data posterior a Data de Vigência";
            }

            
        }
    }

    /// <summary>
    /// Deleta os registros selecionados na gridCadastro
    /// </summary>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdFormaCondominio");
            for (int i = 0; i < keyValuesId.Count; i++)
            {
                int idFormaCondominio = Convert.ToInt32(keyValuesId[i]);
                formaCondominio = new FundoInvestimentoFormaCondominio();

                if (formaCondominio.LoadByPrimaryKey(idFormaCondominio))
                {
                    FundoInvestimentoFormaCondominio formaCondominioClone = (FundoInvestimentoFormaCondominio)Utilitario.Clone(formaCondominio);

                    formaCondominio.MarkAsDeleted();
                    formaCondominio.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de Forma Condominio - Operacao: Delete Forma Condominio: " + idFormaCondominio + "; " + UtilitarioWeb.ToString(formaCondominioClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// Atribui mascara dinamicamente para os campos dependendo do tipo de campo
    /// OBS: O método é um AspxCallBackPanel, ele funciona como o UpdatePanel.
    /// Sua utilização é necessária para que não ocorra o postback na página
    /// e a mascara perca o seu valor.
    /// </summary>
    protected void cbPanel_Callback(object source, CallbackEventArgsBase e)
    {
        ASPxCallbackPanel cbPanel = gridCadastro.FindEditFormTemplateControl("cbPanel") as ASPxCallbackPanel;
        ASPxRadioButtonList rblAliquotaIR = cbPanel.FindControl("rblAliquotaIR") as ASPxRadioButtonList;
        ASPxRadioButtonList rblFormaCondominio = cbPanel.FindControl("rblFormaCondominio") as ASPxRadioButtonList;
        ASPxRadioButtonList rblExecucaoRecolhimento = cbPanel.FindControl("rblExecucaoRecolhimento") as ASPxRadioButtonList;
        ASPxDateEdit textDataRecolhimento = cbPanel.FindControl("textDataRecolhimento") as ASPxDateEdit;
        ASPxDateEdit textDataInicioVigencia = gridCadastro.FindEditFormTemplateControl("textDataInicioVigencia") as ASPxDateEdit;
        ASPxLabel hiddenTipoFundoAtual = cbPanel.FindControl("hiddenTipoFundoAtual") as ASPxLabel;

        carteiraCollection = new CarteiraCollection();
        carteiraCollection.Query.Select(carteiraCollection.Query.IdAgenteAdministrador);
        carteiraCollection.Query.Where(carteiraCollection.Query.IdCarteira.Equal(Convert.ToInt32(e.Parameter)));
        carteiraCollection.Query.Load();

        if (carteiraCollection.Count > 0)
        {
            parametroCollection = new ParametroAdministradorFundoInvestimentoCollection();
            parametroCollection.Query.Select();
            parametroCollection.Query.Where(parametroCollection.Query.Administrador.Equal(carteiraCollection[0].IdAgenteAdministrador));
            parametroCollection.Query.Load();
        }

        if (parametroCollection.Count > 0)
        {
            //Carteira possui parâmetros cadastrados
            VerificaCarteiraParametro = true;
            rblAliquotaIR.Value = parametroCollection[0].AliquotaIR;
            rblExecucaoRecolhimento.Value = parametroCollection[0].ExecucaoRecolhimento;
            textDataRecolhimento.Value = parametroCollection[0].DataRecolhimento;
        }
        else
        {
            //Carteira não possui parâmetros cadastrados
            VerificaCarteiraParametro = false;
            rblAliquotaIR.Value = -1;
            rblExecucaoRecolhimento.Value = -1;
            textDataRecolhimento.Value = null;
        }

        int tipoFundo = RetornaCondominioCarteira(
                Convert.ToInt32(e.Parameter),
                Convert.ToDateTime(textDataInicioVigencia.Text)
                );

        rblFormaCondominio.Value = tipoFundo ==1 ? 2 :1;

        


        hiddenTipoFundoAtual.Value = TipoFundoDescricao.RetornaStringValue(tipoFundo);

        
        
        
        
            

       
    }
    #endregion

    #region gridCadastro
    /// <summary>
    /// Efetua a alteração dos registros
    /// </summary>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        //ASPxComboBox dropFundoInvestimento = gridCadastro.FindEditFormTemplateControl("dropFundoInvestimento") as ASPxComboBox;
        ASPxTextBox hiddenIdCarteira = gridCadastro.FindEditFormTemplateControl("hiddenIdCarteira") as ASPxTextBox;        
        ASPxDateEdit textDataInicioVigencia = gridCadastro.FindEditFormTemplateControl("textDataInicioVigencia") as ASPxDateEdit;
        ASPxCallbackPanel cbPanel = gridCadastro.FindEditFormTemplateControl("cbPanel") as ASPxCallbackPanel;
        ASPxRadioButtonList rblExecucaoRecolhimento = cbPanel.FindControl("rblExecucaoRecolhimento") as ASPxRadioButtonList;
        ASPxRadioButtonList rblAliquotaIR = cbPanel.FindControl("rblAliquotaIR") as ASPxRadioButtonList;
        ASPxDateEdit textDataRecolhimento = cbPanel.FindControl("textDataRecolhimento") as ASPxDateEdit;
        ASPxRadioButtonList rblFormaCondominio = cbPanel.FindControl("rblFormaCondominio") as ASPxRadioButtonList;

        int idFormaCondominio = (int)e.Keys[0];
        if (formaCondominio.LoadByPrimaryKey(idFormaCondominio))
        {
            formaCondominio.IdCarteira = Convert.ToInt32(hiddenIdCarteira.Text);
            formaCondominio.FormaCondominio = Convert.ToInt32(rblFormaCondominio.SelectedItem.Value);
            formaCondominio.DataInicioVigencia = Financial.Util.ConverteTipo.TryParseDateTime(textDataInicioVigencia.Text);
            formaCondominio.ExecucaoRecolhimento = Convert.ToInt32(rblExecucaoRecolhimento.SelectedItem.Value);
            formaCondominio.AliquotaIR = Convert.ToInt32(rblAliquotaIR.SelectedItem.Value);
            formaCondominio.DataRecolhimento = Financial.Util.ConverteTipo.TryParseDateTime(textDataRecolhimento.Text);

            formaCondominio.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de Forma Condominio - Operacao: Update Forma Condominio: " + formaCondominio.IdFormaCondominio + UtilitarioWeb.ToString(formaCondominio),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// Efetua o cadastro 
    /// </summary>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        this.Cadastra();

        e.Cancel = true;
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// Atualiza o gridCadastro quando o usuário fechar o EditForm
    /// </summary>
    protected void gridCadastro_CancelRowEditing(object sender, ASPxStartRowEditingEventArgs e)
    {
        e.Cancel = false;
        gridCadastro.DataBind();
    }

    /// <summary>
    /// Efetua o tratamento dos campos
    /// </summary>
    protected void gridCadastro_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.FieldName == "IdCarteira")
        {
            carteiraCollection = new CarteiraCollection();

            carteiraCollection.Query.Select(carteiraCollection.Query.Nome);
            carteiraCollection.Query.Where(carteiraCollection.Query.IdCarteira.Equal(Convert.ToInt32(e.Value)));
            carteiraCollection.Query.Load();

            e.DisplayText = carteiraCollection[0].Nome;
        }
        if (e.Column.FieldName == "DataRecolhimento")
        {
            if (e.Value == null)
                e.DisplayText = string.Empty;
        }
    }
    #endregion

    #region Suporte
    /// <summary>
    /// Inserção de um novo registro
    /// </summary>
    private void Cadastra()
    {
        //ASPxComboBox dropFundoInvestimento = gridCadastro.FindEditFormTemplateControl("dropFundoInvestimento") as ASPxComboBox;
        ASPxTextBox hiddenIdCarteira = gridCadastro.FindEditFormTemplateControl("hiddenIdCarteira") as ASPxTextBox;
        
        ASPxDateEdit textDataInicioVigencia = gridCadastro.FindEditFormTemplateControl("textDataInicioVigencia") as ASPxDateEdit;
        ASPxCallbackPanel cbPanel = gridCadastro.FindEditFormTemplateControl("cbPanel") as ASPxCallbackPanel;
        ASPxRadioButtonList rblFormaCondominio = cbPanel.FindControl("rblFormaCondominio") as ASPxRadioButtonList;
        ASPxRadioButtonList rblExecucaoRecolhimento = cbPanel.FindControl("rblExecucaoRecolhimento") as ASPxRadioButtonList;
        ASPxRadioButtonList rblAliquotaIR = cbPanel.FindControl("rblAliquotaIR") as ASPxRadioButtonList;
        ASPxDateEdit textDataRecolhimento = cbPanel.FindControl("textDataRecolhimento") as ASPxDateEdit;

        formaCondominio = new FundoInvestimentoFormaCondominio();
        formaCondominio.IdCarteira = Convert.ToInt32(hiddenIdCarteira.Text);
        formaCondominio.FormaCondominio = Convert.ToInt32(rblFormaCondominio.SelectedItem.Value);
        formaCondominio.DataInicioVigencia = Financial.Util.ConverteTipo.TryParseDateTime(textDataInicioVigencia.Text);
        formaCondominio.ExecucaoRecolhimento = Convert.ToInt32(rblExecucaoRecolhimento.SelectedItem.Value);
        formaCondominio.AliquotaIR = Convert.ToInt32(rblAliquotaIR.SelectedItem.Value);
        formaCondominio.DataRecolhimento = Financial.Util.ConverteTipo.TryParseDateTime(textDataRecolhimento.Text);

        formaCondominio.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro Fundo - Operacao: Insert Forma Condominio: " + formaCondominio.IdFormaCondominio + UtilitarioWeb.ToString(formaCondominio),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    /// <summary>
    /// Verifica se o evento a ser cadastrado já possui alguem evento cadastrado na data definida
    /// </summary>
    /// <param name="idCarteira">Codigo da carteira a ser analisada</param>
    /// <param name="data">data do evento </param>
    /// <returns></returns>
    private string VerificaEventoExistente(int? idCarteira, DateTime? data)
    {
        string mensagem = string.Empty;

        #region Desenquadramento Tributário
        DesenquadramentoTributarioCollection dtc = new DesenquadramentoTributarioCollection();
        dtc.Query.Where(dtc.Query.IdCarteira == idCarteira, dtc.Query.Data == data);
        dtc.Query.Load();

        if (dtc.Count > 0)        
            return "Fundo já cadastrado para o evento de 'Desenquadramento Tributário'";
        
        #endregion

        #region Verifica evento
        EventoFundoCollection efc = new EventoFundoCollection();
        efc.Query.Where(efc.Query.IdCarteiraOrigem == idCarteira, efc.Query.DataPosicao == data);

        efc.Query.Load();

        if (efc.Count > 0)        
            return "Fundo já cadastrado para o evento de 'Evento Fundo'";
        

        EventoFundoCollection efc2 = new EventoFundoCollection();
        efc2.Query.Where(efc.Query.IdCarteiraDestino == idCarteira, efc.Query.DataPosicao == data);

        efc2.Query.Load();

        if (efc2.Count > 0)
            return "Fundo já cadastrado para o evento de 'Evento Fundo'";

        return string.Empty;
        #endregion

  
    }

    /// <summary>
    /// Verifica se a carteira possui um evento anterior a data de parâmetro
    /// </summary>
    public static int RetornaCondominioCarteira(int idCarteira, DateTime data)
    {
        int CondominioCarteira = 0;

        FundoInvestimentoFormaCondominioCollection formaCondominioCollection = new FundoInvestimentoFormaCondominioCollection();
        formaCondominioCollection.Query.es.Top = 1;
        formaCondominioCollection.Query.OrderBy(formaCondominioCollection.Query.DataInicioVigencia.Descending);
        formaCondominioCollection.Query.Where(formaCondominioCollection.Query.IdCarteira == idCarteira, formaCondominioCollection.Query.DataInicioVigencia.LessThan(data));

        formaCondominioCollection.Query.Load();

        if (formaCondominioCollection.Count > 0)
            CondominioCarteira = (int)formaCondominioCollection[0].FormaCondominio;
        else
        {
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);
            CondominioCarteira = (int)carteira.TipoFundo;
        }

        return CondominioCarteira;
    }

    /// <summary>
    /// Verifica se existe evento na data posterior a 'Data Inicio Vigencia'
    /// </summary>
    private bool VerificaDataEvento(DateTime data,int idCarteira )
    {

        bool existeEvento = false;

        formaCondominioCollection = new FundoInvestimentoFormaCondominioCollection();
        formaCondominioCollection.Query.Where(formaCondominioCollection.Query.DataInicioVigencia.GreaterThan(data),formaCondominioCollection.Query.IdCarteira == idCarteira );
        formaCondominioCollection.Query.Load();

        if (formaCondominioCollection.Count > 0)
            existeEvento = true;

        return existeEvento;
    }
    #endregion
}