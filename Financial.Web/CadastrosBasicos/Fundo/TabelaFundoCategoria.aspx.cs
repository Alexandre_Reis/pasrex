﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.CRM;
using Financial.Security;
using Financial.Fundo;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Web.Common;
using EntitySpaces.Interfaces;

public partial class CadastrosBasicos_TabelaFundoCategoria : CadastroBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        
        this.AllowUpdate = false; // Não permite Update
        //
        this.HasPopupCarteira = true;   // PopUp Carteira
        //
        base.Page_Load(sender, e);
    }

    #region DataSources

    /// <summary>
    /// Consulta Principal
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSTabelaFundoCategoria_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        TabelaFundoCategoriaQuery tabelaFundoCategoriaQuery = new TabelaFundoCategoriaQuery("T");
        CarteiraQuery carteiraOrigemQuery = new CarteiraQuery("C");
        ListaCategoriaFundoQuery listaCategoriaFundoQuery = new ListaCategoriaFundoQuery("L");
        CategoriaFundoQuery categoriaFundoQuery = new CategoriaFundoQuery("C1");
        
        //
        tabelaFundoCategoriaQuery.Select(tabelaFundoCategoriaQuery, 
                                           ( carteiraOrigemQuery.IdCarteira.Cast(esCastType.String) + " - " + carteiraOrigemQuery.Apelido ).As("Apelido"),
                                           ( listaCategoriaFundoQuery.IdLista.Cast(esCastType.String) + " - " + listaCategoriaFundoQuery.Descricao).As("DescricaoLista"),
                                           ( categoriaFundoQuery.IdCategoria.Cast(esCastType.String) + " - " + categoriaFundoQuery.Descricao).As("DescricaoCategoria")
                                           );
        //
        tabelaFundoCategoriaQuery.InnerJoin(carteiraOrigemQuery).On(tabelaFundoCategoriaQuery.IdCarteira == carteiraOrigemQuery.IdCarteira);
        tabelaFundoCategoriaQuery.InnerJoin(listaCategoriaFundoQuery).On(tabelaFundoCategoriaQuery.IdLista == listaCategoriaFundoQuery.IdLista);
        tabelaFundoCategoriaQuery.InnerJoin(categoriaFundoQuery).On(tabelaFundoCategoriaQuery.IdCategoriaFundo == categoriaFundoQuery.IdCategoria);

        //
        tabelaFundoCategoriaQuery.OrderBy(tabelaFundoCategoriaQuery.IdCarteira.Ascending);
        //

        TabelaFundoCategoriaCollection coll = new TabelaFundoCategoriaCollection();
        coll.Load(tabelaFundoCategoriaQuery);
        //
        e.Collection = coll;
    }

    /// <summary>
    /// Popup Carteira
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CarteiraCollection coll = new CarteiraCollection();
        coll.BuscaCarteirasFundosClubes();

        e.Collection = coll;
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSListaCategoriaFundo_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {                
        ListaCategoriaFundoCollection coll = new ListaCategoriaFundoCollection();
        coll.LoadAll();
        //
        e.Collection = coll;
    }

    /// <summary>
    /// Realiza o Databind do combo Categoria Fundo
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dropCategoriaFundo_Callback(object source, CallbackEventArgsBase e) {
        ((ASPxComboBox)source).DataBind();        
    }
    
    /// <summary>
    /// Baseado no Parametro de Lista seleciona os dados de Categoria
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSCategoriaFundo_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ASPxComboBox dropLista = gridCadastro.FindEditFormTemplateControl("dropLista") as ASPxComboBox;
        
        CategoriaFundoCollection coll = new CategoriaFundoCollection();
        
        if(dropLista.SelectedIndex != -1) {
            coll.Query.Where(coll.Query.IdLista == Convert.ToInt32(dropLista.SelectedItem.Value));
            coll.Query.Load();
        }

        e.Collection = coll;
    }

    #endregion

    #region Popup Carteira
    /// <summary>
    /// Pop Up Carteira
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback1_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        string nome = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idCarteira = Convert.ToInt32(e.Parameter);
            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteira);

            if (carteira.str.Apelido != "") {
                if (carteira.IsAtivo) {
                    PermissaoCliente permissaoCliente = new PermissaoCliente();
                    nome = permissaoCliente.RetornaAcessoCliente(idCarteira, HttpContext.Current.User.Identity.Name)
                           ? carteira.str.Apelido : "no_access";
                }
                else {
                    nome = "no_active";
                }
            }
        }

        e.Result = nome;
    }

    #endregion

    /// <summary>
    /// Tratamento Erro
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";

        ASPxSpinEdit btnEditCodigoCarteiraOrigem = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteiraOrigem") as ASPxSpinEdit;
        ASPxComboBox dropLista = gridCadastro.FindEditFormTemplateControl("dropLista") as ASPxComboBox;
        ASPxComboBox dropCategoriaFundo = gridCadastro.FindEditFormTemplateControl("dropCategoriaFundo") as ASPxComboBox;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(btnEditCodigoCarteiraOrigem);
        controles.Add(dropLista);

        // Select Index do combo CategoriaFundo não é afetado devido ao callback. Dessa maneira deve-se comparar o valor separadamente
        if (base.TestaObrigatorio(controles) != "" || String.IsNullOrEmpty(dropCategoriaFundo.Text)) {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion
       
        int idCarteira = Convert.ToInt32(btnEditCodigoCarteiraOrigem.Text);
        int idLista = Convert.ToInt32(dropLista.SelectedItem.Value);

        TabelaFundoCategoria tabelaFundoCategoria = new TabelaFundoCategoria();
        if (tabelaFundoCategoria.LoadByPrimaryKey(idCarteira, idLista)) {
            e.Result = "Registro já existente.";
        }
    }

    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();

        e.Result = "";
        if (e.Parameter == "clone")
        {
            //e.Result = 0;
        }
        else
        {
            e.Result = "Operação feita com sucesso.";
        }
    }

    private void SalvarNovo()
    {
        ASPxSpinEdit btnEditCodigoCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCodigoCarteiraOrigem") as ASPxSpinEdit;
        ASPxComboBox dropLista = gridCadastro.FindEditFormTemplateControl("dropLista") as ASPxComboBox;
        ASPxComboBox dropCategoriaFundo = gridCadastro.FindEditFormTemplateControl("dropCategoriaFundo") as ASPxComboBox;
        //
        TabelaFundoCategoria tabelaFundoCategoria = new TabelaFundoCategoria();
        //
        tabelaFundoCategoria.IdCarteira = Convert.ToInt32(btnEditCodigoCarteira.Text);
        tabelaFundoCategoria.IdLista = Convert.ToInt32(dropLista.SelectedItem.Value);
        //tabelaFundoCategoria.IdCategoriaFundo = Convert.ToInt32(dropCategoriaFundo.SelectedItem.Value);

        // Propriedade ClientValue por causa do callback
        tabelaFundoCategoria.IdCategoriaFundo = Convert.ToInt32(dropCategoriaFundo.ClientValue);
        //
        tabelaFundoCategoria.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de TabelaFundoCategoria - Operacao: Insert TabelaFundoCategoria: " + tabelaFundoCategoria.IdCarteira.Value + "; " + tabelaFundoCategoria.IdLista.Value + "; " + UtilitarioWeb.ToString(tabelaFundoCategoria),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// Define Chave Primária
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) {
        if (e.Column.FieldName == "CompositeKey") {
            //Compose a primary key value
            string idCarteira = Convert.ToString(e.GetListSourceFieldValue(TabelaFundoCategoriaMetadata.ColumnNames.IdCarteira));
            string idLista = Convert.ToString(e.GetListSourceFieldValue(TabelaFundoCategoriaMetadata.ColumnNames.IdLista));
            //
            e.Value = idCarteira + idLista;
        }
    }

    /// <summary>
    /// Inserção
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// Tratamento Delete
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            #region Delete
            List<object> keyValuesIdCarteira = gridCadastro.GetSelectedFieldValues(TabelaFundoCategoriaMetadata.ColumnNames.IdCarteira);
            List<object> keyValuesIdLista = gridCadastro.GetSelectedFieldValues(TabelaFundoCategoriaMetadata.ColumnNames.IdLista);

            for (int i = 0; i < keyValuesIdCarteira.Count; i++) {
                int idCarteira = Convert.ToInt32(keyValuesIdCarteira[i]);
                int idLista = Convert.ToInt32(keyValuesIdLista[i]);

                TabelaFundoCategoria tabelaFundoCategoria = new TabelaFundoCategoria();
                if (tabelaFundoCategoria.LoadByPrimaryKey(idCarteira, idLista)) {
                    TabelaFundoCategoria tabelaFundoCategoriaClone = (TabelaFundoCategoria)Utilitario.Clone(tabelaFundoCategoria);
                    //

                    tabelaFundoCategoria.MarkAsDeleted();
                    tabelaFundoCategoria.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de TabelaFundoCategoria - Operacao: Delete TabelaFundoCategoria: " + idCarteira + "; " + idLista + "; " + UtilitarioWeb.ToString(tabelaFundoCategoriaClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
            #endregion
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        //this.FocaCampoGrid("btnEditCodigoCarteiraOrigem");
        base.gridCadastro_PreRender(sender, e);        
    }
}