﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using Financial.Common.Enums;
using System.Collections.Generic;
using Financial.Util;
using Financial.Util.Enums;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.CRM;
using Financial.Security;
using Financial.Fundo;
using Financial.Fundo.Enums;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Web.Common;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.InvestidorCotista;
using Financial.BMF;
using Financial.BMF.Enums;
using Financial.Bolsa;
using Financial.Bolsa.Enums;
using Financial.RendaFixa;
using Financial.RendaFixa.Enums;
using Financial.Swap;
using DevExpress.Web.Data;
using Financial.ContaCorrente;

public partial class CadastrosBasicos_IncorporacaoFusaoCisaoFundoMovimento : CadastroBasePage
{
    #region DataSources
    
    /// <summary>
    /// Consulta Principal
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSIncorporacaoFundo_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        EventoFundoQuery eventoFundoQuery = new EventoFundoQuery("EVQ");
        CarteiraQuery carteiraOrigemQuery = new CarteiraQuery("C1");
        CarteiraQuery carteiraDestinoQuery = new CarteiraQuery("C2");
        //
        eventoFundoQuery.Select(eventoFundoQuery,
                                carteiraOrigemQuery.IdCarteira,
                                carteiraOrigemQuery.Apelido.As("Apelido"),
                                carteiraDestinoQuery.IdCarteira,
                                carteiraDestinoQuery.Apelido.As("Apelido1"));
        //
        eventoFundoQuery.InnerJoin(carteiraOrigemQuery).On(eventoFundoQuery.IdCarteiraOrigem.Equal(carteiraOrigemQuery.IdCarteira));
        eventoFundoQuery.InnerJoin(carteiraDestinoQuery).On(eventoFundoQuery.IdCarteiraDestino.Equal(carteiraDestinoQuery.IdCarteira));

        //
        eventoFundoQuery.OrderBy(eventoFundoQuery.IdCarteiraOrigem.Ascending);
        //

        EventoFundoCollection coll = new EventoFundoCollection();
        coll.Load(eventoFundoQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    #endregion

    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// Tratamento Erro
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) 
    {
        e.Result = "";

        //ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;
        //ASPxGridView gridCautela = pageControl.FindControl("ASPxGridView1") as ASPxGridView;

        //List<object> keyValuesCautela = gridCautela.GetSelectedFieldValues("CompositeKeyCautelas");

        //if (keyValuesCautela.Count == 0)
        //    e.Result = "Favor selecionar pelo menu 1 cautela.";

        //ASPxGridView gridAtivos = pageControl.FindControl("ASPxGridView2") as ASPxGridView;

        //List<object> keyValuesAtivo = gridAtivos.GetSelectedFieldValues("CompositeKeyAtivos");

        //if (keyValuesAtivo.Count == 0)
        //    e.Result = "Favor selecionar pelo menu 1 ativo.";

    }

    /// <summary>
    /// Insere Registro
    /// </summary>
    private void Salvar() {
        ASPxTextBox hiddenIdCarteiraOrigem = gridCadastro.FindEditFormTemplateControl("hiddenIdCarteiraOrigem") as ASPxTextBox;
        ASPxTextBox hiddenIdCarteiraDestino = gridCadastro.FindEditFormTemplateControl("hiddenIdCarteiraDestino") as ASPxTextBox;
        ASPxDateEdit textData = gridCadastro.FindEditFormTemplateControl("textData") as ASPxDateEdit;
        ASPxSpinEdit textPercentual = gridCadastro.FindEditFormTemplateControl("textPercentual") as ASPxSpinEdit;
        //ASPxComboBox dropMetodo = gridCadastro.FindEditFormTemplateControl("dropMetodo") as ASPxComboBox;
        ASPxComboBox dropEvento = gridCadastro.FindEditFormTemplateControl("dropEvento") as ASPxComboBox;

        EventoFundo eventoFundo = new EventoFundo();

        eventoFundo.IdCarteiraOrigem = Convert.ToInt32(hiddenIdCarteiraOrigem.Value);
        eventoFundo.IdCarteiraDestino = Convert.ToInt32(hiddenIdCarteiraDestino.Value);
        eventoFundo.TipoEvento = Convert.ToInt32(dropEvento.SelectedItem.Value);
        //eventoFundo.TipoMetodo = Convert.ToInt32(dropMetodo.SelectedItem.Value);
        eventoFundo.DataPosicao = Convert.ToDateTime(textData.Text);

        if (textPercentual.Text != "")
        {
            eventoFundo.Percentual = Convert.ToDecimal(textPercentual.Text);
        }
        else
        {
            eventoFundo.Percentual = 0;
        }

        eventoFundo.Save();

      
        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de Incorporação/Fusão/Cisão Fundo - Operacao: Insert EventoFundo: " + eventoFundo.IdEventoFundo.Value + "; " + UtilitarioWeb.ToString(eventoFundo),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

  
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditCarteira_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
        {
            (sender as ASPxButtonEdit).Enabled = false;
            (sender as ASPxButtonEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "CarteiraOrigem")
        {
            string idCarteira = Convert.ToString(e.GetListSourceFieldValue("IdCarteira"));
            string apelido = Convert.ToString(e.GetListSourceFieldValue("Apelido"));
            e.Value = idCarteira + " - " + apelido;
        }

        if (e.Column.FieldName == "CarteiraDestino")
        {
            string idCarteira = Convert.ToString(e.GetListSourceFieldValue("IdCarteira1"));
            string apelido = Convert.ToString(e.GetListSourceFieldValue("Apelido1"));
            e.Value = idCarteira + " - " + apelido;
        }

    }


    protected void gridCautelas_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "CompositeKeyCautelas")
        {
            string idEventoFundo = Convert.ToString(e.GetListSourceFieldValue("IdEventoFundo"));
            string idCotista = Convert.ToString(e.GetListSourceFieldValue("IdCotista"));
            string dataAplicacao = Convert.ToString(e.GetListSourceFieldValue("DataAplicacao"));
            string idOperacao = Convert.ToString(e.GetListSourceFieldValue("OperacaoAjustada"));
            e.Value = idEventoFundo + "#" + idCotista + "#" + dataAplicacao + "#" + idOperacao;
        }
        if (e.Column.FieldName == "PercentualCisao")
        {
            decimal novoValor = Convert.ToDecimal(e.GetListSourceFieldValue("NovoValor"));
            decimal valorBruto = Convert.ToDecimal(e.GetListSourceFieldValue("ValorBruto"));
            if (valorBruto == 0)
                e.Value = 100.00;
            else
                e.Value = (novoValor * 100 / valorBruto);

        }
    }

    protected void gridAtivos_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "CompositeKeyAtivos")
        {
            string chave = Convert.ToString(e.GetListSourceFieldValue("Chave"));
            e.Value = chave;
        }
        if (e.Column.FieldName == "DescricaoMercado")
        {
            string descricaoTipoMercado = "";
            int tipoMercado = Convert.ToInt32(e.GetListSourceFieldValue("TipoMercado"));
            switch (tipoMercado)
            {
                case 1:
                    descricaoTipoMercado = "Bolsa";
                    break;
                case 2:
                    descricaoTipoMercado = "BMF";
                    break;
                case 3:
                    descricaoTipoMercado = "Renda Fixa";
                    break;
                case 4:
                    descricaoTipoMercado = "Swap";
                    break;
                case 5:
                    descricaoTipoMercado = "Fundos";
                    break;
                case 6:
                    descricaoTipoMercado = "Liquidação";
                    break;
                case 7:
                    descricaoTipoMercado = "Caixa";
                    break;
            }
            e.Value = descricaoTipoMercado;
        }
        if(e.Column.FieldName == "PercentualCisaoAtivos")
        {
            decimal PercentualCisao = Convert.ToDecimal(e.GetListSourceFieldValue("PercentualCisao"));
            int tipoMercado = Convert.ToInt32(e.GetListSourceFieldValue("TipoMercado"));
            decimal novoValor = Convert.ToInt32(e.GetListSourceFieldValue("NovoValor"));
            switch (tipoMercado)
            {
                case 1:
                    break;
                case 2:
                    break;
                case 3:
                    break;
                case 4:
                    break;
                case 5:
                    break;
                case 6:
                    break;
                case 7:
                    PercentualCisao = (novoValor / PercentualCisao) * 100;
                    break;
            }
            e.Value = PercentualCisao;
        }
    }

    /// <summary>
    /// Consulta Principal
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSCautelas_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {

        ASPxTextBox hiddenIdEventoFundo = gridCadastro.FindEditFormTemplateControl("hiddenIdEventoFundo") as ASPxTextBox;
        int idEventoFundo = 0;
        if (hiddenIdEventoFundo.Text == null)
        {
            idEventoFundo = 0;
        }
        else
        {
            idEventoFundo = Convert.ToInt32(hiddenIdEventoFundo.Value);
        }

        EventoFundoQuery eventoFundoQuery = new EventoFundoQuery("EF");
        EventoFundoCautelaQuery eventoFundoCautelaQuery = new EventoFundoCautelaQuery("EFC");
        CotistaQuery cotistaQuery = new CotistaQuery("C");
        PosicaoCotistaHistoricoQuery posicaoCotistaHistoricoQuery = new PosicaoCotistaHistoricoQuery("PCH");
        //
        eventoFundoQuery.Select(eventoFundoQuery,
                                eventoFundoCautelaQuery.Quantidade.As("QtdEvento"),
                                eventoFundoCautelaQuery.IdOperacao.As("OperacaoAjustada"),
                                cotistaQuery.Apelido,
                                posicaoCotistaHistoricoQuery,
                                (posicaoCotistaHistoricoQuery.CotaDia * eventoFundoCautelaQuery.Quantidade).As("NovoValor"),
                                posicaoCotistaHistoricoQuery.ValorBruto,
                                eventoFundoCautelaQuery.Processar);
        //
        eventoFundoQuery.InnerJoin(eventoFundoCautelaQuery).On(eventoFundoQuery.IdEventoFundo.Equal(eventoFundoCautelaQuery.IdEventoFundo));
        eventoFundoQuery.InnerJoin(posicaoCotistaHistoricoQuery).On(eventoFundoQuery.DataPosicao.Equal(posicaoCotistaHistoricoQuery.DataHistorico) &
                                                                    eventoFundoCautelaQuery.IdCotista.Equal(posicaoCotistaHistoricoQuery.IdCotista) &
                                                                    eventoFundoCautelaQuery.DataAplicacao.Equal(posicaoCotistaHistoricoQuery.DataAplicacao) &
                                                                    eventoFundoCautelaQuery.IdOperacao.Equal(posicaoCotistaHistoricoQuery.IdOperacao.Coalesce("0")) &
                                                                    posicaoCotistaHistoricoQuery.IdCarteira.Equal(eventoFundoQuery.IdCarteiraOrigem));
        eventoFundoQuery.InnerJoin(cotistaQuery).On(posicaoCotistaHistoricoQuery.IdCotista.Equal(cotistaQuery.IdCotista));
        //
        eventoFundoQuery.Where(eventoFundoQuery.IdEventoFundo.Equal(idEventoFundo));
        //
        EventoFundoCollection coll = new EventoFundoCollection();
        coll.Load(eventoFundoQuery);

        e.Collection = coll;

    }

    /// <summary>
    /// Consulta Principal
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSAtivos_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {

        ASPxTextBox hiddenIdEventoFundo = gridCadastro.FindEditFormTemplateControl("hiddenIdEventoFundo") as ASPxTextBox;
        int idEventoFundo = 0;
        if (hiddenIdEventoFundo.Text == null)
        {
            idEventoFundo = 0;
        }
        else
        {
            idEventoFundo = Convert.ToInt32(hiddenIdEventoFundo.Value);
        }

        EventoFundoCollection coll = new EventoFundoCollection();


        coll = this.retornaAtivosFundos(idEventoFundo);
        coll.Combine(this.retornaAtivosBolsa(idEventoFundo));
        coll.Combine(this.retornaAtivosRendaFixa(idEventoFundo));
        coll.Combine(this.retornaAtivosSwap(idEventoFundo));
        coll.Combine(this.retornaAtivosBMF(idEventoFundo));
        coll.Combine(this.retornaLiquidacao(idEventoFundo));
        coll.Combine(this.retornaCaixa(idEventoFundo));

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    private EventoFundoCollection retornaAtivosBMF(int idEventoFundo)
    {

        EventoFundoCollection coll = new EventoFundoCollection();
        //
        EventoFundoQuery eventoFundoQuery = new EventoFundoQuery("EF");
        EventoFundoBMFQuery eventoFundoBMFQuery = new EventoFundoBMFQuery("EFB");
        PosicaoBMFHistoricoQuery posicaoBMFHistoricoQuery = new PosicaoBMFHistoricoQuery("PBH");
        //


        eventoFundoQuery.Select(eventoFundoQuery,
                                eventoFundoBMFQuery.Quantidade.As("QtdEvento"),
                                eventoFundoBMFQuery.TipoMercado,
                                (eventoFundoQuery.IdEventoFundo.Cast(esCastType.String) + "#" +
                                 eventoFundoBMFQuery.TipoMercado.Cast(esCastType.String) + "#" + 
                                 eventoFundoBMFQuery.CdAtivoBMF + "#" +
                                 eventoFundoBMFQuery.Serie + "#" +
                                 eventoFundoBMFQuery.IdAgente.Cast(esCastType.String)).As("Chave"),
                                (posicaoBMFHistoricoQuery.CdAtivoBMF + " - " + posicaoBMFHistoricoQuery.Serie).As("Ativo"),
                                eventoFundoBMFQuery.ValorLiquido.As("ValorLiquido"),
                                posicaoBMFHistoricoQuery.Quantidade.As("QtdAtivo"),
                                eventoFundoBMFQuery.ValorLiquido.As("NovoValor"),
                                eventoFundoBMFQuery.ValorLiquido.As("PercentualCisao"),
                                eventoFundoBMFQuery.Processar);
                            
        //
        eventoFundoQuery.InnerJoin(eventoFundoBMFQuery).On(eventoFundoQuery.IdEventoFundo.Equal(eventoFundoBMFQuery.IdEventoFundo));
        eventoFundoQuery.InnerJoin(posicaoBMFHistoricoQuery).On(eventoFundoQuery.DataPosicao.Equal(posicaoBMFHistoricoQuery.DataHistorico) &
                                                                eventoFundoBMFQuery.CdAtivoBMF.Equal(posicaoBMFHistoricoQuery.CdAtivoBMF) &
                                                                eventoFundoBMFQuery.Serie.Equal(posicaoBMFHistoricoQuery.Serie) &
                                                                eventoFundoBMFQuery.IdAgente.Equal(posicaoBMFHistoricoQuery.IdAgente) &
                                                                eventoFundoQuery.IdCarteiraOrigem.Equal(posicaoBMFHistoricoQuery.IdCliente));
        //
        eventoFundoQuery.Where(eventoFundoQuery.IdEventoFundo.Equal(idEventoFundo));
        //

        coll.Load(eventoFundoQuery);

        return coll;
    }

    private EventoFundoCollection retornaAtivosBolsa(int idEventoFundo)
    {
        EventoFundoCollection coll = new EventoFundoCollection();
        //
        EventoFundoQuery eventoFundoQuery = new EventoFundoQuery("EF");
        EventoFundoBolsaQuery eventoFundoBolsaQuery = new EventoFundoBolsaQuery("EFB");
        PosicaoBolsaHistoricoQuery posicaoBolsaHistoricoQuery = new PosicaoBolsaHistoricoQuery("PBH");
        //

        eventoFundoQuery.Select(eventoFundoQuery,
                        eventoFundoBolsaQuery.Quantidade.As("QtdEvento"),
                        eventoFundoBolsaQuery.TipoMercado,
                        (eventoFundoQuery.IdEventoFundo.Cast(esCastType.String) + "#" +
                         eventoFundoBolsaQuery.TipoMercado.Cast(esCastType.String) + "#" +
                         eventoFundoBolsaQuery.CdAtivoBolsa + "#" + 
                         eventoFundoBolsaQuery.IdAgente.Cast(esCastType.String)).As("Chave"),
                        posicaoBolsaHistoricoQuery.CdAtivoBolsa.As("Ativo"),
                        posicaoBolsaHistoricoQuery.ValorMercado.As("ValorLiquido"),
                        posicaoBolsaHistoricoQuery.Quantidade.As("QtdAtivo"),
                        (posicaoBolsaHistoricoQuery.PUMercado * eventoFundoBolsaQuery.Quantidade).As("NovoValor"),
                        ((posicaoBolsaHistoricoQuery.PUMercado * eventoFundoBolsaQuery.Quantidade * 100) / posicaoBolsaHistoricoQuery.ValorMercado).As("PercentualCisao"),
                        eventoFundoBolsaQuery.Processar);
        //
        eventoFundoQuery.InnerJoin(eventoFundoBolsaQuery).On(eventoFundoQuery.IdEventoFundo.Equal(eventoFundoBolsaQuery.IdEventoFundo));
        eventoFundoQuery.InnerJoin(posicaoBolsaHistoricoQuery).On(eventoFundoQuery.DataPosicao.Equal(posicaoBolsaHistoricoQuery.DataHistorico) &
                                                                  eventoFundoBolsaQuery.CdAtivoBolsa.Equal(posicaoBolsaHistoricoQuery.CdAtivoBolsa) &
                                                                  eventoFundoBolsaQuery.IdAgente.Equal(posicaoBolsaHistoricoQuery.IdAgente) &
                                                                  eventoFundoQuery.IdCarteiraOrigem.Equal(posicaoBolsaHistoricoQuery.IdCliente));
        //
        eventoFundoQuery.Where(eventoFundoQuery.IdEventoFundo.Equal(idEventoFundo));
        //

        coll.Load(eventoFundoQuery);

        return coll;
    }

    private EventoFundoCollection retornaAtivosRendaFixa(int idEventoFundo)
    {
        EventoFundoCollection coll = new EventoFundoCollection();
        //
        EventoFundoQuery eventoFundoQuery = new EventoFundoQuery("EF");
        EventoFundoRendaFixaQuery eventoFundoRendaFixaQuery = new EventoFundoRendaFixaQuery("EFRF");
        PosicaoRendaFixaHistoricoQuery posicaoRendaFixaHistoricoQuery = new PosicaoRendaFixaHistoricoQuery("PRFH");
        TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("TRF");
        //

        eventoFundoQuery.Select(eventoFundoQuery.IdEventoFundo,
                        eventoFundoRendaFixaQuery.Quantidade.As("QtdEvento"),
                        eventoFundoRendaFixaQuery.TipoMercado,
                        (eventoFundoQuery.IdEventoFundo.Cast(esCastType.String) + "#" +
                         eventoFundoRendaFixaQuery.TipoMercado.Cast(esCastType.String) + "#" +
                         eventoFundoRendaFixaQuery.IdTitulo.Cast(esCastType.String) + "#" +
                         eventoFundoRendaFixaQuery.TipoOperacao.Cast(esCastType.String) + "#" +
                         eventoFundoRendaFixaQuery.DataOperacao.Cast(esCastType.String) + "#" +
                         eventoFundoRendaFixaQuery.DataLiquidacao.Cast(esCastType.String)).As("Chave"),
                        tituloRendaFixaQuery.DescricaoCompleta.As("Ativo"),
                        (posicaoRendaFixaHistoricoQuery.ValorMercado).Sum().As("ValorLiquido"),
                        (posicaoRendaFixaHistoricoQuery.Quantidade).Sum().As("QtdAtivo"),
                        (eventoFundoRendaFixaQuery.Quantidade * posicaoRendaFixaHistoricoQuery.PUMercado).As("NovoValor"),
                        ((eventoFundoRendaFixaQuery.Quantidade * posicaoRendaFixaHistoricoQuery.PUMercado * 100) / (posicaoRendaFixaHistoricoQuery.ValorMercado.Sum())).As("PercentualCisao"),
                        eventoFundoRendaFixaQuery.Processar);
        //
        eventoFundoQuery.InnerJoin(eventoFundoRendaFixaQuery).On(eventoFundoQuery.IdEventoFundo.Equal(eventoFundoRendaFixaQuery.IdEventoFundo));
        eventoFundoQuery.InnerJoin(posicaoRendaFixaHistoricoQuery).On(eventoFundoQuery.DataPosicao.Equal(posicaoRendaFixaHistoricoQuery.DataHistorico) &
                                                                      eventoFundoRendaFixaQuery.IdTitulo.Equal(posicaoRendaFixaHistoricoQuery.IdTitulo) &
                                                                      eventoFundoRendaFixaQuery.TipoOperacao.Equal(posicaoRendaFixaHistoricoQuery.TipoOperacao) &
                                                                      eventoFundoRendaFixaQuery.DataOperacao.Equal(posicaoRendaFixaHistoricoQuery.DataOperacao) &
                                                                      eventoFundoRendaFixaQuery.DataLiquidacao.Equal(posicaoRendaFixaHistoricoQuery.DataLiquidacao) &
                                                                      eventoFundoQuery.IdCarteiraOrigem.Equal(posicaoRendaFixaHistoricoQuery.IdCliente));
        eventoFundoQuery.InnerJoin(tituloRendaFixaQuery).On(posicaoRendaFixaHistoricoQuery.IdTitulo.Equal(tituloRendaFixaQuery.IdTitulo));
        //
        eventoFundoQuery.Where(eventoFundoQuery.IdEventoFundo.Equal(idEventoFundo));

        //

        eventoFundoQuery.GroupBy(eventoFundoQuery.IdEventoFundo,
                                 eventoFundoRendaFixaQuery.Quantidade,
                                 tituloRendaFixaQuery.DescricaoCompleta,
                                 posicaoRendaFixaHistoricoQuery.PUMercado,
                                 eventoFundoRendaFixaQuery.TipoMercado,
                                 eventoFundoRendaFixaQuery.IdTitulo,
                                 eventoFundoRendaFixaQuery.TipoOperacao,
                                 eventoFundoRendaFixaQuery.DataOperacao,
                                 eventoFundoRendaFixaQuery.DataLiquidacao,
                                 eventoFundoRendaFixaQuery.Processar);

        coll.Load(eventoFundoQuery);

        return coll;
    }

    private EventoFundoCollection retornaAtivosSwap(int idEventoFundo)
    {
        EventoFundoCollection coll = new EventoFundoCollection();
        //
        EventoFundoQuery eventoFundoQuery = new EventoFundoQuery("EF");
        EventoFundoSwapQuery eventoFundoSwapQuery = new EventoFundoSwapQuery("EFA");
        PosicaoSwapHistoricoQuery posicaoSwapHistoricoQuery = new PosicaoSwapHistoricoQuery("PSH");
        //

        eventoFundoQuery.Select(eventoFundoQuery,
                        eventoFundoSwapQuery.Quantidade.As("QtdEvento"),
                        eventoFundoSwapQuery.TipoMercado,
                        (eventoFundoSwapQuery.IdEventoFundo.Cast(esCastType.String) + "#" +
                         eventoFundoSwapQuery.TipoMercado.Cast(esCastType.String) + "#" +
                         eventoFundoSwapQuery.NumeroContrato.Cast(esCastType.String) + "#" +
                         eventoFundoSwapQuery.DataEmissao.Cast(esCastType.String) + "#" +
                         eventoFundoSwapQuery.DataVencimento.Cast(esCastType.String) + "#" +
                         eventoFundoSwapQuery.TipoPonta.Cast(esCastType.String) + "#" +
                         eventoFundoSwapQuery.TipoPontaContraParte.Cast(esCastType.String)).As("Chave"),
                        posicaoSwapHistoricoQuery.NumeroContrato.As("Ativo"),
                        posicaoSwapHistoricoQuery.Saldo.As("ValorLiquido"),
                        eventoFundoSwapQuery.QuantidadeOriginal.As("QtdAtivo"),
                        (eventoFundoSwapQuery.Quantidade * posicaoSwapHistoricoQuery.Saldo).As("NovoValor"),
                        ((eventoFundoSwapQuery.Quantidade * posicaoSwapHistoricoQuery.Saldo * 100) / posicaoSwapHistoricoQuery.Saldo).As("PercentualCisao"),
                         eventoFundoSwapQuery.Processar);
        //
        eventoFundoQuery.InnerJoin(eventoFundoSwapQuery).On(eventoFundoQuery.IdEventoFundo.Equal(eventoFundoSwapQuery.IdEventoFundo));
        eventoFundoQuery.InnerJoin(posicaoSwapHistoricoQuery).On(eventoFundoQuery.DataPosicao.Equal(posicaoSwapHistoricoQuery.DataHistorico) &
                                                                 eventoFundoSwapQuery.NumeroContrato.Equal(posicaoSwapHistoricoQuery.NumeroContrato) &
                                                                 eventoFundoSwapQuery.DataEmissao.Equal(posicaoSwapHistoricoQuery.DataEmissao) &
                                                                 eventoFundoSwapQuery.DataVencimento.Equal(posicaoSwapHistoricoQuery.DataVencimento) &
                                                                 eventoFundoSwapQuery.TipoPonta.Equal(posicaoSwapHistoricoQuery.TipoPonta) &
                                                                 eventoFundoSwapQuery.TipoPontaContraParte.Equal(posicaoSwapHistoricoQuery.TipoPontaContraParte) &
                                                                 eventoFundoQuery.IdCarteiraOrigem.Equal(posicaoSwapHistoricoQuery.IdCliente));
        //
        eventoFundoQuery.Where(eventoFundoQuery.IdEventoFundo.Equal(idEventoFundo));
        //

        coll.Load(eventoFundoQuery);
        

        return coll;
    }    

    private EventoFundoCollection retornaAtivosFundos(int idEventoFundo)
    {
        EventoFundoCollection coll = new EventoFundoCollection();
        //
        EventoFundoQuery eventoFundoQuery = new EventoFundoQuery("EF");
        CarteiraQuery carteiraQuery = new CarteiraQuery("C");
        EventoFundoFundoQuery eventoFundoFundoQuery = new EventoFundoFundoQuery("EFF");
        PosicaoFundoHistoricoQuery posicaoFundoHistoricoQuery = new PosicaoFundoHistoricoQuery("PFH");
        //

        eventoFundoQuery.Select(eventoFundoQuery.IdEventoFundo,
                        eventoFundoFundoQuery.Quantidade.As("QtdEvento"),
                        eventoFundoFundoQuery.TipoMercado,
                        (eventoFundoQuery.IdEventoFundo.Cast(esCastType.String) + "#" + 
                         eventoFundoFundoQuery.TipoMercado.Cast(esCastType.String) + "#" + 
                         eventoFundoFundoQuery.IdCarteira.Cast(esCastType.String)).As("Chave"),
                        carteiraQuery.Apelido.As("Ativo"),
                        posicaoFundoHistoricoQuery.ValorLiquido.Sum(),
                        posicaoFundoHistoricoQuery.Quantidade.Sum().As("QtdAtivo"),
                        (posicaoFundoHistoricoQuery.CotaDia * eventoFundoFundoQuery.Quantidade).As("NovoValor"),
                        ((posicaoFundoHistoricoQuery.CotaDia * eventoFundoFundoQuery.Quantidade * 100) / posicaoFundoHistoricoQuery.ValorLiquido.Sum()).As("PercentualCisao"),
                        eventoFundoFundoQuery.Processar);
        //
        eventoFundoQuery.InnerJoin(eventoFundoFundoQuery).On(eventoFundoQuery.IdEventoFundo.Equal(eventoFundoFundoQuery.IdEventoFundo));
        eventoFundoQuery.InnerJoin(posicaoFundoHistoricoQuery).On(eventoFundoQuery.DataPosicao.Equal(posicaoFundoHistoricoQuery.DataHistorico) &
                                                                  eventoFundoFundoQuery.IdCarteira.Equal(posicaoFundoHistoricoQuery.IdCarteira) &
                                                                  eventoFundoQuery.IdCarteiraOrigem.Equal(posicaoFundoHistoricoQuery.IdCliente));
        eventoFundoQuery.InnerJoin(carteiraQuery).On(posicaoFundoHistoricoQuery.IdCarteira.Equal(carteiraQuery.IdCarteira));
        //
        eventoFundoQuery.Where(eventoFundoQuery.IdEventoFundo.Equal(idEventoFundo) &
                               posicaoFundoHistoricoQuery.ValorLiquido != 0);
        //
        eventoFundoQuery.GroupBy(eventoFundoQuery.IdEventoFundo,
                                 eventoFundoFundoQuery.Quantidade,
                                 carteiraQuery.Apelido,
                                 posicaoFundoHistoricoQuery.CotaDia,
                                 eventoFundoFundoQuery.TipoMercado, 
                                 eventoFundoFundoQuery.IdCarteira,
                                 eventoFundoFundoQuery.Processar);
        coll.Load(eventoFundoQuery);

        return coll;
    }

    private EventoFundoCollection retornaLiquidacao(int idEventoFundo)
    {

        EventoFundoCollection coll = new EventoFundoCollection();
        //
        EventoFundoQuery eventoFundoQuery = new EventoFundoQuery("EF");
        EventoFundoLiquidacaoQuery eventoFundoLiquidacaoQuery = new EventoFundoLiquidacaoQuery("EFL");
        //LiquidacaoHistoricoQuery liquidacaoHistoricoQuery = new LiquidacaoHistoricoQuery("LH");
        //

        eventoFundoQuery.Select(eventoFundoQuery,
                        eventoFundoLiquidacaoQuery.Quantidade.As("QtdEvento"),
                        eventoFundoLiquidacaoQuery.TipoMercado,
                        (eventoFundoLiquidacaoQuery.IdEventoFundo.Cast(esCastType.String) + "#" +
                         eventoFundoLiquidacaoQuery.TipoMercado.Cast(esCastType.String) + "#" + 
                         eventoFundoLiquidacaoQuery.DataLancamento.Cast(esCastType.String) + "#" +
                         eventoFundoLiquidacaoQuery.DataVencimento.Cast(esCastType.String) + "#" +
                         eventoFundoLiquidacaoQuery.Descricao).As("Chave"),
                        eventoFundoLiquidacaoQuery.Descricao.As("Ativo"),
                        eventoFundoLiquidacaoQuery.Valor.As("ValorLiquido"),
                        eventoFundoLiquidacaoQuery.QuantidadeConstante.As("QtdAtivo"),
                        eventoFundoLiquidacaoQuery.Quantidade.As("NovoValor"),
                        (eventoFundoLiquidacaoQuery.Quantidade * 100 / eventoFundoLiquidacaoQuery.Valor).As("PercentualCisao"),
                        eventoFundoLiquidacaoQuery.Processar);
        //
        eventoFundoQuery.InnerJoin(eventoFundoLiquidacaoQuery).On(eventoFundoQuery.IdEventoFundo.Equal(eventoFundoLiquidacaoQuery.IdEventoFundo));
        //eventoFundoQuery.InnerJoin(liquidacaoHistoricoQuery).On(eventoFundoQuery.DataPosicao.Equal(liquidacaoHistoricoQuery.DataHistorico) &
        //                                                          eventoFundoLiquidacaoQuery.DataVencimento.Equal(liquidacaoHistoricoQuery.DataVencimento) &
        //                                                          eventoFundoLiquidacaoQuery.DataLancamento.Equal(liquidacaoHistoricoQuery.DataLancamento) &
        //                                                          eventoFundoLiquidacaoQuery.Descricao.Equal(liquidacaoHistoricoQuery.Descricao) &
        //                                                          eventoFundoQuery.IdCarteiraOrigem.Equal(liquidacaoHistoricoQuery.IdCliente));
        //
        eventoFundoQuery.Where(eventoFundoQuery.IdEventoFundo.Equal(idEventoFundo));
        //

        coll.Load(eventoFundoQuery);

        return coll;
    }

    private EventoFundoCollection retornaCaixa(int idEventoFundo)
    {

        EventoFundoCollection coll = new EventoFundoCollection();
        //
        EventoFundoQuery eventoFundoQuery = new EventoFundoQuery("EF");
        EventoFundoCaixaQuery eventoFundoCaixaQuery = new EventoFundoCaixaQuery("EFC");
        SaldoCaixaQuery saldoCaixaQuery = new SaldoCaixaQuery("SC");
        //

        eventoFundoQuery.Select(eventoFundoQuery,
                        eventoFundoCaixaQuery.Valor.As("QtdEvento"),
                        eventoFundoCaixaQuery.TipoMercado,
                        (eventoFundoCaixaQuery.IdEventoFundo.Cast(esCastType.String) + "#" +
                         eventoFundoCaixaQuery.TipoMercado.Cast(esCastType.String) + "#" +
                         eventoFundoCaixaQuery.Data.Cast(esCastType.String) + "#" +
                         eventoFundoCaixaQuery.IdConta.Cast(esCastType.String) + "#" +
                         eventoFundoCaixaQuery.SaldoAbertura.Cast(esCastType.String)).As("Chave"),
                        eventoFundoCaixaQuery.Descricao.As("Ativo"),
                        saldoCaixaQuery.SaldoFechamento.As("ValorLiquido"),
                        eventoFundoCaixaQuery.QuantidadeConstante.As("QtdAtivo"),
                        eventoFundoCaixaQuery.Valor.As("NovoValor"),
                        saldoCaixaQuery.SaldoFechamento.As("PercentualCisao"),
                        eventoFundoCaixaQuery.Processar);
        //
        eventoFundoQuery.InnerJoin(eventoFundoCaixaQuery).On(eventoFundoQuery.IdEventoFundo.Equal(eventoFundoCaixaQuery.IdEventoFundo));
        eventoFundoQuery.InnerJoin(saldoCaixaQuery).On(eventoFundoQuery.DataPosicao.Equal(saldoCaixaQuery.Data) &
                                                                  eventoFundoCaixaQuery.IdConta.Equal(saldoCaixaQuery.IdConta) &
                                                                  eventoFundoCaixaQuery.SaldoAbertura.Equal(saldoCaixaQuery.SaldoAbertura) &
                                                                  eventoFundoQuery.IdCarteiraOrigem.Equal(saldoCaixaQuery.IdCliente));
        //
        eventoFundoQuery.Where(eventoFundoQuery.IdEventoFundo.Equal(idEventoFundo));
        eventoFundoQuery.Where(eventoFundoCaixaQuery.Valor.NotEqual(0));
        //

        coll.Load(eventoFundoQuery);

        return coll;
    }

    protected void gridCautelas_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;

        string[] keys = ((string)e.Keys[0]).Split('#');
        int idEventoFundo = Convert.ToInt32(keys[0]);
        int idCotista = Convert.ToInt32(keys[1]);
        DateTime dataAplicacao = Convert.ToDateTime(keys[2]);
        int idOperacao = Convert.ToInt32(keys[3]);

        decimal newValue = Convert.ToDecimal(e.NewValues[0]);

        EventoFundoCautela eventoFundoCautela = new EventoFundoCautela();
        if (eventoFundoCautela.LoadByPrimaryKey(dataAplicacao, idCotista, idEventoFundo, idOperacao))
        {
            eventoFundoCautela.Quantidade = newValue;
            eventoFundoCautela.Save();
        }

        e.Cancel = true;
        grid.CancelEdit();
        grid.DataBind();

    }

    protected void gridAtivos_RowUpdating(object sender, ASPxDataUpdatingEventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;

        decimal newValue = Convert.ToDecimal(e.NewValues[0]);

        DateTime dataVencimento = new DateTime();
        int idAgente;
        string s = "";

        string[] keys = ((string)e.Keys[0]).Split('#');
        int idEventoFundo = Convert.ToInt32(keys[0]);
        int tipoMercado = Convert.ToInt32(keys[1]);

        switch (tipoMercado)
        {
            case 1:
                string cdAtivoBolsa = keys[2];
                idAgente = Convert.ToInt32(keys[3]);

                EventoFundoBolsa eventoFundoBolsa = new EventoFundoBolsa();
                if (eventoFundoBolsa.LoadByPrimaryKey(cdAtivoBolsa, idAgente, idEventoFundo, tipoMercado))
                {
                    eventoFundoBolsa.Quantidade = newValue;
                    eventoFundoBolsa.Save();
                }
                break;
            case 2:
                string cdAtivoBMF = keys[2];
                string serie = keys[3];
                idAgente = Convert.ToInt32(keys[4]);

                EventoFundoBMF eventoFundoBMF = new EventoFundoBMF();
                if (eventoFundoBMF.LoadByPrimaryKey(cdAtivoBMF, idAgente, idEventoFundo, serie, tipoMercado))
                {
                    eventoFundoBMF.Quantidade = newValue;
                    eventoFundoBMF.Save();
                }
                break;
            case 3:
                int idTitulo = Convert.ToInt32(keys[2]);
                byte tipoOperacao = Convert.ToByte(keys[3]);
                DateTime dataOperacao = Convert.ToDateTime(keys[4]);
                DateTime dataLiquidacao = Convert.ToDateTime(keys[5]);

                EventoFundoRendaFixa eventoFundoRendaFixa = new EventoFundoRendaFixa();
                if (eventoFundoRendaFixa.LoadByPrimaryKey(dataLiquidacao, dataOperacao, idEventoFundo, idTitulo, tipoMercado, tipoOperacao))
                {
                    eventoFundoRendaFixa.Quantidade = newValue;
                    eventoFundoRendaFixa.Save();
                }
                break;
            case 4:
                string numeroContrato = keys[2];
                DateTime dataEmissao = Convert.ToDateTime(keys[3]);
                dataVencimento = Convert.ToDateTime(keys[4]);
                byte tipoPonta = Convert.ToByte(keys[5]);
                byte tipoPontaContraParte = Convert.ToByte(keys[6]);

                if(newValue <= 0.5M) newValue = 0.0M;
                if(newValue > 0.5M) newValue = 1M;

                EventoFundoSwap eventoFundoSwap = new EventoFundoSwap();
                if (eventoFundoSwap.LoadByPrimaryKey(dataEmissao, dataVencimento, idEventoFundo, numeroContrato, tipoMercado, tipoPonta, tipoPontaContraParte))
                {
                    eventoFundoSwap.Quantidade = newValue;
                    eventoFundoSwap.Save();
                }

                break;
            case 5:
                int idCarteira = Convert.ToInt32(keys[2]);

                EventoFundoFundo eventoFundoFundo = new EventoFundoFundo();
                if (eventoFundoFundo.LoadByPrimaryKey(idCarteira, idEventoFundo, tipoMercado))
                {
                    eventoFundoFundo.Quantidade = newValue;
                    eventoFundoFundo.Save();
                }
                break;
            case 6:
                DateTime dataLancamento = Convert.ToDateTime(keys[2]);
                dataVencimento = Convert.ToDateTime(keys[3]);
                string descricao = keys[4];

                EventoFundoLiquidacao eventoFundoLiquidacao = new EventoFundoLiquidacao();
                if (eventoFundoLiquidacao.LoadByPrimaryKey(dataLancamento, dataVencimento, descricao, idEventoFundo, tipoMercado))
                {
                    eventoFundoLiquidacao.Quantidade = newValue;
                    eventoFundoLiquidacao.Save();
                }
                break;
            case 7:
                DateTime data = Convert.ToDateTime(keys[2]);
                int idConta = Convert.ToInt32(keys[3]);
                s = (keys[4]).ToString(System.Globalization.CultureInfo.InvariantCulture);
                decimal saldoAbertura = decimal.Parse(s, System.Globalization.CultureInfo.InvariantCulture);

                EventoFundoCaixa eventoFundoCaixa = new EventoFundoCaixa();
                if (eventoFundoCaixa.LoadByPrimaryKey(data, idConta, idEventoFundo, saldoAbertura, tipoMercado))
                {
                    eventoFundoCaixa.Valor = newValue;
                    eventoFundoCaixa.Save();
                }
                break;
        }

        e.Cancel = true;
        grid.CancelEdit();
        grid.DataBind();

    }

    protected void grid_CustomSummaryCalculate(object sender, DevExpress.Data.CustomSummaryEventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;
        ASPxSummaryItem summary = e.Item as ASPxSummaryItem;
        if (summary.FieldName != "NovoValor")
            return;

        if (e.IsTotalSummary)
        {
            decimal totalValue = 0;
            //foreach (object value in grid.GetSelectedFieldValues("NovoValor"))
            //{
            //    totalValue += Convert.ToDecimal(value);
            //}


            for (int i = 0; i < grid.VisibleRowCount; i++)
            {
                bool rowEnabled = getRowEnabledStatus(i, grid);

                if (rowEnabled)
                    totalValue += Convert.ToDecimal(grid.GetRowValues(i, "NovoValor"));

            }
            e.TotalValue = totalValue;
            e.TotalValueReady = true;
        }
    }
    protected void grid_SelectionChanged(object sender, EventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;
        grid.DataBind();
    }

    protected void grid_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;
        for (int i = 0; i < grid.VisibleRowCount; i++)
        {
            bool rowEnabled = getRowEnabledStatus(i, grid);

            if (rowEnabled && e.Parameters == "true")
                grid.Selection.SelectRow(i);
            else
                grid.Selection.UnselectRow(i);
        }
    }

    private bool getRowEnabledStatus(int VisibleIndex, ASPxGridView grid)
    {
        int processar = Convert.ToInt32(grid.GetRowValues(VisibleIndex, "Processar"));
        bool isChecked = grid.Selection.IsRowSelected(VisibleIndex);


        if (processar.Equals(Convert.ToInt16(StatusProcessamentoCisao.Processar)) & isChecked) return false;
        if (processar.Equals(Convert.ToInt16(StatusProcessamentoCisao.Processado))) return true;
        return (processar.Equals(Convert.ToInt16(StatusProcessamentoCisao.Processar)) || isChecked) ? true : false;
    }

    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        ASPxPageControl pageControl = gridCadastro.FindEditFormTemplateControl("tabCadastro") as ASPxPageControl;
        ASPxGridView gridCautelas = pageControl.FindControl("ASPxGridView1") as ASPxGridView;

        List<object> keyValuesCautelas = gridCautelas.GetSelectedFieldValues("CompositeKeyCautelas");

        for (int i = 0; i < keyValuesCautelas.Count; i++)
        {
            string[] key = Convert.ToString(keyValuesCautelas[i]).Split('#');

            EventoFundoCautela eventoFundoCautela = new EventoFundoCautela();
            if (eventoFundoCautela.LoadByPrimaryKey(Convert.ToDateTime(key[2]), Convert.ToInt32(key[1]), Convert.ToInt16(key[0]), Convert.ToInt32(key[3])))
            {
                if (eventoFundoCautela.Processar == 1) 
                    eventoFundoCautela.Processar = 0;
                else if (eventoFundoCautela.Processar == 0) 
                    eventoFundoCautela.Processar = 1;
                eventoFundoCautela.Save();
            }
        }

        ASPxGridView gridAtivos = pageControl.FindControl("ASPxGridView2") as ASPxGridView;

        List<object> keyValuesAtivos = gridAtivos.GetSelectedFieldValues("CompositeKeyAtivos");

        for (int i = 0; i < keyValuesAtivos.Count; i++)
        {
            DateTime dataVencimento = new DateTime();
            int idAgente;
            string s = "";

            string[] keys = ((string)keyValuesAtivos[i]).Split('#');
            int idEventoFundo = Convert.ToInt32(keys[0]);
            int tipoMercado = Convert.ToInt32(keys[1]);

            switch (tipoMercado)
            {
                case 1:
                    string cdAtivoBolsa = keys[2];
                    idAgente = Convert.ToInt32(keys[3]);

                    EventoFundoBolsa eventoFundoBolsa = new EventoFundoBolsa();
                    if (eventoFundoBolsa.LoadByPrimaryKey(cdAtivoBolsa, idAgente, idEventoFundo, tipoMercado))
                    {
                        if (eventoFundoBolsa.Processar == 0)
                            eventoFundoBolsa.Processar = 1;
                        else if (eventoFundoBolsa.Processar == 1) 
                            eventoFundoBolsa.Processar = 0;
                        eventoFundoBolsa.Save();
                    }
                    break;
                case 2:
                    string cdAtivoBMF = keys[2];
                    string serie = keys[3];
                    idAgente = Convert.ToInt32(keys[4]);

                    EventoFundoBMF eventoFundoBMF = new EventoFundoBMF();
                    if (eventoFundoBMF.LoadByPrimaryKey(cdAtivoBMF, idAgente, idEventoFundo, serie, tipoMercado))
                    {
                        if (eventoFundoBMF.Processar == 0) 
                            eventoFundoBMF.Processar = 1;
                        else if (eventoFundoBMF.Processar == 1) 
                            eventoFundoBMF.Processar = 0;
                        eventoFundoBMF.Save();
                    }
                    break;
                case 3:
                    int idTitulo = Convert.ToInt32(keys[2]);
                    byte tipoOperacao = Convert.ToByte(keys[3]);
                    DateTime dataOperacao = Convert.ToDateTime(keys[4]);
                    DateTime dataLiquidacao = Convert.ToDateTime(keys[5]);

                    EventoFundoRendaFixa eventoFundoRendaFixa = new EventoFundoRendaFixa();
                    if (eventoFundoRendaFixa.LoadByPrimaryKey(dataLiquidacao, dataOperacao, idEventoFundo, idTitulo, tipoMercado, tipoOperacao))
                    {
                        if (eventoFundoRendaFixa.Processar == 0) 
                            eventoFundoRendaFixa.Processar = 1;
                        else if (eventoFundoRendaFixa.Processar == 1) 
                            eventoFundoRendaFixa.Processar = 0;
                        eventoFundoRendaFixa.Save();
                    }
                    break;
                case 4:
                    string numeroContrato = keys[2];
                    DateTime dataEmissao = Convert.ToDateTime(keys[3]);
                    dataVencimento = Convert.ToDateTime(keys[4]);
                    byte tipoPonta = Convert.ToByte(keys[5]);
                    byte tipoPontaContraParte = Convert.ToByte(keys[6]);

                    EventoFundoSwap eventoFundoSwap = new EventoFundoSwap();
                    if (eventoFundoSwap.LoadByPrimaryKey(dataEmissao, dataVencimento, idEventoFundo, numeroContrato, tipoMercado, tipoPonta, tipoPontaContraParte))
                    {
                        if (eventoFundoSwap.Processar == 0) 
                            eventoFundoSwap.Processar = 1;
                        else if (eventoFundoSwap.Processar == 1) 
                            eventoFundoSwap.Processar = 0;
                        eventoFundoSwap.Save();
                    }

                    break;
                case 5:
                    int idCarteira = Convert.ToInt32(keys[2]);

                    EventoFundoFundo eventoFundoFundo = new EventoFundoFundo();
                    if (eventoFundoFundo.LoadByPrimaryKey(idCarteira, idEventoFundo, tipoMercado))
                    {
                        if (eventoFundoFundo.Processar == 0) 
                            eventoFundoFundo.Processar = 1;
                        else if (eventoFundoFundo.Processar == 1) 
                            eventoFundoFundo.Processar = 0;
                        eventoFundoFundo.Save();
                    }
                    break;
                case 6:
                    DateTime dataLancamento = Convert.ToDateTime(keys[2]);
                    dataVencimento = Convert.ToDateTime(keys[3]);
                    string descricao = keys[4];

                    EventoFundoLiquidacao eventoFundoLiquidacao = new EventoFundoLiquidacao();
                    if (eventoFundoLiquidacao.LoadByPrimaryKey(dataLancamento, dataVencimento, descricao, idEventoFundo, tipoMercado))
                    {
                        if (eventoFundoLiquidacao.Processar == 0) 
                            eventoFundoLiquidacao.Processar = 1;
                        else if (eventoFundoLiquidacao.Processar == 1) 
                            eventoFundoLiquidacao.Processar = 0;
                        eventoFundoLiquidacao.Save();
                    }
                    break;
                case 7:
                    DateTime data = Convert.ToDateTime(keys[2]);
                    int idConta = Convert.ToInt32(keys[3]);
                    s = (keys[4]).ToString(System.Globalization.CultureInfo.InvariantCulture);
                    decimal saldoAbertura = decimal.Parse(s, System.Globalization.CultureInfo.InvariantCulture);

                    EventoFundoCaixa eventoFundoCaixa = new EventoFundoCaixa();
                    if (eventoFundoCaixa.LoadByPrimaryKey(data, idConta, idEventoFundo, saldoAbertura, tipoMercado))
                    {
                        if (eventoFundoCaixa.Processar == 0) 
                            eventoFundoCaixa.Processar = 1;
                        else if (eventoFundoCaixa.Processar == 1) 
                            eventoFundoCaixa.Processar = 0;
                        eventoFundoCaixa.Save();
                    }
                    break;
            }
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    protected void gridCautela_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        if (e.DataColumn.Index == 1)
        {

            if (e.CellValue.ToString() == "2")
            {
                e.Cell.BackColor = System.Drawing.Color.LightBlue;
                e.Cell.ForeColor = System.Drawing.Color.Black;
                e.Cell.Text = StatusProcessamentoCisao.Processado.ToString();
            }
            else if (e.CellValue.ToString() == "1")
            {
                e.Cell.BackColor = System.Drawing.Color.LightGreen;
                e.Cell.ForeColor = System.Drawing.Color.Black;
                e.Cell.Text = StatusProcessamentoCisao.Processar.ToString();
            }
            else
            {
                e.Cell.BackColor = System.Drawing.Color.LightYellow;
                e.Cell.ForeColor = System.Drawing.Color.Black;
                e.Cell.Text = StatusProcessamentoCisao.Aguardando.ToString();
            }
        }
    }

}