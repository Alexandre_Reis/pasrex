﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OperacaoFundo.aspx.cs" Inherits="CadastrosBasicos_OperacaoFundo" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
    var popup = true;
    document.onkeydown=onDocumentKeyDown;
    var operacao = '';
            
    function OnGetDataCliente(data) {
        btnEditCodigoCliente.SetValue(data);        
        ASPxCallback1.SendCallback(btnEditCodigoCliente.GetValue());
        popupCliente.HideWindow();
        btnEditCodigoCliente.Focus();
    }    
    function OnGetDataClienteFiltro(data) {
        btnEditCodigoClienteFiltro.SetValue(data);        
        ASPxCallback1.SendCallback(btnEditCodigoClienteFiltro.GetValue());
        popupCliente.HideWindow();
        btnEditCodigoClienteFiltro.Focus();
    }
    function OnGetDataCarteiraFiltro(data) {
        btnEditCodigoCarteiraFiltro.SetValue(data);        
        ASPxCallback3.SendCallback(btnEditCodigoCarteiraFiltro.GetValue());
        popupCarteira.HideWindow();
        btnEditCodigoCarteiraFiltro.Focus();
    }        
    function OnGetDataPosicaoFundo(data) {
        var resultSplit = data.split('|');        
        var hiddenNotaResgatada = document.getElementById('hiddenNotaResgatada');
        hiddenNotaResgatada.value = resultSplit[0];
        popupPosicaoFundo.HideWindow(); 
        var mensagem = resultSplit[1];
        var textObservacao = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textObservacao');
        textObservacao.value = mensagem; 
    }  
    function OnGetDataCarteira(data) {       
        btnEditCodigoCarteira.SetValue(data);        
        ASPxCallback2.SendCallback(btnEditCodigoCarteira.GetValue());
        popupCarteira.HideWindow();
        btnEditCodigoCarteira.Focus();
    }    
    function ClearTextObservacao()
    {
        var textObservacao = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textObservacao');
        textObservacao.value = '';                                                                                            
    }
    
    
    function PreencheDataRegistro(){
        var data = textDataOperacao.GetValue();
        textDataRegistro.SetValue(data);        
    }

    </script>

</head>
<body>
    <form id="form1" runat="server">
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {             
            var resultSplit = e.result.split('|');          
            if (e.result != '')
            {                   
                if(e.result.indexOf('CONFIRM') > -1)
                {
                    var resultSplit = e.result.split('|');
                    if(confirm(resultSplit[1]))
                    {
                        if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new')
                        {             
                            gridCadastro.UpdateEdit();
                        }
                        else
                        {
                            callbackAdd.SendCallback();
                        }    
                    }
                }
                else
                {
                    alert(e.result);             
                }                             
            }
            else
            {
                if (operacao == 'deletar')
                {
                    gridCadastro.PerformCallback('btnDelete');
                }
                else                
                {   
                    if (operacao == 'salvar' || gridCadastro.cp_EditVisibleIndex != 'new')
                    {             
                        gridCadastro.UpdateEdit();                        
                    }
                    else
                    {
                        callbackAdd.SendCallback();
                    }    
                }          
            }
            
            operacao = '';
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            alert('Operação feita com sucesso.');
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="ASPxCallback1" runat="server" OnCallback="ASPxCallback1_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {
            if (gridCadastro.cp_EditVisibleIndex == -1)
            {  
                var textNomeClienteFiltro = document.getElementById('popupFiltro_textNomeClienteFiltro');
                OnCallBackCompleteClienteFiltro(s, e, popupMensagemCliente, btnEditCodigoClienteFiltro, textNomeClienteFiltro);
            }
            else    
            {
                var textNomeCliente = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCliente');
                OnCallBackCompleteCliente(s, e, popupMensagemCliente, btnEditCodigoCliente, textNomeCliente);
            }
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="ASPxCallback2" runat="server" OnCallback="ASPxCallback2_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {
            var resultSplit = e.result.split('|');
            e.result = resultSplit[0];
            var textNomeCarteira = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCarteira');
            OnCallBackCompleteCliente(s, e, popupMensagemCarteira, btnEditCodigoCarteira, textNomeCarteira);

            if (gridCadastro.cp_EditVisibleIndex == 'new') {            
                if ( resultSplit[1] != '' || resultSplit[2] != '' || resultSplit[3] != '') {
                                                                            
                    if (resultSplit[1] != null && resultSplit[1] != '') {                        
                        var newDate = LocalizedData(resultSplit[1], resultSplit[4]);
                        textDataOperacao.SetValue(newDate);                                                                                              
                        textDataRegistro.SetValue(newDate);                                                                                         
                    }                    
                    else {
                        textDataOperacao.SetValue(null);
                        textDataRegistro.SetValue(null);
                    }
                    
                    if (resultSplit[2] != null && resultSplit[2] != '') { 
                        var newDate = LocalizedData(resultSplit[2], resultSplit[4]);
                        textDataConversao.SetValue(newDate);          
                        
                        var newDate1 = LocalizedData(resultSplit[3], resultSplit[4]);
                        textDataLiquidacao.SetValue(newDate1);                        
                    }
                    else {
                        textDataConversao.SetValue(null);
                        textDataLiquidacao.SetValue(null);                                        
                    }
                    
                    if (resultSplit[5] != null && resultSplit[5] != '')
                    {
                        var idLocalNegociacao = resultSplit[5];
                        dropLocalNegociacao.SetValue(idLocalNegociacao); 
                    }
                }
            }
            
            if (dropTipoOperacao.GetSelectedIndex() == 1 ||
                dropTipoOperacao.GetSelectedIndex() == 2 ||
                dropTipoOperacao.GetSelectedIndex() == 3 ||
                dropTipoOperacao.GetSelectedIndex() == 4 ||
                dropTipoOperacao.GetSelectedIndex() == 7)
                {
                    dropFieTabelaIr.SetEnabled(false);
                    dropFieModalidade.SetEnabled(false);
                    dropFieTabelaIr.SetValue(null);
                    dropFieModalidade.SetValue(null);
                }
                else
                {
                    if (resultSplit[6] == 'S')
                    {
                        dropFieTabelaIr.SetEnabled(true);
                        dropFieModalidade.SetEnabled(true);
                    }
                    else
                    {
                        dropFieTabelaIr.SetEnabled(false);
                        dropFieModalidade.SetEnabled(false);
                        dropFieTabelaIr.SetValue(null);
                        dropFieModalidade.SetValue(null);
                    }
                }
                
                if (gridCadastro.cpMultiConta == 'True' ) { dropConta.PerformCallback(); } 
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="ASPxCallback3" runat="server" OnCallback="ASPxCallback3_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) { 
            var resultSplit = e.result.split('|');
            if (gridCadastro.cp_EditVisibleIndex == -1) {                                  
                e.result = resultSplit[0];
                var textNomeCarteiraFiltro = document.getElementById('popupFiltro_textNomeCarteiraFiltro');
                OnCallBackCompleteCliente(s, e, popupMensagemCarteira, btnEditCodigoCarteiraFiltro, textNomeCarteiraFiltro);
            }
            else {                                                                               
                var textNomeCarteira = document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCarteira');
                OnCallBackCompleteCliente(s, e, popupMensagemCarteira, btnEditCodigoCarteira, textNomeCarteira, textData);
            }
        }        
        " />
        </dxcb:ASPxCallback>
        <dxpc:ASPxPopupControl ID="popupPosicaoFundo" runat="server" Width="500px" HeaderText=""
            ContentStyle-VerticalAlign="Top" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
            AllowDragging="True">
            <ContentCollection>
                <dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                    <div>
                        <dxwgv:ASPxGridView ID="gridPosicaoFundo" runat="server" Width="100%" ClientInstanceName="gridPosicaoFundo"
                            AutoGenerateColumns="False" DataSourceID="EsDSPosicaoFundo" KeyFieldName="IdPosicao"
                            OnCustomDataCallback="gridPosicaoFundo_CustomDataCallback" OnCustomCallback="gridPosicaoFundo_CustomCallback"
                            OnHtmlRowCreated="gridPosicaoFundo_HtmlRowCreated">
                            <Columns>
                                <dxwgv:GridViewDataTextColumn FieldName="DataConversao" Caption="Data Conversão"
                                    VisibleIndex="2" Width="10%">
                                    <PropertiesTextEdit EncodeHtml="false" DisplayFormatString="{0:d}" />
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataSpinEditColumn FieldName="Quantidade" VisibleIndex="3" Width="15%"
                                    HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                                    <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}">
                                    </PropertiesSpinEdit>
                                </dxwgv:GridViewDataSpinEditColumn>
                                <dxwgv:GridViewDataSpinEditColumn FieldName="ValorBruto" VisibleIndex="4" Width="15%"
                                    HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                                    <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                    </PropertiesSpinEdit>
                                </dxwgv:GridViewDataSpinEditColumn>
                                <dxwgv:GridViewDataSpinEditColumn FieldName="ValorIR" VisibleIndex="5" Width="15%"
                                    HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                                    <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                    </PropertiesSpinEdit>
                                </dxwgv:GridViewDataSpinEditColumn>
                                <dxwgv:GridViewDataSpinEditColumn FieldName="ValorIOF" VisibleIndex="6" Width="15%"
                                    HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                                    <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                    </PropertiesSpinEdit>
                                </dxwgv:GridViewDataSpinEditColumn>
                                <dxwgv:GridViewDataSpinEditColumn FieldName="ValorLiquido" Caption="Valor Líquido"
                                    VisibleIndex="7" Width="15%" HeaderStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                                    <PropertiesSpinEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                    </PropertiesSpinEdit>
                                </dxwgv:GridViewDataSpinEditColumn>
                            </Columns>
                            <Settings ShowTitlePanel="True" />
                            <SettingsBehavior ColumnResizeMode="Disabled" />
                            <ClientSideEvents RowDblClick="function(s, e) { if (dropTipoResgate.GetSelectedIndex() == 0)
            gridPosicaoFundo.GetValuesOnCustomCallback(e.visibleIndex, OnGetDataPosicaoFundo);}" Init="function(s, e) {
	        e.cancel = true;
	        }" />
                            <SettingsDetail ShowDetailButtons="False" />
                            <Styles AlternatingRow-Enabled="True" Cell-Wrap="False">
                                <Header ImageSpacing="5px" SortingImageSpacing="5px" />
                            </Styles>
                            <Images>
                                <PopupEditFormWindowClose Height="17px" Width="17px" />
                            </Images>
                            <SettingsText EmptyDataRow="0 Registros" Title="Pesquisa Posições de Cliente" />
                        </dxwgv:ASPxGridView>
                    </div>
                </dxpc:PopupControlContentControl>
            </ContentCollection>
            <ClientSideEvents PopUp="function(s, e) {gridPosicaoFundo.PerformCallback(); }" />
        </dxpc:ASPxPopupControl>
        <asp:HiddenField ID="hiddenNotaResgatada" runat="server" />
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Operações de Clientes (Aplicações e Resgates)"></asp:Label>
                            </div>
                            <div id="mainContent">
                                <dxpc:ASPxPopupControl ID="popupFiltro" AllowDragging="true" PopupElementID="popupFiltro"
                                    EnableClientSideAPI="True" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
                                    CloseAction="CloseButton" Width="400" Left="250" Top="70" HeaderText="Filtros adicionais para consulta"
                                    runat="server" HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
                                    <ContentCollection>
                                        <dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                                            <table>
                                                <tr>
                                                    <td class="td_Label_Longo">
                                                        <asp:Label ID="labelClienteFiltro" runat="server" CssClass="labelNormal" Text="Cliente:"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxSpinEdit ID="btnEditCodigoClienteFiltro" runat="server" CssClass="textButtonEdit"
                                                            ClientInstanceName="btnEditCodigoClienteFiltro" EnableClientSideAPI="true" CssFilePath="../../css/forms.css"
                                                            SpinButtons-ShowIncrementButtons="false" MaxLength="10" NumberType="Integer">
                                                            <Buttons>
                                                                <dxe:EditButton>
                                                                </dxe:EditButton>
                                                            </Buttons>
                                                            <ClientSideEvents KeyPress="function(s, e) {document.getElementById('popupFiltro_textNomeClienteFiltro').value = '';} "
                                                                ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" LostFocus="function(s, e) {popupMensagemCliente.HideWindow();
                                     				                ASPxCallback1.SendCallback(btnEditCodigoClienteFiltro.GetValue());
                                                                    }" />
                                                        </dxe:ASPxSpinEdit>
                                                    </td>
                                                    <td colspan="4" width="450">
                                                        <asp:TextBox ID="textNomeClienteFiltro" runat="server" CssClass="textNome" Enabled="false"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="td_Label_Longo">
                                                        <asp:Label ID="labelCarteiraFiltro" runat="server" CssClass="labelNormal" Text="Carteira:" />
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxSpinEdit ID="btnEditCodigoCarteiraFiltro" runat="server" CssClass="textButtonEdit"
                                                            ClientInstanceName="btnEditCodigoCarteiraFiltro" MaxLength="10" NumberType="Integer">
                                                            <Buttons>
                                                                <dxe:EditButton />
                                                            </Buttons>
                                                            <ClientSideEvents KeyPress="function(s, e) {document.getElementById('popupFiltro_textNomeCarteiraFiltro').value = '';} "
                                                                ButtonClick="function(s, e) {popupCarteira.ShowAtElementByID(s.name);}" LostFocus="function(s, e) {popupMensagemCarteira.HideWindow();
                                     				                ASPxCallback3.SendCallback(btnEditCodigoCarteiraFiltro.GetValue());
                                                                    }" />
                                                        </dxe:ASPxSpinEdit>
                                                    </td>
                                                    <td colspan="2" width="450">
                                                        <asp:TextBox ID="textNomeCarteiraFiltro" runat="server" CssClass="textNome" Enabled="false" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="labelDataInicio" runat="server" CssClass="labelNormal" Text="Início:"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio" />
                                                    </td>
                                                    <td colspan="2">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="labelDataFim" runat="server" CssClass="labelNormal" Text="Fim:" /></td>
                                                                <td>
                                                                    <dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <div class="linkButton linkButtonNoBorder" style="margin-top: 20px">
                                                <asp:LinkButton ID="btnOKFilter" runat="server" Font-Overline="false" ForeColor="Black"
                                                    CssClass="btnOK" OnClientClick="popupFiltro.Hide(); gridCadastro.PerformCallback('btnRefresh'); return false;">
                                                    <asp:Literal ID="Literal7" runat="server" Text="Aplicar" /><div>
                                                    </div>
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="btnFilterCancel" runat="server" Font-Overline="false" ForeColor="Black"
                                                    CssClass="btnFilterCancel" OnClientClick="OnClearFilterClick_FiltroDatas(); return false;">
                                                    <asp:Literal ID="Literal1" runat="server" Text="Limpar" /><div>
                                                    </div>
                                                </asp:LinkButton>
                                            </div>
                                        </dxpc:PopupControlContentControl>
                                    </ContentCollection>
                                </dxpc:ASPxPopupControl>
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;">
                                        <asp:Literal ID="Literal3" runat="server" Text="Novo" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) {operacao='deletar'; callbackErro.SendCallback();} return false;">
                                        <asp:Literal ID="Literal4" runat="server" Text="Excluir" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnFilter" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnFilter" OnClientClick="return OnButtonClick_FiltroDatas()">
                                        <asp:Literal ID="Literal5" runat="server" Text="Filtro" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnPdf" OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal6" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnExcel" OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal8" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh"
                                        OnClientClick=" gridCadastro.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal9" runat="server" Text="Atualizar" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnCustomFields" runat="server" Font-Overline="false" CssClass="btnCustomFields" 
                                        OnClientClick="gridCadastro.ShowCustomizationWindow(); return false;">
                                        <asp:Literal ID="Literal11" runat="server" Text="Mais Campos"/><div>
                                        </div>
                                    </asp:LinkButton>    
                                </div>
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true" KeyFieldName="IdOperacao"
                                        DataSourceID="EsDSOperacaoFundo" OnRowUpdating="gridCadastro_RowUpdating" OnRowInserting="gridCadastro_RowInserting"
                                        OnCustomCallback="gridCadastro_CustomCallback" OnPreRender="gridCadastro_PreRender"
                                        OnCustomJSProperties="gridCadastro_CustomJSProperties" OnHtmlRowPrepared="gridCadastro_HtmlRowPrepared"
                                        OnBeforeGetCallbackResult="gridCadastro_PreRender">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                                                <HeaderTemplate>
                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                                </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataColumn FieldName="IdOperacao" Caption="Id Operação" VisibleIndex="1"
                                                Width="7%" ExportWidth="100" CellStyle-HorizontalAlign="Left">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="ApelidoCliente" Caption="Cliente" VisibleIndex="2"
                                                Width="15%" ExportWidth="200">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataColumn FieldName="ApelidoCarteira" Caption="Carteira" VisibleIndex="3"
                                                Width="15%" ExportWidth="200">
                                            </dxwgv:GridViewDataColumn>
                                            <dxwgv:GridViewDataDateColumn FieldName="DataOperacao" Caption="Data" VisibleIndex="4"
                                                Width="10%" />
                                            <dxwgv:GridViewDataComboBoxColumn Caption="Tipo" FieldName="TipoOperacao" VisibleIndex="4"
                                                Width="8%" ExportWidth="110">
                                                <PropertiesComboBox EncodeHtml="false">
                                                    <Items>
                                                        <dxe:ListEditItem Value="1" Text="<div title='Aplicação'>Aplicação</div>" />
                                                        <dxe:ListEditItem Value="2" Text="<div title='Resgate Bruto'>Resgate Bruto</div>" />
                                                        <dxe:ListEditItem Value="3" Text="<div title='Resgate Líquido'>Resgate Líquido</div>" />
                                                        <dxe:ListEditItem Value="4" Text="<div title='Resgate Cotas'>Resgate Cotas</div>" />
                                                        <dxe:ListEditItem Value="5" Text="<div title='Resgate Total'>Resgate Total</div>" />
                                                        <dxe:ListEditItem Value="10" Text="<div title='Aplicação Especial'>Aplic. Especial</div>" />
                                                        <dxe:ListEditItem Value="11" Text="<div title='Aplicação Ações'>Aplic. Ações</div>" />
                                                        <dxe:ListEditItem Value="12" Text="<div title='Resgate Especial'>Resg. Especial</div>" />
                                                        <dxe:ListEditItem Value="20" Text="<div title='Come Cotas'>Come Cotas</div>" />
                                                        <dxe:ListEditItem Value="40" Text="<div title='Ajuste Posição'>Ajuste Posição</div>" />
                                                        <dxe:ListEditItem Value="80" Text="<div title='Amortização'>Amortização</div>" />
                                                        <dxe:ListEditItem Value="82" Text="<div title='Juros'>Juros</div>" />
                                                        <dxe:ListEditItem Value="84" Text="<div title='Amort.+ Juros'>Amort.+ Juros</div>" />
                                                        <dxe:ListEditItem Value="104" Text="<div title='Ingresso em Ativos com Impacto na Quantidade'>Ingresso em Ativos com Impacto na Quantidade</div>" />
                                                        <dxe:ListEditItem Value="105" Text="<div title='Ingresso em Ativos com Impacto na Cota'>Ingresso em Ativos com Impacto na Cota</div>" />
                                                        <dxe:ListEditItem Value="106" Text="<div title='Retirada em Ativos com Impacto na Quantidade'>Retirada em Ativos com Impacto na Quantidade</div>" />
                                                        <dxe:ListEditItem Value="107" Text="<div title='Retirada em Ativos com Impacto na Cota'>Retirada em Ativos com Impacto na Cota</div>" />
                                                        <dxe:ListEditItem Value="110" Text="<div title='Aplicação Cotas'>Aplicação Cotas</div>" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="ValorBruto" Caption="Vl Bruto" VisibleIndex="5"
                                                Width="10%" HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right"
                                                CellStyle-HorizontalAlign="Right">
                                                <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="Quantidade" VisibleIndex="7" Width="10%"
                                                HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right" CellStyle-HorizontalAlign="Right">
                                                <PropertiesTextEdit DisplayFormatString="{0:#,##0.00000000;(#,##0.00000000);0.00000000}">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn FieldName="ValorLiquido" Caption="Vl Líquido" VisibleIndex="8"
                                                Width="10%" HeaderStyle-HorizontalAlign="Right" FooterCellStyle-HorizontalAlign="Right"
                                                CellStyle-HorizontalAlign="Right">
                                                <PropertiesTextEdit DisplayFormatString="{0:#,##0.00;(#,##0.00);0.00}">
                                                </PropertiesTextEdit>
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataComboBoxColumn Caption="Valores Colados" FieldName="ValoresColados"
                                                VisibleIndex="9" Width="10%">
                                                <EditFormSettings Visible="False" />
                                                <PropertiesComboBox>
                                                    <Items>
                                                        <dxe:ListEditItem Value="S" Text="Sim" />
                                                        <dxe:ListEditItem Value="N" Text="Não" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataColumn FieldName="IdCliente" Visible="false" Width="7%"/>
                                            <dxwgv:GridViewDataColumn FieldName="IdCarteira" Visible="false" Width="7%"/>
                                            <dxwgv:GridViewDataColumn FieldName="DataConversao" Visible="false" Caption="Dt Conversão"
                                                Width="10%" />
                                            <dxwgv:GridViewDataColumn FieldName="DataLiquidacao" Visible="false" Caption="Dt Liquidação"
                                                Width="10%" />
                                            <dxwgv:GridViewDataColumn FieldName="TipoResgate" Visible="false" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataColumn FieldName="IdFormaLiquidacao" Visible="false" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataColumn FieldName="IdConta" Visible="false" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataColumn FieldName="Observacao" Visible="false" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataColumn FieldName="IdPosicaoResgatada" Visible="false" ShowInCustomizationForm="false" />
                                            <dxwgv:GridViewDataTextColumn FieldName="IdLocalNegociacao" Visible="false" />
                                        </Columns>
                                        <Templates>
                                            <EditForm>
                                                <asp:Label ID="labelEdicao" runat="server" CssClass="labelInformation" Text=""></asp:Label>
                                                <asp:Panel ID="panelEdicao" runat="server" OnLoad="panelEdicao_Load">
                                                    <div class="editForm">
                                                        <table >
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelCliente" runat="server" CssClass="labelRequired" Text="Cliente:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit ID="btnEditCodigoCliente" runat="server" ClientInstanceName="btnEditCodigoCliente"
                                                                        CssClass="textButtonEdit" Text='<%#Eval("IdCliente")%>' MaxLength="10" NumberType="Integer">
                                                                        <Buttons>
                                                                            <dxe:EditButton>
                                                                            </dxe:EditButton>
                                                                        </Buttons>
                                                                        <ClientSideEvents KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCliente').value = '';} "
                                                                            ButtonClick="function(s, e) {popupCliente.ShowAtElementByID(s.name);}" LostFocus="function(s, e) {OnLostFocus(popupMensagemCliente, ASPxCallback1, btnEditCodigoCliente);}" 
                                                                            ValueChanged="function(s, e) {if (gridCadastro.cpMultiConta == 'True' ) 
                                                                                  {
                                                                                    dropConta.PerformCallback(); } 
                                                                                  }"/>
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                <td colspan="4">
                                                                    <asp:TextBox ID="textNomeCliente" CssClass="textNome" Style="width: 386px;" runat="server"
                                                                        Enabled="false" Text='<%#Eval("ApelidoCliente")%>'></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelCarteira" runat="server" CssClass="labelRequired" Text="Carteira:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit ID="btnEditCodigoCarteira" runat="server" CssClass="textButtonEdit"
                                                                        ClientInstanceName="btnEditCodigoCarteira" Text='<%#Eval("IdCarteira")%>' MaxLength="10"
                                                                        NumberType="Integer">
                                                                        <Buttons>
                                                                            <dxe:EditButton>
                                                                            </dxe:EditButton>
                                                                        </Buttons>
                                                                        <ClientSideEvents KeyPress="function(s, e) {document.getElementById('gridCadastro_DXPEForm_ef' + gridCadastro.cp_EditVisibleIndex + '_textNomeCarteira').value = '';} "
                                                                            ButtonClick="function(s, e) {popupCarteira.ShowAtElementByID(s.name);}" LostFocus="function(s, e) {OnLostFocus(popupMensagemCarteira, ASPxCallback2, btnEditCodigoCarteira);}" />
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                <td colspan="4">
                                                                    <asp:TextBox ID="textNomeCarteira" runat="server" CssClass="textNome" Style="width: 386px;"
                                                                        Enabled="false" Text='<%#Eval("ApelidoCarteira")%>'></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelDataOperacao" runat="server" CssClass="labelNormal" Text="Operação:" />
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxDateEdit ID="textDataOperacao" runat="server" ClientInstanceName="textDataOperacao"
                                                                        Value='<%#Eval("DataOperacao")%>' ClientSideEvents-ValueChanged="function(s, e) { ASPxCallback2.SendCallback(btnEditCodigoCarteira.GetValue()); PreencheDataRegistro(); }" />
                                                                </td>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelDataRegistro" runat="server" CssClass="labelNormal" Text="Registro:" />
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxDateEdit ID="textDataRegistro" runat="server" ClientInstanceName="textDataRegistro"
                                                                        Value='<%#Eval("DataRegistro")%>'/>
                                                                </td>
                                                            </tr>
                                                        
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelTipoOperacao" runat="server" CssClass="labelRequired" Text="Tipo:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxComboBox ID="dropTipoOperacao" OnLoad="dropTipoOperacao_Load" runat="server"
                                                                        ClientInstanceName="dropTipoOperacao" ShowShadow="true" DropDownStyle="DropDownList"
                                                                        CssClass="dropDownListCurto_2" Text='<%#Eval("TipoOperacao")%>'>
                                                                        <ClientSideEvents SelectedIndexChanged="function(s, e) { ASPxCallback2.SendCallback(btnEditCodigoCarteira.GetValue());
                                                                            if (s.GetSelectedIndex() == 0 || s.GetSelectedIndex() == 5 || s.GetSelectedIndex() == 6 || s.GetSelectedIndex() == 8
                                                                                || s.GetSelectedItem().value == 104 || s.GetSelectedItem().value == 105)
                                                                                { dropTipoResgate.SetSelectedIndex(-1); dropTipoResgate.SetEnabled(false); ClearTextObservacao(); }
                                                                            else
                                                                                {dropTipoResgate.SetEnabled(true);}    
                                                                            }" />
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelDepositoRetirada" runat="server" CssClass="labelRequired" Text="Deposito/Retirada"></asp:Label>
                                                                </td>                                                        
                                                                <td>
                                                                    <dxe:ASPxCheckBox ID="checkDepositoRetirada" runat="server" ClientInstanceName="checkDepositoRetirada"
                                                                        Checked='<%#Eval("DepositoRetirada") ?? false%>' OnDataBound="checkDepositoRetirada_OnDataBound"
                                                                        ClientSideEvents-CheckedChanged="BloqueioDataRegistro">
                                                                        <ClientSideEvents CheckedChanged="
                                                                        function(s,e){
                                                                            textDataRegistro.SetEnabled(s.GetValue());
                                                                            dropFormaLiquidacao.SetValue(10);
                                                                            dropFormaLiquidacao.SetEnabled(!s.GetValue());
                                                                        }"/>
                                                                    </dxe:ASPxCheckBox>
                                                                        
                                                                        
                                                                </td> 
                                                            </tr>
                                                            
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelDataConversao" runat="server" CssClass="labelNormal" Text="Conversão:" />
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxDateEdit ID="textDataConversao" runat="server" ClientInstanceName="textDataConversao"
                                                                        Value='<%#Eval("DataConversao")%>' />
                                                                </td>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelDataLiquidacao" runat="server" CssClass="labelNormal" Text="Liquidação:" />
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxDateEdit ID="textDataLiquidacao" runat="server" ClientInstanceName="textDataLiquidacao"
                                                                        Value='<%#Eval("DataLiquidacao")%>' />
                                                                </td>
                                                            </tr>
                                                            
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelFieModalidade" runat="server" CssClass="labelNormal" Text="Fie/Modalidade:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxComboBox ID="dropFieModalidade" runat="server" ClientInstanceName="dropFieModalidade"
                                                                        ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto_2"
                                                                        Text='<%#Eval("FieModalidade")%>'>
                                                                        <Items>
                                                                            <dxe:ListEditItem Value="1" Text="PGBL" />
                                                                            <dxe:ListEditItem Value="2" Text="VGBL" />
                                                                        </Items>
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                                
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelFieTabelaIr" runat="server" CssClass="labelNormal" Text="Fie/Tabela IR:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxComboBox ID="dropFieTabelaIr" runat="server" ClientInstanceName="dropFieTabelaIr"
                                                                        ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto_2"
                                                                        Text='<%#Eval("FieTabelaIr")%>'>
                                                                        <Items>
                                                                            <dxe:ListEditItem Value="1" Text="Progressiva" />
                                                                            <dxe:ListEditItem Value="2" Text="Regressiva" />
                                                                        </Items>
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                            </tr>
                                                            
                                                            
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelTipoResgate" runat="server" CssClass="labelNormal" Text="Resgate por:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxComboBox ID="dropTipoResgate" runat="server" ClientInstanceName="dropTipoResgate"
                                                                        ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto_2"
                                                                        Text='<%#Eval("TipoResgate")%>'>
                                                                        <Items>
                                                                            <dxe:ListEditItem Value="1" Text="Específico" />
                                                                            <dxe:ListEditItem Value="2" Text="FIFO" />
                                                                            <dxe:ListEditItem Value="3" Text="LIFO" />
                                                                            <dxe:ListEditItem Value="4" Text="Menor Imposto" />
                                                                        </Items>
                                                                        <ClientSideEvents SelectedIndexChanged="function(s, e) { if (s.GetSelectedIndex() != 0)
                                                                                         { ClearTextObservacao(); }                                                                                         
                                                                                      }" />
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                                <td class="td_Label">
                                                                    <div class="linkButton linkButtonNoBorder">
                                                                        <asp:LinkButton ID="btnPosicaoFundo" runat="server" CssClass="btnSearch" OnClientClick="popupPosicaoFundo.ShowAtElementByID(); return false;">
                                                                            <asp:Literal ID="Literal2" runat="server" Text="Aplicações" /><div>
                                                                            </div>
                                                                        </asp:LinkButton>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelValor" runat="server" CssClass="labelNormal" Text="Valor/Qtde:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxSpinEdit ID="textValor" runat="server" CssClass="textValor_5" ClientInstanceName="textValor"
                                                                        MaxLength="28" NumberType="Float" DisplayFormatString="N" >
                                                                    </dxe:ASPxSpinEdit>
                                                                </td>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelFormaLiquidacao" runat="server" CssClass="labelRequired" Text="Liquida por:"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxComboBox ID="dropFormaLiquidacao" runat="server" ClientInstanceName="dropFormaLiquidacao"
                                                                        DataSourceID="EsDSFormaLiquidacao" ShowShadow="false" DropDownStyle="DropDownList"
                                                                        CssClass="dropDownListCurto_5" TextField="Descricao" ValueField="IdFormaLiquidacao"
                                                                        Text='<%# Eval("IdFormaLiquidacao") %>'>
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelConta" runat="server" CssClass="labelNormal" Text="Conta:"></asp:Label>
                                                                </td>
                                                                <td colspan="3">
                                                                    <dxe:ASPxComboBox ID="dropConta" runat="server" ClientInstanceName="dropConta" DataSourceID="EsDSContaCorrente"
                                                                        IncrementalFilteringMode="Contains" ShowShadow="false" DropDownStyle="DropDown"
                                                                        CssClass="dropDownList" TextField="Numero" ValueField="IdConta" OnCallback="dropConta_Callback"
                                                                        Text='<%#Eval("IdConta")%>'>
                                                                        <ClientSideEvents LostFocus="function(s, e) { 
								                                                                    if(s.GetSelectedIndex() == -1)
									                                                                    s.SetText(null); } " />
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="lblLocalNegociacao" runat="server" CssClass="labelRequired" AssociatedControlID="dropLocalNegociacao"
                                                                        Text="Local Negociação:">
                                                                    </asp:Label>
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxComboBox ID="dropLocalNegociacao" runat="server" DataSourceID="EsDSLocalNegociacao"
                                                                        ClientInstanceName="dropLocalNegociacao" ValueField="IdLocalNegociacao" TextField="Descricao"
                                                                        CssClass="dropDownListCurto_2" Text='<%#Eval("IdLocalNegociacao")%>'>
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelTrader" runat="server" CssClass="labelNormal" Text="Trader:"></asp:Label>                        
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxComboBox ID="dropTrader" runat="server" ClientInstanceName="dropTrader"
                                                                                        DataSourceID="EsDSTrader" IncrementalFilteringMode="Contains"  
                                                                                        ShowShadow="false" DropDownStyle="DropDown" 
                                                                                        CssClass="dropDownListCurto_2" TextField="Nome" ValueField="IdTrader"
                                                                                        Text='<%#Eval("IdTrader")%>'>
                                                                      <ClientSideEvents LostFocus="function(s, e) { 
                                                                                                        if(s.GetSelectedIndex() == -1)
                                                                                                            s.SetText(null); } " />
                                                                    </dxe:ASPxComboBox>      
                                                                </td>
                                        <SettingsCommandButton>
                                            <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
                                        </SettingsCommandButton>
                                                            </tr>
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="lblCategoriaMovimentacao" runat="server" CssClass="labelNormal" Text="Categoria Mov:"> </asp:Label>
                                                                </td>                
                                                                <td colspan="3">
                                                                    <dxe:ASPxComboBox ID="dropCategoriaMovimentacao" runat="server" ClientInstanceName="dropCategoriaMovimentacao"
                                                                                        DataSourceID="EsDSCategoriaMovimentacao" IncrementalFilteringMode="Contains"  
                                                                                        ShowShadow="false" DropDownStyle="DropDown"
                                                                                        CssClass="dropDownListLongo" TextField="CodigoCategoria" ValueField="IdCategoriaMovimentacao"
                                                                                        Text='<%#Eval("IdCategoriaMovimentacao")%>'>
                                                                    </dxe:ASPxComboBox>         
                                                                </td>
                                                            </tr>  
                                                            <tr>
                                                                <td class="td_Label">
                                                                    <asp:Label ID="labelObservacao" runat="server" CssClass="labelNormal" Text="Observação:"> </asp:Label>
                                                                </td>
                                                                <td colspan="3">
                                                                    <asp:TextBox ID="textObservacao" runat="server" TextMode="MultiLine" Rows="4" Style="width: 324px;"
                                                                        CssClass="textLongo5" Text='<%#Eval("Observacao")%>' />
                                                                </td>
                                                                <td>
                                                                    <div class="linkButton linkButtonNoBorder" style="display: inline">
                                                                        <asp:LinkButton ID="btnObservacao" runat="server" Font-Overline="false" CssClass="btnOK"
                                                                            Visible="false" OnClientClick="operacao='salvarObservacao'; gridCadastro.PerformCallback('btnObservacao'); return false;">
                                                                            <asp:Literal ID="Literal15" runat="server" Text="Salva Obs." /><div>
                                                                            </div>
                                                                        </asp:LinkButton>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <div class="linhaH">
                                                        </div>
                                                        <div class="linkButton linkButtonNoBorder popupFooter">
                                                            <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd"
                                                                OnInit="btnOKAdd_Init" OnClientClick="if (operacao != '') return false; operacao='salvarAdd'; callbackErro.SendCallback(); return false;">
                                                                <asp:Literal ID="Literal10" runat="server" Text="OK+" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                                OnClientClick="if (operacao != '') return false; operacao='salvar'; callbackErro.SendCallback(); return false;">
                                                                <asp:Literal ID="Literal12" runat="server" Text="OK" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel"
                                                                OnClientClick="gridCadastro.CancelEdit(); return false;">
                                                                <asp:Literal ID="Literal13" runat="server" Text="Cancelar" /><div>
                                                                </div>
                                                            </asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                            </EditForm>
                                            <StatusBar>
                                                <div>
                                                    <div style="float: left">
                                                        <asp:Label ID="labelFiltro" runat="server" CssClass="labelNormal" Text=""></asp:Label>
                                                    </div>
                                                </div>
                                            </StatusBar>
                                        </Templates>
                                        <SettingsPopup EditForm-Width="600px" />
                                        <SettingsBehavior EnableCustomizationWindow ="true" />
                                        <SettingsText CustomizationWindowCaption="Lista de campos" />
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro"
            Landscape="true" LeftMargin="40" RightMargin="40">
        </dxwgv:ASPxGridViewExporter>
        <cc1:esDataSource ID="EsDSOperacaoFundo" runat="server" OnesSelect="EsDSOperacaoFundo_esSelect"
            LowLevelBind="true" />
        <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" />
        <cc1:esDataSource ID="EsDSCarteira" runat="server" OnesSelect="EsDSCarteira_esSelect" />
        <cc1:esDataSource ID="EsDSFormaLiquidacao" runat="server" OnesSelect="EsDSFormaLiquidacao_esSelect" />
        <cc1:esDataSource ID="EsDSContaCorrente" runat="server" OnesSelect="EsDSContaCorrente_esSelect" />
        <cc1:esDataSource ID="EsDSPosicaoFundo" runat="server" OnesSelect="EsDSPosicaoFundo_esSelect" />
        <cc1:esDataSource ID="EsDSLocalNegociacao" runat="server" OnesSelect="EsDSLocalNegociacao_esSelect" />
        <cc1:esDataSource ID="EsDSTrader" runat="server" OnesSelect="EsDSTrader_esSelect" />
        <cc1:esDataSource ID="EsDSCategoriaMovimentacao" runat="server" OnesSelect="EsDSCategoriaMovimentacao_esSelect"/>    
    </form>
</body>
</html>
