﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using Financial.Investidor;
using Financial.ContaCorrente;
using System.Drawing;
using Financial.ContaCorrente.Enums;
using Financial.Security;
using Financial.Fundo;
using Financial.Bolsa;
using Financial.BMF;
using Financial.RendaFixa;
using Financial.Investidor.Enums;
using Financial.InvestidorCotista;
using Financial.CRM;
using Financial.WebConfigConfiguration;
using Financial.Security.Enums;
using System.Collections.Specialized;
using EntitySpaces.Interfaces;
using Financial.Web.Util;
using Financial.InvestidorCotista.Enums;
using Financial.Web.Common;
using System.Web.Configuration;
using System.Xml;
using Financial.Export;
using Financial.Interfaces.Export;
using DevExpress.Web.Data;
using System.ComponentModel;
using Financial.Fundo.Enums;
using DevExpress.XtraGrid;

public partial class CadastrosBasicos_ConsultaEventosCorporativos : CadastroBasePage
{
    public DataTable SessionPesquisa
    {
        get
        {
            if (Session["SessionPesquisa"] == null)
                Session["SessionPesquisa"] = new DataTable();

            return (DataTable)Session["SessionPesquisa"];
        }
        set
        {
            Session["SessionPesquisa"] = value;
        }
    }

    #region Instância
    //Collection
    AgenteMercadoCollection agenteMercadoCollection = new AgenteMercadoCollection();
    FundoInvestimentoFormaCondominioCollection formaCondominioCollection = new FundoInvestimentoFormaCondominioCollection();
    CarteiraCollection carteiraCollection = new CarteiraCollection();
    ParametroAdministradorFundoInvestimentoCollection parametroCollection = new ParametroAdministradorFundoInvestimentoCollection();
    EventoFundoCollection eventoFundoCollection = new EventoFundoCollection();
    DesenquadramentoTributarioCollection desenquadramentoTributarioCollection = new DesenquadramentoTributarioCollection();
    //Query
    EventoFundoQuery eventoFundoQuery = new EventoFundoQuery();
    AgenteMercadoQuery agenteMercadoQuery = new AgenteMercadoQuery();
    FundoInvestimentoFormaCondominioQuery fundoInvestimentoFormaCondominioQuery = new FundoInvestimentoFormaCondominioQuery();
    CarteiraQuery carteiraQuery = new CarteiraQuery();
    DesenquadramentoTributarioQuery desenquadramentoTributarioQuery = new DesenquadramentoTributarioQuery();
    PosicaoFundoHistoricoQuery posicaoFundoHistoricoQuery = new PosicaoFundoHistoricoQuery();
    AgendaComeCotasQuery agendaComeCotasQuery = new AgendaComeCotasQuery();
    #endregion

    #region esDataSources
    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        carteiraCollection = new CarteiraCollection();

        carteiraCollection.Query.Select();
        carteiraCollection.Query.OrderBy(carteiraCollection.Query.IdCarteira.Ascending);
        carteiraCollection.Query.Load();

        e.Collection = carteiraCollection;
    }
    protected void EsDSAgenteMercado_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        agenteMercadoCollection = new AgenteMercadoCollection();

        agenteMercadoCollection.Query.Select();
        agenteMercadoCollection.Query.Where(agenteMercadoCollection.Query.FuncaoAdministrador.Equal("S"));
        agenteMercadoCollection.Query.OrderBy(agenteMercadoCollection.Query.IdAgente.Ascending);
        agenteMercadoCollection.Query.Load();

        e.Collection = agenteMercadoCollection;
    }
    #endregion

    #region gridCadastro
    /// <summary>
    /// Efetua o tratamento dos campos
    /// </summary>
    protected void gridCadastro_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        ASPxGridView gridCadastro = (ASPxGridView)sender;
        if (e.Column.FieldName == "ClassificacaoTributariaAnterior" ||
            e.Column.FieldName == "ClassificacaoTributariaAtual")
        {
            if (e.Value != null)
                e.DisplayText = ClassificacaoTributariaDescricao.RetornaStringValue(Convert.ToInt32(e.Value));
        }
        if (e.Column.FieldName == "DataRecolhimento")
        {
            string data = e.Value.ToString();
            if (data.Equals("31/12/9999 23:59:59"))
                e.DisplayText = string.Empty;
        }
        if (e.Column.FieldName == "EventoTransformacaoMudanca")
        {
            int TipoFundoCarteira = 0;
            int TipoFundoCondominio = 0;
            string texto = string.Empty;

            if (gridCadastro.GetRowValues(e.VisibleRowIndex, "TipoFundoCarteira") != DBNull.Value)
            {
                TipoFundoCarteira = Convert.ToInt16(gridCadastro.GetRowValues(e.VisibleRowIndex, "TipoFundoCarteira"));
                TipoFundoCondominio = Convert.ToInt16(gridCadastro.GetRowValues(e.VisibleRowIndex, "TipoFundoCondominio"));
                texto = TipoFundoDescricao.RetornaStringValue(TipoFundoCarteira) + "-" + TipoFundoDescricao.RetornaStringValue(TipoFundoCondominio);
            }
            else if (gridCadastro.GetRowValues(e.VisibleRowIndex, "TipoFundoDesenquadramento") != DBNull.Value)
            {
                //Desenquadramento Tributário
                texto = EventoTransformacaoMudancaClassificacaoDescricao.RetornaStringValue(6);
            }
            else
            {
                texto = EventoTransformacaoMudancaClassificacaoDescricao.RetornaStringValue(Convert.ToInt16(e.Value));
            }

            //Se os valores não forem do mesmo tipo(Ex: Aberto-Aberto, Fechado-Fechado)
            e.DisplayText = texto;
        }
    }

    /// <summary>
    /// Carrega os filtros da grid
    /// </summary>
    protected void gridCadastro_OnPreRender(object sender, EventArgs e)
    {
        //Esconde os filtros da grid
        gridCadastro.Settings.ShowFilterRow = false;

        //Instancia colunas da grid, para que elas fiquem invisíveis ao usuário
        GridViewDataComboBoxColumn EventoTransfomacaoMudanca = gridCadastro.Columns["EventoTransformacaoMudanca"] as GridViewDataComboBoxColumn;
        GridViewDataComboBoxColumn Administrador = gridCadastro.Columns["Administrador"] as GridViewDataComboBoxColumn;
        GridViewDataComboBoxColumn Fundo = gridCadastro.Columns["Fundo"] as GridViewDataComboBoxColumn;
        GridViewDataComboBoxColumn FundoDestino = gridCadastro.Columns["FundoDestino"] as GridViewDataComboBoxColumn;
        GridViewDataComboBoxColumn ClassificacaoTributariaAnterior = gridCadastro.Columns["ClassificacaoTributariaAnterior"] as GridViewDataComboBoxColumn;
        GridViewDataComboBoxColumn ClassificacaoTributariaAtual = gridCadastro.Columns["ClassificacaoTributariaAtual"] as GridViewDataComboBoxColumn;
        GridViewDataDateColumn Data = gridCadastro.Columns["Data"] as GridViewDataDateColumn;
        GridViewDataColumn ValorCotaAnterior = gridCadastro.Columns["ValorCotaAnterior"] as GridViewDataColumn;
        GridViewDataColumn ValorCotaAtual = gridCadastro.Columns["ValorCotaAtual"] as GridViewDataColumn;
        GridViewDataColumn TotalIR = gridCadastro.Columns["TotalIR"] as GridViewDataColumn;

        //Esconde os filtros da grid
        EventoTransfomacaoMudanca.Settings.AllowAutoFilter = DevExpress.Utils.DefaultBoolean.False;
        Administrador.Settings.AllowAutoFilter = DevExpress.Utils.DefaultBoolean.False;
        Fundo.Settings.AllowAutoFilter = DevExpress.Utils.DefaultBoolean.False;
        Data.Settings.AllowAutoFilter = DevExpress.Utils.DefaultBoolean.False;
        FundoDestino.Settings.AllowAutoFilter = DevExpress.Utils.DefaultBoolean.False;
        ClassificacaoTributariaAnterior.Settings.AllowAutoFilter = DevExpress.Utils.DefaultBoolean.False;
        ClassificacaoTributariaAtual.Settings.AllowAutoFilter = DevExpress.Utils.DefaultBoolean.False;
        ValorCotaAnterior.Settings.AllowAutoFilter = DevExpress.Utils.DefaultBoolean.False;
        ValorCotaAtual.Settings.AllowAutoFilter = DevExpress.Utils.DefaultBoolean.False;
        TotalIR.Settings.AllowAutoFilter = DevExpress.Utils.DefaultBoolean.False;
    }

    /// <summary>
    /// Realiza a paginação da grid
    /// </summary>
    protected void gridCadastro_PageIndexChanged(object sender, EventArgs e)
    {
        ASPxGridView gridCadastro = (ASPxGridView)sender;

        gridCadastro.DataSource = SessionPesquisa;
        gridCadastro.DataBind();
    }

    /// <summary>
    /// Carrega os dados na grid para realizar a extração dos dados 
    /// </summary>
    protected void gridCadastro_OnLoad(object sender, EventArgs e)
    {
        ASPxGridView gridCadastro = (ASPxGridView)sender;

        gridCadastro.DataSource = SessionPesquisa;
        gridCadastro.DataBind();
    }
    #endregion

    #region DropDown
    protected void dropEventoTransformacaoMudancaClassificacao_OnLoad(object sender, EventArgs e)
    {
        //Instancia controle do evento
        ASPxComboBox dropEventoTransformacaoMudancaClassificacao = (ASPxComboBox)sender;

        //Limpa os items para evitar duplicamento
        dropEventoTransformacaoMudancaClassificacao.Items.Clear();
        foreach (int r in Enum.GetValues(typeof(EventoTransformacaoMudancaClassificacao)))
        {
            dropEventoTransformacaoMudancaClassificacao.Items.Add(EventoTransformacaoMudancaClassificacaoDescricao.RetornaStringValue(r), r.ToString());
        }
    }
    #endregion

    #region CallBack
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        //Instancia controles de filtro da tela
        DateTime dataInicio = Convert.ToDateTime(textDataInicio.Text);
        DateTime dataFim = Convert.ToDateTime(textDataFim.Text);
        int idCarteira = dropFundoInvestimento.SelectedItem != null ? Convert.ToInt32(dropFundoInvestimento.SelectedItem.Value) : 0;
        int eventoTransformacaoMudancaClassificacao = dropEventoTransformacaoMudancaClassificacao.SelectedItem != null ? Convert.ToInt32(dropEventoTransformacaoMudancaClassificacao.SelectedItem.Value) : 0;

        //Instancia tables para realizar o UNION das tabelas
        DataTable tableMerge = new DataTable();
        DataTable tableEvento = new DataTable();
        DataTable tableFormaCondominio = new DataTable();
        DataTable tableDesenquadramento = new DataTable();

        //Carrega as collections nas tabelas designadas
        tableEvento = Evento(dataInicio, dataFim, idCarteira, eventoTransformacaoMudancaClassificacao).LoadDataTable();
        tableFormaCondominio = FormaCondominio(dataInicio, dataFim, idCarteira, eventoTransformacaoMudancaClassificacao).LoadDataTable();
        tableDesenquadramento = DesenquadramentoTributario(dataInicio, dataFim, idCarteira, eventoTransformacaoMudancaClassificacao).LoadDataTable();

        //Efetua o processo de UNION das tables
        if (eventoTransformacaoMudancaClassificacao.Equals(1) ||
            eventoTransformacaoMudancaClassificacao.Equals(2) ||
            eventoTransformacaoMudancaClassificacao.Equals(3))
        {
            tableFormaCondominio.Clear();
            tableDesenquadramento.Clear();
            tableMerge.Merge(tableEvento);
            tableMerge.Merge(tableFormaCondominio);
            tableMerge.Merge(tableDesenquadramento);
        }
        else if (eventoTransformacaoMudancaClassificacao.Equals(4) ||
                 eventoTransformacaoMudancaClassificacao.Equals(5))
        {
            tableEvento.Clear();
            tableDesenquadramento.Clear();
            tableMerge.Merge(tableEvento);
            tableMerge.Merge(tableFormaCondominio);
            tableMerge.Merge(tableDesenquadramento);
        }
        else if (eventoTransformacaoMudancaClassificacao.Equals(6))
        {
            tableEvento.Clear();
            tableFormaCondominio.Clear();
            tableMerge.Merge(tableEvento);
            tableMerge.Merge(tableFormaCondominio);
            tableMerge.Merge(tableDesenquadramento);
        }
        else
        {
            tableMerge.Merge(tableEvento);
            tableMerge.Merge(tableFormaCondominio);
            tableMerge.Merge(tableDesenquadramento);
        }

        //Atribui datatable a session para ser utilizada na paginação da grid
        SessionPesquisa = tableMerge;

        //Carrega a table na grid
        gridCadastro.DataSource = tableMerge;
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        if (string.IsNullOrEmpty(textDataInicio.Text))
        {
            e.Result = "Data Inicio deve ser preenchida";
            return;
        }
        if (string.IsNullOrEmpty(textDataFim.Text))
        {
            e.Result = "Data Fim deve ser preenchida";
            return;
        }
    }
    #endregion

    #region Suporte
    /// <summary>
    /// Carrega a query de eventos 
    /// </summary>
    private EventoFundoQuery Evento(DateTime dataInicio, DateTime dataFim, int idCarteira, int eventoTransformacaoMudancaClassificacao)
    {
        //Instancia Querys para realizar a consulta no banco de dados
        eventoFundoQuery = new EventoFundoQuery("e");
        agenteMercadoQuery = new AgenteMercadoQuery("a");
        carteiraQuery = new CarteiraQuery("c");
        posicaoFundoHistoricoQuery = new PosicaoFundoHistoricoQuery("p");
        agendaComeCotasQuery = new AgendaComeCotasQuery("ag"); 

        eventoFundoQuery.Select
        (
            eventoFundoQuery.IdCarteiraOrigem.As("Fundo"),
            eventoFundoQuery.DataPosicao.As("Data"),
            eventoFundoQuery.TipoEvento.As("EventoTransformacaoMudanca"),
            eventoFundoQuery.IdCarteiraDestino.As("FundoDestino"),
            agenteMercadoQuery.Nome.As("Administrador"),
            carteiraQuery.TipoTributacao.As("ClassificacaoTributariaAnterior"),
            carteiraQuery.TipoTributacao.As("ClassificacaoTributariaAtual"),
            agendaComeCotasQuery.ValorCotaUltimoPagamentoIR.As("ValorCotaAnterior"),
            agendaComeCotasQuery.ValorCota.As("ValorCotaAtual"),
            agendaComeCotasQuery.ValorIRPago.As("TotalIR")
        );

        eventoFundoQuery.LeftJoin(carteiraQuery).On(carteiraQuery.IdCarteira == eventoFundoQuery.IdCarteiraOrigem);
        eventoFundoQuery.LeftJoin(agenteMercadoQuery).On(agenteMercadoQuery.IdAgente == carteiraQuery.IdAgenteAdministrador);
        eventoFundoQuery.LeftJoin(posicaoFundoHistoricoQuery).On(posicaoFundoHistoricoQuery.IdCarteira == eventoFundoQuery.IdCarteiraOrigem);
        eventoFundoQuery.LeftJoin(agendaComeCotasQuery).On(agendaComeCotasQuery.IdCarteira == carteiraQuery.IdCarteira);
        eventoFundoQuery.Where(eventoFundoQuery.DataPosicao.Between(dataInicio, dataFim), eventoFundoQuery.ExecucaoRecolhimento.Between(2, 3));
        posicaoFundoHistoricoQuery.Where(posicaoFundoHistoricoQuery.DataAplicacao.LessThan(eventoFundoQuery.DataRecolhimento.Coalesce("eventoFundoQuery.DataPosicao")));
        //Verifica se a carteira foi selecionada
        if (idCarteira != 0) { eventoFundoQuery.Where(eventoFundoQuery.IdCarteiraOrigem == idCarteira); }
        //Verifica se é cisão, fusão, incorporação 
        if (eventoTransformacaoMudancaClassificacao.Equals(1) || eventoTransformacaoMudancaClassificacao.Equals(2) || eventoTransformacaoMudancaClassificacao.Equals(3)) { eventoFundoQuery.Where(eventoFundoQuery.TipoEvento.Equal(eventoTransformacaoMudancaClassificacao)); }

        eventoFundoQuery.GroupBy
        (
            eventoFundoQuery.IdCarteiraOrigem,
            eventoFundoQuery.DataPosicao,
            eventoFundoQuery.TipoEvento,
            eventoFundoQuery.IdCarteiraDestino,
            agenteMercadoQuery.Nome,
            carteiraQuery.TipoTributacao,
            agendaComeCotasQuery.ValorCotaUltimoPagamentoIR,
            agendaComeCotasQuery.ValorCota, 
            agendaComeCotasQuery.ValorIRPago
        );

        return eventoFundoQuery;
    }

    /// <summary>
    /// Carrega a query de Forma de Condominio
    /// </summary>
    private FundoInvestimentoFormaCondominioQuery FormaCondominio(DateTime dataInicio, DateTime dataFim, int idCarteira, int eventoTransformacaoMudancaClassificacao)
    {
        //Instancia Querys para realizar a consulta no banco de dados
        fundoInvestimentoFormaCondominioQuery = new FundoInvestimentoFormaCondominioQuery("f");
        agenteMercadoQuery = new AgenteMercadoQuery("a");
        carteiraQuery = new CarteiraQuery("c");
        posicaoFundoHistoricoQuery = new PosicaoFundoHistoricoQuery("p");
        agendaComeCotasQuery = new AgendaComeCotasQuery("ag");

        fundoInvestimentoFormaCondominioQuery.Select
        (
            fundoInvestimentoFormaCondominioQuery.IdCarteira.As("Fundo"),
            fundoInvestimentoFormaCondominioQuery.DataInicioVigencia.As("Data"),
            carteiraQuery.TipoFundo.As("EventoTransformacaoMudanca"),
            fundoInvestimentoFormaCondominioQuery.IdCarteira.As("FundoDestino"),
            agenteMercadoQuery.Nome.As("Administrador"),
            carteiraQuery.TipoTributacao.As("ClassificacaoTributariaAnterior"),
            carteiraQuery.TipoTributacao.As("ClassificacaoTributariaAtual"),
            carteiraQuery.TipoFundo.As("TipoFundoCarteira"),
            fundoInvestimentoFormaCondominioQuery.FormaCondominio.As("TipoFundoCondominio"),
            agendaComeCotasQuery.ValorCotaUltimoPagamentoIR.As("ValorCotaAnterior"),
            agendaComeCotasQuery.ValorCota.As("ValorCotaAtual"),
            agendaComeCotasQuery.ValorIRPago.As("TotalIR")
        );

        fundoInvestimentoFormaCondominioQuery.LeftJoin(carteiraQuery).On(carteiraQuery.IdCarteira == fundoInvestimentoFormaCondominioQuery.IdCarteira);
        fundoInvestimentoFormaCondominioQuery.LeftJoin(agenteMercadoQuery).On(agenteMercadoQuery.IdAgente == carteiraQuery.IdAgenteAdministrador);
        fundoInvestimentoFormaCondominioQuery.LeftJoin(posicaoFundoHistoricoQuery).On(posicaoFundoHistoricoQuery.IdCarteira == fundoInvestimentoFormaCondominioQuery.IdCarteira);
        fundoInvestimentoFormaCondominioQuery.LeftJoin(agendaComeCotasQuery).On(agendaComeCotasQuery.IdCarteira == carteiraQuery.IdCarteira);
        fundoInvestimentoFormaCondominioQuery.Where(fundoInvestimentoFormaCondominioQuery.DataInicioVigencia.Between(dataInicio, dataFim), fundoInvestimentoFormaCondominioQuery.ExecucaoRecolhimento.Between(2, 3));
        //Verifica se há recolhimento de IR
        posicaoFundoHistoricoQuery.Where(posicaoFundoHistoricoQuery.DataAplicacao.LessThan(fundoInvestimentoFormaCondominioQuery.DataRecolhimento.Coalesce("fundoInvestimentoFormaCondominioQuery.DataInicioVigencia")));
        //Verifica se a carteira foi selecionada
        if (idCarteira != 0) { fundoInvestimentoFormaCondominioQuery.Where(fundoInvestimentoFormaCondominioQuery.IdCarteira == idCarteira); }
        //Verifica o tipo de Transformação selecionada(Aberto-Fechado)
        if (eventoTransformacaoMudancaClassificacao.Equals(4)) { fundoInvestimentoFormaCondominioQuery.Where(carteiraQuery.TipoFundo.Equal(1), fundoInvestimentoFormaCondominioQuery.FormaCondominio.Equal(2)); }
        //Verifica o tipo de Transformação selecionada(Fechado-Aberto)
        if (eventoTransformacaoMudancaClassificacao.Equals(5)) { fundoInvestimentoFormaCondominioQuery.Where(carteiraQuery.TipoFundo.Equal(2), fundoInvestimentoFormaCondominioQuery.FormaCondominio.Equal(1)); }
        fundoInvestimentoFormaCondominioQuery.GroupBy
        (
            fundoInvestimentoFormaCondominioQuery.IdCarteira,
            fundoInvestimentoFormaCondominioQuery.DataInicioVigencia,
            fundoInvestimentoFormaCondominioQuery.FormaCondominio,
            agenteMercadoQuery.Nome,
            carteiraQuery.TipoTributacao,
            carteiraQuery.TipoFundo,
            agendaComeCotasQuery.ValorCotaUltimoPagamentoIR,
            agendaComeCotasQuery.ValorCota, 
            agendaComeCotasQuery.ValorIRPago
        );

        return fundoInvestimentoFormaCondominioQuery;
    }

    /// <summary>
    /// Carrega a query de Desenquadramento Tributario
    /// </summary>
    private DesenquadramentoTributarioQuery DesenquadramentoTributario(DateTime dataInicio, DateTime dataFim, int idCarteira, int eventoTransformacaoMudancaClassificacao)
    {
        //Instancia Querys para realizar a consulta no banco de dados
        desenquadramentoTributarioQuery = new DesenquadramentoTributarioQuery("d");
        agenteMercadoQuery = new AgenteMercadoQuery("a");
        carteiraQuery = new CarteiraQuery("c");
        posicaoFundoHistoricoQuery = new PosicaoFundoHistoricoQuery("p");
        agendaComeCotasQuery = new AgendaComeCotasQuery("ac");

        desenquadramentoTributarioQuery.Select
        (
           desenquadramentoTributarioQuery.IdCarteira.As("Fundo"),
           desenquadramentoTributarioQuery.Data.As("Data"),
           desenquadramentoTributarioQuery.IdCarteira.As("EventoTransformacaoMudanca"),
           carteiraQuery.TipoFundo.As("TipoFundoDesenquadramento"),
           desenquadramentoTributarioQuery.IdCarteira.As("FundoDestino"),
           agenteMercadoQuery.Nome.As("Administrador"),
           carteiraQuery.TipoTributacao.As("ClassificacaoTributariaAnterior"),
           carteiraQuery.TipoTributacao.As("ClassificacaoTributariaAtual"),
           agendaComeCotasQuery.ValorCotaUltimoPagamentoIR.As("ValorCotaAnterior"),
           agendaComeCotasQuery.ValorCota.As("ValorCotaAtual"),
           agendaComeCotasQuery.ValorIRPago.As("TotalIR")

        );

        desenquadramentoTributarioQuery.LeftJoin(carteiraQuery).On(carteiraQuery.IdCarteira == desenquadramentoTributarioQuery.IdCarteira);
        desenquadramentoTributarioQuery.LeftJoin(agenteMercadoQuery).On(agenteMercadoQuery.IdAgente == carteiraQuery.IdAgenteAdministrador);
        desenquadramentoTributarioQuery.LeftJoin(agendaComeCotasQuery).On(agendaComeCotasQuery.IdCarteira == carteiraQuery.IdCarteira);
        desenquadramentoTributarioQuery.LeftJoin(posicaoFundoHistoricoQuery).On(posicaoFundoHistoricoQuery.IdCarteira == desenquadramentoTributarioQuery.IdCarteira);
        desenquadramentoTributarioQuery.Where(desenquadramentoTributarioQuery.Data.Between(dataInicio, dataFim), desenquadramentoTributarioQuery.ExecucaoRecolhimento.Between(2, 3));
        posicaoFundoHistoricoQuery.Where(posicaoFundoHistoricoQuery.DataAplicacao.LessThan(desenquadramentoTributarioQuery.DataRecolhimento.Coalesce("desenquadramentoTributarioQuery.Data")));
        //Verifica se a carteira foi selecionada
        if (idCarteira != 0) { desenquadramentoTributarioQuery.Where(desenquadramentoTributarioQuery.IdCarteira == idCarteira); }

        desenquadramentoTributarioQuery.GroupBy
        (
            desenquadramentoTributarioQuery.IdCarteira,
            desenquadramentoTributarioQuery.Data,
            agenteMercadoQuery.Nome,
            carteiraQuery.TipoTributacao,
            carteiraQuery.TipoFundo,
            agendaComeCotasQuery.ValorCotaUltimoPagamentoIR,
            agendaComeCotasQuery.ValorCota,
            agendaComeCotasQuery.ValorIRPago
        );

        return desenquadramentoTributarioQuery;
    }
    #endregion
}