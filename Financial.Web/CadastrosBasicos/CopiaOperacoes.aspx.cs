﻿using log4net;
using Financial.Web.Common;
using System;
using Financial.RendaFixa;
using Financial.Common;
using Financial.Investidor;
using Financial.Bolsa;
using Financial.Fundo;
using Financial.Security;
using EntitySpaces.Core;
using Financial.Util;
using EntitySpaces.Interfaces;
using System.Collections.Generic;
using Financial.Fundo.Enums;
using System.Data;

public partial class CopiaOperacoes : BasePage {
    private static readonly ILog log = LogManager.GetLogger(typeof(CopiaOperacoes));

    new protected void Page_Load(object sender, EventArgs e) {
    }

    #region DataSources
    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        ClienteCollection coll = new ClienteCollection();
        coll.BuscaClientesComAcesso(Context.User.Identity.Name);        

        e.Collection = coll;
    }

    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        CarteiraCollection coll = new CarteiraCollection();
        coll.BuscaCarteirasComAcesso(Context.User.Identity.Name);
        //        
        e.Collection = coll;
    }

    protected void EsDSTituloRendaFixa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        TituloRendaFixaQuery titulo = new TituloRendaFixaQuery("t");
        EmissorQuery emissor = new EmissorQuery("e");

        titulo.Select(titulo, emissor.Nome.As("NomeEmissor"));
        titulo.InnerJoin(emissor).On(titulo.IdEmissor == emissor.IdEmissor);
        titulo.OrderBy(titulo.Descricao.Ascending, titulo.DataVencimento.Ascending);

        TituloRendaFixaCollection coll = new TituloRendaFixaCollection();
        coll.Load(titulo);
        
        e.Collection = coll;
    }

    protected void  EsDSAtivoBolsa_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        AtivoBolsaCollection coll = new AtivoBolsaCollection();
        coll.Query.OrderBy(coll.Query.CdAtivoBolsa.Ascending);
        //
        coll.LoadAll();

        e.Collection = coll;
    }   
    #endregion
   
    /// <summary>
    /// Copia Operações de Fundo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnCopiarFundo_Click(object sender, EventArgs e) {
       
        if (String.IsNullOrEmpty(dropCarteiraDestinoFundo.Text)) {
            throw new Exception("Carteira Destino Obrigatório.");
        }

        #region Select Operações Fundo de acordo com os parametros

        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
        OperacaoFundoQuery operacaoFundoQuery = new OperacaoFundoQuery("O");
        ClienteQuery clienteQuery = new ClienteQuery("C");
        CarteiraQuery carteiraQuery = new CarteiraQuery("A");

        operacaoFundoQuery.Select(operacaoFundoQuery,
                                  clienteQuery.Apelido.As("ApelidoCliente"),
                                  carteiraQuery.Apelido.As("ApelidoCarteira"));

        operacaoFundoQuery.InnerJoin(clienteQuery).On(operacaoFundoQuery.IdCliente == clienteQuery.IdCliente);
        operacaoFundoQuery.InnerJoin(carteiraQuery).On(operacaoFundoQuery.IdCarteira == carteiraQuery.IdCarteira);
        operacaoFundoQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);

        operacaoFundoQuery.Where(operacaoFundoQuery.TipoOperacao.NotIn((byte)TipoOperacaoFundo.Amortizacao, (byte)TipoOperacaoFundo.Juros),
                                 permissaoClienteQuery.IdUsuario == idUsuario);

        #region Parametros
        if (!String.IsNullOrEmpty(dropCarteiraOrigemFundo.Text)) {
            operacaoFundoQuery.Where(operacaoFundoQuery.IdCarteira == Convert.ToInt32(dropCarteiraOrigemFundo.Text));
        }

        if (!String.IsNullOrEmpty(dataInicioFundo.Text)) {
            operacaoFundoQuery.Where(operacaoFundoQuery.DataOperacao >= dataInicioFundo.Text);            
        }

        if (!String.IsNullOrEmpty(dataFimFundo.Text)) {
            operacaoFundoQuery.Where(operacaoFundoQuery.DataOperacao <= dataFimFundo.Text);
        }

        if (dropTipoOperacaoFundo.SelectedIndex >= 0) {
            operacaoFundoQuery.Where(operacaoFundoQuery.TipoOperacao == Convert.ToString(dropTipoOperacaoFundo.SelectedItem.Value));
        }
        #endregion

        OperacaoFundoCollection coll = new OperacaoFundoCollection();
        coll.Load(operacaoFundoQuery);

        #endregion

        //#region Gera Collection copia Independente da Primeira sem o IdOperacao e com o novo IdCarteira
        //OperacaoFundoCollection operNova = new OperacaoFundoCollection();
        ////
        //for (int i = 0; i < coll.Count; i++) {
        //    //
        //    OperacaoFundo oper = new OperacaoFundo();

        //    // Para cada Coluna de coll copia para OperacaoFundo
        //    foreach (esColumnMetadata col in coll.es.Meta.Columns) {
        //        // Copia todas as colunas menos idOperacao
        //        if (col.PropertyName != OperacaoFundoMetadata.ColumnNames.IdOperacao) {

        //            if (col.PropertyName == OperacaoFundoMetadata.ColumnNames.IdCarteira) {
        //                oper.IdCarteira = Convert.ToInt32(dropCarteiraDestinoFundo.Text);
        //            }
        //            else {
        //                esColumnMetadata colOperacaoFundo = oper.es.Meta.Columns.FindByPropertyName(col.PropertyName);
        //                if (coll[i].GetColumn(colOperacaoFundo.Name) != null) {
        //                    oper.SetColumn(colOperacaoFundo.Name, coll[i].GetColumn(colOperacaoFundo.Name));
        //                }
        //            }
        //        }
        //    }
        //    operNova.AttachEntity(oper);
        //}
        //#endregion

        OperacaoFundoCollection operNova = new OperacaoFundoCollection();
        foreach (OperacaoFundo oper in coll) {
            oper.MarkAllColumnsAsDirty(DataRowState.Added);
            oper.IdCarteira = Convert.ToInt32(dropCarteiraDestinoFundo.Text);

            operNova.AttachEntity(oper);
        }

        // Save
        operNova.Save();

        //
        this.ExibeMensagemSucesso(operNova.Count);        
    }

    /// <summary>
    /// Copia Operações de Bolsa
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnCopiarBolsa_Click(object sender, EventArgs e) {
        if (String.IsNullOrEmpty(dropClienteDestino.Text)) {
            throw new Exception("Cliente Destino Obrigatório.");
        }
        
        #region Select Operações Bolsa de acordo com os parametros

        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
        OperacaoBolsaQuery operacaoBolsaQuery = new OperacaoBolsaQuery("O");
        ClienteQuery clienteQuery = new ClienteQuery("C");

        operacaoBolsaQuery.Select(operacaoBolsaQuery, clienteQuery.Apelido.As("Apelido"));
        //
        operacaoBolsaQuery.InnerJoin(clienteQuery).On(operacaoBolsaQuery.IdCliente == clienteQuery.IdCliente);
        operacaoBolsaQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        //
        operacaoBolsaQuery.Where(permissaoClienteQuery.IdUsuario == idUsuario);

        #region Parametros
        if (!String.IsNullOrEmpty(dropClienteOrigem.Text)) {
            operacaoBolsaQuery.Where(operacaoBolsaQuery.IdCliente == Convert.ToInt32(dropClienteOrigem.Text));
        }

        if (!String.IsNullOrEmpty(dataInicioBolsa.Text)) {
            operacaoBolsaQuery.Where(operacaoBolsaQuery.Data >= dataInicioBolsa.Text);
        }

        if (!String.IsNullOrEmpty(dataFimBolsa.Text)) {
            operacaoBolsaQuery.Where(operacaoBolsaQuery.Data <= dataFimBolsa.Text);
        }

        if (!String.IsNullOrEmpty(dropCodigoAtivo.Text)) {
            operacaoBolsaQuery.Where(operacaoBolsaQuery.CdAtivoBolsa == dropCodigoAtivo.Text.ToUpper());
        }

        if (dropTipoOperacaoBolsa.SelectedIndex >= 0) {
            operacaoBolsaQuery.Where(operacaoBolsaQuery.TipoOperacao == Convert.ToString(dropTipoOperacaoBolsa.SelectedItem.Value));
        }

        if (dropTipoMercado.SelectedIndex >= 0) {
            operacaoBolsaQuery.Where(operacaoBolsaQuery.TipoMercado == dropTipoMercado.SelectedItem.Value);
        }
        #endregion

        OperacaoBolsaCollection coll = new OperacaoBolsaCollection();
        coll.Load(operacaoBolsaQuery);

        #endregion

        OperacaoBolsaCollection operNova = new OperacaoBolsaCollection();
        foreach (OperacaoBolsa oper in coll) {
            oper.MarkAllColumnsAsDirty(DataRowState.Added);
            oper.IdCliente = Convert.ToInt32(dropClienteDestino.Text);

            operNova.AttachEntity(oper);
        }
        
        //#region Gera Collection copia Independente da Primeira sem o IdOperacao e com o novo Idcliente
        //OperacaoBolsaCollection operNova = new OperacaoBolsaCollection();
        ////
        //for (int i = 0; i < coll.Count; i++) {
        //    //
        //    OperacaoBolsa oper = new OperacaoBolsa();

        //    // Para cada Coluna de coll copia para OperacaoBolsa
        //    foreach (esColumnMetadata col in coll.es.Meta.Columns) {
        //        // Copia todas as colunas menos idOperacao
        //        if (col.PropertyName != OperacaoBolsaMetadata.ColumnNames.IdOperacao) {

        //            if (col.PropertyName == OperacaoBolsaMetadata.ColumnNames.IdCliente) {
        //                oper.IdCliente = Convert.ToInt32(dropClienteDestino.Text);
        //            }
        //            else {
        //                esColumnMetadata colOperacaoBolsa = oper.es.Meta.Columns.FindByPropertyName(col.PropertyName);
        //                if (coll[i].GetColumn(colOperacaoBolsa.Name) != null) {
        //                    oper.SetColumn(colOperacaoBolsa.Name, coll[i].GetColumn(colOperacaoBolsa.Name));
        //                }
        //            }
        //        }
        //    }
        //    operNova.AttachEntity(oper);
        //}
        //#endregion

        // Save
        operNova.Save();

        //
        this.ExibeMensagemSucesso(operNova.Count);
    }

    /// <summary>
    /// Copia Operações de Renda Fixa
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnCopiarRendaFixa_Click(object sender, EventArgs e) {
                
        if (String.IsNullOrEmpty(dropClienteRendaFixaDestino.Text)) {
            throw new Exception("Cliente Destino Obrigatório.");
        }

        #region Select Operações Bolsa de acordo com os parametros

        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
        OperacaoRendaFixaQuery operacaoRendaFixaQuery = new OperacaoRendaFixaQuery("O");
        ClienteQuery clienteQuery = new ClienteQuery("C");
        TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");

        operacaoRendaFixaQuery.Select(operacaoRendaFixaQuery, clienteQuery.Apelido.As("Apelido"), tituloRendaFixaQuery.DescricaoCompleta);
        operacaoRendaFixaQuery.InnerJoin(clienteQuery).On(operacaoRendaFixaQuery.IdCliente == clienteQuery.IdCliente);
        operacaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(operacaoRendaFixaQuery.IdTitulo == tituloRendaFixaQuery.IdTitulo);
        operacaoRendaFixaQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        //
        operacaoRendaFixaQuery.Where(permissaoClienteQuery.IdUsuario == idUsuario);
       
        #region Parametros
        if (!String.IsNullOrEmpty(dropClienteRendaFixaOrigem.Text)) {
            operacaoRendaFixaQuery.Where(operacaoRendaFixaQuery.IdCliente == Convert.ToInt32(dropClienteRendaFixaOrigem.Text));            
        }

        if (!String.IsNullOrEmpty(dataInicioRendaFixa.Text)) {            
            operacaoRendaFixaQuery.Where(operacaoRendaFixaQuery.DataOperacao >= dataInicioRendaFixa.Text);
        }

        if (!String.IsNullOrEmpty(dataFimRendaFixa.Text)) {            
            operacaoRendaFixaQuery.Where(operacaoRendaFixaQuery.DataOperacao <= dataFimRendaFixa.Text);
        }

        if (!String.IsNullOrEmpty(dropTitulo.Text)) {
            List<int> idTitulo = new List<int>();
            string[] idTitulos = dropTitulo.Text.Split(',');
            for (int i = 0; i < idTitulos.Length; i++) {
                idTitulo.Add(Convert.ToInt32(idTitulos[i]));
            }
            operacaoRendaFixaQuery.Where(operacaoRendaFixaQuery.IdTitulo.In(idTitulo));         
        }

        if (tipoOperacaoRendaFixa.SelectedIndex >= 0) {
            operacaoRendaFixaQuery.Where(operacaoRendaFixaQuery.TipoOperacao == Convert.ToInt32(tipoOperacaoRendaFixa.SelectedItem.Value));
        }

        #endregion

        OperacaoRendaFixaCollection coll = new OperacaoRendaFixaCollection();
        coll.Load(operacaoRendaFixaQuery);

        #endregion

        //#region Gera Collection copia Independente da Primeira sem o IdOperacao e com o novo IdCliente
        //OperacaoRendaFixaCollection operNova = new OperacaoRendaFixaCollection();
        ////
        //for (int i = 0; i < coll.Count; i++) {
        //    //
        //    OperacaoRendaFixa oper = new OperacaoRendaFixa();

        //    // Para cada Coluna de coll copia para OperacaoRendaFixa
        //    foreach (esColumnMetadata col in coll.es.Meta.Columns) {
        //        // Copia todas as colunas menos idOperacao
        //        if (col.PropertyName != OperacaoRendaFixaMetadata.ColumnNames.IdOperacao) {

        //            if (col.PropertyName == OperacaoRendaFixaMetadata.ColumnNames.IdCliente) {
        //                oper.IdCliente = Convert.ToInt32(dropClienteRendaFixaDestino.Text);
        //            }
        //            else {
        //                esColumnMetadata colOperacaorendaFixa = oper.es.Meta.Columns.FindByPropertyName(col.PropertyName);
        //                if (coll[i].GetColumn(colOperacaorendaFixa.Name) != null) {
        //                    oper.SetColumn(colOperacaorendaFixa.Name, coll[i].GetColumn(colOperacaorendaFixa.Name));
        //                }
        //            }
        //        }
        //    }
        //    operNova.AttachEntity(oper);
        //}
        //#endregion

        OperacaoRendaFixaCollection operNova = new OperacaoRendaFixaCollection();
        foreach (OperacaoRendaFixa oper in coll) {
            oper.MarkAllColumnsAsDirty(DataRowState.Added);
            oper.IdCliente = Convert.ToInt32(dropClienteRendaFixaDestino.Text);

            operNova.AttachEntity(oper);
        }

        // Save
        operNova.Save();

        //
        this.ExibeMensagemSucesso(operNova.Count);        
    }

    /// <summary>
    /// Exibe mensagem de Sucesso
    /// </summary>
    /// <param name="registros"></param>
    private void ExibeMensagemSucesso(int registros) {

        string mensagem = registros == 0 ? "Nenhum Registro copiado."
                                         : "Cópia realizada com sucesso. Registros: " + registros;

        throw new Exception(mensagem);
    }
}