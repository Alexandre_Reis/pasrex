﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PosicaoConsolidada.aspx.cs" Inherits="CustomReport_PosicaoConsolidada" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxw" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
        
        function formatDate(date) {
            var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [year, month, day].join('-');
        }
        
        function OnGetSelectedFieldValues(selectedValues) {
           
            if(selectedValues.length == 0) {
                alert('Favor selecionar um fundo');
                return false;
            }
            
            if(!textDataInicio.GetValue()){
                alert('Favor selecionar uma data');
                return false;
            }
            
            if(!textDataFim.GetValue()){
                alert('Favor selecionar uma data');
                return false;
            }
            
            var dataInicio = formatDate(textDataInicio.GetValue());
            var dataFim = formatDate(textDataFim.GetValue());
            
            /*
            public enum TipoRelatorio
            {
                ExtratoMensal = 1,
                PosicaoConsolidada = 2,
                LaminaFundos = 3,
                RentabilidadeFundos = 4,
                ExtratoMensalExcel = 5
            }
            */
            
            var tipoReport = 2;//PosicaoConsolidada
                        
            for(i = 0; i < selectedValues.length; i++) {
                var clienteID = selectedValues[i][0];
                
                var params = 'clienteID=' + clienteID + '&dataInicio=' + dataInicio + '&dataFim=' + dataFim + '&tipoExibicao=' + window.tipoExibicao + '&tipoReport=' + tipoReport + '&login=' + login;
                
                var url = urlComercialWebReports + '/WebFormReportView.aspx?' + params;
                
                if(tipoExibicao == 'Preview'){
                    window.open(url);
                }else{
                    var iframe = document.createElement('iframe');
                    iframe.style.display = 'none';
                    iframe.src = url;
                    document.body.appendChild(iframe);
                }
                
            }
            
            return false;        
            
        }
        
    </script>

</head>
<body>
    <form id="form1" runat="server">
        
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Posição Consolidada" />
                            </div>
                            <div id="mainContent">
                                <table class="dropDownInlineWrapper">
                                    <tr>
                                        <td>
                                            <asp:Label ID="labelGrupoProcessamento" runat="server" CssClass="labelNormal" Text="Grupo:"></asp:Label>
                                        </td>
                                        <td>
                                            <dxe:ASPxComboBox ID="dropGrupoProcessamento" runat="server" ClientInstanceName="dropGrupoProcessamento"
                                                DataSourceID="EsDSGrupoProcessamento" ShowShadow="true" DropDownStyle="DropDownList"
                                                CssClass="dropDownListCurto1 dropDownInline" TextField="Descricao" ValueField="IdGrupoProcessamento">
                                                <ClientSideEvents SelectedIndexChanged="function(s, e) {gridConsulta.PerformCallback('btnRefresh'); return false;}" />
                                            </dxe:ASPxComboBox>
                                        </td>
                                        
                                        <td>
                                            <asp:Label ID="labelDataInicio" runat="server" CssClass="labelRequired" Text="Data Início:"></asp:Label>
                                        </td>
                                        <td>
                                            <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio">
                                            <ClientSideEvents DateChanged="function(s, e) {gridConsulta.PerformCallback('btnRefresh'); return false;}" />
                                            </dxe:ASPxDateEdit>
                                        </td>
                                        
                                        <td>
                                            <asp:Label ID="labelDataFim" runat="server" CssClass="labelRequired" Text="Data Fim:"></asp:Label>
                                        </td>
                                        <td>
                                            <dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim">
                                            <ClientSideEvents DateChanged="function(s, e) {gridConsulta.PerformCallback('btnRefresh'); return false;}" />
                                            </dxe:ASPxDateEdit>
                                        </td>
                                    </tr>
                                </table>
                                <div class="linkButton">
                                   
                                    <asp:LinkButton ID="LinkButton1" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnView" OnClientClick="
                                            window.tipoExibicao = 'Preview';
                                            var selected = gridConsulta.GetSelectedFieldValues('IdCotista;Nome', OnGetSelectedFieldValues);
                                            return false;
                                        ">
                                        <asp:Literal ID="Literal1" runat="server" Text="Visualizar" /><div>
                                        </div>
                                    </asp:LinkButton>
                                   
                                    <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnPdf" OnClientClick="
                                            window.tipoExibicao = 'PDF';
                                            var selected = gridConsulta.GetSelectedFieldValues('IdCotista;Nome', OnGetSelectedFieldValues);
                                            return false;
                                        ">
                                        <asp:Literal ID="Literal4" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton>
                                </div>
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridConsulta" runat="server" EnableCallBacks="true" KeyFieldName="IdCotista"
                                        DataSourceID="EsDSCliente" 
                                        OnCustomCallback="gridConsulta_CustomCallback">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="10%" ButtonType="Image" ShowClearFilterButton="True">
                                                <HeaderTemplate>
                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                                </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataColumn FieldName="IdCotista" VisibleIndex="1" Width="10%" CellStyle-HorizontalAlign="left" />
                                            <dxwgv:GridViewDataColumn FieldName="Nome" VisibleIndex="2" Width="30%" />
                                        </Columns>
                                        <SettingsEditing PopupEditFormWidth="500px" />
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridConsulta"
            Landscape="true" />
        <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect"
            LowLevelBind="true" />
        <cc1:esDataSource ID="EsDSGrupoProcessamento" runat="server" OnesSelect="EsDSGrupoProcessamento_esSelect" />
    </form>
</body>
</html>
