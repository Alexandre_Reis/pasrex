﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using EntitySpaces.Interfaces;
using Financial.Web.Common;
using DevExpress.Web;
using Financial.Util;
using Financial.ContaCorrente.Enums;
using Financial.Bolsa.Enums;
using Financial.BMF.Enums;
using Financial.Fundo.Enums;
using Financial.Util.ConfiguracaoSistema;
using Financial.Util.Enums;
using System.Globalization;
using System.Data.SqlClient;
using Financial.Fundo;


public partial class ConfiguracaoReports : BasePage
{
    public Dictionary<string, string> stRelatorio = new Dictionary<string, string>();

    new protected void Page_Load(object sender, EventArgs e)
    {
        pnlSeguranca.HeaderText = String.Empty;

        //Carregar infos da ST_RELATORIO
        Carteira carteira = new Carteira();
        string connectionString = carteira.es.Connection.ConnectionString;
        SqlDataAdapter adapterMapping = new SqlDataAdapter();
        DataTable dataTable = new DataTable();

        using (SqlConnection conn = new SqlConnection(connectionString))
        {
            conn.Open();
            dataTable.TableName = "ST_RELATORIO";
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "select * from ST_RELATORIO";

                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Connection = conn;

                adapterMapping.SelectCommand = cmd;
                adapterMapping.Fill(dataTable);
            }
        }

        foreach (DataRow row in dataTable.Rows)
        {
            string key = (string)row["CD_RELATORIO"];
            if (!stRelatorio.ContainsKey(key))
            {
                string description = (string)row["DS_OBJETO_CONTEUDO"];
                stRelatorio.Add(key, description);
            }
        }
    }

    protected void btnSalvar_Click(object sender, EventArgs e) {

        StringBuilder sqlUpdate = new StringBuilder();

        List<System.Web.UI.WebControls.TextBox> controles = new List<System.Web.UI.WebControls.TextBox>(new System.Web.UI.WebControls.TextBox[] { 
    	    this.txtDisclaimerExtrato, 
            this.txtDisclaimerModelo1,
            this.txtDisclaimerModelo2,
            this.txtDisclaimerModelo3,
            this.txtDisclaimerModelo4,
            this.txtDisclaimerModelo5,
            this.txtDisclaimerModelo6
        });

        foreach (System.Web.UI.WebControls.TextBox control in controles)
        {
            sqlUpdate.AppendFormat("update ST_RELATORIO set DS_OBJETO_CONTEUDO='{0}' where CD_RELATORIO='{1}'", control.Text, control.ValidationGroup);
            sqlUpdate.AppendLine();
        }
        
        Carteira carteira = new Carteira();
        string connectionString = carteira.es.Connection.ConnectionString;
        using (SqlConnection conn = new SqlConnection(connectionString))
        {
            conn.Open();
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = sqlUpdate.ToString();

                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Connection = conn;

                cmd.ExecuteNonQuery();
               
            }
        }

        string mensagem = "Alterações Salvas com Sucesso.";
        throw new Exception(mensagem);
    }

    #region TextBoxs

    protected void txtDisclaimerExtrato_OnPrerender(object sender, EventArgs e)
    {
        this.txtDisclaimerExtrato.Text = stRelatorio[txtDisclaimerExtrato.ValidationGroup];
    }

    protected void txtDisclaimerModelo1_OnPrerender(object sender, EventArgs e)
    {
        this.txtDisclaimerModelo1.Text = stRelatorio[txtDisclaimerModelo1.ValidationGroup];
    }

    protected void txtDisclaimerModelo2_OnPrerender(object sender, EventArgs e)
    {
        this.txtDisclaimerModelo2.Text = stRelatorio[txtDisclaimerModelo2.ValidationGroup];
    }

    protected void txtDisclaimerModelo3_OnPrerender(object sender, EventArgs e)
    {
        this.txtDisclaimerModelo3.Text = stRelatorio[txtDisclaimerModelo3.ValidationGroup];
    }

    protected void txtDisclaimerModelo4_OnPrerender(object sender, EventArgs e)
    {
        this.txtDisclaimerModelo4.Text = stRelatorio[txtDisclaimerModelo4.ValidationGroup];
    }

    protected void txtDisclaimerModelo5_OnPrerender(object sender, EventArgs e)
    {
        this.txtDisclaimerModelo5.Text = stRelatorio[txtDisclaimerModelo5.ValidationGroup];
    }

    protected void txtDisclaimerModelo6_OnPrerender(object sender, EventArgs e)
    {
        this.txtDisclaimerModelo6.Text = stRelatorio[txtDisclaimerModelo6.ValidationGroup];
    }

    #endregion



}