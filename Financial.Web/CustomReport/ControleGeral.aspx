﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ControleGeral.aspx.cs"
    Inherits="CustomReport_ControleGeral" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxw" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../js/global.js"></script>

    <script type="text/javascript" src="../js/jquery.js"></script>

    <script type="text/javascript" language="Javascript">
        
        function gerarPDF(){
            //Buscar clientes (carteiras) selecionados
            gridConsulta.GetSelectedFieldValues('IdCliente', onGerarPDFClientesSelected);
        }
       
        function onGerarPDFClientesSelected(selectedValues) {
            window.selectedClientes = selectedValues && selectedValues.length ? selectedValues : [] ;
            //Buscar cotistas selecionados
            gridConsulta1.GetSelectedFieldValues('IdCotista', onGerarPDFCotistasSelected);
        }
        
        function onGerarPDFCotistasSelected(selectedValues) {
            window.selectedCotistas = selectedValues && selectedValues.length ? selectedValues : [] ;
            
            var params = {
                action: window.action,
                dataInicio: formatDate(textDataInicio.GetValue()),
                dataFim: formatDate(textDataFim.GetValue()),
                selectedClientes: window.selectedClientes.join(', '),
                selectedCotistas: window.selectedCotistas.join(', ')
            };
            
            //Chamar ashx passando parametros
            var url = urlComercialWebReports + '/ashx/WebFormGeral.ashx';
                
            $.ajax({
                crossDomain: true,
                contentType: "application/json; charset=utf-8",
                type: 'get',
	            url: url,
	            dataType: 'jsonp',
	            cache: false,
	            data: params,
	            success: onGerarPDFSuccess 
	        });
        }
        
        function onGerarPDFSuccess(data){
	        if(data.success){
                alert("A geração dos relatórios foi finalizada. Os arquivos estão disponíveis na pasta do portal do PAS.");
            }  else{
                if(data.errorMessage && data.errorMessage.length){
                    alert(data.errorMessage);
                }else{
                   alert('Ocorreu um erro');
                }
                
            }
        }
        
        //FIM PDF
        
        function salvarSetup(){
            //Buscar clientes (carteiras) selecionados
            gridConsulta.GetSelectedFieldValues('IdCliente', onSalvarSetupClientesSelected);
        }
        
        function onSalvarSetupClientesSelected(selectedValues) {
            window.selectedClientes = selectedValues && selectedValues.length ? selectedValues : [] ;
            //Buscar cotistas selecionados
            gridConsulta1.GetSelectedFieldValues('IdCotista', onSalvarSetupCotistasSelected);
        }
        
        function onSalvarSetupCotistasSelected(selectedValues) {
            window.selectedCotistas = selectedValues && selectedValues.length ? selectedValues : [] ;
            
            var params = {
                action: "btnSetup_Click",
                //dataInicio: formatDate(textDataInicio.GetValue()),
                //dataFim: formatDate(textDataFim.GetValue()),
                selectedClientes: window.selectedClientes.join(', '),
                selectedCotistas: window.selectedCotistas.join(', ')
            };
            
            //Chamar ashx passando parametros
            var url = urlComercialWebReports + '/ashx/WebFormGeral.ashx';
                
            $.ajax({
                crossDomain: true,
                contentType: "application/json; charset=utf-8",
                type: 'get',
	            url: url,
	            dataType: 'jsonp',
	            cache: false,
	            data: params,
	            success: onSalvarSetupSuccess 
	        });
        }
        
        function onSalvarSetupSuccess(data){
	        if(data.success){
                alert("Os dados foram armazenados!");
            }  
        }
        
        function formatDate(date) {
            var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [year, month, day].join('-');
        }
        
        function OnGetSelectedFieldValues(selectedValues) {
          
            if(selectedValues.length == 0) {
                alert('Favor selecionar um fundo');
                return false;
            }
            
            if(!textDataFim.GetValue()){
                alert('Favor selecionar uma data');
                return false;
            }
            
            var data = formatDate(textDataFim.GetValue());
            var gestorID = 10017;
            var gestorNome = "Porto Seguro Investimentos";
            var tipoReport = 3;//lamina fundos
                        
            for(i = 0; i < selectedValues.length; i++) {
                var clienteID = selectedValues[i][0];
                var nomeFundo = selectedValues[i][1];
                var categorias = selectedValues[i][2];
                var nomeCategoria = selectedValues[i][3];
                var nomeSubCategoria = selectedValues[i][4];
                
                var params = 'clienteID=' + clienteID + '&data=' + data + '&tipoExibicao=' + window.tipoExibicao + '&tipoReport=' + tipoReport + '&login=' + login + 
                    '&nomeFundo=' + nomeFundo + '&categorias=' + categorias + '&nomeCategoria=' + nomeCategoria + '&nomeSubCategoria=' + nomeSubCategoria + 
                    '&gestorID=' + gestorID + '&gestorNome=' + gestorNome;
                
                var url = urlComercialWebReports + '/WebFormReportView.aspx?' + params;
                
                if(tipoExibicao == 'Preview'){
                    window.open(url);
                }else{
                    var iframe = document.createElement('iframe');
                    iframe.style.display = 'none';
                    iframe.src = url;
                    document.body.appendChild(iframe);
                }
                
            }
            
            return false;        
            
            
            //OLD CODE:
            /*
            var clienteIDs = [];
            var nomeFundos = [];
            var categoriasParam = [];//modelos
            var nomeCategorias = [];//categoriafundo
            var nomeSubCategorias = [];//subcategoriafundo
            
                        
            for(i = 0; i < selectedValues.length; i++) {
                clienteIDs.push(selectedValues[i][0]);
                nomeFundos.push(selectedValues[i][1]);
                categoriasParam.push(selectedValues[i][2]);
                nomeCategorias.push(selectedValues[i][3]);
                nomeSubCategorias.push(selectedValues[i][4]);
                
                
            }
            
            var clienteID = clienteIDs.join(';');
            var nomeFundo = nomeFundos.join(';');
            var categorias = categoriasParam.join(';');
            var nomeCategoria = nomeCategorias.join(';');
            var nomeSubCategoria = nomeSubCategorias.join(';');
            var gestorID = 10017;
            var gestorNome = "Porto Seguro Investimentos";
                        
            var tipoReport = 3;//lamina fundos
            
            var params = 'clienteID=' + clienteID + '&data=' + data + '&tipoExibicao=' + window.tipoExibicao + '&tipoReport=' + tipoReport + '&login=' + login + 
            '&nomeFundo=' + nomeFundo + '&categorias=' + categorias + '&nomeCategoria=' + nomeCategoria + '&nomeSubCategoria=' + nomeSubCategoria + 
            '&gestorID=' + gestorID + '&gestorNome=' + gestorNome;
            
            var url = urlComercialWebReports + '/WebFormReportView.aspx?' + params;
            
            window.open(url);
            */
            //sample data:
            //object SClienteID = new List<int>() { 2000080950 };
            //DateTime SData = new DateTime(2014,5,1);
            //string STipoExibicao = "Preview";
            //object STipoReport = TipoRelatorio.ExtratoMensal (1);
            
        }
        
    </script>

</head>
<body>
    <form id="form1" runat="server">
        
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Controle Geral" />
                            </div>
                            <div id="mainContent">
                                <table class="dropDownInlineWrapper">
                                    <tr>
                                        <td>
                                            <asp:Label ID="labelDataInicio" runat="server" CssClass="labelRequired" Text="Data Início:"></asp:Label>
                                        </td>
                                        <td>
                                            <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio">
                                            </dxe:ASPxDateEdit>
                                        </td>
                                        <td>
                                            <asp:Label ID="labelDataFim" runat="server" CssClass="labelRequired" Text="Data Fim:"></asp:Label>
                                        </td>
                                        <td>
                                            <dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim">
                                            </dxe:ASPxDateEdit>
                                        </td>
                                        <td>
                                            <span style="margin-left: 20px;">Gestor: </span><span style="margin-left: 5px; font-weight: bold">Porto Seguro Investimentos</span>
                                        </td>
                                    </tr>
                                </table>
                                <div class="linkButton">
                                   
                                    <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnPdf" OnClientClick="
                                            window.action = 'btnPDF_Click';
                                            gerarPDF();
                                            return false;
                                        ">
                                        <asp:Literal ID="Literal4" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    
                                    <asp:LinkButton ID="LinkButton1" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnView" OnClientClick="
                                            window.action = 'btnExcel_Click';
                                            gerarPDF();
                                            return false;
                                        ">
                                        <asp:Literal ID="Literal1" runat="server" Text="Excel do Extrato" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    
                                    <asp:LinkButton ID="LinkButton2" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnSave" OnClientClick="
                                            salvarSetup();
                                            return false;
                                        ">
                                        <asp:Literal ID="Literal2" runat="server" Text="Salvar Setup" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    
                                </div>
                                <div class="divDataGrid" style ="width: 45%; float: left;">
                                    <dxwgv:ASPxGridView ID="gridConsulta" runat="server" EnableCallBacks="true" KeyFieldName="IdCliente"
                                        DataSourceID="EsDSCliente"   OnCustomUnboundColumnData="gridConsulta_CustomUnboundColumnData" 
                                        OnCustomCallback="gridConsulta_CustomCallback">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="10%" ButtonType="Image" ShowClearFilterButton="True">
                                                <HeaderTemplate>
                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                                </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>                                            
                                            <dxwgv:GridViewDataColumn FieldName="IdCliente" VisibleIndex="1" Width="10%" CellStyle-HorizontalAlign="left" />
                                            <dxwgv:GridViewDataColumn FieldName="Nome" VisibleIndex="2" Width="30%" />
                                            
                                        </Columns>
                                        <SettingsEditing PopupEditFormWidth="500px" />
                                    </dxwgv:ASPxGridView>
                                </div>
                                <div class="divDataGrid" style ="width: 45%; float: left; margin-left: 40px;">
                                    <dxwgv:ASPxGridView ID="gridConsulta1" runat="server" EnableCallBacks="true" KeyFieldName="IdCotista"
                                        DataSourceID="EsDSCotista"   OnCustomUnboundColumnData="gridConsulta_CustomUnboundColumnData" 
                                        OnCustomCallback="gridConsulta_CustomCallback">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="10%" ButtonType="Image" ShowClearFilterButton="True">
                                                <HeaderTemplate>
                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                                </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>                                            
                                            <dxwgv:GridViewDataColumn FieldName="IdCotista" VisibleIndex="1" Width="10%" CellStyle-HorizontalAlign="left" />
                                            <dxwgv:GridViewDataColumn FieldName="Nome" VisibleIndex="2" Width="30%" />
                                            
                                        </Columns>
                                        <SettingsEditing PopupEditFormWidth="500px" />
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridConsulta"
            Landscape="true" />
        <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect"
            LowLevelBind="true" />
            <cc1:esDataSource ID="EsDSCotista" runat="server" OnesSelect="EsDSCotista_esSelect"
            LowLevelBind="true" />
        <cc1:esDataSource ID="EsDSTipoCliente" runat="server" OnesSelect="EsDSTipoCliente_esSelect" />    
        
    </form>
</body>
</html>
