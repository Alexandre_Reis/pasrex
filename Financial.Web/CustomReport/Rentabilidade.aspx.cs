﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using Financial.Investidor;
using Financial.ContaCorrente;
using System.Drawing;
using Financial.ContaCorrente.Enums;
using Financial.Security;
using Financial.Fundo;
using Financial.Bolsa;
using Financial.BMF;
using Financial.RendaFixa;
using Financial.Investidor.Enums;
using Financial.CRM;
using Financial.Bolsa.Enums;
using Financial.BMF.Enums;
using EntitySpaces.Interfaces;
using Financial.WebConfigConfiguration;
using Financial.Security.Enums;
using System.Collections.Specialized;
using Financial.Swap;
using DevExpress.Web.Data;
using Financial.Contabil;
using Financial.Web.Util;
using Financial.Common.Enums;
using Financial.Captacao.Enums;
using Financial.Web.Common;

public partial class CustomReport_Rentabilidade : ConsultaBasePage
{
    new protected void Page_Load(object sender, EventArgs e)
    {

        base.Page_Load(sender, e);

        gridConsulta.EnableViewState = true;
        gridConsulta.Settings.ShowVerticalScrollBar = true;
        gridConsulta.Settings.VerticalScrollableHeight = 340;
        gridConsulta.SettingsPager.PageSize = 200;

        string login = HttpContext.Current.User.Identity.Name;
        string urlComercialWebReports = WebConfig.AppSettings.UrlComercialWebReports.Trim();

        StringBuilder javascript = new StringBuilder();
        javascript.AppendFormat("var login = '{0}';", login);
        javascript.AppendLine();
        javascript.AppendFormat("var urlComercialWebReports = '{0}';", urlComercialWebReports);
        javascript.AppendLine();

        Page.ClientScript.RegisterStartupScript(this.GetType(), "initJavascriptVars",
       "<script type=\"text/javascript\">" +
       javascript.ToString() +
       "</script>");

    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {

        if (textDataFim.Text != "")
        {

            Usuario usuario = new Usuario();
            usuario.BuscaUsuario(Context.User.Identity.Name);
            int idUsuario = usuario.IdUsuario.Value;

            DateTime dataFinal = Convert.ToDateTime(textDataFim.Text);

            ClienteQuery clienteQuery = new ClienteQuery("C");
            PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");

            clienteQuery.Select(clienteQuery);
            clienteQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
            clienteQuery.Where(permissaoClienteQuery.IdUsuario.Equal(idUsuario));
            clienteQuery.Where(clienteQuery.TipoControle.NotEqual((byte)TipoControleCliente.ApenasCotacao),
                               clienteQuery.TipoControle.NotEqual((byte)TipoControleCliente.CarteiraImportada),
                               clienteQuery.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo),
                               clienteQuery.DataImplantacao <= dataFinal,
                               clienteQuery.DataDia >= dataFinal,
                               clienteQuery.IsProcessando.Equal("N"));

            if (dropGrupoProcessamento.SelectedIndex > 0)
            {
                clienteQuery.Where(clienteQuery.IdGrupoProcessamento == dropGrupoProcessamento.SelectedItem.Value);
            }

            ClienteCollection clientes = new ClienteCollection();
            clientes.Load(clienteQuery);

            e.Collection = clientes;

        }
        else
        {
            e.Collection = new ClienteCollection();
        }

        if (!Page.IsPostBack)
        {
            DevExpress.Web.ListEditItem item = new DevExpress.Web.ListEditItem();
            item.Text = " ";
            dropGrupoProcessamento.Items.Insert(0, item);
            DevExpress.Web.ListEditItem item2 = new DevExpress.Web.ListEditItem();
            item2.Text = " ";

        }
    }

    protected void EsDSGrupoProcessamento_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        GrupoProcessamentoCollection coll = new GrupoProcessamentoCollection();
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        List<object> keyValuesId = gridConsulta.GetSelectedFieldValues("IdCliente");

        if (keyValuesId.Count == 0)
        {
            e.Result = "Algum cliente deve ser selecionado!";
            return;
        }

        if (textDataFim.Text == "")
        {
            e.Result = "A Data Final deve ser informada!";
            return;
        }

        foreach (object objectId in keyValuesId)
        {
            int idCliente = (int)objectId;
            DateTime dataFinal = Convert.ToDateTime(textDataFim.Text);

            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.DataDia);
            campos.Add(cliente.Query.IdMoeda);
            //
            campos.Add(cliente.Query.DataBloqueioProcessamento);
            //
            cliente.LoadByPrimaryKey(campos, idCliente);
            DateTime dataDia = cliente.DataDia.Value;
            //
            DateTime? dataBloqueioProcessamento = null;
            if (cliente.DataBloqueioProcessamento.HasValue)
            {
                dataBloqueioProcessamento = cliente.DataBloqueioProcessamento.Value;
            }

            //
            if (cliente.IdMoeda == (int)ListaMoedaFixo.Real)
            {
                if (!Calendario.IsDiaUtil(Convert.ToDateTime(textDataFim.Text)))
                {
                    e.Result = "Data Final não é dia útil para o cliente " + idCliente.ToString() + ".";
                    return;
                }
            }
            else
            {
                if (!Calendario.IsDiaUtil(Convert.ToDateTime(textDataFim.Text), LocalFeriadoFixo.NovaYork, TipoFeriado.Outros))
                {
                    e.Result = "Data Final não é dia útil para o cliente " + idCliente.ToString() + ".";
                    return;
                }
            }

        }
    }

    protected void gridConsulta_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        gridConsulta.DataBind();
        gridConsulta.Selection.UnselectAll();
        gridConsulta.CancelEdit();
    }

}