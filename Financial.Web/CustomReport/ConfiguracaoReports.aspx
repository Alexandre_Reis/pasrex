﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ConfiguracaoReports.aspx.cs"
    Inherits="ConfiguracaoReports" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxrp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxp" %>
<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxw" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />
    <link rel="SHORTCUT ICON" href="~/imagensPersonalizadas/favicon.ico" type="image/x-icon" />

    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
    
    <script type="text/javascript" language="Javascript">
    function SetaMaxLength()
    {
        var maxLength = 1200;
        document.getElementById('pnlSeguranca_tabCadastro_txtDisclaimerExtrato').maxLength = maxLength;
        document.getElementById('pnlSeguranca_tabCadastro_txtDisclaimerModelo6').maxLength = maxLength;
        document.getElementById('pnlSeguranca_tabCadastro_txtDisclaimerModelo5').maxLength = maxLength;
        document.getElementById('pnlSeguranca_tabCadastro_txtDisclaimerModelo4').maxLength = maxLength;
        document.getElementById('pnlSeguranca_tabCadastro_txtDisclaimerModelo3').maxLength = maxLength;
        document.getElementById('pnlSeguranca_tabCadastro_txtDisclaimerModelo2').maxLength = maxLength;
        document.getElementById('pnlSeguranca_tabCadastro_txtDisclaimerModelo1').maxLength = maxLength;
    }

    </script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true"
            EnableScriptLocalization="true" />
        <div class="divPanel divPanelNew">
            <table width="100%" border="0">
                <tr>
                    <td>
                        <div id="container_small">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Configuração dos Relatórios" />
                            </div>
                            <div id="mainContentSpace">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <br />
                                        <br />
                                        <table border="0" align="center">
                                            <tr>
                                                <td>
                                                    <dxrp:ASPxRoundPanel ID="pnlSeguranca" runat="server" HeaderText=" " HeaderStyle-Font-Size="11px"
                                                        Width="459px" ClientSideEvents-Init="function(s,e) { SetaMaxLength(); }">
                                                        <PanelCollection>
                                                            <dxp:PanelContent runat="server">
                                                                <dxtc:ASPxPageControl ID="tabCadastro" runat="server" ClientInstanceName="tabCadastro"
                                                                    EnableHierarchyRecreation="True" ActiveTabIndex="0" Height="100%" Width="100%"
                                                                    TabSpacing="0px" EnableClientSideAPI="true">
                                                                    <TabPages>
                                                                        <dxtc:TabPage Text="Lâmina 1" Name="tab1">
                                                                            <ContentCollection>
                                                                                <dxw:contentcontrol runat="server">
                                                                                    <table border="0" align="center" cellpadding="2" cellspacing="2">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="label3" runat="server" CssClass="labelRequired" Text="Disclaimer:" />
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:TextBox ID="txtDisclaimerModelo1" ValidationGroup="crf_3.19.0.0" runat="server"
                                                                                                    Width="500" TextMode="MultiLine" Rows="10" CssClass="textNormal10" OnPreRender="txtDisclaimerModelo1_OnPrerender" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </dxw:contentcontrol>
                                                                            </ContentCollection>
                                                                        </dxtc:TabPage>
                                                                        <dxtc:TabPage Text="Lâmina 2" Name="tab1">
                                                                            <ContentCollection>
                                                                                <dxw:contentcontrol runat="server">
                                                                                    <table border="0" align="center" cellpadding="2" cellspacing="2">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="label7" runat="server" CssClass="labelRequired" Text="Disclaimer:" />
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:TextBox ID="txtDisclaimerModelo2" ValidationGroup="crf1_3.19.0.0" runat="server"
                                                                                                    Width="500" TextMode="MultiLine" Rows="10" CssClass="textNormal10" OnPreRender="txtDisclaimerModelo2_OnPrerender" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </dxw:contentcontrol>
                                                                            </ContentCollection>
                                                                        </dxtc:TabPage>
                                                                        <dxtc:TabPage Text="Lâmina 3" Name="tab1">
                                                                            <ContentCollection>
                                                                                <dxw:contentcontrol runat="server">
                                                                                    <table border="0" align="center" cellpadding="2" cellspacing="2">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="label13" runat="server" CssClass="labelRequired" Text="Disclaimer:" />
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:TextBox ID="txtDisclaimerModelo3" ValidationGroup="crf2_3.19.0.0" runat="server"
                                                                                                    Width="500" TextMode="MultiLine" Rows="10" CssClass="textNormal10" OnPreRender="txtDisclaimerModelo3_OnPrerender" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </dxw:contentcontrol>
                                                                            </ContentCollection>
                                                                        </dxtc:TabPage>
                                                                        <dxtc:TabPage Text="Lâmina 4" Name="tab1">
                                                                            <ContentCollection>
                                                                                <dxw:contentcontrol runat="server">
                                                                                    <table border="0" align="center" cellpadding="2" cellspacing="2">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="label17" runat="server" CssClass="labelRequired" Text="Disclaimer:" />
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:TextBox ID="txtDisclaimerModelo4" ValidationGroup="crf3_3.19.0.0" runat="server"
                                                                                                    Width="500" TextMode="MultiLine" Rows="10" CssClass="textNormal10" OnPreRender="txtDisclaimerModelo4_OnPrerender" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </dxw:contentcontrol>
                                                                            </ContentCollection>
                                                                        </dxtc:TabPage>
                                                                        <dxtc:TabPage Text="Lâmina 5" Name="tab1">
                                                                            <ContentCollection>
                                                                                <dxw:contentcontrol runat="server">
                                                                                    <table border="0" align="center" cellpadding="2" cellspacing="2">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="label21" runat="server" CssClass="labelRequired" Text="Disclaimer:" />
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:TextBox ID="txtDisclaimerModelo5" ValidationGroup="crf4_3.19.0.0" runat="server"
                                                                                                    Width="500" TextMode="MultiLine" Rows="10" CssClass="textNormal10" OnPreRender="txtDisclaimerModelo5_OnPrerender" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </dxw:contentcontrol>
                                                                            </ContentCollection>
                                                                        </dxtc:TabPage>
                                                                        <dxtc:TabPage Text="Lâmina 6" Name="tab1">
                                                                            <ContentCollection>
                                                                                <dxw:contentcontrol runat="server">
                                                                                    <table border="0" align="center" cellpadding="2" cellspacing="2">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="label25" runat="server" CssClass="labelRequired" Text="Disclaimer:" />
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:TextBox ID="txtDisclaimerModelo6" ValidationGroup="crf5_3.19.0.0" runat="server"
                                                                                                    Width="500" TextMode="MultiLine" Rows="10" CssClass="textNormal10" OnPreRender="txtDisclaimerModelo6_OnPrerender" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </dxw:contentcontrol>
                                                                            </ContentCollection>
                                                                        </dxtc:TabPage>
                                                                        <dxtc:TabPage Text="Extrato" Name="tab1">
                                                                            <ContentCollection>
                                                                                <dxw:contentcontrol runat="server">
                                                                                    <table border="0" align="center" cellpadding="2" cellspacing="2">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="label8" runat="server" CssClass="labelRequired" Text="Disclaimer:" />
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:TextBox ID="txtDisclaimerExtrato" ValidationGroup="1.41.1.0" runat="server"
                                                                                                    Width="500" TextMode="MultiLine" Rows="10" CssClass="textNormal10" OnPreRender="txtDisclaimerExtrato_OnPrerender" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </dxw:contentcontrol>
                                                                            </ContentCollection>
                                                                        </dxtc:TabPage>
                                                                    </TabPages>
                                                                </dxtc:ASPxPageControl>
                                                                <div class="linkButton linkButtonTbar" style="padding-left: 50pt">
                                                                    <div class="linkButtonWrapper">
                                                                        <asp:LinkButton ID="LinkButton1" runat="server" Font-Overline="false" CssClass="btnOK"
                                                                            OnClick="btnSalvar_Click">
                                                                            <asp:Literal ID="Literal1" runat="server" Text="Salvar" /><div>
                                                                            </div>
                                                                        </asp:LinkButton>
                                                                    </div>
                                                                </div>
                                                            </dxp:PanelContent>
                                                        </PanelCollection>
                                                    </dxrp:ASPxRoundPanel>
                                                </td>
                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
