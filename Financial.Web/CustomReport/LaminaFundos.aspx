﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LaminaFundos.aspx.cs"
    Inherits="CustomReport_LaminaFundos" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxw" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../js/global.js"></script>

    <script type="text/javascript" language="Javascript">
        
        function formatDate(date) {
            var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [year, month, day].join('-');
        }
        
        function OnGetSelectedFieldValues(selectedValues) {
          
            if(selectedValues.length == 0) {
                alert('Favor selecionar um fundo');
                return false;
            }
            
            if(!textDataFim.GetValue()){
                alert('Favor selecionar uma data');
                return false;
            }
            
            var data = formatDate(textDataFim.GetValue());
            var gestorID = 10017;
            var gestorNome = "Porto Seguro Investimentos";
            var tipoReport = 3;//lamina fundos
                        
            for(i = 0; i < selectedValues.length; i++) {
                var clienteID = selectedValues[i][0];
                var nomeFundo = selectedValues[i][5];
                var categorias = selectedValues[i][2];
                var nomeCategoria = selectedValues[i][3];
                var nomeSubCategoria = selectedValues[i][4];
                
                var params = 'clienteID=' + clienteID + '&data=' + data + '&tipoExibicao=' + window.tipoExibicao + '&tipoReport=' + tipoReport + '&login=' + login + 
                    '&nomeFundo=' + nomeFundo + '&categorias=' + categorias + '&nomeCategoria=' + nomeCategoria + '&nomeSubCategoria=' + nomeSubCategoria + 
                    '&gestorID=' + gestorID + '&gestorNome=' + gestorNome;
                
                var url = urlComercialWebReports + '/WebFormReportView.aspx?' + params;
                
                if(tipoExibicao == 'Preview'){
                    window.open(url);
                }else{
                    var iframe = document.createElement('iframe');
                    iframe.style.display = 'none';
                    iframe.src = url;
                    document.body.appendChild(iframe);
                }
                
            }
            
            return false;        
            
            
            //OLD CODE:
            /*
            var clienteIDs = [];
            var nomeFundos = [];
            var categoriasParam = [];//modelos
            var nomeCategorias = [];//categoriafundo
            var nomeSubCategorias = [];//subcategoriafundo
            
                        
            for(i = 0; i < selectedValues.length; i++) {
                clienteIDs.push(selectedValues[i][0]);
                nomeFundos.push(selectedValues[i][1]);
                categoriasParam.push(selectedValues[i][2]);
                nomeCategorias.push(selectedValues[i][3]);
                nomeSubCategorias.push(selectedValues[i][4]);
                
                
            }
            
            var clienteID = clienteIDs.join(';');
            var nomeFundo = nomeFundos.join(';');
            var categorias = categoriasParam.join(';');
            var nomeCategoria = nomeCategorias.join(';');
            var nomeSubCategoria = nomeSubCategorias.join(';');
            var gestorID = 10017;
            var gestorNome = "Porto Seguro Investimentos";
                        
            var tipoReport = 3;//lamina fundos
            
            var params = 'clienteID=' + clienteID + '&data=' + data + '&tipoExibicao=' + window.tipoExibicao + '&tipoReport=' + tipoReport + '&login=' + login + 
            '&nomeFundo=' + nomeFundo + '&categorias=' + categorias + '&nomeCategoria=' + nomeCategoria + '&nomeSubCategoria=' + nomeSubCategoria + 
            '&gestorID=' + gestorID + '&gestorNome=' + gestorNome;
            
            var url = urlComercialWebReports + '/WebFormReportView.aspx?' + params;
            
            window.open(url);
            */
            //sample data:
            //object SClienteID = new List<int>() { 2000080950 };
            //DateTime SData = new DateTime(2014,5,1);
            //string STipoExibicao = "Preview";
            //object STipoReport = TipoRelatorio.ExtratoMensal (1);
            
        }
        
    </script>

</head>
<body>
    <form id="form1" runat="server">
        
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Lâmina de Fundos" />
                            </div>
                            <div id="mainContent">
                                <table class="dropDownInlineWrapper">
                                    <tr>
                                        <td>
                                            <asp:Label ID="labelGrupoProcessamento" runat="server" CssClass="labelNormal" Text="Grupo:"></asp:Label>
                                        </td>
                                        <td>
                                            <dxe:ASPxComboBox ID="dropGrupoProcessamento" runat="server" ClientInstanceName="dropGrupoProcessamento"
                                                DataSourceID="EsDSGrupoProcessamento" ShowShadow="true" DropDownStyle="DropDownList"
                                                CssClass="dropDownListCurto1 dropDownInline" TextField="Descricao" ValueField="IdGrupoProcessamento">
                                                <ClientSideEvents SelectedIndexChanged="function(s, e) {gridConsulta.PerformCallback('btnRefresh'); return false;}" />
                                            </dxe:ASPxComboBox>
                                        </td>
                                        <td>
                                            <asp:Label ID="labelDataFim" runat="server" CssClass="labelRequired" Text="Data:"></asp:Label>
                                        </td>
                                        <td>
                                            <dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim" EditFormatString="MM/yyyy">
                                            </dxe:ASPxDateEdit>
                                        </td>
                                    </tr>
                                </table>
                                <div class="linkButton">
                                   
                                    <asp:LinkButton ID="LinkButton1" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnView" OnClientClick="
                                            window.tipoExibicao = 'Preview';
                                            var selected = gridConsulta.GetSelectedFieldValues('IdCliente;Nome;Logotipo;BookEmail;BookMensagem;BookAssunto', OnGetSelectedFieldValues);
                                            return false;
                                        ">
                                        <asp:Literal ID="Literal1" runat="server" Text="Visualizar" /><div>
                                        </div>
                                    </asp:LinkButton>
                                   
                                    <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnPdf" OnClientClick="
                                            window.tipoExibicao = 'PDF';
                                            var selected = gridConsulta.GetSelectedFieldValues('IdCliente;Nome;Logotipo;BookEmail;BookMensagem;BookAssunto', OnGetSelectedFieldValues);
                                            return false;
                                        ">
                                        <asp:Literal ID="Literal4" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton>
                                </div>
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridConsulta" runat="server" EnableCallBacks="true" KeyFieldName="IdCliente"
                                        DataSourceID="EsDSCliente"   OnCustomUnboundColumnData="gridConsulta_CustomUnboundColumnData" 
                                        OnCustomCallback="gridConsulta_CustomCallback">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="10%" ButtonType="Image" ShowClearFilterButton="True">
                                                <HeaderTemplate>
                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                                </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataColumn FieldName="IdCliente" VisibleIndex="1" Width="10%" CellStyle-HorizontalAlign="left" />
                                            <dxwgv:GridViewDataColumn FieldName="Nome" VisibleIndex="2" Width="30%" />
                                            <dxwgv:GridViewDataColumn Visible=false FieldName="Logotipo" VisibleIndex="3" Width="30%" />
                                            <dxwgv:GridViewDataColumn Visible=false FieldName="BookEmail" VisibleIndex="3" Width="30%" />
                                            <dxwgv:GridViewDataColumn Visible=false FieldName="BookMensagem" VisibleIndex="3" Width="30%" />
                                            <dxwgv:GridViewDataColumn Visible=false FieldName="BookAssunto" VisibleIndex="3" Width="30%" />
                                            <dxwgv:GridViewDataComboBoxColumn Caption="Tipo" FieldName="IdTipo" VisibleIndex="3"
                                                Width="20%">
                                                <EditFormSettings Visible="False" />
                                                <PropertiesComboBox DataSourceID="EsDSTipoCliente" TextField="Descricao" ValueField="IdTipo" />
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataComboBoxColumn Caption="Ativo" FieldName="StatusAtivo" VisibleIndex="4"
                                                Width="10%">
                                                <EditFormSettings Visible="False" />
                                                <PropertiesComboBox>
                                                    <Items>
                                                        <dxe:ListEditItem Value="1" Text="Sim" />
                                                        <dxe:ListEditItem Value="2" Text="Não" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataColumn FieldName="Status" Visible="False" />
                                            <dxwgv:GridViewDataTextColumn FieldName="StatusDescricao" Caption="Status" UnboundType="String"
                                                VisibleIndex="4" Width="15%" Settings-AutoFilterCondition="Contains" />
                                            <dxwgv:GridViewDataDateColumn FieldName="DataDia" VisibleIndex="6" Width="10%" />
                                            
                                        </Columns>
                                        <SettingsEditing PopupEditFormWidth="500px" />
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridConsulta"
            Landscape="true" />
        <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect"
            LowLevelBind="true" />
        <cc1:esDataSource ID="EsDSTipoCliente" runat="server" OnesSelect="EsDSTipoCliente_esSelect" />    
        <cc1:esDataSource ID="EsDSGrupoProcessamento" runat="server" OnesSelect="EsDSGrupoProcessamento_esSelect" />
    </form>
</body>
</html>
