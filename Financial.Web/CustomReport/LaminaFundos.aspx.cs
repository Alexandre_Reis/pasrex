﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using Financial.Investidor;
using Financial.ContaCorrente;
using System.Drawing;
using Financial.ContaCorrente.Enums;
using Financial.Security;
using Financial.Fundo;
using Financial.Bolsa;
using Financial.BMF;
using Financial.RendaFixa;
using Financial.Investidor.Enums;
using Financial.CRM;
using Financial.Bolsa.Enums;
using Financial.BMF.Enums;
using EntitySpaces.Interfaces;
using Financial.InvestidorCotista;
using Financial.WebConfigConfiguration;
using Financial.Security.Enums;
using System.Collections.Specialized;
using Financial.Swap;
using DevExpress.Web.Data;
using Financial.Contabil;
using Financial.Web.Util;
using Financial.Common.Enums;
using Financial.Captacao.Enums;
using Financial.Web.Common;

public partial class CustomReport_LaminaFundos : ConsultaBasePage
{
    new protected void Page_Load(object sender, EventArgs e)
    {

        base.Page_Load(sender, e);

        gridConsulta.EnableViewState = true;
        gridConsulta.Settings.ShowVerticalScrollBar = true;
        gridConsulta.Settings.VerticalScrollableHeight = 340;
        gridConsulta.SettingsPager.PageSize = 200;

        string login = HttpContext.Current.User.Identity.Name;
        string urlComercialWebReports = WebConfig.AppSettings.UrlComercialWebReports.Trim();

        StringBuilder javascript = new StringBuilder();
        javascript.AppendFormat("var login = '{0}';", login);
        javascript.AppendLine();
        javascript.AppendFormat("var urlComercialWebReports = '{0}';", urlComercialWebReports);
        javascript.AppendLine();

        Page.ClientScript.RegisterStartupScript(this.GetType(), "initJavascriptVars",
       "<script type=\"text/javascript\">" +
       javascript.ToString() +
       "</script>");

    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {

        #region Consulta
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        ClienteQuery clienteQuery = new ClienteQuery("C");
        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
        clienteQuery.Select(clienteQuery);
        clienteQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        clienteQuery.Where(permissaoClienteQuery.IdUsuario.Equal(idUsuario));
        clienteQuery.Where(clienteQuery.TipoControle.NotEqual((byte)TipoControleCliente.ApenasCotacao),
                           clienteQuery.TipoControle.NotEqual((byte)TipoControleCliente.CarteiraImportada),
                           clienteQuery.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo),
                           clienteQuery.IsProcessando.Equal("N"));

        if (dropGrupoProcessamento.SelectedIndex > 0)
        {
            clienteQuery.Where(clienteQuery.IdGrupoProcessamento == dropGrupoProcessamento.SelectedItem.Value);
        }

        clienteQuery.OrderBy(clienteQuery.Nome.Ascending);

        ClienteCollection coll = new ClienteCollection();
        coll.Load(clienteQuery);


        clienteQuery = new ClienteQuery("C");
        CarteiraQuery carteiraQuery = new CarteiraQuery("A");
        clienteQuery.Select(clienteQuery);
        clienteQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == clienteQuery.IdCliente);
        clienteQuery.Where(clienteQuery.TipoControle.Equal((byte)TipoControleCliente.ApenasCotacao),
                           clienteQuery.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo),
                           clienteQuery.IsProcessando.Equal("N"),
                           carteiraQuery.TipoVisaoFundo.NotEqual((byte)TipoVisaoFundoRebate.NaoTrata));

        if (dropGrupoProcessamento.SelectedIndex > 0)
        {
            clienteQuery.Where(clienteQuery.IdGrupoProcessamento == dropGrupoProcessamento.SelectedItem.Value);
        }

        clienteQuery.OrderBy(clienteQuery.Nome.Ascending);

        ClienteCollection coll2 = new ClienteCollection();
        coll2.Load(clienteQuery);

        coll.Combine(coll2);

        CadastroComplementarCamposCollection campos = new CadastroComplementarCamposCollection();
        campos.Query.Where(campos.Query.NomeCampo == "SG_MODELO_LAMINA_FUNDO");
        campos.Load(campos.Query);
        int idCampo = campos[0].IdCamposComplementares.Value;

        CadastroComplementarCollection cadastroComplementarCollection = new CadastroComplementarCollection();
        cadastroComplementarCollection.Query.Where(cadastroComplementarCollection.Query.IdCamposComplementares == idCampo);
        cadastroComplementarCollection.Load(cadastroComplementarCollection.Query);

        Dictionary<int, string> dicCadastro = new Dictionary<int, string>();
        foreach (CadastroComplementar cadastroComplementar in cadastroComplementarCollection)
        {
            int key = Convert.ToInt32(cadastroComplementar.IdMercadoTipoPessoa);
            if (!dicCadastro.ContainsKey(key))
            {
                dicCadastro.Add(key, cadastroComplementar.ValorCampo);
            }
        }

        ClienteCollection clientesNew = new ClienteCollection();
        CategoriaFundoCollection categorias = new CategoriaFundoCollection();
        SubCategoriaFundoCollection subCategorias = new SubCategoriaFundoCollection();
        CarteiraCollection carteiras = new CarteiraCollection();
        categorias.LoadAll();
        subCategorias.LoadAll();
        carteiras.LoadAll();

        foreach (Cliente cliente in coll)
        {
            if (dicCadastro.ContainsKey(cliente.IdCliente.Value))
            {
                cliente.Logotipo = MapCategoria(dicCadastro[cliente.IdCliente.Value]);

                Carteira carteira = carteiras.FindByPrimaryKey(cliente.IdCliente.Value);
                if (carteira != null)
                {
                    if (carteira.IdCategoria.HasValue)
                    {
                        cliente.BookEmail = categorias.FindByPrimaryKey(carteira.IdCategoria.Value).Descricao;
                    }
                    if (carteira.IdSubCategoria.HasValue)
                    {
                        SubCategoriaFundo subCategoriaFundo = new SubCategoriaFundo();
                        subCategoriaFundo = subCategorias.FindByPrimaryKey(carteira.IdCategoria.Value, carteira.IdSubCategoria.Value);
                        if (subCategoriaFundo != null)
                            cliente.BookMensagem = subCategoriaFundo.Descricao;
                    }
                }

                CadastroComplementarCamposCollection camposCadastro = new CadastroComplementarCamposCollection();
                camposCadastro.Query.Where(camposCadastro.Query.NomeCampo == "NomeFundo",
                    camposCadastro.Query.TipoCadastro == -8);

                cliente.BookAssunto = cliente.Nome;
                camposCadastro.Load(camposCadastro.Query);
                if (camposCadastro.Count > 0)
                {
                    CadastroComplementarCampos campo = camposCadastro[0];

                    CadastroComplementarCollection cadastroComplementarCol = new CadastroComplementarCollection();

                    cadastroComplementarCol.Query.Where(cadastroComplementarCol.Query.IdCamposComplementares == campo.IdCamposComplementares,
                        cadastroComplementarCol.Query.IdMercadoTipoPessoa == cliente.IdCliente.Value.ToString());
                    cadastroComplementarCol.Load(cadastroComplementarCol.Query);

                    if (cadastroComplementarCol.Count > 0)
                    {
                        cliente.BookAssunto = cadastroComplementarCol[0].ValorCampo;
                    }
                }

                clientesNew.Add(cliente);
            }
        }

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = clientesNew;
        #endregion


        #region Consulta varios registros
        // *******************************************************************************************

        //ClienteQuery clienteQuery = new ClienteQuery("C");
        //clienteQuery.Select(clienteQuery);
        //clienteQuery.OrderBy(clienteQuery.Apelido.Ascending);

        //ClienteCollection coll = new ClienteCollection();
        //coll.Load(clienteQuery);

        //e.Collection = coll;
        // *******************************************************************************************
        #endregion


        if (!Page.IsPostBack)
        {
            DevExpress.Web.ListEditItem item = new DevExpress.Web.ListEditItem();
            item.Text = " ";
            dropGrupoProcessamento.Items.Insert(0, item);

        }
    }

    private string MapCategoria(string valorCampo)
    {
        string mapped = "";
        if (valorCampo == "1-Bolsa")
        {
            mapped = "RPT_LAMINA_CATEGORIA_MODELO1";
        }
        else if (valorCampo == "2-RF e Ref. DI (Fundo Invest.)")
        {
            mapped = "RPT_LAMINA_CATEGORIA_MODELO2";
        }
        else if (valorCampo == "3-RF e Ref. DI (Fundo Prev.)")
        {
            mapped = "RPT_LAMINA_CATEGORIA_MODELO22";
        }
        else if (valorCampo == "4-Multiestrategia")
        {
            mapped = "RPT_LAMINA_CATEGORIA_MODELO4";
        }
        else if (valorCampo == "5-Alocacao (Fundo Invest.)")
        {
            mapped = "RPT_LAMINA_CATEGORIA_MODELO5";
        }
        else if (valorCampo == "6-Alocacao (Fundo Prev.)")
        {
            mapped = "RPT_LAMINA_CATEGORIA_MODELO52";
        }
        else if (valorCampo == "7-Alocacao (Fundo Porto Prev.)")
        {
            mapped = "RPT_LAMINA_CATEGORIA_MODELO53";
        }
        else if (valorCampo == "8-RF e Ref. DI (Credito Priv.)")
        {
            mapped = "RPT_LAMINA_CATEGORIA_MODELO3";
        }
        else
        {
            mapped = "RPT_LAMINA_CATEGORIA_MODELO1";
        }

        return mapped;
    }

    protected void EsDSTipoCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        TipoClienteCollection coll = new TipoClienteCollection();
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSGrupoProcessamento_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        GrupoProcessamentoCollection coll = new GrupoProcessamentoCollection();
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        List<object> keyValuesId = gridConsulta.GetSelectedFieldValues("IdCliente");

        if (keyValuesId.Count == 0)
        {
            e.Result = "Algum cliente deve ser selecionado!";
            return;
        }

        if (textDataFim.Text == "")
        {
            e.Result = "A Data Final deve ser informada!";
            return;
        }

        foreach (object objectId in keyValuesId)
        {
            int idCliente = (int)objectId;
            DateTime dataFinal = Convert.ToDateTime(textDataFim.Text);

            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.DataDia);
            campos.Add(cliente.Query.IdMoeda);
            //
            campos.Add(cliente.Query.DataBloqueioProcessamento);
            //
            cliente.LoadByPrimaryKey(campos, idCliente);
            DateTime dataDia = cliente.DataDia.Value;
            //
            DateTime? dataBloqueioProcessamento = null;
            if (cliente.DataBloqueioProcessamento.HasValue)
            {
                dataBloqueioProcessamento = cliente.DataBloqueioProcessamento.Value;
            }

            //
            if (cliente.IdMoeda == (int)ListaMoedaFixo.Real)
            {
                if (!Calendario.IsDiaUtil(Convert.ToDateTime(textDataFim.Text)))
                {
                    e.Result = "Data Final não é dia útil para o cliente " + idCliente.ToString() + ".";
                    return;
                }
            }
            else
            {
                if (!Calendario.IsDiaUtil(Convert.ToDateTime(textDataFim.Text), LocalFeriadoFixo.NovaYork, TipoFeriado.Outros))
                {
                    e.Result = "Data Final não é dia útil para o cliente " + idCliente.ToString() + ".";
                    return;
                }
            }

        }
    }

    protected void gridConsulta_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "StatusDescricao")
        {
            int status = Convert.ToInt32(e.GetListSourceFieldValue("Status"));

            string statusDescricao = TraducaoEnumsInvestidor.EnumStatusCliente.TraduzEnum(status);

            e.Value = statusDescricao;
        }
    }

    protected void gridConsulta_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        gridConsulta.DataBind();
        gridConsulta.Selection.UnselectAll();
        gridConsulta.CancelEdit();
    }

}