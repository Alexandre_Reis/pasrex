﻿using log4net;
using System;
using System.Web;
using Financial.WebConfigConfiguration;

public partial class ComercialReports : System.Web.UI.Page {
    private static readonly ILog log = LogManager.GetLogger(typeof(ComercialReports));

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e) {
        string login = HttpContext.Current.User.Identity.Name;

        string url = WebConfig.AppSettings.UrlComercialWebReports.Trim();
        string target = Request["target"];

        Response.Redirect(url + "/" + target +"?login=" + login);

    }
}