﻿using System;
using System.IO;

using Financial.WebConfigConfiguration;
using Financial.Web.Common;

using DevExpress.Web;

using Dart.PowerTCP.Zip;
using System.Xml;
using System.Collections.Generic;
using System.Threading;
using System.Data.SqlClient;
using System.Configuration;

using EntitySpaces.Core;
using EntitySpaces.Interfaces;
using System.Text;
using System.Data;
using System.Web;
using Financial.Util;
using Financial.Investidor;

public partial class _ProcessaUpdate : BasePage {
    const string ITAU = "ITAU";

    new protected void Page_Load(object sender, EventArgs e) {
    }

    #region primeira aba

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackUpdate_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";
    }

    /// <summary>
    /// Processa Upload do arquivo Financial para fazer Update
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplUpdateFinancial_FileUploadComplete(object sender, FileUploadCompleteEventArgs e) {
        int TAM_MINIMO_ARQUIVO = 35000 * 1024; // tamanho em bytes

        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!this.isExtensaoValida(e.UploadedFile.FileName.Trim())) {
            e.CallbackData = "Formato de Arquivo Inválido. Formato permitido: zip \n\n";
            return;
        }
        #endregion

        #region Trata tamanho menor que 35 megas
        if (e.UploadedFile.ContentLength < TAM_MINIMO_ARQUIVO) {
            e.CallbackData = "Confira arquivo novamente. Tamanho Inconsistente";
            return;
        }
        #endregion

        #region Deleta todos os files da Aplicação com exceção do arquivo web.config. Diretorios não são Deletados
        string pathDelete = DiretorioAplicacao.DiretorioBaseAplicacao;

        this.DeleteFiles(pathDelete);

        #endregion

        // Stream de bytes com o Conteudo do Arquivo zip/rar
        Stream sr = e.UploadedFile.FileContent;

        #region Arquivo Zip
        // Descompacta
        Archive arquivo = new Archive();
        arquivo.PreservePath = true;
        arquivo.Clear();
        arquivo.Overwrite = Overwrite.Always;

        string pathUnzip = pathDelete; // Diretorio da Aplicação
        if (!pathUnzip.Trim().EndsWith("\\")) {
            pathUnzip += "\\";
        }
        pathUnzip = pathUnzip.Replace("\\", "\\\\");

        try {
            arquivo.QuickUnzip(sr, pathUnzip);
        }
        catch (Exception e3) {
            e.CallbackData = "Erro - " + e3.Message;
            return;
        }
        #endregion
    }

    /// <summary>
    /// Executado no clique do botão para preencher a versão do sistema   
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>   
    protected void btnVersao_Click(object sender, EventArgs e) {
        this.LerXml();
    }

    /// <summary>
    /// Executado no carregamento inicial do Label de Versão
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lblVersion_OnPreRender(object sender, EventArgs e) {
        this.LerXml();
    }

    #endregion

    #region segunda aba

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackUpdateWebConfig_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";
    }

    /// <summary>
    /// Processa Upload do arquivo WebConfig
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplUpdateWebConfig_FileUploadComplete(object sender, FileUploadCompleteEventArgs e) {

        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!this.isExtensaoValida(e.UploadedFile.FileName.Trim())) {
            e.CallbackData = "Formato de Arquivo Inválido. Formato permitido: zip \n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo zip/rar
        Stream sr = e.UploadedFile.FileContent;

        #region Arquivo Zip
        // Descompacta
        Archive arquivo = new Archive();
        arquivo.PreservePath = true;
        arquivo.Clear();
        arquivo.Overwrite = Overwrite.Always;

        string pathUnzip = DiretorioAplicacao.DiretorioBaseAplicacao;
        if (!pathUnzip.Trim().EndsWith("\\")) {
            pathUnzip += "\\";
        }
        pathUnzip = pathUnzip.Replace("\\", "\\\\");

        try {
            arquivo.QuickUnzip(sr, pathUnzip);
        }
        catch (Exception e3) {
            e.CallbackData = "Erro - " + e3.Message;
            return;
        }
        #endregion
    }

    #endregion

    #region terceira aba
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackFinancialParcial_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";
    }

    /// <summary>
    /// Processa Upload de Arquivos Parciais do Financial
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplUpdateFinancialParcial_FileUploadComplete(object sender, FileUploadCompleteEventArgs e) {

        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!this.isExtensaoValida(e.UploadedFile.FileName.Trim())) {
            e.CallbackData = "Formato de Arquivo Inválido. Formato permitido: zip \n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo zip/rar
        Stream sr = e.UploadedFile.FileContent;

        #region Arquivo Zip
        // Descompacta
        Archive arquivo = new Archive();
        arquivo.PreservePath = true;
        arquivo.Clear();
        arquivo.Overwrite = Overwrite.Always;

        string pathUnzip = DiretorioAplicacao.DiretorioBaseAplicacao;
        if (!pathUnzip.Trim().EndsWith("\\")) {
            pathUnzip += "\\";
        }
        pathUnzip = pathUnzip.Replace("\\", "\\\\");

        try {
            arquivo.QuickUnzip(sr, pathUnzip);
        }
        catch (Exception e3) {
            e.CallbackData = "Erro - " + e3.Message;
            return;
        }
        #endregion
    }

    #endregion

    #region quarta aba

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackUpdateScripts_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        Financial.Fundo.Carteira carteira = new Financial.Fundo.Carteira();
        string connectionString = carteira.es.Connection.ConnectionString;



        try {
            using (SqlConnection connection = new SqlConnection(
                   connectionString)) {
                int numeroRegistrosAfetados = 0;
                string[] commandStrings = this.textScriptInput.Text.Split(new string[1] { "\n" }, StringSplitOptions.None);
                foreach (string commandString in commandStrings) {
                    if (commandString.Trim().ToLower() != "go" && (commandString.Trim().Length > 0)) {
                        SqlCommand command = new SqlCommand(commandString, connection);
                        if (command.Connection.State != System.Data.ConnectionState.Open) {
                            command.Connection.Open();
                        }
                        numeroRegistrosAfetados += command.ExecuteNonQuery();
                    }
                }

                e.Result = numeroRegistrosAfetados.ToString() + " registro(s) atualizado(s)";
            }
        }
        catch (Exception exception) {
            e.Result = exception.Message;
        }


    }
    #endregion

    #region Funções Auxiliares

    /// <summary>
    /// false se arquivo não possui extensão zip
    /// true se arquivo possui extensão zip
    /// </summary>
    /// <param name="arquivo">Nome do arquivo</param>
    /// <returns></returns>
    private bool isExtensaoValida(string arquivo) {
        string extensao = new FileInfo(arquivo).Extension;
        extensao = extensao.ToLower();
        if (extensao != ".zip") {
            return false;
        }
        return true;
    }

    /// <summary>
    /// Deleta todos os arquivos de todos os subdiretorios com exceção do arquivo webconfig.
    /// Diretorios não são deletados
    /// Cuidado: Função recursiva
    /// </summary>
    /// <param name="target_dir"></param>
    /// <returns></returns>
    private bool DeleteFiles(string target_dir) {

        bool result = false;

        string[] files = Directory.GetFiles(target_dir);
        string[] dirs = Directory.GetDirectories(target_dir);

        List<string> diretorioList = new List<string>(dirs.Length);

        // Remove Imagens Personalizadas para garantir
        foreach (string dir in dirs) {
            string[] diretorioAux = dir.Split(new Char[] { '\\' });
            string nomeDiretorio = diretorioAux[diretorioAux.Length - 1];

            if (nomeDiretorio != "imagensPersonalizadas") {
                diretorioList.Add(dir);
            }
        }

        foreach (string file in files) {
            File.SetAttributes(file, FileAttributes.Normal);

            string[] arquivo = file.Split(new Char[] { '\\' });
            string nomeArquivo = arquivo[arquivo.Length - 1];

            // Não deleta webConfig
            if (nomeArquivo != "web.config" && nomeArquivo != "Web.Config") {
                File.Delete(file);
            }
        }

        for (int i = 0; i < diretorioList.Count; i++) {
            DeleteFiles(diretorioList[i]); // Recursiva
        }

        //foreach (string dir in dirs) {
        //    string[] diretorioAux = dir.Split(new Char[] { '\\' });
        //    string nomeDiretorio = diretorioAux[diretorioAux.Length - 1];

        //    if (nomeDiretorio != "imagensPersonalizadas") {
        //        DeleteFiles(dir); // Recursiva
        //    }

        //    //try
        //    //{
        //    //    if (!Directory.Exists(target_dir))
        //    //    {
        //    //        Directory.Delete(target_dir, false);
        //    //    }
        //    //}
        //    //catch (IOException)
        //    //{
        //    //    /* If you are getting IOException deleting directories that are open in Explorer, add a sleep(0) 
        //    //     * to give Explorer a chance to release the directory handle.
        //    //     */
        //    //    Thread.Sleep(0);

        //    //    if (!Directory.Exists(target_dir))
        //    //    {
        //    //        Directory.Delete(target_dir, false);
        //    //    }
        //    //}
        //}

        return result;
    }

    /// <summary>
    /// Cria o Diretorio de Downloads de acordo com a entrada do webconfig
    /// </summary>
    /// <returns>True se conseguiu criar o diretorio, false caso contrario</returns>
    [Obsolete("Não mais necessário")]
    private bool CreateDownloadsDirectory() {
        bool retorno = false;

        string path = DiretorioAplicacao.DiretorioBaseAplicacao + WebConfig.AppSettings.DiretorioDownloads;
        // Se Path Acaba com barra remove a barra
        if (path.Trim().EndsWith(@"\")) {
            // remove o ultimo caracter
            int ultimaPosicao = path.Length - 1;
            path = path.Remove(ultimaPosicao, 1);
        }

        if (!Directory.Exists(@path)) {
            // Create Dir
            try {
                DirectoryInfo di = Directory.CreateDirectory(@path);
                retorno = true;
            }
            catch (Exception e1) {
                retorno = false;
            }
        }

        return retorno;
    }

    /// <summary>
    /// Le um arquivo xml e preenche a versão do Financial no label 
    /// </summary>
    private void LerXml() {
        string filename = DiretorioAplicacao.DiretorioBaseAplicacao + "Data/version.xml";

        try {

            XmlTextReader tr = new XmlTextReader(filename);

            List<string> info = new List<string>();
            /* [0]=version | [1]=date */
            while (tr.Read()) {
                if (tr.NodeType == XmlNodeType.Text) {
                    info.Add(tr.Value);
                }
            }
            tr.Close();

            if (!String.IsNullOrEmpty(info[0]) && !String.IsNullOrEmpty(info[1])) {
                this.lblVersion.Text = " " + info[0] + " - Data: " + info[1];
            }
        }
        catch (Exception e1) {
        }
    }
    #endregion

    // Obsolete - Transferido para o arquivo .ashx    
    #region 5 aba

    //#region Estrutura Arquivo
    //public class ValoresArquivo {
    //    public string NomeArquivo;
    //    public DateTime DataArquivo;
    //}
    //#endregion

    /// <summary>
    /// Faz Update dos scripts e Atualiza a Tabela versaoSchema do banco de dados com a data do último script
    /// Não suportado para o Itau devido ao componente SMO
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackRunScripts_Callback(object source, DevExpress.Web.CallbackEventArgs e) {        
        //#region Retorna se for Cliente Itau
        //string cliente = WebConfig.AppSettings.Cliente;
        //if (!String.IsNullOrEmpty(cliente)) {
        //    cliente = cliente.ToUpper().Trim();
        //}
        //if (cliente == ITAU) {
        //    e.Result = "Não Suportado no Itau";
        //    return;
        //} 
        //#endregion
                
        //Financial.Fundo.Carteira carteira = new Financial.Fundo.Carteira();
        //string connectionString = carteira.es.Connection.ConnectionString;

        //#region Pega Arquivos de Scripts
        //List<ValoresArquivo> valoresArquivo = new List<ValoresArquivo>();

        //// Pegar todos os Scripts do Diretorio        
        //string pathDiretorio = DiretorioAplicacao.DiretorioBaseAplicacao + "Data/UpdateScripts/";

        //DirectoryInfo dir = new DirectoryInfo(pathDiretorio);
        ////Console.WriteLine("Directory: {0}", dir.FullName);

        //foreach (FileInfo file in dir.GetFiles("*.sql")) {
        //    //Console.WriteLine("File: {0}", file.Name);
        //    ValoresArquivo v = new ValoresArquivo();
        //    v.NomeArquivo = file.Name;
        //    //
        //    // Formato do nome do Arquivo Sql = dd-MM-yyyy
        //    int ano = Convert.ToInt32(v.NomeArquivo.Substring(0, 4));
        //    int mes = Convert.ToInt32(v.NomeArquivo.Substring(5, 2));
        //    int dia = Convert.ToInt32(v.NomeArquivo.Substring(8, 2));

        //    v.DataArquivo = new DateTime(ano, mes, dia);
        //    //
        //    valoresArquivo.Add(v);
        //}

        //// Ordena por DataArquivo
        //valoresArquivo.Sort(delegate(ValoresArquivo v1, ValoresArquivo v2) {            
        //        return v1.DataArquivo.CompareTo(v2.DataArquivo);            
        //});
        //#endregion

        //string sql = " SELECT top(1) * FROM VersaoSchema ORDER by 1 DESC ";
        ////
        //// Data Última Atualização no banco de Dados
        //DateTime dataUltimaAtualizacao = Convert.ToDateTime( new esUtility().ExecuteScalar(esQueryType.Text, sql, "") );
        ////

        //// Scripts a serem executados
        //List<ValoresArquivo> scriptsExecutar = new List<ValoresArquivo>();
        //for (int j = 0; j < valoresArquivo.Count; j++) {
        //    if (valoresArquivo[j].DataArquivo > dataUltimaAtualizacao) {
        //        scriptsExecutar.Add(valoresArquivo[j]);
        //    }
        //}

        //StringBuilder messageSql = new StringBuilder(); ;

        //for (int k = 0; k < scriptsExecutar.Count; k++) {            
        //    string arq = pathDiretorio + "/" + scriptsExecutar[k].NomeArquivo;
        //    //
        //    FileInfo file = new FileInfo(arq);

        //    string script = file.OpenText().ReadToEnd();

        //    string nomeArquivo = scriptsExecutar[k].NomeArquivo;
        //    DateTime dataArquivo = scriptsExecutar[k].DataArquivo;

        //    try
        //    {

        //        using (SqlConnection conn = new SqlConnection(connectionString))
        //        {

        //            /*Microsoft.SqlServer.Management.Common.ServerConnection svrConnection = new Microsoft.SqlServer.Management.Common.ServerConnection(conn);
        //            Microsoft.SqlServer.Management.Smo.Server server = new Microsoft.SqlServer.Management.Smo.Server(svrConnection);
                    
        //            server.ConnectionContext.ExecuteNonQuery(script);
                    
        //            messageSql.AppendLine("Script: " + nomeArquivo + " - Status Ok");
        //            messageSql.AppendLine("------------------------------------------------------------------------------------------------");
        //            messageSql.AppendLine();*/
        //        }
        //    }
        //    catch (Exception exception) {
        //        #region Erro
        //        messageSql.AppendLine("Script: " + nomeArquivo + " - Erro Execução: ");
        //        messageSql.AppendLine(exception.ToString());
        //        messageSql.AppendLine("------------------------------------------------------------------------------------------------");
        //        messageSql.AppendLine();

        //        //
        //        esUtility u = new esUtility();

        //        esParameters esParams = new esParameters();
        //        esParams.Add("Data", dataArquivo);
        //        //
        //        // Delete da Data da Ultima Atualização
        //        string sql2 = " DELETE FROM VersaoSchema Where DataVersao >= @Data ";
        //        //
        //        u.ExecuteNonQuery(esQueryType.Text, sql2, esParams);
        //        //

        //        // Sai do for quando dá erro
        //        break; 
        //        #endregion
        //    }
        //}

        //string sql1 = " SELECT top(1) * FROM VersaoSchema ORDER by 1 DESC ";
        //DateTime dataUltimaAtualizacao1 = Convert.ToDateTime(new esUtility().ExecuteScalar(esQueryType.Text, sql1, ""));
        ////

        //e.Result = !String.IsNullOrEmpty(messageSql.ToString()) ? messageSql.ToString() + "\n\t - Data Ultima Atualização: " + dataUltimaAtualizacao1.ToString("d")
        //                                                        : "Processo Conclúido com Sucesso - Data Ultima Atualização: " + dataUltimaAtualizacao1.ToString("d");
    }    
    #endregion

    #region #6 aba - Funções relativas ao Upload de Logotipo

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackLogotipo_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";
    }
    
    /// <summary>
    /// Processa o salvamento do Logotipo após ter acabado o Upload
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uplLogotipo_FileUploadComplete(object sender, FileUploadCompleteEventArgs e) {
        e.CallbackData = "";

        #region Trata Extensão Válida
        if (!this.isExtensaoPNG(e.UploadedFile.FileName.Trim())) {
            e.CallbackData = "Formato de Arquivo Inválido. Formato permitido: .png\n\n";
            return;
        }
        #endregion

        // Stream de bytes com o Conteudo do Arquivo
        Stream sr = e.UploadedFile.FileContent;

        #region Processamento imagem Logotipo

        try {
            this.SalvaLogotipo(sr);
        }
        catch (Exception e2) {
            e.CallbackData = "Logotipo - " + e2.Message;
            return;
        }
        #endregion
    }

    public bool isExtensaoPNG(string arquivo) {
        string extensao = new FileInfo(arquivo).Extension;
        if (extensao != ".png") {
            return false;
        }
        return true;
    }

    private void SalvaLogotipo(Stream streamImagem) {
        string path = AppDomain.CurrentDomain.BaseDirectory + "\\imagensPersonalizadas\\";

        string imagemCliente = path + "logo_cliente.png";
      
        if(File.Exists(imagemCliente)) {
            File.Delete(imagemCliente);
        }

        this.SaveStreamToFile(imagemCliente, streamImagem);
    }

    /// <summary>
    /// Salva Stream como imagem no disco
    /// </summary>
    /// <param name="fileFullPath"></param>
    /// <param name="stream"></param>
    private void SaveStreamToFile(string fileFullPath, Stream stream) {
        if (stream.Length == 0) return;

        // Create a FileStream object to write a stream to a file
        using (FileStream fileStream = System.IO.File.Create(fileFullPath, (int)stream.Length)) {
            // Fill the bytes[] array with the stream data
            byte[] bytesInStream = new byte[stream.Length];
            stream.Read(bytesInStream, 0, (int)bytesInStream.Length);

            // Use FileStream object to write to the specified file
            fileStream.Write(bytesInStream, 0, bytesInStream.Length);
        }
    }

    #endregion

    #region Setima Aba

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackSelectSinacor_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";

        if (String.IsNullOrEmpty(this.textScriptInputSinacor.Text)) {
            e.Result = "Prencha a consulta. ";
            return;
        }

        try {
            DataTable dados = this.FillDados();

            if (dados.Rows.Count == 0) {
                e.Result = "0 Registros encontrados.";
                return;
            }

            // Salva Datatable na Session                
            Session["dataTable"] = dados;
        }
        catch (Exception e1) {
            e.Result = "Comando inválido: " + e1.Message;
            return;
        }

        string newLocation = HttpContext.Current.Request.Url.AbsolutePath;
        int posicao = newLocation.LastIndexOf("/");
        string newLocation1 = newLocation.Substring(0, posicao+1) + "ExibeProcessaUpdate.aspx"; 

        e.Result = "redirect:" + newLocation1;
    }

    private DataTable FillDados() {
        esUtility u = new esUtility();

        u.ConnectionName = "Sinacor";
        u.Schema = ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor;

        string sqlText = this.textScriptInputSinacor.Text.Trim();
        DataTable dt = u.FillDataTable(esQueryType.Text, sqlText);

        return dt;
    }

    #endregion

    #region Oitava Aba

    /// <summary>
    /// Executado no clique do botão para preencher a versão do sistema   
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>   
    protected void btnVersao1_Click(object sender, EventArgs e) {
        this.LerDataWebDeployDLL();
    }

    /// <summary>
    /// Executado no carregamento inicial do Label de Versão
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lblVersion1_OnPreRender(object sender, EventArgs e) {
        this.LerDataWebDeployDLL();
    }

    /// <summary>
    /// Coloca Informação sobre o banco sendo executado
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lblCatalog_OnPreRender(object sender, EventArgs e) {        
        string connectionString = new Banco().es.Connection.ConnectionString;
        string[] catalog = connectionString.Split(';');

        this.lblCatalog.Text = catalog[1].Substring(16);
    }

    /// <summary>
    /// Coloca Informação sobre o banco sendo executado
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lblVersao_OnPreRender(object sender, EventArgs e) {
        //
        string sql1 = " SELECT top(1) * FROM VersaoSchema ORDER by 1 DESC ";
        DateTime dataUltimaAtualizacao = Convert.ToDateTime(new esUtility().ExecuteScalar(esQueryType.Text, sql1, ""));
        //        
        this.lblVersao.Text = dataUltimaAtualizacao.ToString("d");
    }

    /// <summary>
    /// Lê a data do arquivo Financial.Web_Deploy.dll presente no diretorio Bin da Aplicação
    /// </summary>
    private void LerDataWebDeployDLL() {
        string filename = DiretorioAplicacao.DiretorioBaseAplicacao + "Bin/Financial.Web_Deploy.dll";

        DateTime? data = null;
        
        if (File.Exists(filename)) {
            data = File.GetLastWriteTime(filename);
        }

        this.lblVersion1.Text = data.HasValue ? data.Value.ToString("dd/MM/yyyy HH:mm:ss") : "Financial.Web_Deploy.dll não encontrado";                      
    }
    #endregion
}