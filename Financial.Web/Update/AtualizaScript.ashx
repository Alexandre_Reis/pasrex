<%@ WebHandler Language="C#" Class="AtualizaScript" %>

using System;
using System.IO;
using System.Data;
using System.Web;
using System.Text;

using Financial.WebConfigConfiguration;
using Financial.Web.Common;

using System.Collections.Generic;
using System.Configuration;

using EntitySpaces.Core;
using EntitySpaces.Interfaces;

using Financial.Util;
using Financial.Investidor;

using System.Data.SqlClient;

public class AtualizaScript : IHttpHandler {
    const string ITAU = "ITAU";

    public void ProcessRequest (HttpContext context) {
        StringBuilder result = ProcessaScript();
           
        /* ----------------------------------------------------------------------- */
        context.Response.ContentType = "text/html";

        StringBuilder sb = new StringBuilder();
        sb.Append("<head><title></title></head>");
        sb.Append("<body>");
        sb.Append("<br><br><br>");        
        sb.Append("<center>");
               
        sb.Append("<textarea cols='95' rows='25'>");
        
        if(!String.IsNullOrEmpty(result.ToString())) {
            sb.Append(result);
        }

        sb.Append("</textarea>");

        sb.Append("</center>");
        sb.Append("</body>");

        context.Response.Write(sb.ToString());
    } 

    public bool IsReusable {
        get {
            return false;
        }
    }


    #region 5 aba

    #region Estrutura Arquivo
    public class ValoresArquivo {
        public string NomeArquivo;
        public DateTime DataArquivo;
    }
    #endregion

    /// <summary>
    /// Faz Update dos scripts e Atualiza a Tabela versaoSchema do banco de dados com a data do �ltimo script
    /// N�o suportado para o Itau devido ao componente SMO
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    /// <return>mensagem de erro se ocorrer</return>
    private StringBuilder ProcessaScript() {        
        StringBuilder result = new StringBuilder();

        #region Retorna se for Cliente Itau
        string cliente = WebConfig.AppSettings.Cliente;
        if (!String.IsNullOrEmpty(cliente)) {
            cliente = cliente.ToUpper().Trim();
        }
        if (cliente == ITAU) {
            result.Append("N�o Suportado no Itau");
            return result;
        } 
        #endregion
                
        Financial.Fundo.Carteira carteira = new Financial.Fundo.Carteira();
        string connectionString = carteira.es.Connection.ConnectionString;

        #region Pega Arquivos de Scripts
        List<ValoresArquivo> valoresArquivo = new List<ValoresArquivo>();

        // Pegar todos os Scripts do Diretorio        
        string pathDiretorio = DiretorioAplicacao.DiretorioBaseAplicacao + "Data/UpdateScripts/";

        DirectoryInfo dir = new DirectoryInfo(pathDiretorio);
        //Console.WriteLine("Directory: {0}", dir.FullName);

        foreach (FileInfo file in dir.GetFiles("*.sql")) {
            //Console.WriteLine("File: {0}", file.Name);
            ValoresArquivo v = new ValoresArquivo();
            v.NomeArquivo = file.Name;
            //
            // Formato do nome do Arquivo Sql = dd-MM-yyyy
            int ano = Convert.ToInt32(v.NomeArquivo.Substring(0, 4));
            int mes = Convert.ToInt32(v.NomeArquivo.Substring(5, 2));
            int dia = Convert.ToInt32(v.NomeArquivo.Substring(8, 2));

            v.DataArquivo = new DateTime(ano, mes, dia);
            //
            valoresArquivo.Add(v);
        }

        // Ordena por DataArquivo
        valoresArquivo.Sort(delegate(ValoresArquivo v1, ValoresArquivo v2) {            
                return v1.DataArquivo.CompareTo(v2.DataArquivo);            
        });
        #endregion

        string sql = " SELECT top(1) * FROM VersaoSchema ORDER by 1 DESC ";
        //
        // Data �ltima Atualiza��o no banco de Dados
        DateTime dataUltimaAtualizacao = Convert.ToDateTime( new esUtility().ExecuteScalar(esQueryType.Text, sql, "") );
        //

        // Scripts a serem executados
        List<ValoresArquivo> scriptsExecutar = new List<ValoresArquivo>();
        for (int j = 0; j < valoresArquivo.Count; j++) {
            if (valoresArquivo[j].DataArquivo > dataUltimaAtualizacao) {
                scriptsExecutar.Add(valoresArquivo[j]);
            }
        }

        StringBuilder messageSql = new StringBuilder(); ;

        for (int k = 0; k < scriptsExecutar.Count; k++) {            
            string arq = pathDiretorio + "/" + scriptsExecutar[k].NomeArquivo;
            //
            FileInfo file = new FileInfo(arq);

            string script = file.OpenText().ReadToEnd();

            string nomeArquivo = scriptsExecutar[k].NomeArquivo;
            DateTime dataArquivo = scriptsExecutar[k].DataArquivo;

            try {

                using (SqlConnection conn = new SqlConnection(connectionString)) {

                    Microsoft.SqlServer.Management.Common.ServerConnection svrConnection = new Microsoft.SqlServer.Management.Common.ServerConnection(conn);
                    Microsoft.SqlServer.Management.Smo.Server server = new Microsoft.SqlServer.Management.Smo.Server(svrConnection);
                    
                    server.ConnectionContext.ExecuteNonQuery(script);
                    
                    messageSql.AppendLine("Script: " + nomeArquivo + " - Status Ok");
                    messageSql.AppendLine("------------------------------------------------------------------------------------------------");
                    messageSql.AppendLine();
                }
            }
            catch (Exception exception) {
                #region Erro
                messageSql.AppendLine("Script: " + nomeArquivo + " - Erro Execu��o: ");
                messageSql.AppendLine(exception.ToString());
                messageSql.AppendLine("------------------------------------------------------------------------------------------------");
                messageSql.AppendLine();

                //
                esUtility u = new esUtility();

                esParameters esParams = new esParameters();
                esParams.Add("Data", dataArquivo);
                //
                // Delete da Data da Ultima Atualiza��o
                string sql2 = " DELETE FROM VersaoSchema Where DataVersao >= @Data ";
                //
                u.ExecuteNonQuery(esQueryType.Text, sql2, esParams);
                //

                // Sai do for quando d� erro
                break; 
                #endregion
            }
        }

        string sql1 = " SELECT top(1) * FROM VersaoSchema ORDER by 1 DESC ";
        DateTime dataUltimaAtualizacao1 = Convert.ToDateTime(new esUtility().ExecuteScalar(esQueryType.Text, sql1, ""));
        //

        string mensagem = "Processo Concl�ido com Sucesso - Data Ultima Atualiza��o: " + dataUltimaAtualizacao1.ToString("d");

        result = !String.IsNullOrEmpty(messageSql.ToString()) ? messageSql.AppendLine("\n\t - Data Ultima Atualiza��o: " + dataUltimaAtualizacao1.ToString("d"))
                                                              : new StringBuilder(mensagem);


        return result;
    }    
    #endregion
}