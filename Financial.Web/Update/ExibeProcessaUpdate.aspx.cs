﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using EntitySpaces.Interfaces;
using System.Collections.Generic;
using Financial.Web.Common;
using System.IO;
using System.Globalization;
using Dart.PowerTCP.Zip;
using System.Text;

public partial class _ExibeProcessaUpdate : BasePage {
       
    protected void Page_Load(object sender, EventArgs e) {

        DataTable d = (DataTable)Session["dataTable"];

        this.ExportToExcel(d);
    }

    public void ExportToExcel(DataTable dt) {
        if (dt.Rows.Count > 0) {
            string filename = "Sinacor.xls";
            StringWriter tw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(tw);
            DataGrid dgGrid = new DataGrid();
            dgGrid.DataSource = dt;
            dgGrid.DataBind();

            dgGrid.RenderControl(hw);
            //Write the HTML back to the browser.
            //Response.ContentType = application/vnd.ms-excel;


            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;

            Response.Cache.SetCacheability(HttpCacheability.Private);
            Response.ContentType = "application/xls";
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename + "");
            this.EnableViewState = false;
            Response.Write(tw.ToString());

            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.End();
        }
    }
}