﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ProcessaUpdate.aspx.cs" Inherits="_ProcessaUpdate" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxuc" %>
<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxlp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxrp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxp" %>


<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxtc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxw" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../js/global.js"></script>
    <script type="text/javascript" language="Javascript" src="../js/ext-core.js"></script>

    <script type="text/javascript" language="javascript">
        var msg = '';
        var cont = 0;
        var totalCont = 0;

        function Uploader_OnUploadComplete(args) {                
            cont += 1;
            if (args.callbackData != '') { 
                msg = msg + args.callbackData; 
            }
            
            if (cont == totalCont) {
                LoadingPanel.Hide();
                
                if (msg == '') {
                    alert('Update feito com sucesso.');
                }
                else {               
                // Se Ocorreu erro em alguma importação Concatena mensagem Concluida
                //    msg += '\n -------------------------------------------------------------------------';
                //    msg += '\n\t\t\t\tErro Atualização: ';
                    alert(msg);
                }    
            }
        }
                        
        function ChecagemContUpload(tabPanelName)
        {
            var divTabArquivos = Ext.get(tabPanelName);
            var fileInputs = divTabArquivos.query('input[type=file]');
            
            totalCont = 0;
            for(var count=0; count < fileInputs.length; count++)
            {
                if (fileInputs[count].value.length > 0)
                {
                   totalCont += 1;
                }
            }
        }
    </script>
    
    <script type="text/javascript" language="javascript">   
        function UploaderLogotipo_OnUploadComplete(args) {
            if (args.callbackData != '') { 
                alert(args.callbackData);
            }
            else {
                alert('Upload Logotipo com sucesso.');
            }            
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" />
        
        <dxcb:ASPxCallback ID="callbackUpdate" runat="server" OnCallback="callbackUpdate_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {   
            if (e.result != '') {                   
                alert(e.result);                              
            }    
            else {
                ChecagemContUpload('innerPanel_TabImportacaoFinancial'); 
                                   
                if (totalCont == 0) {
                    return;
                }
                
                msg = '';
                cont = 0;
                                
                LoadingPanel.Show();
                                
                uplUpdateFinancial.UploadFile();                
            }
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackUpdateWebConfig" runat="server" OnCallback="callbackUpdateWebConfig_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {   
            if (e.result != '') {                   
                alert(e.result);                              
            }    
            else {
                ChecagemContUpload('innerPanel_TabImportacaoWebConfig'); 
                                   
                if (totalCont == 0) {
                    return;
                }
                
                msg = '';
                cont = 0;
                                
                LoadingPanel.Show();
                                
                uplUpdateWebConfig.UploadFile();                
            }
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackFinancialParcial" runat="server" OnCallback="callbackFinancialParcial_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {   
            if (e.result != '') {                   
                alert(e.result);                              
            }    
            else {
                ChecagemContUpload('innerPanel_TabImportacaoFinancialParcial'); 
                                   
                if (totalCont == 0) {
                    return;
                }
                
                msg = '';
                cont = 0;
                                
                LoadingPanel.Show();
                                
                uplUpdateFinancialParcial.UploadFile();                
            }
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackUpdateScripts" runat="server" OnCallback="callbackUpdateScripts_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {   
            if (e.result != '') {                   
                alert(e.result);                              
            }
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackRunScripts" runat="server" OnCallback="callbackRunScripts_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {
            if (e.result != '') 
            {
                document.getElementById('ASPxRoundPanel1_tabArquivos_textResultScript').value = e.result;
            }
        }
        " />
        </dxcb:ASPxCallback>        
        <dxcb:ASPxCallback ID="callbackLogotipo" runat="server" OnCallback="callbackLogotipo_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) {
            if (e.result != '') {            
                alert(e.result);                              
            }
            else {
                uplLogotipo.UploadFile();
            }
        }
        " />
        </dxcb:ASPxCallback>
       
       
        <dxcb:ASPxCallback ID="callbackSelectSinacor" runat="server" OnCallback="callbackSelectSinacor_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) 
        {   
            LoadingPanel1.Hide();
            if(e && e.result.length){
                if(e.result.indexOf('redirect') === 0){
                    var newLocation = (e.result.split(':'))[1];
                    
                    var iframeId = 'iframe_download';
                    var iframe = document.getElementById(iframeId);

                    if (!iframe && document.createElement && (iframe = document.createElement('iframe'))) {
               
                        iframe.name = iframe.id = iframeId;
                        iframe.width = 0;
                        iframe.height = 0;
                        iframe.src = 'about:blank';
                        document.body.appendChild(iframe);                                                                    
                    }
                    iframe.src = newLocation;
                   
                } else {
                    alert(e.result);
                }
            }
        }" />
        </dxcb:ASPxCallback>
              
        <div class="divPanel divPanelNew">
            <div id="container_small">
                <div id="header">
                </div>
                <div id="mainContent">
                    <div class="reportFilter">
                        <div class="dataMessage">
                        </div>
                        <dxrp:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" ShowHeader="False" BackColor="White" Border-BorderColor="#AECAF0">
                            <Border BorderColor="#AECAF0"></Border>
                            <PanelCollection>
                                <dxp:PanelContent runat="server">
                                    <dxtc:ASPxPageControl ID="tabArquivos" runat="server" ClientInstanceName="tabArquivos"
                                        Height="100%" Width="100%" ActiveTabIndex="0" TabSpacing="0px">                                        
                                        <TabPages>
                                            <dxtc:TabPage Text="Update Financial">
                                                <ContentCollection>
                                                    <dxw:ContentControl runat="server">
                                                        <div class="innerPanel" id="innerPanel_TabImportacaoFinancial">
                                                            <div class="clear">
                                                            </div>
                                                            <div class="formColumn">
                                                                <asp:Label ID="label1" runat="server" CssClass="labelNormal labelFloat" Text="Versão: " />
                                                                <asp:Label ID="lblVersion" runat="server" CssClass="labelNormal labelFloat" Text=""
                                                                    OnPreRender="lblVersion_OnPreRender" /><br />
                                                                <div class="linkButton linkButtonNoBorder">
                                                                    <asp:LinkButton ID="btnVersao" runat="server" Font-Overline="false" CssClass="btnRun"
                                                                        OnClick="btnVersao_Click">
                                                                        <asp:Literal ID="Literal5" runat="server" Text="Verificar Versão" /><div>
                                                                        </div>
                                                                    </asp:LinkButton>
                                                                </div>
                                                                <br />
                                                                <br />
                                                                <hr />
                                                                <br />
                                                                <asp:Label ID="label41" runat="server" CssClass="labelNormal labelFloat" Text="Update Financial (.zip) - Arquivo Zip sem o web.Config e com as DLLs do Diretorio DevExpress Correspondentes presentes no diretorio BIN da Aplicação" />
                                                                <br>
                                                                <asp:Label ID="label2" runat="server" CssClass="labelNormal labelFloat" Text="web.Config permanece inalterado" />
                                                                <br>
                                                                <font color="red">*</font>
                                                                <asp:Label ID="label4" runat="server" CssClass="labelNormal labelFloat" Text="Cuidado: Nunca Rodar ou Testar localmente pois irá apagar os arquivos do source control" />
                                                                <dxuc:ASPxUploadControl ID="uplUpdateFinancial" runat="server" ClientInstanceName="uplUpdateFinancial"
                                                                    OnFileUploadComplete="uplUpdateFinancial_FileUploadComplete" CssClass="labelNormal"
                                                                    EnableDefaultAppearance="False" FileUploadMode="OnPageLoad">
                                                                    <validationsettings maxfilesize="100000000" />
                                                                    <clientsideevents fileuploadcomplete="function(s, e) { Uploader_OnUploadComplete(e); }"></clientsideevents>
                                                                </dxuc:ASPxUploadControl>
                                                                <div class="linkButton linkButtonNoBorder">
                                                                    <asp:LinkButton ID="btnUpdate" runat="server" Font-Overline="false" CssClass="btnRun"
                                                                        OnClientClick="
                                                                    {
                                                                        if(uplUpdateFinancial.GetText()== '') {
                                                                            alert('Escolha algum arquivo para Atualização!');                                                                            
                                                                        }
                                                                        else {                                                                    
                                                                            callbackUpdate.SendCallback();                                                                             
                                                                        }
                                                                        return false;
                                                                    }
                                                                    ">
                                                                        <asp:Literal ID="Literal4" runat="server" Text="Atualizar" /><div>
                                                                        </div>
                                                                    </asp:LinkButton>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </dxw:ContentControl>
                                                </ContentCollection>
                                            </dxtc:TabPage>
                                        </TabPages>
                                        <TabPages>
                                            <dxtc:TabPage Text="Update WebConfig">
                                                <ContentCollection>
                                                    <dxw:ContentControl runat="server">
                                                        <div class="innerPanel" id="innerPanel_TabImportacaoWebConfig">
                                                            <div class="clear">
                                                            </div>
                                                            <div class="formColumn formColumnFirst">
                                                                <asp:Label ID="label5" runat="server" CssClass="labelNormal labelFloat" Text="Update WebConfig (.zip)" />
                                                                <br>
                                                                <div class="linkButton linkButtonNoBorder">
                                                                    <asp:LinkButton ID="DisplayWebConfigButton" runat="server" Font-Overline="false" CssClass="btnRun"
                                                                        OnClientClick="window.location='DisplayWebConfig.ashx'; return false;">
                                                                        <asp:Literal ID="Literal3" runat="server" Text="Exibir web.config atual" /><div>
                                                                        </div>
                                                                    </asp:LinkButton>
                                                                    </div>
                                                                <br />
                                                                <br />
                                                                <asp:Label ID="label6" runat="server" CssClass="labelNormal labelFloat" Text="Zipar somente web.Config" />
                                                                <br>
                                                                <asp:Label ID="label3" runat="server" CssClass="labelNormal labelFloat" Text="Cuidado: DiretorioBaseAplicacao e Connection String no webConfig devem estar corretos senão a aplicação cai" />
                                                                <dxuc:ASPxUploadControl ID="uplUpdateWebConfig" runat="server" ClientInstanceName="uplUpdateWebConfig"
                                                                    OnFileUploadComplete="uplUpdateWebConfig_FileUploadComplete" CssClass="labelNormal"
                                                                    EnableDefaultAppearance="False" FileUploadMode="OnPageLoad">
                                                                    <validationsettings maxfilesize="100000000" />
                                                                    <clientsideevents fileuploadcomplete="function(s, e) { Uploader_OnUploadComplete(e); }"></clientsideevents>
                                                                </dxuc:ASPxUploadControl>
                                                                <div class="linkButton linkButtonNoBorder">
                                                                    <asp:LinkButton ID="btnUpdateWebConfig" runat="server" Font-Overline="false" CssClass="btnRun"
                                                                        OnClientClick="
                                                                    {
                                                                        if(uplUpdateWebConfig.GetText()== '') {
                                                                            alert('Escolha algum arquivo para Atualização!');                                                                            
                                                                        }
                                                                        else {                                                                    
                                                                            callbackUpdateWebConfig.SendCallback();                                                                             
                                                                        }
                                                                        return false;
                                                                    }
                                                                    ">
                                                                        <asp:Literal ID="Literal2" runat="server" Text="Atualizar" /><div>
                                                                        </div>
                                                                    </asp:LinkButton>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </dxw:ContentControl>
                                                </ContentCollection>
                                            </dxtc:TabPage>
                                        </TabPages>
                                        <TabPages>
                                            <dxtc:TabPage Text="Update Financial Parcial">
                                                <ContentCollection>
                                                    <dxw:ContentControl runat="server">
                                                        <div class="innerPanel" id="innerPanel_TabImportacaoFinancialParcial">
                                                            <div class="clear">
                                                            </div>
                                                            <div class="formColumn formColumnFirst">
                                                                <asp:Label ID="label7" runat="server" CssClass="labelNormal labelFloat" Text="Update Financial Parcial (.zip)" />
                                                                <br>
                                                                <asp:Label ID="label9" runat="server" CssClass="labelNormal labelFloat" Text="Update de Arquivos Parciais do Financial " />
                                                                <br />
                                                                <asp:Label ID="label8" runat="server" CssClass="labelNormal labelFloat" Text="Estrutura de Arquivos deve ser mantida" />
                                                                <dxuc:ASPxUploadControl ID="uplUpdateFinancialParcial" runat="server" ClientInstanceName="uplUpdateFinancialParcial"
                                                                    OnFileUploadComplete="uplUpdateFinancialParcial_FileUploadComplete" CssClass="labelNormal"
                                                                    EnableDefaultAppearance="False" FileUploadMode="OnPageLoad">
                                                                    <validationsettings maxfilesize="100000000" />
                                                                    <clientsideevents fileuploadcomplete="function(s, e) { Uploader_OnUploadComplete(e); }"></clientsideevents>
                                                                </dxuc:ASPxUploadControl>
                                                                <div class="linkButton linkButtonNoBorder">
                                                                    <asp:LinkButton ID="LinkButton3" runat="server" Font-Overline="false" CssClass="btnRun"
                                                                        OnClientClick="
                                                                    {
                                                                        if(uplUpdateFinancialParcial.GetText()== '') {
                                                                            alert('Escolha algum arquivo para Atualização!');                                                                            
                                                                        }
                                                                        else {                                                                    
                                                                            callbackFinancialParcial.SendCallback();                                                                             
                                                                        }
                                                                        return false;
                                                                    }
                                                                    ">
                                                                        <asp:Literal ID="Literal7" runat="server" Text="Atualizar" /><div>
                                                                        </div>
                                                                    </asp:LinkButton>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </dxw:ContentControl>
                                                </ContentCollection>
                                            </dxtc:TabPage>
                                        </TabPages>
                                        <TabPages>
                                            <dxtc:TabPage Text="Update Scripts">
                                                <ContentCollection>
                                                    <dxw:ContentControl runat="server">
                                                        <div class="innerPanel" id="innerPanel_TabImportacaoScripts">
                                                            <div class="clear">
                                                            </div>
                                                            <div class="formColumn formColumnFirst">
                                                                <asp:TextBox ID="textScriptInput" runat="server" TextMode="MultiLine" Rows="15" style="width: 540px;margin-bottom: 20px;" CssClass="textLongo" />
                                                                <br />
                                                                <p style="line-height: 15px;">
                                                                Esta função é ligeiramente diferente da execução de scripts via Sql Server Management Studio. Se ela encontrar um erro em determinada linha, a execução é INTERROMPIDA, diferentemente do Management Studio que continua executando o restante dos scripts.<br />&nbsp<br />
                                                                <span style="color:red">ATENÇÃO:</span> NÃO são suportados comandos de múltiplas linhas. É necessário concatenar comandos de múltiplas linhas em uma única linha antes de executá-los !!!
                                                                </p>
                                                                <div class="linkButton linkButtonNoBorder">
                                                                    <asp:LinkButton ID="LinkButton1" runat="server" Font-Overline="false" CssClass="btnRun"
                                                                        OnClientClick="
                                                                    {
                                                                        callbackUpdateScripts.SendCallback();                                                                             
                                                                        return false;
                                                                    }
                                                                    ">
                                                                        <asp:Literal ID="Literal1" runat="server" Text="Atualizar" /><div>
                                                                        </div>
                                                                    </asp:LinkButton>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </dxw:ContentControl>
                                                </ContentCollection>
                                            </dxtc:TabPage>
                                        </TabPages>
                                        <TabPages>
                                            <dxtc:TabPage Text="Scripts Automático">
                                                <ContentCollection>
                                                    <dxw:ContentControl runat="server">
                                                       <div class="innerPanel" id="innerPanel_TabScriptsFinancial">
                                                            <div class="formColumn">
                                                                <div class="linkButton linkButtonNoBorder">
                                                                    <%--<asp:LinkButton ID="btnRunScripts" runat="server" Font-Overline="false" CssClass="btnRun"
                                                                        OnClientClick="
                                                                    {
                                                                        callbackRunScripts.SendCallback();
                                                                        return false;
                                                                    }
                                                                    ">                                                                                                                                                                                                                                                                                
                                                                        <asp:Literal ID="Literal8" runat="server" Text="Atualizar Scripts" /><div>
                                                                        </div>
                                                                    </asp:LinkButton>    --%>      
                                                                    
                                                                    
                                                                    <asp:LinkButton ID="btnRunScripts" runat="server" Font-Overline="false" CssClass="btnRun"
                                                                        OnClientClick="window.open('AtualizaScript.ashx','_blank'); return false;">
                                                                        <asp:Literal ID="Literal8" runat="server" Text="Atualizar Scripts" /><div>
                                                                        </div>
                                                                    </asp:LinkButton>
                                                                    <asp:Literal ID="Literal11" runat="server" Text="Será aberto uma nova janela..." />
                                                                                                                              
                                                                </div>
                                                            </div>
                                                            
                                                            <%--<asp:TextBox ID="textResultScript" runat="server" TextMode="MultiLine" Rows="1000" style="width: 540px;margin-bottom: 20px;" CssClass="textLongo" />--%>
                                                        </div>
                                                                                                                
                                                    </dxw:ContentControl>
                                                </ContentCollection>
                                            </dxtc:TabPage>
                                        </TabPages>
                                        <TabPages>
                                            <dxtc:TabPage Text="Upload Logotipo">
                                                <ContentCollection>
                                                    <dxw:ContentControl runat="server">
                                                        <div class="innerPanel" id="Div1">
                                                            <div class="formColumn formColumnFirst">
                                                                

                                                                <asp:Label ID="label11" runat="server" CssClass="labelNormal labelFloat" Text="Update Do logotipo Financial " />
                                                                <br />
                                                                
                                                                <dxuc:ASPxUploadControl ID="uplLogotipo" runat="server" ClientInstanceName="uplLogotipo"
                                                                    OnFileUploadComplete="uplLogotipo_FileUploadComplete" CssClass="labelNormal"
                                                                    EnableDefaultAppearance="False" FileUploadMode="OnPageLoad">
                                                                    <validationsettings maxfilesize="100000000" />
                                                                    <clientsideevents fileuploadcomplete="function(s, e) { UploaderLogotipo_OnUploadComplete(e); }"></clientsideevents>
                                                                </dxuc:ASPxUploadControl>
                                                                <div class="linkButton linkButtonNoBorder">
                                                                    <asp:LinkButton ID="btnRunlogotipo" runat="server" Font-Overline="false" CssClass="btnRun"
                                                                        OnClientClick="
                                                                    {
                                                                        if(uplLogotipo.GetText()== '') {
                                                                            alert('Entre com o arquivo de Logotipo!');                                                                            
                                                                        }
                                                                        else {                                                                    
                                                                            callbackLogotipo.SendCallback();                                                                             
                                                                        }
                                                                        return false;
                                                                    }
                                                                    ">
                                                                        <asp:Literal ID="Literal6" runat="server" Text="Upload" /><div>
                                                                        </div>
                                                                    </asp:LinkButton>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </dxw:ContentControl>
                                                </ContentCollection>
                                            </dxtc:TabPage>
                                        </TabPages>
                                        <TabPages>
                                            <dxtc:TabPage Text="Select Sinacor">
                                                <ContentCollection>
                                                    <dxw:ContentControl runat="server">
                                                        <div class="innerPanel" id="Div2">
                                                            <div class="clear">
                                                            </div>
                                                            <div class="formColumn formColumnFirst">
                                                                <asp:TextBox ID="textScriptInputSinacor" runat="server" TextMode="MultiLine" Rows="5" style="width: 540px;margin-bottom: 20px;" CssClass="textLongo" />
                                                                <p style="line-height: 5px;">Comandos Selects simples para Base Sinacor</p>
                                                                <p>Exemplo: select data_mvto from hbtcontrato</p>
                                                                <div class="linkButton linkButtonNoBorder">
                                                                    <asp:LinkButton ID="LinkButton2" runat="server" Font-Overline="false" CssClass="btnRun"
                                                                        OnClientClick="
                                                                    {
                                                                        LoadingPanel1.Show();
                                                                        callbackSelectSinacor.SendCallback();                                                                             
                                                                        return false;
                                                                    }
                                                                    ">
                                                                        <asp:Literal ID="Literal9" runat="server" Text="Gerar" /><div>
                                                                        </div>
                                                                    </asp:LinkButton>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </dxw:ContentControl>
                                                </ContentCollection>
                                            </dxtc:TabPage>
                                        </TabPages>
                                        <TabPages>
                                            <dxtc:TabPage Text="Versão/Catalog">
                                                <ContentCollection>
                                                    <dxw:ContentControl runat="server">
                                                        <div class="innerPanel" id="Div3">
                                                            <div class="clear">
                                                            </div>
                                                            <div class="formColumn">
                                                                <asp:Label ID="label10" runat="server" CssClass="labelNormal labelFloat" Text="Versão: " />
                                                                <asp:Label ID="lblVersion1" runat="server" CssClass="labelNormal labelFloat" Text="" ForeColor="red" OnPreRender="lblVersion1_OnPreRender" /><br />
                                                                <div class="linkButton linkButtonNoBorder">
                                                                    <asp:LinkButton ID="LinkButton4" runat="server" Font-Overline="false" CssClass="btnRun" OnClick="btnVersao1_Click">
                                                                        <asp:Literal ID="Literal10" runat="server" Text="Verificar Versão" /><div>
                                                                        </div>
                                                                    </asp:LinkButton>
                                                                </div>
                                                                
                                                                <br /><br />
                                                                <asp:Label ID="label13" runat="server" CssClass="labelNormal labelFloat" Text="Catalog: " />
                                                                <asp:Label ID="lblCatalog" runat="server" CssClass="labelNormal labelFloat" Text="" OnPreRender="lblCatalog_OnPreRender" ForeColor="red" Font-Size="Larger"/><br />
                                                                
                                                                <br />
                                                                <asp:Label ID="label12" runat="server" CssClass="labelNormal labelFloat" Text="Versão Schema: " />
                                                                <asp:Label ID="lblVersao" runat="server" CssClass="labelNormal labelFloat" Text="" OnPreRender="lblVersao_OnPreRender" ForeColor="red" Font-Size="Larger"/><br />
                                                            </div>
                                                        </div>
                                                    </dxw:ContentControl>
                                                </ContentCollection>
                                            </dxtc:TabPage>
                                        </TabPages>
                                    </dxtc:ASPxPageControl>
                                </dxp:PanelContent>
                            </PanelCollection>
                        </dxrp:ASPxRoundPanel>
                    </div>
                    <dxlp:ASPxLoadingPanel ID="LoadingPanel" runat="server" Text="Updating aguarde..." ClientInstanceName="LoadingPanel" Modal="True" />
                    <dxlp:ASPxLoadingPanel ID="LoadingPanel1" runat="server" Text="Aguarde..." ClientInstanceName="LoadingPanel1" Modal="true" />
                </div>
            </div>
        </div>
    </form>
</body>
</html>