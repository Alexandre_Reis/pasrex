﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using Financial.Util.Enums;
using System.Text;
using DevExpress.Web;
using Financial.Investidor;
using Financial.ContaCorrente;
using System.Drawing;
using Financial.ContaCorrente.Enums;
using Financial.Security;
using Financial.Fundo;
using Financial.Bolsa;
using Financial.BMF;
using Financial.RendaFixa;
using Financial.Investidor.Enums;
using Financial.CRM;
using Financial.Bolsa.Enums;
using Financial.BMF.Enums;
using EntitySpaces.Interfaces;
using Financial.InvestidorCotista;
using Financial.WebConfigConfiguration;
using Financial.Security.Enums;
using System.Collections.Specialized;
using Financial.Swap;
using DevExpress.Web.Data;
using Financial.Contabil;
using Financial.Web.Util;
using Financial.Common.Enums;
using Financial.Captacao.Enums;
using Financial.Web.Common;
using Financial.Investidor.Exceptions;

public partial class Processamento_ProcessaPeriodoTrader : ConsultaBasePage
{
    new protected void Page_Load(object sender, EventArgs e)
    {
        base.Page_Load(sender, e);        
    }

    protected void callbackProcessamento_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = string.Empty;

        DateTime dataInicio = Convert.ToDateTime(textDataInicio.Text);
        DateTime dataFim = Convert.ToDateTime(textDataFim.Text);

        try
        {
            PortfolioPadraoCollection portfolioPadraoColl = new PortfolioPadraoCollection();
            portfolioPadraoColl.ClonaMovimentacao(dataInicio, dataFim);

            PortfolioPadrao portfolioPadrao = new PortfolioPadrao();
            portfolioPadrao = portfolioPadrao.RetornaPortfoliosVigentes(dataInicio);

            portfolioPadraoColl.ResetaStatusPortfolioPadrao(portfolioPadrao);
        }
        catch (ContaDefaultNaoCadastradaException ex)
        {
            e.Result = ex.Message;
        }
        catch (Exception ex)
        {
            e.Result = "Falha no processamento, a alteração foi desfeita!";
        }

    }

    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = string.Empty;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>();
        controles.Add(textDataInicio);
        controles.Add(textDataFim);
        
        if (base.TestaObrigatorio(controles) != "")
        {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }        
        #endregion

        #region Busca os Portfolios Padrões
        PortfolioPadrao portfolioPadrao = new PortfolioPadrao();
        portfolioPadrao = portfolioPadrao.RetornaPortfoliosVigentes(Convert.ToDateTime(textDataInicio.Text));

        if (portfolioPadrao.IdPortfolioPadrao.GetValueOrDefault(0) == 0)
        {
            e.Result = "Não existem portfólios padrões com data de vigência menor ou igual a data de ínicio";
            return;
        }
        #endregion

        DateTime dataInicio = Convert.ToDateTime(textDataInicio.Text);
        DateTime dataFim = Convert.ToDateTime(textDataFim.Text);

        if (dataInicio > dataFim)
        {
            e.Result = "Data ínicio deve ser menor que data fim";
            return;
        }           

        List<int> lstPortfolioPadrao = new List<int>();
        lstPortfolioPadrao.Add(portfolioPadrao.IdCarteiraDespesasReceitas.Value);
        lstPortfolioPadrao.Add(portfolioPadrao.IdCarteiraTaxaGestao.Value);
        lstPortfolioPadrao.Add(portfolioPadrao.IdCarteiraTaxaPerformance.Value);

        List<Cliente> lstCliente = new List<Cliente>();
        ClienteQuery clienteQuery = new ClienteQuery("cliente");
        ClienteCollection clienteColl = new ClienteCollection();
        #region Valida Carteira Destino (Portfólio padrão)
        clienteQuery.Where(clienteQuery.IdCliente.In(lstPortfolioPadrao)
                           & clienteQuery.TipoControle.NotIn((byte)TipoControleCliente.ApenasCotacao, (byte)TipoControleCliente.CarteiraImportada)
                           & clienteQuery.DataDia.NotEqual(dataInicio));

        if (clienteColl.Load(clienteQuery))
        {
            e.Result = "Todos os clientes (Destino - Portifólio padrão) devem estar na data de " + dataInicio.ToString("dd/MM/yyyy");
            return;
        }
        #endregion

        lstCliente = new List<Cliente>();
        clienteColl = new ClienteCollection();
        clienteQuery = new ClienteQuery("cliente");
        TraderQuery traderQuery = new TraderQuery("trader");
        #region Valida Carteira Destino (cliente trader)
        clienteQuery.InnerJoin(traderQuery).On(clienteQuery.IdCliente.Equal(traderQuery.IdCarteira));
        clienteQuery.Where(clienteQuery.IdCliente.NotIn(lstPortfolioPadrao)
                           & clienteQuery.TipoControle.NotIn((byte)TipoControleCliente.ApenasCotacao, (byte)TipoControleCliente.CarteiraImportada)
                           & clienteQuery.DataDia.NotEqual(dataInicio));

        if (clienteColl.Load(clienteQuery))
        {
            e.Result = "Todos os clientes (Destino - Trader) devem estar na data de " + dataInicio.ToString("dd/MM/yyyy");
            return;
        }
        #endregion

        lstCliente = new List<Cliente>();
        clienteColl = new ClienteCollection();
        clienteQuery = new ClienteQuery("cliente");
        #region Valida Cliente Origem
        /*clienteQuery.LeftJoin(traderQuery).On(clienteQuery.IdCliente.Equal(traderQuery.IdCarteira));
        clienteQuery.Where(clienteQuery.IdCliente.NotIn(lstPortfolioPadrao)
                           & traderQuery.IdTrader.IsNull()
                           & clienteQuery.TipoControle.NotIn((byte)TipoControleCliente.ApenasCotacao, (byte)TipoControleCliente.CarteiraImportada));
        

        if (clienteColl.Load(clienteQuery))
        {
            lstCliente = (List<Cliente>)clienteColl;

            Cliente cliente = new Cliente();
            cliente = lstCliente.Find(delegate(Cliente x) { return x.Status.Value != (byte)StatusCliente.Divulgado; });
            bool clienteFechados = (cliente != null && cliente.IdCliente.GetValueOrDefault(0) > 0);

            cliente = new Cliente();
            cliente = lstCliente.Find(delegate(Cliente x) { return x.DataDia.Value != dataFim; });
            bool clienteDatasDiversas = (cliente != null && cliente.IdCliente.GetValueOrDefault(0) > 0);

            string mensagem = string.Empty;
            if (clienteFechados)
                mensagem += "com status 'Fechado'";

            if (clienteDatasDiversas)
            {
                if (!string.IsNullOrEmpty(mensagem))
                    mensagem += " ";

                mensagem += "na data de " + dataFim.ToString("dd/MM/yyyy");
            }

            if (!string.IsNullOrEmpty(mensagem))
            {
                e.Result = "Todos os clientes (Origem) devem estar " + mensagem;
                return;
            }
        }*/
        #endregion
    }

    protected void callbackPortfolioPadrao_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = string.Empty;
        if (!string.IsNullOrEmpty(textDataInicio.Text))
        {
            PortfolioPadrao portfolioPadrao = new PortfolioPadrao();
            portfolioPadrao = portfolioPadrao.RetornaPortfoliosVigentes(Convert.ToDateTime(textDataInicio.Text));

            if (portfolioPadrao.IdPortfolioPadrao.GetValueOrDefault(0) == 0)
            {
                e.Result = "ERRO|Não existem portfólios padrões com data de vigência menor ou igual a data de ínicio";
                return;
            }

            int idCarteiraTxGestao = portfolioPadrao.IdCarteiraTaxaGestao.Value;
            int idCarteiraTxPerformance = portfolioPadrao.IdCarteiraTaxaPerformance.Value;
            int idCarteiraOutrasDespesasReceitas = portfolioPadrao.IdCarteiraDespesasReceitas.Value;

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteiraTxGestao);
            e.Result += carteira.IdCarteira.Value + " - " + carteira.Apelido + "|";

            carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteiraTxPerformance);
            e.Result += carteira.IdCarteira.Value + " - " + carteira.Apelido + "|";

            carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCarteiraOutrasDespesasReceitas);
            e.Result += carteira.IdCarteira.Value + " - " + carteira.Apelido;

        }
    }
}