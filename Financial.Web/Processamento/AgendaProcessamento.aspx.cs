﻿using System;

using System.Collections.Generic;
using System.Web;
using DevExpress.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using EntitySpaces.Interfaces;

using Financial.Web.Common;
using Financial.Investidor;
using Financial.Util;
using Financial.Util.Enums;
using Financial.Security;
using Financial.Web.Util;
using Financial.Security.Enums;

public partial class Processamento_AgendaProcessamento : CadastroBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSAgendaProcessamento_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {

        AgendaProcessosQuery agendaProcessosQuery = new AgendaProcessosQuery("APQ");
        PerfilProcessamentoQuery perfilProcessamentoQuery = new PerfilProcessamentoQuery("PPQ");

        agendaProcessosQuery.Select(agendaProcessosQuery.IdAgendaProcesso,
                                    agendaProcessosQuery.IdPerfil,
                                    agendaProcessosQuery.HoraExecucao,
                                    agendaProcessosQuery.DataFinal,
                                    agendaProcessosQuery.Ativo,
                                    perfilProcessamentoQuery.Descricao);
        agendaProcessosQuery.InnerJoin(perfilProcessamentoQuery).On(agendaProcessosQuery.IdPerfil == perfilProcessamentoQuery.IdPerfil);

        AgendaProcessosCollection agendaProcessosCollection = new AgendaProcessosCollection();
        agendaProcessosCollection.Load(agendaProcessosQuery);

        e.Collection = agendaProcessosCollection;

    }

    /// <summary>
    /// PopUp Perfil Processamento
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSPerfil_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {

        PerfilProcessamentoQuery perfilQuery = new PerfilProcessamentoQuery("PPQ");

        perfilQuery.Select(perfilQuery.IdPerfil, perfilQuery.Descricao);
        perfilQuery.Where(perfilQuery.Tipo.Equal(TipoPerfilProcessamento.Automatico));
        perfilQuery.OrderBy(perfilQuery.Descricao.Ascending);

        PerfilProcessamentoCollection coll = new PerfilProcessamentoCollection();

        coll.Load(perfilQuery);

        e.Collection = coll;
    }

    #endregion

    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";

        ASPxButtonEdit btnEditPerfil = gridCadastro.FindEditFormTemplateControl("btnEditPerfil") as ASPxButtonEdit;
        ASPxTimeEdit textHoraExecucao = gridCadastro.FindEditFormTemplateControl("textHoraExecucao") as ASPxTimeEdit;
        ASPxDateEdit textDataFim = gridCadastro.FindEditFormTemplateControl("textDataFim") as ASPxDateEdit;
        ASPxComboBox dropAtivo = gridCadastro.FindEditFormTemplateControl("dropAtivo") as ASPxComboBox;
        ASPxTextBox hiddenIdPerfil = gridCadastro.FindEditFormTemplateControl("hiddenIdPerfil") as ASPxTextBox;


        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { btnEditPerfil, textHoraExecucao, dropAtivo });

        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

    }

    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();

        e.Result = "";
        if (e.Parameter == "clone")
        {
            //e.Result = 0;
        }
        else
        {
            e.Result = "Operação feita com sucesso.";
        }
    }

    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {

        if (e.Column.FieldName == "DescricaoPerfil")
        {
            e.Value = Convert.ToString(e.GetListSourceFieldValue(PerfilProcessamentoMetadata.ColumnNames.Descricao));
        }
    }

    private void SalvarNovo()
    {
        AgendaProcessos agendaProcessos = new AgendaProcessos();
        //
        ASPxTextBox hiddenIdPerfil = gridCadastro.FindEditFormTemplateControl("hiddenIdPerfil") as ASPxTextBox;
        ASPxTimeEdit textHoraExecucao = gridCadastro.FindEditFormTemplateControl("textHoraExecucao") as ASPxTimeEdit;
        ASPxDateEdit textDataFim = gridCadastro.FindEditFormTemplateControl("textDataFim") as ASPxDateEdit;
        ASPxComboBox dropAtivo = gridCadastro.FindEditFormTemplateControl("dropAtivo") as ASPxComboBox;
        //


        DateTime dtAux = new DateTime(1900, 1, 1);

        dtAux = dtAux + Convert.ToDateTime(textHoraExecucao.Value).TimeOfDay;
        
        agendaProcessos.IdPerfil = Convert.ToInt32(hiddenIdPerfil.Text);
        agendaProcessos.HoraExecucao = dtAux;
        if (textDataFim.Value != null)
        {
            agendaProcessos.DataFinal = Convert.ToDateTime(textDataFim.Value);
        }
        agendaProcessos.Ativo = Convert.ToString(dropAtivo.SelectedItem.Value);


        agendaProcessos.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de AgendaProcessos - Operacao: Insert AgendaProcessos: " + agendaProcessos.IdAgendaProcesso + UtilitarioWeb.ToString(agendaProcessos),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        int idAgendaProcesso = (int)e.Keys[0];
        AgendaProcessos agendaProcessos = new AgendaProcessos();
        //
        //
        ASPxTextBox hiddenIdPerfil = gridCadastro.FindEditFormTemplateControl("hiddenIdPerfil") as ASPxTextBox;
        ASPxTimeEdit textHoraExecucao = gridCadastro.FindEditFormTemplateControl("textHoraExecucao") as ASPxTimeEdit;
        ASPxDateEdit textDataFim = gridCadastro.FindEditFormTemplateControl("textDataFim") as ASPxDateEdit;
        ASPxComboBox dropAtivo = gridCadastro.FindEditFormTemplateControl("dropAtivo") as ASPxComboBox;
        //
        if (agendaProcessos.LoadByPrimaryKey(idAgendaProcesso))
        {
            DateTime dtAux = new DateTime(1900, 1, 1);
            dtAux = dtAux + Convert.ToDateTime(textHoraExecucao.Value).TimeOfDay;

            agendaProcessos.IdPerfil = Convert.ToInt32(hiddenIdPerfil.Text);
            agendaProcessos.HoraExecucao = dtAux;
            if (textDataFim.Value != null)
            {
                agendaProcessos.DataFinal = Convert.ToDateTime(textDataFim.Value);
            }
            agendaProcessos.Ativo = Convert.ToString(dropAtivo.SelectedItem.Value);

            agendaProcessos.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de AgendaProcessos - Operacao: Update AgendaProcessos: " + idAgendaProcesso + "; " + UtilitarioWeb.ToString(agendaProcessos),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }
                     
        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesIdAgendaProcesso = gridCadastro.GetSelectedFieldValues(AgendaProcessosMetadata.ColumnNames.IdAgendaProcesso);

            for (int i = 0; i < keyValuesIdAgendaProcesso.Count; i++)
            {
                int idAgendaProcesso = Convert.ToInt32(keyValuesIdAgendaProcesso[i]);

                AgendaProcessos agendaProcessos = new AgendaProcessos();
                if (agendaProcessos.LoadByPrimaryKey(idAgendaProcesso))
                {
                    //
                    AgendaProcessos agendaProcessosClone = (AgendaProcessos)Utilitario.Clone(agendaProcessos);
                    //

                    agendaProcessos.MarkAsDeleted();
                    agendaProcessos.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de AgendaProcessos - Operacao: Delete AgendaProcessos: " + agendaProcessosClone.IdAgendaProcesso + "; " + UtilitarioWeb.ToString(agendaProcessosClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditPerfil_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
        {
            (sender as ASPxButtonEdit).Enabled = false;
            (sender as ASPxButtonEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridPerfil_CustomDataCallback(object sender, ASPxGridViewCustomDataCallbackEventArgs e)
    {
        ASPxGridView gridView = sender as ASPxGridView;
        //
        int visibleIndex = Convert.ToInt32(e.Parameters);
        //
        int idPerfil = (int)gridView.GetRowValues(visibleIndex, PerfilProcessamentoMetadata.ColumnNames.IdPerfil);

        e.Result = idPerfil.ToString();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridPerfil_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        UtilitarioGrid.SetaCoresGrid(e);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridPerfil_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        this.gridPerfil.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback_Callback_Perfil(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string texto = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idPerfil = Convert.ToInt32(e.Parameter);

            PerfilProcessamentoQuery perfilProcessamentoQuery = new PerfilProcessamentoQuery("PPQ");
            //
            perfilProcessamentoQuery.Select(perfilProcessamentoQuery.Descricao);
            perfilProcessamentoQuery.Where(perfilProcessamentoQuery.IdPerfil == idPerfil);

            PerfilProcessamento perfil = new PerfilProcessamento();

            if (perfil.Load(perfilProcessamentoQuery))
            {
                string descricao = Convert.ToString(perfil.Descricao);
                texto = descricao;
            }
        }
        e.Result = texto;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("btnEditPerfil");
        base.gridCadastro_PreRender(sender, e);
    }

}