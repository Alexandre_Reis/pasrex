﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using Financial.Util.Enums;
using System.Text;
using DevExpress.Web;
using Financial.Investidor;
using Financial.ContaCorrente;
using System.Drawing;
using Financial.ContaCorrente.Enums;
using Financial.Security;
using Financial.Fundo;
using Financial.Bolsa;
using Financial.BMF;
using Financial.RendaFixa;
using Financial.Investidor.Enums;
using Financial.CRM;
using Financial.Bolsa.Enums;
using Financial.BMF.Enums;
using EntitySpaces.Interfaces;
using Financial.InvestidorCotista;
using Financial.WebConfigConfiguration;
using Financial.Security.Enums;
using System.Collections.Specialized;
using Financial.Swap;
using DevExpress.Web.Data;
using Financial.Contabil;
using Financial.Web.Util;
using Financial.Common.Enums;
using Financial.Captacao.Enums;
using Financial.Web.Common;

public partial class Processamento_ProcessaPeriodo : ConsultaBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        const string ITAU = "ITAU";

        base.Page_Load(sender, e);

        if (!Page.IsPostBack && !Page.IsCallback) {
            dropTipoProcessamento.SelectedIndex = 0;
        }
          
        gridConsulta.EnableViewState = true;
        gridConsulta.Settings.ShowVerticalScrollBar = true;
        gridConsulta.Settings.VerticalScrollableHeight = 340;
        gridConsulta.SettingsPager.PageSize = 200;

        bool processamentoViaWebService = ParametrosConfiguracaoSistema.Outras.ProcessamentoViaWebService;
        string webServicesProviderUrl = ConfigurationManager.AppSettings["WebServicesProviderUrl"];

        StringBuilder javascript = new StringBuilder();
        javascript.AppendFormat("var processamentoViaWebService = {0};", processamentoViaWebService.ToString().ToLower());
        javascript.AppendLine();
        javascript.AppendFormat("var webServicesProviderUrl = '{0}';", webServicesProviderUrl);
        javascript.AppendLine();

        ClientScript.RegisterStartupScript(GetType(), "initJavascriptVars", javascript.ToString(), true);
    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {

        #region Consulta
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        ClienteQuery clienteQuery = new ClienteQuery("C");
        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");
        clienteQuery.Select(clienteQuery);
        clienteQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        clienteQuery.Where(permissaoClienteQuery.IdUsuario.Equal(idUsuario));
        clienteQuery.Where(clienteQuery.TipoControle.NotEqual((byte)TipoControleCliente.ApenasCotacao),
                           clienteQuery.TipoControle.NotEqual((byte)TipoControleCliente.CarteiraImportada),
                           clienteQuery.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo),
                           clienteQuery.IsProcessando.Equal("N"));

        if (dropGrupoProcessamento.SelectedIndex > 0) {
            clienteQuery.Where(clienteQuery.IdGrupoProcessamento == dropGrupoProcessamento.SelectedItem.Value);
        }

        if (dropStatus.SelectedIndex > -1) {
            clienteQuery.Where(clienteQuery.Status == Convert.ToInt32(dropStatus.SelectedItem.Value));
        }

        if (dropTipoProcessamento.SelectedIndex == 1) //Avança Periodo
        {
            clienteQuery.Where(clienteQuery.Status.In((byte)StatusCliente.Divulgado,
                                                      (byte)StatusCliente.Aberto,
                                                      (byte)StatusCliente.Fechado));
        }

        clienteQuery.OrderBy(clienteQuery.Apelido.Ascending);

        if (dropPerfilProcessamento.SelectedIndex > 0)
        {
            ClientePerfilQuery clientePerfilQuery = new ClientePerfilQuery("CPQ");
            clienteQuery.InnerJoin(clientePerfilQuery).On(clienteQuery.IdCliente == clientePerfilQuery.IdCliente);
            clienteQuery.Where(clientePerfilQuery.IdPerfil.Equal(dropPerfilProcessamento.SelectedItem.Value));
            clienteQuery.OrderBy(clientePerfilQuery.Sequencia.Ascending);
        }

        ClienteCollection coll = new ClienteCollection();
        coll.Load(clienteQuery);


        clienteQuery = new ClienteQuery("C");
        CarteiraQuery carteiraQuery = new CarteiraQuery("A");
        clienteQuery.Select(clienteQuery);
        clienteQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == clienteQuery.IdCliente);
        clienteQuery.Where(clienteQuery.TipoControle.Equal((byte)TipoControleCliente.ApenasCotacao),
                           clienteQuery.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo),
                           clienteQuery.IsProcessando.Equal("N"),
                           carteiraQuery.TipoVisaoFundo.NotEqual((byte)TipoVisaoFundoRebate.NaoTrata));

        if (dropGrupoProcessamento.SelectedIndex > 0) {
            clienteQuery.Where(clienteQuery.IdGrupoProcessamento == dropGrupoProcessamento.SelectedItem.Value);
        }

        if (dropStatus.SelectedIndex > 0) {
            clienteQuery.Where(clienteQuery.Status == dropStatus.SelectedItem.Value);
        }

        if (dropTipoProcessamento.SelectedIndex == 1) //Avança Periodo
        {
            clienteQuery.Where(clienteQuery.Status.In((byte)StatusCliente.Divulgado,
                                                      (byte)StatusCliente.Aberto,
                                                      (byte)StatusCliente.Fechado));
        }

        if (dropPerfilProcessamento.SelectedIndex > 0)
        {
            ClientePerfilQuery clientePerfilQuery = new ClientePerfilQuery("CPQ");
            clienteQuery.InnerJoin(clientePerfilQuery).On(clienteQuery.IdCliente == clientePerfilQuery.IdCliente);
            clienteQuery.Where(clientePerfilQuery.IdPerfil.Equal(dropPerfilProcessamento.SelectedItem.Value));
            clienteQuery.OrderBy(clientePerfilQuery.Sequencia.Ascending);
        }

        clienteQuery.OrderBy(clienteQuery.Apelido.Ascending);

        ClienteCollection coll2 = new ClienteCollection();
        coll2.Load(clienteQuery);

        coll.Combine(coll2);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
        #endregion


        #region Consulta varios registros
        // *******************************************************************************************

        //ClienteQuery clienteQuery = new ClienteQuery("C");
        //clienteQuery.Select(clienteQuery);
        //clienteQuery.OrderBy(clienteQuery.Apelido.Ascending);

        //ClienteCollection coll = new ClienteCollection();
        //coll.Load(clienteQuery);

        //e.Collection = coll;
        // *******************************************************************************************
        #endregion


        if (!Page.IsPostBack)
        {
            DevExpress.Web.ListEditItem item = new DevExpress.Web.ListEditItem();
            item.Text = " ";
            dropGrupoProcessamento.Items.Insert(0, item);
            DevExpress.Web.ListEditItem item2 = new DevExpress.Web.ListEditItem();
            item2.Text = " ";
            dropStatus.Items.Insert(0, item2);
            dropPerfilProcessamento.Items.Insert(0, item2);
        }
    }

    protected void EsDSTipoCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        TipoClienteCollection coll = new TipoClienteCollection();
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSGrupoProcessamento_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        GrupoProcessamentoCollection coll = new GrupoProcessamentoCollection();
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSPerfilProcessamento_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        PerfilProcessamentoQuery perfilProcessamentoQuery = new PerfilProcessamentoQuery("PPQ");
        perfilProcessamentoQuery.Where(perfilProcessamentoQuery.Tipo.Equal(TipoPerfilProcessamento.Manual));

        PerfilProcessamentoCollection coll = new PerfilProcessamentoCollection();
        coll.Load(perfilProcessamentoQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";

        List<object> keyValuesId = gridConsulta.GetSelectedFieldValues("IdCliente");

        if (keyValuesId.Count == 0) {
            e.Result = "Algum cliente deve ser selecionado!";
            return;
        }

        if (textDataFim.Text == "") {
            e.Result = "A Data Final deve ser informada!";
            return;
        }

        foreach (object objectId in keyValuesId) {
            int idCliente = (int)objectId;
            DateTime dataFinal = Convert.ToDateTime(textDataFim.Text);

            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.DataDia);
            campos.Add(cliente.Query.IdLocal);
            campos.Add(cliente.Query.IdCliente);
            //
            campos.Add(cliente.Query.DataBloqueioProcessamento);
            //
            cliente.LoadByPrimaryKey(campos, idCliente);
            DateTime dataDia = cliente.DataDia.Value;
            //
            DateTime? dataBloqueioProcessamento = null;
            if (cliente.DataBloqueioProcessamento.HasValue) {
                dataBloqueioProcessamento = cliente.DataBloqueioProcessamento.Value;
            }

            //

            TipoFeriado tipoFeriado = cliente.IdLocal.Value == (int)LocalFeriadoFixo.Brasil ? TipoFeriado.Brasil : TipoFeriado.Outros;
            if (!Calendario.IsDiaUtil(Convert.ToDateTime(textDataFim.Text), cliente.IdLocal.Value, tipoFeriado))
            {
                e.Result = "Data Final não é dia útil para o cliente " + idCliente.ToString() + ".";
                return;
            }

            if (dropTipoProcessamento.SelectedIndex == 0) { //Retroacao 
                if (dataDia.CompareTo(dataFinal) < 0) {
                    e.Result = "Data Final deve ser anterior à data dia atual do cliente " + idCliente.ToString();
                    return;                
                }
                else if (dataBloqueioProcessamento.HasValue) {
                    if (dataFinal < dataBloqueioProcessamento.Value) {
                        e.Result = "Data Final anterior à data bloqueio (" + dataBloqueioProcessamento.Value.ToShortDateString() + ") do cliente " + idCliente.ToString();
                        return;
                    }
                }
                else {
                    int numeroDias = Calendario.NumeroDias(dataFinal, dataDia, cliente.IdLocal.Value, tipoFeriado);

                    //Retorna evento de fundo se houver.
                    int tipoEvento = retornaEventoFundo(cliente, Convert.ToDateTime(textDataFim.Text));

                    if (numeroDias > 1 && tipoEvento == 0) {
                        e.Result = "O cliente " + idCliente.ToString() + " vai ser retroagido mais de um dia. Confirma o processo?";
                        return;
                    }
                    if (numeroDias > 1 && tipoEvento > 0)
                    {
                        e.Result = "- O cliente " + idCliente.ToString() + " vai ser retroagido mais de um dia.";

                        if (tipoEvento == 1)
                        {
                            e.Result += "\n- Existe um evento de Cisão/Incorporação/Fusão cadastrado para esta carteira. \n\nConfirma o processo?";
                            return;
                        }

                        if (tipoEvento == 2)
                        {
                            e.Result += "\n- Existe um evento de Troca de Condomínio cadastrada para esta carteira. \n\nConfirma o processo?";
                            return;
                        }

                        if (tipoEvento == 3)
                        {
                            e.Result += "\n- Existe um evento de Troca de Classificação Tributária cadastrada para esta carteira. \n\nConfirma o processo?";
                            return;
                        }
                    }
                    if (numeroDias <= 1 && tipoEvento > 0)
                    {
                        if (tipoEvento == 1)
                        {
                            e.Result = "Existe um evento de Cisão/Incorporação/Fusão cadastrado para esta carteira. Confirma o processo?";
                            return;
                        }

                        if (tipoEvento == 2)
                        {
                            e.Result = "Existe um evento de Troca de Condomínio cadastrada para esta carteira. Confirma o processo?";
                            return;
                        }

                        if (tipoEvento == 3)
                        {
                            e.Result = "Existe um evento de Troca de Classificação Tributária cadastrada para esta carteira. Confirma o processo?";
                            return;
                        }
                    }
                }
            }
            else if (dropTipoProcessamento.SelectedIndex == 1) { //Avanca Periodo
                if (dataDia.CompareTo(dataFinal) >= 0) {
                    e.Result = "Data Final deve ser posterior à data dia atual do cliente " + idCliente.ToString();
                    return;
                }
            }
        }
    }

    protected void gridConsulta_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e) {
        gridConsulta.DataBind();
        gridConsulta.Selection.UnselectAll();
        gridConsulta.CancelEdit();
    }

    protected void gridConsulta_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) {
        if (e.Column.FieldName == "StatusDescricao") {
            int status = Convert.ToInt32(e.GetListSourceFieldValue("Status"));

            string statusDescricao = TraducaoEnumsInvestidor.EnumStatusCliente.TraduzEnum(status);

            e.Value = statusDescricao;
        }
    }

    protected int retornaEventoFundo(Cliente cliente, DateTime data)
    {
        //Verifica se existe Cisão/Incorporação/Fusão
        EventoFundoCollection eventoFundoCollection = new EventoFundoCollection();
        eventoFundoCollection.Query.Where(eventoFundoCollection.Query.DataPosicao.GreaterThanOrEqual(data),
                                          eventoFundoCollection.Query.DataPosicao.LessThan(cliente.DataDia.Value),
                                          eventoFundoCollection.Query.IdCarteiraOrigem.Equal(cliente.IdCliente.Value),
                                          eventoFundoCollection.Query.Status.Equal("Ativo"));
        eventoFundoCollection.Query.Load();

        if (eventoFundoCollection.Count > 0)
        {
            return 1;
        }

        //Verifica se existe alteração de condomínio
        FundoInvestimentoFormaCondominioCollection fundoInvestimentoFormaCondominioCollection = new FundoInvestimentoFormaCondominioCollection();
        fundoInvestimentoFormaCondominioCollection.Query.Where(fundoInvestimentoFormaCondominioCollection.Query.DataInicioVigencia.GreaterThanOrEqual(data),
                                                               fundoInvestimentoFormaCondominioCollection.Query.DataInicioVigencia.LessThan(cliente.DataDia.Value),
                                                               fundoInvestimentoFormaCondominioCollection.Query.IdCarteira.Equal(cliente.IdCliente.Value));
        fundoInvestimentoFormaCondominioCollection.Query.Load();

        if (fundoInvestimentoFormaCondominioCollection.Count > 0)
        {
            return 2;
        }

        //Verifica se existe alteração de classificação tributária
        DesenquadramentoTributarioCollection desenquadramentoTributarioCollection = new DesenquadramentoTributarioCollection();
        desenquadramentoTributarioCollection.Query.Where(desenquadramentoTributarioCollection.Query.Data.GreaterThanOrEqual(data),
                                                         desenquadramentoTributarioCollection.Query.Data.LessThan(cliente.DataDia.Value),
                                                         desenquadramentoTributarioCollection.Query.IdCarteira.Equal(cliente.IdCliente.Value));
        desenquadramentoTributarioCollection.Query.Load();

        if (desenquadramentoTributarioCollection.Count > 0)
        {
            return 3;
        }

        return 0;
    }
}