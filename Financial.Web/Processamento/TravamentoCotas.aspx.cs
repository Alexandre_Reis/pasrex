﻿using System;

using Financial.RendaFixa;
using Financial.Web.Common;
using System.Collections.Generic;
using Financial.Util;
using Financial.Security;
using Financial.Web.Util;
using System.Web;
using Financial.Security.Enums;
using DevExpress.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using Financial.Fundo;
using Financial.Fundo.Enums;
using EntitySpaces.Interfaces;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Util.Enums;

public partial class Processamento_TravamentoCotas : CadastroBasePage
{
    new protected void Page_Load(object sender, EventArgs e) {
        this.FiltroGridCarteira();
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSTravamentoCotas_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {

        CarteiraQuery carteiraQuery = new CarteiraQuery("CQ");
        TravamentoCotasQuery travamentoCotasQuery = new TravamentoCotasQuery("TCQ");

        travamentoCotasQuery.Select(travamentoCotasQuery.IdTravamentoCota,
                                   travamentoCotasQuery.IdCarteira,
                                   travamentoCotasQuery.DataProcessamento,
                                   travamentoCotasQuery.TipoBloqueio,
                                   travamentoCotasQuery.ValorCota,
                                   carteiraQuery.Apelido);

        travamentoCotasQuery.InnerJoin(carteiraQuery).On(travamentoCotasQuery.IdCarteira == carteiraQuery.IdCarteira);

        TravamentoCotasCollection coll = new TravamentoCotasCollection();
        coll.Load(travamentoCotasQuery);

        e.Collection = coll;
    }

    

    /// <summary>
    /// PopUp Cliente
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSCarteira_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {

        CarteiraQuery carteiraQuery = new CarteiraQuery("CQ");
        HistoricoCotaQuery historicoCotaQuery = new HistoricoCotaQuery("HCQ");

        historicoCotaQuery.Select(historicoCotaQuery.Data,
                                  historicoCotaQuery.CotaFechamento,
                                  historicoCotaQuery.IdCarteira,
                                  historicoCotaQuery.CotaImportada,
                                  carteiraQuery.Apelido);
        historicoCotaQuery.InnerJoin(carteiraQuery).On(historicoCotaQuery.IdCarteira == carteiraQuery.IdCarteira);
        historicoCotaQuery.OrderBy(carteiraQuery.Apelido.Ascending, historicoCotaQuery.Data.Descending);
        
        HistoricoCotaCollection coll = new HistoricoCotaCollection();
        coll.Load(historicoCotaQuery);

        e.Collection = coll;
    }

    #endregion
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCarteira_CustomDataCallback(object sender, ASPxGridViewCustomDataCallbackEventArgs e) {
        ASPxGridView gridView = sender as ASPxGridView;
        //
        int visibleIndex = Convert.ToInt32(e.Parameters);
        //
        //int idCarteira = (int)gridView.GetRowValues(visibleIndex, CarteiraMetadata.ColumnNames.IdCarteira);

        //string data = Convert.ToString(e.GetListSourceFieldValue("Data"));
        //string cotaFechamento = Convert.ToString(e.GetListSourceFieldValue("CotaFechamento"));
        //string nome = Convert.ToString(e.GetListSourceFieldValue("Apelido"));
        //e.Value = nome + "|" + data + "|" + cotaFechamento;
        ASPxDateEdit textDataProcessamentoXX = gridView.GetRowValues(visibleIndex, HistoricoCotaMetadata.ColumnNames.Data) as ASPxDateEdit;
        string idCarteira = gridView.GetRowValues(visibleIndex, CarteiraMetadata.ColumnNames.IdCarteira).ToString();
        DateTime data = Convert.ToDateTime(gridView.GetRowValues(visibleIndex, HistoricoCotaMetadata.ColumnNames.Data));
        string cotaFechamento = gridView.GetRowValues(visibleIndex, HistoricoCotaMetadata.ColumnNames.CotaFechamento).ToString();

        e.Result = idCarteira + "|" + data.Year + "|" + (data.Month -1) + "|" + data.Day + "|" + cotaFechamento;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCarteira_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e) {
        UtilitarioGrid.SetaCoresGrid(e);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCarteira_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e) {
        this.gridCarteira.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback2_Callback_Carteira(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string texto = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCarteira = Convert.ToInt32(e.Parameter);

            CarteiraQuery carteiraQuery = new CarteiraQuery("CQ");
            carteiraQuery.Select(carteiraQuery.Apelido);
            carteiraQuery.Where(carteiraQuery.IdCarteira == idCarteira);

            Carteira carteira = new Carteira();

            if (carteira.Load(carteiraQuery))
            {
                string apelido = Convert.ToString(carteira.Apelido);
                texto = apelido;
            }
        }
        e.Result = texto;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditCarteira_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxButtonEdit).Enabled = false;
            (sender as ASPxButtonEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";
        //
        ASPxButtonEdit btnEditCarteira = gridCadastro.FindEditFormTemplateControl("btnEditCarteira") as ASPxButtonEdit;
        ASPxDateEdit textDataProcessamento = gridCadastro.FindEditFormTemplateControl("textDataProcessamento") as ASPxDateEdit;
        ASPxComboBox dropTipoBloqueio = gridCadastro.FindEditFormTemplateControl("dropTipoBloqueio") as ASPxComboBox;
        ASPxSpinEdit textValorCota = gridCadastro.FindEditFormTemplateControl("textValorCota") as ASPxSpinEdit;
        ASPxTextBox hiddenIdCarteira = gridCadastro.FindEditFormTemplateControl("hiddenIdCarteira") as ASPxTextBox;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { btnEditCarteira, textDataProcessamento, dropTipoBloqueio, textValorCota  });

        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

        if (gridCadastro.IsNewRowEditing)
        {
            TravamentoCotasQuery travamentoCotasQuery = new TravamentoCotasQuery();
            travamentoCotasQuery.Where(travamentoCotasQuery.IdCarteira == hiddenIdCarteira.Value);
            travamentoCotasQuery.Where(travamentoCotasQuery.DataProcessamento == textDataProcessamento.Value);

            TravamentoCotasCollection travamentoCotasCollection = new TravamentoCotasCollection();
            travamentoCotasCollection.Load(travamentoCotasQuery);
            if (travamentoCotasCollection.Count > 0)
            {
                e.Result = "Já existe cota travada para esta carteira na data escolhida.";
                return;
            }
        }

    }

    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();

        e.Result = "";
        if (e.Parameter == "clone")
        {
            //e.Result = 0;
        }
        else
        {
            e.Result = "Operação feita com sucesso.";
        }
    }

    private void SalvarNovo()
    {
        TravamentoCotas travamentoCotas = new TravamentoCotas();
        //
        ASPxDateEdit textDataProcessamento = gridCadastro.FindEditFormTemplateControl("textDataProcessamento") as ASPxDateEdit;
        ASPxComboBox dropTipoBloqueio = gridCadastro.FindEditFormTemplateControl("dropTipoBloqueio") as ASPxComboBox;
        ASPxSpinEdit textValorCota = gridCadastro.FindEditFormTemplateControl("textValorCota") as ASPxSpinEdit;
        ASPxTextBox hiddenIdCarteira = gridCadastro.FindEditFormTemplateControl("hiddenIdCarteira") as ASPxTextBox;
        ASPxSpinEdit textValorCotaCalculada = gridCadastro.FindEditFormTemplateControl("textValorCotaCalculada") as ASPxSpinEdit;
        //

        travamentoCotas.IdCarteira = Convert.ToInt32(hiddenIdCarteira.Text);
        travamentoCotas.DataProcessamento = Convert.ToDateTime(textDataProcessamento.Text);
        travamentoCotas.ValorCota = Convert.ToDecimal(textValorCota.Text);
        travamentoCotas.TipoBloqueio = Convert.ToByte(dropTipoBloqueio.SelectedItem.Value);
        travamentoCotas.ValorCotaCalculada = Convert.ToDecimal(textValorCotaCalculada.Text);

        travamentoCotas.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de Travamento de Cotas - Operacao: Insert TravamentoCotas: " + travamentoCotas.IdCarteira + "; " + UtilitarioWeb.ToString(travamentoCotas),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// Compoe a atributos grid cadastro
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "DescricaoCarteira")
        {
            string nome = Convert.ToString(e.GetListSourceFieldValue("Apelido"));
            e.Value = nome;
        }

    }

    /// <summary>
    /// Compoe a atributos grid Carteira
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCarteira_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "CompositeCarteira")
        {

            string data = Convert.ToString(e.GetListSourceFieldValue("Data"));
            string cotaFechamento = Convert.ToString(e.GetListSourceFieldValue("CotaFechamento"));
            string nome = Convert.ToString(e.GetListSourceFieldValue("Apelido"));
            e.Value = nome + data;
        }

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        int idTravamentoCotas = (int)e.Keys[0];
        TravamentoCotas travamentoCotas = new TravamentoCotas();
        //
        ASPxDateEdit textDataProcessamento = gridCadastro.FindEditFormTemplateControl("textDataProcessamento") as ASPxDateEdit;
        ASPxComboBox dropTipoBloqueio = gridCadastro.FindEditFormTemplateControl("dropTipoBloqueio") as ASPxComboBox;
        ASPxSpinEdit textValorCota = gridCadastro.FindEditFormTemplateControl("textValorCota") as ASPxSpinEdit;
        ASPxTextBox hiddenIdCarteira = gridCadastro.FindEditFormTemplateControl("hiddenIdCarteira") as ASPxTextBox;
        ASPxSpinEdit textValorCotaCalculada = gridCadastro.FindEditFormTemplateControl("textValorCotaCalculada") as ASPxSpinEdit;
        //

        if (travamentoCotas.LoadByPrimaryKey(idTravamentoCotas))
        {
            travamentoCotas.IdCarteira = Convert.ToInt32(hiddenIdCarteira.Text);
            travamentoCotas.DataProcessamento = Convert.ToDateTime(textDataProcessamento.Text);
            travamentoCotas.ValorCota = Convert.ToDecimal(textValorCota.Text);
            travamentoCotas.TipoBloqueio = Convert.ToByte(dropTipoBloqueio.SelectedItem.Value);
            travamentoCotas.ValorCotaCalculada = Convert.ToDecimal(textValorCotaCalculada.Text);
            travamentoCotas.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de Travamento cotas - Operacao: Update TravamentoCotas: " + idTravamentoCotas + "; " + UtilitarioWeb.ToString(travamentoCotas),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesIdPerfil = gridCadastro.GetSelectedFieldValues("IdTravamentoCota");

            for (int i = 0; i < keyValuesIdPerfil.Count; i++)
            {
                int idTravamentoCota = Convert.ToInt32(keyValuesIdPerfil[i]);

                TravamentoCotas travamentoCotas = new TravamentoCotas();
                if (travamentoCotas.LoadByPrimaryKey(idTravamentoCota))
                {
                    //
                    TravamentoCotas travamentoCotasClone = (TravamentoCotas)Utilitario.Clone(travamentoCotas);
                    //

                    travamentoCotas.MarkAsDeleted();
                    travamentoCotas.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de TravamentoCotas - Operacao: Delete TravamentoCotas: " + travamentoCotasClone.IdTravamentoCota + "; " + UtilitarioWeb.ToString(travamentoCotasClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("btnEditCarteira");
        base.gridCadastro_PreRender(sender, e);
    }

    /// <summary>
    /// Troca para Like no filtro campo Tabela Adm Associada.
    /// </summary>
    public void FiltroGridCarteira()
    {
        GridViewDataColumn colCarteira = gridCarteira.Columns[CarteiraMetadata.ColumnNames.Apelido] as GridViewDataColumn;
        if (colCarteira != null)
        {
            colCarteira.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
        }
    }
}