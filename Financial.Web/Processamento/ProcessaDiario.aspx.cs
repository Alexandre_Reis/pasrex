﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using Financial.Util.Enums;
using System.Text;
using DevExpress.Web;
using Financial.Investidor;
using Financial.ContaCorrente;
using System.Drawing;
using Financial.ContaCorrente.Enums;
using Financial.Security;
using Financial.Fundo;
using Financial.Bolsa;
using Financial.BMF;
using Financial.RendaFixa;
using Financial.Investidor.Enums;
using Financial.CRM;
using Financial.Bolsa.Enums;
using Financial.BMF.Enums;
using EntitySpaces.Interfaces;
using Financial.InvestidorCotista;
using Financial.WebConfigConfiguration;
using Financial.Security.Enums;
using System.Collections.Specialized;
using Financial.Swap;
using DevExpress.Web.Data;
using Financial.Contabil;
using Financial.Web.Util;
using Financial.Common.Enums;
using Financial.Web.Common;
using Financial.Captacao.Enums;

public partial class Processamento_ProcessaDiario : ConsultaBasePage {

    int chave = BuscaIdUsuario();

    new protected void Page_Load(object sender, EventArgs e) {
        const string ITAU = "ITAU";

        base.Page_Load(sender, e);

        if (!Page.IsPostBack && !Page.IsCallback) {
            dropTipoProcessamento.SelectedIndex = 0;
        }
                
        gridConsulta.EnableViewState = true;
        gridConsulta.Settings.VerticalScrollBarMode = DevExpress.Web.ScrollBarMode.Visible;
        gridConsulta.Settings.VerticalScrollableHeight = 340;
        gridConsulta.SettingsPager.PageSize = 200;

        bool processamentoViaWebService = ParametrosConfiguracaoSistema.Outras.ProcessamentoViaWebService;
        string webServicesProviderUrl = ConfigurationManager.AppSettings["WebServicesProviderUrl"];

        StringBuilder javascript = new StringBuilder();
        javascript.AppendFormat("var processamentoViaWebService = {0};", processamentoViaWebService.ToString().ToLower());
        javascript.AppendLine();
        javascript.AppendFormat("var webServicesProviderUrl = '{0}';", webServicesProviderUrl);
        javascript.AppendLine();

        ClientScript.RegisterStartupScript(GetType(), "initJavascriptVars", javascript.ToString(), true);

        if (!this.IsPostBack)
        {
            PreProcessamentoCollection preProcessamentoColl = new PreProcessamentoCollection();
            preProcessamentoColl.DeletaTabelasChaves(chave);
        }
    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {
        #region Consulta
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        ClienteQuery clienteQuery = new ClienteQuery("C");
        ColagemComeCotasQuery colagemComeCotasQuery = new ColagemComeCotasQuery("colagemComeCotas");
        ColagemResgateQuery colagemResgateQuery = new ColagemResgateQuery("colagemResgate");
        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");

        StringBuilder colagemField = new StringBuilder();
        colagemField.Append("< CASE WHEN colagemResgate.[DataReferencia] is not null OR colagemComeCotas.[DataReferencia] is not null ");
        colagemField.Append(" THEN 'Sim'");
        colagemField.Append(" ELSE 'Não'");
        colagemField.Append(" END as ValoresColados>");

        clienteQuery.es.Distinct = true;
        clienteQuery.Select(clienteQuery, colagemField.ToString());
        clienteQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        clienteQuery.LeftJoin(colagemComeCotasQuery).On(colagemComeCotasQuery.DataReferencia.Equal(clienteQuery.DataDia) & colagemComeCotasQuery.IdCarteira.Equal(clienteQuery.IdCliente));
        clienteQuery.LeftJoin(colagemResgateQuery).On(colagemResgateQuery.DataReferencia.Equal(clienteQuery.DataDia) & colagemResgateQuery.IdCliente.Equal(clienteQuery.IdCliente));
        clienteQuery.Where(permissaoClienteQuery.IdUsuario.Equal(idUsuario));
        clienteQuery.Where(clienteQuery.TipoControle.NotEqual((byte)TipoControleCliente.ApenasCotacao),
                           clienteQuery.TipoControle.NotEqual((byte)TipoControleCliente.CarteiraImportada),
                           clienteQuery.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo),
                           clienteQuery.IsProcessando.Equal("N"));

        if (dropGrupoProcessamento.SelectedIndex > 0) {
            clienteQuery.Where(clienteQuery.IdGrupoProcessamento == dropGrupoProcessamento.SelectedItem.Value);
        }

        if (dropStatus.SelectedIndex > 0) {
            clienteQuery.Where(clienteQuery.Status == dropStatus.SelectedItem.Value);
        }

        TipoProcessamento tipoProcessamento = TipoProcessamento.Abertura;

        if (dropTipoProcessamento.SelectedIndex > 0)
        {
            tipoProcessamento = (TipoProcessamento)Convert.ToInt32(dropTipoProcessamento.SelectedItem.Value);
        }

        if (tipoProcessamento == TipoProcessamento.Fechamento) //Rotina de Cálculo
        {
            clienteQuery.Where(clienteQuery.Status.In((byte)StatusCliente.Fechado,
                                                      (byte)StatusCliente.Aberto,
                                                      (byte)StatusCliente.FechadoComErro));
        }
        else if (tipoProcessamento == TipoProcessamento.Divulgacao) //Fechamento
        {
            clienteQuery.Where(clienteQuery.Status.Equal((byte)StatusCliente.Fechado));
        }

        if (dropPerfilProcessamento.SelectedIndex > 0)
        {
            ClientePerfilQuery clientePerfilQuery = new ClientePerfilQuery("CPQ");
            clienteQuery.Select(clientePerfilQuery.Sequencia);
            clienteQuery.InnerJoin(clientePerfilQuery).On(clienteQuery.IdCliente.Equal(clientePerfilQuery.IdCliente));
            clienteQuery.Where(clientePerfilQuery.IdPerfil.Equal(dropPerfilProcessamento.SelectedItem.Value));
            clienteQuery.OrderBy(clientePerfilQuery.Sequencia.Ascending);
        }
        else
        {
            clienteQuery.OrderBy(clienteQuery.Apelido.Ascending);
        }

        ClienteCollection coll = new ClienteCollection();
        coll.Load(clienteQuery);


        clienteQuery = new ClienteQuery("C");
        CarteiraQuery carteiraQuery = new CarteiraQuery("A");

        clienteQuery.Select(clienteQuery, colagemField.ToString());
        clienteQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == clienteQuery.IdCliente);
        clienteQuery.LeftJoin(colagemComeCotasQuery).On(colagemComeCotasQuery.DataReferencia.Equal(clienteQuery.DataDia) & colagemComeCotasQuery.IdCarteira.Equal(clienteQuery.IdCliente));
        clienteQuery.LeftJoin(colagemResgateQuery).On(colagemResgateQuery.DataReferencia.Equal(clienteQuery.DataDia) & colagemResgateQuery.IdCliente.Equal(clienteQuery.IdCliente));
        clienteQuery.Where(clienteQuery.TipoControle.Equal((byte)TipoControleCliente.ApenasCotacao),
                           carteiraQuery.TipoVisaoFundo.NotEqual((byte)TipoVisaoFundoRebate.NaoTrata),
                           clienteQuery.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo),
                           clienteQuery.IsProcessando.Equal("N"));

        if (dropGrupoProcessamento.SelectedIndex > 0) {
            clienteQuery.Where(clienteQuery.IdGrupoProcessamento == dropGrupoProcessamento.SelectedItem.Value);
        }

        if (dropStatus.SelectedIndex > 0) {
            clienteQuery.Where(clienteQuery.Status == dropStatus.SelectedItem.Value);
        }

        if (tipoProcessamento == TipoProcessamento.Fechamento) //Rotina de Cálculo
        {
            clienteQuery.Where(clienteQuery.Status.In((byte)StatusCliente.Fechado,
                                                      (byte)StatusCliente.Aberto,
                                                      (byte)StatusCliente.FechadoComErro));
        }
        else if (tipoProcessamento == TipoProcessamento.Divulgacao) //Fechamento
        {
            clienteQuery.Where(clienteQuery.Status.Equal((byte)StatusCliente.Fechado));
        }

        if (dropPerfilProcessamento.SelectedIndex > 0)
        {
            ClientePerfilQuery clientePerfilQuery = new ClientePerfilQuery("CPQ");
            clienteQuery.Select(clientePerfilQuery.Sequencia);
            clienteQuery.InnerJoin(clientePerfilQuery).On(clienteQuery.IdCliente == clientePerfilQuery.IdCliente);
            clienteQuery.Where(clientePerfilQuery.IdPerfil.Equal(dropPerfilProcessamento.SelectedItem.Value));
            clienteQuery.OrderBy(clientePerfilQuery.Sequencia.Ascending);
        }
        else
        {
            clienteQuery.OrderBy(clienteQuery.Apelido.Ascending);
        }

        ClienteCollection coll2 = new ClienteCollection();
        coll2.Load(clienteQuery);

        coll.Combine(coll2);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
        #endregion

       
        #region Consulta varios registros
        // *******************************************************************************************

        //ClienteQuery clienteQuery = new ClienteQuery("C");
        //clienteQuery.Select(clienteQuery);
        //clienteQuery.OrderBy(clienteQuery.Apelido.Ascending);

        //ClienteCollection coll = new ClienteCollection();
        //coll.Load(clienteQuery);

        //e.Collection = coll;
        // *******************************************************************************************
        #endregion

        if (!Page.IsPostBack) {
            DevExpress.Web.ListEditItem item = new DevExpress.Web.ListEditItem();
            item.Text = " ";
            dropGrupoProcessamento.Items.Insert(0, item);
            DevExpress.Web.ListEditItem item2 = new DevExpress.Web.ListEditItem();
            item2.Text = " ";
            dropStatus.Items.Insert(0, item2);
            item2.Text = " ";
            dropStatus.Items.Insert(0, item2);
            dropPerfilProcessamento.Items.Insert(0, item2);
        }
    }

    protected void EsDSTipoCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        TipoClienteCollection coll = new TipoClienteCollection();
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSGrupoProcessamento_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        GrupoProcessamentoCollection coll = new GrupoProcessamentoCollection();
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSPerfilProcessamento_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        PerfilProcessamentoQuery perfilProcessamentoQuery = new PerfilProcessamentoQuery("PPQ");
        perfilProcessamentoQuery.Where(perfilProcessamentoQuery.Tipo.Equal(TipoPerfilProcessamento.Manual));

        PerfilProcessamentoCollection coll = new PerfilProcessamentoCollection();
        coll.Load(perfilProcessamentoQuery);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSPreProcessamento_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        PreProcessamentoCollection preProcessamentoColl = new PreProcessamentoCollection();
        preProcessamentoColl.Query.Where(preProcessamentoColl.Query.Chave.Equal(chave));
        preProcessamentoColl.Query.Load();

        e.Collection = preProcessamentoColl;
    }

    #endregion

    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";

        List<object> keyValuesId = gridConsulta.GetSelectedFieldValues("IdCliente");

        if (keyValuesId.Count == 0) {
            e.Result = "Algum cliente deve ser selecionado!";
            return;
        }
    }

    protected void callbackCheck_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "";

        DateTime hoje = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
        DateTime ontem = Calendario.SubtraiDiaUtil(hoje, 1);
        
        HistoricoLogCollection historicoLogCollection = new HistoricoLogCollection();
        historicoLogCollection.Query.Where(historicoLogCollection.Query.Descricao.Like("Importação CDIX%") |
                                           historicoLogCollection.Query.Descricao.Like("Importação CMDF%") |
                                           historicoLogCollection.Query.Descricao.Like("Importação CONR%") |
                                           historicoLogCollection.Query.Descricao.Like("Importação CONL%") |
                                           historicoLogCollection.Query.Descricao.Like("Importação DBTC%") |
                                           historicoLogCollection.Query.Descricao.Like("Importação DBTL%") |
                                           historicoLogCollection.Query.Descricao.Like("Importação PAPT%") |
                                           historicoLogCollection.Query.Descricao.Like("Importação PROD%") |
                                           historicoLogCollection.Query.Descricao.Like("Importação CRCA%"));
        historicoLogCollection.Query.Where(historicoLogCollection.Query.Descricao.Like("%" + ontem.ToShortDateString() + "%"));
        historicoLogCollection.Query.Load();

        StringBuilder strCotacao = new StringBuilder();        
        foreach (HistoricoLog historicoLog in historicoLogCollection)
        {
            strCotacao.Append(historicoLog.Descricao + "\n");
        }

        if (strCotacao.ToString() == "")
        {
            strCotacao.Append("Nenhum arquivo importado em " + ontem.ToShortDateString() + ". \n");
        }

        StringBuilder strArquivos = new StringBuilder();        
        
        CotacaoBolsaCollection cotacaoBolsaCollection = new CotacaoBolsaCollection();
        cotacaoBolsaCollection.Query.Select(cotacaoBolsaCollection.Query.PUFechamento);
        cotacaoBolsaCollection.Query.Where(cotacaoBolsaCollection.Query.Data.Equal(ontem));
        cotacaoBolsaCollection.Query.Load();

        if (cotacaoBolsaCollection.Count < 10)
        {
            strArquivos.Append("Cotações de Bolsa não disponíveis em " + ontem.ToShortDateString() + "\n");
        }

        CotacaoBMFCollection cotacaoBMFCollection = new CotacaoBMFCollection();
        cotacaoBMFCollection.Query.Select(cotacaoBMFCollection.Query.PUFechamento);
        cotacaoBMFCollection.Query.Where(cotacaoBMFCollection.Query.Data.Equal(ontem));
        cotacaoBMFCollection.Query.Load();

        if (cotacaoBMFCollection.Count < 10)
        {
            strArquivos.Append("Cotações de BMF não disponíveis em " + ontem.ToShortDateString() + "\n");
        }

        CotacaoMercadoAndimaCollection cotacaoMercadoAndimaCollection = new CotacaoMercadoAndimaCollection();
        cotacaoMercadoAndimaCollection.Query.Select(cotacaoMercadoAndimaCollection.Query.Pu);
        cotacaoMercadoAndimaCollection.Query.Where(cotacaoMercadoAndimaCollection.Query.DataReferencia.Equal(ontem));
        cotacaoMercadoAndimaCollection.Query.Load();

        if (cotacaoMercadoAndimaCollection.Count < 10)
        {
            strArquivos.Append("Cotações Mercado Andima não disponíveis em " + ontem.ToShortDateString() + "\n");
        }

        CotacaoIndiceCollection cotacaoIndiceCollection = new CotacaoIndiceCollection();
        cotacaoIndiceCollection.Query.Where(cotacaoIndiceCollection.Query.Data.Equal(ontem));
        cotacaoIndiceCollection.Query.Load();

        if (cotacaoIndiceCollection.FindByPrimaryKey(ontem, (short)ListaIndiceFixo.CDI) == null)
        {
            strArquivos.Append("Cotação CDI não disponível em " + ontem.ToShortDateString() + "\n");
        }
        if (cotacaoIndiceCollection.FindByPrimaryKey(ontem, (short)ListaIndiceFixo.IBOVESPA_FECHA) == null)
        {
            strArquivos.Append("Cotação Ibovespa não disponível em " + ontem.ToShortDateString() + "\n");
        }
        if (cotacaoIndiceCollection.FindByPrimaryKey(ontem, (short)ListaIndiceFixo.OUROBMF_MEDIO) == null)
        {
            strArquivos.Append("Cotação Ouro não disponível em " + ontem.ToShortDateString() + "\n");
        }
        if (cotacaoIndiceCollection.FindByPrimaryKey(ontem, (short)ListaIndiceFixo.PTAX_800VENDA) == null)
        {
            strArquivos.Append("Cotação Dólar não disponível em " + ontem.ToShortDateString() + "\n");
        }
        if (cotacaoIndiceCollection.FindByPrimaryKey(ontem, (short)ListaIndiceFixo.SELIC) == null)
        {
            strArquivos.Append("Cotação Selic não disponível em " + ontem.ToShortDateString() + "\n");
        }        

        DateTime primeiroDiaMes = Calendario.RetornaPrimeiroDiaCorridoMes(ontem, 0);
        cotacaoIndiceCollection = new CotacaoIndiceCollection();
        cotacaoIndiceCollection.Query.Where(cotacaoIndiceCollection.Query.Data.Equal(primeiroDiaMes));
        cotacaoIndiceCollection.Query.Load();

        if (cotacaoIndiceCollection.FindByPrimaryKey(primeiroDiaMes, (short)ListaIndiceFixo.IGPM) == null)
        {
            strArquivos.Append("Cotação IGPM não disponível em " + primeiroDiaMes.ToShortDateString() + "\n");
        }
        if (cotacaoIndiceCollection.FindByPrimaryKey(primeiroDiaMes, (short)ListaIndiceFixo.IPCA) == null)
        {
            strArquivos.Append("Cotação IPCA não disponível em " + primeiroDiaMes.ToShortDateString() + "\n");
        }        

        if (strArquivos.ToString() != "")
        {
            strCotacao.Append("----------------------------------------\n\n");
        }
        e.Result = strCotacao.ToString() + strArquivos.ToString();
    }

    protected void gridConsulta_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e) {
        gridConsulta.DataBind();
        gridConsulta.Selection.UnselectAll();
        gridConsulta.CancelEdit();
    }

    protected void gridConsulta_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) {
        if (e.Column.FieldName == "StatusDescricao") {
            int status = Convert.ToInt32(e.GetListSourceFieldValue("Status"));

            string statusDescricao = TraducaoEnumsInvestidor.EnumStatusCliente.TraduzEnum(status);

            e.Value = statusDescricao;
        }
    }

    protected void gridPreProcessamento_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        PreProcessamentoCollection preProcessamentoColl = new PreProcessamentoCollection();
        preProcessamentoColl.DeletaTabelasChaves(chave);

        if (gridConsulta != null)
        {
            List<object> lstIdCliente = gridConsulta.GetSelectedFieldValues("IdCliente");
            List<object> lstIdData = gridConsulta.GetSelectedFieldValues("DataDia");

            if (lstIdCliente.Count > 0)
            {
                preProcessamentoColl.MontaPendencias(lstIdCliente, lstIdData, chave, true);                
            }
        }

        gridPreProcessamento.DataBind();
    }

    protected void brtnPreProc_Click(object sender, EventArgs e)
    {
        exportGridPreProc.ExportedRowType = GridViewExportedRowType.All;        
        exportGridPreProc.WriteXlsxToResponse();
    }

    static private int BuscaIdUsuario()
    {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(HttpContext.Current.User.Identity.Name);

        return usuario.IdUsuario.Value;
    }
}