﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using DevExpress.Web;
using Financial.Web.Common;
using Financial.Security;
using Financial.Security.Enums;
using Financial.Util;
using Financial.Investidor;

public partial class Processamento_MonitorProcessamento : CadastroBasePage
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void Page_Load(object sender, EventArgs e) 
    {
        this.HasEditControl = false;
        this.HasFiltro = true;
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSLogProcessamento_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) 
    {

        ASPxDateEdit textDataInicio = popupFiltro.FindControl("textDataInicio") as ASPxDateEdit;
        ASPxDateEdit textDataFim = popupFiltro.FindControl("textDataFim") as ASPxDateEdit;

        LogProcessamentoQuery logProcessamentoQuery = new LogProcessamentoQuery("LPQ");
        ClienteQuery clienteQuery = new ClienteQuery("CQ");
        BoletoRetroativoQuery boletoRetroativoQuery = new BoletoRetroativoQuery("BRQ");

        clienteQuery.Select(clienteQuery.Apelido,
                            clienteQuery.IdCliente,
                             clienteQuery.DataDia,
                             boletoRetroativoQuery.DataBoleto,
                             logProcessamentoQuery.Data,
                             logProcessamentoQuery.Login,
                             logProcessamentoQuery.Tipo,
                             logProcessamentoQuery.DataInicio,
                             logProcessamentoQuery.DataFim,
                             logProcessamentoQuery.DataInicialPeriodo,
                             logProcessamentoQuery.DataFinalPeriodo,
                             logProcessamentoQuery.Erro,
                             logProcessamentoQuery.Mensagem,
                             boletoRetroativoQuery.DataBoleto);
        clienteQuery.LeftJoin(logProcessamentoQuery).On(logProcessamentoQuery.IdCliente == clienteQuery.IdCliente);
        clienteQuery.LeftJoin(boletoRetroativoQuery).On(boletoRetroativoQuery.IdCliente == clienteQuery.IdCliente);

        if (textDataInicio.Text != "")
        {
            DateTime dataInicio = new DateTime(Convert.ToDateTime(textDataInicio.Text).Year, Convert.ToDateTime(textDataInicio.Text).Month, Convert.ToDateTime(textDataInicio.Text).Day);
            clienteQuery.Where(logProcessamentoQuery.Data.GreaterThan(dataInicio));
        }

        if (textDataFim.Text != "")
        {
            DateTime dataFim = new DateTime(Convert.ToDateTime(textDataFim.Text).Year, Convert.ToDateTime(textDataFim.Text).Month, Convert.ToDateTime(textDataFim.Text).Day);
            clienteQuery.Where(logProcessamentoQuery.Data.LessThan(dataFim));
        }

        clienteQuery.OrderBy(logProcessamentoQuery.Data.Descending, clienteQuery.Apelido.Ascending);

        ClienteCollection coll = new ClienteCollection();
        coll.Load(clienteQuery);

        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) 
    {        
        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "Status")
        {
            string erro = Convert.ToString(e.GetListSourceFieldValue("Erro"));
            if (erro == "1")
            {
                e.Value = "3";
            }
            else if (erro == "0")
            {

                DateTime data = Convert.ToDateTime(e.GetListSourceFieldValue("Data"));
                DateTime dataBoleto;

                if (DBNull.Value.Equals(e.GetListSourceFieldValue("DataBoleto")))
                {
                    e.Value = "1";
                }
                else
                {
                    dataBoleto = Convert.ToDateTime(e.GetListSourceFieldValue("DataBoleto"));

                    if (DateTime.Compare(dataBoleto, data) <= 0)
                    {
                        e.Value = "2";
                    }
                    else
                    {
                        e.Value = "1";
                    }

                }
                

            }
        }

        if (e.Column.FieldName == "CompositeKey")
        {
            //Compose a primary key value
            string idCliente = Convert.ToString(e.GetListSourceFieldValue(LogProcessamentoMetadata.ColumnNames.IdCliente));
            string data = Convert.ToString(e.GetListSourceFieldValue(LogProcessamentoMetadata.ColumnNames.Data));
            e.Value = idCliente + data;
        }
    }

    protected void gridCadastro_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        if (e.DataColumn.Index == 2)
        {

            if (e.CellValue == "1")
            {
                e.Cell.BackColor = System.Drawing.Color.Green;
                e.Cell.ForeColor = System.Drawing.Color.Green;
            }
            if (e.CellValue == "2")
            {
                e.Cell.BackColor = System.Drawing.Color.Yellow;
                e.Cell.ForeColor = System.Drawing.Color.Yellow;
            }
            if (e.CellValue == "3")
            {
                e.Cell.BackColor = System.Drawing.Color.Red;
                e.Cell.ForeColor = System.Drawing.Color.Red;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) 
    {        
        base.gridCadastro_PreRender(sender, e);
        
        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");
        if (textDataInicio.Text != "") {
            texto.Append(" Data >= ").Append(textDataInicio.Text);
        }
        if (textDataFim.Text != "") {
            if (texto.Length > 0) {
                texto.Append(" |");
            }

            texto.Append(" Data <= ").Append(textDataFim.Text);
        }
        
        labelFiltro.Text = texto.ToString();
    }
        
}