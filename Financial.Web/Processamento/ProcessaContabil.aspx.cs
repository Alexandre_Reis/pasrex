﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using Financial.Investidor;
using Financial.ContaCorrente;
using System.Drawing;
using Financial.ContaCorrente.Enums;
using Financial.Security;
using Financial.Fundo;
using Financial.Bolsa;
using Financial.BMF;
using Financial.RendaFixa;
using Financial.Investidor.Enums;
using Financial.CRM;
using Financial.Bolsa.Enums;
using Financial.BMF.Enums;
using EntitySpaces.Interfaces;
using Financial.InvestidorCotista;
using Financial.WebConfigConfiguration;
using Financial.Security.Enums;
using System.Collections.Specialized;
using Financial.Swap;
using DevExpress.Web.Data;
using Financial.Contabil;
using Financial.Web.Util;
using Financial.Common.Enums;
using Financial.Web.Common;
using Financial.Captacao.Enums;

public partial class Processamento_ProcessaContabil : ConsultaBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        base.Page_Load(sender, e);


        gridConsulta.Settings.VerticalScrollBarMode = DevExpress.Web.ScrollBarMode.Visible;
        gridConsulta.Settings.VerticalScrollableHeight = 340;
        gridConsulta.Settings.VerticalScrollBarStyle = GridViewVerticalScrollBarStyle.Virtual;                        
        gridConsulta.SettingsPager.PageSize = 10000;
    }

    #region Load dos esDataSources (grid principal, eventuais grids de popup e dropdownlists)
    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        Usuario usuario = new Usuario();
        usuario.BuscaUsuario(Context.User.Identity.Name);
        int idUsuario = usuario.IdUsuario.Value;

        ClienteQuery clienteQuery = new ClienteQuery("C");
        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");

        clienteQuery.Select(clienteQuery);
        clienteQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        clienteQuery.Where(permissaoClienteQuery.IdUsuario.Equal(idUsuario));
        clienteQuery.Where(clienteQuery.TipoControle.NotEqual((byte)TipoControleCliente.ApenasCotacao),
                           clienteQuery.TipoControle.NotEqual((byte)TipoControleCliente.CarteiraImportada),
                           clienteQuery.IsProcessando.Equal("N"));

        if (dropGrupoProcessamento.SelectedIndex > 0)
        {
            clienteQuery.Where(clienteQuery.IdGrupoProcessamento == dropGrupoProcessamento.SelectedItem.Value);
        }

        if (dropStatus.SelectedIndex > 0)
        {
            clienteQuery.Where(clienteQuery.Status == dropStatus.SelectedItem.Value);
        }
        
        clienteQuery.OrderBy(clienteQuery.Apelido.Ascending);

        ClienteCollection coll = new ClienteCollection();
        coll.Load(clienteQuery);


        clienteQuery = new ClienteQuery("C");
        CarteiraQuery carteiraQuery = new CarteiraQuery("A");

        clienteQuery.Select(clienteQuery);
        clienteQuery.InnerJoin(carteiraQuery).On(carteiraQuery.IdCarteira == clienteQuery.IdCliente);
        clienteQuery.Where(clienteQuery.TipoControle.Equal((byte)TipoControleCliente.ApenasCotacao),
                           carteiraQuery.TipoVisaoFundo.NotEqual((byte)TipoVisaoFundoRebate.NaoTrata),
                           clienteQuery.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo),
                           clienteQuery.IsProcessando.Equal("N"));

        if (dropGrupoProcessamento.SelectedIndex > 0)
        {
            clienteQuery.Where(clienteQuery.IdGrupoProcessamento == dropGrupoProcessamento.SelectedItem.Value);
        }

        if (dropStatus.SelectedIndex > 0)
        {
            clienteQuery.Where(clienteQuery.Status == dropStatus.SelectedItem.Value);
        }

        clienteQuery.OrderBy(clienteQuery.Apelido.Ascending);

        ClienteCollection coll2 = new ClienteCollection();
        coll2.Load(clienteQuery);

        coll.Combine(coll2);

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;

        if (!Page.IsPostBack)
        {
            DevExpress.Web.ListEditItem item = new DevExpress.Web.ListEditItem();
            item.Text = " ";
            dropGrupoProcessamento.Items.Insert(0, item);
            DevExpress.Web.ListEditItem item2 = new DevExpress.Web.ListEditItem();
            item2.Text = " ";
            dropStatus.Items.Insert(0, item2);
        }
    }

    protected void EsDSTipoCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        TipoClienteCollection coll = new TipoClienteCollection();
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void EsDSGrupoProcessamento_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        GrupoProcessamentoCollection coll = new GrupoProcessamentoCollection();
        coll.LoadAll();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }
    #endregion

    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";

        if (textDataInicio.Text == "")
        {
            e.Result = "A Data Inicial deve ser informada!";
            return;
        }

        if (textDataFim.Text == "")
        {
            e.Result = "A Data Final deve ser informada!";
            return;
        }

        DateTime dataInicio = Convert.ToDateTime(textDataInicio.Text);
        DateTime dataFinal = Convert.ToDateTime(textDataFim.Text);

        if (dataFinal.Date < dataInicio.Date)
        {
            e.Result = "Data final anterior à inicial!";
            return;
        }

        List<object> keyValuesId = gridConsulta.GetSelectedFieldValues("IdCliente");

        if (keyValuesId.Count == 0) {
            e.Result = "Algum cliente deve ser selecionado!";
            return;
        }
    }

    protected void gridConsulta_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e) {
        gridConsulta.DataBind();
        gridConsulta.Selection.UnselectAll();
        gridConsulta.CancelEdit();
    }

    protected void gridConsulta_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e) 
    {
        if (e.Column.FieldName == "DataContabil") 
        {
            int idCliente = Convert.ToInt32(e.GetListSourceFieldValue("IdCliente"));

            ContabSaldo contabSaldo = new ContabSaldo();
            contabSaldo.Query.Select(contabSaldo.Query.Data.Max());
            contabSaldo.Query.Where(contabSaldo.Query.IdCliente.Equal(idCliente));
            contabSaldo.Query.Load();

            string dataContabil = "";
            if (contabSaldo.Data.HasValue)
            {
                dataContabil = contabSaldo.Data.Value.ToShortDateString();
            }

            e.Value = dataContabil;
        }
    }
}