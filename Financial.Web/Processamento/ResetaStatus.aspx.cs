﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.UI.HtmlControls;
using Financial.Common;
using System.Collections.Generic;
using Financial.Util;
using System.Text;
using DevExpress.Web;
using System.Drawing;
using Financial.CRM;
using Financial.Security;
using Financial.Fundo;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Web.Util;
using Financial.Security.Enums;
using Financial.Captacao;

public partial class Processamento_ResetaStatus : Financial.Web.Common.ConsultaBasePage
{
    new protected void Page_Load(object sender, EventArgs e) {
        base.Page_Load(sender, e);
    }

    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) 
    {
        ClienteQuery clienteQuery = new ClienteQuery("C");
        UsuarioQuery usuarioQuery = new UsuarioQuery("U");
        PermissaoClienteQuery permissaoClienteQuery = new PermissaoClienteQuery("P");

        clienteQuery.Select();
        clienteQuery.InnerJoin(permissaoClienteQuery).On(permissaoClienteQuery.IdCliente == clienteQuery.IdCliente);
        clienteQuery.InnerJoin(usuarioQuery).On(permissaoClienteQuery.IdUsuario == usuarioQuery.IdUsuario);
        clienteQuery.Where(usuarioQuery.Login == Context.User.Identity.Name,
                           clienteQuery.TipoControle.NotEqual(TipoControleCliente.ApenasCotacao));
        clienteQuery.OrderBy(clienteQuery.Apelido.Ascending);

        ClienteCollection coll = new ClienteCollection();
        coll.Load(clienteQuery);
        
        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    protected void callBackReset_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        List<object> clientesSelecionados = gridConsulta.GetSelectedFieldValues(ClienteMetadata.ColumnNames.IdCliente);

        if (clientesSelecionados.Count == 0)
        {
            e.Result = "Selecione um ou mais Clientes.";
        }
        else
        {
            for (int i = 0; i < clientesSelecionados.Count; i++)
            {
                int idCliente = Convert.ToInt32(clientesSelecionados[i]);

                Cliente cliente = new Cliente();
                //
                if (cliente.LoadByPrimaryKey(idCliente))
                {
                    cliente.IsProcessando = "N";

                    cliente.Save();
                }
            }
            e.Result = "Processo executado com sucesso.";
        }
    }

    protected void gridConsulta_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "StatusProcessando")
        {
            string status = Convert.ToString(e.GetListSourceFieldValue(ClienteMetadata.ColumnNames.IsProcessando));

            string statusDescricao = status == "S" ? "Sim" : "Não";

            e.Value = statusDescricao;
        }
        else if (e.Column.FieldName == "StatusDescricao")
        {
            int status = Convert.ToInt32(e.GetListSourceFieldValue(ClienteMetadata.ColumnNames.Status));

            string statusDescricao = TraducaoEnumsInvestidor.EnumStatusCliente.TraduzEnum(status);

            e.Value = statusDescricao;
        }
    }

    protected void gridConsulta_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        gridConsulta.DataBind();
        gridConsulta.CancelEdit();
    }
}