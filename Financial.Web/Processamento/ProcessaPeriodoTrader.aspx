﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ProcessaPeriodoTrader.aspx.cs"
    Inherits="Processamento_ProcessaPeriodoTrader" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxrp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxp" %>
<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../js/global.js"></script>

    <script type="text/javascript" src="../js/jquery.js"></script>

    <script type="text/javascript" src="../js/jquery.progressbar.js"></script>

    <script type="text/javascript" src="../js/process.js?v=2"></script>

    <script type="text/javascript" language="Javascript">
        var popup = true;
        document.onkeydown=onDocumentKeyDown;
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true"
            EnableScriptLocalization="true" />
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {            
            if (e.result != '')
            {                   
                alert(e.result);   
                window.processing = false;                                                        
            }
            else
            {
                progressBar.SetValue(0);
                progressWindow.SetVisible(true);
                callbackProcessamento.SendCallback();
            }
            
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackProcessamento" runat="server" OnCallback="callbackProcessamento_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {  
            progressBar.SetValue(100); 
            if (e.result != '')
            {   
                document.getElementById('mensagem').innerHTML = e.result;
                document.getElementById('mensagem').style.color = 'red';                 
            }
            else
            {
                document.getElementById('mensagem').innerHTML = 'Processamento realizado com sucesso!';
                document.getElementById('mensagem').style.color = 'green';
            }                      
            window.processing = false;  
            
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackPortfolioPadrao" runat="server" OnCallback="callbackPortfolioPadrao_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {            
            if(e.result != '')
            {
                var resultSplit = e.result.split('|');
                
                if(resultSplit[0] == 'ERRO')
                {
                    alert(resultSplit[1]);
                    textPortfolioTaxaGestao.SetValue(null);
                    textPortfolioTaxaPerformance.SetValue(null);
                    textPortfolioDespesasReceitas.SetValue(null);  
                    return;
                }
            
                textPortfolioTaxaGestao.SetValue(resultSplit[0]);
                textPortfolioTaxaPerformance.SetValue(resultSplit[1]);
                textPortfolioDespesasReceitas.SetValue(resultSplit[2]);
            }
        }        
        " />
        </dxcb:ASPxCallback>
        <dx:ASPxPopupControl ID="progressWindow" ClientInstanceName="progressWindow" Width="300"
            Height="120" HeaderText="Processamento" Modal="true" CloseAction="None" ShowCloseButton="false"
            PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" runat="server">
            <ContentCollection>
                <dx:PopupControlContentControl>
                    <div id="progressBarWrapper">
                        <div id="Div1">
                            <p id="mensagem">
                            </p>
                        </div>
                        <div id="resumo-processamento">
                            <dxe:ASPxProgressBar ID="progressBar" runat="server" ClientInstanceName="progressBar"
                                DisplayMode="Position" ShowPosition="true" Width="100%">
                            </dxe:ASPxProgressBar>
                        </div>
                        <div class="linkButton linkButtonNoBorder">
                            <asp:LinkButton ID="btnClose" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                CssClass="btnOK" OnClientClick="progressWindow.Hide(); return false;">
                                <asp:Literal ID="Literal1" runat="server" Text="Fechar" /><div>
                                </div>
                            </asp:LinkButton>
                        </div>
                    </div>
                </dx:PopupControlContentControl>
            </ContentCollection>
        </dx:ASPxPopupControl>
        <div class="divPanel divPanelNew">
            <table width="100%" border="0">
                <tr>
                    <td>
                        <div id="container_small">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Processamento Período - Trader" />
                            </div>
                            <div id="mainContentSpace">
                                <br />
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <table border="0" align="center">
                                            <tr>
                                                <td valign="middle">
                                                    <dxrp:ASPxRoundPanel ID="ASPxRoundPanel2" runat="server" HeaderStyle-Font-Bold="true"
                                                        HeaderText="Parâmetros" Width="600px" HeaderStyle-Font-Size="11px">
                                                        <PanelCollection>
                                                            <dxp:PanelContent runat="server">
                                                                <table border="0" align="center" cellpadding="2" cellspacing="2">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="labelDataInicio" runat="server" CssClass="labelRequired" Text="Data Ínicio:"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxDateEdit ID="textDataInicio" runat="server" ClientInstanceName="textDataInicio"
                                                                                ClientSideEvents-DateChanged="function(s, e) { callbackPortfolioPadrao.SendCallback(); }" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="labelDataFim" runat="server" CssClass="labelRequired" Text="Data Final:"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxDateEdit ID="textDataFim" runat="server" ClientInstanceName="textDataFim" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="td_Label">
                                                                            <asp:Label ID="lblPortfolioTaxaGestao" runat="server" AssociatedControlID="lblPortfolioTaxaGestao"
                                                                                CssClass="labelNormal" Text="Portfólio - Tx.Gestão:" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxTextBox ID="textPortfolioTaxaGestao" ClientInstanceName="textPortfolioTaxaGestao"
                                                                                runat="server" CssClass="textLongo" ClientEnabled="false" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="td_Label">
                                                                            <asp:Label ID="lblPortfolioTaxaPerformance" runat="server" AssociatedControlID="lblPortfolioTaxaPerformance"
                                                                                CssClass="labelNormal" Text="Portfólio - Tx.Performance:" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxTextBox ID="textPortfolioTaxaPerformance" ClientInstanceName="textPortfolioTaxaPerformance"
                                                                                runat="server" CssClass="textLongo" ClientEnabled="false" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="td_Label">
                                                                            <asp:Label ID="lblPortfolioDespesasReceitas" runat="server" AssociatedControlID="lblPortfolioDespesasReceitas"
                                                                                CssClass="labelNormal" Text="Portfólio - Outras Despesas e Receitas:" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxTextBox ID="textPortfolioDespesasReceitas" ClientInstanceName="textPortfolioDespesasReceitas"
                                                                                runat="server" CssClass="textLongo" ClientEnabled="false" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <table border="0" align="center" cellpadding="2" cellspacing="2">
                                                                    <div class="linkButton">
                                                                        <asp:LinkButton ID="btnRun" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                                                            CssClass="btnRun" OnClientClick="
                                                                            if(window.processing === true){
                                                                                    alert('Processamento já foi iniciado. Por favor, aguarde.');
                                                                            }else{
                                                                                 window.processing = true; 
                                                                                callbackErro.SendCallback();
                                                                            }
                                                                            return false;
                                                                            ">
                                                                            <asp:Literal ID="Literal2" runat="server" Text="Processar" /><div>
                                                                            </div>
                                                                        </asp:LinkButton>
                                                                    </div>
                                                                </table>
                                                            </dxp:PanelContent>
                                                        </PanelCollection>
                                                    </dxrp:ASPxRoundPanel>
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
