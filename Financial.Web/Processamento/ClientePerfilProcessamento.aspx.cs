﻿using System;

using Financial.RendaFixa;
using Financial.Web.Common;
using System.Collections.Generic;
using Financial.Util;
using Financial.Security;
using Financial.Web.Util;
using System.Web;
using Financial.Security.Enums;
using DevExpress.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using Financial.Fundo;
using Financial.Fundo.Enums;
using EntitySpaces.Interfaces;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Util.Enums;

public partial class Processamento_ClientePerfilProcessamento : CadastroBasePage
{
    new protected void Page_Load(object sender, EventArgs e) {
        this.FiltroGridPerfil();
        this.FiltroGridCliente();
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSPerfilCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {

        ClientePerfilQuery clientePerfilQuery = new ClientePerfilQuery("CP");
        ClienteQuery clienteQuery = new ClienteQuery("C");
        PerfilProcessamentoQuery perfilQuery = new PerfilProcessamentoQuery("PQ");

        clientePerfilQuery.Select(clientePerfilQuery.IdCliente,
                                   clientePerfilQuery.IdPerfil,
                                   clientePerfilQuery.Sequencia,
                                   clienteQuery.Nome,
                                   perfilQuery.Descricao);

        clientePerfilQuery.InnerJoin(clienteQuery).On(clientePerfilQuery.IdCliente == clienteQuery.IdCliente);
        clientePerfilQuery.InnerJoin(perfilQuery).On(clientePerfilQuery.IdPerfil == perfilQuery.IdPerfil);

        ClientePerfilCollection coll = new ClientePerfilCollection();
        coll.Load(clientePerfilQuery);

        e.Collection = coll;
    }

    

    /// <summary>
    /// PopUp Cliente
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSCliente_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {

        ClienteQuery clienteQuery = new ClienteQuery("C");

        clienteQuery.Select(clienteQuery.IdCliente, clienteQuery.Nome);
        clienteQuery.Where(clienteQuery.TipoControle.NotEqual((byte)TipoControleCliente.ApenasCotacao),
                           clienteQuery.TipoControle.NotEqual((byte)TipoControleCliente.CarteiraImportada),
                           clienteQuery.StatusAtivo.Equal((byte)StatusAtivoCliente.Ativo),
                           clienteQuery.IsProcessando.Equal("N"));
        clienteQuery.OrderBy(clienteQuery.Nome.Ascending);

        ClienteCollection coll = new ClienteCollection();

        coll.Load(clienteQuery);

        e.Collection = coll;
    }


    /// <summary>
    /// PopUp Perfil Processamento
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void EsDSPerfil_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {

        PerfilProcessamentoQuery perfilQuery = new PerfilProcessamentoQuery("PPQ");

        perfilQuery.Select(perfilQuery.IdPerfil, perfilQuery.Descricao);
        perfilQuery.OrderBy(perfilQuery.Descricao.Ascending);

        PerfilProcessamentoCollection coll = new PerfilProcessamentoCollection();

        coll.Load(perfilQuery);

        e.Collection = coll;
    }
    #endregion
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCliente_CustomDataCallback(object sender, ASPxGridViewCustomDataCallbackEventArgs e) {
        ASPxGridView gridView = sender as ASPxGridView;
        //
        int visibleIndex = Convert.ToInt32(e.Parameters);
        //
        int idCliente = (int)gridView.GetRowValues(visibleIndex, ClienteMetadata.ColumnNames.IdCliente);

        e.Result = idCliente.ToString();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCliente_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e) {
        UtilitarioGrid.SetaCoresGrid(e);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCliente_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e) {
        this.gridCliente.DataBind();
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridPerfil_CustomDataCallback(object sender, ASPxGridViewCustomDataCallbackEventArgs e)
    {
        ASPxGridView gridView = sender as ASPxGridView;
        //
        int visibleIndex = Convert.ToInt32(e.Parameters);
        //
        int idPerfil = (int)gridView.GetRowValues(visibleIndex, PerfilProcessamentoMetadata.ColumnNames.IdPerfil);

        e.Result = idPerfil.ToString();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridPerfil_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        UtilitarioGrid.SetaCoresGrid(e);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridPerfil_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        this.gridPerfil.DataBind();
    }



    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback_Callback_Perfil(object source, DevExpress.Web.CallbackEventArgs e) {
        string texto = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null") {
            int idPerfil = Convert.ToInt32(e.Parameter);

            PerfilProcessamentoQuery perfilProcessamentoQuery = new PerfilProcessamentoQuery("PPQ");
            //
            perfilProcessamentoQuery.Select(perfilProcessamentoQuery.Descricao);
            perfilProcessamentoQuery.Where(perfilProcessamentoQuery.IdPerfil == idPerfil);

            PerfilProcessamento perfil = new PerfilProcessamento();

            if (perfil.Load(perfilProcessamentoQuery))
            {
                string descricao = Convert.ToString(perfil.Descricao);
                texto = descricao;
            }
        }
        e.Result = texto;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void ASPxCallback2_Callback_Cliente(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string texto = "";
        if (!String.IsNullOrEmpty(e.Parameter) && e.Parameter != "null")
        {
            int idCliente = Convert.ToInt32(e.Parameter);

            ClienteQuery clienteQuery = new ClienteQuery("CQ");
            clienteQuery.Select(clienteQuery.Nome);
            clienteQuery.Where(clienteQuery.IdCliente == idCliente);

            Cliente cliente = new Cliente();

            if (cliente.Load(clienteQuery))
            {
                string nome = Convert.ToString(cliente.Nome);
                texto = nome;
            }
        }
        e.Result = texto;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditCliente_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing) {
            (sender as ASPxButtonEdit).Enabled = false;
            (sender as ASPxButtonEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnEditPerfil_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
        {
            (sender as ASPxButtonEdit).Enabled = false;
            (sender as ASPxButtonEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textSequencia_Load(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
        {
            (sender as ASPxSpinEdit).Enabled = false;
            (sender as ASPxSpinEdit).BackColor = Color.FromName("#EBEBEB");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";
        //
        ASPxButtonEdit btnEditPerfil = gridCadastro.FindEditFormTemplateControl("btnEditPerfil") as ASPxButtonEdit;
        ASPxButtonEdit btnEditCliente = gridCadastro.FindEditFormTemplateControl("btnEditCliente") as ASPxButtonEdit;
        ASPxSpinEdit textSequencia = gridCadastro.FindEditFormTemplateControl("textSequencia") as ASPxSpinEdit;
        ASPxTextBox hiddenIdPerfil = gridCadastro.FindEditFormTemplateControl("hiddenIdPerfil") as ASPxTextBox;
        ASPxTextBox hiddenIdCliente = gridCadastro.FindEditFormTemplateControl("hiddenIdCliente") as ASPxTextBox;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { btnEditPerfil, btnEditCliente, textSequencia });

        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }

        ClientePerfilCollection clientePerfilCollection = new ClientePerfilCollection();

        PerfilProcessamento perfilProcessamento = new PerfilProcessamento();
        perfilProcessamento.LoadByPrimaryKey(Convert.ToInt32(hiddenIdPerfil.Text));
        if (perfilProcessamento.Tipo.Equals((int)TipoPerfilProcessamento.Automatico))
        {
            
            clientePerfilCollection.Query.Where(clientePerfilCollection.Query.IdCliente == hiddenIdCliente.Value);
            clientePerfilCollection.Query.Where(clientePerfilCollection.Query.IdPerfil != hiddenIdPerfil.Value);
            clientePerfilCollection.LoadAll();
            if (clientePerfilCollection.Count > 0)
            {
                e.Result = "Cliente já foi selecionado em outro perfil automático (" + clientePerfilCollection[0].UpToPerfilProcessamentoByIdPerfil.Descricao + ").";
                return;
            }
        }

        if (gridCadastro.IsNewRowEditing)
        {
            clientePerfilCollection = new ClientePerfilCollection();
            clientePerfilCollection.Query.Where(clientePerfilCollection.Query.IdPerfil == hiddenIdPerfil.Value);
            clientePerfilCollection.Query.Where(clientePerfilCollection.Query.IdCliente == hiddenIdCliente.Value);
            clientePerfilCollection.LoadAll();
            if (clientePerfilCollection.Count > 0)
            {
                e.Result = "Cliente já está selecionado para esse perfil.";
                return;
            }
        }
        #endregion

    }

    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();

        e.Result = "";
        if (e.Parameter == "clone")
        {
            //e.Result = 0;
        }
        else
        {
            e.Result = "Operação feita com sucesso.";
        }
    }

    private void SalvarNovo()
    {
        ClientePerfil clientePerfil = new ClientePerfil();
        //
        ASPxTextBox hiddenIdPerfil = gridCadastro.FindEditFormTemplateControl("hiddenIdPerfil") as ASPxTextBox;
        ASPxTextBox hiddenIdCliente = gridCadastro.FindEditFormTemplateControl("hiddenIdCliente") as ASPxTextBox;
        ASPxSpinEdit textSequencia = gridCadastro.FindEditFormTemplateControl("textSequencia") as ASPxSpinEdit;
        //

        clientePerfil.IdPerfil = Convert.ToInt32(hiddenIdPerfil.Text);
        clientePerfil.IdCliente = Convert.ToInt32(hiddenIdCliente.Text);
        clientePerfil.Sequencia = Convert.ToInt32(textSequencia.Text);

        clientePerfil.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de ClientePerfil - Operacao: Insert ClientePerfil: " + clientePerfil.IdCliente + "; " + clientePerfil.IdCliente + "; " + UtilitarioWeb.ToString(clientePerfil),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// Compoe a Chave Primária
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {

        if (e.Column.FieldName == "CompositeKey")
        {
            string idPerfil = Convert.ToString(e.GetListSourceFieldValue("IdPerfil"));
            string idCliente = Convert.ToString(e.GetListSourceFieldValue("IdCliente"));
            string sequencia = Convert.ToString(e.GetListSourceFieldValue("Sequencia"));
            e.Value = idPerfil + idCliente + sequencia;
        }

        if (e.Column.FieldName == "DescricaoPerfil")
        {
            string descricao = Convert.ToString(e.GetListSourceFieldValue("Descricao"));
            e.Value = descricao;
        }

        if (e.Column.FieldName == "DescricaoCliente")
        {
            string nome = Convert.ToString(e.GetListSourceFieldValue("Nome"));
            e.Value = nome;
        }

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        ClientePerfil clientePerfil = new ClientePerfil();
        //
        ASPxTextBox hiddenIdPerfil = gridCadastro.FindEditFormTemplateControl("hiddenIdPerfil") as ASPxTextBox;
        ASPxTextBox hiddenIdCliente = gridCadastro.FindEditFormTemplateControl("hiddenIdCliente") as ASPxTextBox;
        ASPxSpinEdit textSequencia = gridCadastro.FindEditFormTemplateControl("textSequencia") as ASPxSpinEdit;
        //
        // Chaves
        int idPerfil   = Convert.ToInt32(hiddenIdPerfil.Text);
        int idCliente = Convert.ToInt32(hiddenIdCliente.Text);
        int sequencia = Convert.ToInt32(textSequencia.Text);
        //
        if (clientePerfil.LoadByPrimaryKey(idCliente, idPerfil))
        {
            clientePerfil.Sequencia = sequencia;
            clientePerfil.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de ClientePerfil - Operacao: Update ClientePerfil: " + idPerfil + "; " + idCliente + "; " + UtilitarioWeb.ToString(clientePerfil),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesIdPerfil = gridCadastro.GetSelectedFieldValues("IdPerfil");
            List<object> keyValuesIdCliente = gridCadastro.GetSelectedFieldValues("IdCliente");
            List<object> keyValuesSequencia = gridCadastro.GetSelectedFieldValues("Sequencia");

            for (int i = 0; i < keyValuesIdPerfil.Count; i++)
            {
                int idPerfil   = Convert.ToInt32(keyValuesIdPerfil[i]);
                int idCliente = Convert.ToInt32(keyValuesIdCliente[i]);
                int sequencia = Convert.ToInt32(keyValuesSequencia[i]);

                ClientePerfil clientePerfil = new ClientePerfil();
                if (clientePerfil.LoadByPrimaryKey(idCliente, idPerfil))
                {
                    //
                    ClientePerfil clientePerfilClone = (ClientePerfil)Utilitario.Clone(clientePerfil);
                    //

                    clientePerfil.MarkAsDeleted();
                    clientePerfil.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de ClientePerfil - Operacao: Delete ClientePerfil: " + clientePerfilClone.IdPerfil + "; " + clientePerfilClone.IdCliente + UtilitarioWeb.ToString(clientePerfilClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("btnEditPerfil");
        base.gridCadastro_PreRender(sender, e);
    }

    /// <summary>
    /// Troca para Like no filtro campo Tabela Adm Global.
    /// </summary>
    public void FiltroGridPerfil() 
    {
        GridViewDataColumn colPerfil = gridPerfil.Columns[PerfilProcessamentoMetadata.ColumnNames.Descricao] as GridViewDataColumn;
        if (colPerfil != null)
        {
            colPerfil.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
        }
    }

    /// <summary>
    /// Troca para Like no filtro campo Tabela Adm Associada.
    /// </summary>
    public void FiltroGridCliente()
    {
        GridViewDataColumn colCliente = gridCliente.Columns[ClienteMetadata.ColumnNames.Nome] as GridViewDataColumn;
        if (colCliente != null)
        {
            colCliente.Settings.AutoFilterCondition = AutoFilterCondition.Contains;
        }
    }
}