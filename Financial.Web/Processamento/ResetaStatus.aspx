﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ResetaStatus.aspx.cs" Inherits="Processamento_ResetaStatus" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="Atatika.Web.UI" Namespace="Atatika.Web.UI" TagPrefix="atk" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">    
    
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />    
    <script type="text/javascript" language="Javascript" src="../js/global.js"></script>
    <script type="text/javascript" language="Javascript">
    var popup = true;
    document.onkeydown=onDocumentKeyDown;    
    </script>
</head>

<body>
    <form id="form1" runat="server">
    
    <dxcb:ASPxCallback ID="callBackReset" runat="server" OnCallback="callBackReset_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {            
            if (e.result != '') 
            {
                alert(e.result);
                gridConsulta.PerformCallback('btnRefresh');                
            }
        }        
        "/>
    </dxcb:ASPxCallback>
    
    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">

    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Reseta Status de Processamento"></asp:Label>
    </div>
           
    <div id="mainContent">

            <div class="linkButton" >               
               <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal4" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel"  OnClick="btnExcel_Click"><asp:Literal ID="Literal5" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>              
               <asp:LinkButton ID="btnReset" runat="server" Font-Overline="false" CssClass="btnLote" OnClientClick="if (confirm('Este é um processo irreversível, tem certeza que quer realizar o reset?')==true) callBackReset.SendCallback(); return false;"><asp:Literal ID="Literal8" runat="server" Text="Reseta Status"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick=" gridConsulta.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal6" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
            </div>
        
            <div class="divDataGrid">            
                <dxwgv:ASPxGridView ID="gridConsulta" runat="server" EnableCallBacks="true"
                    KeyFieldName="IdCliente" DataSourceID="EsDSCliente"                    
                    OnCustomUnboundColumnData="gridConsulta_CustomUnboundColumnData"   
                    OnCustomCallback="gridConsulta_CustomCallback"                
                    >        
                        
                <Columns>           
                    <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                        <HeaderStyle HorizontalAlign="Center"/>
                        <HeaderTemplate>
                            <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridConsulta);}" OnInit="CheckBoxSelectAll"/>
                        </HeaderTemplate>
                    </dxwgv:GridViewCommandColumn>
                    
                    <dxwgv:GridViewDataTextColumn FieldName="IdCliente" Caption="Id" Width="10%" VisibleIndex="1" CellStyle-HorizontalAlign="left" ReadOnly="true" />
                    <dxwgv:GridViewDataTextColumn FieldName="Apelido" Caption="Nome" Width="60%" VisibleIndex="2" ReadOnly="true" />
                    
                    <dxwgv:GridViewDataDateColumn FieldName="DataDia" VisibleIndex="3" Width="10%"/>                    
                    <dxwgv:GridViewDataTextColumn FieldName="StatusDescricao" Caption="Status" UnboundType="String" VisibleIndex="4" Width="15%" Settings-AutoFilterCondition="Contains" />                    
                    <dxwgv:GridViewDataTextColumn FieldName="Status" Visible="False"/>
                    
                    <dxwgv:GridViewDataTextColumn FieldName="StatusProcessando" Caption="Em Processamento" UnboundType="String" VisibleIndex="5" Width="10%" ReadOnly="true"
                     CellStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"/>
                    <dxwgv:GridViewDataTextColumn FieldName="IsProcessando" Visible="false"  />
                    
                </Columns>
                <SettingsCommandButton>
                    <ClearFilterButton Image-Url="../imagens/funnel--minus.png"/>
                </SettingsCommandButton>
                
            </dxwgv:ASPxGridView>            
            </div>     
    </div>
    </div>
    </td></tr></table>
    </div>        
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridConsulta" />
        
    <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect" />
        
    </form>
</body>
</html>