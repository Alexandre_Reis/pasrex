﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using DevExpress.Web;
using Financial.Web.Common;
using Financial.Security;
using Financial.Security.Enums;
using Financial.Util;
using Financial.Fundo;

public partial class Processamento_ConsultaAjusteCotas : CadastroBasePage
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void Page_Load(object sender, EventArgs e) 
    {
        this.HasEditControl = false;
        this.HasFiltro = true;
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSConsultaAjusteCotas_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) 
    {
        ASPxDateEdit textDataInicio = popupFiltro.FindControl("textDataInicio") as ASPxDateEdit;
        ASPxDateEdit textDataFim = popupFiltro.FindControl("textDataFim") as ASPxDateEdit;

        HistoricoCotaQuery historicoCotaQuery = new HistoricoCotaQuery("HC");
        TravamentoCotasQuery travamentoCotasQuery = new TravamentoCotasQuery("TC");
        CarteiraQuery carteiraQuery = new CarteiraQuery("C");

        travamentoCotasQuery.Select(travamentoCotasQuery.IdTravamentoCota,
                                    carteiraQuery.Apelido,
                                    travamentoCotasQuery.IdCarteira,
                                    travamentoCotasQuery.DataProcessamento,
                                    travamentoCotasQuery.TipoBloqueio,
                                    historicoCotaQuery.CotaImportada,
                                    travamentoCotasQuery.ValorCotaCalculada,
                                    travamentoCotasQuery.ValorCota,
                                    travamentoCotasQuery.AjusteCompensacao,
                                    carteiraQuery.BandaVariacao);
        travamentoCotasQuery.LeftJoin(carteiraQuery).On(travamentoCotasQuery.IdCarteira == carteiraQuery.IdCarteira);
        travamentoCotasQuery.LeftJoin(historicoCotaQuery).On(travamentoCotasQuery.DataProcessamento == historicoCotaQuery.Data & travamentoCotasQuery.IdCarteira == historicoCotaQuery.IdCarteira);


        if (textDataInicio.Text != "")
        {
            DateTime dataInicio = new DateTime(Convert.ToDateTime(textDataInicio.Text).Year, Convert.ToDateTime(textDataInicio.Text).Month, Convert.ToDateTime(textDataInicio.Text).Day);
            travamentoCotasQuery.Where(travamentoCotasQuery.DataProcessamento.GreaterThan(dataInicio));
        }

        if (textDataFim.Text != "")
        {
            DateTime dataFim = new DateTime(Convert.ToDateTime(textDataFim.Text).Year, Convert.ToDateTime(textDataFim.Text).Month, Convert.ToDateTime(textDataFim.Text).Day);
            travamentoCotasQuery.Where(travamentoCotasQuery.DataProcessamento.LessThan(dataFim));
        }

        travamentoCotasQuery.OrderBy(travamentoCotasQuery.DataProcessamento.Descending, carteiraQuery.Apelido.Ascending);

        TravamentoCotasCollection coll = new TravamentoCotasCollection();
        coll.Load(travamentoCotasQuery);

        e.Collection = coll;
    }
    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) 
    {        
        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    protected void gridCadastro_CustomUnboundColumnData(object sender, DevExpress.Web.ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "Variacao")
        {
            decimal cotaTravada = Convert.ToDecimal(e.GetListSourceFieldValue("ValorCota"));
            decimal cotaCalculada = Convert.ToDecimal(e.GetListSourceFieldValue("ValorCotaCalculada"));

            decimal valorPercentual = ((cotaCalculada / cotaTravada) - 1) * 100;
            if (valorPercentual < 0) valorPercentual = valorPercentual * (-1);
            decimal valorAbsoluto = cotaCalculada - cotaTravada;
            e.Value = valorAbsoluto.ToString("0.00000000") + " | " + valorPercentual.ToString("0.00000") + "%"; 
        }

        if (e.Column.FieldName == "VariacaoBanda")
        {
            decimal cotaTravada = Convert.ToDecimal(e.GetListSourceFieldValue("ValorCota"));
            decimal cotaCalculada = Convert.ToDecimal(e.GetListSourceFieldValue("ValorCotaCalculada"));
            decimal variacaoBanda = Convert.ToDecimal(e.GetListSourceFieldValue("BandaVariacao"));

            decimal valorPercentual = ((cotaCalculada / cotaTravada) - 1) * 100;
            if (valorPercentual < 0) valorPercentual = valorPercentual * (-1);

            if (variacaoBanda == null)
            {
                e.Value = "";
            }
            else
            {
                if (valorPercentual > variacaoBanda)
                {
                    e.Value = "SIM";
                }
                else
                {
                    e.Value = "Não";
                }
            }
            
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) 
    {        
        base.gridCadastro_PreRender(sender, e);
        
        Label labelFiltro = gridCadastro.FindStatusBarTemplateControl("labelFiltro") as Label;
        StringBuilder texto = new StringBuilder();
        texto.Append("");
        if (textDataInicio.Text != "") {
            texto.Append(" Data >= ").Append(textDataInicio.Text);
        }
        if (textDataFim.Text != "") {
            if (texto.Length > 0) {
                texto.Append(" |");
            }

            texto.Append(" Data <= ").Append(textDataFim.Text);
        }
        
        labelFiltro.Text = texto.ToString();
    }
        
}