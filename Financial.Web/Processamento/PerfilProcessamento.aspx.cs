﻿using System;

using Financial.RendaFixa;
using Financial.Web.Common;
using System.Collections.Generic;
using Financial.Util;
using Financial.Security;
using Financial.Web.Util;
using System.Web;
using Financial.Security.Enums;
using DevExpress.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using Financial.Fundo;
using EntitySpaces.Interfaces;

public partial class Processamento_PerfilProcessamento : CadastroBasePage {
    new protected void Page_Load(object sender, EventArgs e) {
        base.Page_Load(sender, e);
    }

    #region DataSources
    protected void EsDSPerfilProcessamento_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e) {

        PerfilProcessamentoQuery perfilProcessamentoQuery = new PerfilProcessamentoQuery("PPQ");
        PerfilProcessamentoCollection perfilProcessamentoCollection = new PerfilProcessamentoCollection();
        perfilProcessamentoCollection.Load(perfilProcessamentoQuery);

        e.Collection = perfilProcessamentoCollection;

    }

    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackErro_Callback(object source, DevExpress.Web.CallbackEventArgs e) {
        e.Result = "";
        //
        TextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as TextBox;
        ASPxComboBox dropTipo = gridCadastro.FindEditFormTemplateControl("dropTipo") as ASPxComboBox;

        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { textDescricao, dropTipo});

        if (base.TestaObrigatorio(controles) != "") {
            e.Result = "Campos com * são obrigatórios!";
            return;
        }
        #endregion

    }

    protected void callbackAdd_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        SalvarNovo();

        e.Result = "";
        if (e.Parameter == "clone")
        {
            //e.Result = 0;
        }
        else
        {
            e.Result = "Operação feita com sucesso.";
        }
    }

    private void SalvarNovo()
    {
        PerfilProcessamento perfilProcessamento = new PerfilProcessamento();
        //
        TextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as TextBox;
        ASPxComboBox dropTipo = gridCadastro.FindEditFormTemplateControl("dropTipo") as ASPxComboBox;
        ASPxSpinEdit textDiasFechamento = gridCadastro.FindEditFormTemplateControl("textDiasFechamento") as ASPxSpinEdit;
        ASPxSpinEdit textDiasRetroagir = gridCadastro.FindEditFormTemplateControl("textDiasRetroagir") as ASPxSpinEdit;
        ASPxComboBox dropAberturaAutomatica = gridCadastro.FindEditFormTemplateControl("dropAberturaAutomatica") as ASPxComboBox;
        //

        perfilProcessamento.Descricao = Convert.ToString(textDescricao.Text);
        perfilProcessamento.Tipo = Convert.ToInt32(dropTipo.SelectedItem.Value);
        perfilProcessamento.DiasFechamento = Convert.ToInt32(textDiasFechamento.Value);
        perfilProcessamento.DiasRetroagir = Convert.ToInt32(textDiasRetroagir.Value);
        perfilProcessamento.AberturaAutomatica = Convert.ToString(dropAberturaAutomatica.SelectedItem.Value);

        perfilProcessamento.Save();

        #region Log do Processo
        HistoricoLog historicoLog = new HistoricoLog();
        historicoLog.InsereHistoricoLog(DateTime.Now,
                                        DateTime.Now,
                                        "Cadastro de PerfilProcessamento - Operacao: Insert PerfilProcessamento: " + perfilProcessamento.IdPerfil+ UtilitarioWeb.ToString(perfilProcessamento),
                                        HttpContext.Current.User.Identity.Name,
                                        UtilitarioWeb.GetIP(Request),
                                        "",
                                        HistoricoLogOrigem.Outros);
        #endregion
    }

    protected void btnOKAdd_Init(object sender, EventArgs e)
    {
        if (!gridCadastro.IsNewRowEditing)
            (sender as LinkButton).Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e) {
        int idPerfil = (int)e.Keys[0];
        PerfilProcessamento perfilProcessamento = new PerfilProcessamento();
        //
        TextBox textDescricao = gridCadastro.FindEditFormTemplateControl("textDescricao") as TextBox;
        ASPxComboBox dropTipo = gridCadastro.FindEditFormTemplateControl("dropTipo") as ASPxComboBox;
        ASPxSpinEdit textDiasFechamento = gridCadastro.FindEditFormTemplateControl("textDiasFechamento") as ASPxSpinEdit;
        ASPxSpinEdit textDiasRetroagir = gridCadastro.FindEditFormTemplateControl("textDiasRetroagir") as ASPxSpinEdit;
        ASPxComboBox dropAberturaAutomatica = gridCadastro.FindEditFormTemplateControl("dropAberturaAutomatica") as ASPxComboBox;
        //
        if (perfilProcessamento.LoadByPrimaryKey(idPerfil))
        {
            perfilProcessamento.Descricao = Convert.ToString(textDescricao.Text);
            perfilProcessamento.Tipo = Convert.ToInt32(dropTipo.Value);
            perfilProcessamento.DiasFechamento = Convert.ToInt32(textDiasFechamento.Value);
            perfilProcessamento.DiasRetroagir = Convert.ToInt32(textDiasRetroagir.Value);
            perfilProcessamento.AberturaAutomatica = Convert.ToString(dropAberturaAutomatica.Value);

            perfilProcessamento.Save();

            #region Log do Processo
            HistoricoLog historicoLog = new HistoricoLog();
            historicoLog.InsereHistoricoLog(DateTime.Now,
                                            DateTime.Now,
                                            "Cadastro de PerfilProcessamento - Operacao: Update PerfilProcessamento: " + idPerfil + "; " + UtilitarioWeb.ToString(perfilProcessamento),
                                            HttpContext.Current.User.Identity.Name,
                                            UtilitarioWeb.GetIP(Request),
                                            "",
                                            HistoricoLogOrigem.Outros);
            #endregion
        }
                     
        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e) {
        SalvarNovo();

        e.Cancel = true;
        gridCadastro.CancelEdit();
        gridCadastro.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e) {
        if (e.Parameters == "btnDelete") {
            List<object> keyValuesIdPerfil = gridCadastro.GetSelectedFieldValues(PerfilProcessamentoMetadata.ColumnNames.IdPerfil);

            for (int i = 0; i < keyValuesIdPerfil.Count; i++)
            {
                int idPerfil = Convert.ToInt32(keyValuesIdPerfil[i]);

                PerfilProcessamento perfilProcessamento = new PerfilProcessamento();
                if (perfilProcessamento.LoadByPrimaryKey(idPerfil))
                {
                    //
                    PerfilProcessamento perfilProcessamentoClone = (PerfilProcessamento)Utilitario.Clone(perfilProcessamento);
                    //

                    perfilProcessamento.MarkAsDeleted();
                    perfilProcessamento.Save();

                    #region Log do Processo
                    HistoricoLog historicoLog = new HistoricoLog();
                    historicoLog.InsereHistoricoLog(DateTime.Now,
                                                    DateTime.Now,
                                                    "Cadastro de PerfilProcessamento - Operacao: Delete PerfilProcessamento: " + perfilProcessamentoClone.IdPerfil + "; " + UtilitarioWeb.ToString(perfilProcessamentoClone),
                                                    HttpContext.Current.User.Identity.Name,
                                                    UtilitarioWeb.GetIP(Request),
                                                    "",
                                                    HistoricoLogOrigem.Outros);
                    #endregion
                }
            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    new protected void gridCadastro_PreRender(object sender, EventArgs e) {
        this.FocaCampoGrid("btnEditTabelaAdm", "textPercentual");
        base.gridCadastro_PreRender(sender, e);
    }

}