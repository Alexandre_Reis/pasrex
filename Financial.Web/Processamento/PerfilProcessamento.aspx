﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PerfilProcessamento.aspx.cs" Inherits="Processamento_PerfilProcessamento" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>    
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>  
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">

    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />    
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>
</head>

<body>
    <form id="form1" runat="server">
    
    <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
        <ClientSideEvents CallbackComplete="function(s, e) {             
            if (e.result != '')
            {                   
                alert(e.result);                              
            }
            else
            {
                if (operacao == 'salvar')
                {             
                    gridCadastro.UpdateEdit();
                }
                else
                {
                    callbackAdd.SendCallback();
                }                
            }
            operacao = '';
        }        
        "/>
    </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackAdd" runat="server" OnCallback="callbackAdd_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            alert('Operação feita com sucesso.');
        }        
        " />
        </dxcb:ASPxCallback>

    <div class="divPanel">
    <table width="100%"><tr><td>
    <div id="container">
    
    <div id="header">
        <asp:Label ID="lblHeader" runat="server" Text="Perfil de Processamento"/>
    </div>
    
    <div id="mainContent">
                   
            <div class="linkButton" >               
               <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal10" runat="server" Text="Novo"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;"><asp:Literal ID="Literal1" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" CssClass="btnPdf" OnClick="btnPDF_Click"><asp:Literal ID="Literal2" runat="server" Text="Gerar PDF"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" CssClass="btnExcel" OnClick="btnExcel_Click"><asp:Literal ID="Literal3" runat="server" Text="Gerar Excel"/><div></div></asp:LinkButton>
               <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh" OnClientClick="gridCadastro.PerformCallback('btnRefresh'); return false;"><asp:Literal ID="Literal4" runat="server" Text="Atualizar"/><div></div></asp:LinkButton>
            </div>
    
            <div class="divDataGrid">            
            <dxwgv:ASPxGridView ID="gridCadastro" runat="server" EnableCallBacks="true"
                KeyFieldName="IdPerfil" DataSourceID="EsDSPerfilProcessamento"
                OnCustomCallback="gridCadastro_CustomCallback"
                OnRowInserting="gridCadastro_RowInserting"
                OnRowUpdating="gridCadastro_RowUpdating"
                OnBeforeGetCallbackResult="gridCadastro_PreRender"
                >                    
            <Columns>           
                <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="7%" ButtonType="Image" ShowClearFilterButton="True">
                    <HeaderTemplate>
                        <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                    </HeaderTemplate>
                </dxwgv:GridViewCommandColumn>
                
                <dxwgv:GridViewDataTextColumn FieldName="CompositeKey" UnboundType="String" Visible="false" />
                <dxwgv:GridViewDataColumn FieldName="IdPerfil" Visible="false"></dxwgv:GridViewDataColumn>
                                           
                <dxwgv:GridViewDataColumn FieldName="Descricao" Caption="Descrição" UnboundType="String" VisibleIndex="1" Width="40%" CellStyle-HorizontalAlign="left" Settings-AutoFilterCondition="Contains" />
                
                <dxwgv:GridViewDataComboBoxColumn FieldName="Tipo" Caption="Tipo" VisibleIndex="2" Width="15%">
                    <PropertiesComboBox EncodeHtml="false">
                        <Items>
                            <dxe:ListEditItem Value="1" Text="<div title='Automático'>Automático</div>" />
                            <dxe:ListEditItem Value="2" Text="<div title='Manual'>Manual</div>" />
                        </Items>
                    </PropertiesComboBox>
                </dxwgv:GridViewDataComboBoxColumn>  
                                              
            </Columns>
            
            <Templates>
                
                <EditForm>                                                                   
                    <div class="editForm">       
                    
                        <table>
                            <tr>                                    
                                <td  class="td_Label">
                                    <asp:Label ID="labelDescricao" runat="server" CssClass="labelRequired" Text="Descriçao:"  />
                                </td>                                     
                                
                                <td>
                                    <asp:TextBox ID="textDescricao" runat="server" CssClass="textValor_4" MaxLength="40" Text='<%# Eval("Descricao") %>'/>
                                </td>
                                
                            </tr>  
                                                                                                                                                                       
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelTipo" runat="server" CssClass="labelRequired" Text="Tipo:"/>
                                </td>
                                <td>
                                    <dxe:ASPxComboBox ID="dropTipo" runat="server" ClientInstanceName="dropTipo"
                                        ShowShadow="False" CssClass="dropDownListCurto_1" ValueType="System.String" Text='<%# Eval("Tipo") %>'>
                                        <Items>
                                            <dxe:ListEditItem Value="" Text="" />
                                            <dxe:ListEditItem Value="1" Text="Automático" />
                                            <dxe:ListEditItem Value="2" Text="Manual" />
                                        </Items>
                                    </dxe:ASPxComboBox>
                                </td>                                                      
                            </tr>
                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelDiasFechamento" runat="server" CssClass="labelNormal" Text="Qtd. Dias para Data Fechamento (D-N):"/>
                                </td>
                                <td>
                                    <dxe:ASPxSpinEdit ID="textDiasFechamento" runat="server" CssClass="textCurto" ClientInstanceName="textDiasFechamento"
                                        MaxLength="2" NumberType="integer" Text='<%# Eval("DiasFechamento") %>'/>
                                </td>                                                    
                            </tr>
                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelDiasRetroagir" runat="server" CssClass="labelNormal" Text="Limite de Dias para Retroagir:"/>
                                </td>
                                <td>
                                    <dxe:ASPxSpinEdit ID="textDiasRetroagir" runat="server" CssClass="textCurto" ClientInstanceName="textDiasRetroagir"
                                        MaxLength="2" NumberType="integer" Text='<%# Eval("DiasRetroagir") %>'/>
                                </td>                                                    
                            </tr>
                            
                            <tr>
                                <td class="td_Label">
                                    <asp:Label ID="labelAberturaAutomatica" runat="server" CssClass="labelNormal" Text="Executa Processo Abertura Automática (D+1)?"/>
                                </td>
                                <td>
                                    <dxe:ASPxComboBox ID="dropAberturaAutomatica" runat="server" ClientInstanceName="dropAberturaAutomatica"
                                        ShowShadow="False" CssClass="dropDownListCurto_6" ValueType="System.String" Text='<%# Eval("AberturaAutomatica") %>'>
                                        <Items>
                                            <dxe:ListEditItem Value="" Text="" />
                                            <dxe:ListEditItem Value="S" Text="Sim" />
                                            <dxe:ListEditItem Value="N" Text="Não" />
                                        </Items>
                                    </dxe:ASPxComboBox>
                                </td>                                                      
                            </tr>
                            
                        </table>
                        
                        <div class="linhaH"></div>
                
                        <div class="linkButton linkButtonNoBorder popupFooter">
                            <asp:LinkButton ID="btnOKAdd" runat="server" Font-Overline="false" CssClass="btnSaveadd" OnInit="btnOKAdd_Init"
                                                            OnClientClick="operacao='salvarAdd';callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal6" runat="server" Text="OK+"/><div></div></asp:LinkButton>
                            <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" CssClass="btnOK"
                                                            OnClientClick="operacao='salvar';callbackErro.SendCallback(); return false;"><asp:Literal ID="Literal10" runat="server" Text="OK"/><div></div></asp:LinkButton>
                            <asp:LinkButton ID="btnCancel" runat="server" Font-Overline="false" CssClass="btnCancel" OnClientClick="gridCadastro.CancelEdit(); return false;"><asp:Literal ID="Literal5" runat="server" Text="Cancelar"/><div></div></asp:LinkButton>
                        </div>
                    </div>                    
                </EditForm>
                
            </Templates>
            
            <SettingsPopup EditForm-Width="500px" />
            <SettingsCommandButton>
                <ClearFilterButton Image-Url="../../imagens/funnel--minus.png"/>
            </SettingsCommandButton>
            
        </dxwgv:ASPxGridView>            
        </div>
                    
    </div>
    </div>
    </td></tr></table>
    </div>        
    
    <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridCadastro" />
        
    <cc1:esDataSource ID="EsDSPerfilProcessamento" runat="server" OnesSelect="EsDSPerfilProcessamento_esSelect" />      
    </form>
</body>
</html>