﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ProcessaDiario.aspx.cs" Inherits="Processamento_ProcessaDiario" %>

<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxw" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../js/global.js"></script>

    <script type="text/javascript" src="../js/jquery.js"></script>

    <script type="text/javascript" src="../js/jquery.progressbar.js"></script>

    <script type="text/javascript" src="../js/process.js?v=2"></script>

    <script type="text/javascript" language="Javascript">
        var popup = true;
        document.onkeydown=onDocumentKeyDown;
        var tipoProcesso = 2;    
        var perfil = 0;
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <dxcb:ASPxCallback ID="callbackErro" runat="server" OnCallback="callbackErro_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            if (e.result != '')
            {                   
                alert(e.result); 
                window.processing = false;                             
            }
            else
            {                
                processaBolsa = document.getElementById('popupOpcoes_checkProcessaBolsa').checked;
                processaBMF = document.getElementById('popupOpcoes_checkProcessaBMF').checked;
                processaRendaFixa = document.getElementById('popupOpcoes_checkProcessaRendaFixa').checked;
                processaSwap = document.getElementById('popupOpcoes_checkProcessaSwap').checked;
                processaFundo = document.getElementById('popupOpcoes_checkProcessaFundo').checked;
                processaCambio = document.getElementById('popupOpcoes_checkProcessaCambio').checked;
                integraBolsa = document.getElementById('popupOpcoes_checkIntegraBolsa').checked;
                integraRendaFixa = document.getElementById('popupOpcoes_checkIntegraRendaFixa').checked;
                integraBMF = document.getElementById('popupOpcoes_checkIntegraBMF').checked;
                integraCC = document.getElementById('popupOpcoes_checkIntegraCC').checked;
                ignoraOperacoes = document.getElementById('popupOpcoes_checkIgnoraOperacoes').checked;
                remuneraRF = document.getElementById('popupOpcoes_checkRemuneraRF').checked;
                cravaCota = document.getElementById('popupOpcoes_checkCravaCota').checked;
                resetCusto = document.getElementById('popupOpcoes_checkResetCusto').checked;
                
                Financial.Util.ProgressBar.PrepareProcess({Grid: gridConsulta, TipoProcesso: tipoProcesso, ProcessaBolsa: processaBolsa, ProcessaBMF: processaBMF,
                        ProcessaRendaFixa: processaRendaFixa, ProcessaSwap: processaSwap, ProcessaFundo: processaFundo, ProcessaCambio: processaCambio, IntegraBolsa: integraBolsa, 
                        IntegraRendaFixa: integraRendaFixa, IntegraBMF: integraBMF, IntegraCC: integraCC, IgnoraOperacoes: ignoraOperacoes, RemuneraRF: remuneraRF,
                        CravaCota: cravaCota, ResetCusto: resetCusto, Perfil: perfil, TipoProcessamento: tipoProcesso});
            }
            operacao = '';
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackCheck" runat="server" OnCallback="callbackCheck_Callback">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            if (e.result != '')
            {                   
                alert(e.result);
            }
        }        
        " />
        </dxcb:ASPxCallback>
        <dxcb:ASPxCallback ID="callbackPreProcessamento" runat="server">
            <ClientSideEvents CallbackComplete="function(s, e) 
        {
            popupPreProcessamento.Show();
            gridPreProcessamento.PerformCallback('processar');
        }        
        " />
        </dxcb:ASPxCallback>        
        <dx:ASPxPopupControl ID="progressWindow" ClientInstanceName="progressWindow" Width="300"
            Height="120" HeaderText="Processamento de Clientes" Modal="true" CloseAction="None"
            ShowCloseButton="false" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
            runat="server">
            <ContentCollection>
                <dx:PopupControlContentControl>
                    <div id="progressBarWrapper">
                        <div id="client-info">
                        </div>
                        <div id="resumo-processamento">
                            <p style="color: green">
                                Processados com sucesso: <span id="total-sucessos">0</span></p>
                            <p id="wrapper-erros" style="color: red; display: none;">
                                Processados com erro: <span id="total-erros">0</span></p>
                        </div>
                        <div class="progressBar" id="spaceused1">
                            0%
                        </div>
                        <div class="linkButton linkButtonNoBorder">
                            <asp:LinkButton ID="btnStop" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                CssClass="btnStop" OnClientClick="Financial.Util.ProgressBar.StopProcess();return false;">
                                <asp:Literal ID="Literal7" runat="server" Text="Parar" /><div>
                                </div>
                            </asp:LinkButton>
                            <asp:LinkButton ID="btnClose" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                CssClass="btnOK" OnClientClick="gridConsulta.PerformCallback('btnRefresh'); progressWindow.Hide(); return false;">
                                <asp:Literal ID="Literal1" runat="server" Text="Fechar" /><div>
                                </div>
                            </asp:LinkButton>
                            <asp:LinkButton ID="btnErrors" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                CssClass="btnStop" OnClientClick="outputProcessamento.Show();return false;">
                                <asp:Literal ID="Literal9" runat="server" Text="Exibir Erros" /><div>
                                </div>
                            </asp:LinkButton>
                        </div>
                    </div>
                </dx:PopupControlContentControl>
            </ContentCollection>
        </dx:ASPxPopupControl>
        <dx:ASPxPopupControl ID="outputProcessamento" ClientInstanceName="outputProcessamento"
            Width="600" Height="420" HeaderText="Resultado do Processamento" Modal="true"
            CloseAction="CloseButton" ScrollBars="Vertical" ShowCloseButton="true" PopupHorizontalAlign="WindowCenter"
            PopupVerticalAlign="WindowCenter" runat="server">
            <ContentCollection>
                <dx:PopupControlContentControl>
                    <div id="output-info">
                    </div>
                </dx:PopupControlContentControl>
            </ContentCollection>
        </dx:ASPxPopupControl>
        <dxpc:ASPxPopupControl ID="popupOpcoes" AllowDragging="true" PopupElementID="popupOpcoes"
            EnableClientSideAPI="True" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
            Width="700" Left="250" Top="70" HeaderText="Opções para Processamento" runat="server"
            HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
            <ContentCollection>
                <dxpc:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                    <table>
                        <tr>
                            <td>
                                <asp:CheckBox ID="checkProcessaBolsa" runat="server" Text="Processa Bolsa" Checked="true" />
                            </td>
                            <td>
                                <asp:CheckBox ID="checkProcessaBMF" runat="server" Text="Processa BMF" Checked="true" />
                            </td>
                            <td>
                                <asp:CheckBox ID="checkProcessaRendaFixa" runat="server" Text="Processa Renda Fixa" Checked="true" />
                            </td>
                            <td>
                                <asp:CheckBox ID="checkProcessaSwap" runat="server" Text="Processa Swap" Checked="true" />
                            </td>
                            <td>
                                <asp:CheckBox ID="checkProcessaFundo" runat="server" Text="Processa Fundo" Checked="true" />
                            </td>
                            <td>
                                <asp:CheckBox ID="checkProcessaCambio" runat="server" Text="Processa Câmbio" Checked="true" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="checkIntegraBolsa" runat="server" Text="Integra Bolsa" Checked="true" />
                            </td>
                            <td>
                                <asp:CheckBox ID="checkIntegraBMF" runat="server" Text="Integra BMF" Checked="true" />
                            </td>
                            <td>
                                <asp:CheckBox ID="checkIntegraRendaFixa" runat="server" Text="Integra Renda Fixa"
                                    Checked="true" />
                            </td>
                            <td>
                                <asp:CheckBox ID="checkIntegraCC" runat="server" Text="Integra C/C" Checked="true" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="checkIgnoraOperacoes" runat="server" Text="Ignora Operações" Checked="false" />
                            </td>
                            <td>
                                <asp:CheckBox ID="checkRemuneraRF" runat="server" Text="Remunera RF" Checked="true" Enabled="false" />
                            </td>
                            <td>
                                <asp:CheckBox ID="checkCravaCota" runat="server" Text="Crava Cota" Checked="false" />
                            </td>
                            <td>
                                <asp:CheckBox ID="checkResetCusto" runat="server" Text="Reset Custo" Checked="false" />
                            </td>
                        </tr>
                    </table>
                    <div class="linkButton linkButtonNoBorder" style="margin-top: 20px">
                        <asp:LinkButton ID="btnOK" runat="server" Font-Overline="false" ForeColor="Black"
                            CssClass="btnOK" OnClientClick="popupOpcoes.Hide(); return false;">
                            <asp:Literal ID="Literal10" runat="server" Text="OK" /><div>
                            </div>
                        </asp:LinkButton>
                    </div>
                </dxpc:PopupControlContentControl>
            </ContentCollection>
        </dxpc:ASPxPopupControl>
        <dxpc:ASPxPopupControl ID="popupPreProcessamento" AllowDragging="true" PopupElementID="popupFiltro"
            EnableClientSideAPI="True" PopupVerticalAlign="Middle" PopupHorizontalAlign="OutsideRight"
             Width="500" Left="250" Top="70" HeaderText="Pré Processamento"
            runat="server" HeaderStyle-BackColor="#EBECEE" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Size="11px">
            <ContentCollection>
                <dxpc:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                    <table border="0">
                        <tr>
                            <dxwgv:ASPxGridViewExporter ExportedRowType="Selected" GridViewID="gridPreProcessamento" ID="exportGridPreProc" runat="server">
                            </dxwgv:ASPxGridViewExporter>
                            <dxwgv:ASPxGridView ID="gridPreProcessamento" runat="server" EnableCallBacks="true" ClientInstanceName="gridPreProcessamento"
                            OnCustomCallback="gridPreProcessamento_CustomCallback" DataSourceID="EsDSPreProcessamento" Width="500">
                            <Columns>
                                <dxwgv:GridViewDataColumn FieldName="Pendencia" Caption="Pendência" VisibleIndex="0" Width="30%" CellStyle-HorizontalAlign="left" />
                                <dxwgv:GridViewDataColumn FieldName="Descricao" Caption="Descrição" VisibleIndex="1" Width="55%" CellStyle-HorizontalAlign="left" />
                                <dxwgv:GridViewDataColumn FieldName="Data" Caption="Data" VisibleIndex="2" Width="15%" CellStyle-HorizontalAlign="left" />
                            </Columns>
                            </dxwgv:ASPxGridView>
                        </tr>
                        <tr> 
                            <td>
                            <div class="linkButton">
	                            <asp:LinkButton ID="LinkButton1" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                    CssClass="btnExcel" OnClick="brtnPreProc_Click">
                                    <asp:Literal ID="Literal11" runat="server" Text="Gerar Excel" /><div>
                                    </div>
                                </asp:LinkButton>									
                            </div>
                            </td>
                        </tr>
                    </table>
                </dxpc:PopupControlContentControl>
            </ContentCollection>
        </dxpc:ASPxPopupControl>        
        <div class="divPanel">
            <table width="100%">
                <tr>
                    <td>
                        <div id="container">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Processamento Diário" />
                            </div>
                            <div id="mainContent">
                                <table class="dropDownInlineWrapper">
                                    <tr>
                                        <td>
                                            <asp:Label ID="labelGrupoProcessamento" runat="server" CssClass="labelNormal" Text="Grupo:"></asp:Label>
                                        </td>
                                        <td>
                                            <dxe:ASPxComboBox ID="dropGrupoProcessamento" runat="server" ClientInstanceName="dropGrupoProcessamento"
                                                DataSourceID="EsDSGrupoProcessamento" ShowShadow="true" DropDownStyle="DropDownList"
                                                CssClass="dropDownListCurto1 dropDownInline" TextField="Descricao" ValueField="IdGrupoProcessamento">
                                                <ClientSideEvents SelectedIndexChanged="function(s, e) {gridConsulta.PerformCallback('btnRefresh'); return false;}" />
                                            </dxe:ASPxComboBox>
                                        </td>
                                        <td>
                                            <asp:Label ID="label2" runat="server" CssClass="labelNormal" Text="Status:"></asp:Label>
                                        </td>
                                        <td>
                                            <dxe:ASPxComboBox ID="dropStatus" ClientInstanceName="dropStatus" runat="server"
                                                ShowShadow="false" DropDownStyle="DropDownList" CssClass="dropDownListCurto1 dropDownInline">
                                                <Items>
                                                    <dxe:ListEditItem Value="1" Text="Abertos" />
                                                    <dxe:ListEditItem Value="5" Text="Abertos C/Erro" />
                                                    <dxe:ListEditItem Value="2" Text="Fechados" />
                                                    <dxe:ListEditItem Value="4" Text="Fechados C/Erro" />                                                    
                                                    <dxe:ListEditItem Value="3" Text="Divulgados" />                                                                                                        
                                                </Items>
                                                <ClientSideEvents SelectedIndexChanged="function(s, e) {gridConsulta.PerformCallback('btnRefresh'); return false;}" />
                                            </dxe:ASPxComboBox>
                                        </td>
                                        <td>
                                            <asp:Label ID="label1" runat="server" CssClass="labelNormal" Text="Tipo Processamento:"></asp:Label>
                                        </td>
                                        <td>
                                            <dxe:ASPxComboBox ID="dropTipoProcessamento" ClientInstanceName="dropTipoProcessamento" runat="server" ShowShadow="false" DropDownStyle="DropDownList"
                                                CssClass="dropDownListCurto1 dropDownInline">
                                                <Items>
                                                    <dxe:ListEditItem Value="2" Text="Abertura" />
                                                    <dxe:ListEditItem Value="0" Text="Fechamento" />
                                                    <dxe:ListEditItem Value="1" Text="Divulgação" />                                                    
                                                </Items>
                                                <ClientSideEvents SelectedIndexChanged="function(s, e) {tipoProcesso = s.GetSelectedItem().value;gridConsulta.PerformCallback('btnRefresh'); return false;}" />
                                            </dxe:ASPxComboBox>
                                        </td>                                        
                                        <td style="padding-left: 50px;">
                                            <asp:Label ID="label3" runat="server" CssClass="labelNormal" Text="Perfil:"></asp:Label>
                                        </td>
                                        <td>
                                            <dxe:ASPxComboBox ID="dropPerfilProcessamento" runat="server" ClientInstanceName="dropPerfilProcessamento"
                                                DataSourceID="EsDSPerfilProcessamento" ShowShadow="true" DropDownStyle="DropDownList"
                                                CssClass="dropDownListCurto1 dropDownInline" TextField="Descricao" ValueField="IdPerfil">
                                                <ClientSideEvents SelectedIndexChanged="function(s, e) {perfil = s.GetValue();gridConsulta.PerformCallback('btnRefresh'); return false;}" />
                                            </dxe:ASPxComboBox>
                                        </td>
                                                                                
                                    </tr>
                                </table>
                                <div class="linkButton">
                                    <asp:LinkButton ID="btnRun" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnRun" OnClientClick="
                                            if(window.processing === true){
                                                alert('Processamento já foi iniciado. Por favor, aguarde.');
                                            }else{
                                                window.processing = true; callbackErro.SendCallback();
                                            }
                                            return false;">
                                        <asp:Literal ID="Literal2" runat="server" Text="Processar" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnPreProcessamento" runat="server" Font-Overline="false" ValidationGroup="ATK" 
                                        CssClass="btnTick" OnClientClick="callbackPreProcessamento.SendCallback(); return false;">
                                        <asp:Literal ID="Literal12" runat="server" Text="Pré-Processamento" /><div>
                                        </div>
                                    </asp:LinkButton>                                    
                                    <asp:LinkButton ID="btnOptions" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnOptions" OnClientClick="popupOpcoes.Show(); return false;">
                                        <asp:Literal ID="Literal3" runat="server" Text="Opções" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnCheck" runat="server" Font-Overline="false" CssClass="btnTick"
                                        OnClientClick="callbackCheck.SendCallback();return false;">
                                        <asp:Literal ID="Literal8" runat="server" Text="Check Diário" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnPDF" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnPdf" OnClick="btnPDF_Click">
                                        <asp:Literal ID="Literal4" runat="server" Text="Gerar PDF" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnExcel" runat="server" Font-Overline="false" ValidationGroup="ATK"
                                        CssClass="btnExcel" OnClick="btnExcel_Click">
                                        <asp:Literal ID="Literal5" runat="server" Text="Gerar Excel" /><div>
                                        </div>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnRefresh" runat="server" Font-Overline="false" CssClass="btnRefresh"
                                        OnClientClick=" gridConsulta.PerformCallback('btnRefresh'); return false;">
                                        <asp:Literal ID="Literal6" runat="server" Text="Atualizar" /><div>
                                        </div>
                                    </asp:LinkButton>
                                </div>
                                <div class="divDataGrid">
                                    <dxwgv:ASPxGridView ID="gridConsulta" runat="server" EnableCallBacks="true" KeyFieldName="IdCliente"
                                        DataSourceID="EsDSCliente" OnCustomUnboundColumnData="gridConsulta_CustomUnboundColumnData"
                                        OnCustomCallback="gridConsulta_CustomCallback">
                                        <Columns>
                                            <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="5%" ButtonType="Image" ShowClearFilterButton="True">
                                                <HeaderStyle HorizontalAlign="Center"/>
                                                <HeaderTemplate>
                                                    <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridConsulta);}" OnInit="CheckBoxSelectAll"/>
                                                </HeaderTemplate>
                                            </dxwgv:GridViewCommandColumn>
                                            <dxwgv:GridViewDataColumn FieldName="IdCliente" VisibleIndex="1" Width="10%" CellStyle-HorizontalAlign="left" />
                                            <dxwgv:GridViewDataColumn FieldName="Apelido" VisibleIndex="2" Width="30%" />
                                            <dxwgv:GridViewDataComboBoxColumn Caption="Tipo" FieldName="IdTipo" VisibleIndex="3"
                                                Width="20%">
                                                <EditFormSettings Visible="False" />
                                                <PropertiesComboBox DataSourceID="EsDSTipoCliente" TextField="Descricao" ValueField="IdTipo" />
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataComboBoxColumn Caption="Ativo" FieldName="StatusAtivo" VisibleIndex="4"
                                                Width="10%">
                                                <EditFormSettings Visible="False" />
                                                <PropertiesComboBox>
                                                    <Items>
                                                        <dxe:ListEditItem Value="1" Text="Sim" />
                                                        <dxe:ListEditItem Value="2" Text="Não" />
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>                                            
                                            <dxwgv:GridViewDataComboBoxColumn FieldName="Status" Caption="Status"
                                                VisibleIndex="4" Width="15%" Settings-AutoFilterCondition="Contains">
                                                <EditFormSettings Visible="False" />
                                                <PropertiesComboBox>
                                                    <Items>
                                                        <dxe:ListEditItem Value="1" Text="Aberto" />
                                                        <dxe:ListEditItem Value="5" Text="Aberto C/Erro" />
                                                        <dxe:ListEditItem Value="2" Text="Fechado" />
                                                        <dxe:ListEditItem Value="4" Text="Fechado C/Erro" />                                                    
                                                        <dxe:ListEditItem Value="3" Text="Divulgado" />                                                                                                        
                                                    </Items>
                                                </PropertiesComboBox>
                                            </dxwgv:GridViewDataComboBoxColumn>
                                            <dxwgv:GridViewDataDateColumn FieldName="DataDia" VisibleIndex="6" Width="10%" />
                                            <dxwgv:GridViewDataTextColumn FieldName="ValoresColados" Caption="Valores Colados" UnboundType="String"
                                                VisibleIndex="7" Width="10%" Settings-AutoFilterCondition="Contains" />
                                        </Columns>
                                        <SettingsPopup EditForm-Width="500px" />
                                        <SettingsCommandButton>
                                            <ClearFilterButton Image-Url="../imagens/funnel--minus.png"/>
                                        </SettingsCommandButton>
                                    </dxwgv:ASPxGridView>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <dxwgv:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gridConsulta"
            Landscape="true" />
        <cc1:esDataSource ID="EsDSCliente" runat="server" OnesSelect="EsDSCliente_esSelect"
            LowLevelBind="true" />
        <cc1:esDataSource ID="EsDSTipoCliente" runat="server" OnesSelect="EsDSTipoCliente_esSelect" />
        <cc1:esDataSource ID="EsDSGrupoProcessamento" runat="server" OnesSelect="EsDSGrupoProcessamento_esSelect" />
        <cc1:esDataSource ID="EsDSPreProcessamento" runat="server" OnesSelect="EsDSPreProcessamento_esSelect" />
        <cc1:esDataSource ID="EsDSPerfilProcessamento" runat="server" OnesSelect="EsDSPerfilProcessamento_esSelect" />
        
    </form>
</body>
</html>
