﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using log4net;
using EntitySpaces.Interfaces;
using Financial.Web.Common;
using DevExpress.Web;
using Financial.Util;
using Financial.ContaCorrente.Enums;
using Financial.Bolsa.Enums;
using Financial.BMF.Enums;
using Financial.Fundo.Enums;
using Financial.Util.ConfiguracaoSistema;
using Financial.Util.Enums;
using System.Globalization;
using Financial.Common;

public partial class ConfiguracaoFront : BasePage
{
    private static readonly ILog log = LogManager.GetLogger(typeof(ConfiguracaoFront));

    new protected void Page_Load(object sender, EventArgs e)
    {
        pnlFront.HeaderText = String.Empty;

        if (!Page.IsPostBack)
        {
            TabelaParametrosFrontCollection tabelaParametrosFrontCollection = new TabelaParametrosFrontCollection();
            tabelaParametrosFrontCollection.LoadAll();

            if (tabelaParametrosFrontCollection.Count > 0)
            {
                if (!String.IsNullOrEmpty(tabelaParametrosFrontCollection[0].ObservacaoFundo))
                {
                    textFundo.Text = tabelaParametrosFrontCollection[0].ObservacaoFundo;
                }
                if (!String.IsNullOrEmpty(tabelaParametrosFrontCollection[0].ObservacaoCotista))
                {
                    textCotista.Text = tabelaParametrosFrontCollection[0].ObservacaoCotista;
                }
                if (!String.IsNullOrEmpty(tabelaParametrosFrontCollection[0].ObservacaoRendaFixa))
                {
                    textRendaFixa.Text = tabelaParametrosFrontCollection[0].ObservacaoRendaFixa;
                }
                if (!String.IsNullOrEmpty(tabelaParametrosFrontCollection[0].EmailFundo))
                {
                    textEmailFundo.Text = tabelaParametrosFrontCollection[0].EmailFundo;
                }
                if (!String.IsNullOrEmpty(tabelaParametrosFrontCollection[0].EmailCotista))
                {
                    textEmailCotista.Text = tabelaParametrosFrontCollection[0].EmailCotista;
                }
                if (!String.IsNullOrEmpty(tabelaParametrosFrontCollection[0].EmailRendaFixa))
                {
                    textEmailRendaFixa.Text = tabelaParametrosFrontCollection[0].EmailRendaFixa;
                }
                if (tabelaParametrosFrontCollection[0].HorarioFimFundo.HasValue)
                {
                    textHoraFundo.Text = tabelaParametrosFrontCollection[0].HorarioFimFundo.Value.Hour.ToString();
                    textMinutoFundo.Text = tabelaParametrosFrontCollection[0].HorarioFimFundo.Value.Minute.ToString();
                }
                if (tabelaParametrosFrontCollection[0].HorarioFimCotista.HasValue)
                {
                    textHoraCotista.Text = tabelaParametrosFrontCollection[0].HorarioFimCotista.Value.Hour.ToString();
                    textMinutoCotista.Text = tabelaParametrosFrontCollection[0].HorarioFimCotista.Value.Minute.ToString();
                }
                if (tabelaParametrosFrontCollection[0].HorarioFimRendaFixa.HasValue)
                {
                    textHoraRendaFixa.Text = tabelaParametrosFrontCollection[0].HorarioFimRendaFixa.Value.Hour.ToString();
                    textMinutoRendaFixa.Text = tabelaParametrosFrontCollection[0].HorarioFimRendaFixa.Value.Minute.ToString();
                }
            }
        }
    }

    /// <summary>
    /// Salva os Parametros de Configuração no Banco
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>   
    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        // Salva todas as Configurações
        try
        {
            TabelaParametrosFrontCollection tabelaParametrosFrontCollection = new TabelaParametrosFrontCollection();
            tabelaParametrosFrontCollection.LoadAll();
            tabelaParametrosFrontCollection.MarkAllAsDeleted();
            tabelaParametrosFrontCollection.Save();

            TabelaParametrosFront tabelaParametrosFront = new TabelaParametrosFront();
            tabelaParametrosFront.ObservacaoFundo = textFundo.Text;
            tabelaParametrosFront.ObservacaoCotista = textCotista.Text;
            tabelaParametrosFront.ObservacaoRendaFixa = textRendaFixa.Text;
            tabelaParametrosFront.EmailFundo = textEmailFundo.Text;
            tabelaParametrosFront.EmailCotista = textEmailCotista.Text;
            tabelaParametrosFront.EmailRendaFixa = textEmailRendaFixa.Text;

            if (textHoraFundo.Text != "" && textMinutoFundo.Text != "")
            {
                tabelaParametrosFront.HorarioFimFundo = new DateTime(2000, 1, 1,
                                    Convert.ToInt32(textHoraFundo.Text), Convert.ToInt32(textMinutoFundo.Text), 0);
            }
            else
            {
                tabelaParametrosFront.HorarioFimFundo = null;
            }

            if (textHoraCotista.Text != "" && textMinutoCotista.Text != "")
            {
                tabelaParametrosFront.HorarioFimCotista = new DateTime(2000, 1, 1,
                                    Convert.ToInt32(textHoraCotista.Text), Convert.ToInt32(textMinutoCotista.Text), 0);
            }
            else
            {
                tabelaParametrosFront.HorarioFimCotista = null;
            }

            if (textHoraRendaFixa.Text != "" && textMinutoRendaFixa.Text != "")
            {
                tabelaParametrosFront.HorarioFimRendaFixa = new DateTime(2000, 1, 1,
                                    Convert.ToInt32(textHoraRendaFixa.Text), Convert.ToInt32(textMinutoRendaFixa.Text), 0);
            }
            else
            {
                tabelaParametrosFront.HorarioFimRendaFixa = null;
            }

            tabelaParametrosFront.Save();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        string mensagem = "Alterações Salvas com Sucesso.";
        throw new Exception(mensagem);
    }




}