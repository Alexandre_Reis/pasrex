﻿using System;
using System.Collections.Generic;

using log4net;

using Financial.Web.Common;
using Financial.Util;
using DevExpress.Web;
using System.Drawing;
using EntitySpaces.Interfaces;
using Financial.Relatorio;
using Financial.Investidor;
using Financial.Relatorio.Enums;
using System.Text;
using Financial.WebConfigConfiguration;
using System.Xml;
using System.Collections;
using System.Web.UI.WebControls;
using Financial.Util.ConfiguracaoSistema;
using Financial.Util.Enums;

public partial class ConfiguracaoExtratoCliente : CadastroBasePage {

    new protected void Page_Load(object sender, EventArgs e)
    {
        this.AllowUpdate = true;

        string diretorio = DiretorioAplicacao.DiretorioBaseAplicacao;
        
        if (!System.IO.Directory.Exists(diretorio + "/App_Data/"))
        {
            System.IO.Directory.CreateDirectory(diretorio + "/App_Data/");
        }

        if (!System.IO.File.Exists(diretorio + "/App_Data/FaixaLiquidez.xml"))
        {
            XmlDocument xmlDoc = new XmlDocument();
            XmlNode xmlNode = xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", null);
            xmlDoc.AppendChild(xmlNode);
            xmlNode = xmlDoc.CreateElement("Lista");

            xmlDoc.AppendChild(xmlNode);
                        
            xmlDoc.Save(diretorio + "/App_Data/FaixaLiquidez.xml");
        }

        base.Page_Load(sender, e, this.gridCadastro);
        
        this.gridCadastro.SettingsPager.PageSize = 6;
        this.gridCadastro.Settings.VerticalScrollableHeight = 150;
        this.gridCadastro.Settings.ShowFooter = false;
        
        //
    }

    private static readonly ILog log = LogManager.GetLogger(typeof(ConfiguracaoExtratoCliente));

    /// <summary>
    /// Salva os Parametros de Configuração no Banco
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>   
    protected void btnSalvar_Click(object sender, EventArgs e) {

        // Monta uma string separada por "," com as cores do Extrato
        /* Formato: header1Style Foreground Color, header1Style Background Color, 
         *          header2Style Foreground Color, header2Style Background Color, 
         *          header3Style Foreground Color, header3Style Background Color, 
         *          oddRowStyle Foreground Color, oddRowStyle Background Color, 
         *          evenRowStyle Foreground Color, evenRowStyle Background Color, 
         *          line1Style Foreground Color, line1Style Background Color, 
         *          headerFieldStyle Foreground Color, headerFieldStyle Background Color, 
         *          positivoStyle Foreground Color, positivoStyle Background Color, 
         *          negativoStyle Foreground Color, negativoStyle Background Color, 
         */
        
        // Defaults
        Color preto = ColorTranslator.FromHtml("#000000");
        Color vermelho = ColorTranslator.FromHtml("#ff0000");
        Color branco = ColorTranslator.FromHtml("#ffffff");
        Color line = ColorTranslator.FromHtml("#002940");
        Color positive = ColorTranslator.FromHtml("#002940");
        //
        List<string> coresExtrato = new List<string>(new string[] {
                ColorTranslator.ToHtml(header1ForeStyle.Color), ColorTranslator.ToHtml(header1BackStyle.Color),
                ColorTranslator.ToHtml(header2ForeStyle.Color), ColorTranslator.ToHtml(header2BackStyle.Color),
                ColorTranslator.ToHtml(header3ForeStyle.Color), ColorTranslator.ToHtml(header3BackStyle.Color),
                ColorTranslator.ToHtml(oddForeStyle.Color), ColorTranslator.ToHtml(oddBackStyle.Color),
                ColorTranslator.ToHtml(evenForeStyle.Color), ColorTranslator.ToHtml(evenBackStyle.Color),
                ColorTranslator.ToHtml(line), ColorTranslator.ToHtml(branco),
                ColorTranslator.ToHtml(headerFieldForeStyle.Color), ColorTranslator.ToHtml(headerFieldBackStyle.Color),
                ColorTranslator.ToHtml(positive), ColorTranslator.ToHtml(branco),
                ColorTranslator.ToHtml(vermelho), ColorTranslator.ToHtml(branco)            
        });
        
        string valor = string.Join("," , coresExtrato.ToArray() );
        
        try {
            using (esTransactionScope scope = new esTransactionScope()) {
                // Salva a Configuração de Cores e a Observacao do Relatorio de ExtratoCliente
                ParametrosConfiguracaoSistema.ConfiguracaoRelatorios.RelatorioExtratoCliente.CoresExtrato = valor;
                //
                ParametrosConfiguracaoSistema.ConfiguracaoRelatorios.RelatorioExtratoCliente.ObservacaoExtrato = this.htmlEditorObservacao.Html.Trim();
                //
                ParametrosConfiguracaoSistema.ConfiguracaoRelatorios.RelatorioExtratoCliente.EstiloGraficoDistribuicao = this.dropEstiloGraficodistribuicao.SelectedItem.Value.ToString();
                //
                ParametrosConfiguracaoSistema.ConfiguracaoRelatorios.RelatorioExtratoCliente.MostraPatrimonio = this.dropPatrimonioTotal.SelectedItem.Value.ToString();
                //
                ParametrosConfiguracaoSistema.ConfiguracaoRelatorios.RelatorioExtratoCliente.ExibePizzaTopN = Convert.ToInt32(this.dropExibeTopN.SelectedItem.Value);

                this.SalvaConfigRelatorios();
                //
                scope.Complete();
            }            
        }
        catch (Exception ex) {
            throw new Exception(ex.Message);
        }
        string mensagem = "Alterações Salvas com Sucesso.";
        throw new Exception(mensagem);
    }
       

    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void callbackCores_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        e.Result = "Atualização de cores realizada. Caso queira ver as paletas de cores atualizadas, faça um refresh na página.";

        TabelaExtratoClienteCollection t = new TabelaExtratoClienteCollection();

        string valor = "";
        if (dropCores.SelectedIndex == -1)
            return;
        

        if (Convert.ToInt32(dropCores.SelectedItem.Value) == 1) //Azul claro
        {
            valor = "#FFFFFF,#062C53,#002940,#8FB5DC,#002940,#F5F7FF,#002940,#FDFAF3,#002940,#E9EBF2,#002940,#FFFFFF,#002940,#FFFFFF";
        }
        else if (Convert.ToInt32(dropCores.SelectedItem.Value) == 2) //Azul escuro
        {
            valor = "#FFFFFF,#BDAF33,#222371,#C0C0C0,#FFFFFF,#222371,#222371,#E1E1E1,#222371,#FFFFFF,#002940,#FFFFFF,#222371,#FFFFFF";
        }
        else if (Convert.ToInt32(dropCores.SelectedItem.Value) == 3) //Cinza
        {
            valor = "#FFFFFF,#333333,#333333,#C0C0C0,#333333,#C0C0C0,#333333,#FFFFFF,#333333,#FFFFFF,#002940,#FFFFFF,#000000,#FFFFFF";
        }
        else if (Convert.ToInt32(dropCores.SelectedItem.Value) == 4) //Verde
        {
            valor = "#FFFFFF,#167C62,#222371,#33997F,#002940,#E0EFE0,#222371,#FDFAF3,#222371,#F9F2E2,#002940,#FFFFFF,#222371,#FFFFFF";
        }
        else if (Convert.ToInt32(dropCores.SelectedItem.Value) == 5) //Vermelho
        {
            valor = "#FFFFFF,#C31D22,#333333,#F3595E,#333333,#DFB1A2,#333333,#FFFFFF,#333333,#FFFFFF,#002940,#FFFFFF,#000000,#FFFFFF";
        }
        else if (Convert.ToInt32(dropCores.SelectedItem.Value) == 6) //Padrão
        {
            valor = "#FFFFFF,#002940,#002940,#D7CFB9,#002940,#F9F2E2,#002940,#FDFAF3,#002940,#F9F2E2,#002940,#FFFFFF,#002940,#FFFFFF";
        }
        
        ParametrosConfiguracaoSistema.ConfiguracaoRelatorios.RelatorioExtratoCliente.CoresExtrato = valor;
    }

    /// <summary>
    /// De acordo com o Id do Componente carrega a Cor de Cada Configuracao do Relatorio 
    /// analisando o parametro presente na tabela de Configuracao
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ColorEdit_OnPreRender(object sender, EventArgs e) {
        ASPxColorEdit c = sender as ASPxColorEdit;

        switch (c.ID) {
            case "header1ForeStyle": this.header1ForeStyle.Color = ParametrosConfiguracaoSistema.ConfiguracaoRelatorios.RelatorioExtratoCliente.Header1_Style[0]; break;
            case "header1BackStyle": this.header1BackStyle.Color = ParametrosConfiguracaoSistema.ConfiguracaoRelatorios.RelatorioExtratoCliente.Header1_Style[1]; break;
            //
            case "header2ForeStyle": this.header2ForeStyle.Color = ParametrosConfiguracaoSistema.ConfiguracaoRelatorios.RelatorioExtratoCliente.Header2_Style[0]; break;
            case "header2BackStyle": this.header2BackStyle.Color = ParametrosConfiguracaoSistema.ConfiguracaoRelatorios.RelatorioExtratoCliente.Header2_Style[1]; break;
            //
            case "header3ForeStyle": this.header3ForeStyle.Color = ParametrosConfiguracaoSistema.ConfiguracaoRelatorios.RelatorioExtratoCliente.Header3_Style[0]; break;
            case "header3BackStyle": this.header3BackStyle.Color = ParametrosConfiguracaoSistema.ConfiguracaoRelatorios.RelatorioExtratoCliente.Header3_Style[1]; break;
            //
            case "headerFieldForeStyle": this.headerFieldForeStyle.Color = ParametrosConfiguracaoSistema.ConfiguracaoRelatorios.RelatorioExtratoCliente.HeaderField_Style[0]; break;
            case "headerFieldBackStyle": this.headerFieldBackStyle.Color = ParametrosConfiguracaoSistema.ConfiguracaoRelatorios.RelatorioExtratoCliente.HeaderField_Style[1]; break;
            //
            case "evenForeStyle": this.evenForeStyle.Color = ParametrosConfiguracaoSistema.ConfiguracaoRelatorios.RelatorioExtratoCliente.Even_Style[0]; break;
            case "evenBackStyle": this.evenBackStyle.Color = ParametrosConfiguracaoSistema.ConfiguracaoRelatorios.RelatorioExtratoCliente.Even_Style[1]; break;
            //
            case "oddForeStyle": this.oddForeStyle.Color = ParametrosConfiguracaoSistema.ConfiguracaoRelatorios.RelatorioExtratoCliente.Odd_Style[0]; break;
            case "oddBackStyle": this.oddBackStyle.Color = ParametrosConfiguracaoSistema.ConfiguracaoRelatorios.RelatorioExtratoCliente.Odd_Style[1]; break;
        }
    }

    /// <summary>
    /// DataBinding Inicial da Observação do Extrato Cliente
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void HtmlEditorObservacao_OnPreRender(object sender, EventArgs e) {
        this.htmlEditorObservacao.Html = ParametrosConfiguracaoSistema.ConfiguracaoRelatorios.RelatorioExtratoCliente.ObservacaoExtrato;
    }

    /// <summary>
    /// Deleta e Depois Salva as Configurações de Posicões do Relatório de Extrato Cliente
    /// </summary>
    private void SalvaConfigRelatorios() {                
        ListEditItemCollection lista = this.lbChoosen.Items;        
        //
        int posicaoY = 0;
        int posicaoX = 0;
        if (lista.Count !=0) { // Se tiver dados na lista
            //

            // Função para tratar Erros
            this.TrataErros();

            TabelaExtratoClienteCollection t = new TabelaExtratoClienteCollection();
            int contEspaco = 0;
            for (int i = 0; i < lista.Count; i++) {

                #region switch por Relatorio
                switch ( Convert.ToInt32(lista[i].Value) ) {
                    case (int)TipoReportExtratoCliente.Espaço_Branco:
                    case (int)TipoReportExtratoCliente.Espaço_Branco1:
                    case (int)TipoReportExtratoCliente.Espaço_Branco2:
                    case (int)TipoReportExtratoCliente.Espaço_Branco3:
                    case (int)TipoReportExtratoCliente.Espaço_Branco4:
                        #region Insere espaço em branco no posicionamentos dos gráficos
                        contEspaco += 1;
                        if (contEspaco > 4)
                        {
                            throw new Exception("Só são permitidos até 4 espaços em branco.");
                        }
                        TabelaExtratoCliente espacoBranco = t.AddNew();
                        this.InsereReport(espacoBranco, 9990 + contEspaco, posicaoX, posicaoY);
                        if (posicaoX == 0)
                            posicaoX = 975;
                        else
                        {
                            posicaoX = 0;
                            posicaoY += 100;
                        }
                        #endregion
                        break;
                    case (int)TipoReportExtratoCliente.Tabela_RetornoCarteira:
                    case (int)TipoReportExtratoCliente.Tabela_RiscoRetorno:
                    case (int)TipoReportExtratoCliente.Tabela_PosicaoAtivos:
                    case (int)TipoReportExtratoCliente.Tabela_MovimentoPeriodo:
                    case (int)TipoReportExtratoCliente.Tabela_Movimentacao:
                    case (int)TipoReportExtratoCliente.Tabela_RetornoAtivos:
                    case (int)TipoReportExtratoCliente.Tabela_RetornoAtivosExplodidos:
                    case (int)TipoReportExtratoCliente.Tabela_ResultadoEstrategias:
                    case (int)TipoReportExtratoCliente.Tabela_RetornoEstrategias:
                    case (int)TipoReportExtratoCliente.Tabela_RetornoEstrategiasBenchmark:
                    case (int)TipoReportExtratoCliente.Tabela_PosicaoAplicacoes:
                    case (int)TipoReportExtratoCliente.Tabela_RetornoEstrategiasLiquido:
                    case (int)TipoReportExtratoCliente.Tabela_ResultadoEstrategiasLiquido:
                    case (int)TipoReportExtratoCliente.Tabela_RetornoEstrategiasBenchmarkLiquido:  
                    case (int)TipoReportExtratoCliente.Tabela_ContaCorrente:
                    case (int)TipoReportExtratoCliente.Tabela_PosicaoAtivos2:                    
                    case (int)TipoReportExtratoCliente.Detalhamento_Fundo:
                        #region Gera Registro
                        if (posicaoX != 0)
                        {
                            posicaoX = 0;
                            posicaoY += 100;
                        }
                        TabelaExtratoCliente tabelaExtrato = t.AddNew();
                        this.InsereReport(tabelaExtrato, Convert.ToInt32(lista[i].Value), posicaoX, posicaoY);
                        //
                        posicaoY += 100;
                        #endregion
                        break;
                    case (int)TipoReportExtratoCliente.Somente_Grafico_AlocacaoEstrategia:
                    case (int)TipoReportExtratoCliente.Grafico_AlocacaoEstrategiaExplodida:
                    case (int)TipoReportExtratoCliente.Somente_Grafico_AlocacaoAtivo:
                    case (int)TipoReportExtratoCliente.Grafico_AlocacaoAtivo2:
                    case (int)TipoReportExtratoCliente.Grafico_FrequenciaRetornos:
                    case (int)TipoReportExtratoCliente.Grafico_RetornoComparativo:
                    case (int)TipoReportExtratoCliente.Grafico_Liquidez2:
                    case (int)TipoReportExtratoCliente.Tabela_RiscoRetorno2:
                    case (int)TipoReportExtratoCliente.Tabela_RetornoIndices:
                    case (int)TipoReportExtratoCliente.Grafico_RetornoAcumulado2:
                    case (int)TipoReportExtratoCliente.Grafico_RetornoAcumulado3:
                    case (int)TipoReportExtratoCliente.Tabela_PosicaoAtivosExplodida:
                        #region Gera Registro Grafico
                        TabelaExtratoCliente graficoExtrato = t.AddNew();
                        this.InsereReport(graficoExtrato, Convert.ToInt32(lista[i].Value), posicaoX, posicaoY);
                        //
                        if (posicaoX == 0)
                            posicaoX = 975;
                        else
                        {
                            posicaoX = 0;
                            posicaoY += 100;
                        }
                        #endregion
                        break;
                    case (int)TipoReportExtratoCliente.Observacao:
                        #region Gera Registro para SubReport Observação
                        TabelaExtratoCliente tabelaExtrato10 = t.AddNew();
                        // Insere Na posicao -200,-200; Poderia ser em qualquer posicao pois tem Posição Fixa
                        this.InsereReport(tabelaExtrato10, Convert.ToInt32(TipoReportExtratoCliente.Observacao), -200, -200); 
                        #endregion
                        break;

                    case (int)TipoReportExtratoCliente.Grafico_RetornoAcumulado_EvolucaoPL:
                        #region Se for Combinação de Relatorio Gera Dois Registros
                        if (posicaoX != 0)
                        {
                            posicaoX = 0;
                            posicaoY += 100;
                        }
                        TabelaExtratoCliente tabelaExtrato1 = t.AddNew();
                        this.InsereReport(tabelaExtrato1, (int)TipoReportExtratoCliente.Grafico_RetornoAcumulado, 0, posicaoY);
                        //
                        TabelaExtratoCliente tabelaExtrato2 = t.AddNew();
                        this.InsereReport(tabelaExtrato2, (int)TipoReportExtratoCliente.Grafico_EvolucaoPL, 975, posicaoY);
                        //
                        posicaoY += 100;

                        #endregion
                        break;

                    case (int)TipoReportExtratoCliente.Grafico_Retorno12Meses_Liquidez:
                        #region Se for Combinação de Relatorio Gera Dois Registros
                        if (posicaoX != 0)
                        {
                            posicaoX = 0;
                            posicaoY += 100;
                        }
                        TabelaExtratoCliente tabelaExtrato3 = t.AddNew();
                        this.InsereReport(tabelaExtrato3, (int)TipoReportExtratoCliente.Grafico_Retorno12meses, 0, posicaoY);
                        //
                        TabelaExtratoCliente tabelaExtrato4 = t.AddNew();
                        this.InsereReport(tabelaExtrato4, (int)TipoReportExtratoCliente.Grafico_Liquidez, 975, posicaoY);
                        //
                        posicaoY += 100;

                        #endregion
                        break;

                    case (int)TipoReportExtratoCliente.Grafico_AlocacaoGestor_AlocacaoEstrategia:
                        #region Se for Combinação de Relatorio Gera Dois Registros
                        if (posicaoX != 0)
                        {
                            posicaoX = 0;
                            posicaoY += 100;
                        }
                        TabelaExtratoCliente tabelaExtrato5 = t.AddNew();
                        this.InsereReport(tabelaExtrato5, (int)TipoReportExtratoCliente.Grafico_AlocacaoGestor, 0, posicaoY);
                        //
                        TabelaExtratoCliente tabelaExtrato6 = t.AddNew();
                        this.InsereReport(tabelaExtrato6, (int)TipoReportExtratoCliente.Grafico_AlocacaoEstrategia, 975, posicaoY);
                        //
                        posicaoY += 100;

                        #endregion
                        break;

                    case (int)TipoReportExtratoCliente.Grafico_AlocacaoGestor_AlocacaoAtivo:
                        #region Se for Combinação de Relatorio Gera Dois Registros
                        if (posicaoX != 0)
                        {
                            posicaoX = 0;
                            posicaoY += 100;
                        }
                        TabelaExtratoCliente tabelaExtrato7 = t.AddNew();
                        this.InsereReport(tabelaExtrato7, (int)TipoReportExtratoCliente.Grafico_AlocacaoGestor, 0, posicaoY);
                        //
                        TabelaExtratoCliente tabelaExtrato8 = t.AddNew();
                        this.InsereReport(tabelaExtrato8, (int)TipoReportExtratoCliente.Grafico_AlocacaoAtivo, 975, posicaoY);
                        //
                        posicaoY += 100;

                        #endregion
                        break;

                    case (int)TipoReportExtratoCliente.Grafico_AlocacaoEstrategia_AlocacaoAtivo:
                        #region Se for Combinação de Relatorio Gera Dois Registros
                        if (posicaoX != 0)
                        {
                            posicaoX = 0;
                            posicaoY += 100;
                        }
                        TabelaExtratoCliente tabelaExtrato9 = t.AddNew();
                        this.InsereReport(tabelaExtrato9, (int)TipoReportExtratoCliente.Grafico_AlocacaoEstrategia, 0, posicaoY);
                        //
                        TabelaExtratoCliente tabelaExtrato11 = t.AddNew();
                        this.InsereReport(tabelaExtrato11, (int)TipoReportExtratoCliente.Grafico_AlocacaoAtivo, 975, posicaoY);
                        //
                        posicaoY += 100;

                        #endregion
                        break;
                }
                #endregion
            }

            /*
             * Substituir por outra constante para poder testar; const string SOCOPA = "Atatika";
            */
            const string SOCOPA = "Socopa";
            const string PRIVATE = "Socopa Private";

            // Se for Cliente Socopa ou Private insere o LogotipoRodape
            if (WebConfig.AppSettings.Cliente.Trim() == SOCOPA ||
                WebConfig.AppSettings.Cliente.Trim() == PRIVATE) {

                TabelaExtratoCliente tabelaExtrato = t.AddNew();
                // Insere Na posicao -100,-100; Poderia ser em qualquer posicao
                this.InsereReport(tabelaExtrato,  Convert.ToInt32(TipoReportExtratoCliente.Logotipo_Rodape), -100, -100);            
            }
                    
            // Deleta Tudo de Extrato Cliente
            #region Deleta Extrato
            TabelaExtratoClienteCollection tDelete = new TabelaExtratoClienteCollection();
            tDelete.LoadAll();
            tDelete.MarkAllAsDeleted();
            tDelete.Save();
            #endregion

            // Salva novos dados
            t.Sort = TabelaExtratoClienteMetadata.ColumnNames.PosicaoY + ", " + TabelaExtratoClienteMetadata.ColumnNames.PosicaoX + " ASC";
            t.Save();
        }
    }

    /// <summary>
    /// Trata Erros Salvar as Configurações dos Relatórios
    /// Não pode escolher G/ES / G/AT ao mesmo tempo
    /// Não pode escolher G/ES / ES/AT ao mesmo tempo
    /// Não pode escolher G/AT ES/AT ao mesmo tempo
    /// </summary>
    private void TrataErros() {
        //
        ListEditItemCollection lista = this.lbChoosen.Items; // itens escolhidos
        //

        #region TrataErros
        List<int> listaErros = new List<int>();
        for (int i = 0; i < lista.Count; i++) {
            listaErros.Add(Convert.ToInt32(lista[i].Value));
        }

        // Trata Duplicação de Chave
        if (listaErros.Contains((int)TipoReportExtratoCliente.Grafico_AlocacaoGestor_AlocacaoEstrategia) &&
            listaErros.Contains((int)TipoReportExtratoCliente.Grafico_AlocacaoGestor_AlocacaoAtivo)) {

            throw new Exception("Gráfico Alocação Gestor/Estratégia e Gráfico Alocação Gestor/Ativo não podem ser escolhidos ao mesmo tempo");
        }

        // Trata Duplicação de Chave
        if (listaErros.Contains((int)TipoReportExtratoCliente.Grafico_AlocacaoGestor_AlocacaoEstrategia) &&
            listaErros.Contains((int)TipoReportExtratoCliente.Grafico_AlocacaoEstrategia_AlocacaoAtivo)) {

            throw new Exception("Gráfico Alocação Gestor/Estratégia e Gráfico Alocação Estratégia/Ativo não podem ser escolhidos ao mesmo tempo");
        }

        // Trata Duplicação de Chave
        if (listaErros.Contains((int)TipoReportExtratoCliente.Grafico_AlocacaoGestor_AlocacaoAtivo) &&
            listaErros.Contains((int)TipoReportExtratoCliente.Grafico_AlocacaoEstrategia_AlocacaoAtivo)) {

            throw new Exception("Gráfico Alocação Gestor/Ativo e Gráfico Alocação Estratégia/Ativo não podem ser escolhidos ao mesmo tempo");
        }

        // Trata Duplicação de Chave
        if (listaErros.Contains((int)TipoReportExtratoCliente.Tabela_RetornoAtivos) &&
            listaErros.Contains((int)TipoReportExtratoCliente.Tabela_RetornoAtivosExplodidos))
        {

            throw new Exception("Tabela de Retorno de Ativos e Retorno de Ativos Explodidos não podem ser escolhidos ao mesmo tempo");
        }
        #endregion
    }

    /// <summary>
    /// Insere um Report no Objeto TabelaExtratoCliente
    /// modifies: t.idCliente, t.IdSubReport, t.PosicaoX, t.PosicaoY
    /// </summary>
    /// <param name="t"></param>
    /// <param name="idSubReport"></param>
    /// <param name="posicaoX"></param>
    /// <param name="posicaoY"></param>
    private void InsereReport(TabelaExtratoCliente t, int idSubReport, int posicaoX, int posicaoY) {

        // Pega o primeiro Cliente
        ClienteCollection clienteCollection = new ClienteCollection();
        clienteCollection.Query.Select(clienteCollection.Query.IdCliente);
        clienteCollection.Query.Load();
        //
        t.IdCliente = clienteCollection[0].IdCliente.Value;
        t.IdSubReport = idSubReport;
        t.PosicaoX = posicaoX;
        t.PosicaoY = posicaoY;
    }

    /// <summary>
    /// Lista possui os ids AlocacaoGestor = 8 e AlocacaoEstrategia = 12
    /// </summary>
    private bool IsGraficoAlocacaoGestorAlocacaoEstrategia(List<int> subRelatorios) {
        return subRelatorios.Contains((int)TipoReportExtratoCliente.Grafico_AlocacaoGestor) &&
               subRelatorios.Contains((int)TipoReportExtratoCliente.Grafico_AlocacaoEstrategia);
    }

    /// <summary>
    /// Lista possui os ids AlocacaoGestor = 8 e AlocacaoAtivo = 21
    /// </summary>
    private bool IsGraficoAlocacaoGestorAlocacaoAtivo(List<int> subRelatorios) {
        return subRelatorios.Contains((int)TipoReportExtratoCliente.Grafico_AlocacaoGestor) &&
               subRelatorios.Contains((int)TipoReportExtratoCliente.Grafico_AlocacaoAtivo);
    }

    /// <summary>
    /// Lista possui os ids AlocacaoEstrategia = 12 e AlocacaoAtivo = 21
    /// </summary>
    private bool IsGraficoAlocacaoEstrategiaAlocacaoAtivo(List<int> subRelatorios) {
        return subRelatorios.Contains((int)TipoReportExtratoCliente.Grafico_AlocacaoEstrategia) &&
               subRelatorios.Contains((int)TipoReportExtratoCliente.Grafico_AlocacaoAtivo);
    }

    /// <summary>
    /// Binding Inicial da lista de Relatorios Disponiveis
    ///
    /// <Items>    
    /// <%--De Acordo com o Enum TipoReportExtratoCliente--%>
    /// <dxe:ListEditItem Text="Tabela Retorno Carteira" Value="1" />
    /// <dxe:ListEditItem Text="Tabela Risco Retorno" Value="5" />
    /// <dxe:ListEditItem Text="Tabela Posição Ativos" Value="10" />
    /// <dxe:ListEditItem Text="Grafico_AlocacaoEstrategia" Value="12" />
    /// <dxe:ListEditItem Text="Tabela Movimento Período" Value="14" />
    /// <dxe:ListEditItem Text="Tabela Retorno Ativos" Value="15" />
    /// <dxe:ListEditItem Text="Tabela Retorno Estratégias" Value="16" />
    /// <dxe:ListEditItem Text="Tabela Resultado Estratégias" Value="17" />
    /// <dxe:ListEditItem Text="Tabela Posição Aplicações" Value="19" />
    /// <dxe:ListEditItem Text="Grafico_Alocacao Ativo" Value="21" />
    /// <dxe:ListEditItem Text="Tabela_RetornoEstrategiasBenchmark" Value="22" />
    /// 
    /// <dxe:ListEditItem Text="Tabela Retorno Estrategias Liquido" Value="23" />
    /// <dxe:ListEditItem Text="Tabela Resultado Estrategias Liquido" Value="24" />
    /// <dxe:ListEditItem Text="Tabela Retorno Estrategias Benchmark Liquido" Value="25" />
    /// 
    /// <dxe:ListEditItem Text="Observação" Value="99" />
    /// <%--Itens Duplos --%>   
    /// <dxe:ListEditItem Text="Gráfico Retorno Acumulado/Evolução PL" Value="200" />
    /// <dxe:ListEditItem Text="Gráfico Retorno 12 Meses/Liquidez " Value="201" />    
    /// <dxe:ListEditItem Text="Gráfico Alocação Gestor/Estratégia" Value="202" />
    /// <dxe:ListEditItem Text="Gráfico Alocação Gestor/AlocacaoAtivo" Value="203" />
    /// <dxe:ListEditItem Text="Gráfico Alocação Estratégia/AlocacaoAtivo" Value="204" />
    /// </Items>
    ///  
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbAvailable_OnPreRender(object sender, EventArgs e) {
        
        List<int> todosDisponiveis = new List<int>( new int[] {
            (int)TipoReportExtratoCliente.Tabela_RetornoCarteira, 
            (int)TipoReportExtratoCliente.Tabela_RiscoRetorno,
            (int)TipoReportExtratoCliente.Tabela_PosicaoAtivos, 
            (int)TipoReportExtratoCliente.Somente_Grafico_AlocacaoEstrategia, 
            (int)TipoReportExtratoCliente.Grafico_AlocacaoEstrategiaExplodida,
            (int)TipoReportExtratoCliente.Somente_Grafico_AlocacaoAtivo,
            (int)TipoReportExtratoCliente.Grafico_AlocacaoAtivo2,
            (int)TipoReportExtratoCliente.Tabela_MovimentoPeriodo, 
            (int)TipoReportExtratoCliente.Tabela_Movimentacao, 
            (int)TipoReportExtratoCliente.Tabela_RetornoAtivos, 
            (int)TipoReportExtratoCliente.Tabela_RetornoAtivosExplodidos,
            (int)TipoReportExtratoCliente.Tabela_RetornoEstrategias, 
            (int)TipoReportExtratoCliente.Tabela_RetornoEstrategiasBenchmark,
            (int)TipoReportExtratoCliente.Tabela_ResultadoEstrategias, 
            (int)TipoReportExtratoCliente.Tabela_PosicaoAplicacoes,            
            (int)TipoReportExtratoCliente.Tabela_RetornoEstrategiasLiquido,
            (int)TipoReportExtratoCliente.Tabela_ResultadoEstrategiasLiquido,
            (int)TipoReportExtratoCliente.Tabela_RetornoEstrategiasBenchmarkLiquido,
            (int)TipoReportExtratoCliente.Grafico_FrequenciaRetornos,
            (int)TipoReportExtratoCliente.Grafico_RetornoComparativo,
            (int)TipoReportExtratoCliente.Tabela_RiscoRetorno2,
            (int)TipoReportExtratoCliente.Tabela_RetornoIndices,
            (int)TipoReportExtratoCliente.Grafico_Liquidez2,
            (int)TipoReportExtratoCliente.Grafico_RetornoAcumulado2,
            (int)TipoReportExtratoCliente.Grafico_RetornoAcumulado3,
            (int)TipoReportExtratoCliente.Tabela_ContaCorrente,
            (int)TipoReportExtratoCliente.Tabela_PosicaoAtivos2,
            (int)TipoReportExtratoCliente.Detalhamento_Fundo,
            (int)TipoReportExtratoCliente.Tabela_PosicaoAtivosExplodida,
            (int)TipoReportExtratoCliente.Observacao,
            //  itens Duplos
            (int)TipoReportExtratoCliente.Grafico_RetornoAcumulado_EvolucaoPL,
            (int)TipoReportExtratoCliente.Grafico_Retorno12Meses_Liquidez,
            (int)TipoReportExtratoCliente.Grafico_AlocacaoGestor_AlocacaoEstrategia,
            (int)TipoReportExtratoCliente.Grafico_AlocacaoGestor_AlocacaoAtivo,
            (int)TipoReportExtratoCliente.Grafico_AlocacaoEstrategia_AlocacaoAtivo,
            (int)TipoReportExtratoCliente.Espaço_Branco
        } );

        #region Remove da Lista de Todos se não for para exibir
        TabelaExtratoClienteCollection t = new TabelaExtratoClienteCollection();
        t.Query.OrderBy(t.Query.PosicaoY.Ascending, t.Query.PosicaoX.Ascending);
        t.Query.Load();
        //               

        List<int> idsSubRelatorio = new List<int>(); // Lista Original de Ids
        //
        for (int j = 0; j < t.Count; j++) {
            idsSubRelatorio.Add(t[j].IdSubReport.Value);
        }
        //

        // Remove da lista de todos se nao for para exibir        
        for (int i = 0; i < idsSubRelatorio.Count; i++) {
            if (idsSubRelatorio[i] == (int)TipoReportExtratoCliente.Grafico_RetornoAcumulado ||
                idsSubRelatorio[i] == (int)TipoReportExtratoCliente.Grafico_EvolucaoPL) {
                // Remove id Grafico_RetornoAcumulado_EvolucaoPL
                if (todosDisponiveis.Contains((int)TipoReportExtratoCliente.Grafico_RetornoAcumulado_EvolucaoPL)) {
                    todosDisponiveis.Remove((int)TipoReportExtratoCliente.Grafico_RetornoAcumulado_EvolucaoPL);
                }
            }
            else if (idsSubRelatorio[i] == (int)TipoReportExtratoCliente.Grafico_Retorno12meses ||
                     idsSubRelatorio[i] == (int)TipoReportExtratoCliente.Grafico_Liquidez) {
                // Remove id Grafico_Retorno12Meses_Liquidez
                if (todosDisponiveis.Contains((int)TipoReportExtratoCliente.Grafico_Retorno12Meses_Liquidez)) {
                    todosDisponiveis.Remove((int)TipoReportExtratoCliente.Grafico_Retorno12Meses_Liquidez);
                }
            }
            //else if (idsSubRelatorio[i] == (int)TipoReportExtratoCliente.Grafico_AlocacaoGestor ||
            //         idsSubRelatorio[i] == (int)TipoReportExtratoCliente.Grafico_AlocacaoEstrategia ||
            //         idsSubRelatorio[i] == (int)TipoReportExtratoCliente.Grafico_AlocacaoAtivo) {
                
            //    // Se tiver Gestor combinado Gestor Não Existe
            //    if (idsSubRelatorio[i] == (int)TipoReportExtratoCliente.Grafico_AlocacaoGestor) {

            //        #region Remove combinados com Gestor
            //        // Remove id Grafico_AlocacaoGestor_AlocacaoEstrategia
            //        if (todosDisponiveis.Contains((int)TipoReportExtratoCliente.Grafico_AlocacaoGestor_AlocacaoEstrategia)) {
            //            todosDisponiveis.Remove((int)TipoReportExtratoCliente.Grafico_AlocacaoGestor_AlocacaoEstrategia);
            //        }

            //        // Remove id Grafico_AlocacaoGestor_AlocacaoAtivo
            //        if (todosDisponiveis.Contains((int)TipoReportExtratoCliente.Grafico_AlocacaoGestor_AlocacaoAtivo)) {
            //            todosDisponiveis.Remove((int)TipoReportExtratoCliente.Grafico_AlocacaoGestor_AlocacaoAtivo);
            //        } 
            //        #endregion
            //    }

            //    // Se tiver Estrategia combinado Estrategia Não Existe
            //    if (idsSubRelatorio[i] == (int)TipoReportExtratoCliente.Grafico_AlocacaoEstrategia) {

            //        #region Remove Combinados com Estrategia
            //        // Remove id Grafico_AlocacaoGestor_AlocacaoEstrategia
            //        if (todosDisponiveis.Contains((int)TipoReportExtratoCliente.Grafico_AlocacaoGestor_AlocacaoEstrategia)) {
            //            todosDisponiveis.Remove((int)TipoReportExtratoCliente.Grafico_AlocacaoGestor_AlocacaoEstrategia);
            //        } 
            //        #endregion
            //    }

            //    // Se tiver Alocação Ativo combinado Alocação Ativo Não Existe
            //    if (idsSubRelatorio[i] == (int)TipoReportExtratoCliente.Grafico_AlocacaoAtivo) {

            //        #region Remove combinados com Alocacao Ativo
            //        // Remove id Grafico_AlocacaoGestor_AlocacaoAtivo
            //        if (todosDisponiveis.Contains((int)TipoReportExtratoCliente.Grafico_AlocacaoGestor_AlocacaoAtivo)) {
            //            todosDisponiveis.Remove((int)TipoReportExtratoCliente.Grafico_AlocacaoGestor_AlocacaoAtivo);
            //        } 
            //        #endregion
            //    }

            //}

            if (todosDisponiveis.Contains(idsSubRelatorio[i]) && idsSubRelatorio[i] != (int)TipoReportExtratoCliente.Espaço_Branco)
            {                
                todosDisponiveis.Remove(idsSubRelatorio[i]); // Trata Relatorios Individuais
            }
        }

        // Trata combinado Alocação Gestor com Alocação Estrategia
        if(this.IsGraficoAlocacaoGestorAlocacaoEstrategia(idsSubRelatorio)){
            if (todosDisponiveis.Contains((int)TipoReportExtratoCliente.Grafico_AlocacaoGestor_AlocacaoEstrategia)) {
                todosDisponiveis.Remove((int)TipoReportExtratoCliente.Grafico_AlocacaoGestor_AlocacaoEstrategia);
            } 
        }

        // Trata combinado Alocação Gestor com Alocação Ativo
        if (this.IsGraficoAlocacaoGestorAlocacaoAtivo(idsSubRelatorio)) {
            if (todosDisponiveis.Contains((int)TipoReportExtratoCliente.Grafico_AlocacaoGestor_AlocacaoAtivo)) {
                todosDisponiveis.Remove((int)TipoReportExtratoCliente.Grafico_AlocacaoGestor_AlocacaoAtivo);
            } 
        }

        // Trata combinado Alocação Estrategia com Alocação Ativo
        if (this.IsGraficoAlocacaoEstrategiaAlocacaoAtivo(idsSubRelatorio)) {
            if (todosDisponiveis.Contains((int)TipoReportExtratoCliente.Grafico_AlocacaoEstrategia_AlocacaoAtivo)) {
                todosDisponiveis.Remove((int)TipoReportExtratoCliente.Grafico_AlocacaoEstrategia_AlocacaoAtivo);
            }
        }

        #endregion

        //
        //
        ListEditItemCollection l = new ListEditItemCollection();
        //

        for (int i = 0; i < todosDisponiveis.Count; i++) {
            string textoRelatorio = StringEnum.GetStringValue((TipoReportExtratoCliente)todosDisponiveis[i]);
            //
            ListEditItem item = new ListEditItem(textoRelatorio, todosDisponiveis[i]);
            l.Add(item);
        }
        //
        this.lbAvailable.Items.AddRange(l);
    }

    protected void dropEstiloGraficodistribuicao_Init(object sender, EventArgs e)
    {
        dropEstiloGraficodistribuicao.SelectedItem = dropEstiloGraficodistribuicao.Items.FindByValue(ParametrosConfiguracaoSistema.ConfiguracaoRelatorios.RelatorioExtratoCliente.EstiloGraficoDistribuicao);
    }

    protected void dropExibeTopN_Init(object sender, EventArgs e)
    {
        dropExibeTopN.SelectedItem = dropExibeTopN.Items.FindByValue(ParametrosConfiguracaoSistema.ConfiguracaoRelatorios.RelatorioExtratoCliente.ExibePizzaTopN.ToString());
    }


    /// <summary>
    /// Preenche o Combo de Patrimonio
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropPatrimonioTotal_OnPreRender(object sender, EventArgs e) {
        string mostraPatrimonio = ParametrosConfiguracaoSistema.ConfiguracaoRelatorios.RelatorioExtratoCliente.MostraPatrimonio;
        //
        this.dropPatrimonioTotal.SelectedItem = dropPatrimonioTotal.Items.FindByValue(mostraPatrimonio);
    }

    /// <summary>
    /// Binding Inicial da lista de Relatorios Escolhidos
    ///
    /// <Items>    
    /// <%--De Acordo com o Enum TipoReportExtratoCliente--%>
    /// <dxe:ListEditItem Text="Tabela Retorno Carteira" Value="1" />
    /// <dxe:ListEditItem Text="Tabela Risco Retorno" Value="5" />    
    /// <dxe:ListEditItem Text="Tabela Posição Ativos" Value="10" />
    /// <dxe:ListEditItem Text="Grafico_AlocacaoEstrategia" Value="12" />
    /// <dxe:ListEditItem Text="Tabela Movimento Período" Value="14" />
    /// <dxe:ListEditItem Text="Tabela Retorno Ativos" Value="15" />
    /// <dxe:ListEditItem Text="Tabela Retorno Estratégias" Value="16" />
    /// <dxe:ListEditItem Text="Tabela Resultado Estratégias" Value="17" />
    /// <dxe:ListEditItem Text="Tabela Posição Aplicações" Value="19" />
    /// <dxe:ListEditItem Text="Grafico_Alocacao Ativo" Value="21" />
    /// <dxe:ListEditItem Text="Tabela_RetornoEstrategiasBenchmark" Value="22" />
    /// <dxe:ListEditItem Text="Tabela Retorno Estrategias Liquido" Value="23" />
    /// <dxe:ListEditItem Text="Tabela Resultado Estrategias Liquido" Value="24" />
    /// <dxe:ListEditItem Text="Tabela Retorno Estrategias Benchmark Liquido" Value="25" />
    /// <dxe:ListEditItem Text="Observação" Value="99" />
    /// itens Duplos
    /// <dxe:ListEditItem Text="Gráfico Retorno Acumulado/Evolução PL" Value="200" />
    /// <dxe:ListEditItem Text="Gráfico Retorno 12 Meses/Liquidez " Value="201" />
    /// <dxe:ListEditItem Text="Gráfico Alocação Gestor/Estratégia" Value="202" />
    /// <dxe:ListEditItem Text="Gráfico Alocação Gestor/AlocacaoAtivo" Value="203" />
    /// <dxe:ListEditItem Text="Gráfico Alocação Estratégia/AlocacaoAtivo" Value="204" />
    /// </Items>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbChoosen_OnPreRender(object sender, EventArgs e) {
        TabelaExtratoClienteCollection t = new TabelaExtratoClienteCollection();
        t.Query.OrderBy(t.Query.PosicaoY.Ascending, t.Query.PosicaoX.Ascending);
        t.Query.Load();
        //               

        //#region Refaz Collection Substuindo Relatorios Duplos por um unico Registro
        //List<int> tiposDuplos = new List<int>( new int[] {
        //    TipoReportExtratoCliente.Grafico_RetornoAcumulado, TipoReportExtratoCliente.Grafico_EvolucaoPL,
        //    TipoReportExtratoCliente.Grafico_Retorno12meses, TipoReportExtratoCliente.Grafico_Liquidez,
        //    TipoReportExtratoCliente.Grafico_AlocacaoGestor, TipoReportExtratoCliente.Grafico_AlocacaoEstrategia
        //} );
        //#endregion

        #region Refaz Collection Substuindo Relatorios Duplos por um unico Registro
        List<int> idsSubRelatorio = new List<int>(); // Lista Original de Ids
        //
        for (int j = 0; j < t.Count; j++) {
            // Não insere LogotipoRodape
            if (t[j].IdSubReport.Value == (int)TipoReportExtratoCliente.Logotipo_Rodape) {
                continue;
            }
            else {
                idsSubRelatorio.Add(t[j].IdSubReport.Value);
            }
        }
        //
        List<int> idsSubRelatorioModificados = new List<int>(); // Lista Modificada de Ids        
        //
        for (int k = 0; k < idsSubRelatorio.Count; k++) {
            if (idsSubRelatorio[k] == (int)TipoReportExtratoCliente.Grafico_RetornoAcumulado || 
                idsSubRelatorio[k] == (int)TipoReportExtratoCliente.Grafico_EvolucaoPL) { 
                // Insere registro modificado
                if(!idsSubRelatorioModificados.Contains((int)TipoReportExtratoCliente.Grafico_RetornoAcumulado_EvolucaoPL)){
                    idsSubRelatorioModificados.Add((int)TipoReportExtratoCliente.Grafico_RetornoAcumulado_EvolucaoPL);
                }
            }
            else if (idsSubRelatorio[k] == (int)TipoReportExtratoCliente.Grafico_Retorno12meses ||
                     idsSubRelatorio[k] == (int)TipoReportExtratoCliente.Grafico_Liquidez) {
                // Insere registro modificado
                if(!idsSubRelatorioModificados.Contains((int)TipoReportExtratoCliente.Grafico_Retorno12Meses_Liquidez)){
                    idsSubRelatorioModificados.Add((int)TipoReportExtratoCliente.Grafico_Retorno12Meses_Liquidez);
                }
            }
            else { // Não é relatório Duplo
                
                // Se aparecer Alocação Gestor, Alocação Estrategia ou Alocação Ativo sozinho sem ser combinação não inclui nada
                if (idsSubRelatorio[k] != (int)TipoReportExtratoCliente.Grafico_AlocacaoGestor &&
                    idsSubRelatorio[k] != (int)TipoReportExtratoCliente.Grafico_AlocacaoEstrategia &&
                    idsSubRelatorio[k] != (int)TipoReportExtratoCliente.Grafico_AlocacaoAtivo) {

                    // Insere registro Normal
                    idsSubRelatorioModificados.Add(idsSubRelatorio[k]);
                }

                else {

                    // Trata combinado Alocação Gestor com Alocação Estrategia
                    if (this.IsGraficoAlocacaoGestorAlocacaoEstrategia(idsSubRelatorio)) {
                        if (!idsSubRelatorioModificados.Contains((int)TipoReportExtratoCliente.Grafico_AlocacaoGestor_AlocacaoEstrategia)) {
                            idsSubRelatorioModificados.Add((int)TipoReportExtratoCliente.Grafico_AlocacaoGestor_AlocacaoEstrategia);
                        }
                    }

                    // Trata combinado Alocação Gestor com Alocação Ativo
                    if (this.IsGraficoAlocacaoGestorAlocacaoAtivo(idsSubRelatorio)) {
                        if (!idsSubRelatorioModificados.Contains((int)TipoReportExtratoCliente.Grafico_AlocacaoGestor_AlocacaoAtivo)) {
                            idsSubRelatorioModificados.Add((int)TipoReportExtratoCliente.Grafico_AlocacaoGestor_AlocacaoAtivo);
                        }
                    }

                    // Trata combinado Alocação Estretégia com Alocação Ativo
                    if (this.IsGraficoAlocacaoEstrategiaAlocacaoAtivo(idsSubRelatorio)) {
                        if (!idsSubRelatorioModificados.Contains((int)TipoReportExtratoCliente.Grafico_AlocacaoEstrategia_AlocacaoAtivo)) {
                            idsSubRelatorioModificados.Add((int)TipoReportExtratoCliente.Grafico_AlocacaoEstrategia_AlocacaoAtivo);
                        }
                    }
                }
            }
        }
             
        #endregion
        //
        ListEditItemCollection l = new ListEditItemCollection();
        //
        for (int i = 0; i < idsSubRelatorioModificados.Count; i++) {
            string textoRelatorio = StringEnum.GetStringValue((TipoReportExtratoCliente)idsSubRelatorioModificados[i]);
            //
            ListEditItem item = new ListEditItem(textoRelatorio, idsSubRelatorioModificados[i]);
            l.Add(item);           
        }        
        //
        this.lbChoosen.Items.AddRange(l);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        e.Editor.ReadOnly = false;
    }
        
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_CustomCallback(object sender, DevExpress.Web.ASPxGridViewCustomCallbackEventArgs e)
    {
        if (e.Parameters == "btnDelete")
        {
            List<object> keyValuesId = gridCadastro.GetSelectedFieldValues("IdFaixa");
            List<object> keyValuesData = gridCadastro.GetSelectedFieldValues("MaxDias");

            for (int i = 0; i < keyValuesData.Count; i++)
            {
                int idFaixaDelete = Convert.ToInt32(keyValuesId[i]);
                int numDiasDelete = Convert.ToInt32(keyValuesData[i]);

                XmlNode node = XmlFaixaLiquidez.GetXmlDocument().SelectSingleNode(string.Format("Lista/Faixa[@IdFaixa='{0}']", idFaixaDelete));
                XmlNode parent = node.ParentNode;
                parent.RemoveChild(node);

                SaveXml(XmlFaixaLiquidez);

            }
        }

        gridCadastro.DataBind();
        gridCadastro.Selection.UnselectAll();
        gridCadastro.CancelEdit();
    }

    /// <summary>
    /// Insert no arquivo XML
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        int maxID = 0;
        List<int> faixas = new List<int>();
        ASPxGridView grid = (ASPxGridView)sender;

        foreach (XmlNode selectedNode in XmlFaixaLiquidez.GetXmlDocument().SelectNodes("Lista/Faixa"))
        {
            int idFaixa = int.Parse(selectedNode.Attributes["IdFaixa"].Value);
            faixas.Add(int.Parse(selectedNode.Attributes["MaxDias"].Value));
            if (idFaixa > maxID)
                maxID = idFaixa;
        }

        foreach (int maxDias in faixas)
        {
            if (Convert.ToInt32(e.NewValues["MaxDias"].ToString()) == maxDias)
            {
                grid.JSProperties["cpException"] = string.Format("A faixa de liquidez até '{0}' dias já está cadastrada.", maxDias);
                e.Cancel = true;
                grid.CancelEdit();
                return;
            }
        }

        e.NewValues.Insert(0, "IdFaixa", maxID + 1);

        XmlNode node = XmlFaixaLiquidez.GetXmlDocument().CreateElement("Faixa");
        foreach (DictionaryEntry entry in e.NewValues)
        {
            XmlAttribute attribute = XmlFaixaLiquidez.GetXmlDocument().CreateAttribute(entry.Key.ToString());
            attribute.Value = entry.Value.ToString();
            node.Attributes.Append(attribute);
        }
        XmlFaixaLiquidez.GetXmlDocument().SelectSingleNode("Lista").AppendChild(node);
        SaveXml(XmlFaixaLiquidez);

        e.Cancel = true;

        grid.CancelEdit();
    }

    protected readonly static object lockObject = new object();

    /// <summary>
    /// Salva dados no arquivo XML
    /// </summary>
    /// <param name="xmlDataSource"></param>
    protected void SaveXml(XmlDataSource xmlDataSource)
    {
        lock (lockObject)
        {
            xmlDataSource.Save();
        }
    }

    /// <summary>
    /// Update no arquivo XML
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gridCadastro_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        ASPxGridView grid = (ASPxGridView)sender;

        int faixa = int.Parse(e.Keys["IdFaixa"].ToString());

        XmlNode node = XmlFaixaLiquidez.GetXmlDocument().SelectSingleNode(string.Format("Lista/Faixa[@IdFaixa='{0}']", faixa));
        if (node != null)
        {
            List<int> faixas = new List<int>();
            foreach (XmlNode selectedNode in XmlFaixaLiquidez.GetXmlDocument().SelectNodes("Lista/Faixa"))
            {
                faixas.Add(int.Parse(selectedNode.Attributes["MaxDias"].Value));
            }

            foreach (int maxDias in faixas)
            {
                if (Convert.ToInt32(e.NewValues["MaxDias"].ToString()) == maxDias)
                {
                    grid.JSProperties["cpException"] = string.Format("A faixa de liquidez até '{0}' dias já está cadastrada.", maxDias);
                    e.Cancel = true;
                    grid.CancelEdit();
                    return;
                }
            }
            foreach (DictionaryEntry entry in e.NewValues)
                node.Attributes[entry.Key.ToString()].Value = entry.Value.ToString();

            SaveXml(XmlFaixaLiquidez);
        }
        else
            grid.JSProperties["cpException"] = string.Format("A faixa = '{0}' foi apagada por outro usuário.", faixa);

        e.Cancel = true;

        grid.CancelEdit();
    }

}