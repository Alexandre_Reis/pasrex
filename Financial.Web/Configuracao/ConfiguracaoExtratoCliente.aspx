﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ConfiguracaoExtratoCliente.aspx.cs" Inherits="ConfiguracaoExtratoCliente" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxrp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="System.Web.UI" TagPrefix="cc1" %>

<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxw" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxwgv" %>

<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="Javascript" src="../js/global.js"></script>
    <script type="text/javascript">
    // <![CDATA[
        function AddSelectedItems() {
            MoveSelectedItems(lbAvailable, lbChoosen, false);
            UpdateButtonState();
        }
        function AddAllItems() {
            MoveAllItems(lbAvailable, lbChoosen, false);
            UpdateButtonState();
        }
        function RemoveSelectedItems() {
            MoveSelectedItems(lbChoosen, lbAvailable, true);
            UpdateButtonState();
        }
        function RemoveAllItems() {
            MoveAllItems(lbChoosen, lbAvailable, true);
            UpdateButtonState();
        }
        function MoveSelectedItems(srcListBox, dstListBox, removeEspaco) {
            srcListBox.BeginUpdate();
            dstListBox.BeginUpdate();
            var items = srcListBox.GetSelectedItems();
            var contaEspaco = 0;
            for(var i = items.length - 1; i >= 0; i = i - 1) {
                if (items[i].value == 999 || items[i].value == 9991 || items[i].value == 9992 ||
                    items[i].value == 9993 || items[i].value == 9994) {
                    if (removeEspaco)
                        srcListBox.RemoveItem(items[i].index);
                    else {
                        contaEspaco++;
                        dstListBox.AddItem(items[i].text, (items[i].value*10)+contaEspaco);
                        srcListBox.RemoveItem(items[i].index);
                        srcListBox.AddItem(items[i].text, items[i].value);
                    }
                }
                else {
                    dstListBox.AddItem(items[i].text, items[i].value);
                    srcListBox.RemoveItem(items[i].index);
                }
            }
            srcListBox.EndUpdate();
            dstListBox.EndUpdate();
        }
        function MoveAllItems(srcListBox, dstListBox, removeEspaco) {
            srcListBox.BeginUpdate();
            var count = srcListBox.GetItemCount();
            for(var i = 0; i < count; i++) {
                var item = srcListBox.GetItem(i);
                if (item.value != 999 && item.value != 9991 && item.value != 9992 &&
                    item.value != 9993 && item.value != 9994) {
                    dstListBox.AddItem(item.text, item.value);
                }
            }
            srcListBox.EndUpdate();
            srcListBox.ClearItems();
            if (!removeEspaco) {
                srcListBox.AddItem("Espaço em Branco", 999);
            }
        }
        function UpdateButtonState() {
            btnMoveAllItemsToRight.SetEnabled(lbAvailable.GetItemCount() > 0);
            btnMoveAllItemsToLeft.SetEnabled(lbChoosen.GetItemCount() > 0);
            btnMoveSelectedItemsToRight.SetEnabled(lbAvailable.GetSelectedItems().length > 0);
            btnMoveSelectedItemsToLeft.SetEnabled(lbChoosen.GetSelectedItems().length > 0);
        }
    // ]]> 
    </script>
</head>

<body>
    <form id="form1" runat="server">
        
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" />

        <dxcb:ASPxCallback ID="callbackCores" runat="server" OnCallback="callbackCores_Callback">
            <ClientSideEvents CallbackComplete="function(s, e)
            {             
                alert(e.result);
            }        
            "/>
        </dxcb:ASPxCallback>

        <div class="divPanel divPanelNew">
            <table width="100%" border="0">
                <tr>
                    <td>
                        <div id="container_small">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Configuração Relatório Extrato Cliente" />
                            </div>
                            
                            <div id="mainContentSpace">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <table border="0" align="center">
                                            <tr>
                                                <td>
                                                    
                                                                <table border="0" align="center" cellpadding="2" cellspacing="2">
                                                                    <tr>
                                                                        <td align="center" colspan="3">
                                                                            <b><asp:Label ID="label9" runat="server" Text="Escolha de Cores" Font-Size="Small" /></b>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right"><asp:Label ID="label113" runat="server" CssClass="labelBold" Text="Estilo de cor padrão" /></td>
                                                                        <td>
                                                                            <dxe:ASPxComboBox ID="dropCores" runat="server" IncrementalFilteringMode="StartsWith" 
                                                                                                ShowShadow="false" DropDownStyle="DropDown" CssClass="dropDownListCurto_1" >
                                                                            <Items>
                                                                            <dxe:ListEditItem Value="1" Text="Azul claro" />
                                                                            <dxe:ListEditItem Value="2" Text="Azul escuro" />
                                                                            <dxe:ListEditItem Value="3" Text="Preto e branco" />
                                                                            <dxe:ListEditItem Value="4" Text="Verde" />
                                                                            <dxe:ListEditItem Value="5" Text="Vermelho" />                                                                            
                                                                            <dxe:ListEditItem Value="6" Text="Padrão" />                                                                            
                                                                            </Items>
                                                                            
                                                                            <ClientSideEvents
                                                                                 SelectedIndexChanged="function(s, e) { if (confirm('As configurações de cores atuais serão substituídas pelo padrão escolhido. Confirma?')==true){callbackCores.PerformCallback();}     
                                                                                                            }"
                                                                                />
                                                                            
                                                                            </dxe:ASPxComboBox>
                                                                        </td>                                                                        
                                                                    </tr>
                                                                    <tr>
                                                                        <td></td>
                                                                        <td align="center"><asp:Label ID="label6" runat="server" CssClass="labelBold" Text="Cor do Texto" /></td>
                                                                        <td align="center"><asp:Label ID="label7" runat="server" CssClass="labelBold" Text="Cor de Fundo" /></td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td style="white-space:nowrap">
                                                                            <asp:Label ID="label3" runat="server" CssClass="labelNormal" Text="Cabeçalho - Cor Texto/Fundo:" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxColorEdit runat="server" ID="headerFieldForeStyle" ClientInstanceName="headerFieldForeStyle" ToolTip="Define a Cor do Texto" OnPreRender="ColorEdit_OnPreRender" AllowNull="false"/>
                                                                        </td>
                                                                        
                                                                        <td>
                                                                            <dxe:ASPxColorEdit runat="server" ID="headerFieldBackStyle" ClientInstanceName="headerFieldBackStyle" ToolTip="Define a Cor de Fundo" OnPreRender="ColorEdit_OnPreRender" AllowNull="false"/>
                                                                        </td>                                                                        
                                                                    </tr>
                                                                    
                                                                    <tr><td></td></tr>
                                                                    <tr>
                                                                        <td style="white-space:nowrap">
                                                                            <asp:Label ID="label13" runat="server" CssClass="labelNormal" Text="Título Relatório - Cor Texto/Fundo:" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxColorEdit runat="server" ID="header1ForeStyle" ClientInstanceName="header1ForeStyle" ToolTip="Define a Cor do Texto" OnPreRender="ColorEdit_OnPreRender" AllowNull="false" />
                                                                        </td>
                                                                        
                                                                        <td>
                                                                            <dxe:ASPxColorEdit runat="server" ID="header1BackStyle" ClientInstanceName="header1BackStyle" ToolTip="Define a Cor de Fundo" OnPreRender="ColorEdit_OnPreRender" AllowNull="false"/>
                                                                        </td>                                                                        
                                                                    </tr>
                                                                    
                                                                    <tr><td></td></tr>
                                                                    <tr>
                                                                        <td style="white-space:nowrap">
                                                                            <asp:Label ID="label1" runat="server" CssClass="labelNormal" Text="Header Principal - Cor Texto/Fundo:" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxColorEdit runat="server" ID="header2ForeStyle" ClientInstanceName="header2ForeStyle" ToolTip="Define a Cor do Texto" OnPreRender="ColorEdit_OnPreRender" AllowNull="false"/>
                                                                        </td>
                                                                        
                                                                        <td>
                                                                            <dxe:ASPxColorEdit runat="server" ID="header2BackStyle" ClientInstanceName="header2BackStyle" ToolTip="Define a Cor de Fundo" OnPreRender="ColorEdit_OnPreRender" AllowNull="false"/>                                                                                                                                                            
                                                                        </td>                                                                        
                                                                    </tr>
                                                                    <tr><td></td></tr>
                                                                    <tr>
                                                                        <td style="white-space:nowrap">
                                                                            <asp:Label ID="label2" runat="server" CssClass="labelNormal" Text="Header Secundário - Cor Texto/Fundo:" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxColorEdit runat="server" ID="header3ForeStyle" ClientInstanceName="header3ForeStyle" ToolTip="Define a Cor do Texto" OnPreRender="ColorEdit_OnPreRender" AllowNull="false"/>
                                                                        </td>
                                                                        
                                                                        <td>
                                                                            <dxe:ASPxColorEdit runat="server" ID="header3BackStyle" ClientInstanceName="header3BackStyle" ToolTip="Define a Cor de Fundo" OnPreRender="ColorEdit_OnPreRender" AllowNull="false"/>
                                                                        </td>                                                                        
                                                                    </tr>
                                                                                                                                                                                                                                                                               
                                                                   <tr><td></td></tr>                                                                    
                                                                    <tr>
                                                                        <td style="white-space:nowrap">
                                                                            <asp:Label ID="label4" runat="server" CssClass="labelNormal" Text="Linhas Pares Grid - Cor Texto/Fundo:" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxColorEdit runat="server" ID="evenForeStyle" ClientInstanceName="evenForeStyle" ToolTip="Define a Cor do Texto" OnPreRender="ColorEdit_OnPreRender" AllowNull="false"/>
                                                                        </td>
                                                                        
                                                                        <td>
                                                                            <dxe:ASPxColorEdit runat="server" ID="evenBackStyle" ClientInstanceName="evenBackStyle" ToolTip="Define a Cor de Fundo" OnPreRender="ColorEdit_OnPreRender" AllowNull="false"/>
                                                                        </td>                                                                        
                                                                    </tr>
                                                                    <tr><td></td></tr>
                                                                    <tr>
                                                                        <td style="white-space:nowrap">
                                                                            <asp:Label ID="label5" runat="server" CssClass="labelNormal" Text="Linhas Impares Grid - Cor Texto/Fundo:" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxColorEdit runat="server" ID="oddForeStyle" ClientInstanceName="oddForeStyle" ToolTip="Define a Cor do Texto" OnPreRender="ColorEdit_OnPreRender" AllowNull="false"/>
                                                                        </td>
                                                                        
                                                                        <td>
                                                                            <dxe:ASPxColorEdit runat="server" ID="oddBackStyle" ClientInstanceName="oddBackStyle" ToolTip="Define a Cor de Fundo" OnPreRender="ColorEdit_OnPreRender" AllowNull="false"/>                                                                            
                                                                        </td>                                                                        
                                                                    </tr>

                                                                </table>
                                                     <dxrp:ASPxRoundPanel ID="pnlFront" runat="server" HeaderText="" Width="800px">
                                                        <PanelCollection>
                                                            <dxp:PanelContent runat="server">                                                                                                                           
                                                                <table align="center" cellpadding="0" cellspacing="0" width="800px">
                                                                    <tr>
                                                                        <td align="center" colspan="3"><br />   
                                                                            <b><asp:Label ID="label10" runat="server" Text="Escolha de Posicionamentos" Font-Size="Small" /></b>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td valign="top" style="width: 38%">
                                                                            <div class="BottomPadding">
                                                                                <asp:Label ID="lblAvailable" runat="server" Text="Disponíveis:" />
                                                                            </div>
                                                                            <dxe:ASPxListBox ID="lbAvailable" runat="server" ClientInstanceName="lbAvailable" Width="100%" Height="466px" SelectionMode="CheckColumn" OnPreRender="lbAvailable_OnPreRender">                                                                            
                                                                                <ClientSideEvents SelectedIndexChanged="function(s, e) { UpdateButtonState(); }" />
                                                                            </dxe:ASPxListBox>
                                                                        </td>
                                                                        <td valign="middle" align="center" style="padding: 10px; width: 26%">
                                                                            <div>
                                                                                <dxe:ASPxButton ID="btnMoveSelectedItemsToRight" runat="server" ClientInstanceName="btnMoveSelectedItemsToRight"
                                                                                    AutoPostBack="False" Text="Adicionar >" Width="130px" Height="23px" ClientEnabled="False">
                                                                                    <ClientSideEvents Click="function(s, e) { AddSelectedItems(); }" />
                                                                                </dxe:ASPxButton>
                                                                            </div>
                                                                            <div class="TopPadding">
                                                                                <dxe:ASPxButton ID="btnMoveAllItemsToRight" runat="server" ClientInstanceName="btnMoveAllItemsToRight"
                                                                                    AutoPostBack="False" Text="Adicionar todos >>" Width="130px" Height="23px">
                                                                                    <ClientSideEvents Click="function(s, e) { AddAllItems(); }" />
                                                                                </dxe:ASPxButton>
                                                                            </div>
                                                                            <div style="height: 32px">
                                                                            </div>
                                                                            <div>
                                                                                <dxe:ASPxButton ID="btnMoveSelectedItemsToLeft" runat="server" ClientInstanceName="btnMoveSelectedItemsToLeft"
                                                                                    AutoPostBack="False" Text="< Remover" Width="130px" Height="23px" ClientEnabled="False">
                                                                                    <ClientSideEvents Click="function(s, e) { RemoveSelectedItems(); }" />
                                                                                </dxe:ASPxButton>
                                                                            </div>
                                                                            <div class="TopPadding">
                                                                                <dxe:ASPxButton ID="btnMoveAllItemsToLeft" runat="server" ClientInstanceName="btnMoveAllItemsToLeft"
                                                                                    AutoPostBack="False" Text="<< Remover todos" Width="130px" Height="23px" ClientEnabled="False">
                                                                                    <ClientSideEvents Click="function(s, e) { RemoveAllItems(); }" />
                                                                                </dxe:ASPxButton>
                                                                            </div>
                                                                        </td>
                                                                        <td valign="top" style="width: 38%">
                                                                            <div class="BottomPadding"><asp:Label ID="lblChosen" runat="server" Text="Escolhidos:" /></div>
                                                                            <dxe:ASPxListBox ID="lbChoosen" runat="server" ClientInstanceName="lbChoosen" Width="100%" Height="466px" SelectionMode="CheckColumn" OnPreRender="lbChoosen_OnPreRender">
                                                                                <ClientSideEvents SelectedIndexChanged="function(s, e) { UpdateButtonState(); }" />
                                                                            </dxe:ASPxListBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                
                                                                </dxp:PanelContent>
                                                        </PanelCollection>
                                                    </dxrp:ASPxRoundPanel>
                                                                
                                                                <table border="0" align="center" cellpadding="2" cellspacing="2">
                                                                    <tr>
                                                                        <td align="center">
                                                                            <h3><b><asp:Label ID="label8" runat="server" Text="Texto Observação" /></b></h3>
                                                                       
                                                                            <dx:ASPxHtmlEditor width="800px" ID="htmlEditorObservacao" ClientInstanceName="htmlEditorObservacao" runat="server" OnPreRender="HtmlEditorObservacao_OnPreRender"/>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                
                                                            
                                                                <table align="left" border="0" width="600px"  cellpadding="2" cellspacing="2">
                                                                <tr>
                                                                <td align="right" width="200px">
                                                                <asp:Label ID="label11" runat="server" CssClass="labelBold" Text="Estilo dos gráficos de distribuição" />
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxComboBox ID="dropEstiloGraficodistribuicao" ClientInstanceName="dropEstiloGraficodistribuicao" runat="server" 
                                                                                        CssClass="dropDownListCurto_5" OnInit="dropEstiloGraficodistribuicao_Init" >
                                                                    <Items>
                                                                    <dxe:ListEditItem Value="P" Text="Pizza" />
                                                                    <dxe:ListEditItem Value="D" Text="Donut" />                                                                       
                                                                    </Items>
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                                
                                                                <td align="right" width="70px">
                                                                <asp:Label ID="label15" runat="server" CssClass="labelBold" Text="Exibe" />
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxComboBox ID="dropExibeTopN" ClientInstanceName="dropExibeTopN" runat="server" 
                                                                                        CssClass="dropDownListCurto_5" OnInit="dropExibeTopN_Init" >
                                                                    <Items>
                                                                    <dxe:ListEditItem Value="3" Text="Top 3" />
                                                                    <dxe:ListEditItem Value="5" Text="Top 5" /> 
                                                                    <dxe:ListEditItem Value="99" Text="Todos" />                                                                       
                                                                    </Items>
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                                
                                                                <td align="right" width="150px">
                                                                <asp:Label ID="label14" runat="server" CssClass="labelBold" Text="Mostra Patrimônio Total" />
                                                                </td>
                                                                <td>
                                                                    <dxe:ASPxComboBox ID="dropPatrimonioTotal" ClientInstanceName="dropPatrimonioTotal" runat="server" 
                                                                                        CssClass="dropDownListCurto_6" OnPreRender="dropPatrimonioTotal_OnPreRender">
                                                                    <Items>
                                                                    <dxe:ListEditItem Value="S" Text="Sim" />
                                                                    <dxe:ListEditItem Value="N" Text="Não" />                                                                       
                                                                    </Items>
                                                                    </dxe:ASPxComboBox>
                                                                </td>
                                                                                                                                                                                                                                                                
                                                                </tr>
                                                                </table>
                                                                <br /><br />
                                                                <div class="linkButton linkButtonTbar" style="margin: 0px 0 0 0px !important;">
                                                                    <div class="linkButtonWrapper">
                                                                        <asp:LinkButton ID="LinkButton1" runat="server" Font-Overline="false" CssClass="btnOK" OnClick="btnSalvar_Click">
                                                                            <asp:Literal ID="Literal1" runat="server" Text="Salvar" /><div></div>
                                                                        </asp:LinkButton>
                                                                    </div>
                                                                </div>
                                                                <br />
                                                                
                                                                <table align="center" border="0" width="600px"  cellpadding="2" cellspacing="2">
                                                                <tr><td align="center"><b>
                                                                    <asp:Label ID="label12" runat="server" Text="Faixas de Liquidez" Font-Size="Small" /></b>
                                                                </td></tr>
                                                                <tr><td>
                                                                        <div class="linkButton linkButtonTbar" style="margin: 0px 0 0 0px !important;">
                                                                            <asp:LinkButton ID="btnAdd" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnAdd" OnClientClick="gridCadastro.AddNewRow(); return false;"><asp:Literal ID="Literal2" runat="server" Text="Novo"/><div></div></asp:LinkButton>
                                                                            <asp:LinkButton ID="btnDelete" runat="server" Font-Overline="false" ValidationGroup="ATK" CssClass="btnDelete" OnClientClick=" if (confirm('Tem certeza que quer excluir?')==true) gridCadastro.PerformCallback('btnDelete');return false;"><asp:Literal ID="Literal3" runat="server" Text="Excluir"/><div></div></asp:LinkButton>
                                                                        </div>
                                                                </td></tr>
                                                                <tr>
                                                                    <td>
                                                                        <div class="divDataGrid">
                                                                            <dxwgv:ASPxGridView ID="gridCadastro"  runat="server" EnableCallBacks="true" AutoGenerateColumns="False"
                                                                                ClientInstanceName="gridCadastro" DataSourceID="XmlFaixaLiquidez" KeyFieldName="IdFaixa"
                                                                                OnCellEditorInitialize="gridCadastro_CellEditorInitialize" Width="600px" OnPreRender="gridCadastro_PreRender"
                                                                                OnRowInserting="gridCadastro_RowInserting" 
                                                                                OnRowUpdating="gridCadastro_RowUpdating" OnCustomCallback="gridCadastro_CustomCallback">
                                                                                <Columns>
                                                                                        <dxwgv:GridViewCommandColumn VisibleIndex="0" ShowClearFilterButton="True">
                                                                                            <HeaderTemplate>
                                                                                                <dxe:ASPxCheckBox ID="cbAll" ClientInstanceName="cbAll" Width="15%" runat="server" ToolTip="Seleciona todos os CheckBoxs de todas as páginas" BackColor="white" ClientSideEvents-CheckedChanged="function(s, e) {OnAllCheckedChanged(s, e, gridCadastro);}" OnInit="CheckBoxSelectAll"/>
                                                                                            </HeaderTemplate>
                                                                                        </dxwgv:GridViewCommandColumn>
                                                                                    
                                                                                    <dxwgv:GridViewDataColumn FieldName="IdFaixa" VisibleIndex="1" SortIndex="0" SortOrder="Ascending" Visible="False">
                                                                                    </dxwgv:GridViewDataColumn>
                                                                                    
                                                                                    <dxwgv:GridViewDataSpinEditColumn FieldName="MaxDias" Caption="Máximo de Dias" VisibleIndex="1" 
                                                                                    Width="85%" PropertiesSpinEdit-NumberFormat="Number" PropertiesSpinEdit-NumberType="Integer" >
                                                                                        <EditFormSettings Visible="True" />
                                                                                    </dxwgv:GridViewDataSpinEditColumn>
                                                                                </Columns>
                                                                                <Settings ShowStatusBar="Hidden" />
                                                                                <SettingsCommandButton>
                                                                                    <ClearFilterButton>
                                                                                        <Image Url="~/imagens/funnel--minus.png">
                                                                                        </Image>
                                                                                    </ClearFilterButton>
                                                                                    <UpdateButton>
                                                                                        <Image Url="~/imagens/ico_form_ok_inline.gif">
                                                                                        </Image>
                                                                                    </UpdateButton>
                                                                                    <CancelButton>
                                                                                        <Image Url="~/imagens/ico_form_back_inline.gif">
                                                                                        </Image>
                                                                                    </CancelButton>
                                                                                </SettingsCommandButton>
                                                                            </dxwgv:ASPxGridView>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                </table>
                                                </td>
                                        </table>                
                                      <asp:XmlDataSource ID="XmlFaixaLiquidez" runat="server" DataFile="~/App_Data/FaixaLiquidez.xml"></asp:XmlDataSource>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>