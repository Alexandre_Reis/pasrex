﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ConfiguracaoSeguranca.aspx.cs"
    Inherits="ConfiguracaoSeguranca" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxpc" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxrp" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxp" %>
<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI.HtmlControls" TagPrefix="cc2" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxcb" %>
<%@ Register Assembly="System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="System.Web.UI" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web" TagPrefix="dxe" %>
<%@ Register Assembly="EntitySpaces.Web" Namespace="EntitySpaces.Web" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/forms.css" rel="stylesheet" type="text/css" />
    <link rel="SHORTCUT ICON" href="~/imagensPersonalizadas/favicon.ico" type="image/x-icon" />
    
    <script type="text/javascript" language="Javascript" src="../../js/global.js"></script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" />

        <div class="divPanel divPanelNew">
            <table width="100%" border="0">
                <tr>
                    <td>
                        <div id="container_small">
                            <div id="header">
                                <asp:Label ID="lblHeader" runat="server" Text="Configuração de Segurança" />
                            </div>
                            <div id="mainContentSpace">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <br />
                                        <br />
                                        <table border="0" align="center">
                                            <tr>
                                                <td>
                                                    <dxrp:ASPxRoundPanel ID="pnlSeguranca" runat="server" HeaderText=" " HeaderStyle-Font-Size="11px"
                                                        Width="459px">
                                                        <PanelCollection>
                                                            <dxp:PanelContent runat="server">
                                                                <br />
                                                                <table border="0" align="center" cellpadding="2" cellspacing="2">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label11" runat="server" CssClass="labelRequired" Text="Tamanho Mínimo da Senha:" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxSpinEdit ID="txtTamanhoMinimoSenha" runat="server" NumberType="Integer"
                                                                                ClientInstanceName="txtTamanhoMinimoSenha" MaxLength="2" OnPreRender="txtTamanhoMinimoSenha_OnPrerender"
                                                                                CssClass="textCurto" MinValue="0" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label1" runat="server" CssClass="labelRequired" Text="Tempo de Expiração da Senha (Dias):" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxSpinEdit ID="txtTempoExpiracaoSenha" runat="server" NumberType="Integer"
                                                                                ClientInstanceName="txtTempoExpiracaoSenha" MaxLength="4" OnPreRender="txtTempoExpiracaoSenha_OnPrerender"
                                                                                CssClass="textCurto" MinValue="0" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label2" runat="server" CssClass="labelRequired" Text="Tentativas para Bloqueio de Senha:" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxSpinEdit ID="txtTentativasBloqueioSenha" runat="server" NumberType="Integer"
                                                                                ClientInstanceName="txtTentativasBloqueioSenha" MaxLength="2" OnPreRender="txtTentativasBloqueioSenha_OnPrerender"
                                                                                CssClass="textCurto" MinValue="0" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label7" runat="server" CssClass="labelRequired" Text="Reset de Tentativas de Senha (Minutos):" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxSpinEdit ID="txtJanelaTempoResetSenha" runat="server" NumberType="Integer"
                                                                                ClientInstanceName="txtJanelaTempoResetSenha" MaxLength="4" OnPreRender="txtJanelaTempoResetSenha_OnPrerender"
                                                                                CssClass="textCurto" MinValue="0" />
                                                                        </td>
                                                                    </tr>                                                                    
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label3" runat="server" CssClass="labelRequired" Text="Historico Senhas:" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxSpinEdit ID="txtHistoricoSenhas" runat="server" NumberType="Integer" ClientInstanceName="txtHistoricoSenhas"
                                                                                MaxLength="2" OnPreRender="txtHistoricoSenhas_OnPrerender" CssClass="textCurto" MinValue="0" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label4" runat="server" CssClass="labelRequired" Text="Mín. Número de Caracteres Especiais na Senha:" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxSpinEdit ID="txtMinimoCaracterEspecialSenha" runat="server" NumberType="Integer"
                                                                                ClientInstanceName="txtMinimoCaracterEspecialSenha" MaxLength="2" OnPreRender="txtMinimoCaracterEspecialSenha_OnPrerender"
                                                                                CssClass="textCurto" MinValue="0" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label5" runat="server" CssClass="labelRequired" Text="Mín. de Caracteres Numéricos na Senha:" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxSpinEdit ID="txtMinimoNumeroSenha" runat="server" NumberType="Integer" ClientInstanceName="txtMinimoNumeroSenha"
                                                                                MaxLength="2" OnPreRender="txtMinimoNumeroSenha_OnPrerender" CssClass="textCurto" MinValue="0" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="label6" runat="server" CssClass="labelRequired" Text="Mín. Num. de Letras Maísculas na Senha:" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxSpinEdit ID="txtLetrasMaiusculas" runat="server" NumberType="Integer" ClientInstanceName="txtLetrasMaiusculas"
                                                                                MaxLength="2" OnPreRender="txtLetrasMaiusculas_OnPrerender" CssClass="textCurto" MinValue="0" />
                                                                        </td>
                                                                    </tr>
                                                                     <tr>
                                                                        <td>
                                                                            <asp:Label ID="label8" runat="server" CssClass="labelRequired" Text="Acesso Simultâneo de um mesmo login:" />
                                                                        </td>
                                                                        <td>
                                                                            <dxe:ASPxComboBox ID="dropAcessoSimultaneo" runat="server" CssClass="dropDownListCurto_6"
                                                                                ClientInstanceName="dropAcessoSimultaneo" OnPreRender="dropAcessoSimultaneo_OnPreRender">
                                                                                <Items>
                                                                                    <dxe:ListEditItem Value="S" Text="Sim" />
                                                                                    <dxe:ListEditItem Value="N" Text="Não" />
                                                                                </Items>
                                                                            </dxe:ASPxComboBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <div class="linkButton linkButtonTbar" style="padding-left: 50pt">
                                                                    <div class="linkButtonWrapper">
                                                                        <asp:LinkButton ID="LinkButton1" runat="server" Font-Overline="false" CssClass="btnOK"
                                                                            OnClick="btnSalvar_Click">
                                                                            <asp:Literal ID="Literal1" runat="server" Text="Salvar" /><div>
                                                                            </div>
                                                                        </asp:LinkButton>
                                                                    </div>
                                                                </div>
                                                            </dxp:PanelContent>
                                                        </PanelCollection>
                                                    </dxrp:ASPxRoundPanel>
                                                </td>
                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
