﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using log4net;
using EntitySpaces.Interfaces;
using Financial.Web.Common;
using DevExpress.Web;
using Financial.ContaCorrente.Enums;
using Financial.Bolsa.Enums;
using Financial.BMF.Enums;
using Financial.Fundo.Enums;
using Financial.Util.Enums;
using Financial.Util;
using Financial.Util.ConfiguracaoSistema;
using Financial.WebConfigConfiguration;
using System.Text.RegularExpressions;
using Financial.Common;
using Financial.RendaFixa.Enums;

public partial class Configuracao : BasePage {
    private static readonly ILog log = LogManager.GetLogger(typeof(Configuracao));

    protected void EsDSAgenteMercadoDistribuidor_esSelect(object sender, EntitySpaces.Web.esDataSourceSelectEventArgs e)
    {
        AgenteMercadoCollection coll = new AgenteMercadoCollection();
        coll.Query.Where(coll.Query.FuncaoDistribuidor.Equal("S"));
        coll.Query.OrderBy(coll.Query.Nome.Ascending);
        coll.Query.Load();

        // Assign the esDataSourcSelectEvenArgs Collection property
        e.Collection = coll;
    }

    #region Enums
    public enum IntegracaoBolsa {
        NaoIntegra = 0,
        Sinacor = 1
    }

    public enum IntegracaoBMF {
        NaoIntegra = 0,
        Sinacor = 1
    }

    public enum IntegracaoRendaFixa {
        NaoIntegra = 0,
        Sinacor = 1
    }

    public enum IntegracaoCC {
        NaoIntegra = 0,
        Sinacor = 1
    }

    public enum EntradaOperacao {
        Compras = 1,
        Vendas = 2
    }
    
    #endregion

    new protected void Page_Load(object sender, EventArgs e) {
        // 
        if (!Page.IsPostBack) {

            /* Se não existir os Ids de Outras Configurações Insere esses ids */
            
            ConfiguracaoCollection configuracaoCollection = new ConfiguracaoCollection();
            configuracaoCollection.LoadAll();

            List<int> idsExistentes = new List<int>();
            // Lista de id Existentes
            for (int i = 0; i < configuracaoCollection.Count; i++) {
                idsExistentes.Add(configuracaoCollection[i].Id.Value);
            }

            if(!idsExistentes.Contains(ConfiguracaoRegrasNegocio.Outras.MultiConta)) {
                Financial.Util.ConfiguracaoSistema.Configuracao c = new Financial.Util.ConfiguracaoSistema.Configuracao();
                c.Id = ConfiguracaoRegrasNegocio.Outras.MultiConta;
                c.Descricao = "MultiConta";
                c.ValorTexto = WebConfig.AppSettings.MultiConta ? "S" : "N";;
                c.Save();
            }
            if(!idsExistentes.Contains(ConfiguracaoRegrasNegocio.Outras.AgenteLiquidacaoExclusivo)) {
                Financial.Util.ConfiguracaoSistema.Configuracao c = new Financial.Util.ConfiguracaoSistema.Configuracao();
                c.Id = ConfiguracaoRegrasNegocio.Outras.AgenteLiquidacaoExclusivo;
                c.Descricao = "AgenteLiquidacaoExclusivo";
                c.ValorTexto = WebConfig.AppSettings.AgenteLiquidacaoExclusivo == "true" ? "S" : "N";
                c.Save();
            }
            if(!idsExistentes.Contains(ConfiguracaoRegrasNegocio.Outras.CalculaContatil)) {
                Financial.Util.ConfiguracaoSistema.Configuracao c = new Financial.Util.ConfiguracaoSistema.Configuracao();
                c.Id = ConfiguracaoRegrasNegocio.Outras.CalculaContatil;
                c.Descricao = "CalculaContatil";
                c.ValorTexto = WebConfig.AppSettings.CalculaContatil ? "S" : "N";
                c.Save();
            }
            if(!idsExistentes.Contains(ConfiguracaoRegrasNegocio.Outras.PermissaoInternoAuto)) {
                Financial.Util.ConfiguracaoSistema.Configuracao c = new Financial.Util.ConfiguracaoSistema.Configuracao();
                c.Id = ConfiguracaoRegrasNegocio.Outras.PermissaoInternoAuto;
                c.Descricao = "PermissaoInternoAuto";
                c.ValorTexto = WebConfig.AppSettings.PermissaoInternoAuto ? "S" : "N";
                c.Save();
            }
            if (!idsExistentes.Contains(ConfiguracaoRegrasNegocio.Outras.LiberaDataLancamentoLiquidacao)) {
                Financial.Util.ConfiguracaoSistema.Configuracao c = new Financial.Util.ConfiguracaoSistema.Configuracao();
                c.Id = ConfiguracaoRegrasNegocio.Outras.LiberaDataLancamentoLiquidacao;
                c.Descricao = "LiberaDataLancamentoLiquidacao";
                c.ValorTexto = WebConfig.AppSettings.LiberaDataLancamentoLiquidacao ? "S" : "N";
                c.Save();
            }
            if (!idsExistentes.Contains(ConfiguracaoRegrasNegocio.Bolsa.RenovacaoTermo)) {
                Financial.Util.ConfiguracaoSistema.Configuracao c = new Financial.Util.ConfiguracaoSistema.Configuracao();
                c.Id = ConfiguracaoRegrasNegocio.Bolsa.RenovacaoTermo;
                c.Descricao = "Renovação Automática de Termo";
                c.ValorTexto = "N";
                c.Save();
            }
            if (!idsExistentes.Contains(ConfiguracaoRegrasNegocio.Bolsa.ConsolidacaoCustoMedio)) {
                Financial.Util.ConfiguracaoSistema.Configuracao c = new Financial.Util.ConfiguracaoSistema.Configuracao();
                c.Id = ConfiguracaoRegrasNegocio.Bolsa.ConsolidacaoCustoMedio;
                c.Descricao = "Consolidação Custo Médio";
                c.ValorTexto = "S";
                c.Save();
            }
            if (!idsExistentes.Contains(ConfiguracaoRegrasNegocio.Bolsa.IsencaoIRAcoes))
            {
                Financial.Util.ConfiguracaoSistema.Configuracao c = new Financial.Util.ConfiguracaoSistema.Configuracao();
                c.Id = ConfiguracaoRegrasNegocio.Bolsa.IsencaoIRAcoes;
                c.Descricao = "Aplica Isenção em IR sobre Ações";
                c.ValorTexto = "S";
                c.Save();
            }
            if (!idsExistentes.Contains(ConfiguracaoRegrasNegocio.Integracoes.IntegracaoRendaFixa)) {
                Financial.Util.ConfiguracaoSistema.Configuracao c = new Financial.Util.ConfiguracaoSistema.Configuracao();
                c.Id = ConfiguracaoRegrasNegocio.Integracoes.IntegracaoRendaFixa;
                c.Descricao = "Integra Renda Fixa";
                c.ValorNumerico = 0;
                c.Save();
            }
            if (!idsExistentes.Contains(ConfiguracaoRegrasNegocio.RendaFixaSwap.ControleCustodia))
            {
                Financial.Util.ConfiguracaoSistema.Configuracao c = new Financial.Util.ConfiguracaoSistema.Configuracao();
                c.Id = ConfiguracaoRegrasNegocio.RendaFixaSwap.ControleCustodia;
                c.Descricao = "Controle de Custódia RF";
                c.ValorNumerico = 0;
                c.Save();
            }
            if (!idsExistentes.Contains(ConfiguracaoRegrasNegocio.Outras.CalculaFatorIndice))
            {
                Financial.Util.ConfiguracaoSistema.Configuracao c = new Financial.Util.ConfiguracaoSistema.Configuracao();
                c.Id = ConfiguracaoRegrasNegocio.Outras.CalculaFatorIndice;
                c.Descricao = "Calcula Fator Índice";
                c.ValorTexto = "N";
                c.Save();
            }
            if (!idsExistentes.Contains(ConfiguracaoRegrasNegocio.Outras.ProcessaIndicadorCarteira))
            {
                Financial.Util.ConfiguracaoSistema.Configuracao c = new Financial.Util.ConfiguracaoSistema.Configuracao();
                c.Id = ConfiguracaoRegrasNegocio.Outras.ProcessaIndicadorCarteira;
                c.Descricao = "Processa Indicador Carteira";
                c.ValorTexto = "N";
                c.Save();
            }
            if (!idsExistentes.Contains(ConfiguracaoRegrasNegocio.Outras.SuitabilityRendaFixa))
            {
                Financial.Util.ConfiguracaoSistema.Configuracao c = new Financial.Util.ConfiguracaoSistema.Configuracao();
                c.Id = ConfiguracaoRegrasNegocio.Outras.SuitabilityRendaFixa;
                c.Descricao = "Suitability Renda Fixa";
                c.ValorTexto = "N";
                c.Save();
            }
            if (!idsExistentes.Contains(ConfiguracaoRegrasNegocio.Outras.DescontarRendimentoCupom))
            {
                Financial.Util.ConfiguracaoSistema.Configuracao c = new Financial.Util.ConfiguracaoSistema.Configuracao();
                c.Id = ConfiguracaoRegrasNegocio.Outras.DescontarRendimentoCupom;
                c.Descricao = "Descontar Rendimento Cupom";
                c.ValorTexto = "S";
                c.Save();
            }
            if (!idsExistentes.Contains(ConfiguracaoRegrasNegocio.Fundo.PfeeCotasFundos)) {
                Financial.Util.ConfiguracaoSistema.Configuracao c = new Financial.Util.ConfiguracaoSistema.Configuracao();
                c.Id = ConfiguracaoRegrasNegocio.Fundo.PfeeCotasFundos;
                c.Descricao = "'PfeeCotasFundos";
                c.ValorTexto = "N";
                c.Save();
            }

            if (!idsExistentes.Contains(ConfiguracaoRegrasNegocio.Outras.IdAutomaticoPessoa))
            {
                Financial.Util.ConfiguracaoSistema.Configuracao c = new Financial.Util.ConfiguracaoSistema.Configuracao();
                c.Id = ConfiguracaoRegrasNegocio.Outras.IdAutomaticoPessoa;
                c.Descricao = "IdAutomaticoPessoa";
                c.ValorTexto = "N";
                c.Save();
            }

            if (!idsExistentes.Contains(ConfiguracaoRegrasNegocio.Outras.PermiteDuplDocumentoPessoa))
            {
                Financial.Util.ConfiguracaoSistema.Configuracao c = new Financial.Util.ConfiguracaoSistema.Configuracao();
                c.Id = ConfiguracaoRegrasNegocio.Outras.PermiteDuplDocumentoPessoa;
                c.Descricao = "PermiteDuplDocumentoPessoa";
                c.ValorTexto = "N";
                c.Save();
            }

            if (!idsExistentes.Contains(ConfiguracaoRegrasNegocio.Outras.PropagaAlterNomeCliente))
            {
                Financial.Util.ConfiguracaoSistema.Configuracao c = new Financial.Util.ConfiguracaoSistema.Configuracao();
                c.Id = ConfiguracaoRegrasNegocio.Outras.PropagaAlterNomeCliente;
                c.Descricao = "PropagaAlterNomeCliente";
                c.ValorTexto = "N";
                c.Save();
            }

            if (!idsExistentes.Contains(ConfiguracaoRegrasNegocio.Outras.EstendeConceitoPessoaAgente))
            {
                Financial.Util.ConfiguracaoSistema.Configuracao c = new Financial.Util.ConfiguracaoSistema.Configuracao();
                c.Id = ConfiguracaoRegrasNegocio.Outras.EstendeConceitoPessoaAgente;
                c.Descricao = "EstendeConceitoPessoaAgente";
                c.ValorTexto = "N";
                c.Save();
            }

            if (!idsExistentes.Contains(ConfiguracaoRegrasNegocio.Outras.PropagaAlterNomeAgente))
            {
                Financial.Util.ConfiguracaoSistema.Configuracao c = new Financial.Util.ConfiguracaoSistema.Configuracao();
                c.Id = ConfiguracaoRegrasNegocio.Outras.PropagaAlterNomeAgente;
                c.Descricao = "PropagaAlterNomeAgente";
                c.ValorTexto = "N";
                c.Save();
            }

            if (!idsExistentes.Contains(ConfiguracaoRegrasNegocio.Outras.AbortaProcRFPorErroCadastraTitulo))
            {
                Financial.Util.ConfiguracaoSistema.Configuracao c = new Financial.Util.ConfiguracaoSistema.Configuracao();
                c.Id = ConfiguracaoRegrasNegocio.Outras.AbortaProcRFPorErroCadastraTitulo;
                c.Descricao = "Aborta processamento por erro cadastral no título de RF";
                c.ValorTexto = "S";
                c.Save();
            }

            if (!idsExistentes.Contains(ConfiguracaoRegrasNegocio.Outras.CalculaCofUsandoPLD0))
            {
                Financial.Util.ConfiguracaoSistema.Configuracao c = new Financial.Util.ConfiguracaoSistema.Configuracao();
                c.Id = ConfiguracaoRegrasNegocio.Outras.CalculaCofUsandoPLD0;
                c.Descricao = "Calcula COF usando pl de D-0";
                c.ValorTexto = "N";
                c.Save();
            }


            if (!idsExistentes.Contains(ConfiguracaoRegrasNegocio.Outras.ValidaQtdeResgateProcessamento))
            {
                Financial.Util.ConfiguracaoSistema.Configuracao c = new Financial.Util.ConfiguracaoSistema.Configuracao();
                c.Id = ConfiguracaoRegrasNegocio.Outras.ValidaQtdeResgateProcessamento;
                c.Descricao = "Valida Qtde na data de Conversão (Processamento)";
                c.ValorTexto = "N";
                c.Save();
            }

            if (!idsExistentes.Contains(ConfiguracaoRegrasNegocio.Outras.TipoVisualizacaoFundoResgateCotista))
            {
                Financial.Util.ConfiguracaoSistema.Configuracao c = new Financial.Util.ConfiguracaoSistema.Configuracao();
                c.Id = ConfiguracaoRegrasNegocio.Outras.TipoVisualizacaoFundoResgateCotista;
                c.Descricao = "Tipo Visualização Fundos (Resgates de Cotistas)";
                c.ValorTexto = "C";
                c.Save();
            }
        }
    }

    /// <summary>
    /// Salva os Parametros de Configuração no Banco
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>   
    protected void btnSalvar_Click(object sender, EventArgs e) {
        this.TrataErros();

        ConfiguracaoCollection configuracaoCollection = new ConfiguracaoCollection();
        configuracaoCollection.LoadAll();
        //
        for (int i = 0; i < configuracaoCollection.Count; i++) {
            int id = configuracaoCollection[i].Id.Value;
            //                   
            switch (id) 
            {
                #region Integracoes
                case ConfiguracaoRegrasNegocio.Integracoes.IntegracaoBolsa:
                    configuracaoCollection[i].ValorNumerico = Convert.ToDecimal(this.dropIntegracaoBolsa.SelectedItem.Value);
                    break;
                case ConfiguracaoRegrasNegocio.Integracoes.IntegracaoRendaFixa:
                    configuracaoCollection[i].ValorNumerico = Convert.ToDecimal(this.dropIntegracaoRendaFixa.SelectedItem.Value);
                    break;
                case ConfiguracaoRegrasNegocio.Integracoes.IntegracaoBMF:
                    configuracaoCollection[i].ValorNumerico = Convert.ToDecimal(this.dropIntegracaoBMF.Value);
                    break;
                case ConfiguracaoRegrasNegocio.Integracoes.IntegracaoCC:
                    configuracaoCollection[i].ValorNumerico = Convert.ToDecimal(this.dropIntegracaoCC.Value);
                    break;
                case ConfiguracaoRegrasNegocio.Integracoes.IntegraGalgoPlCota:
                    configuracaoCollection[i].ValorNumerico = Convert.ToDecimal(this.dropGalgoPlCota.Value);
                    break;
                case ConfiguracaoRegrasNegocio.Integracoes.IntegracaoCodigosCC:
                    configuracaoCollection[i].ValorTexto = Convert.ToString(this.textCodigosCC.Text);
                    break;
                case ConfiguracaoRegrasNegocio.Integracoes.SchemaSinacor:
                    configuracaoCollection[i].ValorTexto = Convert.ToString(this.textSchemaSinacor.Text);
                    break;
                case ConfiguracaoRegrasNegocio.Integracoes.SchemaBTC:
                    configuracaoCollection[i].ValorTexto = Convert.ToString(this.textSchemaBTC.Text);
                    break;
                case ConfiguracaoRegrasNegocio.Integracoes.UsuarioIntegracao:
                    configuracaoCollection[i].ValorTexto = Convert.ToString(this.textUsuarioIntegracao.Text);
                    break;
                case ConfiguracaoRegrasNegocio.Integracoes.SenhaIntegracao:
                    string usuarioIntegracao = Convert.ToString(this.textUsuarioIntegracao.Text);
                    string senhaIntegracao = Convert.ToString(this.textSenhaIntegracao.Text);
                    if (!String.IsNullOrEmpty(senhaIntegracao))
                    {
                        configuracaoCollection[i].ValorTexto = Convert.ToString(this.textSenhaIntegracao.Text);
                    }
                    else
                    {
                        //O campo de senha por questoes de seguranca nunca eh recarregado. Ele soh pode ser atualizado com um novo valor
                        //Para limpa-lo, soh se o campo usuario tambem estiver nulo
                        if (String.IsNullOrEmpty(usuarioIntegracao))
                        {
                            configuracaoCollection[i].ValorTexto = senhaIntegracao;
                        }
                    }
                    
                    break;
                case ConfiguracaoRegrasNegocio.Integracoes.NomeInstituicao:
                    configuracaoCollection[i].ValorTexto = Convert.ToString(this.textNomeInstituicao.Text);
                    break;
                case ConfiguracaoRegrasNegocio.Integracoes.CodInstituicao:
                    configuracaoCollection[i].ValorTexto = Convert.ToString(this.textCodInstituicao.Text);
                    break;
                case ConfiguracaoRegrasNegocio.Integracoes.ResponsavelInstituicao:
                    configuracaoCollection[i].ValorTexto = Convert.ToString(this.textResponsavelInstituicao.Text);
                    break;
                case ConfiguracaoRegrasNegocio.Integracoes.TelefoneInstituicao:
                    configuracaoCollection[i].ValorTexto = Convert.ToString(this.textTelefoneInstituicao.Text);
                    break;
                case ConfiguracaoRegrasNegocio.Integracoes.EmailInstituicao:
                    configuracaoCollection[i].ValorTexto = Convert.ToString(this.textEmailInstituicao.Text);
                    break;
                #endregion

                #region ParametrosBolsa
                case ConfiguracaoRegrasNegocio.Bolsa.TipoVisualizacao:
                    configuracaoCollection[i].ValorNumerico = Convert.ToDecimal(this.dropTipoVizualizacaoBolsa.Value);
                    break;
                case ConfiguracaoRegrasNegocio.Bolsa.PrioridadeCasamentoVencimento:
                    configuracaoCollection[i].ValorNumerico = Convert.ToDecimal(this.dropBolsaPrioridadeCasamento.Value);
                    break;
                case ConfiguracaoRegrasNegocio.Bolsa.MTMTermoVendido:
                    configuracaoCollection[i].ValorTexto = (string)this.dropBolsaMTMTermoVendido.Value;
                    break;
                case ConfiguracaoRegrasNegocio.Bolsa.CompensaIRDayTrade:
                    configuracaoCollection[i].ValorTexto = (string)this.dropBolsaCompensaIRDaytrade.Value;
                    break;
                case ConfiguracaoRegrasNegocio.Bolsa.CodigoAgenteDefault:
                    configuracaoCollection[i].ValorNumerico = Convert.ToDecimal(this.txtCodigoBovespa.Text);
                    break;
                case ConfiguracaoRegrasNegocio.Bolsa.EntradaOperacoes:
                    configuracaoCollection[i].ValorNumerico = Convert.ToDecimal(this.dropEntradaOperacoes.Value);
                    break;
                case ConfiguracaoRegrasNegocio.Bolsa.CustodiaBTC:
                    configuracaoCollection[i].ValorNumerico = Convert.ToDecimal(this.dropCustodiaBTC.Value);
                    break;
                case ConfiguracaoRegrasNegocio.Bolsa.RenovacaoTermo:
                    configuracaoCollection[i].ValorTexto = (string)this.dropBolsaRenovacaoTermo.Value;
                    break;
                case ConfiguracaoRegrasNegocio.Bolsa.ConsolidacaoCustoMedio:
                    configuracaoCollection[i].ValorTexto = (string)this.dropConsolidaCustoMedio.Value;
                    break;
                case ConfiguracaoRegrasNegocio.Bolsa.EmiteMsgSemCotacaoBolsa:
                    configuracaoCollection[i].ValorTexto = (string)this.chkEmiteMsgSemCotacaoBolsa.Value;
                    break;
                case ConfiguracaoRegrasNegocio.Bolsa.IsencaoIRAcoes:
                    configuracaoCollection[i].ValorTexto = (string)this.dropBolsaIsencaoIRAcoes.Value;
                    break;
                #endregion

                #region ParametrosBMF
                case ConfiguracaoRegrasNegocio.BMF.TipoVisualizacao:
                    configuracaoCollection[i].ValorNumerico = Convert.ToDecimal(this.dropTipoVizualizacaoBMF.Value);
                    break;
                case ConfiguracaoRegrasNegocio.BMF.TipoCalculoTaxasBMF:
                    configuracaoCollection[i].ValorNumerico = Convert.ToDecimal(this.dropTipoCalculoTaxasBMF.Value);
                    break;
                #endregion

                #region ParametrosFundo e ParametrosRendFixa
                case ConfiguracaoRegrasNegocio.Fundo.TratamentoCotaInexistente:
                    configuracaoCollection[i].ValorNumerico = Convert.ToDecimal(this.dropCotaInexistente.Value);
                    break;

                case ConfiguracaoRegrasNegocio.Fundo.RecalculaCota:
                    configuracaoCollection[i].ValorTexto = (string)this.dropRecalculaCotaFechamento.Value;
                    break;

                case ConfiguracaoRegrasNegocio.Fundo.RetornoBenchmark:
                    configuracaoCollection[i].ValorNumerico = Convert.ToDecimal(this.dropRetornoBenchmark.Value);
                    break;

                case ConfiguracaoRegrasNegocio.Fundo.UsaCotaInicialPosicaoIR:
                    configuracaoCollection[i].ValorTexto = (string)this.dropUsaCotaInicialPosicaoIR.Value;
                    break;

                case ConfiguracaoRegrasNegocio.Fundo.RetornoFDICAjustado:
                    configuracaoCollection[i].ValorTexto = (string)this.dropRetornoFDICAjustado.Value;
                    break;

                case ConfiguracaoRegrasNegocio.Fundo.DistribuidorPadrao:
                    if (this.dropAgenteDistribuidor.SelectedIndex > -1)
                    {
                        configuracaoCollection[i].ValorNumerico = Convert.ToInt32(this.dropAgenteDistribuidor.Value);
                    }
                    else if(String.IsNullOrEmpty(Convert.ToString(this.dropAgenteDistribuidor.Value)))
                    {
                        configuracaoCollection[i].ValorNumerico = null;
                    }
                    break;

                case ConfiguracaoRegrasNegocio.Fundo.PfeeCotasFundos:
                    configuracaoCollection[i].ValorTexto = (string)this.dropPfeeCotasFundos.Value;
                    break;

                case ConfiguracaoRegrasNegocio.RendaFixaSwap.ControleCustodia:
                    configuracaoCollection[i].ValorNumerico = Convert.ToInt32(this.dropCustodiaRF.Value);
                    break;

                case ConfiguracaoRegrasNegocio.RendaFixaSwap.AliquotaUnicaGrossup:
                    configuracaoCollection[i].ValorTexto = (string)this.dropAliquotaUnicaGrossup.Value;
                    break;

                #endregion

                #region Configurações Gerais
                case ConfiguracaoRegrasNegocio.Outras.IdAutomaticoPessoa:
                    configuracaoCollection[i].ValorTexto = (string)this.dropIdAutomaticoPessoa.Value;
                    break;

                case ConfiguracaoRegrasNegocio.Outras.PermiteDuplDocumentoPessoa:
                    configuracaoCollection[i].ValorTexto = (string)this.dropPermiteDuplDocumentoPessoa.Value;
                    break;

                case ConfiguracaoRegrasNegocio.Outras.PropagaAlterNomeCliente:
                    configuracaoCollection[i].ValorTexto = (string)this.dropPropagaAlterNomeCliente.Value;
                    break;

                case ConfiguracaoRegrasNegocio.Outras.EstendeConceitoPessoaAgente:
                    configuracaoCollection[i].ValorTexto = (string)this.dropEstendeConceitoPessoaAgente.Value;
                    break;

                case ConfiguracaoRegrasNegocio.Outras.PropagaAlterNomeAgente:
                    configuracaoCollection[i].ValorTexto = (string)this.dropPropagaAlterNomeAgente.Value;
                    break;

                case ConfiguracaoRegrasNegocio.Outras.ValidaQtdeResgateProcessamento:
                    configuracaoCollection[i].ValorTexto = (string)this.dropValidaQtdeResgateProcessamento.Value;
                    break;

                case ConfiguracaoRegrasNegocio.Outras.TipoVisualizacaoFundoResgateCotista:
                    configuracaoCollection[i].ValorTexto = (string)this.dropTipoVisualizacaoResgCotista.Value;
                    break;

                case ConfiguracaoRegrasNegocio.Outras.AbortaProcRFPorErroCadastraTitulo:
                    configuracaoCollection[i].ValorTexto = (string)this.dropAbortaProcRFPorErroCadastraTitulo.Value;
                    break;

                case ConfiguracaoRegrasNegocio.Outras.CalculaCofUsandoPLD0:
                    configuracaoCollection[i].ValorTexto = (string)this.dropCalculaCofUsandoPLD0.Value;
                    break;

                #endregion

                #region Outras Configs
                case ConfiguracaoRegrasNegocio.Outras.DataBase:
                    configuracaoCollection[i].ValorTexto = this.textDataBase.Date.ToString("dd/MM/yyyy");
                    break;
                case ConfiguracaoRegrasNegocio.Outras.MultiConta:
                    configuracaoCollection[i].ValorTexto = (string)this.chkMultiConta.Value;
                    break;
                case ConfiguracaoRegrasNegocio.Outras.AgenteLiquidacaoExclusivo:
                    configuracaoCollection[i].ValorTexto = (string)this.chkAgenteLiquidacao.Value;
                    break;
                case ConfiguracaoRegrasNegocio.Outras.CalculaContatil:
                    configuracaoCollection[i].ValorTexto = (string)this.chkCalculaContabil.Value;
                    break;
                case ConfiguracaoRegrasNegocio.Outras.PermissaoInternoAuto:
                    configuracaoCollection[i].ValorTexto = (string)this.chkPermissaoAuto.Value;
                    break;
                case ConfiguracaoRegrasNegocio.Outras.ResetDataInicio:
                    configuracaoCollection[i].ValorTexto = (string)this.chkResetDataInicio.Value;
                    break;
                case ConfiguracaoRegrasNegocio.Outras.LiberaDataLancamentoLiquidacao:
                    configuracaoCollection[i].ValorTexto = (string)this.chkLiberaDtLancamento.Value;
                    break;
                case ConfiguracaoRegrasNegocio.RendaFixaSwap.EmiteMsgSemCotacao:
                    configuracaoCollection[i].ValorTexto = (string)this.chkEmiteMsgSemCotacao.Value;
                    break;
                case ConfiguracaoRegrasNegocio.Outras.CalculaFatorIndice:
                    configuracaoCollection[i].ValorTexto = (string)this.chkCalculaFatorIndice.Value;
                    break;
                case ConfiguracaoRegrasNegocio.Outras.ProcessaIndicadorCarteira:
                    configuracaoCollection[i].ValorTexto = (string)this.chkProcessaIndicadorcarteira.Value;
                    break;
                case ConfiguracaoRegrasNegocio.Outras.SuitabilityRendaFixa:
                    configuracaoCollection[i].ValorTexto = (string)this.chkSuitabilityRendaFixa.Value;
                    break;             
                case ConfiguracaoRegrasNegocio.Outras.RetroagirCarteirasDependentes:
                    configuracaoCollection[i].ValorTexto = (string)this.chkRetroagirCarteirasDependentes.Value;
                    break;
                case ConfiguracaoRegrasNegocio.Outras.PermitirOperacoesRetroativas:
                    configuracaoCollection[i].ValorTexto = (string)this.chkPermitirOperacoesRetroativas.Value;
                    break;
                case ConfiguracaoRegrasNegocio.Outras.DescontarRendimentoCupom:
                    configuracaoCollection[i].ValorTexto = (string)this.dropDescontaRendimentoCupom.Value;                   
                    break; 
                #endregion
            }
        }

        // Salva todas as Configurações
        try {
            configuracaoCollection.Save();
        }
        catch (Exception ex) {
            throw new Exception(ex.Message);
        }
        string mensagem = "Alterações Salvas com Sucesso.";
        throw new Exception(mensagem);
    }

    /// <summary>
    /// Tratamento de erros:
    /// TextEdits não podem ser nulos nem conter porcentagens maiores que 100%            
    /// </summary>
    /// <exception cref="Exception">Se algum campo obrigatório não foi preenchido ou se algum
    /// textEdits contem porcentagens maiores que 100%
    /// </exception>
    private void TrataErros() {

        NumberFormatInfo formato = new NumberFormatInfo();
        formato.CurrencyDecimalSeparator = ".";
        //
        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { 
    	    this.txtCodigoBovespa});

        if (base.TestaObrigatorio(controles) != "") {
            throw new Exception("Campos com * são obrigatórios!");
        }
        #endregion

        string listaCodigos = textCodigosCC.Text;
        if (listaCodigos != "")
        {
            listaCodigos = listaCodigos.Replace(",", ";");
            string[] codigos = listaCodigos.Split(new Char[] { ';' });
            foreach (string codigo in codigos)
            {
                if (!Utilitario.IsInteger(codigo.Replace("M", "")))
                {
                    throw new Exception("Código não definido como número: " + codigo);
                }
            }
        }
        
    }
    
    #region Integrações - Combos

    /// <summary>
    /// Preenche o Combo de Integração Bolsa
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropIntegracaoBolsa_OnPreRender(object sender, EventArgs e) {
        int integracaoBolsa = ParametrosConfiguracaoSistema.Integracoes.IntegracaoBolsa;
        IntegracaoBolsa integraBolsa = (IntegracaoBolsa)integracaoBolsa;
        this.dropIntegracaoBolsa.SelectedIndex = (int)integraBolsa;
    }

    protected void dropTipoVisualizacaoResgCotista_OnPreRender(object sender, EventArgs e)
    {
        string tipoVisualizacaoFundoResgateCotista = ParametrosConfiguracaoSistema.Outras.TipoVisualizacaoFundoResgateCotista;
        this.dropTipoVisualizacaoResgCotista.SelectedIndex = tipoVisualizacaoFundoResgateCotista.Equals("C") ? 0 : 1;
    }

    /// <summary>
    /// Preenche o Combo de Integração Renda Fixa
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropIntegracaoRendaFixa_OnPreRender(object sender, EventArgs e) {
        int integracaoRendaFixa = ParametrosConfiguracaoSistema.Integracoes.IntegracaoRendaFixa;
        IntegracaoRendaFixa integraRF = (IntegracaoRendaFixa)integracaoRendaFixa;
        this.dropIntegracaoRendaFixa.SelectedIndex = (int)integraRF;
    }

    /// <summary>
    /// Preenche o Combo de Integração BMF
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropIntegracaoBMF_OnPreRender(object sender, EventArgs e) {
        int integracaoBMF = ParametrosConfiguracaoSistema.Integracoes.IntegracaoBMF;
        IntegracaoBMF integraBMF = (IntegracaoBMF)integracaoBMF;
        //
        this.dropIntegracaoBMF.SelectedIndex = (int)integraBMF;
    }

    /// <summary>
    /// Preenche o Combo de Integração CC
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropIntegracaoCC_OnPreRender(object sender, EventArgs e) {
        int integracaoCC = ParametrosConfiguracaoSistema.Integracoes.IntegracaoCC;
        IntegracaoCC integraCC = (IntegracaoCC)integracaoCC;
        //
        this.dropIntegracaoCC.SelectedIndex = (int)integraCC;
    }

    /// <summary>
    /// Preenche o Combo galgo pl cota
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropGalgoPlCota_OnPreRender(object sender, EventArgs e)
    {
        int galgoPlCota = ParametrosConfiguracaoSistema.Integracoes.IntegraGalgoPlCota;
        IntegraGalgoPlCota integraGalgoPlCota = (IntegraGalgoPlCota)galgoPlCota;
        //
        this.dropGalgoPlCota.SelectedIndex = (int)integraGalgoPlCota;
    }

    #endregion

    #region Bolsa - Combos

    /// <summary>
    /// Preenche o Combo de Tipo Vizualização Bolsa
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropTipoVizualizacaoBolsa_OnPreRender(object sender, EventArgs e) {
        int tipoVisualização = ParametrosConfiguracaoSistema.Bolsa.TipoVisualizacao;
        VizualizacaoOperacaoBolsa tipo = (VizualizacaoOperacaoBolsa)tipoVisualização;
        //
        this.dropTipoVizualizacaoBolsa.SelectedIndex = (tipo == VizualizacaoOperacaoBolsa.Analitico) ? 0 : 1;
    }

    /// <summary>
    /// Preenche o Combo de Prioridade Casamento de Bolsa
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropBolsaPrioridadeCasamento_OnPreRender(object sender, EventArgs e) {
        int prioridadeCasamento = ParametrosConfiguracaoSistema.Bolsa.PrioridadeCasamentoVencimento;
        PrioridadeCasamentoVencimento prioridade = (PrioridadeCasamentoVencimento)prioridadeCasamento;
        //
        this.dropBolsaPrioridadeCasamento.SelectedIndex = (prioridade == PrioridadeCasamentoVencimento.ExercicioPrimeiro) ? 0 : 1;
    }

    /// <summary>
    /// Preenche o Combo de MTM Termo Vendido
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropBolsaMTMTermoVendido_OnPreRender(object sender, EventArgs e) {
        bool mtmVendido = ParametrosConfiguracaoSistema.Bolsa.MTMTermoVendido;
        //
        this.dropBolsaMTMTermoVendido.SelectedIndex = mtmVendido == true ? 0 : 1;
    }

    /// <summary>
    /// Preenche o Combo de Renovação Termo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropBolsaRenovacaoTermo_OnPreRender(object sender, EventArgs e) {
        bool renovacaoTermo = ParametrosConfiguracaoSistema.Bolsa.RenovacaoTermo;
        //
        this.dropBolsaRenovacaoTermo.SelectedIndex = renovacaoTermo == true ? 0 : 1;
    }

    /// <summary>
    /// Preenche o Combo de Consolidação Custo Médio
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropConsolidaCustoMedio_OnPreRender(object sender, EventArgs e) {
        bool consolidacaoCustoMedio = ParametrosConfiguracaoSistema.Bolsa.ConsolidacaoCustoMedio;
        //
        this.dropConsolidaCustoMedio.SelectedIndex = consolidacaoCustoMedio == true ? 0 : 1;
    }

    /// <summary>
    /// Preenche o ASPxCheckBox
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chkEmiteMsgSemCotacaoBolsa_OnPreRender(object sender, EventArgs e)
    {
        bool retornoEmiteMsgSemCotacao = ParametrosConfiguracaoSistema.Bolsa.EmiteMsgSemCotacaoBolsa;
        this.chkEmiteMsgSemCotacaoBolsa.SelectedIndex = retornoEmiteMsgSemCotacao ? 0 : 1;
    }

    /// <summary>
    /// Preenche o Combo de Isenção IR Ações
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropBolsaIsencaoIRAcoes_OnPreRender(object sender, EventArgs e)
    {
        bool isencaoIRAcoes = ParametrosConfiguracaoSistema.Bolsa.IsencaoIRAcoes;
        //
        this.dropBolsaIsencaoIRAcoes.SelectedIndex = isencaoIRAcoes == true ? 0 : 1;
    }
    
    /// <summary>
    /// Preenche o Combo de Compensa IR Daytrade
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropBolsaCompensaIRDaytrade_OnPreRender(object sender, EventArgs e) {
        bool compensaIRDayTrade = ParametrosConfiguracaoSistema.Bolsa.CompensaIRDayTrade;
        //
        this.dropBolsaCompensaIRDaytrade.SelectedIndex = compensaIRDayTrade == true ? 0 : 1;
    }

    /// <summary>
    /// Preenche o Combo de Entrada de Operações
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropEntradaOperacoes_OnPreRender(object sender, EventArgs e) {
        //
        int entradaOperacao = ParametrosConfiguracaoSistema.Bolsa.EntradaOperacoes;
        EntradaOperacao entrada = (EntradaOperacao)entradaOperacao;
        this.dropEntradaOperacoes.SelectedIndex = (entrada == EntradaOperacao.Compras) ? 0 : 1;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropCustodiaBTC_OnPreRender(object sender, EventArgs e)
    {
        //
        int entradaOperacao = ParametrosConfiguracaoSistema.Bolsa.CustodiaBTC;
        EntradaOperacao entrada = (EntradaOperacao)entradaOperacao;
        this.dropCustodiaBTC.SelectedIndex = (byte)entrada - 1;
    }
    #endregion

    #region BMF - Combos

    /// <summary>
    /// Preenche o Combo de Tipo Vizualização BMF
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropTipoVizualizacaoBMF_OnPreRender(object sender, EventArgs e) {
        int tipoVisualização = ParametrosConfiguracaoSistema.BMF.TipoVisualizacao;
        VizualizacaoOperacaoBMF tipo = (VizualizacaoOperacaoBMF)tipoVisualização;
        //
        this.dropTipoVizualizacaoBMF.SelectedIndex = (tipo == VizualizacaoOperacaoBMF.Analitico) ? 0 : 1;
    }

    /// <summary>
    /// Preenche o Combo de Tipo Cálculo Taxas BMF
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropTipoCalculoTaxasBMF_OnPreRender(object sender, EventArgs e) {
        int calculoTaxas = ParametrosConfiguracaoSistema.BMF.TipoCalculoTaxasBMF;
        TipoCalculoTaxasBMF tipo = (TipoCalculoTaxasBMF)calculoTaxas;
        //
        if (tipo == TipoCalculoTaxasBMF.Parametros) {
            this.dropTipoCalculoTaxasBMF.SelectedIndex = 0;
        }
        else if (tipo == TipoCalculoTaxasBMF.Direto) {
            this.dropTipoCalculoTaxasBMF.SelectedIndex = 1;
        }
        else if (tipo == TipoCalculoTaxasBMF.NaoCalcula) {
            this.dropTipoCalculoTaxasBMF.SelectedIndex = 2;
        }
        else if (tipo == TipoCalculoTaxasBMF.NaoCalculaPerm) {
            this.dropTipoCalculoTaxasBMF.SelectedIndex = 3;
        }
    }

    #endregion

    #region Carteira Gerais - Combos

    /// <summary>
    /// Preenche o Combo de Cota Inexistente
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropCotaInexistente_OnPreRender(object sender, EventArgs e) {
        //
        int trataCotaInexistente = ParametrosConfiguracaoSistema.Fundo.TratamentoCotaInexistente;
        TratamentoCotaInexistente trataCota = (TratamentoCotaInexistente)trataCotaInexistente;
        //                
        this.dropCotaInexistente.SelectedIndex = ((int)trataCota) - 1;
    }

    /// <summary>
    /// Preenche o Combo de ReCalcula Cota Fechamento
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropRecalculaCotaFechamento_OnPreRender(object sender, EventArgs e) {
        string recalculaCota = ParametrosConfiguracaoSistema.Fundo.RecalculaCota;
        this.dropRecalculaCotaFechamento.SelectedIndex = recalculaCota == "S" ? 0 : 1;
    }

    /// <summary>
    /// Preenche o Combo de Usa Cota Inicial da Posicao para o IR
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropUsaCotaInicialPosicaoIR_OnPreRender(object sender, EventArgs e)
    {
        string usaCotaInicialPosicaoIR = ParametrosConfiguracaoSistema.Fundo.UsaCotaInicialPosicaoIR;
        this.dropUsaCotaInicialPosicaoIR.SelectedIndex = usaCotaInicialPosicaoIR == "S" ? 0 : 1;
    }

    /// <summary>
    /// Preenche o Combo de Retorno Benchmark
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropRetornoBenchmark_OnPreRender(object sender, EventArgs e) {
        ASPxComboBox combo = sender as ASPxComboBox;
        //
        int retornoBenchmark = ParametrosConfiguracaoSistema.Fundo.RetornoBenchmark;
        //                
        this.dropRetornoBenchmark.SelectedIndex = retornoBenchmark - 1;
    }

    /// <summary>
    /// Preenche o Combo de Retorno FDIC ajustado
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropRetornoFDICAjustado_OnPreRender(object sender, EventArgs e)
    {
        string retornoFDICAjustado = ParametrosConfiguracaoSistema.Fundo.RetornoFDICAjustado;
        this.dropRetornoFDICAjustado.SelectedIndex = retornoFDICAjustado == "S" ? 0 : 1;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropAgenteDistribuidor_OnPreRender(object sender, EventArgs e)
    {
        ASPxComboBox combo = sender as ASPxComboBox;
        //
        if (ParametrosConfiguracaoSistema.Fundo.DistribuidorPadrao.HasValue)
        {
            int idAgente = ParametrosConfiguracaoSistema.Fundo.DistribuidorPadrao.Value;

            AgenteMercado agenteMercado = new AgenteMercado();
            agenteMercado.LoadByPrimaryKey(idAgente);

            this.dropAgenteDistribuidor.Value = agenteMercado.Nome;
        }
        //                
        
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropCustodiaRF_OnPreRender(object sender, EventArgs e)
    {
        int controleCustodia = ParametrosConfiguracaoSistema.RendaFixaSwap.ControleCustodia;
        TipoControleCustodia tipoControleCustodia = (TipoControleCustodia)controleCustodia;
        //                
        this.dropCustodiaRF.SelectedIndex = ((int)tipoControleCustodia) - 1;
    }

    /// <summary>
    /// Preenche o Combo de Aliquota Unica Grossup
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropAliquotaUnicaGrossup_OnPreRender(object sender, EventArgs e)
    {
        bool aliquotaUnicaGrossup = ParametrosConfiguracaoSistema.RendaFixaSwap.AliquotaUnicaGrossup;
        this.dropAliquotaUnicaGrossup.SelectedIndex = aliquotaUnicaGrossup ? 0 : 1;
    }

    /// <summary>
    /// Preenche o Combo PfeeCotasFundos
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropPfeeCotasFundos_OnPreRender(object sender, EventArgs e)
    {
        bool retornoPfeeCotasFundos = ParametrosConfiguracaoSistema.Fundo.PfeeCotaFundos;
        this.dropPfeeCotasFundos.SelectedIndex = retornoPfeeCotasFundos ? 0 : 1;
    }

    /// <summary>
    /// Preenche o Combo DescontarRendimentoCupom
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropDescontaRendimentoCupom_OnPreRender(object sender, EventArgs e)
    {
        bool retornoDescontaRendimentoCupom = ParametrosConfiguracaoSistema.Outras.DescontarRendimentoCupom;
        this.dropDescontaRendimentoCupom.SelectedIndex = retornoDescontaRendimentoCupom ? 0 : 1;
    }

    
    #endregion
    #region TextBoxs

    /// <summary>
    /// Carrega o TextBox de Codigo Bovespa
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void txtCodigoBovespa_OnPrerender(object sender, EventArgs e) {
        int codigoBovespa = ParametrosConfiguracaoSistema.Bolsa.CodigoAgenteDefault;
        this.txtCodigoBovespa.Text = Convert.ToString(codigoBovespa);
    }

    /// <summary>
    /// Carrega o TextBox de Integracao Codigos CC
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textCodigosCC_OnPrerender(object sender, EventArgs e)
    {
        this.textCodigosCC.Text = Convert.ToString(ParametrosConfiguracaoSistema.Integracoes.IntegracaoCodigosCC);
    }

    protected void textUsuarioIntegracao_OnPrerender(object sender, EventArgs e)
    {
        this.textUsuarioIntegracao.Text = Convert.ToString(ParametrosConfiguracaoSistema.Integracoes.UsuarioIntegracao);
    }

    protected void textSenhaIntegracao_OnPrerender(object sender, EventArgs e)
    {
        //this.textSenhaIntegracao.Text = Convert.ToString(ParametrosConfiguracaoSistema.Integracoes.SenhaIntegracao);
    }

    /// <summary>
    /// Carrega o TextBox de Schema Sinacor
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textSchemaSinacor_OnPrerender(object sender, EventArgs e)
    {
        this.textSchemaSinacor.Text = Convert.ToString(ParametrosConfiguracaoSistema.Integracoes.SchemaSinacor);
    }

    /// <summary>
    /// Carrega o TextBox de Schema BTC
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textSchemaBTC_OnPrerender(object sender, EventArgs e)
    {
        this.textSchemaBTC.Text = Convert.ToString(ParametrosConfiguracaoSistema.Integracoes.SchemaBTC);
    }

    protected void textNomeInstituicao_OnPrerender(object sender, EventArgs e)
    {
        this.textNomeInstituicao.Text = Convert.ToString(ParametrosConfiguracaoSistema.Integracoes.NomeInstituicao);
    }
    
    protected void textCodInstituicao_OnPrerender(object sender, EventArgs e)
    {
        this.textCodInstituicao.Text = Convert.ToString(ParametrosConfiguracaoSistema.Integracoes.CodInstituicao);
    }
    
    protected void textResponsavelInstituicao_OnPrerender(object sender, EventArgs e)
    {
        this.textResponsavelInstituicao.Text = Convert.ToString(ParametrosConfiguracaoSistema.Integracoes.ResponsavelInstituicao);
    }
    
    protected void textTelefoneInstituicao_OnPrerender(object sender, EventArgs e)
    {
        this.textTelefoneInstituicao.Text = Convert.ToString(ParametrosConfiguracaoSistema.Integracoes.TelefoneInstituicao);
    }
    
    protected void textEmailInstituicao_OnPrerender(object sender, EventArgs e)
    {
        this.textEmailInstituicao.Text = Convert.ToString(ParametrosConfiguracaoSistema.Integracoes.EmailInstituicao);
    }
    
    
    
    
    

    #endregion

    #region Outras Configs
    
    /// <summary>
    /// Preenche o ASPxCheckBox
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chkMultiConta_OnPreRender(object sender, EventArgs e) {
        bool retornoMultiConta = ParametrosConfiguracaoSistema.Outras.MultiConta;
        this.chkMultiConta.SelectedIndex = retornoMultiConta ? 0 : 1;
    }
   
    /// <summary>
    /// Preenche o ASPxCheckBox
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chkAgenteLiquidacao_OnPreRender(object sender, EventArgs e) {        
        bool retornoAgenteLiquidacao = ParametrosConfiguracaoSistema.Outras.AgenteLiquidacaoExclusivo;
        this.chkAgenteLiquidacao.SelectedIndex = retornoAgenteLiquidacao ? 0 : 1;
    }

    /// <summary>
    /// Preenche o ASPxCheckBox
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chkCalculaContabil_OnPreRender(object sender, EventArgs e) {
        bool retornoCalculaContabil = ParametrosConfiguracaoSistema.Outras.CalculaContatil;
        this.chkCalculaContabil.SelectedIndex = retornoCalculaContabil ? 0 : 1;               
    }

    /// <summary>
    /// Preenche o ASPxCheckBox
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chkPermissaoAuto_OnPreRender(object sender, EventArgs e) {
        bool retornoPermissaoAuto = ParametrosConfiguracaoSistema.Outras.PermissaoInternoAuto;
        this.chkPermissaoAuto.SelectedIndex = retornoPermissaoAuto ? 0 : 1;
    }

    /// <summary>
    /// Preenche o ASPxCheckBox
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chkLiberaDtLancamento_OnPreRender(object sender, EventArgs e) {
        bool retornoDtLancamento = ParametrosConfiguracaoSistema.Outras.LiberaDataLancamentoLiquidacao;
        this.chkLiberaDtLancamento.SelectedIndex = retornoDtLancamento ? 0 : 1;                
    }

    /// <summary>
    /// Preenche o ASPxCheckBox
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chkResetDataInicio_OnPreRender(object sender, EventArgs e) {
        bool retornoResetDataInicio = ParametrosConfiguracaoSistema.Outras.ResetDataInicio;
        this.chkResetDataInicio.SelectedIndex = retornoResetDataInicio ? 0 : 1;
    }

    /// <summary>
    /// Preenche o ASPxCheckBox
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chkEmiteMsgSemCotacao_OnPreRender(object sender, EventArgs e) {
        bool retornoEmiteMsgSemCotacao = ParametrosConfiguracaoSistema.RendaFixaSwap.EmiteMsgSemCotacao;
        this.chkEmiteMsgSemCotacao.SelectedIndex = retornoEmiteMsgSemCotacao ? 0 : 1;
    }

    /// <summary>
    /// Preenche o ASPxCheckBox
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chkCalculaFatorIndice_OnPreRender(object sender, EventArgs e)
    {
        bool retornoCalculaFatorIndice = ParametrosConfiguracaoSistema.Outras.CalculaFatorIndice;
        this.chkCalculaFatorIndice.SelectedIndex = retornoCalculaFatorIndice ? 0 : 1;
    }

    /// <summary>
    /// Preenche o ASPxCheckBox
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chkProcessaIndicadorcarteira_OnPreRender(object sender, EventArgs e)
    {
        bool retornoProcessaIndicadorcarteira = ParametrosConfiguracaoSistema.Outras.ProcessaIndicadorCarteira;
        this.chkProcessaIndicadorcarteira.SelectedIndex = retornoProcessaIndicadorcarteira ? 0 : 1;
    }

    protected void chkSuitabilityRendaFixa_OnPreRender(object sender, EventArgs e)
    {
        bool retornoSuitabilityRendaFixa = ParametrosConfiguracaoSistema.Outras.SuitabilityRendaFixa;
        this.chkSuitabilityRendaFixa.SelectedIndex = retornoSuitabilityRendaFixa ? 0 : 1;
    }

    /// <summary>
    /// Preenche o ASPxCheckBox
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chkRetroagirCarteirasDependentes_OnPreRender(object sender, EventArgs e)
    {
        bool retornoRetroagirCarteirasDependentes = ParametrosConfiguracaoSistema.Outras.RetroagirCarteirasDependentes;
        this.chkRetroagirCarteirasDependentes.SelectedIndex = retornoRetroagirCarteirasDependentes ? 0 : 1;
    }

    /// <summary>
    /// Preenche o ASPxCheckBox
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chkPermitirOperacoesRetroativas_OnPreRender(object sender, EventArgs e)
    {
        bool retornoPermitirOperacoesRetroativas = ParametrosConfiguracaoSistema.Outras.PermitirOperacoesRetroativas;
        this.chkPermitirOperacoesRetroativas.SelectedIndex = retornoPermitirOperacoesRetroativas ? 0 : 1;
    }

    /// <summary>
    /// Preenche o Data Base
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void textDataBase_OnPreRender(object sender, EventArgs e)
    {
        this.textDataBase.Date = ParametrosConfiguracaoSistema.Outras.DataBase;
    }

    #endregion

    #region Parametros Gerais
    protected void dropIdAutomaticoPessoa_OnPreRender(object sender, EventArgs e)
    {
        string IdAutomaticoPessoa = ParametrosConfiguracaoSistema.Outras.IdAutomaticoPessoa;
        this.dropIdAutomaticoPessoa.SelectedIndex = IdAutomaticoPessoa == "S" ? 0 : 1;
    }

    protected void dropPermiteDuplDocumentoPessoa_OnPreRender(object sender, EventArgs e)
    {
        string PermiteDuplDocumentoPessoa = ParametrosConfiguracaoSistema.Outras.PermiteDuplDocumentoPessoa;
        this.dropPermiteDuplDocumentoPessoa.SelectedIndex = PermiteDuplDocumentoPessoa == "S" ? 0 : 1;
    }

    protected void dropPropagaAlterNomeCliente_OnPreRender(object sender, EventArgs e)
    {
        string PropagaAlterNomeCliente = ParametrosConfiguracaoSistema.Outras.PropagaAlterNomeCliente;
        this.dropPropagaAlterNomeCliente.SelectedIndex = PropagaAlterNomeCliente == "S" ? 0 : 1;
    }

    protected void dropEstendeConceitoPessoaAgente_OnPreRender(object sender, EventArgs e)
    {
        string EstendeConceitoPessoaAgente = ParametrosConfiguracaoSistema.Outras.EstendeConceitoPessoaAgente;
        this.dropEstendeConceitoPessoaAgente.SelectedIndex = EstendeConceitoPessoaAgente == "S" ? 0 : 1;
    }

    protected void dropPropagaAlterNomeAgente_OnPreRender(object sender, EventArgs e)
    {
        string PropagaAlterNomeAgente = ParametrosConfiguracaoSistema.Outras.PropagaAlterNomeAgente;
        this.dropPropagaAlterNomeAgente.SelectedIndex = PropagaAlterNomeAgente == "S" ? 0 : 1;
    }

    protected void dropValidaQtdeResgateProcessamento_OnPreRender(object sender, EventArgs e)
    {
        string ValidaQtdeResgateProcessamento = ParametrosConfiguracaoSistema.Outras.ValidaQtdeResgateProcessamento;
        this.dropValidaQtdeResgateProcessamento.SelectedIndex = ValidaQtdeResgateProcessamento == "S" ? 0 : 1;
    }

    protected void dropAbortaProcRFPorErroCadastralTitulo_OnPreRender(object sender, EventArgs e)
    {
        string abortaProcRFPorErroCadastralTitulo = ParametrosConfiguracaoSistema.Outras.AbortaProcRFPorErroCadastraTitulo;
        this.dropAbortaProcRFPorErroCadastraTitulo.SelectedIndex = abortaProcRFPorErroCadastralTitulo == "S" ? 0 : 1;
    }

    protected void dropCalculaCofUsandoPLD0_OnPreRender(object sender, EventArgs e)
    {
        string calculaCofUsandoPLD0 = ParametrosConfiguracaoSistema.Outras.CalculaCofUsandoPLD0;
        this.dropCalculaCofUsandoPLD0.SelectedIndex = calculaCofUsandoPLD0 == "S" ? 0 : 1;
    }
    #endregion
}