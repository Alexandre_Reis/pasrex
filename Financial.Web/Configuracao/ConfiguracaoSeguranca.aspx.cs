﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Text;
using EntitySpaces.Interfaces;
using Financial.Web.Common;
using DevExpress.Web;
using Financial.Util;
using Financial.ContaCorrente.Enums;
using Financial.Bolsa.Enums;
using Financial.BMF.Enums;
using Financial.Fundo.Enums;
using Financial.Util.ConfiguracaoSistema;
using Financial.Util.Enums;
using System.Globalization;

public partial class ConfiguracaoSeguranca : BasePage 
{    
    new protected void Page_Load(object sender, EventArgs e)
    {
        pnlSeguranca.HeaderText = String.Empty;

    }

    /// <summary>
    /// Salva os Parametros de Configuração no Banco
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>   
    protected void btnSalvar_Click(object sender, EventArgs e) {
        this.TrataErros();

        ConfiguracaoCollection configuracaoCollection = new ConfiguracaoCollection();
        configuracaoCollection.Query.Where(configuracaoCollection.Query.Id >= 2000);
        configuracaoCollection.Query.Load();
        //
        for (int i = 0; i < configuracaoCollection.Count; i++) {
            int id = configuracaoCollection[i].Id.Value;
            //                   
            switch (id) {
                
                #region Parametros Seguranca
                case ConfiguracaoRegrasNegocio.Seguranca.TamanhoMinimoSenha:
                    configuracaoCollection[i].ValorNumerico = Convert.ToDecimal(this.txtTamanhoMinimoSenha.Text);
                    break;
                case ConfiguracaoRegrasNegocio.Seguranca.TempoExpiracaoSenha:
                    configuracaoCollection[i].ValorNumerico = Convert.ToDecimal(this.txtTempoExpiracaoSenha.Text);
                    break;
                case ConfiguracaoRegrasNegocio.Seguranca.TentativasBloqueioSenha:
                    configuracaoCollection[i].ValorNumerico = Convert.ToDecimal(this.txtTentativasBloqueioSenha.Text);
                    break;
                case ConfiguracaoRegrasNegocio.Seguranca.JanelaTempoResetSenha:
                    configuracaoCollection[i].ValorNumerico = Convert.ToDecimal(this.txtJanelaTempoResetSenha.Text);
                    break;
                case ConfiguracaoRegrasNegocio.Seguranca.HistoricoSenhas:
                    configuracaoCollection[i].ValorNumerico = Convert.ToDecimal(this.txtHistoricoSenhas.Text);
                    break;
                case ConfiguracaoRegrasNegocio.Seguranca.MinimoCaracterEspecialSenha:
                    configuracaoCollection[i].ValorNumerico = Convert.ToDecimal(this.txtMinimoCaracterEspecialSenha.Text);
                    break;
                case ConfiguracaoRegrasNegocio.Seguranca.MinimoNumeroSenha:
                    configuracaoCollection[i].ValorNumerico = Convert.ToDecimal(this.txtMinimoNumeroSenha.Text);
                    break;
                case ConfiguracaoRegrasNegocio.Seguranca.LetrasMaiusculas:
                    configuracaoCollection[i].ValorNumerico = Convert.ToDecimal(this.txtLetrasMaiusculas.Text);
                    break;
                case ConfiguracaoRegrasNegocio.Seguranca.AcessoSimultaneo:
                    configuracaoCollection[i].ValorTexto = Convert.ToString(this.dropAcessoSimultaneo.SelectedItem.Value);
                    break; 
                #endregion                                               
            }
        }

        // Salva todas as Configurações
        try {
            configuracaoCollection.Save();
        }
        catch (Exception ex) {
            throw new Exception(ex.Message);
        }
        string mensagem = "Alterações Salvas com Sucesso.";
        throw new Exception(mensagem);
    }

    /// <summary>
    /// Tratamento para Campos Obrigatórios
    /// </summary>
    /// <exception cref="Exception"></exception>
    private void TrataErros() {

        #region Campos obrigatórios
        List<Control> controles = new List<Control>(new Control[] { 
            this.txtTamanhoMinimoSenha, this.txtTempoExpiracaoSenha, this.txtTentativasBloqueioSenha, 
            this.txtHistoricoSenhas, this.txtMinimoCaracterEspecialSenha, this.txtMinimoNumeroSenha,
            this.txtJanelaTempoResetSenha
        });

        if (base.TestaObrigatorio(controles) != "") {
            throw new Exception("Campos com * são obrigatórios!");
        }
        #endregion

        bool flag = false;

        // Testa Numeros Negativos
        if ( Convert.ToInt32(txtTamanhoMinimoSenha.Text) < 0) {
            this.txtTamanhoMinimoSenha.Focus();
            flag = true;
        }
        else if ( Convert.ToInt32(txtTempoExpiracaoSenha.Text) < 0) {
            this.txtTempoExpiracaoSenha.Focus();
            flag = true;
        }
        else if ( Convert.ToInt32(txtTentativasBloqueioSenha.Text) < 0) {
            this.txtTentativasBloqueioSenha.Focus();
            flag = true;
        }
        else if ( Convert.ToInt32(txtJanelaTempoResetSenha.Text) < 0) {
            this.txtJanelaTempoResetSenha.Focus();
            flag = true;
        }
        else if ( Convert.ToInt32(txtHistoricoSenhas.Text) < 0) {
            this.txtHistoricoSenhas.Focus();
            flag = true;
        }
        else if ( Convert.ToInt32(txtMinimoCaracterEspecialSenha.Text) < 0) {
            this.txtMinimoCaracterEspecialSenha.Focus();
            flag = true;
        }
        else if ( Convert.ToInt32(txtMinimoNumeroSenha.Text) < 0) {
            this.txtMinimoNumeroSenha.Focus();
            flag = true;
        }
        else if ( Convert.ToInt32(txtLetrasMaiusculas.Text) < 0) {
            this.txtLetrasMaiusculas.Focus();
            flag = true;
        }

        if (flag) {
            throw new Exception("Número Negativo não é permitido");
        }
    }
           
    #region TextBoxs
    
    /// <summary>
    /// Carrega o TextBox de Tamanho Minimo Senha
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void txtTamanhoMinimoSenha_OnPrerender(object sender, EventArgs e) {
        int codigo = ParametrosConfiguracaoSistema.Seguranca.TamanhoMinimoSenha;
        this.txtTamanhoMinimoSenha.Text = Convert.ToString(codigo);
    }

    /// <summary>
    /// Carrega o TextBox de Tempo Expiracao Senha
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void txtTempoExpiracaoSenha_OnPrerender(object sender, EventArgs e) {
        int codigo = ParametrosConfiguracaoSistema.Seguranca.TempoExpiracaoSenha;
        this.txtTempoExpiracaoSenha.Text = Convert.ToString(codigo);
    }

    /// <summary>
    /// Carrega o TextBox de Tentativas Bloqueio Senha
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void txtTentativasBloqueioSenha_OnPrerender(object sender, EventArgs e) {
        int codigo = ParametrosConfiguracaoSistema.Seguranca.TentativasBloqueioSenha;
        this.txtTentativasBloqueioSenha.Text = Convert.ToString(codigo);
    }

    /// <summary>
    /// Carrega o TextBox de Historico Senhas
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void txtHistoricoSenhas_OnPrerender(object sender, EventArgs e) {
        int codigo = ParametrosConfiguracaoSistema.Seguranca.HistoricoSenhas;
        this.txtHistoricoSenhas.Text = Convert.ToString(codigo);
    }

    /// <summary>
    /// Carrega o TextBox de Minimo Caracter Especial de Senha
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void txtMinimoCaracterEspecialSenha_OnPrerender(object sender, EventArgs e) {
        int codigo = ParametrosConfiguracaoSistema.Seguranca.MinimoCaracterEspecialSenha;
        this.txtMinimoCaracterEspecialSenha.Text = Convert.ToString(codigo);
    }

    /// <summary>
    /// Carrega o TextBox de Minimo Caracteres Númericos na Senha
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void txtMinimoNumeroSenha_OnPrerender(object sender, EventArgs e) {
        int codigo = ParametrosConfiguracaoSistema.Seguranca.MinimoNumeroSenha;
        this.txtMinimoNumeroSenha.Text = Convert.ToString(codigo);
    }

    /// <summary>
    /// Carrega o TextBox de Letras Maiusculas
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void txtLetrasMaiusculas_OnPrerender(object sender, EventArgs e) {
        int codigo = ParametrosConfiguracaoSistema.Seguranca.LetrasMaiusculas;
        this.txtLetrasMaiusculas.Text = Convert.ToString(codigo);
    }

    /// <summary>
    /// Carrega o TextBox de Janela Tempo Reset Senha
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void txtJanelaTempoResetSenha_OnPrerender(object sender, EventArgs e) {
        int codigo = ParametrosConfiguracaoSistema.Seguranca.JanelaTempoResetSenha;
        this.txtJanelaTempoResetSenha.Text = Convert.ToString(codigo);
    }
    #endregion

    /// <summary>
    /// Preenche o Combo de Integração Bolsa
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dropAcessoSimultaneo_OnPreRender(object sender, EventArgs e)
    {
        bool acessoSimultaneo = ParametrosConfiguracaoSistema.Seguranca.AcessoSimultaneo;
        //
        this.dropAcessoSimultaneo.SelectedIndex = acessoSimultaneo == true ? 0 : 1;
    }
}