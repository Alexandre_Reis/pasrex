﻿// The following code was generated by Microsoft Visual Studio 2005.
// The test owner should check each test for validity.
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Text;
using System.Collections.Generic;
using System.Configuration;
using Financial.Interfaces.Import.BMF;
namespace Test.Financial.Interfaces {
    /// <summary>
    ///This is a test class for Financial.BMF.Interfaces.RComiesp and is intended
    ///to contain all Financial.BMF.Interfaces.RComiesp Unit Tests
    ///</summary>
    [TestClass()]
    public class RComiespTest {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }
        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext) {
            EntitySpaces.Interfaces.esProviderFactory.Factory = new EntitySpaces.LoaderMT.esDataProviderFactory();
        }

        //
        //Use ClassCleanup to run code after all tests in a class have run
        //
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for ProcessaRComiesp (string, DateTime)
        ///</summary>
        [TestMethod()]
        public void ProcessaRComiespTest() {
            RComiesp target = new RComiesp();

            string diretorioBase = ConfigurationManager.AppSettings["DiretorioBaseTeste"].ToString();
            StringBuilder nomeArquivo = new StringBuilder();
            nomeArquivo.Append(diretorioBase).Append("RCOMIESP.txt");
            DateTime data = new DateTime(2007, 01, 19);
            RComiespCollection actual = target.ProcessaRComiesp(nomeArquivo.ToString(), data);

            List<RComiesp> listaEsperada = new List<RComiesp>();

            #region Registro 0
            RComiesp rcomiesp1 = new RComiesp();
            rcomiesp1.CodigoAgenteCorretora = 000070;
            rcomiesp1.CodigoCliente = 000010;
            rcomiesp1.NumeroNegocio = 00057;
            rcomiesp1.CodigoMercadoria = "DOL";
            rcomiesp1.TipoMercado = 2;
            rcomiesp1.TipoOperacao = "V";
            rcomiesp1.Preco = 000000002139.000M;
            rcomiesp1.Quantidade = 00005;

            // Adiciona na lista
            listaEsperada.Add(rcomiesp1);
            #endregion
            //
            #region Registro 1
            RComiesp rcomiesp2 = new RComiesp();
            rcomiesp2.CodigoAgenteCorretora = 000070;
            rcomiesp2.CodigoCliente = 000010;
            rcomiesp2.NumeroNegocio = 00058;
            rcomiesp2.CodigoMercadoria = "DOL";
            rcomiesp2.TipoMercado = 2;
            rcomiesp2.TipoOperacao = "V";
            rcomiesp2.Preco = 000000002139.000M;
            rcomiesp2.Quantidade = 00045;

            // Adiciona na lista
            listaEsperada.Add(rcomiesp2);
            #endregion
            
            Assert.AreEqual(129, actual.CollectionRComiesp.Count, "Collection.Size errado");
            //            
            Assert.AreEqual(listaEsperada[0].CodigoAgenteCorretora, actual.CollectionRComiesp[1].CodigoAgenteCorretora, "CodigoAgenteCorretora rcomisep1");
            Assert.AreEqual(listaEsperada[0].CodigoCliente, actual.CollectionRComiesp[1].CodigoCliente, "CodigoCliente rcomisep1");
            Assert.AreEqual(listaEsperada[0].NumeroNegocio, actual.CollectionRComiesp[1].NumeroNegocio, "NumeroNegocio rcomisep1");
            Assert.AreEqual(listaEsperada[0].CodigoMercadoria, actual.CollectionRComiesp[1].CodigoMercadoria, "CodigoMercadoria rcomisep1");
            Assert.AreEqual(listaEsperada[0].TipoMercado, actual.CollectionRComiesp[1].TipoMercado, "TipoMercado rcomisep1");
            Assert.AreEqual(listaEsperada[0].TipoOperacao, actual.CollectionRComiesp[1].TipoOperacao, "TipoOperacao rcomisep1");
            Assert.AreEqual(listaEsperada[0].Preco, actual.CollectionRComiesp[1].Preco, "Preco rcomisep1");
            Assert.AreEqual(listaEsperada[0].Quantidade, actual.CollectionRComiesp[1].Quantidade, "Quantidade rcomisep1");
            //
            Assert.AreEqual(listaEsperada[1].CodigoAgenteCorretora, actual.CollectionRComiesp[2].CodigoAgenteCorretora, "CodigoAgenteCorretora rcomisep2");
            Assert.AreEqual(listaEsperada[1].CodigoCliente, actual.CollectionRComiesp[2].CodigoCliente, "CodigoCliente rcomisep2");
            Assert.AreEqual(listaEsperada[1].NumeroNegocio, actual.CollectionRComiesp[2].NumeroNegocio, "NumeroNegocio rcomisep2");
            Assert.AreEqual(listaEsperada[1].CodigoMercadoria, actual.CollectionRComiesp[2].CodigoMercadoria, "CodigoMercadoria rcomisep2");
            Assert.AreEqual(listaEsperada[1].TipoMercado, actual.CollectionRComiesp[2].TipoMercado, "TipoMercado rcomisep2");
            Assert.AreEqual(listaEsperada[1].TipoOperacao, actual.CollectionRComiesp[2].TipoOperacao, "TipoOperacao rcomisep2");
            Assert.AreEqual(listaEsperada[1].Preco, actual.CollectionRComiesp[2].Preco, "Preco rcomisep2");
            Assert.AreEqual(listaEsperada[1].Quantidade, actual.CollectionRComiesp[2].Quantidade, "Quantidade rcomisep2");
        }

    }


}
