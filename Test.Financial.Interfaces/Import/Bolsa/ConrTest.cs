﻿// The following code was generated by Microsoft Visual Studio 2005.
// The test owner should check each test for validity.
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Text;
using System.Collections.Generic;
using Financial.Interfaces.Import.Bolsa;
using System.Configuration;
namespace Test.Financial.Interfaces {
    /// <summary>
    ///This is a test class for Financial.Bolsa.Interfaces.Conr and is intended
    ///to contain all Financial.Bolsa.Interfaces.Conr Unit Tests
    ///</summary>
    [TestClass()]
    public class ConrTest {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }
        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext) {
            EntitySpaces.Interfaces.esProviderFactory.Factory = new EntitySpaces.LoaderMT.esDataProviderFactory();
        }

        //
        //Use ClassCleanup to run code after all tests in a class have run
        //
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for ProcessaConr (string)
        ///</summary>
        [TestMethod()]
        public void ProcessaConrTest() {
            Conr target = new Conr();

            string diretorioBase = ConfigurationManager.AppSettings["DiretorioBaseTeste"].ToString();
            StringBuilder nomeArquivo = new StringBuilder();
            nomeArquivo.Append(diretorioBase).Append("CONR.txt");

            DateTime data = new DateTime(2005, 09, 28);

            ConrCollection actual = target.ProcessaConr(nomeArquivo.ToString(), data);

            // Conferir um registro do tipo00, 1 registro do tipo02
            List<Conr> listaEsperada = new List<Conr>();

            #region Tipo00
            Conr conr1 = new Conr();
            conr1.CodigoAgente = 0150;
            conr1.DataMovimento = new DateTime(2005, 09, 28);

            // Adiciona na lista
            listaEsperada.Add(conr1);
            #endregion
            //

            #region Tipo01
            Conr conr2 = new Conr();
            conr2.DataRegistro = new DateTime(2005, 09, 26);
            conr2.DataVencimento = new DateTime(2005, 10, 26);
            conr2.CodigoCliente = 8000005;
            conr2.NumeroContrato = 000154101;
            conr2.NumeroNegocio = 0002010;
            conr2.Quantidade = 000000348880000;
            conr2.Pu = 00000000010.21M;
            conr2.Valor = 00003562064.80M;
            conr2.Tipo = 'C';
            conr2.CdAtivoBolsa = "BRTO4T";
            conr2.TipoContrato = 00001;
            conr2.IndicadorCorrecaoContrato = 09000;

            // Adiciona na lista
            listaEsperada.Add(conr2);
            #endregion

            Assert.AreEqual(1451, actual.CollectionConr.Count, "Collection.Size errado");
            //            
            Assert.AreEqual(conr1.CodigoAgente, actual.CollectionConr[0].CodigoAgente, "CodigoAgente conr1");
            Assert.AreEqual(conr1.DataMovimento, actual.CollectionConr[0].DataMovimento, "DataMovimento conr1");

            //
            Assert.AreEqual(conr2.DataRegistro, actual.CollectionConr[1].DataRegistro, "DataRegistro conr2");
            Assert.AreEqual(conr2.DataVencimento, actual.CollectionConr[1].DataVencimento, "DataVencimento conr2");
            Assert.AreEqual(conr2.CodigoCliente, actual.CollectionConr[1].CodigoCliente, "CodigoCliente conr2");
            Assert.AreEqual(conr2.NumeroContrato, actual.CollectionConr[1].NumeroContrato, "NumeroContrato conr2");
            Assert.AreEqual(conr2.NumeroNegocio, actual.CollectionConr[1].NumeroNegocio, "NumeroNegocio conr2");
            Assert.AreEqual(conr2.Quantidade, actual.CollectionConr[1].Quantidade, "Quantidade conr2");
            Assert.AreEqual(conr2.Pu, actual.CollectionConr[1].Pu, "Pu conr2");
            Assert.AreEqual(conr2.Valor, actual.CollectionConr[1].Valor, "Valor conr2");
            Assert.AreEqual(conr2.Tipo, actual.CollectionConr[1].Tipo, "Tipo conr2");
            Assert.AreEqual(conr2.CdAtivoBolsa, actual.CollectionConr[1].CdAtivoBolsa, "CdAtivoBolsa conr2");
            Assert.AreEqual(conr2.TipoContrato, actual.CollectionConr[1].TipoContrato, "TipoContrato conr2");
            Assert.AreEqual(conr2.IndicadorCorrecaoContrato, actual.CollectionConr[1].IndicadorCorrecaoContrato, "IndicadorCorrecaoContrato conr2");
        }


        /// <summary>
        ///A test for GeraArquivoConr (string, string, List&lt;int&gt;)
        ///</summary>
        [TestMethod()]
        public void GeraArquivoConrTest() {
            Conr target = new Conr();

            string nomeArquivoCompletoConr = @"S:\backup1\Especficacao\GeraArquivos\CONR.txt";
            string diretorioSaidaArquivo = @"S:\backup1\Especficacao\GeraArquivos";

            List<int> listaIdCliente = new List<int>(new int[] { 1, 2, 8000005, 0017147, 8012198 });

            bool actual = target.GeraArquivoConr(nomeArquivoCompletoConr, diretorioSaidaArquivo, listaIdCliente);

            Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }


}
