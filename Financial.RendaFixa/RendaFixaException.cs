﻿using System;

namespace Financial.RendaFixa.Exceptions {
    /// <summary>
    /// Classe base de Exceção do componente de RendaFixa
    /// </summary>
    public class RendaFixaException : Exception {
        /// <summary>
        ///  Construtor
        /// </summary>
        public RendaFixaException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public RendaFixaException(string mensagem) : base(mensagem) { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        /// <param name="inner"></param>
        public RendaFixaException(string mensagem, Exception inner) : base(mensagem, inner) { }
                
    }

    /// <summary>
    /// Exceção de SaldoInsuficienteException
    /// </summary>
    public class SaldoInsuficienteException : RendaFixaException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public SaldoInsuficienteException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public SaldoInsuficienteException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção de PosicaoInexistenteException
    /// </summary>
    public class PosicaoInexistenteException : RendaFixaException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public PosicaoInexistenteException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public PosicaoInexistenteException(string mensagem) : base(mensagem) { }
    }

    /// <summary>
    /// Exceção de CotacaoMTMInexistenteException
    /// </summary>
    public class CotacaoMTMInexistenteException : RendaFixaException
    {
        /// <summary>
        ///  Construtor
        /// </summary>
        public CotacaoMTMInexistenteException() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensagem"></param>
        public CotacaoMTMInexistenteException(string mensagem) : base(mensagem) { }
    }
}


