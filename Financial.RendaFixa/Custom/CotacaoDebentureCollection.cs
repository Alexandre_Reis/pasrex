using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

namespace Financial.RendaFixa {
    public partial class CotacaoDebentureCollection : esCotacaoDebentureCollection {
        private static readonly ILog log = LogManager.GetLogger(typeof(CotacaoDebentureCollection));

        /// <summary>
        ///  Deleta tudo da CotacaoDebenture de uma determinada data
        /// </summary>
        /// <param name="data"></param>
        public void DeletaCotacaoDebenture(DateTime data) {

            this.QueryReset();
            this.Query
              .Select(this.query.Data, this.query.CodigoPapel)
              .Where(this.query.Data == data);

            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }
    }
}