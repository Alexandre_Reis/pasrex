﻿/*
===============================================================================
                     EntitySpaces(TM) by EntitySpaces, LLC
                 A New 2.0 Architecture for the .NET Framework
                          http://www.entityspaces.net
===============================================================================
                       EntitySpaces Version # 2007.0.0528.0
                       MyGeneration Version # 1.2.0.7
                           5/6/2007 15:07:49
-------------------------------------------------------------------------------
*/


using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.RendaFixa {
    public partial class TituloRendaFixaCollection : esTituloRendaFixaCollection {

        [Obsolete("Teste")]
        public void InserePosicaoRendaFixaHistorico(TituloRendaFixaCollection t) {
            TituloRendaFixaCollection aux = new TituloRendaFixaCollection();

            for (int i = 0; i < t.Count; i++) {

                TituloRendaFixa t1 = new TituloRendaFixa();

                //foreach (esColumnMetadata col in t.es.Meta.Columns) {
                //    t1.SetColumn(col.Name, t[i].GetColumn(col.Name));
                //}

                t1.IdTitulo = 501; // prrenche id titulo
                //
                //t1.AcceptChanges();

                t1.IdPapel = t[i].IdPapel;
                t1.IdIndice = t[i].IdIndice;
                t1.IdEmissor = t[i].IdEmissor;
                t1.Descricao = t[i].Descricao;
                t1.Taxa = t[i].Taxa;
                t1.Percentual = t[i].Percentual;
                t1.DataEmissao = t[i].DataEmissao;
                t1.DataVencimento = t[i].DataVencimento;
                t1.ValorNominal = t[i].ValorNominal;
                t1.CodigoCustodia = t[i].CodigoCustodia;
                t1.PUNominal = t[i].PUNominal;
                t1.DescricaoCompleta = t[i].DescricaoCompleta;
                t1.IsentoIR = t[i].IsentoIR;
                t1.IsentoIOF = t[i].IsentoIOF;
                t1.IdMoeda = t[i].IdMoeda;
                t1.IdEstrategia = t[i].IdEstrategia;
                //
                this.AttachEntity(t1);
            }

            this.Save();
        }

        /// <summary>
        /// Insere um Titulo Renda Fixa Desligando o campo Identity idTitulo
        /// </summary>
        /// <param name="t"></param>
        public void InsereTituloRendaFixa(TituloRendaFixaCollection t, bool IDsEspecificos) {

            for (int i = 0; i < t.Count; i++) {

                TituloRendaFixa tituloRendaFixa = t[i];

                #region Monta Consulta                                
                esParameters esParams = new esParameters();
                
                StringBuilder sqlBuilder = new StringBuilder();
                if (IDsEspecificos)
                    sqlBuilder.Append("SET IDENTITY_INSERT TituloRendaFixa ON ");
                sqlBuilder.Append("INSERT INTO TituloRendaFixa (");
                
                StringBuilder sqlBuilderValues = new StringBuilder();                
                sqlBuilderValues.Append(") VALUES ( ");

                foreach (esColumnMetadata colTitulo in tituloRendaFixa.es.Meta.Columns)
                {
                    if (string.IsNullOrEmpty(tituloRendaFixa.GetColumn(colTitulo.Name).ToString()) && colTitulo.HasDefault)
                        continue;

                    if (colTitulo.Name.Equals(TituloRendaFixaMetadata.ColumnNames.IdTitulo))
                    {
                        if (string.IsNullOrEmpty(tituloRendaFixa.GetColumn(colTitulo.Name).ToString()))
                            continue;
                    }

                    esParams.Add(colTitulo.Name, tituloRendaFixa.GetColumn(colTitulo.Name));
                
                    sqlBuilder.Append(colTitulo.Name);
                    sqlBuilder.Append(",");
                    
                    sqlBuilderValues.Append("@" + colTitulo.Name);
                    sqlBuilderValues.Append(",");
                }

                //Remove última virgula
                sqlBuilder.Remove(sqlBuilder.Length - 1, 1);
                sqlBuilderValues.Remove(sqlBuilderValues.Length - 1, 1);

                //Concatena as strings
                sqlBuilder.Append(sqlBuilderValues.ToString());

                //
                sqlBuilder.Append(")");
                if (IDsEspecificos)
                    sqlBuilder.Append("SET IDENTITY_INSERT TituloRendaFixa OFF ");

                //Executa
                esUtility u = new esUtility();
                u.ExecuteNonQuery(esQueryType.Text, sqlBuilder.ToString(), esParams);
                #endregion
            }
        }
    }
}