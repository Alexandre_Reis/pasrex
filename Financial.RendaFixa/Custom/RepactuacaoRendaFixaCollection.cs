/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 02/12/2014 19:24:24
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.RendaFixa
{
	public partial class RepactuacaoRendaFixaCollection : esRepactuacaoRendaFixaCollection
	{
        public bool BuscaRepactuacao(DateTime data)
        {
            this.QueryReset();
            this.Query
                 .Select()
                 .Where(this.Query.DataEvento.Equal(data));

            this.Query.Load();

            return this.HasData;
        }
	}
}
