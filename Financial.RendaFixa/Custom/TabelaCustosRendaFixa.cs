﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Util;
using Financial.Investidor;
using Financial.ContaCorrente;
using Financial.ContaCorrente.Enums;
using Financial.RendaFixa.Enums;
using Financial.Common.Enums;
using System.Collections;

namespace Financial.RendaFixa
{
    public partial class TabelaCustosRendaFixa : esTabelaCustosRendaFixa
    {
        public class TaxaCustodiaAnual
        {
            public int idCliente;
            public decimal valor;
        }

        public class TaxaCustodia
        {
            public int idCliente;
            public decimal valor;
        }

        public List<TaxaCustodia> RetornaLiquidacaoTaxaCustodia(DateTime data)
        {
            //Ignorar lancamentos semestrais
            DateTime primeiroDiaMesPrimeiroSemestre = Calendario.RetornaPrimeiroDiaUtilMes(new DateTime(data.Year, 1, 1));
            DateTime primeiroDiaMesSegundoSemestre = Calendario.RetornaPrimeiroDiaUtilMes(new DateTime(data.Year, 7, 1));
            
            List<TaxaCustodia> lista = new List<TaxaCustodia>();

            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
            liquidacaoCollection.Query.Select(liquidacaoCollection.Query.IdCliente,
                                                             liquidacaoCollection.Query.Valor.Sum());
            liquidacaoCollection.Query.Where(
                liquidacaoCollection.Query.DataLancamento.Equal(data),
                liquidacaoCollection.Query.DataLancamento.NotEqual(primeiroDiaMesPrimeiroSemestre),
                liquidacaoCollection.Query.DataLancamento.NotEqual(primeiroDiaMesSegundoSemestre),
                liquidacaoCollection.Query.Origem.Equal((int)OrigemLancamentoLiquidacao.RendaFixa.TaxaCustodia));
            liquidacaoCollection.Query.GroupBy(liquidacaoCollection.Query.IdCliente);
            liquidacaoCollection.Query.Load();

            foreach (Liquidacao liquidacao in liquidacaoCollection)
            {
                int idCliente = liquidacao.IdCliente.Value;
                decimal valor = liquidacao.Valor.Value;

                TaxaCustodia taxaCustodia = new TaxaCustodia();
                taxaCustodia.idCliente = idCliente;
                taxaCustodia.valor = valor;

                lista.Add(taxaCustodia);
            }

            return lista;
        }
        
        public List<TaxaCustodiaAnual> RetornaValorCustodiaAnual(DateTime data)
        {
            List<TaxaCustodiaAnual> lista = new List<TaxaCustodiaAnual>();

            DateTime primeiroDia = Calendario.RetornaPrimeiroDiaUtilMes(data);

            PosicaoRendaFixaHistoricoCollection posicaoRendaFixaHistoricoCollection = new PosicaoRendaFixaHistoricoCollection();
            posicaoRendaFixaHistoricoCollection.Query.Select(posicaoRendaFixaHistoricoCollection.Query.IdCliente,
                                                             posicaoRendaFixaHistoricoCollection.Query.CustoCustodia.Sum());
            posicaoRendaFixaHistoricoCollection.Query.Where(posicaoRendaFixaHistoricoCollection.Query.DataHistorico.Equal(data));
            posicaoRendaFixaHistoricoCollection.Query.GroupBy(posicaoRendaFixaHistoricoCollection.Query.IdCliente);
            posicaoRendaFixaHistoricoCollection.Query.Load();

            foreach (PosicaoRendaFixaHistorico posicaoRendaFixaHistorico in posicaoRendaFixaHistoricoCollection)
            {
                int idCliente = posicaoRendaFixaHistorico.IdCliente.Value;
                decimal valor = posicaoRendaFixaHistorico.CustoCustodia.Value;

                TaxaCustodiaAnual taxaCustodiaAnual = new TaxaCustodiaAnual();
                taxaCustodiaAnual.idCliente = idCliente;
                taxaCustodiaAnual.valor = valor;

                lista.Add(taxaCustodiaAnual);
            }

            return lista;
        }

        /// <summary>
        /// Retorna clientes associados com taxa de custo mensal.
        /// </summary>
        /// <param name="data"></param>        
        /// <returns></returns>
        public List<int> RetornaClientesCustoMensal(DateTime data)
        {
            List<int> lista = new List<int>();
            List<int> listaCblc = new List<int>();

            PosicaoRendaFixaHistoricoCollection posicaoRendaFixaHistoricoCollection = new PosicaoRendaFixaHistoricoCollection();
            posicaoRendaFixaHistoricoCollection.Query.Select(posicaoRendaFixaHistoricoCollection.Query.IdCliente);
            posicaoRendaFixaHistoricoCollection.Query.Where(
                posicaoRendaFixaHistoricoCollection.Query.IdCustodia.Equal((byte)LocalCustodiaFixo.Cetip),
                posicaoRendaFixaHistoricoCollection.Query.DataHistorico.Equal(data),
                                                            posicaoRendaFixaHistoricoCollection.Query.Quantidade.GreaterThan(0));
            posicaoRendaFixaHistoricoCollection.Query.es.Distinct = true;

            //recupera posição em CBLC
            PosicaoRendaFixaHistoricoCollection posicaoRendaFixaHistoricoCollectionCblc = new PosicaoRendaFixaHistoricoCollection();
            posicaoRendaFixaHistoricoCollectionCblc.Query.Select(posicaoRendaFixaHistoricoCollection.Query.IdCliente);
            posicaoRendaFixaHistoricoCollectionCblc.Query.Where(
                posicaoRendaFixaHistoricoCollectionCblc.Query.IdCustodia.Equal((byte)LocalCustodiaFixo.CBLC),
                posicaoRendaFixaHistoricoCollectionCblc.Query.DataHistorico.Equal(data),
                                                            posicaoRendaFixaHistoricoCollectionCblc.Query.Quantidade.GreaterThan(0));
            posicaoRendaFixaHistoricoCollectionCblc.Query.es.Distinct = true;

            if (posicaoRendaFixaHistoricoCollectionCblc.Query.Load())
            {
                foreach (PosicaoRendaFixaHistorico posicaoRendaFixaHistoricoCblc in posicaoRendaFixaHistoricoCollectionCblc)
                    listaCblc.Add(posicaoRendaFixaHistoricoCblc.IdCliente.Value);
            }

            if (posicaoRendaFixaHistoricoCollection.Query.Load())
            {
                foreach (PosicaoRendaFixaHistorico posicaoRendaFixaHistorico in posicaoRendaFixaHistoricoCollection)
                    //Exporta somente clientes que não possuem custódia em CBLC.
                    if(!listaCblc.Contains(posicaoRendaFixaHistorico.IdCliente.Value)){
                        lista.Add(posicaoRendaFixaHistorico.IdCliente.Value);
                    }
            }

            return lista;
        }

        /// <summary>
        /// Calcula custo de custódia baseado em taxa ano.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="idAgente"></param>
        public void CalculaCustoCustodiaAnual(int idCliente, DateTime data)
        {
            DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1);

            TabelaCustosRendaFixaCollection tabelaCustosRendaFixaCollection = new TabelaCustosRendaFixaCollection();
            tabelaCustosRendaFixaCollection.Query.Where(tabelaCustosRendaFixaCollection.Query.DataReferencia.LessThanOrEqual(data) &
                                                        tabelaCustosRendaFixaCollection.Query.TaxaAno.IsNotNull() & 
                                                        ( tabelaCustosRendaFixaCollection.Query.IdCliente.IsNull() | tabelaCustosRendaFixaCollection.Query.IdCliente.Equal(idCliente )));
            tabelaCustosRendaFixaCollection.Query.OrderBy(tabelaCustosRendaFixaCollection.Query.DataReferencia.Descending,
                //Precisamos ordenar por especificidade de titulo e classe, para que primeiro sejam processados os mais especificos ateh os mais genericos
                    tabelaCustosRendaFixaCollection.Query.IdTitulo.Descending, tabelaCustosRendaFixaCollection.Query.Classe.Descending);
            tabelaCustosRendaFixaCollection.Query.Load();


            //Criar hashtable pra guardar posicoes que ja foram processadas. As posicoes que tiveram sido 
            //processadas por regras mais especificas nao podem ser processadas novamente
            Dictionary<int, int> posicoesProcessadas = new Dictionary<int, int>();
            foreach (TabelaCustosRendaFixa tabelaCustosRendaFixa in tabelaCustosRendaFixaCollection)
            {
                decimal taxaAno = tabelaCustosRendaFixa.TaxaAno.Value;
                int idAgente = tabelaCustosRendaFixa.IdAgente.Value;

                PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
                posicaoRendaFixaCollection.Query.Select(posicaoRendaFixaCollection.Query.IdPosicao,
                                                        posicaoRendaFixaCollection.Query.ValorMercado,
                                                        posicaoRendaFixaCollection.Query.CustoCustodia);
                posicaoRendaFixaCollection.Query.Where(posicaoRendaFixaCollection.Query.IdCliente.Equal(idCliente),
                                                       posicaoRendaFixaCollection.Query.IdAgente.Equal(idAgente),
                                                       posicaoRendaFixaCollection.Query.Quantidade.NotEqual(0),
                                                       posicaoRendaFixaCollection.Query.DataOperacao.LessThan(data),
                                                       posicaoRendaFixaCollection.Query.IdAgente.Equal(idAgente)); //Considera custódia em d+1 da operação

                if (tabelaCustosRendaFixa.IdTitulo.HasValue)
                {
                    posicaoRendaFixaCollection.Query.Where(posicaoRendaFixaCollection.Query.IdTitulo.Equal(tabelaCustosRendaFixa.IdTitulo.Value));
                }
                else if (tabelaCustosRendaFixa.Classe.HasValue)
                {
                    TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
                    PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery("P");
                    tituloRendaFixaQuery.Select(tituloRendaFixaQuery.IdTitulo);
                    tituloRendaFixaQuery.InnerJoin(papelRendaFixaQuery).On(papelRendaFixaQuery.IdPapel == tituloRendaFixaQuery.IdPapel);
                    tituloRendaFixaQuery.Where(papelRendaFixaQuery.Classe.Equal(tabelaCustosRendaFixa.Classe.Value));

                    posicaoRendaFixaCollection.Query.Where(posicaoRendaFixaCollection.Query.IdTitulo.In(tituloRendaFixaQuery));
                }

                posicaoRendaFixaCollection.Query.Load();

                foreach (PosicaoRendaFixa posicaoRendaFixa in posicaoRendaFixaCollection)
                {
                    if (!posicoesProcessadas.ContainsKey(posicaoRendaFixa.IdPosicao.Value))
                    {
                        decimal valorPosicao = posicaoRendaFixa.ValorMercado.HasValue ? posicaoRendaFixa.ValorMercado.Value : 0;

                        int numeroDias = Calendario.NumeroDias(dataAnterior, data);
                        decimal custoCustodiaDia = Utilitario.Truncate(valorPosicao * ((decimal)Math.Pow((double)(taxaAno / 100M + 1), (double)(numeroDias / 365M)) - 1), 2);

                        posicaoRendaFixa.CustoCustodia = posicaoRendaFixa.CustoCustodia.HasValue ? posicaoRendaFixa.CustoCustodia.Value : 0; 

                        posicaoRendaFixa.CustoCustodia += custoCustodiaDia;
                        posicoesProcessadas.Add(posicaoRendaFixa.IdPosicao.Value, 1);
                    }
                }

                posicaoRendaFixaCollection.Save();
            }
        }

        /// <summary>
        /// Trata o pagamento dos valores de custódia na data de vencimento. Assume pagamento nos meses de janeiro e julho de cada ano.
        /// </summary>
        public void TrataVencimentoCustodiaAnual(int idCliente, DateTime data)
        {
            if (data.Month != 07 && data.Month != 01)
                return;

            DateTime primeiroDiaMes = Calendario.RetornaPrimeiroDiaUtilMes(data);

            if (data != primeiroDiaMes)
                return;

            PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
            posicaoRendaFixaCollection.Query.Select(posicaoRendaFixaCollection.Query.IdPosicao,
                                                    posicaoRendaFixaCollection.Query.CustoCustodia,
                                                    posicaoRendaFixaCollection.Query.IdOperacao,
                                                    posicaoRendaFixaCollection.Query.IdCliente);
            posicaoRendaFixaCollection.Query.Where(posicaoRendaFixaCollection.Query.IdCliente.Equal(idCliente),
                                                   posicaoRendaFixaCollection.Query.CustoCustodia.IsNotNull());
            posicaoRendaFixaCollection.Query.Load();

            decimal custodiaCobrar = 0;

            LiquidacaoTaxaCustodiaCollection coll = new LiquidacaoTaxaCustodiaCollection();
            LiquidacaoTaxaCustodiaQuery query = new LiquidacaoTaxaCustodiaQuery();
            query.Where(query.DataHistorico >= data && query.IdCliente == idCliente);
            coll.Load(query);
            coll.MarkAllAsDeleted();
            coll.Save();

            foreach (PosicaoRendaFixa posicaoRendaFixa in posicaoRendaFixaCollection)
            {
                custodiaCobrar += posicaoRendaFixa.CustoCustodia.Value;
                

                if (posicaoRendaFixa.CustoCustodia.Value != 0)
                {
                    LiquidacaoTaxaCustodia liqTaxaCustodia = new LiquidacaoTaxaCustodia();
                    liqTaxaCustodia.IdOperacao = posicaoRendaFixa.IdOperacao;
                    liqTaxaCustodia.IdCliente = posicaoRendaFixa.IdCliente;
                    liqTaxaCustodia.CustoCustodia = posicaoRendaFixa.CustoCustodia.Value;
                    liqTaxaCustodia.TipoCustodia = 2;
                    liqTaxaCustodia.DataHistorico = data;
                    liqTaxaCustodia.Save();
                }

                posicaoRendaFixa.CustoCustodia = 0;
            }

            if (custodiaCobrar > 0)
            {
                //Busca o idContaDefault do cliente
                Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                int idContaDefault = contaCorrente.RetornaContaDefault(idCliente);
                //

                

                Liquidacao liquidacao = new Liquidacao();
                liquidacao.DataLancamento = data;
                liquidacao.DataVencimento = data;
                liquidacao.Descricao = "Cobrança de taxa de custódia anual";
                liquidacao.Valor = custodiaCobrar * -1;
                liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                liquidacao.Origem = OrigemLancamentoLiquidacao.RendaFixa.TaxaCustodia;
                liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                liquidacao.IdCliente = idCliente;
                liquidacao.IdConta = idContaDefault;
                liquidacao.Save();
            }

            posicaoRendaFixaCollection.Save();
        }

        public void TrataPosicoesLiquidadas(int idCliente, DateTime dataProcessamento, PosicaoRendaFixaCollection posicaoRendaFixaCollectionDeletar)
        {

            //Carregar todas as posicoes de renda fixa com qtde > 0 do cliente segregadas por classe papel
            PosicaoRendaFixaCollection posicaoRendaFixaCollectionCliente = new PosicaoRendaFixaCollection();
            PosicaoRendaFixaQuery posicaoRendaFixaQueryCliente = new PosicaoRendaFixaQuery("P");
            TituloRendaFixaQuery tituloRendaFixaQueryCliente = new TituloRendaFixaQuery("T");
            PapelRendaFixaQuery papelRendaFixaQueryCliente = new PapelRendaFixaQuery("A");

            //posicaoRendaFixaQueryCliente.Select(posicaoRendaFixaQueryCliente.CustoCustodia, papelRendaFixaQueryCliente.Classe);
            posicaoRendaFixaQueryCliente.InnerJoin(tituloRendaFixaQueryCliente).On(posicaoRendaFixaQueryCliente.IdTitulo == tituloRendaFixaQueryCliente.IdTitulo);
            posicaoRendaFixaQueryCliente.InnerJoin(papelRendaFixaQueryCliente).On(papelRendaFixaQueryCliente.IdPapel == tituloRendaFixaQueryCliente.IdPapel);

            posicaoRendaFixaQueryCliente.Where(posicaoRendaFixaQueryCliente.IdCliente.Equal(idCliente),
                posicaoRendaFixaQueryCliente.Quantidade.GreaterThan(0));

            posicaoRendaFixaCollectionCliente.Load(posicaoRendaFixaQueryCliente);

            //Para cada posicao resgatada, checar se existem posicoes remanescentes com a mesma classe de papel.
            foreach (PosicaoRendaFixa posicaoResgatada in posicaoRendaFixaCollectionDeletar)
            {
                

                PosicaoRendaFixa posicaoResgatadaLoaded = new PosicaoRendaFixa();

                posicaoResgatadaLoaded.LoadByPrimaryKey(posicaoResgatada.IdPosicao.Value);

                if (posicaoResgatadaLoaded.CustoCustodia.HasValue && posicaoResgatadaLoaded.CustoCustodia.Value > 0)
                {
                    LiquidaTaxaCustodiaPosicao(dataProcessamento, posicaoResgatadaLoaded);
                }

            }

            //Armazenar transferencias de valor da custodia entre posicoes
            posicaoRendaFixaCollectionCliente.Save();
        }

        private void LiquidaTaxaCustodiaPosicao(DateTime dataProcessamento, PosicaoRendaFixa posicaoResgatada)
        {
            int idCliente = posicaoResgatada.IdCliente.Value;

            //Busca o idContaDefault do cliente
            Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
            int idContaDefault = contaCorrente.RetornaContaDefault(idCliente);
            //

            LiquidacaoTaxaCustodiaCollection coll = new LiquidacaoTaxaCustodiaCollection();
            LiquidacaoTaxaCustodiaQuery query = new LiquidacaoTaxaCustodiaQuery();
            query.Where(query.DataHistorico >= dataProcessamento && query.IdCliente == idCliente);
            coll.Load(query);
            coll.MarkAllAsDeleted();
            coll.Save();


            LiquidacaoTaxaCustodia liqTaxaCustodia = new LiquidacaoTaxaCustodia();
            liqTaxaCustodia.IdOperacao = posicaoResgatada.IdOperacao;
            liqTaxaCustodia.IdCliente = idCliente;
            liqTaxaCustodia.CustoCustodia = posicaoResgatada.CustoCustodia;
            liqTaxaCustodia.TipoCustodia = 1;
            liqTaxaCustodia.DataHistorico = dataProcessamento;
            liqTaxaCustodia.Save();

            Liquidacao liquidacao = new Liquidacao();
            liquidacao.DataLancamento = dataProcessamento;
            liquidacao.DataVencimento = dataProcessamento;
            liquidacao.Descricao = "Cobrança de taxa de custódia";
            liquidacao.Valor = posicaoResgatada.CustoCustodia * -1;
            liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
            liquidacao.Origem = OrigemLancamentoLiquidacao.RendaFixa.TaxaCustodia;
            liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
            liquidacao.IdCliente = idCliente;
            liquidacao.IdConta = idContaDefault;
            liquidacao.Save();
        }
    }
}
