using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

namespace Financial.RendaFixa {
    public partial class CotacaoResolucao238Collection : esCotacaoResolucao238Collection {
        private static readonly ILog log = LogManager.GetLogger(typeof(CotacaoResolucao238Collection));

        /// <summary>
        ///  Deleta tudo da CotacaoResolucao238 de uma determinada data.
        /// </summary>
        /// <param name="data"></param>
        public void DeletaCotacaoResolucao238(DateTime data)
        {
            this.QueryReset();
            this.Query
              .Select(this.Query.DataReferencia, this.Query.CodigoSELIC, this.Query.DataVencimento)
              .Where(this.Query.DataReferencia == data);

            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }

    }
}
