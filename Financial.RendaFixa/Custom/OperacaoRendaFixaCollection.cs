﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.RendaFixa.Enums;
using log4net;
using Financial.Interfaces.Export.RendaFixa;
using Financial.Util;
using Financial.Common;
using Financial.CRM;
using Financial.CRM.Enums;
using Financial.Common.Enums;

namespace Financial.RendaFixa
{
    public partial class OperacaoRendaFixaCollection : esOperacaoRendaFixaCollection
    {
        /// <summary>
        /// Carrega o objeto OperacaoRendaFixaCollection com os campos IdTitulo, Valor.
        /// Agrupado por IdTitulo.
        /// Filtra para TipoOperacao.In(TipoOperacaoTitulo.CompraCasada, TipoOperacaoTitulo.VendaCasada).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataOperacao"></param>
        /// <returns>bool indicando se trouxe registro.</returns>
        public bool BuscaOperacaoCasada(int idCliente, DateTime dataOperacao)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdTitulo, this.Query.Valor.Sum())
                 .Where(this.Query.IdCliente == idCliente &
                        this.Query.DataOperacao.Equal(dataOperacao) &
                            (this.Query.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraCasada) |
                             this.Query.TipoOperacao.Equal((byte)TipoOperacaoTitulo.VendaCasada))
                        )
                 .GroupBy(this.Query.IdTitulo);

            this.Query.Load();

            return this.HasData;
        }
                
        /// <summary>
        /// Carrega o objeto OperacaoRendaFixaCollection com os campos DataLiquidacao, TipoOperacao, IdTitulo, Valor.
        /// Agrupado por DataLiquidacao, TipoOperacao, IdTitulo.
        /// Filtra para TipoOperacao.NotIn(TipoOperacaoTitulo.CompraCasada, TipoOperacaoTitulo.VendaCasada).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataOperacao"></param>
        /// <returns>bool indicando se trouxe registro.</returns>
        public bool BuscaOperacaoAgrupado(int idCliente, DateTime dataOperacao)
        {
            List<byte> lstTipoOpSemMovFinanceira = new List<byte>();
            lstTipoOpSemMovFinanceira.Add((byte)TipoOperacaoTitulo.CompraCasada);
            lstTipoOpSemMovFinanceira.Add((byte)TipoOperacaoTitulo.VendaCasada);
            lstTipoOpSemMovFinanceira.Add((byte)TipoOperacaoTitulo.Deposito);
            lstTipoOpSemMovFinanceira.Add((byte)TipoOperacaoTitulo.Retirada);
            lstTipoOpSemMovFinanceira.Add((byte)TipoOperacaoTitulo.IngressoAtivoImpactoCota);
            lstTipoOpSemMovFinanceira.Add((byte)TipoOperacaoTitulo.IngressoAtivoImpactoQtde);
            lstTipoOpSemMovFinanceira.Add((byte)TipoOperacaoTitulo.RetiradaAtivoImpactoCota);
            lstTipoOpSemMovFinanceira.Add((byte)TipoOperacaoTitulo.RetiradaAtivoImpactoQtde);

            //O método LancaCCOperacaoPremio lança valores referentes ao exercicio
            lstTipoOpSemMovFinanceira.Add((byte)TipoOperacaoTitulo.ExercicioOpcao);

            this.QueryReset();
            this.Query
                 .Select(this.Query.DataLiquidacao,
                         this.Query.TipoOperacao,
                         this.Query.IdTitulo,
                         this.Query.IdConta,
                         this.Query.Valor.Sum(),
                         this.Query.ValorLiquido.Sum(),
                         this.Query.ValorIR.Sum(),
                         this.Query.ValorIOF.Sum(),
                         this.Query.ValorCorretagem.Sum())
                 .Where(this.Query.IdCliente == idCliente &
                        this.Query.DataOperacao.Equal(dataOperacao) &
                            (
                                this.Query.TipoOperacao.NotIn(lstTipoOpSemMovFinanceira.ToArray())
                            )
                        )
                 .GroupBy(this.Query.DataLiquidacao, this.Query.TipoOperacao, this.Query.IdTitulo, this.Query.IdConta);

            this.Query.Load();

            return this.HasData;
        }

        /// <summary>
        /// Carrega o objeto OperacaoRendaFixaCollection com os campos DataLiquidacao, TipoOperacao, IdTitulo, Valor, ValorLiquido.
        /// Agrupado por DataLiquidacao, TipoOperacao, IdTitulo.
        /// Considera operações retroativas que liquidam em DataProcessamento e DataOperacao menor DataRegistro 
        /// Filtra para TipoOperacao.NotIn(TipoOperacaoTitulo.CompraCasada, TipoOperacaoTitulo.VendaCasada).        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataOperacao"></param>
        /// <returns>bool indicando se trouxe registro.</returns>
        public bool BuscaOperacaoAgrupadoLiquidacao(int idCliente, DateTime dataOperacao)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.DataLiquidacao,
                         this.Query.TipoOperacao,
                         this.Query.IdTitulo,
                         this.Query.Valor.Sum(),
                         this.Query.ValorLiquido.Sum(),
                         this.Query.ValorIR.Sum(),
                         this.Query.ValorIOF.Sum(),
                         this.Query.ValorCorretagem.Sum())
                 .Where(((this.Query.IdCliente == idCliente &
                        this.Query.DataOperacao.Equal(dataOperacao)) |
                        (this.Query.IdCliente == idCliente & 
                         this.Query.DataRegistro.Equal(dataOperacao) &
                         this.Query.DataLiquidacao.Equal(dataOperacao) & 
                         this.Query.DataOperacao.LessThan(dataOperacao))) &                         
                        (
                            this.Query.TipoOperacao.NotEqual((byte)TipoOperacaoTitulo.CompraCasada) &
                            this.Query.TipoOperacao.NotEqual((byte)TipoOperacaoTitulo.VendaCasada) &
                            this.Query.TipoOperacao.NotEqual((byte)TipoOperacaoTitulo.Deposito) &
                            this.Query.TipoOperacao.NotEqual((byte)TipoOperacaoTitulo.Retirada)
                        )
                        )
                 .GroupBy(this.Query.DataLiquidacao, this.Query.TipoOperacao, this.Query.IdTitulo);

            this.Query.Load();

            return this.HasData;
        }

        /// <summary>
        /// Carrega o objeto OperacaoRendaFixaCollection com os campos IdTitulo, Valor.
        /// Agrupado por IdTitulo.
        /// Filtra para TipoOperacao.In((byte)VendaFinal, (byte)VendaTotal, (byte)Retirada, (byte)AntecipacaoRevenda, (byte)AntecipacaoRecompra).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataOperacao"></param>
        /// <returns>bool indicando se trouxe registro.</returns>
        public bool BuscaLiquidacaoPosicao(int idCliente, DateTime dataOperacao)
        {
            List<byte> lstTipoOpLiquidaPosicao = new List<byte>();
            lstTipoOpLiquidaPosicao.Add((byte)TipoOperacaoTitulo.VendaFinal);
            lstTipoOpLiquidaPosicao.Add((byte)TipoOperacaoTitulo.VendaTotal);
            lstTipoOpLiquidaPosicao.Add((byte)TipoOperacaoTitulo.Retirada);
            lstTipoOpLiquidaPosicao.Add((byte)TipoOperacaoTitulo.AntecipacaoRevenda);
            lstTipoOpLiquidaPosicao.Add((byte)TipoOperacaoTitulo.AntecipacaoRecompra);
            lstTipoOpLiquidaPosicao.Add((byte)TipoOperacaoTitulo.RetiradaAtivoImpactoCota);
            lstTipoOpLiquidaPosicao.Add((byte)TipoOperacaoTitulo.RetiradaAtivoImpactoQtde);
            lstTipoOpLiquidaPosicao.Add((byte)TipoOperacaoTitulo.ExercicioOpcao);

            this.QueryReset();
            this.Query
                 .Select()
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.DataOperacao.Equal(dataOperacao),
                        this.Query.TipoOperacao.In(lstTipoOpLiquidaPosicao.ToArray()));

            this.Query.Load();

            return this.HasData;
        }

        /// <summary>
        /// Carrega o objeto OperacaoRendaFixaCollection com os campos IdTitulo, Valor.
        /// Agrupado por IdTitulo.
        /// Filtra para TipoOperacao.Equal(TipoOperacaoTitulo.VendaFinal).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataOperacao"></param>
        /// <returns>bool indicando se trouxe registro.</returns>
        public bool BuscaVendaFinal(int idCliente, DateTime dataOperacao)
        {
            this.QueryReset();
            this.Query
                 .Select()
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.DataOperacao.Equal(dataOperacao),
                        this.Query.TipoOperacao.In((byte)TipoOperacaoTitulo.VendaFinal,
                                                   (byte)TipoOperacaoTitulo.Retirada,
                                                   (byte)TipoOperacaoTitulo.RetiradaAtivoImpactoCota,
                                                   (byte)TipoOperacaoTitulo.RetiradaAtivoImpactoQtde,
                                                   (byte)TipoOperacaoTitulo.VendaCasada));

            this.Query.Load();

            return this.HasData;
        }

        /// <summary>
        /// Carrega o objeto OperacaoRendaFixaCollection com os campos IdTitulo, Valor.
        /// Agrupado por IdTitulo.
        /// Filtra para TipoOperacao.In(TipoOperacaoTitulo.CompraCasada, TipoOperacaoTitulo.VendaCasada).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataOperacao"></param>
        /// <returns>bool indicando se trouxe registro.</returns>
        public bool BuscaCompraFinal(int idCliente, DateTime data) 
        {
            this.QueryReset();
            this.Query
                 .Select()
                 .Where(this.Query.IdCliente == idCliente,   
                        this.Query.DataRegistro.Equal(data),    
                        this.Query.TipoOperacao.In((byte)TipoOperacaoTitulo.CompraFinal,
                                                   (byte)TipoOperacaoTitulo.Deposito,
                                                   (byte)TipoOperacaoTitulo.IngressoAtivoImpactoCota,
                                                   (byte)TipoOperacaoTitulo.IngressoAtivoImpactoQtde,
                                                   (byte)TipoOperacaoTitulo.CompraCasada));
            
           
            
            this.Query.Load();

            return this.HasData;
        }

        /// <summary>
        /// Carrega o objeto OperacaoRendaFixaCollection todos os campos de OperacaoRendaFixa.
        /// Filtra para TipoOperacao.In(TipoOperacaoTitulo.CompraRevenda, TipoOperacaoTitulo.VendaRecompra).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataOperacao"></param>
        /// <returns>bool indicando se trouxe registro.</returns>
        public bool BuscaOperacaoCompromissada(int idCliente, DateTime dataOperacao)
        {
            this.QueryReset();
            this.Query
                 .Select()
                 .Where(this.Query.IdCliente == idCliente &
                        this.Query.DataRegistro.Equal(dataOperacao) &
                            (
                                this.Query.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraRevenda) |
                                this.Query.TipoOperacao.Equal((byte)TipoOperacaoTitulo.VendaRecompra)
                            )
                        );

            this.Query.Load();

            return this.HasData;
        }

        /// <summary>
        /// Carrega o objeto OperacaoRendaFixaCollection com todos os campos de OperacaoRendaFixa.
        /// Filtra para TipoOperacao.In(TipoOperacaoTitulo.AntecipacaoRevenda, TipoOperacaoTitulo.AntecipacaoRecompra).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataOperacao"></param>
        /// <returns>bool indicando se trouxe registro.</returns>
        public bool BuscaAntecipacaoCompromisso(int idCliente, DateTime dataOperacao)
        {
            this.QueryReset();
            this.Query
                 .Where(this.Query.IdCliente == idCliente &
                        this.Query.DataOperacao.Equal(dataOperacao) &
                            (
                                this.Query.TipoOperacao.Equal((byte)TipoOperacaoTitulo.AntecipacaoRevenda) |
                                this.Query.TipoOperacao.Equal((byte)TipoOperacaoTitulo.AntecipacaoRecompra)
                             )
                        );

            this.Query.Load();

            return this.HasData;
        }

        public OperacaoCetipViewModel ExportaOperacaoCetip(DateTime dataExportacao, int classe)
        {
            const string CODIGO_OPERACAO = "LCOP";
            const string TIPO_REGISTRO_HEADER = "0";
            const string TIPO_REGISTRO_OPERACAO = "1";
            const int VERSAO_LAYOUT = 10;
            const string DELIMITADOR = "<";
            //const int MAX_IDOPERACAO = 999999;
            const string CONTA_CETIP = "3412";//Todo: meter como campos no agente mercado
            int CONTA_AGENTE_MERCADO = Convert.ToInt32(CONTA_CETIP + "00" + "4");//Todo: como tem digito verificador precisamos meter como fields no agente mercado
            int CONTA_CLIENTE = Convert.ToInt32(CONTA_CETIP + "10" + "7");//Todo: como tem digito verificador precisamos meter como fields no agente mercado
            int CONTA_BROKERAGE = Convert.ToInt32(CONTA_CETIP + "69" + "5");//Todo: como tem digito verificador precisamos meter como fields no agente mercado
            const string TIPO_PESSOA_JURIDICA = "PJ";
            const string TIPO_PESSOA_FISICA = "PF";

            Dictionary<string, string> operacoesCompraBrokerage = new Dictionary<string,string>();

            int codigoBovespa = ParametrosConfiguracaoSistema.Bolsa.CodigoAgenteDefault;
            AgenteMercado agenteMercado = new AgenteMercadoCollection().BuscaAgenteMercadoPorCodigoBovespa(codigoBovespa);

            string cnpjAgenteMercado = Utilitario.RemoveCaracteresEspeciais(agenteMercado.Cnpj);

            OperacaoRendaFixaQuery operacaoQuery = new OperacaoRendaFixaQuery("O");
            PapelRendaFixaQuery papelQuery = new PapelRendaFixaQuery("P");
            TituloRendaFixaQuery tituloQuery = new TituloRendaFixaQuery("T");

            operacaoQuery.Where(
                operacaoQuery.DataOperacao.Equal(dataExportacao.Date),
                operacaoQuery.IdCustodia.Equal((byte)LocalCustodiaFixo.Cetip),
                operacaoQuery.StatusExportacao.Equal((int)StatusExportacaoRendaFixa.LiberadoExportacao),
                papelQuery.Classe.Equal(classe));

            operacaoQuery.InnerJoin(tituloQuery).On(tituloQuery.IdTitulo.Equal(operacaoQuery.IdTitulo));
            operacaoQuery.InnerJoin(papelQuery).On(papelQuery.IdPapel.Equal(tituloQuery.IdPapel));
            operacaoQuery.OrderBy(operacaoQuery.TipoOperacao.Ascending);

            this.Load(operacaoQuery);

            OperacaoCetipViewModel operacaoCetipViewModel = new OperacaoCetipViewModel();

            #region Preenchimento do header
            string tipoIF = operacaoCetipViewModel.header.TipoIF = MapClasseParaTipoIF(classe);
            operacaoCetipViewModel.header.TipoRegistro = TIPO_REGISTRO_HEADER;
            operacaoCetipViewModel.header.CodigoOperacao = CODIGO_OPERACAO;

            
            operacaoCetipViewModel.header.NomeSimplificadoParticipante = agenteMercado.Apelido;
            operacaoCetipViewModel.header.Data = dataExportacao;
            operacaoCetipViewModel.header.VersaoLayout = VERSAO_LAYOUT;
            operacaoCetipViewModel.header.Delimitador = DELIMITADOR;
            #endregion

            #region Preenchimento das linhas detalhe de operação
            foreach (OperacaoRendaFixa operacao in this)
            {
                //Descobrir se quem está lançando a operação é a própria corretora
                Pessoa pessoa = new Pessoa();
                pessoa.LoadByPrimaryKey(operacao.IdCliente.Value);
                string cpfcnpjPessoa = Utilitario.RemoveCaracteresEspeciais(pessoa.Cpfcnpj);

                bool lancadorIsAgenteMercado = operacao.IdAgenteContraParte.HasValue;

                


                RegistroOperacaoCetip registro = new RegistroOperacaoCetip();
                registro.TipoIF = tipoIF;
                registro.TipoRegistro = TIPO_REGISTRO_OPERACAO;
                
                bool isTermo = operacao.DataLiquidacao.Value.Date > operacao.DataOperacao.Value.Date;
                if (isTermo)
                {
                    registro.CodigoOperacao = (int)CodigoOperacaoCetip.CompraVendaTermo;
                }
                else
                {
                    registro.CodigoOperacao = (int)CodigoOperacaoCetip.CompraVendaDefinitiva;
                }

                TituloRendaFixa titulo = new TituloRendaFixa();
                titulo.LoadByPrimaryKey(operacao.IdTitulo.Value);
                registro.CodigoIF = titulo.CodigoCetip;

                registro.IFComRestricao = "";

                int sequencial = operacao.IdOperacao.Value;
                registro.MeuNumero = sequencial;

                registro.NumeroOperacaoCetip = null;
                registro.NumeroAssociacao = null;
                
                bool isBrokerage = operacao.TipoOperacao == (byte)TipoOperacaoTitulo.CompraCasada || 
                    operacao.TipoOperacao == (byte)TipoOperacaoTitulo.VendaCasada;

                if (!isBrokerage)
                {
                    //Nao brokerage
                    if (lancadorIsAgenteMercado)
                    {
                        //Encarteiramento via mercado ou via cliente
                        registro.ContaParte = CONTA_AGENTE_MERCADO;
                        //registro.ContaContraparte =  String.IsNullOrEmpty(operacao.CodigoContraParte) ? CONTA_CLIENTE : 
                        //    Convert.ToInt32(Utilitario.RemoveCaracteresEspeciais(operacao.CodigoContraParte));

                        AgenteMercado agenteMercadoContraparte = new AgenteMercado();
                        agenteMercado.LoadByPrimaryKey(operacao.IdAgenteContraParte.Value);
                        registro.ContaContraparte = int.Parse(agenteMercado.CodigoCetip);


                        //Ótica da corretora: compra da corretora é sempre compra - idem pra venda
                        registro.TipoCompraVenda = operacao.TipoOperacao == (byte)TipoOperacaoTitulo.CompraFinal ? (int)TipoCompraVendaCetip.Compra :
                    (int)TipoCompraVendaCetip.VendaCarteiraPropria;

                    }
                    else
                    {
                        //Cliente comprando ou vendendo sempre através do agente de mercado
                        registro.ContaParte = CONTA_AGENTE_MERCADO;
                        registro.ContaContraparte = CONTA_CLIENTE;

                        //Operacao é o inverso pois estamos vendo sob a ótica da corretora vendendo para o cliente. 
                        //Compra do cliente é venda da corretora e venda do cliente é compra da corretora
                        registro.TipoCompraVenda = operacao.TipoOperacao == (byte)TipoOperacaoTitulo.CompraFinal ? (int)TipoCompraVendaCetip.VendaCarteiraPropria :
                                            (int)TipoCompraVendaCetip.Compra;
                   }
                }
                else
                {
                    //Brokerage
                    string chaveAssociacao = operacao.IdTitulo.Value + "-" + operacao.DataOperacao.Value + "-" + 
                            operacao.Quantidade.Value + "-" + operacao.PUOperacao.Value;

                    if (lancadorIsAgenteMercado)
                    {
                        registro.ContaParte = CONTA_BROKERAGE;
                        //registro.ContaContraparte = Convert.ToInt32(Utilitario.RemoveCaracteresEspeciais(operacao.CodigoContraParte));

                        AgenteMercado agenteMercadoContraparte = new AgenteMercado();
                        agenteMercado.LoadByPrimaryKey(operacao.IdAgenteContraParte.Value);
                        registro.ContaContraparte = int.Parse(agenteMercado.CodigoCetip);

                        registro.TipoCompraVenda = operacao.TipoOperacao == (byte)TipoOperacaoTitulo.CompraCasada ? (int)TipoCompraVendaCetip.Compra :
                    (int)TipoCompraVendaCetip.VendaCarteiraPropria;

                        registro.NumeroAssociacao = Convert.ToString(sequencial).PadLeft(6, '0');
                        operacoesCompraBrokerage.Add(chaveAssociacao, registro.NumeroAssociacao);
                    }
                    else
                    {
                        registro.ContaParte = CONTA_BROKERAGE;
                        registro.ContaContraparte = CONTA_CLIENTE;

                        //Operacao é o inverso pois estamos vendo sobre a ótica da corretora vendendo para o cliente
                        registro.TipoCompraVenda = operacao.TipoOperacao == (byte)TipoOperacaoTitulo.CompraCasada ? (int)TipoCompraVendaCetip.VendaCarteiraPropria :
                                            (int)TipoCompraVendaCetip.Compra;

                        try
                        {
                            registro.NumeroAssociacao = operacoesCompraBrokerage[chaveAssociacao];
                            
                            //Limpar operacao de compra para nao ser utilizado por outra operacao de venda
                            operacoesCompraBrokerage.Remove(chaveAssociacao); 
                        }
                        catch
                        {
                            throw new Exception("Operação de venda de brokerage não possui operação de compra equivalente. IdOperacao: " + operacao.IdOperacao.Value);
                        }

                    }
                }

                

                registro.QuantidadeOperacao = Math.Truncate(operacao.Quantidade.Value);
                registro.ValorOperacao = Math.Truncate(operacao.Valor.Value * 100);
                registro.PrecoUnitarioOperacao = Math.Truncate(operacao.PUOperacao.Value * 100000000);

                //se for operação com o mercado então modalidade bruta se for com cliente(interna) então sem modalidade
                registro.CodigoModalidadeLiquidacao = operacao.IdAgenteContraParte.HasValue ? (int)CodigoModalidadeLiquidacaoCetip.Bruta : (int)CodigoModalidadeLiquidacaoCetip.SemModalidade;

                //não preencher cpf quando operações com o mercado
                if (!operacao.IdAgenteContraParte.HasValue)
                {
                    registro.CpfcnpjCliente = Convert.ToString(Convert.ToInt64(cpfcnpjPessoa));
                    if (registro.CpfcnpjCliente.Length < 11)
                    {
                        registro.CpfcnpjCliente = registro.CpfcnpjCliente.PadLeft(11, '0');
                    }
                    else if (registro.CpfcnpjCliente.Length > 11)
                    {
                        registro.CpfcnpjCliente = registro.CpfcnpjCliente.PadLeft(14, '0');
                    }
                }
                else
                {
                    registro.CpfcnpjCliente = "";
                }


                if (!operacao.IdAgenteContraParte.HasValue)
                {
                    registro.NaturezaEmitente = (pessoa.Tipo == (byte)TipoPessoa.Fisica) ?
                        TIPO_PESSOA_FISICA : TIPO_PESSOA_JURIDICA;
                }
                else
                {
                    registro.NaturezaEmitente = "";
                }


                if (isTermo)
                {
                    registro.DataLiquidacao = operacao.DataLiquidacao.Value.ToString("yyyyMMdd");
                }

                registro.Delimitador = DELIMITADOR;
                
                operacao.StatusExportacao = (int)StatusExportacaoRendaFixa.Exportado;
                operacaoCetipViewModel.registros.Add(registro);
            }
            #endregion

            this.Save();

            return operacaoCetipViewModel;
        }

        private string MapClasseParaTipoIF(int classe)
        {

            string mappedClasse = "";
            if (classe == (int)ClasseRendaFixa.CCB)
            {
                mappedClasse = "CCB";
            }
            else if (classe == (int)ClasseRendaFixa.CDB)
            {
                mappedClasse = "CDB";
            }
            else if (classe == (int)ClasseRendaFixa.CDA)
            {
                mappedClasse = "CDA";
            }
            else if (classe == (int)ClasseRendaFixa.CDCA)
            {
                mappedClasse = "CDCA";
            }
            else if (classe == (int)ClasseRendaFixa.CPR)
            {
                mappedClasse = "CPR";
            }
            else if (classe == (int)ClasseRendaFixa.CRA)
            {
                mappedClasse = "CRA";
            }
            else if (classe == (int)ClasseRendaFixa.CRI)
            {
                mappedClasse = "CRI";
            }
            else if (classe == (int)ClasseRendaFixa.CCI)
            {
                mappedClasse = "CCI";
            }
            else if (classe == (int)ClasseRendaFixa.Debenture)
            {
                mappedClasse = "DEB";
            }
            else if (classe == (int)ClasseRendaFixa.DPGE)
            {
                mappedClasse = "DPGE";
            }
            else if (classe == (int)ClasseRendaFixa.LC)
            {
                mappedClasse = "LC";
            }
            else if (classe == (int)ClasseRendaFixa.LCA)
            {
                mappedClasse = "LCA";
            }
            else if (classe == (int)ClasseRendaFixa.LCI)
            {
                mappedClasse = "LCI";
            }
            else if (classe == (int)ClasseRendaFixa.LF)
            {
                mappedClasse = "LF";
            }
            else if (classe == (int)ClasseRendaFixa.LH)
            {
                mappedClasse = "LH";
            }
            else
            {
                throw new Exception("Classe não suportada: " + classe);
            }

            return mappedClasse;
        }

    }
}
