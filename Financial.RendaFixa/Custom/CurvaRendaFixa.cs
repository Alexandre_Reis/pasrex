﻿/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 15/09/2014 16:56:41
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using System.Collections;
using Financial.Util;
using Financial.Common.Enums;

namespace Financial.RendaFixa
{
	public partial class CurvaRendaFixa : esCurvaRendaFixa
	{
        public bool ReprocessaCurvaComposta(DateTime data, int idCurvaAlterada, bool CurvaSpread, ref Hashtable hsTaxasCurvas, ref Hashtable hsTaxasCurvasBase)
        {
            CurvaRendaFixaCollection curvaRendaFixaColl = new CurvaRendaFixaCollection();
            TaxaCurvaCollection taxaCurvaColl = new TaxaCurvaCollection();

            //Se não foi usada para formar uma curva composta, acaba o looping
            if (CurvaSpread)
                curvaRendaFixaColl.Query.Where(curvaRendaFixaColl.Query.IdCurvaSpread.Equal(idCurvaAlterada));
            else
                curvaRendaFixaColl.Query.Where(curvaRendaFixaColl.Query.IdCurvaBase.Equal(idCurvaAlterada));

            if (!curvaRendaFixaColl.Query.Load())
                return true;

            foreach (CurvaRendaFixa curvaRendaFixa in curvaRendaFixaColl)
            {
                do
                {
                    //Processa a curva composta
                    taxaCurvaColl.ExcluirTaxas(data, curvaRendaFixa.IdCurvaRendaFixa.Value);
                    this.ProcessaCurvaComposta(data, curvaRendaFixa.IdCurvaRendaFixa.Value, ref hsTaxasCurvas, ref hsTaxasCurvasBase);

                }
                while (!ReprocessaCurvaComposta(data, curvaRendaFixa.IdCurvaRendaFixa.Value, false, ref hsTaxasCurvas, ref hsTaxasCurvasBase));
            }

            return true;
        }

        public void ProcessaCurvaComposta(DateTime data)
        {
            Hashtable hsTaxasCurvas = new Hashtable();
            Hashtable hsTaxasCurvasCompostas = new Hashtable();
            this.ProcessaCurvaComposta(data, 0, ref hsTaxasCurvas, ref hsTaxasCurvasCompostas);
        }

        public void ProcessaCurvaComposta(DateTime data, int idCurvaRendaFixa, ref Hashtable hsMemoria)
        {
            #region Controle de Memoria Inicio
            Hashtable hsTaxasCurvas;
            int keyTaxasCurvas = (int)Financial.Util.Enums.KeyHashTable.TaxaCurva;
            if (hsMemoria.Contains(keyTaxasCurvas))
                hsTaxasCurvas = (Hashtable)hsMemoria[keyTaxasCurvas];
            else
                hsTaxasCurvas = new Hashtable();

            Hashtable hsTaxasCurvasCompostas;
            int keyCurvasCompostas = (int)Financial.Util.Enums.KeyHashTable.CurvasCompostas;
            if (hsMemoria.Contains(keyCurvasCompostas))
                hsTaxasCurvasCompostas = (Hashtable)hsMemoria[keyCurvasCompostas];
            else
                hsTaxasCurvasCompostas = new Hashtable();
            #endregion

            this.ProcessaCurvaComposta(data, idCurvaRendaFixa, ref hsTaxasCurvas, ref hsTaxasCurvasCompostas);

            if (!hsMemoria.Contains(keyTaxasCurvas))
                hsMemoria.Add(keyTaxasCurvas, hsTaxasCurvas);

            if (!hsMemoria.Contains(keyCurvasCompostas))
                hsMemoria.Add(keyCurvasCompostas, hsTaxasCurvasCompostas);
        }


        public void ProcessaCurvaComposta(DateTime data, int idCurvaRendaFixa, ref Hashtable hsTaxasCurvas, ref Hashtable hsTaxasCurvasCompostas)
        {
            #region Objetos/Variaveis
            CurvaRendaFixaQuery curvasCompostasQuery = new CurvaRendaFixaQuery("CurvasCompostas");
            TaxaCurvaQuery taxaCurvaCompostaQuery = new TaxaCurvaQuery("TaxaCurvaComposta");
            CurvaRendaFixaCollection curvasCompostasColl = new CurvaRendaFixaCollection();
            TaxaCurvaCollection taxaConjuntoUniaoColl;
            CalculoRendaFixa calculoRendaFixa = new CalculoRendaFixa();
            CurvaRendaFixa curvaBase;
            CurvaRendaFixa curvaSpread;
            DateTime dataVencimento;
            int? idCurvaBase;
            int? idCurvaSpread;
            int? tipoCurvaBase;
            int enumTipoCurvaComposta = (int)Financial.RendaFixa.Enums.TipoCurva.Composta;
            #endregion

            #region Query
            curvasCompostasQuery.es.Distinct = true;
            curvasCompostasQuery.Select(curvasCompostasQuery, 
                                        taxaCurvaCompostaQuery.IdCurvaRendaFixa);
            curvasCompostasQuery.LeftJoin(taxaCurvaCompostaQuery).On(taxaCurvaCompostaQuery.IdCurvaRendaFixa.Equal(curvasCompostasQuery.IdCurvaRendaFixa) & taxaCurvaCompostaQuery.DataBase.Equal("'" + data.ToString("yyyyMMdd") + "'"));

            if(idCurvaRendaFixa > 0)
                curvasCompostasQuery.Where(curvasCompostasQuery.IdCurvaRendaFixa.Equal(idCurvaRendaFixa)); 
            else
                curvasCompostasQuery.Where(curvasCompostasQuery.TipoCurva.Equal(enumTipoCurvaComposta));

            curvasCompostasQuery.Where(taxaCurvaCompostaQuery.IdCurvaRendaFixa.IsNull());

            curvasCompostasColl.Load(curvasCompostasQuery);
            #endregion

            foreach (CurvaRendaFixa curvaComposta in curvasCompostasColl)
            {
                taxaConjuntoUniaoColl = new TaxaCurvaCollection();

                if (curvaComposta.IdCurvaBase.HasValue)
                {
                    curvaBase = curvaComposta.UpToCurvaRendaFixaByIdCurvaBase;
                    idCurvaBase = curvaBase.IdCurvaRendaFixa;
                    tipoCurvaBase = (int)curvaBase.TipoCurva;
                }
                else
                {
                    curvaBase = new CurvaRendaFixa();
                    idCurvaBase = null;
                    tipoCurvaBase = null;
                }

                curvaSpread = curvaComposta.UpToCurvaRendaFixaByIdCurvaSpread;
                idCurvaSpread = curvaSpread.IdCurvaRendaFixa;

                if (idCurvaBase.HasValue)
                {
                    if (curvaBase.TipoCurva.Value != enumTipoCurvaComposta)
                    {
                        if (!hsTaxasCurvas.Contains(idCurvaBase.Value))
                        {
                            TaxaCurvaCollection taxaCurvaCollAux = taxaConjuntoUniaoColl.obterTaxaCurvaCollection(data, idCurvaBase.Value);

                            if (taxaCurvaCollAux.Count > 0)
                                hsTaxasCurvas.Add(idCurvaBase.Value, taxaCurvaCollAux);
                        }
                    }
                }

                if (!hsTaxasCurvas.Contains(idCurvaSpread.Value))
                {
                    TaxaCurvaCollection taxaCurvaCollAux = taxaConjuntoUniaoColl.obterTaxaCurvaCollection(data, idCurvaSpread.Value);

                    if (taxaCurvaCollAux.Count > 0)
                        hsTaxasCurvas.Add(idCurvaSpread.Value, taxaCurvaCollAux);
                }

                TaxaCurvaCollection taxaCurvaFluxos = this.montaConjuntoUniaoDatas(idCurvaBase, idCurvaSpread, tipoCurvaBase, data, hsTaxasCurvas);
                if (taxaCurvaFluxos.Count > 0)
                    dataVencimento = taxaCurvaFluxos[taxaCurvaFluxos.Count - 1].DataVertice.Value;
                else
                    continue;

                foreach (TaxaCurva taxaCurvaFluxo in taxaCurvaFluxos)
                {
                    if (taxaCurvaFluxo.DataVertice < data)
                        continue;

                    TaxaCurva taxaCurvaComposta = taxaConjuntoUniaoColl.AddNew();                    
                    taxaCurvaComposta.DataBase = data;
                    taxaCurvaComposta.DataVertice = taxaCurvaFluxo.DataVertice.Value;
                    taxaCurvaComposta.IdCurvaRendaFixa = curvaComposta.IdCurvaRendaFixa;
                    taxaCurvaComposta.PrazoDC = Calendario.NumeroDias(data, taxaCurvaComposta.DataVertice.Value);
                    taxaCurvaComposta.PrazoDU = Calendario.NumeroDias(data, taxaCurvaComposta.DataVertice.Value, (int)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                    taxaCurvaComposta.Taxa = calculoRendaFixa.CalculaTaxaCurvaMTM(data, taxaCurvaComposta.DataVertice.Value, dataVencimento, curvaComposta, ref hsTaxasCurvas, ref hsTaxasCurvasCompostas);
                }

                taxaConjuntoUniaoColl.Save();
            }
        }

        private TaxaCurvaCollection montaConjuntoUniaoDatas(int? idCurvaBase, int? idCurvaSpread, int? tipoCurvaBase, DateTime data, Hashtable hsTaxasCurvas)
        {
            TaxaCurvaCollection taxaCurvaCollection = new TaxaCurvaCollection();

            #region Verifica se há taxas cadastradas
            if (idCurvaBase.HasValue)
            {
                if (tipoCurvaBase.Value != (int)Financial.RendaFixa.Enums.TipoCurva.Composta)
                {
                    if (!hsTaxasCurvas.Contains(idCurvaBase.Value))
                        return new TaxaCurvaCollection();
                }
            }

            if (!hsTaxasCurvas.Contains(idCurvaSpread.Value))
                return new TaxaCurvaCollection();
            #endregion

            taxaCurvaCollection.Query.es.Distinct = true;
            taxaCurvaCollection.Query.Select(taxaCurvaCollection.Query.DataVertice);

            if (idCurvaBase.HasValue)
                taxaCurvaCollection.Query.Where(taxaCurvaCollection.Query.IdCurvaRendaFixa.In(idCurvaBase, idCurvaSpread));
            else
                taxaCurvaCollection.Query.Where(taxaCurvaCollection.Query.IdCurvaRendaFixa.Equal(idCurvaSpread));

            taxaCurvaCollection.Query.Where(taxaCurvaCollection.Query.DataBase.Equal(data));

            taxaCurvaCollection.Query.OrderBy(taxaCurvaCollection.Query.DataVertice.Ascending);

            if (taxaCurvaCollection.Query.Load())
                return taxaCurvaCollection;

            return new TaxaCurvaCollection();
        }
	}
}
