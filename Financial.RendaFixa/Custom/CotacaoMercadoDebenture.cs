﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Interfaces.Import.RendaFixa;
//using Financial.Interfaces.Sinacor;

namespace Financial.RendaFixa {
    public partial class CotacaoMercadoDebenture : esCotacaoMercadoDebenture {
        
        /// <summary>
        /// Faz o Download de http://www.andima.com.br/
        /// </summary>
        /// <param name="data"></param>
        /// <param name="pathArquivo">path completo do Diretorio onde procurar o Arquivo</param>
        public void CarregaDebenture(DateTime data, string pathArquivo) {            
            Debenture debenture = new Debenture();
            CotacaoMercadoDebentureCollection cotacaoMercadoDebentureCollection = new CotacaoMercadoDebentureCollection();
            CotacaoMercadoDebentureCollection cotacaoMercadoDebentureCollectionAux = new CotacaoMercadoDebentureCollection();

            // Pega os registros da collection de Debenture
            DebentureCollection debentureCollection = debenture.ProcessaDebenture(data, pathArquivo);
            DebentureCollection debentureCollectionAux = new DebentureCollection();

            // Filtra a Collection somente tipoRegistro = Dados
            debentureCollectionAux.CollectionDebenture = debentureCollection.CollectionDebenture.FindAll(debenture.FilterDebentureByDados);
            //

            if (debentureCollectionAux.CollectionDebenture.Count > 1)
            {
                // Deleta a tabela de CotacaoMercadoDebenture da data em questão
                cotacaoMercadoDebentureCollectionAux.DeletaCotacaoMercadoDebenture(data);
            }

            for (int i = 0; i < debentureCollectionAux.CollectionDebenture.Count; i++) {
                debenture = debentureCollectionAux.CollectionDebenture[i];

                #region Copia informações para CotacaoMercadoDebenture
                CotacaoMercadoDebenture cotacaoMercadoDebenture = new CotacaoMercadoDebenture();
                //
                cotacaoMercadoDebenture.DataReferencia = data;
                cotacaoMercadoDebenture.DataVencimento = debenture.DataVencimento;
                cotacaoMercadoDebenture.CodigoPapel = debenture.Codigo;
                cotacaoMercadoDebenture.TaxaIndicativa = debenture.TaxaIndicativa;
                cotacaoMercadoDebenture.Pu = debenture.Pu;
                //
                cotacaoMercadoDebentureCollection.AttachEntity(cotacaoMercadoDebenture);
                #endregion
            }

            cotacaoMercadoDebentureCollection.Save();            
        }        

        
        /* ---------------------------------------------------------------------------------*/
        /// <summary>
        /// Somente para teste Sinacor Schema
        /// </summary>
        [Obsolete("Funcionou")]
        public void TesteCelso() {

            //VTccmovtoCollection vtccMovtoCollection = new VTccmovtoCollection();
            //vtccMovtoCollection.es.Connection.Name = "Sinacor";

            ////vtccMovtoCollection.es.Connection.Catalog = "SINACOR";
            //vtccMovtoCollection.es.Connection.Schema = "SINACOR";

            //vtccMovtoCollection.Query.Select(vtccMovtoCollection.Query.CdCliente.As("IdCliente"),
            //                                 vtccMovtoCollection.Query.DtLiquidacao.As("DtLiquidacao"),
            //                                 vtccMovtoCollection.Query.DsLancamento.As("Descricao"),
            //                                 vtccMovtoCollection.Query.VlLancamento.As("ValorLancamento"));

            //vtccMovtoCollection.Query.Load();

            //
            //Outra query
            PapelRendaFixaCollection s = new PapelRendaFixaCollection();
            s.Query.Where(s.Query.IdPapel == 1);
            s.Query.Load();
        }
    }
}
