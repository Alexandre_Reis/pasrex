using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.ContaCorrente;
using Financial.ContaCorrente.Enums;
using Financial.RendaFixa.Enums;
using Financial.RendaFixa.Exceptions;
using Financial.Investidor;
using Financial.Util;
using Financial.Investidor.Enums;
using Financial.Interfaces.Sinacor;
using Financial.Common;
using Financial.InterfacesDB;
using Financial.Interfaces.Export.RendaFixa;
using Financial.CRM;
using Financial.CRM.Enums;
using Financial.Util.Enums;
using System.Collections;
using Financial.Fundo;
using Financial.Common.Enums;
using Financial.RendaFixa.Controller;

namespace Financial.RendaFixa
{
    public partial class OperacaoRendaFixa : esOperacaoRendaFixa
    {
        /// <summary>
        /// Retorna o valor de mercado, de acordo com os filtros passados. Usado para apuraÃ§Ã£o das regras
        /// de enquadramento.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="operacaoCompromissada"></param>
        /// <param name="idIndice"></param>
        /// <param name="idPapel"></param>
        /// <param name="idEmissor"></param>
        /// <param name="tipoEmissor"></param>
        /// <param name="idSetor"></param>
        /// <param name="tipoPapel"></param>
        /// <returns></returns>
        public decimal RetornaValorEnquadra(int idCliente, DateTime data, string operacaoCompromissada, int? idIndice,
                                                   int? idPapel, int? idEmissor, int? tipoEmissor, int? idSetor,
                                                   int? tipoPapel)
        {
            OperacaoRendaFixaCollection operacaoRendaFixaCollection = new OperacaoRendaFixaCollection();

            operacaoRendaFixaCollection.Query
                 .Select(operacaoRendaFixaCollection.Query.IdTitulo,
                         operacaoRendaFixaCollection.Query.TipoOperacao,
                         operacaoRendaFixaCollection.Query.Valor.Sum())
                 .Where(operacaoRendaFixaCollection.Query.IdCliente.Equal(idCliente),
                        operacaoRendaFixaCollection.Query.DataOperacao.Equal(data))
                 .GroupBy(operacaoRendaFixaCollection.Query.IdTitulo,
                          operacaoRendaFixaCollection.Query.TipoOperacao);

            operacaoRendaFixaCollection.Query.Load();

            decimal totalValor = 0;

            for (int i = 0; i < operacaoRendaFixaCollection.Count; i++)
            {
                OperacaoRendaFixa operacaoRendaFixa = operacaoRendaFixaCollection[i];
                int idTituloComparar = operacaoRendaFixa.IdTitulo.Value;
                int tipoOperacao = operacaoRendaFixa.TipoOperacao.Value;
                decimal valor = operacaoRendaFixa.Valor.Value;

                if (tipoOperacao == (byte)TipoOperacaoTitulo.VendaFinal ||
                    tipoOperacao == (byte)TipoOperacaoTitulo.VendaRecompra ||
                    tipoOperacao == (byte)TipoOperacaoTitulo.VendaTotal)
                {
                    valor = valor * -1;
                }

                bool passou = true;
                if (!String.IsNullOrEmpty(operacaoCompromissada))
                {
                    if (operacaoCompromissada == "S" && (tipoOperacao != (int)TipoOperacaoTitulo.CompraRevenda ||
                                                         tipoOperacao != (int)TipoOperacaoTitulo.VendaRecompra))
                        passou = false;
                    else if (operacaoCompromissada == "N" && tipoOperacao != (int)TipoOperacaoTitulo.CompraFinal)
                        passou = false;
                }
                else if (idIndice.HasValue || idPapel.HasValue || idEmissor.HasValue)
                {
                    #region Busca os campos de TituloRendaFixa
                    TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
                    List<esQueryItem> campos = new List<esQueryItem>();
                    campos.Add(tituloRendaFixa.Query.IdIndice);
                    campos.Add(tituloRendaFixa.Query.IdPapel);
                    campos.Add(tituloRendaFixa.Query.IdEmissor);
                    tituloRendaFixa.LoadByPrimaryKey(campos, idTituloComparar);

                    int idIndiceComparar = tituloRendaFixa.IdIndice.Value;
                    int idPapelComparar = tituloRendaFixa.IdPapel.Value;
                    int idEmissorComparar = tituloRendaFixa.IdEmissor.Value;
                    #endregion

                    #region Verifica se passa pelos filtros de Indice, Papel, Emissor
                    if (idIndice.HasValue)
                    {
                        if (idIndiceComparar != idIndice)
                            passou = false;
                    }

                    if (idPapel.HasValue)
                    {
                        if (idPapelComparar != idPapel)
                            passou = false;
                    }

                    if (idEmissor.HasValue)
                    {
                        if (idEmissorComparar != idPapel)
                            passou = false;
                    }
                    #endregion

                    #region Busca os campos de Setor
                    if (idSetor.HasValue || tipoEmissor.HasValue)
                    {
                        //Busca antes o IdEmissor
                        tituloRendaFixa = new TituloRendaFixa();
                        campos = new List<esQueryItem>();
                        campos.Add(tituloRendaFixa.Query.IdEmissor);
                        tituloRendaFixa.LoadByPrimaryKey(campos, idTituloComparar);
                        idEmissorComparar = tituloRendaFixa.IdEmissor.Value;
                        //

                        Emissor emissor = new Emissor();
                        campos = new List<esQueryItem>();
                        campos.Add(emissor.Query.IdSetor);
                        campos.Add(emissor.Query.TipoEmissor);
                        emissor.LoadByPrimaryKey(campos, idEmissorComparar);
                        int idSetorComparar = emissor.IdSetor.Value;
                        int tipoEmissorComparar = emissor.TipoEmissor.Value;

                        if (idSetor.HasValue)
                        {
                            if (idSetorComparar != idSetor)
                                passou = false;
                        }

                        if (tipoEmissor.HasValue)
                        {
                            if (tipoEmissorComparar != tipoEmissor)
                                passou = false;
                        }

                    }
                    #endregion
                }

                if (passou)
                    totalValor += valor;
            }

            return totalValor;
        }

        /// <summary>
        /// Processa todas as compras finais, gerando as posições correspondentes.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ProcessaCompraFinal(int idCliente, DateTime data)
        {
            CalculoRendaFixa calculoRendaFixa = new CalculoRendaFixa();

            OperacaoRendaFixaCollection operacaoRendaFixaCollection = new OperacaoRendaFixaCollection();
            operacaoRendaFixaCollection.BuscaCompraFinal(idCliente, data);
            PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();



            for (int i = 0; i < operacaoRendaFixaCollection.Count; i++)
            {
                OperacaoRendaFixa operacaoRendaFixa = operacaoRendaFixaCollection[i];
                int idTitulo = operacaoRendaFixa.IdTitulo.Value;

                #region Carrega a nova PosicaoRendaFixa
                PosicaoRendaFixa posicaoRendaFixa = new PosicaoRendaFixa();
                posicaoRendaFixa.IdCliente = idCliente;
                posicaoRendaFixa.IdTitulo = operacaoRendaFixa.IdTitulo;
                posicaoRendaFixa.TipoOperacao = (byte)TipoOperacaoTitulo.CompraFinal;

                //Busca o vencimento do titulo
                TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
                tituloRendaFixa.LoadByPrimaryKey(idTitulo);
                DateTime dataVencimento = tituloRendaFixa.DataVencimento.Value;

                if (tituloRendaFixa.DefasagemLiquidacao > 0)
                {
                    int defasagem = calculoRendaFixa.CalculaPrazoLiquidacao(data, operacaoRendaFixa, tituloRendaFixa);
                    dataVencimento = dataVencimento.AddDays(-defasagem);
                }

                //
                posicaoRendaFixa.DataVencimento = dataVencimento;
                posicaoRendaFixa.Quantidade = operacaoRendaFixa.Quantidade.Value;
                posicaoRendaFixa.QuantidadeBloqueada = 0;
                posicaoRendaFixa.DataOperacao = operacaoRendaFixa.DataOperacao.Value;
                posicaoRendaFixa.DataLiquidacao = operacaoRendaFixa.DataLiquidacao.Value;
                posicaoRendaFixa.PUOperacao = operacaoRendaFixa.PUOperacao.Value;
                posicaoRendaFixa.PUCurva = operacaoRendaFixa.PUOperacao.Value;
                posicaoRendaFixa.ValorCurva = operacaoRendaFixa.Valor.Value;
                posicaoRendaFixa.PUMercado = operacaoRendaFixa.PUOperacao.Value;
                posicaoRendaFixa.ValorMercado = operacaoRendaFixa.Valor.Value;
                posicaoRendaFixa.PUJuros = operacaoRendaFixa.PUOperacao.Value;
                posicaoRendaFixa.ValorJuros = 0;
                posicaoRendaFixa.PUCorrecao = operacaoRendaFixa.PUOperacao.Value;
                posicaoRendaFixa.ValorCorrecao = 0;
                posicaoRendaFixa.TaxaOperacao = operacaoRendaFixa.TaxaOperacao;
                posicaoRendaFixa.QuantidadeInicial = operacaoRendaFixa.Quantidade.Value;
                posicaoRendaFixa.ValorIR = 0;
                posicaoRendaFixa.ValorIOF = 0;
                posicaoRendaFixa.TipoNegociacao = operacaoRendaFixa.TipoNegociacao.Value;
                posicaoRendaFixa.PUVolta = 0;
                posicaoRendaFixa.ValorVolta = 0;
                posicaoRendaFixa.IdAgente = operacaoRendaFixa.IdAgenteCustodia;
                posicaoRendaFixa.IdCorretora = operacaoRendaFixa.IdAgenteCorretora;
                posicaoRendaFixa.IdCustodia = operacaoRendaFixa.IdCustodia;
                posicaoRendaFixa.IdOperacao = operacaoRendaFixa.IdOperacao;
                posicaoRendaFixa.OperacaoTermo = operacaoRendaFixa.OperacaoTermo;

                posicaoRendaFixaCollection.AttachEntity(posicaoRendaFixa);
                #endregion
            }

            posicaoRendaFixaCollection.Save();
        }

        /// <summary>
        /// Processa todas as compras com revenda e venda com recompra, criando as posiÃ§Ãµes respectivas.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ProcessaOperacaoCompromissada(int idCliente, DateTime data)
        {
            OperacaoRendaFixaCollection operacaoRendaFixaCollection = new OperacaoRendaFixaCollection();
            operacaoRendaFixaCollection.BuscaOperacaoCompromissada(idCliente, data);
            PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();

            for (int i = 0; i < operacaoRendaFixaCollection.Count; i++)
            {
                OperacaoRendaFixa operacaoRendaFixa = operacaoRendaFixaCollection[i];

                #region Carrega a nova PosicaoRendaFixa
                PosicaoRendaFixa posicaoRendaFixa = new PosicaoRendaFixa();
                posicaoRendaFixa.IdCliente = idCliente;
                posicaoRendaFixa.IdTitulo = operacaoRendaFixa.IdTitulo;
                posicaoRendaFixa.TipoOperacao = operacaoRendaFixa.TipoOperacao;
                posicaoRendaFixa.DataVencimento = operacaoRendaFixa.DataVolta; //Assume a dataVcto a propria data da volta
                if (operacaoRendaFixa.TipoOperacao == (int)TipoOperacaoTitulo.VendaRecompra)
                {
                    posicaoRendaFixa.Quantidade = operacaoRendaFixa.Quantidade * -1;
                }
                else
                {
                    posicaoRendaFixa.Quantidade = operacaoRendaFixa.Quantidade;
                }
                posicaoRendaFixa.QuantidadeBloqueada = 0;
                posicaoRendaFixa.DataOperacao = operacaoRendaFixa.DataOperacao;
                posicaoRendaFixa.DataLiquidacao = operacaoRendaFixa.DataLiquidacao;
                posicaoRendaFixa.PUOperacao = operacaoRendaFixa.PUOperacao.Value;
                posicaoRendaFixa.PUCurva = operacaoRendaFixa.PUOperacao.Value;
                posicaoRendaFixa.PUJuros = operacaoRendaFixa.PUOperacao.Value;
                posicaoRendaFixa.ValorJuros = 0;
                posicaoRendaFixa.PUCorrecao = operacaoRendaFixa.PUOperacao.Value;
                posicaoRendaFixa.ValorCorrecao = 0;
                posicaoRendaFixa.TaxaOperacao = operacaoRendaFixa.TaxaOperacao.Value;

                decimal valor = Utilitario.Truncate(operacaoRendaFixa.PUOperacao.Value * operacaoRendaFixa.Quantidade.Value, 2);
                posicaoRendaFixa.ValorCurva = valor;

                posicaoRendaFixa.PUMercado = operacaoRendaFixa.PUOperacao;
                posicaoRendaFixa.ValorMercado = valor;
                posicaoRendaFixa.DataVolta = operacaoRendaFixa.DataVolta;
                posicaoRendaFixa.TaxaVolta = operacaoRendaFixa.TaxaVolta;
                posicaoRendaFixa.PUVolta = operacaoRendaFixa.PUVolta;
                posicaoRendaFixa.ValorVolta = operacaoRendaFixa.ValorVolta;
                posicaoRendaFixa.IdIndiceVolta = operacaoRendaFixa.IdIndiceVolta;
                posicaoRendaFixa.QuantidadeInicial = operacaoRendaFixa.Quantidade;
                posicaoRendaFixa.ValorIR = 0;
                posicaoRendaFixa.ValorIOF = 0;
                posicaoRendaFixa.TipoNegociacao = (int)TipoNegociacaoTitulo.Negociacao;
                posicaoRendaFixa.IdAgente = operacaoRendaFixa.IdAgenteCustodia;
                posicaoRendaFixa.IdCustodia = operacaoRendaFixa.IdCustodia;
                posicaoRendaFixa.IdCorretora = operacaoRendaFixa.IdAgenteCorretora;
                posicaoRendaFixa.IdOperacao = operacaoRendaFixa.IdOperacao;

                posicaoRendaFixaCollection.AttachEntity(posicaoRendaFixa);
                #endregion
            }

            posicaoRendaFixaCollection.Save();
        }

        /// <summary>
        /// Processa todas as vendas finais e antecipações de revenda/recompra, baixando as posições respectivas.
        /// Calcula o IR, IOF e rendimento incidentes na operação.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ProcessaLiquidacaoPosicao(int idCliente, DateTime data)
        {
            DetalhePosicaoAfetadaRFCollection detalhePosicaoColl = new DetalhePosicaoAfetadaRFCollection();

            List<byte> lstSemMovFinanceiro = new List<byte>();
            lstSemMovFinanceiro.Add((byte)TipoOperacaoTitulo.Retirada);
            lstSemMovFinanceiro.Add((byte)TipoOperacaoTitulo.RetiradaAtivoImpactoCota);
            lstSemMovFinanceiro.Add((byte)TipoOperacaoTitulo.RetiradaAtivoImpactoQtde);

            LiquidacaoRendaFixaCollection liquidacaoRendaFixaCollectionDeletar = new LiquidacaoRendaFixaCollection();
            liquidacaoRendaFixaCollectionDeletar.Query.Select(liquidacaoRendaFixaCollectionDeletar.Query.IdLiquidacao);
            liquidacaoRendaFixaCollectionDeletar.Query.Where(liquidacaoRendaFixaCollectionDeletar.Query.IdCliente.Equal(idCliente),
                                                             liquidacaoRendaFixaCollectionDeletar.Query.DataLiquidacao.GreaterThanOrEqual(data),
                                                             liquidacaoRendaFixaCollectionDeletar.Query.Status.NotIn((byte)StatusLiquidacaoRendaFixa.LiberadoComAjuste,
                                                             (byte)StatusLiquidacaoRendaFixa.LiberadoSemAjuste,
                                                                                                                     (byte)StatusLiquidacaoRendaFixa.Ajustado),
                                                             liquidacaoRendaFixaCollectionDeletar.Query.TipoLancamento.In((byte)TipoLancamentoLiquidacao.Venda,
                                                                                                                          (byte)TipoLancamentoLiquidacao.Revenda,
                                                                                                                          (byte)TipoLancamentoLiquidacao.Recompra));
            liquidacaoRendaFixaCollectionDeletar.Query.Load();
            liquidacaoRendaFixaCollectionDeletar.MarkAllAsDeleted();
            liquidacaoRendaFixaCollectionDeletar.Save();

            LiquidacaoRendaFixaCollection liquidacaoRendaFixaCollection = new LiquidacaoRendaFixaCollection();
            OperacaoRendaFixaCollection operacaoRendaFixaCollection = new OperacaoRendaFixaCollection();
            operacaoRendaFixaCollection.BuscaLiquidacaoPosicao(idCliente, data);

            Cliente cliente = new Cliente();
            ClienteRendaFixa clienteRendaFixa = new ClienteRendaFixa();
            int tipoCliente = 0;
            if (operacaoRendaFixaCollection.Count > 0)
            {
                clienteRendaFixa.LoadByPrimaryKey(idCliente);

                cliente.LoadByPrimaryKey(idCliente);
            }


            ExcecoesTributacaoIRCollection excecaoColl = new ExcecoesTributacaoIRCollection();
            if (operacaoRendaFixaCollection.Count > 0)
            {
                excecaoColl.RetornaExcessaoTributacaoIR(cliente.UpToPessoaByIdPessoa.Tipo, data, (int)TipoMercado.RendaFixa);
            }
            for (int i = 0; i < operacaoRendaFixaCollection.Count; i++)
            {
                OperacaoRendaFixa operacaoRendaFixa = operacaoRendaFixaCollection[i];

                byte tipoOperacao = operacaoRendaFixa.TipoOperacao.Value;
                int idTitulo = operacaoRendaFixa.IdTitulo.Value;
                decimal puOperacao = operacaoRendaFixa.PUOperacao.Value;
                decimal puPremio = 0;

                byte tipoOperacaoPosicao = 0;
                if (tipoOperacao == (byte)TipoOperacaoTitulo.VendaFinal ||
                    tipoOperacao == (byte)TipoOperacaoTitulo.VendaTotal ||
                    tipoOperacao == (byte)TipoOperacaoTitulo.RetiradaAtivoImpactoCota ||
                    tipoOperacao == (byte)TipoOperacaoTitulo.ExercicioOpcao ||
                    tipoOperacao == (byte)TipoOperacaoTitulo.RetiradaAtivoImpactoQtde ||
                    tipoOperacao == (byte)TipoOperacaoTitulo.Retirada)
                {
                    tipoOperacaoPosicao = (byte)TipoOperacaoTitulo.CompraFinal;
                }
                else if (tipoOperacao == (byte)TipoOperacaoTitulo.AntecipacaoRevenda)
                {
                    tipoOperacaoPosicao = (byte)TipoOperacaoTitulo.CompraRevenda;
                }
                else if (tipoOperacao == (byte)TipoOperacaoTitulo.AntecipacaoRecompra)
                {
                    tipoOperacaoPosicao = (byte)TipoOperacaoTitulo.VendaRecompra;
                }

                TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
                tituloRendaFixa.LoadByPrimaryKey(idTitulo);

                TituloIsentoIR tituloIsentoIR = (TituloIsentoIR)tituloRendaFixa.IsentoIR.Value;

                bool grossUp = cliente.GrossUP.Value != (byte)GrossupCliente.NaoFaz && tituloIsentoIR != TituloIsentoIR.NaoIsento;

                bool isentoCliente = clienteRendaFixa.IsIsentoIR() ||
                                        (tituloRendaFixa.IsentoIR.Value == (byte)TituloIsentoIR.IsentoPF && cliente.IdTipo.Value == (byte)TipoClienteFixo.ClientePessoaFisica &&
                                         cliente.GrossUP.Value != (byte)GrossupCliente.SempreFaz);


                decimal quantidadeOperacao = 0;
                decimal quantidadeResgatar = 0;
                if (tipoOperacao != (byte)TipoOperacaoTitulo.VendaTotal)
                {
                    quantidadeOperacao = operacaoRendaFixa.Quantidade.Value;
                    quantidadeResgatar = operacaoRendaFixa.Quantidade.Value;

                    if (tipoOperacao == (byte)TipoOperacaoTitulo.AntecipacaoRecompra)
                    {
                        quantidadeResgatar = quantidadeResgatar * -1;
                    }

                }

                int? idPosicao = operacaoRendaFixa.IdPosicaoResgatada;
                int? idOperacaoResgatada = operacaoRendaFixa.IdOperacaoResgatada;

                //Busca todas as posições compradas para o idTitulo
                PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
                posicaoRendaFixaCollection.BuscaPosicao(idCliente, idTitulo, null, idOperacaoResgatada, tipoOperacaoPosicao);
                //                
                decimal iofOperacao = 0;
                decimal irOperacao = 0;
                decimal rendimentoOperacao = 0;
                bool opcaoEmbutida = tituloRendaFixa.OpcaoEmbutida.Equals("S");
                for (int j = 0; j < posicaoRendaFixaCollection.Count; j++)
                {
                    PosicaoRendaFixa posicaoRendaFixa = posicaoRendaFixaCollection[j];
                    ExcecoesTributacaoIR excecao = new ExcecoesTributacaoIR();

                    decimal quantidadePosicao = posicaoRendaFixa.Quantidade.Value;

                    if (opcaoEmbutida)
                    {
                        puOperacao = posicaoRendaFixa.PUCurva.Value;
                        puPremio = operacaoRendaFixa.PUOperacao.Value;

                        //Feito para o cálculo de IR/IOF da posição
                        operacaoRendaFixa.PUOperacao = puOperacao;
                    }

                    if (puOperacao == 0)
                    {
                        puOperacao = posicaoRendaFixa.PUMercado.Value; //Caso o PU da operação não seja informado, assume o PU da própria posição
                    }

                    if (quantidadeOperacao == 0 && tipoOperacao != (byte)TipoOperacaoTitulo.VendaTotal && puOperacao != 0)
                    {
                        quantidadeOperacao = operacaoRendaFixa.Valor.Value / puOperacao;
                    }

                    if (tipoOperacao == (byte)TipoOperacaoTitulo.VendaTotal)
                    {
                        quantidadeOperacao += quantidadePosicao;
                    }

                    decimal quantidadeBaixar;
                    #region Calcula quantidadeBaixar, deduz quantidadeResgatar
                    if (tipoOperacao == (byte)TipoOperacaoTitulo.VendaTotal)
                    {
                        quantidadeBaixar = quantidadePosicao;
                    }
                    else
                    {
                        if (Math.Abs(quantidadeResgatar) > Math.Abs(quantidadePosicao))
                        {
                            quantidadeBaixar = quantidadePosicao;
                            quantidadeResgatar -= quantidadePosicao;
                        }
                        else
                        {
                            quantidadeBaixar = quantidadeResgatar;
                            quantidadeResgatar = 0;
                        }
                    }
                    #endregion

                    decimal iof = 0;
                    decimal ir = 0;
                    decimal rendimento = 0;
                    if (operacaoRendaFixa.Status.Value == (byte)StatusOperacaoRendaFixa.LiberadoComAjuste ||
                        operacaoRendaFixa.Status.Value == (byte)StatusOperacaoRendaFixa.LiberadoSemAjuste ||
                        operacaoRendaFixa.Status.Value == (byte)StatusOperacaoRendaFixa.Ajustado)
                    {
                        iof = operacaoRendaFixa.ValorIOF.Value;
                        ir = operacaoRendaFixa.ValorIR.Value;
                        rendimento = operacaoRendaFixa.Rendimento.Value;
                    }
                    else
                    {
                        #region Cálculo do IR, IOF, rendimento da posição baixada
                        if (tipoOperacao != (byte)TipoOperacaoTitulo.Retirada &&
                            tipoOperacao != (byte)TipoOperacaoTitulo.VendaCasada &&
                            tipoOperacao != (byte)TipoOperacaoTitulo.AntecipacaoRecompra)
                        {
                            PosicaoRendaFixa posicaoRendaFixaAjustadaAux = posicaoRendaFixaCollection[j];
                            //
                            PosicaoRendaFixa posicaoRendaFixaAjustada = new PosicaoRendaFixa();
                            foreach (esColumnMetadata colPosicao in posicaoRendaFixaAjustadaAux.es.Meta.Columns)
                            {
                                posicaoRendaFixaAjustada.SetColumn(colPosicao.Name, posicaoRendaFixaAjustadaAux.GetColumn(colPosicao.Name));
                            }

                            if (tipoOperacao != (byte)TipoOperacaoTitulo.AntecipacaoRevenda)
                            {
                                //Ajusta o PU de custo pelas amortizações havidas desde a compra
                                AgendaRendaFixa agendaRendaFixa = new AgendaRendaFixa();
                                decimal totalAmortizacao = agendaRendaFixa.RetornaTotalAmortizadoPU(tituloRendaFixa, data, posicaoRendaFixa.DataOperacao.Value);

                                posicaoRendaFixaAjustada.PUOperacao = posicaoRendaFixaAjustada.PUOperacao.Value - totalAmortizacao;
                            }

                            CalculoRendaFixa calculoRendaFixa = new CalculoRendaFixa();
                            iof = calculoRendaFixa.RetornaIOFOperacao(cliente, clienteRendaFixa, posicaoRendaFixaAjustada.PUMercado.Value,
                                                                              posicaoRendaFixaAjustada, quantidadeBaixar, data);

                            excecao = excecaoColl.ObtemPrioridade(cliente.UpToPessoaByIdPessoa.Tipo, (int)TipoMercado.RendaFixa, tituloRendaFixa.IdPapel.Value, tituloRendaFixa.IdTitulo.Value);

                            ir = calculoRendaFixa.RetornaIROperacao(cliente, clienteRendaFixa, operacaoRendaFixa,
                                                                            posicaoRendaFixaAjustada, quantidadeBaixar, data, iof, excecao);

                            rendimento = Utilitario.Truncate(quantidadeBaixar * (puOperacao - posicaoRendaFixaAjustada.PUOperacao.Value), 2);
                        }
                        #endregion
                    }

                    iofOperacao += iof;
                    irOperacao += ir;
                    rendimentoOperacao += rendimento;

                    #region Atualiza em posicaoRendaFixa Quantidade, ValorMercado, ValorIR, ValorIOF
                    posicaoRendaFixa.Quantidade -= quantidadeBaixar;
                    posicaoRendaFixa.ValorMercado = Math.Round(posicaoRendaFixa.Quantidade.Value * posicaoRendaFixa.PUMercado.Value, 2);

                    if (posicaoRendaFixa.ValorMercado.Value == 0)
                    {
                        posicaoRendaFixa.Quantidade = 0;
                    }

                    if (posicaoRendaFixa.Quantidade.Value == 0)
                    {
                        posicaoRendaFixa.ValorIR = 0;
                        posicaoRendaFixa.ValorIOF = 0;
                    }
                    else
                    {
                        posicaoRendaFixa.ValorIR -= Math.Round(posicaoRendaFixa.ValorIR.Value * posicaoRendaFixa.Quantidade.Value / quantidadePosicao, 2);
                        posicaoRendaFixa.ValorIOF -= Math.Round(posicaoRendaFixa.ValorIOF.Value * posicaoRendaFixa.Quantidade.Value / quantidadePosicao, 2);
                    }
                    #endregion

                    #region Lanca em LiquidacaoRendaFixa
                    if (operacaoRendaFixa.Status.Value != (byte)StatusOperacaoRendaFixa.LiberadoSemAjuste &&
                        operacaoRendaFixa.Status.Value != (byte)StatusOperacaoRendaFixa.LiberadoComAjuste &&
                        operacaoRendaFixa.Status.Value != (byte)StatusOperacaoRendaFixa.Ajustado)
                        if (!lstSemMovFinanceiro.Contains(tipoOperacao))
                        {
                            byte tipoLancamento = 0;
                            if (opcaoEmbutida)
                            {
                                tipoLancamento = (byte)TipoLancamentoLiquidacao.PremioPosicao;
                            }
                            else if (tipoOperacao == (byte)TipoOperacaoTitulo.AntecipacaoRevenda)
                            {
                                tipoLancamento = (byte)TipoLancamentoLiquidacao.Revenda;
                            }
                            else if (tipoOperacao == (byte)TipoOperacaoTitulo.AntecipacaoRecompra)
                            {
                                tipoLancamento = (byte)TipoLancamentoLiquidacao.Recompra;
                            }
                            else
                            {
                                tipoLancamento = (byte)TipoLancamentoLiquidacao.Venda;
                            }

                            decimal valorPosicaoBaixada = Math.Round(quantidadeBaixar * puOperacao, 2);
                            LiquidacaoRendaFixa liquidacaoRendaFixa = liquidacaoRendaFixaCollection.AddNew();
                            liquidacaoRendaFixa.DataLiquidacao = data;
                            liquidacaoRendaFixa.IdCliente = idCliente;
                            liquidacaoRendaFixa.IdPosicaoResgatada = posicaoRendaFixa.IdPosicao.Value;
                            liquidacaoRendaFixa.IdTitulo = idTitulo;
                            liquidacaoRendaFixa.PULiquidacao = puOperacao;
                            liquidacaoRendaFixa.Quantidade = quantidadeBaixar;
                            liquidacaoRendaFixa.Rendimento = rendimento;
                            liquidacaoRendaFixa.TipoLancamento = tipoLancamento;
                            liquidacaoRendaFixa.ValorBruto = valorPosicaoBaixada;
                            liquidacaoRendaFixa.ValorIOF = iof;
                            liquidacaoRendaFixa.ValorIR = ir;
                            liquidacaoRendaFixa.IdOperacaoVenda = operacaoRendaFixa.IdOperacao.Value;

                            if (grossUp && !isentoCliente)
                            {
                                liquidacaoRendaFixa.ValorLiquido = valorPosicaoBaixada;
                                liquidacaoRendaFixa.ValorBruto += (ir + iof);

                            }
                            else
                            {
                                liquidacaoRendaFixa.ValorLiquido = valorPosicaoBaixada - ir - iof;
                            }

                            //Lança liquidação referente ao prêmio
                            if (opcaoEmbutida)
                            {
                                LiquidacaoRendaFixa liquidacaoRendaFixaPremio = liquidacaoRendaFixaCollection.AddNew();
                                liquidacaoRendaFixaPremio.DataLiquidacao = data;
                                liquidacaoRendaFixaPremio.IdCliente = idCliente;
                                liquidacaoRendaFixaPremio.IdPosicaoResgatada = posicaoRendaFixa.IdPosicao.Value;
                                liquidacaoRendaFixaPremio.IdTitulo = idTitulo;
                                liquidacaoRendaFixaPremio.PULiquidacao = puPremio;
                                liquidacaoRendaFixaPremio.Quantidade = quantidadeBaixar;
                                liquidacaoRendaFixaPremio.Rendimento = puPremio * quantidadeBaixar;
                                liquidacaoRendaFixaPremio.TipoLancamento = (byte)TipoLancamentoLiquidacao.Premio;
                                liquidacaoRendaFixaPremio.ValorBruto = puPremio * quantidadeBaixar;
                                liquidacaoRendaFixaPremio.ValorIOF = 0;
                                liquidacaoRendaFixaPremio.IdOperacaoVenda = operacaoRendaFixa.IdOperacao.Value;

                                ir = 0;
                                decimal? aliquotaExcecaoTributaria = null;
                                if (!ControllerRendaFixa.ClienteIsentoIR(cliente, data, clienteRendaFixa, tituloRendaFixa, excecao, operacaoRendaFixa.TipoOperacao.Value, out aliquotaExcecaoTributaria))
                                {
                                    decimal rendaTotal = liquidacaoRendaFixaPremio.Rendimento.Value;
                                    int prazoTotal = Calendario.NumeroDias(posicaoRendaFixa.DataOperacao.Value, data);

                                    if (aliquotaExcecaoTributaria.HasValue)
                                    {
                                        ir = rendaTotal * aliquotaExcecaoTributaria.Value;
                                    }
                                    else
                                    {
                                        if (prazoTotal <= 180)
                                            ir = rendaTotal * 0.225M;
                                        else if (prazoTotal <= 360)
                                            ir = rendaTotal * 0.2M;
                                        else if (prazoTotal <= 720)
                                            ir = rendaTotal * 0.175M;
                                        else
                                            ir = rendaTotal * 0.15M;
                                    }
                                }
                                liquidacaoRendaFixaPremio.ValorIR = ir;
                                liquidacaoRendaFixaPremio.ValorLiquido = liquidacaoRendaFixaPremio.Rendimento.Value - ir;
                            }
                        }
                    #endregion

                    DetalhePosicaoAfetadaRF detalhePosicao = detalhePosicaoColl.AddNew();
                    detalhePosicao.DataOperacao = data;
                    detalhePosicao.IdOperacao = operacaoRendaFixa.IdOperacao.Value;
                    detalhePosicao.IdPosicaoAfetada = posicaoRendaFixa.IdPosicao.Value;
                    detalhePosicao.IdCliente = idCliente;
                    detalhePosicao.QtdeMovimentada = quantidadeBaixar;
                    detalhePosicao.PuOperacao = puOperacao;

                    if (quantidadeResgatar == 0 && tipoOperacao != (byte)TipoOperacaoTitulo.VendaTotal)
                    {
                        break;
                    }
                }

                if (quantidadeResgatar > 0) //Quando houver a distorção de se lançar uma venda com mais qtde do que suporta a posição, ajusta a qtde da operação
                {
                    quantidadeOperacao -= quantidadeResgatar;
                }

                operacaoRendaFixa.Valor = operacaoRendaFixa.ValorLiquido = Math.Round(quantidadeOperacao * puOperacao, 2);

                #region Faz o tratamento para valorBruto e valorLiquido e atualiza em operacaoRendaFixa para o caso de grossup
                if (grossUp && !isentoCliente)
                    operacaoRendaFixa.Valor = operacaoRendaFixa.ValorLiquido + irOperacao + iofOperacao;

                else
                    operacaoRendaFixa.ValorLiquido = operacaoRendaFixa.Valor.Value - irOperacao - iofOperacao;
                if (!isentoCliente)
                {
                    operacaoRendaFixa.ValorLiquido = operacaoRendaFixa.Valor.Value - irOperacao - iofOperacao;
                }
                #endregion

                if (opcaoEmbutida)
                {
                    operacaoRendaFixa.ValorIOF = 0;
                    operacaoRendaFixa.ValorIR = 0;
                    operacaoRendaFixa.Quantidade = quantidadeOperacao;
                    operacaoRendaFixa.PUOperacao = puPremio;
                    operacaoRendaFixa.Rendimento = quantidadeOperacao * puPremio;
                }
                else
                {
                    operacaoRendaFixa.ValorIOF = iofOperacao;
                    operacaoRendaFixa.ValorIR = irOperacao;
                    operacaoRendaFixa.Rendimento = rendimentoOperacao;
                    operacaoRendaFixa.Quantidade = quantidadeOperacao;
                    operacaoRendaFixa.PUOperacao = puOperacao;
                }

                posicaoRendaFixaCollection.Save();
                operacaoRendaFixaCollection.Save();
                liquidacaoRendaFixaCollection.Save();
                detalhePosicaoColl.Save();

            }

            if (operacaoRendaFixaCollection.Count > 0)
            {
                PosicaoRendaFixaCollection posicaoRendaFixaCollectionDeletar = new PosicaoRendaFixaCollection();
                posicaoRendaFixaCollectionDeletar.Query.Select(posicaoRendaFixaCollectionDeletar.Query.IdPosicao);
                posicaoRendaFixaCollectionDeletar.Query.Where(posicaoRendaFixaCollectionDeletar.Query.IdCliente.Equal(idCliente),
                                                              posicaoRendaFixaCollectionDeletar.Query.Quantidade.Equal(0));
                posicaoRendaFixaCollectionDeletar.Query.Load();

                if (posicaoRendaFixaCollectionDeletar.Count > 0)
                {
                    //Tratar custodia anual
                    TabelaCustosRendaFixa tabelaCustosRendaFixa = new TabelaCustosRendaFixa();
                    tabelaCustosRendaFixa.TrataPosicoesLiquidadas(idCliente, data, posicaoRendaFixaCollectionDeletar);
                }
            }
        }

        /// <summary>
        /// Lança em Liquidacao os valores das operações finais e de compromisso do dia, bem como as revendas e recompras.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void LancaCCOperacao(int idCliente, DateTime data)
        {
            //Guarda as liquidaÃ§Ãµes provenientes das compras e vendas do dia
            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
            //

            OperacaoRendaFixaCollection operacaoRendaFixaCollection = new OperacaoRendaFixaCollection();
            operacaoRendaFixaCollection.BuscaOperacaoAgrupadoLiquidacao(idCliente, data);

            Hashtable hsContaAtivo = new Hashtable();

            for (int i = 0; i < operacaoRendaFixaCollection.Count; i++)
            {
                OperacaoRendaFixa operacaoRendaFixa = operacaoRendaFixaCollection[i];

                int idTitulo = operacaoRendaFixa.IdTitulo.Value;
                DateTime dataLiquidacao = operacaoRendaFixa.DataLiquidacao.Value;
                int tipoOperacao = operacaoRendaFixa.TipoOperacao.Value;
                decimal valorLiquido = operacaoRendaFixa.ValorLiquido.Value;
                decimal valorIR = operacaoRendaFixa.ValorIR.Value;
                decimal valorIOF = operacaoRendaFixa.ValorIOF.Value;
                decimal valorCorretagem = operacaoRendaFixa.ValorCorretagem.Value;
                decimal valor = operacaoRendaFixa.Valor.Value;

                if (valorLiquido != 0 &&
                    (tipoOperacao == (int)TipoOperacaoTitulo.VendaFinal || tipoOperacao == (int)TipoOperacaoTitulo.VendaTotal))
                {
                    valor = valorLiquido + valorIR + valorIOF;
                }

                //Busca a descrição do título
                TituloRendaFixa titulo = new TituloRendaFixa();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(titulo.Query.Descricao);
                campos.Add(titulo.Query.IdMoeda);
                titulo.LoadByPrimaryKey(campos, idTitulo);
                string descricaoTitulo = titulo.Descricao;
                int idMoedaAtivo = titulo.IdMoeda.Value;
                //

                int idContaDefault = 0;
                #region Busca Conta Default
                if (hsContaAtivo.Contains(idTitulo))
                {
                    idContaDefault = (int)hsContaAtivo[idTitulo];
                }
                else
                {
                    Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                    idContaDefault = contaCorrente.RetornaContaDefault(idCliente, idMoedaAtivo);
                    hsContaAtivo.Add(idTitulo, idContaDefault);
                }
                #endregion

                Liquidacao liquidacao = new Liquidacao();

                liquidacao = new Liquidacao();
                liquidacao.DataLancamento = data;
                liquidacao.DataVencimento = dataLiquidacao;

                string descricao = "";
                int origem = OrigemLancamentoLiquidacao.RendaFixa.None;
                switch (tipoOperacao)
                {
                    case (int)TipoOperacaoTitulo.CompraFinal:
                        descricao = "Compra Final - " + descricaoTitulo;
                        origem = OrigemLancamentoLiquidacao.RendaFixa.CompraFinal;
                        valor = valor * -1;
                        break;
                    case (int)TipoOperacaoTitulo.VendaFinal:
                        descricao = "Venda Final - " + descricaoTitulo;
                        origem = OrigemLancamentoLiquidacao.RendaFixa.VendaFinal;
                        break;
                    case (int)TipoOperacaoTitulo.VendaTotal:
                        descricao = "Venda Total - " + descricaoTitulo;
                        origem = OrigemLancamentoLiquidacao.RendaFixa.VendaFinal;
                        break;
                    case (int)TipoOperacaoTitulo.CompraRevenda:
                        descricao = "Compra c/ Revenda - " + descricaoTitulo;
                        origem = OrigemLancamentoLiquidacao.RendaFixa.CompraRevenda;
                        valor = valor * -1;
                        break;
                    case (int)TipoOperacaoTitulo.VendaRecompra:
                        descricao = "Venda c/ Recompra - " + descricaoTitulo;
                        origem = OrigemLancamentoLiquidacao.RendaFixa.VendaRecompra;
                        break;
                    case (int)TipoOperacaoTitulo.AntecipacaoRevenda:
                        descricao = "Antecipação de Revenda - " + descricaoTitulo;
                        origem = OrigemLancamentoLiquidacao.RendaFixa.Revenda;
                        break;
                    case (int)TipoOperacaoTitulo.ExercicioOpcao:
                        descricao = "Exercicio de Opção (Posição) - " + descricaoTitulo;
                        origem = OrigemLancamentoLiquidacao.RendaFixa.ExercicioOpcao;
                        break;
                }

                liquidacao.Descricao = descricao;
                liquidacao.Valor = valor;
                liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                liquidacao.Origem = origem;
                liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                liquidacao.IdCliente = idCliente;

                liquidacao.IdConta = ParametrosConfiguracaoSistema.Outras.MultiConta && operacaoRendaFixa.IdConta.HasValue && operacaoRendaFixa.IdConta.Value > 0 ? operacaoRendaFixa.IdConta.Value : idContaDefault;

                liquidacaoCollection.AttachEntity(liquidacao);

                if (valorIR != 0)
                {
                    string descricaoLancamento = "";
                    if (tipoOperacao == (int)TipoOperacaoTitulo.AntecipacaoRevenda)
                    {
                        descricaoLancamento = "IR sobre Revenda - " + descricaoTitulo;
                    }
                    else
                    {
                        descricaoLancamento = "IR sobre Venda - " + descricaoTitulo;
                    }

                    liquidacao = new Liquidacao();
                    liquidacao.DataLancamento = data;
                    liquidacao.DataVencimento = dataLiquidacao;
                    liquidacao.Descricao = descricaoLancamento;
                    liquidacao.Valor = valorIR * -1;
                    liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                    liquidacao.Origem = OrigemLancamentoLiquidacao.RendaFixa.IR;
                    liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                    liquidacao.IdCliente = idCliente;
                    liquidacao.IdConta = ParametrosConfiguracaoSistema.Outras.MultiConta && operacaoRendaFixa.IdConta.HasValue && operacaoRendaFixa.IdConta.Value > 0 ? operacaoRendaFixa.IdConta.Value : idContaDefault;

                    liquidacaoCollection.AttachEntity(liquidacao);
                }

                if (valorIOF != 0)
                {
                    string descricaoLancamento = "";
                    if (tipoOperacao == (int)TipoOperacaoTitulo.AntecipacaoRevenda)
                    {
                        descricaoLancamento = "IOF sobre Revenda - " + descricaoTitulo;
                    }
                    else
                    {
                        descricaoLancamento = "IOF sobre Venda - " + descricaoTitulo;
                    }

                    liquidacao = new Liquidacao();
                    liquidacao.DataLancamento = data;
                    liquidacao.DataVencimento = dataLiquidacao;
                    liquidacao.Descricao = descricaoLancamento;
                    liquidacao.Valor = valorIOF * -1;
                    liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                    liquidacao.Origem = OrigemLancamentoLiquidacao.RendaFixa.IOF;
                    liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                    liquidacao.IdCliente = idCliente;
                    liquidacao.IdConta = ParametrosConfiguracaoSistema.Outras.MultiConta && operacaoRendaFixa.IdConta.HasValue && operacaoRendaFixa.IdConta.Value > 0 ? operacaoRendaFixa.IdConta.Value : idContaDefault;

                    liquidacaoCollection.AttachEntity(liquidacao);
                }

                if (valorCorretagem != 0)
                {
                    string descricaoLancamento = "";
                    if (tipoOperacao == (int)TipoOperacaoTitulo.AntecipacaoRevenda)
                    {
                        descricaoLancamento = "Corretagem sobre Revenda - " + descricaoTitulo;
                    }
                    else
                    {
                        descricaoLancamento = "Corretagem sobre Venda - " + descricaoTitulo;
                    }

                    liquidacao = new Liquidacao();
                    liquidacao.DataLancamento = data;
                    liquidacao.DataVencimento = dataLiquidacao;
                    liquidacao.Descricao = descricaoLancamento;
                    liquidacao.Valor = valorCorretagem * -1;
                    liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                    liquidacao.Origem = OrigemLancamentoLiquidacao.RendaFixa.Corretagem;
                    liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                    liquidacao.IdCliente = idCliente;
                    liquidacao.IdConta = ParametrosConfiguracaoSistema.Outras.MultiConta && operacaoRendaFixa.IdConta.HasValue && operacaoRendaFixa.IdConta.Value > 0 ? operacaoRendaFixa.IdConta.Value : idContaDefault;

                    liquidacaoCollection.AttachEntity(liquidacao);
                }
            }

            liquidacaoCollection.Save();
        }

        /// <summary>
        /// Lança em Liquidacao o net das operações casadas do dia.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void LancaCCOperacaoCasada(int idCliente, DateTime data)
        {
            //Guarda as liquidações provenientes do net de operações casadas
            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
            //

            OperacaoRendaFixaCollection operacaoRendaFixaCollection = new OperacaoRendaFixaCollection();
            operacaoRendaFixaCollection.BuscaOperacaoCasada(idCliente, data);
            Hashtable hsContaAtivo = new Hashtable();
            for (int i = 0; i < operacaoRendaFixaCollection.Count; i++)
            {
                OperacaoRendaFixa operacaoRendaFixa = operacaoRendaFixaCollection[i];
                int idTitulo = operacaoRendaFixa.IdTitulo.Value;
                decimal valor = operacaoRendaFixa.Valor.Value;

                int idContaOperacao = operacaoRendaFixa.IdConta.Value;
                int idOperacao = operacaoRendaFixa.IdOperacao.Value;

                //Busca a descrição do título
                TituloRendaFixa titulo = new TituloRendaFixa();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(titulo.Query.Descricao);
                campos.Add(titulo.Query.IdMoeda);
                titulo.LoadByPrimaryKey(campos, idTitulo);
                string descricaoTitulo = titulo.Descricao;
                int idMoedaAtivo = titulo.IdMoeda.Value;
                //

                int idContaDefault = 0;
                #region Busca Conta Default
                if (hsContaAtivo.Contains(idTitulo))
                {
                    idContaDefault = (int)hsContaAtivo[idTitulo];
                }
                else
                {
                    Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                    idContaDefault = contaCorrente.RetornaContaDefault(idCliente, idMoedaAtivo);
                    hsContaAtivo.Add(idTitulo, idContaDefault);
                }
                #endregion

                Liquidacao liquidacao = new Liquidacao();

                liquidacao = new Liquidacao();
                liquidacao.DataLancamento = data;
                liquidacao.DataVencimento = data;
                liquidacao.Descricao = "Net de operações casadas do título - " + descricaoTitulo;
                liquidacao.Valor = valor;
                liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                liquidacao.Origem = OrigemLancamentoLiquidacao.RendaFixa.NetOperacaoCasada;
                liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                liquidacao.IdCliente = idCliente;

                liquidacaoCollection.AttachEntity(liquidacao);
            }

            liquidacaoCollection.Save();
        }

        /// <summary>
        /// Retorna o valor total operado de compras.
        /// Filtrado por: TipoOperacao.Equal(TipoOperacaoTitulo.CompraFinal)
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="idAtivo"></param>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <returns></returns>
        public decimal RetornaTotalCompras(int idCliente, int idTitulo, DateTime dataInicio, DateTime dataFim)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.Valor.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.IdTitulo == idTitulo,
                        this.Query.DataOperacao.Between(dataInicio, dataFim),
                        this.Query.TipoOperacao.In((byte)TipoOperacaoTitulo.CompraFinal));

            this.Query.Load();

            return this.Valor.HasValue ? this.Valor.Value : 0;
        }

        /// <summary>
        /// Retorna o valor total operado de compras.
        /// Filtrado por: TipoOperacao.Equal(TipoOperacaoTitulo.VendaFinal)
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="idAtivo"></param>
        /// <param name="dataInicio"></param>
        /// <param name="dataFim"></param>
        /// <returns></returns>
        public decimal RetornaTotalVendas(int idCliente, int idTitulo, DateTime dataInicio, DateTime dataFim)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.Valor.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.IdTitulo == idTitulo,
                        this.Query.DataOperacao.Between(dataInicio, dataFim),
                        this.Query.TipoOperacao.In((byte)TipoOperacaoTitulo.VendaFinal,
                                                   (byte)TipoOperacaoTitulo.VendaTotal));

            this.Query.Load();

            return this.Valor.HasValue ? this.Valor.Value : 0;
        }

        /// <summary>
        /// Integra operações do Tesouro Direto do Sinacor.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public void IntegraOperacoesSinacor(int idCliente, DateTime data)
        {
            OperacaoRendaFixaCollection operacaoRendaFixaCollection = new OperacaoRendaFixaCollection();

            OperacaoRendaFixaCollection operacaoRendaFixaCollectionDeletar = new OperacaoRendaFixaCollection();
            operacaoRendaFixaCollectionDeletar.Query.Select(operacaoRendaFixaCollectionDeletar.Query.IdOperacao);
            operacaoRendaFixaCollectionDeletar.Query.Where(operacaoRendaFixaCollectionDeletar.Query.IdCliente.Equal(idCliente),
                                                           operacaoRendaFixaCollectionDeletar.Query.DataOperacao.Equal(data),
                                                           operacaoRendaFixaCollectionDeletar.Query.Fonte.Equal((byte)FonteOperacaoTitulo.Sinacor));
            operacaoRendaFixaCollectionDeletar.Query.Load();
            operacaoRendaFixaCollectionDeletar.MarkAllAsDeleted();
            operacaoRendaFixaCollectionDeletar.Save();


            ClienteRendaFixa clienteRendaFixa = new ClienteRendaFixa();
            if (!clienteRendaFixa.LoadByPrimaryKey(idCliente))
                return;

            string codigoInterface = clienteRendaFixa.CodigoInterface.Replace(";", ",").Trim();

            if (codigoInterface == "")
                return;

            string[] codigoSplit1 = codigoInterface.Split(new char[] { ',' });

            List<int> lista = new List<int>();

            foreach (string codigo in codigoSplit1)
            {
                //Se não é número, lança exception
                if (!Utilitario.IsInteger(codigo))
                {
                    throw new Exception("Código Renda Fixa inválido para o IdCliente " + idCliente);
                }

                if (!lista.Contains(Convert.ToInt32(codigo)))
                {
                    lista.Add(Convert.ToInt32(codigo));
                }
            }

            VcfmoviTediCollection vcfmoviTediCollection = new VcfmoviTediCollection();
            vcfmoviTediCollection.BuscaOperacoesSINACOR(lista, data);

            DateTime dataLiquidacao = new DateTime();

            if (vcfmoviTediCollection.Count > 0)
            {
                dataLiquidacao = Calendario.AdicionaDiaUtil(data, 1);
            }

            foreach (VcfmoviTedi vcfmoviTedi in vcfmoviTediCollection)
            {
                DateTime dataVencimento = vcfmoviTedi.DataVenc.Value;
                int codigoSelic = (int)vcfmoviTedi.CodSeli.Value;

                int idTitulo = 0;
                TituloRendaFixaCollection tituloRendaFixaCollection = new TituloRendaFixaCollection();
                tituloRendaFixaCollection.Query.Select(tituloRendaFixaCollection.Query.IdTitulo);
                tituloRendaFixaCollection.Query.Where(tituloRendaFixaCollection.Query.CodigoCustodia.Equal(codigoSelic.ToString()),
                                                      tituloRendaFixaCollection.Query.DataVencimento.Equal(dataVencimento));
                tituloRendaFixaCollection.Query.Load();

                if (tituloRendaFixaCollection.Count > 0)
                {
                    idTitulo = tituloRendaFixaCollection[0].IdTitulo.Value;
                }
                else
                {
                    TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
                    tituloRendaFixa.CriaTituloPublico(codigoSelic, dataVencimento, data, null);

                    idTitulo = tituloRendaFixa.IdTitulo.Value;
                }


                OperacaoRendaFixa operacaoRendaFixa = operacaoRendaFixaCollection.AddNew();
                operacaoRendaFixa.DataRegistro = data;
                operacaoRendaFixa.DataOperacao = data;
                operacaoRendaFixa.DataLiquidacao = dataLiquidacao;
                operacaoRendaFixa.Fonte = (byte)FonteOperacaoTitulo.Sinacor;
                operacaoRendaFixa.IdCliente = idCliente;
                operacaoRendaFixa.IdCustodia = (byte)LocalCustodiaFixo.CBLC;
                operacaoRendaFixa.IdLiquidacao = (byte)ClearingFixo.CBLC;
                operacaoRendaFixa.IdTitulo = idTitulo;
                operacaoRendaFixa.PUOperacao = Math.Abs(vcfmoviTedi.PrecTrns.Value);
                operacaoRendaFixa.Quantidade = Math.Abs(vcfmoviTedi.QtdeTitu.Value);
                operacaoRendaFixa.Rendimento = vcfmoviTedi.ValRes.Value;
                operacaoRendaFixa.TaxaNegociacao = vcfmoviTedi.ValTaxaAgen.Value; //Não utilizado no sistema hoje!!!!
                operacaoRendaFixa.TaxaOperacao = 0;
                operacaoRendaFixa.TipoNegociacao = (byte)TipoNegociacaoTitulo.Negociacao;

                //Hoje estamos pegando o valor já descontado da taxa CBLC, deve ser ajustado depois pois o resultado (base do IR) é calculado em cima do valor sem desconto da taxa CBLC
                operacaoRendaFixa.Valor = Math.Abs(vcfmoviTedi.ValTrns.Value);

                decimal valorTotal = Math.Abs(vcfmoviTedi.ValTot.Value);

                decimal corretagem = Math.Abs(valorTotal - operacaoRendaFixa.Valor.Value);

                if (vcfmoviTedi.DescHistMvto == "COMPRA")
                {
                    operacaoRendaFixa.TipoOperacao = (byte)TipoOperacaoTitulo.CompraFinal;
                }
                else if (vcfmoviTedi.DescHistMvto == "VENDA")
                {
                    operacaoRendaFixa.TipoOperacao = (byte)TipoOperacaoTitulo.VendaFinal;
                }

                operacaoRendaFixa.ValorCorretagem = corretagem;
                //
                operacaoRendaFixa.ValorIOF = Math.Abs(vcfmoviTedi.ValIof.Value);
                operacaoRendaFixa.ValorIR = Math.Abs(vcfmoviTedi.ValIr.Value);
                operacaoRendaFixa.ValorLiquido = operacaoRendaFixa.Valor.Value - operacaoRendaFixa.ValorIOF.Value - operacaoRendaFixa.ValorIR.Value;
            }

            operacaoRendaFixaCollection.Save();
        }

        /// <summary>
        /// Integra operações do sistema Virtual.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public void IntegraOperacoesVirtual(int idCliente, DateTime data)
        {
            OperacaoRendaFixaCollection operacaoRendaFixaCollection = new OperacaoRendaFixaCollection();

            OperacaoRendaFixaCollection operacaoRendaFixaCollectionDeletar = new OperacaoRendaFixaCollection();
            operacaoRendaFixaCollectionDeletar.Query.Select(operacaoRendaFixaCollectionDeletar.Query.IdOperacao);
            operacaoRendaFixaCollectionDeletar.Query.Where(operacaoRendaFixaCollectionDeletar.Query.IdCliente.Equal(idCliente),
                                                           operacaoRendaFixaCollectionDeletar.Query.DataOperacao.Equal(data),
                                                           operacaoRendaFixaCollectionDeletar.Query.Fonte.Equal((byte)FonteOperacaoTitulo.OpenVirtual));
            operacaoRendaFixaCollectionDeletar.Query.Load();
            operacaoRendaFixaCollectionDeletar.MarkAllAsDeleted();
            operacaoRendaFixaCollectionDeletar.Save();


            ClienteRendaFixa clienteRendaFixa = new ClienteRendaFixa();
            if (!clienteRendaFixa.LoadByPrimaryKey(idCliente))
                return;

            string codigoInterface = clienteRendaFixa.CodigoInterface.Replace(";", ",").Trim();

            if (codigoInterface == "")
                return;

            string[] codigoSplit = codigoInterface.Split(new char[] { ',' });

            List<string> lista = new List<string>();
            lista.Add(codigoSplit[0].PadLeft(9, '0'));


            OpdiabolCollection opdiabolCollection = new OpdiabolCollection();
            opdiabolCollection.BuscaOperacoesVirtual(lista[0], data);

            OphisbolCollection ophisbolCollection = new OphisbolCollection();
            ophisbolCollection.BuscaOperacoesVirtual(lista[0], data);

            OpdiabolCollection opdiabolCollectionAux = new OpdiabolCollection(ophisbolCollection);
            opdiabolCollection.Combine(opdiabolCollectionAux);

            foreach (Opdiabol opdiabol in opdiabolCollection)
            {
                string codigoTitulo = opdiabol.FcCodtitulo;

                int idTitulo = 0;

                TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
                tituloRendaFixa.TrataTituloVirtual(codigoTitulo);


                OperacaoRendaFixa operacaoRendaFixa = operacaoRendaFixaCollection.AddNew();
                operacaoRendaFixa.DataRegistro = data;
                operacaoRendaFixa.DataOperacao = data;
                operacaoRendaFixa.DataLiquidacao = opdiabol.FdDatliquidacao.Value;
                operacaoRendaFixa.Fonte = (byte)FonteOperacaoTitulo.OpenVirtual;
                operacaoRendaFixa.IdCliente = idCliente;
                operacaoRendaFixa.IdCustodia = (byte)LocalCustodiaFixo.CBLC; //Aparentemente não tem no Virtual
                operacaoRendaFixa.IdLiquidacao = (byte)ClearingFixo.CBLC; //Aparentemente não tem no Virtual
                operacaoRendaFixa.IdTitulo = tituloRendaFixa.IdTitulo.Value; ;
                operacaoRendaFixa.PUOperacao = (decimal)opdiabol.FnPuoper.Value;
                operacaoRendaFixa.Quantidade = (decimal)opdiabol.FnQtdeoper.Value;
                //operacaoRendaFixa.Rendimento = vcfmoviTedi.ValRes.Value;
                operacaoRendaFixa.TaxaNegociacao = 0; //Aparentemente nÃ£o tem no Virtual
                operacaoRendaFixa.TaxaOperacao = 0;
                operacaoRendaFixa.TipoNegociacao = (byte)TipoNegociacaoTitulo.Negociacao;
                operacaoRendaFixa.IdLocalNegociacao = (byte)LocalNegociacaoFixo.Brasil;

                operacaoRendaFixa.Valor = (decimal)opdiabol.FnValorbruto.Value;

                if (opdiabol.FcTpoper == "V-FINAL")
                {
                    operacaoRendaFixa.TipoOperacao = (byte)TipoOperacaoTitulo.CompraFinal;
                }
                else if (opdiabol.FcTpoper == "RESGATE")
                {
                    operacaoRendaFixa.TipoOperacao = (byte)TipoOperacaoTitulo.VendaFinal;
                }

                operacaoRendaFixa.ValorCorretagem = 0; //Aparentemente nÃ£o tem no Virtual
                //
                operacaoRendaFixa.ValorIOF = (decimal)opdiabol.FnFir.Value;
                operacaoRendaFixa.ValorIR = (decimal)opdiabol.FnIof.Value;
                operacaoRendaFixa.ValorLiquido = operacaoRendaFixa.Valor.Value - operacaoRendaFixa.ValorIOF.Value - operacaoRendaFixa.ValorIR.Value;
            }

            operacaoRendaFixaCollection.Save();
        }

        public OperacaoIPOCorretoraCetipViewModel ExportaIPOCorretoraCetip()
        {
            const string ID_SISTEMA = "MDA";
            const int TIPO_REGISTRO_HEADER = 0;
            const int TIPO_REGISTRO_OPERACAO = 1;
            const string CODIGO_TIPO_OPERACAO = "0452";
            const int TIPO_DEB = 1;
            const int TIPO_LF = 5;
            const int TIPO_CRI = 7;
            const int TIPO_CRA = 8;
            const int MODALIDADE_LIQUIDACAO_BRUTA = 0;
            const string CONTA_CETIP = "3412";//Todo: meter como campos no agente mercado
            int CONTA_AGENTE_MERCADO = Convert.ToInt32(CONTA_CETIP + "10" + "7");//Todo: como tem digito verificador precisamos meter como fields no agente mercado
            const string PAPEL_COMPRADOR = "0";

            OperacaoIPOCorretoraCetipViewModel operacaoIPOCorretoraCetipViewModel = new OperacaoIPOCorretoraCetipViewModel();

            operacaoIPOCorretoraCetipViewModel.header.IdSistema = ID_SISTEMA;
            operacaoIPOCorretoraCetipViewModel.header.IdTipoLinha = TIPO_REGISTRO_HEADER;
            operacaoIPOCorretoraCetipViewModel.header.CodigoTipoOperacao = CODIGO_TIPO_OPERACAO;
            operacaoIPOCorretoraCetipViewModel.header.IdSistema = ID_SISTEMA;
            operacaoIPOCorretoraCetipViewModel.header.Data = this.DataOperacao.Value;


            RegistroOperacaoIPOCorretoraCetip registro = new RegistroOperacaoIPOCorretoraCetip();
            registro.IdSistema = ID_SISTEMA;
            registro.IdTipoLinha = TIPO_REGISTRO_OPERACAO;
            registro.CodigoTipoOperacao = CODIGO_TIPO_OPERACAO;
            int sequencial = this.IdOperacao.Value;
            registro.MeuNumero = sequencial;
            registro.PapelParticipante = Convert.ToInt32(PAPEL_COMPRADOR);

            TituloRendaFixa titulo = this.UpToTituloRendaFixaByIdTitulo;
            PapelRendaFixa papel = titulo.UpToPapelRendaFixaByIdPapel;

            if (papel.Classe == (int)ClasseRendaFixa.LF)
            {
                registro.TipoAtivo = TIPO_LF;
            }
            else if (papel.Classe == (int)ClasseRendaFixa.Debenture)
            {
                registro.TipoAtivo = TIPO_DEB;
            }
            else if (papel.Classe == (int)ClasseRendaFixa.CRI)
            {
                registro.TipoAtivo = TIPO_CRI;
            }
            else if (papel.Classe == (int)ClasseRendaFixa.CRA)
            {
                registro.TipoAtivo = TIPO_CRA;
            }

            registro.CodigoAtivo = titulo.CodigoCetip;
            registro.Parte = CONTA_AGENTE_MERCADO;

            //
            if (this.IdAgenteContraParte.HasValue)
            {
                AgenteMercado agenteMercadoContraparte = new AgenteMercado();
                agenteMercadoContraparte.LoadByPrimaryKey(this.IdAgenteContraParte.Value);
                registro.ContraParte = int.Parse(agenteMercadoContraparte.CodigoCetip);
            }
            else if (this.IdCarteiraContraparte.HasValue)
            {
                Carteira carteiraContraparte = new Carteira();
                carteiraContraparte.LoadByPrimaryKey(this.IdCarteiraContraparte.Value);
                registro.ContraParte = int.Parse(carteiraContraparte.CodigoCetip);
            }
            else
            {
                registro.ContraParte = 0;
            }

            registro.QuantidadeOperacaoInteiro = Math.Truncate(this.Quantidade.Value);
            registro.QuantidadeOperacaoDecimal = this.Quantidade.Value - Math.Truncate(this.Quantidade.Value);
            registro.QuantidadeOperacaoDecimal = Convert.ToDecimal(Convert.ToString(registro.QuantidadeOperacaoDecimal).Replace("0.", "").Replace("0,", ""));

            registro.PUOperacaoInteiro = Math.Truncate(this.PUOperacao.Value);
            registro.PUOperacaoDecimal = this.PUOperacao.Value - Math.Truncate(this.PUOperacao.Value);
            registro.PUOperacaoDecimal = Convert.ToDecimal(Convert.ToString(registro.PUOperacaoDecimal).Replace("0.", "").Replace("0,", ""));

            registro.TaxaDistribuidorInteiro = "";
            registro.TaxaDistribuidorDecimal = "";

            registro.ModalidadeLiquidacao = MODALIDADE_LIQUIDACAO_BRUTA;

            operacaoIPOCorretoraCetipViewModel.registros.Add(registro);

            this.StatusExportacao = (int)StatusExportacaoRendaFixa.Exportado;
            this.Save();

            return operacaoIPOCorretoraCetipViewModel;
        }

        public OperacaoIPOClientesCetipViewModel ExportaIPOClientesCetip()
        {
            const string TIPO_PESSOA_JURIDICA = "PJ";
            const string TIPO_PESSOA_FISICA = "PF";
            const string ID_SISTEMA = "MDA";
            const string CODIGO_ACAO = "ESPE";
            const int TIPO_REGISTRO_HEADER = 0;
            const int TIPO_REGISTRO_OPERACAO = 1;
            const int DESTINO_COLOCACAO_PF = 1; //destino pessoas fisicas
            const string CONTA_CETIP = "3412";//Todo: meter como campos no agente mercado
            int CONTA_CLIENTE = Convert.ToInt32(CONTA_CETIP + "10" + "7");//Todo: como tem digito verificador precisamos meter como fields no agente mercado

            OperacaoIPOClientesCetipViewModel operacaoIPOClientesCetipViewModel = new OperacaoIPOClientesCetipViewModel();

            operacaoIPOClientesCetipViewModel.header.IdSistema = ID_SISTEMA;
            operacaoIPOClientesCetipViewModel.header.IdTipoLinha = TIPO_REGISTRO_HEADER;
            operacaoIPOClientesCetipViewModel.header.CodigoAcao = CODIGO_ACAO;
            operacaoIPOClientesCetipViewModel.header.Data = this.DataOperacao.Value;

            //TODO: Pesquisar todas as operacoes de IPO deste titulo, ignorando a operacao atual (compra corretora)
            /*OperacaoRendaFixaCollection operacoesClientes = new OperacaoRendaFixaCollection();
            operacoesClientes.Query.Where(operacoesClientes.Query.IdTitulo.Equal(this.IdTitulo),
                operacoesClientes.Query.IdOperacao.NotEqual(this.IdOperacao),
                operacoesClientes.Query.IsIPO.Equal("S"));*/

            int codigoBovespa = ParametrosConfiguracaoSistema.Bolsa.CodigoAgenteDefault;
            AgenteMercado agenteMercado = new AgenteMercadoCollection().BuscaAgenteMercadoPorCodigoBovespa(codigoBovespa);

            string cnpjAgenteMercado = Utilitario.RemoveCaracteresEspeciais(agenteMercado.Cnpj);

            OperacaoRendaFixaQuery operacaoRendaFixaQuery = new OperacaoRendaFixaQuery("O");
            ClienteQuery clienteQuery = new ClienteQuery("C");
            PessoaQuery pessoaQuery = new PessoaQuery("E");
            TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");

            operacaoRendaFixaQuery.InnerJoin(clienteQuery).On(operacaoRendaFixaQuery.IdCliente == clienteQuery.IdCliente);
            operacaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(operacaoRendaFixaQuery.IdTitulo == tituloRendaFixaQuery.IdTitulo);
            operacaoRendaFixaQuery.InnerJoin(pessoaQuery).On(pessoaQuery.IdPessoa == clienteQuery.IdCliente);

            operacaoRendaFixaQuery.Where(
                operacaoRendaFixaQuery.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraFinal),
                operacaoRendaFixaQuery.IdTitulo.Equal(this.IdTitulo),
                operacaoRendaFixaQuery.IdCustodia.Equal((byte)CustodiaTitulo.Cetip),
                pessoaQuery.Cpfcnpj.NotEqual(cnpjAgenteMercado),
                operacaoRendaFixaQuery.IsIPO.Equal("S"));

            OperacaoRendaFixaCollection operacoesClientes = new OperacaoRendaFixaCollection();
            operacoesClientes.Load(operacaoRendaFixaQuery);

            foreach (OperacaoRendaFixa operacaoCliente in operacoesClientes)
            {
                Pessoa pessoa = new Pessoa();
                pessoa.LoadByPrimaryKey(operacaoCliente.IdCliente.Value);

                RegistroOperacaoIPOClientesCetip registro = new RegistroOperacaoIPOClientesCetip();

                registro.IdSistema = ID_SISTEMA;
                registro.IdTipoLinha = TIPO_REGISTRO_OPERACAO;
                registro.CodigoAcao = CODIGO_ACAO;
                registro.CodigoOperacao = this.IdOperacaoExterna.Value;
                registro.QuantidadeOperacaoInteiro = Math.Truncate(operacaoCliente.Quantidade.Value);
                registro.QuantidadeOperacaoDecimal = operacaoCliente.Quantidade.Value - Math.Truncate(operacaoCliente.Quantidade.Value);
                registro.QuantidadeOperacaoDecimal = Convert.ToDecimal(Convert.ToString(registro.QuantidadeOperacaoDecimal).Replace("0.", "").Replace("0,", ""));
                registro.ContaCliente = CONTA_CLIENTE;
                registro.TipoComitente = (pessoa.Tipo == (byte)TipoPessoa.Fisica) ?
                    TIPO_PESSOA_FISICA : TIPO_PESSOA_JURIDICA;
                registro.CpfcnpjCliente = Utilitario.RemoveCaracteresEspeciais(pessoa.Cpfcnpj);
                registro.DestinoColocacao = DESTINO_COLOCACAO_PF;
                registro.CodigoAgendamento = "";

                operacaoIPOClientesCetipViewModel.registros.Add(registro);

                operacaoCliente.StatusExportacao = (int)StatusExportacaoRendaFixa.Exportado;
            }

            operacoesClientes.Save();

            return operacaoIPOClientesCetipViewModel;
        }

        public void LancaCCOperacaoPremio(int idCliente, DateTime data)
        {
            LiquidacaoRendaFixaCollection liquidacaoColl = new LiquidacaoRendaFixaCollection();
            liquidacaoColl.Query.Where(liquidacaoColl.Query.DataLiquidacao.Equal(data)
                                       & liquidacaoColl.Query.IdCliente.Equal(idCliente)
                                       & liquidacaoColl.Query.TipoLancamento.In((byte)TipoLancamentoLiquidacao.PremioPosicao,
                                                                                (byte)TipoLancamentoLiquidacao.Premio));

            if (liquidacaoColl.Query.Load())
            {
                LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
                Hashtable hsContaAtivo = new Hashtable();

                foreach (LiquidacaoRendaFixa liquidacaoRF in liquidacaoColl)
                {
                    TituloRendaFixa tituloRendaFixa = liquidacaoRF.UpToTituloRendaFixaByIdTitulo;

                    string complemento = string.Empty;

                    string descricaoTitulo = tituloRendaFixa.Descricao;
                    int idMoedaAtivo = tituloRendaFixa.IdMoeda.Value;
                    int idTitulo = tituloRendaFixa.IdTitulo.Value;
                    int idOperacao = liquidacaoRF.IdOperacaoVenda.Value;
                    short tipoOpcao = tituloRendaFixa.TipoOpcao.Value;
                    byte tipoLancamento = liquidacaoRF.TipoLancamento.Value;
                    decimal valor = liquidacaoRF.ValorLiquido.Value;
                    decimal valorIR = liquidacaoRF.ValorIR.Value;
                    decimal valorIOF = liquidacaoRF.ValorIOF.Value;

                    if (tipoLancamento == (byte)TipoLancamentoLiquidacao.PremioPosicao)
                        complemento = "Prêmio";
                    else
                        complemento = "Posição";

                    complemento += " - Id.Operação:" + idOperacao;

                    int idContaDefault = 0;
                    #region Busca Conta Default
                    if (hsContaAtivo.Contains(idTitulo))
                    {
                        idContaDefault = (int)hsContaAtivo[idTitulo];
                    }
                    else
                    {
                        Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                        idContaDefault = contaCorrente.RetornaContaDefault(idCliente, idMoedaAtivo);
                        hsContaAtivo.Add(idTitulo, idContaDefault);
                    }
                    #endregion

                    Liquidacao liquidacao = liquidacaoCollection.AddNew();
                    liquidacao.DataLancamento = data;
                    liquidacao.DataVencimento = data;
                    liquidacao.Descricao = "Exercicio de Opção (" + complemento + ") - " + descricaoTitulo;

                    if (tipoLancamento == (byte)TipoLancamentoLiquidacao.PremioPosicao)
                        liquidacao.Valor = valor;
                    else
                        liquidacao.Valor = valor * (tipoOpcao == (short)TipoOpcao.Put ? -1 : 1);
                    liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                    liquidacao.Origem = OrigemLancamentoLiquidacao.RendaFixa.ExercicioOpcao;
                    liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                    liquidacao.IdCliente = idCliente;
                    liquidacao.IdConta = idContaDefault;

                    if (tipoOpcao == (short)TipoOpcao.Call || tipoLancamento == (byte)TipoLancamentoLiquidacao.PremioPosicao)
                    {
                        if (valorIR > 0)
                        {
                            Liquidacao liquidacaoIR = liquidacaoCollection.AddNew();
                            liquidacaoIR.DataLancamento = data;
                            liquidacaoIR.DataVencimento = data;
                            liquidacaoIR.Descricao = "IR sobre " + complemento + " - " + descricaoTitulo;
                            liquidacaoIR.Valor = valorIR;
                            liquidacaoIR.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                            liquidacaoIR.Origem = OrigemLancamentoLiquidacao.RendaFixa.IR;
                            liquidacaoIR.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                            liquidacaoIR.IdCliente = idCliente;
                            liquidacaoIR.IdConta = idContaDefault;
                        }

                        if (valorIOF > 0)
                        {
                            Liquidacao liquidacaoIOF = liquidacaoCollection.AddNew();
                            liquidacaoIOF.DataLancamento = data;
                            liquidacaoIOF.DataVencimento = data;
                            liquidacaoIOF.Descricao = "IOF sobre " + complemento + " - " + descricaoTitulo;
                            liquidacaoIOF.Valor = valorIR;
                            liquidacaoIOF.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                            liquidacaoIOF.Origem = OrigemLancamentoLiquidacao.RendaFixa.IOF;
                            liquidacaoIOF.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                            liquidacaoIOF.IdCliente = idCliente;
                            liquidacaoIOF.IdConta = idContaDefault;
                        }
                    }
                }
                liquidacaoCollection.Save();
            }
        }
    }
}
