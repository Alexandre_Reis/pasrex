using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

namespace Financial.RendaFixa
{
	public partial class AgendaRendaFixaCollection : esAgendaRendaFixaCollection
	{
        /// <summary>
        /// Carrega o objeto AgendaRendaFixaCollection com os campos IdTitulo, TipoEvento.
        /// </summary>
        /// <param name="dataPagamento"></param>
        /// <returns>bool indicando se trouxe registro.</returns>
        public bool BuscaAgenda(DateTime dataPagamento)
        {

            this.QueryReset();
            this.Query
                 .Select(this.Query.IdTitulo, this.Query.TipoEvento)
                 .Where(this.Query.DataPagamento.Equal(dataPagamento));

            this.Query.Load();

            return this.HasData;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idTitulo"></param>
        /// <returns></returns>
        public List<DateTime> BuscaDataFluxo(int idTitulo, DateTime data)
        {
            AgendaRendaFixaCollection agendaRendaFixaCollection = new AgendaRendaFixaCollection();

            agendaRendaFixaCollection.Query.es.Distinct = true;
            agendaRendaFixaCollection.Query.Select(agendaRendaFixaCollection.Query.IdTitulo,
                                                    agendaRendaFixaCollection.Query.DataPagamento)
                                                  .Where(agendaRendaFixaCollection.Query.IdTitulo.Equal(idTitulo) &
                                                  (agendaRendaFixaCollection.Query.Taxa.NotEqual(0) | agendaRendaFixaCollection.Query.Valor.NotEqual(0)) &
                                                   agendaRendaFixaCollection.Query.DataAgenda.GreaterThan(data))
                                                  .OrderBy(agendaRendaFixaCollection.Query.IdTitulo.Ascending,
                                                           agendaRendaFixaCollection.Query.DataPagamento.Ascending);

            if (!agendaRendaFixaCollection.Query.Load())
                return new List<DateTime>();

            List<AgendaRendaFixa> lstAgenda = (List<AgendaRendaFixa>)agendaRendaFixaCollection;
            List<DateTime> lstDatas = lstAgenda.ConvertAll(new Converter<AgendaRendaFixa, DateTime>(delegate(AgendaRendaFixa x) { return x.DataPagamento.Value; }));

            return lstDatas;

        }
	}
}
