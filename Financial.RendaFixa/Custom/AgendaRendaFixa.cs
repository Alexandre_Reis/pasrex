﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.ContaCorrente;
using Financial.ContaCorrente.Enums;
using Financial.Investidor;
using Financial.Util;
using Financial.Util.Enums;
using Financial.Investidor.Enums;
using Financial.Common;
using Financial.Common.Enums;
using Financial.Util.Enums;
using Financial.RendaFixa.Enums;
using Financial.CRM.Enums;
using System.Collections;

namespace Financial.RendaFixa
{
    public partial class AgendaRendaFixa : esAgendaRendaFixa
    {
        /// <summary>
        /// Retorna a data do evento anterior mais próxima à data passada, dado o tipo de evento informado.
        /// Se não tiver nenhuma, retorna a própria data de emissão do título.
        /// </summary>
        /// <param name="dataEvento"></param>
        /// <param name="idTitulo"></param>
        /// <param name="tipoEvento"></param>
        /// <returns></returns>
        public DateTime BuscaDataEventoAnterior(DateTime dataEvento, int idTitulo, int tipoEvento)
        {
            AgendaRendaFixaCollection agendaRendaFixaCollection = new AgendaRendaFixaCollection();

            agendaRendaFixaCollection.QueryReset();

            agendaRendaFixaCollection.Query
                 .Select(agendaRendaFixaCollection.Query.DataEvento)
                 .Where(agendaRendaFixaCollection.Query.DataEvento.LessThan(dataEvento),
                        agendaRendaFixaCollection.Query.IdTitulo == idTitulo,
                        agendaRendaFixaCollection.Query.TipoEvento == tipoEvento)
                 .OrderBy(agendaRendaFixaCollection.Query.DataEvento.Descending);

            agendaRendaFixaCollection.Query.Load();

            DateTime dataRetorno;

            //Se não achou data, pega a data de emissão do título
            if (agendaRendaFixaCollection.HasData)
            {
                dataRetorno = agendaRendaFixaCollection[0].DataEvento.Value;
            }
            else
            {
                TituloRendaFixa titulo = new TituloRendaFixa();
                titulo.LoadByPrimaryKey(idTitulo);

                dataRetorno = titulo.DataEmissao.Value;
            }

            return dataRetorno;
        }

        /// <summary>
        /// Retorna a data do evento posterior mais próxima à data passada, dado o tipo de evento informado.
        /// Se não tiver nenhuma, retorna a própria data de vencimento do título.
        /// </summary>
        /// <param name="dataEvento"></param>
        /// <param name="idTitulo"></param>
        /// <param name="tipoEvento"></param>
        /// <returns></returns>
        public DateTime BuscaDataEventoPosterior(DateTime dataEvento, int idTitulo, int tipoEvento)
        {
            AgendaRendaFixaCollection agendaRendaFixaCollection = new AgendaRendaFixaCollection();

            agendaRendaFixaCollection.QueryReset();

            agendaRendaFixaCollection.Query
                 .Select(agendaRendaFixaCollection.Query.DataEvento)
                 .Where(agendaRendaFixaCollection.Query.DataEvento.LessThan(dataEvento),
                        agendaRendaFixaCollection.Query.IdTitulo == idTitulo,
                        agendaRendaFixaCollection.Query.TipoEvento == tipoEvento)
                 .OrderBy(agendaRendaFixaCollection.Query.DataEvento.Descending);

            agendaRendaFixaCollection.Query.Load();

            DateTime dataRetorno;

            //Se não achou data, pega a data de vencimento do título
            if (agendaRendaFixaCollection.HasData)
            {
                dataRetorno = agendaRendaFixaCollection[0].DataEvento.Value;
            }
            else
            {
                TituloRendaFixa titulo = new TituloRendaFixa();
                titulo.LoadByPrimaryKey(idTitulo);

                dataRetorno = titulo.DataVencimento.Value;
            }

            return dataRetorno;
        }

        /// <summary>
        /// Lança em Liquidacao qq pagamento de parcela de amortização e/ou juros.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void TrataLiquidacaoAgenda(int idCliente, DateTime data, TipoProcessamento tipoProcessamento)
        {
            LiquidacaoRendaFixaCollection liquidacaoRendaFixaCollection = new LiquidacaoRendaFixaCollection();
            liquidacaoRendaFixaCollection.Query.Select(liquidacaoRendaFixaCollection.Query.IdLiquidacao);
            liquidacaoRendaFixaCollection.Query.Where(liquidacaoRendaFixaCollection.Query.IdCliente.Equal(idCliente),
                                                      liquidacaoRendaFixaCollection.Query.DataLiquidacao.GreaterThanOrEqual(data),
                                                      liquidacaoRendaFixaCollection.Query.Status.NotIn((byte)StatusLiquidacaoRendaFixa.LiberadoComAjuste,
                                                                                                        (byte)StatusLiquidacaoRendaFixa.LiberadoSemAjuste,
                                                                                                       (byte)StatusLiquidacaoRendaFixa.Ajustado),
                                                      liquidacaoRendaFixaCollection.Query.TipoLancamento.In((byte)TipoLancamentoLiquidacao.Amortizacao,
                                                                                                            (byte)TipoLancamentoLiquidacao.Juros,
                                                                                                            (byte)TipoLancamentoLiquidacao.PagtoPrincipal));
            liquidacaoRendaFixaCollection.Query.Load();
            liquidacaoRendaFixaCollection.MarkAllAsDeleted();
            liquidacaoRendaFixaCollection.Save();

            Cliente cliente = new Cliente();
            ClienteRendaFixa clienteRendaFixa = new ClienteRendaFixa();

            AgendaRendaFixaQuery agendaRendaFixaQuery = new AgendaRendaFixaQuery("A");
            PosicaoRendaFixaQuery posicaoRendaFixaQuery = new PosicaoRendaFixaQuery("P");
            agendaRendaFixaQuery.Select(agendaRendaFixaQuery.Valor, agendaRendaFixaQuery.Taxa, agendaRendaFixaQuery.DataEvento,
                                        agendaRendaFixaQuery.DataPagamento, agendaRendaFixaQuery.TipoEvento,
                                        posicaoRendaFixaQuery.Quantidade, posicaoRendaFixaQuery.IdPosicao,
                                        posicaoRendaFixaQuery.IdTitulo, posicaoRendaFixaQuery.DataOperacao,
                                        posicaoRendaFixaQuery.PUOperacao, posicaoRendaFixaQuery.ValorIR,
                                        posicaoRendaFixaQuery.OperacaoTermo, posicaoRendaFixaQuery.DataLiquidacao,
                                        posicaoRendaFixaQuery.IdOperacao, agendaRendaFixaQuery.DataAgenda);
            agendaRendaFixaQuery.InnerJoin(posicaoRendaFixaQuery).On(posicaoRendaFixaQuery.IdTitulo == agendaRendaFixaQuery.IdTitulo);
            agendaRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente.Equal(idCliente),
                                       posicaoRendaFixaQuery.Quantidade.NotEqual(0),
                                       posicaoRendaFixaQuery.DataOperacao.NotEqual(data),
                                       agendaRendaFixaQuery.DataEvento.LessThanOrEqual(data),
                                       agendaRendaFixaQuery.DataPagamento.GreaterThanOrEqual(data));

            AgendaRendaFixaCollection agendaRendaFixaCollection = new AgendaRendaFixaCollection();
            agendaRendaFixaCollection.Load(agendaRendaFixaQuery);

            if (agendaRendaFixaCollection.Count > 0)
            {
                List<esQueryItem> camposCliente = new List<esQueryItem>();
                camposCliente.Add(cliente.Query.IdTipo);
                camposCliente.Add(cliente.Query.GrossUP);
                camposCliente.Add(cliente.Query.DescontaTributoPL);
                cliente.LoadByPrimaryKey(camposCliente, idCliente);

                clienteRendaFixa.LoadByPrimaryKey(idCliente);
            }

            #region Carrega Memoria de Cálculo
            List<MemoriaCalculoRendaFixa> lstMemoriaCalculo = new List<MemoriaCalculoRendaFixa>();
            MemoriaCalculoRendaFixaCollection MemoriaCalculoRendaFixaColl = new MemoriaCalculoRendaFixaCollection();
            MemoriaCalculoRendaFixaQuery MemoriaCalculoRendaFixaQuery = new MemoriaCalculoRendaFixaQuery("M");
            agendaRendaFixaQuery = new AgendaRendaFixaQuery("A");
            posicaoRendaFixaQuery = new PosicaoRendaFixaQuery("P");
            MemoriaCalculoRendaFixaQuery.Select(MemoriaCalculoRendaFixaQuery.ValorVencimento,
                                                MemoriaCalculoRendaFixaQuery.IdOperacao,
                                                MemoriaCalculoRendaFixaQuery.TipoEventoTitulo);
            MemoriaCalculoRendaFixaQuery.InnerJoin(posicaoRendaFixaQuery).On(posicaoRendaFixaQuery.IdOperacao.Equal(MemoriaCalculoRendaFixaQuery.IdOperacao));
            MemoriaCalculoRendaFixaQuery.InnerJoin(agendaRendaFixaQuery).On(posicaoRendaFixaQuery.IdTitulo.Equal(agendaRendaFixaQuery.IdTitulo)
                                                                            & agendaRendaFixaQuery.DataEvento.LessThanOrEqual(MemoriaCalculoRendaFixaQuery.DataAtual)
                                                                            & agendaRendaFixaQuery.DataAgenda.Equal(MemoriaCalculoRendaFixaQuery.DataFluxo)
                                                                            & agendaRendaFixaQuery.DataPagamento.GreaterThanOrEqual(MemoriaCalculoRendaFixaQuery.DataAtual));            
            MemoriaCalculoRendaFixaQuery.Where(MemoriaCalculoRendaFixaQuery.IdCliente.Equal(idCliente)
                                               & MemoriaCalculoRendaFixaQuery.TipoPreco.Equal((int)TipoPreco.Curva)
                                               & MemoriaCalculoRendaFixaQuery.DataAtual.Equal(data)                                               
                                               & MemoriaCalculoRendaFixaQuery.TipoProcessamento.Equal((int)tipoProcessamento));

            if (MemoriaCalculoRendaFixaColl.Load(MemoriaCalculoRendaFixaQuery))
                lstMemoriaCalculo = (List<MemoriaCalculoRendaFixa>)MemoriaCalculoRendaFixaColl;
            #endregion

            int i = 0;
            Hashtable hsContaAtivo = new Hashtable();
            foreach (AgendaRendaFixa agendaRendaFixa in agendaRendaFixaCollection)
            {
                int idOperacao = (int)agendaRendaFixaCollection[i].GetColumn(OperacaoRendaFixaMetadata.ColumnNames.IdOperacao);

                DateTime dataEvento = agendaRendaFixa.DataEvento.Value;
                string operacaoTermo = (string)agendaRendaFixaCollection[i].GetColumn(PosicaoRendaFixaMetadata.ColumnNames.OperacaoTermo);
                DateTime dataLiquidacao = (DateTime)agendaRendaFixaCollection[i].GetColumn(PosicaoRendaFixaMetadata.ColumnNames.DataLiquidacao);
                if (operacaoTermo == "S" && dataLiquidacao >= dataEvento)
                {
                    i++;
                    continue;
                }

                int idPosicao = (int)agendaRendaFixaCollection[i].GetColumn(PosicaoRendaFixaMetadata.ColumnNames.IdPosicao);
                int idTitulo = (int)agendaRendaFixaCollection[i].GetColumn(TituloRendaFixaMetadata.ColumnNames.IdTitulo);
                decimal puOperacao = (decimal)agendaRendaFixaCollection[i].GetColumn(PosicaoRendaFixaMetadata.ColumnNames.PUOperacao);
                byte tipoEvento = agendaRendaFixa.TipoEvento.Value;
                DateTime dataPagamento = agendaRendaFixa.DataPagamento.Value;
                DateTime dataOperacao = (DateTime)agendaRendaFixaCollection[i].GetColumn(PosicaoRendaFixaMetadata.ColumnNames.DataOperacao);                
                decimal valor = agendaRendaFixa.Valor.Value;
                decimal taxa = agendaRendaFixa.Taxa.Value;
                decimal quantidade = (decimal)agendaRendaFixaCollection[i].GetColumn(PosicaoRendaFixaMetadata.ColumnNames.Quantidade);

                TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
                tituloRendaFixa.LoadByPrimaryKey(idTitulo);

                bool parametrosAvancados = tituloRendaFixa.ParametrosAvancados.Equals("S");

                PapelRendaFixa papelRendaFixa = new PapelRendaFixa();
                papelRendaFixa.LoadByPrimaryKey(tituloRendaFixa.IdPapel.Value);

                string descricaoTitulo = tituloRendaFixa.Descricao;
                int idMoedaAtivo = tituloRendaFixa.IdMoeda.Value;

                int idContaDefault = 0;
                #region Busca Conta Default
                if (hsContaAtivo.Contains(idTitulo))
                {
                    idContaDefault = (int)hsContaAtivo[idTitulo];
                }
                else
                {
                    Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                    idContaDefault = contaCorrente.RetornaContaDefault(idCliente, idMoedaAtivo);
                    hsContaAtivo.Add(idTitulo, idContaDefault);
                }
                #endregion

                #region Busca conta corrente da operacao

                int idConta = idContaDefault;

                OperacaoRendaFixa orf = new OperacaoRendaFixa();
                if (ParametrosConfiguracaoSistema.Outras.MultiConta && orf.LoadByPrimaryKey(idOperacao))
                {
                    if (orf.IdConta.HasValue && orf.IdConta.Value > 0)
                        idConta = orf.IdConta.Value;
                }

                #endregion

                int origem = 0;
                byte tipoLancamento = 0;
                string descricao = "";
                #region Verifica origem, tipoLancamento, descricao
                if (tipoEvento == (byte)TipoEventoTitulo.Juros)
                {
                    origem = OrigemLancamentoLiquidacao.RendaFixa.Juros;
                    tipoLancamento = (byte)TipoLancamentoLiquidacao.Juros;
                    descricao = "Pagamento Juros " + descricaoTitulo;
                }
                if (tipoEvento == (byte)TipoEventoTitulo.JurosCorrecao)
                {
                    origem = OrigemLancamentoLiquidacao.RendaFixa.Juros;
                    tipoLancamento = (byte)TipoLancamentoLiquidacao.Juros;
                    descricao = "Pagamento Juros + Correção " + descricaoTitulo;
                }
                else if (tipoEvento == (byte)TipoEventoTitulo.PagamentoCorrecao)
                {
                    origem = OrigemLancamentoLiquidacao.RendaFixa.Juros;
                    tipoLancamento = (byte)TipoLancamentoLiquidacao.Juros;
                    descricao = "Pagamento Juros (Correção) " + descricaoTitulo;
                }
                else if (tipoEvento == (byte)TipoEventoTitulo.PagamentoPU)
                {
                    origem = OrigemLancamentoLiquidacao.RendaFixa.Juros;
                    tipoLancamento = (byte)TipoLancamentoLiquidacao.Juros;
                    descricao = "Pagamento Juros " + descricaoTitulo;
                }
                else if (tipoEvento == (byte)TipoEventoTitulo.Amortizacao)
                {
                    origem = OrigemLancamentoLiquidacao.RendaFixa.Amortizacao;
                    tipoLancamento = (byte)TipoLancamentoLiquidacao.Amortizacao;
                    descricao = "Pagamento Amortização " + descricaoTitulo;
                }
                else if (tipoEvento == (byte)TipoEventoTitulo.AmortizacaoCorrigida)
                {
                    origem = OrigemLancamentoLiquidacao.RendaFixa.Amortizacao;
                    tipoLancamento = (byte)TipoLancamentoLiquidacao.Amortizacao;
                    descricao = "Pagamento Amortização + Correção " + descricaoTitulo;
                }
                else if (tipoEvento == (byte)TipoEventoTitulo.PagamentoPrincipal)
                {
                    origem = OrigemLancamentoLiquidacao.RendaFixa.Vencimento;
                    tipoLancamento = (byte)TipoLancamentoLiquidacao.PagtoPrincipal;
                    descricao = "Vencimento " + descricaoTitulo;
                }
                #endregion

                MemoriaCalculoRendaFixa memoriaCalculoRendaFixa = new MemoriaCalculoRendaFixa();
                int eventoMemoriaCalculo = 0;
                if (lstMemoriaCalculo.Count > 0 && parametrosAvancados)
                {
                    List<byte> lstAmortizacao = new List<byte>();
                    lstAmortizacao.Add((byte)TipoEventoTitulo.Amortizacao);
                    lstAmortizacao.Add((byte)TipoEventoTitulo.AmortizacaoCorrigida);

                    List<byte> lstJuros = new List<byte>();
                    lstJuros.Add((byte)TipoEventoTitulo.Juros);
                    lstJuros.Add((byte)TipoEventoTitulo.JurosCorrecao);

                    eventoMemoriaCalculo = agendaRendaFixa.TipoEvento.Value;
                    if (lstAmortizacao.Contains(tipoEvento))
                        eventoMemoriaCalculo = (int)TipoEventoTitulo.Amortizacao;

                    if (lstJuros.Contains(tipoEvento))
                        eventoMemoriaCalculo = (int)TipoEventoTitulo.Juros;

                    List<MemoriaCalculoRendaFixa> lstMemoriaCalculoAux = lstMemoriaCalculo.FindAll(delegate(MemoriaCalculoRendaFixa x) { return x.IdOperacao.Value == (int)idOperacao; });

                    memoriaCalculoRendaFixa = ((List<MemoriaCalculoRendaFixa>)lstMemoriaCalculoAux).Find(delegate(MemoriaCalculoRendaFixa x) { return x.TipoEventoTitulo.Value == (int)eventoMemoriaCalculo; });

                    memoriaCalculoRendaFixa = memoriaCalculoRendaFixa == null ? new MemoriaCalculoRendaFixa() : memoriaCalculoRendaFixa;

                    //Se for CDI+ soma o valor dos juros ao principal
                    if ((tituloRendaFixa.IdIndice.GetValueOrDefault(0) == (short)ListaIndiceFixo.CDI) && tituloRendaFixa.Taxa.GetValueOrDefault(0) != 0)
                    {
                        MemoriaCalculoRendaFixa memoriaCalculoRendaFixaJuros = ((List<MemoriaCalculoRendaFixa>)lstMemoriaCalculoAux).Find(delegate(MemoriaCalculoRendaFixa x) { return x.TipoEventoTitulo.Value == (int)TipoEventoTitulo.Juros; ; });
                        if (memoriaCalculoRendaFixaJuros != null && memoriaCalculoRendaFixaJuros.ValorVencimento.HasValue)
                            memoriaCalculoRendaFixa.ValorVencimento += memoriaCalculoRendaFixaJuros.ValorVencimento.Value;
                    }
                }

                DateTime dataBase = tituloRendaFixa.DataEmissao.Value;
                DateTime dataUltimoJuro = tituloRendaFixa.DataEmissao.Value;
                DateTime dataBaseCorrecao = dataBase;
                Indice indice = new Indice();
                decimal rendimentoPU = 0;
                decimal fatorCorrecao = 1;
                decimal puNominalAmortizado = tituloRendaFixa.PUNominal.Value;

                #region Calcula Fator de Correção
                if (tituloRendaFixa.IdIndice.HasValue)
                {
                    #region Acha data base de correção do indice
                    dataBaseCorrecao = tituloRendaFixa.DataEmissao.Value;
                    AgendaRendaFixaCollection agendaRendaFixaCollectionCorrecao = new AgendaRendaFixaCollection();
                    agendaRendaFixaCollectionCorrecao.Query.Select(agendaRendaFixaCollectionCorrecao.Query.DataEvento);
                    agendaRendaFixaCollectionCorrecao.Query.Where(agendaRendaFixaCollectionCorrecao.Query.IdTitulo.Equal(idTitulo) &
                                                                  agendaRendaFixaCollectionCorrecao.Query.DataEvento.LessThan(dataEvento) &
                                                                     (
                                                                       agendaRendaFixaCollectionCorrecao.Query.TipoEvento.Equal((byte)TipoEventoTitulo.JurosCorrecao) |
                                                                       (agendaRendaFixaCollectionCorrecao.Query.TipoEvento.Equal((byte)TipoEventoTitulo.PagamentoCorrecao) &
                                                                        agendaRendaFixaCollectionCorrecao.Query.Taxa.In(0, 100)
                                                                       )
                                                                      )
                                                                  );
                    agendaRendaFixaCollectionCorrecao.Query.OrderBy(agendaRendaFixaCollectionCorrecao.Query.DataEvento.Descending);
                    agendaRendaFixaCollectionCorrecao.Query.Load();

                    if (agendaRendaFixaCollectionCorrecao.Count > 0)
                    {
                        dataBaseCorrecao = agendaRendaFixaCollectionCorrecao[0].DataEvento.Value;
                    }
                    #endregion

                    indice.LoadByPrimaryKey(tituloRendaFixa.IdIndice.Value);

                    CalculoRendaFixa calculoRendaFixa = new CalculoRendaFixa();
                    fatorCorrecao = calculoRendaFixa.RetornaFatorCorrecao(dataEvento, dataBaseCorrecao, papelRendaFixa, tituloRendaFixa, indice);
                }
                #endregion

                if (memoriaCalculoRendaFixa.ValorVencimento.HasValue && memoriaCalculoRendaFixa.ValorVencimento.Value > 0)
                {
                    valor = memoriaCalculoRendaFixa.ValorVencimento.Value;

                    if (eventoMemoriaCalculo == (int)TipoEventoTitulo.Juros)
                        rendimentoPU = valor;
                }
                else
                {
                    puNominalAmortizado = tituloRendaFixa.PUNominal.Value;
                    #region Ajusta o PU nominal pelas amortizações ocorridas
                    AgendaRendaFixaCollection agendaRendaFixaCollectionAmortizacao = new AgendaRendaFixaCollection();
                    agendaRendaFixaCollectionAmortizacao.Query.Select(agendaRendaFixaCollectionAmortizacao.Query.Taxa,
                                                                      agendaRendaFixaCollectionAmortizacao.Query.Valor);
                    agendaRendaFixaCollectionAmortizacao.Query.Where(agendaRendaFixaCollectionAmortizacao.Query.IdTitulo.Equal(idTitulo),
                                                          agendaRendaFixaCollectionAmortizacao.Query.DataEvento.LessThan(dataEvento),
                                                          agendaRendaFixaCollectionAmortizacao.Query.TipoEvento.In((byte)TipoEventoTitulo.Amortizacao,
                                                                                                                   (byte)TipoEventoTitulo.AmortizacaoCorrigida));
                    agendaRendaFixaCollectionAmortizacao.Query.Load();

                    foreach (AgendaRendaFixa agendaRendaFixaAmortizacao in agendaRendaFixaCollectionAmortizacao)
                    {
                        if (agendaRendaFixaAmortizacao.Taxa.HasValue && agendaRendaFixaAmortizacao.Taxa.Value != 0)
                        {
                            decimal taxaAmortizacao = agendaRendaFixaAmortizacao.Taxa.Value;
                            puNominalAmortizado = puNominalAmortizado - tituloRendaFixa.PUNominal.Value * taxaAmortizacao / 100M;
                        }
                        else if (agendaRendaFixaAmortizacao.Valor.HasValue)
                        {
                            puNominalAmortizado = puNominalAmortizado - agendaRendaFixaAmortizacao.Valor.Value;
                        }
                    }
                    #endregion

                    dataUltimoJuro = tituloRendaFixa.DataEmissao.Value;
                    #region Acha data do último juro
                    AgendaRendaFixaCollection agendaRendaFixaCollectionJuros = new AgendaRendaFixaCollection();
                    agendaRendaFixaCollectionJuros.Query.Select(agendaRendaFixaCollectionJuros.Query.DataPagamento);
                    agendaRendaFixaCollectionJuros.Query.Where(agendaRendaFixaCollectionJuros.Query.IdTitulo.Equal(tituloRendaFixa.IdTitulo.Value),
                                                          agendaRendaFixaCollectionJuros.Query.DataPagamento.LessThan(dataEvento),
                                                          agendaRendaFixaCollectionJuros.Query.TipoEvento.In((byte)TipoEventoTitulo.Juros,
                                                                                                             (byte)TipoEventoTitulo.JurosCorrecao));
                    agendaRendaFixaCollectionJuros.Query.OrderBy(agendaRendaFixaCollectionJuros.Query.DataEvento.Descending);
                    agendaRendaFixaCollectionJuros.Query.Load();

                    if (agendaRendaFixaCollectionJuros.Count > 0)
                    {
                        dataUltimoJuro = agendaRendaFixaCollectionJuros[0].DataPagamento.Value;
                    }
                    #endregion

                    rendimentoPU = 0;
                    //Se a taxa for informada para os juros, calculo o valor a ser pago, senão assume o valor informado
                    if (tipoEvento == (byte)TipoEventoTitulo.Amortizacao || tipoEvento == (byte)TipoEventoTitulo.AmortizacaoCorrigida)
                    {
                        if (taxa != 0 && valor == 0)
                        {
                            valor = tituloRendaFixa.PUNominal.Value * taxa / 100M;
                        }

                        if (tipoEvento == (byte)TipoEventoTitulo.AmortizacaoCorrigida)
                        {
                            valor = valor * fatorCorrecao;
                        }
                    }
                    else if (tipoEvento == (byte)TipoEventoTitulo.PagamentoPrincipal)
                    {
                        valor = puNominalAmortizado;

                        valor = valor * fatorCorrecao;

                        rendimentoPU = valor - puOperacao;
                    }
                    else if (tipoEvento == (byte)TipoEventoTitulo.PagamentoCorrecao)
                    {
                        #region Trata PagamentoCorrecao
                        if (taxa == 0)
                        {
                            taxa = 100;
                        }

                        if (taxa == 100)
                        {
                            valor = (puNominalAmortizado * fatorCorrecao) - puNominalAmortizado;
                        }
                        else
                        {
                            decimal valorAmortizar = tituloRendaFixa.PUNominal.Value * taxa / 100M;
                            valor = (valorAmortizar * fatorCorrecao) - valorAmortizar;
                        }

                        rendimentoPU = valor;
                        #endregion
                    }
                    else if (taxa != 0 && valor == 0 && tipoEvento != (byte)TipoEventoTitulo.PagamentoPrincipal && tipoEvento != (byte)TipoEventoTitulo.PagamentoPU)
                    {
                        #region Trata eventos com taxa != 0 (Pagamento de Juros)
                        int numeroDiasNumerador = 0;
                        int numeroDiasBase = papelRendaFixa.BaseAno.Value;
                        if (papelRendaFixa.PagamentoJuros == (byte)PagamentoJurosTitulo.ContaDias || papelRendaFixa.PagamentoJuros == (byte)PagamentoJurosTitulo.ContaDiasCorridos)
                        {

                            if (papelRendaFixa.ContagemDias == (byte)ContagemDiasTitulo.Corridos)
                            {
                                numeroDiasNumerador = Calendario.NumeroDias(dataUltimoJuro, dataEvento);
                            }
                            else if (papelRendaFixa.ContagemDias == (byte)ContagemDiasTitulo.Dias360)
                            {
                                numeroDiasNumerador = Calendario.NumeroDias360(dataUltimoJuro, dataEvento);
                            }
                            else
                            {
                                numeroDiasNumerador = Calendario.NumeroDias(dataUltimoJuro, dataEvento, (byte)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                            }
                        }
                        else
                        {
                            numeroDiasNumerador = numeroDiasBase;
                        }

                        decimal fatorJuros = 1;
                        if (papelRendaFixa.TipoCurva == (byte)TipoCurvaTitulo.Exponencial)
                        {
                            fatorJuros = Math.Round((decimal)Math.Pow((double)(1 + taxa / 100M), (double)numeroDiasNumerador / numeroDiasBase), 9);
                        }
                        else
                        {
                            fatorJuros = Math.Round((taxa / 100M) * numeroDiasNumerador / numeroDiasBase, 9) + 1;
                        }

                        if (tipoEvento == (byte)TipoEventoTitulo.Juros)
                        {
                            valor = (puNominalAmortizado * fatorCorrecao) * (fatorJuros - 1);
                        }
                        else if (tipoEvento == (byte)TipoEventoTitulo.JurosCorrecao) //Descarrega toda a correção
                        {
                            valor = (puNominalAmortizado * fatorJuros * fatorCorrecao) - puNominalAmortizado;
                        }

                        rendimentoPU = valor;

                        if (data == tituloRendaFixa.DataVencimento.Value)
                        {
                            rendimentoPU = puNominalAmortizado + valor - puOperacao;
                        }
                        #endregion
                    }
                    else
                    {
                        if (tipoEvento != (byte)TipoEventoTitulo.PagamentoPU)
                        {
                            if (tipoEvento == (byte)TipoEventoTitulo.JurosCorrecao) //Descarrega toda a correção
                            {
                                decimal fatorJuros = valor / puNominalAmortizado + 1M;
                                valor = (puNominalAmortizado * fatorJuros * fatorCorrecao) - puNominalAmortizado;
                            }
                            else
                            {
                                valor = valor * fatorCorrecao;
                            }

                            rendimentoPU = valor;

                            if (data == tituloRendaFixa.DataVencimento.Value)
                            {
                                rendimentoPU = puNominalAmortizado + valor - puOperacao;
                            }
                        }
                        else
                        {
                            rendimentoPU = valor;
                        }
                    }
                }

                decimal valorLiquidacao = Utilitario.Truncate(valor * quantidade, 2);
                decimal rendimento = Utilitario.Truncate(rendimentoPU * quantidade, 2);

                bool debentureInfra = tituloRendaFixa.DebentureInfra == "S";

                decimal valorIR = 0;
                #region Cálculo do IR
                bool isentoCliente = clienteRendaFixa.IsIsentoIR() ||
                                        (tituloRendaFixa.IsentoIR.Value == (byte)TituloIsentoIR.IsentoPF && cliente.IdTipo.Value == (byte)TipoClienteFixo.ClientePessoaFisica &&
                                         cliente.GrossUP.Value != (byte)GrossupCliente.SempreFaz);

                //booleano para não repetir a lógica aplicada, caso soma IR teórico na liquidação
                bool calculouGrossUpResgate = false;
                if (!isentoCliente)
                {
                    #region Tratamento especifico para IR de debenture (Notar: Só se aplica para o 1o pagto de juros)
                    bool trataIREspecifico = tituloRendaFixa.IsentoIR.Value == (byte)TituloIsentoIR.IsentoCupom || (
                                                (papelRendaFixa.Classe.Value == (int)ClasseRendaFixa.Debenture && !debentureInfra));
                    //Tratamento especifico para debenture NÃO INCENTIVADAS ou para títulos do Itau - IsentoCupom (permite descontar o rendimento do ultimo juro ate a data de operacao)
                    if (trataIREspecifico && dataOperacao > new DateTime(2011, 01, 01) &&
                        dataOperacao > dataUltimoJuro && //pois só vale para o 1o pagto de juros depois da aquisição
                                                    (tipoEvento == (byte)TipoEventoTitulo.Juros ||
                                                     tipoEvento == (byte)TipoEventoTitulo.JurosCorrecao ||
                                                     tipoEvento == (byte)TipoEventoTitulo.PagamentoCorrecao ||
                                                     tipoEvento == (byte)TipoEventoTitulo.PagamentoPU))
                    {
                        CalculoRendaFixa calculoRendaFixa = new CalculoRendaFixa();
                        decimal fatorCorrecaoDebenture = calculoRendaFixa.RetornaFatorCorrecao(dataOperacao, dataBaseCorrecao, papelRendaFixa, tituloRendaFixa, indice);

                        int numeroDiasNumerador = 0;
                        int numeroDiasBase = papelRendaFixa.BaseAno.Value;

                        DateTime dataBaseIR = dataUltimoJuro > dataOperacao ? tituloRendaFixa.DataEmissao.Value : dataUltimoJuro;

                        if (papelRendaFixa.ContagemDias == (byte)ContagemDiasTitulo.Corridos)
                        {
                            numeroDiasNumerador = Calendario.NumeroDias(dataBaseIR, dataOperacao);
                        }
                        else
                        {
                            numeroDiasNumerador = Calendario.NumeroDias(dataBaseIR, dataOperacao, (byte)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                        }

                        int numeroDiasOperacaoAteEvento = 0;
                        int numeroDiasTotal = 0;

                        numeroDiasOperacaoAteEvento = Calendario.NumeroDias(dataOperacao, dataEvento);
                        numeroDiasTotal = Calendario.NumeroDias(dataUltimoJuro, dataEvento);

                        decimal fatorJurosDebenture = 1;
                        if (papelRendaFixa.TipoCurva == (byte)TipoCurvaTitulo.Exponencial)
                        {
                            fatorJurosDebenture = Math.Round((decimal)Math.Pow((double)(1 + taxa / 100M), (double)numeroDiasNumerador / numeroDiasBase), 9);
                        }
                        else
                        {
                            fatorJurosDebenture = Math.Round((taxa / 100M) * numeroDiasNumerador / numeroDiasBase, 9) + 1;
                        }

                        decimal rendimentoPUAbater = 0;
                        if (tipoEvento == (byte)TipoEventoTitulo.Juros)
                        {
                            if (taxa == 0)
                            {
                                decimal valorAgenda = agendaRendaFixa.Valor.Value;
                                int numeroDiasAteOperacao = Calendario.NumeroDias(dataUltimoJuro, dataOperacao);
                                rendimentoPUAbater = (valorAgenda / numeroDiasTotal * numeroDiasAteOperacao) * fatorCorrecao;
                            }
                            else
                            {
                                rendimentoPUAbater = (puNominalAmortizado * fatorCorrecaoDebenture) * (fatorJurosDebenture - 1);
                            }
                        }
                        else if (tipoEvento == (byte)TipoEventoTitulo.JurosCorrecao)
                        {
                            rendimentoPUAbater = (puNominalAmortizado * fatorJurosDebenture * fatorCorrecaoDebenture) - puNominalAmortizado;
                        }

                        rendimento = Utilitario.Truncate((rendimentoPU - rendimentoPUAbater) * quantidade, 2);

                        #region ajusta o rendimento para tempo (pro-rata) do ativo em carteira (sempre por dias corridos )
                        if (tipoEvento == (byte)TipoEventoTitulo.Juros && dataOperacao.CompareTo(dataUltimoJuro) > 0)
                        {
                            rendimento = Utilitario.Truncate(rendimentoPU * quantidade, 2);
                            rendimento = Utilitario.Truncate(rendimento / numeroDiasTotal * numeroDiasOperacaoAteEvento, 2);
                        }
                        #endregion

                        //Ajusta PU da operação
                        PosicaoRendaFixa posicaoRendaFixa = new PosicaoRendaFixa();
                        posicaoRendaFixa.Query.Select(posicaoRendaFixa.Query.IdPosicao, posicaoRendaFixa.Query.PUOperacao);
                        posicaoRendaFixa.Query.Where(posicaoRendaFixa.Query.IdPosicao.Equal(idPosicao));

                        if (posicaoRendaFixa.Query.Load())
                        {
                            posicaoRendaFixa.PUOperacao -= rendimentoPUAbater;
                            posicaoRendaFixa.Save();
                        }
                    }
                    #endregion

                    int prazoTotal = Calendario.NumeroDias(dataOperacao, data);

                    TituloIsentoIR tituloIsentoIR = (TituloIsentoIR)tituloRendaFixa.IsentoIR.Value;
                    int tipoCliente = cliente.IdTipo.Value;

                    bool isentoTitulo = tituloIsentoIR == TituloIsentoIR.Isento ||
                        (tituloIsentoIR != TituloIsentoIR.NaoIsento &&
                        cliente.IdTipo.Value == (byte)TipoClienteFixo.ClientePessoaFisica &&
                        tipoEvento != (byte)TipoEventoTitulo.PagamentoPrincipal); //Isenta cupom de juros 


                    bool grossUp = cliente.GrossUP.Value != (byte)GrossupCliente.NaoFaz && tituloIsentoIR != TituloIsentoIR.NaoIsento;
                    if (grossUp && cliente.GrossUP.Value == (byte)GrossupCliente.ApenasResgate &&
                            isentoTitulo && tipoEvento == (byte)TipoEventoTitulo.PagamentoPrincipal)
                    {
                        //Trata grossup no caso de pagamento de principal (descarrega o IR da posição calculado até d-1
                        valorIR = (decimal)agendaRendaFixaCollection[i].GetColumn(PosicaoRendaFixaMetadata.ColumnNames.ValorIR);
                        rendimento += valorIR;
                        valorLiquidacao += valorIR;
                        calculouGrossUpResgate = true;
                    }
                    else if (rendimento > 0)
                    {
                        if (grossUp && cliente.GrossUP.Value == (byte)GrossupCliente.SempreFaz && isentoTitulo && tipoEvento != (byte)TipoEventoTitulo.Amortizacao)
                        {
                            //Trata grossup no caso de pagamento de juros
                            valorIR = rendimento * 0.15M / 0.85M; //Totalmente forçado para aliquota de 15%
                            rendimento += valorIR;
                            valorLiquidacao += valorIR;
                            calculouGrossUpResgate = true;
                        }
                        else if (!isentoTitulo && tipoEvento != (byte)TipoEventoTitulo.Amortizacao)
                        {
                            if (cliente.IdTipo == TipoClienteFixo.InvestidorEstrangeiro)
                            {
                                valorIR = Utilitario.Truncate(rendimento * 0.15M, 2);
                            }
                            else
                            {
                                if (debentureInfra)
                                {
                                    valorIR = rendimento * 0.15M;
                                }
                                else if (papelRendaFixa.TipoPapel.Value == (byte)TipoPapelTitulo.Publico && dataOperacao > dataUltimoJuro)
                                {
                                    int diasDesdeCompra = Calendario.NumeroDias(dataOperacao, data);
                                    int diasDesdeUltimoJuro = Calendario.NumeroDias(dataUltimoJuro, data);
                                    decimal rendimentoAjustado = Math.Round((decimal)diasDesdeCompra / (decimal)diasDesdeUltimoJuro * rendimento, 2);

                                    if (prazoTotal <= 180)
                                        valorIR = rendimentoAjustado * 0.225M;
                                    else if (prazoTotal <= 360)
                                        valorIR = rendimentoAjustado * 0.2M;
                                    else if (prazoTotal <= 720)
                                        valorIR = rendimentoAjustado * 0.175M;
                                    else
                                        valorIR = rendimentoAjustado * 0.15M;
                                }
                                else
                                {
                                    if (prazoTotal <= 180)
                                        valorIR = rendimento * 0.225M;
                                    else if (prazoTotal <= 360)
                                        valorIR = rendimento * 0.2M;
                                    else if (prazoTotal <= 720)
                                        valorIR = rendimento * 0.175M;
                                    else
                                        valorIR = rendimento * 0.15M;
                                }
                            }
                        }
                    }
                }
                #endregion

                decimal valorLiquido = valorLiquidacao - valorIR;

                AgendaRendaFixaCollection agendaRendaFixaCollectionCheck = new AgendaRendaFixaCollection();
                agendaRendaFixaCollectionCheck.Query.Select(agendaRendaFixaCollectionCheck.Query.DataEvento);
                agendaRendaFixaCollectionCheck.Query.Where(agendaRendaFixaCollectionCheck.Query.IdTitulo.Equal(idTitulo),
                                                           agendaRendaFixaCollectionCheck.Query.TipoEvento.Equal(tipoEvento),
                                                           agendaRendaFixaCollectionCheck.Query.DataPagamento.GreaterThanOrEqual(dataEvento));
                agendaRendaFixaCollectionCheck.Query.OrderBy(agendaRendaFixaCollectionCheck.Query.DataEvento.Ascending);
                agendaRendaFixaCollectionCheck.Query.Load();

                DateTime dataEventoCheck = dataEvento;
                if (agendaRendaFixaCollectionCheck.Count > 0)
                {
                    dataEventoCheck = agendaRendaFixaCollectionCheck[0].DataEvento.Value;
                }

                LiquidacaoRendaFixaCollection liquidacaoRendaFixaCollectionCheck = new LiquidacaoRendaFixaCollection();
                liquidacaoRendaFixaCollectionCheck.Query.Where(liquidacaoRendaFixaCollectionCheck.Query.IdCliente.Equal(idCliente),
                                                          liquidacaoRendaFixaCollectionCheck.Query.IdPosicaoResgatada.Equal(idPosicao),
                                                          liquidacaoRendaFixaCollectionCheck.Query.IdTitulo.Equal(idTitulo),
                                                          liquidacaoRendaFixaCollectionCheck.Query.Quantidade.Equal(quantidade),
                                                          liquidacaoRendaFixaCollectionCheck.Query.TipoLancamento.Equal(tipoLancamento),
                                                          liquidacaoRendaFixaCollectionCheck.Query.DataLiquidacao.GreaterThanOrEqual(dataEventoCheck));
                liquidacaoRendaFixaCollectionCheck.Query.Load();

                //Teste para não deixar duplicar em situações raras de feriados bovespa (e mais recentemente em funcao do conceito de "liberado")
                if (liquidacaoRendaFixaCollectionCheck.Count == 0)
                {
                    #region Add na LiquidacaoRendaFixa
                    LiquidacaoRendaFixa liquidacaoRendaFixa = new LiquidacaoRendaFixa();
                    liquidacaoRendaFixa.TipoLancamento = tipoLancamento;
                    liquidacaoRendaFixa.IdCliente = idCliente;
                    liquidacaoRendaFixa.IdTitulo = idTitulo;
                    liquidacaoRendaFixa.DataLiquidacao = data;
                    liquidacaoRendaFixa.Quantidade = quantidade;
                    liquidacaoRendaFixa.Rendimento = rendimento;
                    liquidacaoRendaFixa.ValorIOF = 0;
                    liquidacaoRendaFixa.ValorIR = valorIR;
                    liquidacaoRendaFixa.ValorBruto = valorLiquidacao;
                    liquidacaoRendaFixa.ValorLiquido = valorLiquido;
                    liquidacaoRendaFixa.IdPosicaoResgatada = idPosicao;
                    liquidacaoRendaFixa.PULiquidacao = valor;
                    liquidacaoRendaFixa.Save();
                    #endregion

                    Liquidacao liquidacao = new Liquidacao();
                    #region Lanca em Liquidacao valor IR
                    if (valorIR != 0 && tipoEvento != (byte)TipoEventoTitulo.Amortizacao && !calculouGrossUpResgate)
                    {
                        liquidacao = new Liquidacao();
                        liquidacao.DataLancamento = data;
                        liquidacao.DataVencimento = dataPagamento;
                        liquidacao.Descricao = "IR s/ Pagamento Parcela " + descricaoTitulo;
                        liquidacao.Valor = valorIR * -1;
                        liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                        liquidacao.Origem = OrigemLancamentoLiquidacao.RendaFixa.IR;
                        liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                        liquidacao.IdCliente = idCliente;
                        liquidacao.IdConta = idConta;
                        liquidacao.Save();
                    }
                    #endregion

                    #region Lança em Liquidacao estorno do IR de modo a não se gerar impacto no c/c (mas assegurando que será gerado um resgate especial)
                    if (valorIR != 0 && tipoEvento == (byte)TipoEventoTitulo.PagamentoPrincipal && !calculouGrossUpResgate)
                    {
                        liquidacao = new Liquidacao();
                        liquidacao.DataLancamento = data;
                        liquidacao.DataVencimento = dataPagamento;
                        liquidacao.Descricao = "IR s/ Pagamento Parcela " + descricaoTitulo;
                        liquidacao.Valor = valorIR;
                        liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                        liquidacao.Origem = OrigemLancamentoLiquidacao.RendaFixa.Outros;
                        liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                        liquidacao.IdCliente = idCliente;
                        liquidacao.IdConta = idConta;
                        liquidacao.Save();

                        //Ajusta o valor da liquidação de modo a voltar o valor sem o IR
                        valorLiquidacao -= valorIR;
                    }
                    #endregion

                    #region Lanca em Liquidacao valor da liquidacao
                    liquidacao = new Liquidacao();
                    liquidacao.DataLancamento = data;
                    liquidacao.DataVencimento = dataPagamento;
                    liquidacao.Descricao = descricao;

                    if (calculouGrossUpResgate)
                        liquidacao.Valor = valorLiquido;
                    else
                        liquidacao.Valor = valorLiquidacao;

                    liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                    liquidacao.Origem = origem;
                    liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                    liquidacao.IdCliente = idCliente;
                    liquidacao.IdConta = idConta;
                    liquidacao.Save();
                    #endregion
                }

                i++;
            }
        }

        /// <summary>
        /// Cria fluxo de juros a partir dos parametros passados.
        /// </summary>
        /// <param name="codigoSelic"></param>
        /// <param name="dataEmissao"></param>
        /// <param name="dataVencimento"></param>
        public void CriaAgendaFluxosTituloPublico(int idTitulo, string descricaoTitulo, DateTime dataEmissao, DateTime dataVencimento)
        {
            AgendaRendaFixaCollection agendaRendaFixaCollectionDeletar = new AgendaRendaFixaCollection();
            agendaRendaFixaCollectionDeletar.Query.Where(agendaRendaFixaCollectionDeletar.Query.IdTitulo.Equal(idTitulo));
            agendaRendaFixaCollectionDeletar.Query.Load();
            agendaRendaFixaCollectionDeletar.MarkAllAsDeleted();
            agendaRendaFixaCollectionDeletar.Save();

            AgendaRendaFixaCollection agendaRendaFixaCollection = new AgendaRendaFixaCollection();

            AgendaRendaFixa agendaRendaFixaVencimento = agendaRendaFixaCollection.AddNew();
            agendaRendaFixaVencimento.DataAgenda = dataVencimento;
            agendaRendaFixaVencimento.DataEvento = dataVencimento;
            agendaRendaFixaVencimento.DataPagamento = dataVencimento;

            if (!Calendario.IsDiaUtil(agendaRendaFixaVencimento.DataPagamento.Value))
            {
                agendaRendaFixaVencimento.DataPagamento = Calendario.AdicionaDiaUtil(agendaRendaFixaVencimento.DataPagamento.Value, 1);
            }

            agendaRendaFixaVencimento.IdTitulo = idTitulo;
            agendaRendaFixaVencimento.Taxa = 0;
            agendaRendaFixaVencimento.TipoEvento = (byte)TipoEventoTitulo.PagamentoPrincipal;
            agendaRendaFixaVencimento.Valor = 1000M;

            decimal valor = 0;
            if (descricaoTitulo == "NTN-B") //NTN-B com cupom de 6% a.a.
            {
                valor = 29.5630140987M;
            }
            else if (descricaoTitulo == "NTN-F") //NTN-F com cupom de 10% a.a.
            {
                valor = 48.8088481701516M;
            }
            else if (descricaoTitulo == "NTN-C" && dataVencimento == new DateTime(2031, 01, 01)) //NTN-C com cupom diferenciado de 12% a.a.
            {
                valor = 58.3005244258363M;
            }
            else if (descricaoTitulo == "NTN-C" && dataVencimento != new DateTime(2031, 01, 01)) //NTN-C com cupom de 6% a.a.
            {
                valor = 29.5630140987M;
            }

            if (valor != 0)
            {
                DateTime dataAux = dataVencimento.AddMonths(-6);
                while (dataAux > dataEmissao)
                {
                    DateTime dataPagamento = dataAux;
                    if (!Calendario.IsDiaUtil(dataPagamento))
                    {
                        dataPagamento = Calendario.AdicionaDiaUtil(dataPagamento, 1);
                    }

                    AgendaRendaFixa agendaRendaFixa = agendaRendaFixaCollection.AddNew();
                    agendaRendaFixa.DataAgenda = dataAux;
                    agendaRendaFixa.DataEvento = dataAux;
                    agendaRendaFixa.DataPagamento = dataPagamento;
                    agendaRendaFixa.IdTitulo = idTitulo;
                    agendaRendaFixa.Taxa = 0;
                    agendaRendaFixa.TipoEvento = (byte)TipoEventoTitulo.Juros;
                    agendaRendaFixa.Valor = valor;

                    dataAux = dataAux.AddMonths(-6);
                }

                agendaRendaFixaCollection.Save();
            }
        }

        /// <summary>
        /// Calcula o total amortizado por pu, considera tanto amortizações simples quanto amortizações com incidência de correção.
        /// </summary>
        /// <param name="tituloRendaFixa"></param>
        /// <param name="dataCalculo"></param>
        /// <returns></returns>
        public decimal RetornaTotalAmortizadoPU(TituloRendaFixa tituloRendaFixa, DateTime dataCalculo, DateTime dataCompra)
        {
            decimal puNominalAmortizado = tituloRendaFixa.PUNominal.Value;

            decimal totalAmortizadoPU = 0;
            AgendaRendaFixaCollection agendaRendaFixaCollectionAmortizacao = new AgendaRendaFixaCollection();
            agendaRendaFixaCollectionAmortizacao.Query.Where(agendaRendaFixaCollectionAmortizacao.Query.IdTitulo.Equal(tituloRendaFixa.IdTitulo.Value),
                                                  agendaRendaFixaCollectionAmortizacao.Query.DataPagamento.LessThanOrEqual(dataCalculo),
                                                  agendaRendaFixaCollectionAmortizacao.Query.DataPagamento.GreaterThan(dataCompra),
                                                  agendaRendaFixaCollectionAmortizacao.Query.TipoEvento.In((byte)TipoEventoTitulo.Amortizacao,
                                                                                                           (byte)TipoEventoTitulo.AmortizacaoCorrigida));
            agendaRendaFixaCollectionAmortizacao.Query.Load();

            Indice indice = new Indice();
            if (tituloRendaFixa.IdIndice.HasValue)
            {
                indice.LoadByPrimaryKey((short)tituloRendaFixa.IdIndice.Value);
            }

            PapelRendaFixa papelRendaFixa = new PapelRendaFixa();
            papelRendaFixa.LoadByPrimaryKey(tituloRendaFixa.IdPapel.Value);

            foreach (AgendaRendaFixa agendaRendaFixaAmortizacao in agendaRendaFixaCollectionAmortizacao)
            {
                DateTime dataEvento = agendaRendaFixaAmortizacao.DataEvento.Value;

                decimal puAmortizado = 0;
                if (agendaRendaFixaAmortizacao.Taxa.HasValue && agendaRendaFixaAmortizacao.Taxa.Value != 0)
                {
                    decimal taxaAmortizacao = agendaRendaFixaAmortizacao.Taxa.Value;
                    puAmortizado = tituloRendaFixa.PUNominal.Value * taxaAmortizacao / 100M;
                }
                else if (agendaRendaFixaAmortizacao.Valor.HasValue)
                {
                    puAmortizado = agendaRendaFixaAmortizacao.Valor.Value;
                }

                if (agendaRendaFixaAmortizacao.TipoEvento.Value == (byte)TipoEventoTitulo.AmortizacaoCorrigida)
                {
                    decimal fatorCorrecao = 1;
                    #region Calcula Fator de Correção
                    DateTime dataBaseCorrecao = tituloRendaFixa.DataEmissao.Value;
                    if (tituloRendaFixa.IdIndice.HasValue)
                    {
                        #region Acha data base de correção do indice
                        dataBaseCorrecao = tituloRendaFixa.DataEmissao.Value;
                        AgendaRendaFixaCollection agendaRendaFixaCollectionCorrecao = new AgendaRendaFixaCollection();
                        agendaRendaFixaCollectionCorrecao.Query.Select(agendaRendaFixaCollectionCorrecao.Query.DataEvento);
                        agendaRendaFixaCollectionCorrecao.Query.Where(agendaRendaFixaCollectionCorrecao.Query.IdTitulo.Equal(tituloRendaFixa.IdTitulo.Value) &
                                                                      agendaRendaFixaCollectionCorrecao.Query.DataEvento.LessThan(dataEvento) &
                                                                         (
                                                                           agendaRendaFixaCollectionCorrecao.Query.TipoEvento.Equal((byte)TipoEventoTitulo.JurosCorrecao) |
                                                                           (agendaRendaFixaCollectionCorrecao.Query.TipoEvento.Equal((byte)TipoEventoTitulo.PagamentoCorrecao) &
                                                                            agendaRendaFixaCollectionCorrecao.Query.Taxa.In(0, 100)
                                                                           )
                                                                          )
                                                                      );
                        agendaRendaFixaCollectionCorrecao.Query.OrderBy(agendaRendaFixaCollectionCorrecao.Query.DataEvento.Descending);
                        agendaRendaFixaCollectionCorrecao.Query.Load();

                        if (agendaRendaFixaCollectionCorrecao.Count > 0)
                        {
                            dataBaseCorrecao = agendaRendaFixaCollectionCorrecao[0].DataEvento.Value;
                        }
                        #endregion

                        CalculoRendaFixa calculoRendaFixa = new CalculoRendaFixa();
                        fatorCorrecao = calculoRendaFixa.RetornaFatorCorrecao(dataEvento, dataBaseCorrecao, papelRendaFixa, tituloRendaFixa, indice);
                    }
                    #endregion

                    puAmortizado = puAmortizado * fatorCorrecao;
                }

                totalAmortizadoPU += puAmortizado;

                puNominalAmortizado -= puAmortizado;
            }

            return totalAmortizadoPU;
        }

        /// <summary>
        /// Calcula o total, em valor de PU, pago em eventos desde a operação até a data de referência.
        /// </summary>
        /// <param name="dataOperacao"></param>
        /// <param name="dataReferencia"></param>
        /// <param name="papelRendaFixa"></param>
        /// <param name="tituloRendaFixa"></param>
        /// <param name="indice"></param>
        /// <returns></returns>
        public decimal RetornaValorFluxo(DateTime dataOperacao, DateTime dataReferencia, int idTitulo, PapelRendaFixa papelRendaFixa, TituloRendaFixa tituloRendaFixa, Indice indice)
        {
            AgendaRendaFixaQuery agendaRendaFixaQuery = new AgendaRendaFixaQuery("A");
            agendaRendaFixaQuery.Select(agendaRendaFixaQuery.Valor, agendaRendaFixaQuery.Taxa, agendaRendaFixaQuery.DataEvento,
                                        agendaRendaFixaQuery.DataPagamento, agendaRendaFixaQuery.TipoEvento);
            agendaRendaFixaQuery.Where(agendaRendaFixaQuery.DataEvento.GreaterThan(dataOperacao),
                                       agendaRendaFixaQuery.DataEvento.LessThanOrEqual(dataReferencia),
                                       agendaRendaFixaQuery.IdTitulo.Equal(idTitulo),
                                       agendaRendaFixaQuery.TipoEvento.NotIn((byte)TipoEventoTitulo.Amortizacao, (byte)TipoEventoTitulo.AmortizacaoCorrigida));

            AgendaRendaFixaCollection agendaRendaFixaCollection = new AgendaRendaFixaCollection();
            agendaRendaFixaCollection.Load(agendaRendaFixaQuery);

            decimal valorTotal = 0;
            foreach (AgendaRendaFixa agendaRendaFixa in agendaRendaFixaCollection)
            {
                byte tipoEvento = agendaRendaFixa.TipoEvento.Value;
                DateTime dataPagamento = agendaRendaFixa.DataPagamento.Value;
                DateTime dataEvento = agendaRendaFixa.DataEvento.Value;
                decimal valor = agendaRendaFixa.Valor.Value;
                decimal taxa = agendaRendaFixa.Taxa.Value;

                if (tituloRendaFixa == null)
                {
                    tituloRendaFixa = new TituloRendaFixa();
                }

                if (!tituloRendaFixa.es.HasData)
                {
                    tituloRendaFixa.LoadByPrimaryKey(idTitulo);
                }

                if (papelRendaFixa == null)
                {
                    papelRendaFixa = new PapelRendaFixa();
                }

                if (!papelRendaFixa.es.HasData)
                {
                    papelRendaFixa.LoadByPrimaryKey(tituloRendaFixa.IdPapel.Value);
                }

                DateTime dataBase = tituloRendaFixa.DataEmissao.Value;

                decimal fatorCorrecao = 1;
                #region Calcula Fator de Correção
                DateTime dataBaseCorrecao = dataBase;
                if (tituloRendaFixa.IdIndice.HasValue)
                {
                    #region Acha data base de correção do indice
                    dataBaseCorrecao = tituloRendaFixa.DataEmissao.Value;
                    AgendaRendaFixaCollection agendaRendaFixaCollectionCorrecao = new AgendaRendaFixaCollection();
                    agendaRendaFixaCollectionCorrecao.Query.Select(agendaRendaFixaCollectionCorrecao.Query.DataEvento);
                    agendaRendaFixaCollectionCorrecao.Query.Where(agendaRendaFixaCollectionCorrecao.Query.IdTitulo.Equal(idTitulo) &
                                                                  agendaRendaFixaCollectionCorrecao.Query.DataEvento.LessThan(dataEvento) &
                                                                     (
                                                                       agendaRendaFixaCollectionCorrecao.Query.TipoEvento.Equal((byte)TipoEventoTitulo.JurosCorrecao) |
                                                                       (agendaRendaFixaCollectionCorrecao.Query.TipoEvento.Equal((byte)TipoEventoTitulo.PagamentoCorrecao) &
                                                                        agendaRendaFixaCollectionCorrecao.Query.Taxa.In(0, 100)
                                                                       )
                                                                      )
                                                                  );
                    agendaRendaFixaCollectionCorrecao.Query.OrderBy(agendaRendaFixaCollectionCorrecao.Query.DataEvento.Descending);
                    agendaRendaFixaCollectionCorrecao.Query.Load();

                    if (agendaRendaFixaCollectionCorrecao.Count > 0)
                    {
                        dataBaseCorrecao = agendaRendaFixaCollectionCorrecao[0].DataEvento.Value;
                    }
                    #endregion

                    if (indice == null)
                    {
                        indice = new Indice();
                    }

                    if (!indice.es.HasData)
                    {
                        indice.LoadByPrimaryKey(tituloRendaFixa.IdIndice.Value);
                    }

                    CalculoRendaFixa calculoRendaFixa = new CalculoRendaFixa();
                    fatorCorrecao = calculoRendaFixa.RetornaFatorCorrecao(dataEvento, dataBaseCorrecao, papelRendaFixa, tituloRendaFixa, indice);
                }
                #endregion

                decimal puNominalAmortizado = tituloRendaFixa.PUNominal.Value;
                #region Ajusta o PU nominal pelas amortizações ocorridas
                AgendaRendaFixaCollection agendaRendaFixaCollectionAmortizacao = new AgendaRendaFixaCollection();
                agendaRendaFixaCollectionAmortizacao.Query.Select(agendaRendaFixaCollectionAmortizacao.Query.Taxa,
                                                                  agendaRendaFixaCollectionAmortizacao.Query.Valor);
                agendaRendaFixaCollectionAmortizacao.Query.Where(agendaRendaFixaCollectionAmortizacao.Query.IdTitulo.Equal(idTitulo),
                                                      agendaRendaFixaCollectionAmortizacao.Query.DataPagamento.LessThan(dataEvento),
                                                      agendaRendaFixaCollectionAmortizacao.Query.TipoEvento.In((byte)TipoEventoTitulo.Amortizacao),
                                                      agendaRendaFixaCollectionAmortizacao.Query.Taxa.IsNotNull());
                agendaRendaFixaCollectionAmortizacao.Query.Load();

                foreach (AgendaRendaFixa agendaRendaFixaAmortizacao in agendaRendaFixaCollectionAmortizacao)
                {
                    if (agendaRendaFixaAmortizacao.Taxa.HasValue && agendaRendaFixaAmortizacao.Taxa.Value != 0)
                    {
                        decimal taxaAmortizacao = agendaRendaFixaAmortizacao.Taxa.Value;
                        puNominalAmortizado = puNominalAmortizado - tituloRendaFixa.PUNominal.Value * taxaAmortizacao / 100M;
                    }
                    else if (agendaRendaFixaAmortizacao.Valor.HasValue)
                    {
                        puNominalAmortizado = puNominalAmortizado - agendaRendaFixaAmortizacao.Valor.Value;
                    }
                }
                #endregion

                DateTime dataUltimoJuro = tituloRendaFixa.DataEmissao.Value;
                #region Acha data do último juro
                AgendaRendaFixaCollection agendaRendaFixaCollectionJuros = new AgendaRendaFixaCollection();
                agendaRendaFixaCollectionJuros.Query.Select(agendaRendaFixaCollectionJuros.Query.DataEvento);
                agendaRendaFixaCollectionJuros.Query.Where(agendaRendaFixaCollectionJuros.Query.IdTitulo.Equal(tituloRendaFixa.IdTitulo.Value),
                                                      agendaRendaFixaCollectionJuros.Query.DataPagamento.LessThan(dataEvento),
                                                      agendaRendaFixaCollectionJuros.Query.TipoEvento.In((byte)TipoEventoTitulo.Juros,
                                                                                                         (byte)TipoEventoTitulo.JurosCorrecao));
                agendaRendaFixaCollectionJuros.Query.OrderBy(agendaRendaFixaCollectionJuros.Query.DataEvento.Descending);
                agendaRendaFixaCollectionJuros.Query.Load();

                if (agendaRendaFixaCollectionJuros.Count > 0)
                {
                    dataUltimoJuro = agendaRendaFixaCollectionJuros[0].DataEvento.Value;
                }
                #endregion

                //Se a taxa for informada para os juros, calculo o valor a ser pago, senão assume o valor informado
                if (tipoEvento == (byte)TipoEventoTitulo.Amortizacao || tipoEvento == (byte)TipoEventoTitulo.AmortizacaoCorrigida)
                {
                    if (taxa != 0)
                    {
                        valor = tituloRendaFixa.PUNominal.Value * taxa / 100M;
                    }

                    if (tipoEvento == (byte)TipoEventoTitulo.AmortizacaoCorrigida)
                    {
                        valor = valor * fatorCorrecao;
                    }
                }
                else if (tipoEvento == (byte)TipoEventoTitulo.PagamentoCorrecao)
                {
                    #region Trata Pagamento Correção
                    if (taxa == 0)
                    {
                        taxa = 100;
                    }

                    if (taxa == 100)
                    {
                        valor = (puNominalAmortizado * fatorCorrecao) - puNominalAmortizado;
                    }
                    else
                    {
                        decimal valorAmortizar = tituloRendaFixa.PUNominal.Value * taxa / 100M;
                        valor = (valorAmortizar * fatorCorrecao) - valorAmortizar;
                    }
                    #endregion
                }
                else if (taxa != 0 && valor == 0 && tipoEvento != (byte)TipoEventoTitulo.PagamentoPrincipal && tipoEvento != (byte)TipoEventoTitulo.PagamentoPU)
                {
                    #region Trata eventos com taxa != 0 (Pagamento de Juros)
                    int numeroDiasNumerador = 0;
                    int numeroDiasBase = papelRendaFixa.BaseAno.Value;
                    if (papelRendaFixa.PagamentoJuros == (byte)PagamentoJurosTitulo.ContaDias || papelRendaFixa.PagamentoJuros == (byte)PagamentoJurosTitulo.ContaDiasCorridos)
                    {

                        if (papelRendaFixa.ContagemDias == (byte)ContagemDiasTitulo.Corridos)
                        {
                            numeroDiasNumerador = Calendario.NumeroDias(dataUltimoJuro, dataEvento);
                        }
                        else if (papelRendaFixa.ContagemDias == (byte)ContagemDiasTitulo.Dias360)
                        {
                            numeroDiasNumerador = Calendario.NumeroDias360(dataUltimoJuro, dataEvento);
                        }
                        else
                        {
                            numeroDiasNumerador = Calendario.NumeroDias(dataUltimoJuro, dataEvento, (byte)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                        }
                    }
                    else
                    {
                        numeroDiasNumerador = numeroDiasBase;
                    }

                    decimal fatorJuros = 1;
                    if (papelRendaFixa.TipoCurva == (byte)TipoCurvaTitulo.Exponencial)
                    {
                        fatorJuros = Math.Round((decimal)Math.Pow((double)(1 + taxa / 100M), (double)numeroDiasNumerador / numeroDiasBase), 9);
                    }
                    else
                    {
                        fatorJuros = Math.Round((taxa / 100M) * numeroDiasNumerador / numeroDiasBase, 9) + 1;
                    }

                    if (tipoEvento == (byte)TipoEventoTitulo.Juros)
                    {
                        valor = (puNominalAmortizado * fatorCorrecao) * (fatorJuros - 1);
                    }
                    else if (tipoEvento == (byte)TipoEventoTitulo.JurosCorrecao)
                    {
                        valor = (puNominalAmortizado * fatorJuros * fatorCorrecao) - puNominalAmortizado;
                    }
                    #endregion
                }
                else
                {
                    if (tipoEvento != (byte)TipoEventoTitulo.PagamentoPU)
                    {
                        if (tipoEvento == (byte)TipoEventoTitulo.JurosCorrecao) //Descarrega toda a correção
                        {
                            decimal fatorJuros = valor / puNominalAmortizado + 1M;
                            valor = (puNominalAmortizado * fatorJuros * fatorCorrecao) - puNominalAmortizado;
                        }
                        else
                        {
                            valor = valor * fatorCorrecao;
                        }
                    }
                }

                valorTotal += valor;
            }

            return valorTotal;
        }


    }
}
