﻿/*
===============================================================================
                     EntitySpaces(TM) by EntitySpaces, LLC
                 A New 2.0 Architecture for the .NET Framework
                          http://www.entityspaces.net
===============================================================================
                       EntitySpaces Version # 2007.0.0709.0
                       MyGeneration Version # 1.2.0.7
                           8/8/2007 16:00:41
-------------------------------------------------------------------------------
*/


using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.RendaFixa
{
	public partial class PosicaoRendaFixaAbertura : esPosicaoRendaFixaAbertura
	{
        /// <summary>
        /// Retorna o valor de mercado liquido de IR e IOF de todas as posições do cliente.
        /// </summary>
        /// <param name="dataHistorico"></param>
        /// <param name="idCliente"></param>
        /// <returns>soma do valor de mercado das posições</returns>
        public decimal RetornaValorMercadoLiquido(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorMercado.Sum(),
                         this.Query.ValorIR.Sum(),
                         this.Query.ValorIOF.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.DataHistorico == dataHistorico);

            this.Query.Load();

            decimal valorMercado = this.ValorMercado.HasValue ? this.ValorMercado.Value : 0;
            decimal valorIR = this.ValorIR.HasValue ? this.ValorIR.Value : 0;
            decimal valorIOF = this.ValorIOF.HasValue ? this.ValorIOF.Value : 0;

            return (valorMercado - valorIR - valorIOF);
        }

        /// <summary>
        /// Retorna o valor de mercado de todas as posições do cliente.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        /// <returns>soma do valor de mercado das posições</returns>
        public decimal RetornaValorMercado(int idCliente, DateTime dataHistorico) 
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorMercado.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.DataHistorico == dataHistorico);

            this.Query.Load();

            return this.ValorMercado.HasValue ? this.ValorMercado.Value : 0;
        }

	}
}
