﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.RendaFixa
{
	public partial class ClienteRendaFixa : esClienteRendaFixa
	{
        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se ClienteRendaFixa é Isento IR</returns>
        public bool IsIsentoIR()
        {
            return (this.IsentoIR == "S" || this.IsentoIR == null);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se ClienteRendaFixa é Isento IOF</returns>
        public bool IsIsentoIOF()
        {
            return (this.IsentoIOF == "S" || this.IsentoIOF == null);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>bool indicando se ClienteRendaFixa usa Custo Médio</returns>
        public bool IsUsaCustoMedio()
        {
            return this.UsaCustoMedio == "S";
        }

	}
}
