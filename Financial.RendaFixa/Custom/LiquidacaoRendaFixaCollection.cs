﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.RendaFixa.Enums;

namespace Financial.RendaFixa
{
	public partial class LiquidacaoRendaFixaCollection : esLiquidacaoRendaFixaCollection
	{
        /// <summary>
        /// Deleta Liquidação por cliente e por dataLiquidacao.        
        /// </summary>
        /// <param name="idCliente"></param>     
        /// <param name="dataLiquidacao"></param>        
        public void DeletaLiquidacao(int idCliente, DateTime dataLiquidacao, int tipoLancamentoLiquidacao)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdLiquidacao)
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.DataLiquidacao.Equal(dataLiquidacao),
                        this.Query.TipoLancamento.Equal(tipoLancamentoLiquidacao));
            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// Adiciona uma Coluna Nova na Entidade
        /// </summary>
        /// <param name="columnName"></param>
        /// <param name="typeColumn"></param>
        public void AddColumn(string columnName, Type typeColumn) {
            if (this.Table != null && !this.Table.Columns.Contains(columnName)) {
                this.Table.Columns.Add(columnName, typeColumn);
            }
        }
	}
}
