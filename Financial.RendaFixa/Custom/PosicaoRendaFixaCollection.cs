using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.RendaFixa.Enums;
using log4net;

namespace Financial.RendaFixa {
    public partial class PosicaoRendaFixaCollection : esPosicaoRendaFixaCollection 
    {        
        // Construtor
        // Cria uma nova PosicaoRendaFixaCollection com os dados de PosicaoRendaFixaHistoricoCollection
        // Todos do dados de PosicaoRendaFixaHistoricoCollection são copiados com excessão da dataHistorico
        public PosicaoRendaFixaCollection(PosicaoRendaFixaHistoricoCollection posicaoRendaFixaHistoricoCollection) 
        {
            for (int i = 0; i < posicaoRendaFixaHistoricoCollection.Count; i++) {
                //
                PosicaoRendaFixa p = new PosicaoRendaFixa();

                // Para cada Coluna de PosicaoRendaFixaHistorico copia para PosicaoRendaFixa
                foreach (esColumnMetadata colPosicaoHistorico in posicaoRendaFixaHistoricoCollection.es.Meta.Columns) {
                    // Copia todas as colunas menos a Data Historico
                    if (colPosicaoHistorico.PropertyName != PosicaoRendaFixaHistoricoMetadata.ColumnNames.DataHistorico) {
                        esColumnMetadata colPosicaoRendaFixa = p.es.Meta.Columns.FindByPropertyName(colPosicaoHistorico.PropertyName);
                        if (posicaoRendaFixaHistoricoCollection[i].GetColumn(colPosicaoHistorico.Name) != null) {
                            p.SetColumn(colPosicaoRendaFixa.Name, posicaoRendaFixaHistoricoCollection[i].GetColumn(colPosicaoHistorico.Name));
                        }
                    }
                }
                this.AttachEntity(p);
            }
        }

        /// <summary>
        /// Semelhante ao Construtor PosicaoRendaFixaCollection mas em format Função void
        /// </summary>
        /// <param name="posicaoRendaFixaHistoricoCollection"></param>
        public void SetPosicaoRendaFixaCollection(PosicaoRendaFixaHistoricoCollection posicaoRendaFixaHistoricoCollection) {
            for (int i = 0; i < posicaoRendaFixaHistoricoCollection.Count; i++) {
                //
                PosicaoRendaFixa p = new PosicaoRendaFixa();

                // Para cada Coluna de PosicaoRendaFixaHistorico copia para PosicaoRendaFixa
                foreach (esColumnMetadata colPosicaoHistorico in posicaoRendaFixaHistoricoCollection.es.Meta.Columns) {
                    // Copia todas as colunas menos a Data Historico
                    if (colPosicaoHistorico.PropertyName != PosicaoRendaFixaHistoricoMetadata.ColumnNames.DataHistorico) {
                        esColumnMetadata colPosicaoRendaFixa = p.es.Meta.Columns.FindByPropertyName(colPosicaoHistorico.PropertyName);
                        if (posicaoRendaFixaHistoricoCollection[i].GetColumn(colPosicaoHistorico.Name) != null) {
                            p.SetColumn(colPosicaoRendaFixa.Name, posicaoRendaFixaHistoricoCollection[i].GetColumn(colPosicaoHistorico.Name));
                        }
                    }
                }
                this.AttachEntity(p);
            }
        }

        /// <summary>
        /// Adiciona uma Coluna Nova na Entidade
        /// </summary>
        /// <param name="columnName"></param>
        /// <param name="typeColumn"></param>
        public void AddColumn(string columnName, Type typeColumn) {
            if (this.Table != null && !this.Table.Columns.Contains(columnName)) {
                this.Table.Columns.Add(columnName, typeColumn);
            }
        }

        /// <summary>
        /// Carrega o objeto PosicaoRendaFixaCollection com os campos IdTitulo, ValorMercado.Sum.
        /// Group By IdTitulo
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns>indica se retornou registro</returns>
        public bool BuscaPosicaoRendaFixaAgrupadoEnquadra(int idCliente) {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdTitulo, this.Query.ValorMercado.Sum())
                 .Where(this.Query.IdCliente == idCliente)
                 .GroupBy(this.Query.IdTitulo);

            bool retorno = this.Query.Load();

            return retorno;
        }

        /// <summary>
        /// Deleta todas as posições do cliente passado.
        /// </summary>
        /// <param name="idCliente"></param>
        public void DeletaPosicaoRendaFixa(int idCliente) {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdPosicao)
                 .Where(this.Query.IdCliente == idCliente);
            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// Método de inserção em PosicaoRendaFixa, a partir de PosicaoRendaFixaHistorico.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void InserePosicaoRendaFixaHistorico(int idCliente, DateTime dataHistorico)
        {
            #region Copia de posicaoRendaFixaHistorico para PosicaoRendaFixa
            StringBuilder sqlBuilder = new StringBuilder();
            string strSelect = string.Empty;
            string strFrom = string.Empty;

            sqlBuilder.Append("SET IDENTITY_INSERT PosicaoRendaFixa ON ");
            sqlBuilder.Append("INSERT INTO PosicaoRendaFixa (");
            strSelect = " SELECT ";
            strFrom = " FROM PosicaoRendaFixaHistorico WHERE DataHistorico = " + "'" + dataHistorico.ToString("yyyyMMdd") + "' AND IdCliente = " + idCliente;
            int count = 0;

            PosicaoRendaFixaHistorico posicaoRendaFixaHistorico = new PosicaoRendaFixaHistorico();
            int columnCount = posicaoRendaFixaHistorico.es.Meta.Columns.Count;
            foreach (esColumnMetadata colPosicaoRendaFixa in posicaoRendaFixaHistorico.es.Meta.Columns)
            {
                count++;

                if (colPosicaoRendaFixa.Name == PosicaoRendaFixaHistoricoMetadata.ColumnNames.DataHistorico)
                    continue;
               
                //Insert
                sqlBuilder.Append(colPosicaoRendaFixa.Name);
                if (count != columnCount)
                {
                    sqlBuilder.Append(",");
                }
                else
                {
                    sqlBuilder.Append(")");
                }

                //select 
                strSelect += colPosicaoRendaFixa.Name;                

                if (count != columnCount)
                {
                    strSelect += ",";
                }
            }
            sqlBuilder.Append(strSelect + strFrom);
            sqlBuilder.AppendLine();
            sqlBuilder.Append("SET IDENTITY_INSERT PosicaoRendaFixa OFF ");

            esUtility u = new esUtility();
            u.ExecuteNonQuery(esQueryType.Text, sqlBuilder.ToString());
            #endregion
        }
    

        /// <summary>
        /// Método de inserção em PosicaoRendaFixa, a partir de PosicaoRendaFixaAbertura.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataAbertura"></param>
        public void InserePosicaoRendaFixaAbertura(int idCliente, DateTime dataAbertura) 
        {
            #region Copia de posicaoRendaFixaHistorico para PosicaoRendaFixa
            StringBuilder sqlBuilder = new StringBuilder();
            string strSelect = string.Empty;
            string strFrom = string.Empty;

            sqlBuilder.Append("SET IDENTITY_INSERT PosicaoRendaFixa ON ");
            sqlBuilder.Append("INSERT INTO PosicaoRendaFixa (");
            strSelect = " SELECT ";
            strFrom = " FROM PosicaoRendaFixaAbertura WHERE DataHistorico = " + "'" + dataAbertura.ToString("yyyyMMdd") + "' AND IdCliente = " + idCliente;
            int count = 0;

            PosicaoRendaFixaAbertura posicaoRendaFixaAbertura = new PosicaoRendaFixaAbertura();
            int columnCount = posicaoRendaFixaAbertura.es.Meta.Columns.Count;
            foreach (esColumnMetadata colPosicaoRendaFixa in posicaoRendaFixaAbertura.es.Meta.Columns)
            {
                count++;

                if (colPosicaoRendaFixa.Name == PosicaoRendaFixaAberturaMetadata.ColumnNames.DataHistorico)
                    continue;
                
                //Insert
                sqlBuilder.Append(colPosicaoRendaFixa.Name);
                if (count != columnCount)
                {
                    sqlBuilder.Append(",");
                }
                else
                {
                    sqlBuilder.Append(")");
                }

                //select 
                strSelect += colPosicaoRendaFixa.Name;

                if (count != columnCount)
                {
                    strSelect += ",";
                }
            }
            sqlBuilder.Append(strSelect + strFrom);
            sqlBuilder.AppendLine();
            sqlBuilder.Append("SET IDENTITY_INSERT PosicaoRendaFixa OFF ");

            esUtility u = new esUtility();
            u.ExecuteNonQuery(esQueryType.Text, sqlBuilder.ToString());
            #endregion
        }

        /// <summary>
        /// Carrega o objeto PosicaoRendaFixaCollection com todos os campos de PosicaoRendaFixa.
        /// </summary>
        /// <param name="idCliente"></param>
        public void BuscaPosicaoRendaFixaCompleta(int idCliente) {
            this.QueryReset();
            this.Query.Where(this.Query.IdCliente == idCliente & this.Query.Quantidade.NotEqual(0));
            this.Query.Load();
        }

        /// <summary>
        /// Carrega o objeto PosicaoRendaFixaCollection com todos os campos de PosicaoRendaFixa.
        /// Filtra TipoOperacao.Equal(TipoOperacaoTitulo.CompraFinal).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns>bool indicando se trouxe registro.</returns>
        public bool BuscaPosicaoCompradoFinal(int idCliente, int idTitulo, int? idPosicao) {
            this.QueryReset();
            this.Query.Where(this.Query.IdCliente == idCliente,
                        this.Query.IdTitulo == idTitulo,
                        this.Query.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraFinal));

            if (idPosicao.HasValue) {
                this.Query.Where(this.Query.IdPosicao.Equal(idPosicao.Value));
            }

            this.Query.OrderBy(this.Query.DataOperacao.Ascending);

            this.Query.Load();

            return this.HasData;
        }

        /// <summary>
        /// Carrega o objeto PosicaoRendaFixaCollection com todos os campos de PosicaoRendaFixa.
        /// Filtra TipoOperacao.Equal(tipoOperacao).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns>bool indicando se trouxe registro.</returns>
        public bool BuscaPosicao(int idCliente, int idTitulo, int? idPosicao, byte tipoOperacao)
        {
            this.QueryReset();
            this.Query.Where(this.Query.IdCliente == idCliente,
                        this.Query.IdTitulo == idTitulo,
                        this.Query.Quantidade.NotEqual(0),
                        this.Query.TipoOperacao.Equal(tipoOperacao));

            if (idPosicao.HasValue)
            {
                this.Query.Where(this.Query.IdPosicao.Equal(idPosicao.Value));
            }

            this.Query.OrderBy(this.Query.DataOperacao.Ascending);

            this.Query.Load();

            return this.HasData;
        }

        /// <summary>
        /// Carrega o objeto PosicaoRendaFixaCollection com todos os campos de PosicaoRendaFixa.
        /// Filtra TipoOperacao.Equal(tipoOperacao).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns>bool indicando se trouxe registro.</returns>
        public bool BuscaPosicao(int idCliente, int idTitulo, int? idPosicao, int? idOperacaoResgatada, byte tipoOperacao)
        {
            this.QueryReset();
            this.Query.Where(this.Query.IdCliente == idCliente,
                        this.Query.IdTitulo == idTitulo,
                        this.Query.Quantidade.NotEqual(0),
                        this.Query.TipoOperacao.Equal(tipoOperacao));

            if (idPosicao.HasValue)
            {
                this.Query.Where(this.Query.IdPosicao.Equal(idPosicao.Value));
            }

            if (idOperacaoResgatada.HasValue)
            {
                this.Query.Where(this.Query.IdOperacao.Equal(idOperacaoResgatada.Value));
            }

            this.Query.OrderBy(this.Query.DataOperacao.Ascending);

            this.Query.Load();

            return this.HasData;
        }

        /// <summary>
        /// Carrega o objeto PosicaoRendaFixaCollection com todos os campos de PosicaoRendaFixa.
        /// Filtra por this.Query.Quantidade.NotEqual(0).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns>bool indicando se trouxe registro.</returns>
        public bool BuscaPosicao(int idCliente) {
            this.QueryReset();
            this.Query
                 .Select()
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Quantidade.NotEqual(0));

            this.Query.Load();

            return this.HasData;
        }

        /// <summary>
        /// Carrega o objeto PosicaoRendaFixaCollection com todos os campos de PosicaoRendaFixa.
        /// Filtra por this.Query.Quantidade.NotEqual(0), this.Query.DataOperacao.NotEqual(data).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns>bool indicando se trouxe registro.</returns>
        public bool BuscaPosicao(int idCliente, DateTime data) {
            this.QueryReset();
            this.Query
                 .Select()
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.DataOperacao.NotEqual(data),
                        this.Query.Quantidade.NotEqual(0));

            this.Query.Load();

            return this.HasData;
        }

        /// <summary>
        /// Carrega o objeto PosicaoRendaFixaCollection com todos os campos de PosicaoRendaFixa.
        /// Filtra por: Quantidade.NotEqual(this.Query.QuantidadeInicial)
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns>bool indicando se trouxe registro.</returns>
        public bool BuscaPosicaoResgatada(int idCliente) {
            this.QueryReset();
            this.Query
                 .Select()
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Quantidade.NotEqual(this.Query.QuantidadeInicial));

            this.Query.Load();

            return this.HasData;
        }

        /// <summary>
        /// Carrega o objeto PosicaoRendaFixaCollection com os campos IdPosicao, IdTitulo, Quantidade, 
        /// PUMercado, ValorMercado.
        /// Filtra para DataVolta.IsNull().
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns>bool indicando se trouxe registro.</returns>
        public bool BuscaPosicaoTitulos(int idCliente) {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdPosicao, this.Query.IdTitulo, this.Query.Quantidade, this.Query.PUMercado,
                         this.Query.ValorMercado, this.Query.DataVencimento)
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.DataVolta.IsNull(),
                        this.Query.Quantidade.NotEqual(0));

            this.Query.Load();

            return this.HasData;
        }

        /// <summary>
        /// Carrega o objeto com todos os campos de PosicaoRendaFixaCollection.
        /// Filtra para Quantidade.NotEqual(0), DataVencimento.LessThanOrEqual(dataVencimento) e DataVencimento.GreaterThan(dataAnterior)
        /// e Equal((byte)TipoOperacaoTitulo.CompraFinal).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataVencimento"></param>
        /// <param name="dataAnterior"></param>
        /// <returns>bool indicando se trouxe registro.</returns>
        public bool BuscaPosicaoVencimento(int idCliente, DateTime dataVencimento, DateTime dataAnterior) {
            this.QueryReset();
            this.Query
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Quantidade.NotEqual(0),
                        this.Query.DataVencimento.LessThanOrEqual(dataVencimento),
                        this.Query.DataVencimento.GreaterThan(dataAnterior),
                        this.Query.TipoOperacao.Equal((byte)TipoOperacaoTitulo.CompraFinal));

            this.Query.Load();

            return this.HasData;
        }

        /// <summary>
        /// Carrega o objeto PosicaoRendaFixaCollection com os campos IdPosicao, IdTitulo, Quantidade, PUCurva, ValorCurva,
        /// TipoOperacao, PUOperacao.
        /// Filtra para DataVencimento.LessThanOrEqual(dataVencimento) e DataVencimento.GreaterThan(dataAnterior)
        ///  e TipoOperacao.In((byte)TipoOperacaoTitulo.CompraRevenda, 
        ///                    (byte)TipoOperacaoTitulo.VendaRecompra).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataVolta"></param>
        /// <param name="dataAnterior"></param>
        /// <returns>bool indicando se trouxe registro.</returns>
        public bool BuscaPosicaoVoltaCompromisso(int idCliente, DateTime dataVolta, DateTime dataAnterior) {
            this.QueryReset();
            this.Query                 
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.DataVolta.LessThanOrEqual(dataVolta),
                        this.Query.DataVolta.GreaterThan(dataAnterior),
                        this.Query.TipoOperacao.In((byte)TipoOperacaoTitulo.CompraRevenda,
                                                   (byte)TipoOperacaoTitulo.VendaRecompra));

            this.Query.Load();

            return this.HasData;
        }

        /// <summary>
        /// Carrega o objeto PosicaoRendaFixaCollection com os campos PUOperacao, PUCurva, ValorCurva, 
        /// PUMercado, ValorMercado, PUJuros, ValorJuros.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="idTitulo"></param>
        /// <returns>bool indicando se trouxe registro.</returns>
        public bool BuscaPosicaoLiquidaEvento(int idCliente, int idTitulo) {
            this.QueryReset();
            this.Query
                 .Select(this.Query.PUOperacao, this.Query.PUCurva, this.Query.ValorCurva, this.Query.PUMercado,
                         this.Query.ValorMercado, this.Query.PUJuros, this.Query.ValorJuros)
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.IdTitulo == idTitulo);

            this.Query.Load();

            return this.HasData;
        }

        /// <summary>
        /// Carrega o objeto PosicaoRendaFixaCollection com os campos IdTitulo, Quantidade.Sum, PUMercado.Avg, PUOperacao.Avg, 
        /// ValorMercado.Sum, ValorIR.Sum, ValorIOF.Sum.
        /// Group By IdTitulo
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns>indica se retornou registro</returns>
        public bool BuscaPosicaoRendaFixaAgrupado(int idCliente) {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdTitulo,
                         this.Query.Quantidade.Sum(),
                         this.Query.PUMercado.Avg(),
                         this.Query.PUOperacao.Avg(),
                         this.Query.ValorMercado.Sum(),
                         this.Query.ValorIR.Sum(),
                         this.Query.ValorIOF.Sum())
                 .Where(this.Query.IdCliente == idCliente)
                 .GroupBy(this.Query.IdTitulo);

            bool retorno = this.Query.Load();

            return retorno;
        }
    }
}