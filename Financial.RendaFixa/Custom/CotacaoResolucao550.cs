﻿/*
===============================================================================
                     EntitySpaces(TM) by EntitySpaces, LLC
                 A New 2.0 Architecture for the .NET Framework
                          http://www.entityspaces.net
===============================================================================
                       EntitySpaces Version # 2007.0.0304.0
                       MyGeneration Version # 1.2.0.2
                           14/3/2007 11:34:38
-------------------------------------------------------------------------------
*/


using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Interfaces.Import.RendaFixa;


namespace Financial.RendaFixa {
    public partial class CotacaoResolucao550 : esCotacaoResolucao550 {
        private static readonly ILog log = LogManager.GetLogger(typeof(CotacaoResolucao550));

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="pathArquivo">path completo do Diretorio onde procurar o Arquivo</param>
        public void CarregaRes550(DateTime data, string pathArquivo) {
            #region logEntrada
            if (log.IsDebugEnabled) {
                log.Debug("Entrada nomeMetodo: ");
            }
            #endregion

            Res550 res550 = new Res550();
            CotacaoResolucao550Collection cotacaoResolucao550Collection = new CotacaoResolucao550Collection();
            CotacaoResolucao550Collection cotacaoResolucao550CollectionAux = new CotacaoResolucao550Collection();

            // Deleta a tabela de CotacaoResolucao550 da data em questão
            cotacaoResolucao550CollectionAux.DeletaCotacaoResolucao550(data);

            // Pega os registros da collection de Res550
            Res550Collection res550Collection = res550.ProcessaRes550(data, pathArquivo);
            Res550Collection res550CollectionAux = new Res550Collection();

            // Filtra a Collection somente tipoRegistro = Dados
            res550CollectionAux.CollectionRes550 = res550Collection.CollectionRes550.FindAll(res550.FilterRes550ByDados);
            //
            for (int i = 0; i < res550CollectionAux.CollectionRes550.Count; i++) {
                res550 = res550CollectionAux.CollectionRes550[i];

                #region Copia informações para CotacaoResolucao550
                CotacaoResolucao550 cotacaoResolucao550 = new CotacaoResolucao550();
                //
                cotacaoResolucao550.DataReferencia = res550.DataReferencia;
                cotacaoResolucao550.DataVencimento = res550.DataVencimento;
                cotacaoResolucao550.CodigoSELIC = Convert.ToString(res550.CodigoSelic);
                cotacaoResolucao550.Pu = res550.Pu;
                //
                cotacaoResolucao550Collection.AttachEntity(cotacaoResolucao550);
                #endregion
            }

            cotacaoResolucao550Collection.Save();

            #region logSaida
            if (log.IsDebugEnabled) {
                log.Debug("Entrada nomeMetodo: ");
            }
            #endregion
        }
    }
}
