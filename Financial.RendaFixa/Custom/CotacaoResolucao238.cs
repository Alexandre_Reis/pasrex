﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Interfaces.Import.RendaFixa;


namespace Financial.RendaFixa {
    public partial class CotacaoResolucao238 : esCotacaoResolucao238 {
        private static readonly ILog log = LogManager.GetLogger(typeof(CotacaoResolucao238));
        
        /// <summary>
        /// Carrega as cotações da resolução 238 a partir do arquivo da Andima.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="pathArquivo">path completo do Diretorio onde procurar o Arquivo</param>
        public void CarregaRes238(DateTime data, string pathArquivo) 
        {
            Res238 res238 = new Res238();
            CotacaoResolucao238Collection cotacaoResolucao238Collection = new CotacaoResolucao238Collection();
            CotacaoResolucao238Collection cotacaoResolucao238CollectionAux = new CotacaoResolucao238Collection();

            // Deleta a tabela de CotacaoResolucao238 da data em questão
            cotacaoResolucao238CollectionAux.DeletaCotacaoResolucao238(data);
            
            // Pega os registros da collection de Res238
            Res238Collection res238Collection = res238.ProcessaRes238(data, pathArquivo);
            Res238Collection res238CollectionAux = new Res238Collection();

            // Filtra a Collection somente tipoRegistro = Dados
            res238CollectionAux.CollectionRes238 = res238Collection.CollectionRes238.FindAll(res238.FilterRes238ByDados);            
            //
            for (int i = 0; i < res238CollectionAux.CollectionRes238.Count; i++) {
                res238 = res238CollectionAux.CollectionRes238[i];

                #region Copia informações para CotacaoResolucao238
                CotacaoResolucao238 cotacaoResolucao238 = new CotacaoResolucao238();
                //
                cotacaoResolucao238.DataReferencia = res238.DataReferencia;
                cotacaoResolucao238.DataVencimento = res238.DataVencimento;
                cotacaoResolucao238.CodigoSELIC = Convert.ToString(res238.CodigoSelic);
                cotacaoResolucao238.Pu = res238.Pu;
                //
                cotacaoResolucao238Collection.AttachEntity(cotacaoResolucao238);
                #endregion
            }

            cotacaoResolucao238Collection.Save();
        }
    }
}
