﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Interfaces.Import.RendaFixa;
using Financial.Util;

namespace Financial.RendaFixa {
    public partial class CotacaoMercadoTesouro : esCotacaoMercadoTesouro {

        /// <summary>
        ///  Carrega a tabela CotacaoTesouro a partir do site do tesouro
        ///  http://www.tesouro.fazenda.gov.br/tesouro_direto/
        /// </summary>
        /// <param name="data"></param>
        /// <param name="pathArquivo">path completo do Diretorio de Destino do Download do arquivo xls do tesouro</param>
        public void CarregaCotacaoTesouro(DateTime data, string pathArquivo) {

            Tesouro tesouro = new Tesouro();

            // Pega os Registros da collection de Tesouro
            TesouroCollection tesouroCollection = tesouro.ProcessaCotacaoTesouro(data, pathArquivo);
            //
            for (int i = 0; i < tesouroCollection.CollectionTesouro.Count; i++) {
                tesouro = tesouroCollection.CollectionTesouro[i];

                #region Copia informações para CotacaoMercadoTesouro
                // Verifica se é Update ou Insert
                tesouro.Descricao = tesouro.Descricao.Replace(Utilitario.Right(tesouro.Descricao, 6), "").Trim();
                CotacaoMercadoTesouro update = new CotacaoMercadoTesouro();
                if (update.LoadByPrimaryKey(tesouro.DataReferencia, tesouro.Descricao, tesouro.DataVencimento)) 
                {
                    // Update
                    update.Pu = tesouro.Pu;
                    //
                    update.Save();
                }
                else 
                { 
                    // Insert
                    CotacaoMercadoTesouro cotacaoMercadoTesouro = new CotacaoMercadoTesouro();
                    //
                    cotacaoMercadoTesouro.DataReferencia = tesouro.DataReferencia;
                    cotacaoMercadoTesouro.Descricao = tesouro.Descricao;
                    cotacaoMercadoTesouro.DataVencimento = tesouro.DataVencimento;
                    cotacaoMercadoTesouro.Pu = tesouro.Pu;
                    //
                    cotacaoMercadoTesouro.Save();
                }
                               
                #endregion
            }
            
        }
    }
}