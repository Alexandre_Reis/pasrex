/*
===============================================================================
                     EntitySpaces(TM) by EntitySpaces, LLC
                 A New 2.0 Architecture for the .NET Framework
                          http://www.entityspaces.net
===============================================================================
                       EntitySpaces Version # 2007.0.0304.0
                       MyGeneration Version # 1.2.0.2
                           14/3/2007 11:34:37
-------------------------------------------------------------------------------
*/


using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

namespace Financial.RendaFixa {
    public partial class CotacaoMercadoDebentureCollection : esCotacaoMercadoDebentureCollection {
        
        /// <summary>
        ///  Deleta tudo da CotacaoMercadoDebenture de uma determinada data.
        /// </summary>
        /// <param name="data"></param>
        public void DeletaCotacaoMercadoDebenture(DateTime data) 
        {
            this.QueryReset();
            this.Query
              .Select(this.Query.DataReferencia, this.Query.CodigoPapel, this.Query.DataVencimento)
              .Where(this.Query.DataReferencia == data);

            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }
    }
}