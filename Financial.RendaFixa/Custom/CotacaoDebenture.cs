﻿/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 29/09/2011 13:16:21
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Interfaces.Import.RendaFixa;

namespace Financial.RendaFixa {
    public partial class CotacaoDebenture : esCotacaoDebenture {
        private static readonly ILog log = LogManager.GetLogger(typeof(CotacaoDebenture));

        /// <summary>
        /// Faz o Download de http://www.debentures.com.br/
        /// </summary>
        /// <param name="data"></param>
        /// <param name="pathArquivo">path completo do Diretorio onde procurar o Arquivo</param>
        public void CarregaDebenture(DateTime data, string pathArquivo) {
            DebentureSiteDebentures debenture = new DebentureSiteDebentures();
            //
            CotacaoDebentureCollection cotacaoDebentureCollection = new CotacaoDebentureCollection();
            //
            CotacaoDebentureCollection cotacaoDebentureCollectionAux = new CotacaoDebentureCollection();

            // Pega os registros da collection de Debenture
            List<DebentureSiteDebentures> debentureCollection = debenture.ProcessaDebenture(data, pathArquivo);
            //

            if (debentureCollection.Count > 1)
            {
                // Deleta a tabela de CotacaoDebenture da data em questão
                cotacaoDebentureCollectionAux.DeletaCotacaoDebenture(data);
            }

            for (int i = 0; i < debentureCollection.Count; i++) {
                debenture = debentureCollection[i];

                #region Copia informações para CotacaoDebenture
                CotacaoDebenture cotacaoDebenture = new CotacaoDebenture();
                //
                cotacaoDebenture.Data = data;
                cotacaoDebenture.CodigoPapel = debenture.CodigoAtivo;
                cotacaoDebenture.Pu = debenture.PU;
                //
                cotacaoDebentureCollection.AttachEntity(cotacaoDebenture);
                #endregion
            }

            cotacaoDebentureCollection.Save();
        }        
    }
}