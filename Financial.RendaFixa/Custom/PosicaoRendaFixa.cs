﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Util;
using Financial.Common.Enums;
using Financial.ContaCorrente;
using Financial.Investidor;
using Financial.ContaCorrente.Enums;
using Financial.RendaFixa.Enums;
using Financial.RendaFixa.Exceptions;
using Financial.Common;
using Financial.Util.ConfiguracaoSistema;
using Financial.Fundo;
using Financial.Fundo.Enums;
using Financial.Bolsa;
using Financial.Util.Enums;
using Financial.Investidor.Enums;
using Financial.InterfacesDB;
using System.Collections;
using Financial.RendaFixa.Controller;
using System.Linq;

namespace Financial.RendaFixa
{
    public partial class PosicaoRendaFixa : esPosicaoRendaFixa
    {
        TipoProcessamento tipoProcessamento = TipoProcessamento.Fechamento;

        public class PosicaoAtualizada
        {
            private decimal puAtualizado;
            public decimal PuAtualizado
            {
                get { return puAtualizado; }
                set { puAtualizado = value; }
            }

            private decimal puCorrecao;
            public decimal PuCorrecao
            {
                get { return puCorrecao; }
                set { puCorrecao = value; }
            }

            private decimal puJuros;
            public decimal PuJuros
            {
                get { return puJuros; }
                set { puJuros = value; }
            }

            private decimal taxa;
            public decimal Taxa
            {
                get { return taxa; }
                set { taxa = value; }
            }
        }

        public class VencimentoProjetado
        {
            public int? idCliente;
            public DateTime dataLancamento;
            public DateTime dataVencimento;
            public string descricao;
            public decimal? valor;
        }

        public PosicaoRendaFixa(TipoProcessamento tipoProc)
        {
            this.tipoProcessamento = tipoProc;
        }

        /// <summary>
        /// Retorna o valor de mercado, de acordo com os filtros passados. Usado para apuração das regras
        /// de enquadramento.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="operacaoCompromissada"></param>
        /// <param name="idIndice"></param>
        /// <param name="idPapel"></param>
        /// <param name="idEmissor"></param>
        /// <param name="tipoEmissor"></param>
        /// <param name="idSetor"></param>
        /// <param name="tipoPapel"></param>
        /// <returns></returns>
        public decimal RetornaValorMercadoEnquadra(int idCliente, string operacaoCompromissada, int? idIndice,
                                                   int? idPapel, int? idEmissor, int? tipoEmissor, int? idSetor,
                                                   int? tipoPapel)
        {
            PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();

            posicaoRendaFixaCollection.Query
                 .Select(posicaoRendaFixaCollection.Query.IdTitulo,
                         posicaoRendaFixaCollection.Query.TipoOperacao,
                         posicaoRendaFixaCollection.Query.ValorMercado)
                 .Where(posicaoRendaFixaCollection.Query.IdCliente == idCliente);

            posicaoRendaFixaCollection.Query.Load();

            decimal totalValorMercado = 0;

            for (int i = 0; i < posicaoRendaFixaCollection.Count; i++)
            {
                PosicaoRendaFixa posicaoRendaFixa = posicaoRendaFixaCollection[i];
                int idTituloComparar = posicaoRendaFixa.IdTitulo.Value;
                int tipoOperacao = posicaoRendaFixa.TipoOperacao.Value;
                decimal valorMercado = posicaoRendaFixa.ValorMercado.Value;

                bool passou = true;
                if (!String.IsNullOrEmpty(operacaoCompromissada))
                {
                    if (operacaoCompromissada == "S" && (tipoOperacao != (int)TipoOperacaoTitulo.CompraRevenda ||
                                                         tipoOperacao != (int)TipoOperacaoTitulo.VendaRecompra))
                        passou = false;
                    else if (operacaoCompromissada == "N" && tipoOperacao != (int)TipoOperacaoTitulo.CompraFinal)
                        passou = false;
                }

                if (passou) //Testa as condições outras
                {
                    #region Busca os campos de TituloRendaFixa
                    TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
                    List<esQueryItem> campos = new List<esQueryItem>();
                    campos.Add(tituloRendaFixa.Query.IdIndice);
                    campos.Add(tituloRendaFixa.Query.IdPapel);
                    campos.Add(tituloRendaFixa.Query.IdEmissor);
                    tituloRendaFixa.LoadByPrimaryKey(campos, idTituloComparar);

                    int? idIndiceComparar = tituloRendaFixa.IdIndice;
                    int idPapelComparar = tituloRendaFixa.IdPapel.Value;
                    int idEmissorComparar = tituloRendaFixa.IdEmissor.Value;
                    #endregion

                    #region Verifica se passa pelos filtros de Indice, Papel, Emissor, TipoPapel
                    if (idIndice.HasValue && idIndiceComparar.HasValue)
                    {
                        if (idIndiceComparar.Value != idIndice.Value)
                            passou = false;
                    }

                    if (idPapel.HasValue)
                    {
                        if (idPapelComparar != idPapel)
                            passou = false;
                    }

                    if (idEmissor.HasValue)
                    {
                        if (idEmissorComparar != idEmissor)
                            passou = false;
                    }

                    if (tipoPapel.HasValue)
                    {
                        PapelRendaFixa papelRendaFixa = new PapelRendaFixa();
                        List<esQueryItem> camposPapel = new List<esQueryItem>();
                        camposPapel.Add(papelRendaFixa.Query.TipoPapel);
                        papelRendaFixa.LoadByPrimaryKey(camposPapel, idPapelComparar);

                        if (papelRendaFixa.TipoPapel != tipoPapel.Value)
                        {
                            passou = false;
                        }
                    }
                    #endregion

                    #region Busca os campos de Setor
                    if (idSetor.HasValue || tipoEmissor.HasValue)
                    {
                        //Busca antes o IdEmissor
                        tituloRendaFixa = new TituloRendaFixa();
                        campos = new List<esQueryItem>();
                        campos.Add(tituloRendaFixa.Query.IdEmissor);
                        tituloRendaFixa.LoadByPrimaryKey(campos, idTituloComparar);
                        idEmissorComparar = tituloRendaFixa.IdEmissor.Value;
                        //

                        Emissor emissor = new Emissor();
                        campos = new List<esQueryItem>();
                        campos.Add(emissor.Query.IdSetor);
                        campos.Add(emissor.Query.TipoEmissor);
                        emissor.LoadByPrimaryKey(campos, idEmissorComparar);
                        int idSetorComparar = emissor.IdSetor.Value;
                        int tipoEmissorComparar = emissor.TipoEmissor.Value;

                        if (idSetor.HasValue)
                        {
                            if (idSetorComparar != idSetor)
                                passou = false;
                        }

                        if (tipoEmissor.HasValue)
                        {
                            if (tipoEmissorComparar != tipoEmissor)
                                passou = false;
                        }

                    }
                    #endregion
                }

                if (passou)
                    totalValorMercado += valorMercado;
            }

            return totalValorMercado;
        }
        /// <summary>
        /// Função básica para copiar de Posicao para PosicaoHistorico.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void GeraBackup(int idCliente, DateTime data)
        {
            PosicaoRendaFixaHistoricoCollection posicaoRendaFixaDeletarHistoricoCollection = new PosicaoRendaFixaHistoricoCollection();
            posicaoRendaFixaDeletarHistoricoCollection.DeletaPosicaoRendaFixaHistoricoDataHistoricoMaiorIgual(idCliente, data);

            StringBuilder sqlBuilder = new StringBuilder();
            string strSelect = "";
            string strFrom = "";

            sqlBuilder.Append("INSERT INTO PosicaoRendaFixaHistorico (");
            strSelect = " SELECT ";
            strFrom = " FROM PosicaoRendaFixa WHERE IdCliente = " + idCliente;
            int count = 0;

            PosicaoRendaFixaHistorico posicaoRendaFixaHistorico = new PosicaoRendaFixaHistorico();
            int columnCount = posicaoRendaFixaHistorico.es.Meta.Columns.Count;
            foreach (esColumnMetadata colPosicaoRendaFixa in posicaoRendaFixaHistorico.es.Meta.Columns)
            {
                count++;

                //Insert
                sqlBuilder.Append(colPosicaoRendaFixa.Name);
                if (count != columnCount)
                {
                    sqlBuilder.Append(",");
                }
                else
                {
                    sqlBuilder.Append(")");
                }

                //select 
                if (colPosicaoRendaFixa.Name == PosicaoRendaFixaHistoricoMetadata.ColumnNames.DataHistorico)
                {
                    strSelect += "'" + data.ToString("yyyyMMdd") + "'";
                }
                else
                {
                    strSelect += colPosicaoRendaFixa.Name;
                }

                if (count != columnCount)
                {
                    strSelect += ",";
                }
            }
            sqlBuilder.Append(strSelect + strFrom);
            sqlBuilder.AppendLine();

            esUtility u = new esUtility();
            u.ExecuteNonQuery(esQueryType.Text, sqlBuilder.ToString());

        }

        /// <summary>
        /// Função básica para copiar de Posicao para PosicaoAbertura.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void GeraPosicaoAbertura(int idCliente, DateTime data)
        {

            StringBuilder sqlBuilder = new StringBuilder();
            string strSelect = "";
            string strFrom = "";

            sqlBuilder.Append("INSERT INTO PosicaoRendaFixaAbertura (");
            strSelect = " SELECT ";
            strFrom = " FROM PosicaoRendaFixa WHERE IdCliente = " + idCliente;
            int count = 0;

            PosicaoRendaFixaAbertura posicaoRendaFixaAbertura = new PosicaoRendaFixaAbertura();
            int columnCount = posicaoRendaFixaAbertura.es.Meta.Columns.Count;
            foreach (esColumnMetadata colPosicaoRendaFixa in posicaoRendaFixaAbertura.es.Meta.Columns)
            {
                count++;

                //Insert
                sqlBuilder.Append(colPosicaoRendaFixa.Name);
                if (count != columnCount)
                {
                    sqlBuilder.Append(",");
                }
                else
                {
                    sqlBuilder.Append(")");
                }

                //select 
                if (colPosicaoRendaFixa.Name == PosicaoRendaFixaHistoricoMetadata.ColumnNames.DataHistorico)
                {
                    strSelect += "'" + data.ToString("yyyyMMdd") + "'";
                }
                else
                {
                    strSelect += colPosicaoRendaFixa.Name;
                }

                if (count != columnCount)
                {
                    strSelect += ",";
                }
            }
            sqlBuilder.Append(strSelect + strFrom);
            sqlBuilder.AppendLine();

            esUtility u = new esUtility();
            u.ExecuteNonQuery(esQueryType.Text, sqlBuilder.ToString());

        }

        /// <summary>
        /// Retorna o valor de mercado de todas as posições do cliente.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns>soma do valor de mercado das posições</returns>
        public decimal RetornaValorMercado(int idCliente)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorMercado.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Quantidade.NotEqual(0));

            this.Query.Load();

            return this.ValorMercado.HasValue ? this.ValorMercado.Value : 0;
        }

        /// <summary>
        /// Retorna o valor de mercado do cliente. Leva em conta o IdMoedaCliente passado e faz as devidas conversões.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="idMoedaCliente"></param>
        /// <param name="data"></param>
        /// <returns>soma do valor de mercado de todas as posições</returns>
        public decimal RetornaValorMercado(int idCliente, int idMoedaCliente, DateTime data)
        {
            PosicaoRendaFixaQuery posicaoRendaFixaQuery = new PosicaoRendaFixaQuery("P");
            TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");

            posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery.IdPosicao);
            posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
            posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente.Equal(idCliente),
                                    posicaoRendaFixaQuery.Quantidade.NotEqual(0),
                                    tituloRendaFixaQuery.IdMoeda.NotEqual(idMoedaCliente));

            PosicaoRendaFixaCollection posicaoRendaFixaCollectionExiste = new PosicaoRendaFixaCollection();
            posicaoRendaFixaCollectionExiste.Load(posicaoRendaFixaQuery);

            if (posicaoRendaFixaCollectionExiste.Count == 0)
            {
                return this.RetornaValorMercado(idCliente);
            }

            PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
            posicaoRendaFixaCollection.Query.Select(posicaoRendaFixaCollection.Query.IdTitulo, posicaoRendaFixaCollection.Query.ValorMercado.Sum());
            posicaoRendaFixaCollection.Query.Where(posicaoRendaFixaCollection.Query.IdCliente.Equal(idCliente));
            posicaoRendaFixaCollection.Query.GroupBy(posicaoRendaFixaCollection.Query.IdTitulo);
            posicaoRendaFixaCollection.Query.Load();

            decimal totalValor = 0;

            //Pega a ptax para agilizar, se o cliente estiver em dólar
            decimal ptax = 0;
            if (idMoedaCliente == (int)ListaMoedaFixo.Dolar && posicaoRendaFixaCollection.Count > 0)
            {
                CotacaoIndice cotacaoIndice = new CotacaoIndice();
                ptax = cotacaoIndice.BuscaCotacaoIndice((int)ListaIndiceFixo.PTAX_800VENDA, data);
            }

            foreach (PosicaoRendaFixa posicaoRendaFixa in posicaoRendaFixaCollection)
            {
                int idTitulo = posicaoRendaFixa.IdTitulo.Value;
                decimal valor = posicaoRendaFixa.ValorMercado.Value;

                TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(tituloRendaFixa.Query.IdMoeda);
                tituloRendaFixa.LoadByPrimaryKey(campos, idTitulo);
                int idMoedaAtivo = tituloRendaFixa.IdMoeda.Value;

                if (idMoedaCliente != idMoedaAtivo)
                {
                    decimal fatorConversao = 0;
                    if (idMoedaCliente == (int)ListaMoedaFixo.Dolar && idMoedaAtivo == (int)ListaMoedaFixo.Real)
                    {
                        fatorConversao = 1 / ptax;
                    }
                    else
                    {
                        ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
                        conversaoMoeda.Query.Where(conversaoMoeda.Query.IdMoedaDe.Equal(idMoedaCliente),
                                                   conversaoMoeda.Query.IdMoedaPara.Equal(idMoedaAtivo));
                        if (conversaoMoeda.Query.Load())
                        {
                            int idIndiceConversao = conversaoMoeda.IdIndice.Value;
                            CotacaoIndice cotacaoIndice = new CotacaoIndice();
                            if (conversaoMoeda.Tipo.Value == (byte)TipoConversaoMoeda.Divide)
                            {
                                fatorConversao = 1 / cotacaoIndice.BuscaCotacaoIndice(idIndiceConversao, data);
                            }
                            else
                            {
                                fatorConversao = cotacaoIndice.BuscaCotacaoIndice(idIndiceConversao, data);
                            }
                        }
                    }

                    valor = Math.Round(valor * fatorConversao, 2);
                }

                totalValor += valor;
            }

            return totalValor;
        }

        /// <summary>
        /// Retorna o valor de mercado liquido de IR e IOF de todas as posições do cliente.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns>soma do valor de mercado das posições</returns>
        public decimal RetornaValorMercadoLiquido(int idCliente)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorMercado.Sum(),
                         this.Query.ValorIR.Sum(),
                         this.Query.ValorIOF.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.Quantidade.NotEqual(0));

            this.Query.Load();

            decimal valorMercado = this.ValorMercado.HasValue ? this.ValorMercado.Value : 0;
            decimal valorIR = this.ValorIR.HasValue ? this.ValorIR.Value : 0;
            decimal valorIOF = this.ValorIOF.HasValue ? this.ValorIOF.Value : 0;

            return (valorMercado - valorIR - valorIOF);
        }

        /// <summary>
        /// Retorna o valor de mercado do cliente. Leva em conta o IdMoedaCliente passado e faz as devidas conversões.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="idMoedaCliente"></param>
        /// <param name="data"></param>
        /// <returns>soma do valor de mercado de todas as posições</returns>
        public decimal RetornaValorMercadoLiquido(int idCliente, int idMoedaCliente, DateTime data)
        {
            PosicaoRendaFixaQuery posicaoRendaFixaQuery = new PosicaoRendaFixaQuery("P");
            TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");

            posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery.IdPosicao);
            posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
            posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente.Equal(idCliente),
                                    posicaoRendaFixaQuery.Quantidade.NotEqual(0),
                                    tituloRendaFixaQuery.IdMoeda.NotEqual(idMoedaCliente));

            PosicaoRendaFixaCollection posicaoRendaFixaCollectionExiste = new PosicaoRendaFixaCollection();
            posicaoRendaFixaCollectionExiste.Load(posicaoRendaFixaQuery);

            if (posicaoRendaFixaCollectionExiste.Count == 0)
            {
                return this.RetornaValorMercadoLiquido(idCliente);
            }

            PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
            posicaoRendaFixaCollection.Query.Select(posicaoRendaFixaCollection.Query.IdTitulo,
                                                    posicaoRendaFixaCollection.Query.ValorMercado.Sum(),
                                                    posicaoRendaFixaCollection.Query.ValorIR.Sum(),
                                                    posicaoRendaFixaCollection.Query.ValorIOF.Sum());
            posicaoRendaFixaCollection.Query.Where(posicaoRendaFixaCollection.Query.IdCliente.Equal(idCliente));
            posicaoRendaFixaCollection.Query.GroupBy(posicaoRendaFixaCollection.Query.IdTitulo);
            posicaoRendaFixaCollection.Query.Load();

            decimal totalValor = 0;

            //Pega a ptax para agilizar, se o cliente estiver em dólar
            decimal ptax = 0;
            if (idMoedaCliente == (int)ListaMoedaFixo.Dolar && posicaoRendaFixaCollection.Count > 0)
            {
                CotacaoIndice cotacaoIndice = new CotacaoIndice();
                ptax = cotacaoIndice.BuscaCotacaoIndice((int)ListaIndiceFixo.PTAX_800VENDA, data);
            }

            foreach (PosicaoRendaFixa posicaoRendaFixa in posicaoRendaFixaCollection)
            {
                int idTitulo = posicaoRendaFixa.IdTitulo.Value;
                decimal valor = posicaoRendaFixa.ValorMercado.Value - posicaoRendaFixa.ValorIR.Value - posicaoRendaFixa.ValorIOF.Value;

                TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(tituloRendaFixa.Query.IdMoeda);
                tituloRendaFixa.LoadByPrimaryKey(campos, idTitulo);
                int idMoedaAtivo = tituloRendaFixa.IdMoeda.Value;

                if (idMoedaCliente != idMoedaAtivo)
                {
                    decimal fatorConversao = 0;
                    if (idMoedaCliente == (int)ListaMoedaFixo.Dolar && idMoedaAtivo == (int)ListaMoedaFixo.Real)
                    {
                        fatorConversao = 1 / ptax;
                    }
                    else
                    {
                        ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
                        conversaoMoeda.Query.Where(conversaoMoeda.Query.IdMoedaDe.Equal(idMoedaCliente),
                                                   conversaoMoeda.Query.IdMoedaPara.Equal(idMoedaAtivo));
                        if (conversaoMoeda.Query.Load())
                        {
                            int idIndiceConversao = conversaoMoeda.IdIndice.Value;
                            CotacaoIndice cotacaoIndice = new CotacaoIndice();
                            if (conversaoMoeda.Tipo.Value == (byte)TipoConversaoMoeda.Divide)
                            {
                                fatorConversao = 1 / cotacaoIndice.BuscaCotacaoIndice(idIndiceConversao, data);
                            }
                            else
                            {
                                fatorConversao = cotacaoIndice.BuscaCotacaoIndice(idIndiceConversao, data);
                            }
                        }
                    }

                    valor = Math.Round(valor * fatorConversao, 2);
                }

                totalValor += valor;
            }

            return totalValor;
        }

        /// <summary>
        /// Retorna o valor de mercado de todas as posições do cliente.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="tipoPapelTitulo">enum do tipo de papel (publico ou privado)</param>
        /// <returns>soma do valor de mercado das posições</returns>
        public decimal RetornaValorMercado(int idCliente, TipoPapelTitulo tipoPapelTitulo)
        {
            PosicaoRendaFixaQuery posicaoRendaFixa = new PosicaoRendaFixaQuery("P");
            TituloRendaFixaQuery tituloRendaFixa = new TituloRendaFixaQuery("T");
            PapelRendaFixaQuery papelRendaFixa = new PapelRendaFixaQuery("R");

            posicaoRendaFixa.Select(posicaoRendaFixa.ValorMercado.Sum());
            posicaoRendaFixa.InnerJoin(tituloRendaFixa).On(posicaoRendaFixa.IdTitulo == tituloRendaFixa.IdTitulo);
            posicaoRendaFixa.InnerJoin(papelRendaFixa).On(tituloRendaFixa.IdPapel == papelRendaFixa.IdPapel);
            posicaoRendaFixa.Where(posicaoRendaFixa.IdCliente == idCliente,
                                   papelRendaFixa.TipoPapel == (int)tipoPapelTitulo);

            this.QueryReset();
            this.Load(posicaoRendaFixa);

            return this.ValorMercado.HasValue ? this.ValorMercado.Value : 0;
        }

        /// <summary>
        /// Baixa todas as posições finais com vcto na data.
        /// Calcula o IR/IOF incidentes e joga na Liquidacao os valores liquidados.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void BaixaPosicaoVencimento(int idCliente, DateTime data)
        {
            DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);

            //Deleta as liquidações por vencimento na data
            LiquidacaoRendaFixaCollection liquidacaoRendaFixaCollectionDeletar = new LiquidacaoRendaFixaCollection();
            liquidacaoRendaFixaCollectionDeletar.DeletaLiquidacao(idCliente, data, (int)TipoLancamentoLiquidacao.Vencimento);
            //

            //Guarda as liquidações provenientes de vencimento
            LiquidacaoRendaFixaCollection liquidacaoRendaFixaCollection = new LiquidacaoRendaFixaCollection();
            //

            //Guarda os lançamentos em Liquidacao (ContaCorrente), de vencimento e eventual IR/IOF oriundo da baixa
            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
            //

            PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
            posicaoRendaFixaCollection.BuscaPosicaoVencimento(idCliente, data, dataAnterior);

            Cliente cliente = new Cliente();
            ClienteRendaFixa clienteRendaFixa = new ClienteRendaFixa();

            ExcecoesTributacaoIRCollection excecoesColl = new ExcecoesTributacaoIRCollection();
            if (posicaoRendaFixaCollection.Count > 0)
            {
                cliente.LoadByPrimaryKey(idCliente);
                clienteRendaFixa.LoadByPrimaryKey(idCliente);

                excecoesColl.RetornaExcessaoTributacaoIR(cliente.UpToPessoaByIdPessoa.Tipo, data, (int)TipoMercado.RendaFixa);
            }

            Hashtable hsContaAtivo = new Hashtable();
            for (int i = 0; i < posicaoRendaFixaCollection.Count; i++)
            {
                PosicaoRendaFixa posicaoRendaFixa = posicaoRendaFixaCollection[i];
                int idPosicao = posicaoRendaFixa.IdPosicao.Value;
                int idTitulo = posicaoRendaFixa.IdTitulo.Value;
                int idOperacao = posicaoRendaFixa.IdOperacao.Value;

                decimal quantidade = posicaoRendaFixa.Quantidade.Value;
                decimal valorCurva = posicaoRendaFixa.ValorCurva.Value;
                decimal valorMercado = posicaoRendaFixa.ValorMercado.Value;
                decimal valorOperacao = Utilitario.Truncate(posicaoRendaFixa.Quantidade.Value * posicaoRendaFixa.PUOperacao.Value, 2);

                AgendaRendaFixaCollection agendaRendaFixaCollection = new AgendaRendaFixaCollection();
                agendaRendaFixaCollection.Query.Select(agendaRendaFixaCollection.Query.IdAgenda);
                agendaRendaFixaCollection.Query.Where(agendaRendaFixaCollection.Query.IdTitulo.Equal(idTitulo));
                agendaRendaFixaCollection.Query.Load();

                //Se tiver fluxo, o tratamento do principal (e eventual IR sobre o principal) é feito no método TrataLiquidacaoAgenda da classe AgendaRF
                if (agendaRendaFixaCollection.Count == 0)
                {
                    TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
                    tituloRendaFixa.LoadByPrimaryKey(idTitulo);
                    int idMoedaAtivo = tituloRendaFixa.IdMoeda.Value;
                    int idContaDefault = 0;
                    #region Busca Conta Default
                    if (hsContaAtivo.Contains(idTitulo))
                    {
                        idContaDefault = (int)hsContaAtivo[idTitulo];
                    }
                    else
                    {
                        Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                        idContaDefault = contaCorrente.RetornaContaDefault(idCliente, idMoedaAtivo);
                        hsContaAtivo.Add(idTitulo, idContaDefault);
                    }
                    #endregion

                    #region Busca conta correte da operacao

                    int idConta = idContaDefault;

                    OperacaoRendaFixa orf = new OperacaoRendaFixa();
                    if (ParametrosConfiguracaoSistema.Outras.MultiConta && orf.LoadByPrimaryKey(idOperacao))
                    {
                        if (orf.IdConta.HasValue && orf.IdConta.Value > 0)
                            idConta = orf.IdConta.Value;
                    }

                    #endregion

                    decimal rendimento = valorCurva - valorOperacao;

                    TituloIsentoIR tituloIsentoIR;
                    if (tituloRendaFixa.IsentoIR.GetValueOrDefault(0) > 0)
                    {
                        tituloIsentoIR = (TituloIsentoIR)tituloRendaFixa.IsentoIR.Value;
                    }
                    else
                    {
                        PapelRendaFixa papel = tituloRendaFixa.UpToPapelRendaFixaByIdPapel;
                        tituloIsentoIR = (TituloIsentoIR)papel.IsentoIR.Value;
                    }
                    bool grossUp = cliente.GrossUP.Value != (byte)GrossupCliente.NaoFaz && tituloIsentoIR != TituloIsentoIR.NaoIsento;

                    if (grossUp)
                    {
                        posicaoRendaFixa.ValorMercado -= posicaoRendaFixa.ValorIR.Value; //Para calcular corretamente o IR do grossup no vencimento
                        posicaoRendaFixa.ValorMercado -= posicaoRendaFixa.ValorIOF.Value; //Para calcular corretamente o IR do grossup no vencimento
                    }

                    #region Cálculo de IR/IOF e eventual grossup
                    CalculoRendaFixa calculoRendaFixa = new CalculoRendaFixa();

                    ExcecoesTributacaoIR excecaoTributacao = new ExcecoesTributacaoIR();
                    excecaoTributacao = excecoesColl.ObtemPrioridade(cliente.UpToPessoaByIdPessoa.Tipo, (int)TipoMercado.RendaFixa, tituloRendaFixa.IdPapel.Value, tituloRendaFixa.IdTitulo.Value);

                    decimal iof = calculoRendaFixa.RetornaIOFOperacao(cliente, clienteRendaFixa, posicaoRendaFixa.PUMercado.Value, posicaoRendaFixa, posicaoRendaFixa.Quantidade.Value, data);

                    decimal ir = calculoRendaFixa.RetornaIRPosicao(cliente, clienteRendaFixa, posicaoRendaFixa, tituloRendaFixa, data, iof, true, excecaoTributacao);
                    #endregion

                    decimal valorBruto = valorCurva;
                    decimal valorLiquido = valorBruto - ir - iof;

                    if (grossUp)
                    {
                        valorLiquido = posicaoRendaFixa.ValorMercado.Value;

                        posicaoRendaFixa.ValorMercado += ir; //Volta o IR (embora a posição vá ser deletada)
                        posicaoRendaFixa.ValorMercado += iof; //Volta o IOF (embora a posição vá ser deletada)

                        valorBruto = posicaoRendaFixa.ValorMercado.Value;
                    }

                    #region Add na liquidacaoRendaFixaCollection
                    LiquidacaoRendaFixa liquidacaoRendaFixa = new LiquidacaoRendaFixa();
                    liquidacaoRendaFixa.TipoLancamento = (int)TipoLancamentoLiquidacao.Vencimento;
                    liquidacaoRendaFixa.IdCliente = idCliente;
                    liquidacaoRendaFixa.IdTitulo = idTitulo;
                    liquidacaoRendaFixa.DataLiquidacao = data;
                    liquidacaoRendaFixa.Quantidade = quantidade;
                    liquidacaoRendaFixa.ValorBruto = valorBruto;
                    liquidacaoRendaFixa.ValorLiquido = valorLiquido;
                    liquidacaoRendaFixa.Rendimento = rendimento;
                    liquidacaoRendaFixa.ValorIOF = iof;
                    liquidacaoRendaFixa.ValorIR = ir;
                    liquidacaoRendaFixa.IdPosicaoResgatada = idPosicao;
                    liquidacaoRendaFixaCollection.AttachEntity(liquidacaoRendaFixa);
                    #endregion

                    #region Add na liquidacao (valor do título na data)
                    string descricaoTitulo = tituloRendaFixa.Descricao;

                    Liquidacao liquidacao = new Liquidacao();
                    liquidacao.DataLancamento = data;
                    liquidacao.DataVencimento = data;
                    liquidacao.Descricao = "Vencimento do título - " + descricaoTitulo;
                    liquidacao.Valor = valorCurva;
                    liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                    liquidacao.Origem = OrigemLancamentoLiquidacao.RendaFixa.Vencimento;
                    liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                    liquidacao.IdCliente = idCliente;
                    liquidacao.IdConta = idConta;

                    liquidacaoCollection.AttachEntity(liquidacao);
                    #endregion

                    #region Add na liquidacao (IR)
                    //Lançamento de IR
                    if (ir > 0 && !grossUp)
                    {
                        liquidacao = new Liquidacao();
                        liquidacao.DataLancamento = data;
                        liquidacao.DataVencimento = data;
                        liquidacao.Descricao = "IR s/ Vencimento do título - " + descricaoTitulo;
                        liquidacao.Valor = ir * -1;
                        liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                        liquidacao.Origem = OrigemLancamentoLiquidacao.RendaFixa.IR;
                        liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                        liquidacao.IdCliente = idCliente;
                        liquidacao.IdConta = idConta;

                        liquidacaoCollection.AttachEntity(liquidacao);
                    }

                    if (iof > 0 && !grossUp)
                    {
                        liquidacao = new Liquidacao();
                        liquidacao.DataLancamento = data;
                        liquidacao.DataVencimento = data;
                        liquidacao.Descricao = "IOF s/ Vencimento do título - " + descricaoTitulo;
                        liquidacao.Valor = iof * -1;
                        liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                        liquidacao.Origem = OrigemLancamentoLiquidacao.RendaFixa.IOF;
                        liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                        liquidacao.IdCliente = idCliente;
                        liquidacao.IdConta = idConta;

                        liquidacaoCollection.AttachEntity(liquidacao);
                    }
                    //  
                    #endregion
                }
            }

            if (posicaoRendaFixaCollection.Count > 0)
            {
                //Tratar custodia anual
                TabelaCustosRendaFixa tabelaCustosRendaFixa = new TabelaCustosRendaFixa();
                tabelaCustosRendaFixa.TrataPosicoesLiquidadas(idCliente, data, posicaoRendaFixaCollection);
            }

            liquidacaoRendaFixaCollection.Save();
            liquidacaoCollection.Save();

            //Deleta todas as posições que vencem no dia
            posicaoRendaFixaCollection.MarkAllAsDeleted();
            posicaoRendaFixaCollection.Save();
            //
        }

        /// <summary>
        /// Baixa todas as posições de compromisso (compra c/ revenda, venda c/ recompra) com "volta" na data.
        /// Joga na Liquidacao os valores liquidados.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void BaixaPosicaoCompromissada(int idCliente, DateTime data)
        {
            DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);

            //Deleta as liquidações por vencimento na data
            LiquidacaoRendaFixaCollection liquidacaoRendaFixaCollectionDeletar = new LiquidacaoRendaFixaCollection();
            liquidacaoRendaFixaCollectionDeletar.DeletaLiquidacao(idCliente, data, (int)TipoLancamentoLiquidacao.Recompra);
            liquidacaoRendaFixaCollectionDeletar.DeletaLiquidacao(idCliente, data, (int)TipoLancamentoLiquidacao.Revenda);
            //

            //Guarda as liquidações provenientes de vencimento
            LiquidacaoRendaFixaCollection liquidacaoRendaFixaCollection = new LiquidacaoRendaFixaCollection();
            //

            //Guarda os lançamentos em Liquidacao (ContaCorrente)
            LiquidacaoCollection liquidacaoCollection = new LiquidacaoCollection();
            //

            PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
            posicaoRendaFixaCollection.BuscaPosicaoVoltaCompromisso(idCliente, data, dataAnterior);

            Cliente cliente = new Cliente();
            ClienteRendaFixa clienteRendaFixa = new ClienteRendaFixa();

            if (posicaoRendaFixaCollection.Count > 0)
            {
                cliente.LoadByPrimaryKey(idCliente);
                clienteRendaFixa.LoadByPrimaryKey(idCliente);
            }

            Hashtable hsContaAtivo = new Hashtable();
            for (int i = 0; i < posicaoRendaFixaCollection.Count; i++)
            {
                PosicaoRendaFixa posicaoRendaFixa = posicaoRendaFixaCollection[i];
                int idPosicao = posicaoRendaFixa.IdPosicao.Value;
                int idTitulo = posicaoRendaFixa.IdTitulo.Value;
                int tipoOperacao = posicaoRendaFixa.TipoOperacao.Value;
                int idOperacao = posicaoRendaFixa.IdOperacao.Value;

                decimal quantidade = posicaoRendaFixa.Quantidade.Value;
                decimal valorCurva = posicaoRendaFixa.ValorCurva.Value;
                decimal valorOperacao = Utilitario.Truncate(posicaoRendaFixa.Quantidade.Value * posicaoRendaFixa.PUOperacao.Value, 2);
                decimal rendimento = valorCurva - valorOperacao;

                TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
                tituloRendaFixa.LoadByPrimaryKey(idTitulo);

                int idMoedaAtivo = tituloRendaFixa.IdMoeda.Value;
                int idContaDefault = 0;
                #region Busca Conta Default
                if (hsContaAtivo.Contains(idTitulo))
                {
                    idContaDefault = (int)hsContaAtivo[idTitulo];
                }
                else
                {
                    Investidor.ContaCorrente contaCorrente = new Investidor.ContaCorrente();
                    idContaDefault = contaCorrente.RetornaContaDefault(idCliente, idMoedaAtivo);
                    hsContaAtivo.Add(idTitulo, idContaDefault);
                }
                #endregion

                #region Busca conta correte da operacao

                int idConta = idContaDefault;

                OperacaoRendaFixa orf = new OperacaoRendaFixa();
                if (ParametrosConfiguracaoSistema.Outras.MultiConta && orf.LoadByPrimaryKey(idOperacao))
                {
                    if (orf.IdConta.HasValue && orf.IdConta.Value > 0)
                        idConta = orf.IdConta.Value;
                }

                #endregion

                CalculoRendaFixa calculoRendaFixa = new CalculoRendaFixa();
                decimal ir = calculoRendaFixa.RetornaIRPosicao(cliente, clienteRendaFixa, posicaoRendaFixa, tituloRendaFixa, data, 0, true, null);

                #region Add na liquidacao (valor do título na data)
                string descricaoTitulo = tituloRendaFixa.Descricao;

                Liquidacao liquidacao = new Liquidacao();
                liquidacao.DataLancamento = data;
                liquidacao.DataVencimento = data;

                if (tipoOperacao == (int)TipoOperacaoTitulo.CompraRevenda)
                {
                    liquidacao.Descricao = "Revenda - " + descricaoTitulo;
                    liquidacao.Origem = OrigemLancamentoLiquidacao.RendaFixa.Revenda;
                }
                else
                    liquidacao.Descricao = "Recompra - " + descricaoTitulo;
                liquidacao.Origem = OrigemLancamentoLiquidacao.RendaFixa.Recompra;

                liquidacao.Valor = valorCurva;
                liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                liquidacao.IdCliente = idCliente;
                liquidacao.IdConta = idConta;

                liquidacaoCollection.AttachEntity(liquidacao);
                #endregion

                #region Add na liquidacao (IR)
                //Lançamento de IR
                if (ir > 0)
                {
                    liquidacao = new Liquidacao();
                    liquidacao.DataLancamento = data;
                    liquidacao.DataVencimento = data;
                    liquidacao.Descricao = "IR s/ Vencimento (compromissado) do título - " + descricaoTitulo;
                    liquidacao.Valor = ir * -1;
                    liquidacao.Situacao = (byte)SituacaoLancamentoLiquidacao.Normal;
                    liquidacao.Origem = OrigemLancamentoLiquidacao.RendaFixa.IR;
                    liquidacao.Fonte = (byte)FonteLancamentoLiquidacao.Interno;
                    liquidacao.IdCliente = idCliente;
                    liquidacao.IdConta = idConta;

                    liquidacaoCollection.AttachEntity(liquidacao);
                }
                //                
                #endregion

                #region Add na liquidacaoRendaFixaCollection
                LiquidacaoRendaFixa liquidacaoRendaFixa = new LiquidacaoRendaFixa();

                if (tipoOperacao == (int)TipoOperacaoTitulo.CompraRevenda)
                    liquidacaoRendaFixa.TipoLancamento = (byte)TipoLancamentoLiquidacao.Revenda;
                else
                    liquidacaoRendaFixa.TipoLancamento = (byte)TipoLancamentoLiquidacao.Recompra;

                liquidacaoRendaFixa.IdCliente = idCliente;
                liquidacaoRendaFixa.IdTitulo = idTitulo;
                liquidacaoRendaFixa.DataLiquidacao = data;
                liquidacaoRendaFixa.Quantidade = quantidade;
                liquidacaoRendaFixa.Rendimento = rendimento;
                liquidacaoRendaFixa.ValorBruto = valorCurva;
                liquidacaoRendaFixa.ValorLiquido = valorCurva;
                liquidacaoRendaFixa.ValorIOF = 0;
                liquidacaoRendaFixa.ValorIR = ir;
                liquidacaoRendaFixa.IdPosicaoResgatada = idPosicao;
                liquidacaoRendaFixaCollection.AttachEntity(liquidacaoRendaFixa);
                #endregion
            }

            liquidacaoRendaFixaCollection.Save();
            liquidacaoCollection.Save();

            //Deleta todas as posições que vencem no dia
            posicaoRendaFixaCollection.MarkAllAsDeleted();
            posicaoRendaFixaCollection.Save();
            //
        }

        /// <summary>
        /// Atualiza as posições finais e compromissadas (PU, financeiro, impostos) do cliente na data.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="enumTipoProcessamento"></param>
        public void AtualizaPosicao(int idCliente, DateTime data)
        {
            TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();


            CalculoRendaFixa calculoRendaFixa = new CalculoRendaFixa();

            Cliente cliente = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(cliente.Query.CalculaContabil);
            campos.Add(cliente.Query.IdLocal);
            cliente.LoadByPrimaryKey(campos, idCliente);

            Carteira carteira = new Carteira();
            campos = new List<esQueryItem>();
            campos.Add(carteira.Query.TipoCota);
            bool cotaAbertura = false;
            if (carteira.LoadByPrimaryKey(campos, idCliente))
            {
                cotaAbertura = carteira.TipoCota.Value == (byte)TipoCotaFundo.Abertura;
            }

            PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
            posicaoRendaFixaCollection.BuscaPosicao(idCliente);

            DateTime dataAnterior = new DateTime();

            #region Método Fincs            
            Hashtable hsMemoria = new Hashtable();
            #endregion

            if (posicaoRendaFixaCollection.Count > 0)
            {
                dataAnterior = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            }

            bool primeiroDia;
            for (int i = 0; i < posicaoRendaFixaCollection.Count; i++)
            {
                try
                {
                    PosicaoRendaFixa posicaoRendaFixa = posicaoRendaFixaCollection[i];

                    primeiroDia = (posicaoRendaFixa.DataOperacao.Value == data);

                    //Busca titulo, papel e indice
                    tituloRendaFixa = posicaoRendaFixa.UpToTituloRendaFixaByIdTitulo;
                    PapelRendaFixa papelRendaFixa = tituloRendaFixa.UpToPapelRendaFixaByIdPapel;

                    bool parametrosAvancados = tituloRendaFixa.ParametrosAvancados.Equals("S");
                    int idTitulo = posicaoRendaFixa.IdTitulo.Value;
                    decimal quantidade = posicaoRendaFixa.Quantidade.Value;
                    int tipoOperacao = posicaoRendaFixa.TipoOperacao.Value;
                    int classe = papelRendaFixa.Classe.Value;
                    DateTime dtEmissao = tituloRendaFixa.DataEmissao.Value;
                    DateTime dtVencimento = tituloRendaFixa.DataVencimento.Value;

                    posicaoRendaFixa.AliquotaIOF = 0;
                    posicaoRendaFixa.AliquotaIR = 0;

                    if (!primeiroDia && data > posicaoRendaFixa.DataOperacao.Value)
                    {
                        posicaoRendaFixa.PrazoDecorridoDC = Calendario.NumeroDias(posicaoRendaFixa.DataOperacao.Value, data);
                        if (cliente.IdLocal.Value != (int)LocalFeriadoFixo.Brasil)
                            posicaoRendaFixa.PrazoDecorridoDU = Calendario.NumeroDias(posicaoRendaFixa.DataOperacao.Value, data, cliente.IdLocal.Value, TipoFeriado.Outros);
                        else
                            posicaoRendaFixa.PrazoDecorridoDU = Calendario.NumeroDias(posicaoRendaFixa.DataOperacao.Value, data, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                    }
                    else
                    {
                        posicaoRendaFixa.PrazoDecorridoDC = 0;
                        posicaoRendaFixa.PrazoDecorridoDU = 0;
                    }

                    Indice indice = new Indice();
                    if (tituloRendaFixa.IdIndice.HasValue)
                    {
                        indice.LoadByPrimaryKey(tituloRendaFixa.IdIndice.Value);
                    }

                    if (primeiroDia && parametrosAvancados)
                    {
                        if (tipoOperacao == (int)TipoOperacaoTitulo.CompraRevenda || tipoOperacao == (int)TipoOperacaoTitulo.VendaRecompra)
                            continue;

                        //Gera a memória de cálculo para o primeiro dia da operação, para o cálculo do PrazoMédio
                        calculoRendaFixa.CalculaPosicao_Fincs(data, papelRendaFixa, tituloRendaFixa, posicaoRendaFixa.PUOperacao.Value, null, null, posicaoRendaFixa.IdOperacao.Value, (int)TipoPreco.Curva, (int)tipoProcessamento, true, ref hsMemoria);
                    }
                    else
                    {
                        if (tipoOperacao == (int)TipoOperacaoTitulo.CompraRevenda || tipoOperacao == (int)TipoOperacaoTitulo.VendaRecompra ||
                            (tituloRendaFixa.DataVencimento.Value <= data && tituloRendaFixa.DataVencimento.Value >= dataAnterior) ||
                            ParametrosConfiguracaoSistema.Outras.CalculaContatil ||
                            cotaAbertura)
                        {
                            decimal taxa = 0;
                            if (posicaoRendaFixa.TaxaOperacao.HasValue && posicaoRendaFixa.TaxaOperacao.Value != 0)
                            {
                                taxa = posicaoRendaFixa.TaxaOperacao.Value;
                            }
                            else if (tituloRendaFixa.Taxa.HasValue)
                            {
                                taxa = tituloRendaFixa.Taxa.Value;
                            }

                            decimal puAtualizado = 0;
                            decimal puJuros = 0;
                            decimal puCorrecao = 0;
                            PosicaoAtualizada posicaoAtualizada = new PosicaoAtualizada();

                            if (tipoOperacao == (int)TipoOperacaoTitulo.CompraRevenda ||
                                tipoOperacao == (int)TipoOperacaoTitulo.VendaRecompra)
                            {
                                if (!posicaoRendaFixa.IdIndiceVolta.HasValue)
                                {
                                    //Compromissada pre-fixada
                                    puAtualizado = calculoRendaFixa.RetornaCurvaCompromissadaPre(posicaoRendaFixa, data);
                                }
                                else
                                {
                                    //Compromissada pós-fixada
                                    puAtualizado = calculoRendaFixa.RetornaCurvaCompromissadaPos(posicaoRendaFixa, data);
                                }
                            }
                            else if (parametrosAvancados)
                            {
                                List<double> lstTaxas = new List<double>();
                                lstTaxas.Add((double)taxa);

                                puAtualizado = (calculoRendaFixa.CalculaPosicao_Fincs(data, papelRendaFixa, tituloRendaFixa, null, lstTaxas, null, posicaoRendaFixa.IdOperacao.Value, (int)TipoPreco.Curva, (int)this.tipoProcessamento, true, ref hsMemoria)).PuAtualizado;
                                puAtualizado = (puAtualizado == 0 ? posicaoRendaFixa.PUCurva.Value : puAtualizado);
                            }
                            else
                            {
                                bool temFluxoPagtoCorrecao = false;
                                bool temFluxoDesconto = false;
                                #region Checa se tem algum dos 2 tipos de fluxo (com desconto ou corrigido)
                                AgendaRendaFixaCollection agendaRendaFixaCollection = new AgendaRendaFixaCollection();
                                agendaRendaFixaCollection.Query.Select(agendaRendaFixaCollection.Query.IdAgenda);
                                agendaRendaFixaCollection.Query.Where(agendaRendaFixaCollection.Query.IdTitulo.Equal(idTitulo) &
                                                                      agendaRendaFixaCollection.Query.TipoEvento.NotIn((byte)TipoEventoTitulo.PagamentoCorrecao) &
                                                                      agendaRendaFixaCollection.Query.DataEvento.GreaterThanOrEqual(data) &
                                                                      (agendaRendaFixaCollection.Query.Taxa.NotEqual(0) | agendaRendaFixaCollection.Query.Valor.NotEqual(0)));
                                agendaRendaFixaCollection.Query.Load();
                                temFluxoDesconto = agendaRendaFixaCollection.Count > 0;

                                agendaRendaFixaCollection = new AgendaRendaFixaCollection();
                                agendaRendaFixaCollection.Query.Select(agendaRendaFixaCollection.Query.IdAgenda);
                                agendaRendaFixaCollection.Query.Where(agendaRendaFixaCollection.Query.IdTitulo.Equal(idTitulo) &
                                                                      agendaRendaFixaCollection.Query.TipoEvento.Equal((byte)TipoEventoTitulo.PagamentoCorrecao) &
                                                                      agendaRendaFixaCollection.Query.DataEvento.GreaterThanOrEqual(data));
                                agendaRendaFixaCollection.Query.Load();
                                temFluxoPagtoCorrecao = agendaRendaFixaCollection.Count > 0;
                                #endregion

                                if (temFluxoDesconto && classe != (int)ClasseRendaFixa.PosFixado_FluxoCorrigido) //PU Calculado a partir do desconto dos fluxos informados em AgendaRendaFixa
                                {
                                    if (temFluxoPagtoCorrecao)
                                    {
                                        taxa = posicaoRendaFixa.TaxaOperacao.HasValue ? posicaoRendaFixa.TaxaOperacao.Value : 0; //IMPORTANTE PARA EVITAR USAR A TAXA DE % DO INDICE DA POSICAORENDAFIXA

                                        if (taxa == 0)
                                        {
                                            taxa = tituloRendaFixa.Taxa.HasValue ? tituloRendaFixa.Taxa.Value : 0;
                                        }
                                    }

                                    posicaoAtualizada = calculoRendaFixa.CalculaPUFluxoDesconto(data, papelRendaFixa, tituloRendaFixa, posicaoRendaFixa, indice, taxa);

                                    puAtualizado = posicaoAtualizada.PuAtualizado;
                                    puJuros = posicaoAtualizada.PuJuros;
                                    puCorrecao = posicaoAtualizada.PuCorrecao;
                                }
                                else if (temFluxoPagtoCorrecao && (!temFluxoDesconto || classe == (int)ClasseRendaFixa.PosFixado_FluxoCorrigido))
                                {
                                    posicaoAtualizada = calculoRendaFixa.CalculaPUFluxoCorrigido(data, papelRendaFixa, tituloRendaFixa, posicaoRendaFixa, indice);

                                    puAtualizado = posicaoAtualizada.PuAtualizado;
                                    puJuros = posicaoAtualizada.PuJuros;
                                    puCorrecao = posicaoAtualizada.PuCorrecao;
                                }

                                if (!temFluxoDesconto && !temFluxoPagtoCorrecao)
                                {
                                    //Pós-fixado padrão
                                    if (papelRendaFixa.TipoRentabilidade.Value == (byte)TipoRentabilidadeTitulo.PosFixado && indice.IdIndice.HasValue)
                                    {
                                        posicaoAtualizada = calculoRendaFixa.RetornaCurvaCETIP_Pos(data, dataAnterior, papelRendaFixa, tituloRendaFixa, posicaoRendaFixa, indice, ref taxa);

                                        puAtualizado = posicaoAtualizada.PuAtualizado;
                                        puJuros = posicaoAtualizada.PuJuros;
                                        puCorrecao = posicaoAtualizada.PuCorrecao;

                                        //ARTIFICIO P/ GUARDAR A TAXA RELATIVA AO % DO INDICE NA POSICAORENDAFIXA, SOBRETUDO PARA INDICE ESCALONADO
                                        posicaoRendaFixa.TaxaVolta = taxa;
                                    }
                                    else
                                    {
                                        //Pré-fixado padrão
                                        switch (classe)
                                        {
                                            case (int)ClasseRendaFixa.LTN:
                                            case (int)ClasseRendaFixa.Pre_Descontado:
                                                puAtualizado = calculoRendaFixa.RetornaCurvaPreDescontado(data, dataAnterior, papelRendaFixa, tituloRendaFixa, posicaoRendaFixa, taxa);
                                                puJuros = puAtualizado;
                                                puCorrecao = puAtualizado;
                                                break;
                                            case (int)ClasseRendaFixa.Pre_Atualizado:
                                                puAtualizado = calculoRendaFixa.RetornaCurvaPreAtualizado(data, dataAnterior, papelRendaFixa, tituloRendaFixa, posicaoRendaFixa, taxa);
                                                puJuros = puAtualizado;
                                                puCorrecao = puAtualizado;
                                                break;
                                            default:
                                                puAtualizado = calculoRendaFixa.RetornaCurvaPreAtualizado(data, dataAnterior, papelRendaFixa, tituloRendaFixa, posicaoRendaFixa, taxa);
                                                puJuros = puAtualizado;
                                                puCorrecao = puAtualizado;
                                                break;
                                        }
                                    }
                                }
                            }

                            decimal valorAtualizado = Math.Round(quantidade * puAtualizado, 2);

                            posicaoRendaFixa.PUCurva = puAtualizado;
                            posicaoRendaFixa.ValorCurva = valorAtualizado;

                            //if (data == tituloRendaFixa.DataVencimento.Value) //PAS-2049
                            //{
                            posicaoRendaFixa.PUMercado = puAtualizado;
                            posicaoRendaFixa.ValorMercado = valorAtualizado;
                            //}
                        }
                    }
                }
                catch (Exception ex)
                {
                    if (ParametrosConfiguracaoSistema.Outras.AbortaProcRFPorErroCadastraTitulo.Equals("S"))
                    {
                        throw ex;
                    }
                }
            }


            posicaoRendaFixaCollection.Save();
        }

        /// <summary>
        /// Realiza a marcação a mercado dos títulos. 
        /// Métodos previstos:
        /// -> Andima
        /// -> Séries de renda fixa
        /// Atualiza apenas títulos de posições com tipo de negociação diferente de "mantidos até o vencimento".
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void MarcaMTM(int idCliente, DateTime data)
        {
            Hashtable hsMemoria = new Hashtable();
            this.MarcaMTM(idCliente, data, ref hsMemoria);
        }

        /// <summary>
        /// Realiza a marcação a mercado dos títulos. 
        /// Métodos previstos:
        /// -> Andima
        /// -> Séries de renda fixa
        /// Atualiza apenas títulos de posições com tipo de negociação diferente de "mantidos até o vencimento".
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="hsMemoria">HashTable para diminuir o acesso ao BD repetitivamente, a Key deve ser setada no Enum do Projeto Util</param>
        public void MarcaMTM(int idCliente, DateTime data, ref Hashtable hsMemoria)
        {
            #region Declaração de Objetos
            PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
            PosicaoRendaFixaCollection posicaoRendaFixaCollectionManual = new PosicaoRendaFixaCollection();
            PosicaoRendaFixaQuery posicaoRendaFixaQuery;
            MTMManualQuery mTMManualQuery;
            OperacaoRendaFixaQuery operacaoRendaFixaQuery;
            TaxaCurvaCollection objTaxaCurvaCollection = new TaxaCurvaCollection();
            PerfilMTMCollection objPerfilMTMCollection;
            MTMManualCollection mtmManualColl;
            PosicaoRendaFixa posicaoRendaFixa;
            SerieRendaFixa objSerieRendaFixa;
            ControllerRendaFixa controllerRendaFixa = new ControllerRendaFixa();
            VigenciaCategoriaCollection vigenciaCategoriaCollection;
            TituloRendaFixa titulo;
            PerfilMTM objPerfilMTM;
            Carteira carteira;
            DateTime dataVencimento;
            int idTitulo;
            int? tipoMTM;
            int? IdSerie;
            int idPapel;
            bool calculaMTM = false;
            int? idGrupoPerfilMTM = null;
            int idOperacao;
            List<MTMManual> lstMTMManual = new List<MTMManual>();
            #endregion

            #region Controle de Memoria Inicio
            Hashtable hsPerfilMTM;
            int keyPerfilMTM = (int)Financial.Util.Enums.KeyHashTable.PerfilMTM;
            if (hsMemoria.Contains(keyPerfilMTM))
                hsPerfilMTM = (Hashtable)hsMemoria[keyPerfilMTM];
            else
                hsPerfilMTM = new Hashtable();
            #endregion

            #region Popula carteira
            carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCliente);
            if (carteira.CalculaMTM == Convert.ToInt32(ExecutaFuncao.Sim))
            {
                calculaMTM = true;

                if (carteira.IdGrupoPerfilMTM.GetValueOrDefault(0) != 0)
                    idGrupoPerfilMTM = carteira.IdGrupoPerfilMTM.Value;
                else
                    throw new Exception("Cliente não está vinculado a nenhum Grupo MTM");
            }

            #endregion

            #region Popula carteira
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);
            #endregion

            #region Carrega MTMManual
            mtmManualColl = new MTMManualCollection();
            mtmManualColl.Query.Where(mtmManualColl.Query.IdCliente.Equal(idCliente)
                                      & mtmManualColl.Query.DtVigencia.Equal(data));

            if (mtmManualColl.Query.Load())
                lstMTMManual = (List<MTMManual>)mtmManualColl;
            #endregion

            #region Calcula MTM por fonte de Preço
            posicaoRendaFixaQuery = new PosicaoRendaFixaQuery("posicaoRF");
            mTMManualQuery = new MTMManualQuery("MTMManual");
            operacaoRendaFixaQuery = new OperacaoRendaFixaQuery("operacaoRendaFixa");

            posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery,
                                         operacaoRendaFixaQuery.TipoNegociacao.Coalesce("0"));
            posicaoRendaFixaQuery.InnerJoin(operacaoRendaFixaQuery).On(posicaoRendaFixaQuery.IdOperacao.Equal(operacaoRendaFixaQuery.IdOperacao));
            posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente.Equal(idCliente),
                                        posicaoRendaFixaQuery.DataVolta.IsNull(),
                                        posicaoRendaFixaQuery.Quantidade.NotEqual(0),
                                        posicaoRendaFixaQuery.DataVencimento.GreaterThan(data));
            posicaoRendaFixaCollection.Load(posicaoRendaFixaQuery);

            for (int i = 0; i < posicaoRendaFixaCollection.Count; i++)
            {

                posicaoRendaFixa = posicaoRendaFixaCollection[i];

                idTitulo = posicaoRendaFixa.IdTitulo.Value;
                dataVencimento = posicaoRendaFixa.DataVencimento.Value;
                titulo = posicaoRendaFixa.UpToTituloRendaFixaByIdTitulo;
                idOperacao = posicaoRendaFixa.IdOperacao.Value;

                bool parametrosAvancados = posicaoRendaFixa.UpToTituloRendaFixaByIdTitulo.ParametrosAvancados.Equals("S");

                posicaoRendaFixa.AliquotaIOF = 0;
                posicaoRendaFixa.AliquotaIR = 0;

                if (!posicaoRendaFixa.DataOperacao.Equals(data) && data > posicaoRendaFixa.DataOperacao.Value)
                {
                    posicaoRendaFixa.PrazoDecorridoDC = Calendario.NumeroDias(posicaoRendaFixa.DataOperacao.Value, data);
                    if (cliente.IdLocal.Value != (int)LocalFeriadoFixo.Brasil)
                        posicaoRendaFixa.PrazoDecorridoDU = Calendario.NumeroDias(posicaoRendaFixa.DataOperacao.Value, data, cliente.IdLocal.Value, TipoFeriado.Outros);
                    else
                        posicaoRendaFixa.PrazoDecorridoDU = Calendario.NumeroDias(posicaoRendaFixa.DataOperacao.Value, data, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                }
                else
                {
                    posicaoRendaFixa.PrazoDecorridoDC = 0;
                    posicaoRendaFixa.PrazoDecorridoDU = 0;
                }

                posicaoRendaFixa.PUCurvaVencimento = 0;
                posicaoRendaFixa.ValorCurvaVencimento = 0;
                posicaoRendaFixa.AjusteVencimento = 0;
                posicaoRendaFixa.PUMercado = posicaoRendaFixa.PUCurva;
                posicaoRendaFixa.ValorMercado = posicaoRendaFixa.ValorCurva;

                #region MTM Forçado
                if (lstMTMManual.Count > 0)
                {
                    MTMManual mtmManual = lstMTMManual.Find(delegate(MTMManual x) { return x.IdOperacao == idOperacao; });
                    if (mtmManual == null || mtmManual.IdMTMManual == 0)
                    {
                        mtmManual = lstMTMManual.Find(delegate(MTMManual x) { return x.IdCliente == idCliente && x.IdTitulo == idTitulo && x.IdOperacao == null; });

                        mtmManual = mtmManual == null ? new MTMManual() : mtmManual;
                    }

                    if (mtmManual.Taxa252.GetValueOrDefault(0) + mtmManual.PuMTM.GetValueOrDefault(0) > 0)
                    {
                        this.MarcaMTMManual(data, mtmManual, posicaoRendaFixa, ref hsMemoria);
                        continue;
                    }
                }
                #endregion

                #region Verifica categoria da Operação
                vigenciaCategoriaCollection = new VigenciaCategoriaCollection();
                vigenciaCategoriaCollection.Query.es.Top = 1;
                vigenciaCategoriaCollection.Query.Where(vigenciaCategoriaCollection.Query.IdOperacao.Equal(posicaoRendaFixa.IdOperacao.Value) &
                                                        vigenciaCategoriaCollection.Query.DataVigencia.LessThanOrEqual(data));
                vigenciaCategoriaCollection.Query.OrderBy(vigenciaCategoriaCollection.Query.DataVigencia.Descending);

                int? tipoVigenciaCategoria = null;
                if (vigenciaCategoriaCollection.Load(vigenciaCategoriaCollection.Query))
                {
                    //Última alteração realizada
                    tipoVigenciaCategoria = Convert.ToInt32(((TipoVigenciaCategoria)vigenciaCategoriaCollection[0].TipoVigenciaCategoria));
                }

                if (!tipoVigenciaCategoria.HasValue)
                {
                    //Se não houve alteração de categoria no meio da vida da operação, verifica se a mesma já nasceu como Vencimento
                    if (Convert.ToInt32(posicaoRendaFixa.GetColumn(OperacaoRendaFixaMetadata.ColumnNames.TipoNegociacao)) == (int)TipoNegociacaoTitulo.Vencimento)
                    {
                        posicaoRendaFixa.PUCurvaVencimento = posicaoRendaFixa.PUMercado;
                        posicaoRendaFixa.AjusteVencimento = 0;
                        if (parametrosAvancados)
                            continue;
                    }
                }
                else if (tipoVigenciaCategoria.Value == (int)TipoVigenciaCategoria.Vencimento)
                {
                    this.MarcaMTMVencimento(data, posicaoRendaFixa, titulo, ref hsMemoria);
                    continue;
                }
                #endregion

                if (calculaMTM)
                {
                    objPerfilMTMCollection = new PerfilMTMCollection();
                    objPerfilMTM = new PerfilMTM();
                    objSerieRendaFixa = new SerieRendaFixa();
                    tipoMTM = null;
                    IdSerie = null;
                    idPapel = (int)titulo.IdPapel;

                    string chave = idPapel + "|" + idGrupoPerfilMTM.Value;

                    if (!hsPerfilMTM.Contains(chave))
                    {
                        objPerfilMTMCollection = objPerfilMTMCollection.obterPerfilMTMCollection(idPapel, idGrupoPerfilMTM.Value, data);
                        if (objPerfilMTMCollection.Count > 0)
                            hsPerfilMTM.Add(chave, objPerfilMTMCollection);
                        else
                        {
                            if (ParametrosConfiguracaoSistema.RendaFixaSwap.EmiteMsgSemCotacao)
                            {
                                GrupoPerfilMTM grupoPerfilMTM = new GrupoPerfilMTM();
                                grupoPerfilMTM.LoadByPrimaryKey(idGrupoPerfilMTM.Value);
                                throw new Exception("Não existe perfil cadastrado para o Papel - " + idPapel + " do Grupo - " + grupoPerfilMTM.Descricao);
                            }
                            else
                            {
                                continue; // Sem perfil Cadastrado
                            }
                        }
                    }

                    objPerfilMTMCollection = (PerfilMTMCollection)hsPerfilMTM[chave];
                    objPerfilMTM = objPerfilMTM.obtemPerfilMTM_Prioridade(posicaoRendaFixa.IdOperacao, posicaoRendaFixa.IdTitulo, objPerfilMTMCollection);

                    if (objPerfilMTM.IdSerie.HasValue)
                    {
                        objSerieRendaFixa = objPerfilMTM.UpToSerieRendaFixaByIdSerie;
                        tipoMTM = objSerieRendaFixa.TipoTaxa;
                        IdSerie = objSerieRendaFixa.IdSerie;
                    }

                    if (tipoMTM.HasValue && tipoMTM.Value != (int)TipoMTMTitulo.NaoFaz)
                    {
                        if (tipoMTM == (int)TipoMTMTitulo.Serie)
                        {
                            this.MarcaMTMSerie(data, posicaoRendaFixa, titulo, IdSerie, ref hsMemoria);
                        }
                        else if (tipoMTM == (int)TipoMTMTitulo.SerieTaxa)
                        {
                            this.MarcaMTMSerieTaxa(data, posicaoRendaFixa, titulo, IdSerie, ref hsMemoria);
                        }
                        else if (tipoMTM == (int)TipoMTMTitulo.Andima_TituloPublico && dataVencimento > data)
                        {
                            this.MarcaMTMAnbima(data, posicaoRendaFixa, titulo, ref hsMemoria);
                        }
                        else if (tipoMTM == (int)TipoMTMTitulo.Tesouro && dataVencimento > data)
                        {
                            this.MarcaMTMTesouro(data, posicaoRendaFixa, titulo);
                        }
                        else if (tipoMTM == (int)TipoMTMTitulo.Mercado_Debenture && dataVencimento > data)
                        {
                            this.MarcaMTM_MercadoDebenture(data, posicaoRendaFixa, titulo);
                        }
                        else if (tipoMTM == (int)TipoMTMTitulo.Par_Debenture && dataVencimento > data)
                        {
                            this.MarcaMTM_ParDebenture(data, posicaoRendaFixa, titulo);
                        }
                        else if (tipoMTM == (int)TipoMTMTitulo.CotaAnbima)
                        {
                            this.MarcaMTMCotaAnbima(data, posicaoRendaFixa, titulo);
                        }
                        else if (tipoMTM == (int)TipoMTMTitulo.BovespaBMF)
                        {
                            this.MarcaMTMCotacaoBovespa(data, posicaoRendaFixa, titulo);
                        }
                        else if (tipoMTM == (int)TipoMTMTitulo.Curva)
                        {
                            this.MarcaMTMCurva(data, posicaoRendaFixa, titulo, IdSerie, ref hsMemoria);
                        }
                        else if (tipoMTM == (int)TipoMTMTitulo.SerieTaxaPuFace)
                        {
                            this.MarcaMTMSerieTaxaPuFace(data, posicaoRendaFixa, titulo, IdSerie, ref hsMemoria);
                        }
                        else
                        {
                            throw new Exception(" Tipo de MTM " + TipoMTMTituloDescricao.RetornaDescricao(tipoMTM.Value) + " não implementado para Renda Fixa! ");
                        }
                    }

                    posicaoRendaFixa.AjusteMTM = posicaoRendaFixa.PUMercado.Value - posicaoRendaFixa.PUCurva.Value;
                }
            }

            posicaoRendaFixaCollection.Save();

            #region Controle de Memoria Fim
            if (!hsMemoria.Contains(keyPerfilMTM))
                hsMemoria.Add(keyPerfilMTM, hsPerfilMTM);
            #endregion

            #endregion
        }

        /// <summary>
        /// Método private para tratar MTM dos títulos pela série de renda fixa.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="posicaoRendaFixa"></param>
        /// <param name="titulo"></param>
        private void MarcaMTMSerie(DateTime data, PosicaoRendaFixa posicaoRendaFixa, TituloRendaFixa titulo, int? idSerie, ref Hashtable hsMemoria)
        {
            //Atenção: este método é auxiliar para o escopo superior, ptt o save é dado na collection no caller!
            CalculoRendaFixa calculoRendaFixa = new CalculoRendaFixa();
            decimal quantidade = posicaoRendaFixa.Quantidade.Value;
            decimal pu = 0;
            decimal valorMercado;
            decimal taxa = 0;

            PapelRendaFixa papelRendaFixa = titulo.UpToPapelRendaFixaByIdPapel;

            try
            {
                if (idSerie.HasValue)
                {
                    CotacaoSerie cotacaoSerie = new CotacaoSerie();
                    if (cotacaoSerie.LoadByPrimaryKey(data, idSerie.Value))
                    {
                        pu = cotacaoSerie.Cotacao.Value;
                        valorMercado = Math.Round(quantidade * pu, 2);

                        if (titulo.ParametrosAvancados.Equals("S"))
                        {
                            PosicaoAtualizada posicaoAtualizada = calculoRendaFixa.CalculaPosicao_Fincs(data, papelRendaFixa, titulo, pu, null, null, posicaoRendaFixa.IdOperacao.Value, (int)TipoPreco.MTM, (int)this.tipoProcessamento, true, ref hsMemoria);
                            taxa = posicaoAtualizada.Taxa;
                        }

                        posicaoRendaFixa.PUMercado = pu;
                        posicaoRendaFixa.ValorMercado = valorMercado;
                        posicaoRendaFixa.TaxaMTM = taxa;
                    }
                    else
                    {
                        if (ParametrosConfiguracaoSistema.RendaFixaSwap.EmiteMsgSemCotacao)
                        {
                            throw new CotacaoMTMInexistenteException("Cotação Série não existente para o título " + titulo.Descricao + " - vcto " + titulo.DataVencimento.Value.ToShortDateString() + " na data " + data.ToShortDateString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Encontrou série
                if (pu > 0)
                {
                    if (!ParametrosConfiguracaoSistema.Outras.AbortaProcRFPorErroCadastraTitulo.Equals("S"))
                    {
                        posicaoRendaFixa.PUCurva = posicaoRendaFixa.PUMercado = pu;
                        posicaoRendaFixa.ValorCurva = posicaoRendaFixa.ValorMercado = Math.Round(quantidade * pu, 2);
                        posicaoRendaFixa.TaxaMTM = posicaoRendaFixa.AjusteMTM = null;

                        return;
                    }
                }

                throw ex;
            }
        }

        /// <summary>
        /// Método private para tratar MTM dos títulos pela série de renda fixa (Usando Taxa ao invés de preço).
        /// </summary>
        /// <param name="data"></param>
        /// <param name="posicaoRendaFixa"></param>
        /// <param name="titulo"></param>
        private void MarcaMTMSerieTaxa(DateTime data, PosicaoRendaFixa posicaoRendaFixa, TituloRendaFixa titulo, int? idSerie, ref Hashtable hsMemoria)
        {
            if (titulo.ParametrosAvancados.Equals("S"))
                return;

            //Atenção: este método é auxiliar para o escopo superior, ptt o save é dado na collection no caller!
            decimal quantidade = posicaoRendaFixa.Quantidade.Value;
            CalculoRendaFixa calculoRendaFixa = new CalculoRendaFixa();
            PapelRendaFixa papel = titulo.UpToPapelRendaFixaByIdPapel;

            if (idSerie.HasValue)
            {
                TaxaSerie taxaSerie = new TaxaSerie();
                if (taxaSerie.LoadByPrimaryKey(data, idSerie.Value))
                {
                    List<double> lstTaxa = new List<double>();
                    lstTaxa.Add((double)taxaSerie.Taxa.Value);
                    PosicaoAtualizada posicaoAtualizada = calculoRendaFixa.CalculaPosicao_Fincs(data, papel, titulo, null, lstTaxa, null, posicaoRendaFixa.IdOperacao.Value, (int)TipoPreco.MTM, (int)this.tipoProcessamento, true, ref hsMemoria);
                    posicaoRendaFixa.PUMercado = posicaoAtualizada.PuAtualizado;
                    posicaoRendaFixa.ValorMercado = Math.Round(quantidade * posicaoAtualizada.PuAtualizado, 2);
                    posicaoRendaFixa.TaxaMTM = posicaoAtualizada.Taxa;
                }
                else
                {
                    if (ParametrosConfiguracaoSistema.RendaFixaSwap.EmiteMsgSemCotacao)
                    {
                        throw new CotacaoMTMInexistenteException("Taxa Série não existente para o título " + titulo.Descricao + " - vcto " + titulo.DataVencimento.Value.ToShortDateString() + " na data " + data.ToShortDateString());
                    }
                }
            }
        }

        /// <summary>
        /// Método private para tratar MTM dos títulos pela série de renda fixa (Usando % do pu face ao invés de preço).
        /// </summary>
        /// <param name="data"></param>
        /// <param name="posicaoRendaFixa"></param>
        /// <param name="titulo"></param>
        private void MarcaMTMSerieTaxaPuFace(DateTime data, PosicaoRendaFixa posicaoRendaFixa, TituloRendaFixa titulo, int? idSerie, ref Hashtable hsMemoria)
        {
            if (titulo.ParametrosAvancados.Equals("S"))
                return;

            //Atenção: este método é auxiliar para o escopo superior, ptt o save é dado na collection no caller!
            decimal quantidade = posicaoRendaFixa.Quantidade.Value;
            CalculoRendaFixa calculoRendaFixa = new CalculoRendaFixa();
            PapelRendaFixa papel = titulo.UpToPapelRendaFixaByIdPapel;

            if (idSerie.HasValue)
            {
                TaxaSerie taxaSerie = new TaxaSerie();
                if (taxaSerie.LoadByPrimaryKey(data, idSerie.Value))
                {
                    decimal puLimpo = titulo.ValorNominal.Value * (taxaSerie.Taxa.Value / 100);
                    PosicaoAtualizada posicaoAtualizada = calculoRendaFixa.CalculaPosicao_Fincs(data, papel, titulo, null, null, null, puLimpo, posicaoRendaFixa.IdOperacao.Value, (int)TipoPreco.MTM, (int)this.tipoProcessamento, true, ref hsMemoria);
                    posicaoRendaFixa.PUMercado = posicaoAtualizada.PuAtualizado;
                    posicaoRendaFixa.ValorMercado = Math.Round(quantidade * posicaoAtualizada.PuAtualizado, 2);
                    posicaoRendaFixa.TaxaMTM = posicaoAtualizada.Taxa;
                }
                else
                {
                    if (ParametrosConfiguracaoSistema.RendaFixaSwap.EmiteMsgSemCotacao)
                    {
                        throw new CotacaoMTMInexistenteException("Taxa Série - % do PU Face não existente para o título " + titulo.Descricao + " - vcto " + titulo.DataVencimento.Value.ToShortDateString() + " na data " + data.ToShortDateString());
                    }
                }
            }
        }

        /// <summary>
        /// Método private para tratar MTM dos títulos pela série de renda fixa (Usando Taxa ao invés de preço).
        /// </summary>
        /// <param name="data"></param>
        /// <param name="posicaoRendaFixa"></param>
        /// <param name="titulo"></param>
        private void MarcaMTMVencimento(DateTime data, PosicaoRendaFixa posicaoRendaFixa, TituloRendaFixa titulo, ref Hashtable hsMemoria)
        {
            if (titulo.ParametrosAvancados.Equals("S"))
                return;

            //Atenção: este método é auxiliar para o escopo superior, ptt o save é dado na collection no caller!
            decimal quantidade = posicaoRendaFixa.Quantidade.Value;
            CalculoRendaFixa calculoRendaFixa = new CalculoRendaFixa();
            PapelRendaFixa papel = titulo.UpToPapelRendaFixaByIdPapel;

            //Se a operação já nasceu com a categoria vencimento
            if ((!posicaoRendaFixa.TaxaMTM.HasValue || (posicaoRendaFixa.TaxaMTM.HasValue && posicaoRendaFixa.TaxaMTM == 0)) && posicaoRendaFixa.DataOperacao.Value == data)
                posicaoRendaFixa.TaxaMTM = posicaoRendaFixa.TaxaOperacao;

            if (posicaoRendaFixa.TaxaMTM.HasValue)
            {
                List<double> lstTaxa = new List<double>();
                lstTaxa.Add((double)posicaoRendaFixa.TaxaMTM.Value);

                PosicaoAtualizada posicaoAtualizada = calculoRendaFixa.CalculaPosicao_Fincs(data, papel, titulo, null, lstTaxa, null, posicaoRendaFixa.IdOperacao.Value, (int)TipoPreco.CurvaVencimento, (int)this.tipoProcessamento, true, ref hsMemoria);
                posicaoRendaFixa.PUCurvaVencimento = posicaoAtualizada.PuAtualizado;
                posicaoRendaFixa.ValorCurvaVencimento = Math.Round(quantidade * posicaoAtualizada.PuAtualizado, 2);
                posicaoRendaFixa.AjusteVencimento = posicaoRendaFixa.PUCurvaVencimento - posicaoRendaFixa.PUMercado;
            }
        }

        /// <summary>
        /// Método para calcular os valores de MTM seguindo as parametrizações feitas pelo usuário no cadastro de Curva Renda Fixa
        /// </summary>
        /// <param name="data"></param>
        /// <param name="posicaoRendaFixa"></param>
        /// <param name="curvaRendaFixa"></param>
        /// <param name="titulo"></param>
        /// <param name="IdCurva"></param>
        /// <param name="hsMemoria">HashTable para diminuir o acesso ao BD repetitivamente, a Key deve ser setada no Enum do Projeto Util</param>
        private void MarcaMTMCurva(DateTime data, PosicaoRendaFixa posicaoRendaFixa, TituloRendaFixa titulo, int? IdCurva, ref Hashtable hsMemoria)
        {
            if (titulo.ParametrosAvancados.Equals("S"))
                return;

            CurvaRendaFixa curvaRendaFixa = new CurvaRendaFixa();
            PapelRendaFixa papel = titulo.UpToPapelRendaFixaByIdPapel;

            if (!curvaRendaFixa.LoadByPrimaryKey(IdCurva.Value))
                return;

            #region Controle de Memoria Inicio
            Hashtable hsTaxasCurvas;
            int keyTaxasCurvas = (int)Financial.Util.Enums.KeyHashTable.TaxaCurva;
            if (hsMemoria.Contains(keyTaxasCurvas))
                hsTaxasCurvas = (Hashtable)hsMemoria[keyTaxasCurvas];
            else
                hsTaxasCurvas = new Hashtable();

            Hashtable hsTaxasCurvasCompostas;
            int keyCurvasCompostas = (int)Financial.Util.Enums.KeyHashTable.CurvasCompostas;
            if (hsMemoria.Contains(keyCurvasCompostas))
                hsTaxasCurvasCompostas = (Hashtable)hsMemoria[keyCurvasCompostas];
            else
                hsTaxasCurvasCompostas = new Hashtable();
            #endregion

            CalculoRendaFixa calculoRendaFixa = new CalculoRendaFixa();
            AgendaRendaFixaCollection agendaRendaFixaCollection = new AgendaRendaFixaCollection();

            List<DateTime> lstDataFluxos = agendaRendaFixaCollection.BuscaDataFluxo(titulo.IdTitulo.Value, data);

            List<double> lstTaxas = new List<double>();
            if (lstDataFluxos != null && lstDataFluxos.Count > 0)
            {
                foreach (DateTime dataFluxo in lstDataFluxos)
                {
                    lstTaxas.Add(Convert.ToDouble(calculoRendaFixa.CalculaTaxaCurvaMTM(data, dataFluxo, titulo.DataVencimento.Value, curvaRendaFixa, ref hsTaxasCurvas, ref hsTaxasCurvasCompostas)));
                }
            }
            else //Bullet
            {
                lstTaxas.Add(Convert.ToDouble(calculoRendaFixa.CalculaTaxaCurvaMTM(data, titulo.DataVencimento.Value, titulo.DataVencimento.Value, curvaRendaFixa, ref hsTaxasCurvas, ref hsTaxasCurvasCompostas)));
            }

            PosicaoRendaFixa.PosicaoAtualizada posicaoAtualizada = calculoRendaFixa.CalculaPosicao_Fincs(data, papel, titulo, null, lstTaxas, null, posicaoRendaFixa.IdOperacao.Value, (int)TipoPreco.MTM, (int)this.tipoProcessamento, true, ref hsMemoria);

            posicaoRendaFixa.PUMercado = posicaoAtualizada.PuAtualizado;
            posicaoRendaFixa.ValorMercado = Math.Round(((decimal)posicaoRendaFixa.PUMercado * (decimal)posicaoRendaFixa.Quantidade), 2);
            posicaoRendaFixa.TaxaMTM = posicaoAtualizada.Taxa;

            if (!hsMemoria.Contains(keyTaxasCurvas))
                hsMemoria.Add(keyTaxasCurvas, hsTaxasCurvas);

            if (!hsMemoria.Contains(keyCurvasCompostas))
                hsMemoria.Add(keyCurvasCompostas, hsTaxasCurvasCompostas);
        }

        private void MarcaMTMManual(DateTime data, MTMManual mtmManual, PosicaoRendaFixa posicaoRendaFixa, ref Hashtable hsMemoria)
        {           
            //Atenção: este método é auxiliar para o escopo superior, ptt o save é dado na collection no caller! 
            PosicaoAtualizada posicaoAtualizada = new PosicaoAtualizada(); ;
            CalculoRendaFixa calculoRendaFixa = new CalculoRendaFixa();
            TituloRendaFixa titulo = posicaoRendaFixa.UpToTituloRendaFixaByIdTitulo;
            PapelRendaFixa papel = titulo.UpToPapelRendaFixaByIdPapel;
            decimal quantidade = posicaoRendaFixa.Quantidade.Value;
            decimal? puMTM = mtmManual.PuMTM;
            decimal? taxa252 = mtmManual.Taxa252;
            bool parametrosAvancados = titulo.ParametrosAvancados.Equals("S");

            try
            {
                if (puMTM.HasValue && puMTM.Value > 0)
                {
                    if (parametrosAvancados)
                    {
                        posicaoAtualizada = calculoRendaFixa.CalculaPosicao_Fincs(data, papel, titulo, puMTM, null, null, posicaoRendaFixa.IdOperacao.Value, (int)TipoPreco.MTM, (int)this.tipoProcessamento, true, ref hsMemoria);
                    }
                    else
                    {
                        posicaoAtualizada.PuAtualizado = puMTM.Value;
                        posicaoAtualizada.Taxa = 0;
                    }
                }
                else if (taxa252.HasValue && taxa252.Value > 0)
                {
                    if (parametrosAvancados)
                        return;

                    List<double> lstTaxas = new List<double>();
                    lstTaxas.Add((double)taxa252.Value);
                    posicaoAtualizada = calculoRendaFixa.CalculaPosicao_Fincs(data, papel, titulo, null, lstTaxas, null, posicaoRendaFixa.IdOperacao.Value, (int)TipoPreco.MTM, (int)this.tipoProcessamento, true, ref hsMemoria);
                }
            }
            catch (Exception ex)
            {
                if (puMTM.HasValue && puMTM.Value > 0)                
                {
                    if (!ParametrosConfiguracaoSistema.Outras.AbortaProcRFPorErroCadastraTitulo.Equals("S"))
                    {
                        posicaoRendaFixa.PUCurva = posicaoRendaFixa.PUMercado = puMTM.Value;
                        posicaoRendaFixa.ValorCurva = posicaoRendaFixa.ValorMercado = Math.Round(quantidade * puMTM.Value, 2);
                        posicaoRendaFixa.TaxaMTM = posicaoRendaFixa.AjusteMTM = null;

                        return;
                    }
                }

                throw ex;

            }

            posicaoRendaFixa.PUMercado = posicaoAtualizada.PuAtualizado;
            posicaoRendaFixa.ValorMercado = Math.Round(quantidade * posicaoAtualizada.PuAtualizado, 2);
            posicaoRendaFixa.TaxaMTM = posicaoAtualizada.Taxa;
            posicaoRendaFixa.AjusteMTM = posicaoRendaFixa.PUMercado.Value - posicaoRendaFixa.PUCurva.Value;
        }

        /// <summary>
        /// Método private para tratar MTM dos títulos publicos pelo mercado secundário da Andima.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="posicaoRendaFixa"></param>
        /// <param name="titulo"></param>
        private void MarcaMTMAnbima(DateTime data, PosicaoRendaFixa posicaoRendaFixa, TituloRendaFixa titulo, ref Hashtable hsMemoria)
        {
            //Atenção: este método é auxiliar para o escopo superior, ptt o save é dado na collection no caller!

            decimal quantidade = posicaoRendaFixa.Quantidade.Value;
            DateTime dataEmissao = titulo.DataEmissao.Value;
            DateTime dataVencimento = titulo.DataVencimento.Value;
            PapelRendaFixa papel = titulo.UpToPapelRendaFixaByIdPapel;
            CalculoRendaFixa calculoRendaFixa = new CalculoRendaFixa();
            PosicaoAtualizada posicaoAtualizada = new PosicaoAtualizada();
            if (!String.IsNullOrEmpty(titulo.CodigoCustodia))
            {
                string codigoCustodia = titulo.CodigoCustodia;

                if (codigoCustodia.Contains("7601")) //Garante todas as classes de NTN-B
                {
                    codigoCustodia = "760199";
                }

                if (codigoCustodia.Contains("9501")) //Garante todas as classes de NTN-F
                {
                    codigoCustodia = "950199";
                }

                CotacaoMercadoAndima cotacao = new CotacaoMercadoAndima();
                if (cotacao.BuscaPU(data, codigoCustodia, dataEmissao, dataVencimento))
                {
                    decimal pu = cotacao.Pu.Value;
                    decimal taxa = 0;
                    decimal valorMercado = Math.Round(quantidade * pu, 2);

                    if (titulo.ParametrosAvancados.Equals("S"))
                    {
                        posicaoAtualizada = calculoRendaFixa.CalculaPosicao_Fincs(data, papel, titulo, cotacao.Pu, null, null, posicaoRendaFixa.IdOperacao.Value, (int)TipoPreco.MTM, (int)this.tipoProcessamento, true, ref hsMemoria);
                        taxa = posicaoAtualizada.Taxa;
                    }

                    posicaoRendaFixa.PUMercado = pu;
                    posicaoRendaFixa.ValorMercado = valorMercado;
                    posicaoRendaFixa.TaxaMTM = taxa;
                }
                else
                {
                    if (ParametrosConfiguracaoSistema.RendaFixaSwap.EmiteMsgSemCotacao)
                    {
                        throw new CotacaoMTMInexistenteException("Cotação Andima não existente para o título " + titulo.Descricao + " - vcto " + titulo.DataVencimento.Value.ToShortDateString() + " na data " + data.ToShortDateString());
                    }
                }
            }
        }

        /// <summary>
        /// Método private para tratar MTM dos títulos pelo mercado de debenture do site Debentures da Anbima.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="posicaoRendaFixa"></param>
        /// <param name="titulo"></param>
        private void MarcaMTM_MercadoDebenture(DateTime data, PosicaoRendaFixa posicaoRendaFixa, TituloRendaFixa titulo)
        {
            //Atenção: este método é auxiliar para o escopo superior, ptt o save é dado na collection no caller!

            decimal quantidade = posicaoRendaFixa.Quantidade.Value;
            DateTime dataVencimento = titulo.DataVencimento.Value;

            if (!String.IsNullOrEmpty(titulo.CodigoCustodia))
            {
                string codigoCustodia = titulo.CodigoCustodia;

                CotacaoMercadoDebenture cotacaoMercadoDebenture = new CotacaoMercadoDebenture();
                if (cotacaoMercadoDebenture.LoadByPrimaryKey(data, codigoCustodia, dataVencimento))
                {
                    decimal pu = cotacaoMercadoDebenture.Pu.Value;
                    decimal valorMercado = Math.Round(quantidade * pu, 2);
                    posicaoRendaFixa.PUMercado = pu;
                    posicaoRendaFixa.ValorMercado = valorMercado;
                }
                else
                {
                    if (ParametrosConfiguracaoSistema.RendaFixaSwap.EmiteMsgSemCotacao)
                    {
                        throw new CotacaoMTMInexistenteException("Cotação Anbima Debenture (valor mercado) não existente para o título " + titulo.Descricao + " - vcto " + titulo.DataVencimento.Value.ToShortDateString() + " na data " + data.ToShortDateString());
                    }
                }
            }
        }

        /// <summary>
        /// Método private para tratar MTM dos títulos pelo par de debenture do site Debentures da Anbima.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="posicaoRendaFixa"></param>
        /// <param name="titulo"></param>
        private void MarcaMTM_ParDebenture(DateTime data, PosicaoRendaFixa posicaoRendaFixa, TituloRendaFixa titulo)
        {
            //Atenção: este método é auxiliar para o escopo superior, ptt o save é dado na collection no caller!

            decimal quantidade = posicaoRendaFixa.Quantidade.Value;

            if (!String.IsNullOrEmpty(titulo.CodigoCustodia))
            {
                string codigoCustodia = titulo.CodigoCustodia;

                CotacaoDebenture cotacaoDebenture = new CotacaoDebenture();
                if (cotacaoDebenture.LoadByPrimaryKey(data, codigoCustodia))
                {
                    decimal pu = cotacaoDebenture.Pu.Value;
                    decimal valorMercado = Math.Round(quantidade * pu, 2);
                    posicaoRendaFixa.PUMercado = pu;
                    posicaoRendaFixa.ValorMercado = valorMercado;
                }
                else
                {
                    if (ParametrosConfiguracaoSistema.RendaFixaSwap.EmiteMsgSemCotacao)
                    {
                        throw new CotacaoMTMInexistenteException("Cotação Anbima Debenture (Valor par) não existente para o título " + titulo.Descricao + " na data " + data.ToShortDateString());
                    }
                }
            }
        }

        /// <summary>
        /// Método private para tratar MTM dos títulos pelo PU divulgado pelo Tesouro Direto.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="posicaoRendaFixa"></param>
        /// <param name="titulo"></param>
        private void MarcaMTMTesouro(DateTime data, PosicaoRendaFixa posicaoRendaFixa, TituloRendaFixa titulo)
        {
            //Atenção: este método é auxiliar para o escopo superior, ptt o save é dado na collection no caller!

            decimal quantidade = posicaoRendaFixa.Quantidade.Value;
            string descricao = titulo.Descricao.Trim().ToUpper().Replace("-", "").Replace("PRINCIPAL", "P").Replace("PRINC", "P").Trim();
            DateTime dataVencimento = titulo.DataVencimento.Value;

            CotacaoMercadoTesouroCollection cotacaoMercadoTesouroCollection = new CotacaoMercadoTesouroCollection();
            cotacaoMercadoTesouroCollection.Query.Where(cotacaoMercadoTesouroCollection.Query.DataReferencia.Equal(data),
                                                        cotacaoMercadoTesouroCollection.Query.DataVencimento.Equal(dataVencimento));
            cotacaoMercadoTesouroCollection.Query.Load();

            bool achou = false;
            foreach (CotacaoMercadoTesouro cotacao in cotacaoMercadoTesouroCollection)
            {
                if (cotacao.Descricao.Trim().ToUpper().Replace("-", "").Replace("PRINCIPAL", "P").Replace("PRINC", "P").Trim() == descricao)
                {
                    decimal pu = cotacao.Pu.Value;
                    decimal valorMercado = Math.Round(quantidade * pu, 2);
                    posicaoRendaFixa.PUMercado = pu;
                    posicaoRendaFixa.ValorMercado = valorMercado;

                    achou = true;

                    break;
                }
            }

            if (!achou)
            {
                if (ParametrosConfiguracaoSistema.RendaFixaSwap.EmiteMsgSemCotacao)
                {
                    throw new CotacaoMTMInexistenteException("Cotação Tesouro não existente para o título " + descricao + " - vcto " + dataVencimento.ToShortDateString() + " na data " + data.ToShortDateString());
                }
            }
        }

        /// <summary>
        /// Método private para tratar MTM dos títulos pela cotação da Bolsa.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="posicaoRendaFixa"></param>
        /// <param name="titulo"></param>
        private void MarcaMTMCotacaoBovespa(DateTime data, PosicaoRendaFixa posicaoRendaFixa, TituloRendaFixa titulo)
        {
            //Atenção: este método é auxiliar para o escopo superior, ptt o save é dado na collection no caller!

            decimal quantidade = posicaoRendaFixa.Quantidade.Value;

            if (!String.IsNullOrEmpty(titulo.CodigoCBLC))
            {
                string cdAtivoBolsa = titulo.CodigoCBLC;

                CotacaoBolsa cotacaoBolsa = new CotacaoBolsa();
                if (cotacaoBolsa.LoadByPrimaryKey(data, cdAtivoBolsa))
                {
                    decimal pu = cotacaoBolsa.PUFechamento.Value;
                    decimal valorMercado = Math.Round(quantidade * pu, 2);
                    posicaoRendaFixa.PUMercado = pu;
                    posicaoRendaFixa.ValorMercado = valorMercado;
                }
            }
        }

        /// <summary>
        /// Método private para tratar MTM dos títulos pela cota Ambima.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="posicaoRendaFixa"></param>
        /// <param name="titulo"></param>
        private void MarcaMTMCotaAnbima(DateTime data, PosicaoRendaFixa posicaoRendaFixa, TituloRendaFixa titulo)
        {
            //Atenção: este método é auxiliar para o escopo superior, ptt o save é dado na collection no caller!

            decimal quantidade = posicaoRendaFixa.Quantidade.Value;

            if (!String.IsNullOrEmpty(titulo.CodigoCustodia))
            {
                if (Utilitario.IsInteger(titulo.CodigoCustodia))
                {
                    int codigoAmbima = Convert.ToInt32(titulo.CodigoCustodia);
                    HistoricoCotaCollection historicoCotaCollection = new HistoricoCotaCollection();
                    historicoCotaCollection.Query.Select(historicoCotaCollection.Query.CotaFechamento);
                    historicoCotaCollection.Query.Where(historicoCotaCollection.Query.Data.Equal(data),
                                                        historicoCotaCollection.Query.IdCarteira.Equal(codigoAmbima));
                    historicoCotaCollection.Query.Load();

                    if (historicoCotaCollection.Count > 0 && historicoCotaCollection[0].CotaFechamento.HasValue)
                    {
                        decimal pu = historicoCotaCollection[0].CotaFechamento.Value;
                        decimal valorMercado = Math.Round(quantidade * pu, 2);
                        posicaoRendaFixa.PUMercado = pu;
                        posicaoRendaFixa.ValorMercado = valorMercado;
                    }
                }
            }
        }

        /// <summary>
        /// Calcula IR e IOF das posições.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void CalculaTributacao(int idCliente, DateTime data)
        {
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);

            ClienteRendaFixa clienteRendaFixa = new ClienteRendaFixa();
            if (clienteRendaFixa.LoadByPrimaryKey(idCliente))
            {
                if (clienteRendaFixa.IsIsentoIR() && clienteRendaFixa.IsIsentoIOF())
                {
                    return;
                }
            }
            else
            {
                return;
            }

            PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
            posicaoRendaFixaCollection.BuscaPosicao(idCliente);

            ExcecoesTributacaoIRCollection excecoesColl = new ExcecoesTributacaoIRCollection();
            if (posicaoRendaFixaCollection.Count > 0)
            {
                excecoesColl.RetornaExcessaoTributacaoIR(cliente.UpToPessoaByIdPessoa.Tipo, data, (int)TipoMercado.RendaFixa);
            }

            for (int i = 0; i < posicaoRendaFixaCollection.Count; i++)
            {
                PosicaoRendaFixa posicaoRendaFixa = posicaoRendaFixaCollection[i];

                TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
                tituloRendaFixa.LoadByPrimaryKey(posicaoRendaFixa.IdTitulo.Value);

                TituloIsentoIR tituloIsentoIR;
                if (tituloRendaFixa.IsentoIR.GetValueOrDefault(0) > 0)
                    tituloIsentoIR = (TituloIsentoIR)tituloRendaFixa.IsentoIR.Value;
                else
                {
                    PapelRendaFixa papel = tituloRendaFixa.UpToPapelRendaFixaByIdPapel;
                    tituloIsentoIR = (TituloIsentoIR)papel.IsentoIR.Value;
                }

                ExcecoesTributacaoIR excecao = excecoesColl.ObtemPrioridade(cliente.UpToPessoaByIdPessoa.Tipo, (int)TipoMercado.RendaFixa, tituloRendaFixa.IdPapel.Value, tituloRendaFixa.IdTitulo.Value);

                CalculoRendaFixa calculoRendaFixa = new CalculoRendaFixa();
                decimal aliquotaIOF = 0, aliquotaIR = 0;
                decimal iof = calculoRendaFixa.RetornaIOFPosicao(cliente, clienteRendaFixa, posicaoRendaFixa, data, out aliquotaIOF);
                decimal ir = calculoRendaFixa.RetornaIRPosicao(cliente, clienteRendaFixa, posicaoRendaFixa, tituloRendaFixa, data, iof, false, excecao, out aliquotaIR);

                posicaoRendaFixa.ValorIOF = iof;
                posicaoRendaFixa.ValorIR = ir;
                posicaoRendaFixa.AliquotaIOF = aliquotaIOF * 100;
                posicaoRendaFixa.AliquotaIR = aliquotaIR * 100;
                posicaoRendaFixa.ValorBrutoGrossUp = 0;

                bool grossup = cliente.GrossUP.Value != (byte)GrossupCliente.NaoFaz && tituloIsentoIR != TituloIsentoIR.NaoIsento;

                bool isentoCliente = clienteRendaFixa.IsIsentoIR() ||
                                        (tituloRendaFixa.IsentoIR.Value == (byte)TituloIsentoIR.IsentoPF &&
                                         cliente.IdTipo.Value == (byte)TipoClienteFixo.ClientePessoaFisica &&
                                         cliente.GrossUP.Value != (byte)GrossupCliente.SempreFaz);

                if (grossup && !isentoCliente)
                {
                    decimal valorLiquido = posicaoRendaFixa.ValorMercado.Value;
                    decimal valorBruto = valorLiquido + ir;

                    posicaoRendaFixa.ValorMercado = valorBruto;
                    posicaoRendaFixa.PUMercado = Utilitario.Truncate(posicaoRendaFixa.ValorMercado.Value / posicaoRendaFixa.Quantidade.Value, 8);
                    posicaoRendaFixa.ValorBrutoGrossUp = valorBruto;
                }
            }

            posicaoRendaFixaCollection.Save();
        }

        /// <summary>
        /// Carrega o objeto PosicaoRendaFixa com os campos IdPosicao, Quantidade, QuantidadeBloqueada.
        /// </summary>
        /// <param name="idPosicao"></param>
        /// <returns>bool indicando se trouxe registro.</returns>
        public bool BuscaQuantidades(int idPosicao)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdPosicao, this.Query.Quantidade, this.Query.QuantidadeBloqueada)
                 .Where(this.Query.IdPosicao == idPosicao);

            bool retorno = this.Query.Load();

            return retorno;
        }

        /// <summary>
        /// Atualiza as posições finais (curva e mercado), somente posições resgatadas 
        /// (e q portanto sofreram mudança na qtde).
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void AtualizaPosicoesResgatadas(int idCliente, DateTime data)
        {
            PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
            posicaoRendaFixaCollection.BuscaPosicaoResgatada(idCliente);

            for (int i = 0; i < posicaoRendaFixaCollection.Count; i++)
            {
                PosicaoRendaFixa posicaoRendaFixa = posicaoRendaFixaCollection[i];

                decimal quantidade = posicaoRendaFixa.Quantidade.Value;
                decimal puMercado = posicaoRendaFixa.PUMercado.Value;
                decimal puCurva = posicaoRendaFixa.PUCurva.Value;

                decimal valorMercado = Math.Round(quantidade * puMercado, 2);
                decimal valorCurva = Math.Round(quantidade * puCurva, 2);

                posicaoRendaFixa.ValorCurva = valorCurva;
                posicaoRendaFixa.ValorMercado = valorMercado;
            }

            posicaoRendaFixaCollection.Save();
        }

        /// <summary>
        /// Retorna a quantidade em posição do ativo do cliente.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns></returns>
        public decimal RetornaQuantidadeAtivo(int idCliente, int idTitulo)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.Quantidade.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.IdTitulo.Equal(idTitulo));

            this.Query.Load();

            return this.Quantidade.HasValue ? this.Quantidade.Value : 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idcliente"></param>
        /// <param name="idTitulo"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public decimal RetornaPuAjustadoPosicao(int idCliente, int idPosicao, DateTime data, DateTime dataHistorico, bool historico)
        {
            decimal pu = 0;

            if (historico)
            {
                PosicaoRendaFixaHistoricoCollection p = new PosicaoRendaFixaHistoricoCollection();
                p.Query.Select(p.Query.PUMercado, p.Query.DataOperacao)
                       .Where(p.Query.DataHistorico == data &&
                              p.Query.IdPosicao == idPosicao,
                              p.Query.IdCliente == idCliente);

                if (p.Query.Load())
                {
                    if (p.Count > 0)
                    {
                        pu = p[0].PUMercado.Value;
                    }
                }
            }
            else
            {
                PosicaoRendaFixaCollection p = new PosicaoRendaFixaCollection();
                p.Query.Select(p.Query.PUMercado, p.Query.DataOperacao)
                       .Where(p.Query.IdPosicao == idPosicao,
                              p.Query.IdCliente == idCliente);

                if (p.Query.Load())
                {
                    if (p.Count > 0)
                    {
                        pu = p[0].PUMercado.Value;
                    }
                }
            }

            if (pu != 0)
            {
                LiquidacaoRendaFixaCollection liquidacaoRendaFixaCollection = new LiquidacaoRendaFixaCollection();
                liquidacaoRendaFixaCollection.Query.Select(liquidacaoRendaFixaCollection.Query.ValorBruto,
                                                           liquidacaoRendaFixaCollection.Query.Quantidade)
                            .Where(liquidacaoRendaFixaCollection.Query.DataLiquidacao <= data &
                                   liquidacaoRendaFixaCollection.Query.DataLiquidacao > dataHistorico &
                                   liquidacaoRendaFixaCollection.Query.IdPosicaoResgatada == idPosicao &
                                   liquidacaoRendaFixaCollection.Query.IdCliente == idCliente &
                                   liquidacaoRendaFixaCollection.Query.TipoLancamento.In((byte)TipoLancamentoLiquidacao.Juros,
                                                                                         (byte)TipoLancamentoLiquidacao.Amortizacao));
                liquidacaoRendaFixaCollection.Query.Load();

                foreach (LiquidacaoRendaFixa liquidacaoRendaFixa in liquidacaoRendaFixaCollection)
                {
                    decimal puEvento = liquidacaoRendaFixa.ValorBruto.Value / liquidacaoRendaFixa.Quantidade.Value;
                    pu += puEvento;
                }
            }

            return pu;
        }

        /// <summary>
        /// Retorna vencimentos projetados de título e de pagamento de amortização e cupom de juros.
        /// </summary>
        /// <returns></returns>
        public List<VencimentoProjetado> RetornaVencimentosProjetados(DateTime dataInicio, DateTime? dataFim)
        {
            return RetornaVencimentosProjetados(null, dataInicio, dataFim);
        }

        /// <summary>
        /// Retorna vencimentos projetados de título e de pagamento de amortização e cupom de juros.
        /// </summary>
        /// <returns></returns>
        public List<VencimentoProjetado> RetornaVencimentosProjetados(int? idCliente, DateTime dataInicio, DateTime? dataFim)
        {
            List<VencimentoProjetado> listaVencimento = new List<VencimentoProjetado>();

            #region Vencimentos de Títulos pós-fixados
            PosicaoRendaFixaQuery posicaoRendaFixaQuery = new PosicaoRendaFixaQuery("P");
            TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            posicaoRendaFixaQuery.Select(tituloRendaFixaQuery.Descricao,
                                         posicaoRendaFixaQuery.DataVencimento,
                                         posicaoRendaFixaQuery.ValorMercado.Sum());
            posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
            posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.Quantidade.NotEqual(0),
                                        posicaoRendaFixaQuery.DataVencimento.GreaterThanOrEqual(dataInicio),
                                        tituloRendaFixaQuery.IdIndice.IsNotNull());
            if (idCliente.HasValue)
            {
                posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente.Equal(idCliente.Value));
            }

            if (dataFim.GetValueOrDefault(new DateTime()) != new DateTime())
            {
                posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.DataVencimento.LessThanOrEqual(dataFim.Value));
            }

            posicaoRendaFixaQuery.GroupBy(tituloRendaFixaQuery.Descricao,
                                          posicaoRendaFixaQuery.DataVencimento);

            PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
            posicaoRendaFixaCollection.Load(posicaoRendaFixaQuery);

            foreach (PosicaoRendaFixa posicaoRendaFixa in posicaoRendaFixaCollection)
            {
                VencimentoProjetado fluxo = new VencimentoProjetado();

                if (idCliente.HasValue)
                {
                    fluxo.idCliente = idCliente.Value;
                    fluxo.valor = posicaoRendaFixa.ValorMercado.Value;
                }

                fluxo.dataLancamento = dataInicio;
                fluxo.dataVencimento = posicaoRendaFixa.DataVencimento.Value;
                fluxo.descricao = "Vencimento " + Convert.ToString(posicaoRendaFixa.GetColumn(TituloRendaFixaMetadata.ColumnNames.Descricao));

                listaVencimento.Add(fluxo);
            }
            #endregion

            #region Vencimentos de Títulos pré-fixados
            posicaoRendaFixaQuery = new PosicaoRendaFixaQuery("P");
            tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            posicaoRendaFixaQuery.Select(tituloRendaFixaQuery.Descricao,
                                         posicaoRendaFixaQuery.DataVencimento,
                                         posicaoRendaFixaQuery.Quantidade.Sum(),
                                         tituloRendaFixaQuery.PUNominal.Avg());
            posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
            posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.Quantidade.NotEqual(0),
                                        posicaoRendaFixaQuery.DataVencimento.GreaterThanOrEqual(dataInicio),
                                        tituloRendaFixaQuery.IdIndice.IsNull());
            if (idCliente.HasValue)
            {
                posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente.Equal(idCliente.Value));
            }

            if (dataFim.GetValueOrDefault(new DateTime()) != new DateTime())
            {
                posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.DataVencimento.LessThanOrEqual(dataFim.Value));
            }

            posicaoRendaFixaQuery.GroupBy(tituloRendaFixaQuery.Descricao,
                                         posicaoRendaFixaQuery.DataVencimento);

            posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
            posicaoRendaFixaCollection.Load(posicaoRendaFixaQuery);

            foreach (PosicaoRendaFixa posicaoRendaFixa in posicaoRendaFixaCollection)
            {
                VencimentoProjetado fluxo = new VencimentoProjetado();

                if (idCliente.HasValue)
                {
                    fluxo.idCliente = idCliente.Value;

                    decimal puNominal = Convert.ToDecimal(posicaoRendaFixa.GetColumn(TituloRendaFixaMetadata.ColumnNames.PUNominal));
                    fluxo.valor = Math.Round(posicaoRendaFixa.Quantidade.Value * puNominal, 2);
                }

                fluxo.dataLancamento = dataInicio;
                fluxo.dataVencimento = posicaoRendaFixa.DataVencimento.Value;
                fluxo.descricao = "Vencimento " + Convert.ToString(posicaoRendaFixa.GetColumn(TituloRendaFixaMetadata.ColumnNames.Descricao));

                listaVencimento.Add(fluxo);
            }
            #endregion

            #region Pagamentos de eventos (cupons, amortização)
            AgendaRendaFixaQuery agendaRendaFixaQuery = new AgendaRendaFixaQuery("A");
            tituloRendaFixaQuery = new TituloRendaFixaQuery("T");
            posicaoRendaFixaQuery = new PosicaoRendaFixaQuery("P");
            agendaRendaFixaQuery.Select(tituloRendaFixaQuery.Descricao,
                                        agendaRendaFixaQuery.DataPagamento,
                                        agendaRendaFixaQuery.TipoEvento,
                                        agendaRendaFixaQuery.Valor,
                                        posicaoRendaFixaQuery.Quantidade.Sum());
            agendaRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == agendaRendaFixaQuery.IdTitulo);
            agendaRendaFixaQuery.InnerJoin(posicaoRendaFixaQuery).On(posicaoRendaFixaQuery.IdTitulo == agendaRendaFixaQuery.IdTitulo);
            agendaRendaFixaQuery.Where(agendaRendaFixaQuery.DataPagamento.GreaterThanOrEqual(dataInicio),
                                       posicaoRendaFixaQuery.Quantidade.NotEqual(0));

            if (idCliente.HasValue)
            {
                agendaRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente.Equal(idCliente.Value));
            }

            if (dataFim.GetValueOrDefault(new DateTime()) != new DateTime())
            {
                agendaRendaFixaQuery.Where(agendaRendaFixaQuery.DataPagamento.LessThanOrEqual(dataFim.Value));
            }

            agendaRendaFixaQuery.GroupBy(tituloRendaFixaQuery.Descricao,
                                        agendaRendaFixaQuery.DataPagamento,
                                        agendaRendaFixaQuery.TipoEvento,
                                        agendaRendaFixaQuery.Valor);

            AgendaRendaFixaCollection agendaRendaFixaCollection = new AgendaRendaFixaCollection();
            agendaRendaFixaCollection.Load(agendaRendaFixaQuery);

            foreach (AgendaRendaFixa agendaRendaFixa in agendaRendaFixaCollection)
            {
                VencimentoProjetado fluxo = new VencimentoProjetado();

                if (idCliente.HasValue)
                {
                    fluxo.idCliente = idCliente.Value;
                    decimal quantidade = Convert.ToDecimal(agendaRendaFixa.GetColumn(PosicaoRendaFixaMetadata.ColumnNames.Quantidade));
                    fluxo.valor = Math.Round(quantidade * agendaRendaFixa.Valor.Value, 2);
                }

                fluxo.dataLancamento = dataInicio;
                fluxo.dataVencimento = agendaRendaFixa.DataPagamento.Value;

                string descricao = "";
                if (agendaRendaFixa.TipoEvento.Value == (byte)TipoEventoTitulo.Amortizacao)
                {
                    descricao = "Amortização";
                }
                else if (agendaRendaFixa.TipoEvento.Value == (byte)TipoEventoTitulo.IncorporacaoJuros ||
                         agendaRendaFixa.TipoEvento.Value == (byte)TipoEventoTitulo.Juros ||
                         agendaRendaFixa.TipoEvento.Value == (byte)TipoEventoTitulo.JurosCorrecao)
                {
                    descricao = "Pagto Juros";
                }
                else if (agendaRendaFixa.TipoEvento.Value == (byte)TipoEventoTitulo.PagamentoCorrecao)
                {
                    descricao = "Pagto Correção";
                }
                else if (agendaRendaFixa.TipoEvento.Value == (byte)TipoEventoTitulo.PagamentoPrincipal)
                {
                    descricao = "Pagto Principal";
                }

                descricao = descricao + " - " + Convert.ToString(agendaRendaFixa.GetColumn(TituloRendaFixaMetadata.ColumnNames.Descricao));

                fluxo.descricao = descricao;

                listaVencimento.Add(fluxo);
            }
            #endregion

            return listaVencimento;
        }

        public decimal RetornaValorNegativo(int idCliente)
        {
            decimal valor = 0;
            PosicaoRendaFixa posicao = new PosicaoRendaFixa();
            posicao.Query.Select(posicao.Query.ValorMercado.Sum());
            posicao.Query.Where(posicao.Query.Quantidade.LessThan(0),
                                posicao.Query.IdCliente.Equal(idCliente));
            posicao.Query.Load();

            if (posicao.ValorMercado.HasValue)
            {
                valor = posicao.ValorMercado.Value;
            }
            return valor;
        }

        /// <summary>
        /// Importa posições de títulos a partir do sistema de Open Virtual.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void IntegraPosicoesVirtual(int idCliente, DateTime data)
        {
            PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();

            PosicaoRendaFixaQuery posicaoRendaFixaQuery = new PosicaoRendaFixaQuery("P");
            posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery.IdPosicao);
            posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente.Equal(idCliente));

            PosicaoRendaFixaCollection posicaoRendaFixaCollectionDeletar = new PosicaoRendaFixaCollection();
            posicaoRendaFixaCollectionDeletar.Load(posicaoRendaFixaQuery);
            posicaoRendaFixaCollectionDeletar.MarkAllAsDeleted();
            posicaoRendaFixaCollectionDeletar.Save();


            ClienteRendaFixa clienteRendaFixa = new ClienteRendaFixa();
            if (!clienteRendaFixa.LoadByPrimaryKey(idCliente))
                return;

            string codigoInterface = clienteRendaFixa.CodigoInterface.Replace(";", ",").Trim();

            if (codigoInterface == "")
                return;

            string[] codigoSplit = codigoInterface.Split(new char[] { ',' });

            List<string> lista = new List<string>();
            lista.Add(codigoSplit[0].PadLeft(9, '0'));


            OphisposfechamentoCollection ophisposfechamentoCollection = new OphisposfechamentoCollection();
            ophisposfechamentoCollection.BuscaPosicoesVirtual(lista[0], data);

            foreach (Ophisposfechamento ophisposfechamento in ophisposfechamentoCollection)
            {
                string codigoTitulo = ophisposfechamento.FcCodtitulo;

                TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
                tituloRendaFixa.TrataTituloVirtual(codigoTitulo);


                PosicaoRendaFixa posicaoRendaFixa = posicaoRendaFixaCollection.AddNew();

                posicaoRendaFixa.CustoCustodia = 0;
                posicaoRendaFixa.DataLiquidacao = ophisposfechamento.FdDatoper.Value;
                posicaoRendaFixa.DataOperacao = ophisposfechamento.FdDatoper.Value;
                posicaoRendaFixa.DataVencimento = tituloRendaFixa.DataVencimento.Value;
                posicaoRendaFixa.IdCliente = idCliente;
                posicaoRendaFixa.IdCustodia = (byte)LocalCustodiaFixo.CBLC; //Aparentemente não tem no Virtual
                posicaoRendaFixa.IdTitulo = tituloRendaFixa.IdTitulo.Value;
                posicaoRendaFixa.PUCurva = (decimal)ophisposfechamento.FnPucurva.Value;
                posicaoRendaFixa.PUMercado = (decimal)ophisposfechamento.FnPucurva.Value;
                posicaoRendaFixa.PUOperacao = (decimal)ophisposfechamento.FnPuoper.Value;
                posicaoRendaFixa.Quantidade = (decimal)ophisposfechamento.FnQtdebancado.Value;
                posicaoRendaFixa.QuantidadeInicial = (decimal)ophisposfechamento.FnQtdebancado.Value;
                posicaoRendaFixa.QuantidadeBloqueada = 0;
                posicaoRendaFixa.PUJuros = 0;
                posicaoRendaFixa.PUCorrecao = 0;
                posicaoRendaFixa.ValorCorrecao = 0;
                posicaoRendaFixa.ValorJuros = 0;

                posicaoRendaFixa.TaxaOperacao = 0;
                if (ophisposfechamento.FnTxoper.HasValue)
                {
                    posicaoRendaFixa.TaxaOperacao = (decimal)ophisposfechamento.FnTxoper.Value;
                }

                posicaoRendaFixa.TipoNegociacao = (byte)TipoNegociacaoTitulo.Negociacao;
                posicaoRendaFixa.TipoOperacao = (byte)TipoOperacaoTitulo.CompraFinal;
                posicaoRendaFixa.ValorCurva = (decimal)ophisposfechamento.FnFincurva.Value;

                posicaoRendaFixa.ValorIOF = 0;
                if (ophisposfechamento.FnIof.HasValue)
                {
                    posicaoRendaFixa.ValorIOF = (decimal)ophisposfechamento.FnIof.Value;
                }

                posicaoRendaFixa.ValorIR = 0;
                if (ophisposfechamento.FnIr.HasValue)
                {
                    posicaoRendaFixa.ValorIR = (decimal)ophisposfechamento.FnIr.Value;
                }

                posicaoRendaFixa.ValorMercado = (decimal)ophisposfechamento.FnFincurva.Value;
            }

            posicaoRendaFixaCollection.Save();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idcliente"></param>
        /// <param name="idTitulo"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public decimal RetornaPuCusto(int idCliente, int idTitulo, DateTime dataOperacao)
        {
            decimal pu = 0;
            PosicaoRendaFixaCollection p = new PosicaoRendaFixaCollection();
            p.Query.Select(p.Query.PUOperacao, p.Query.Quantidade)
                   .Where(p.Query.IdCliente == idCliente &&
                          p.Query.IdTitulo == idTitulo,
                          p.Query.DataOperacao == dataOperacao);
            p.Query.Load();

            decimal totalQuantidade = 0;
            decimal totalValorOperacao = 0;
            foreach (PosicaoRendaFixa posicaoRendaFixa in p)
            {
                totalQuantidade += posicaoRendaFixa.Quantidade.Value;
                totalValorOperacao += posicaoRendaFixa.PUOperacao.Value * posicaoRendaFixa.Quantidade.Value;
            }

            if (totalQuantidade != 0)
            {
                pu = totalValorOperacao / totalQuantidade;
            }

            return pu;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void RepacutacaoRendaFixa(int idCliente, DateTime data)
        {
            Hashtable hsMemoria = new Hashtable();
            RepacutacaoRendaFixa(idCliente, data, ref hsMemoria);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void RepacutacaoRendaFixa(int idCliente, DateTime data, ref Hashtable hsMemoria)
        {
            #region Carrega Posição
            PosicaoRendaFixaCollection posicaoRendaFixaColl = new PosicaoRendaFixaCollection();
            RepactuacaoRendaFixaQuery repactuacaoQuery = new RepactuacaoRendaFixaQuery("repactuacao");
            PosicaoRendaFixaQuery posicaoQuery = new PosicaoRendaFixaQuery("posicao");
            TituloRendaFixaQuery tituloQuery = new TituloRendaFixaQuery("titulo");
            TituloRendaFixa titulo = new TituloRendaFixa();
            PapelRendaFixa papel = new PapelRendaFixa();

            posicaoQuery.Select(posicaoQuery,
                                repactuacaoQuery.IdTituloNovo);
            posicaoQuery.InnerJoin(repactuacaoQuery).On(posicaoQuery.IdTitulo.Equal(repactuacaoQuery.IdTituloOriginal));
            posicaoQuery.InnerJoin(tituloQuery).On(posicaoQuery.IdTitulo.Equal(tituloQuery.IdTitulo));
            posicaoQuery.Where(posicaoQuery.IdCliente.Equal(idCliente)
                              & repactuacaoQuery.DataEvento.Equal(data)
                              & tituloQuery.PermiteRepactuacao.Equal('S'));
            posicaoQuery.OrderBy(posicaoQuery.IdTitulo.Ascending);

            if (!posicaoRendaFixaColl.Load(posicaoQuery))
                return;
            #endregion
            
            List<int> titulosSemRepactuacao = new List<int>();
            CalculoRendaFixa calculoRendaFixa = new CalculoRendaFixa();

            foreach (PosicaoRendaFixa posicaoRendaFixa in posicaoRendaFixaColl)
            {
                int idTituloOrig = posicaoRendaFixa.IdTitulo.Value;
                int idTituloNovo = Convert.ToInt32(posicaoRendaFixa.GetColumn(RepactuacaoRendaFixaMetadata.ColumnNames.IdTituloNovo).ToString());

                //Mudança de Título
                posicaoRendaFixa.IdTitulo = idTituloNovo;

                titulo.LoadByPrimaryKey(idTituloNovo);
                papel.LoadByPrimaryKey(titulo.IdPapel.Value);

                int classe = papel.Classe.Value;
                bool parametrosAvancados = titulo.ParametrosAvancados.Equals("S");

                if (parametrosAvancados)
                {
                    //Calcula nova Taxa
                    PosicaoAtualizada posicaoAtualizada = calculoRendaFixa.CalculaPosicao_Fincs(data, papel, titulo, posicaoRendaFixa.PUCurva.Value, null, null, posicaoRendaFixa.IdOperacao.GetValueOrDefault(0), 0, 0, false, ref  hsMemoria);
                    posicaoRendaFixa.TaxaOperacao = posicaoAtualizada.Taxa;
                }
            }

            posicaoRendaFixaColl.Save();
        }

        //
        public void CalculoTIR_PosicoesZeradas(int idCliente, DateTime data, ref Hashtable hsMemoria)
        {
            CalculoRendaFixa calculoRendaFixa = new CalculoRendaFixa();
            TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
            PapelRendaFixa papelRendaFixa = new PapelRendaFixa();
            Hashtable hsTitulo = new Hashtable();

            PosicaoRendaFixaCollection posicaoRendaFixaColl = new PosicaoRendaFixaCollection();
            PosicaoRendaFixaQuery posicaoRendaFixaQuery = new PosicaoRendaFixaQuery("posicao");
            posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente.Equal(idCliente) &
                                        posicaoRendaFixaQuery.TaxaOperacao.Equal(0) &
                                        posicaoRendaFixaQuery.Quantidade.NotEqual(0));

            if (!posicaoRendaFixaColl.Load(posicaoRendaFixaQuery))
                return;

            foreach (PosicaoRendaFixa posicaoRendaFixa in posicaoRendaFixaColl)
            {
                try
                {
                    int idTitulo = posicaoRendaFixa.IdTitulo.Value;
                    tituloRendaFixa = new TituloRendaFixa();
                    papelRendaFixa = new PapelRendaFixa();

                    //Diminuir o número de consultas no banco de dados, em busca de papel e titulo
                    if (!hsTitulo.Contains(idTitulo))
                    {
                        tituloRendaFixa.LoadByPrimaryKey(posicaoRendaFixa.IdTitulo.Value);
                        papelRendaFixa = tituloRendaFixa.UpToPapelRendaFixaByIdPapel;

                        hsTitulo.Add(idTitulo, tituloRendaFixa);
                    }
                    else
                    {
                        tituloRendaFixa = (TituloRendaFixa)hsTitulo[idTitulo];
                        papelRendaFixa = tituloRendaFixa.UpToPapelRendaFixaByIdPapel;
                    }

                    if (!tituloRendaFixa.ParametrosAvancados.Equals("S"))
                        continue;

                    OperacaoRendaFixa operacao = posicaoRendaFixa.UpToOperacaoRendaFixaByIdOperacao;

                    PosicaoAtualizada posicaoAtualizada = calculoRendaFixa.CalculaPosicao_Fincs(posicaoRendaFixa.DataOperacao.Value, papelRendaFixa, tituloRendaFixa, operacao.PUOperacao.Value, null, null, posicaoRendaFixa.IdOperacao.Value, 0, 0, false, ref hsMemoria);
                    posicaoRendaFixa.TaxaOperacao = posicaoAtualizada.Taxa;

                    if (operacao != null && operacao.IdOperacao.GetValueOrDefault(0) > 0)
                    {
                        operacao.TaxaOperacao = posicaoAtualizada.Taxa;
                        operacao.Save();
                    }
                }
                catch (Exception ex)
                {
                    if (ParametrosConfiguracaoSistema.Outras.AbortaProcRFPorErroCadastraTitulo.Equals("S"))
                    {
                        throw new Exception("Operação:" + posicaoRendaFixa.IdOperacao + " - " + ex.Message);
                    }
                }

            }

            posicaoRendaFixaColl.Save();
        }

        public void DeletaPosicaoZerada(int idCliente, DateTime data)
        {
            PosicaoRendaFixaCollection posicaoRendaFixaCollectionDeletar = new PosicaoRendaFixaCollection();
            posicaoRendaFixaCollectionDeletar.Query.Select(posicaoRendaFixaCollectionDeletar.Query.IdPosicao);
            posicaoRendaFixaCollectionDeletar.Query.Where(posicaoRendaFixaCollectionDeletar.Query.IdCliente.Equal(idCliente),
                                                          posicaoRendaFixaCollectionDeletar.Query.Quantidade.Equal(0));

            if (posicaoRendaFixaCollectionDeletar.Query.Load())
            {
                posicaoRendaFixaCollectionDeletar.MarkAllAsDeleted();
                posicaoRendaFixaCollectionDeletar.Save();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="tipoAberturaIndexada"></param>
        public void AberturaIndexada(Cliente cliente, DateTime data, DateTime? dataAnterior)
        {
            PosicaoRendaFixaCollection posicaoColl = new PosicaoRendaFixaCollection();
            PosicaoRendaFixaQuery posicaoQuery = new PosicaoRendaFixaQuery("Posicao");
            PosicaoRendaFixaHistoricoQuery posicaoHistoricoQuery = new PosicaoRendaFixaHistoricoQuery("PosicaoHistorico");            
            TituloRendaFixaQuery tituloQuery = new TituloRendaFixaQuery("Titulo");
            PerfilMTMQuery perfilMTMQuery = new PerfilMTMQuery("Perfil");            

            TipoAberturaIndexada tipoAberturaIndexada = (TipoAberturaIndexada)cliente.AberturaIndexada;
            short idIndexador = cliente.IdIndiceAbertura.Value;
            int idCliente = cliente.IdCliente.Value;
            int idLocalFeriado = cliente.IdLocal.Value;
            decimal cotacaoIndexador = 0;

            if (dataAnterior == null)
            {
                dataAnterior = new DateTime();
                if (idLocalFeriado != LocalFeriadoFixo.Brasil)
                {
                    dataAnterior = Calendario.SubtraiDiaUtil(data, 1, idLocalFeriado, TipoFeriado.Outros);
                }
                else
                {
                    dataAnterior = Calendario.SubtraiDiaUtil(data, 1, idLocalFeriado, TipoFeriado.Brasil);
                }
            }

            #region Carrega Posição
            posicaoQuery.Select(posicaoQuery, posicaoHistoricoQuery.PUCurva.Coalesce("0").As("PuCurvaAnterior"));
            posicaoQuery.InnerJoin(posicaoHistoricoQuery).On(posicaoQuery.IdPosicao.Equal(posicaoHistoricoQuery.IdPosicao));

            if (tipoAberturaIndexada == TipoAberturaIndexada.Opcional)
            {
                posicaoQuery.InnerJoin(tituloQuery).On(tituloQuery.IdTitulo.Equal(posicaoQuery.IdTitulo));
                posicaoQuery.InnerJoin(perfilMTMQuery).On(perfilMTMQuery.IdPapel.Equal(tituloQuery.IdPapel));
                posicaoQuery.Where(perfilMTMQuery.DtReferencia.Equal(data), posicaoQuery.IdCliente.Equal(idCliente));
            }
            else
            {
                posicaoQuery.Where(posicaoQuery.IdCliente.Equal(idCliente));
            }

            posicaoQuery.Where(posicaoHistoricoQuery.DataHistorico.Equal(dataAnterior));

            if (!posicaoColl.Load(posicaoQuery))
                return;
            #endregion

            #region Busca Cotacao
            CotacaoIndice cotacaoIndice = new CotacaoIndice();
            cotacaoIndice.Query.Where(cotacaoIndice.Query.IdIndice.Equal(idIndexador)
                                      & cotacaoIndice.Query.Data.Equal(dataAnterior));

            if (cotacaoIndice.Query.Load())
            {
                cotacaoIndexador = Convert.ToDecimal(Math.Pow((double)(cotacaoIndice.Valor.Value / 100 + 1), (double)1 / 252));
            }
            else
            {
                Indice indice = new Indice();
                indice.LoadByPrimaryKey(idIndexador);
                throw new Exception("Cotação não cadastrada para o Indexador - " + indice.Descricao + " na data - " + dataAnterior);
            }
            #endregion

            #region Carrega LiquidacaoRendaFixa
            LiquidacaoRendaFixaCollection liqRendaFixaColl = new LiquidacaoRendaFixaCollection();
            liqRendaFixaColl.Query.Where(liqRendaFixaColl.Query.IdCliente.Equal(idCliente) &
                                         liqRendaFixaColl.Query.DataLiquidacao.Equal(data) &
                                         liqRendaFixaColl.Query.TipoLancamento.In((int)TipoLancamentoLiquidacao.Amortizacao, 
                                                                                  (int)TipoLancamentoLiquidacao.Juros,
                                                                                  (int)TipoLancamentoLiquidacao.PagtoPrincipal, 
                                                                                  (int)TipoLancamentoLiquidacao.Premio, 
                                                                                  (int)TipoLancamentoLiquidacao.PremioPosicao));

            List<LiquidacaoRendaFixa> lstAgendaRendaFixa = new List<LiquidacaoRendaFixa>();

            if (liqRendaFixaColl.Query.Load())            
                lstAgendaRendaFixa = (List<LiquidacaoRendaFixa>)liqRendaFixaColl;                                                             
            #endregion

            foreach (PosicaoRendaFixa posicaoRendaFixa in posicaoColl)
            {
                decimal pu = posicaoRendaFixa.PUMercado.Value;
                decimal puAnterior = Convert.ToDecimal(posicaoRendaFixa.GetColumn("PuCurvaAnterior"));

                decimal eventos = (from LiquidacaoRendaFixa agenda in lstAgendaRendaFixa
                                   where agenda.IdPosicaoResgatada == posicaoRendaFixa.IdPosicao
                                   select agenda.ValorBruto / agenda.Quantidade).Sum().GetValueOrDefault(0);

                posicaoRendaFixa.PUMercado = posicaoRendaFixa.PUCurva = ((puAnterior * cotacaoIndexador) - eventos);
                posicaoRendaFixa.ValorCurva = posicaoRendaFixa.ValorMercado = (posicaoRendaFixa.PUMercado.Value * posicaoRendaFixa.Quantidade.Value);
            }

            posicaoColl.Save();
        }

        public int BuscaIdOperacaoResgatada(int idPosicaoResgatada)
        {
            PosicaoRendaFixaQuery posicaoQuery = new PosicaoRendaFixaQuery();
            posicaoQuery.Where(posicaoQuery.IdPosicao == idPosicaoResgatada);

            if (!this.Load(posicaoQuery))
                throw new Exception("Posição selecionada não encontrada");
            if (!this.IdOperacao.HasValue)
                throw new Exception("Posição selecionada não possui IdOperação");

            return this.IdOperacao.Value;
        }
    }
}
