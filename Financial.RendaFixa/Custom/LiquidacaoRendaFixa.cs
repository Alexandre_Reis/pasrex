using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.RendaFixa.Enums;

namespace Financial.RendaFixa
{
    public class RendaOperacao //Usada no contexto do Itau
    {
        public decimal rendaTotal;
        public decimal rendaTributavel;
        public decimal rendaNaoTributavel;
        public decimal valorIR;
    }

    public partial class LiquidacaoRendaFixa : esLiquidacaoRendaFixa
    {


        public RendaOperacao CalculaRendaItau(int idOperacaoVenda)
        {
            LiquidacaoRendaFixaCollection liquidacaoRendaFixaCollection = new LiquidacaoRendaFixaCollection();
            liquidacaoRendaFixaCollection.Query.Where(liquidacaoRendaFixaCollection.Query.IdOperacaoVenda.Equal(idOperacaoVenda));
            liquidacaoRendaFixaCollection.Query.Load();

            decimal totalRendaTotal = 0;
            decimal totalRendaTributavel = 0;
            decimal totalRendaNaoTributavel = 0;
            decimal totalValorIR = 0;
            foreach (LiquidacaoRendaFixa liquidacaoRendaFixa in liquidacaoRendaFixaCollection)
            {
                int idPosicao = liquidacaoRendaFixa.IdPosicaoResgatada.Value;
                decimal puOperacao = liquidacaoRendaFixa.PULiquidacao.Value;
                decimal quantidade = liquidacaoRendaFixa.Quantidade.Value;

                PosicaoRendaFixaAbertura posicaoRendaFixaAbertura = new PosicaoRendaFixaAbertura();
                if (!posicaoRendaFixaAbertura.LoadByPrimaryKey(idPosicao, liquidacaoRendaFixa.DataLiquidacao.Value))
                {
                    continue;
                }
                decimal puPosicao = posicaoRendaFixaAbertura.PUOperacao.Value;

                decimal rendaTributavel = 0;
                decimal rendaNaoTributavel = 0;
                decimal rendaTotal = 0;
                if (liquidacaoRendaFixa.Status.Value == (byte)StatusLiquidacaoRendaFixa.Ajustado || 
                    liquidacaoRendaFixa.Status.Value == (byte)StatusLiquidacaoRendaFixa.LiberadoComAjuste)
                {
                    //Se ocorreu um ajuste, ignoro o calculo e acato o que foi preenchido no ajuste
                    rendaTributavel = liquidacaoRendaFixa.Rendimento.Value;
                    rendaNaoTributavel = liquidacaoRendaFixa.RendimentoNaoTributavel.Value;
                    rendaTotal = rendaTributavel + rendaNaoTributavel;
                }
                else
                {
                    rendaTotal = Math.Round(quantidade * (puOperacao - puPosicao), 2);
                    rendaTributavel = Math.Round(quantidade * (puOperacao - posicaoRendaFixaAbertura.PUMercado.Value), 2);
                    rendaNaoTributavel = rendaTotal - rendaTributavel;
                }

                decimal valorIR = liquidacaoRendaFixa.ValorIR.Value;

                totalRendaTotal += rendaTotal;
                totalRendaTributavel += rendaTributavel;
                totalRendaNaoTributavel += rendaNaoTributavel;
                totalValorIR += valorIR;
            }

            RendaOperacao rendaOperacao = new RendaOperacao();
            rendaOperacao.rendaNaoTributavel = totalRendaNaoTributavel;
            rendaOperacao.rendaTotal = totalRendaTotal;
            rendaOperacao.rendaTributavel = totalRendaTributavel;
            rendaOperacao.valorIR = totalValorIR;

            return rendaOperacao;
        }

        public RendaOperacao CalculaRendaItau()
        {
            decimal totalRendaTotal = 0;
            decimal totalRendaTributavel = 0;
            decimal totalRendaNaoTributavel = 0;
            decimal totalValorIR = 0;
            int idPosicao = IdPosicaoResgatada.Value;
            decimal puOperacao = PULiquidacao.Value;
            decimal quantidade = Quantidade.Value;

            PosicaoRendaFixaAbertura posicaoRendaFixaAbertura = new PosicaoRendaFixaAbertura();

            decimal puPosicao = puOperacao;
            if (posicaoRendaFixaAbertura.LoadByPrimaryKey(idPosicao, DataLiquidacao.Value))
            {
                puPosicao = posicaoRendaFixaAbertura.PUOperacao.Value;
            }

            decimal rendaTributavel = 0;
            decimal rendaNaoTributavel = 0;
            decimal rendaTotal = 0;
            if (Status.Value == (byte)StatusLiquidacaoRendaFixa.Ajustado || Status.Value == (byte)StatusLiquidacaoRendaFixa.LiberadoComAjuste)
            {
                rendaTributavel = Rendimento.Value;
                rendaNaoTributavel = RendimentoNaoTributavel.Value;
                rendaTotal = rendaTributavel + rendaNaoTributavel;
            }
            else
            {
                rendaTotal = Math.Round(quantidade * (puOperacao - puPosicao), 2);
                rendaTributavel = Math.Round(quantidade * (puOperacao - posicaoRendaFixaAbertura.PUMercado.Value), 2);
                rendaNaoTributavel = rendaTotal - rendaTributavel;
            }

            decimal valorIR = ValorIR.Value;

            totalRendaTotal += rendaTotal;
            totalRendaTributavel += rendaTributavel;
            totalRendaNaoTributavel += rendaNaoTributavel;
            totalValorIR += valorIR;
            
            RendaOperacao rendaOperacao = new RendaOperacao();
            rendaOperacao.rendaNaoTributavel = totalRendaNaoTributavel;
            rendaOperacao.rendaTotal = totalRendaTotal;
            rendaOperacao.rendaTributavel = totalRendaTributavel;
            rendaOperacao.valorIR = totalValorIR;

            return rendaOperacao;
        }

        public void DeletaLiquidacao(int idCliente, DateTime data)
        {
            LiquidacaoRendaFixaCollection liquidacaoRendaFixaCollection = new LiquidacaoRendaFixaCollection();
            liquidacaoRendaFixaCollection.Query.Select(liquidacaoRendaFixaCollection.Query.IdLiquidacao);
            liquidacaoRendaFixaCollection.Query.Where(liquidacaoRendaFixaCollection.Query.IdCliente.Equal(idCliente),
                                                      liquidacaoRendaFixaCollection.Query.DataLiquidacao.Equal(data),
                                                      liquidacaoRendaFixaCollection.Query.Status.NotIn((byte)StatusLiquidacaoRendaFixa.Ajustado,
                                                      (byte)StatusLiquidacaoRendaFixa.LiberadoComAjuste, (byte)StatusLiquidacaoRendaFixa.LiberadoSemAjuste));
            liquidacaoRendaFixaCollection.Query.Load();
            liquidacaoRendaFixaCollection.MarkAllAsDeleted();
            liquidacaoRendaFixaCollection.Save();
        }

    }
}
