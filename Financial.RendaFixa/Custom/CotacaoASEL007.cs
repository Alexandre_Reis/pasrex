﻿/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 22/12/2014 09:52:59
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Interfaces.Import.RendaFixa;
using log4net;
using System.Globalization;

namespace Financial.RendaFixa
{
    public partial class CotacaoASEL007 : esCotacaoASEL007
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(CotacaoASEL007));

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="pathArquivo">path completo do Diretorio onde procurar o Arquivo</param>
        public void CarregaASEL007(DateTime data, string pathArquivo)
        {
            #region logEntrada
            if (log.IsDebugEnabled)
            {
                log.Debug("Entrada nomeMetodo: ");
            }
            #endregion
            try
            {
                ASEL007 asel007 = new ASEL007();
                CotacaoASEL007Collection cotacaoASEL007Collection = new CotacaoASEL007Collection();
                CotacaoASEL007Collection cotacaoASEL007CollectionAux = new CotacaoASEL007Collection();

                // Deleta a tabela de CotacaoResolucao550 da data em questão
                cotacaoASEL007CollectionAux.DeletaCotacaoASEL007(data);

                // Pega os registros da collection de Res550
                ASEL007Collection asel007Collection = asel007.ProcessaASEL007(data, pathArquivo);
                ASEL007Collection asel007CollectionAux = new ASEL007Collection();

                // Filtra a Collection somente tipoRegistro = Dados
                asel007CollectionAux.CollectionASEL007 = asel007Collection.CollectionASEL007.FindAll(asel007.FilterASEL007ByDados);
                //
                for (int i = 0; i < asel007CollectionAux.CollectionASEL007.Count; i++)
                {
                    asel007 = asel007CollectionAux.CollectionASEL007[i];

                    #region Copia informações para CotacaoResolucao550
                    CotacaoASEL007 cotacaoASEL007 = new CotacaoASEL007();
                    //
                    cotacaoASEL007.Data = asel007.Data;
                    cotacaoASEL007.DataVencimento = asel007.DataVencimento;
                    cotacaoASEL007.CodigoTitulo = asel007.CodigoTitulo;
                    //
                    cotacaoASEL007Collection.AttachEntity(cotacaoASEL007);
                    #endregion
                }

                //cotacaoASEL007Collection.Save();

                #region logSaida
                if (log.IsDebugEnabled)
                {
                    log.Debug("Entrada nomeMetodo: ");
                }
                #endregion
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
