using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

namespace Financial.RendaFixa
{
	public partial class BloqueioRendaFixaCollection : esBloqueioRendaFixaCollection
	{
        private static readonly ILog log = LogManager.GetLogger(typeof(BloqueioRendaFixaCollection));

        /// <summary>
        /// Carrega o objeto BloqueioRendaFixaCollection com todos os campos de BloqueioRendaFixa.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns>bool indicando se trouxe registro.</returns>
        public bool BuscaBloqueio(int idCliente, DateTime data, byte tipoOperacao)
        {

            this.QueryReset();
            this.Query
                 .Select()
                 .Where(this.Query.IdCliente.Equal(idCliente),
                        this.Query.DataOperacao.Equal(data),
                        this.Query.TipoOperacao.Equal(tipoOperacao));

            this.Query.Load();

            return this.HasData;
        }

	}
}
