using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

namespace Financial.RendaFixa {
    public partial class CotacaoMercadoAndimaCollection : esCotacaoMercadoAndimaCollection {
        private static readonly ILog log = LogManager.GetLogger(typeof(CotacaoMercadoAndimaCollection));

        /// <summary>
        ///  Deleta tudo da CotacaoMercadoAndima de uma determinada data.
        /// </summary>
        /// <param name="data"></param>
        public void DeletaCotacaoMercadoAndima(DateTime data) {
            
            this.QueryReset();
            this.Query
              .Select(this.Query.Descricao, this.Query.DataReferencia, this.Query.DataEmissao,
                      this.Query.CodigoSELIC, this.Query.DataVencimento)
              .Where(this.Query.DataReferencia == data);

            this.Query.Load();
            
            this.MarkAllAsDeleted();
            this.Save();
        }
    }
}
