﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

namespace Financial.RendaFixa
{
	public partial class PosicaoRendaFixaAberturaCollection : esPosicaoRendaFixaAberturaCollection
	{
        public PosicaoRendaFixaAberturaCollection(PosicaoRendaFixaCollection posicaoRendaFixaCollection, DateTime dataHistorico)
        {
            for (int i = 0; i < posicaoRendaFixaCollection.Count; i++)
            {
                //
                PosicaoRendaFixaAbertura p = new PosicaoRendaFixaAbertura();

                // Para cada Coluna de PosicaoRendaFixa copia para PosicaoRendaFixa
                foreach (esColumnMetadata colPosicao in posicaoRendaFixaCollection.es.Meta.Columns)
                {
                    // Copia todas as colunas menos a Data 
                    esColumnMetadata colPosicaoRendaFixa = p.es.Meta.Columns.FindByPropertyName(colPosicao.PropertyName);
                    if (posicaoRendaFixaCollection[i].GetColumn(colPosicao.Name) != null)
                    {
                        p.SetColumn(colPosicaoRendaFixa.Name, posicaoRendaFixaCollection[i].GetColumn(colPosicao.Name));
                    }
                    p.DataHistorico = dataHistorico;
                }
                this.AttachEntity(p);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void BuscaPosicaoRendaFixaAberturaCompleta(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                .Where(this.Query.IdCliente == idCliente,
                       this.Query.DataHistorico.Equal(dataHistorico));

            this.Query.Load();
        }

        /// <summary>
        /// Deleta posições de renda fixa históricas.
        /// Filtra por DataHistorico > dataHistorico.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void DeletaPosicaoRendaFixaAberturaDataHistoricoMaior(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                    .Select(this.Query.IdPosicao, this.query.DataHistorico)
                    .Where(this.Query.IdCliente == idCliente,
                           this.Query.DataHistorico.GreaterThan(dataHistorico));

            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }
	}
}
