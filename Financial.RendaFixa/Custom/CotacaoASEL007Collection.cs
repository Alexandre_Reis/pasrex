/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 22/12/2014 09:52:58
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

namespace Financial.RendaFixa
{
    public partial class CotacaoASEL007Collection : esCotacaoASEL007Collection
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(CotacaoASEL007Collection));

        /// <summary>
        ///  Deleta tudo da CotacaoASEL007 de uma determinada data.
        /// </summary>
        /// <param name="data"></param>
        public void DeletaCotacaoASEL007(DateTime data)
        {
            this.QueryReset();
            this.Query
              .Select(this.Query.CodigoTitulo, this.Query.Data, this.Query.DataVencimento, this.Query.Pu)
              .Where(this.Query.Data == data);

            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }
    }
}
