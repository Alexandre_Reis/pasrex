﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

namespace Financial.RendaFixa
{
	public partial class PosicaoRendaFixaHistoricoCollection : esPosicaoRendaFixaHistoricoCollection
	{
        // Construtor
        // Cria uma nova PosicaoRendaFixaHistoricoCollection com os dados de PosicaoRendaFixaCollection
        // Todos os dados de PosicaoRendaFixaCollection são copiados, adicionando a dataHistorico
        public PosicaoRendaFixaHistoricoCollection(PosicaoRendaFixaCollection posicaoRendaFixaCollection, DateTime dataHistorico)
        {
            using (esTransactionScope scope = new esTransactionScope())
            {
                int max = 0;
                PosicaoRendaFixaHistorico posicaoRendaFixaHistoricoMax = new PosicaoRendaFixaHistorico();
                posicaoRendaFixaHistoricoMax.Query.Select(posicaoRendaFixaHistoricoMax.Query.IdPosicao.Max());
                posicaoRendaFixaHistoricoMax.Query.Load();

                if (posicaoRendaFixaHistoricoMax.IdPosicao.HasValue)
                {
                    max = posicaoRendaFixaHistoricoMax.IdPosicao.Value + 1;
                }
                else
                {
                    max = 1;
                }

                for (int i = 0; i < posicaoRendaFixaCollection.Count; i++)
                {
                    PosicaoRendaFixaHistorico p = new PosicaoRendaFixaHistorico();

                    // Para cada Coluna de PosicaoRendaFixa copia para PosicaoRendaFixaHistorico
                    foreach (esColumnMetadata colPosicao in posicaoRendaFixaCollection.es.Meta.Columns)
                    {
                        // Copia todas as colunas 
                        esColumnMetadata colPosicaoRendaFixaHistorico = p.es.Meta.Columns.FindByPropertyName(colPosicao.PropertyName);
                        if (posicaoRendaFixaCollection[i].GetColumn(colPosicao.Name) != null)
                        {
                            p.SetColumn(colPosicaoRendaFixaHistorico.Name, posicaoRendaFixaCollection[i].GetColumn(colPosicao.Name));
                        }
                    }

                    p.DataHistorico = dataHistorico;
                    p.IdPosicao = max;

                    max += 1;

                    this.AttachEntity(p);
                }

                scope.Complete();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void BuscaPosicaoRendaFixaHistoricoCompleta(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                .Where(this.Query.IdCliente == idCliente,
                       this.Query.DataHistorico.Equal(dataHistorico));

            this.Query.Load();
        }

        /// <summary>
        /// Deleta posições de renda fixa históricas.
        /// Filtra por DataHistorico >= dataHistorico.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void DeletaPosicaoRendaFixaHistoricoDataHistoricoMaiorIgual(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                    .Select(this.Query.IdPosicao, this.query.DataHistorico)
                    .Where(this.Query.IdCliente == idCliente,
                           this.Query.DataHistorico.GreaterThanOrEqual(dataHistorico));

            this.Query.Load();
            
            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// Deleta posições de renda fixa históricas.
        /// Filtra por DataHistorico > dataHistorico.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        public void DeletaPosicaoRendaFixaHistoricoDataHistoricoMaior(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                    .Select(this.Query.IdPosicao, this.query.DataHistorico)
                    .Where(this.Query.IdCliente == idCliente,
                           this.Query.DataHistorico.GreaterThan(dataHistorico));

            this.Query.Load();

            this.MarkAllAsDeleted();
            this.Save();
        }

        /// <summary>
        /// Carrega o objeto PosicaoRendaFixaCollection com os campos IdTitulo, Quantidade.Sum, PUMercado.Avg, PUOperacao.Avg, 
        /// ValorMercado.Sum, ValorIR.Sum, ValorIOF.Sum.
        /// Filtra por: this.Query.DataHistorico == dataHistorico.
        /// Group By IdTitulo
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns>indica se retornou registro</returns>
        public bool BuscaPosicaoRendaFixaAgrupado(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.IdTitulo,
                         this.Query.Quantidade.Sum(),
                         this.Query.PUMercado.Avg(),
                         this.Query.PUOperacao.Avg(),
                         this.Query.ValorMercado.Sum(),
                         this.Query.ValorIR.Sum(),
                         this.Query.ValorIOF.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.DataHistorico.Equal(dataHistorico))
                 .GroupBy(this.Query.IdTitulo);

            bool retorno = this.Query.Load();

            return retorno;
        }
	}
}
