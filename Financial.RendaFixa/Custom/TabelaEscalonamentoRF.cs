/*
===============================================================================
                     EntitySpaces(TM) by EntitySpaces, LLC
                 A New 2.0 Architecture for the .NET Framework
                          http://www.entityspaces.net
===============================================================================
                       EntitySpaces Version # 2007.0.0709.0
                       MyGeneration Version # 1.2.0.7
                           31/7/2007 15:09:11
-------------------------------------------------------------------------------
*/


using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Util;

namespace Financial.RendaFixa
{
    public partial class TabelaEscalonamentoRF : esTabelaEscalonamentoRF
	{
        /// <summary>
        /// Retorna taxa Escalonado por dias corridos
        /// </summary>
        /// <param name="idTitulo"></param>
        /// <param name="dataInicial"></param>
        /// <param name="dataFim"></param>
        /// <returns></returns>
        public decimal? RetornaTaxaEscalonada(int idTitulo, DateTime dataInicial, DateTime dataFim)
        {
            decimal? taxa = null;
            int prazo = Calendario.NumeroDias(dataInicial, dataFim);

            TabelaEscalonamentoRFCollection tabelaEscalonamentoRF_Coll = new TabelaEscalonamentoRFCollection();

            tabelaEscalonamentoRF_Coll.Query.Where(tabelaEscalonamentoRF_Coll.Query.IdTitulo.Equal(idTitulo) &
                                                   tabelaEscalonamentoRF_Coll.Query.Prazo.GreaterThanOrEqual(prazo));

            tabelaEscalonamentoRF_Coll.Query.OrderBy(tabelaEscalonamentoRF_Coll.Query.Prazo.Ascending);

            if (tabelaEscalonamentoRF_Coll.Query.Load())
            {
                taxa = tabelaEscalonamentoRF_Coll[0].Taxa.Value;
            }
            else
            {
                tabelaEscalonamentoRF_Coll = new TabelaEscalonamentoRFCollection();

                tabelaEscalonamentoRF_Coll.Query.Where(tabelaEscalonamentoRF_Coll.Query.IdTitulo.Equal(idTitulo) &
                                                       tabelaEscalonamentoRF_Coll.Query.Prazo.LessThan(prazo));

                tabelaEscalonamentoRF_Coll.Query.OrderBy(tabelaEscalonamentoRF_Coll.Query.Prazo.Descending);

                if (tabelaEscalonamentoRF_Coll.Query.Load())
                {
                    taxa = tabelaEscalonamentoRF_Coll[0].Taxa.Value;
                }
            }

            return taxa;
        }
	}
}
