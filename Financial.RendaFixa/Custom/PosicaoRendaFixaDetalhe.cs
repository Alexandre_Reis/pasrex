using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Util;

namespace Financial.RendaFixa
{
	public partial class PosicaoRendaFixaDetalhe : esPosicaoRendaFixaDetalhe
	{
        public void TrataProcessamentoDiario(int idCliente, DateTime data)
        {
            PosicaoRendaFixaDetalheCollection posicaoRendaFixaDetalheCollectionDeletar = new PosicaoRendaFixaDetalheCollection();
            posicaoRendaFixaDetalheCollectionDeletar.Query.Where(posicaoRendaFixaDetalheCollectionDeletar.Query.IdCliente.Equal(idCliente),
                                                                 posicaoRendaFixaDetalheCollectionDeletar.Query.DataHistorico.GreaterThanOrEqual(data));
            posicaoRendaFixaDetalheCollectionDeletar.Query.Load();

            posicaoRendaFixaDetalheCollectionDeletar.MarkAllAsDeleted();
            posicaoRendaFixaDetalheCollectionDeletar.Save();

            DateTime dataAnterior = Calendario.SubtraiDiaUtil(data, 1);


            PosicaoRendaFixaDetalheCollection posicaoRendaFixaDetalheCollection = new PosicaoRendaFixaDetalheCollection();
            posicaoRendaFixaDetalheCollection.Query.Where(posicaoRendaFixaDetalheCollection.Query.IdCliente.Equal(idCliente),
                                                          posicaoRendaFixaDetalheCollection.Query.DataHistorico.Equal(dataAnterior));
            posicaoRendaFixaDetalheCollection.Query.Load();

            PosicaoRendaFixaDetalheCollection posicaoRendaFixaDetalheCollectionInsert = new PosicaoRendaFixaDetalheCollection();

            foreach (PosicaoRendaFixaDetalhe posicaoRendaFixaDetalhe in posicaoRendaFixaDetalheCollection)
            {
                PosicaoRendaFixaDetalhe posicaoRendaFixaDetalheInsert = posicaoRendaFixaDetalheCollectionInsert.AddNew();
                posicaoRendaFixaDetalheInsert.DataHistorico = data;
                posicaoRendaFixaDetalheInsert.IdCliente = idCliente;
                posicaoRendaFixaDetalheInsert.IdPosicao = posicaoRendaFixaDetalhe.IdPosicao.Value;
                posicaoRendaFixaDetalheInsert.IdTitulo = posicaoRendaFixaDetalhe.IdTitulo.Value;
                posicaoRendaFixaDetalheInsert.Motivo = posicaoRendaFixaDetalhe.Motivo;
                posicaoRendaFixaDetalheInsert.QuantidadeBloqueada = posicaoRendaFixaDetalhe.QuantidadeBloqueada.Value;                
            }

            posicaoRendaFixaDetalheCollectionInsert.Save();
        }
	}
}
