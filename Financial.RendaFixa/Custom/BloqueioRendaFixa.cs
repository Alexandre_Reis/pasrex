﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.RendaFixa.Exceptions;
using Financial.RendaFixa.Enums;

namespace Financial.RendaFixa
{
	public partial class BloqueioRendaFixa : esBloqueioRendaFixa
	{
        /// <summary>
        /// Processa os bloqueios e desbloqueios de títulos, a partir de operações lançadas em BloqueioRendaFixa.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public void ProcessaBloqueio(int idCliente, DateTime data, byte tipoOperacao)
        {
            PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
            BloqueioRendaFixaCollection bloqueioRendaFixaCollection = new BloqueioRendaFixaCollection();

            bloqueioRendaFixaCollection.BuscaBloqueio(idCliente, data, tipoOperacao);

            for (int i = 0; i < bloqueioRendaFixaCollection.Count; i++)
            {
                BloqueioRendaFixa bloqueioRendaFixa = bloqueioRendaFixaCollection[i];
                int idTitulo = bloqueioRendaFixa.IdTitulo.Value;
                int idPosicao = bloqueioRendaFixa.IdPosicao.Value;
                decimal quantidade = bloqueioRendaFixa.Quantidade.Value;
                string motivoBloqueio = !String.IsNullOrEmpty(bloqueioRendaFixa.Motivo)? bloqueioRendaFixa.Motivo: "";

                PosicaoRendaFixa posicaoRendaFixa = new PosicaoRendaFixa();
                if (!posicaoRendaFixa.BuscaQuantidades(idPosicao))
                {
                    throw new PosicaoInexistenteException("Posição inexistente a ser (des)bloqueada. Nr da posição " + idPosicao);
                }

                if (tipoOperacao == (int)TipoOperacaoBloqueio.Bloqueio)
                {
                    posicaoRendaFixa.QuantidadeBloqueada += quantidade;

                    if (posicaoRendaFixa.QuantidadeBloqueada.Value > posicaoRendaFixa.Quantidade.Value)
                    {
                        posicaoRendaFixa.QuantidadeBloqueada = posicaoRendaFixa.Quantidade.Value;
                    }
                }
                else
                {
                    posicaoRendaFixa.QuantidadeBloqueada -= quantidade;

                    if (posicaoRendaFixa.QuantidadeBloqueada.Value < 0)
                    {
                        posicaoRendaFixa.QuantidadeBloqueada = 0;
                    }
                }

                PosicaoRendaFixaDetalhe posicaoRendaFixaDetalhe = new PosicaoRendaFixaDetalhe();
                if (posicaoRendaFixaDetalhe.LoadByPrimaryKey(idCliente, idPosicao, data))
                {
                    posicaoRendaFixaDetalhe.QuantidadeBloqueada = posicaoRendaFixa.QuantidadeBloqueada.Value;
                    posicaoRendaFixaDetalhe.Motivo = motivoBloqueio;                    
                }
                else
                {
                    posicaoRendaFixaDetalhe = new PosicaoRendaFixaDetalhe();
                    posicaoRendaFixaDetalhe.DataHistorico = data;
                    posicaoRendaFixaDetalhe.IdCliente = idCliente;
                    posicaoRendaFixaDetalhe.IdPosicao = idPosicao;
                    posicaoRendaFixaDetalhe.IdTitulo = idTitulo;
                    posicaoRendaFixaDetalhe.Motivo = motivoBloqueio;
                    posicaoRendaFixaDetalhe.QuantidadeBloqueada = posicaoRendaFixa.QuantidadeBloqueada.Value;                    
                }
                posicaoRendaFixaDetalhe.Save();

                posicaoRendaFixa.Save();
            }
            
        }


	}
}
