﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Common;
using Financial.Common.Enums;
using Financial.Util;
using Financial.RendaFixa.Enums;
using Financial.RendaFixa.Exceptions;
using Financial.Util.Enums;
using Financial.Investidor;
using Financial.Investidor.Enums;
using Financial.Tributo;
using Financial.ContaCorrente;
using Financial.ContaCorrente.Enums;
using Financial.RendaFixa;
using Financial.Fundo;
using Financial.Fundo.Enums;
using Financial.Tributo.Custom;
using System.Collections;
using Financial.BibliotecaFincs.RendaFixa;
using Financial.RendaFixa.Controller;

namespace Financial.RendaFixa
{
    public class PricingDiario
    {
        public string contagemDias; //Indica se é por du, dc ou dc360
        public DateTime dataEmissao;
        public DateTime dataVencimento;
        public string indexador;
        public int? idIndexador;
        public byte? tipoIndice;
        public decimal puNominal;
        public decimal taxaJuros;
        public decimal fatorCorrecao;
        public decimal pu;
        public List<ValoresFluxoDesconto> listaValores;
        public List<ValoresFluxoCorrecao> listaValoresCorrecao;
        public string descricao;
        public DateTime? dataOperacao;
        public decimal puOperacao;
        public decimal puGrossup;
        public decimal valorFluxoJuros;
        public decimal valorFluxoAmortizacao;

        public List<CalculoFinanceiro.CorrecaoIndice> listaCorrecaoIndice;

        public class ValoresFluxoDesconto
        {
            public DateTime dataEvento;
            public DateTime dataPagamento;
            public decimal valor;
            public decimal valorCorrigido;
            public int numeroDias;
            public decimal valorAtual;
        }

        public class ValoresFluxoCorrecao
        {
            public decimal? taxa;
            public decimal? valor;
            public DateTime dataEvento;
            public string tipoEvento;
            public decimal fatorCorrecaoFluxo;
            public decimal puNominalAmortizado;
            public decimal valorCorrigido;

            public decimal fatorCorrecaoOperacao;
            public decimal puCorrigido;            
        }
        
        public PricingDiario()
        { }

        public PricingDiario(DateTime dataCalculo, PosicaoRendaFixa posicaoRendaFixa, bool eventosData, decimal taxa)
        {
            this.listaValores = new List<ValoresFluxoDesconto>();
            this.listaCorrecaoIndice = new List<CalculoFinanceiro.CorrecaoIndice>();

            #region Carga das informações do "cabeçalho"
            TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
            tituloRendaFixa.LoadByPrimaryKey(posicaoRendaFixa.IdTitulo.Value);

            PapelRendaFixa papelRendaFixa = new PapelRendaFixa();
            papelRendaFixa.LoadByPrimaryKey(tituloRendaFixa.IdPapel.Value);

            if (papelRendaFixa.ContagemDias.Value == (byte)ContagemDias.Corridos)
            {
                this.contagemDias = "Dias Corridos";
            }
            else if (papelRendaFixa.ContagemDias.Value == (byte)ContagemDias.Dias360)
            {
                this.contagemDias = "Dias 360";
            }
            else if (papelRendaFixa.ContagemDias.Value == (byte)ContagemDias.Uteis)
            {
                this.contagemDias = "Dias Úteis";
            }

            this.descricao = tituloRendaFixa.Descricao;
            this.dataEmissao = tituloRendaFixa.DataEmissao.Value;
            this.dataVencimento = tituloRendaFixa.DataVencimento.Value;
            this.puNominal = tituloRendaFixa.PUNominal.Value;

            this.taxaJuros = 0;

            if (taxa != 0)
            {
                this.taxaJuros = taxa;
            }
            else
            {
                if (posicaoRendaFixa.TaxaOperacao.HasValue && posicaoRendaFixa.TaxaOperacao.Value != 0)
                {
                    this.taxaJuros = posicaoRendaFixa.TaxaOperacao.Value;
                }
                else if (tituloRendaFixa.Taxa.HasValue && tituloRendaFixa.Taxa.Value != 0)
                {
                    this.taxaJuros = tituloRendaFixa.Taxa.Value;
                }
            }

            decimal percentual = 100;
            if (tituloRendaFixa.Percentual.HasValue)
            {
                percentual = tituloRendaFixa.Percentual.Value;
            }

            this.puOperacao = posicaoRendaFixa.PUOperacao.Value;
            this.dataOperacao = posicaoRendaFixa.DataOperacao.Value;
            #endregion

            Indice indice = new Indice();
            #region Informações de Indice
            this.indexador = "";
            if (tituloRendaFixa.IdIndice.HasValue)
            {
                indice.LoadByPrimaryKey(tituloRendaFixa.IdIndice.Value);

                this.indexador = indice.Descricao;
                this.tipoIndice = indice.Tipo.Value;

                int idIndice = tituloRendaFixa.IdIndice.Value;
                if (indice.IdIndiceBase.HasValue)
                {
                    idIndice = indice.IdIndiceBase.Value;
                }
                this.idIndexador = idIndice;
            }
            #endregion

            CalculoRendaFixa calculoRendaFixa = new CalculoRendaFixa();
            if (papelRendaFixa.Classe.Value == (int)ClasseRendaFixa.PosFixado_FluxoCorrigido)
            {
                PosicaoRendaFixa.PosicaoAtualizada posicaoAtualizada = calculoRendaFixa.CalculaPUFluxoCorrigido(dataCalculo, papelRendaFixa, tituloRendaFixa, posicaoRendaFixa, indice, 
                                                                                                                out this.listaValoresCorrecao);
                if (this.listaValoresCorrecao.Count > 0)
                {
                    this.pu = posicaoAtualizada.PuAtualizado;
                }
            }
            else
            {
                #region Trata memória de cálculo para títulos sem fluxo com pagto de correção

                decimal puAtualizado = 0;

                this.fatorCorrecao = 1;
                #region Cálculo do fator de correção desde a emissão
                DateTime dataBase = dataEmissao;
                AgendaRendaFixaCollection agendaRendaFixaCollectionCorrecao = new AgendaRendaFixaCollection();
                agendaRendaFixaCollectionCorrecao.Query.Select(agendaRendaFixaCollectionCorrecao.Query.DataEvento);
                agendaRendaFixaCollectionCorrecao.Query.Where(agendaRendaFixaCollectionCorrecao.Query.IdTitulo == tituloRendaFixa.IdTitulo.Value &&
                                                            (agendaRendaFixaCollectionCorrecao.Query.TipoEvento.Equal((byte)TipoEventoTitulo.JurosCorrecao) ||
                                                            (agendaRendaFixaCollectionCorrecao.Query.TipoEvento.Equal((byte)TipoEventoTitulo.PagamentoCorrecao) &&
                                                            (agendaRendaFixaCollectionCorrecao.Query.Taxa == 0 || agendaRendaFixaCollectionCorrecao.Query.Taxa == 100))));

                if (eventosData)
                {
                    agendaRendaFixaCollectionCorrecao.Query.Where(agendaRendaFixaCollectionCorrecao.Query.DataPagamento.LessThan(dataCalculo));                    
                }
                else
                {
                    agendaRendaFixaCollectionCorrecao.Query.Where(agendaRendaFixaCollectionCorrecao.Query.DataPagamento.LessThanOrEqual(dataCalculo));
                }

                agendaRendaFixaCollectionCorrecao.Query.OrderBy(agendaRendaFixaCollectionCorrecao.Query.DataEvento.Descending);
                agendaRendaFixaCollectionCorrecao.Query.Load();

                if (agendaRendaFixaCollectionCorrecao.Count > 0)
                {
                    dataBase = agendaRendaFixaCollectionCorrecao[0].DataEvento.Value;
                }

                if (tituloRendaFixa.IdIndice.HasValue && dataCalculo != dataBase)
                {
                    if (indice.Tipo.Value == (byte)TipoIndice.Decimal && indice.TipoDivulgacao.Value == (byte)TipoDivulgacaoIndice.Mensal)
                    {
                        int diaAniversario = dataVencimento.Day;
                        List<DateTime> listaDatasIndice = new List<DateTime>();
                        if (papelRendaFixa.ContagemDias.Value == (byte)ContagemDias.Corridos || papelRendaFixa.ContagemDias.Value == (byte)ContagemDias.Dias360)
                        {
                            listaDatasIndice = CalculoFinanceiro.RetornaDatasAniversario(dataBase, dataCalculo, diaAniversario);
                            this.fatorCorrecao = CalculoFinanceiro.CalculaFatorIndicePreco(listaDatasIndice, indice, percentual,
                                                                 diaAniversario, null, null, out this.listaCorrecaoIndice);
                        }
                        else
                        {
                            listaDatasIndice = CalculoFinanceiro.RetornaDatasAniversario(dataBase, dataCalculo, diaAniversario,
                                                (byte)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                            this.fatorCorrecao = CalculoFinanceiro.CalculaFatorIndicePreco(listaDatasIndice, indice, percentual,
                                                diaAniversario, (byte)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil, out this.listaCorrecaoIndice);
                        }
                    }
                    else if (this.tipoIndice.Value == (byte)TipoIndice.Decimal)
                    {
                        this.fatorCorrecao = CalculoFinanceiro.CalculaFatorIndiceDecimal(dataBase, dataCalculo, indice.IdIndice.Value,
                                                                        percentual, out this.listaCorrecaoIndice);
                    }
                    else
                    {
                        DateTime dataAnterior = Calendario.SubtraiDiaUtil(dataCalculo, 1);
                        this.fatorCorrecao = CalculoFinanceiro.CalculaFatorIndicePercentual(dataBase, dataAnterior, indice,
                                                                    tituloRendaFixa.Percentual.Value, false, out this.listaCorrecaoIndice);
                    }
                }                
                #endregion

                AgendaRendaFixaCollection agendaRendaFixaCollection = new AgendaRendaFixaCollection();
                agendaRendaFixaCollection.Query.Select(agendaRendaFixaCollection.Query.DataEvento,
                                                       agendaRendaFixaCollection.Query.DataPagamento,
                                                       agendaRendaFixaCollection.Query.TipoEvento,
                                                       agendaRendaFixaCollection.Query.Valor,
                                                       agendaRendaFixaCollection.Query.Taxa);
                agendaRendaFixaCollection.Query.Where(agendaRendaFixaCollection.Query.IdTitulo.Equal(posicaoRendaFixa.IdTitulo.Value),
                                                      agendaRendaFixaCollection.Query.DataEvento.GreaterThan(dataCalculo),
                                                      agendaRendaFixaCollection.Query.TipoEvento.NotEqual((byte)TipoEventoTitulo.PagamentoCorrecao));
                agendaRendaFixaCollection.Query.OrderBy(agendaRendaFixaCollection.Query.DataEvento.Ascending,
                                                        agendaRendaFixaCollection.Query.IdAgenda.Ascending);
                agendaRendaFixaCollection.Query.Load();

                bool existeFluxoDesconto = agendaRendaFixaCollection.Count > 0;

                if (existeFluxoDesconto)
                {
                    PosicaoRendaFixa.PosicaoAtualizada posicaoAtualizada = calculoRendaFixa.CalculaPUFluxoDesconto(dataCalculo, papelRendaFixa, tituloRendaFixa, posicaoRendaFixa, indice, 
                                                                                                                   this.taxaJuros, out this.listaValores);
                    if (this.listaValores.Count > 0)
                    {
                        puAtualizado = posicaoAtualizada.PuAtualizado;
                    }                    
                }
                else if (!this.idIndexador.HasValue)
                {
                    #region PRÉ-FIXADO SEM FLUXO
                    decimal puFinal = 0;
                    int numeroDiasDesconto = 0;
                    if (papelRendaFixa.Classe.Value == (int)ClasseRendaFixa.Pre_Atualizado) //Calcula atualizando o PU de Operação
                    {
                        if (papelRendaFixa.BaseAno.Value == (int)BaseCalculo.Base252)
                        {
                            numeroDiasDesconto = Calendario.NumeroDias(posicaoRendaFixa.DataOperacao.Value, dataCalculo, (int)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                        }
                        else if (papelRendaFixa.BaseAno.Value == (int)BaseCalculo.Base360)
                        {
                            numeroDiasDesconto = Calendario.NumeroDias360(posicaoRendaFixa.DataOperacao.Value, dataCalculo);
                        }
                        else
                        {
                            numeroDiasDesconto = Calendario.NumeroDias(posicaoRendaFixa.DataOperacao.Value, dataCalculo);
                        }

                        decimal fator = (decimal)Math.Pow((double)(1 + this.taxaJuros / 100M), (double)((decimal)numeroDiasDesconto / (int)papelRendaFixa.BaseAno.Value));
                        puFinal = posicaoRendaFixa.PUOperacao.Value * fator;
                    }
                    else //Calcula descontando o PU Nominal
                    {
                        if (papelRendaFixa.BaseAno.Value == (int)BaseCalculo.Base252)
                        {
                            numeroDiasDesconto = Calendario.NumeroDias(dataCalculo, dataVencimento, (int)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                        }
                        else if (papelRendaFixa.BaseAno.Value == (int)BaseCalculo.Base360)
                        {
                            numeroDiasDesconto = Calendario.NumeroDias360(dataCalculo, dataVencimento);
                        }
                        else
                        {
                            numeroDiasDesconto = Calendario.NumeroDias(dataCalculo, dataVencimento);
                        }

                        decimal fator = (decimal)Math.Pow((double)(1 + this.taxaJuros / 100M), (double)((decimal)numeroDiasDesconto / (int)papelRendaFixa.BaseAno.Value));
                        puFinal = puNominal / fator;
                    }


                    ValoresFluxoDesconto valores = new ValoresFluxoDesconto();
                    valores.dataEvento = dataVencimento;
                    valores.dataPagamento = dataVencimento;
                    valores.numeroDias = numeroDiasDesconto;
                    valores.valor = puNominal;
                    valores.valorAtual = puFinal;
                    valores.valorCorrigido = puNominal;

                    this.listaValores.Add(valores);

                    puAtualizado = puFinal;
                    #endregion
                }
                else
                {
                    puAtualizado = posicaoRendaFixa.PUOperacao.Value * this.fatorCorrecao;
                }

                this.pu = puAtualizado;
                #endregion
            }

            this.puGrossup = this.pu;
            Cliente cliente = posicaoRendaFixa.UpToClienteByIdCliente;
            bool grossUp = cliente.GrossUP.Value != (byte)GrossupCliente.NaoFaz;
            TituloIsentoIR tituloIsentoIR = (TituloIsentoIR)tituloRendaFixa.IsentoIR.Value;
            if (grossUp && (tituloIsentoIR == TituloIsentoIR.Isento || tituloIsentoIR == TituloIsentoIR.IsentoPF))
            {
                this.puGrossup = posicaoRendaFixa.PUMercado.Value;
            }
        }

        public PricingDiario(DateTime dataCalculo, TituloRendaFixa titulo, bool eventosData, decimal taxa, 
                             DateTime? dataOperacao, decimal? puOperacao)
        {
            CalculoRendaFixa calculoRendaFixa = new CalculoRendaFixa();

            this.listaValores = new List<ValoresFluxoDesconto>();
            this.listaCorrecaoIndice = new List<CalculoFinanceiro.CorrecaoIndice>();

            #region Carga das informações do "cabeçalho"
            PapelRendaFixa papelRendaFixa = new PapelRendaFixa();
            papelRendaFixa.LoadByPrimaryKey(titulo.IdPapel.Value);

            if (papelRendaFixa.ContagemDias.Value == (byte)ContagemDias.Corridos)
            {
                this.contagemDias = "Dias Corridos";
            }
            else if (papelRendaFixa.ContagemDias.Value == (byte)ContagemDias.Dias360)
            {
                this.contagemDias = "Dias 360";
            }
            else if (papelRendaFixa.ContagemDias.Value == (byte)ContagemDias.Uteis)
            {
                this.contagemDias = "Dias Úteis";
            }

            this.descricao = titulo.Descricao;
            this.dataEmissao = titulo.DataEmissao.Value;
            this.dataVencimento = titulo.DataVencimento.Value;
            this.puNominal = titulo.PUNominal.Value;
            
            this.taxaJuros = taxa;
            
            decimal percentual = 100;
            if (titulo.Percentual.HasValue)
            {
                percentual = titulo.Percentual.Value;
            }

            this.puOperacao = 0;
            if (puOperacao.HasValue)
            {
                this.puOperacao = puOperacao.Value;
            }

            this.dataOperacao = null;
            if (dataOperacao.HasValue)
            {
                this.dataOperacao = dataOperacao.Value;
            }
            #endregion

            Indice indice = new Indice();
            #region Informações de Indice
            this.indexador = "";
            if (titulo.IdIndice.HasValue)
            {                
                indice.LoadByPrimaryKey(titulo.IdIndice.Value);

                this.indexador = indice.Descricao;
                this.tipoIndice = indice.Tipo.Value;

                int idIndice = titulo.IdIndice.Value;
                if (indice.IdIndiceBase.HasValue)
                {
                    idIndice = indice.IdIndiceBase.Value;
                }
                this.idIndexador = idIndice;
            }
            #endregion

            AgendaRendaFixaCollection agendaRendaFixaCollection = new AgendaRendaFixaCollection();
            agendaRendaFixaCollection.Query.Select(agendaRendaFixaCollection.Query.DataEvento,
                                                   agendaRendaFixaCollection.Query.DataPagamento,
                                                   agendaRendaFixaCollection.Query.TipoEvento,
                                                   agendaRendaFixaCollection.Query.Valor,
                                                   agendaRendaFixaCollection.Query.Taxa);
            agendaRendaFixaCollection.Query.Where(agendaRendaFixaCollection.Query.IdTitulo.Equal(titulo.IdTitulo.Value),
                                                  agendaRendaFixaCollection.Query.DataEvento.GreaterThan(dataCalculo),
                                                  agendaRendaFixaCollection.Query.TipoEvento.NotEqual((byte)TipoEventoTitulo.PagamentoCorrecao));
            agendaRendaFixaCollection.Query.OrderBy(agendaRendaFixaCollection.Query.DataEvento.Ascending,
                                                    agendaRendaFixaCollection.Query.IdAgenda.Ascending);
            agendaRendaFixaCollection.Query.Load();

            bool existeFluxoDesconto = agendaRendaFixaCollection.Count > 0;
            bool existeFluxoCorrecao = false;

            if (!existeFluxoDesconto)
            {
                DateTime dataBaseFluxo = dataOperacao.HasValue? dataOperacao.Value: dataEmissao;
                agendaRendaFixaCollection = new AgendaRendaFixaCollection();
                agendaRendaFixaCollection.Query.Select(agendaRendaFixaCollection.Query.DataEvento,
                                                       agendaRendaFixaCollection.Query.Taxa,
                                                       agendaRendaFixaCollection.Query.Valor,
                                                       agendaRendaFixaCollection.Query.TipoEvento);
                agendaRendaFixaCollection.Query.Where(agendaRendaFixaCollection.Query.IdTitulo.Equal(titulo.IdTitulo.Value),
                                                      agendaRendaFixaCollection.Query.TipoEvento.In((byte)TipoEventoTitulo.Amortizacao,
                                                                                                    (byte)TipoEventoTitulo.PagamentoCorrecao),
                                                      agendaRendaFixaCollection.Query.DataEvento.GreaterThan(dataBaseFluxo),
                                                      agendaRendaFixaCollection.Query.DataEvento.LessThanOrEqual(dataCalculo));
                agendaRendaFixaCollection.Query.OrderBy(agendaRendaFixaCollection.Query.DataEvento.Ascending,
                                                        agendaRendaFixaCollection.Query.IdAgenda.Ascending);
                agendaRendaFixaCollection.Query.Load();

                existeFluxoCorrecao = agendaRendaFixaCollection.Count > 0;
            }

            this.fatorCorrecao = 1;
            if (!existeFluxoCorrecao)
            {
                #region Cálculo do fator de correção "único"
                DateTime dataBase = !existeFluxoDesconto && dataOperacao.HasValue ? dataOperacao.Value : dataEmissao;
                AgendaRendaFixaCollection agendaRendaFixaCollectionCorrecao = new AgendaRendaFixaCollection();
                agendaRendaFixaCollectionCorrecao.Query.Select(agendaRendaFixaCollectionCorrecao.Query.DataEvento);
                agendaRendaFixaCollectionCorrecao.Query.Where(agendaRendaFixaCollectionCorrecao.Query.IdTitulo == titulo.IdTitulo.Value &
                                                            (
                                                              agendaRendaFixaCollectionCorrecao.Query.TipoEvento.Equal((byte)TipoEventoTitulo.JurosCorrecao) |
                                                              (agendaRendaFixaCollectionCorrecao.Query.TipoEvento.Equal((byte)TipoEventoTitulo.PagamentoCorrecao) &
                                                               agendaRendaFixaCollectionCorrecao.Query.Taxa.In(0, 100)
                                                               )
                                                            ));

                if (eventosData)
                {
                    agendaRendaFixaCollectionCorrecao.Query.Where(agendaRendaFixaCollectionCorrecao.Query.DataPagamento.LessThanOrEqual(dataCalculo));
                }
                else
                {
                    agendaRendaFixaCollectionCorrecao.Query.Where(agendaRendaFixaCollectionCorrecao.Query.DataPagamento.LessThan(dataCalculo));
                }

                agendaRendaFixaCollectionCorrecao.Query.OrderBy(agendaRendaFixaCollectionCorrecao.Query.DataEvento.Descending);
                agendaRendaFixaCollectionCorrecao.Query.Load();

                if (agendaRendaFixaCollectionCorrecao.Count > 0)
                {
                    dataBase = agendaRendaFixaCollectionCorrecao[0].DataEvento.Value;
                }

                if (titulo.IdIndice.HasValue && dataCalculo != dataBase)
                {
                    if (indice.Tipo.Value == (byte)TipoIndice.Decimal && indice.TipoDivulgacao.Value == (byte)TipoDivulgacaoIndice.Mensal)
                    {
                        int diaAniversario = dataVencimento.Day;
                        List<DateTime> listaDatasIndice = new List<DateTime>();
                        if (papelRendaFixa.ContagemDias.Value == (byte)ContagemDias.Corridos || papelRendaFixa.ContagemDias.Value == (byte)ContagemDias.Dias360)
                        {
                            listaDatasIndice = CalculoFinanceiro.RetornaDatasAniversario(dataBase, dataCalculo, diaAniversario);
                            this.fatorCorrecao = CalculoFinanceiro.CalculaFatorIndicePreco(listaDatasIndice, indice, percentual,
                                                                 diaAniversario, null, null, out this.listaCorrecaoIndice);
                        }
                        else
                        {
                            listaDatasIndice = CalculoFinanceiro.RetornaDatasAniversario(dataBase, dataCalculo, diaAniversario,
                                                (byte)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                            this.fatorCorrecao = CalculoFinanceiro.CalculaFatorIndicePreco(listaDatasIndice, indice, percentual,
                                                diaAniversario, (byte)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil, out this.listaCorrecaoIndice);
                        }
                    }
                    else if (this.tipoIndice.Value == (byte)TipoIndice.Decimal)
                    {
                        this.fatorCorrecao = CalculoFinanceiro.CalculaFatorIndiceDecimal(dataBase, dataCalculo, indice.IdIndice.Value,
                                                                        percentual, out this.listaCorrecaoIndice);
                    }
                    else
                    {
                        DateTime dataAnterior = Calendario.SubtraiDiaUtil(dataCalculo, 1);
                        this.fatorCorrecao = CalculoFinanceiro.CalculaFatorIndicePercentual(dataBase, dataAnterior, indice,
                                                                    titulo.Percentual.Value, false, out this.listaCorrecaoIndice);
                    }
                }
                #endregion
            }

            decimal puNominalAmortizado = puNominal;
            DateTime dataBaseAmortizacao = (existeFluxoCorrecao && dataOperacao.HasValue)? dataOperacao.Value : dataCalculo;
            #region Ajusta o PU nominal pelas amortizações ocorridas
            AgendaRendaFixaCollection agendaRendaFixaCollectionAmortizacao = new AgendaRendaFixaCollection();
            agendaRendaFixaCollectionAmortizacao.Query.Select(agendaRendaFixaCollectionAmortizacao.Query.Taxa,
                                                              agendaRendaFixaCollectionAmortizacao.Query.Valor);
            agendaRendaFixaCollectionAmortizacao.Query.Where(agendaRendaFixaCollectionAmortizacao.Query.IdTitulo.Equal(titulo.IdTitulo.Value),
                                                  agendaRendaFixaCollectionAmortizacao.Query.TipoEvento.In((byte)TipoEventoTitulo.Amortizacao));


            if (eventosData)
            {
                agendaRendaFixaCollectionAmortizacao.Query.Where(agendaRendaFixaCollectionAmortizacao.Query.DataPagamento.LessThanOrEqual(dataBaseAmortizacao));
            }
            else
            {
                agendaRendaFixaCollectionAmortizacao.Query.Where(agendaRendaFixaCollectionAmortizacao.Query.DataPagamento.LessThan(dataBaseAmortizacao));
            }

            agendaRendaFixaCollectionAmortizacao.Query.Load();

            foreach (AgendaRendaFixa agendaRendaFixaAmortizacao in agendaRendaFixaCollectionAmortizacao)
            {
                if (agendaRendaFixaAmortizacao.Taxa.HasValue && agendaRendaFixaAmortizacao.Taxa.Value != 0)
                {
                    decimal taxaAmortizacao = agendaRendaFixaAmortizacao.Taxa.Value;
                    puNominalAmortizado -= titulo.PUNominal.Value * taxaAmortizacao / 100M;
                }
                else if (agendaRendaFixaAmortizacao.Valor.HasValue && agendaRendaFixaAmortizacao.Valor.Value != 0)
                {
                    puNominalAmortizado -= agendaRendaFixaAmortizacao.Valor.Value; ;
                }
            }
            #endregion
                            
            decimal puAtualizado = 0;                
            decimal vpl = 0;
            if (existeFluxoDesconto)
            {
                #region Pricing com fluxo com taxa ou valor indicado
                DateTime dataUltima = agendaRendaFixaCollection[agendaRendaFixaCollection.Count - 1].DataEvento.Value;

                DiasNaoUteisContainer diasNaoUteisContainer = new DiasNaoUteisContainer();
                if (papelRendaFixa.BaseAno.Value == (int)BaseCalculo.Base252)
                {
                    diasNaoUteisContainer = new DiasNaoUteisContainer((int)LocalFeriadoFixo.Brasil, dataCalculo, dataUltima);
                }

                foreach (AgendaRendaFixa agendaRendaFixa in agendaRendaFixaCollection)
                {
                    DateTime dataEvento = agendaRendaFixa.DataEvento.Value;
                    DateTime dataPagamento = agendaRendaFixa.DataPagamento.Value;

                    decimal valorFluxoOriginal = 0;
                    decimal valorFluxo = 0;
                    #region Calcula valor do fluxo
                    if (agendaRendaFixa.TipoEvento.Value == (byte)TipoEventoTitulo.Amortizacao)
                    {
                        decimal valorAmortizacao = 0;
                        if (agendaRendaFixa.Taxa.HasValue && agendaRendaFixa.Taxa.Value != 0)
                        {
                            valorAmortizacao = puNominal * agendaRendaFixa.Taxa.Value / 100M;
                        }
                        else
                        {
                            valorAmortizacao = agendaRendaFixa.Valor.Value;
                        }

                        valorFluxoOriginal = valorAmortizacao;
                        valorFluxo = valorAmortizacao * this.fatorCorrecao;
                        puNominalAmortizado -= valorAmortizacao;
                    }
                    else
                    {
                        decimal valorJuro = 0;
                        if (agendaRendaFixa.Taxa.HasValue && agendaRendaFixa.Taxa.Value != 0)
                        {
                            DateTime dataBaseJuros = titulo.DataEmissao.Value;
                            #region Acha data base do último pagamento de cupom de juros
                            AgendaRendaFixaCollection agendaRendaFixaCollectionJuro = new AgendaRendaFixaCollection();
                            agendaRendaFixaCollectionJuro.Query.Select(agendaRendaFixaCollectionJuro.Query.DataEvento);
                            agendaRendaFixaCollectionJuro.Query.Where(agendaRendaFixaCollectionJuro.Query.IdTitulo.Equal(titulo.IdTitulo.Value),
                                                                      agendaRendaFixaCollectionJuro.Query.DataEvento.LessThan(dataEvento),
                                                                      agendaRendaFixaCollectionJuro.Query.TipoEvento.In((byte)TipoEventoTitulo.JurosCorrecao,
                                                                                                                        (byte)TipoEventoTitulo.Juros));
                            agendaRendaFixaCollectionJuro.Query.OrderBy(agendaRendaFixaCollectionJuro.Query.DataEvento.Descending);
                            agendaRendaFixaCollectionJuro.Query.Load();

                            if (agendaRendaFixaCollectionJuro.Count > 0)
                            {
                                dataBaseJuros = agendaRendaFixaCollectionJuro[0].DataEvento.Value;
                            }
                            #endregion

                            if (papelRendaFixa.ContagemDias.Value == (byte)ContagemDias.Uteis)
                            {
                                if (!Calendario.IsDiaUtil(dataEvento))
                                {
                                    dataEvento = Calendario.AdicionaDiaUtil(dataEvento, 1);
                                }
                            }

                            int numeroDias = 0;
                            if (papelRendaFixa.ContagemDias.Value == (byte)ContagemDias.Uteis)
                            {
                                numeroDias = Calendario.NumeroDias(dataBaseJuros, dataEvento, (int)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                            }
                            else if (papelRendaFixa.ContagemDias.Value == (byte)ContagemDias.Dias360)
                            {
                                numeroDias = Calendario.NumeroDias360(dataBaseJuros, dataEvento);
                            }
                            else
                            {
                                numeroDias = Calendario.NumeroDias(dataBaseJuros, dataEvento);
                            }

                            decimal fatorAno = agendaRendaFixa.Taxa.Value / 100M + 1;
                            decimal fatorPeriodo = (decimal)Math.Pow((double)fatorAno, (double)((double)numeroDias / (double)papelRendaFixa.BaseAno.Value));

                            valorJuro = puNominalAmortizado * (fatorPeriodo - 1M);
                        }
                        else
                        {
                            valorJuro = agendaRendaFixa.Valor.Value;
                        }

                        valorFluxoOriginal = valorJuro;
                        valorFluxo = valorJuro * this.fatorCorrecao;
                    }
                    #endregion

                    int numeroDiasDesconto = 0;
                    if (papelRendaFixa.BaseAno.Value == (int)BaseCalculo.Base252)
                    {
                        numeroDiasDesconto = diasNaoUteisContainer.CalculaNumeroDias(dataCalculo, dataEvento);
                    }
                    else if (papelRendaFixa.BaseAno.Value == (int)BaseCalculo.Base360)
                    {
                        numeroDiasDesconto = Calendario.NumeroDias360(dataCalculo, dataEvento);
                    }
                    else
                    {
                        numeroDiasDesconto = Calendario.NumeroDias(dataCalculo, dataEvento);
                    }

                    decimal fator = (decimal)Math.Pow((double)(1 + this.taxaJuros / 100M), (double)((decimal)numeroDiasDesconto / (int)papelRendaFixa.BaseAno.Value));
                    decimal valorPresente = valorFluxo / fator;

                    vpl += valorPresente;

                    ValoresFluxoDesconto valores = new ValoresFluxoDesconto();
                    valores.dataEvento = dataEvento;
                    valores.dataPagamento = dataPagamento;
                    valores.numeroDias = numeroDiasDesconto;
                    valores.valor = valorFluxoOriginal;
                    valores.valorAtual = valorPresente;
                    valores.valorCorrigido = valorFluxo;

                    this.listaValores.Add(valores);
                }

                puAtualizado = vpl;
                #endregion
            }
            else if (existeFluxoCorrecao)
            {
                return; //Hoje não há memória de cálculo para este tipo de título
            }
            else if (!this.idIndexador.HasValue)
            {
                #region PRÉ-FIXADO SEM FLUXO
                decimal puFinal = 0;
                int numeroDiasDesconto = 0;
                if (dataOperacao.HasValue && puOperacao.HasValue) //Calcula atualizando o PU de Operação
                {
                    if (papelRendaFixa.BaseAno.Value == (int)BaseCalculo.Base252)
                    {
                        numeroDiasDesconto = Calendario.NumeroDias(dataOperacao.Value, dataCalculo, (int)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                    }
                    else if (papelRendaFixa.BaseAno.Value == (int)BaseCalculo.Base360)
                    {
                        numeroDiasDesconto = Calendario.NumeroDias360(dataOperacao.Value, dataCalculo);
                    }
                    else
                    {
                        numeroDiasDesconto = Calendario.NumeroDias(dataOperacao.Value, dataCalculo);
                    }

                    decimal fator = (decimal)Math.Pow((double)(1 + this.taxaJuros / 100M), (double)((decimal)numeroDiasDesconto / (int)papelRendaFixa.BaseAno.Value));
                    puFinal = puOperacao.Value * fator;
                }
                else //Calcula descontando o PU Nominal
                {
                    if (papelRendaFixa.BaseAno.Value == (int)BaseCalculo.Base252)
                    {
                        numeroDiasDesconto = Calendario.NumeroDias(dataCalculo, dataVencimento, (int)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                    }
                    else if (papelRendaFixa.BaseAno.Value == (int)BaseCalculo.Base360)
                    {
                        numeroDiasDesconto = Calendario.NumeroDias360(dataCalculo, dataVencimento);
                    }
                    else
                    {
                        numeroDiasDesconto = Calendario.NumeroDias(dataCalculo, dataVencimento);
                    }

                    decimal fator = (decimal)Math.Pow((double)(1 + this.taxaJuros / 100M), (double)((decimal)numeroDiasDesconto / (int)papelRendaFixa.BaseAno.Value));
                    puFinal = puNominal / fator;
                }


                ValoresFluxoDesconto valores = new ValoresFluxoDesconto();
                valores.dataEvento = dataVencimento;
                valores.dataPagamento = dataVencimento;
                valores.numeroDias = numeroDiasDesconto;
                valores.valor = puNominal;
                valores.valorAtual = puFinal;
                valores.valorCorrigido = puNominal;

                this.listaValores.Add(valores);

                puAtualizado = puFinal;
                #endregion
            }
            else
            {
                decimal puBase = puOperacao.HasValue ? puOperacao.Value : titulo.PUNominal.Value;
                puAtualizado = puBase * this.fatorCorrecao;
            }
            
            this.pu = puAtualizado;

        }

    }

    public class CalculoRendaFixa
    {
        public decimal RetornaIOFPosicao(Cliente cliente, ClienteRendaFixa clienteRendaFixa, PosicaoRendaFixa posicao,
                                 DateTime data)
        {
            decimal aliquotaIOF = 0;
            return this.RetornaIOFPosicao(cliente, clienteRendaFixa, posicao, data, out aliquotaIOF);
        }

        //O fato de ter as funções replicadas se explica pela maneira como é tratado o valor atual.
        //No caso da posição, pega-se direto pelo ValorCurva, no caso da operação, computa-se PuOperacao * Quantidade
        #region Cálculos de IR/IOF
        /// <summary>
        /// Calcula o IOF, dado o prazo da aplicação do cliente.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="posicao"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public decimal RetornaIOFPosicao(Cliente cliente, ClienteRendaFixa clienteRendaFixa, PosicaoRendaFixa posicao,
                                         DateTime data, out decimal aliquotaIOF)
        {
            DateTime dataOperacao = posicao.DataOperacao.Value;
            aliquotaIOF = 0;

            decimal valorAtual = posicao.ValorMercado.Value;
            decimal valorOperacao = Utilitario.Truncate(posicao.Quantidade.Value * posicao.PUOperacao.Value, 2);

            int tipoCliente = cliente.IdTipo.Value;

            decimal iof;
            if (clienteRendaFixa.IsIsentoIOF())
                return 0;
            else
            {
                TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(tituloRendaFixa.Query.IsentoIOF);
                tituloRendaFixa.LoadByPrimaryKey(campos, posicao.IdTitulo.Value);

                if (tituloRendaFixa.IsentoIOF == (byte)TituloIsentoIOF.Isento ||
                    (tituloRendaFixa.IsentoIOF == (byte)TituloIsentoIOF.IsentoPF &&
                     tipoCliente == (byte)TipoClienteFixo.ClientePessoaFisica))
                {
                    iof = 0;
                }
                else
                {
                    TabelaIOF tabelaIOF = new TabelaIOF();
                    aliquotaIOF = tabelaIOF.RetornaAliquotaIOF(dataOperacao, data);
                    decimal rendimento = valorAtual - valorOperacao;
                    if (rendimento < 0)
                    {
                        iof = 0;
                    }
                    else
                    {
                        //iof = Utilitario.Truncate(Math.Max(valorAtual * 0.01M, rendimento * aliquotaIOF / 100M), 2);
                        aliquotaIOF = aliquotaIOF / 100M;
                        iof = Utilitario.Truncate(rendimento * aliquotaIOF, 2);
                    }
                }
            }

            return iof;
        }

        /// <summary>
        /// Calcula o IR, dado o prazo da aplicação do cliente.
        /// Abate do IOF informado da base de rendimento.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="posicao"></param>
        /// <param name="data"></param>
        /// <param name="iof">valor do IOF a ser abatido do rendimento no cálculo do IR</param>
        /// <returns></returns>
        public decimal RetornaIRPosicao(Cliente cliente, ClienteRendaFixa clienteRendaFixa, PosicaoRendaFixa posicaoRendaFixa, TituloRendaFixa tituloRendaFixa,
                                        DateTime data, decimal iof, bool vencimento, ExcecoesTributacaoIR excecao)
        {
            decimal aliquotaIR = 0;
            return RetornaIRPosicao(cliente, clienteRendaFixa, posicaoRendaFixa, tituloRendaFixa, data, iof, vencimento, excecao, out aliquotaIR);
        }

        /// <summary>
        /// Calcula o IR, dado o prazo da aplicação do cliente.
        /// Abate do IOF informado da base de rendimento.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="posicao"></param>
        /// <param name="data"></param>
        /// <param name="iof">valor do IOF a ser abatido do rendimento no cálculo do IR</param>
        /// <returns></returns>
        public decimal RetornaIRPosicao(Cliente cliente, ClienteRendaFixa clienteRendaFixa, PosicaoRendaFixa posicaoRendaFixa, TituloRendaFixa tituloRendaFixa,
                                        DateTime data, decimal iof, bool vencimento, ExcecoesTributacaoIR excecao, out decimal aliquotaIR)
        {
            decimal ir = 0;
            aliquotaIR = 0;
            bool isPosicaoCompromisso = posicaoRendaFixa.TipoOperacao.Value == (byte)TipoOperacaoTitulo.CompraRevenda;

            bool? isIsentoIR = null;
            bool regimeEspecialTributacao = !string.IsNullOrEmpty(cliente.RegimeEspecialTributacao) && cliente.RegimeEspecialTributacao.Equals("S");
            decimal aliquotaEspecial = 0;
            #region Carrega excecoes
            if (!isPosicaoCompromisso)
            {
                if (excecao == null)
                {
                    ExcecoesTributacaoIRCollection excecoesColl = new ExcecoesTributacaoIRCollection();
                    excecoesColl.RetornaExcessaoTributacaoIR(cliente.UpToPessoaByIdPessoa.Tipo, data, (int)TipoMercado.RendaFixa);
                    excecao = excecoesColl.ObtemPrioridade(cliente.UpToPessoaByIdPessoa.Tipo, (int)TipoMercado.RendaFixa, tituloRendaFixa.IdPapel.Value, tituloRendaFixa.IdTitulo.Value);
                }
                
                if (excecao.IdExcecoesTributacaoIR.HasValue && excecao.IdExcecoesTributacaoIR.Value > 0)
                {
                    isIsentoIR = (excecao.IsencaoIR.GetValueOrDefault(0) == 1);

                    if (!isIsentoIR.Value)
                    {
                        if (!string.IsNullOrEmpty(excecao.AliquotaIR))
                        {
                            aliquotaEspecial = Convert.ToDecimal(excecao.AliquotaIR) / 100;
                        }
                    }
                }
            }
            #endregion

            TituloIsentoIR tituloIsentoIR;
            if (tituloRendaFixa.IsentoIR.GetValueOrDefault(0) > 0)
                tituloIsentoIR = (TituloIsentoIR)tituloRendaFixa.IsentoIR.Value;
            else
            {
                PapelRendaFixa papel = tituloRendaFixa.UpToPapelRendaFixaByIdPapel;
                tituloIsentoIR = (TituloIsentoIR)papel.IsentoIR.Value;
            }

            #region Verifica se o cliente é isento / grossup
            bool grossup = (cliente.GrossUP.Value != (byte)GrossupCliente.NaoFaz && tituloIsentoIR != TituloIsentoIR.NaoIsento);
            if (!grossup)
            {
                if (isIsentoIR == null)
                {
                    //Nivel Titulo/Papel
                    if (tituloIsentoIR == TituloIsentoIR.Isento)
                        return 0;

                    if (tituloIsentoIR == TituloIsentoIR.IsentoPF && cliente.IdTipo.Value == (byte)TipoClienteFixo.ClientePessoaFisica
                        && cliente.GrossUP.Value != (byte)GrossupCliente.SempreFaz)
                        return 0;

                    //Nivel ClienteRendaFixa
                    if (clienteRendaFixa.IsIsentoIR())
                        return 0;

                    //Nivel Cliente
                    if (cliente.IsentoIR.Equals("S"))
                        return 0;
                }
                else
                {
                    if (isIsentoIR.Value)
                        return 0;
                }
            }
            #endregion

            DateTime dataLiquidacaoCompra = posicaoRendaFixa.DataLiquidacao.Value;
            if (data < dataLiquidacaoCompra)
            {
                dataLiquidacaoCompra = data;
            }

            decimal valorAtual = posicaoRendaFixa.ValorMercado.Value;
            decimal puOperacao = posicaoRendaFixa.PUOperacao.Value;
            
            AgendaRendaFixa agendaRendaFixa = new AgendaRendaFixa();
            if (!isPosicaoCompromisso)
            {
                //Ajusta o PU de custo pelas amortizações havidas desde a compra                
                decimal totalAmortizacao = agendaRendaFixa.RetornaTotalAmortizadoPU(tituloRendaFixa, data, posicaoRendaFixa.DataOperacao.Value);

                puOperacao = puOperacao - totalAmortizacao;
                //
            }

            decimal valorOperacao = Utilitario.Truncate(posicaoRendaFixa.Quantidade.Value * puOperacao, 2);

            bool debentureInfra = tituloRendaFixa.DebentureInfra == "S";

            decimal rendimentoFluxo = 0;
            if (!isPosicaoCompromisso && grossup)
            {
                decimal valorIRSimulado = 0;
                #region Calcula IR por grossup
                if (cliente.GrossUP.Value == (byte)GrossupCliente.ApenasResgate)
                {
                    //Calcula o fluxo de eventos (NÃO considera Amortizações, pois estas já foram ajustadas no PU de operação acima)
                    decimal rendimentoFluxoPU = agendaRendaFixa.RetornaValorFluxo(posicaoRendaFixa.DataOperacao.Value, data, posicaoRendaFixa.IdTitulo.Value, null, null, null);
                    rendimentoFluxo = Utilitario.Truncate(rendimentoFluxoPU * posicaoRendaFixa.Quantidade.Value, 2);
                }

                decimal rendimento = valorAtual + rendimentoFluxo - valorOperacao;
                rendimento -= iof;

                if (rendimento > 0)
                {
                    if (ParametrosConfiguracaoSistema.RendaFixaSwap.AliquotaUnicaGrossup)
                    {
                        aliquotaIR = 0.15M;
                        valorIRSimulado = rendimento * aliquotaIR / 0.85M; //Totalmente forçado para aliquota de 15% 
                    }
                    else
                    {
                        CalculoTributo calculoTributo = new CalculoTributo();
                        aliquotaIR = 0;

                        if (aliquotaEspecial == 0)
                            aliquotaIR = calculoTributo.RetornaAliquotaIRLongoPrazo(posicaoRendaFixa.DataOperacao.Value, data);
                        else
                            aliquotaIR = aliquotaEspecial;

                        aliquotaIR = aliquotaIR / 100M;

                        valorIRSimulado = rendimento * aliquotaIR / (1 - aliquotaIR);
                    }
                }
                #endregion
                return valorIRSimulado;
            }
            else if (cliente.IdTipo.Value == TipoClienteFixo.InvestidorEstrangeiro)
            {
                if (regimeEspecialTributacao)
                {
                    decimal rendimento = valorAtual - valorOperacao - iof;
                    if (rendimento < 0)
                        return 0;
                    else
                    {
                        if (aliquotaEspecial == 0)
                        {
                            aliquotaIR = 0.15M;
                            ir = Utilitario.Truncate(rendimento * aliquotaIR, 2);
                        }
                        else
                        {
                            aliquotaIR = aliquotaEspecial;
                            ir = Utilitario.Truncate(rendimento * aliquotaIR, 2);
                        }
                        return ir;
                    }
                }
                else
                {
                    return 0;
                }
            }
            else if (tituloIsentoIR != TituloIsentoIR.Isento) 
            {                
                #region Lei 11.033
                decimal rendaTotal = 0;
                if (tituloIsentoIR == TituloIsentoIR.IsentoCupom && cliente.IdTipo.Value == (byte)TipoClienteFixo.ClientePessoaFisica) 
                {
                    //Situação hoje usada apenas no caso de CRIs isentos nos cupons mas com IR sobre o ganho de capital em relação à curva    
                    decimal valorCurva = Utilitario.Truncate(posicaoRendaFixa.Quantidade.Value * posicaoRendaFixa.PUCurva.Value, 2); //Curva da compra
                    rendaTotal = valorAtual - valorCurva - iof;
                }
                else
                {
                    rendaTotal = valorAtual - valorOperacao - iof;
                }

                // Calculo de prazo - Se a data da opera��o for diferente da data de liquida��o, e n�o for termo:
                DateTime dataOperacao = posicaoRendaFixa.DataOperacao.Value;
                int prazoTotal;
                if (posicaoRendaFixa.DataOperacao < posicaoRendaFixa.DataLiquidacao && (string)posicaoRendaFixa.OperacaoTermo != "S")
                {
                    prazoTotal = Calendario.NumeroDias(dataOperacao, data);
                }
                // se a data da opera��o for diferente da data de liquida��o, e for termo:
                else if (posicaoRendaFixa.DataOperacao < posicaoRendaFixa.DataLiquidacao && (string)posicaoRendaFixa.OperacaoTermo == "S")
                {
                    prazoTotal = Calendario.NumeroDias(dataLiquidacaoCompra, data);
                }
                else
                {
                    prazoTotal = Calendario.NumeroDias(dataLiquidacaoCompra, data);
                }

                if (debentureInfra)
                {
                    aliquotaIR = 0.15M;
                    ir = rendaTotal * 0.15M;
                }
                else
                {
                    //Se dataLiquidacaoCompra for menor que a data da transição, precisa ser calculado o IR prorata de 20%
                    //até a data da transição (01/01/2005).
                    //Caso contrário, basta aplicar a alíquota do prazo direto em cima do rendimento - IOF.
                    DateTime dataVirada = new DateTime(2005, 01, 01);
                    if (DateTime.Compare(dataLiquidacaoCompra, dataVirada) < 0 && tituloIsentoIR != TituloIsentoIR.IsentoCupom)
                    {
                        int prazoAteDataVirada = Calendario.NumeroDias(dataLiquidacaoCompra, dataVirada);

                        decimal rendaAteDataVirada = prazoTotal > 0 ? Utilitario.Truncate(rendaTotal / prazoTotal * prazoAteDataVirada, 2) : rendaTotal;
                        decimal rendaPosDataVirada = rendaTotal - rendaAteDataVirada;

                        // Até 22/12/2004 20%
                        decimal irAteDataVirada = rendaAteDataVirada * 0.2M;

                        //Depois de 31/12/2004, de acordo com o prazo                  
                        decimal irPosDataVirada;
                        if (aliquotaEspecial == 0)
                        {
                            if (prazoTotal <= 180)                            
                                irPosDataVirada = rendaPosDataVirada * 0.225M;                            
                            else if (prazoTotal <= 360)
                                irPosDataVirada = rendaPosDataVirada * 0.2M;
                            else if (prazoTotal <= 720)
                                irPosDataVirada = rendaPosDataVirada * 0.175M;
                            else
                                irPosDataVirada = rendaPosDataVirada * 0.15M;
                        }
                        else
                        {
                            irPosDataVirada = rendaPosDataVirada * aliquotaEspecial;
                        }

                        ir = irAteDataVirada + irPosDataVirada;
                    }
                    else
                    {
                        if (aliquotaEspecial == 0)
                        {
                            if (prazoTotal <= 180)
                            {
                                aliquotaIR = 0.225M;
                                ir = rendaTotal * aliquotaIR;
                            }
                            else if (prazoTotal <= 360)
                            {
                                aliquotaIR = 0.2M;
                                ir = rendaTotal * aliquotaIR;
                            }
                            else if (prazoTotal <= 720)
                            {
                                aliquotaIR = 0.175M;
                                ir = rendaTotal * aliquotaIR;
                            }
                            else
                            {
                                aliquotaIR = 0.15M;
                                ir = rendaTotal * aliquotaIR;
                            }
                        }
                        else
                        {
                            aliquotaIR = aliquotaEspecial;
                            ir = rendaTotal * aliquotaIR;
                        }
                    }
                }
                #endregion                

                if (ir < 0)
                {
                    ir = 0;
                }
                else
                {
                    ir = Math.Round(ir, 2);
                }
            }
            return ir;
        }

        /// <summary>
        /// Calcula o IOF, dado o prazo da aplicação do cliente.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="operacao"></param>
        /// <param name="posicao"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public decimal RetornaIOFOperacao(Cliente cliente, ClienteRendaFixa clienteRendaFixa, decimal puAtual, PosicaoRendaFixa posicao,
                                          decimal quantidade, DateTime data)
        {

            DateTime dataOperacao = posicao.DataOperacao.Value;
            decimal aliquotaIOF;
            decimal puCusto = posicao.PUOperacao.Value;            

            AgendaRendaFixa agendaRendaFixa = new AgendaRendaFixa();

            decimal rendimento = Utilitario.Truncate(quantidade * (puAtual - puCusto), 2);

            int tipoCliente = cliente.IdTipo.Value;

            decimal iof;
            if (clienteRendaFixa.IsIsentoIOF())
                return 0;
            else
            {
                TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(tituloRendaFixa.Query.IsentoIOF);
                tituloRendaFixa.LoadByPrimaryKey(campos, posicao.IdTitulo.Value);

                if (tituloRendaFixa.IsentoIOF == (byte)TituloIsentoIOF.Isento ||
                    (tituloRendaFixa.IsentoIOF == (byte)TituloIsentoIOF.IsentoPF &&
                        tipoCliente == (byte)TipoClienteFixo.ClientePessoaFisica))                    
                {
                    iof = 0;
                }
                else
                {
                    TabelaIOF tabelaIOF = new TabelaIOF();
                    aliquotaIOF = tabelaIOF.RetornaAliquotaIOF(dataOperacao, data);
                    if (rendimento < 0)
                    {
                        iof = 0;
                    }
                    else
                    {
                        iof = Utilitario.Truncate(rendimento * aliquotaIOF / 100, 2);
                    }
                }
            }

            return iof;
        }

        /// <summary>
        /// Calcula o IR, dado o prazo da aplicação do cliente.
        /// Abate do IOF informado da base de rendimento.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="operacao"></param>
        /// <param name="posicao"></param>
        /// <param name="data"></param>
        /// <param name="iof">valor do IOF a ser abatido do rendimento no cálculo do IR</param>
        /// <returns></returns>
        public decimal RetornaIROperacao(Cliente cliente, ClienteRendaFixa clienteRendaFixa, OperacaoRendaFixa operacao, PosicaoRendaFixa posicaoRendaFixa,
                                         decimal quantidade, DateTime data, decimal iof, ExcecoesTributacaoIR excecao)
        {
            TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
            tituloRendaFixa.LoadByPrimaryKey(posicaoRendaFixa.IdTitulo.Value);

            bool isPosicaoCompromisso = posicaoRendaFixa.TipoOperacao.Value == (byte)TipoOperacaoTitulo.CompraRevenda;

            bool? isIsentoIR = null;
            bool regimeEspecialTributacao = !string.IsNullOrEmpty(cliente.RegimeEspecialTributacao) && cliente.RegimeEspecialTributacao.Equals("S");
            decimal aliquotaEspecial = 0;
            #region Carrega excecoes
            if (!isPosicaoCompromisso)
            {
                if (excecao == null)
                {
                    ExcecoesTributacaoIRCollection excecoesColl = new ExcecoesTributacaoIRCollection();
                    excecoesColl.RetornaExcessaoTributacaoIR(cliente.UpToPessoaByIdPessoa.Tipo, data, (int)TipoMercado.RendaFixa);
                    excecao = excecoesColl.ObtemPrioridade(cliente.UpToPessoaByIdPessoa.Tipo, (int)TipoMercado.RendaFixa, tituloRendaFixa.IdPapel.Value, tituloRendaFixa.IdTitulo.Value);
                }

                if (excecao.IdExcecoesTributacaoIR.HasValue && excecao.IdExcecoesTributacaoIR.Value > 0)
                {
                    isIsentoIR = (excecao.IsencaoIR.GetValueOrDefault(0) == 1);

                    if (!isIsentoIR.Value)
                    {
                        if (!string.IsNullOrEmpty(excecao.AliquotaIR))
                        {
                            aliquotaEspecial = Convert.ToDecimal(excecao.AliquotaIR) / 100;
                        }
                    }
                }
            }
            #endregion

            TituloIsentoIR tituloIsentoIR;
            if (tituloRendaFixa.IsentoIR.GetValueOrDefault(0) > 0)
                tituloIsentoIR = (TituloIsentoIR)tituloRendaFixa.IsentoIR.Value;
            else
            {
                PapelRendaFixa papel = tituloRendaFixa.UpToPapelRendaFixaByIdPapel;
                tituloIsentoIR = (TituloIsentoIR)papel.IsentoIR.Value;
            }

            #region Verifica se o cliente é isento / grossup
            bool grossup = (cliente.GrossUP.Value != (byte)GrossupCliente.NaoFaz && tituloIsentoIR != TituloIsentoIR.NaoIsento);
            if (!grossup)
            {
                if (isIsentoIR == null)
                {
                    //Nivel Titulo/Papel
                    if (tituloIsentoIR == TituloIsentoIR.Isento)
                        return 0;

                    if (tituloIsentoIR == TituloIsentoIR.IsentoPF && cliente.IdTipo.Value == (byte)TipoClienteFixo.ClientePessoaFisica
                        && cliente.GrossUP.Value != (byte)GrossupCliente.SempreFaz)
                        return 0;

                    //Nivel ClienteRendaFixa
                    if (clienteRendaFixa.IsIsentoIR())
                        return 0;

                    //Nivel Cliente
                    if (cliente.IsentoIR.Equals("S"))
                        return 0;
                }
                else
                {
                    if (isIsentoIR.Value)
                        return 0;
                }
            }
            #endregion

            DateTime dataLiquidacaoCompra = posicaoRendaFixa.DataLiquidacao.Value;
            if (data < dataLiquidacaoCompra)
            {
                dataLiquidacaoCompra = data;
            }

            DateTime dataLiquidacaoVenda = operacao.DataLiquidacao.Value;
            int prazoTotal = Calendario.NumeroDias(dataLiquidacaoCompra, dataLiquidacaoVenda);

            decimal puCusto = posicaoRendaFixa.PUOperacao.Value;

            decimal puOperacao = operacao.PUOperacao.Value;

            decimal ir = 0;
            
            bool debentureInfra = tituloRendaFixa.DebentureInfra == "S";

            if (grossup)
            {
                decimal rendimento = Utilitario.Truncate(quantidade * (puOperacao - puCusto), 2);

                if (cliente.GrossUP.Value == (byte)GrossupCliente.ApenasResgate)
                {
                    AgendaRendaFixa agendaRendaFixa = new AgendaRendaFixa();
                    //Calcula o fluxo de eventos (NÃO considera Amortizações, pois estas já foram ajustadas no PU de operação acima)
                    decimal rendimentoFluxoPU = agendaRendaFixa.RetornaValorFluxo(posicaoRendaFixa.DataOperacao.Value, data, posicaoRendaFixa.IdTitulo.Value, null, null, null);
                    
                    decimal rendimentoFluxo = Utilitario.Truncate(quantidade * rendimentoFluxoPU, 2);

                    rendimento += rendimentoFluxo;
                }
                rendimento -= iof;
                decimal valorIRSimulado = 0;
                if (ParametrosConfiguracaoSistema.RendaFixaSwap.AliquotaUnicaGrossup)
                {
                    valorIRSimulado = rendimento * 0.15M / 0.85M; //Totalmente forçado para aliquota de 15% 
                }
                else
                {
                    CalculoTributo calculoTributo = new CalculoTributo();
                    decimal aliquotaIR = 0;
                    if (aliquotaEspecial == 0)
                        aliquotaIR = calculoTributo.RetornaAliquotaIRLongoPrazo(posicaoRendaFixa.DataOperacao.Value, data);
                    else
                        aliquotaIR = aliquotaEspecial;

                    aliquotaIR = aliquotaIR / 100M;

                    valorIRSimulado = rendimento * aliquotaIR / (1 - aliquotaIR);
                }

                if (valorIRSimulado < 0)
                {
                    return 0;
                }

                return valorIRSimulado;
            }
            else if (cliente.IdTipo == TipoClienteFixo.InvestidorEstrangeiro)
            {
                if (regimeEspecialTributacao)
                {
                    decimal rendimento = Utilitario.Truncate(quantidade * (puOperacao - puCusto), 2);
                    if (rendimento < 0)
                        return 0;
                    else
                    {
                        if (aliquotaEspecial == 0)
                            ir = Utilitario.Truncate(rendimento * 0.15M, 2);
                        else
                            ir = Utilitario.Truncate(rendimento * aliquotaEspecial, 2);
                        return ir;
                    }
                }
                else
                {
                    return 0;
                }
            }
            else if (tituloIsentoIR != TituloIsentoIR.Isento)
            {                
                #region Lei 11.033
                decimal rendaTotal = 0;
                if (tituloIsentoIR == TituloIsentoIR.IsentoCupom && cliente.IdTipo.Value == (byte)TipoClienteFixo.ClientePessoaFisica)
                { 
                    //Situação hoje usada apenas no caso de CRIs isentos nos cupons mas com IR sobre o ganho de capital em relação à curva
                    decimal valorCurva = Utilitario.Truncate(quantidade * posicaoRendaFixa.PUCurva.Value, 2); //Curva da compra
                    decimal valorVenda = Utilitario.Truncate(quantidade * puOperacao, 2);
                    rendaTotal = valorVenda - valorCurva - iof;
                }
                else
                {
                    rendaTotal = Utilitario.Truncate(quantidade * (puOperacao - puCusto), 2);
                    rendaTotal -= iof;
                }

                if (debentureInfra)
                {
                    ir = rendaTotal * 0.15M;
                }
                else
                {
                    //Se dataOperacao for menor que a data da transição, precisa ser calculado o IR prorata de 20%
                    //até a data da transição (01/01/2005).
                    //Caso contrário, basta aplicar a alíquota do prazo direto em cima do rendimento - IOF.
                    DateTime dataVirada = new DateTime(2005, 01, 01);
                    if (DateTime.Compare(dataLiquidacaoCompra, dataVirada) < 0 && tituloIsentoIR != TituloIsentoIR.IsentoCupom)
                    {
                        int prazoAteDataVirada = Calendario.NumeroDias(dataLiquidacaoCompra, dataVirada);

                        decimal rendaAteDataVirada = Utilitario.Truncate(rendaTotal / prazoTotal * prazoAteDataVirada, 2);
                        decimal rendaPosDataVirada = rendaTotal - rendaAteDataVirada;

                        // Até 22/12/2004 20%
                        decimal irAteDataVirada = rendaAteDataVirada * 0.2M;

                        //Depois de 31/12/2004, de acordo com o prazo                  
                        decimal irPosDataVirada;
                        if (aliquotaEspecial == 0)
                        {
                            if (prazoTotal <= 180)
                                irPosDataVirada = rendaPosDataVirada * 0.225M;
                            else if (prazoTotal <= 360)
                                irPosDataVirada = rendaPosDataVirada * 0.2M;
                            else if (prazoTotal <= 720)
                                irPosDataVirada = rendaPosDataVirada * 0.175M;
                            else
                                irPosDataVirada = rendaPosDataVirada * 0.15M;
                        }
                        else
                        {
                            irPosDataVirada = rendaPosDataVirada * aliquotaEspecial;
                        }

                        ir = irAteDataVirada + irPosDataVirada;
                    }
                    else
                    {
                        if (aliquotaEspecial == 0)
                        {
                            if (prazoTotal <= 180)
                                ir = rendaTotal * 0.225M;
                            else if (prazoTotal <= 360)
                                ir = rendaTotal * 0.2M;
                            else if (prazoTotal <= 720)
                                ir = rendaTotal * 0.175M;
                            else
                                ir = rendaTotal * 0.15M;
                        }
                        else
                        {
                            ir = rendaTotal * aliquotaEspecial;
                        }
                    }
                }
                #endregion                
            }

            if (ir < 0)
            {
                ir = 0;
            }
            else
            {
                ir = Math.Round(ir, 2);
            }

            return ir;
        }

        /// <summary>
        /// Calcula o IR sobre juros periódicos (cupons), dado o prazo da aplicação do cliente.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="clienteRendaFixa"></param>
        /// <param name="posicao"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public decimal RetornaIRJuros(Cliente cliente, ClienteRendaFixa clienteRendaFixa, PosicaoRendaFixa posicao, DateTime data)
        {
            DateTime dataLiquidacaoCompra = posicao.DataLiquidacao.Value;
            decimal valorJuros = posicao.ValorJuros.Value;

            int tipoCliente = cliente.IdTipo.Value;

            decimal ir;
            if (clienteRendaFixa.IsIsentoIR())
                return 0;

            #region Cliente Estrangeiro
            else if (cliente.IdTipo == TipoClienteFixo.InvestidorEstrangeiro)
            {
                ir = Utilitario.Truncate(valorJuros * 0.15M, 2);
                return ir;
            }
            #endregion

            #region Lei 11.033
            else
            {
                int prazoTotal = Calendario.NumeroDias(dataLiquidacaoCompra, data);

                if (prazoTotal <= 180)
                    ir = valorJuros * 0.225M;
                else if (prazoTotal <= 360)
                    ir = valorJuros * 0.2M;
                else if (prazoTotal <= 720)
                    ir = valorJuros * 0.175M;
                else
                    ir = valorJuros * 0.15M;

            }
            #endregion

            if (ir < 0)
                ir = 0;
            else
                ir = Math.Round(ir, 2);

            return ir;
        }

        /// <summary>
        /// Calcula o IR sobre valor passado.
        /// </summary>
        /// <param name="cliente"></param>
        /// <param name="clienteRendaFixa"></param>
        /// <param name="posicao"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public decimal RetornaIRCalculado(Cliente cliente, ClienteRendaFixa clienteRendaFixa, DateTime dataOperacao, DateTime dataCalculo, 
                                          decimal rendimento)
        {
            int tipoCliente = cliente.IdTipo.Value;

            decimal ir;
            if (clienteRendaFixa.IsIsentoIR())
                return 0;

            #region Cliente Estrangeiro
            else if (cliente.IdTipo == TipoClienteFixo.InvestidorEstrangeiro)
            {
                ir = Utilitario.Truncate(rendimento * 0.15M, 2);
                return ir;
            }
            #endregion

            #region Lei 11.033
            else
            {
                int prazoTotal = Calendario.NumeroDias(dataOperacao, dataCalculo);

                if (prazoTotal <= 180)
                    ir = rendimento * 0.225M;
                else if (prazoTotal <= 360)
                    ir = rendimento * 0.2M;
                else if (prazoTotal <= 720)
                    ir = rendimento * 0.175M;
                else
                    ir = rendimento * 0.15M;

            }
            #endregion

            if (ir < 0)
                ir = 0;
            else
                ir = Math.Round(ir, 2);

            return ir;
        }
        #endregion

        #region Cálculo de curva de compromisso
        /// <summary>
        /// Calcula a curva da posição compromissada pre-fixada.
        /// </summary>
        /// <param name="posicao"></param>
        /// <param name="data"></param>
        /// <returns>pu atualizado na data</returns>
        public decimal RetornaCurvaCompromissadaPre(PosicaoRendaFixa posicao, DateTime data)
        {
            DateTime dataOperacao = posicao.DataOperacao.Value;
            DateTime dataVolta = posicao.DataVolta.Value;
            decimal puVolta = posicao.PUVolta.Value;
            decimal puOperacao = posicao.PUOperacao.Value;

            if (data == dataVolta) //Se próprio dia da volta, retorna o PU da volta
                return posicao.PUVolta.Value;
            else
            {
                decimal fatorTaxa = puVolta / puOperacao;

                int numeroDiasNumerador = Calendario.NumeroDias(dataOperacao, data, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil); ;
                int numeroDiasBase = Calendario.NumeroDias(dataOperacao, dataVolta, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil); ;

                decimal fatorJuros = Math.Round((decimal)Math.Pow((double)fatorTaxa, (double)numeroDiasNumerador / numeroDiasBase), 9);

                decimal puAtualizado = Math.Round(puOperacao * fatorJuros, 8);

                return puAtualizado;

            }
        }

        /// <summary>
        /// Calcula a curva da posição compromissada pos-fixada.
        /// </summary>
        /// <param name="posicao"></param>
        /// <param name="data"></param>
        /// <returns>pu atualizado na data</returns>
        public decimal RetornaCurvaCompromissadaPos(PosicaoRendaFixa posicao, DateTime data)
        {
            DateTime dataOperacao = posicao.DataOperacao.Value;
            decimal puOperacao = posicao.PUOperacao.Value;

            decimal percentual = 100;
            if (posicao.TaxaVolta.HasValue && posicao.TaxaVolta.Value != 0)
            {
                percentual = posicao.TaxaVolta.Value;
            }

            //********************************************ARTIFICIO PARA PODER USAR A FUNCAO RetornaFatorCorrecao
            PapelRendaFixa papel = new PapelRendaFixa();
            TituloRendaFixa titulo = new TituloRendaFixa();

            papel.ContagemDias = (byte)ContagemDias.Uteis; //FORÇA DIAS UTEIS NO CASO DA CONTAGEM DE DIAS PARA COMPROMISSADA POS-FIXADA
            titulo.IdIndice = posicao.IdIndiceVolta.Value;
            titulo.Percentual = percentual;
            titulo.DataVencimento = posicao.DataVolta.Value;
            //***************************************************************************************************

            Indice indice = new Indice();
            indice.LoadByPrimaryKey(posicao.IdIndiceVolta.Value);

            decimal percentualIndice = 0;
            decimal fatorIndice = 1;
            if (data != dataOperacao)
            {
                fatorIndice = this.CalculaFatorCorrecao(data, dataOperacao, papel, titulo, indice, percentual);
            }

            decimal puAtualizado = Utilitario.Truncate(puOperacao * fatorIndice, 8);            
            
            return puAtualizado;
        }
        #endregion

        #region Cálculos de curvas de posições finais
        /// <summary>
        /// Calcula correção monetária para qq indice.
        /// </summary>
        /// <param name="dataCalculo"></param>
        /// <param name="dataBase"></param>
        /// <param name="papelRendaFixa"></param>
        /// <param name="tituloRendaFixa"></param>
        /// <param name="posicaoRendaFixa"></param>
        /// <param name="indice"></param>
        /// <param name="taxa"></param>
        /// <returns></returns>
        public decimal RetornaFatorCorrecao(DateTime dataCalculo, DateTime dataBase, PapelRendaFixa papelRendaFixa,
                                            TituloRendaFixa tituloRendaFixa, Indice indice)
        {
            decimal taxa = 0;
            return this.RetornaFatorCorrecao(dataCalculo, dataBase, papelRendaFixa, tituloRendaFixa, indice, false, ref taxa);
        }

        /// <summary>
        /// Calcula correção monetária para qq indice.
        /// </summary>
        /// <param name="dataCalculo"></param>
        /// <param name="dataBase"></param>
        /// <param name="papelRendaFixa"></param>
        /// <param name="tituloRendaFixa"></param>
        /// <param name="posicaoRendaFixa"></param>
        /// <param name="indice"></param>
        /// <param name="taxa"></param>
        /// <returns></returns>
        public decimal RetornaFatorCorrecao(DateTime dataCalculo, DateTime dataBase, PapelRendaFixa papelRendaFixa,
                                            TituloRendaFixa tituloRendaFixa, Indice indice, bool forcaDataBase)
        {
            decimal taxa = 0;
            return this.RetornaFatorCorrecao(dataCalculo, dataBase, papelRendaFixa, tituloRendaFixa, indice, forcaDataBase, ref taxa);
        }

        /// <summary>
        /// Calcula correção monetária para qq indice.
        /// </summary>
        /// <param name="dataCalculo"></param>
        /// <param name="dataBase"></param>
        /// <param name="papelRendaFixa"></param>
        /// <param name="tituloRendaFixa"></param>
        /// <param name="posicaoRendaFixa"></param>
        /// <param name="indice"></param>
        /// <param name="taxa"></param>
        /// <returns></returns>
        public decimal RetornaFatorCorrecao(DateTime dataCalculo, DateTime dataBase, PapelRendaFixa papelRendaFixa,
                                            TituloRendaFixa tituloRendaFixa, Indice indice, bool forcaDataBase, ref decimal taxaOperacao)
        {
            #region Informações do Papel, Titulo
            int? idIndice = tituloRendaFixa.IdIndice;

            decimal percentual = 100;
            if (tituloRendaFixa.Percentual.HasValue)
            {
                percentual = tituloRendaFixa.Percentual.Value;
            }
            int contagemDias = papelRendaFixa.ContagemDias.Value;
            #endregion
            
            decimal fatorIndice = 1;
            if (idIndice.HasValue)
            {
                byte tipoIndice = indice.Tipo.Value;

                if (!forcaDataBase)
                {
                    AgendaRendaFixaCollection agendaRendaFixaCollection = new AgendaRendaFixaCollection();
                    agendaRendaFixaCollection.Query.Select(agendaRendaFixaCollection.Query.DataEvento);

                    agendaRendaFixaCollection.Query.Where(agendaRendaFixaCollection.Query.IdTitulo.Equal(tituloRendaFixa.IdTitulo.Value) &&
                                                          agendaRendaFixaCollection.Query.DataPagamento.LessThan(dataCalculo) &&
                                                         (agendaRendaFixaCollection.Query.TipoEvento.Equal((byte)TipoEventoTitulo.JurosCorrecao) ||
                                                        (agendaRendaFixaCollection.Query.TipoEvento.Equal((byte)TipoEventoTitulo.PagamentoCorrecao) &&
                                                        (agendaRendaFixaCollection.Query.Taxa == 0 || agendaRendaFixaCollection.Query.Taxa == 100))));
                    agendaRendaFixaCollection.Query.OrderBy(agendaRendaFixaCollection.Query.DataEvento.Descending);
                    agendaRendaFixaCollection.Query.Load();

                    if (agendaRendaFixaCollection.Count > 0)
                    {
                        dataBase = agendaRendaFixaCollection[0].DataEvento.Value;
                    }
                }

                if (tipoIndice == (int)TipoIndice.Percentual && indice.IdIndice.Value != (byte)ListaIndiceFixo.TR)
                {
                    TimeSpan numeroDias = dataCalculo - dataBase;
                    TabelaEscalonamentoRFCollection tabelaEscalonamentoRFCollection = new TabelaEscalonamentoRFCollection();
                    tabelaEscalonamentoRFCollection.Query.Select(tabelaEscalonamentoRFCollection.Query.Taxa);
                    tabelaEscalonamentoRFCollection.Query.Where(tabelaEscalonamentoRFCollection.Query.IdTitulo.Equal(tituloRendaFixa.IdTitulo.Value),
                                                      tabelaEscalonamentoRFCollection.Query.Prazo.LessThanOrEqual(numeroDias.Days));
                    tabelaEscalonamentoRFCollection.Query.OrderBy(tabelaEscalonamentoRFCollection.Query.Prazo.Descending);
                    tabelaEscalonamentoRFCollection.Query.Load();

                    if (tabelaEscalonamentoRFCollection.Count > 0)
                    {
                        percentual = tabelaEscalonamentoRFCollection[0].Taxa.Value;
                    }
                }

                fatorIndice = this.CalculaFatorCorrecao(dataCalculo, dataBase, papelRendaFixa, tituloRendaFixa, indice, percentual);                
            }

            taxaOperacao = percentual;

            return fatorIndice;
        }

        /// <summary>
        /// Calcula correção monetária para qq indice.
        /// </summary>
        /// <param name="dataCalculo"></param>
        /// <param name="dataBase"></param>
        /// <param name="papelRendaFixa"></param>
        /// <param name="tituloRendaFixa"></param>
        /// <param name="posicaoRendaFixa"></param>
        /// <param name="indice"></param>
        /// <param name="taxa"></param>
        /// <returns></returns>
        public decimal CalculaFatorCorrecao(DateTime dataCalculo, DateTime dataBase, PapelRendaFixa papelRendaFixa,
                                            TituloRendaFixa tituloRendaFixa, Indice indice, decimal percentual)
        {
            int idIndice = tituloRendaFixa.IdIndice.Value;            
            int contagemDias = papelRendaFixa.ContagemDias.Value;
            
            decimal fatorIndice = 1;
            
            byte tipoIndice = indice.Tipo.Value;
            if (indice.IdIndiceBase.HasValue)
            {
                idIndice = indice.IdIndiceBase.Value;
            }

            DateTime dataVencimento = tituloRendaFixa.DataVencimento.Value;
            if (tipoIndice == (byte)TipoIndice.Decimal &&
                    (indice.TipoDivulgacao.Value == (byte)TipoDivulgacaoIndice.Mensal ||
                     indice.TipoDivulgacao.Value == (byte)TipoDivulgacaoIndice.Mensal_1 ||
                     indice.TipoDivulgacao.Value == (byte)TipoDivulgacaoIndice.Mensal_2 ||
                     indice.TipoDivulgacao.Value == (byte)TipoDivulgacaoIndice.Mensal_3))
            {
                if (contagemDias == (byte)ContagemDias.Corridos || contagemDias == (byte)ContagemDias.Dias360)
                {
                    fatorIndice = CalculoFinanceiro.CalculaFatorIndicePreco(dataBase, dataCalculo, dataVencimento.Day, indice, percentual);
                }
                else
                {
                    fatorIndice = CalculoFinanceiro.CalculaFatorIndicePreco(dataBase, dataCalculo, dataVencimento.Day,
                                                            indice, percentual, (int)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                }
            }
            else
            {
                if (tipoIndice == (int)TipoIndice.Decimal)
                {
                    fatorIndice = CalculoFinanceiro.CalculaFatorIndiceDecimal(dataBase, dataCalculo, idIndice, percentual);
                }
                else if (indice.IdIndice.Value == (byte)ListaIndiceFixo.TR)
                {
                    fatorIndice = CalculoFinanceiro.CalculaFatorTR(dataBase, dataCalculo, dataVencimento.Day, percentual);
                }
                else
                {
                    //Para índices percentuais, não leva em conta o próprio dia de cálculo
                    DateTime dataFimParam = dataCalculo;

                    if (dataBase != dataCalculo)
                    {
                        dataFimParam = Calendario.SubtraiDiaUtil(dataCalculo, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                    }
                    //
                    if (papelRendaFixa.Classe == (int)ClasseRendaFixa.PosFixado_Truncado)
                    {
                        fatorIndice = CalculoFinanceiro.CalculaFatorIndicePercentual(dataBase, dataFimParam, indice, percentual, true);
                    }
                    else
                    {
                        fatorIndice = CalculoFinanceiro.CalculaFatorIndicePercentual(dataBase, dataFimParam, indice, percentual, false);
                    }
                }
            }            

            return fatorIndice;
        }

        /// <summary>
        /// Calcula a curva para pré, exponencial ou linear, dias úteis ou corridos. 
        /// Faz o cálculo descontando o PU nominal pelo fator da taxa.
        /// </summary>
        /// <param name="papel"></param>
        /// <param name="titulo"></param>
        /// <param name="posicao"></param>
        /// <param name="dataAnterior"></param>
        /// <param name="data"></param>
        /// <returns>pu atualizado na data</returns>
        public decimal RetornaCurvaPreDescontado(DateTime dataCalculo, DateTime dataAnterior, PapelRendaFixa papel, TituloRendaFixa titulo,
                                                 PosicaoRendaFixa posicao, decimal taxa)
        {
            decimal puNominal = titulo.PUNominal.Value;
            decimal puOperacao = posicao.PUOperacao.Value;
            DateTime dataEmissao = titulo.DataEmissao.Value;
            DateTime dataVencimento = titulo.DataVencimento.Value;

            //Na data de vencimento, vence com o PU Nominal
            if (dataCalculo >= dataVencimento)
                return puNominal;
            //

            int casasDecimaisPU = papel.CasasDecimaisPU.Value;
            int contagemDias = papel.ContagemDias.Value;
            byte tipoCurva = papel.TipoCurva.Value;

            decimal puAtualizado = 0;

            decimal fatorTaxa = 1;
            if (taxa == 0)
            {
                return posicao.PUCurva.Value;
            }            

            #region Calcula o fator da taxa pela taxa ano informada na operação, desconta o PU Nominal pelo fator
            int numeroDias;
            if (contagemDias == (int)ContagemDias.Corridos)
            {
                numeroDias = Calendario.NumeroDias(dataCalculo, dataVencimento);
            }
            else if (contagemDias == (int)ContagemDias.Dias360)
            {
                numeroDias = Calendario.NumeroDias360(dataCalculo, dataVencimento);
            }
            else
            {
                if (!Calendario.IsDiaUtil(dataVencimento))
                {
                    dataVencimento = Calendario.AdicionaDiaUtil(dataVencimento, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                }
                numeroDias = Calendario.NumeroDias(dataCalculo, dataVencimento, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            }

            if (tipoCurva == (byte)TipoCurvaTitulo.Exponencial)
            {
                fatorTaxa = (decimal)Math.Pow((double)(taxa / 100M + 1), (double)((decimal)numeroDias / papel.BaseAno.Value));
            }
            else
            {
                fatorTaxa = ((taxa / 100M) * numeroDias / papel.BaseAno.Value) + 1M;
            }

            puAtualizado = Math.Round(puNominal / fatorTaxa, casasDecimaisPU);
            #endregion
            
            return puAtualizado;
        }

        /// <summary>
        /// Calcula a curva para pré, exponencial ou linear, dias úteis ou corridos. 
        /// Faz o cálculo atualizando o PU nominal pelo fator da taxa.
        /// </summary>
        /// <param name="papel"></param>
        /// <param name="titulo"></param>
        /// <param name="posicao"></param>
        /// <param name="dataAnterior"></param>
        /// <param name="data"></param>
        /// <returns>pu atualizado na data</returns>
        public decimal RetornaCurvaPreAtualizado(DateTime dataCalculo, DateTime dataAnterior, PapelRendaFixa papel, TituloRendaFixa titulo,
                                                 PosicaoRendaFixa posicao, decimal taxa)
        {
            decimal puOperacao = posicao.PUOperacao.Value;
            DateTime dataOperacao = posicao.DataOperacao.Value;
            
            int casasDecimaisPU = papel.CasasDecimaisPU.Value;
            int contagemDias = papel.ContagemDias.Value;
            byte tipoCurva = papel.TipoCurva.Value;
            
            decimal puAtualizado = 0;

            decimal fatorTaxa = 1;
            if (taxa == 0)
            {
                return posicao.PUCurva.Value;
            }

            #region Calcula o fator da taxa pela taxa ano informada na operação, atualiza o PU Nominal pelo fator
            int numeroDias;
            if (contagemDias == (int)ContagemDias.Corridos)
            {
                numeroDias = Calendario.NumeroDias(dataOperacao, dataCalculo);
            }
            else if (contagemDias == (int)ContagemDias.Dias360)
            {
                numeroDias = Calendario.NumeroDias360(dataOperacao, dataCalculo);
            }
            else
            {
                numeroDias = Calendario.NumeroDias(dataOperacao, dataCalculo, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            }

            if (tipoCurva == (byte)TipoCurvaTitulo.Exponencial)
            {
                fatorTaxa = (decimal)Math.Pow((double)(taxa / 100M + 1), (double)((decimal)numeroDias / papel.BaseAno.Value));
            }
            else
            {
                fatorTaxa = ((taxa / 100M) * numeroDias / papel.BaseAno.Value) + 1M;
            }

            puAtualizado = Math.Round(puOperacao * fatorTaxa, casasDecimaisPU);
            #endregion

            return puAtualizado;
        }
        
        /// <summary>
        /// Calcula a curva para CDBs e debêntures pós-fixados, vinculados a qq índice, 
        /// com ou sem spread, com ou sem percentual modificador.
        /// Faz o prorata de eventual ágio/deságio embutido na compra do título.
        /// Trata para dias úteis ou corridos.
        /// </summary>
        /// <param name="papel"></param>
        /// <param name="titulo"></param>
        /// <param name="posicao"></param>
        /// <param name="dataAnterior"></param>
        /// <param name="data"></param>
        /// <param name="taxaOperacao">percentual da operação</param>
        /// <returns>pu atualizado na data</returns>
        public PosicaoRendaFixa.PosicaoAtualizada RetornaCurvaCETIP_Pos(DateTime dataCalculo, DateTime dataAnterior, PapelRendaFixa papel, TituloRendaFixa titulo,
                                            PosicaoRendaFixa posicao, Indice indice, ref decimal taxa)
        {
            #region Variáveis iniciais de titulo, papel e posição
            int idTitulo = titulo.IdTitulo.Value;
            DateTime dataEmissao = titulo.DataEmissao.Value;            
            DateTime dataVencimento = titulo.DataVencimento.Value;
            DateTime dataOperacao = posicao.DataOperacao.Value;
            decimal puOperacao = posicao.PUOperacao.Value;
            decimal puEmissao = titulo.PUNominal.Value;

            DateTime dataBase = new DateTime();
            if (dataCalculo >= dataVencimento)
            {
                dataBase = dataEmissao;                
            }
            else
            {
                dataBase = dataOperacao;
            }

            decimal puBase = 0;
            if (dataCalculo >= dataVencimento)
            {
                puBase = puEmissao;                
            }
            else
            {
                puBase = puOperacao;
            }

            decimal percentual = 0;
            if (titulo.Percentual.HasValue)
                percentual = titulo.Percentual.Value;

            int casasDecimaisPU = papel.CasasDecimaisPU.Value;
            byte contagemDias = papel.ContagemDias.Value;
            int baseAno = papel.BaseAno.Value;            
            #endregion

            #region Busca último pagamento correção
            AgendaRendaFixaCollection agendaRendaFixaCollectionCorrecao = new AgendaRendaFixaCollection();
            agendaRendaFixaCollectionCorrecao.Query.Select(agendaRendaFixaCollectionCorrecao.Query.DataEvento);
            agendaRendaFixaCollectionCorrecao.Query.Where(agendaRendaFixaCollectionCorrecao.Query.IdTitulo.Equal(idTitulo) &
                                                          agendaRendaFixaCollectionCorrecao.Query.DataEvento.LessThanOrEqual(dataCalculo) &
                                                          (agendaRendaFixaCollectionCorrecao.Query.TipoEvento.Equal((byte)TipoEventoTitulo.JurosCorrecao) |
                                                            (agendaRendaFixaCollectionCorrecao.Query.TipoEvento.Equal((byte)TipoEventoTitulo.PagamentoCorrecao) &
                                                             agendaRendaFixaCollectionCorrecao.Query.Taxa.In(0, 100)
                                                            )
                                                          ));
            agendaRendaFixaCollectionCorrecao.Query.OrderBy(agendaRendaFixaCollectionCorrecao.Query.DataEvento.Descending);
            agendaRendaFixaCollectionCorrecao.Query.Load();

            if (agendaRendaFixaCollectionCorrecao.Count > 0)
            {
                dataBase = agendaRendaFixaCollectionCorrecao[0].DataEvento.Value;
            }
            #endregion

            decimal percentualIndice = 0;
            decimal fatorIndice = 1;
            if (dataCalculo != dataBase)
            {
                fatorIndice = this.RetornaFatorCorrecao(dataCalculo, dataBase, papel, titulo, indice, false, ref percentualIndice);
            }
            
            decimal puAtualizado = Utilitario.Truncate(puBase * fatorIndice, 8);

            #region Cálculo do fator de juros
            int numeroDiasNumerador;
            int numeroDiasOperacao;

            if (papel.PagamentoJuros == (byte)PagamentoJurosTitulo.ContaDiasCorridos)
            {
                numeroDiasNumerador = Calendario.NumeroDias(dataAnterior, dataCalculo);
            }
            else
            {
                numeroDiasNumerador = 1;
            }

            if (contagemDias == (byte)ContagemDias.Corridos)
            {
                numeroDiasOperacao = Calendario.NumeroDias(dataBase, dataCalculo);
                
            }
            else if (contagemDias == (byte)ContagemDias.Dias360)
            {
                numeroDiasOperacao = Calendario.NumeroDias360(dataBase, dataCalculo);                
            }
            else
            {
                numeroDiasOperacao = Calendario.NumeroDias(dataBase, dataCalculo, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);                
            }

            decimal fatorJuros = (decimal)Math.Pow((double)(1 + taxa / 100M), (double)numeroDiasNumerador / baseAno);
            fatorJuros = (decimal)Math.Pow((double)fatorJuros, (double)numeroDiasOperacao);

            puAtualizado = Math.Round(puAtualizado * fatorJuros, casasDecimaisPU);
            #endregion

            PosicaoRendaFixa.PosicaoAtualizada posicaoAtualizada = new PosicaoRendaFixa.PosicaoAtualizada();
            posicaoAtualizada.PuAtualizado = puAtualizado;
            posicaoAtualizada.PuCorrecao = puAtualizado;
            posicaoAtualizada.PuJuros = puAtualizado;

            if (taxa == 0) //Se a taxa pre não for passada, a taxa da operação é a taxa do % do indice
            {
                taxa = percentualIndice;
            }

            return posicaoAtualizada;
        }

        /// <summary>
        /// Calcula o PU relativo ao desconto dos fluxos passados, seja com taxa ou com valor já informado.
        /// </summary>
        /// <param name="posicaoRendaFixa"></param>
        /// <param name="agendaRendaFixaCollection"></param>
        /// <param name="taxa"></param>
        /// <returns></returns>
        public PosicaoRendaFixa.PosicaoAtualizada CalculaPUFluxoDesconto(DateTime dataCalculo, PapelRendaFixa papelRendaFixa, TituloRendaFixa tituloRendaFixa,
                                      PosicaoRendaFixa posicaoRendaFixa, Indice indice, decimal taxaDesconto)
        {
            List<PricingDiario.ValoresFluxoDesconto> listaFluxo = new List<PricingDiario.ValoresFluxoDesconto>();
            decimal fatorCorrecao = 0;
            return this.CalculaPUFluxoDesconto(dataCalculo, papelRendaFixa, tituloRendaFixa, posicaoRendaFixa, indice, taxaDesconto, out listaFluxo);
        }

        /// <summary>
        /// Calcula o PU relativo ao desconto dos fluxos passados, seja com taxa ou com valor já informado.
        /// </summary>
        /// <param name="posicaoRendaFixa"></param>
        /// <param name="agendaRendaFixaCollection"></param>
        /// <param name="taxa"></param>
        /// <returns></returns>
        public PosicaoRendaFixa.PosicaoAtualizada CalculaPUFluxoDesconto(DateTime dataCalculo, PapelRendaFixa papelRendaFixa, TituloRendaFixa tituloRendaFixa, PosicaoRendaFixa posicaoRendaFixa, 
                                        Indice indice, decimal taxaDesconto, out List<PricingDiario.ValoresFluxoDesconto> listaFluxo)
        {
            listaFluxo = new List<PricingDiario.ValoresFluxoDesconto>();

            #region Informações do Papel, Titulo
            int idTitulo = tituloRendaFixa.IdTitulo.Value;
            int? idIndice = tituloRendaFixa.IdIndice;

            decimal percentual = 100;
            if (tituloRendaFixa.Percentual.HasValue)
            {
                percentual = tituloRendaFixa.Percentual.Value;
            }
            int baseCalculo = papelRendaFixa.BaseAno.Value;
            int contagemDias = papelRendaFixa.ContagemDias.Value;
            int tipoCurva = papelRendaFixa.TipoCurva.Value;
            decimal puNominal = tituloRendaFixa.PUNominal.Value;
            #endregion

            DateTime dataBaseCorrecao = new DateTime();
            #region Acha data base de correção do indice
            dataBaseCorrecao = tituloRendaFixa.DataEmissao.Value;
            AgendaRendaFixaCollection agendaRendaFixaCollection = new AgendaRendaFixaCollection();
            agendaRendaFixaCollection.Query.Select(agendaRendaFixaCollection.Query.DataEvento);
            agendaRendaFixaCollection.Query.Where(agendaRendaFixaCollection.Query.IdTitulo.Equal(idTitulo) &
                                                  agendaRendaFixaCollection.Query.DataEvento.LessThanOrEqual(dataCalculo) &
                                                  (agendaRendaFixaCollection.Query.TipoEvento.Equal((byte)TipoEventoTitulo.JurosCorrecao) |
                                                      (agendaRendaFixaCollection.Query.TipoEvento.Equal((byte)TipoEventoTitulo.PagamentoCorrecao) &
                                                       agendaRendaFixaCollection.Query.Taxa.In(0, 100)
                                                       )
                                                   ));
            agendaRendaFixaCollection.Query.OrderBy(agendaRendaFixaCollection.Query.DataEvento.Descending);
            agendaRendaFixaCollection.Query.Load();

            if (agendaRendaFixaCollection.Count > 0)
            {
                dataBaseCorrecao = agendaRendaFixaCollection[0].DataEvento.Value;
            }
            #endregion
            
            decimal fatorIndice = 1;
            if (dataCalculo != dataBaseCorrecao)
            {
                fatorIndice = this.RetornaFatorCorrecao(dataCalculo, dataBaseCorrecao, papelRendaFixa, tituloRendaFixa, indice, true);
            }

            bool abertura = false;
            Carteira carteira = new Carteira();
            if (carteira.LoadByPrimaryKey(posicaoRendaFixa.IdCliente.Value))
            {
                abertura = carteira.TipoCota.Value == (byte)TipoCotaFundo.Abertura;
            }

            agendaRendaFixaCollection = new AgendaRendaFixaCollection();
            agendaRendaFixaCollection.Query.Select(agendaRendaFixaCollection.Query.DataEvento,
                                                   agendaRendaFixaCollection.Query.DataPagamento,
                                                   agendaRendaFixaCollection.Query.Taxa,
                                                   agendaRendaFixaCollection.Query.Valor,
                                                   agendaRendaFixaCollection.Query.TipoEvento);
            agendaRendaFixaCollection.Query.Where(agendaRendaFixaCollection.Query.IdTitulo.Equal(posicaoRendaFixa.IdTitulo.Value),
                                                  agendaRendaFixaCollection.Query.TipoEvento.In((byte)TipoEventoTitulo.Juros,
                                                                                                (byte)TipoEventoTitulo.JurosCorrecao,
                                                                                                (byte)TipoEventoTitulo.Amortizacao,
                                                                                                (byte)TipoEventoTitulo.AmortizacaoCorrigida,
                                                                                                (byte)TipoEventoTitulo.PagamentoPrincipal,
                                                                                                (byte)TipoEventoTitulo.PagamentoPU));

            if (abertura)
            {
                agendaRendaFixaCollection.Query.Where(agendaRendaFixaCollection.Query.DataEvento.GreaterThanOrEqual(dataCalculo));
            }
            else
            {
                agendaRendaFixaCollection.Query.Where(agendaRendaFixaCollection.Query.DataEvento.GreaterThan(dataCalculo));
            }

            agendaRendaFixaCollection.Query.OrderBy(agendaRendaFixaCollection.Query.DataEvento.Ascending, 
                                                    agendaRendaFixaCollection.Query.IdAgenda.Ascending);
            agendaRendaFixaCollection.Query.Load();

            decimal puNominalAmortizado = puNominal;
            #region Ajusta o PU nominal pelas amortizações ocorridas
            AgendaRendaFixaCollection agendaRendaFixaCollectionAmortizacao = new AgendaRendaFixaCollection();
            agendaRendaFixaCollectionAmortizacao.Query.Select(agendaRendaFixaCollectionAmortizacao.Query.Taxa,
                                                              agendaRendaFixaCollectionAmortizacao.Query.Valor);
            agendaRendaFixaCollectionAmortizacao.Query.Where(agendaRendaFixaCollectionAmortizacao.Query.IdTitulo.Equal(idTitulo),
                                                  agendaRendaFixaCollectionAmortizacao.Query.DataPagamento.LessThanOrEqual(dataCalculo),
                                                  agendaRendaFixaCollectionAmortizacao.Query.TipoEvento.In((byte)TipoEventoTitulo.Amortizacao));
            agendaRendaFixaCollectionAmortizacao.Query.Load();

            foreach (AgendaRendaFixa agendaRendaFixaAmortizacao in agendaRendaFixaCollectionAmortizacao)
            {
                if (agendaRendaFixaAmortizacao.Taxa.HasValue && agendaRendaFixaAmortizacao.Taxa.Value != 0)
                {
                    decimal taxaAmortizacao = agendaRendaFixaAmortizacao.Taxa.Value;
                    puNominalAmortizado -= tituloRendaFixa.PUNominal.Value * taxaAmortizacao / 100M;
                }
                else if (agendaRendaFixaAmortizacao.Valor.HasValue && agendaRendaFixaAmortizacao.Valor.Value != 0)
                {
                    puNominalAmortizado -= agendaRendaFixaAmortizacao.Valor.Value; ;
                }
            }
            #endregion

            decimal totalFluxoAtualizado = 0;
            List<DateTime> listaDatas = new List<DateTime>();
            List<decimal> listaValores = new List<decimal>();
            foreach (AgendaRendaFixa agendaRendaFixa in agendaRendaFixaCollection)
            {
                DateTime dataEvento = agendaRendaFixa.DataEvento.Value;
                DateTime dataPagamento = agendaRendaFixa.DataPagamento.Value;

                decimal valorFluxoOriginal = 0;
                decimal valorFluxo = 0;
                if (agendaRendaFixa.TipoEvento.Value == (byte)TipoEventoTitulo.Amortizacao || agendaRendaFixa.TipoEvento.Value == (byte)TipoEventoTitulo.AmortizacaoCorrigida)
                {
                    decimal valorAmortizacao = 0;
                    if (agendaRendaFixa.Taxa.HasValue && agendaRendaFixa.Taxa.Value != 0)
                    {
                        valorAmortizacao = puNominal * agendaRendaFixa.Taxa.Value / 100M;
                    }
                    else
                    {
                        valorAmortizacao = agendaRendaFixa.Valor.Value;
                    }

                    valorFluxoOriginal = valorAmortizacao;
                    valorFluxo = valorFluxoOriginal * fatorIndice;

                    puNominalAmortizado -= valorAmortizacao;
                }
                else
                {
                    decimal valorJuro = 0;
                    if (agendaRendaFixa.Taxa.HasValue && agendaRendaFixa.Taxa.Value != 0)
                    {
                        DateTime dataBaseJuros = tituloRendaFixa.DataEmissao.Value;
                        #region Acha data base do último pagamento de cupom de juros
                        AgendaRendaFixaCollection agendaRendaFixaCollectionJuro = new AgendaRendaFixaCollection();
                        agendaRendaFixaCollectionJuro.Query.Select(agendaRendaFixaCollectionJuro.Query.DataEvento);
                        agendaRendaFixaCollectionJuro.Query.Where(agendaRendaFixaCollectionJuro.Query.IdTitulo.Equal(idTitulo),
                                                              agendaRendaFixaCollectionJuro.Query.DataEvento.LessThan(dataEvento),
                                                              agendaRendaFixaCollectionJuro.Query.TipoEvento.In((byte)TipoEventoTitulo.JurosCorrecao,
                                                                                                                (byte)TipoEventoTitulo.Juros));
                        agendaRendaFixaCollectionJuro.Query.OrderBy(agendaRendaFixaCollectionJuro.Query.DataEvento.Descending);
                        agendaRendaFixaCollectionJuro.Query.Load();

                        if (agendaRendaFixaCollectionJuro.Count > 0)
                        {
                            dataBaseJuros = agendaRendaFixaCollectionJuro[0].DataEvento.Value;
                        }
                        #endregion

                        if (contagemDias == (byte)ContagemDias.Uteis)
                        {
                            if (!Calendario.IsDiaUtil(dataEvento))
                            {
                                dataEvento = Calendario.AdicionaDiaUtil(dataEvento, 1);
                            }
                        }

                        int numeroDias = 0;
                        if (contagemDias == (byte)ContagemDias.Uteis)
                        {
                            numeroDias = Calendario.NumeroDias(dataBaseJuros, dataEvento, (int)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                        }
                        else if (contagemDias == (byte)ContagemDias.Dias360)
                        {
                            numeroDias = Calendario.NumeroDias360(dataBaseJuros, dataEvento);
                        }
                        else
                        {
                            numeroDias = Calendario.NumeroDias(dataBaseJuros, dataEvento);
                        }

                        decimal fatorAno = agendaRendaFixa.Taxa.Value / 100M + 1;
                        decimal fatorPeriodo = (decimal)Math.Pow((double)fatorAno, (double)((double)numeroDias / (double)baseCalculo));

                        valorJuro = puNominalAmortizado * (fatorPeriodo - 1M);
                    }
                    else if (agendaRendaFixa.Valor.HasValue && agendaRendaFixa.Valor.Value != 0)
                    {
                        valorJuro = agendaRendaFixa.Valor.Value;
                    }

                    valorFluxoOriginal = valorJuro;
                    valorFluxo = valorJuro;

                    if (agendaRendaFixa.TipoEvento.Value != (byte)TipoEventoTitulo.PagamentoPU)
                    {
                        valorFluxo = valorFluxo * fatorIndice;
                    }
                }

                totalFluxoAtualizado += valorFluxo;

                listaDatas.Add(dataEvento);
                listaValores.Add(valorFluxo);

                #region Preenche Memória de Cálculo
                #region Monta tipoEventoDescricao
                string tipoEventoDescricao = "";
                if (agendaRendaFixa.TipoEvento.Value == (byte)TipoEventoTitulo.Amortizacao || agendaRendaFixa.TipoEvento.Value == (byte)TipoEventoTitulo.AmortizacaoCorrigida)
                {
                    tipoEventoDescricao = "Amortização";
                }
                else if (agendaRendaFixa.TipoEvento.Value == (byte)TipoEventoTitulo.PagamentoCorrecao)
                {
                    tipoEventoDescricao = "Pagto Correção";
                }
                else if (agendaRendaFixa.TipoEvento.Value == (byte)TipoEventoTitulo.Juros)
                {
                    tipoEventoDescricao = "Juros";
                }
                else if (agendaRendaFixa.TipoEvento.Value == (byte)TipoEventoTitulo.JurosCorrecao)
                {
                    tipoEventoDescricao = "Juros+Correção";
                }
                else if (agendaRendaFixa.TipoEvento.Value == (byte)TipoEventoTitulo.PagamentoPrincipal)
                {
                    tipoEventoDescricao = "Pagto Principal";
                }
                else if (agendaRendaFixa.TipoEvento.Value == (byte)TipoEventoTitulo.PagamentoPU)
                {
                    tipoEventoDescricao = "Pagto PU";
                }
                #endregion

                PricingDiario.ValoresFluxoDesconto fluxo = new PricingDiario.ValoresFluxoDesconto();
                fluxo.dataEvento = dataEvento;
                fluxo.dataPagamento = dataPagamento;
                fluxo.valor = valorFluxoOriginal;
                fluxo.valorCorrigido = valorFluxo;

                if (taxaDesconto != 0 && listaValores.Count > 0)
                {
                    fluxo.valorAtual = valorFluxo;
                    fluxo.numeroDias = 0;
                }

                listaFluxo.Add(fluxo);
                #endregion
            }

            decimal puAtualizado = 0;
            if (taxaDesconto != 0 && listaValores.Count > 0)
            {
                List<CalculoFinanceiro.CashFlow> listaCF = new List<CalculoFinanceiro.CashFlow>();
                puAtualizado = CalculoFinanceiro.CalculaVPL(taxaDesconto, dataCalculo, (BaseCalculo)baseCalculo, listaValores, listaDatas, out listaCF);

                #region Finaliza Memória de Cálculo com os dados do fluxo de VPL
                int j = 0;
                foreach (PricingDiario.ValoresFluxoDesconto fluxo in listaFluxo)
                {
                    listaFluxo[j].numeroDias = listaCF[j].numeroDias.Value;
                    listaFluxo[j].valorAtual = (decimal)listaCF[j].valor;
                    j++;
                }
                #endregion
            }
            else
            {
                puAtualizado = totalFluxoAtualizado;
            }

            PosicaoRendaFixa.PosicaoAtualizada posicaoAtualizada = new PosicaoRendaFixa.PosicaoAtualizada();
            posicaoAtualizada.PuAtualizado = puAtualizado;
            posicaoAtualizada.PuCorrecao = puAtualizado;
            posicaoAtualizada.PuJuros = puAtualizado;

            return posicaoAtualizada;
        }

        /// <summary>
        /// Calcula o PU atualizando ele desde o último fluxo encontrado.
        /// </summary>
        /// <param name="posicaoRendaFixa"></param>
        /// <param name="agendaRendaFixaCollection"></param>
        /// <param name="taxa"></param>
        /// <returns></returns>
        public PosicaoRendaFixa.PosicaoAtualizada CalculaPUFluxoCorrigido(DateTime dataCalculo, PapelRendaFixa papelRendaFixa, TituloRendaFixa tituloRendaFixa,
                                      PosicaoRendaFixa posicaoRendaFixa, Indice indice)
        {
            List<PricingDiario.ValoresFluxoCorrecao> listaFluxo = new List<PricingDiario.ValoresFluxoCorrecao>();
            return this.CalculaPUFluxoCorrigido(dataCalculo, papelRendaFixa, tituloRendaFixa, posicaoRendaFixa, indice, out listaFluxo);
        }

        /// <summary>
        /// Calcula o PU atualizando ele desde o último fluxo encontrado.
        /// </summary>
        /// <param name="posicaoRendaFixa"></param>
        /// <param name="agendaRendaFixaCollection"></param>
        /// <param name="taxa"></param>
        /// <returns></returns>
        public PosicaoRendaFixa.PosicaoAtualizada CalculaPUFluxoCorrigido(DateTime dataCalculo, PapelRendaFixa papelRendaFixa, TituloRendaFixa tituloRendaFixa,
                                      PosicaoRendaFixa posicaoRendaFixa, Indice indice, out List<PricingDiario.ValoresFluxoCorrecao> listaFluxo)
        {
            listaFluxo = new List<PricingDiario.ValoresFluxoCorrecao>();

            #region Informações do Papel, Titulo
            int? idIndice = tituloRendaFixa.IdIndice;

            decimal percentual = 100;
            if (tituloRendaFixa.Percentual.HasValue)
            {
                percentual = tituloRendaFixa.Percentual.Value;
            }

            int idTitulo = tituloRendaFixa.IdTitulo.Value;
            int baseCalculo = papelRendaFixa.BaseAno.Value;
            int contagemDias = papelRendaFixa.ContagemDias.Value;
            int tipoCurva = papelRendaFixa.TipoCurva.Value;
            decimal puNominal = tituloRendaFixa.PUNominal.Value;
            decimal puOperacao = posicaoRendaFixa.PUOperacao.Value;
            int numeroDiasBase = papelRendaFixa.BaseAno.Value;
            DateTime dataEmissao = tituloRendaFixa.DataEmissao.Value;
            DateTime dataOperacao = posicaoRendaFixa.DataOperacao.Value;

            decimal taxaOperacao = 0;
            if (posicaoRendaFixa.TaxaOperacao.HasValue && posicaoRendaFixa.TaxaOperacao.Value != 0)
            {
                taxaOperacao = posicaoRendaFixa.TaxaOperacao.Value;
            }
            else
            {
                taxaOperacao = percentual;
            }
            #endregion

            AgendaRendaFixaCollection agendaRendaFixaCollection = new AgendaRendaFixaCollection();
            agendaRendaFixaCollection.Query.Select(agendaRendaFixaCollection.Query.DataEvento,
                                                   agendaRendaFixaCollection.Query.Taxa,
                                                   agendaRendaFixaCollection.Query.Valor,
                                                   agendaRendaFixaCollection.Query.TipoEvento);
            agendaRendaFixaCollection.Query.Where(agendaRendaFixaCollection.Query.IdTitulo.Equal(posicaoRendaFixa.IdTitulo.Value),
                                                  agendaRendaFixaCollection.Query.TipoEvento.In((byte)TipoEventoTitulo.Amortizacao,
                                                                                                (byte)TipoEventoTitulo.AmortizacaoCorrigida,
                                                                                                (byte)TipoEventoTitulo.PagamentoCorrecao),
                                                  agendaRendaFixaCollection.Query.DataEvento.GreaterThan(dataOperacao),
                                                  agendaRendaFixaCollection.Query.DataEvento.LessThanOrEqual(dataCalculo));            
            agendaRendaFixaCollection.Query.OrderBy(agendaRendaFixaCollection.Query.DataEvento.Ascending,
                                                    agendaRendaFixaCollection.Query.IdAgenda.Ascending);
            agendaRendaFixaCollection.Query.Load();

            //IMPORTANTE: Artifício para se setar a taxa de correção do indice usando a taxa de operação informada
            TituloRendaFixa tituloRendaFixaClone = (TituloRendaFixa)Utilitario.Clone(tituloRendaFixa);
            tituloRendaFixaClone.Percentual = taxaOperacao;

            decimal puNominalAmortizado = puNominal;
            #region Ajusta o PU nominal pelas amortizações ocorridas
            AgendaRendaFixaCollection agendaRendaFixaCollectionAmortizacao = new AgendaRendaFixaCollection();
            agendaRendaFixaCollectionAmortizacao.Query.Select(agendaRendaFixaCollectionAmortizacao.Query.Taxa,
                                                              agendaRendaFixaCollectionAmortizacao.Query.Valor);
            agendaRendaFixaCollectionAmortizacao.Query.Where(agendaRendaFixaCollectionAmortizacao.Query.IdTitulo.Equal(idTitulo),
                                                  agendaRendaFixaCollectionAmortizacao.Query.DataPagamento.LessThanOrEqual(dataOperacao),
                                                  agendaRendaFixaCollectionAmortizacao.Query.TipoEvento.In((byte)TipoEventoTitulo.Amortizacao));
            agendaRendaFixaCollectionAmortizacao.Query.Load();

            foreach (AgendaRendaFixa agendaRendaFixaAmortizacao in agendaRendaFixaCollectionAmortizacao)
            {
                if (agendaRendaFixaAmortizacao.Taxa.HasValue && agendaRendaFixaAmortizacao.Taxa.Value != 0)
                {
                    decimal taxaAmortizacao = agendaRendaFixaAmortizacao.Taxa.Value;
                    puNominalAmortizado -= tituloRendaFixa.PUNominal.Value * taxaAmortizacao / 100M;
                }
                else if (agendaRendaFixaAmortizacao.Valor.HasValue && agendaRendaFixaAmortizacao.Valor.Value != 0)
                {
                    puNominalAmortizado -= agendaRendaFixaAmortizacao.Valor.Value;
                }
            }
            #endregion

            decimal puOperacaoAtualizado = 0;
            decimal puOperacaoAtualizadoAnterior = puOperacao;
            decimal valorFluxo = 0;
            DateTime dataBaseCorrecao = tituloRendaFixa.DataEmissao.Value;
            DateTime dataBaseCorrecaoEvento = dataOperacao;
            decimal fatorCorrecaoOperacao = 1;
            foreach (AgendaRendaFixa agendaRendaFixa in agendaRendaFixaCollection)
            {
                DateTime dataEvento = agendaRendaFixa.DataEvento.Value;
                decimal? taxa = agendaRendaFixa.Taxa;                

                #region Calcula valor do fluxo
                #region Acha data base de correção do indice
                AgendaRendaFixaCollection agendaRendaFixaCollectionCorrecao = new AgendaRendaFixaCollection();
                agendaRendaFixaCollectionCorrecao.Query.Select(agendaRendaFixaCollectionCorrecao.Query.DataEvento);
                agendaRendaFixaCollectionCorrecao.Query.Where(agendaRendaFixaCollectionCorrecao.Query.IdTitulo.Equal(idTitulo) &
                                                              agendaRendaFixaCollectionCorrecao.Query.DataEvento.LessThan(dataEvento) &
                                                              agendaRendaFixaCollectionCorrecao.Query.TipoEvento.Equal((byte)TipoEventoTitulo.PagamentoCorrecao) &
                                                              agendaRendaFixaCollectionCorrecao.Query.Taxa.In(0, 100));
                agendaRendaFixaCollectionCorrecao.Query.OrderBy(agendaRendaFixaCollectionCorrecao.Query.DataEvento.Descending);
                agendaRendaFixaCollectionCorrecao.Query.Load();

                if (agendaRendaFixaCollectionCorrecao.Count > 0)
                {
                    dataBaseCorrecao = agendaRendaFixaCollectionCorrecao[0].DataEvento.Value;
                }
                #endregion

                decimal fatorIndice = 1;
                if (dataEvento != dataBaseCorrecao)
                {
                    fatorIndice = this.RetornaFatorCorrecao(dataEvento, dataBaseCorrecao, papelRendaFixa, tituloRendaFixa, indice, true);
                }

                if (agendaRendaFixa.TipoEvento.Value == (byte)TipoEventoTitulo.Amortizacao || agendaRendaFixa.TipoEvento.Value == (byte)TipoEventoTitulo.AmortizacaoCorrigida)
                {
                    decimal valorAmortizacao = 0;
                    if (taxa.HasValue && taxa.Value != 0)
                    {
                        valorAmortizacao = puNominal * taxa.Value / 100M;
                    }
                    else
                    {
                        valorAmortizacao = agendaRendaFixa.Valor.Value;
                    }

                    if (agendaRendaFixa.TipoEvento.Value == (byte)TipoEventoTitulo.AmortizacaoCorrigida)
                    {
                        valorFluxo = valorAmortizacao * fatorIndice;
                    }
                    else
                    {
                        valorFluxo = valorAmortizacao;
                    }

                    puNominalAmortizado -= valorAmortizacao;                        
                }
                else if (agendaRendaFixa.TipoEvento.Value == (byte)TipoEventoTitulo.PagamentoCorrecao)
                {
                    if (!taxa.HasValue || taxa.Value == 0)
                    {
                        taxa = 100;
                    }

                    if (taxa == 100)
                    {
                        valorFluxo = (puNominalAmortizado * fatorIndice) - puNominalAmortizado;                        
                    }
                    else
                    {
                        decimal valorAmortizar = tituloRendaFixa.PUNominal.Value * taxa.Value / 100M;
                        valorFluxo = (valorAmortizar * fatorIndice) - valorAmortizar;
                    }
                }
                #endregion

                fatorCorrecaoOperacao = 1;
                if (dataEvento != dataBaseCorrecaoEvento && agendaRendaFixa.TipoEvento.Value != (byte)TipoEventoTitulo.Amortizacao)
                {
                    fatorCorrecaoOperacao = this.RetornaFatorCorrecao(dataEvento, dataBaseCorrecaoEvento, papelRendaFixa, tituloRendaFixaClone, indice, true);
                }

                puOperacaoAtualizado = (puOperacaoAtualizadoAnterior * fatorCorrecaoOperacao) - valorFluxo;
                puOperacaoAtualizadoAnterior = puOperacaoAtualizado;

                if (agendaRendaFixa.TipoEvento.Value == (byte)TipoEventoTitulo.PagamentoCorrecao && taxa.HasValue && taxa.Value == 100)
                {
                    dataBaseCorrecaoEvento = dataEvento;
                }

                #region Preenche Memória de Cálculo
                PricingDiario.ValoresFluxoCorrecao fluxo = new PricingDiario.ValoresFluxoCorrecao();
                fluxo.dataEvento = dataEvento;
                fluxo.fatorCorrecaoFluxo = fatorIndice;
                fluxo.fatorCorrecaoOperacao = fatorCorrecaoOperacao;
                fluxo.puCorrigido = puOperacaoAtualizado;
                fluxo.puNominalAmortizado = puNominalAmortizado;
                fluxo.taxa = taxa;
                fluxo.tipoEvento = agendaRendaFixa.TipoEvento.Value == (byte)TipoEventoTitulo.Amortizacao ? "Amortização" : "Pagto Correção";
                fluxo.valor = agendaRendaFixa.Valor;
                fluxo.valorCorrigido = valorFluxo;

                listaFluxo.Add(fluxo);
                #endregion
            }

            if (dataCalculo != dataBaseCorrecaoEvento)
            {
                fatorCorrecaoOperacao = this.RetornaFatorCorrecao(dataCalculo, dataBaseCorrecaoEvento, papelRendaFixa, tituloRendaFixaClone, indice, true);
                puOperacaoAtualizado = (puOperacaoAtualizadoAnterior * fatorCorrecaoOperacao);

                #region Preenche Memória de Cálculo
                PricingDiario.ValoresFluxoCorrecao fluxo = new PricingDiario.ValoresFluxoCorrecao();
                fluxo.dataEvento = dataCalculo;
                fluxo.fatorCorrecaoFluxo = 0;
                fluxo.fatorCorrecaoOperacao = fatorCorrecaoOperacao;
                fluxo.puCorrigido = puOperacaoAtualizado;
                fluxo.puNominalAmortizado = puNominalAmortizado;
                fluxo.taxa = 0;
                fluxo.tipoEvento = "Data Cálculo";
                fluxo.valor = 0;
                fluxo.valorCorrigido = 0;

                listaFluxo.Add(fluxo);
                #endregion
            }

            puOperacaoAtualizado = Math.Round(puOperacaoAtualizado, papelRendaFixa.CasasDecimaisPU.Value);

            PosicaoRendaFixa.PosicaoAtualizada posicaoAtualizada = new PosicaoRendaFixa.PosicaoAtualizada();
            posicaoAtualizada.PuAtualizado = puOperacaoAtualizado;
            posicaoAtualizada.PuCorrecao = puOperacaoAtualizado;
            posicaoAtualizada.PuJuros = puOperacaoAtualizado;

            return posicaoAtualizada;
        }
        #endregion

        #region Calculo de MTM

        /// <summary>
        /// Retorna a Taxa Calculada seguindo as parametrizações da curva
        /// </summary>
        /// <param name="dataAtual"></param>
        /// <param name="dataFluxo"></param>
        /// <param name="dataVencimento"></param>
        /// <param name="curvaReferencia"></param>
        /// <param name="hsTaxasCurvas"></param>
        /// <param name="hsTaxasCurvasCompostas"></param>
        /// <returns></returns>
        public decimal CalculaTaxaCurvaMTM(DateTime dataAtual, DateTime dataFluxo, DateTime dataVencimento, CurvaRendaFixa curvaReferencia, ref Hashtable hsTaxasCurvas, ref Hashtable hsTaxasCurvasCompostas)
        {
            #region Declaração de objetos
            TaxaCurvaCollection taxaCurvaCollection = new TaxaCurvaCollection();
            DateTime dataTaxaSpread;
            TaxaCurva taxaSpread = new TaxaCurva();
            TaxaCurva taxaCurva = new TaxaCurva();
            CurvaRendaFixa curvaBase = new CurvaRendaFixa();
            CurvaRendaFixa curvaSpread = new CurvaRendaFixa();
            decimal taxa = 0; 
            decimal taxaBase= 0;
            #endregion

            bool permExtr = (curvaReferencia.PermiteExtrapolacao == 1);
            bool permInterp = (curvaReferencia.PermiteInterpolacao == 1);
            int enumApropriacao = Convert.ToInt32(ApropriacaoCurva.Diario);
            int tpComposicao = (curvaReferencia.TipoComposicao.HasValue ? curvaReferencia.TipoComposicao.Value : 0);
            int tpInterpola = curvaReferencia.CriterioInterpolacao.Value;
            int enumBaseDias = curvaReferencia.ExpressaoTaxaZero.Value;
            int tipoCurva = curvaReferencia.TipoCurva.Value;

            if (tipoCurva == (int)TipoCurva.Composta)
            {
                #region Curva Base
                string hsCurvasCompostasKey;
                if (curvaReferencia.IdCurvaBase.HasValue)
                {
                    curvaBase = curvaReferencia.UpToCurvaRendaFixaByIdCurvaBase;
                    
                    //Hashtable criado para diminuir as chamadas do método recursivo
                    hsCurvasCompostasKey = "Curva-" + curvaBase.IdCurvaRendaFixa + "-" + dataAtual.ToString("MM/dd/yyyy") + "-" + dataFluxo.ToString("MM/dd/yyyy");
                    if (hsTaxasCurvasCompostas.Contains(hsCurvasCompostasKey))
                    {
                        taxaBase = ((decimal)hsTaxasCurvasCompostas[hsCurvasCompostasKey]);
                    }
                    else
                    {
                        //Chama recursivamente, pois a curva base, pode ser composta
                        taxaBase = CalculaTaxaCurvaMTM(dataAtual, dataFluxo, dataVencimento, curvaBase, ref hsTaxasCurvas, ref hsTaxasCurvasCompostas);
                        hsTaxasCurvasCompostas.Add(hsCurvasCompostasKey, taxaBase);
                    }
                }
                else
                {
                    hsCurvasCompostasKey = "Indice-" + curvaReferencia.IdIndice + "-" + dataAtual.ToString("MM/dd/yyyy");
                    if (hsTaxasCurvasCompostas.Contains(hsCurvasCompostasKey))
                    {
                        taxaBase = ((decimal)hsTaxasCurvasCompostas[hsCurvasCompostasKey]);
                    }
                    else
                    {
                        DateTime dataAnterior = Calendario.SubtraiDiaUtil(dataAtual, 1, (int)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                        Indice indice = curvaReferencia.UpToIndiceByIdIndice;

                        decimal fator = CalculoFinanceiro.CalculaFatorIndicePercentual(dataAnterior, dataAnterior, indice, 100m, false);
                        taxaBase = ((decimal)Math.Pow((double)fator, (double)252) - 1) * 100;
                        hsTaxasCurvasCompostas.Add(hsCurvasCompostasKey, taxaBase);
                    }
                }
                #endregion

                #region Compor Taxa
                curvaSpread = curvaReferencia.UpToCurvaRendaFixaByIdCurvaSpread;
                
                if (tpComposicao == (int)TipoComposicao.Vencimento)
                    dataTaxaSpread = dataVencimento;
                else
                    dataTaxaSpread = dataFluxo;

                taxa = this.CalculaTaxaComposta(dataAtual, dataTaxaSpread, curvaSpread, taxaBase, permExtr, enumBaseDias, enumApropriacao, ref hsTaxasCurvas);
                #endregion
            }            
            else
            {
                taxaCurva = taxaCurva.RetornaTaxaCurvaIgualOuAnterior((int)curvaReferencia.IdCurvaRendaFixa, dataAtual, dataFluxo, ref hsTaxasCurvas);

                //Se não encontrou curva com data igual ou anterior, demanda extrapolação
                if (!taxaCurva.IdCurvaRendaFixa.HasValue)
                {
                    if (!permExtr)
                        return 0;

                    //Assume a primeira taxa da Curva
                    taxaCurva = taxaCurva.RetornaTaxaCurvaPosterior((int)curvaReferencia.IdCurvaRendaFixa, dataAtual, dataFluxo, ref hsTaxasCurvas);
                    return (taxaCurva.Taxa.HasValue ? taxaCurva.Taxa.Value : 0);
                }

                //Demanda interpolação
                if (taxaCurva.DataVertice != dataFluxo)
                {
                    if (!permInterp)
                        return 0;

                    TaxaCurva taxaCurvaAnterior = taxaCurva;
                    TaxaCurva taxaCurvaPosterior = taxaCurva.RetornaTaxaCurvaPosterior((int)curvaReferencia.IdCurvaRendaFixa, dataAtual, dataFluxo, ref hsTaxasCurvas);

                    if (!taxaCurvaPosterior.IdCurvaRendaFixa.HasValue)
                    {
                        if (!permExtr)
                            return 0;

                        taxaCurvaPosterior = taxaCurva.RetornaTaxaCurvaIgualOuAnterior((int)curvaReferencia.IdCurvaRendaFixa, dataAtual, dataFluxo, ref hsTaxasCurvas);
                        return (taxaCurvaPosterior.Taxa.HasValue ? taxaCurvaPosterior.Taxa.Value : 0);
                    }

                    taxa = InterpolaTaxasMTM(dataAtual, dataFluxo, taxaCurvaAnterior, taxaCurvaPosterior, tpInterpola);
                }
                else
                {
                    taxa = taxaCurva.Taxa.Value;
                }

            }
            return taxa;
        }

        /// <summary>
        /// Interpola Taxa MTM
        /// </summary>
        /// <param name="dataAtual"></param>
        /// <param name="dataFluxo"></param>
        /// <param name="taxaCurvaAnterior"></param>
        /// <param name="taxaCurvaPosterior"></param>
        /// <param name="tpInterpola"></param>
        /// <returns></returns>
        public decimal InterpolaTaxasMTM(DateTime dataAtual, DateTime dataFluxo, TaxaCurva taxaCurvaAnterior, TaxaCurva taxaCurvaPosterior, int tpInterpola)
        {
            #region Declaração de objetos
            decimal taxaAnterior = 0;
            decimal taxaPosterior = 0;
            decimal prazoFluxo = 0;
            decimal prazoAnterior = 0;
            decimal prazoPosterior = 0;
            decimal taxa = 0;
            #endregion

            taxaAnterior = taxaCurvaAnterior.Taxa.Value;
            taxaPosterior = taxaCurvaPosterior.Taxa.Value;

            prazoAnterior = taxaCurvaAnterior.PrazoDU.Value;
            prazoPosterior = taxaCurvaPosterior.PrazoDU.Value;
            prazoFluxo = Calendario.NumeroDias(dataAtual, dataFluxo, (int)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);

            //Taxa Interpolada Linear
            if (tpInterpola == Convert.ToInt32(CriterioInterpolacao.Linear))
            {
                taxa = (taxaAnterior / 100) + (((taxaPosterior / 100) - (taxaAnterior / 100)) * ((prazoFluxo - prazoAnterior) / (prazoPosterior - prazoAnterior)));
            }

            if (tpInterpola == Convert.ToInt32(CriterioInterpolacao.Exponencial))
            {
                taxa = ((1 + (taxaAnterior / 100)) * (decimal)(Math.Pow((double)((1 + (taxaPosterior / 100)) / (1 + (taxaAnterior / 100))), (double)((prazoFluxo - prazoAnterior) / (prazoPosterior - prazoAnterior))))) - 1;
            }

            return (taxa * 100);
        }

        /// <summary>
        /// Calcula a Taxa Composta
        /// </summary>
        /// <param name="dataAtual"></param>
        /// <param name="dataFluxo"></param>
        /// <param name="curvaSpread"></param>
        /// <param name="taxaBase"></param>
        /// <param name="permExtr"></param>
        /// <param name="enumBaseDias"></param>
        /// <param name="enumApropriacao"></param>
        /// <param name="hsTaxasCurvas"></param>
        /// <returns></returns>
        public decimal CalculaTaxaComposta(DateTime dataAtual, DateTime dataFluxo, CurvaRendaFixa curvaSpread, Decimal taxaBase, bool permExtr, int enumBaseDias, int enumApropriacao, ref Hashtable hsTaxasCurvas)
        {
            TaxaCurvaCollection taxaCurvaCollection = new TaxaCurvaCollection();
            TaxaCurva taxaCurva = new TaxaCurva();
            decimal taxaRetorno = 0;

            taxaCurva = taxaCurva.RetornaTaxaCurvaPosterior((int)curvaSpread.IdCurvaRendaFixa, dataAtual, dataFluxo, ref hsTaxasCurvas);

            //Se não encontrou curva com data igual ou anterior, demanda extrapolação
            if (!taxaCurva.IdCurvaRendaFixa.HasValue)
            {
                if (!permExtr)
                    return 0;

                //Assume a primeira taxa da Curva
                taxaCurva = taxaCurva.RetornaTaxaCurvaIgualOuAnterior((int)curvaSpread.IdCurvaRendaFixa, dataAtual, dataFluxo, ref hsTaxasCurvas);

                if (!taxaCurva.Taxa.HasValue)
                    return 0;
            }
            
            if (curvaSpread.TipoCurva == (int)TipoCurva.Spread)
            {
                //Por ora, só tratamos o diario
                if (enumApropriacao == (int)ApropriacaoCurva.Diario)
                    taxaRetorno = ((1 + (taxaCurva.Taxa.Value / 100m)) * (1 + (taxaBase / 100m))) - 1;
                else
                    taxaRetorno = (taxaCurva.Taxa.Value / 100m) + (taxaBase / 100m);             
            }
            else if (curvaSpread.TipoCurva == (int)TipoCurva.Fator)
            {
                decimal baseDias = this.RetornaBaseDiasMTM(enumBaseDias);

                //Por ora, só tratamos o diario
                if (enumApropriacao == (int)ApropriacaoCurva.Diario)
                {
                    taxaRetorno = (decimal)(Math.Pow((double)(1 + (taxaBase / 100m)), (double)(1 / baseDias)) - 1);
                    taxaRetorno = taxaRetorno * (taxaCurva.Taxa.Value / 100m);
                    taxaRetorno = (decimal)(Math.Pow((double)(1 + taxaRetorno), (double)baseDias) - 1); 
                }
                else
                {
                    taxaRetorno = (taxaBase/100m) / baseDias;
                    taxaRetorno = taxaRetorno * (taxaCurva.Taxa.Value / 100);
                    taxaRetorno = taxaRetorno * baseDias;
                }
            }

            return (taxaRetorno * 100m);
        }

        /// <summary>
        /// Retorn a base de Dias para o calculo da Taxa MTM
        /// </summary>
        /// <param name="enumBaseDias"></param>
        /// <returns></returns>
        public decimal RetornaBaseDiasMTM(int enumBaseDias)
        {
            switch (enumBaseDias)
            {
                case (int)ExpressaoTaxaZero.LIN_30_360:
                case (int)ExpressaoTaxaZero.EXP_DC_360:
                case (int)ExpressaoTaxaZero.LIN_DC_360:
                    {
                        return 360;
                    }
                case (int)ExpressaoTaxaZero.EXP_30_365:
                case (int)ExpressaoTaxaZero.LIN_DC_365:
                    {
                        return 365;
                    }
                case (int)ExpressaoTaxaZero.EXP_DU_252:
                    {
                        return 252;
                    }
                default:
                    {
                        throw new Exception("Base de Dias MTM - Não implementado!");
                    }
            }
        }

        /// <summary>
        /// Calcula Posição Atualizada usando a biblioteca do Fincs
        /// </summary>
        /// <param name="data"></param>
        /// <param name="papelRendaFixa"></param>
        /// <param name="tituloRendaFixa"></param>
        /// <param name="pu"></param>
        /// <param name="lstTaxas"></param>
        /// <param name="previa"></param>
        /// <param name="idOperacao"></param>
        /// <param name="enumTipoPreco"></param>
        /// <param name="enumTipoProcessamento"></param>
        /// <param name="persisteMemoria"></param>
        /// <param name="hsMemoria"></param>
        /// <returns></returns>
        public PosicaoRendaFixa.PosicaoAtualizada CalculaPosicao_Fincs(DateTime data, PapelRendaFixa papelRendaFixa, TituloRendaFixa tituloRendaFixa, decimal? pu, List<double> lstTaxas, decimal? previa, int idOperacao, int enumTipoPreco, int enumTipoProcessamento, bool persisteMemoria, ref Hashtable hsMemoria)
        {
            return CalculaPosicao_Fincs(data, papelRendaFixa, tituloRendaFixa, pu, lstTaxas, previa, null, idOperacao, enumTipoPreco, enumTipoProcessamento, persisteMemoria, ref hsMemoria);
        }

        /// <summary>
        /// Calcula Posição Atualizada usando a biblioteca do Fincs
        /// </summary>
        /// <param name="data"></param>
        /// <param name="papelRendaFixa"></param>
        /// <param name="tituloRendaFixa"></param>
        /// <param name="pu"></param>
        /// <param name="lstTaxas"></param>
        /// <param name="previa"></param>
        /// <param name="puLimpo"></param>
        /// <param name="idOperacao"></param>
        /// <param name="enumTipoPreco"></param>
        /// <param name="enumTipoProcessamento"></param>
        /// <param name="persisteMemoria"></param>
        /// <param name="hsMemoria"></param>
        /// <returns></returns>
        public PosicaoRendaFixa.PosicaoAtualizada CalculaPosicao_Fincs(DateTime data, PapelRendaFixa papelRendaFixa, TituloRendaFixa tituloRendaFixa, decimal? pu, List<double> lstTaxas, decimal? previa, decimal? puLimpo, int idOperacao, int enumTipoPreco, int enumTipoProcessamento, bool persisteMemoria, ref Hashtable hsMemoria)
       {            
            OperacaoRendaFixa operacaoRendaFixa = new OperacaoRendaFixa();
            operacaoRendaFixa.LoadByPrimaryKey(idOperacao);
            return CalculaPosicao_Fincs(data, papelRendaFixa, tituloRendaFixa, pu, lstTaxas, previa, puLimpo, operacaoRendaFixa, enumTipoPreco, enumTipoProcessamento, persisteMemoria, ref hsMemoria);
        }


        public PosicaoRendaFixa.PosicaoAtualizada CalculaPosicao_Fincs(DateTime data, PapelRendaFixa papelRendaFixa, TituloRendaFixa tituloRendaFixa, decimal? pu, List<double> lstTaxas, decimal? previa, decimal? puLimpo, OperacaoRendaFixa operacaoRendaFixa, int enumTipoPreco, int enumTipoProcessamento, bool persisteMemoria, ref Hashtable hsMemoria)
        {
            PosicaoRendaFixa.PosicaoAtualizada posicaoAtualizada = new PosicaoRendaFixa.PosicaoAtualizada();
            CalculoRendaFixaFincs calculoRendaFixaFincs = new CalculoRendaFixaFincs();
            ControllerRendaFixa controllerRendaFixa = new ControllerRendaFixa();

            int idTitulo = tituloRendaFixa.IdTitulo.Value;
            int idPapel = tituloRendaFixa.IdPapel.Value;
            int enumClasse = papelRendaFixa.Classe.Value;
            short? idIndice = tituloRendaFixa.IdIndice;
            DateTime dataEmissao = tituloRendaFixa.DataEmissao.Value;
            DateTime dataVencimento = tituloRendaFixa.DataVencimento.Value;
            decimal valorNominal = tituloRendaFixa.ValorNominal.GetValueOrDefault(0);
            int defasagemLiquidacao = tituloRendaFixa.DefasagemLiquidacao.GetValueOrDefault(0);
            int periodicidade = tituloRendaFixa.Periodicidade.GetValueOrDefault(0);
            double taxaTitulo = (double)tituloRendaFixa.Taxa.GetValueOrDefault(0);            
            Indice indice = new Indice();
            List<double> lstExpectedRateRange = null;

            bool ativoExotico = tituloRendaFixa.AtivoRegra != Financial.BibliotecaFincs.Enums.AtivoRegra.SemRegraEspecifica;

            #region Calcula prazoLiquidacao de acordo com feriado da clearing(defasagem) 
            int prazoLiquidacao = 0;
            if(defasagemLiquidacao > 0)
            {
                prazoLiquidacao = this.CalculaPrazoLiquidacao(data, (operacaoRendaFixa.IdOperacao.GetValueOrDefault(0) > 0 ? operacaoRendaFixa : new OperacaoRendaFixa()), tituloRendaFixa);
            }
            #endregion

            #region Calculo de previa/curva
            if ((int)TipoProcessamento.Abertura != enumTipoProcessamento)
            {
                if (idIndice.HasValue)
                {
                    indice.LoadByPrimaryKey(idIndice.Value);

                    List<int> lstIndicesDiarios = new List<int>() { (int)TipoDivulgacaoIndice.Diario, (int)TipoDivulgacaoIndice.Diario_1, (int)TipoDivulgacaoIndice.Diario_2, (int)TipoDivulgacaoIndice.Diario_3 };
                    if (!previa.HasValue && !lstIndicesDiarios.Contains(indice.TipoDivulgacao.Value))
                    {
                        previa = this.CalculaPrevia(data, indice);
                    }

                    if (indice.IdCurva.HasValue)
                    {
                        #region Controle de Memoria
                        Hashtable hsTaxasCurvasCompostas;
                        int keyCurvaComposta = (int)KeyHashTable.CurvasCompostas;
                        if (hsMemoria.Contains(keyCurvaComposta))
                            hsTaxasCurvasCompostas = (Hashtable)hsMemoria[keyCurvaComposta];
                        else
                            hsTaxasCurvasCompostas = new Hashtable();

                        Hashtable hsTaxasCurvas;
                        int keyTaxaCurva = (int)KeyHashTable.TaxaCurva;
                        if (hsMemoria.Contains(keyTaxaCurva))
                            hsTaxasCurvas = (Hashtable)hsMemoria[keyTaxaCurva];
                        else
                            hsTaxasCurvas = new Hashtable();
                        #endregion

                        #region Obtem datas de fluxo
                        AgendaRendaFixaCollection agendaColl = new AgendaRendaFixaCollection();
                        List<DateTime> lstDataFluxos = agendaColl.BuscaDataFluxo(tituloRendaFixa.IdTitulo.Value, data);
                        #endregion

                        lstExpectedRateRange = new List<double>();
                        CurvaRendaFixa curvaRendaFixa = new CurvaRendaFixa();
                        curvaRendaFixa.LoadByPrimaryKey(indice.IdCurva.Value);

                        if (lstDataFluxos != null && lstDataFluxos.Count > 0)
                        {
                            foreach (DateTime dataFluxo in lstDataFluxos)
                            {
                                lstExpectedRateRange.Add((double)this.CalculaTaxaCurvaMTM(data, dataFluxo, dataVencimento, curvaRendaFixa, ref hsTaxasCurvas, ref hsTaxasCurvasCompostas));
                            }
                        }
                        else
                        {
                            lstExpectedRateRange.Add((double)this.CalculaTaxaCurvaMTM(data, dataVencimento, dataVencimento, curvaRendaFixa, ref hsTaxasCurvas, ref hsTaxasCurvasCompostas));
                        }
                    }
                }                                    
            }

            previa = previa ?? 0;
            #endregion

            List<int> lstPrivados = new List<int>();
            lstPrivados.Add((int)ClasseRendaFixa.CDB);
            lstPrivados.Add((int)ClasseRendaFixa.LCA);
            lstPrivados.Add((int)ClasseRendaFixa.LCI);

            List<int> lstOffShore = new List<int>();
            lstOffShore.Add((int)ClasseRendaFixa.GLOBALS);
            lstOffShore.Add((int)ClasseRendaFixa.TBILL);

            if (enumClasse == (int)ClasseRendaFixa.NTN)
            {
                int serieNTN = controllerRendaFixa.DescobreSerieDeNTN(idIndice);

                if (serieNTN == (int)SerieNTN.ntnB)
                    posicaoAtualizada = calculoRendaFixaFincs.Calcula_NTNB(data, dataVencimento, operacaoRendaFixa, (double)previa, lstTaxas, (double?)pu, enumTipoPreco, enumTipoProcessamento, persisteMemoria);
                else if (serieNTN == (int)SerieNTN.ntnC)
                    posicaoAtualizada = calculoRendaFixaFincs.Calcula_NTNC(data, dataVencimento, operacaoRendaFixa, (double)previa, lstTaxas, (double?)pu, enumTipoPreco, enumTipoProcessamento, persisteMemoria);
                else if (serieNTN == (int)SerieNTN.ntnF)
                    posicaoAtualizada = calculoRendaFixaFincs.Calcula_NTNF(data, dataVencimento, operacaoRendaFixa, lstTaxas, (double?)pu, enumTipoPreco, enumTipoProcessamento, persisteMemoria);
            }
            else if (enumClasse == (int)ClasseRendaFixa.LTN)
            {
                posicaoAtualizada = calculoRendaFixaFincs.Calcula_LTN(data, dataVencimento, operacaoRendaFixa, lstTaxas, (double?)pu, enumTipoPreco, enumTipoProcessamento, persisteMemoria);
            }
            else if (enumClasse == (int)ClasseRendaFixa.LFT)
            {
                posicaoAtualizada = calculoRendaFixaFincs.Calcula_LFT(data, dataVencimento, operacaoRendaFixa, lstTaxas, (double?)pu, enumTipoPreco, enumTipoProcessamento, persisteMemoria);
            }
            else if (lstOffShore.Contains(enumClasse))
            {
                if (puLimpo.HasValue)
                {
                    if (enumClasse == (int)ClasseRendaFixa.GLOBALS)
                    {
                        posicaoAtualizada = calculoRendaFixaFincs.CalculaOffShoreGLOBALS(data, dataEmissao, dataVencimento, prazoLiquidacao, taxaTitulo, periodicidade, (double)valorNominal, (double)puLimpo);
                    }
                    else if (enumClasse == (int)ClasseRendaFixa.TBILL)
                    {
                        posicaoAtualizada = calculoRendaFixaFincs.CalculaOffShoreTBILL(data, dataEmissao, dataVencimento, prazoLiquidacao, taxaTitulo, periodicidade, (double)valorNominal, (double)puLimpo);
                    }
                }
                else
                {
                    if (enumClasse == (int)ClasseRendaFixa.GLOBALS)
                    {
                        posicaoAtualizada = calculoRendaFixaFincs.CalculaOffShoreGLOBALS(data, dataEmissao, dataVencimento, prazoLiquidacao, taxaTitulo, periodicidade, (double)valorNominal, lstTaxas, (double?)pu, operacaoRendaFixa, enumTipoPreco, enumTipoProcessamento, persisteMemoria);
                    }
                    else if (enumClasse == (int)ClasseRendaFixa.TBILL)
                    {
                        posicaoAtualizada = calculoRendaFixaFincs.CalculaOffShoreTBILL(data, dataEmissao, dataVencimento, prazoLiquidacao, taxaTitulo, periodicidade, (double)valorNominal, lstTaxas, (double?)pu, operacaoRendaFixa, enumTipoPreco, enumTipoProcessamento, persisteMemoria);
                    }
                }
            }
            else
            {
                if (ativoExotico)
                    posicaoAtualizada = calculoRendaFixaFincs.Calcula_AtivoExotico(data, papelRendaFixa, tituloRendaFixa, lstTaxas, (double?)pu, (double)previa, lstExpectedRateRange, operacaoRendaFixa, enumTipoPreco, enumTipoProcessamento, persisteMemoria, ref hsMemoria);
                else
                    posicaoAtualizada = calculoRendaFixaFincs.Calcula_Geral(data, papelRendaFixa, tituloRendaFixa, lstTaxas, (double?)pu, (double)previa, lstExpectedRateRange, operacaoRendaFixa, enumTipoPreco, enumTipoProcessamento, persisteMemoria, ref hsMemoria);
            }

            return posicaoAtualizada;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="indice"></param>
        /// <returns></returns>
        public decimal CalculaPrevia(DateTime data, Indice indice)
        {
            CotacaoIndice cotacaoIndiceAtual = new CotacaoIndice();
            CotacaoIndice cotacaoIndiceAnterior = new CotacaoIndice();
            DateTime dtCotacaoAtual = new DateTime();
            DateTime dtCotacaoAnterior = new DateTime();

            #region Seta datas
            if (Convert.ToInt32(indice.TipoDivulgacao.Value) == (int)TipoDivulgacaoIndice.Mensal)
            {
                int diaDivulgacao = 0;
                if (indice.IdIndice.Value == (short)ListaIndiceFixo.IGPM)
                    diaDivulgacao = 1;
                else if (indice.IdIndice.Value == (short)ListaIndiceFixo.IPCA)
                    diaDivulgacao = 15;

                dtCotacaoAtual = (new DateTime(data.Year, data.Month, diaDivulgacao));

                //Se a data for o dia de Divulgacao, nao ha previa
                if (dtCotacaoAtual.Equals(data))
                    return 0;            

                if (diaDivulgacao > data.Day)
                {
                    dtCotacaoAtual = (new DateTime(data.Year, data.Month, 1)).AddMonths(-1);
                    dtCotacaoAnterior = (new DateTime(data.Year, data.Month, 1)).AddMonths(-2);
                }
                else
                {
                    dtCotacaoAtual = (new DateTime(data.Year, data.Month, 1));
                    dtCotacaoAnterior = (new DateTime(data.Year, data.Month, 1)).AddMonths(-1);
                }

            }
            else if (Convert.ToInt32(indice.TipoDivulgacao.Value) == (int)TipoDivulgacaoIndice.Diario)
            {
                dtCotacaoAtual = Calendario.SubtraiDiaUtil(data, 1, (int)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                dtCotacaoAnterior = Calendario.SubtraiDiaUtil(data, 2, (int)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
            }
            else
            {
                Financial.Common.Enums.TipoDivulgacaoIndiceDescricao.RetornaDescricao(Convert.ToInt32(indice.TipoDivulgacao.Value));
                throw new Exception("Método de Cálculo de Prévia não implementada para Tipo de Divulgação " + Financial.Common.Enums.TipoDivulgacaoIndiceDescricao.RetornaDescricao(Convert.ToInt32(indice.TipoDivulgacao.Value)) + "!");
            }                
            #endregion

            #region Carrega Objetos
            if (!cotacaoIndiceAtual.LoadByPrimaryKey(dtCotacaoAtual, indice.IdIndice.Value))
                throw new Exception("Cotação não cadastrada: Índice - " + indice.Descricao + " para o dia " + String.Format("{0:dd/MM/yyyy}", dtCotacaoAtual));

            if (!cotacaoIndiceAnterior.LoadByPrimaryKey(dtCotacaoAnterior, indice.IdIndice.Value))
                throw new Exception("Cotação não cadastrada: Índice - " + indice.Descricao + " para o dia " + String.Format("{0:dd/MM/yyyy}", dtCotacaoAnterior));
            #endregion

            return Convert.ToDecimal(((cotacaoIndiceAtual.Valor.Value / cotacaoIndiceAnterior.Valor.Value) - 1) * 100);
        }

        
        public int CalculaPrazoLiquidacao(DateTime data, OperacaoRendaFixa operacao, TituloRendaFixa tituloRendaFixa)
        {
            Clearing clearing = new Clearing();
            PapelRendaFixa papelRendaFixa = tituloRendaFixa.UpToPapelRendaFixaByIdPapel;
            int defasagemLiquidacao = tituloRendaFixa.DefasagemLiquidacao.GetValueOrDefault(0);
            int idClearing = 0;
            int idLocalFeriado = 0;
            int prazoLiquidacao = defasagemLiquidacao;

            if (operacao.IdLiquidacao.GetValueOrDefault(0) > 0)
                idClearing = operacao.IdLiquidacao.Value;
            else
                idClearing = papelRendaFixa.IdClearing.Value;

            if (clearing.LoadByPrimaryKey(idClearing))
            {
                idLocalFeriado = clearing.IdLocalFeriado.Value;
                TipoFeriado tipoFeriado = (clearing.IdLocalFeriado.Value == LocalFeriadoFixo.Brasil ? TipoFeriado.Brasil : TipoFeriado.Outros);
                DateTime dataAux = Calendario.AdicionaDiaUtil(data, defasagemLiquidacao, idLocalFeriado, tipoFeriado);
                prazoLiquidacao = Calendario.NumeroDias(data, dataAux);
            }

            return prazoLiquidacao;
        }
        #endregion        
    }
}