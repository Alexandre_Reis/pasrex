/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 15/10/2014 14:20:12
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.RendaFixa
{
	public partial class MemoriaCalculoRendaFixaCollection : esMemoriaCalculoRendaFixaCollection
	{
        public void DeletaMemoriaCalculo(int idCliente, DateTime data, int idOperacao, int enumTipoPreco, int enumTipoProcessamento)
        {
            MemoriaCalculoRendaFixaCollection memoriaColl = new MemoriaCalculoRendaFixaCollection();
            memoriaColl.Query.Where(memoriaColl.Query.IdCliente.Equal(idCliente) 
                                    & memoriaColl.Query.DataAtual.Equal(data) 
                                    & memoriaColl.Query.IdOperacao.Equal(idOperacao) 
                                    & memoriaColl.Query.TipoPreco.Equal(enumTipoPreco)
                                    & memoriaColl.Query.TipoProcessamento.Equal(enumTipoProcessamento));

            if (memoriaColl.Load(memoriaColl.Query))
            {
                memoriaColl.MarkAllAsDeleted();
                memoriaColl.Save();
            }
        }
	}
}
