/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 23/09/2014 10:57:13
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using System.Collections;

namespace Financial.RendaFixa
{
	public partial class PerfilMTM : esPerfilMTM
	{
        public PerfilMTM obtemPerfilMTM(DateTime? data, int? idPapel, int? idOperacao, int? idTitulo, int idGrupoPerfilMTM)
        {
            PerfilMTMCollection perfilMTM_Coll = new PerfilMTMCollection();

            perfilMTM_Coll = perfilMTM_Coll.obterPerfilMTMCollection(idPapel.Value, idGrupoPerfilMTM, data.Value);

            if (perfilMTM_Coll.Count > 0)
                return obtemPerfilMTM_Prioridade(idOperacao, idTitulo, perfilMTM_Coll);

            return new PerfilMTM();
        }

        public PerfilMTM obtemPerfilMTM_Prioridade(int? idOperacao, int? idTitulo, PerfilMTMCollection perfilMTMCollection)
        {
            PerfilMTM perfilMTM;
            List<PerfilMTM> lstPerfilMTM = (List<PerfilMTM>)perfilMTMCollection;

            perfilMTM = lstPerfilMTM.Find(delegate(PerfilMTM x) { return x.IdOperacao == idOperacao; });
            if (perfilMTM != null && perfilMTM.IdOperacao > 0)
                return perfilMTM;

            perfilMTM = lstPerfilMTM.Find(delegate(PerfilMTM x) { return x.IdOperacao == null && x.IdTitulo == idTitulo; });
            if (perfilMTM != null && perfilMTM.IdTitulo > 0)
                return perfilMTM;

            perfilMTM = lstPerfilMTM.Find(delegate(PerfilMTM x) { return x.IdOperacao == null && x.IdTitulo == null; });
            return (perfilMTM != null ? perfilMTM : new PerfilMTM());
        }
	}
}
