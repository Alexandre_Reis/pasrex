/*
===============================================================================
                     EntitySpaces(TM) by EntitySpaces, LLC
                 A New 2.0 Architecture for the .NET Framework
                          http://www.entityspaces.net
===============================================================================
                       EntitySpaces Version # 2007.0.0304.0
                       MyGeneration Version # 1.2.0.2
                           14/3/2007 11:34:37
-------------------------------------------------------------------------------
*/


using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;

namespace Financial.RendaFixa {
    public partial class CotacaoResolucao550Collection : esCotacaoResolucao550Collection {
        private static readonly ILog log = LogManager.GetLogger(typeof(CotacaoResolucao550Collection));

        /// <summary>
        ///  Deleta tudo da CotacaoResolucao550 de uma determinada data.
        /// </summary>
        /// <param name="data"></param>
        public void DeletaCotacaoResolucao550(DateTime data) {

            #region logEntrada
            if (log.IsDebugEnabled) {
                log.Debug("Entrada nomeMetodo: ");
            }
            #endregion

            this.QueryReset();
            this.Query
              .Select(this.query.DataReferencia, this.query.CodigoSELIC, this.query.DataVencimento)
              .Where(this.query.DataReferencia == data);

            this.Query.Load();

            #region logSql
            if (log.IsInfoEnabled) {
                StringBuilder sql = new StringBuilder("Delete: " + this.query.es.LastQuery);
                sql = sql.Replace("@DataReferencia1", "'" + data.ToString("yyyy-MM-dd") + "'");
                log.Info(sql.ToString());
            }
            #endregion

            this.MarkAllAsDeleted();
            this.Save();

            #region logSaida
            if (log.IsDebugEnabled) {
                log.Debug("Entrada nomeMetodo: ");
            }
            #endregion
        }

    }
}
