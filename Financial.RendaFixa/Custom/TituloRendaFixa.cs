﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.RendaFixa.Enums;
using Financial.Common.Enums;
using Financial.Fundo;
using Financial.Common;
using Financial.Util;
using Financial.Util.Enums;
using Financial.Interfaces;
using Financial.InterfacesDB;
using Financial.Fundo.Enums;

namespace Financial.RendaFixa
{
    public partial class TituloRendaFixa : esTituloRendaFixa
    {
        public void CriaTitulo(int idTitulo, string descricao, int idPapel, int idEmissor, DateTime dataEmissao, DateTime dataVencimento,
                               decimal puEmissao, decimal? taxa, short? idIndice, decimal percentual)
        {
            int? idEstrategia = null;
            #region Verifica estrategia
            if (idIndice.HasValue && idIndice.Value == (short)ListaIndiceFixo.CDI)
            {
                EstrategiaCollection estrategiaCollection = new EstrategiaCollection();
                estrategiaCollection.Query.Select(estrategiaCollection.Query.IdEstrategia);
                estrategiaCollection.Query.Where(estrategiaCollection.Query.Descricao.Like("%DI%"));
                estrategiaCollection.Query.Load();

                if (estrategiaCollection.Count > 0)
                {
                    idEstrategia = estrategiaCollection[0].IdEstrategia.Value;
                }
            }
            else
            {
                EstrategiaCollection estrategiaCollection = new EstrategiaCollection();
                estrategiaCollection.Query.Select(estrategiaCollection.Query.IdEstrategia);
                estrategiaCollection.Query.Where(estrategiaCollection.Query.Descricao.Like("%Fixa%"));
                estrategiaCollection.Query.Load();

                if (estrategiaCollection.Count > 0)
                {
                    idEstrategia = estrategiaCollection[0].IdEstrategia.Value;
                }
            }
            #endregion

            string descricaoCompleta = this.RetornaDescricaoTitulo(descricao, dataVencimento, taxa, idIndice, percentual);

            esParameters esParams = new esParameters();
            esParams.Add(TituloRendaFixaMetadata.ColumnNames.IdTitulo, idTitulo);
            esParams.Add(TituloRendaFixaMetadata.ColumnNames.DataEmissao, dataEmissao);
            esParams.Add(TituloRendaFixaMetadata.ColumnNames.DataVencimento, dataVencimento);
            esParams.Add(TituloRendaFixaMetadata.ColumnNames.PUNominal, puEmissao);

            if (taxa.HasValue)
            {
                esParams.Add(TituloRendaFixaMetadata.ColumnNames.Taxa, taxa);
            }
            else
            {
                esParams.Add(TituloRendaFixaMetadata.ColumnNames.Taxa, DBNull.Value);
            }
            if (idIndice.HasValue)
            {
                esParams.Add(TituloRendaFixaMetadata.ColumnNames.IdIndice, idIndice);
            }
            else
            {
                esParams.Add(TituloRendaFixaMetadata.ColumnNames.IdIndice, DBNull.Value);
            }

            esParams.Add(TituloRendaFixaMetadata.ColumnNames.Percentual, percentual);
            esParams.Add(TituloRendaFixaMetadata.ColumnNames.IdPapel, idPapel);
            esParams.Add(TituloRendaFixaMetadata.ColumnNames.Descricao, descricao);
            esParams.Add(TituloRendaFixaMetadata.ColumnNames.DescricaoCompleta, descricaoCompleta);
            esParams.Add(TituloRendaFixaMetadata.ColumnNames.IdEmissor, idEmissor);

            if (idEstrategia.HasValue)
            {
                esParams.Add(TituloRendaFixaMetadata.ColumnNames.IdEstrategia, idEstrategia.Value);
            }
            else
            {
                esParams.Add(TituloRendaFixaMetadata.ColumnNames.IdEstrategia, DBNull.Value);
            }

            esParams.Add(TituloRendaFixaMetadata.ColumnNames.IdMoeda, (byte)ListaMoedaFixo.Real);
            esParams.Add(TituloRendaFixaMetadata.ColumnNames.IsentoIR, (byte)TituloIsentoIR.NaoIsento);
            esParams.Add(TituloRendaFixaMetadata.ColumnNames.IsentoIOF, (byte)TituloIsentoIOF.NaoIsento);
            esParams.Add(TituloRendaFixaMetadata.ColumnNames.ValorNominal, puEmissao);

            StringBuilder sqlBuilder = new StringBuilder();

            sqlBuilder.Append("SET IDENTITY_INSERT TituloRendaFixa ON ");
            sqlBuilder.Append("INSERT INTO TITULORENDAFIXA (");
            sqlBuilder.Append(TituloRendaFixaMetadata.ColumnNames.IdTitulo + ", ");
            sqlBuilder.Append(TituloRendaFixaMetadata.ColumnNames.DataEmissao + ", ");
            sqlBuilder.Append(TituloRendaFixaMetadata.ColumnNames.DataVencimento + ", ");
            sqlBuilder.Append(TituloRendaFixaMetadata.ColumnNames.PUNominal + ", ");
            sqlBuilder.Append(TituloRendaFixaMetadata.ColumnNames.Taxa + ", ");
            sqlBuilder.Append(TituloRendaFixaMetadata.ColumnNames.IdIndice + ", ");
            sqlBuilder.Append(TituloRendaFixaMetadata.ColumnNames.Percentual + ", ");
            sqlBuilder.Append(TituloRendaFixaMetadata.ColumnNames.IdPapel + ", ");
            sqlBuilder.Append(TituloRendaFixaMetadata.ColumnNames.Descricao + ", ");
            sqlBuilder.Append(TituloRendaFixaMetadata.ColumnNames.DescricaoCompleta + ", ");
            sqlBuilder.Append(TituloRendaFixaMetadata.ColumnNames.IdEmissor + ", ");
            sqlBuilder.Append(TituloRendaFixaMetadata.ColumnNames.IdEstrategia + ", ");
            sqlBuilder.Append(TituloRendaFixaMetadata.ColumnNames.IdMoeda + ", ");
            sqlBuilder.Append(TituloRendaFixaMetadata.ColumnNames.IsentoIR + ", ");
            sqlBuilder.Append(TituloRendaFixaMetadata.ColumnNames.IsentoIOF + ", ");
            sqlBuilder.Append(TituloRendaFixaMetadata.ColumnNames.ValorNominal);
            sqlBuilder.Append(") VALUES ( ");
            sqlBuilder.Append("@" + TituloRendaFixaMetadata.ColumnNames.IdTitulo);
            sqlBuilder.Append(",");
            sqlBuilder.Append("@" + TituloRendaFixaMetadata.ColumnNames.DataEmissao);
            sqlBuilder.Append(",");
            sqlBuilder.Append("@" + TituloRendaFixaMetadata.ColumnNames.DataVencimento);
            sqlBuilder.Append(",");
            sqlBuilder.Append("@" + TituloRendaFixaMetadata.ColumnNames.PUNominal);
            sqlBuilder.Append(",");
            sqlBuilder.Append("@" + TituloRendaFixaMetadata.ColumnNames.Taxa);
            sqlBuilder.Append(",");
            sqlBuilder.Append("@" + TituloRendaFixaMetadata.ColumnNames.IdIndice);
            sqlBuilder.Append(",");
            sqlBuilder.Append("@" + TituloRendaFixaMetadata.ColumnNames.Percentual);
            sqlBuilder.Append(",");
            sqlBuilder.Append("@" + TituloRendaFixaMetadata.ColumnNames.IdPapel);
            sqlBuilder.Append(",");
            sqlBuilder.Append("@" + TituloRendaFixaMetadata.ColumnNames.Descricao);
            sqlBuilder.Append(",");
            sqlBuilder.Append("@" + TituloRendaFixaMetadata.ColumnNames.DescricaoCompleta);
            sqlBuilder.Append(",");
            sqlBuilder.Append("@" + TituloRendaFixaMetadata.ColumnNames.IdEmissor);
            sqlBuilder.Append(",");
            sqlBuilder.Append("@" + TituloRendaFixaMetadata.ColumnNames.IdEstrategia);
            sqlBuilder.Append(",");
            sqlBuilder.Append("@" + TituloRendaFixaMetadata.ColumnNames.IdMoeda);
            sqlBuilder.Append(",");
            sqlBuilder.Append("@" + TituloRendaFixaMetadata.ColumnNames.IsentoIR);
            sqlBuilder.Append(",");
            sqlBuilder.Append("@" + TituloRendaFixaMetadata.ColumnNames.IsentoIOF);
            sqlBuilder.Append(",");
            sqlBuilder.Append("@" + TituloRendaFixaMetadata.ColumnNames.ValorNominal);
            sqlBuilder.Append(")");

            sqlBuilder.Append("SET IDENTITY_INSERT TituloRendaFixa OFF ");

            esUtility u = new esUtility();
            u.ExecuteNonQuery(esQueryType.Text, sqlBuilder.ToString(), esParams);
        }

        public void CriaTitulo(string codigoInterface, string descricao, int idPapel, int idEmissor, DateTime dataEmissao, DateTime dataVencimento,
                               decimal puEmissao, decimal? taxa, short? idIndice, decimal percentual, byte tipoMTM)
        {
            int? idEstrategia = null;
            #region Verifica estrategia
            if (idIndice.HasValue && idIndice.Value == (short)ListaIndiceFixo.CDI)
            {
                EstrategiaCollection estrategiaCollection = new EstrategiaCollection();
                estrategiaCollection.Query.Select(estrategiaCollection.Query.IdEstrategia);
                estrategiaCollection.Query.Where(estrategiaCollection.Query.Descricao.Like("%DI%"));
                estrategiaCollection.Query.Load();

                if (estrategiaCollection.Count > 0)
                {
                    idEstrategia = estrategiaCollection[0].IdEstrategia.Value;
                }
            }
            else
            {
                EstrategiaCollection estrategiaCollection = new EstrategiaCollection();
                estrategiaCollection.Query.Select(estrategiaCollection.Query.IdEstrategia);
                estrategiaCollection.Query.Where(estrategiaCollection.Query.Descricao.Like("%Fixa%"));
                estrategiaCollection.Query.Load();

                if (estrategiaCollection.Count > 0)
                {
                    idEstrategia = estrategiaCollection[0].IdEstrategia.Value;
                }
            }
            #endregion

            string descricaoCompleta = this.RetornaDescricaoTitulo(descricao, dataVencimento, taxa, idIndice, percentual);

            this.Descricao = descricao;
            this.DescricaoCompleta = descricaoCompleta;

            this.IdEmissor = idEmissor;
            this.IdMoeda = (int)ListaMoedaFixo.Real;

            this.IdEstrategia = idEstrategia;
            
            this.IdPapel = idPapel;
            this.DataEmissao = dataEmissao;

            this.CodigoIsin = ""; //Hoje não tem como saber!!!!
            this.CodigoCDA = 0; //Hoje não tem como saber!!!!

            this.DataVencimento = dataVencimento;
            this.IdIndice = idIndice;
            this.IsentoIOF = (byte)TituloIsentoIOF.NaoIsento;
            this.IsentoIR = (byte)TituloIsentoIR.NaoIsento;
            this.Percentual = percentual;
            this.PUNominal = puEmissao;
            this.Taxa = taxa;
            this.ValorNominal = puEmissao;
            this.CodigoInterface = codigoInterface;
                        
            this.Save();
        }
                
        public string RetornaDescricaoTitulo(string descricao, DateTime dataOperacao, DateTime dataVencimento, decimal? taxa, decimal? idIndice, decimal? percentual)
        {
            StringBuilder descricaoCompleta = new StringBuilder();
            descricaoCompleta.Append(descricao.Trim() + " - ");
            descricaoCompleta.Append(" Vcto: ");
            descricaoCompleta.Append(dataVencimento.ToShortDateString() + " - ");
            descricaoCompleta.Append(" Compra: ");
            descricaoCompleta.Append(dataOperacao.ToShortDateString());

            if (idIndice.HasValue || (taxa.HasValue && taxa.Value != 0))
            {
                descricaoCompleta.Append(" - ");
            }

            if (idIndice.HasValue)
            {
                Indice indice = new Indice();
                if (indice.LoadByPrimaryKey((short)idIndice.Value))
                {
                    if (percentual.HasValue)
                    {
                        descricaoCompleta.Append(String.Format("{0:n}", percentual.Value) + "% ");
                    }
                    else
                    {
                        descricaoCompleta.Append("100% ");
                    }

                    descricaoCompleta.Append(indice.Descricao);
                }
            }

            if (taxa.HasValue && taxa.Value != 0)
            {
                if (idIndice.HasValue)
                {
                    descricaoCompleta.Append(" + " + String.Format("{0:n}", taxa.Value) + "%");
                }
                else
                {
                    descricaoCompleta.Append("Tx = " + String.Format("{0:n}", taxa.Value) + "%");
                }
            }

            return descricaoCompleta.ToString().Replace("/", "-");
        }

        public string RetornaDescricaoTitulo(string descricao, DateTime dataVencimento, decimal? taxa, decimal? idIndice, decimal? percentual)
        {
            StringBuilder descricaoCompleta = new StringBuilder();
            descricaoCompleta.Append(descricao.Trim() + " - ");
            descricaoCompleta.Append(" Vcto: ");
            descricaoCompleta.Append(dataVencimento.ToShortDateString());

            if (idIndice.HasValue || (taxa.HasValue && taxa.Value != 0))
            {
                descricaoCompleta.Append(" -> ");
            }

            if (idIndice.HasValue)
            {
                Indice indice = new Indice();
                if (indice.LoadByPrimaryKey((short)idIndice.Value))
                {
                    if (percentual.HasValue)
                    {
                        descricaoCompleta.Append(String.Format("{0:n}", percentual.Value) + "% ");
                    }
                    else
                    {
                        descricaoCompleta.Append("100% ");
                    }

                    descricaoCompleta.Append(indice.Descricao);
                }
            }

            if (taxa.HasValue && taxa.Value != 0)
            {
                if (idIndice.HasValue)
                {
                    descricaoCompleta.Append(" + " + String.Format("{0:n}", taxa.Value) + "%");
                }
                else
                {
                    descricaoCompleta.Append("Tx = " + String.Format("{0:n}", taxa.Value) + "%");
                }
            }

            return descricaoCompleta.ToString().Replace("/", "-");
        }

        public string RetornaDescricaoTitulo(TituloRendaFixa tituloRendaFixa)
        {
            return this.RetornaDescricaoTitulo(tituloRendaFixa.Descricao, tituloRendaFixa.DataVencimento.Value, tituloRendaFixa.Taxa, tituloRendaFixa.IdIndice, tituloRendaFixa.Percentual);
        }

        /// <summary>
        /// Retorna o prazo médio em dias corridos, ponderado pelo valor nominal (se houver) de cada fluxo e vencimento do titulo.
        /// </summary>
        /// <param name="idTitulo"></param>
        /// <param name="dataReferencia"></param>
        /// <returns></returns>
        public decimal RetornaPrazoMedioTitulo(int idTitulo, DateTime dataReferencia)
        {
            decimal prazoMedio = 0;

            TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
            tituloRendaFixa.LoadByPrimaryKey(idTitulo);

            PapelRendaFixa papelRendaFixa = new PapelRendaFixa();
            papelRendaFixa.LoadByPrimaryKey(tituloRendaFixa.IdPapel.Value);

            decimal puNominal = tituloRendaFixa.PUNominal.Value;

            decimal puNominalAmortizado = puNominal;
            #region Ajusta o PU nominal pelas amortizações ocorridas
            AgendaRendaFixaCollection agendaRendaFixaCollectionAmortizacao = new AgendaRendaFixaCollection();
            agendaRendaFixaCollectionAmortizacao.Query.Select(agendaRendaFixaCollectionAmortizacao.Query.Taxa,
                                                              agendaRendaFixaCollectionAmortizacao.Query.Valor);
            agendaRendaFixaCollectionAmortizacao.Query.Where(agendaRendaFixaCollectionAmortizacao.Query.IdTitulo.Equal(idTitulo),
                                                             agendaRendaFixaCollectionAmortizacao.Query.TipoEvento.In((byte)TipoEventoTitulo.Amortizacao),
                                                             agendaRendaFixaCollectionAmortizacao.Query.DataPagamento.LessThanOrEqual(dataReferencia));

            agendaRendaFixaCollectionAmortizacao.Query.Load();

            foreach (AgendaRendaFixa agendaRendaFixaAmortizacao in agendaRendaFixaCollectionAmortizacao)
            {
                if (agendaRendaFixaAmortizacao.Taxa.HasValue && agendaRendaFixaAmortizacao.Taxa.Value != 0)
                {
                    decimal taxaAmortizacao = agendaRendaFixaAmortizacao.Taxa.Value;
                    puNominalAmortizado -= puNominal * taxaAmortizacao / 100M;
                }
                else if (agendaRendaFixaAmortizacao.Valor.HasValue && agendaRendaFixaAmortizacao.Valor.Value != 0)
                {
                    puNominalAmortizado -= agendaRendaFixaAmortizacao.Valor.Value; ;
                }
            }
            #endregion

            AgendaRendaFixaCollection agendaRendaFixaCollection = new AgendaRendaFixaCollection();
            agendaRendaFixaCollection.Query.Select(agendaRendaFixaCollection.Query.DataEvento,
                                                   agendaRendaFixaCollection.Query.DataPagamento,
                                                   agendaRendaFixaCollection.Query.TipoEvento,
                                                   agendaRendaFixaCollection.Query.Valor,
                                                   agendaRendaFixaCollection.Query.Taxa);
            agendaRendaFixaCollection.Query.Where(agendaRendaFixaCollection.Query.IdTitulo.Equal(idTitulo),
                                                  agendaRendaFixaCollection.Query.DataEvento.GreaterThan(dataReferencia),
                                                  agendaRendaFixaCollection.Query.TipoEvento.NotEqual((byte)TipoEventoTitulo.PagamentoCorrecao));
            agendaRendaFixaCollection.Query.OrderBy(agendaRendaFixaCollection.Query.DataEvento.Ascending,
                                                    agendaRendaFixaCollection.Query.IdAgenda.Ascending);
            agendaRendaFixaCollection.Query.Load();

            if (agendaRendaFixaCollection.Count > 0)
            {
                #region Cálculo do prazo médio baseado no fluxo de eventos
                DateTime dataUltima = agendaRendaFixaCollection[agendaRendaFixaCollection.Count - 1].DataEvento.Value;

                DiasNaoUteisContainer diasNaoUteisContainer = new DiasNaoUteisContainer();
                if (papelRendaFixa.BaseAno.Value == (int)BaseCalculo.Base252)
                {
                    diasNaoUteisContainer = new DiasNaoUteisContainer((int)LocalFeriadoFixo.Brasil, dataReferencia, dataUltima);
                }

                decimal totalValorFluxo = 0;
                decimal totalFluxoDias = 0;
                foreach (AgendaRendaFixa agendaRendaFixa in agendaRendaFixaCollection)
                {
                    DateTime dataEvento = agendaRendaFixa.DataEvento.Value;
                    DateTime dataPagamento = agendaRendaFixa.DataPagamento.Value;

                    decimal valorFluxo = 0;
                    #region Calcula valor do fluxo
                    if (agendaRendaFixa.TipoEvento.Value == (byte)TipoEventoTitulo.Amortizacao)
                    {
                        decimal valorAmortizacao = 0;
                        if (agendaRendaFixa.Taxa.HasValue && agendaRendaFixa.Taxa.Value != 0)
                        {
                            valorAmortizacao = puNominal * agendaRendaFixa.Taxa.Value / 100M;
                        }
                        else
                        {
                            valorAmortizacao = agendaRendaFixa.Valor.Value;
                        }

                        valorFluxo = valorAmortizacao;
                        puNominalAmortizado -= valorAmortizacao;
                    }
                    else
                    {
                        decimal valorJuro = 0;
                        if (agendaRendaFixa.Taxa.HasValue && agendaRendaFixa.Taxa.Value != 0)
                        {
                            DateTime dataBaseJuros = tituloRendaFixa.DataEmissao.Value;
                            #region Acha data base do último pagamento de cupom de juros
                            AgendaRendaFixaCollection agendaRendaFixaCollectionJuro = new AgendaRendaFixaCollection();
                            agendaRendaFixaCollectionJuro.Query.Select(agendaRendaFixaCollectionJuro.Query.DataEvento);
                            agendaRendaFixaCollectionJuro.Query.Where(agendaRendaFixaCollectionJuro.Query.IdTitulo.Equal(tituloRendaFixa.IdTitulo.Value),
                                                                      agendaRendaFixaCollectionJuro.Query.DataEvento.LessThan(dataEvento),
                                                                      agendaRendaFixaCollectionJuro.Query.TipoEvento.In((byte)TipoEventoTitulo.JurosCorrecao,
                                                                                                                        (byte)TipoEventoTitulo.Juros));
                            agendaRendaFixaCollectionJuro.Query.OrderBy(agendaRendaFixaCollectionJuro.Query.DataEvento.Descending);
                            agendaRendaFixaCollectionJuro.Query.Load();

                            if (agendaRendaFixaCollectionJuro.Count > 0)
                            {
                                dataBaseJuros = agendaRendaFixaCollectionJuro[0].DataEvento.Value;
                            }
                            #endregion

                            if (papelRendaFixa.ContagemDias.Value == (byte)ContagemDias.Uteis)
                            {
                                if (!Calendario.IsDiaUtil(dataEvento))
                                {
                                    dataEvento = Calendario.AdicionaDiaUtil(dataEvento, 1);
                                }
                            }

                            int numeroDias = 0;
                            if (papelRendaFixa.ContagemDias.Value == (byte)ContagemDias.Uteis)
                            {
                                numeroDias = Calendario.NumeroDias(dataBaseJuros, dataEvento, (int)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                            }
                            else if (papelRendaFixa.ContagemDias.Value == (byte)ContagemDias.Dias360)
                            {
                                numeroDias = Calendario.NumeroDias360(dataBaseJuros, dataEvento);
                            }
                            else
                            {
                                numeroDias = Calendario.NumeroDias(dataBaseJuros, dataEvento);
                            }

                            decimal fatorAno = agendaRendaFixa.Taxa.Value / 100M + 1;
                            decimal fatorPeriodo = (decimal)Math.Pow((double)fatorAno, (double)((double)numeroDias / (double)papelRendaFixa.BaseAno.Value));

                            valorJuro = puNominalAmortizado * (fatorPeriodo - 1M);
                        }
                        else
                        {
                            valorJuro = agendaRendaFixa.Valor.Value;
                        }

                        valorFluxo = valorJuro;
                    }
                    #endregion

                    int numeroDiasEvento = Calendario.NumeroDias(dataReferencia, dataEvento);

                    totalFluxoDias += valorFluxo * numeroDiasEvento;

                    totalValorFluxo += valorFluxo;
                }

                prazoMedio = totalFluxoDias / totalValorFluxo;
                #endregion
            }
            else
            {
                prazoMedio = Calendario.NumeroDias(dataReferencia, tituloRendaFixa.DataVencimento.Value);
            }

            return prazoMedio;
        }

        /// <summary>
        /// Indica se o emissor do título é empresa ligada (é o adm ou gestor do fundo - idCarteira passado).
        /// </summary>
        /// <param name="idTitulo"></param>
        /// <param name="idCarteira"></param>
        /// <returns></returns>
        public bool IndicaEmpresaLigada(int idTitulo, int idCarteira)
        {
            Carteira carteira = new Carteira();
            List<esQueryItem> campos = new List<esQueryItem>();
            campos.Add(carteira.Query.IdAgenteAdministrador);
            campos.Add(carteira.Query.IdAgenteGestor);
            carteira.LoadByPrimaryKey(campos, idCarteira);

            TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
            campos = new List<esQueryItem>();
            campos.Add(tituloRendaFixa.Query.IdEmissor);
            tituloRendaFixa.LoadByPrimaryKey(campos, idTitulo);

            Emissor emissor = new Emissor();
            campos = new List<esQueryItem>();
            campos.Add(emissor.Query.IdAgente);
            emissor.LoadByPrimaryKey(campos, tituloRendaFixa.IdEmissor.Value);

            bool empresaLigada = emissor.IdAgente.HasValue && (emissor.IdAgente.Value == carteira.IdAgenteAdministrador.Value ||
                                                               emissor.IdAgente.Value == carteira.IdAgenteGestor.Value);

            return empresaLigada;
        }

        /// <summary>
        /// Retorna a duration (em anos e calculado por dias corridos), ponderado pelo valor atual (se houver) de cada fluxo e vencimento do titulo.
        /// </summary>
        /// <param name="idTitulo"></param>
        /// <param name="dataReferencia"></param>
        /// <returns></returns>
        public void RetornaDurationTitulo(int idTitulo, DateTime dataReferencia, out decimal tir, out decimal macaulayDuration, out decimal modifiedDuration)
        {
            decimal duration = 0;

            TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
            tituloRendaFixa.LoadByPrimaryKey(idTitulo);

            if (dataReferencia > tituloRendaFixa.DataVencimento.Value)
            {
                throw new Exception("Data de vencimento do título " + idTitulo.ToString() + " menor que a data de referência passada.");
            }

            PapelRendaFixa papelRendaFixa = new PapelRendaFixa();
            papelRendaFixa.LoadByPrimaryKey(tituloRendaFixa.IdPapel.Value);

            AgendaRendaFixaCollection agendaRendaFixaCollection = new AgendaRendaFixaCollection();
            agendaRendaFixaCollection.Query.Select(agendaRendaFixaCollection.Query.DataEvento,
                                                   agendaRendaFixaCollection.Query.DataPagamento,
                                                   agendaRendaFixaCollection.Query.TipoEvento,
                                                   agendaRendaFixaCollection.Query.Valor,
                                                   agendaRendaFixaCollection.Query.Taxa);
            agendaRendaFixaCollection.Query.Where(agendaRendaFixaCollection.Query.IdTitulo.Equal(idTitulo),
                                                  agendaRendaFixaCollection.Query.DataEvento.GreaterThan(dataReferencia),
                                                  agendaRendaFixaCollection.Query.TipoEvento.NotEqual((byte)TipoEventoTitulo.PagamentoCorrecao));
            agendaRendaFixaCollection.Query.OrderBy(agendaRendaFixaCollection.Query.DataEvento.Ascending,
                                                    agendaRendaFixaCollection.Query.IdAgenda.Ascending);
            agendaRendaFixaCollection.Query.Load();

            bool existeFluxoDesconto = agendaRendaFixaCollection.Count > 0;

            tir = 0;
            decimal durationDias = 0;
            if (existeFluxoDesconto)
            {
                decimal puNominal = tituloRendaFixa.PUNominal.Value;
                decimal puNominalAmortizado = puNominal;
                #region Ajusta o PU nominal pelas amortizações ocorridas
                AgendaRendaFixaCollection agendaRendaFixaCollectionAmortizacao = new AgendaRendaFixaCollection();
                agendaRendaFixaCollectionAmortizacao.Query.Select(agendaRendaFixaCollectionAmortizacao.Query.Taxa,
                                                                  agendaRendaFixaCollectionAmortizacao.Query.Valor);
                agendaRendaFixaCollectionAmortizacao.Query.Where(agendaRendaFixaCollectionAmortizacao.Query.IdTitulo.Equal(idTitulo),
                                                      agendaRendaFixaCollectionAmortizacao.Query.TipoEvento.In((byte)TipoEventoTitulo.Amortizacao),
                                                      agendaRendaFixaCollectionAmortizacao.Query.DataPagamento.LessThanOrEqual(dataReferencia));
                agendaRendaFixaCollectionAmortizacao.Query.Load();

                foreach (AgendaRendaFixa agendaRendaFixaAmortizacao in agendaRendaFixaCollectionAmortizacao)
                {
                    if (agendaRendaFixaAmortizacao.Taxa.HasValue && agendaRendaFixaAmortizacao.Taxa.Value != 0)
                    {
                        decimal taxaAmortizacao = agendaRendaFixaAmortizacao.Taxa.Value;
                        puNominalAmortizado -= puNominal * taxaAmortizacao / 100M;
                    }
                    else if (agendaRendaFixaAmortizacao.Valor.HasValue && agendaRendaFixaAmortizacao.Valor.Value != 0)
                    {
                        puNominalAmortizado -= agendaRendaFixaAmortizacao.Valor.Value; ;
                    }
                }
                #endregion

                decimal vpl = 0;
                decimal vplPonderado = 0;
                #region Cálculo com fluxo com taxa ou valor indicado
                DateTime dataUltima = agendaRendaFixaCollection[agendaRendaFixaCollection.Count - 1].DataEvento.Value;

                DiasNaoUteisContainer diasNaoUteisContainer = new DiasNaoUteisContainer();
                if (papelRendaFixa.BaseAno.Value == (int)BaseCalculo.Base252)
                {
                    diasNaoUteisContainer = new DiasNaoUteisContainer((int)LocalFeriadoFixo.Brasil, dataReferencia, dataUltima);
                }

                foreach (AgendaRendaFixa agendaRendaFixa in agendaRendaFixaCollection)
                {
                    DateTime dataEvento = agendaRendaFixa.DataEvento.Value;
                    DateTime dataPagamento = agendaRendaFixa.DataPagamento.Value;

                    decimal valorFluxo = 0;
                    #region Calcula valor do fluxo
                    if (agendaRendaFixa.TipoEvento.Value == (byte)TipoEventoTitulo.Amortizacao)
                    {
                        decimal valorAmortizacao = 0;
                        if (agendaRendaFixa.Taxa.HasValue && agendaRendaFixa.Taxa.Value != 0)
                        {
                            valorAmortizacao = puNominal * agendaRendaFixa.Taxa.Value / 100M;
                        }
                        else
                        {
                            valorAmortizacao = agendaRendaFixa.Valor.Value;
                        }

                        valorFluxo = valorAmortizacao;
                        puNominalAmortizado -= valorAmortizacao;
                    }
                    else
                    {
                        decimal valorJuro = 0;
                        if (agendaRendaFixa.Taxa.HasValue && agendaRendaFixa.Taxa.Value != 0)
                        {
                            DateTime dataBaseJuros = tituloRendaFixa.DataEmissao.Value;
                            #region Acha data base do último pagamento de cupom de juros
                            AgendaRendaFixaCollection agendaRendaFixaCollectionJuro = new AgendaRendaFixaCollection();
                            agendaRendaFixaCollectionJuro.Query.Select(agendaRendaFixaCollectionJuro.Query.DataEvento);
                            agendaRendaFixaCollectionJuro.Query.Where(agendaRendaFixaCollectionJuro.Query.IdTitulo.Equal(idTitulo),
                                                                      agendaRendaFixaCollectionJuro.Query.DataEvento.LessThan(dataEvento),
                                                                      agendaRendaFixaCollectionJuro.Query.TipoEvento.In((byte)TipoEventoTitulo.JurosCorrecao,
                                                                                                                        (byte)TipoEventoTitulo.Juros));
                            agendaRendaFixaCollectionJuro.Query.OrderBy(agendaRendaFixaCollectionJuro.Query.DataEvento.Descending);
                            agendaRendaFixaCollectionJuro.Query.Load();

                            if (agendaRendaFixaCollectionJuro.Count > 0)
                            {
                                dataBaseJuros = agendaRendaFixaCollectionJuro[0].DataEvento.Value;
                            }
                            #endregion

                            if (papelRendaFixa.ContagemDias.Value == (byte)ContagemDias.Uteis)
                            {
                                if (!Calendario.IsDiaUtil(dataEvento))
                                {
                                    dataEvento = Calendario.AdicionaDiaUtil(dataEvento, 1);
                                }
                            }

                            int numeroDias = 0;
                            if (papelRendaFixa.ContagemDias.Value == (byte)ContagemDias.Uteis)
                            {
                                numeroDias = Calendario.NumeroDias(dataBaseJuros, dataEvento, (int)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                            }
                            else if (papelRendaFixa.ContagemDias.Value == (byte)ContagemDias.Dias360)
                            {
                                numeroDias = Calendario.NumeroDias360(dataBaseJuros, dataEvento);
                            }
                            else
                            {
                                numeroDias = Calendario.NumeroDias(dataBaseJuros, dataEvento);
                            }

                            decimal fatorAno = agendaRendaFixa.Taxa.Value / 100M + 1;
                            decimal fatorPeriodo = (decimal)Math.Pow((double)fatorAno, (double)((double)numeroDias / (double)papelRendaFixa.BaseAno.Value));

                            valorJuro = puNominalAmortizado * (fatorPeriodo - 1M);
                        }
                        else if (agendaRendaFixa.Valor.HasValue && agendaRendaFixa.Valor.Value != 0)
                        {
                            valorJuro = agendaRendaFixa.Valor.Value;
                        }

                        valorFluxo = valorJuro;
                    }
                    #endregion

                    int numeroDiasDesconto = 0;
                    if (papelRendaFixa.BaseAno.Value == (int)BaseCalculo.Base252)
                    {
                        numeroDiasDesconto = diasNaoUteisContainer.CalculaNumeroDias(dataReferencia, dataEvento);
                    }
                    else if (papelRendaFixa.BaseAno.Value == (int)BaseCalculo.Base360)
                    {
                        numeroDiasDesconto = Calendario.NumeroDias360(dataReferencia, dataEvento);
                    }
                    else
                    {
                        numeroDiasDesconto = Calendario.NumeroDias(dataReferencia, dataEvento);
                    }

                    decimal fator = (decimal)Math.Pow((double)(1 + tir / 100M), (double)((decimal)numeroDiasDesconto / (int)papelRendaFixa.BaseAno.Value));
                    decimal valorPresente = valorFluxo / fator;

                    vpl += valorPresente;
                    vplPonderado += valorPresente * numeroDiasDesconto;
                }
                #endregion

                durationDias = vplPonderado / vpl;
            }

            if (papelRendaFixa.TipoRentabilidade.Value == (byte)TipoRentabilidadeTitulo.PosFixado && (!tituloRendaFixa.Taxa.HasValue || tituloRendaFixa.Taxa.Value == 0))
            {
                macaulayDuration = 0;
                modifiedDuration = 0;
            }
            else if (!existeFluxoDesconto)
            {
                int numeroDias = Calendario.NumeroDias(dataReferencia, tituloRendaFixa.DataVencimento.Value, (int)LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);
                macaulayDuration = numeroDias / 252M;
                modifiedDuration = macaulayDuration / (1M + tituloRendaFixa.Taxa.Value / 100M);
                tir = tituloRendaFixa.Taxa.Value;
            }
            else
            {
                macaulayDuration = durationDias / 252M;
                modifiedDuration = macaulayDuration / (1M + tir / 100M);
            }
        }

        /// <summary>
        /// Cria um titulo em TituloRendaFixa e chama o metodo de estrutura de fluxos a partir dos parametros passados.
        /// </summary>
        /// <param name="codigoSelic"></param>
        /// <param name="dataVencimento"></param>
        /// <param name="dataReferencia"></param>
        public void CriaTituloPublico(int codigoSelic, DateTime dataVencimento, DateTime dataReferencia, DateTime? dataEmissaoTitulo)
        {
            short? idIndice = null;
            decimal percentual = 0;
            decimal taxa = 0;
            DateTime dataEmissao = new DateTime();

            string codigoInterno = codigoSelic.ToString().Substring(0, 4);

            string descricaoTitulo = "";

            if (codigoSelic == 100000)
            {
                descricaoTitulo = "LTN";
            }
            else if (codigoSelic == 210100)
            {
                descricaoTitulo = "LFT";
            }
            else if (codigoSelic == 770100)
            {
                descricaoTitulo = "NTN-C";
            }
            else if (codigoInterno == "9501")
            {
                descricaoTitulo = "NTN-F";
            }
            else if (codigoInterno == "7601")
            {
                if (codigoSelic == 760198)
                {
                    descricaoTitulo = "NTNB PRINC";
                }
                else
                {
                    descricaoTitulo = "NTN-B";
                }
            }
            
            if (descricaoTitulo == "LTN")
            {
                dataEmissao = dataEmissaoTitulo.HasValue ? dataEmissaoTitulo.Value : dataReferencia;                
            }
            else if (descricaoTitulo == "NTN-C")
            {
                idIndice = (short)ListaIndiceFixo.IGPM;
                percentual = 100;
                taxa = 6M;
                dataEmissao = dataEmissaoTitulo.HasValue ? dataEmissaoTitulo.Value : new DateTime(2000, 07, 01);                
            }
            else if (descricaoTitulo == "NTN-B" || descricaoTitulo == "NTNB PRINC")
            {
                idIndice = (short)ListaIndiceFixo.IPCA;
                percentual = 100;
                dataEmissao = dataEmissaoTitulo.HasValue ? dataEmissaoTitulo.Value : new DateTime(2000, 07, 15);
                taxa = 6M;                
            }
            else if (descricaoTitulo == "NTN-F")
            {
                dataEmissao = dataEmissaoTitulo.HasValue ? dataEmissaoTitulo.Value : dataReferencia;                
            }
            else if (descricaoTitulo == "LFT")
            {
                idIndice = (short)ListaIndiceFixo.SELIC;
                percentual = 100;
                dataEmissao = dataEmissaoTitulo.HasValue ? dataEmissaoTitulo.Value : new DateTime(2000, 07, 01);                
            }

            PapelRendaFixa papelRendaFixa = new PapelRendaFixa();
            int idPapel = papelRendaFixa.RetornaPapelPublico(codigoSelic);

            this.Descricao = descricaoTitulo;

            this.IdEmissor = (int)ListaEmissorFixo.TesouroNacional;
            this.IdMoeda = (int)ListaMoedaFixo.Real;

            EstrategiaCollection estrategiaCollection = new EstrategiaCollection();
            estrategiaCollection.Query.Select(estrategiaCollection.Query.IdEstrategia);
            estrategiaCollection.Query.Where(estrategiaCollection.Query.Descricao.Like("%Renda Fixa%"));
            estrategiaCollection.Query.Load();

            if (estrategiaCollection.Count > 0)
            {
                this.IdEstrategia = estrategiaCollection[0].IdEstrategia.Value;
            }

            this.IdPapel = idPapel;
            this.DataEmissao = dataEmissao;

            this.CodigoIsin = ""; //Hoje não tem como saber!!!!
            this.CodigoCDA = 0; //Hoje não tem como saber!!!!

            this.CodigoCustodia = codigoSelic.ToString();
            this.DataVencimento = dataVencimento;
            this.IdIndice = idIndice;
            this.IsentoIOF = (byte)TituloIsentoIOF.NaoIsento;
            this.IsentoIR = (byte)TituloIsentoIR.NaoIsento;
            this.Percentual = percentual;
            this.PUNominal = 1000;
            this.Taxa = taxa;
            this.ValorNominal = 1000;            

            TituloRendaFixa tituloDescricao = new TituloRendaFixa();
            this.DescricaoCompleta = tituloDescricao.RetornaDescricaoTitulo(this.Descricao, this.DataVencimento.Value, this.Taxa, this.IdIndice, this.Percentual);

            this.Save();

            if (descricaoTitulo == "NTN-F" || descricaoTitulo == "NTN-C" || descricaoTitulo == "NTN-B")
            {
                AgendaRendaFixa agendaRendaFixa = new AgendaRendaFixa();
                agendaRendaFixa.CriaAgendaFluxosTituloPublico(this.IdTitulo.Value, descricaoTitulo, dataEmissao, dataVencimento);
            }
        }

        public bool BuscaPorCodigoIsin(string codigoIsin)
        {
            this.Query.Where(this.Query.CodigoIsin.Equal(codigoIsin));
            return this.Load(this.query);
        }

        public bool BuscaPorCodigoCetip(string codigoCetip)
        {
            this.Query.Where(this.Query.CodigoCetip.Equal(codigoCetip));
            return this.Load(this.query);
        }

        public void RetornaQuantidadeAbertura(int idCliente, DateTime dataReferencia, out decimal quantidadeAbertura, out decimal quantidadeBloqueadaAbertura)
        {
            //Buscar todas as posicoes de abertura que este cliente possui para este titulo
            PosicaoRendaFixaAberturaCollection posicaoRendaFixaAberturaCollection = new PosicaoRendaFixaAberturaCollection();
            posicaoRendaFixaAberturaCollection.Query.Select(posicaoRendaFixaAberturaCollection.Query.Quantidade.Sum(),
                posicaoRendaFixaAberturaCollection.Query.QuantidadeBloqueada.Sum());
            posicaoRendaFixaAberturaCollection.Query.Where(posicaoRendaFixaAberturaCollection.Query.DataHistorico.Equal(dataReferencia),
                posicaoRendaFixaAberturaCollection.Query.IdCliente.Equal(idCliente),
                posicaoRendaFixaAberturaCollection.Query.IdTitulo.Equal(this.IdTitulo));

            quantidadeAbertura = 0;
            quantidadeBloqueadaAbertura = 0;

            if (posicaoRendaFixaAberturaCollection.Query.Load() && posicaoRendaFixaAberturaCollection[0].Quantidade.HasValue)
            {
                quantidadeAbertura = posicaoRendaFixaAberturaCollection[0].Quantidade.Value;
                quantidadeBloqueadaAbertura = posicaoRendaFixaAberturaCollection[0].QuantidadeBloqueada.Value;
            }

        }
                
        public decimal RetornaQuantidadeDisponivel(int idCliente, DateTime dataReferencia, int? idOperacaoIgnorar)
        {
            decimal quantidadeDisponivel = 0;

            decimal quantidadeAbertura, quantidadeBloqueadaAbertura;
            RetornaQuantidadeAbertura(idCliente, dataReferencia, out quantidadeAbertura, out quantidadeBloqueadaAbertura);

            //Carregar todas as operações que o cliente realizou nesta data, para este titulo
            OperacaoRendaFixaCollection operacaoRendaFixaCollection = new OperacaoRendaFixaCollection();
            operacaoRendaFixaCollection.Query.Select(operacaoRendaFixaCollection.Query.Quantidade, operacaoRendaFixaCollection.Query.TipoOperacao);
            operacaoRendaFixaCollection.Query.Where(operacaoRendaFixaCollection.Query.DataOperacao.Equal(dataReferencia),
                operacaoRendaFixaCollection.Query.IdCliente.Equal(idCliente),
                operacaoRendaFixaCollection.Query.IdTitulo.Equal(this.IdTitulo));

            if (idOperacaoIgnorar.HasValue)
            {
                operacaoRendaFixaCollection.Query.Where(operacaoRendaFixaCollection.Query.IdOperacao.NotEqual(idOperacaoIgnorar.Value));
            }

            operacaoRendaFixaCollection.Query.Load();

            decimal quantidadeCompradaNaData = 0, quantidadeVendidaNaData = 0;
            foreach (OperacaoRendaFixa operacaoRendaFixa in operacaoRendaFixaCollection)
            {
                if (
                    operacaoRendaFixa.TipoOperacao == (byte)TipoOperacaoTitulo.AntecipacaoRecompra ||
                    operacaoRendaFixa.TipoOperacao == (byte)TipoOperacaoTitulo.CompraCasada ||
                    operacaoRendaFixa.TipoOperacao == (byte)TipoOperacaoTitulo.CompraFinal ||
                    operacaoRendaFixa.TipoOperacao == (byte)TipoOperacaoTitulo.CompraRevenda ||
                    operacaoRendaFixa.TipoOperacao == (byte)TipoOperacaoTitulo.IngressoAtivoImpactoCota ||
                    operacaoRendaFixa.TipoOperacao == (byte)TipoOperacaoTitulo.IngressoAtivoImpactoQtde ||
                    operacaoRendaFixa.TipoOperacao == (byte)TipoOperacaoTitulo.Deposito)
                {
                    quantidadeCompradaNaData += operacaoRendaFixa.Quantidade.Value;
                }
                else if (operacaoRendaFixa.TipoOperacao == (byte)TipoOperacaoTitulo.AntecipacaoRevenda ||
                    operacaoRendaFixa.TipoOperacao == (byte)TipoOperacaoTitulo.Retirada ||
                    operacaoRendaFixa.TipoOperacao == (byte)TipoOperacaoTitulo.VendaCasada ||
                    operacaoRendaFixa.TipoOperacao == (byte)TipoOperacaoTitulo.VendaFinal ||
                    operacaoRendaFixa.TipoOperacao == (byte)TipoOperacaoTitulo.RetiradaAtivoImpactoCota ||
                    operacaoRendaFixa.TipoOperacao == (byte)TipoOperacaoTitulo.RetiradaAtivoImpactoQtde ||
                    operacaoRendaFixa.TipoOperacao == (byte)TipoOperacaoTitulo.VendaRecompra)
                {
                    quantidadeVendidaNaData += operacaoRendaFixa.Quantidade.Value;
                }
                else if (operacaoRendaFixa.TipoOperacao == (byte)TipoOperacaoTitulo.VendaTotal)
                {
                    return 0; //Se existe uma venda total, o disponível é sempre zero
                }
                else
                {
                    throw new Exception("Tipo de operação não suportado");
                }
            }

            BloqueioRendaFixaCollection bloqueioRendaFixaCollection = new BloqueioRendaFixaCollection();
            bloqueioRendaFixaCollection.Query.Where(bloqueioRendaFixaCollection.Query.DataOperacao.Equal(dataReferencia),
                                                    bloqueioRendaFixaCollection.Query.IdCliente.Equal(idCliente),
                                                    bloqueioRendaFixaCollection.Query.IdTitulo.Equal(this.IdTitulo));
            bloqueioRendaFixaCollection.Query.Load();

            decimal quantidadeBloqueio = 0;
            decimal quantidadeDesbloqueio = 0;
            foreach (BloqueioRendaFixa bloqueioRendaFixa in bloqueioRendaFixaCollection)
            {
                decimal quantidade = bloqueioRendaFixa.Quantidade.Value;
                byte tipoBloqueio = bloqueioRendaFixa.TipoOperacao.Value;

                if (tipoBloqueio == (byte)TipoOperacaoBloqueio.Bloqueio)
                {
                    quantidadeBloqueio += quantidade;
                }
                else
                {
                    quantidadeDesbloqueio += quantidade;
                }
            }

            quantidadeDisponivel = (quantidadeAbertura - quantidadeBloqueadaAbertura) + 
                                   (quantidadeCompradaNaData - quantidadeVendidaNaData) +
                                   (quantidadeDesbloqueio - quantidadeBloqueio);

            return quantidadeDisponivel;
        }

        public decimal RetornaQuantidadeBloqueada(int idCliente, DateTime dataReferencia)
        {
            //Vejo na abertura quanto estava bloqueado e somo com operacoes de bloqueio do dia e subtraio das operacoes de desbloqueio do dia

            decimal quantidadeAbertura = 0;
            decimal quantidadeBloqueadaAbertura = 0;
            this.RetornaQuantidadeAbertura(idCliente, dataReferencia, out quantidadeAbertura, out quantidadeBloqueadaAbertura);

            BloqueioRendaFixaCollection bloqueioRendaFixaCollection = new BloqueioRendaFixaCollection();
            bloqueioRendaFixaCollection.Query.Where(bloqueioRendaFixaCollection.Query.DataOperacao.Equal(dataReferencia),
                                                    bloqueioRendaFixaCollection.Query.IdCliente.Equal(idCliente),
                                                    bloqueioRendaFixaCollection.Query.IdTitulo.Equal(this.IdTitulo));
            bloqueioRendaFixaCollection.Query.Load();

            decimal quantidadeBloqueio = 0;
            decimal quantidadeDesbloqueio = 0;
            foreach (BloqueioRendaFixa bloqueioRendaFixa in bloqueioRendaFixaCollection)
            {
                decimal quantidade = bloqueioRendaFixa.Quantidade.Value;
                byte tipoBloqueio = bloqueioRendaFixa.TipoOperacao.Value;

                if (tipoBloqueio == (byte)TipoOperacaoBloqueio.Bloqueio)
                {
                    quantidadeBloqueio += quantidade;
                }
                else
                {
                    quantidadeDesbloqueio += quantidade;
                }
            }

            return quantidadeBloqueadaAbertura + quantidadeBloqueio - quantidadeDesbloqueio;
        }

        public void ProcessaBloqueio(int idCliente, decimal quantidade)
        {
            PosicaoRendaFixaCollection posicoes = new PosicaoRendaFixaCollection();
            posicoes.Query.Where(posicoes.Query.IdCliente.Equal(idCliente),
                posicoes.Query.IdTitulo.Equal(this.IdTitulo));

            decimal quantidadeFaltaBloquear = quantidade;
            posicoes.Query.Load();
            if (posicoes.Count > 0)
            {
                foreach (PosicaoRendaFixa posicao in posicoes)
                {
                    //Descobrir quanto eu posso bloquear desta posicao
                    decimal quantidadeDisponivelPosicao = posicao.Quantidade.Value - posicao.QuantidadeBloqueada.Value;
                    if (quantidadeFaltaBloquear <= quantidadeDisponivelPosicao)
                    {
                        posicao.QuantidadeBloqueada += quantidadeFaltaBloquear;
                        quantidadeFaltaBloquear = 0;
                        break;
                    }
                    else
                    {
                        posicao.QuantidadeBloqueada += quantidadeDisponivelPosicao;
                        quantidadeFaltaBloquear -= quantidadeDisponivelPosicao;
                    }
                }

                //Se terminarmos o loop com quantidade a ser bloqueada, dar erro, pois foi vendida posicao bloqueada
                if (quantidadeFaltaBloquear > 0)
                {
                    throw new Exception(String.Format(
                        "Quantidade disponível do título não é suficiente para efetuar bloqueio (Cliente: {0} - Título: {1})", idCliente, IdTitulo.Value));
                }

                posicoes.Save();
            }
        }

        public void ProcessaDesbloqueio(int idCliente, decimal quantidade)
        {
            PosicaoRendaFixaCollection posicoes = new PosicaoRendaFixaCollection();
            posicoes.Query.Where(posicoes.Query.IdCliente.Equal(idCliente),
                posicoes.Query.IdTitulo.Equal(this.IdTitulo));

            decimal quantidadeFaltaDesbloquear = quantidade;
            posicoes.Query.Load();
            if (posicoes.Count > 0)
            {
                foreach (PosicaoRendaFixa posicao in posicoes)
                {
                    //Descobrir quanto eu posso desbloquear desta posicao
                    decimal quantidadeBloqueadaPosicao = posicao.QuantidadeBloqueada.Value;
                    if (quantidadeFaltaDesbloquear <= quantidadeBloqueadaPosicao)
                    {
                        posicao.QuantidadeBloqueada -= quantidadeFaltaDesbloquear;
                        quantidadeFaltaDesbloquear = 0;
                        break;
                    }
                    else
                    {
                        posicao.QuantidadeBloqueada = 0;
                        quantidadeFaltaDesbloquear -= quantidadeBloqueadaPosicao;
                    }
                }

                //Se terminarmos o loop com quantidade a ser bloqueada, dar erro, pois foi vendida posicao bloqueada
                if (quantidadeFaltaDesbloquear > 0)
                {
                    throw new Exception(String.Format(
                        "Quantidade bloqueada no título não é suficiente para efetuar desbloqueio (Cliente: {0} - Título: {1}", idCliente, IdTitulo.Value));
                }
                posicoes.Save();
            }
        }

        /// <summary>
        /// Importa, se for necessário, o titulo da Virtual. Se for necessário importa também o papel associado.
        /// </summary>
        /// <param name="codigoTitulo"></param>
        public void TrataTituloVirtual(string codigoTitulo)
        {
            TituloRendaFixaCollection tituloRendaFixaCollection = new TituloRendaFixaCollection();
            tituloRendaFixaCollection.Query.Where(tituloRendaFixaCollection.Query.CodigoInterface.Equal(codigoTitulo));
            tituloRendaFixaCollection.Query.Load();

            if (tituloRendaFixaCollection.Count > 0)
            {
                TituloRendaFixa titulo = tituloRendaFixaCollection[0];
                //Carrega o objeto
                foreach (esColumnMetadata colTituloCollection in titulo.es.Meta.Columns)
                {
                    esColumnMetadata colTitulo = this.es.Meta.Columns.FindByPropertyName(colTituloCollection.PropertyName);
                    this.SetColumn(colTitulo.Name, titulo.GetColumn(colTituloCollection.Name));                    
                }
                //

                return;
            }

            //Importa titulo da Virtual
            Optitulo optitulo = new Optitulo();
            optitulo.Query.es.Connection.Name = "VIRTUAL";
            optitulo.Query.Where(optitulo.Query.FcCodtitulo.Equal(codigoTitulo));
            if (!optitulo.Query.Load())
            {
                throw new Exception("Título não encontrado no sistema Virtual Open - " + codigoTitulo);
            }

            PapelRendaFixaCollection papelRendaFixaCollection = new PapelRendaFixaCollection();
            papelRendaFixaCollection.Query.Where(papelRendaFixaCollection.Query.CodigoInterface.Equal(optitulo.FcCodpapel));
            papelRendaFixaCollection.Query.Load();

            PapelRendaFixa papelRendaFixa = new PapelRendaFixa();
            if (papelRendaFixaCollection.Count > 0)
            {
                papelRendaFixa = papelRendaFixaCollection[0];
            }
            else
            {
                //IMPORTA PAPEL DA VIRTUAL
                Oppapel oppapel = new Oppapel();
                oppapel.Query.es.Connection.Name = "VIRTUAL";
                oppapel.Query.Where(oppapel.Query.FcCodpapel.Equal(optitulo.FcCodpapel));
                if (!oppapel.Query.Load())
                {
                    throw new Exception("Papel não encontrado no sistema Virtual Open - " + optitulo.FcCodpapel);
                }

                papelRendaFixa = new PapelRendaFixa();
                papelRendaFixa.BaseAno = Convert.ToInt32(oppapel.FcBasecalculo);
                papelRendaFixa.CasasDecimaisPU = Convert.ToByte(oppapel.FnDecimais.Value);

                //A SER AJUSTADO DEPOIS, ASSOCIANDO CORRETAMENTE AS CLASSES DA VIRTUAL COM AS CLASSES DO FINANCIAL
                papelRendaFixa.Classe = oppapel.FcRentabilidade == "PRE-FIXADO" ? (int)ClasseRendaFixa.Pre_Descontado : (int)ClasseRendaFixa.PosFixado;
                //************************************************************************************************

                papelRendaFixa.CodigoInterface = oppapel.FcCodpapel;

                if (oppapel.FcCriterio == "U")
                {
                    papelRendaFixa.ContagemDias = (byte)ContagemDiasTitulo.Uteis;
                }
                else
                {
                    papelRendaFixa.ContagemDias = (byte)ContagemDiasTitulo.Corridos;
                }


                papelRendaFixa.Descricao = oppapel.FcPapel;
                papelRendaFixa.PagamentoJuros = (byte)PagamentoJurosTitulo.ContaDias;
                papelRendaFixa.TipoCurva = oppapel.FcTipocurva == "EXPONENCIAL" ? (byte)TipoCurvaTitulo.Exponencial : (byte)TipoCurvaTitulo.Linear;
                papelRendaFixa.TipoCustodia = oppapel.FcCustodia == "SELIC" ? (byte)LocalCustodiaFixo.Selic : (byte)LocalCustodiaFixo.Cetip;
                papelRendaFixa.TipoPapel = oppapel.FcTipopapel == "PUBLICO" ? (byte)TipoPapelTitulo.Publico : (byte)TipoPapelTitulo.Privado;
                papelRendaFixa.TipoRentabilidade = oppapel.FcRentabilidade == "PRE-FIXADO" ? (byte)TipoRentabilidadeTitulo.PreFixado : (byte)TipoRentabilidadeTitulo.PosFixado;
                papelRendaFixa.TipoVolume = oppapel.FcTipovolume == "Q" ? (byte)TipoVolumeTitulo.Quantidade : (byte)TipoVolumeTitulo.Valor;

                papelRendaFixa.Save();
            }

            //IMPORTA TITULO DA VIRTUAL
            this.CodigoCetip = "";
            this.CodigoCustodia = optitulo.FcCodcetsel;
            this.CodigoInterface = codigoTitulo;
            this.CodigoIsin = optitulo.FcIsin;
            this.DataEmissao = optitulo.FdDatemissao.Value;
            this.DataVencimento = optitulo.FdDatvencimento.Value;
            this.DebentureConversivel = "N";
            this.Descricao = optitulo.FcNometitulo;

            int idEmissor = 0;
            #region Trata IdEmissor - Exception se não achar pelo CodigoInterface
            EmissorCollection emissorCollection = new EmissorCollection();
            emissorCollection.Query.Select(emissorCollection.Query.IdEmissor);
            emissorCollection.Query.Where(emissorCollection.Query.CodigoInterface.Equal(optitulo.FcCodemissor));
            emissorCollection.Query.Load();

            if (emissorCollection.Count > 0)
            {
                idEmissor = emissorCollection[0].IdEmissor.Value;
            }
            else
            {
                string nomeEmissor = "";
                Gbcliente gbcliente = new Gbcliente();
                gbcliente.es.Connection.Name = "VIRTUAL";

                if (gbcliente.LoadByPrimaryKey(optitulo.FcCodemissor))
                {
                    nomeEmissor = gbcliente.FcAbreviado;
                }
                
                throw new Exception("Emissor " + nomeEmissor + " não cadastrado no Financial com o código de emissor no Virtual - " + optitulo.FcCodemissor);
            }
            #endregion
            this.IdEmissor = idEmissor;

            short? idIndice = null;
            #region Trata IdIndice - Exception se não achar pela listagem fixa de indices disponiveis
            if (optitulo.FcIndice != "")
            {
                if (optitulo.FcIndice == "CDI")
                {
                    idIndice = (short)ListaIndiceFixo.CDI;
                }
                else if (optitulo.FcIndice == "DOLAR")
                {
                    idIndice = (short)ListaIndiceFixo.PTAX_800VENDA;
                }
                else if (optitulo.FcIndice == "IGPM" || optitulo.FcIndice == "IGP-M")
                {
                    idIndice = (short)ListaIndiceFixo.IGPM;
                }
                else if (optitulo.FcIndice == "IPCA" || optitulo.FcIndice == "IPC-A")
                {
                    idIndice = (short)ListaIndiceFixo.IPCA;
                }
                else if (optitulo.FcIndice == "IGPDI" || optitulo.FcIndice == "IGP-DI")
                {
                    idIndice = (short)ListaIndiceFixo.IGPDI;
                }
                else if (optitulo.FcIndice == "LFT")
                {
                    idIndice = (short)ListaIndiceFixo.SELIC;
                }
                else if (optitulo.FcIndice == "ANBID")
                {
                    idIndice = (short)ListaIndiceFixo.ANBID;
                }
                else if (optitulo.FcIndice == "TR")
                {
                    idIndice = (short)ListaIndiceFixo.TR;
                }
                else
                {
                    throw new Exception("Indice desconhecido na importação do sistema Virtual - " + optitulo.FcIndice);
                }
            }
            #endregion
            this.IdIndice = idIndice;

            this.IdMoeda = (int)ListaMoedaFixo.Real;
            this.IdPapel = papelRendaFixa.IdPapel.Value;
            this.IsentoIOF = (byte)TituloIsentoIOF.NaoIsento; //NÃO TEM INFORMAÇÃO NO VIRTUAL
            this.IsentoIR = (byte)TituloIsentoIR.NaoIsento; //NÃO TEM INFORMAÇÃO NO VIRTUAL

            if (optitulo.FnPercentind1.HasValue)
            {
                this.Percentual = (decimal)optitulo.FnPercentind1.Value;
            }

            this.PUNominal = (decimal)optitulo.FnNominal.Value;

            if (optitulo.FnSpread.HasValue)
            {
                this.Taxa = (decimal)optitulo.FnSpread.Value;
            }

            this.ValorNominal = 0;

            this.DescricaoCompleta = this.RetornaDescricaoTitulo(this.Descricao, this.DataVencimento.Value, this.Taxa, this.IdIndice, this.Percentual);

            int? idEstrategia = null;
            #region Verifica estrategia
            if (this.IdIndice.HasValue && this.IdIndice.Value == (short)ListaIndiceFixo.CDI)
            {
                EstrategiaCollection estrategiaCollection = new EstrategiaCollection();
                estrategiaCollection.Query.Select(estrategiaCollection.Query.IdEstrategia);
                estrategiaCollection.Query.Where(estrategiaCollection.Query.Descricao.Like("%DI%"));
                estrategiaCollection.Query.Load();

                if (estrategiaCollection.Count > 0)
                {
                    idEstrategia = estrategiaCollection[0].IdEstrategia.Value;
                }
            }
            else
            {
                EstrategiaCollection estrategiaCollection = new EstrategiaCollection();
                estrategiaCollection.Query.Select(estrategiaCollection.Query.IdEstrategia);
                estrategiaCollection.Query.Where(estrategiaCollection.Query.Descricao.Like("%Fixa%"));
                estrategiaCollection.Query.Load();

                if (estrategiaCollection.Count > 0)
                {
                    idEstrategia = estrategiaCollection[0].IdEstrategia.Value;
                }
            }
            #endregion
            this.IdEstrategia = idEstrategia;

            this.Save();
        }

        
    }
}
