/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 23/09/2014 10:57:13
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Util;
using Financial.Common.Enums;

namespace Financial.RendaFixa
{
	public partial class PerfilMTMCollection : esPerfilMTMCollection
	{
        public PerfilMTMCollection obterPerfilMTMCollection(int IdPapel, int idGrupoPerfilMTM, DateTime date)
        {
            PerfilMTMQuery objPerfilMTMQuery = new PerfilMTMQuery();
            PerfilMTMCollection objPerfilMTMCollection = new PerfilMTMCollection();
            objPerfilMTMQuery.Where(objPerfilMTMQuery.IdPapel.Equal(IdPapel) 
                                    & objPerfilMTMQuery.DtReferencia.Equal(date)
                                    & objPerfilMTMQuery.IdGrupoPerfilMTM.Equal(idGrupoPerfilMTM));

            objPerfilMTMCollection.Load(objPerfilMTMQuery);

            return (objPerfilMTMCollection.Count > 0 ? objPerfilMTMCollection : new PerfilMTMCollection());
        }

        /// <summary>
        /// Executa a copia do perfil MTM para o Dia util Seguinte a data Atual (Apenas uma vez por dia, independente de Reprocessamento)
        /// </summary>
        /// <param name="dataBase"></param>
        /// <param name="dataAlvo"></param>
        /// <param name="btnResetar"></param>
        public void copiarPerfilMTM(DateTime? dataBase, DateTime? dataAlvo, bool btnResetar)
        {
            if (dataBase == null && dataAlvo == null)
                return;

            PerfilMTMCollection objPerfilMTMCollectionBase = new PerfilMTMCollection();
            PerfilMTMCollection objPerfilMTMCollectionAlvo = new PerfilMTMCollection();

            if (!dataBase.HasValue)
                dataBase = Calendario.SubtraiDiaUtil((DateTime)dataAlvo, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);

            if(!dataAlvo.HasValue)
                dataAlvo = Calendario.AdicionaDiaUtil((DateTime)dataBase, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);

            objPerfilMTMCollectionBase.Query.Where(objPerfilMTMCollectionBase.Query.DtReferencia == dataBase);
            objPerfilMTMCollectionBase.Load(objPerfilMTMCollectionBase.Query);

            objPerfilMTMCollectionAlvo.Query.Where(objPerfilMTMCollectionAlvo.Query.DtReferencia == dataAlvo);
            objPerfilMTMCollectionAlvo.Load(objPerfilMTMCollectionAlvo.Query);

            if (objPerfilMTMCollectionBase.Count == 0)
            {
                return;
            }
            else if (objPerfilMTMCollectionAlvo.Count > 0)
            {
                if (btnResetar)
                {
                    objPerfilMTMCollectionAlvo.MarkAllAsDeleted();
                    objPerfilMTMCollectionAlvo.Save();
                }
                else
                {
                    List<PerfilMTM> lstPerfilMTMBase = (List<PerfilMTM>)objPerfilMTMCollectionBase;
                    PerfilMTM perfilMTMBase = new PerfilMTM();

                    foreach (PerfilMTM perfilMTM in objPerfilMTMCollectionAlvo)
                    {
                        if (perfilMTM.IdOperacao.HasValue)
                        {
                            perfilMTMBase = lstPerfilMTMBase.Find(x => x.IdGrupoPerfilMTM == perfilMTM.IdGrupoPerfilMTM.Value && x.IdSerie == perfilMTM.IdSerie.Value && x.IdOperacao == perfilMTM.IdOperacao.Value);
                            if (perfilMTMBase != null && perfilMTMBase.IdOperacao > 0)
                                continue;
                        }

                        if (perfilMTM.IdTitulo.HasValue)
                        {
                            perfilMTMBase = lstPerfilMTMBase.Find(x => x.IdGrupoPerfilMTM == perfilMTM.IdGrupoPerfilMTM.Value && x.IdSerie == perfilMTM.IdSerie.Value && x.IdOperacao == null && x.IdTitulo == perfilMTM.IdTitulo.Value);
                            if (perfilMTMBase != null && perfilMTMBase.IdTitulo > 0)
                                continue;
                        }

                        perfilMTMBase = lstPerfilMTMBase.Find(x => x.IdGrupoPerfilMTM == perfilMTM.IdGrupoPerfilMTM.Value && x.IdSerie == perfilMTM.IdSerie.Value && x.IdOperacao == null && x.IdTitulo == null);
                        if (perfilMTMBase != null && perfilMTMBase.IdPapel > 0)
                            continue;

                        PerfilMTM objPerfilMTMClone = objPerfilMTMCollectionBase.AddNew();
                        objPerfilMTMClone.DtReferencia = perfilMTM.DtReferencia;
                        objPerfilMTMClone.IdOperacao = perfilMTM.IdOperacao;
                        objPerfilMTMClone.IdPapel = perfilMTM.IdPapel;
                        objPerfilMTMClone.IdSerie = perfilMTM.IdSerie;
                        objPerfilMTMClone.IdTitulo = perfilMTM.IdTitulo;
                        objPerfilMTMClone.IdGrupoPerfilMTM = perfilMTM.IdGrupoPerfilMTM;
                    }

                    objPerfilMTMCollectionAlvo.MarkAllAsDeleted();
                    objPerfilMTMCollectionAlvo.Save();
                }
            }

            objPerfilMTMCollectionAlvo.ClearData();

            foreach (PerfilMTM perfilMTM in objPerfilMTMCollectionBase)
            {
                PerfilMTM objPerfilMTMClone = objPerfilMTMCollectionAlvo.AddNew();
                objPerfilMTMClone.DtReferencia = dataAlvo;
                objPerfilMTMClone.IdOperacao = perfilMTM.IdOperacao;
                objPerfilMTMClone.IdPapel = perfilMTM.IdPapel;
                objPerfilMTMClone.IdSerie = perfilMTM.IdSerie;
                objPerfilMTMClone.IdTitulo = perfilMTM.IdTitulo;
                objPerfilMTMClone.IdGrupoPerfilMTM = perfilMTM.IdGrupoPerfilMTM;
            }

            objPerfilMTMCollectionAlvo.Save();
        }
	}
}
