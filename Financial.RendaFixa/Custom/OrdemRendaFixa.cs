using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.RendaFixa.Enums;

namespace Financial.RendaFixa
{
    public partial class OrdemRendaFixa : esOrdemRendaFixa
    {
        /// <summary>
        /// Carrega ordens para OperacaoRendaFixa.
        /// Altera o status de Liberado para Processado.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void CarregaOrdemRendaFixa(int idCliente, DateTime data)
        {
            OperacaoRendaFixaCollection operacaoRendaFixaCollection = new OperacaoRendaFixaCollection();

            OrdemRendaFixaCollection ordemRendaFixaCollection = new OrdemRendaFixaCollection();
            ordemRendaFixaCollection.Query.Where(ordemRendaFixaCollection.Query.IdCliente.Equal(idCliente),
                                                 ordemRendaFixaCollection.Query.DataOperacao.Equal(data),
                                                 ordemRendaFixaCollection.Query.Status.Equal((byte)StatusOrdemRendaFixa.Aprovado));
            ordemRendaFixaCollection.Query.Load();

            foreach (OrdemRendaFixa ordemRendaFixa in ordemRendaFixaCollection)
            {
                OperacaoRendaFixa operacaoRendaFixa = operacaoRendaFixaCollection.AddNew();
                operacaoRendaFixa.DataRegistro = ordemRendaFixa.DataOperacao;
                operacaoRendaFixa.DataLiquidacao = ordemRendaFixa.DataLiquidacao;
                operacaoRendaFixa.DataOperacao = ordemRendaFixa.DataOperacao;
                operacaoRendaFixa.DataVolta = ordemRendaFixa.DataVolta;
                operacaoRendaFixa.Fonte = (byte)FonteOperacaoTitulo.OrdemRendaFixa;
                operacaoRendaFixa.IdCliente = ordemRendaFixa.IdCliente;
                operacaoRendaFixa.IdOperacaoResgatada = ordemRendaFixa.IdOperacaoResgatada;
                operacaoRendaFixa.IdPosicaoResgatada = ordemRendaFixa.IdPosicaoResgatada;
                operacaoRendaFixa.IdTitulo = ordemRendaFixa.IdTitulo;
                operacaoRendaFixa.IdLiquidacao = ordemRendaFixa.IdLiquidacao;
                operacaoRendaFixa.IdCustodia = ordemRendaFixa.IdCustodia;
                operacaoRendaFixa.PUOperacao = ordemRendaFixa.PUOperacao;
                operacaoRendaFixa.PUVolta = ordemRendaFixa.PUVolta;
                operacaoRendaFixa.Quantidade = ordemRendaFixa.Quantidade;
                operacaoRendaFixa.TaxaOperacao = ordemRendaFixa.TaxaOperacao;
                operacaoRendaFixa.TaxaVolta = ordemRendaFixa.TaxaVolta;
                operacaoRendaFixa.TipoNegociacao = ordemRendaFixa.TipoNegociacao;
                operacaoRendaFixa.TipoOperacao = ordemRendaFixa.TipoOperacao;
                operacaoRendaFixa.Valor = ordemRendaFixa.Valor;
                operacaoRendaFixa.ValorVolta = ordemRendaFixa.ValorVolta;
                operacaoRendaFixa.IdTrader = ordemRendaFixa.IdTrader;
                operacaoRendaFixa.IdAgenteContraParte = ordemRendaFixa.IdAgenteContraParte;
                operacaoRendaFixa.IdCarteiraContraparte = ordemRendaFixa.IdCarteiraContraParte;
                operacaoRendaFixa.IdCategoriaMovimentacao = ordemRendaFixa.IdCategoriaMovimentacao;
                
                ordemRendaFixa.Status = (byte)StatusOrdemRendaFixa.Processado;
            }

            ordemRendaFixaCollection.Save();
            operacaoRendaFixaCollection.Save();
        }
    }
}
