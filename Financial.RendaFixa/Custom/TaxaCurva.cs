﻿/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 06/10/2014 11:56:41
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using System.Collections;

namespace Financial.RendaFixa
{
	public partial class TaxaCurva : esTaxaCurva
	{
        public TaxaCurva RetornaTaxaCurvaIgualOuAnterior(int idCurva, DateTime dataBase, DateTime dataFluxo, ref Hashtable hsTaxasCurvas)
        {
            TaxaCurvaCollection taxaCurvaColl = new TaxaCurvaCollection();
            TaxaCurva taxaCurva = new TaxaCurva();

            if (!hsTaxasCurvas.Contains(idCurva))
            {
                TaxaCurvaCollection taxaCurvaCollAux = taxaCurvaColl.obterTaxaCurvaCollection(dataBase, idCurva);

                if (taxaCurvaCollAux.Count > 0)
                    hsTaxasCurvas.Add(idCurva, taxaCurvaCollAux);
                else
                {
                    CurvaRendaFixa curvaSemTaxa = new CurvaRendaFixa();
                    curvaSemTaxa.LoadByPrimaryKey(idCurva);
                    throw new Exception(" Valores de Curva - " + curvaSemTaxa.Descricao + " não cadastrados!");
                }
            }

            taxaCurva = this.BuscaTaxaCurvaIgualOuAnterior(idCurva, dataBase, dataFluxo, ref hsTaxasCurvas);

            if (taxaCurva != null && taxaCurva.IdCurvaRendaFixa.HasValue && taxaCurva.IdCurvaRendaFixa.Value > 0)
                return taxaCurva;

            //Contingência
            taxaCurvaColl.Query.Where(taxaCurvaColl.Query.IdCurvaRendaFixa == idCurva,
                                      taxaCurvaColl.Query.DataBase == dataBase,
                                      taxaCurvaColl.Query.DataVertice <= dataFluxo)
                                      .OrderBy(taxaCurvaColl.Query.DataBase.Descending);
            taxaCurvaColl.Query.es.Top = 1;

            if (taxaCurvaColl.Load(taxaCurvaColl.Query))
                return taxaCurvaColl[0];

            return new TaxaCurva();
        }

        private TaxaCurva BuscaTaxaCurvaIgualOuAnterior(int idCurva, DateTime dataBase, DateTime dataFluxo, ref Hashtable hsTaxasCurvas)
        {
            List<TaxaCurva> lstTaxaCurva = (List<TaxaCurva>)((TaxaCurvaCollection)hsTaxasCurvas[idCurva]);
            TaxaCurva taxaCurva = new TaxaCurva();

            //taxaCurva = lstTaxaCurva.Find(x => x.DataVertice == dataFluxo);
            taxaCurva = lstTaxaCurva.Find(delegate(TaxaCurva x) { return x.DataVertice == dataFluxo; });
            if (taxaCurva != null && taxaCurva.IdCurvaRendaFixa > 0)
                return taxaCurva;

            //taxaCurva = lstTaxaCurva.FindLast(x => x.DataVertice < dataFluxo);
            taxaCurva = lstTaxaCurva.FindLast(delegate(TaxaCurva x) { return x.DataVertice < dataFluxo; });
            if (taxaCurva != null && taxaCurva.IdCurvaRendaFixa > 0)
                return taxaCurva;

            return new TaxaCurva();
        }

        public TaxaCurva RetornaTaxaCurvaPosterior(int idCurva, DateTime dataBase, DateTime dataFluxo, ref Hashtable hsTaxasCurvas)
        {
            TaxaCurvaCollection taxaCurvaColl = new TaxaCurvaCollection();
            TaxaCurva objTaxaCurva = new TaxaCurva();

            if (!hsTaxasCurvas.Contains(idCurva))
            {
                TaxaCurvaCollection taxaCurvaCollAux = taxaCurvaColl.obterTaxaCurvaCollection(dataBase, idCurva);

                if (taxaCurvaCollAux.Count > 0)
                    hsTaxasCurvas.Add(idCurva, taxaCurvaCollAux);
                else
                {
                    CurvaRendaFixa curvaSemTaxa = new CurvaRendaFixa();
                    curvaSemTaxa.LoadByPrimaryKey(idCurva);
                    throw new Exception(" Valores de Curva - " + curvaSemTaxa.Descricao + " não cadastrados!");
                }
            }

            objTaxaCurva = this.BuscaTaxaCurvaPosterior(idCurva, dataBase, dataFluxo, ref hsTaxasCurvas);

            if (objTaxaCurva != null && objTaxaCurva.IdCurvaRendaFixa.HasValue && objTaxaCurva.IdCurvaRendaFixa.Value > 0)
                return objTaxaCurva;

            taxaCurvaColl.Query.Where(taxaCurvaColl.Query.IdCurvaRendaFixa == idCurva,
                                      taxaCurvaColl.Query.DataBase == dataBase,
                                      taxaCurvaColl.Query.DataVertice > dataFluxo)
                                      .OrderBy(taxaCurvaColl.Query.DataBase.Descending);
            taxaCurvaColl.Query.es.Top = 1;

            if (taxaCurvaColl.Load(taxaCurvaColl.Query))
                return taxaCurvaColl[0];

            return new TaxaCurva();
        }

        private TaxaCurva BuscaTaxaCurvaPosterior(int idCurva, DateTime dataBase, DateTime dataFluxo, ref Hashtable hsTaxasCurvas)
        {
            TaxaCurvaCollection taxaCurvaColl = new TaxaCurvaCollection();
            TaxaCurva taxaCurva = new TaxaCurva();

            if (hsTaxasCurvas.Contains(idCurva))
            {
                List<TaxaCurva> lstTaxaCurva = (List<TaxaCurva>)((TaxaCurvaCollection)hsTaxasCurvas[idCurva]);

                //taxaCurva = lstTaxaCurva.Find(x => x.DataVertice == dataFluxo);
                taxaCurva = lstTaxaCurva.Find(delegate(TaxaCurva x) { return x.DataVertice == dataFluxo; });
                if (taxaCurva != null && taxaCurva.IdCurvaRendaFixa > 0)
                    return taxaCurva;

                //taxaCurva = lstTaxaCurva.Find(x => x.DataVertice > dataFluxo);
                taxaCurva = lstTaxaCurva.Find(delegate(TaxaCurva x) { return x.DataVertice > dataFluxo; });
                if (taxaCurva != null && taxaCurva.IdCurvaRendaFixa > 0)
                    return taxaCurva;
            }

            return new TaxaCurva();
        }
	}
}
