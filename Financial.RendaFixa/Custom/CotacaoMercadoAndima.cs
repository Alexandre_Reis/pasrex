﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.Util;
using Financial.Interfaces.Import.RendaFixa;

namespace Financial.RendaFixa
{
    public partial class CotacaoMercadoAndima : esCotacaoMercadoAndima
    {

        /// <summary>
        /// Carrega as cotações a partir do arquivo Andima
        /// </summary>
        /// <param name="data"></param>
        /// <param name="pathArquivo">path completo do Diretorio onde procurar o Arquivo</param>
        public void CarregaAndima(DateTime data, string pathArquivo)
        {            
            CotacaoMercadoAndimaCollection cotacaoMercadoAndimaCollectionAux = new CotacaoMercadoAndimaCollection();

            // Pega os registros da collection de Andima
            Andima andima = new Andima();
            AndimaCollection andimaCollection = andima.ProcessaAndima(data, pathArquivo);

            AndimaCollection andimaCollectionAux = new AndimaCollection();
            // Filtra a Collection somente tipoRegistro = Dados
            andimaCollectionAux.CollectionAndima = andimaCollection.CollectionAndima.FindAll(andima.FilterAndimaByDados);
            //

            this.CarregaAndima(andimaCollectionAux);
        }

        /// <summary>
        /// Carrega as cotações a partir do arquivo Andima
        /// </summary>
        /// <param name="data"></param>
        /// <param name="pathArquivo">path completo do Diretorio onde procurar o Arquivo</param>
        public void CarregaAndima(AndimaCollection andimaCollection)
        {
            CotacaoMercadoAndimaCollection cotacaoMercadoAndimaCollection = new CotacaoMercadoAndimaCollection();

            for (int i = 0; i < andimaCollection.CollectionAndima.Count; i++)
            {
                Andima andima = andimaCollection.CollectionAndima[i];

                try
                {
                    this.CadastraTituloPublico(andima);
                }
                catch { }

                #region Copia informações para CotacaoMercadoAndima
                CotacaoMercadoAndima cotacaoMercadoAndimaUpdate = new CotacaoMercadoAndima();
                if (cotacaoMercadoAndimaUpdate.LoadByPrimaryKey(andima.DataReferencia, andima.Descricao, andima.CodigoSelic, andima.DataEmissao, andima.DataVencimento))
                {
                    cotacaoMercadoAndimaUpdate.TaxaIndicativa = andima.TaxaIndicativa;
                    cotacaoMercadoAndimaUpdate.Pu = andima.Pu;

                    cotacaoMercadoAndimaUpdate.Save();
                }
                else
                {
                    CotacaoMercadoAndima cotacaoMercadoAndima = cotacaoMercadoAndimaCollection.AddNew();
                    cotacaoMercadoAndima.Descricao = andima.Descricao;
                    cotacaoMercadoAndima.DataReferencia = andima.DataReferencia;
                    cotacaoMercadoAndima.DataVencimento = andima.DataVencimento;
                    cotacaoMercadoAndima.DataEmissao = andima.DataEmissao;
                    cotacaoMercadoAndima.CodigoSELIC = andima.CodigoSelic;
                    cotacaoMercadoAndima.TaxaIndicativa = andima.TaxaIndicativa;
                    cotacaoMercadoAndima.Pu = andima.Pu;                    
                }
                #endregion
            }
            try
            {
                cotacaoMercadoAndimaCollection.Save();
            }
            catch
            {
                foreach (CotacaoMercadoAndima cotacaoMercadoAndima in cotacaoMercadoAndimaCollection.Errors)
                {
                    Console.WriteLine(cotacaoMercadoAndima.es.RowError);
                }
            }
        }

        private void CadastraTituloPublico(Andima andima)
        {
            TituloRendaFixa titulo = new TituloRendaFixa();
            titulo.Query.Where(titulo.Query.CodigoCustodia.Equal(andima.CodigoSelic),
                titulo.Query.DataVencimento.Equal(andima.DataVencimento),
                titulo.Query.DataEmissao.Equal(andima.DataEmissao));
            titulo.Query.es.Top = 1;
            if (!titulo.Load(titulo.Query))
            {
                titulo.CriaTituloPublico(Convert.ToInt32(andima.CodigoSelic), andima.DataVencimento, andima.DataReferencia, andima.DataEmissao);
            }
        }

        /// <summary>
        /// Carrega o objeto CotacaoMercadoAndima com o campo PU.        
        /// </summary>
        /// <param name="dataReferencia"></param>
        /// <param name="codigoSelic"></param>
        /// <param name="dataEmissao"></param>
        /// <param name="dataVencimento"></param>
        /// <returns>bool se achou registro.</returns>
        public bool BuscaPU(DateTime dataReferencia, string codigoSelic, DateTime dataEmissao, DateTime dataVencimento)
        {
            CotacaoMercadoAndimaCollection cotacaoMercadoAndimaCollection = new CotacaoMercadoAndimaCollection();

            cotacaoMercadoAndimaCollection.Query
                 .Select(cotacaoMercadoAndimaCollection.Query.Pu)
                 .Where(cotacaoMercadoAndimaCollection.Query.DataReferencia.Equal(dataReferencia),
                        cotacaoMercadoAndimaCollection.Query.CodigoSELIC.Equal(codigoSelic),
                        cotacaoMercadoAndimaCollection.Query.DataEmissao.Equal(dataEmissao),
                        cotacaoMercadoAndimaCollection.Query.DataVencimento.Equal(dataVencimento));

            cotacaoMercadoAndimaCollection.Query.Load();

            if (cotacaoMercadoAndimaCollection.HasData)
            {
                // Copia informações de cotacaoMercadoAndimaCollection para CotacaoMercadoAndima
                this.AddNew();
                this.Pu = (cotacaoMercadoAndimaCollection[0]).Pu;
            }

            return cotacaoMercadoAndimaCollection.HasData;
        }

        /// <summary>
        /// Carrega o objeto CotacaoMercadoAndima com o campo PU.        
        /// </summary>
        /// <param name="dataReferencia"></param>
        /// <param name="codigoSelic"></param>
        /// <param name="dataEmissao"></param>
        /// <param name="dataVencimento"></param>
        /// <returns>bool se achou registro.</returns>
        public bool BuscaPU(DateTime dataReferencia, string codigoSelic, DateTime dataVencimento)
        {
            CotacaoMercadoAndimaCollection cotacaoMercadoAndimaCollection = new CotacaoMercadoAndimaCollection();

            cotacaoMercadoAndimaCollection.Query
                 .Select(cotacaoMercadoAndimaCollection.Query.Pu)
                 .Where(cotacaoMercadoAndimaCollection.Query.DataReferencia.Equal(dataReferencia),
                        cotacaoMercadoAndimaCollection.Query.CodigoSELIC.Equal(codigoSelic),
                        cotacaoMercadoAndimaCollection.Query.DataVencimento.Equal(dataVencimento));

            cotacaoMercadoAndimaCollection.Query.Load();

            if (cotacaoMercadoAndimaCollection.Count > 0)
            {
                // Copia informações de cotacaoMercadoAndimaCollection para CotacaoMercadoAndima
                this.AddNew();
                this.Pu = (cotacaoMercadoAndimaCollection[0]).Pu;
            }

            return cotacaoMercadoAndimaCollection.Count > 0;
        }
    }
}
