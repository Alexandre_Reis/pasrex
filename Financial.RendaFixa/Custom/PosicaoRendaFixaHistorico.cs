﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using log4net;
using Financial.RendaFixa.Enums;
using Financial.Common;
using Financial.Common.Enums;

namespace Financial.RendaFixa {
    public partial class PosicaoRendaFixaHistorico : esPosicaoRendaFixaHistorico {
        
        /// <summary>
        /// Retorna a soma do valor de mercado de todas as posições do cliente.
        /// Retorna 0 se não houver registros
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="tipoPapelTitulo">enum do tipo de papel (publico ou privado)</param>
        /// <param name="dataHistorico"></param>
        /// <returns>soma do valor de mercado das posições</returns>
        public decimal RetornaValorMercado(int idCliente, TipoPapelTitulo tipoPapelTitulo, DateTime dataHistorico) 
        {
            PosicaoRendaFixaHistoricoQuery posicaoRendaFixaHistorico = new PosicaoRendaFixaHistoricoQuery("P");
            TituloRendaFixaQuery tituloRendaFixa = new TituloRendaFixaQuery("T");
            PapelRendaFixaQuery papelRendaFixa = new PapelRendaFixaQuery("R");

            posicaoRendaFixaHistorico.Select(posicaoRendaFixaHistorico.ValorMercado.Sum());
            posicaoRendaFixaHistorico.InnerJoin(tituloRendaFixa).On(posicaoRendaFixaHistorico.IdTitulo == tituloRendaFixa.IdTitulo);
            posicaoRendaFixaHistorico.InnerJoin(papelRendaFixa).On(tituloRendaFixa.IdPapel == papelRendaFixa.IdPapel);
            posicaoRendaFixaHistorico.Where(posicaoRendaFixaHistorico.IdCliente == idCliente,
                                    posicaoRendaFixaHistorico.DataHistorico.Equal(dataHistorico),
                                    papelRendaFixa.TipoPapel == (int)tipoPapelTitulo);

            this.QueryReset();
            this.Load(posicaoRendaFixaHistorico);

            return this.ValorMercado.HasValue ? this.ValorMercado.Value : 0;
        }

        /// <summary>
        /// Retorna a soma do valor de mercado de todas as posições do cliente.
        /// Retorna 0 se não houver registros
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="tipoPapelTitulo">enum do tipo de papel (publico ou privado)</param>
        /// <param name="dataHistorico"></param>
        /// <returns>soma do valor de mercado das posições</returns>
        public decimal RetornaValorMercado(int idCliente, ClasseRendaFixa classeRendaFixa, DateTime dataHistorico)
        {
            PosicaoRendaFixaHistoricoQuery posicaoRendaFixaHistorico = new PosicaoRendaFixaHistoricoQuery("P");
            TituloRendaFixaQuery tituloRendaFixa = new TituloRendaFixaQuery("T");
            PapelRendaFixaQuery papelRendaFixa = new PapelRendaFixaQuery("R");

            posicaoRendaFixaHistorico.Select(posicaoRendaFixaHistorico.ValorMercado.Sum());
            posicaoRendaFixaHistorico.InnerJoin(tituloRendaFixa).On(posicaoRendaFixaHistorico.IdTitulo == tituloRendaFixa.IdTitulo);
            posicaoRendaFixaHistorico.InnerJoin(papelRendaFixa).On(tituloRendaFixa.IdPapel == papelRendaFixa.IdPapel);
            posicaoRendaFixaHistorico.Where(posicaoRendaFixaHistorico.IdCliente == idCliente,
                                    posicaoRendaFixaHistorico.DataHistorico.Equal(dataHistorico),
                                    papelRendaFixa.Classe == (int)classeRendaFixa);

            this.QueryReset();
            this.Load(posicaoRendaFixaHistorico);

            return this.ValorMercado.HasValue ? this.ValorMercado.Value : 0;
        }

        /// <summary>
        /// Retorna a soma do valor de mercado de todas as posições do cliente.
        /// Retorna 0 se não houver registros
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        /// <returns>soma do valor de mercado das posições</returns>
        public decimal RetornaValorMercado(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorMercado.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.DataHistorico.Equal(dataHistorico));

            this.Query.Load();

            return this.ValorMercado.HasValue ? this.ValorMercado.Value : 0;
        }

        /// <summary>
        /// Retorna a soma do valor de mercado de todas as posições do cliente.
        /// Retorna 0 se não houver registros
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="dataHistorico"></param>
        /// <returns>soma do valor de mercado das posições</returns>
        public decimal RetornaValorMercado(int idCliente, DateTime dataHistorico, int idTitulo)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorMercado.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.Query.DataHistorico.Equal(dataHistorico),
                        this.Query.IdTitulo.Equal(idTitulo));

            this.Query.Load();

            return this.ValorMercado.HasValue ? this.ValorMercado.Value : 0;
        }


        /// <summary>
        /// Retorna o valor de mercado do cliente. Leva em conta o IdMoedaCliente passado e faz as devidas conversões.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="idMoedaCliente"></param>
        /// <param name="data"></param>
        /// <returns>soma do valor de mercado de todas as posições</returns>
        public decimal RetornaValorMercado(int idCliente, int idMoedaCliente, DateTime data)
        {
            PosicaoRendaFixaHistoricoQuery posicaoRendaFixaQuery = new PosicaoRendaFixaHistoricoQuery("P");
            TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");

            posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery.IdPosicao);
            posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
            posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente.Equal(idCliente),
                                        posicaoRendaFixaQuery.DataHistorico.Equal(data),
                                    posicaoRendaFixaQuery.Quantidade.NotEqual(0),
                                    tituloRendaFixaQuery.IdMoeda.NotEqual(idMoedaCliente));

            PosicaoRendaFixaHistoricoCollection posicaoRendaFixaCollectionExiste = new PosicaoRendaFixaHistoricoCollection();
            posicaoRendaFixaCollectionExiste.Load(posicaoRendaFixaQuery);

            if (posicaoRendaFixaCollectionExiste.Count == 0)
            {
                return this.RetornaValorMercado(idCliente, data);
            }

            PosicaoRendaFixaHistoricoCollection posicaoRendaFixaCollection = new PosicaoRendaFixaHistoricoCollection();
            posicaoRendaFixaCollection.Query.Select(posicaoRendaFixaCollection.Query.IdTitulo, posicaoRendaFixaCollection.Query.ValorMercado.Sum());
            posicaoRendaFixaCollection.Query.Where(posicaoRendaFixaCollection.Query.IdCliente.Equal(idCliente),
                                                   posicaoRendaFixaCollection.Query.DataHistorico.Equal(data));
            posicaoRendaFixaCollection.Query.GroupBy(posicaoRendaFixaCollection.Query.IdTitulo);
            posicaoRendaFixaCollection.Query.Load();

            decimal totalValor = 0;

            //Pega a ptax para agilizar, se o cliente estiver em dólar
            decimal ptax = 0;
            if (idMoedaCliente == (int)ListaMoedaFixo.Dolar && posicaoRendaFixaCollection.Count > 0)
            {
                CotacaoIndice cotacaoIndice = new CotacaoIndice();
                ptax = cotacaoIndice.BuscaCotacaoIndice((int)ListaIndiceFixo.PTAX_800VENDA, data);
            }

            foreach (PosicaoRendaFixaHistorico posicaoRendaFixa in posicaoRendaFixaCollection)
            {
                int idTitulo = posicaoRendaFixa.IdTitulo.Value;
                decimal valor = posicaoRendaFixa.ValorMercado.Value;

                TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(tituloRendaFixa.Query.IdMoeda);
                tituloRendaFixa.LoadByPrimaryKey(campos, idTitulo);
                int idMoedaAtivo = tituloRendaFixa.IdMoeda.Value;

                if (idMoedaCliente != idMoedaAtivo)
                {
                    decimal fatorConversao = 0;
                    if (idMoedaCliente == (int)ListaMoedaFixo.Dolar && idMoedaAtivo == (int)ListaMoedaFixo.Real)
                    {
                        fatorConversao = 1 / ptax;
                    }
                    else
                    {
                        ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
                        conversaoMoeda.Query.Where(conversaoMoeda.Query.IdMoedaDe.Equal(idMoedaCliente),
                                                   conversaoMoeda.Query.IdMoedaPara.Equal(idMoedaAtivo));
                        if (conversaoMoeda.Query.Load())
                        {
                            int idIndiceConversao = conversaoMoeda.IdIndice.Value;
                            CotacaoIndice cotacaoIndice = new CotacaoIndice();
                            if (conversaoMoeda.Tipo.Value == (byte)TipoConversaoMoeda.Divide)
                            {
                                fatorConversao = 1 / cotacaoIndice.BuscaCotacaoIndice(idIndiceConversao, data);
                            }
                            else
                            {
                                fatorConversao = cotacaoIndice.BuscaCotacaoIndice(idIndiceConversao, data);
                            }
                        }
                    }

                    valor = Math.Round(valor * fatorConversao, 2);
                }

                totalValor += valor;
            }

            return totalValor;
        }

        /// <summary>
        /// Retorna o valor de mercado liquido de IR e IOF de todas as posições do cliente.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns>soma do valor de mercado das posições</returns>
        public decimal RetornaValorMercadoLiquido(int idCliente, DateTime dataHistorico)
        {
            this.QueryReset();
            this.Query
                 .Select(this.Query.ValorMercado.Sum(),
                         this.Query.ValorIR.Sum(),
                         this.Query.ValorIOF.Sum())
                 .Where(this.Query.IdCliente == idCliente,
                        this.query.DataHistorico == dataHistorico,
                        this.Query.Quantidade.NotEqual(0));

            this.Query.Load();

            decimal valorMercado = this.ValorMercado.HasValue ? this.ValorMercado.Value : 0;
            decimal valorIR = this.ValorIR.HasValue ? this.ValorIR.Value : 0;
            decimal valorIOF = this.ValorIOF.HasValue ? this.ValorIOF.Value : 0;

            return (valorMercado - valorIR - valorIOF);
        }


        /// <summary>
        /// Retorna o valor de mercado do cliente. Leva em conta o IdMoedaCliente passado e faz as devidas conversões.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="idMoedaCliente"></param>
        /// <param name="data"></param>
        /// <returns>soma do valor de mercado de todas as posições</returns>
        public decimal RetornaValorMercadoLiquido(int idCliente, int idMoedaCliente, DateTime data)
        {
            PosicaoRendaFixaHistoricoQuery posicaoRendaFixaQuery = new PosicaoRendaFixaHistoricoQuery("P");
            TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery("T");

            posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery.IdPosicao);
            posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(tituloRendaFixaQuery.IdTitulo == posicaoRendaFixaQuery.IdTitulo);
            posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente.Equal(idCliente),
                                        posicaoRendaFixaQuery.DataHistorico.Equal(data),
                                    posicaoRendaFixaQuery.Quantidade.NotEqual(0),
                                    tituloRendaFixaQuery.IdMoeda.NotEqual(idMoedaCliente));

            PosicaoRendaFixaHistoricoCollection posicaoRendaFixaCollectionExiste = new PosicaoRendaFixaHistoricoCollection();
            posicaoRendaFixaCollectionExiste.Load(posicaoRendaFixaQuery);

            if (posicaoRendaFixaCollectionExiste.Count == 0)
            {
                return this.RetornaValorMercadoLiquido(idCliente , data);
            }

            PosicaoRendaFixaHistoricoCollection posicaoRendaFixaCollection = new PosicaoRendaFixaHistoricoCollection();
            posicaoRendaFixaCollection.Query.Select(posicaoRendaFixaCollection.Query.IdTitulo,
                                                    posicaoRendaFixaCollection.Query.ValorMercado.Sum(),
                                                    posicaoRendaFixaCollection.Query.ValorIR.Sum(),
                                                    posicaoRendaFixaCollection.Query.ValorIOF.Sum());
            posicaoRendaFixaCollection.Query.Where(posicaoRendaFixaCollection.Query.IdCliente.Equal(idCliente),
                                                   posicaoRendaFixaCollection.Query.DataHistorico.Equal(data));
            posicaoRendaFixaCollection.Query.GroupBy(posicaoRendaFixaCollection.Query.IdTitulo);
            posicaoRendaFixaCollection.Query.Load();

            decimal totalValor = 0;

            //Pega a ptax para agilizar, se o cliente estiver em dólar
            decimal ptax = 0;
            if (idMoedaCliente == (int)ListaMoedaFixo.Dolar && posicaoRendaFixaCollection.Count > 0)
            {
                CotacaoIndice cotacaoIndice = new CotacaoIndice();
                ptax = cotacaoIndice.BuscaCotacaoIndice((int)ListaIndiceFixo.PTAX_800VENDA, data);
            }

            foreach (PosicaoRendaFixaHistorico posicaoRendaFixa in posicaoRendaFixaCollection)
            {
                int idTitulo = posicaoRendaFixa.IdTitulo.Value;
                decimal valor = posicaoRendaFixa.ValorMercado.Value - posicaoRendaFixa.ValorIR.Value - posicaoRendaFixa.ValorIOF.Value;

                TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
                List<esQueryItem> campos = new List<esQueryItem>();
                campos.Add(tituloRendaFixa.Query.IdMoeda);
                tituloRendaFixa.LoadByPrimaryKey(campos, idTitulo);
                int idMoedaAtivo = tituloRendaFixa.IdMoeda.Value;

                if (idMoedaCliente != idMoedaAtivo)
                {
                    decimal fatorConversao = 0;
                    if (idMoedaCliente == (int)ListaMoedaFixo.Dolar && idMoedaAtivo == (int)ListaMoedaFixo.Real)
                    {
                        fatorConversao = 1 / ptax;
                    }
                    else
                    {
                        ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
                        conversaoMoeda.Query.Where(conversaoMoeda.Query.IdMoedaDe.Equal(idMoedaCliente),
                                                   conversaoMoeda.Query.IdMoedaPara.Equal(idMoedaAtivo));
                        if (conversaoMoeda.Query.Load())
                        {
                            int idIndiceConversao = conversaoMoeda.IdIndice.Value;
                            CotacaoIndice cotacaoIndice = new CotacaoIndice();
                            if (conversaoMoeda.Tipo.Value == (byte)TipoConversaoMoeda.Divide)
                            {
                                fatorConversao = 1 / cotacaoIndice.BuscaCotacaoIndice(idIndiceConversao, data);
                            }
                            else
                            {
                                fatorConversao = cotacaoIndice.BuscaCotacaoIndice(idIndiceConversao, data);
                            }
                        }
                    }

                    valor = Math.Round(valor * fatorConversao, 2);
                }

                totalValor += valor;
            }

            return totalValor;
        }

        public decimal RetornaValorNegativo(int idCliente, DateTime dataHistorico)
        {
            decimal valor = 0;
            PosicaoRendaFixaHistorico posicao = new PosicaoRendaFixaHistorico();
            posicao.Query.Select(posicao.Query.ValorMercado.Sum());
            posicao.Query.Where(posicao.Query.Quantidade.LessThan(0),
                                posicao.Query.IdCliente.Equal(idCliente),
                                posicao.Query.DataHistorico.Equal(dataHistorico));
            posicao.Query.Load();

            if (posicao.ValorMercado.HasValue)
            {
                valor = posicao.ValorMercado.Value;
            }
            return valor;
        }

        /// <summary>
        /// Busca PUMercado da PosicaoRendaFixaHistorico.
        /// </summary>
        /// <param name="idPosicao"></param>
        /// <param name="dataHistorico"></param>
        /// <returns></returns>
        public decimal RetornaPuPosicaoHistorico(int idPosicao, DateTime dataHistorico)
        {
            decimal pu = 0;

            PosicaoRendaFixaHistoricoCollection p = new PosicaoRendaFixaHistoricoCollection();
            p.Query.Select(p.Query.PUMercado)
                   .Where(p.Query.DataHistorico == dataHistorico &&
                          p.Query.IdPosicao == idPosicao);

            if (p.Query.Load())
            {
                if (p.Count > 0)
                {
                    pu = p[0].PUMercado.Value;
                }
            }
            
            return pu;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idPosicao"></param>
        /// <param name="dataHistorico"></param>
        /// <returns></returns>
        public decimal RetornaPuPosicaoHistorico(int idTitulo, DateTime dataOperacao, decimal? taxaOperacao, DateTime dataHistorico)
        {
            decimal pu = 0;

            PosicaoRendaFixaHistoricoCollection p = new PosicaoRendaFixaHistoricoCollection();
            p.Query.Select(p.Query.PUMercado)
                   .Where(p.Query.DataHistorico == dataHistorico &&
                          p.Query.IdTitulo == idTitulo);

            if (taxaOperacao.HasValue)
            {
                p.Query.Where(p.Query.TaxaOperacao == taxaOperacao.Value);
            }

            if (p.Query.Load())
            {
                if (p.Count > 0)
                {
                    pu = p[0].PUMercado.Value;
                }
            }

            return pu;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idcliente"></param>
        /// <param name="idTitulo"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public decimal RetornaPuPosicaoHistorico(int idCliente, int idTitulo, DateTime dataHistorico)
        {
            decimal pu = 0;
            PosicaoRendaFixaHistoricoCollection p = new PosicaoRendaFixaHistoricoCollection();
            p.Query.Select(p.Query.PUMercado, p.Query.DataOperacao)
                   .Where(p.Query.IdCliente == idCliente &&
                          p.Query.DataHistorico == dataHistorico &&
                          p.Query.IdTitulo == idTitulo);

            if (p.Query.Load())
            {
                if (p.Count > 0)
                {
                    pu = p[0].PUMercado.Value;
                }
            }
            
            return pu;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idcliente"></param>
        /// <param name="idTitulo"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public decimal RetornaPuAjustadoPosicao(int idCliente, int idPosicao, DateTime data, DateTime dataHistorico)
        {
            decimal pu = 0;

            PosicaoRendaFixaHistoricoCollection p = new PosicaoRendaFixaHistoricoCollection();
            p.Query.Select(p.Query.PUMercado, p.Query.DataOperacao)
                   .Where(p.Query.DataHistorico == data &&
                          p.Query.IdPosicao == idPosicao,
                          p.Query.IdCliente == idCliente);

            if (p.Query.Load())
            {
                if (p.Count > 0)
                {
                    pu = p[0].PUMercado.Value;
                }
            }

            if (pu != 0)
            {
                LiquidacaoRendaFixaCollection liquidacaoRendaFixaCollection = new LiquidacaoRendaFixaCollection();
                liquidacaoRendaFixaCollection.Query.Select(liquidacaoRendaFixaCollection.Query.ValorBruto,
                                                           liquidacaoRendaFixaCollection.Query.Quantidade)
                            .Where(liquidacaoRendaFixaCollection.Query.DataLiquidacao <= data &
                                   liquidacaoRendaFixaCollection.Query.DataLiquidacao > dataHistorico &
                                   liquidacaoRendaFixaCollection.Query.IdPosicaoResgatada == idPosicao &
                                   liquidacaoRendaFixaCollection.Query.IdCliente == idCliente &
                                   liquidacaoRendaFixaCollection.Query.TipoLancamento.In((byte)TipoLancamentoLiquidacao.Juros,
                                                                                         (byte)TipoLancamentoLiquidacao.Amortizacao));
                liquidacaoRendaFixaCollection.Query.Load();

                foreach (LiquidacaoRendaFixa liquidacaoRendaFixa in liquidacaoRendaFixaCollection)
                {
                    decimal puEvento = liquidacaoRendaFixa.ValorBruto.Value / liquidacaoRendaFixa.Quantidade.Value;
                    
                    pu += puEvento;
                }
            }

            return pu;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idcliente"></param>
        /// <param name="idTitulo"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public decimal RetornaPuAjustadoPosicao(int idCliente, int idPosicao, DateTime data, DateTime dataHistorico, int idMoedaCliente, int idMoedaAtivo)
        {
            ConversaoMoeda conversaoMoeda = new ConversaoMoeda();

            decimal pu = 0;

            PosicaoRendaFixaHistoricoCollection p = new PosicaoRendaFixaHistoricoCollection();
            p.Query.Select(p.Query.PUMercado, p.Query.DataOperacao)
                   .Where(p.Query.DataHistorico == data &&
                          p.Query.IdPosicao == idPosicao,
                          p.Query.IdCliente == idCliente);

            if (p.Query.Load())
            {
                if (p.Count > 0)
                {
                    pu = p[0].PUMercado.Value;
                }
            }

            decimal fatorMoeda = conversaoMoeda.RetornaFatorConversao(idMoedaCliente, idMoedaAtivo, data);
            pu = pu * fatorMoeda;

            if (pu != 0)
            {
                LiquidacaoRendaFixaCollection liquidacaoRendaFixaCollection = new LiquidacaoRendaFixaCollection();
                liquidacaoRendaFixaCollection.Query.Select(liquidacaoRendaFixaCollection.Query.ValorBruto,
                                                           liquidacaoRendaFixaCollection.Query.Quantidade,
                                                           liquidacaoRendaFixaCollection.Query.DataLiquidacao)
                            .Where(liquidacaoRendaFixaCollection.Query.DataLiquidacao <= data &
                                   liquidacaoRendaFixaCollection.Query.DataLiquidacao > dataHistorico &
                                   liquidacaoRendaFixaCollection.Query.IdPosicaoResgatada == idPosicao &
                                   liquidacaoRendaFixaCollection.Query.IdCliente == idCliente &
                                   liquidacaoRendaFixaCollection.Query.TipoLancamento.In((byte)TipoLancamentoLiquidacao.Juros,
                                                                                         (byte)TipoLancamentoLiquidacao.Amortizacao));
                liquidacaoRendaFixaCollection.Query.Load();

                foreach (LiquidacaoRendaFixa liquidacaoRendaFixa in liquidacaoRendaFixaCollection)
                {
                    DateTime dataLiquidacao = liquidacaoRendaFixa.DataLiquidacao.Value;
                    
                    decimal puEvento = liquidacaoRendaFixa.ValorBruto.Value / liquidacaoRendaFixa.Quantidade.Value;
                    fatorMoeda = conversaoMoeda.RetornaFatorConversao(idMoedaCliente, idMoedaAtivo, dataLiquidacao);
                    puEvento = puEvento * fatorMoeda;

                    pu += puEvento;
                }
            }

            return pu;
        }
    }
}
