﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.RendaFixa.Enums;

namespace Financial.RendaFixa
{
	public partial class PapelRendaFixa : esPapelRendaFixa
	{
        public int RetornaPapelPublico(int codigoSelic)
        {
            string descricaoTitulo = "";

            string codigoInterno = codigoSelic.ToString().Substring(0, 4);

            PapelRendaFixaCollection papelRendaFixaCollection = new PapelRendaFixaCollection();

            if (codigoSelic == 100000)
            {
                papelRendaFixaCollection.Query.Where(papelRendaFixaCollection.Query.Classe.Equal((int)ClasseRendaFixa.LTN));
            }
            else if (codigoSelic == 210100)
            {
                papelRendaFixaCollection.Query.Where(papelRendaFixaCollection.Query.Classe.Equal((int)ClasseRendaFixa.LFT));
            }
            else if (codigoSelic == 770100)
            {
                papelRendaFixaCollection.Query.Where(papelRendaFixaCollection.Query.Classe.Equal((int)ClasseRendaFixa.NTN),
                                                                     papelRendaFixaCollection.Query.Descricao.Like("%C%"));
            }
            else if (codigoInterno == "9501")
            {
                papelRendaFixaCollection.Query.Where(papelRendaFixaCollection.Query.Classe.Equal((int)ClasseRendaFixa.NTN),
                                                                     papelRendaFixaCollection.Query.Descricao.Like("%F%"));
            }
            else if (codigoInterno == "7601")
            {
                if (codigoSelic == 760198)
                {
                    papelRendaFixaCollection.Query.Where(papelRendaFixaCollection.Query.Classe.Equal((int)ClasseRendaFixa.NTN),
                                                                     papelRendaFixaCollection.Query.Descricao.Like("%B%"),
                                                                     papelRendaFixaCollection.Query.Descricao.Like("%P%")); //NTN-B PRINCIPAL
                }
                else
                {
                    papelRendaFixaCollection.Query.Where(papelRendaFixaCollection.Query.Classe.Equal((int)ClasseRendaFixa.NTN),
                                                                     papelRendaFixaCollection.Query.Descricao.Like("%B%"),
                                                                     papelRendaFixaCollection.Query.Descricao.NotLike("%P%")); //NTN-B
                }
            }
                        
            papelRendaFixaCollection.Query.Load();

            if (papelRendaFixaCollection.Count == 0)
            {
                throw new Exception("Não existe papel cadastrado associado ao tipo de papel " + descricaoTitulo);
            }

            int idPapel = papelRendaFixaCollection[0].IdPapel.Value;

            return idPapel;
        }
	}
}
