/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 06/10/2014 11:56:41
===============================================================================
*/

using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using System.Data;

using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.RendaFixa
{
	public partial class TaxaCurvaCollection : esTaxaCurvaCollection
	{
        public TaxaCurvaCollection obterTaxaCurvaCollection(DateTime date, int IdCurva)
        {
            TaxaCurvaQuery taxaCurvaQuery = new TaxaCurvaQuery();
            TaxaCurvaCollection taxaCurvaCollection = new TaxaCurvaCollection();
            taxaCurvaQuery.Where(taxaCurvaQuery.DataBase == date && taxaCurvaQuery.IdCurvaRendaFixa == IdCurva)
                                    .OrderBy(taxaCurvaQuery.DataVertice.Ascending);
                
            taxaCurvaCollection.Load(taxaCurvaQuery);

            return taxaCurvaCollection;
        }

        public void ExcluirTaxas(DateTime date, int idCurva)
        {
            TaxaCurvaCollection taxaCurvaCollection = new TaxaCurvaCollection();
            taxaCurvaCollection.Query.Where(taxaCurvaCollection.Query.DataBase == date && taxaCurvaCollection.Query.IdCurvaRendaFixa == idCurva);

            taxaCurvaCollection.Load(taxaCurvaCollection.Query);

            taxaCurvaCollection.MarkAllAsDeleted();
            taxaCurvaCollection.Save();
        }
	}
}
