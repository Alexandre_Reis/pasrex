/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 23/10/2015 15:31:08
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.RendaFixa
{

	[Serializable]
	abstract public class esLiquidacaoTaxaCustodiaCollection : esEntityCollection
	{
		public esLiquidacaoTaxaCustodiaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "LiquidacaoTaxaCustodiaCollection";
		}

		#region Query Logic
		protected void InitQuery(esLiquidacaoTaxaCustodiaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esLiquidacaoTaxaCustodiaQuery);
		}
		#endregion
		
		virtual public LiquidacaoTaxaCustodia DetachEntity(LiquidacaoTaxaCustodia entity)
		{
			return base.DetachEntity(entity) as LiquidacaoTaxaCustodia;
		}
		
		virtual public LiquidacaoTaxaCustodia AttachEntity(LiquidacaoTaxaCustodia entity)
		{
			return base.AttachEntity(entity) as LiquidacaoTaxaCustodia;
		}
		
		virtual public void Combine(LiquidacaoTaxaCustodiaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public LiquidacaoTaxaCustodia this[int index]
		{
			get
			{
				return base[index] as LiquidacaoTaxaCustodia;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(LiquidacaoTaxaCustodia);
		}
	}



	[Serializable]
	abstract public class esLiquidacaoTaxaCustodia : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esLiquidacaoTaxaCustodiaQuery GetDynamicQuery()
		{
			return null;
		}

		public esLiquidacaoTaxaCustodia()
		{

		}

		public esLiquidacaoTaxaCustodia(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idLiquidacaoTaxaCustodia)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idLiquidacaoTaxaCustodia);
			else
				return LoadByPrimaryKeyStoredProcedure(idLiquidacaoTaxaCustodia);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idLiquidacaoTaxaCustodia)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idLiquidacaoTaxaCustodia);
			else
				return LoadByPrimaryKeyStoredProcedure(idLiquidacaoTaxaCustodia);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idLiquidacaoTaxaCustodia)
		{
			esLiquidacaoTaxaCustodiaQuery query = this.GetDynamicQuery();
			query.Where(query.IdLiquidacaoTaxaCustodia == idLiquidacaoTaxaCustodia);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idLiquidacaoTaxaCustodia)
		{
			esParameters parms = new esParameters();
			parms.Add("IdLiquidacaoTaxaCustodia",idLiquidacaoTaxaCustodia);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdLiquidacaoTaxaCustodia": this.str.IdLiquidacaoTaxaCustodia = (string)value; break;							
						case "IdOperacao": this.str.IdOperacao = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "DataHistorico": this.str.DataHistorico = (string)value; break;							
						case "CustoCustodia": this.str.CustoCustodia = (string)value; break;							
						case "TipoCustodia": this.str.TipoCustodia = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdLiquidacaoTaxaCustodia":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdLiquidacaoTaxaCustodia = (System.Int32?)value;
							break;
						
						case "IdOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacao = (System.Int32?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "DataHistorico":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataHistorico = (System.DateTime?)value;
							break;
						
						case "CustoCustodia":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CustoCustodia = (System.Decimal?)value;
							break;
						
						case "TipoCustodia":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.TipoCustodia = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to LiquidacaoTaxaCustodia.IdLiquidacaoTaxaCustodia
		/// </summary>
		virtual public System.Int32? IdLiquidacaoTaxaCustodia
		{
			get
			{
				return base.GetSystemInt32(LiquidacaoTaxaCustodiaMetadata.ColumnNames.IdLiquidacaoTaxaCustodia);
			}
			
			set
			{
				base.SetSystemInt32(LiquidacaoTaxaCustodiaMetadata.ColumnNames.IdLiquidacaoTaxaCustodia, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoTaxaCustodia.IdOperacao
		/// </summary>
		virtual public System.Int32? IdOperacao
		{
			get
			{
				return base.GetSystemInt32(LiquidacaoTaxaCustodiaMetadata.ColumnNames.IdOperacao);
			}
			
			set
			{
				base.SetSystemInt32(LiquidacaoTaxaCustodiaMetadata.ColumnNames.IdOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoTaxaCustodia.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(LiquidacaoTaxaCustodiaMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				base.SetSystemInt32(LiquidacaoTaxaCustodiaMetadata.ColumnNames.IdCliente, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoTaxaCustodia.DataHistorico
		/// </summary>
		virtual public System.DateTime? DataHistorico
		{
			get
			{
				return base.GetSystemDateTime(LiquidacaoTaxaCustodiaMetadata.ColumnNames.DataHistorico);
			}
			
			set
			{
				base.SetSystemDateTime(LiquidacaoTaxaCustodiaMetadata.ColumnNames.DataHistorico, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoTaxaCustodia.CustoCustodia
		/// </summary>
		virtual public System.Decimal? CustoCustodia
		{
			get
			{
				return base.GetSystemDecimal(LiquidacaoTaxaCustodiaMetadata.ColumnNames.CustoCustodia);
			}
			
			set
			{
				base.SetSystemDecimal(LiquidacaoTaxaCustodiaMetadata.ColumnNames.CustoCustodia, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoTaxaCustodia.TipoCustodia
		/// </summary>
		virtual public System.Int32? TipoCustodia
		{
			get
			{
				return base.GetSystemInt32(LiquidacaoTaxaCustodiaMetadata.ColumnNames.TipoCustodia);
			}
			
			set
			{
				base.SetSystemInt32(LiquidacaoTaxaCustodiaMetadata.ColumnNames.TipoCustodia, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esLiquidacaoTaxaCustodia entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdLiquidacaoTaxaCustodia
			{
				get
				{
					System.Int32? data = entity.IdLiquidacaoTaxaCustodia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdLiquidacaoTaxaCustodia = null;
					else entity.IdLiquidacaoTaxaCustodia = Convert.ToInt32(value);
				}
			}
				
			public System.String IdOperacao
			{
				get
				{
					System.Int32? data = entity.IdOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacao = null;
					else entity.IdOperacao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String DataHistorico
			{
				get
				{
					System.DateTime? data = entity.DataHistorico;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataHistorico = null;
					else entity.DataHistorico = Convert.ToDateTime(value);
				}
			}
				
			public System.String CustoCustodia
			{
				get
				{
					System.Decimal? data = entity.CustoCustodia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CustoCustodia = null;
					else entity.CustoCustodia = Convert.ToDecimal(value);
				}
			}
				
			public System.String TipoCustodia
			{
				get
				{
					System.Int32? data = entity.TipoCustodia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoCustodia = null;
					else entity.TipoCustodia = Convert.ToInt32(value);
				}
			}
			

			private esLiquidacaoTaxaCustodia entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esLiquidacaoTaxaCustodiaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esLiquidacaoTaxaCustodia can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class LiquidacaoTaxaCustodia : esLiquidacaoTaxaCustodia
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esLiquidacaoTaxaCustodiaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return LiquidacaoTaxaCustodiaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdLiquidacaoTaxaCustodia
		{
			get
			{
				return new esQueryItem(this, LiquidacaoTaxaCustodiaMetadata.ColumnNames.IdLiquidacaoTaxaCustodia, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdOperacao
		{
			get
			{
				return new esQueryItem(this, LiquidacaoTaxaCustodiaMetadata.ColumnNames.IdOperacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, LiquidacaoTaxaCustodiaMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataHistorico
		{
			get
			{
				return new esQueryItem(this, LiquidacaoTaxaCustodiaMetadata.ColumnNames.DataHistorico, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem CustoCustodia
		{
			get
			{
				return new esQueryItem(this, LiquidacaoTaxaCustodiaMetadata.ColumnNames.CustoCustodia, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TipoCustodia
		{
			get
			{
				return new esQueryItem(this, LiquidacaoTaxaCustodiaMetadata.ColumnNames.TipoCustodia, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("LiquidacaoTaxaCustodiaCollection")]
	public partial class LiquidacaoTaxaCustodiaCollection : esLiquidacaoTaxaCustodiaCollection, IEnumerable<LiquidacaoTaxaCustodia>
	{
		public LiquidacaoTaxaCustodiaCollection()
		{

		}
		
		public static implicit operator List<LiquidacaoTaxaCustodia>(LiquidacaoTaxaCustodiaCollection coll)
		{
			List<LiquidacaoTaxaCustodia> list = new List<LiquidacaoTaxaCustodia>();
			
			foreach (LiquidacaoTaxaCustodia emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  LiquidacaoTaxaCustodiaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new LiquidacaoTaxaCustodiaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new LiquidacaoTaxaCustodia(row);
		}

		override protected esEntity CreateEntity()
		{
			return new LiquidacaoTaxaCustodia();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public LiquidacaoTaxaCustodiaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new LiquidacaoTaxaCustodiaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(LiquidacaoTaxaCustodiaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public LiquidacaoTaxaCustodia AddNew()
		{
			LiquidacaoTaxaCustodia entity = base.AddNewEntity() as LiquidacaoTaxaCustodia;
			
			return entity;
		}

		public LiquidacaoTaxaCustodia FindByPrimaryKey(System.Int32 idLiquidacaoTaxaCustodia)
		{
			return base.FindByPrimaryKey(idLiquidacaoTaxaCustodia) as LiquidacaoTaxaCustodia;
		}


		#region IEnumerable<LiquidacaoTaxaCustodia> Members

		IEnumerator<LiquidacaoTaxaCustodia> IEnumerable<LiquidacaoTaxaCustodia>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as LiquidacaoTaxaCustodia;
			}
		}

		#endregion
		
		private LiquidacaoTaxaCustodiaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'LiquidacaoTaxaCustodia' table
	/// </summary>

	[Serializable]
	public partial class LiquidacaoTaxaCustodia : esLiquidacaoTaxaCustodia
	{
		public LiquidacaoTaxaCustodia()
		{

		}
	
		public LiquidacaoTaxaCustodia(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return LiquidacaoTaxaCustodiaMetadata.Meta();
			}
		}
		
		
		
		override protected esLiquidacaoTaxaCustodiaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new LiquidacaoTaxaCustodiaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public LiquidacaoTaxaCustodiaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new LiquidacaoTaxaCustodiaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(LiquidacaoTaxaCustodiaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private LiquidacaoTaxaCustodiaQuery query;
	}



	[Serializable]
	public partial class LiquidacaoTaxaCustodiaQuery : esLiquidacaoTaxaCustodiaQuery
	{
		public LiquidacaoTaxaCustodiaQuery()
		{

		}		
		
		public LiquidacaoTaxaCustodiaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class LiquidacaoTaxaCustodiaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected LiquidacaoTaxaCustodiaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(LiquidacaoTaxaCustodiaMetadata.ColumnNames.IdLiquidacaoTaxaCustodia, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = LiquidacaoTaxaCustodiaMetadata.PropertyNames.IdLiquidacaoTaxaCustodia;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoTaxaCustodiaMetadata.ColumnNames.IdOperacao, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = LiquidacaoTaxaCustodiaMetadata.PropertyNames.IdOperacao;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoTaxaCustodiaMetadata.ColumnNames.IdCliente, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = LiquidacaoTaxaCustodiaMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoTaxaCustodiaMetadata.ColumnNames.DataHistorico, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = LiquidacaoTaxaCustodiaMetadata.PropertyNames.DataHistorico;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoTaxaCustodiaMetadata.ColumnNames.CustoCustodia, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = LiquidacaoTaxaCustodiaMetadata.PropertyNames.CustoCustodia;	
			c.NumericPrecision = 18;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoTaxaCustodiaMetadata.ColumnNames.TipoCustodia, 5, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = LiquidacaoTaxaCustodiaMetadata.PropertyNames.TipoCustodia;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public LiquidacaoTaxaCustodiaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdLiquidacaoTaxaCustodia = "IdLiquidacaoTaxaCustodia";
			 public const string IdOperacao = "IdOperacao";
			 public const string IdCliente = "IdCliente";
			 public const string DataHistorico = "DataHistorico";
			 public const string CustoCustodia = "CustoCustodia";
			 public const string TipoCustodia = "TipoCustodia";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdLiquidacaoTaxaCustodia = "IdLiquidacaoTaxaCustodia";
			 public const string IdOperacao = "IdOperacao";
			 public const string IdCliente = "IdCliente";
			 public const string DataHistorico = "DataHistorico";
			 public const string CustoCustodia = "CustoCustodia";
			 public const string TipoCustodia = "TipoCustodia";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(LiquidacaoTaxaCustodiaMetadata))
			{
				if(LiquidacaoTaxaCustodiaMetadata.mapDelegates == null)
				{
					LiquidacaoTaxaCustodiaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (LiquidacaoTaxaCustodiaMetadata.meta == null)
				{
					LiquidacaoTaxaCustodiaMetadata.meta = new LiquidacaoTaxaCustodiaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdLiquidacaoTaxaCustodia", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdOperacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataHistorico", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("CustoCustodia", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("TipoCustodia", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "LiquidacaoTaxaCustodia";
				meta.Destination = "LiquidacaoTaxaCustodia";
				
				meta.spInsert = "proc_LiquidacaoTaxaCustodiaInsert";				
				meta.spUpdate = "proc_LiquidacaoTaxaCustodiaUpdate";		
				meta.spDelete = "proc_LiquidacaoTaxaCustodiaDelete";
				meta.spLoadAll = "proc_LiquidacaoTaxaCustodiaLoadAll";
				meta.spLoadByPrimaryKey = "proc_LiquidacaoTaxaCustodiaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private LiquidacaoTaxaCustodiaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
