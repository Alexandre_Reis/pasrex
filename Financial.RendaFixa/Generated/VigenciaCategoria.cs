/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 03/10/2014 12:39:51
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.RendaFixa
{

	[Serializable]
	abstract public class esVigenciaCategoriaCollection : esEntityCollection
	{
		public esVigenciaCategoriaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "VigenciaCategoriaCollection";
		}

		#region Query Logic
		protected void InitQuery(esVigenciaCategoriaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esVigenciaCategoriaQuery);
		}
		#endregion
		
		virtual public VigenciaCategoria DetachEntity(VigenciaCategoria entity)
		{
			return base.DetachEntity(entity) as VigenciaCategoria;
		}
		
		virtual public VigenciaCategoria AttachEntity(VigenciaCategoria entity)
		{
			return base.AttachEntity(entity) as VigenciaCategoria;
		}
		
		virtual public void Combine(VigenciaCategoriaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public VigenciaCategoria this[int index]
		{
			get
			{
				return base[index] as VigenciaCategoria;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(VigenciaCategoria);
		}
	}



	[Serializable]
	abstract public class esVigenciaCategoria : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esVigenciaCategoriaQuery GetDynamicQuery()
		{
			return null;
		}

		public esVigenciaCategoria()
		{

		}

		public esVigenciaCategoria(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idVigenciaCategoria)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idVigenciaCategoria);
			else
				return LoadByPrimaryKeyStoredProcedure(idVigenciaCategoria);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idVigenciaCategoria)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idVigenciaCategoria);
			else
				return LoadByPrimaryKeyStoredProcedure(idVigenciaCategoria);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idVigenciaCategoria)
		{
			esVigenciaCategoriaQuery query = this.GetDynamicQuery();
			query.Where(query.IdVigenciaCategoria == idVigenciaCategoria);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idVigenciaCategoria)
		{
			esParameters parms = new esParameters();
			parms.Add("IdVigenciaCategoria",idVigenciaCategoria);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdVigenciaCategoria": this.str.IdVigenciaCategoria = (string)value; break;							
						case "DataVigencia": this.str.DataVigencia = (string)value; break;							
						case "IdOperacao": this.str.IdOperacao = (string)value; break;							
						case "TipoVigenciaCategoria": this.str.TipoVigenciaCategoria = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdVigenciaCategoria":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdVigenciaCategoria = (System.Int32?)value;
							break;
						
						case "DataVigencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataVigencia = (System.DateTime?)value;
							break;
						
						case "IdOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacao = (System.Int32?)value;
							break;
						
						case "TipoVigenciaCategoria":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.TipoVigenciaCategoria = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to VigenciaCategoria.IdVigenciaCategoria
		/// </summary>
		virtual public System.Int32? IdVigenciaCategoria
		{
			get
			{
				return base.GetSystemInt32(VigenciaCategoriaMetadata.ColumnNames.IdVigenciaCategoria);
			}
			
			set
			{
				base.SetSystemInt32(VigenciaCategoriaMetadata.ColumnNames.IdVigenciaCategoria, value);
			}
		}
		
		/// <summary>
		/// Maps to VigenciaCategoria.DataVigencia
		/// </summary>
		virtual public System.DateTime? DataVigencia
		{
			get
			{
				return base.GetSystemDateTime(VigenciaCategoriaMetadata.ColumnNames.DataVigencia);
			}
			
			set
			{
				base.SetSystemDateTime(VigenciaCategoriaMetadata.ColumnNames.DataVigencia, value);
			}
		}
		
		/// <summary>
		/// Maps to VigenciaCategoria.IdOperacao
		/// </summary>
		virtual public System.Int32? IdOperacao
		{
			get
			{
				return base.GetSystemInt32(VigenciaCategoriaMetadata.ColumnNames.IdOperacao);
			}
			
			set
			{
				base.SetSystemInt32(VigenciaCategoriaMetadata.ColumnNames.IdOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to VigenciaCategoria.TipoVigenciaCategoria
		/// </summary>
		virtual public System.Int32? TipoVigenciaCategoria
		{
			get
			{
				return base.GetSystemInt32(VigenciaCategoriaMetadata.ColumnNames.TipoVigenciaCategoria);
			}
			
			set
			{
				base.SetSystemInt32(VigenciaCategoriaMetadata.ColumnNames.TipoVigenciaCategoria, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esVigenciaCategoria entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdVigenciaCategoria
			{
				get
				{
					System.Int32? data = entity.IdVigenciaCategoria;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdVigenciaCategoria = null;
					else entity.IdVigenciaCategoria = Convert.ToInt32(value);
				}
			}
				
			public System.String DataVigencia
			{
				get
				{
					System.DateTime? data = entity.DataVigencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataVigencia = null;
					else entity.DataVigencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdOperacao
			{
				get
				{
					System.Int32? data = entity.IdOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacao = null;
					else entity.IdOperacao = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoVigenciaCategoria
			{
				get
				{
					System.Int32? data = entity.TipoVigenciaCategoria;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoVigenciaCategoria = null;
					else entity.TipoVigenciaCategoria = Convert.ToInt32(value);
				}
			}
			

			private esVigenciaCategoria entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esVigenciaCategoriaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esVigenciaCategoria can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class VigenciaCategoria : esVigenciaCategoria
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esVigenciaCategoriaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return VigenciaCategoriaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdVigenciaCategoria
		{
			get
			{
				return new esQueryItem(this, VigenciaCategoriaMetadata.ColumnNames.IdVigenciaCategoria, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataVigencia
		{
			get
			{
				return new esQueryItem(this, VigenciaCategoriaMetadata.ColumnNames.DataVigencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdOperacao
		{
			get
			{
				return new esQueryItem(this, VigenciaCategoriaMetadata.ColumnNames.IdOperacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoVigenciaCategoria
		{
			get
			{
				return new esQueryItem(this, VigenciaCategoriaMetadata.ColumnNames.TipoVigenciaCategoria, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("VigenciaCategoriaCollection")]
	public partial class VigenciaCategoriaCollection : esVigenciaCategoriaCollection, IEnumerable<VigenciaCategoria>
	{
		public VigenciaCategoriaCollection()
		{

		}
		
		public static implicit operator List<VigenciaCategoria>(VigenciaCategoriaCollection coll)
		{
			List<VigenciaCategoria> list = new List<VigenciaCategoria>();
			
			foreach (VigenciaCategoria emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  VigenciaCategoriaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new VigenciaCategoriaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new VigenciaCategoria(row);
		}

		override protected esEntity CreateEntity()
		{
			return new VigenciaCategoria();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public VigenciaCategoriaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new VigenciaCategoriaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(VigenciaCategoriaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public VigenciaCategoria AddNew()
		{
			VigenciaCategoria entity = base.AddNewEntity() as VigenciaCategoria;
			
			return entity;
		}

		public VigenciaCategoria FindByPrimaryKey(System.Int32 idVigenciaCategoria)
		{
			return base.FindByPrimaryKey(idVigenciaCategoria) as VigenciaCategoria;
		}


		#region IEnumerable<VigenciaCategoria> Members

		IEnumerator<VigenciaCategoria> IEnumerable<VigenciaCategoria>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as VigenciaCategoria;
			}
		}

		#endregion
		
		private VigenciaCategoriaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'VigenciaCategoria' table
	/// </summary>

	[Serializable]
	public partial class VigenciaCategoria : esVigenciaCategoria
	{
		public VigenciaCategoria()
		{

		}
	
		public VigenciaCategoria(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return VigenciaCategoriaMetadata.Meta();
			}
		}
		
		
		
		override protected esVigenciaCategoriaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new VigenciaCategoriaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public VigenciaCategoriaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new VigenciaCategoriaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(VigenciaCategoriaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private VigenciaCategoriaQuery query;
	}



	[Serializable]
	public partial class VigenciaCategoriaQuery : esVigenciaCategoriaQuery
	{
		public VigenciaCategoriaQuery()
		{

		}		
		
		public VigenciaCategoriaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class VigenciaCategoriaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected VigenciaCategoriaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(VigenciaCategoriaMetadata.ColumnNames.IdVigenciaCategoria, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = VigenciaCategoriaMetadata.PropertyNames.IdVigenciaCategoria;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VigenciaCategoriaMetadata.ColumnNames.DataVigencia, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = VigenciaCategoriaMetadata.PropertyNames.DataVigencia;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VigenciaCategoriaMetadata.ColumnNames.IdOperacao, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = VigenciaCategoriaMetadata.PropertyNames.IdOperacao;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(VigenciaCategoriaMetadata.ColumnNames.TipoVigenciaCategoria, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = VigenciaCategoriaMetadata.PropertyNames.TipoVigenciaCategoria;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public VigenciaCategoriaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdVigenciaCategoria = "IdVigenciaCategoria";
			 public const string DataVigencia = "DataVigencia";
			 public const string IdOperacao = "IdOperacao";
			 public const string TipoVigenciaCategoria = "TipoVigenciaCategoria";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdVigenciaCategoria = "IdVigenciaCategoria";
			 public const string DataVigencia = "DataVigencia";
			 public const string IdOperacao = "IdOperacao";
			 public const string TipoVigenciaCategoria = "TipoVigenciaCategoria";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(VigenciaCategoriaMetadata))
			{
				if(VigenciaCategoriaMetadata.mapDelegates == null)
				{
					VigenciaCategoriaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (VigenciaCategoriaMetadata.meta == null)
				{
					VigenciaCategoriaMetadata.meta = new VigenciaCategoriaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdVigenciaCategoria", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataVigencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdOperacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoVigenciaCategoria", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "VigenciaCategoria";
				meta.Destination = "VigenciaCategoria";
				
				meta.spInsert = "proc_VigenciaCategoriaInsert";				
				meta.spUpdate = "proc_VigenciaCategoriaUpdate";		
				meta.spDelete = "proc_VigenciaCategoriaDelete";
				meta.spLoadAll = "proc_VigenciaCategoriaLoadAll";
				meta.spLoadByPrimaryKey = "proc_VigenciaCategoriaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private VigenciaCategoriaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
