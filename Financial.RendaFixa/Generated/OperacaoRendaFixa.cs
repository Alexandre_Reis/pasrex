/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 04/03/2016 18:55:46
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Fundo;
using Financial.Common;
using refeInvestidor = Financial.Investidor;
using refeContaCorrente = Financial.ContaCorrente;

namespace Financial.RendaFixa
{

	[Serializable]
	abstract public class esOperacaoRendaFixaCollection : esEntityCollection
	{
		public esOperacaoRendaFixaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "OperacaoRendaFixaCollection";
		}

		#region Query Logic
		protected void InitQuery(esOperacaoRendaFixaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esOperacaoRendaFixaQuery);
		}
		#endregion
		
		virtual public OperacaoRendaFixa DetachEntity(OperacaoRendaFixa entity)
		{
			return base.DetachEntity(entity) as OperacaoRendaFixa;
		}
		
		virtual public OperacaoRendaFixa AttachEntity(OperacaoRendaFixa entity)
		{
			return base.AttachEntity(entity) as OperacaoRendaFixa;
		}
		
		virtual public void Combine(OperacaoRendaFixaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public OperacaoRendaFixa this[int index]
		{
			get
			{
				return base[index] as OperacaoRendaFixa;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(OperacaoRendaFixa);
		}
	}



	[Serializable]
	abstract public class esOperacaoRendaFixa : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esOperacaoRendaFixaQuery GetDynamicQuery()
		{
			return null;
		}

		public esOperacaoRendaFixa()
		{

		}

		public esOperacaoRendaFixa(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idOperacao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idOperacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idOperacao);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idOperacao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idOperacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idOperacao);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idOperacao)
		{
			esOperacaoRendaFixaQuery query = this.GetDynamicQuery();
			query.Where(query.IdOperacao == idOperacao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idOperacao)
		{
			esParameters parms = new esParameters();
			parms.Add("IdOperacao",idOperacao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdOperacao": this.str.IdOperacao = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "IdTitulo": this.str.IdTitulo = (string)value; break;							
						case "DataOperacao": this.str.DataOperacao = (string)value; break;							
						case "DataLiquidacao": this.str.DataLiquidacao = (string)value; break;							
						case "TipoOperacao": this.str.TipoOperacao = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "PUOperacao": this.str.PUOperacao = (string)value; break;							
						case "Valor": this.str.Valor = (string)value; break;							
						case "TaxaOperacao": this.str.TaxaOperacao = (string)value; break;							
						case "Fonte": this.str.Fonte = (string)value; break;							
						case "DataVolta": this.str.DataVolta = (string)value; break;							
						case "TaxaVolta": this.str.TaxaVolta = (string)value; break;							
						case "PUVolta": this.str.PUVolta = (string)value; break;							
						case "ValorVolta": this.str.ValorVolta = (string)value; break;							
						case "IdOperacaoResgatada": this.str.IdOperacaoResgatada = (string)value; break;							
						case "TipoNegociacao": this.str.TipoNegociacao = (string)value; break;							
						case "Rendimento": this.str.Rendimento = (string)value; break;							
						case "ValorIR": this.str.ValorIR = (string)value; break;							
						case "ValorIOF": this.str.ValorIOF = (string)value; break;							
						case "IdPosicaoResgatada": this.str.IdPosicaoResgatada = (string)value; break;							
						case "ValorLiquido": this.str.ValorLiquido = (string)value; break;							
						case "TaxaNegociacao": this.str.TaxaNegociacao = (string)value; break;							
						case "IdAgenteCorretora": this.str.IdAgenteCorretora = (string)value; break;							
						case "IdCustodia": this.str.IdCustodia = (string)value; break;							
						case "IdLiquidacao": this.str.IdLiquidacao = (string)value; break;							
						case "ValorCorretagem": this.str.ValorCorretagem = (string)value; break;							
						case "NumeroNota": this.str.NumeroNota = (string)value; break;							
						case "ValorISS": this.str.ValorISS = (string)value; break;							
						case "CodigoContraParte": this.str.CodigoContraParte = (string)value; break;							
						case "Emolumento": this.str.Emolumento = (string)value; break;							
						case "StatusExportacao": this.str.StatusExportacao = (string)value; break;							
						case "Observacao": this.str.Observacao = (string)value; break;							
						case "IdIndiceVolta": this.str.IdIndiceVolta = (string)value; break;							
						case "DataRegistro": this.str.DataRegistro = (string)value; break;							
						case "IdOperacaoVinculo": this.str.IdOperacaoVinculo = (string)value; break;							
						case "OperacaoTermo": this.str.OperacaoTermo = (string)value; break;							
						case "RendimentoNaoTributavel": this.str.RendimentoNaoTributavel = (string)value; break;							
						case "Status": this.str.Status = (string)value; break;							
						case "IsIPO": this.str.IsIPO = (string)value; break;							
						case "IdOperacaoExterna": this.str.IdOperacaoExterna = (string)value; break;							
						case "IdCarteiraContraparte": this.str.IdCarteiraContraparte = (string)value; break;							
						case "IdAgenteContraParte": this.str.IdAgenteContraParte = (string)value; break;							
						case "IdOperacaoEspelho": this.str.IdOperacaoEspelho = (string)value; break;							
						case "DataModificacao": this.str.DataModificacao = (string)value; break;							
						case "StatusBatimento": this.str.StatusBatimento = (string)value; break;							
						case "PercentualPuFace": this.str.PercentualPuFace = (string)value; break;							
						case "IdLocalNegociacao": this.str.IdLocalNegociacao = (string)value; break;							
						case "IdAgenteCustodia": this.str.IdAgenteCustodia = (string)value; break;							
						case "IdBoletaExterna": this.str.IdBoletaExterna = (string)value; break;							
						case "IdTrader": this.str.IdTrader = (string)value; break;							
						case "IdConta": this.str.IdConta = (string)value; break;							
						case "IdCategoriaMovimentacao": this.str.IdCategoriaMovimentacao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacao = (System.Int32?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "IdTitulo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTitulo = (System.Int32?)value;
							break;
						
						case "DataOperacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataOperacao = (System.DateTime?)value;
							break;
						
						case "DataLiquidacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataLiquidacao = (System.DateTime?)value;
							break;
						
						case "TipoOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoOperacao = (System.Byte?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Quantidade = (System.Decimal?)value;
							break;
						
						case "PUOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PUOperacao = (System.Decimal?)value;
							break;
						
						case "Valor":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Valor = (System.Decimal?)value;
							break;
						
						case "TaxaOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TaxaOperacao = (System.Decimal?)value;
							break;
						
						case "Fonte":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Fonte = (System.Byte?)value;
							break;
						
						case "DataVolta":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataVolta = (System.DateTime?)value;
							break;
						
						case "TaxaVolta":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TaxaVolta = (System.Decimal?)value;
							break;
						
						case "PUVolta":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PUVolta = (System.Decimal?)value;
							break;
						
						case "ValorVolta":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorVolta = (System.Decimal?)value;
							break;
						
						case "IdOperacaoResgatada":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacaoResgatada = (System.Int32?)value;
							break;
						
						case "TipoNegociacao":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoNegociacao = (System.Byte?)value;
							break;
						
						case "Rendimento":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Rendimento = (System.Decimal?)value;
							break;
						
						case "ValorIR":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIR = (System.Decimal?)value;
							break;
						
						case "ValorIOF":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIOF = (System.Decimal?)value;
							break;
						
						case "IdPosicaoResgatada":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPosicaoResgatada = (System.Int32?)value;
							break;
						
						case "ValorLiquido":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorLiquido = (System.Decimal?)value;
							break;
						
						case "TaxaNegociacao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TaxaNegociacao = (System.Decimal?)value;
							break;
						
						case "IdAgenteCorretora":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgenteCorretora = (System.Int32?)value;
							break;
						
						case "IdCustodia":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.IdCustodia = (System.Byte?)value;
							break;
						
						case "IdLiquidacao":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.IdLiquidacao = (System.Byte?)value;
							break;
						
						case "ValorCorretagem":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorCorretagem = (System.Decimal?)value;
							break;
						
						case "NumeroNota":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.NumeroNota = (System.Int32?)value;
							break;
						
						case "ValorISS":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorISS = (System.Decimal?)value;
							break;
						
						case "Emolumento":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Emolumento = (System.Decimal?)value;
							break;
						
						case "StatusExportacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.StatusExportacao = (System.Int32?)value;
							break;
						
						case "IdIndiceVolta":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdIndiceVolta = (System.Int16?)value;
							break;
						
						case "DataRegistro":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataRegistro = (System.DateTime?)value;
							break;
						
						case "IdOperacaoVinculo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacaoVinculo = (System.Int32?)value;
							break;
						
						case "RendimentoNaoTributavel":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RendimentoNaoTributavel = (System.Decimal?)value;
							break;
						
						case "Status":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Status = (System.Byte?)value;
							break;
						
						case "IdOperacaoExterna":
						
							if (value == null || value.GetType().ToString() == "System.Int64")
								this.IdOperacaoExterna = (System.Int64?)value;
							break;
						
						case "IdCarteiraContraparte":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteiraContraparte = (System.Int32?)value;
							break;
						
						case "IdAgenteContraParte":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgenteContraParte = (System.Int32?)value;
							break;
						
						case "IdOperacaoEspelho":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacaoEspelho = (System.Int32?)value;
							break;
						
						case "DataModificacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataModificacao = (System.DateTime?)value;
							break;
						
						case "StatusBatimento":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.StatusBatimento = (System.Int32?)value;
							break;
						
						case "PercentualPuFace":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PercentualPuFace = (System.Decimal?)value;
							break;
						
						case "IdLocalNegociacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdLocalNegociacao = (System.Int32?)value;
							break;
						
						case "IdAgenteCustodia":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgenteCustodia = (System.Int32?)value;
							break;
						
						case "IdBoletaExterna":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdBoletaExterna = (System.Int32?)value;
							break;
						
						case "IdTrader":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTrader = (System.Int32?)value;
							break;
						
						case "IdConta":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdConta = (System.Int32?)value;
							break;
						
						case "IdCategoriaMovimentacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCategoriaMovimentacao = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to OperacaoRendaFixa.IdOperacao
		/// </summary>
		virtual public System.Int32? IdOperacao
		{
			get
			{
				return base.GetSystemInt32(OperacaoRendaFixaMetadata.ColumnNames.IdOperacao);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoRendaFixaMetadata.ColumnNames.IdOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoRendaFixa.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(OperacaoRendaFixaMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(OperacaoRendaFixaMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OperacaoRendaFixa.IdTitulo
		/// </summary>
		virtual public System.Int32? IdTitulo
		{
			get
			{
				return base.GetSystemInt32(OperacaoRendaFixaMetadata.ColumnNames.IdTitulo);
			}
			
			set
			{
				if(base.SetSystemInt32(OperacaoRendaFixaMetadata.ColumnNames.IdTitulo, value))
				{
					this._UpToTituloRendaFixaByIdTitulo = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OperacaoRendaFixa.DataOperacao
		/// </summary>
		virtual public System.DateTime? DataOperacao
		{
			get
			{
				return base.GetSystemDateTime(OperacaoRendaFixaMetadata.ColumnNames.DataOperacao);
			}
			
			set
			{
				base.SetSystemDateTime(OperacaoRendaFixaMetadata.ColumnNames.DataOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoRendaFixa.DataLiquidacao
		/// </summary>
		virtual public System.DateTime? DataLiquidacao
		{
			get
			{
				return base.GetSystemDateTime(OperacaoRendaFixaMetadata.ColumnNames.DataLiquidacao);
			}
			
			set
			{
				base.SetSystemDateTime(OperacaoRendaFixaMetadata.ColumnNames.DataLiquidacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoRendaFixa.TipoOperacao
		/// </summary>
		virtual public System.Byte? TipoOperacao
		{
			get
			{
				return base.GetSystemByte(OperacaoRendaFixaMetadata.ColumnNames.TipoOperacao);
			}
			
			set
			{
				base.SetSystemByte(OperacaoRendaFixaMetadata.ColumnNames.TipoOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoRendaFixa.Quantidade
		/// </summary>
		virtual public System.Decimal? Quantidade
		{
			get
			{
				return base.GetSystemDecimal(OperacaoRendaFixaMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoRendaFixaMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoRendaFixa.PUOperacao
		/// </summary>
		virtual public System.Decimal? PUOperacao
		{
			get
			{
				return base.GetSystemDecimal(OperacaoRendaFixaMetadata.ColumnNames.PUOperacao);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoRendaFixaMetadata.ColumnNames.PUOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoRendaFixa.Valor
		/// </summary>
		virtual public System.Decimal? Valor
		{
			get
			{
				return base.GetSystemDecimal(OperacaoRendaFixaMetadata.ColumnNames.Valor);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoRendaFixaMetadata.ColumnNames.Valor, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoRendaFixa.TaxaOperacao
		/// </summary>
		virtual public System.Decimal? TaxaOperacao
		{
			get
			{
				return base.GetSystemDecimal(OperacaoRendaFixaMetadata.ColumnNames.TaxaOperacao);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoRendaFixaMetadata.ColumnNames.TaxaOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoRendaFixa.Fonte
		/// </summary>
		virtual public System.Byte? Fonte
		{
			get
			{
				return base.GetSystemByte(OperacaoRendaFixaMetadata.ColumnNames.Fonte);
			}
			
			set
			{
				base.SetSystemByte(OperacaoRendaFixaMetadata.ColumnNames.Fonte, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoRendaFixa.DataVolta
		/// </summary>
		virtual public System.DateTime? DataVolta
		{
			get
			{
				return base.GetSystemDateTime(OperacaoRendaFixaMetadata.ColumnNames.DataVolta);
			}
			
			set
			{
				base.SetSystemDateTime(OperacaoRendaFixaMetadata.ColumnNames.DataVolta, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoRendaFixa.TaxaVolta
		/// </summary>
		virtual public System.Decimal? TaxaVolta
		{
			get
			{
				return base.GetSystemDecimal(OperacaoRendaFixaMetadata.ColumnNames.TaxaVolta);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoRendaFixaMetadata.ColumnNames.TaxaVolta, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoRendaFixa.PUVolta
		/// </summary>
		virtual public System.Decimal? PUVolta
		{
			get
			{
				return base.GetSystemDecimal(OperacaoRendaFixaMetadata.ColumnNames.PUVolta);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoRendaFixaMetadata.ColumnNames.PUVolta, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoRendaFixa.ValorVolta
		/// </summary>
		virtual public System.Decimal? ValorVolta
		{
			get
			{
				return base.GetSystemDecimal(OperacaoRendaFixaMetadata.ColumnNames.ValorVolta);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoRendaFixaMetadata.ColumnNames.ValorVolta, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoRendaFixa.IdOperacaoResgatada
		/// </summary>
		virtual public System.Int32? IdOperacaoResgatada
		{
			get
			{
				return base.GetSystemInt32(OperacaoRendaFixaMetadata.ColumnNames.IdOperacaoResgatada);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoRendaFixaMetadata.ColumnNames.IdOperacaoResgatada, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoRendaFixa.TipoNegociacao
		/// </summary>
		virtual public System.Byte? TipoNegociacao
		{
			get
			{
				return base.GetSystemByte(OperacaoRendaFixaMetadata.ColumnNames.TipoNegociacao);
			}
			
			set
			{
				base.SetSystemByte(OperacaoRendaFixaMetadata.ColumnNames.TipoNegociacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoRendaFixa.Rendimento
		/// </summary>
		virtual public System.Decimal? Rendimento
		{
			get
			{
				return base.GetSystemDecimal(OperacaoRendaFixaMetadata.ColumnNames.Rendimento);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoRendaFixaMetadata.ColumnNames.Rendimento, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoRendaFixa.ValorIR
		/// </summary>
		virtual public System.Decimal? ValorIR
		{
			get
			{
				return base.GetSystemDecimal(OperacaoRendaFixaMetadata.ColumnNames.ValorIR);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoRendaFixaMetadata.ColumnNames.ValorIR, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoRendaFixa.ValorIOF
		/// </summary>
		virtual public System.Decimal? ValorIOF
		{
			get
			{
				return base.GetSystemDecimal(OperacaoRendaFixaMetadata.ColumnNames.ValorIOF);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoRendaFixaMetadata.ColumnNames.ValorIOF, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoRendaFixa.IdPosicaoResgatada
		/// </summary>
		virtual public System.Int32? IdPosicaoResgatada
		{
			get
			{
				return base.GetSystemInt32(OperacaoRendaFixaMetadata.ColumnNames.IdPosicaoResgatada);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoRendaFixaMetadata.ColumnNames.IdPosicaoResgatada, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoRendaFixa.ValorLiquido
		/// </summary>
		virtual public System.Decimal? ValorLiquido
		{
			get
			{
				return base.GetSystemDecimal(OperacaoRendaFixaMetadata.ColumnNames.ValorLiquido);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoRendaFixaMetadata.ColumnNames.ValorLiquido, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoRendaFixa.TaxaNegociacao
		/// </summary>
		virtual public System.Decimal? TaxaNegociacao
		{
			get
			{
				return base.GetSystemDecimal(OperacaoRendaFixaMetadata.ColumnNames.TaxaNegociacao);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoRendaFixaMetadata.ColumnNames.TaxaNegociacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoRendaFixa.IdAgenteCorretora
		/// </summary>
		virtual public System.Int32? IdAgenteCorretora
		{
			get
			{
				return base.GetSystemInt32(OperacaoRendaFixaMetadata.ColumnNames.IdAgenteCorretora);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoRendaFixaMetadata.ColumnNames.IdAgenteCorretora, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoRendaFixa.IdCustodia
		/// </summary>
		virtual public System.Byte? IdCustodia
		{
			get
			{
				return base.GetSystemByte(OperacaoRendaFixaMetadata.ColumnNames.IdCustodia);
			}
			
			set
			{
				base.SetSystemByte(OperacaoRendaFixaMetadata.ColumnNames.IdCustodia, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoRendaFixa.IdLiquidacao
		/// </summary>
		virtual public System.Byte? IdLiquidacao
		{
			get
			{
				return base.GetSystemByte(OperacaoRendaFixaMetadata.ColumnNames.IdLiquidacao);
			}
			
			set
			{
				base.SetSystemByte(OperacaoRendaFixaMetadata.ColumnNames.IdLiquidacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoRendaFixa.ValorCorretagem
		/// </summary>
		virtual public System.Decimal? ValorCorretagem
		{
			get
			{
				return base.GetSystemDecimal(OperacaoRendaFixaMetadata.ColumnNames.ValorCorretagem);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoRendaFixaMetadata.ColumnNames.ValorCorretagem, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoRendaFixa.NumeroNota
		/// </summary>
		virtual public System.Int32? NumeroNota
		{
			get
			{
				return base.GetSystemInt32(OperacaoRendaFixaMetadata.ColumnNames.NumeroNota);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoRendaFixaMetadata.ColumnNames.NumeroNota, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoRendaFixa.ValorISS
		/// </summary>
		virtual public System.Decimal? ValorISS
		{
			get
			{
				return base.GetSystemDecimal(OperacaoRendaFixaMetadata.ColumnNames.ValorISS);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoRendaFixaMetadata.ColumnNames.ValorISS, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoRendaFixa.CodigoContraParte
		/// </summary>
		virtual public System.String CodigoContraParte
		{
			get
			{
				return base.GetSystemString(OperacaoRendaFixaMetadata.ColumnNames.CodigoContraParte);
			}
			
			set
			{
				base.SetSystemString(OperacaoRendaFixaMetadata.ColumnNames.CodigoContraParte, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoRendaFixa.Emolumento
		/// </summary>
		virtual public System.Decimal? Emolumento
		{
			get
			{
				return base.GetSystemDecimal(OperacaoRendaFixaMetadata.ColumnNames.Emolumento);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoRendaFixaMetadata.ColumnNames.Emolumento, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoRendaFixa.StatusExportacao
		/// </summary>
		virtual public System.Int32? StatusExportacao
		{
			get
			{
				return base.GetSystemInt32(OperacaoRendaFixaMetadata.ColumnNames.StatusExportacao);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoRendaFixaMetadata.ColumnNames.StatusExportacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoRendaFixa.Observacao
		/// </summary>
		virtual public System.String Observacao
		{
			get
			{
				return base.GetSystemString(OperacaoRendaFixaMetadata.ColumnNames.Observacao);
			}
			
			set
			{
				base.SetSystemString(OperacaoRendaFixaMetadata.ColumnNames.Observacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoRendaFixa.IdIndiceVolta
		/// </summary>
		virtual public System.Int16? IdIndiceVolta
		{
			get
			{
				return base.GetSystemInt16(OperacaoRendaFixaMetadata.ColumnNames.IdIndiceVolta);
			}
			
			set
			{
				base.SetSystemInt16(OperacaoRendaFixaMetadata.ColumnNames.IdIndiceVolta, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoRendaFixa.DataRegistro
		/// </summary>
		virtual public System.DateTime? DataRegistro
		{
			get
			{
				return base.GetSystemDateTime(OperacaoRendaFixaMetadata.ColumnNames.DataRegistro);
			}
			
			set
			{
				base.SetSystemDateTime(OperacaoRendaFixaMetadata.ColumnNames.DataRegistro, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoRendaFixa.IdOperacaoVinculo
		/// </summary>
		virtual public System.Int32? IdOperacaoVinculo
		{
			get
			{
				return base.GetSystemInt32(OperacaoRendaFixaMetadata.ColumnNames.IdOperacaoVinculo);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoRendaFixaMetadata.ColumnNames.IdOperacaoVinculo, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoRendaFixa.OperacaoTermo
		/// </summary>
		virtual public System.String OperacaoTermo
		{
			get
			{
				return base.GetSystemString(OperacaoRendaFixaMetadata.ColumnNames.OperacaoTermo);
			}
			
			set
			{
				base.SetSystemString(OperacaoRendaFixaMetadata.ColumnNames.OperacaoTermo, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoRendaFixa.RendimentoNaoTributavel
		/// </summary>
		virtual public System.Decimal? RendimentoNaoTributavel
		{
			get
			{
				return base.GetSystemDecimal(OperacaoRendaFixaMetadata.ColumnNames.RendimentoNaoTributavel);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoRendaFixaMetadata.ColumnNames.RendimentoNaoTributavel, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoRendaFixa.Status
		/// </summary>
		virtual public System.Byte? Status
		{
			get
			{
				return base.GetSystemByte(OperacaoRendaFixaMetadata.ColumnNames.Status);
			}
			
			set
			{
				base.SetSystemByte(OperacaoRendaFixaMetadata.ColumnNames.Status, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoRendaFixa.IsIPO
		/// </summary>
		virtual public System.String IsIPO
		{
			get
			{
				return base.GetSystemString(OperacaoRendaFixaMetadata.ColumnNames.IsIPO);
			}
			
			set
			{
				base.SetSystemString(OperacaoRendaFixaMetadata.ColumnNames.IsIPO, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoRendaFixa.IdOperacaoExterna
		/// </summary>
		virtual public System.Int64? IdOperacaoExterna
		{
			get
			{
				return base.GetSystemInt64(OperacaoRendaFixaMetadata.ColumnNames.IdOperacaoExterna);
			}
			
			set
			{
				base.SetSystemInt64(OperacaoRendaFixaMetadata.ColumnNames.IdOperacaoExterna, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoRendaFixa.IdCarteiraContraparte
		/// </summary>
		virtual public System.Int32? IdCarteiraContraparte
		{
			get
			{
				return base.GetSystemInt32(OperacaoRendaFixaMetadata.ColumnNames.IdCarteiraContraparte);
			}
			
			set
			{
				if(base.SetSystemInt32(OperacaoRendaFixaMetadata.ColumnNames.IdCarteiraContraparte, value))
				{
					this._UpToCarteiraByIdCarteiraContraparte = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OperacaoRendaFixa.IdAgenteContraParte
		/// </summary>
		virtual public System.Int32? IdAgenteContraParte
		{
			get
			{
				return base.GetSystemInt32(OperacaoRendaFixaMetadata.ColumnNames.IdAgenteContraParte);
			}
			
			set
			{
				if(base.SetSystemInt32(OperacaoRendaFixaMetadata.ColumnNames.IdAgenteContraParte, value))
				{
					this._UpToAgenteMercadoByIdAgenteContraParte = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OperacaoRendaFixa.IdOperacaoEspelho
		/// </summary>
		virtual public System.Int32? IdOperacaoEspelho
		{
			get
			{
				return base.GetSystemInt32(OperacaoRendaFixaMetadata.ColumnNames.IdOperacaoEspelho);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoRendaFixaMetadata.ColumnNames.IdOperacaoEspelho, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoRendaFixa.DataModificacao
		/// </summary>
		virtual public System.DateTime? DataModificacao
		{
			get
			{
				return base.GetSystemDateTime(OperacaoRendaFixaMetadata.ColumnNames.DataModificacao);
			}
			
			set
			{
				base.SetSystemDateTime(OperacaoRendaFixaMetadata.ColumnNames.DataModificacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoRendaFixa.StatusBatimento
		/// </summary>
		virtual public System.Int32? StatusBatimento
		{
			get
			{
				return base.GetSystemInt32(OperacaoRendaFixaMetadata.ColumnNames.StatusBatimento);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoRendaFixaMetadata.ColumnNames.StatusBatimento, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoRendaFixa.PercentualPuFace
		/// </summary>
		virtual public System.Decimal? PercentualPuFace
		{
			get
			{
				return base.GetSystemDecimal(OperacaoRendaFixaMetadata.ColumnNames.PercentualPuFace);
			}
			
			set
			{
				base.SetSystemDecimal(OperacaoRendaFixaMetadata.ColumnNames.PercentualPuFace, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoRendaFixa.IdLocalNegociacao
		/// </summary>
		virtual public System.Int32? IdLocalNegociacao
		{
			get
			{
				return base.GetSystemInt32(OperacaoRendaFixaMetadata.ColumnNames.IdLocalNegociacao);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoRendaFixaMetadata.ColumnNames.IdLocalNegociacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoRendaFixa.IdAgenteCustodia
		/// </summary>
		virtual public System.Int32? IdAgenteCustodia
		{
			get
			{
				return base.GetSystemInt32(OperacaoRendaFixaMetadata.ColumnNames.IdAgenteCustodia);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoRendaFixaMetadata.ColumnNames.IdAgenteCustodia, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoRendaFixa.IdBoletaExterna
		/// </summary>
		virtual public System.Int32? IdBoletaExterna
		{
			get
			{
				return base.GetSystemInt32(OperacaoRendaFixaMetadata.ColumnNames.IdBoletaExterna);
			}
			
			set
			{
				base.SetSystemInt32(OperacaoRendaFixaMetadata.ColumnNames.IdBoletaExterna, value);
			}
		}
		
		/// <summary>
		/// Maps to OperacaoRendaFixa.IdTrader
		/// </summary>
		virtual public System.Int32? IdTrader
		{
			get
			{
				return base.GetSystemInt32(OperacaoRendaFixaMetadata.ColumnNames.IdTrader);
			}
			
			set
			{
				if(base.SetSystemInt32(OperacaoRendaFixaMetadata.ColumnNames.IdTrader, value))
				{
					this._UpToTraderByIdTrader = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OperacaoRendaFixa.IdConta
		/// </summary>
		virtual public System.Int32? IdConta
		{
			get
			{
				return base.GetSystemInt32(OperacaoRendaFixaMetadata.ColumnNames.IdConta);
			}
			
			set
			{
				if(base.SetSystemInt32(OperacaoRendaFixaMetadata.ColumnNames.IdConta, value))
				{
					this._UpToContaCorrenteByIdConta = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OperacaoRendaFixa.IdCategoriaMovimentacao
		/// </summary>
		virtual public System.Int32? IdCategoriaMovimentacao
		{
			get
			{
				return base.GetSystemInt32(OperacaoRendaFixaMetadata.ColumnNames.IdCategoriaMovimentacao);
			}
			
			set
			{
				if(base.SetSystemInt32(OperacaoRendaFixaMetadata.ColumnNames.IdCategoriaMovimentacao, value))
				{
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao = null;
				}
			}
		}
		
		[CLSCompliant(false)]
		internal protected AgenteMercado _UpToAgenteMercadoByIdAgenteContraParte;
		[CLSCompliant(false)]
		internal protected Carteira _UpToCarteiraByIdCarteiraContraparte;
		[CLSCompliant(false)]
		internal protected CategoriaMovimentacao _UpToCategoriaMovimentacaoByIdCategoriaMovimentacao;
		[CLSCompliant(false)]
        internal protected refeInvestidor.Cliente _UpToClienteByIdCliente;
		[CLSCompliant(false)]
        internal protected refeInvestidor.ContaCorrente _UpToContaCorrenteByIdConta;
		[CLSCompliant(false)]
		internal protected TituloRendaFixa _UpToTituloRendaFixaByIdTitulo;
		[CLSCompliant(false)]
		internal protected Trader _UpToTraderByIdTrader;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esOperacaoRendaFixa entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdOperacao
			{
				get
				{
					System.Int32? data = entity.IdOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacao = null;
					else entity.IdOperacao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String IdTitulo
			{
				get
				{
					System.Int32? data = entity.IdTitulo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTitulo = null;
					else entity.IdTitulo = Convert.ToInt32(value);
				}
			}
				
			public System.String DataOperacao
			{
				get
				{
					System.DateTime? data = entity.DataOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataOperacao = null;
					else entity.DataOperacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataLiquidacao
			{
				get
				{
					System.DateTime? data = entity.DataLiquidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataLiquidacao = null;
					else entity.DataLiquidacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String TipoOperacao
			{
				get
				{
					System.Byte? data = entity.TipoOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoOperacao = null;
					else entity.TipoOperacao = Convert.ToByte(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Decimal? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String PUOperacao
			{
				get
				{
					System.Decimal? data = entity.PUOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PUOperacao = null;
					else entity.PUOperacao = Convert.ToDecimal(value);
				}
			}
				
			public System.String Valor
			{
				get
				{
					System.Decimal? data = entity.Valor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Valor = null;
					else entity.Valor = Convert.ToDecimal(value);
				}
			}
				
			public System.String TaxaOperacao
			{
				get
				{
					System.Decimal? data = entity.TaxaOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TaxaOperacao = null;
					else entity.TaxaOperacao = Convert.ToDecimal(value);
				}
			}
				
			public System.String Fonte
			{
				get
				{
					System.Byte? data = entity.Fonte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Fonte = null;
					else entity.Fonte = Convert.ToByte(value);
				}
			}
				
			public System.String DataVolta
			{
				get
				{
					System.DateTime? data = entity.DataVolta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataVolta = null;
					else entity.DataVolta = Convert.ToDateTime(value);
				}
			}
				
			public System.String TaxaVolta
			{
				get
				{
					System.Decimal? data = entity.TaxaVolta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TaxaVolta = null;
					else entity.TaxaVolta = Convert.ToDecimal(value);
				}
			}
				
			public System.String PUVolta
			{
				get
				{
					System.Decimal? data = entity.PUVolta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PUVolta = null;
					else entity.PUVolta = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorVolta
			{
				get
				{
					System.Decimal? data = entity.ValorVolta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorVolta = null;
					else entity.ValorVolta = Convert.ToDecimal(value);
				}
			}
				
			public System.String IdOperacaoResgatada
			{
				get
				{
					System.Int32? data = entity.IdOperacaoResgatada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacaoResgatada = null;
					else entity.IdOperacaoResgatada = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoNegociacao
			{
				get
				{
					System.Byte? data = entity.TipoNegociacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoNegociacao = null;
					else entity.TipoNegociacao = Convert.ToByte(value);
				}
			}
				
			public System.String Rendimento
			{
				get
				{
					System.Decimal? data = entity.Rendimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Rendimento = null;
					else entity.Rendimento = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIR
			{
				get
				{
					System.Decimal? data = entity.ValorIR;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIR = null;
					else entity.ValorIR = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIOF
			{
				get
				{
					System.Decimal? data = entity.ValorIOF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIOF = null;
					else entity.ValorIOF = Convert.ToDecimal(value);
				}
			}
				
			public System.String IdPosicaoResgatada
			{
				get
				{
					System.Int32? data = entity.IdPosicaoResgatada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPosicaoResgatada = null;
					else entity.IdPosicaoResgatada = Convert.ToInt32(value);
				}
			}
				
			public System.String ValorLiquido
			{
				get
				{
					System.Decimal? data = entity.ValorLiquido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorLiquido = null;
					else entity.ValorLiquido = Convert.ToDecimal(value);
				}
			}
				
			public System.String TaxaNegociacao
			{
				get
				{
					System.Decimal? data = entity.TaxaNegociacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TaxaNegociacao = null;
					else entity.TaxaNegociacao = Convert.ToDecimal(value);
				}
			}
				
			public System.String IdAgenteCorretora
			{
				get
				{
					System.Int32? data = entity.IdAgenteCorretora;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgenteCorretora = null;
					else entity.IdAgenteCorretora = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCustodia
			{
				get
				{
					System.Byte? data = entity.IdCustodia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCustodia = null;
					else entity.IdCustodia = Convert.ToByte(value);
				}
			}
				
			public System.String IdLiquidacao
			{
				get
				{
					System.Byte? data = entity.IdLiquidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdLiquidacao = null;
					else entity.IdLiquidacao = Convert.ToByte(value);
				}
			}
				
			public System.String ValorCorretagem
			{
				get
				{
					System.Decimal? data = entity.ValorCorretagem;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorCorretagem = null;
					else entity.ValorCorretagem = Convert.ToDecimal(value);
				}
			}
				
			public System.String NumeroNota
			{
				get
				{
					System.Int32? data = entity.NumeroNota;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.NumeroNota = null;
					else entity.NumeroNota = Convert.ToInt32(value);
				}
			}
				
			public System.String ValorISS
			{
				get
				{
					System.Decimal? data = entity.ValorISS;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorISS = null;
					else entity.ValorISS = Convert.ToDecimal(value);
				}
			}
				
			public System.String CodigoContraParte
			{
				get
				{
					System.String data = entity.CodigoContraParte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoContraParte = null;
					else entity.CodigoContraParte = Convert.ToString(value);
				}
			}
				
			public System.String Emolumento
			{
				get
				{
					System.Decimal? data = entity.Emolumento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Emolumento = null;
					else entity.Emolumento = Convert.ToDecimal(value);
				}
			}
				
			public System.String StatusExportacao
			{
				get
				{
					System.Int32? data = entity.StatusExportacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.StatusExportacao = null;
					else entity.StatusExportacao = Convert.ToInt32(value);
				}
			}
				
			public System.String Observacao
			{
				get
				{
					System.String data = entity.Observacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Observacao = null;
					else entity.Observacao = Convert.ToString(value);
				}
			}
				
			public System.String IdIndiceVolta
			{
				get
				{
					System.Int16? data = entity.IdIndiceVolta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdIndiceVolta = null;
					else entity.IdIndiceVolta = Convert.ToInt16(value);
				}
			}
				
			public System.String DataRegistro
			{
				get
				{
					System.DateTime? data = entity.DataRegistro;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataRegistro = null;
					else entity.DataRegistro = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdOperacaoVinculo
			{
				get
				{
					System.Int32? data = entity.IdOperacaoVinculo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacaoVinculo = null;
					else entity.IdOperacaoVinculo = Convert.ToInt32(value);
				}
			}
				
			public System.String OperacaoTermo
			{
				get
				{
					System.String data = entity.OperacaoTermo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.OperacaoTermo = null;
					else entity.OperacaoTermo = Convert.ToString(value);
				}
			}
				
			public System.String RendimentoNaoTributavel
			{
				get
				{
					System.Decimal? data = entity.RendimentoNaoTributavel;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RendimentoNaoTributavel = null;
					else entity.RendimentoNaoTributavel = Convert.ToDecimal(value);
				}
			}
				
			public System.String Status
			{
				get
				{
					System.Byte? data = entity.Status;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Status = null;
					else entity.Status = Convert.ToByte(value);
				}
			}
				
			public System.String IsIPO
			{
				get
				{
					System.String data = entity.IsIPO;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IsIPO = null;
					else entity.IsIPO = Convert.ToString(value);
				}
			}
				
			public System.String IdOperacaoExterna
			{
				get
				{
					System.Int64? data = entity.IdOperacaoExterna;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacaoExterna = null;
					else entity.IdOperacaoExterna = Convert.ToInt64(value);
				}
			}
				
			public System.String IdCarteiraContraparte
			{
				get
				{
					System.Int32? data = entity.IdCarteiraContraparte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteiraContraparte = null;
					else entity.IdCarteiraContraparte = Convert.ToInt32(value);
				}
			}
				
			public System.String IdAgenteContraParte
			{
				get
				{
					System.Int32? data = entity.IdAgenteContraParte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgenteContraParte = null;
					else entity.IdAgenteContraParte = Convert.ToInt32(value);
				}
			}
				
			public System.String IdOperacaoEspelho
			{
				get
				{
					System.Int32? data = entity.IdOperacaoEspelho;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacaoEspelho = null;
					else entity.IdOperacaoEspelho = Convert.ToInt32(value);
				}
			}
				
			public System.String DataModificacao
			{
				get
				{
					System.DateTime? data = entity.DataModificacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataModificacao = null;
					else entity.DataModificacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String StatusBatimento
			{
				get
				{
					System.Int32? data = entity.StatusBatimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.StatusBatimento = null;
					else entity.StatusBatimento = Convert.ToInt32(value);
				}
			}
				
			public System.String PercentualPuFace
			{
				get
				{
					System.Decimal? data = entity.PercentualPuFace;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PercentualPuFace = null;
					else entity.PercentualPuFace = Convert.ToDecimal(value);
				}
			}
				
			public System.String IdLocalNegociacao
			{
				get
				{
					System.Int32? data = entity.IdLocalNegociacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdLocalNegociacao = null;
					else entity.IdLocalNegociacao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdAgenteCustodia
			{
				get
				{
					System.Int32? data = entity.IdAgenteCustodia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgenteCustodia = null;
					else entity.IdAgenteCustodia = Convert.ToInt32(value);
				}
			}
				
			public System.String IdBoletaExterna
			{
				get
				{
					System.Int32? data = entity.IdBoletaExterna;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdBoletaExterna = null;
					else entity.IdBoletaExterna = Convert.ToInt32(value);
				}
			}
				
			public System.String IdTrader
			{
				get
				{
					System.Int32? data = entity.IdTrader;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTrader = null;
					else entity.IdTrader = Convert.ToInt32(value);
				}
			}
				
			public System.String IdConta
			{
				get
				{
					System.Int32? data = entity.IdConta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdConta = null;
					else entity.IdConta = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCategoriaMovimentacao
			{
				get
				{
					System.Int32? data = entity.IdCategoriaMovimentacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCategoriaMovimentacao = null;
					else entity.IdCategoriaMovimentacao = Convert.ToInt32(value);
				}
			}
			

			private esOperacaoRendaFixa entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esOperacaoRendaFixaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esOperacaoRendaFixa can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class OperacaoRendaFixa : esOperacaoRendaFixa
	{

				
		#region DetalhePosicaoAfetadaRFCollectionByIdOperacao - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - DetalhePosAfetadaRF_OpRF_FK
		/// </summary>

		[XmlIgnore]
		public DetalhePosicaoAfetadaRFCollection DetalhePosicaoAfetadaRFCollectionByIdOperacao
		{
			get
			{
				if(this._DetalhePosicaoAfetadaRFCollectionByIdOperacao == null)
				{
					this._DetalhePosicaoAfetadaRFCollectionByIdOperacao = new DetalhePosicaoAfetadaRFCollection();
					this._DetalhePosicaoAfetadaRFCollectionByIdOperacao.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("DetalhePosicaoAfetadaRFCollectionByIdOperacao", this._DetalhePosicaoAfetadaRFCollectionByIdOperacao);
				
					if(this.IdOperacao != null)
					{
						this._DetalhePosicaoAfetadaRFCollectionByIdOperacao.Query.Where(this._DetalhePosicaoAfetadaRFCollectionByIdOperacao.Query.IdOperacao == this.IdOperacao);
						this._DetalhePosicaoAfetadaRFCollectionByIdOperacao.Query.Load();

						// Auto-hookup Foreign Keys
						this._DetalhePosicaoAfetadaRFCollectionByIdOperacao.fks.Add(DetalhePosicaoAfetadaRFMetadata.ColumnNames.IdOperacao, this.IdOperacao);
					}
				}

				return this._DetalhePosicaoAfetadaRFCollectionByIdOperacao;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._DetalhePosicaoAfetadaRFCollectionByIdOperacao != null) 
				{ 
					this.RemovePostSave("DetalhePosicaoAfetadaRFCollectionByIdOperacao"); 
					this._DetalhePosicaoAfetadaRFCollectionByIdOperacao = null;
					
				} 
			} 			
		}

		private DetalhePosicaoAfetadaRFCollection _DetalhePosicaoAfetadaRFCollectionByIdOperacao;
		#endregion

				
		#region MemoriaCalculoRendaFixaCollectionByIdOperacao - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - MemCalRF_Operacao_FK
		/// </summary>

		[XmlIgnore]
		public MemoriaCalculoRendaFixaCollection MemoriaCalculoRendaFixaCollectionByIdOperacao
		{
			get
			{
				if(this._MemoriaCalculoRendaFixaCollectionByIdOperacao == null)
				{
					this._MemoriaCalculoRendaFixaCollectionByIdOperacao = new MemoriaCalculoRendaFixaCollection();
					this._MemoriaCalculoRendaFixaCollectionByIdOperacao.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("MemoriaCalculoRendaFixaCollectionByIdOperacao", this._MemoriaCalculoRendaFixaCollectionByIdOperacao);
				
					if(this.IdOperacao != null)
					{
						this._MemoriaCalculoRendaFixaCollectionByIdOperacao.Query.Where(this._MemoriaCalculoRendaFixaCollectionByIdOperacao.Query.IdOperacao == this.IdOperacao);
						this._MemoriaCalculoRendaFixaCollectionByIdOperacao.Query.Load();

						// Auto-hookup Foreign Keys
						this._MemoriaCalculoRendaFixaCollectionByIdOperacao.fks.Add(MemoriaCalculoRendaFixaMetadata.ColumnNames.IdOperacao, this.IdOperacao);
					}
				}

				return this._MemoriaCalculoRendaFixaCollectionByIdOperacao;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._MemoriaCalculoRendaFixaCollectionByIdOperacao != null) 
				{ 
					this.RemovePostSave("MemoriaCalculoRendaFixaCollectionByIdOperacao"); 
					this._MemoriaCalculoRendaFixaCollectionByIdOperacao = null;
					
				} 
			} 			
		}

		private MemoriaCalculoRendaFixaCollection _MemoriaCalculoRendaFixaCollectionByIdOperacao;
		#endregion

				
		#region PerfilMTMCollectionByIdOperacao - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - PerfilMTM_Operacao_FK
		/// </summary>

		[XmlIgnore]
		public PerfilMTMCollection PerfilMTMCollectionByIdOperacao
		{
			get
			{
				if(this._PerfilMTMCollectionByIdOperacao == null)
				{
					this._PerfilMTMCollectionByIdOperacao = new PerfilMTMCollection();
					this._PerfilMTMCollectionByIdOperacao.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PerfilMTMCollectionByIdOperacao", this._PerfilMTMCollectionByIdOperacao);
				
					if(this.IdOperacao != null)
					{
						this._PerfilMTMCollectionByIdOperacao.Query.Where(this._PerfilMTMCollectionByIdOperacao.Query.IdOperacao == this.IdOperacao);
						this._PerfilMTMCollectionByIdOperacao.Query.Load();

						// Auto-hookup Foreign Keys
						this._PerfilMTMCollectionByIdOperacao.fks.Add(PerfilMTMMetadata.ColumnNames.IdOperacao, this.IdOperacao);
					}
				}

				return this._PerfilMTMCollectionByIdOperacao;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PerfilMTMCollectionByIdOperacao != null) 
				{ 
					this.RemovePostSave("PerfilMTMCollectionByIdOperacao"); 
					this._PerfilMTMCollectionByIdOperacao = null;
					
				} 
			} 			
		}

		private PerfilMTMCollection _PerfilMTMCollectionByIdOperacao;
		#endregion

				
		#region PosicaoRendaFixaCollectionByIdOperacao - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Operacao_PosicaoRendaFixa_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoRendaFixaCollection PosicaoRendaFixaCollectionByIdOperacao
		{
			get
			{
				if(this._PosicaoRendaFixaCollectionByIdOperacao == null)
				{
					this._PosicaoRendaFixaCollectionByIdOperacao = new PosicaoRendaFixaCollection();
					this._PosicaoRendaFixaCollectionByIdOperacao.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoRendaFixaCollectionByIdOperacao", this._PosicaoRendaFixaCollectionByIdOperacao);
				
					if(this.IdOperacao != null)
					{
						this._PosicaoRendaFixaCollectionByIdOperacao.Query.Where(this._PosicaoRendaFixaCollectionByIdOperacao.Query.IdOperacao == this.IdOperacao);
						this._PosicaoRendaFixaCollectionByIdOperacao.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoRendaFixaCollectionByIdOperacao.fks.Add(PosicaoRendaFixaMetadata.ColumnNames.IdOperacao, this.IdOperacao);
					}
				}

				return this._PosicaoRendaFixaCollectionByIdOperacao;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoRendaFixaCollectionByIdOperacao != null) 
				{ 
					this.RemovePostSave("PosicaoRendaFixaCollectionByIdOperacao"); 
					this._PosicaoRendaFixaCollectionByIdOperacao = null;
					
				} 
			} 			
		}

		private PosicaoRendaFixaCollection _PosicaoRendaFixaCollectionByIdOperacao;
		#endregion

				
		#region UpToAgenteMercadoByIdAgenteContraParte - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK__OperacaoR__IdAge__7B5130AA
		/// </summary>

		[XmlIgnore]
		public AgenteMercado UpToAgenteMercadoByIdAgenteContraParte
		{
			get
			{
				if(this._UpToAgenteMercadoByIdAgenteContraParte == null
					&& IdAgenteContraParte != null					)
				{
					this._UpToAgenteMercadoByIdAgenteContraParte = new AgenteMercado();
					this._UpToAgenteMercadoByIdAgenteContraParte.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAgenteMercadoByIdAgenteContraParte", this._UpToAgenteMercadoByIdAgenteContraParte);
					this._UpToAgenteMercadoByIdAgenteContraParte.Query.Where(this._UpToAgenteMercadoByIdAgenteContraParte.Query.IdAgente == this.IdAgenteContraParte);
					this._UpToAgenteMercadoByIdAgenteContraParte.Query.Load();
				}

				return this._UpToAgenteMercadoByIdAgenteContraParte;
			}
			
			set
			{
				this.RemovePreSave("UpToAgenteMercadoByIdAgenteContraParte");
				

				if(value == null)
				{
					this.IdAgenteContraParte = null;
					this._UpToAgenteMercadoByIdAgenteContraParte = null;
				}
				else
				{
					this.IdAgenteContraParte = value.IdAgente;
					this._UpToAgenteMercadoByIdAgenteContraParte = value;
					this.SetPreSave("UpToAgenteMercadoByIdAgenteContraParte", this._UpToAgenteMercadoByIdAgenteContraParte);
				}
				
			}
		}
		#endregion
		

				
		#region UpToCarteiraByIdCarteiraContraparte - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK__OperacaoR__IdCar__48C5B0DD
		/// </summary>

		[XmlIgnore]
		public Carteira UpToCarteiraByIdCarteiraContraparte
		{
			get
			{
				if(this._UpToCarteiraByIdCarteiraContraparte == null
					&& IdCarteiraContraparte != null					)
				{
					this._UpToCarteiraByIdCarteiraContraparte = new Carteira();
					this._UpToCarteiraByIdCarteiraContraparte.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCarteiraByIdCarteiraContraparte", this._UpToCarteiraByIdCarteiraContraparte);
					this._UpToCarteiraByIdCarteiraContraparte.Query.Where(this._UpToCarteiraByIdCarteiraContraparte.Query.IdCarteira == this.IdCarteiraContraparte);
					this._UpToCarteiraByIdCarteiraContraparte.Query.Load();
				}

				return this._UpToCarteiraByIdCarteiraContraparte;
			}
			
			set
			{
				this.RemovePreSave("UpToCarteiraByIdCarteiraContraparte");
				

				if(value == null)
				{
					this.IdCarteiraContraparte = null;
					this._UpToCarteiraByIdCarteiraContraparte = null;
				}
				else
				{
					this.IdCarteiraContraparte = value.IdCarteira;
					this._UpToCarteiraByIdCarteiraContraparte = value;
					this.SetPreSave("UpToCarteiraByIdCarteiraContraparte", this._UpToCarteiraByIdCarteiraContraparte);
				}
				
			}
		}
		#endregion
		

				
		#region UpToCategoriaMovimentacaoByIdCategoriaMovimentacao - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - OperacaoRF_CatMovimentacao_FK1
		/// </summary>

		[XmlIgnore]
		public CategoriaMovimentacao UpToCategoriaMovimentacaoByIdCategoriaMovimentacao
		{
			get
			{
				if(this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao == null
					&& IdCategoriaMovimentacao != null					)
				{
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao = new CategoriaMovimentacao();
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCategoriaMovimentacaoByIdCategoriaMovimentacao", this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao);
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao.Query.Where(this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao.Query.IdCategoriaMovimentacao == this.IdCategoriaMovimentacao);
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao.Query.Load();
				}

				return this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao;
			}
			
			set
			{
				this.RemovePreSave("UpToCategoriaMovimentacaoByIdCategoriaMovimentacao");
				

				if(value == null)
				{
					this.IdCategoriaMovimentacao = null;
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao = null;
				}
				else
				{
					this.IdCategoriaMovimentacao = value.IdCategoriaMovimentacao;
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao = value;
					this.SetPreSave("UpToCategoriaMovimentacaoByIdCategoriaMovimentacao", this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao);
				}
				
			}
		}
		#endregion
		

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_OperacaoRendaFixa_FK1
		/// </summary>

		[XmlIgnore]
        public refeInvestidor.Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
                    this._UpToClienteByIdCliente = new refeInvestidor.Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToContaCorrenteByIdConta - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK_OperacaoRendaFixa_ContaCorrente
		/// </summary>

		[XmlIgnore]
        public refeInvestidor.ContaCorrente UpToContaCorrenteByIdConta
		{
			get
			{
				if(this._UpToContaCorrenteByIdConta == null
					&& IdConta != null					)
				{
                    this._UpToContaCorrenteByIdConta = new refeInvestidor.ContaCorrente();
					this._UpToContaCorrenteByIdConta.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToContaCorrenteByIdConta", this._UpToContaCorrenteByIdConta);
					this._UpToContaCorrenteByIdConta.Query.Where(this._UpToContaCorrenteByIdConta.Query.IdConta == this.IdConta);
					this._UpToContaCorrenteByIdConta.Query.Load();
				}

				return this._UpToContaCorrenteByIdConta;
			}
			
			set
			{
				this.RemovePreSave("UpToContaCorrenteByIdConta");
				

				if(value == null)
				{
					this.IdConta = null;
					this._UpToContaCorrenteByIdConta = null;
				}
				else
				{
					this.IdConta = value.IdConta;
					this._UpToContaCorrenteByIdConta = value;
					this.SetPreSave("UpToContaCorrenteByIdConta", this._UpToContaCorrenteByIdConta);
				}
				
			}
		}
		#endregion
		

				
		#region UpToTituloRendaFixaByIdTitulo - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - TituloRendaFixa_OperacaoRendaFixa_FK1
		/// </summary>

		[XmlIgnore]
		public TituloRendaFixa UpToTituloRendaFixaByIdTitulo
		{
			get
			{
				if(this._UpToTituloRendaFixaByIdTitulo == null
					&& IdTitulo != null					)
				{
					this._UpToTituloRendaFixaByIdTitulo = new TituloRendaFixa();
					this._UpToTituloRendaFixaByIdTitulo.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToTituloRendaFixaByIdTitulo", this._UpToTituloRendaFixaByIdTitulo);
					this._UpToTituloRendaFixaByIdTitulo.Query.Where(this._UpToTituloRendaFixaByIdTitulo.Query.IdTitulo == this.IdTitulo);
					this._UpToTituloRendaFixaByIdTitulo.Query.Load();
				}

				return this._UpToTituloRendaFixaByIdTitulo;
			}
			
			set
			{
				this.RemovePreSave("UpToTituloRendaFixaByIdTitulo");
				

				if(value == null)
				{
					this.IdTitulo = null;
					this._UpToTituloRendaFixaByIdTitulo = null;
				}
				else
				{
					this.IdTitulo = value.IdTitulo;
					this._UpToTituloRendaFixaByIdTitulo = value;
					this.SetPreSave("UpToTituloRendaFixaByIdTitulo", this._UpToTituloRendaFixaByIdTitulo);
				}
				
			}
		}
		#endregion
		

				
		#region UpToTraderByIdTrader - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - OpRendaFixa_Trader_FK
		/// </summary>

		[XmlIgnore]
		public Trader UpToTraderByIdTrader
		{
			get
			{
				if(this._UpToTraderByIdTrader == null
					&& IdTrader != null					)
				{
					this._UpToTraderByIdTrader = new Trader();
					this._UpToTraderByIdTrader.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToTraderByIdTrader", this._UpToTraderByIdTrader);
					this._UpToTraderByIdTrader.Query.Where(this._UpToTraderByIdTrader.Query.IdTrader == this.IdTrader);
					this._UpToTraderByIdTrader.Query.Load();
				}

				return this._UpToTraderByIdTrader;
			}
			
			set
			{
				this.RemovePreSave("UpToTraderByIdTrader");
				

				if(value == null)
				{
					this.IdTrader = null;
					this._UpToTraderByIdTrader = null;
				}
				else
				{
					this.IdTrader = value.IdTrader;
					this._UpToTraderByIdTrader = value;
					this.SetPreSave("UpToTraderByIdTrader", this._UpToTraderByIdTrader);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "DetalhePosicaoAfetadaRFCollectionByIdOperacao", typeof(DetalhePosicaoAfetadaRFCollection), new DetalhePosicaoAfetadaRF()));
			props.Add(new esPropertyDescriptor(this, "MemoriaCalculoRendaFixaCollectionByIdOperacao", typeof(MemoriaCalculoRendaFixaCollection), new MemoriaCalculoRendaFixa()));
			props.Add(new esPropertyDescriptor(this, "PerfilMTMCollectionByIdOperacao", typeof(PerfilMTMCollection), new PerfilMTM()));
			props.Add(new esPropertyDescriptor(this, "PosicaoRendaFixaCollectionByIdOperacao", typeof(PosicaoRendaFixaCollection), new PosicaoRendaFixa()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToAgenteMercadoByIdAgenteContraParte != null)
			{
				this.IdAgenteContraParte = this._UpToAgenteMercadoByIdAgenteContraParte.IdAgente;
			}
			if(!this.es.IsDeleted && this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao != null)
			{
				this.IdCategoriaMovimentacao = this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao.IdCategoriaMovimentacao;
			}
			if(!this.es.IsDeleted && this._UpToContaCorrenteByIdConta != null)
			{
				this.IdConta = this._UpToContaCorrenteByIdConta.IdConta;
			}
			if(!this.es.IsDeleted && this._UpToTituloRendaFixaByIdTitulo != null)
			{
				this.IdTitulo = this._UpToTituloRendaFixaByIdTitulo.IdTitulo;
			}
			if(!this.es.IsDeleted && this._UpToTraderByIdTrader != null)
			{
				this.IdTrader = this._UpToTraderByIdTrader.IdTrader;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._DetalhePosicaoAfetadaRFCollectionByIdOperacao != null)
			{
				foreach(DetalhePosicaoAfetadaRF obj in this._DetalhePosicaoAfetadaRFCollectionByIdOperacao)
				{
					if(obj.es.IsAdded)
					{
						obj.IdOperacao = this.IdOperacao;
					}
				}
			}
			if(this._MemoriaCalculoRendaFixaCollectionByIdOperacao != null)
			{
				foreach(MemoriaCalculoRendaFixa obj in this._MemoriaCalculoRendaFixaCollectionByIdOperacao)
				{
					if(obj.es.IsAdded)
					{
						obj.IdOperacao = this.IdOperacao;
					}
				}
			}
			if(this._PerfilMTMCollectionByIdOperacao != null)
			{
				foreach(PerfilMTM obj in this._PerfilMTMCollectionByIdOperacao)
				{
					if(obj.es.IsAdded)
					{
						obj.IdOperacao = this.IdOperacao;
					}
				}
			}
			if(this._PosicaoRendaFixaCollectionByIdOperacao != null)
			{
				foreach(PosicaoRendaFixa obj in this._PosicaoRendaFixaCollectionByIdOperacao)
				{
					if(obj.es.IsAdded)
					{
						obj.IdOperacao = this.IdOperacao;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esOperacaoRendaFixaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return OperacaoRendaFixaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdOperacao
		{
			get
			{
				return new esQueryItem(this, OperacaoRendaFixaMetadata.ColumnNames.IdOperacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, OperacaoRendaFixaMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdTitulo
		{
			get
			{
				return new esQueryItem(this, OperacaoRendaFixaMetadata.ColumnNames.IdTitulo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataOperacao
		{
			get
			{
				return new esQueryItem(this, OperacaoRendaFixaMetadata.ColumnNames.DataOperacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataLiquidacao
		{
			get
			{
				return new esQueryItem(this, OperacaoRendaFixaMetadata.ColumnNames.DataLiquidacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem TipoOperacao
		{
			get
			{
				return new esQueryItem(this, OperacaoRendaFixaMetadata.ColumnNames.TipoOperacao, esSystemType.Byte);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, OperacaoRendaFixaMetadata.ColumnNames.Quantidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PUOperacao
		{
			get
			{
				return new esQueryItem(this, OperacaoRendaFixaMetadata.ColumnNames.PUOperacao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Valor
		{
			get
			{
				return new esQueryItem(this, OperacaoRendaFixaMetadata.ColumnNames.Valor, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TaxaOperacao
		{
			get
			{
				return new esQueryItem(this, OperacaoRendaFixaMetadata.ColumnNames.TaxaOperacao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Fonte
		{
			get
			{
				return new esQueryItem(this, OperacaoRendaFixaMetadata.ColumnNames.Fonte, esSystemType.Byte);
			}
		} 
		
		public esQueryItem DataVolta
		{
			get
			{
				return new esQueryItem(this, OperacaoRendaFixaMetadata.ColumnNames.DataVolta, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem TaxaVolta
		{
			get
			{
				return new esQueryItem(this, OperacaoRendaFixaMetadata.ColumnNames.TaxaVolta, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PUVolta
		{
			get
			{
				return new esQueryItem(this, OperacaoRendaFixaMetadata.ColumnNames.PUVolta, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorVolta
		{
			get
			{
				return new esQueryItem(this, OperacaoRendaFixaMetadata.ColumnNames.ValorVolta, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IdOperacaoResgatada
		{
			get
			{
				return new esQueryItem(this, OperacaoRendaFixaMetadata.ColumnNames.IdOperacaoResgatada, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoNegociacao
		{
			get
			{
				return new esQueryItem(this, OperacaoRendaFixaMetadata.ColumnNames.TipoNegociacao, esSystemType.Byte);
			}
		} 
		
		public esQueryItem Rendimento
		{
			get
			{
				return new esQueryItem(this, OperacaoRendaFixaMetadata.ColumnNames.Rendimento, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIR
		{
			get
			{
				return new esQueryItem(this, OperacaoRendaFixaMetadata.ColumnNames.ValorIR, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIOF
		{
			get
			{
				return new esQueryItem(this, OperacaoRendaFixaMetadata.ColumnNames.ValorIOF, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IdPosicaoResgatada
		{
			get
			{
				return new esQueryItem(this, OperacaoRendaFixaMetadata.ColumnNames.IdPosicaoResgatada, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ValorLiquido
		{
			get
			{
				return new esQueryItem(this, OperacaoRendaFixaMetadata.ColumnNames.ValorLiquido, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TaxaNegociacao
		{
			get
			{
				return new esQueryItem(this, OperacaoRendaFixaMetadata.ColumnNames.TaxaNegociacao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IdAgenteCorretora
		{
			get
			{
				return new esQueryItem(this, OperacaoRendaFixaMetadata.ColumnNames.IdAgenteCorretora, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCustodia
		{
			get
			{
				return new esQueryItem(this, OperacaoRendaFixaMetadata.ColumnNames.IdCustodia, esSystemType.Byte);
			}
		} 
		
		public esQueryItem IdLiquidacao
		{
			get
			{
				return new esQueryItem(this, OperacaoRendaFixaMetadata.ColumnNames.IdLiquidacao, esSystemType.Byte);
			}
		} 
		
		public esQueryItem ValorCorretagem
		{
			get
			{
				return new esQueryItem(this, OperacaoRendaFixaMetadata.ColumnNames.ValorCorretagem, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem NumeroNota
		{
			get
			{
				return new esQueryItem(this, OperacaoRendaFixaMetadata.ColumnNames.NumeroNota, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ValorISS
		{
			get
			{
				return new esQueryItem(this, OperacaoRendaFixaMetadata.ColumnNames.ValorISS, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CodigoContraParte
		{
			get
			{
				return new esQueryItem(this, OperacaoRendaFixaMetadata.ColumnNames.CodigoContraParte, esSystemType.String);
			}
		} 
		
		public esQueryItem Emolumento
		{
			get
			{
				return new esQueryItem(this, OperacaoRendaFixaMetadata.ColumnNames.Emolumento, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem StatusExportacao
		{
			get
			{
				return new esQueryItem(this, OperacaoRendaFixaMetadata.ColumnNames.StatusExportacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Observacao
		{
			get
			{
				return new esQueryItem(this, OperacaoRendaFixaMetadata.ColumnNames.Observacao, esSystemType.String);
			}
		} 
		
		public esQueryItem IdIndiceVolta
		{
			get
			{
				return new esQueryItem(this, OperacaoRendaFixaMetadata.ColumnNames.IdIndiceVolta, esSystemType.Int16);
			}
		} 
		
		public esQueryItem DataRegistro
		{
			get
			{
				return new esQueryItem(this, OperacaoRendaFixaMetadata.ColumnNames.DataRegistro, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdOperacaoVinculo
		{
			get
			{
				return new esQueryItem(this, OperacaoRendaFixaMetadata.ColumnNames.IdOperacaoVinculo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem OperacaoTermo
		{
			get
			{
				return new esQueryItem(this, OperacaoRendaFixaMetadata.ColumnNames.OperacaoTermo, esSystemType.String);
			}
		} 
		
		public esQueryItem RendimentoNaoTributavel
		{
			get
			{
				return new esQueryItem(this, OperacaoRendaFixaMetadata.ColumnNames.RendimentoNaoTributavel, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Status
		{
			get
			{
				return new esQueryItem(this, OperacaoRendaFixaMetadata.ColumnNames.Status, esSystemType.Byte);
			}
		} 
		
		public esQueryItem IsIPO
		{
			get
			{
				return new esQueryItem(this, OperacaoRendaFixaMetadata.ColumnNames.IsIPO, esSystemType.String);
			}
		} 
		
		public esQueryItem IdOperacaoExterna
		{
			get
			{
				return new esQueryItem(this, OperacaoRendaFixaMetadata.ColumnNames.IdOperacaoExterna, esSystemType.Int64);
			}
		} 
		
		public esQueryItem IdCarteiraContraparte
		{
			get
			{
				return new esQueryItem(this, OperacaoRendaFixaMetadata.ColumnNames.IdCarteiraContraparte, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdAgenteContraParte
		{
			get
			{
				return new esQueryItem(this, OperacaoRendaFixaMetadata.ColumnNames.IdAgenteContraParte, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdOperacaoEspelho
		{
			get
			{
				return new esQueryItem(this, OperacaoRendaFixaMetadata.ColumnNames.IdOperacaoEspelho, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataModificacao
		{
			get
			{
				return new esQueryItem(this, OperacaoRendaFixaMetadata.ColumnNames.DataModificacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem StatusBatimento
		{
			get
			{
				return new esQueryItem(this, OperacaoRendaFixaMetadata.ColumnNames.StatusBatimento, esSystemType.Int32);
			}
		} 
		
		public esQueryItem PercentualPuFace
		{
			get
			{
				return new esQueryItem(this, OperacaoRendaFixaMetadata.ColumnNames.PercentualPuFace, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IdLocalNegociacao
		{
			get
			{
				return new esQueryItem(this, OperacaoRendaFixaMetadata.ColumnNames.IdLocalNegociacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdAgenteCustodia
		{
			get
			{
				return new esQueryItem(this, OperacaoRendaFixaMetadata.ColumnNames.IdAgenteCustodia, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdBoletaExterna
		{
			get
			{
				return new esQueryItem(this, OperacaoRendaFixaMetadata.ColumnNames.IdBoletaExterna, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdTrader
		{
			get
			{
				return new esQueryItem(this, OperacaoRendaFixaMetadata.ColumnNames.IdTrader, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdConta
		{
			get
			{
				return new esQueryItem(this, OperacaoRendaFixaMetadata.ColumnNames.IdConta, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCategoriaMovimentacao
		{
			get
			{
				return new esQueryItem(this, OperacaoRendaFixaMetadata.ColumnNames.IdCategoriaMovimentacao, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("OperacaoRendaFixaCollection")]
	public partial class OperacaoRendaFixaCollection : esOperacaoRendaFixaCollection, IEnumerable<OperacaoRendaFixa>
	{
		public OperacaoRendaFixaCollection()
		{

		}
		
		public static implicit operator List<OperacaoRendaFixa>(OperacaoRendaFixaCollection coll)
		{
			List<OperacaoRendaFixa> list = new List<OperacaoRendaFixa>();
			
			foreach (OperacaoRendaFixa emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  OperacaoRendaFixaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new OperacaoRendaFixaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new OperacaoRendaFixa(row);
		}

		override protected esEntity CreateEntity()
		{
			return new OperacaoRendaFixa();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public OperacaoRendaFixaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new OperacaoRendaFixaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(OperacaoRendaFixaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public OperacaoRendaFixa AddNew()
		{
			OperacaoRendaFixa entity = base.AddNewEntity() as OperacaoRendaFixa;
			
			return entity;
		}

		public OperacaoRendaFixa FindByPrimaryKey(System.Int32 idOperacao)
		{
			return base.FindByPrimaryKey(idOperacao) as OperacaoRendaFixa;
		}


		#region IEnumerable<OperacaoRendaFixa> Members

		IEnumerator<OperacaoRendaFixa> IEnumerable<OperacaoRendaFixa>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as OperacaoRendaFixa;
			}
		}

		#endregion
		
		private OperacaoRendaFixaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'OperacaoRendaFixa' table
	/// </summary>

	[Serializable]
	public partial class OperacaoRendaFixa : esOperacaoRendaFixa
	{
		public OperacaoRendaFixa()
		{

		}
	
		public OperacaoRendaFixa(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return OperacaoRendaFixaMetadata.Meta();
			}
		}
		
		
		
		override protected esOperacaoRendaFixaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new OperacaoRendaFixaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public OperacaoRendaFixaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new OperacaoRendaFixaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(OperacaoRendaFixaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private OperacaoRendaFixaQuery query;
	}



	[Serializable]
	public partial class OperacaoRendaFixaQuery : esOperacaoRendaFixaQuery
	{
		public OperacaoRendaFixaQuery()
		{

		}		
		
		public OperacaoRendaFixaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class OperacaoRendaFixaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected OperacaoRendaFixaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(OperacaoRendaFixaMetadata.ColumnNames.IdOperacao, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoRendaFixaMetadata.PropertyNames.IdOperacao;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoRendaFixaMetadata.ColumnNames.IdCliente, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoRendaFixaMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoRendaFixaMetadata.ColumnNames.IdTitulo, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoRendaFixaMetadata.PropertyNames.IdTitulo;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoRendaFixaMetadata.ColumnNames.DataOperacao, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OperacaoRendaFixaMetadata.PropertyNames.DataOperacao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoRendaFixaMetadata.ColumnNames.DataLiquidacao, 4, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OperacaoRendaFixaMetadata.PropertyNames.DataLiquidacao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoRendaFixaMetadata.ColumnNames.TipoOperacao, 5, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = OperacaoRendaFixaMetadata.PropertyNames.TipoOperacao;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoRendaFixaMetadata.ColumnNames.Quantidade, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoRendaFixaMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoRendaFixaMetadata.ColumnNames.PUOperacao, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoRendaFixaMetadata.PropertyNames.PUOperacao;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoRendaFixaMetadata.ColumnNames.Valor, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoRendaFixaMetadata.PropertyNames.Valor;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoRendaFixaMetadata.ColumnNames.TaxaOperacao, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoRendaFixaMetadata.PropertyNames.TaxaOperacao;	
			c.NumericPrecision = 25;
			c.NumericScale = 16;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoRendaFixaMetadata.ColumnNames.Fonte, 10, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = OperacaoRendaFixaMetadata.PropertyNames.Fonte;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoRendaFixaMetadata.ColumnNames.DataVolta, 11, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OperacaoRendaFixaMetadata.PropertyNames.DataVolta;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoRendaFixaMetadata.ColumnNames.TaxaVolta, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoRendaFixaMetadata.PropertyNames.TaxaVolta;	
			c.NumericPrecision = 8;
			c.NumericScale = 4;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoRendaFixaMetadata.ColumnNames.PUVolta, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoRendaFixaMetadata.PropertyNames.PUVolta;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoRendaFixaMetadata.ColumnNames.ValorVolta, 14, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoRendaFixaMetadata.PropertyNames.ValorVolta;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoRendaFixaMetadata.ColumnNames.IdOperacaoResgatada, 15, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoRendaFixaMetadata.PropertyNames.IdOperacaoResgatada;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoRendaFixaMetadata.ColumnNames.TipoNegociacao, 16, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = OperacaoRendaFixaMetadata.PropertyNames.TipoNegociacao;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoRendaFixaMetadata.ColumnNames.Rendimento, 17, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoRendaFixaMetadata.PropertyNames.Rendimento;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoRendaFixaMetadata.ColumnNames.ValorIR, 18, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoRendaFixaMetadata.PropertyNames.ValorIR;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoRendaFixaMetadata.ColumnNames.ValorIOF, 19, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoRendaFixaMetadata.PropertyNames.ValorIOF;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoRendaFixaMetadata.ColumnNames.IdPosicaoResgatada, 20, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoRendaFixaMetadata.PropertyNames.IdPosicaoResgatada;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoRendaFixaMetadata.ColumnNames.ValorLiquido, 21, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoRendaFixaMetadata.PropertyNames.ValorLiquido;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoRendaFixaMetadata.ColumnNames.TaxaNegociacao, 22, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoRendaFixaMetadata.PropertyNames.TaxaNegociacao;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoRendaFixaMetadata.ColumnNames.IdAgenteCorretora, 23, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoRendaFixaMetadata.PropertyNames.IdAgenteCorretora;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoRendaFixaMetadata.ColumnNames.IdCustodia, 24, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = OperacaoRendaFixaMetadata.PropertyNames.IdCustodia;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoRendaFixaMetadata.ColumnNames.IdLiquidacao, 25, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = OperacaoRendaFixaMetadata.PropertyNames.IdLiquidacao;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoRendaFixaMetadata.ColumnNames.ValorCorretagem, 26, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoRendaFixaMetadata.PropertyNames.ValorCorretagem;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoRendaFixaMetadata.ColumnNames.NumeroNota, 27, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoRendaFixaMetadata.PropertyNames.NumeroNota;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoRendaFixaMetadata.ColumnNames.ValorISS, 28, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoRendaFixaMetadata.PropertyNames.ValorISS;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoRendaFixaMetadata.ColumnNames.CodigoContraParte, 29, typeof(System.String), esSystemType.String);
			c.PropertyName = OperacaoRendaFixaMetadata.PropertyNames.CodigoContraParte;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoRendaFixaMetadata.ColumnNames.Emolumento, 30, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoRendaFixaMetadata.PropertyNames.Emolumento;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoRendaFixaMetadata.ColumnNames.StatusExportacao, 31, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoRendaFixaMetadata.PropertyNames.StatusExportacao;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoRendaFixaMetadata.ColumnNames.Observacao, 32, typeof(System.String), esSystemType.String);
			c.PropertyName = OperacaoRendaFixaMetadata.PropertyNames.Observacao;
			c.CharacterMaxLength = 200;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoRendaFixaMetadata.ColumnNames.IdIndiceVolta, 33, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = OperacaoRendaFixaMetadata.PropertyNames.IdIndiceVolta;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoRendaFixaMetadata.ColumnNames.DataRegistro, 34, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OperacaoRendaFixaMetadata.PropertyNames.DataRegistro;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoRendaFixaMetadata.ColumnNames.IdOperacaoVinculo, 35, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoRendaFixaMetadata.PropertyNames.IdOperacaoVinculo;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoRendaFixaMetadata.ColumnNames.OperacaoTermo, 36, typeof(System.String), esSystemType.String);
			c.PropertyName = OperacaoRendaFixaMetadata.PropertyNames.OperacaoTermo;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoRendaFixaMetadata.ColumnNames.RendimentoNaoTributavel, 37, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoRendaFixaMetadata.PropertyNames.RendimentoNaoTributavel;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoRendaFixaMetadata.ColumnNames.Status, 38, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = OperacaoRendaFixaMetadata.PropertyNames.Status;	
			c.NumericPrecision = 3;
			c.HasDefault = true;
			c.Default = @"((1))";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoRendaFixaMetadata.ColumnNames.IsIPO, 39, typeof(System.String), esSystemType.String);
			c.PropertyName = OperacaoRendaFixaMetadata.PropertyNames.IsIPO;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoRendaFixaMetadata.ColumnNames.IdOperacaoExterna, 40, typeof(System.Int64), esSystemType.Int64);
			c.PropertyName = OperacaoRendaFixaMetadata.PropertyNames.IdOperacaoExterna;	
			c.NumericPrecision = 19;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoRendaFixaMetadata.ColumnNames.IdCarteiraContraparte, 41, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoRendaFixaMetadata.PropertyNames.IdCarteiraContraparte;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoRendaFixaMetadata.ColumnNames.IdAgenteContraParte, 42, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoRendaFixaMetadata.PropertyNames.IdAgenteContraParte;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoRendaFixaMetadata.ColumnNames.IdOperacaoEspelho, 43, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoRendaFixaMetadata.PropertyNames.IdOperacaoEspelho;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoRendaFixaMetadata.ColumnNames.DataModificacao, 44, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OperacaoRendaFixaMetadata.PropertyNames.DataModificacao;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"(getdate())";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoRendaFixaMetadata.ColumnNames.StatusBatimento, 45, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoRendaFixaMetadata.PropertyNames.StatusBatimento;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoRendaFixaMetadata.ColumnNames.PercentualPuFace, 46, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OperacaoRendaFixaMetadata.PropertyNames.PercentualPuFace;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			c.HasDefault = true;
			c.Default = @"('0')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoRendaFixaMetadata.ColumnNames.IdLocalNegociacao, 47, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoRendaFixaMetadata.PropertyNames.IdLocalNegociacao;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoRendaFixaMetadata.ColumnNames.IdAgenteCustodia, 48, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoRendaFixaMetadata.PropertyNames.IdAgenteCustodia;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoRendaFixaMetadata.ColumnNames.IdBoletaExterna, 49, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoRendaFixaMetadata.PropertyNames.IdBoletaExterna;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoRendaFixaMetadata.ColumnNames.IdTrader, 50, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoRendaFixaMetadata.PropertyNames.IdTrader;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoRendaFixaMetadata.ColumnNames.IdConta, 51, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoRendaFixaMetadata.PropertyNames.IdConta;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OperacaoRendaFixaMetadata.ColumnNames.IdCategoriaMovimentacao, 52, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OperacaoRendaFixaMetadata.PropertyNames.IdCategoriaMovimentacao;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public OperacaoRendaFixaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdOperacao = "IdOperacao";
			 public const string IdCliente = "IdCliente";
			 public const string IdTitulo = "IdTitulo";
			 public const string DataOperacao = "DataOperacao";
			 public const string DataLiquidacao = "DataLiquidacao";
			 public const string TipoOperacao = "TipoOperacao";
			 public const string Quantidade = "Quantidade";
			 public const string PUOperacao = "PUOperacao";
			 public const string Valor = "Valor";
			 public const string TaxaOperacao = "TaxaOperacao";
			 public const string Fonte = "Fonte";
			 public const string DataVolta = "DataVolta";
			 public const string TaxaVolta = "TaxaVolta";
			 public const string PUVolta = "PUVolta";
			 public const string ValorVolta = "ValorVolta";
			 public const string IdOperacaoResgatada = "IdOperacaoResgatada";
			 public const string TipoNegociacao = "TipoNegociacao";
			 public const string Rendimento = "Rendimento";
			 public const string ValorIR = "ValorIR";
			 public const string ValorIOF = "ValorIOF";
			 public const string IdPosicaoResgatada = "IdPosicaoResgatada";
			 public const string ValorLiquido = "ValorLiquido";
			 public const string TaxaNegociacao = "TaxaNegociacao";
			 public const string IdAgenteCorretora = "IdAgenteCorretora";
			 public const string IdCustodia = "IdCustodia";
			 public const string IdLiquidacao = "IdLiquidacao";
			 public const string ValorCorretagem = "ValorCorretagem";
			 public const string NumeroNota = "NumeroNota";
			 public const string ValorISS = "ValorISS";
			 public const string CodigoContraParte = "CodigoContraParte";
			 public const string Emolumento = "Emolumento";
			 public const string StatusExportacao = "StatusExportacao";
			 public const string Observacao = "Observacao";
			 public const string IdIndiceVolta = "IdIndiceVolta";
			 public const string DataRegistro = "DataRegistro";
			 public const string IdOperacaoVinculo = "IdOperacaoVinculo";
			 public const string OperacaoTermo = "OperacaoTermo";
			 public const string RendimentoNaoTributavel = "RendimentoNaoTributavel";
			 public const string Status = "Status";
			 public const string IsIPO = "IsIPO";
			 public const string IdOperacaoExterna = "IdOperacaoExterna";
			 public const string IdCarteiraContraparte = "IdCarteiraContraparte";
			 public const string IdAgenteContraParte = "IdAgenteContraParte";
			 public const string IdOperacaoEspelho = "IdOperacaoEspelho";
			 public const string DataModificacao = "DataModificacao";
			 public const string StatusBatimento = "StatusBatimento";
			 public const string PercentualPuFace = "PercentualPuFace";
			 public const string IdLocalNegociacao = "IdLocalNegociacao";
			 public const string IdAgenteCustodia = "IdAgenteCustodia";
			 public const string IdBoletaExterna = "IdBoletaExterna";
			 public const string IdTrader = "IdTrader";
			 public const string IdConta = "IdConta";
			 public const string IdCategoriaMovimentacao = "IdCategoriaMovimentacao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdOperacao = "IdOperacao";
			 public const string IdCliente = "IdCliente";
			 public const string IdTitulo = "IdTitulo";
			 public const string DataOperacao = "DataOperacao";
			 public const string DataLiquidacao = "DataLiquidacao";
			 public const string TipoOperacao = "TipoOperacao";
			 public const string Quantidade = "Quantidade";
			 public const string PUOperacao = "PUOperacao";
			 public const string Valor = "Valor";
			 public const string TaxaOperacao = "TaxaOperacao";
			 public const string Fonte = "Fonte";
			 public const string DataVolta = "DataVolta";
			 public const string TaxaVolta = "TaxaVolta";
			 public const string PUVolta = "PUVolta";
			 public const string ValorVolta = "ValorVolta";
			 public const string IdOperacaoResgatada = "IdOperacaoResgatada";
			 public const string TipoNegociacao = "TipoNegociacao";
			 public const string Rendimento = "Rendimento";
			 public const string ValorIR = "ValorIR";
			 public const string ValorIOF = "ValorIOF";
			 public const string IdPosicaoResgatada = "IdPosicaoResgatada";
			 public const string ValorLiquido = "ValorLiquido";
			 public const string TaxaNegociacao = "TaxaNegociacao";
			 public const string IdAgenteCorretora = "IdAgenteCorretora";
			 public const string IdCustodia = "IdCustodia";
			 public const string IdLiquidacao = "IdLiquidacao";
			 public const string ValorCorretagem = "ValorCorretagem";
			 public const string NumeroNota = "NumeroNota";
			 public const string ValorISS = "ValorISS";
			 public const string CodigoContraParte = "CodigoContraParte";
			 public const string Emolumento = "Emolumento";
			 public const string StatusExportacao = "StatusExportacao";
			 public const string Observacao = "Observacao";
			 public const string IdIndiceVolta = "IdIndiceVolta";
			 public const string DataRegistro = "DataRegistro";
			 public const string IdOperacaoVinculo = "IdOperacaoVinculo";
			 public const string OperacaoTermo = "OperacaoTermo";
			 public const string RendimentoNaoTributavel = "RendimentoNaoTributavel";
			 public const string Status = "Status";
			 public const string IsIPO = "IsIPO";
			 public const string IdOperacaoExterna = "IdOperacaoExterna";
			 public const string IdCarteiraContraparte = "IdCarteiraContraparte";
			 public const string IdAgenteContraParte = "IdAgenteContraParte";
			 public const string IdOperacaoEspelho = "IdOperacaoEspelho";
			 public const string DataModificacao = "DataModificacao";
			 public const string StatusBatimento = "StatusBatimento";
			 public const string PercentualPuFace = "PercentualPuFace";
			 public const string IdLocalNegociacao = "IdLocalNegociacao";
			 public const string IdAgenteCustodia = "IdAgenteCustodia";
			 public const string IdBoletaExterna = "IdBoletaExterna";
			 public const string IdTrader = "IdTrader";
			 public const string IdConta = "IdConta";
			 public const string IdCategoriaMovimentacao = "IdCategoriaMovimentacao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(OperacaoRendaFixaMetadata))
			{
				if(OperacaoRendaFixaMetadata.mapDelegates == null)
				{
					OperacaoRendaFixaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (OperacaoRendaFixaMetadata.meta == null)
				{
					OperacaoRendaFixaMetadata.meta = new OperacaoRendaFixaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdOperacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdTitulo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataOperacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataLiquidacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("TipoOperacao", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("Quantidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PUOperacao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Valor", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("TaxaOperacao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Fonte", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("DataVolta", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("TaxaVolta", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PUVolta", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorVolta", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IdOperacaoResgatada", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoNegociacao", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("Rendimento", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIR", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIOF", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IdPosicaoResgatada", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ValorLiquido", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("TaxaNegociacao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IdAgenteCorretora", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCustodia", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("IdLiquidacao", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("ValorCorretagem", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("NumeroNota", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ValorISS", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("CodigoContraParte", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Emolumento", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("StatusExportacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Observacao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IdIndiceVolta", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("DataRegistro", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdOperacaoVinculo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("OperacaoTermo", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("RendimentoNaoTributavel", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Status", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("IsIPO", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("IdOperacaoExterna", new esTypeMap("bigint", "System.Int64"));
				meta.AddTypeMap("IdCarteiraContraparte", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdAgenteContraParte", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdOperacaoEspelho", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataModificacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("StatusBatimento", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("PercentualPuFace", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IdLocalNegociacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdAgenteCustodia", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdBoletaExterna", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdTrader", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdConta", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCategoriaMovimentacao", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "OperacaoRendaFixa";
				meta.Destination = "OperacaoRendaFixa";
				
				meta.spInsert = "proc_OperacaoRendaFixaInsert";				
				meta.spUpdate = "proc_OperacaoRendaFixaUpdate";		
				meta.spDelete = "proc_OperacaoRendaFixaDelete";
				meta.spLoadAll = "proc_OperacaoRendaFixaLoadAll";
				meta.spLoadByPrimaryKey = "proc_OperacaoRendaFixaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private OperacaoRendaFixaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
