/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 12/12/2014 10:53:25 AM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.RendaFixa
{

	[Serializable]
	abstract public class esAgendaRendaFixaCollection : esEntityCollection
	{
		public esAgendaRendaFixaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "AgendaRendaFixaCollection";
		}

		#region Query Logic
		protected void InitQuery(esAgendaRendaFixaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esAgendaRendaFixaQuery);
		}
		#endregion
		
		virtual public AgendaRendaFixa DetachEntity(AgendaRendaFixa entity)
		{
			return base.DetachEntity(entity) as AgendaRendaFixa;
		}
		
		virtual public AgendaRendaFixa AttachEntity(AgendaRendaFixa entity)
		{
			return base.AttachEntity(entity) as AgendaRendaFixa;
		}
		
		virtual public void Combine(AgendaRendaFixaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public AgendaRendaFixa this[int index]
		{
			get
			{
				return base[index] as AgendaRendaFixa;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(AgendaRendaFixa);
		}
	}



	[Serializable]
	abstract public class esAgendaRendaFixa : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esAgendaRendaFixaQuery GetDynamicQuery()
		{
			return null;
		}

		public esAgendaRendaFixa()
		{

		}

		public esAgendaRendaFixa(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idAgenda)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idAgenda);
			else
				return LoadByPrimaryKeyStoredProcedure(idAgenda);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idAgenda)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esAgendaRendaFixaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdAgenda == idAgenda);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idAgenda)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idAgenda);
			else
				return LoadByPrimaryKeyStoredProcedure(idAgenda);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idAgenda)
		{
			esAgendaRendaFixaQuery query = this.GetDynamicQuery();
			query.Where(query.IdAgenda == idAgenda);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idAgenda)
		{
			esParameters parms = new esParameters();
			parms.Add("IdAgenda",idAgenda);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdAgenda": this.str.IdAgenda = (string)value; break;							
						case "IdTitulo": this.str.IdTitulo = (string)value; break;							
						case "TipoEvento": this.str.TipoEvento = (string)value; break;							
						case "DataAgenda": this.str.DataAgenda = (string)value; break;							
						case "DataEvento": this.str.DataEvento = (string)value; break;							
						case "DataPagamento": this.str.DataPagamento = (string)value; break;							
						case "Taxa": this.str.Taxa = (string)value; break;							
						case "Valor": this.str.Valor = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdAgenda":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgenda = (System.Int32?)value;
							break;
						
						case "IdTitulo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTitulo = (System.Int32?)value;
							break;
						
						case "TipoEvento":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoEvento = (System.Byte?)value;
							break;
						
						case "DataAgenda":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataAgenda = (System.DateTime?)value;
							break;
						
						case "DataEvento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataEvento = (System.DateTime?)value;
							break;
						
						case "DataPagamento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataPagamento = (System.DateTime?)value;
							break;
						
						case "Taxa":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Taxa = (System.Decimal?)value;
							break;
						
						case "Valor":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Valor = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to AgendaRendaFixa.IdAgenda
		/// </summary>
		virtual public System.Int32? IdAgenda
		{
			get
			{
				return base.GetSystemInt32(AgendaRendaFixaMetadata.ColumnNames.IdAgenda);
			}
			
			set
			{
				base.SetSystemInt32(AgendaRendaFixaMetadata.ColumnNames.IdAgenda, value);
			}
		}
		
		/// <summary>
		/// Maps to AgendaRendaFixa.IdTitulo
		/// </summary>
		virtual public System.Int32? IdTitulo
		{
			get
			{
				return base.GetSystemInt32(AgendaRendaFixaMetadata.ColumnNames.IdTitulo);
			}
			
			set
			{
				if(base.SetSystemInt32(AgendaRendaFixaMetadata.ColumnNames.IdTitulo, value))
				{
					this._UpToTituloRendaFixaByIdTitulo = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to AgendaRendaFixa.TipoEvento
		/// </summary>
		virtual public System.Byte? TipoEvento
		{
			get
			{
				return base.GetSystemByte(AgendaRendaFixaMetadata.ColumnNames.TipoEvento);
			}
			
			set
			{
				base.SetSystemByte(AgendaRendaFixaMetadata.ColumnNames.TipoEvento, value);
			}
		}
		
		/// <summary>
		/// Maps to AgendaRendaFixa.DataAgenda
		/// </summary>
		virtual public System.DateTime? DataAgenda
		{
			get
			{
				return base.GetSystemDateTime(AgendaRendaFixaMetadata.ColumnNames.DataAgenda);
			}
			
			set
			{
				base.SetSystemDateTime(AgendaRendaFixaMetadata.ColumnNames.DataAgenda, value);
			}
		}
		
		/// <summary>
		/// Maps to AgendaRendaFixa.DataEvento
		/// </summary>
		virtual public System.DateTime? DataEvento
		{
			get
			{
				return base.GetSystemDateTime(AgendaRendaFixaMetadata.ColumnNames.DataEvento);
			}
			
			set
			{
				base.SetSystemDateTime(AgendaRendaFixaMetadata.ColumnNames.DataEvento, value);
			}
		}
		
		/// <summary>
		/// Maps to AgendaRendaFixa.DataPagamento
		/// </summary>
		virtual public System.DateTime? DataPagamento
		{
			get
			{
				return base.GetSystemDateTime(AgendaRendaFixaMetadata.ColumnNames.DataPagamento);
			}
			
			set
			{
				base.SetSystemDateTime(AgendaRendaFixaMetadata.ColumnNames.DataPagamento, value);
			}
		}
		
		/// <summary>
		/// Maps to AgendaRendaFixa.Taxa
		/// </summary>
		virtual public System.Decimal? Taxa
		{
			get
			{
				return base.GetSystemDecimal(AgendaRendaFixaMetadata.ColumnNames.Taxa);
			}
			
			set
			{
				base.SetSystemDecimal(AgendaRendaFixaMetadata.ColumnNames.Taxa, value);
			}
		}
		
		/// <summary>
		/// Maps to AgendaRendaFixa.Valor
		/// </summary>
		virtual public System.Decimal? Valor
		{
			get
			{
				return base.GetSystemDecimal(AgendaRendaFixaMetadata.ColumnNames.Valor);
			}
			
			set
			{
				base.SetSystemDecimal(AgendaRendaFixaMetadata.ColumnNames.Valor, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected TituloRendaFixa _UpToTituloRendaFixaByIdTitulo;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esAgendaRendaFixa entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdAgenda
			{
				get
				{
					System.Int32? data = entity.IdAgenda;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgenda = null;
					else entity.IdAgenda = Convert.ToInt32(value);
				}
			}
				
			public System.String IdTitulo
			{
				get
				{
					System.Int32? data = entity.IdTitulo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTitulo = null;
					else entity.IdTitulo = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoEvento
			{
				get
				{
					System.Byte? data = entity.TipoEvento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoEvento = null;
					else entity.TipoEvento = Convert.ToByte(value);
				}
			}
				
			public System.String DataAgenda
			{
				get
				{
					System.DateTime? data = entity.DataAgenda;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataAgenda = null;
					else entity.DataAgenda = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataEvento
			{
				get
				{
					System.DateTime? data = entity.DataEvento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataEvento = null;
					else entity.DataEvento = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataPagamento
			{
				get
				{
					System.DateTime? data = entity.DataPagamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataPagamento = null;
					else entity.DataPagamento = Convert.ToDateTime(value);
				}
			}
				
			public System.String Taxa
			{
				get
				{
					System.Decimal? data = entity.Taxa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Taxa = null;
					else entity.Taxa = Convert.ToDecimal(value);
				}
			}
				
			public System.String Valor
			{
				get
				{
					System.Decimal? data = entity.Valor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Valor = null;
					else entity.Valor = Convert.ToDecimal(value);
				}
			}
			

			private esAgendaRendaFixa entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esAgendaRendaFixaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esAgendaRendaFixa can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class AgendaRendaFixa : esAgendaRendaFixa
	{

				
		#region UpToTituloRendaFixaByIdTitulo - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - TituloRendaFixa_AgendaRendaFixa_FK1
		/// </summary>

		[XmlIgnore]
		public TituloRendaFixa UpToTituloRendaFixaByIdTitulo
		{
			get
			{
				if(this._UpToTituloRendaFixaByIdTitulo == null
					&& IdTitulo != null					)
				{
					this._UpToTituloRendaFixaByIdTitulo = new TituloRendaFixa();
					this._UpToTituloRendaFixaByIdTitulo.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToTituloRendaFixaByIdTitulo", this._UpToTituloRendaFixaByIdTitulo);
					this._UpToTituloRendaFixaByIdTitulo.Query.Where(this._UpToTituloRendaFixaByIdTitulo.Query.IdTitulo == this.IdTitulo);
					this._UpToTituloRendaFixaByIdTitulo.Query.Load();
				}

				return this._UpToTituloRendaFixaByIdTitulo;
			}
			
			set
			{
				this.RemovePreSave("UpToTituloRendaFixaByIdTitulo");
				

				if(value == null)
				{
					this.IdTitulo = null;
					this._UpToTituloRendaFixaByIdTitulo = null;
				}
				else
				{
					this.IdTitulo = value.IdTitulo;
					this._UpToTituloRendaFixaByIdTitulo = value;
					this.SetPreSave("UpToTituloRendaFixaByIdTitulo", this._UpToTituloRendaFixaByIdTitulo);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToTituloRendaFixaByIdTitulo != null)
			{
				this.IdTitulo = this._UpToTituloRendaFixaByIdTitulo.IdTitulo;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esAgendaRendaFixaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return AgendaRendaFixaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdAgenda
		{
			get
			{
				return new esQueryItem(this, AgendaRendaFixaMetadata.ColumnNames.IdAgenda, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdTitulo
		{
			get
			{
				return new esQueryItem(this, AgendaRendaFixaMetadata.ColumnNames.IdTitulo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoEvento
		{
			get
			{
				return new esQueryItem(this, AgendaRendaFixaMetadata.ColumnNames.TipoEvento, esSystemType.Byte);
			}
		} 
		
		public esQueryItem DataAgenda
		{
			get
			{
				return new esQueryItem(this, AgendaRendaFixaMetadata.ColumnNames.DataAgenda, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataEvento
		{
			get
			{
				return new esQueryItem(this, AgendaRendaFixaMetadata.ColumnNames.DataEvento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataPagamento
		{
			get
			{
				return new esQueryItem(this, AgendaRendaFixaMetadata.ColumnNames.DataPagamento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Taxa
		{
			get
			{
				return new esQueryItem(this, AgendaRendaFixaMetadata.ColumnNames.Taxa, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Valor
		{
			get
			{
				return new esQueryItem(this, AgendaRendaFixaMetadata.ColumnNames.Valor, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("AgendaRendaFixaCollection")]
	public partial class AgendaRendaFixaCollection : esAgendaRendaFixaCollection, IEnumerable<AgendaRendaFixa>
	{
		public AgendaRendaFixaCollection()
		{

		}
		
		public static implicit operator List<AgendaRendaFixa>(AgendaRendaFixaCollection coll)
		{
			List<AgendaRendaFixa> list = new List<AgendaRendaFixa>();
			
			foreach (AgendaRendaFixa emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  AgendaRendaFixaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new AgendaRendaFixaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new AgendaRendaFixa(row);
		}

		override protected esEntity CreateEntity()
		{
			return new AgendaRendaFixa();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public AgendaRendaFixaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new AgendaRendaFixaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(AgendaRendaFixaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public AgendaRendaFixa AddNew()
		{
			AgendaRendaFixa entity = base.AddNewEntity() as AgendaRendaFixa;
			
			return entity;
		}

		public AgendaRendaFixa FindByPrimaryKey(System.Int32 idAgenda)
		{
			return base.FindByPrimaryKey(idAgenda) as AgendaRendaFixa;
		}


		#region IEnumerable<AgendaRendaFixa> Members

		IEnumerator<AgendaRendaFixa> IEnumerable<AgendaRendaFixa>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as AgendaRendaFixa;
			}
		}

		#endregion
		
		private AgendaRendaFixaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'AgendaRendaFixa' table
	/// </summary>

	[Serializable]
	public partial class AgendaRendaFixa : esAgendaRendaFixa
	{
		public AgendaRendaFixa()
		{

		}
	
		public AgendaRendaFixa(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return AgendaRendaFixaMetadata.Meta();
			}
		}
		
		
		
		override protected esAgendaRendaFixaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new AgendaRendaFixaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public AgendaRendaFixaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new AgendaRendaFixaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(AgendaRendaFixaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private AgendaRendaFixaQuery query;
	}



	[Serializable]
	public partial class AgendaRendaFixaQuery : esAgendaRendaFixaQuery
	{
		public AgendaRendaFixaQuery()
		{

		}		
		
		public AgendaRendaFixaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class AgendaRendaFixaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected AgendaRendaFixaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(AgendaRendaFixaMetadata.ColumnNames.IdAgenda, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = AgendaRendaFixaMetadata.PropertyNames.IdAgenda;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaRendaFixaMetadata.ColumnNames.IdTitulo, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = AgendaRendaFixaMetadata.PropertyNames.IdTitulo;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaRendaFixaMetadata.ColumnNames.TipoEvento, 2, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = AgendaRendaFixaMetadata.PropertyNames.TipoEvento;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaRendaFixaMetadata.ColumnNames.DataAgenda, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = AgendaRendaFixaMetadata.PropertyNames.DataAgenda;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaRendaFixaMetadata.ColumnNames.DataEvento, 4, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = AgendaRendaFixaMetadata.PropertyNames.DataEvento;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaRendaFixaMetadata.ColumnNames.DataPagamento, 5, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = AgendaRendaFixaMetadata.PropertyNames.DataPagamento;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaRendaFixaMetadata.ColumnNames.Taxa, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = AgendaRendaFixaMetadata.PropertyNames.Taxa;	
			c.NumericPrecision = 26;
			c.NumericScale = 20;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(AgendaRendaFixaMetadata.ColumnNames.Valor, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = AgendaRendaFixaMetadata.PropertyNames.Valor;	
			c.NumericPrecision = 20;
			c.NumericScale = 8;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public AgendaRendaFixaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdAgenda = "IdAgenda";
			 public const string IdTitulo = "IdTitulo";
			 public const string TipoEvento = "TipoEvento";
			 public const string DataAgenda = "DataAgenda";
			 public const string DataEvento = "DataEvento";
			 public const string DataPagamento = "DataPagamento";
			 public const string Taxa = "Taxa";
			 public const string Valor = "Valor";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdAgenda = "IdAgenda";
			 public const string IdTitulo = "IdTitulo";
			 public const string TipoEvento = "TipoEvento";
			 public const string DataAgenda = "DataAgenda";
			 public const string DataEvento = "DataEvento";
			 public const string DataPagamento = "DataPagamento";
			 public const string Taxa = "Taxa";
			 public const string Valor = "Valor";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(AgendaRendaFixaMetadata))
			{
				if(AgendaRendaFixaMetadata.mapDelegates == null)
				{
					AgendaRendaFixaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (AgendaRendaFixaMetadata.meta == null)
				{
					AgendaRendaFixaMetadata.meta = new AgendaRendaFixaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdAgenda", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdTitulo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoEvento", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("DataAgenda", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataEvento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataPagamento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Taxa", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Valor", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "AgendaRendaFixa";
				meta.Destination = "AgendaRendaFixa";
				
				meta.spInsert = "proc_AgendaRendaFixaInsert";				
				meta.spUpdate = "proc_AgendaRendaFixaUpdate";		
				meta.spDelete = "proc_AgendaRendaFixaDelete";
				meta.spLoadAll = "proc_AgendaRendaFixaLoadAll";
				meta.spLoadByPrimaryKey = "proc_AgendaRendaFixaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private AgendaRendaFixaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
