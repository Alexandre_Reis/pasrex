/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 06/10/2014 11:57:04
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.RendaFixa
{

	[Serializable]
	abstract public class esTaxaCurvaCollection : esEntityCollection
	{
		public esTaxaCurvaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TaxaCurvaCollection";
		}

		#region Query Logic
		protected void InitQuery(esTaxaCurvaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTaxaCurvaQuery);
		}
		#endregion
		
		virtual public TaxaCurva DetachEntity(TaxaCurva entity)
		{
			return base.DetachEntity(entity) as TaxaCurva;
		}
		
		virtual public TaxaCurva AttachEntity(TaxaCurva entity)
		{
			return base.AttachEntity(entity) as TaxaCurva;
		}
		
		virtual public void Combine(TaxaCurvaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TaxaCurva this[int index]
		{
			get
			{
				return base[index] as TaxaCurva;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TaxaCurva);
		}
	}



	[Serializable]
	abstract public class esTaxaCurva : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTaxaCurvaQuery GetDynamicQuery()
		{
			return null;
		}

		public esTaxaCurva()
		{

		}

		public esTaxaCurva(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime dataBase, System.DateTime dataVertice, System.Int32 idCurvaRendaFixa)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataBase, dataVertice, idCurvaRendaFixa);
			else
				return LoadByPrimaryKeyStoredProcedure(dataBase, dataVertice, idCurvaRendaFixa);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime dataBase, System.DateTime dataVertice, System.Int32 idCurvaRendaFixa)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataBase, dataVertice, idCurvaRendaFixa);
			else
				return LoadByPrimaryKeyStoredProcedure(dataBase, dataVertice, idCurvaRendaFixa);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime dataBase, System.DateTime dataVertice, System.Int32 idCurvaRendaFixa)
		{
			esTaxaCurvaQuery query = this.GetDynamicQuery();
			query.Where(query.DataBase == dataBase, query.DataVertice == dataVertice, query.IdCurvaRendaFixa == idCurvaRendaFixa);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime dataBase, System.DateTime dataVertice, System.Int32 idCurvaRendaFixa)
		{
			esParameters parms = new esParameters();
			parms.Add("DataBase",dataBase);			parms.Add("DataVertice",dataVertice);			parms.Add("IdCurvaRendaFixa",idCurvaRendaFixa);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdCurvaRendaFixa": this.str.IdCurvaRendaFixa = (string)value; break;							
						case "DataBase": this.str.DataBase = (string)value; break;							
						case "DataVertice": this.str.DataVertice = (string)value; break;							
						case "CodigoVertice": this.str.CodigoVertice = (string)value; break;							
						case "PrazoDU": this.str.PrazoDU = (string)value; break;							
						case "PrazoDC": this.str.PrazoDC = (string)value; break;							
						case "Taxa": this.str.Taxa = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdCurvaRendaFixa":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCurvaRendaFixa = (System.Int32?)value;
							break;
						
						case "DataBase":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataBase = (System.DateTime?)value;
							break;
						
						case "DataVertice":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataVertice = (System.DateTime?)value;
							break;
						
						case "PrazoDU":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.PrazoDU = (System.Int32?)value;
							break;
						
						case "PrazoDC":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.PrazoDC = (System.Int32?)value;
							break;
						
						case "Taxa":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Taxa = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TaxaCurva.IdCurvaRendaFixa
		/// </summary>
		virtual public System.Int32? IdCurvaRendaFixa
		{
			get
			{
				return base.GetSystemInt32(TaxaCurvaMetadata.ColumnNames.IdCurvaRendaFixa);
			}
			
			set
			{
				if(base.SetSystemInt32(TaxaCurvaMetadata.ColumnNames.IdCurvaRendaFixa, value))
				{
					this._UpToCurvaRendaFixaByIdCurvaRendaFixa = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TaxaCurva.DataBase
		/// </summary>
		virtual public System.DateTime? DataBase
		{
			get
			{
				return base.GetSystemDateTime(TaxaCurvaMetadata.ColumnNames.DataBase);
			}
			
			set
			{
				base.SetSystemDateTime(TaxaCurvaMetadata.ColumnNames.DataBase, value);
			}
		}
		
		/// <summary>
		/// Maps to TaxaCurva.DataVertice
		/// </summary>
		virtual public System.DateTime? DataVertice
		{
			get
			{
				return base.GetSystemDateTime(TaxaCurvaMetadata.ColumnNames.DataVertice);
			}
			
			set
			{
				base.SetSystemDateTime(TaxaCurvaMetadata.ColumnNames.DataVertice, value);
			}
		}
		
		/// <summary>
		/// Maps to TaxaCurva.CodigoVertice
		/// </summary>
		virtual public System.String CodigoVertice
		{
			get
			{
				return base.GetSystemString(TaxaCurvaMetadata.ColumnNames.CodigoVertice);
			}
			
			set
			{
				base.SetSystemString(TaxaCurvaMetadata.ColumnNames.CodigoVertice, value);
			}
		}
		
		/// <summary>
		/// Maps to TaxaCurva.PrazoDU
		/// </summary>
		virtual public System.Int32? PrazoDU
		{
			get
			{
				return base.GetSystemInt32(TaxaCurvaMetadata.ColumnNames.PrazoDU);
			}
			
			set
			{
				base.SetSystemInt32(TaxaCurvaMetadata.ColumnNames.PrazoDU, value);
			}
		}
		
		/// <summary>
		/// Maps to TaxaCurva.PrazoDC
		/// </summary>
		virtual public System.Int32? PrazoDC
		{
			get
			{
				return base.GetSystemInt32(TaxaCurvaMetadata.ColumnNames.PrazoDC);
			}
			
			set
			{
				base.SetSystemInt32(TaxaCurvaMetadata.ColumnNames.PrazoDC, value);
			}
		}
		
		/// <summary>
		/// Maps to TaxaCurva.Taxa
		/// </summary>
		virtual public System.Decimal? Taxa
		{
			get
			{
				return base.GetSystemDecimal(TaxaCurvaMetadata.ColumnNames.Taxa);
			}
			
			set
			{
				base.SetSystemDecimal(TaxaCurvaMetadata.ColumnNames.Taxa, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected CurvaRendaFixa _UpToCurvaRendaFixaByIdCurvaRendaFixa;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTaxaCurva entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdCurvaRendaFixa
			{
				get
				{
					System.Int32? data = entity.IdCurvaRendaFixa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCurvaRendaFixa = null;
					else entity.IdCurvaRendaFixa = Convert.ToInt32(value);
				}
			}
				
			public System.String DataBase
			{
				get
				{
					System.DateTime? data = entity.DataBase;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataBase = null;
					else entity.DataBase = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataVertice
			{
				get
				{
					System.DateTime? data = entity.DataVertice;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataVertice = null;
					else entity.DataVertice = Convert.ToDateTime(value);
				}
			}
				
			public System.String CodigoVertice
			{
				get
				{
					System.String data = entity.CodigoVertice;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoVertice = null;
					else entity.CodigoVertice = Convert.ToString(value);
				}
			}
				
			public System.String PrazoDU
			{
				get
				{
					System.Int32? data = entity.PrazoDU;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PrazoDU = null;
					else entity.PrazoDU = Convert.ToInt32(value);
				}
			}
				
			public System.String PrazoDC
			{
				get
				{
					System.Int32? data = entity.PrazoDC;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PrazoDC = null;
					else entity.PrazoDC = Convert.ToInt32(value);
				}
			}
				
			public System.String Taxa
			{
				get
				{
					System.Decimal? data = entity.Taxa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Taxa = null;
					else entity.Taxa = Convert.ToDecimal(value);
				}
			}
			

			private esTaxaCurva entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTaxaCurvaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTaxaCurva can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TaxaCurva : esTaxaCurva
	{

				
		#region UpToCurvaRendaFixaByIdCurvaRendaFixa - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - TaxaCurva_CurvaRF_FK
		/// </summary>

		[XmlIgnore]
		public CurvaRendaFixa UpToCurvaRendaFixaByIdCurvaRendaFixa
		{
			get
			{
				if(this._UpToCurvaRendaFixaByIdCurvaRendaFixa == null
					&& IdCurvaRendaFixa != null					)
				{
					this._UpToCurvaRendaFixaByIdCurvaRendaFixa = new CurvaRendaFixa();
					this._UpToCurvaRendaFixaByIdCurvaRendaFixa.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCurvaRendaFixaByIdCurvaRendaFixa", this._UpToCurvaRendaFixaByIdCurvaRendaFixa);
					this._UpToCurvaRendaFixaByIdCurvaRendaFixa.Query.Where(this._UpToCurvaRendaFixaByIdCurvaRendaFixa.Query.IdCurvaRendaFixa == this.IdCurvaRendaFixa);
					this._UpToCurvaRendaFixaByIdCurvaRendaFixa.Query.Load();
				}

				return this._UpToCurvaRendaFixaByIdCurvaRendaFixa;
			}
			
			set
			{
				this.RemovePreSave("UpToCurvaRendaFixaByIdCurvaRendaFixa");
				

				if(value == null)
				{
					this.IdCurvaRendaFixa = null;
					this._UpToCurvaRendaFixaByIdCurvaRendaFixa = null;
				}
				else
				{
					this.IdCurvaRendaFixa = value.IdCurvaRendaFixa;
					this._UpToCurvaRendaFixaByIdCurvaRendaFixa = value;
					this.SetPreSave("UpToCurvaRendaFixaByIdCurvaRendaFixa", this._UpToCurvaRendaFixaByIdCurvaRendaFixa);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTaxaCurvaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TaxaCurvaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdCurvaRendaFixa
		{
			get
			{
				return new esQueryItem(this, TaxaCurvaMetadata.ColumnNames.IdCurvaRendaFixa, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataBase
		{
			get
			{
				return new esQueryItem(this, TaxaCurvaMetadata.ColumnNames.DataBase, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataVertice
		{
			get
			{
				return new esQueryItem(this, TaxaCurvaMetadata.ColumnNames.DataVertice, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem CodigoVertice
		{
			get
			{
				return new esQueryItem(this, TaxaCurvaMetadata.ColumnNames.CodigoVertice, esSystemType.String);
			}
		} 
		
		public esQueryItem PrazoDU
		{
			get
			{
				return new esQueryItem(this, TaxaCurvaMetadata.ColumnNames.PrazoDU, esSystemType.Int32);
			}
		} 
		
		public esQueryItem PrazoDC
		{
			get
			{
				return new esQueryItem(this, TaxaCurvaMetadata.ColumnNames.PrazoDC, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Taxa
		{
			get
			{
				return new esQueryItem(this, TaxaCurvaMetadata.ColumnNames.Taxa, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TaxaCurvaCollection")]
	public partial class TaxaCurvaCollection : esTaxaCurvaCollection, IEnumerable<TaxaCurva>
	{
		public TaxaCurvaCollection()
		{

		}
		
		public static implicit operator List<TaxaCurva>(TaxaCurvaCollection coll)
		{
			List<TaxaCurva> list = new List<TaxaCurva>();
			
			foreach (TaxaCurva emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TaxaCurvaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TaxaCurvaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TaxaCurva(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TaxaCurva();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TaxaCurvaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TaxaCurvaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TaxaCurvaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TaxaCurva AddNew()
		{
			TaxaCurva entity = base.AddNewEntity() as TaxaCurva;
			
			return entity;
		}

		public TaxaCurva FindByPrimaryKey(System.DateTime dataBase, System.DateTime dataVertice, System.Int32 idCurvaRendaFixa)
		{
			return base.FindByPrimaryKey(dataBase, dataVertice, idCurvaRendaFixa) as TaxaCurva;
		}


		#region IEnumerable<TaxaCurva> Members

		IEnumerator<TaxaCurva> IEnumerable<TaxaCurva>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TaxaCurva;
			}
		}

		#endregion
		
		private TaxaCurvaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TaxaCurva' table
	/// </summary>

	[Serializable]
	public partial class TaxaCurva : esTaxaCurva
	{
		public TaxaCurva()
		{

		}
	
		public TaxaCurva(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TaxaCurvaMetadata.Meta();
			}
		}
		
		
		
		override protected esTaxaCurvaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TaxaCurvaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TaxaCurvaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TaxaCurvaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TaxaCurvaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TaxaCurvaQuery query;
	}



	[Serializable]
	public partial class TaxaCurvaQuery : esTaxaCurvaQuery
	{
		public TaxaCurvaQuery()
		{

		}		
		
		public TaxaCurvaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TaxaCurvaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TaxaCurvaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TaxaCurvaMetadata.ColumnNames.IdCurvaRendaFixa, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TaxaCurvaMetadata.PropertyNames.IdCurvaRendaFixa;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TaxaCurvaMetadata.ColumnNames.DataBase, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TaxaCurvaMetadata.PropertyNames.DataBase;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TaxaCurvaMetadata.ColumnNames.DataVertice, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TaxaCurvaMetadata.PropertyNames.DataVertice;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TaxaCurvaMetadata.ColumnNames.CodigoVertice, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = TaxaCurvaMetadata.PropertyNames.CodigoVertice;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TaxaCurvaMetadata.ColumnNames.PrazoDU, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TaxaCurvaMetadata.PropertyNames.PrazoDU;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TaxaCurvaMetadata.ColumnNames.PrazoDC, 5, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TaxaCurvaMetadata.PropertyNames.PrazoDC;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TaxaCurvaMetadata.ColumnNames.Taxa, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TaxaCurvaMetadata.PropertyNames.Taxa;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TaxaCurvaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdCurvaRendaFixa = "IdCurvaRendaFixa";
			 public const string DataBase = "DataBase";
			 public const string DataVertice = "DataVertice";
			 public const string CodigoVertice = "CodigoVertice";
			 public const string PrazoDU = "PrazoDU";
			 public const string PrazoDC = "PrazoDC";
			 public const string Taxa = "Taxa";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdCurvaRendaFixa = "IdCurvaRendaFixa";
			 public const string DataBase = "DataBase";
			 public const string DataVertice = "DataVertice";
			 public const string CodigoVertice = "CodigoVertice";
			 public const string PrazoDU = "PrazoDU";
			 public const string PrazoDC = "PrazoDC";
			 public const string Taxa = "Taxa";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TaxaCurvaMetadata))
			{
				if(TaxaCurvaMetadata.mapDelegates == null)
				{
					TaxaCurvaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TaxaCurvaMetadata.meta == null)
				{
					TaxaCurvaMetadata.meta = new TaxaCurvaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdCurvaRendaFixa", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataBase", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataVertice", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("CodigoVertice", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("PrazoDU", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("PrazoDC", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Taxa", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "TaxaCurva";
				meta.Destination = "TaxaCurva";
				
				meta.spInsert = "proc_TaxaCurvaInsert";				
				meta.spUpdate = "proc_TaxaCurvaUpdate";		
				meta.spDelete = "proc_TaxaCurvaDelete";
				meta.spLoadAll = "proc_TaxaCurvaLoadAll";
				meta.spLoadByPrimaryKey = "proc_TaxaCurvaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TaxaCurvaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
