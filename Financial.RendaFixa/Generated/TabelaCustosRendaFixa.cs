/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 6/23/2015 5:27:46 PM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;































































































































































using Financial.Investidor;















































namespace Financial.RendaFixa
{

    [Serializable]
    abstract public class esTabelaCustosRendaFixaCollection : esEntityCollection
    {
        public esTabelaCustosRendaFixaCollection()
        {

        }

        protected override string GetCollectionName()
        {
            return "TabelaCustosRendaFixaCollection";
        }

        #region Query Logic
        protected void InitQuery(esTabelaCustosRendaFixaQuery query)
        {
            query.OnLoadDelegate = this.OnQueryLoaded;
            query.es.Connection = ((IEntityCollection)this).Connection;
        }

        protected bool OnQueryLoaded(DataTable table)
        {
            this.PopulateCollection(table);
            return (this.RowCount > 0) ? true : false;
        }

        protected override void HookupQuery(esDynamicQuery query)
        {
            this.InitQuery(query as esTabelaCustosRendaFixaQuery);
        }
        #endregion

        virtual public TabelaCustosRendaFixa DetachEntity(TabelaCustosRendaFixa entity)
        {
            return base.DetachEntity(entity) as TabelaCustosRendaFixa;
        }

        virtual public TabelaCustosRendaFixa AttachEntity(TabelaCustosRendaFixa entity)
        {
            return base.AttachEntity(entity) as TabelaCustosRendaFixa;
        }

        virtual public void Combine(TabelaCustosRendaFixaCollection collection)
        {
            base.Combine(collection);
        }

        new public TabelaCustosRendaFixa this[int index]
        {
            get
            {
                return base[index] as TabelaCustosRendaFixa;
            }
        }

        public override Type GetEntityType()
        {
            return typeof(TabelaCustosRendaFixa);
        }
    }



    [Serializable]
    abstract public class esTabelaCustosRendaFixa : esEntity
    {
        /// <summary>
        /// Used internally by the entity's DynamicQuery mechanism.
        /// </summary>
        virtual protected esTabelaCustosRendaFixaQuery GetDynamicQuery()
        {
            return null;
        }

        public esTabelaCustosRendaFixa()
        {

        }

        public esTabelaCustosRendaFixa(DataRow row)
            : base(row)
        {

        }

        #region LoadByPrimaryKey
        public virtual bool LoadByPrimaryKey(System.Int32 idTabela)
        {
            if (this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
                return LoadByPrimaryKeyDynamic(idTabela);
            else
                return LoadByPrimaryKeyStoredProcedure(idTabela);
        }

        /// <summary>
        /// Loads an entity by primary key
        /// </summary>
        /// <remarks>
        /// EntitySpaces requires primary keys be defined on all tables.
        /// If a table does not have a primary key set,
        /// this method will not compile.
        /// Does not support sqlAcessType. It only works with DynamicQuery
        /// </remarks>
        /// <param name="fieldsToReturn">Fields desired</param>
        public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idTabela)
        {
            esQueryItem[] fields = fieldsToReturn.ToArray();
            esTabelaCustosRendaFixaQuery query = this.GetDynamicQuery();
            query
                .Select(fields)
                .Where(query.IdTabela == idTabela);

            return query.Load();
        }

        public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idTabela)
        {
            if (sqlAccessType == esSqlAccessType.DynamicSQL)
                return LoadByPrimaryKeyDynamic(idTabela);
            else
                return LoadByPrimaryKeyStoredProcedure(idTabela);
        }

        private bool LoadByPrimaryKeyDynamic(System.Int32 idTabela)
        {
            esTabelaCustosRendaFixaQuery query = this.GetDynamicQuery();
            query.Where(query.IdTabela == idTabela);
            return query.Load();
        }

        private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idTabela)
        {
            esParameters parms = new esParameters();
            parms.Add("IdTabela", idTabela);
            return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
        }
        #endregion



        #region Properties


        public override void SetProperties(IDictionary values)
        {
            foreach (string propertyName in values.Keys)
            {
                this.SetProperty(propertyName, values[propertyName]);
            }
        }

        public override void SetProperty(string name, object value)
        {
            if (this.Row == null) this.AddNew();

            esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
            if (col != null)
            {
                if (value == null || value.GetType().ToString() == "System.String")
                {
                    // Use the strongly typed property
                    switch (name)
                    {
                        case "IdTabela": this.str.IdTabela = (string)value; break;
                        case "DataReferencia": this.str.DataReferencia = (string)value; break;
                        case "IdAgente": this.str.IdAgente = (string)value; break;
                        case "IdCliente": this.str.IdCliente = (string)value; break;
                        case "ValorMensal": this.str.ValorMensal = (string)value; break;
                        case "TaxaAno": this.str.TaxaAno = (string)value; break;
                        case "Classe": this.str.Classe = (string)value; break;
                        case "Canal": this.str.Canal = (string)value; break;
                        case "IdTitulo": this.str.IdTitulo = (string)value; break;
                    }
                }
                else
                {
                    switch (name)
                    {
                        case "IdTabela":

                            if (value == null || value.GetType().ToString() == "System.Int32")
                                this.IdTabela = (System.Int32?)value;
                            break;

                        case "DataReferencia":

                            if (value == null || value.GetType().ToString() == "System.DateTime")
                                this.DataReferencia = (System.DateTime?)value;
                            break;

                        case "IdAgente":

                            if (value == null || value.GetType().ToString() == "System.Int32")
                                this.IdAgente = (System.Int32?)value;
                            break;

                        case "IdCliente":

                            if (value == null || value.GetType().ToString() == "System.Int32")
                                this.IdCliente = (System.Int32?)value;
                            break;

                        case "ValorMensal":

                            if (value == null || value.GetType().ToString() == "System.Decimal")
                                this.ValorMensal = (System.Decimal?)value;
                            break;

                        case "TaxaAno":

                            if (value == null || value.GetType().ToString() == "System.Decimal")
                                this.TaxaAno = (System.Decimal?)value;
                            break;

                        case "Classe":

                            if (value == null || value.GetType().ToString() == "System.Int32")
                                this.Classe = (System.Int32?)value;
                            break;

                        case "Canal":

                            if (value == null || value.GetType().ToString() == "System.Byte")
                                this.Canal = (System.Byte?)value;
                            break;

                        case "IdTitulo":

                            if (value == null || value.GetType().ToString() == "System.Int32")
                                this.IdTitulo = (System.Int32?)value;
                            break;


                        default:
                            break;
                    }
                }
            }
            else if (this.Row.Table.Columns.Contains(name))
            {
                this.Row[name] = value;
            }
            else
            {
                throw new Exception("SetProperty Error: '" + name + "' not found");
            }
        }


        /// <summary>
        /// Maps to TabelaCustosRendaFixa.IdTabela
        /// </summary>
        virtual public System.Int32? IdTabela
        {
            get
            {
                return base.GetSystemInt32(TabelaCustosRendaFixaMetadata.ColumnNames.IdTabela);
            }

            set
            {
                base.SetSystemInt32(TabelaCustosRendaFixaMetadata.ColumnNames.IdTabela, value);
            }
        }

        /// <summary>
        /// Maps to TabelaCustosRendaFixa.DataReferencia
        /// </summary>
        virtual public System.DateTime? DataReferencia
        {
            get
            {
                return base.GetSystemDateTime(TabelaCustosRendaFixaMetadata.ColumnNames.DataReferencia);
            }

            set
            {
                base.SetSystemDateTime(TabelaCustosRendaFixaMetadata.ColumnNames.DataReferencia, value);
            }
        }

        /// <summary>
        /// Maps to TabelaCustosRendaFixa.IdAgente
        /// </summary>
        virtual public System.Int32? IdAgente
        {
            get
            {
                return base.GetSystemInt32(TabelaCustosRendaFixaMetadata.ColumnNames.IdAgente);
            }

            set
            {
                base.SetSystemInt32(TabelaCustosRendaFixaMetadata.ColumnNames.IdAgente, value);
            }
        }

        /// <summary>
        /// Maps to TabelaCustosRendaFixa.IdCliente
        /// </summary>
        virtual public System.Int32? IdCliente
        {
            get
            {
                return base.GetSystemInt32(TabelaCustosRendaFixaMetadata.ColumnNames.IdCliente);
            }

            set
            {
                if (base.SetSystemInt32(TabelaCustosRendaFixaMetadata.ColumnNames.IdCliente, value))
                {
                    this._UpToClienteByIdCliente = null;
                }
            }
        }

        /// <summary>
        /// Maps to TabelaCustosRendaFixa.ValorMensal
        /// </summary>
        virtual public System.Decimal? ValorMensal
        {
            get
            {
                return base.GetSystemDecimal(TabelaCustosRendaFixaMetadata.ColumnNames.ValorMensal);
            }

            set
            {
                base.SetSystemDecimal(TabelaCustosRendaFixaMetadata.ColumnNames.ValorMensal, value);
            }
        }

        /// <summary>
        /// Maps to TabelaCustosRendaFixa.TaxaAno
        /// </summary>
        virtual public System.Decimal? TaxaAno
        {
            get
            {
                return base.GetSystemDecimal(TabelaCustosRendaFixaMetadata.ColumnNames.TaxaAno);
            }

            set
            {
                base.SetSystemDecimal(TabelaCustosRendaFixaMetadata.ColumnNames.TaxaAno, value);
            }
        }

        /// <summary>
        /// Maps to TabelaCustosRendaFixa.Classe
        /// </summary>
        virtual public System.Int32? Classe
        {
            get
            {
                return base.GetSystemInt32(TabelaCustosRendaFixaMetadata.ColumnNames.Classe);
            }

            set
            {
                base.SetSystemInt32(TabelaCustosRendaFixaMetadata.ColumnNames.Classe, value);
            }
        }

        /// <summary>
        /// Maps to TabelaCustosRendaFixa.Canal
        /// </summary>
        virtual public System.Byte? Canal
        {
            get
            {
                return base.GetSystemByte(TabelaCustosRendaFixaMetadata.ColumnNames.Canal);
            }

            set
            {
                base.SetSystemByte(TabelaCustosRendaFixaMetadata.ColumnNames.Canal, value);
            }
        }

        /// <summary>
        /// Maps to TabelaCustosRendaFixa.IdTitulo
        /// </summary>
        virtual public System.Int32? IdTitulo
        {
            get
            {
                return base.GetSystemInt32(TabelaCustosRendaFixaMetadata.ColumnNames.IdTitulo);
            }

            set
            {
                if (base.SetSystemInt32(TabelaCustosRendaFixaMetadata.ColumnNames.IdTitulo, value))
                {
                    this._UpToTituloRendaFixaByIdTitulo = null;
                }
            }
        }

        [CLSCompliant(false)]
        internal protected Cliente _UpToClienteByIdCliente;
        [CLSCompliant(false)]
        internal protected TituloRendaFixa _UpToTituloRendaFixaByIdTitulo;
        #endregion

        #region String Properties


        [BrowsableAttribute(false)]
        public esStrings str
        {
            get
            {
                if (esstrings == null)
                {
                    esstrings = new esStrings(this);
                }
                return esstrings;
            }
        }


        [Serializable]
        sealed public class esStrings
        {
            public esStrings(esTabelaCustosRendaFixa entity)
            {
                this.entity = entity;
            }


            public System.String IdTabela
            {
                get
                {
                    System.Int32? data = entity.IdTabela;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.IdTabela = null;
                    else entity.IdTabela = Convert.ToInt32(value);
                }
            }

            public System.String DataReferencia
            {
                get
                {
                    System.DateTime? data = entity.DataReferencia;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.DataReferencia = null;
                    else entity.DataReferencia = Convert.ToDateTime(value);
                }
            }

            public System.String IdAgente
            {
                get
                {
                    System.Int32? data = entity.IdAgente;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.IdAgente = null;
                    else entity.IdAgente = Convert.ToInt32(value);
                }
            }

            public System.String IdCliente
            {
                get
                {
                    System.Int32? data = entity.IdCliente;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.IdCliente = null;
                    else entity.IdCliente = Convert.ToInt32(value);
                }
            }

            public System.String ValorMensal
            {
                get
                {
                    System.Decimal? data = entity.ValorMensal;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.ValorMensal = null;
                    else entity.ValorMensal = Convert.ToDecimal(value);
                }
            }

            public System.String TaxaAno
            {
                get
                {
                    System.Decimal? data = entity.TaxaAno;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.TaxaAno = null;
                    else entity.TaxaAno = Convert.ToDecimal(value);
                }
            }

            public System.String Classe
            {
                get
                {
                    System.Int32? data = entity.Classe;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.Classe = null;
                    else entity.Classe = Convert.ToInt32(value);
                }
            }

            public System.String Canal
            {
                get
                {
                    System.Byte? data = entity.Canal;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.Canal = null;
                    else entity.Canal = Convert.ToByte(value);
                }
            }

            public System.String IdTitulo
            {
                get
                {
                    System.Int32? data = entity.IdTitulo;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.IdTitulo = null;
                    else entity.IdTitulo = Convert.ToInt32(value);
                }
            }


            private esTabelaCustosRendaFixa entity;
        }
        #endregion

        #region Query Logic
        protected void InitQuery(esTabelaCustosRendaFixaQuery query)
        {
            query.OnLoadDelegate = this.OnQueryLoaded;
            query.es.Connection = ((IEntity)this).Connection;
        }

        [System.Diagnostics.DebuggerNonUserCode]
        protected bool OnQueryLoaded(DataTable table)
        {
            bool dataFound = this.PopulateEntity(table);

            if (this.RowCount > 1)
            {
                throw new Exception("esTabelaCustosRendaFixa can only hold one record of data");
            }

            return dataFound;
        }
        #endregion

        [NonSerialized]
        private esStrings esstrings;
    }



    public partial class TabelaCustosRendaFixa : esTabelaCustosRendaFixa
    {


        #region UpToClienteByIdCliente - Many To One
        /// <summary>
        /// Many to One
        /// Foreign Key Name - FK_TabelaCustosRendaFixa_Cliente
        /// </summary>

        [XmlIgnore]
        public Cliente UpToClienteByIdCliente
        {
            get
            {
                if (this._UpToClienteByIdCliente == null
                    && IdCliente != null)
                {
                    this._UpToClienteByIdCliente = new Cliente();
                    this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
                    this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
                    this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
                    this._UpToClienteByIdCliente.Query.Load();
                }

                return this._UpToClienteByIdCliente;
            }

            set
            {
                this.RemovePreSave("UpToClienteByIdCliente");


                if (value == null)
                {
                    this.IdCliente = null;
                    this._UpToClienteByIdCliente = null;
                }
                else
                {
                    this.IdCliente = value.IdCliente;
                    this._UpToClienteByIdCliente = value;
                    this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
                }

            }
        }
        #endregion



        #region UpToTituloRendaFixaByIdTitulo - Many To One
        /// <summary>
        /// Many to One
        /// Foreign Key Name - FK_TabelaCustosRendaFixa_TituloRendaFixa
        /// </summary>

        [XmlIgnore]
        public TituloRendaFixa UpToTituloRendaFixaByIdTitulo
        {
            get
            {
                if (this._UpToTituloRendaFixaByIdTitulo == null
                    && IdTitulo != null)
                {
                    this._UpToTituloRendaFixaByIdTitulo = new TituloRendaFixa();
                    this._UpToTituloRendaFixaByIdTitulo.es.Connection.Name = this.es.Connection.Name;
                    this.SetPreSave("UpToTituloRendaFixaByIdTitulo", this._UpToTituloRendaFixaByIdTitulo);
                    this._UpToTituloRendaFixaByIdTitulo.Query.Where(this._UpToTituloRendaFixaByIdTitulo.Query.IdTitulo == this.IdTitulo);
                    this._UpToTituloRendaFixaByIdTitulo.Query.Load();
                }

                return this._UpToTituloRendaFixaByIdTitulo;
            }

            set
            {
                this.RemovePreSave("UpToTituloRendaFixaByIdTitulo");


                if (value == null)
                {
                    this.IdTitulo = null;
                    this._UpToTituloRendaFixaByIdTitulo = null;
                }
                else
                {
                    this.IdTitulo = value.IdTitulo;
                    this._UpToTituloRendaFixaByIdTitulo = value;
                    this.SetPreSave("UpToTituloRendaFixaByIdTitulo", this._UpToTituloRendaFixaByIdTitulo);
                }

            }
        }
        #endregion



        /// <summary>
        /// Used internally by the entity's hierarchical properties.
        /// </summary>
        protected override List<esPropertyDescriptor> GetHierarchicalProperties()
        {
            List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();


            return props;
        }

        /// <summary>
        /// Used internally for retrieving AutoIncrementing keys
        /// during hierarchical PreSave.
        /// </summary>
        protected override void ApplyPreSaveKeys()
        {
            if (!this.es.IsDeleted && this._UpToTituloRendaFixaByIdTitulo != null)
            {
                this.IdTitulo = this._UpToTituloRendaFixaByIdTitulo.IdTitulo;
            }
        }

        /// <summary>
        /// Used internally for retrieving AutoIncrementing keys
        /// during hierarchical PostSave.
        /// </summary>
        protected override void ApplyPostSaveKeys()
        {
        }

        /// <summary>
        /// Used internally for retrieving AutoIncrementing keys
        /// during hierarchical PostOneToOneSave.
        /// </summary>
        protected override void ApplyPostOneSaveKeys()
        {
        }

    }



    [Serializable]
    abstract public class esTabelaCustosRendaFixaQuery : esDynamicQuery
    {
        override protected IMetadata Meta
        {
            get
            {
                return TabelaCustosRendaFixaMetadata.Meta();
            }
        }


        public esQueryItem IdTabela
        {
            get
            {
                return new esQueryItem(this, TabelaCustosRendaFixaMetadata.ColumnNames.IdTabela, esSystemType.Int32);
            }
        }

        public esQueryItem DataReferencia
        {
            get
            {
                return new esQueryItem(this, TabelaCustosRendaFixaMetadata.ColumnNames.DataReferencia, esSystemType.DateTime);
            }
        }

        public esQueryItem IdAgente
        {
            get
            {
                return new esQueryItem(this, TabelaCustosRendaFixaMetadata.ColumnNames.IdAgente, esSystemType.Int32);
            }
        }

        public esQueryItem IdCliente
        {
            get
            {
                return new esQueryItem(this, TabelaCustosRendaFixaMetadata.ColumnNames.IdCliente, esSystemType.Int32);
            }
        }

        public esQueryItem ValorMensal
        {
            get
            {
                return new esQueryItem(this, TabelaCustosRendaFixaMetadata.ColumnNames.ValorMensal, esSystemType.Decimal);
            }
        }

        public esQueryItem TaxaAno
        {
            get
            {
                return new esQueryItem(this, TabelaCustosRendaFixaMetadata.ColumnNames.TaxaAno, esSystemType.Decimal);
            }
        }

        public esQueryItem Classe
        {
            get
            {
                return new esQueryItem(this, TabelaCustosRendaFixaMetadata.ColumnNames.Classe, esSystemType.Int32);
            }
        }

        public esQueryItem Canal
        {
            get
            {
                return new esQueryItem(this, TabelaCustosRendaFixaMetadata.ColumnNames.Canal, esSystemType.Byte);
            }
        }

        public esQueryItem IdTitulo
        {
            get
            {
                return new esQueryItem(this, TabelaCustosRendaFixaMetadata.ColumnNames.IdTitulo, esSystemType.Int32);
            }
        }

    }



    [Serializable]
    [XmlType("TabelaCustosRendaFixaCollection")]
    public partial class TabelaCustosRendaFixaCollection : esTabelaCustosRendaFixaCollection, IEnumerable<TabelaCustosRendaFixa>
    {
        public TabelaCustosRendaFixaCollection()
        {

        }

        public static implicit operator List<TabelaCustosRendaFixa>(TabelaCustosRendaFixaCollection coll)
        {
            List<TabelaCustosRendaFixa> list = new List<TabelaCustosRendaFixa>();

            foreach (TabelaCustosRendaFixa emp in coll)
            {
                list.Add(emp);
            }

            return list;
        }

        #region Housekeeping methods
        override protected IMetadata Meta
        {
            get
            {
                return TabelaCustosRendaFixaMetadata.Meta();
            }
        }



        override protected esDynamicQuery GetDynamicQuery()
        {
            if (this.query == null)
            {
                this.query = new TabelaCustosRendaFixaQuery();
                this.InitQuery(query);
            }
            return this.query;
        }

        override protected esEntity CreateEntityForCollection(DataRow row)
        {
            return new TabelaCustosRendaFixa(row);
        }

        override protected esEntity CreateEntity()
        {
            return new TabelaCustosRendaFixa();
        }


        #endregion


        [BrowsableAttribute(false)]
        public TabelaCustosRendaFixaQuery Query
        {
            get
            {
                if (this.query == null)
                {
                    this.query = new TabelaCustosRendaFixaQuery();
                    base.InitQuery(this.query);
                }

                return this.query;
            }
        }

        public void QueryReset()
        {
            this.query = null;
        }

        public bool Load(TabelaCustosRendaFixaQuery query)
        {
            this.query = query;
            base.InitQuery(this.query);
            return this.Query.Load();
        }

        public TabelaCustosRendaFixa AddNew()
        {
            TabelaCustosRendaFixa entity = base.AddNewEntity() as TabelaCustosRendaFixa;

            return entity;
        }

        public TabelaCustosRendaFixa FindByPrimaryKey(System.Int32 idTabela)
        {
            return base.FindByPrimaryKey(idTabela) as TabelaCustosRendaFixa;
        }


        #region IEnumerable<TabelaCustosRendaFixa> Members

        IEnumerator<TabelaCustosRendaFixa> IEnumerable<TabelaCustosRendaFixa>.GetEnumerator()
        {
            System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
            System.Collections.IEnumerator iterator = enumer.GetEnumerator();

            while (iterator.MoveNext())
            {
                yield return iterator.Current as TabelaCustosRendaFixa;
            }
        }

        #endregion

        private TabelaCustosRendaFixaQuery query;
    }


    /// <summary>
    /// Encapsulates the 'TabelaCustosRendaFixa' table
    /// </summary>

    [Serializable]
    public partial class TabelaCustosRendaFixa : esTabelaCustosRendaFixa
    {
        public TabelaCustosRendaFixa()
        {

        }

        public TabelaCustosRendaFixa(DataRow row)
            : base(row)
        {

        }

        #region Housekeeping methods
        override protected IMetadata Meta
        {
            get
            {
                return TabelaCustosRendaFixaMetadata.Meta();
            }
        }



        override protected esTabelaCustosRendaFixaQuery GetDynamicQuery()
        {
            if (this.query == null)
            {
                this.query = new TabelaCustosRendaFixaQuery();
                this.InitQuery(query);
            }
            return this.query;
        }
        #endregion




        [BrowsableAttribute(false)]
        public TabelaCustosRendaFixaQuery Query
        {
            get
            {
                if (this.query == null)
                {
                    this.query = new TabelaCustosRendaFixaQuery();
                    base.InitQuery(this.query);
                }

                return this.query;
            }
        }

        public void QueryReset()
        {
            this.query = null;
        }


        public bool Load(TabelaCustosRendaFixaQuery query)
        {
            this.query = query;
            base.InitQuery(this.query);
            return this.Query.Load();
        }

        private TabelaCustosRendaFixaQuery query;
    }



    [Serializable]
    public partial class TabelaCustosRendaFixaQuery : esTabelaCustosRendaFixaQuery
    {
        public TabelaCustosRendaFixaQuery()
        {

        }

        public TabelaCustosRendaFixaQuery(string joinAlias)
        {
            this.es.JoinAlias = joinAlias;
        }


    }



    [Serializable]
    public partial class TabelaCustosRendaFixaMetadata : esMetadata, IMetadata
    {
        #region Protected Constructor
        protected TabelaCustosRendaFixaMetadata()
        {
            _columns = new esColumnMetadataCollection();
            esColumnMetadata c;

            c = new esColumnMetadata(TabelaCustosRendaFixaMetadata.ColumnNames.IdTabela, 0, typeof(System.Int32), esSystemType.Int32);
            c.PropertyName = TabelaCustosRendaFixaMetadata.PropertyNames.IdTabela;
            c.IsInPrimaryKey = true;
            c.IsAutoIncrement = true;
            c.NumericPrecision = 10;
            _columns.Add(c);


            c = new esColumnMetadata(TabelaCustosRendaFixaMetadata.ColumnNames.DataReferencia, 1, typeof(System.DateTime), esSystemType.DateTime);
            c.PropertyName = TabelaCustosRendaFixaMetadata.PropertyNames.DataReferencia;
            c.NumericPrecision = 0;
            _columns.Add(c);


            c = new esColumnMetadata(TabelaCustosRendaFixaMetadata.ColumnNames.IdAgente, 2, typeof(System.Int32), esSystemType.Int32);
            c.PropertyName = TabelaCustosRendaFixaMetadata.PropertyNames.IdAgente;
            c.NumericPrecision = 10;
            _columns.Add(c);


            c = new esColumnMetadata(TabelaCustosRendaFixaMetadata.ColumnNames.IdCliente, 3, typeof(System.Int32), esSystemType.Int32);
            c.PropertyName = TabelaCustosRendaFixaMetadata.PropertyNames.IdCliente;
            c.NumericPrecision = 10;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(TabelaCustosRendaFixaMetadata.ColumnNames.ValorMensal, 4, typeof(System.Decimal), esSystemType.Decimal);
            c.PropertyName = TabelaCustosRendaFixaMetadata.PropertyNames.ValorMensal;
            c.NumericPrecision = 6;
            c.NumericScale = 2;
            _columns.Add(c);


            c = new esColumnMetadata(TabelaCustosRendaFixaMetadata.ColumnNames.TaxaAno, 5, typeof(System.Decimal), esSystemType.Decimal);
            c.PropertyName = TabelaCustosRendaFixaMetadata.PropertyNames.TaxaAno;
            c.NumericPrecision = 10;
            c.NumericScale = 4;
            _columns.Add(c);


            c = new esColumnMetadata(TabelaCustosRendaFixaMetadata.ColumnNames.Classe, 6, typeof(System.Int32), esSystemType.Int32);
            c.PropertyName = TabelaCustosRendaFixaMetadata.PropertyNames.Classe;
            c.NumericPrecision = 10;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(TabelaCustosRendaFixaMetadata.ColumnNames.Canal, 7, typeof(System.Byte), esSystemType.Byte);
            c.PropertyName = TabelaCustosRendaFixaMetadata.PropertyNames.Canal;
            c.NumericPrecision = 3;
            c.IsNullable = true;
            _columns.Add(c);


            c = new esColumnMetadata(TabelaCustosRendaFixaMetadata.ColumnNames.IdTitulo, 8, typeof(System.Int32), esSystemType.Int32);
            c.PropertyName = TabelaCustosRendaFixaMetadata.PropertyNames.IdTitulo;
            c.NumericPrecision = 10;
            c.IsNullable = true;
            _columns.Add(c);


        }
        #endregion

        static public TabelaCustosRendaFixaMetadata Meta()
        {
            return meta;
        }

        public Guid DataID
        {
            get { return base._dataID; }
        }

        public bool MultiProviderMode
        {
            get { return false; }
        }

        public esColumnMetadataCollection Columns
        {
            get { return base._columns; }
        }

        #region ColumnNames
        public class ColumnNames
        {
            public const string IdTabela = "IdTabela";
            public const string DataReferencia = "DataReferencia";
            public const string IdAgente = "IdAgente";
            public const string IdCliente = "IdCliente";
            public const string ValorMensal = "ValorMensal";
            public const string TaxaAno = "TaxaAno";
            public const string Classe = "Classe";
            public const string Canal = "Canal";
            public const string IdTitulo = "IdTitulo";
        }
        #endregion

        #region PropertyNames
        public class PropertyNames
        {
            public const string IdTabela = "IdTabela";
            public const string DataReferencia = "DataReferencia";
            public const string IdAgente = "IdAgente";
            public const string IdCliente = "IdCliente";
            public const string ValorMensal = "ValorMensal";
            public const string TaxaAno = "TaxaAno";
            public const string Classe = "Classe";
            public const string Canal = "Canal";
            public const string IdTitulo = "IdTitulo";
        }
        #endregion

        public esProviderSpecificMetadata GetProviderMetadata(string mapName)
        {
            MapToMeta mapMethod = mapDelegates[mapName];

            if (mapMethod != null)
                return mapMethod(mapName);
            else
                return null;
        }

        #region MAP esDefault

        static private int RegisterDelegateesDefault()
        {
            // This is only executed once per the life of the application
            lock (typeof(TabelaCustosRendaFixaMetadata))
            {
                if (TabelaCustosRendaFixaMetadata.mapDelegates == null)
                {
                    TabelaCustosRendaFixaMetadata.mapDelegates = new Dictionary<string, MapToMeta>();
                }

                if (TabelaCustosRendaFixaMetadata.meta == null)
                {
                    TabelaCustosRendaFixaMetadata.meta = new TabelaCustosRendaFixaMetadata();
                }

                MapToMeta mapMethod = new MapToMeta(meta.esDefault);
                mapDelegates.Add("esDefault", mapMethod);
                mapMethod("esDefault");
            }
            return 0;
        }

        private esProviderSpecificMetadata esDefault(string mapName)
        {
            if (!_providerMetadataMaps.ContainsKey(mapName))
            {
                esProviderSpecificMetadata meta = new esProviderSpecificMetadata();


                meta.AddTypeMap("IdTabela", new esTypeMap("int", "System.Int32"));
                meta.AddTypeMap("DataReferencia", new esTypeMap("datetime", "System.DateTime"));
                meta.AddTypeMap("IdAgente", new esTypeMap("int", "System.Int32"));
                meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
                meta.AddTypeMap("ValorMensal", new esTypeMap("decimal", "System.Decimal"));
                meta.AddTypeMap("TaxaAno", new esTypeMap("decimal", "System.Decimal"));
                meta.AddTypeMap("Classe", new esTypeMap("int", "System.Int32"));
                meta.AddTypeMap("Canal", new esTypeMap("tinyint", "System.Byte"));
                meta.AddTypeMap("IdTitulo", new esTypeMap("int", "System.Int32"));



                meta.Source = "TabelaCustosRendaFixa";
                meta.Destination = "TabelaCustosRendaFixa";

                meta.spInsert = "proc_TabelaCustosRendaFixaInsert";
                meta.spUpdate = "proc_TabelaCustosRendaFixaUpdate";
                meta.spDelete = "proc_TabelaCustosRendaFixaDelete";
                meta.spLoadAll = "proc_TabelaCustosRendaFixaLoadAll";
                meta.spLoadByPrimaryKey = "proc_TabelaCustosRendaFixaLoadByPrimaryKey";

                this._providerMetadataMaps["esDefault"] = meta;
            }

            return this._providerMetadataMaps["esDefault"];
        }

        #endregion

        static private TabelaCustosRendaFixaMetadata meta;
        static protected Dictionary<string, MapToMeta> mapDelegates;
        static private int _esDefault = RegisterDelegateesDefault();
    }
}
