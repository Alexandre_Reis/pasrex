/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 01/10/2014 15:55:23
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.RendaFixa
{

	[Serializable]
	abstract public class esTaxaSerieCollection : esEntityCollection
	{
		public esTaxaSerieCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TaxaSerieCollection";
		}

		#region Query Logic
		protected void InitQuery(esTaxaSerieQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTaxaSerieQuery);
		}
		#endregion
		
		virtual public TaxaSerie DetachEntity(TaxaSerie entity)
		{
			return base.DetachEntity(entity) as TaxaSerie;
		}
		
		virtual public TaxaSerie AttachEntity(TaxaSerie entity)
		{
			return base.AttachEntity(entity) as TaxaSerie;
		}
		
		virtual public void Combine(TaxaSerieCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TaxaSerie this[int index]
		{
			get
			{
				return base[index] as TaxaSerie;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TaxaSerie);
		}
	}



	[Serializable]
	abstract public class esTaxaSerie : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTaxaSerieQuery GetDynamicQuery()
		{
			return null;
		}

		public esTaxaSerie()
		{

		}

		public esTaxaSerie(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime data, System.Int32 idSerie)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(data, idSerie);
			else
				return LoadByPrimaryKeyStoredProcedure(data, idSerie);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime data, System.Int32 idSerie)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(data, idSerie);
			else
				return LoadByPrimaryKeyStoredProcedure(data, idSerie);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime data, System.Int32 idSerie)
		{
			esTaxaSerieQuery query = this.GetDynamicQuery();
			query.Where(query.Data == data, query.IdSerie == idSerie);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime data, System.Int32 idSerie)
		{
			esParameters parms = new esParameters();
			parms.Add("Data",data);			parms.Add("IdSerie",idSerie);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdSerie": this.str.IdSerie = (string)value; break;							
						case "Data": this.str.Data = (string)value; break;							
						case "Taxa": this.str.Taxa = (string)value; break;							
						case "DigitadoImportado": this.str.DigitadoImportado = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdSerie":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdSerie = (System.Int32?)value;
							break;
						
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "Taxa":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Taxa = (System.Decimal?)value;
							break;
						
						case "DigitadoImportado":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.DigitadoImportado = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TaxaSerie.IdSerie
		/// </summary>
		virtual public System.Int32? IdSerie
		{
			get
			{
				return base.GetSystemInt32(TaxaSerieMetadata.ColumnNames.IdSerie);
			}
			
			set
			{
				base.SetSystemInt32(TaxaSerieMetadata.ColumnNames.IdSerie, value);
			}
		}
		
		/// <summary>
		/// Maps to TaxaSerie.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(TaxaSerieMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(TaxaSerieMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to TaxaSerie.Taxa
		/// </summary>
		virtual public System.Decimal? Taxa
		{
			get
			{
				return base.GetSystemDecimal(TaxaSerieMetadata.ColumnNames.Taxa);
			}
			
			set
			{
				base.SetSystemDecimal(TaxaSerieMetadata.ColumnNames.Taxa, value);
			}
		}
		
		/// <summary>
		/// Maps to TaxaSerie.DigitadoImportado
		/// </summary>
		virtual public System.Int32? DigitadoImportado
		{
			get
			{
				return base.GetSystemInt32(TaxaSerieMetadata.ColumnNames.DigitadoImportado);
			}
			
			set
			{
				base.SetSystemInt32(TaxaSerieMetadata.ColumnNames.DigitadoImportado, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTaxaSerie entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdSerie
			{
				get
				{
					System.Int32? data = entity.IdSerie;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdSerie = null;
					else entity.IdSerie = Convert.ToInt32(value);
				}
			}
				
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String Taxa
			{
				get
				{
					System.Decimal? data = entity.Taxa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Taxa = null;
					else entity.Taxa = Convert.ToDecimal(value);
				}
			}
				
			public System.String DigitadoImportado
			{
				get
				{
					System.Int32? data = entity.DigitadoImportado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DigitadoImportado = null;
					else entity.DigitadoImportado = Convert.ToInt32(value);
				}
			}
			

			private esTaxaSerie entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTaxaSerieQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTaxaSerie can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TaxaSerie : esTaxaSerie
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTaxaSerieQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TaxaSerieMetadata.Meta();
			}
		}	
		

		public esQueryItem IdSerie
		{
			get
			{
				return new esQueryItem(this, TaxaSerieMetadata.ColumnNames.IdSerie, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, TaxaSerieMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Taxa
		{
			get
			{
				return new esQueryItem(this, TaxaSerieMetadata.ColumnNames.Taxa, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DigitadoImportado
		{
			get
			{
				return new esQueryItem(this, TaxaSerieMetadata.ColumnNames.DigitadoImportado, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TaxaSerieCollection")]
	public partial class TaxaSerieCollection : esTaxaSerieCollection, IEnumerable<TaxaSerie>
	{
		public TaxaSerieCollection()
		{

		}
		
		public static implicit operator List<TaxaSerie>(TaxaSerieCollection coll)
		{
			List<TaxaSerie> list = new List<TaxaSerie>();
			
			foreach (TaxaSerie emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TaxaSerieMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TaxaSerieQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TaxaSerie(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TaxaSerie();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TaxaSerieQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TaxaSerieQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TaxaSerieQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TaxaSerie AddNew()
		{
			TaxaSerie entity = base.AddNewEntity() as TaxaSerie;
			
			return entity;
		}

		public TaxaSerie FindByPrimaryKey(System.DateTime data, System.Int32 idSerie)
		{
			return base.FindByPrimaryKey(data, idSerie) as TaxaSerie;
		}


		#region IEnumerable<TaxaSerie> Members

		IEnumerator<TaxaSerie> IEnumerable<TaxaSerie>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TaxaSerie;
			}
		}

		#endregion
		
		private TaxaSerieQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TaxaSerie' table
	/// </summary>

	[Serializable]
	public partial class TaxaSerie : esTaxaSerie
	{
		public TaxaSerie()
		{

		}
	
		public TaxaSerie(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TaxaSerieMetadata.Meta();
			}
		}
		
		
		
		override protected esTaxaSerieQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TaxaSerieQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TaxaSerieQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TaxaSerieQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TaxaSerieQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TaxaSerieQuery query;
	}



	[Serializable]
	public partial class TaxaSerieQuery : esTaxaSerieQuery
	{
		public TaxaSerieQuery()
		{

		}		
		
		public TaxaSerieQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TaxaSerieMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TaxaSerieMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TaxaSerieMetadata.ColumnNames.IdSerie, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TaxaSerieMetadata.PropertyNames.IdSerie;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TaxaSerieMetadata.ColumnNames.Data, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TaxaSerieMetadata.PropertyNames.Data;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TaxaSerieMetadata.ColumnNames.Taxa, 2, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TaxaSerieMetadata.PropertyNames.Taxa;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TaxaSerieMetadata.ColumnNames.DigitadoImportado, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TaxaSerieMetadata.PropertyNames.DigitadoImportado;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TaxaSerieMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdSerie = "IdSerie";
			 public const string Data = "Data";
			 public const string Taxa = "Taxa";
			 public const string DigitadoImportado = "DigitadoImportado";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdSerie = "IdSerie";
			 public const string Data = "Data";
			 public const string Taxa = "Taxa";
			 public const string DigitadoImportado = "DigitadoImportado";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TaxaSerieMetadata))
			{
				if(TaxaSerieMetadata.mapDelegates == null)
				{
					TaxaSerieMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TaxaSerieMetadata.meta == null)
				{
					TaxaSerieMetadata.meta = new TaxaSerieMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdSerie", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Taxa", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("DigitadoImportado", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "TaxaSerie";
				meta.Destination = "TaxaSerie";
				
				meta.spInsert = "proc_TaxaSerieInsert";				
				meta.spUpdate = "proc_TaxaSerieUpdate";		
				meta.spDelete = "proc_TaxaSerieDelete";
				meta.spLoadAll = "proc_TaxaSerieLoadAll";
				meta.spLoadByPrimaryKey = "proc_TaxaSerieLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TaxaSerieMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
