/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 04/03/2016 18:55:47
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Investidor;
using Financial.Common;

namespace Financial.RendaFixa
{

	[Serializable]
	abstract public class esPosicaoRendaFixaCollection : esEntityCollection
	{
		public esPosicaoRendaFixaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "PosicaoRendaFixaCollection";
		}

		#region Query Logic
		protected void InitQuery(esPosicaoRendaFixaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esPosicaoRendaFixaQuery);
		}
		#endregion
		
		virtual public PosicaoRendaFixa DetachEntity(PosicaoRendaFixa entity)
		{
			return base.DetachEntity(entity) as PosicaoRendaFixa;
		}
		
		virtual public PosicaoRendaFixa AttachEntity(PosicaoRendaFixa entity)
		{
			return base.AttachEntity(entity) as PosicaoRendaFixa;
		}
		
		virtual public void Combine(PosicaoRendaFixaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public PosicaoRendaFixa this[int index]
		{
			get
			{
				return base[index] as PosicaoRendaFixa;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(PosicaoRendaFixa);
		}
	}



	[Serializable]
	abstract public class esPosicaoRendaFixa : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esPosicaoRendaFixaQuery GetDynamicQuery()
		{
			return null;
		}

		public esPosicaoRendaFixa()
		{

		}

		public esPosicaoRendaFixa(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idPosicao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPosicao);
			else
				return LoadByPrimaryKeyStoredProcedure(idPosicao);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idPosicao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPosicao);
			else
				return LoadByPrimaryKeyStoredProcedure(idPosicao);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idPosicao)
		{
			esPosicaoRendaFixaQuery query = this.GetDynamicQuery();
			query.Where(query.IdPosicao == idPosicao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idPosicao)
		{
			esParameters parms = new esParameters();
			parms.Add("IdPosicao",idPosicao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdPosicao": this.str.IdPosicao = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "IdTitulo": this.str.IdTitulo = (string)value; break;							
						case "TipoOperacao": this.str.TipoOperacao = (string)value; break;							
						case "DataVencimento": this.str.DataVencimento = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "QuantidadeBloqueada": this.str.QuantidadeBloqueada = (string)value; break;							
						case "DataOperacao": this.str.DataOperacao = (string)value; break;							
						case "DataLiquidacao": this.str.DataLiquidacao = (string)value; break;							
						case "PUOperacao": this.str.PUOperacao = (string)value; break;							
						case "PUCurva": this.str.PUCurva = (string)value; break;							
						case "ValorCurva": this.str.ValorCurva = (string)value; break;							
						case "PUMercado": this.str.PUMercado = (string)value; break;							
						case "ValorMercado": this.str.ValorMercado = (string)value; break;							
						case "PUJuros": this.str.PUJuros = (string)value; break;							
						case "ValorJuros": this.str.ValorJuros = (string)value; break;							
						case "DataVolta": this.str.DataVolta = (string)value; break;							
						case "TaxaVolta": this.str.TaxaVolta = (string)value; break;							
						case "PUVolta": this.str.PUVolta = (string)value; break;							
						case "ValorVolta": this.str.ValorVolta = (string)value; break;							
						case "QuantidadeInicial": this.str.QuantidadeInicial = (string)value; break;							
						case "ValorIR": this.str.ValorIR = (string)value; break;							
						case "ValorIOF": this.str.ValorIOF = (string)value; break;							
						case "TipoNegociacao": this.str.TipoNegociacao = (string)value; break;							
						case "PUCorrecao": this.str.PUCorrecao = (string)value; break;							
						case "ValorCorrecao": this.str.ValorCorrecao = (string)value; break;							
						case "TaxaOperacao": this.str.TaxaOperacao = (string)value; break;							
						case "IdAgente": this.str.IdAgente = (string)value; break;							
						case "IdCustodia": this.str.IdCustodia = (string)value; break;							
						case "CustoCustodia": this.str.CustoCustodia = (string)value; break;							
						case "IdIndiceVolta": this.str.IdIndiceVolta = (string)value; break;							
						case "IdOperacao": this.str.IdOperacao = (string)value; break;							
						case "OperacaoTermo": this.str.OperacaoTermo = (string)value; break;							
						case "ValorCurvaVencimento": this.str.ValorCurvaVencimento = (string)value; break;							
						case "PUCurvaVencimento": this.str.PUCurvaVencimento = (string)value; break;							
						case "AjusteMTM": this.str.AjusteMTM = (string)value; break;							
						case "AjusteVencimento": this.str.AjusteVencimento = (string)value; break;							
						case "TaxaMTM": this.str.TaxaMTM = (string)value; break;							
						case "IdCorretora": this.str.IdCorretora = (string)value; break;							
						case "ValorBrutoGrossUp": this.str.ValorBrutoGrossUp = (string)value; break;							
						case "AliquotaIR": this.str.AliquotaIR = (string)value; break;							
						case "AliquotaIOF": this.str.AliquotaIOF = (string)value; break;							
						case "PrazoDecorridoDC": this.str.PrazoDecorridoDC = (string)value; break;							
						case "PrazoDecorridoDU": this.str.PrazoDecorridoDU = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdPosicao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPosicao = (System.Int32?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "IdTitulo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTitulo = (System.Int32?)value;
							break;
						
						case "TipoOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoOperacao = (System.Byte?)value;
							break;
						
						case "DataVencimento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataVencimento = (System.DateTime?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Quantidade = (System.Decimal?)value;
							break;
						
						case "QuantidadeBloqueada":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QuantidadeBloqueada = (System.Decimal?)value;
							break;
						
						case "DataOperacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataOperacao = (System.DateTime?)value;
							break;
						
						case "DataLiquidacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataLiquidacao = (System.DateTime?)value;
							break;
						
						case "PUOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PUOperacao = (System.Decimal?)value;
							break;
						
						case "PUCurva":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PUCurva = (System.Decimal?)value;
							break;
						
						case "ValorCurva":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorCurva = (System.Decimal?)value;
							break;
						
						case "PUMercado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PUMercado = (System.Decimal?)value;
							break;
						
						case "ValorMercado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorMercado = (System.Decimal?)value;
							break;
						
						case "PUJuros":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PUJuros = (System.Decimal?)value;
							break;
						
						case "ValorJuros":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorJuros = (System.Decimal?)value;
							break;
						
						case "DataVolta":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataVolta = (System.DateTime?)value;
							break;
						
						case "TaxaVolta":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TaxaVolta = (System.Decimal?)value;
							break;
						
						case "PUVolta":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PUVolta = (System.Decimal?)value;
							break;
						
						case "ValorVolta":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorVolta = (System.Decimal?)value;
							break;
						
						case "QuantidadeInicial":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QuantidadeInicial = (System.Decimal?)value;
							break;
						
						case "ValorIR":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIR = (System.Decimal?)value;
							break;
						
						case "ValorIOF":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIOF = (System.Decimal?)value;
							break;
						
						case "TipoNegociacao":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoNegociacao = (System.Byte?)value;
							break;
						
						case "PUCorrecao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PUCorrecao = (System.Decimal?)value;
							break;
						
						case "ValorCorrecao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorCorrecao = (System.Decimal?)value;
							break;
						
						case "TaxaOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TaxaOperacao = (System.Decimal?)value;
							break;
						
						case "IdAgente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgente = (System.Int32?)value;
							break;
						
						case "IdCustodia":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.IdCustodia = (System.Byte?)value;
							break;
						
						case "CustoCustodia":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CustoCustodia = (System.Decimal?)value;
							break;
						
						case "IdIndiceVolta":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdIndiceVolta = (System.Int16?)value;
							break;
						
						case "IdOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacao = (System.Int32?)value;
							break;
						
						case "ValorCurvaVencimento":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorCurvaVencimento = (System.Decimal?)value;
							break;
						
						case "PUCurvaVencimento":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PUCurvaVencimento = (System.Decimal?)value;
							break;
						
						case "AjusteMTM":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.AjusteMTM = (System.Decimal?)value;
							break;
						
						case "AjusteVencimento":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.AjusteVencimento = (System.Decimal?)value;
							break;
						
						case "TaxaMTM":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TaxaMTM = (System.Decimal?)value;
							break;
						
						case "IdCorretora":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCorretora = (System.Int32?)value;
							break;
						
						case "ValorBrutoGrossUp":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorBrutoGrossUp = (System.Decimal?)value;
							break;
						
						case "AliquotaIR":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.AliquotaIR = (System.Decimal?)value;
							break;
						
						case "AliquotaIOF":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.AliquotaIOF = (System.Decimal?)value;
							break;
						
						case "PrazoDecorridoDC":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.PrazoDecorridoDC = (System.Int32?)value;
							break;
						
						case "PrazoDecorridoDU":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.PrazoDecorridoDU = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to PosicaoRendaFixa.IdPosicao
		/// </summary>
		virtual public System.Int32? IdPosicao
		{
			get
			{
				return base.GetSystemInt32(PosicaoRendaFixaMetadata.ColumnNames.IdPosicao);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoRendaFixaMetadata.ColumnNames.IdPosicao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixa.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(PosicaoRendaFixaMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(PosicaoRendaFixaMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixa.IdTitulo
		/// </summary>
		virtual public System.Int32? IdTitulo
		{
			get
			{
				return base.GetSystemInt32(PosicaoRendaFixaMetadata.ColumnNames.IdTitulo);
			}
			
			set
			{
				if(base.SetSystemInt32(PosicaoRendaFixaMetadata.ColumnNames.IdTitulo, value))
				{
					this._UpToTituloRendaFixaByIdTitulo = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixa.TipoOperacao
		/// </summary>
		virtual public System.Byte? TipoOperacao
		{
			get
			{
				return base.GetSystemByte(PosicaoRendaFixaMetadata.ColumnNames.TipoOperacao);
			}
			
			set
			{
				base.SetSystemByte(PosicaoRendaFixaMetadata.ColumnNames.TipoOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixa.DataVencimento
		/// </summary>
		virtual public System.DateTime? DataVencimento
		{
			get
			{
				return base.GetSystemDateTime(PosicaoRendaFixaMetadata.ColumnNames.DataVencimento);
			}
			
			set
			{
				base.SetSystemDateTime(PosicaoRendaFixaMetadata.ColumnNames.DataVencimento, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixa.Quantidade
		/// </summary>
		virtual public System.Decimal? Quantidade
		{
			get
			{
				return base.GetSystemDecimal(PosicaoRendaFixaMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoRendaFixaMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixa.QuantidadeBloqueada
		/// </summary>
		virtual public System.Decimal? QuantidadeBloqueada
		{
			get
			{
				return base.GetSystemDecimal(PosicaoRendaFixaMetadata.ColumnNames.QuantidadeBloqueada);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoRendaFixaMetadata.ColumnNames.QuantidadeBloqueada, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixa.DataOperacao
		/// </summary>
		virtual public System.DateTime? DataOperacao
		{
			get
			{
				return base.GetSystemDateTime(PosicaoRendaFixaMetadata.ColumnNames.DataOperacao);
			}
			
			set
			{
				base.SetSystemDateTime(PosicaoRendaFixaMetadata.ColumnNames.DataOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixa.DataLiquidacao
		/// </summary>
		virtual public System.DateTime? DataLiquidacao
		{
			get
			{
				return base.GetSystemDateTime(PosicaoRendaFixaMetadata.ColumnNames.DataLiquidacao);
			}
			
			set
			{
				base.SetSystemDateTime(PosicaoRendaFixaMetadata.ColumnNames.DataLiquidacao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixa.PUOperacao
		/// </summary>
		virtual public System.Decimal? PUOperacao
		{
			get
			{
				return base.GetSystemDecimal(PosicaoRendaFixaMetadata.ColumnNames.PUOperacao);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoRendaFixaMetadata.ColumnNames.PUOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixa.PUCurva
		/// </summary>
		virtual public System.Decimal? PUCurva
		{
			get
			{
				return base.GetSystemDecimal(PosicaoRendaFixaMetadata.ColumnNames.PUCurva);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoRendaFixaMetadata.ColumnNames.PUCurva, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixa.ValorCurva
		/// </summary>
		virtual public System.Decimal? ValorCurva
		{
			get
			{
				return base.GetSystemDecimal(PosicaoRendaFixaMetadata.ColumnNames.ValorCurva);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoRendaFixaMetadata.ColumnNames.ValorCurva, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixa.PUMercado
		/// </summary>
		virtual public System.Decimal? PUMercado
		{
			get
			{
				return base.GetSystemDecimal(PosicaoRendaFixaMetadata.ColumnNames.PUMercado);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoRendaFixaMetadata.ColumnNames.PUMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixa.ValorMercado
		/// </summary>
		virtual public System.Decimal? ValorMercado
		{
			get
			{
				return base.GetSystemDecimal(PosicaoRendaFixaMetadata.ColumnNames.ValorMercado);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoRendaFixaMetadata.ColumnNames.ValorMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixa.PUJuros
		/// </summary>
		virtual public System.Decimal? PUJuros
		{
			get
			{
				return base.GetSystemDecimal(PosicaoRendaFixaMetadata.ColumnNames.PUJuros);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoRendaFixaMetadata.ColumnNames.PUJuros, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixa.ValorJuros
		/// </summary>
		virtual public System.Decimal? ValorJuros
		{
			get
			{
				return base.GetSystemDecimal(PosicaoRendaFixaMetadata.ColumnNames.ValorJuros);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoRendaFixaMetadata.ColumnNames.ValorJuros, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixa.DataVolta
		/// </summary>
		virtual public System.DateTime? DataVolta
		{
			get
			{
				return base.GetSystemDateTime(PosicaoRendaFixaMetadata.ColumnNames.DataVolta);
			}
			
			set
			{
				base.SetSystemDateTime(PosicaoRendaFixaMetadata.ColumnNames.DataVolta, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixa.TaxaVolta
		/// </summary>
		virtual public System.Decimal? TaxaVolta
		{
			get
			{
				return base.GetSystemDecimal(PosicaoRendaFixaMetadata.ColumnNames.TaxaVolta);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoRendaFixaMetadata.ColumnNames.TaxaVolta, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixa.PUVolta
		/// </summary>
		virtual public System.Decimal? PUVolta
		{
			get
			{
				return base.GetSystemDecimal(PosicaoRendaFixaMetadata.ColumnNames.PUVolta);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoRendaFixaMetadata.ColumnNames.PUVolta, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixa.ValorVolta
		/// </summary>
		virtual public System.Decimal? ValorVolta
		{
			get
			{
				return base.GetSystemDecimal(PosicaoRendaFixaMetadata.ColumnNames.ValorVolta);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoRendaFixaMetadata.ColumnNames.ValorVolta, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixa.QuantidadeInicial
		/// </summary>
		virtual public System.Decimal? QuantidadeInicial
		{
			get
			{
				return base.GetSystemDecimal(PosicaoRendaFixaMetadata.ColumnNames.QuantidadeInicial);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoRendaFixaMetadata.ColumnNames.QuantidadeInicial, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixa.ValorIR
		/// </summary>
		virtual public System.Decimal? ValorIR
		{
			get
			{
				return base.GetSystemDecimal(PosicaoRendaFixaMetadata.ColumnNames.ValorIR);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoRendaFixaMetadata.ColumnNames.ValorIR, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixa.ValorIOF
		/// </summary>
		virtual public System.Decimal? ValorIOF
		{
			get
			{
				return base.GetSystemDecimal(PosicaoRendaFixaMetadata.ColumnNames.ValorIOF);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoRendaFixaMetadata.ColumnNames.ValorIOF, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixa.TipoNegociacao
		/// </summary>
		virtual public System.Byte? TipoNegociacao
		{
			get
			{
				return base.GetSystemByte(PosicaoRendaFixaMetadata.ColumnNames.TipoNegociacao);
			}
			
			set
			{
				base.SetSystemByte(PosicaoRendaFixaMetadata.ColumnNames.TipoNegociacao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixa.PUCorrecao
		/// </summary>
		virtual public System.Decimal? PUCorrecao
		{
			get
			{
				return base.GetSystemDecimal(PosicaoRendaFixaMetadata.ColumnNames.PUCorrecao);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoRendaFixaMetadata.ColumnNames.PUCorrecao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixa.ValorCorrecao
		/// </summary>
		virtual public System.Decimal? ValorCorrecao
		{
			get
			{
				return base.GetSystemDecimal(PosicaoRendaFixaMetadata.ColumnNames.ValorCorrecao);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoRendaFixaMetadata.ColumnNames.ValorCorrecao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixa.TaxaOperacao
		/// </summary>
		virtual public System.Decimal? TaxaOperacao
		{
			get
			{
				return base.GetSystemDecimal(PosicaoRendaFixaMetadata.ColumnNames.TaxaOperacao);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoRendaFixaMetadata.ColumnNames.TaxaOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixa.IdAgente
		/// </summary>
		virtual public System.Int32? IdAgente
		{
			get
			{
				return base.GetSystemInt32(PosicaoRendaFixaMetadata.ColumnNames.IdAgente);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoRendaFixaMetadata.ColumnNames.IdAgente, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixa.IdCustodia
		/// </summary>
		virtual public System.Byte? IdCustodia
		{
			get
			{
				return base.GetSystemByte(PosicaoRendaFixaMetadata.ColumnNames.IdCustodia);
			}
			
			set
			{
				base.SetSystemByte(PosicaoRendaFixaMetadata.ColumnNames.IdCustodia, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixa.CustoCustodia
		/// </summary>
		virtual public System.Decimal? CustoCustodia
		{
			get
			{
				return base.GetSystemDecimal(PosicaoRendaFixaMetadata.ColumnNames.CustoCustodia);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoRendaFixaMetadata.ColumnNames.CustoCustodia, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixa.IdIndiceVolta
		/// </summary>
		virtual public System.Int16? IdIndiceVolta
		{
			get
			{
				return base.GetSystemInt16(PosicaoRendaFixaMetadata.ColumnNames.IdIndiceVolta);
			}
			
			set
			{
				base.SetSystemInt16(PosicaoRendaFixaMetadata.ColumnNames.IdIndiceVolta, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixa.IdOperacao
		/// </summary>
		virtual public System.Int32? IdOperacao
		{
			get
			{
				return base.GetSystemInt32(PosicaoRendaFixaMetadata.ColumnNames.IdOperacao);
			}
			
			set
			{
				if(base.SetSystemInt32(PosicaoRendaFixaMetadata.ColumnNames.IdOperacao, value))
				{
					this._UpToOperacaoRendaFixaByIdOperacao = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixa.OperacaoTermo
		/// </summary>
		virtual public System.String OperacaoTermo
		{
			get
			{
				return base.GetSystemString(PosicaoRendaFixaMetadata.ColumnNames.OperacaoTermo);
			}
			
			set
			{
				base.SetSystemString(PosicaoRendaFixaMetadata.ColumnNames.OperacaoTermo, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixa.ValorCurvaVencimento
		/// </summary>
		virtual public System.Decimal? ValorCurvaVencimento
		{
			get
			{
				return base.GetSystemDecimal(PosicaoRendaFixaMetadata.ColumnNames.ValorCurvaVencimento);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoRendaFixaMetadata.ColumnNames.ValorCurvaVencimento, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixa.PUCurvaVencimento
		/// </summary>
		virtual public System.Decimal? PUCurvaVencimento
		{
			get
			{
				return base.GetSystemDecimal(PosicaoRendaFixaMetadata.ColumnNames.PUCurvaVencimento);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoRendaFixaMetadata.ColumnNames.PUCurvaVencimento, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixa.AjusteMTM
		/// </summary>
		virtual public System.Decimal? AjusteMTM
		{
			get
			{
				return base.GetSystemDecimal(PosicaoRendaFixaMetadata.ColumnNames.AjusteMTM);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoRendaFixaMetadata.ColumnNames.AjusteMTM, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixa.AjusteVencimento
		/// </summary>
		virtual public System.Decimal? AjusteVencimento
		{
			get
			{
				return base.GetSystemDecimal(PosicaoRendaFixaMetadata.ColumnNames.AjusteVencimento);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoRendaFixaMetadata.ColumnNames.AjusteVencimento, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixa.TaxaMTM
		/// </summary>
		virtual public System.Decimal? TaxaMTM
		{
			get
			{
				return base.GetSystemDecimal(PosicaoRendaFixaMetadata.ColumnNames.TaxaMTM);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoRendaFixaMetadata.ColumnNames.TaxaMTM, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixa.IdCorretora
		/// </summary>
		virtual public System.Int32? IdCorretora
		{
			get
			{
				return base.GetSystemInt32(PosicaoRendaFixaMetadata.ColumnNames.IdCorretora);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoRendaFixaMetadata.ColumnNames.IdCorretora, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixa.ValorBrutoGrossUp
		/// </summary>
		virtual public System.Decimal? ValorBrutoGrossUp
		{
			get
			{
				return base.GetSystemDecimal(PosicaoRendaFixaMetadata.ColumnNames.ValorBrutoGrossUp);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoRendaFixaMetadata.ColumnNames.ValorBrutoGrossUp, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixa.AliquotaIR
		/// </summary>
		virtual public System.Decimal? AliquotaIR
		{
			get
			{
				return base.GetSystemDecimal(PosicaoRendaFixaMetadata.ColumnNames.AliquotaIR);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoRendaFixaMetadata.ColumnNames.AliquotaIR, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixa.AliquotaIOF
		/// </summary>
		virtual public System.Decimal? AliquotaIOF
		{
			get
			{
				return base.GetSystemDecimal(PosicaoRendaFixaMetadata.ColumnNames.AliquotaIOF);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoRendaFixaMetadata.ColumnNames.AliquotaIOF, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixa.PrazoDecorridoDC
		/// </summary>
		virtual public System.Int32? PrazoDecorridoDC
		{
			get
			{
				return base.GetSystemInt32(PosicaoRendaFixaMetadata.ColumnNames.PrazoDecorridoDC);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoRendaFixaMetadata.ColumnNames.PrazoDecorridoDC, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixa.PrazoDecorridoDU
		/// </summary>
		virtual public System.Int32? PrazoDecorridoDU
		{
			get
			{
				return base.GetSystemInt32(PosicaoRendaFixaMetadata.ColumnNames.PrazoDecorridoDU);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoRendaFixaMetadata.ColumnNames.PrazoDecorridoDU, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		[CLSCompliant(false)]
		internal protected OperacaoRendaFixa _UpToOperacaoRendaFixaByIdOperacao;
		[CLSCompliant(false)]
		internal protected TituloRendaFixa _UpToTituloRendaFixaByIdTitulo;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esPosicaoRendaFixa entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdPosicao
			{
				get
				{
					System.Int32? data = entity.IdPosicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPosicao = null;
					else entity.IdPosicao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String IdTitulo
			{
				get
				{
					System.Int32? data = entity.IdTitulo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTitulo = null;
					else entity.IdTitulo = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoOperacao
			{
				get
				{
					System.Byte? data = entity.TipoOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoOperacao = null;
					else entity.TipoOperacao = Convert.ToByte(value);
				}
			}
				
			public System.String DataVencimento
			{
				get
				{
					System.DateTime? data = entity.DataVencimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataVencimento = null;
					else entity.DataVencimento = Convert.ToDateTime(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Decimal? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String QuantidadeBloqueada
			{
				get
				{
					System.Decimal? data = entity.QuantidadeBloqueada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeBloqueada = null;
					else entity.QuantidadeBloqueada = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataOperacao
			{
				get
				{
					System.DateTime? data = entity.DataOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataOperacao = null;
					else entity.DataOperacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataLiquidacao
			{
				get
				{
					System.DateTime? data = entity.DataLiquidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataLiquidacao = null;
					else entity.DataLiquidacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String PUOperacao
			{
				get
				{
					System.Decimal? data = entity.PUOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PUOperacao = null;
					else entity.PUOperacao = Convert.ToDecimal(value);
				}
			}
				
			public System.String PUCurva
			{
				get
				{
					System.Decimal? data = entity.PUCurva;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PUCurva = null;
					else entity.PUCurva = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorCurva
			{
				get
				{
					System.Decimal? data = entity.ValorCurva;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorCurva = null;
					else entity.ValorCurva = Convert.ToDecimal(value);
				}
			}
				
			public System.String PUMercado
			{
				get
				{
					System.Decimal? data = entity.PUMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PUMercado = null;
					else entity.PUMercado = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorMercado
			{
				get
				{
					System.Decimal? data = entity.ValorMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorMercado = null;
					else entity.ValorMercado = Convert.ToDecimal(value);
				}
			}
				
			public System.String PUJuros
			{
				get
				{
					System.Decimal? data = entity.PUJuros;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PUJuros = null;
					else entity.PUJuros = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorJuros
			{
				get
				{
					System.Decimal? data = entity.ValorJuros;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorJuros = null;
					else entity.ValorJuros = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataVolta
			{
				get
				{
					System.DateTime? data = entity.DataVolta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataVolta = null;
					else entity.DataVolta = Convert.ToDateTime(value);
				}
			}
				
			public System.String TaxaVolta
			{
				get
				{
					System.Decimal? data = entity.TaxaVolta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TaxaVolta = null;
					else entity.TaxaVolta = Convert.ToDecimal(value);
				}
			}
				
			public System.String PUVolta
			{
				get
				{
					System.Decimal? data = entity.PUVolta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PUVolta = null;
					else entity.PUVolta = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorVolta
			{
				get
				{
					System.Decimal? data = entity.ValorVolta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorVolta = null;
					else entity.ValorVolta = Convert.ToDecimal(value);
				}
			}
				
			public System.String QuantidadeInicial
			{
				get
				{
					System.Decimal? data = entity.QuantidadeInicial;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeInicial = null;
					else entity.QuantidadeInicial = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIR
			{
				get
				{
					System.Decimal? data = entity.ValorIR;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIR = null;
					else entity.ValorIR = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIOF
			{
				get
				{
					System.Decimal? data = entity.ValorIOF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIOF = null;
					else entity.ValorIOF = Convert.ToDecimal(value);
				}
			}
				
			public System.String TipoNegociacao
			{
				get
				{
					System.Byte? data = entity.TipoNegociacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoNegociacao = null;
					else entity.TipoNegociacao = Convert.ToByte(value);
				}
			}
				
			public System.String PUCorrecao
			{
				get
				{
					System.Decimal? data = entity.PUCorrecao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PUCorrecao = null;
					else entity.PUCorrecao = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorCorrecao
			{
				get
				{
					System.Decimal? data = entity.ValorCorrecao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorCorrecao = null;
					else entity.ValorCorrecao = Convert.ToDecimal(value);
				}
			}
				
			public System.String TaxaOperacao
			{
				get
				{
					System.Decimal? data = entity.TaxaOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TaxaOperacao = null;
					else entity.TaxaOperacao = Convert.ToDecimal(value);
				}
			}
				
			public System.String IdAgente
			{
				get
				{
					System.Int32? data = entity.IdAgente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgente = null;
					else entity.IdAgente = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCustodia
			{
				get
				{
					System.Byte? data = entity.IdCustodia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCustodia = null;
					else entity.IdCustodia = Convert.ToByte(value);
				}
			}
				
			public System.String CustoCustodia
			{
				get
				{
					System.Decimal? data = entity.CustoCustodia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CustoCustodia = null;
					else entity.CustoCustodia = Convert.ToDecimal(value);
				}
			}
				
			public System.String IdIndiceVolta
			{
				get
				{
					System.Int16? data = entity.IdIndiceVolta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdIndiceVolta = null;
					else entity.IdIndiceVolta = Convert.ToInt16(value);
				}
			}
				
			public System.String IdOperacao
			{
				get
				{
					System.Int32? data = entity.IdOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacao = null;
					else entity.IdOperacao = Convert.ToInt32(value);
				}
			}
				
			public System.String OperacaoTermo
			{
				get
				{
					System.String data = entity.OperacaoTermo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.OperacaoTermo = null;
					else entity.OperacaoTermo = Convert.ToString(value);
				}
			}
				
			public System.String ValorCurvaVencimento
			{
				get
				{
					System.Decimal? data = entity.ValorCurvaVencimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorCurvaVencimento = null;
					else entity.ValorCurvaVencimento = Convert.ToDecimal(value);
				}
			}
				
			public System.String PUCurvaVencimento
			{
				get
				{
					System.Decimal? data = entity.PUCurvaVencimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PUCurvaVencimento = null;
					else entity.PUCurvaVencimento = Convert.ToDecimal(value);
				}
			}
				
			public System.String AjusteMTM
			{
				get
				{
					System.Decimal? data = entity.AjusteMTM;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AjusteMTM = null;
					else entity.AjusteMTM = Convert.ToDecimal(value);
				}
			}
				
			public System.String AjusteVencimento
			{
				get
				{
					System.Decimal? data = entity.AjusteVencimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AjusteVencimento = null;
					else entity.AjusteVencimento = Convert.ToDecimal(value);
				}
			}
				
			public System.String TaxaMTM
			{
				get
				{
					System.Decimal? data = entity.TaxaMTM;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TaxaMTM = null;
					else entity.TaxaMTM = Convert.ToDecimal(value);
				}
			}
				
			public System.String IdCorretora
			{
				get
				{
					System.Int32? data = entity.IdCorretora;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCorretora = null;
					else entity.IdCorretora = Convert.ToInt32(value);
				}
			}
				
			public System.String ValorBrutoGrossUp
			{
				get
				{
					System.Decimal? data = entity.ValorBrutoGrossUp;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorBrutoGrossUp = null;
					else entity.ValorBrutoGrossUp = Convert.ToDecimal(value);
				}
			}
				
			public System.String AliquotaIR
			{
				get
				{
					System.Decimal? data = entity.AliquotaIR;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AliquotaIR = null;
					else entity.AliquotaIR = Convert.ToDecimal(value);
				}
			}
				
			public System.String AliquotaIOF
			{
				get
				{
					System.Decimal? data = entity.AliquotaIOF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AliquotaIOF = null;
					else entity.AliquotaIOF = Convert.ToDecimal(value);
				}
			}
				
			public System.String PrazoDecorridoDC
			{
				get
				{
					System.Int32? data = entity.PrazoDecorridoDC;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PrazoDecorridoDC = null;
					else entity.PrazoDecorridoDC = Convert.ToInt32(value);
				}
			}
				
			public System.String PrazoDecorridoDU
			{
				get
				{
					System.Int32? data = entity.PrazoDecorridoDU;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PrazoDecorridoDU = null;
					else entity.PrazoDecorridoDU = Convert.ToInt32(value);
				}
			}
			

			private esPosicaoRendaFixa entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esPosicaoRendaFixaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esPosicaoRendaFixa can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class PosicaoRendaFixa : esPosicaoRendaFixa
	{

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_PosicaoRendaFixa_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToOperacaoRendaFixaByIdOperacao - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Operacao_PosicaoRendaFixa_FK1
		/// </summary>

		[XmlIgnore]
		public OperacaoRendaFixa UpToOperacaoRendaFixaByIdOperacao
		{
			get
			{
				if(this._UpToOperacaoRendaFixaByIdOperacao == null
					&& IdOperacao != null					)
				{
					this._UpToOperacaoRendaFixaByIdOperacao = new OperacaoRendaFixa();
					this._UpToOperacaoRendaFixaByIdOperacao.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToOperacaoRendaFixaByIdOperacao", this._UpToOperacaoRendaFixaByIdOperacao);
					this._UpToOperacaoRendaFixaByIdOperacao.Query.Where(this._UpToOperacaoRendaFixaByIdOperacao.Query.IdOperacao == this.IdOperacao);
					this._UpToOperacaoRendaFixaByIdOperacao.Query.Load();
				}

				return this._UpToOperacaoRendaFixaByIdOperacao;
			}
			
			set
			{
				this.RemovePreSave("UpToOperacaoRendaFixaByIdOperacao");
				

				if(value == null)
				{
					this.IdOperacao = null;
					this._UpToOperacaoRendaFixaByIdOperacao = null;
				}
				else
				{
					this.IdOperacao = value.IdOperacao;
					this._UpToOperacaoRendaFixaByIdOperacao = value;
					this.SetPreSave("UpToOperacaoRendaFixaByIdOperacao", this._UpToOperacaoRendaFixaByIdOperacao);
				}
				
			}
		}
		#endregion
		

				
		#region UpToTituloRendaFixaByIdTitulo - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - TituloRendaFixa_PosicaoRendaFixa_FK1
		/// </summary>

		[XmlIgnore]
		public TituloRendaFixa UpToTituloRendaFixaByIdTitulo
		{
			get
			{
				if(this._UpToTituloRendaFixaByIdTitulo == null
					&& IdTitulo != null					)
				{
					this._UpToTituloRendaFixaByIdTitulo = new TituloRendaFixa();
					this._UpToTituloRendaFixaByIdTitulo.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToTituloRendaFixaByIdTitulo", this._UpToTituloRendaFixaByIdTitulo);
					this._UpToTituloRendaFixaByIdTitulo.Query.Where(this._UpToTituloRendaFixaByIdTitulo.Query.IdTitulo == this.IdTitulo);
					this._UpToTituloRendaFixaByIdTitulo.Query.Load();
				}

				return this._UpToTituloRendaFixaByIdTitulo;
			}
			
			set
			{
				this.RemovePreSave("UpToTituloRendaFixaByIdTitulo");
				

				if(value == null)
				{
					this.IdTitulo = null;
					this._UpToTituloRendaFixaByIdTitulo = null;
				}
				else
				{
					this.IdTitulo = value.IdTitulo;
					this._UpToTituloRendaFixaByIdTitulo = value;
					this.SetPreSave("UpToTituloRendaFixaByIdTitulo", this._UpToTituloRendaFixaByIdTitulo);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToOperacaoRendaFixaByIdOperacao != null)
			{
				this.IdOperacao = this._UpToOperacaoRendaFixaByIdOperacao.IdOperacao;
			}
			if(!this.es.IsDeleted && this._UpToTituloRendaFixaByIdTitulo != null)
			{
				this.IdTitulo = this._UpToTituloRendaFixaByIdTitulo.IdTitulo;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esPosicaoRendaFixaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return PosicaoRendaFixaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdPosicao
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaMetadata.ColumnNames.IdPosicao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdTitulo
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaMetadata.ColumnNames.IdTitulo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoOperacao
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaMetadata.ColumnNames.TipoOperacao, esSystemType.Byte);
			}
		} 
		
		public esQueryItem DataVencimento
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaMetadata.ColumnNames.DataVencimento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaMetadata.ColumnNames.Quantidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem QuantidadeBloqueada
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaMetadata.ColumnNames.QuantidadeBloqueada, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataOperacao
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaMetadata.ColumnNames.DataOperacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataLiquidacao
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaMetadata.ColumnNames.DataLiquidacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem PUOperacao
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaMetadata.ColumnNames.PUOperacao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PUCurva
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaMetadata.ColumnNames.PUCurva, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorCurva
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaMetadata.ColumnNames.ValorCurva, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PUMercado
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaMetadata.ColumnNames.PUMercado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorMercado
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaMetadata.ColumnNames.ValorMercado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PUJuros
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaMetadata.ColumnNames.PUJuros, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorJuros
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaMetadata.ColumnNames.ValorJuros, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataVolta
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaMetadata.ColumnNames.DataVolta, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem TaxaVolta
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaMetadata.ColumnNames.TaxaVolta, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PUVolta
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaMetadata.ColumnNames.PUVolta, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorVolta
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaMetadata.ColumnNames.ValorVolta, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem QuantidadeInicial
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaMetadata.ColumnNames.QuantidadeInicial, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIR
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaMetadata.ColumnNames.ValorIR, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIOF
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaMetadata.ColumnNames.ValorIOF, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TipoNegociacao
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaMetadata.ColumnNames.TipoNegociacao, esSystemType.Byte);
			}
		} 
		
		public esQueryItem PUCorrecao
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaMetadata.ColumnNames.PUCorrecao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorCorrecao
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaMetadata.ColumnNames.ValorCorrecao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TaxaOperacao
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaMetadata.ColumnNames.TaxaOperacao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IdAgente
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaMetadata.ColumnNames.IdAgente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCustodia
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaMetadata.ColumnNames.IdCustodia, esSystemType.Byte);
			}
		} 
		
		public esQueryItem CustoCustodia
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaMetadata.ColumnNames.CustoCustodia, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IdIndiceVolta
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaMetadata.ColumnNames.IdIndiceVolta, esSystemType.Int16);
			}
		} 
		
		public esQueryItem IdOperacao
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaMetadata.ColumnNames.IdOperacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem OperacaoTermo
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaMetadata.ColumnNames.OperacaoTermo, esSystemType.String);
			}
		} 
		
		public esQueryItem ValorCurvaVencimento
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaMetadata.ColumnNames.ValorCurvaVencimento, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PUCurvaVencimento
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaMetadata.ColumnNames.PUCurvaVencimento, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem AjusteMTM
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaMetadata.ColumnNames.AjusteMTM, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem AjusteVencimento
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaMetadata.ColumnNames.AjusteVencimento, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TaxaMTM
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaMetadata.ColumnNames.TaxaMTM, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IdCorretora
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaMetadata.ColumnNames.IdCorretora, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ValorBrutoGrossUp
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaMetadata.ColumnNames.ValorBrutoGrossUp, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem AliquotaIR
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaMetadata.ColumnNames.AliquotaIR, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem AliquotaIOF
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaMetadata.ColumnNames.AliquotaIOF, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PrazoDecorridoDC
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaMetadata.ColumnNames.PrazoDecorridoDC, esSystemType.Int32);
			}
		} 
		
		public esQueryItem PrazoDecorridoDU
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaMetadata.ColumnNames.PrazoDecorridoDU, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("PosicaoRendaFixaCollection")]
	public partial class PosicaoRendaFixaCollection : esPosicaoRendaFixaCollection, IEnumerable<PosicaoRendaFixa>
	{
		public PosicaoRendaFixaCollection()
		{

		}
		
		public static implicit operator List<PosicaoRendaFixa>(PosicaoRendaFixaCollection coll)
		{
			List<PosicaoRendaFixa> list = new List<PosicaoRendaFixa>();
			
			foreach (PosicaoRendaFixa emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  PosicaoRendaFixaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PosicaoRendaFixaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new PosicaoRendaFixa(row);
		}

		override protected esEntity CreateEntity()
		{
			return new PosicaoRendaFixa();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public PosicaoRendaFixaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PosicaoRendaFixaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(PosicaoRendaFixaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public PosicaoRendaFixa AddNew()
		{
			PosicaoRendaFixa entity = base.AddNewEntity() as PosicaoRendaFixa;
			
			return entity;
		}

		public PosicaoRendaFixa FindByPrimaryKey(System.Int32 idPosicao)
		{
			return base.FindByPrimaryKey(idPosicao) as PosicaoRendaFixa;
		}


		#region IEnumerable<PosicaoRendaFixa> Members

		IEnumerator<PosicaoRendaFixa> IEnumerable<PosicaoRendaFixa>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as PosicaoRendaFixa;
			}
		}

		#endregion
		
		private PosicaoRendaFixaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'PosicaoRendaFixa' table
	/// </summary>

	[Serializable]
	public partial class PosicaoRendaFixa : esPosicaoRendaFixa
	{
		public PosicaoRendaFixa()
		{

		}
	
		public PosicaoRendaFixa(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return PosicaoRendaFixaMetadata.Meta();
			}
		}
		
		
		
		override protected esPosicaoRendaFixaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PosicaoRendaFixaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public PosicaoRendaFixaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PosicaoRendaFixaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(PosicaoRendaFixaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private PosicaoRendaFixaQuery query;
	}



	[Serializable]
	public partial class PosicaoRendaFixaQuery : esPosicaoRendaFixaQuery
	{
		public PosicaoRendaFixaQuery()
		{

		}		
		
		public PosicaoRendaFixaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class PosicaoRendaFixaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected PosicaoRendaFixaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(PosicaoRendaFixaMetadata.ColumnNames.IdPosicao, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoRendaFixaMetadata.PropertyNames.IdPosicao;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaMetadata.ColumnNames.IdCliente, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoRendaFixaMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaMetadata.ColumnNames.IdTitulo, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoRendaFixaMetadata.PropertyNames.IdTitulo;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaMetadata.ColumnNames.TipoOperacao, 3, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = PosicaoRendaFixaMetadata.PropertyNames.TipoOperacao;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaMetadata.ColumnNames.DataVencimento, 4, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PosicaoRendaFixaMetadata.PropertyNames.DataVencimento;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaMetadata.ColumnNames.Quantidade, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoRendaFixaMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaMetadata.ColumnNames.QuantidadeBloqueada, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoRendaFixaMetadata.PropertyNames.QuantidadeBloqueada;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaMetadata.ColumnNames.DataOperacao, 7, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PosicaoRendaFixaMetadata.PropertyNames.DataOperacao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaMetadata.ColumnNames.DataLiquidacao, 8, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PosicaoRendaFixaMetadata.PropertyNames.DataLiquidacao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaMetadata.ColumnNames.PUOperacao, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoRendaFixaMetadata.PropertyNames.PUOperacao;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaMetadata.ColumnNames.PUCurva, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoRendaFixaMetadata.PropertyNames.PUCurva;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaMetadata.ColumnNames.ValorCurva, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoRendaFixaMetadata.PropertyNames.ValorCurva;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaMetadata.ColumnNames.PUMercado, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoRendaFixaMetadata.PropertyNames.PUMercado;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaMetadata.ColumnNames.ValorMercado, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoRendaFixaMetadata.PropertyNames.ValorMercado;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaMetadata.ColumnNames.PUJuros, 14, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoRendaFixaMetadata.PropertyNames.PUJuros;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaMetadata.ColumnNames.ValorJuros, 15, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoRendaFixaMetadata.PropertyNames.ValorJuros;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaMetadata.ColumnNames.DataVolta, 16, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PosicaoRendaFixaMetadata.PropertyNames.DataVolta;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaMetadata.ColumnNames.TaxaVolta, 17, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoRendaFixaMetadata.PropertyNames.TaxaVolta;	
			c.NumericPrecision = 8;
			c.NumericScale = 4;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaMetadata.ColumnNames.PUVolta, 18, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoRendaFixaMetadata.PropertyNames.PUVolta;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaMetadata.ColumnNames.ValorVolta, 19, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoRendaFixaMetadata.PropertyNames.ValorVolta;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaMetadata.ColumnNames.QuantidadeInicial, 20, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoRendaFixaMetadata.PropertyNames.QuantidadeInicial;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaMetadata.ColumnNames.ValorIR, 21, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoRendaFixaMetadata.PropertyNames.ValorIR;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaMetadata.ColumnNames.ValorIOF, 22, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoRendaFixaMetadata.PropertyNames.ValorIOF;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaMetadata.ColumnNames.TipoNegociacao, 23, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = PosicaoRendaFixaMetadata.PropertyNames.TipoNegociacao;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaMetadata.ColumnNames.PUCorrecao, 24, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoRendaFixaMetadata.PropertyNames.PUCorrecao;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaMetadata.ColumnNames.ValorCorrecao, 25, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoRendaFixaMetadata.PropertyNames.ValorCorrecao;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaMetadata.ColumnNames.TaxaOperacao, 26, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoRendaFixaMetadata.PropertyNames.TaxaOperacao;	
			c.NumericPrecision = 25;
			c.NumericScale = 16;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaMetadata.ColumnNames.IdAgente, 27, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoRendaFixaMetadata.PropertyNames.IdAgente;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaMetadata.ColumnNames.IdCustodia, 28, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = PosicaoRendaFixaMetadata.PropertyNames.IdCustodia;	
			c.NumericPrecision = 3;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaMetadata.ColumnNames.CustoCustodia, 29, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoRendaFixaMetadata.PropertyNames.CustoCustodia;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaMetadata.ColumnNames.IdIndiceVolta, 30, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = PosicaoRendaFixaMetadata.PropertyNames.IdIndiceVolta;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaMetadata.ColumnNames.IdOperacao, 31, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoRendaFixaMetadata.PropertyNames.IdOperacao;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaMetadata.ColumnNames.OperacaoTermo, 32, typeof(System.String), esSystemType.String);
			c.PropertyName = PosicaoRendaFixaMetadata.PropertyNames.OperacaoTermo;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaMetadata.ColumnNames.ValorCurvaVencimento, 33, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoRendaFixaMetadata.PropertyNames.ValorCurvaVencimento;	
			c.NumericPrecision = 25;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"('0')";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaMetadata.ColumnNames.PUCurvaVencimento, 34, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoRendaFixaMetadata.PropertyNames.PUCurvaVencimento;	
			c.NumericPrecision = 25;
			c.NumericScale = 8;
			c.HasDefault = true;
			c.Default = @"('0')";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaMetadata.ColumnNames.AjusteMTM, 35, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoRendaFixaMetadata.PropertyNames.AjusteMTM;	
			c.NumericPrecision = 25;
			c.NumericScale = 8;
			c.HasDefault = true;
			c.Default = @"('0')";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaMetadata.ColumnNames.AjusteVencimento, 36, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoRendaFixaMetadata.PropertyNames.AjusteVencimento;	
			c.NumericPrecision = 25;
			c.NumericScale = 8;
			c.HasDefault = true;
			c.Default = @"('0')";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaMetadata.ColumnNames.TaxaMTM, 37, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoRendaFixaMetadata.PropertyNames.TaxaMTM;	
			c.NumericPrecision = 25;
			c.NumericScale = 8;
			c.HasDefault = true;
			c.Default = @"('0')";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaMetadata.ColumnNames.IdCorretora, 38, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoRendaFixaMetadata.PropertyNames.IdCorretora;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaMetadata.ColumnNames.ValorBrutoGrossUp, 39, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoRendaFixaMetadata.PropertyNames.ValorBrutoGrossUp;	
			c.NumericPrecision = 28;
			c.NumericScale = 10;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaMetadata.ColumnNames.AliquotaIR, 40, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoRendaFixaMetadata.PropertyNames.AliquotaIR;	
			c.NumericPrecision = 28;
			c.NumericScale = 10;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaMetadata.ColumnNames.AliquotaIOF, 41, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoRendaFixaMetadata.PropertyNames.AliquotaIOF;	
			c.NumericPrecision = 28;
			c.NumericScale = 10;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaMetadata.ColumnNames.PrazoDecorridoDC, 42, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoRendaFixaMetadata.PropertyNames.PrazoDecorridoDC;	
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaMetadata.ColumnNames.PrazoDecorridoDU, 43, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoRendaFixaMetadata.PropertyNames.PrazoDecorridoDU;	
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public PosicaoRendaFixaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdPosicao = "IdPosicao";
			 public const string IdCliente = "IdCliente";
			 public const string IdTitulo = "IdTitulo";
			 public const string TipoOperacao = "TipoOperacao";
			 public const string DataVencimento = "DataVencimento";
			 public const string Quantidade = "Quantidade";
			 public const string QuantidadeBloqueada = "QuantidadeBloqueada";
			 public const string DataOperacao = "DataOperacao";
			 public const string DataLiquidacao = "DataLiquidacao";
			 public const string PUOperacao = "PUOperacao";
			 public const string PUCurva = "PUCurva";
			 public const string ValorCurva = "ValorCurva";
			 public const string PUMercado = "PUMercado";
			 public const string ValorMercado = "ValorMercado";
			 public const string PUJuros = "PUJuros";
			 public const string ValorJuros = "ValorJuros";
			 public const string DataVolta = "DataVolta";
			 public const string TaxaVolta = "TaxaVolta";
			 public const string PUVolta = "PUVolta";
			 public const string ValorVolta = "ValorVolta";
			 public const string QuantidadeInicial = "QuantidadeInicial";
			 public const string ValorIR = "ValorIR";
			 public const string ValorIOF = "ValorIOF";
			 public const string TipoNegociacao = "TipoNegociacao";
			 public const string PUCorrecao = "PUCorrecao";
			 public const string ValorCorrecao = "ValorCorrecao";
			 public const string TaxaOperacao = "TaxaOperacao";
			 public const string IdAgente = "IdAgente";
			 public const string IdCustodia = "IdCustodia";
			 public const string CustoCustodia = "CustoCustodia";
			 public const string IdIndiceVolta = "IdIndiceVolta";
			 public const string IdOperacao = "IdOperacao";
			 public const string OperacaoTermo = "OperacaoTermo";
			 public const string ValorCurvaVencimento = "ValorCurvaVencimento";
			 public const string PUCurvaVencimento = "PUCurvaVencimento";
			 public const string AjusteMTM = "AjusteMTM";
			 public const string AjusteVencimento = "AjusteVencimento";
			 public const string TaxaMTM = "TaxaMTM";
			 public const string IdCorretora = "IdCorretora";
			 public const string ValorBrutoGrossUp = "ValorBrutoGrossUp";
			 public const string AliquotaIR = "AliquotaIR";
			 public const string AliquotaIOF = "AliquotaIOF";
			 public const string PrazoDecorridoDC = "PrazoDecorridoDC";
			 public const string PrazoDecorridoDU = "PrazoDecorridoDU";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdPosicao = "IdPosicao";
			 public const string IdCliente = "IdCliente";
			 public const string IdTitulo = "IdTitulo";
			 public const string TipoOperacao = "TipoOperacao";
			 public const string DataVencimento = "DataVencimento";
			 public const string Quantidade = "Quantidade";
			 public const string QuantidadeBloqueada = "QuantidadeBloqueada";
			 public const string DataOperacao = "DataOperacao";
			 public const string DataLiquidacao = "DataLiquidacao";
			 public const string PUOperacao = "PUOperacao";
			 public const string PUCurva = "PUCurva";
			 public const string ValorCurva = "ValorCurva";
			 public const string PUMercado = "PUMercado";
			 public const string ValorMercado = "ValorMercado";
			 public const string PUJuros = "PUJuros";
			 public const string ValorJuros = "ValorJuros";
			 public const string DataVolta = "DataVolta";
			 public const string TaxaVolta = "TaxaVolta";
			 public const string PUVolta = "PUVolta";
			 public const string ValorVolta = "ValorVolta";
			 public const string QuantidadeInicial = "QuantidadeInicial";
			 public const string ValorIR = "ValorIR";
			 public const string ValorIOF = "ValorIOF";
			 public const string TipoNegociacao = "TipoNegociacao";
			 public const string PUCorrecao = "PUCorrecao";
			 public const string ValorCorrecao = "ValorCorrecao";
			 public const string TaxaOperacao = "TaxaOperacao";
			 public const string IdAgente = "IdAgente";
			 public const string IdCustodia = "IdCustodia";
			 public const string CustoCustodia = "CustoCustodia";
			 public const string IdIndiceVolta = "IdIndiceVolta";
			 public const string IdOperacao = "IdOperacao";
			 public const string OperacaoTermo = "OperacaoTermo";
			 public const string ValorCurvaVencimento = "ValorCurvaVencimento";
			 public const string PUCurvaVencimento = "PUCurvaVencimento";
			 public const string AjusteMTM = "AjusteMTM";
			 public const string AjusteVencimento = "AjusteVencimento";
			 public const string TaxaMTM = "TaxaMTM";
			 public const string IdCorretora = "IdCorretora";
			 public const string ValorBrutoGrossUp = "ValorBrutoGrossUp";
			 public const string AliquotaIR = "AliquotaIR";
			 public const string AliquotaIOF = "AliquotaIOF";
			 public const string PrazoDecorridoDC = "PrazoDecorridoDC";
			 public const string PrazoDecorridoDU = "PrazoDecorridoDU";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(PosicaoRendaFixaMetadata))
			{
				if(PosicaoRendaFixaMetadata.mapDelegates == null)
				{
					PosicaoRendaFixaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (PosicaoRendaFixaMetadata.meta == null)
				{
					PosicaoRendaFixaMetadata.meta = new PosicaoRendaFixaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdPosicao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdTitulo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoOperacao", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("DataVencimento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Quantidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("QuantidadeBloqueada", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("DataOperacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataLiquidacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("PUOperacao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PUCurva", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorCurva", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PUMercado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorMercado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PUJuros", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorJuros", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("DataVolta", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("TaxaVolta", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PUVolta", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorVolta", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("QuantidadeInicial", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIR", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIOF", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("TipoNegociacao", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("PUCorrecao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorCorrecao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("TaxaOperacao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IdAgente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCustodia", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("CustoCustodia", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IdIndiceVolta", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("IdOperacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("OperacaoTermo", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("ValorCurvaVencimento", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PUCurvaVencimento", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("AjusteMTM", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("AjusteVencimento", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("TaxaMTM", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IdCorretora", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ValorBrutoGrossUp", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("AliquotaIR", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("AliquotaIOF", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PrazoDecorridoDC", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("PrazoDecorridoDU", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "PosicaoRendaFixa";
				meta.Destination = "PosicaoRendaFixa";
				
				meta.spInsert = "proc_PosicaoRendaFixaInsert";				
				meta.spUpdate = "proc_PosicaoRendaFixaUpdate";		
				meta.spDelete = "proc_PosicaoRendaFixaDelete";
				meta.spLoadAll = "proc_PosicaoRendaFixaLoadAll";
				meta.spLoadByPrimaryKey = "proc_PosicaoRendaFixaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private PosicaoRendaFixaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
