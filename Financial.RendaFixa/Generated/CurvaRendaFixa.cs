/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 15/09/2014 16:56:48
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Common;

namespace Financial.RendaFixa
{

	[Serializable]
	abstract public class esCurvaRendaFixaCollection : esEntityCollection
	{
		public esCurvaRendaFixaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "CurvaRendaFixaCollection";
		}

		#region Query Logic
		protected void InitQuery(esCurvaRendaFixaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esCurvaRendaFixaQuery);
		}
		#endregion
		
		virtual public CurvaRendaFixa DetachEntity(CurvaRendaFixa entity)
		{
			return base.DetachEntity(entity) as CurvaRendaFixa;
		}
		
		virtual public CurvaRendaFixa AttachEntity(CurvaRendaFixa entity)
		{
			return base.AttachEntity(entity) as CurvaRendaFixa;
		}
		
		virtual public void Combine(CurvaRendaFixaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public CurvaRendaFixa this[int index]
		{
			get
			{
				return base[index] as CurvaRendaFixa;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(CurvaRendaFixa);
		}
	}



	[Serializable]
	abstract public class esCurvaRendaFixa : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esCurvaRendaFixaQuery GetDynamicQuery()
		{
			return null;
		}

		public esCurvaRendaFixa()
		{

		}

		public esCurvaRendaFixa(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idCurvaRendaFixa)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCurvaRendaFixa);
			else
				return LoadByPrimaryKeyStoredProcedure(idCurvaRendaFixa);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idCurvaRendaFixa)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCurvaRendaFixa);
			else
				return LoadByPrimaryKeyStoredProcedure(idCurvaRendaFixa);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idCurvaRendaFixa)
		{
			esCurvaRendaFixaQuery query = this.GetDynamicQuery();
			query.Where(query.IdCurvaRendaFixa == idCurvaRendaFixa);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idCurvaRendaFixa)
		{
			esParameters parms = new esParameters();
			parms.Add("IdCurvaRendaFixa",idCurvaRendaFixa);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdCurvaRendaFixa": this.str.IdCurvaRendaFixa = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;							
						case "CriterioInterpolacao": this.str.CriterioInterpolacao = (string)value; break;							
						case "ExpressaoTaxaZero": this.str.ExpressaoTaxaZero = (string)value; break;							
						case "PermiteInterpolacao": this.str.PermiteInterpolacao = (string)value; break;							
						case "PermiteExtrapolacao": this.str.PermiteExtrapolacao = (string)value; break;							
						case "TipoCurva": this.str.TipoCurva = (string)value; break;							
						case "IdCurvaBase": this.str.IdCurvaBase = (string)value; break;							
						case "IdCurvaSpread": this.str.IdCurvaSpread = (string)value; break;							
						case "TipoComposicao": this.str.TipoComposicao = (string)value; break;							
						case "IdIndice": this.str.IdIndice = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdCurvaRendaFixa":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCurvaRendaFixa = (System.Int32?)value;
							break;
						
						case "CriterioInterpolacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.CriterioInterpolacao = (System.Int32?)value;
							break;
						
						case "ExpressaoTaxaZero":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.ExpressaoTaxaZero = (System.Int32?)value;
							break;
						
						case "PermiteInterpolacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.PermiteInterpolacao = (System.Int32?)value;
							break;
						
						case "PermiteExtrapolacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.PermiteExtrapolacao = (System.Int32?)value;
							break;
						
						case "TipoCurva":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.TipoCurva = (System.Int32?)value;
							break;
						
						case "IdCurvaBase":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCurvaBase = (System.Int32?)value;
							break;
						
						case "IdCurvaSpread":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCurvaSpread = (System.Int32?)value;
							break;
						
						case "TipoComposicao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.TipoComposicao = (System.Int32?)value;
							break;
						
						case "IdIndice":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdIndice = (System.Int16?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to CurvaRendaFixa.IdCurvaRendaFixa
		/// </summary>
		virtual public System.Int32? IdCurvaRendaFixa
		{
			get
			{
				return base.GetSystemInt32(CurvaRendaFixaMetadata.ColumnNames.IdCurvaRendaFixa);
			}
			
			set
			{
				base.SetSystemInt32(CurvaRendaFixaMetadata.ColumnNames.IdCurvaRendaFixa, value);
			}
		}
		
		/// <summary>
		/// Maps to CurvaRendaFixa.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(CurvaRendaFixaMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(CurvaRendaFixaMetadata.ColumnNames.Descricao, value);
			}
		}
		
		/// <summary>
		/// Maps to CurvaRendaFixa.CriterioInterpolacao
		/// </summary>
		virtual public System.Int32? CriterioInterpolacao
		{
			get
			{
				return base.GetSystemInt32(CurvaRendaFixaMetadata.ColumnNames.CriterioInterpolacao);
			}
			
			set
			{
				base.SetSystemInt32(CurvaRendaFixaMetadata.ColumnNames.CriterioInterpolacao, value);
			}
		}
		
		/// <summary>
		/// Maps to CurvaRendaFixa.ExpressaoTaxaZero
		/// </summary>
		virtual public System.Int32? ExpressaoTaxaZero
		{
			get
			{
				return base.GetSystemInt32(CurvaRendaFixaMetadata.ColumnNames.ExpressaoTaxaZero);
			}
			
			set
			{
				base.SetSystemInt32(CurvaRendaFixaMetadata.ColumnNames.ExpressaoTaxaZero, value);
			}
		}
		
		/// <summary>
		/// Maps to CurvaRendaFixa.PermiteInterpolacao
		/// </summary>
		virtual public System.Int32? PermiteInterpolacao
		{
			get
			{
				return base.GetSystemInt32(CurvaRendaFixaMetadata.ColumnNames.PermiteInterpolacao);
			}
			
			set
			{
				base.SetSystemInt32(CurvaRendaFixaMetadata.ColumnNames.PermiteInterpolacao, value);
			}
		}
		
		/// <summary>
		/// Maps to CurvaRendaFixa.PermiteExtrapolacao
		/// </summary>
		virtual public System.Int32? PermiteExtrapolacao
		{
			get
			{
				return base.GetSystemInt32(CurvaRendaFixaMetadata.ColumnNames.PermiteExtrapolacao);
			}
			
			set
			{
				base.SetSystemInt32(CurvaRendaFixaMetadata.ColumnNames.PermiteExtrapolacao, value);
			}
		}
		
		/// <summary>
		/// Maps to CurvaRendaFixa.TipoCurva
		/// </summary>
		virtual public System.Int32? TipoCurva
		{
			get
			{
				return base.GetSystemInt32(CurvaRendaFixaMetadata.ColumnNames.TipoCurva);
			}
			
			set
			{
				base.SetSystemInt32(CurvaRendaFixaMetadata.ColumnNames.TipoCurva, value);
			}
		}
		
		/// <summary>
		/// Maps to CurvaRendaFixa.IdCurvaBase
		/// </summary>
		virtual public System.Int32? IdCurvaBase
		{
			get
			{
				return base.GetSystemInt32(CurvaRendaFixaMetadata.ColumnNames.IdCurvaBase);
			}
			
			set
			{
				if(base.SetSystemInt32(CurvaRendaFixaMetadata.ColumnNames.IdCurvaBase, value))
				{
					this._UpToCurvaRendaFixaByIdCurvaBase = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to CurvaRendaFixa.IdCurvaSpread
		/// </summary>
		virtual public System.Int32? IdCurvaSpread
		{
			get
			{
				return base.GetSystemInt32(CurvaRendaFixaMetadata.ColumnNames.IdCurvaSpread);
			}
			
			set
			{
				if(base.SetSystemInt32(CurvaRendaFixaMetadata.ColumnNames.IdCurvaSpread, value))
				{
					this._UpToCurvaRendaFixaByIdCurvaSpread = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to CurvaRendaFixa.TipoComposicao
		/// </summary>
		virtual public System.Int32? TipoComposicao
		{
			get
			{
				return base.GetSystemInt32(CurvaRendaFixaMetadata.ColumnNames.TipoComposicao);
			}
			
			set
			{
				base.SetSystemInt32(CurvaRendaFixaMetadata.ColumnNames.TipoComposicao, value);
			}
		}
		
		/// <summary>
		/// Maps to CurvaRendaFixa.IdIndice
		/// </summary>
		virtual public System.Int16? IdIndice
		{
			get
			{
				return base.GetSystemInt16(CurvaRendaFixaMetadata.ColumnNames.IdIndice);
			}
			
			set
			{
				if(base.SetSystemInt16(CurvaRendaFixaMetadata.ColumnNames.IdIndice, value))
				{
					this._UpToIndiceByIdIndice = null;
				}
			}
		}
		
		[CLSCompliant(false)]
		internal protected CurvaRendaFixa _UpToCurvaRendaFixaByIdCurvaSpread;
		[CLSCompliant(false)]
		internal protected CurvaRendaFixa _UpToCurvaRendaFixaByIdCurvaBase;
		[CLSCompliant(false)]
		internal protected Indice _UpToIndiceByIdIndice;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esCurvaRendaFixa entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdCurvaRendaFixa
			{
				get
				{
					System.Int32? data = entity.IdCurvaRendaFixa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCurvaRendaFixa = null;
					else entity.IdCurvaRendaFixa = Convert.ToInt32(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
				
			public System.String CriterioInterpolacao
			{
				get
				{
					System.Int32? data = entity.CriterioInterpolacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CriterioInterpolacao = null;
					else entity.CriterioInterpolacao = Convert.ToInt32(value);
				}
			}
				
			public System.String ExpressaoTaxaZero
			{
				get
				{
					System.Int32? data = entity.ExpressaoTaxaZero;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ExpressaoTaxaZero = null;
					else entity.ExpressaoTaxaZero = Convert.ToInt32(value);
				}
			}
				
			public System.String PermiteInterpolacao
			{
				get
				{
					System.Int32? data = entity.PermiteInterpolacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PermiteInterpolacao = null;
					else entity.PermiteInterpolacao = Convert.ToInt32(value);
				}
			}
				
			public System.String PermiteExtrapolacao
			{
				get
				{
					System.Int32? data = entity.PermiteExtrapolacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PermiteExtrapolacao = null;
					else entity.PermiteExtrapolacao = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoCurva
			{
				get
				{
					System.Int32? data = entity.TipoCurva;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoCurva = null;
					else entity.TipoCurva = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCurvaBase
			{
				get
				{
					System.Int32? data = entity.IdCurvaBase;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCurvaBase = null;
					else entity.IdCurvaBase = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCurvaSpread
			{
				get
				{
					System.Int32? data = entity.IdCurvaSpread;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCurvaSpread = null;
					else entity.IdCurvaSpread = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoComposicao
			{
				get
				{
					System.Int32? data = entity.TipoComposicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoComposicao = null;
					else entity.TipoComposicao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdIndice
			{
				get
				{
					System.Int16? data = entity.IdIndice;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdIndice = null;
					else entity.IdIndice = Convert.ToInt16(value);
				}
			}
			

			private esCurvaRendaFixa entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esCurvaRendaFixaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esCurvaRendaFixa can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class CurvaRendaFixa : esCurvaRendaFixa
	{

				
		#region CurvaRendaFixaCollectionByIdCurvaSpread - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - CurvaRF_CurvaSpread_FK
		/// </summary>

		[XmlIgnore]
		public CurvaRendaFixaCollection CurvaRendaFixaCollectionByIdCurvaSpread
		{
			get
			{
				if(this._CurvaRendaFixaCollectionByIdCurvaSpread == null)
				{
					this._CurvaRendaFixaCollectionByIdCurvaSpread = new CurvaRendaFixaCollection();
					this._CurvaRendaFixaCollectionByIdCurvaSpread.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("CurvaRendaFixaCollectionByIdCurvaSpread", this._CurvaRendaFixaCollectionByIdCurvaSpread);
				
					if(this.IdCurvaRendaFixa != null)
					{
						this._CurvaRendaFixaCollectionByIdCurvaSpread.Query.Where(this._CurvaRendaFixaCollectionByIdCurvaSpread.Query.IdCurvaSpread == this.IdCurvaRendaFixa);
						this._CurvaRendaFixaCollectionByIdCurvaSpread.Query.Load();

						// Auto-hookup Foreign Keys
						this._CurvaRendaFixaCollectionByIdCurvaSpread.fks.Add(CurvaRendaFixaMetadata.ColumnNames.IdCurvaSpread, this.IdCurvaRendaFixa);
					}
				}

				return this._CurvaRendaFixaCollectionByIdCurvaSpread;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._CurvaRendaFixaCollectionByIdCurvaSpread != null) 
				{ 
					this.RemovePostSave("CurvaRendaFixaCollectionByIdCurvaSpread"); 
					this._CurvaRendaFixaCollectionByIdCurvaSpread = null;
					
				} 
			} 			
		}

		private CurvaRendaFixaCollection _CurvaRendaFixaCollectionByIdCurvaSpread;
		#endregion

				
		#region UpToCurvaRendaFixaByIdCurvaSpread - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - CurvaRF_CurvaSpread_FK
		/// </summary>

		[XmlIgnore]
		public CurvaRendaFixa UpToCurvaRendaFixaByIdCurvaSpread
		{
			get
			{
				if(this._UpToCurvaRendaFixaByIdCurvaSpread == null
					&& IdCurvaSpread != null					)
				{
					this._UpToCurvaRendaFixaByIdCurvaSpread = new CurvaRendaFixa();
					this._UpToCurvaRendaFixaByIdCurvaSpread.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCurvaRendaFixaByIdCurvaSpread", this._UpToCurvaRendaFixaByIdCurvaSpread);
					this._UpToCurvaRendaFixaByIdCurvaSpread.Query.Where(this._UpToCurvaRendaFixaByIdCurvaSpread.Query.IdCurvaRendaFixa == this.IdCurvaSpread);
					this._UpToCurvaRendaFixaByIdCurvaSpread.Query.Load();
				}

				return this._UpToCurvaRendaFixaByIdCurvaSpread;
			}
			
			set
			{
				this.RemovePreSave("UpToCurvaRendaFixaByIdCurvaSpread");
				

				if(value == null)
				{
					this.IdCurvaSpread = null;
					this._UpToCurvaRendaFixaByIdCurvaSpread = null;
				}
				else
				{
					this.IdCurvaSpread = value.IdCurvaRendaFixa;
					this._UpToCurvaRendaFixaByIdCurvaSpread = value;
					this.SetPreSave("UpToCurvaRendaFixaByIdCurvaSpread", this._UpToCurvaRendaFixaByIdCurvaSpread);
				}
				
			}
		}
		#endregion
		

				
		#region CurvaRendaFixaCollectionByIdCurvaBase - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - CurvaRF_CurvaBase_FK
		/// </summary>

		[XmlIgnore]
		public CurvaRendaFixaCollection CurvaRendaFixaCollectionByIdCurvaBase
		{
			get
			{
				if(this._CurvaRendaFixaCollectionByIdCurvaBase == null)
				{
					this._CurvaRendaFixaCollectionByIdCurvaBase = new CurvaRendaFixaCollection();
					this._CurvaRendaFixaCollectionByIdCurvaBase.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("CurvaRendaFixaCollectionByIdCurvaBase", this._CurvaRendaFixaCollectionByIdCurvaBase);
				
					if(this.IdCurvaRendaFixa != null)
					{
						this._CurvaRendaFixaCollectionByIdCurvaBase.Query.Where(this._CurvaRendaFixaCollectionByIdCurvaBase.Query.IdCurvaBase == this.IdCurvaRendaFixa);
						this._CurvaRendaFixaCollectionByIdCurvaBase.Query.Load();

						// Auto-hookup Foreign Keys
						this._CurvaRendaFixaCollectionByIdCurvaBase.fks.Add(CurvaRendaFixaMetadata.ColumnNames.IdCurvaBase, this.IdCurvaRendaFixa);
					}
				}

				return this._CurvaRendaFixaCollectionByIdCurvaBase;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._CurvaRendaFixaCollectionByIdCurvaBase != null) 
				{ 
					this.RemovePostSave("CurvaRendaFixaCollectionByIdCurvaBase"); 
					this._CurvaRendaFixaCollectionByIdCurvaBase = null;
					
				} 
			} 			
		}

		private CurvaRendaFixaCollection _CurvaRendaFixaCollectionByIdCurvaBase;
		#endregion

				
		#region UpToCurvaRendaFixaByIdCurvaBase - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - CurvaRF_CurvaBase_FK
		/// </summary>

		[XmlIgnore]
		public CurvaRendaFixa UpToCurvaRendaFixaByIdCurvaBase
		{
			get
			{
				if(this._UpToCurvaRendaFixaByIdCurvaBase == null
					&& IdCurvaBase != null					)
				{
					this._UpToCurvaRendaFixaByIdCurvaBase = new CurvaRendaFixa();
					this._UpToCurvaRendaFixaByIdCurvaBase.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCurvaRendaFixaByIdCurvaBase", this._UpToCurvaRendaFixaByIdCurvaBase);
					this._UpToCurvaRendaFixaByIdCurvaBase.Query.Where(this._UpToCurvaRendaFixaByIdCurvaBase.Query.IdCurvaRendaFixa == this.IdCurvaBase);
					this._UpToCurvaRendaFixaByIdCurvaBase.Query.Load();
				}

				return this._UpToCurvaRendaFixaByIdCurvaBase;
			}
			
			set
			{
				this.RemovePreSave("UpToCurvaRendaFixaByIdCurvaBase");
				

				if(value == null)
				{
					this.IdCurvaBase = null;
					this._UpToCurvaRendaFixaByIdCurvaBase = null;
				}
				else
				{
					this.IdCurvaBase = value.IdCurvaRendaFixa;
					this._UpToCurvaRendaFixaByIdCurvaBase = value;
					this.SetPreSave("UpToCurvaRendaFixaByIdCurvaBase", this._UpToCurvaRendaFixaByIdCurvaBase);
				}
				
			}
		}
		#endregion
		

				
		#region TaxaCurvaCollectionByIdCurvaRendaFixa - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - TaxaCurva_CurvaRF_FK
		/// </summary>

		[XmlIgnore]
		public TaxaCurvaCollection TaxaCurvaCollectionByIdCurvaRendaFixa
		{
			get
			{
				if(this._TaxaCurvaCollectionByIdCurvaRendaFixa == null)
				{
					this._TaxaCurvaCollectionByIdCurvaRendaFixa = new TaxaCurvaCollection();
					this._TaxaCurvaCollectionByIdCurvaRendaFixa.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TaxaCurvaCollectionByIdCurvaRendaFixa", this._TaxaCurvaCollectionByIdCurvaRendaFixa);
				
					if(this.IdCurvaRendaFixa != null)
					{
						this._TaxaCurvaCollectionByIdCurvaRendaFixa.Query.Where(this._TaxaCurvaCollectionByIdCurvaRendaFixa.Query.IdCurvaRendaFixa == this.IdCurvaRendaFixa);
						this._TaxaCurvaCollectionByIdCurvaRendaFixa.Query.Load();

						// Auto-hookup Foreign Keys
						this._TaxaCurvaCollectionByIdCurvaRendaFixa.fks.Add(TaxaCurvaMetadata.ColumnNames.IdCurvaRendaFixa, this.IdCurvaRendaFixa);
					}
				}

				return this._TaxaCurvaCollectionByIdCurvaRendaFixa;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TaxaCurvaCollectionByIdCurvaRendaFixa != null) 
				{ 
					this.RemovePostSave("TaxaCurvaCollectionByIdCurvaRendaFixa"); 
					this._TaxaCurvaCollectionByIdCurvaRendaFixa = null;
					
				} 
			} 			
		}

		private TaxaCurvaCollection _TaxaCurvaCollectionByIdCurvaRendaFixa;
		#endregion

				
		#region UpToIndiceByIdIndice - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Curva_Indice_FK
		/// </summary>

		[XmlIgnore]
		public Indice UpToIndiceByIdIndice
		{
			get
			{
				if(this._UpToIndiceByIdIndice == null
					&& IdIndice != null					)
				{
					this._UpToIndiceByIdIndice = new Indice();
					this._UpToIndiceByIdIndice.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToIndiceByIdIndice", this._UpToIndiceByIdIndice);
					this._UpToIndiceByIdIndice.Query.Where(this._UpToIndiceByIdIndice.Query.IdIndice == this.IdIndice);
					this._UpToIndiceByIdIndice.Query.Load();
				}

				return this._UpToIndiceByIdIndice;
			}
			
			set
			{
				this.RemovePreSave("UpToIndiceByIdIndice");
				

				if(value == null)
				{
					this.IdIndice = null;
					this._UpToIndiceByIdIndice = null;
				}
				else
				{
					this.IdIndice = value.IdIndice;
					this._UpToIndiceByIdIndice = value;
					this.SetPreSave("UpToIndiceByIdIndice", this._UpToIndiceByIdIndice);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "CurvaRendaFixaCollectionByIdCurvaSpread", typeof(CurvaRendaFixaCollection), new CurvaRendaFixa()));
			props.Add(new esPropertyDescriptor(this, "CurvaRendaFixaCollectionByIdCurvaBase", typeof(CurvaRendaFixaCollection), new CurvaRendaFixa()));
			props.Add(new esPropertyDescriptor(this, "TaxaCurvaCollectionByIdCurvaRendaFixa", typeof(TaxaCurvaCollection), new TaxaCurva()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esCurvaRendaFixaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return CurvaRendaFixaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdCurvaRendaFixa
		{
			get
			{
				return new esQueryItem(this, CurvaRendaFixaMetadata.ColumnNames.IdCurvaRendaFixa, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, CurvaRendaFixaMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
		public esQueryItem CriterioInterpolacao
		{
			get
			{
				return new esQueryItem(this, CurvaRendaFixaMetadata.ColumnNames.CriterioInterpolacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ExpressaoTaxaZero
		{
			get
			{
				return new esQueryItem(this, CurvaRendaFixaMetadata.ColumnNames.ExpressaoTaxaZero, esSystemType.Int32);
			}
		} 
		
		public esQueryItem PermiteInterpolacao
		{
			get
			{
				return new esQueryItem(this, CurvaRendaFixaMetadata.ColumnNames.PermiteInterpolacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem PermiteExtrapolacao
		{
			get
			{
				return new esQueryItem(this, CurvaRendaFixaMetadata.ColumnNames.PermiteExtrapolacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoCurva
		{
			get
			{
				return new esQueryItem(this, CurvaRendaFixaMetadata.ColumnNames.TipoCurva, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCurvaBase
		{
			get
			{
				return new esQueryItem(this, CurvaRendaFixaMetadata.ColumnNames.IdCurvaBase, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCurvaSpread
		{
			get
			{
				return new esQueryItem(this, CurvaRendaFixaMetadata.ColumnNames.IdCurvaSpread, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoComposicao
		{
			get
			{
				return new esQueryItem(this, CurvaRendaFixaMetadata.ColumnNames.TipoComposicao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdIndice
		{
			get
			{
				return new esQueryItem(this, CurvaRendaFixaMetadata.ColumnNames.IdIndice, esSystemType.Int16);
			}
		} 
		
	}



	[Serializable]
	[XmlType("CurvaRendaFixaCollection")]
	public partial class CurvaRendaFixaCollection : esCurvaRendaFixaCollection, IEnumerable<CurvaRendaFixa>
	{
		public CurvaRendaFixaCollection()
		{

		}
		
		public static implicit operator List<CurvaRendaFixa>(CurvaRendaFixaCollection coll)
		{
			List<CurvaRendaFixa> list = new List<CurvaRendaFixa>();
			
			foreach (CurvaRendaFixa emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  CurvaRendaFixaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CurvaRendaFixaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new CurvaRendaFixa(row);
		}

		override protected esEntity CreateEntity()
		{
			return new CurvaRendaFixa();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public CurvaRendaFixaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CurvaRendaFixaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(CurvaRendaFixaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public CurvaRendaFixa AddNew()
		{
			CurvaRendaFixa entity = base.AddNewEntity() as CurvaRendaFixa;
			
			return entity;
		}

		public CurvaRendaFixa FindByPrimaryKey(System.Int32 idCurvaRendaFixa)
		{
			return base.FindByPrimaryKey(idCurvaRendaFixa) as CurvaRendaFixa;
		}


		#region IEnumerable<CurvaRendaFixa> Members

		IEnumerator<CurvaRendaFixa> IEnumerable<CurvaRendaFixa>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as CurvaRendaFixa;
			}
		}

		#endregion
		
		private CurvaRendaFixaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'CurvaRendaFixa' table
	/// </summary>

	[Serializable]
	public partial class CurvaRendaFixa : esCurvaRendaFixa
	{
		public CurvaRendaFixa()
		{

		}
	
		public CurvaRendaFixa(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return CurvaRendaFixaMetadata.Meta();
			}
		}
		
		
		
		override protected esCurvaRendaFixaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CurvaRendaFixaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public CurvaRendaFixaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CurvaRendaFixaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(CurvaRendaFixaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private CurvaRendaFixaQuery query;
	}



	[Serializable]
	public partial class CurvaRendaFixaQuery : esCurvaRendaFixaQuery
	{
		public CurvaRendaFixaQuery()
		{

		}		
		
		public CurvaRendaFixaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class CurvaRendaFixaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected CurvaRendaFixaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(CurvaRendaFixaMetadata.ColumnNames.IdCurvaRendaFixa, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CurvaRendaFixaMetadata.PropertyNames.IdCurvaRendaFixa;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CurvaRendaFixaMetadata.ColumnNames.Descricao, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = CurvaRendaFixaMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CurvaRendaFixaMetadata.ColumnNames.CriterioInterpolacao, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CurvaRendaFixaMetadata.PropertyNames.CriterioInterpolacao;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CurvaRendaFixaMetadata.ColumnNames.ExpressaoTaxaZero, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CurvaRendaFixaMetadata.PropertyNames.ExpressaoTaxaZero;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CurvaRendaFixaMetadata.ColumnNames.PermiteInterpolacao, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CurvaRendaFixaMetadata.PropertyNames.PermiteInterpolacao;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CurvaRendaFixaMetadata.ColumnNames.PermiteExtrapolacao, 5, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CurvaRendaFixaMetadata.PropertyNames.PermiteExtrapolacao;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CurvaRendaFixaMetadata.ColumnNames.TipoCurva, 6, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CurvaRendaFixaMetadata.PropertyNames.TipoCurva;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CurvaRendaFixaMetadata.ColumnNames.IdCurvaBase, 7, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CurvaRendaFixaMetadata.PropertyNames.IdCurvaBase;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CurvaRendaFixaMetadata.ColumnNames.IdCurvaSpread, 8, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CurvaRendaFixaMetadata.PropertyNames.IdCurvaSpread;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CurvaRendaFixaMetadata.ColumnNames.TipoComposicao, 9, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CurvaRendaFixaMetadata.PropertyNames.TipoComposicao;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CurvaRendaFixaMetadata.ColumnNames.IdIndice, 10, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = CurvaRendaFixaMetadata.PropertyNames.IdIndice;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public CurvaRendaFixaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdCurvaRendaFixa = "IdCurvaRendaFixa";
			 public const string Descricao = "Descricao";
			 public const string CriterioInterpolacao = "CriterioInterpolacao";
			 public const string ExpressaoTaxaZero = "ExpressaoTaxaZero";
			 public const string PermiteInterpolacao = "PermiteInterpolacao";
			 public const string PermiteExtrapolacao = "PermiteExtrapolacao";
			 public const string TipoCurva = "TipoCurva";
			 public const string IdCurvaBase = "IdCurvaBase";
			 public const string IdCurvaSpread = "IdCurvaSpread";
			 public const string TipoComposicao = "TipoComposicao";
			 public const string IdIndice = "IdIndice";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdCurvaRendaFixa = "IdCurvaRendaFixa";
			 public const string Descricao = "Descricao";
			 public const string CriterioInterpolacao = "CriterioInterpolacao";
			 public const string ExpressaoTaxaZero = "ExpressaoTaxaZero";
			 public const string PermiteInterpolacao = "PermiteInterpolacao";
			 public const string PermiteExtrapolacao = "PermiteExtrapolacao";
			 public const string TipoCurva = "TipoCurva";
			 public const string IdCurvaBase = "IdCurvaBase";
			 public const string IdCurvaSpread = "IdCurvaSpread";
			 public const string TipoComposicao = "TipoComposicao";
			 public const string IdIndice = "IdIndice";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(CurvaRendaFixaMetadata))
			{
				if(CurvaRendaFixaMetadata.mapDelegates == null)
				{
					CurvaRendaFixaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (CurvaRendaFixaMetadata.meta == null)
				{
					CurvaRendaFixaMetadata.meta = new CurvaRendaFixaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdCurvaRendaFixa", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("CriterioInterpolacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ExpressaoTaxaZero", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("PermiteInterpolacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("PermiteExtrapolacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoCurva", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCurvaBase", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCurvaSpread", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoComposicao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdIndice", new esTypeMap("smallint", "System.Int16"));			
				
				
				
				meta.Source = "CurvaRendaFixa";
				meta.Destination = "CurvaRendaFixa";
				
				meta.spInsert = "proc_CurvaRendaFixaInsert";				
				meta.spUpdate = "proc_CurvaRendaFixaUpdate";		
				meta.spDelete = "proc_CurvaRendaFixaDelete";
				meta.spLoadAll = "proc_CurvaRendaFixaLoadAll";
				meta.spLoadByPrimaryKey = "proc_CurvaRendaFixaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private CurvaRendaFixaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
