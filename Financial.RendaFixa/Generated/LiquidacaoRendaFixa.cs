/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 12/12/2014 2:02:40 PM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
		
using Financial.Investidor;
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.RendaFixa
{

	[Serializable]
	abstract public class esLiquidacaoRendaFixaCollection : esEntityCollection
	{
		public esLiquidacaoRendaFixaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "LiquidacaoRendaFixaCollection";
		}

		#region Query Logic
		protected void InitQuery(esLiquidacaoRendaFixaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esLiquidacaoRendaFixaQuery);
		}
		#endregion
		
		virtual public LiquidacaoRendaFixa DetachEntity(LiquidacaoRendaFixa entity)
		{
			return base.DetachEntity(entity) as LiquidacaoRendaFixa;
		}
		
		virtual public LiquidacaoRendaFixa AttachEntity(LiquidacaoRendaFixa entity)
		{
			return base.AttachEntity(entity) as LiquidacaoRendaFixa;
		}
		
		virtual public void Combine(LiquidacaoRendaFixaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public LiquidacaoRendaFixa this[int index]
		{
			get
			{
				return base[index] as LiquidacaoRendaFixa;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(LiquidacaoRendaFixa);
		}
	}



	[Serializable]
	abstract public class esLiquidacaoRendaFixa : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esLiquidacaoRendaFixaQuery GetDynamicQuery()
		{
			return null;
		}

		public esLiquidacaoRendaFixa()
		{

		}

		public esLiquidacaoRendaFixa(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idLiquidacao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idLiquidacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idLiquidacao);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idLiquidacao)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esLiquidacaoRendaFixaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdLiquidacao == idLiquidacao);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idLiquidacao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idLiquidacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idLiquidacao);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idLiquidacao)
		{
			esLiquidacaoRendaFixaQuery query = this.GetDynamicQuery();
			query.Where(query.IdLiquidacao == idLiquidacao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idLiquidacao)
		{
			esParameters parms = new esParameters();
			parms.Add("IdLiquidacao",idLiquidacao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdLiquidacao": this.str.IdLiquidacao = (string)value; break;							
						case "TipoLancamento": this.str.TipoLancamento = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "IdTitulo": this.str.IdTitulo = (string)value; break;							
						case "DataLiquidacao": this.str.DataLiquidacao = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "Rendimento": this.str.Rendimento = (string)value; break;							
						case "ValorIR": this.str.ValorIR = (string)value; break;							
						case "ValorIOF": this.str.ValorIOF = (string)value; break;							
						case "IdPosicaoResgatada": this.str.IdPosicaoResgatada = (string)value; break;							
						case "ValorBruto": this.str.ValorBruto = (string)value; break;							
						case "ValorLiquido": this.str.ValorLiquido = (string)value; break;							
						case "PULiquidacao": this.str.PULiquidacao = (string)value; break;							
						case "RendimentoNaoTributavel": this.str.RendimentoNaoTributavel = (string)value; break;							
						case "Status": this.str.Status = (string)value; break;							
						case "IdOperacaoVenda": this.str.IdOperacaoVenda = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdLiquidacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdLiquidacao = (System.Int32?)value;
							break;
						
						case "TipoLancamento":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoLancamento = (System.Byte?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "IdTitulo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTitulo = (System.Int32?)value;
							break;
						
						case "DataLiquidacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataLiquidacao = (System.DateTime?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Quantidade = (System.Decimal?)value;
							break;
						
						case "Rendimento":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Rendimento = (System.Decimal?)value;
							break;
						
						case "ValorIR":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIR = (System.Decimal?)value;
							break;
						
						case "ValorIOF":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIOF = (System.Decimal?)value;
							break;
						
						case "IdPosicaoResgatada":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPosicaoResgatada = (System.Int32?)value;
							break;
						
						case "ValorBruto":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorBruto = (System.Decimal?)value;
							break;
						
						case "ValorLiquido":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorLiquido = (System.Decimal?)value;
							break;
						
						case "PULiquidacao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PULiquidacao = (System.Decimal?)value;
							break;
						
						case "RendimentoNaoTributavel":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.RendimentoNaoTributavel = (System.Decimal?)value;
							break;
						
						case "Status":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Status = (System.Byte?)value;
							break;
						
						case "IdOperacaoVenda":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacaoVenda = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to LiquidacaoRendaFixa.IdLiquidacao
		/// </summary>
		virtual public System.Int32? IdLiquidacao
		{
			get
			{
				return base.GetSystemInt32(LiquidacaoRendaFixaMetadata.ColumnNames.IdLiquidacao);
			}
			
			set
			{
				base.SetSystemInt32(LiquidacaoRendaFixaMetadata.ColumnNames.IdLiquidacao, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoRendaFixa.TipoLancamento
		/// </summary>
		virtual public System.Byte? TipoLancamento
		{
			get
			{
				return base.GetSystemByte(LiquidacaoRendaFixaMetadata.ColumnNames.TipoLancamento);
			}
			
			set
			{
				base.SetSystemByte(LiquidacaoRendaFixaMetadata.ColumnNames.TipoLancamento, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoRendaFixa.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(LiquidacaoRendaFixaMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(LiquidacaoRendaFixaMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoRendaFixa.IdTitulo
		/// </summary>
		virtual public System.Int32? IdTitulo
		{
			get
			{
				return base.GetSystemInt32(LiquidacaoRendaFixaMetadata.ColumnNames.IdTitulo);
			}
			
			set
			{
				if(base.SetSystemInt32(LiquidacaoRendaFixaMetadata.ColumnNames.IdTitulo, value))
				{
					this._UpToTituloRendaFixaByIdTitulo = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoRendaFixa.DataLiquidacao
		/// </summary>
		virtual public System.DateTime? DataLiquidacao
		{
			get
			{
				return base.GetSystemDateTime(LiquidacaoRendaFixaMetadata.ColumnNames.DataLiquidacao);
			}
			
			set
			{
				base.SetSystemDateTime(LiquidacaoRendaFixaMetadata.ColumnNames.DataLiquidacao, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoRendaFixa.Quantidade
		/// </summary>
		virtual public System.Decimal? Quantidade
		{
			get
			{
				return base.GetSystemDecimal(LiquidacaoRendaFixaMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemDecimal(LiquidacaoRendaFixaMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoRendaFixa.Rendimento
		/// </summary>
		virtual public System.Decimal? Rendimento
		{
			get
			{
				return base.GetSystemDecimal(LiquidacaoRendaFixaMetadata.ColumnNames.Rendimento);
			}
			
			set
			{
				base.SetSystemDecimal(LiquidacaoRendaFixaMetadata.ColumnNames.Rendimento, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoRendaFixa.ValorIR
		/// </summary>
		virtual public System.Decimal? ValorIR
		{
			get
			{
				return base.GetSystemDecimal(LiquidacaoRendaFixaMetadata.ColumnNames.ValorIR);
			}
			
			set
			{
				base.SetSystemDecimal(LiquidacaoRendaFixaMetadata.ColumnNames.ValorIR, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoRendaFixa.ValorIOF
		/// </summary>
		virtual public System.Decimal? ValorIOF
		{
			get
			{
				return base.GetSystemDecimal(LiquidacaoRendaFixaMetadata.ColumnNames.ValorIOF);
			}
			
			set
			{
				base.SetSystemDecimal(LiquidacaoRendaFixaMetadata.ColumnNames.ValorIOF, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoRendaFixa.IdPosicaoResgatada
		/// </summary>
		virtual public System.Int32? IdPosicaoResgatada
		{
			get
			{
				return base.GetSystemInt32(LiquidacaoRendaFixaMetadata.ColumnNames.IdPosicaoResgatada);
			}
			
			set
			{
				base.SetSystemInt32(LiquidacaoRendaFixaMetadata.ColumnNames.IdPosicaoResgatada, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoRendaFixa.ValorBruto
		/// </summary>
		virtual public System.Decimal? ValorBruto
		{
			get
			{
				return base.GetSystemDecimal(LiquidacaoRendaFixaMetadata.ColumnNames.ValorBruto);
			}
			
			set
			{
				base.SetSystemDecimal(LiquidacaoRendaFixaMetadata.ColumnNames.ValorBruto, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoRendaFixa.ValorLiquido
		/// </summary>
		virtual public System.Decimal? ValorLiquido
		{
			get
			{
				return base.GetSystemDecimal(LiquidacaoRendaFixaMetadata.ColumnNames.ValorLiquido);
			}
			
			set
			{
				base.SetSystemDecimal(LiquidacaoRendaFixaMetadata.ColumnNames.ValorLiquido, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoRendaFixa.PULiquidacao
		/// </summary>
		virtual public System.Decimal? PULiquidacao
		{
			get
			{
				return base.GetSystemDecimal(LiquidacaoRendaFixaMetadata.ColumnNames.PULiquidacao);
			}
			
			set
			{
				base.SetSystemDecimal(LiquidacaoRendaFixaMetadata.ColumnNames.PULiquidacao, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoRendaFixa.RendimentoNaoTributavel
		/// </summary>
		virtual public System.Decimal? RendimentoNaoTributavel
		{
			get
			{
				return base.GetSystemDecimal(LiquidacaoRendaFixaMetadata.ColumnNames.RendimentoNaoTributavel);
			}
			
			set
			{
				base.SetSystemDecimal(LiquidacaoRendaFixaMetadata.ColumnNames.RendimentoNaoTributavel, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoRendaFixa.Status
		/// </summary>
		virtual public System.Byte? Status
		{
			get
			{
				return base.GetSystemByte(LiquidacaoRendaFixaMetadata.ColumnNames.Status);
			}
			
			set
			{
				base.SetSystemByte(LiquidacaoRendaFixaMetadata.ColumnNames.Status, value);
			}
		}
		
		/// <summary>
		/// Maps to LiquidacaoRendaFixa.IdOperacaoVenda
		/// </summary>
		virtual public System.Int32? IdOperacaoVenda
		{
			get
			{
				return base.GetSystemInt32(LiquidacaoRendaFixaMetadata.ColumnNames.IdOperacaoVenda);
			}
			
			set
			{
				base.SetSystemInt32(LiquidacaoRendaFixaMetadata.ColumnNames.IdOperacaoVenda, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		[CLSCompliant(false)]
		internal protected TituloRendaFixa _UpToTituloRendaFixaByIdTitulo;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esLiquidacaoRendaFixa entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdLiquidacao
			{
				get
				{
					System.Int32? data = entity.IdLiquidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdLiquidacao = null;
					else entity.IdLiquidacao = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoLancamento
			{
				get
				{
					System.Byte? data = entity.TipoLancamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoLancamento = null;
					else entity.TipoLancamento = Convert.ToByte(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String IdTitulo
			{
				get
				{
					System.Int32? data = entity.IdTitulo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTitulo = null;
					else entity.IdTitulo = Convert.ToInt32(value);
				}
			}
				
			public System.String DataLiquidacao
			{
				get
				{
					System.DateTime? data = entity.DataLiquidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataLiquidacao = null;
					else entity.DataLiquidacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Decimal? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String Rendimento
			{
				get
				{
					System.Decimal? data = entity.Rendimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Rendimento = null;
					else entity.Rendimento = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIR
			{
				get
				{
					System.Decimal? data = entity.ValorIR;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIR = null;
					else entity.ValorIR = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIOF
			{
				get
				{
					System.Decimal? data = entity.ValorIOF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIOF = null;
					else entity.ValorIOF = Convert.ToDecimal(value);
				}
			}
				
			public System.String IdPosicaoResgatada
			{
				get
				{
					System.Int32? data = entity.IdPosicaoResgatada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPosicaoResgatada = null;
					else entity.IdPosicaoResgatada = Convert.ToInt32(value);
				}
			}
				
			public System.String ValorBruto
			{
				get
				{
					System.Decimal? data = entity.ValorBruto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorBruto = null;
					else entity.ValorBruto = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorLiquido
			{
				get
				{
					System.Decimal? data = entity.ValorLiquido;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorLiquido = null;
					else entity.ValorLiquido = Convert.ToDecimal(value);
				}
			}
				
			public System.String PULiquidacao
			{
				get
				{
					System.Decimal? data = entity.PULiquidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PULiquidacao = null;
					else entity.PULiquidacao = Convert.ToDecimal(value);
				}
			}
				
			public System.String RendimentoNaoTributavel
			{
				get
				{
					System.Decimal? data = entity.RendimentoNaoTributavel;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.RendimentoNaoTributavel = null;
					else entity.RendimentoNaoTributavel = Convert.ToDecimal(value);
				}
			}
				
			public System.String Status
			{
				get
				{
					System.Byte? data = entity.Status;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Status = null;
					else entity.Status = Convert.ToByte(value);
				}
			}
				
			public System.String IdOperacaoVenda
			{
				get
				{
					System.Int32? data = entity.IdOperacaoVenda;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacaoVenda = null;
					else entity.IdOperacaoVenda = Convert.ToInt32(value);
				}
			}
			

			private esLiquidacaoRendaFixa entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esLiquidacaoRendaFixaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esLiquidacaoRendaFixa can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class LiquidacaoRendaFixa : esLiquidacaoRendaFixa
	{

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_LiquidacaoRendaFixa_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToTituloRendaFixaByIdTitulo - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - TituloRendaFixa_LiquidacaoRendaFixa_FK1
		/// </summary>

		[XmlIgnore]
		public TituloRendaFixa UpToTituloRendaFixaByIdTitulo
		{
			get
			{
				if(this._UpToTituloRendaFixaByIdTitulo == null
					&& IdTitulo != null					)
				{
					this._UpToTituloRendaFixaByIdTitulo = new TituloRendaFixa();
					this._UpToTituloRendaFixaByIdTitulo.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToTituloRendaFixaByIdTitulo", this._UpToTituloRendaFixaByIdTitulo);
					this._UpToTituloRendaFixaByIdTitulo.Query.Where(this._UpToTituloRendaFixaByIdTitulo.Query.IdTitulo == this.IdTitulo);
					this._UpToTituloRendaFixaByIdTitulo.Query.Load();
				}

				return this._UpToTituloRendaFixaByIdTitulo;
			}
			
			set
			{
				this.RemovePreSave("UpToTituloRendaFixaByIdTitulo");
				

				if(value == null)
				{
					this.IdTitulo = null;
					this._UpToTituloRendaFixaByIdTitulo = null;
				}
				else
				{
					this.IdTitulo = value.IdTitulo;
					this._UpToTituloRendaFixaByIdTitulo = value;
					this.SetPreSave("UpToTituloRendaFixaByIdTitulo", this._UpToTituloRendaFixaByIdTitulo);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToTituloRendaFixaByIdTitulo != null)
			{
				this.IdTitulo = this._UpToTituloRendaFixaByIdTitulo.IdTitulo;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esLiquidacaoRendaFixaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return LiquidacaoRendaFixaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdLiquidacao
		{
			get
			{
				return new esQueryItem(this, LiquidacaoRendaFixaMetadata.ColumnNames.IdLiquidacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoLancamento
		{
			get
			{
				return new esQueryItem(this, LiquidacaoRendaFixaMetadata.ColumnNames.TipoLancamento, esSystemType.Byte);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, LiquidacaoRendaFixaMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdTitulo
		{
			get
			{
				return new esQueryItem(this, LiquidacaoRendaFixaMetadata.ColumnNames.IdTitulo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataLiquidacao
		{
			get
			{
				return new esQueryItem(this, LiquidacaoRendaFixaMetadata.ColumnNames.DataLiquidacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, LiquidacaoRendaFixaMetadata.ColumnNames.Quantidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Rendimento
		{
			get
			{
				return new esQueryItem(this, LiquidacaoRendaFixaMetadata.ColumnNames.Rendimento, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIR
		{
			get
			{
				return new esQueryItem(this, LiquidacaoRendaFixaMetadata.ColumnNames.ValorIR, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIOF
		{
			get
			{
				return new esQueryItem(this, LiquidacaoRendaFixaMetadata.ColumnNames.ValorIOF, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IdPosicaoResgatada
		{
			get
			{
				return new esQueryItem(this, LiquidacaoRendaFixaMetadata.ColumnNames.IdPosicaoResgatada, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ValorBruto
		{
			get
			{
				return new esQueryItem(this, LiquidacaoRendaFixaMetadata.ColumnNames.ValorBruto, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorLiquido
		{
			get
			{
				return new esQueryItem(this, LiquidacaoRendaFixaMetadata.ColumnNames.ValorLiquido, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PULiquidacao
		{
			get
			{
				return new esQueryItem(this, LiquidacaoRendaFixaMetadata.ColumnNames.PULiquidacao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem RendimentoNaoTributavel
		{
			get
			{
				return new esQueryItem(this, LiquidacaoRendaFixaMetadata.ColumnNames.RendimentoNaoTributavel, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Status
		{
			get
			{
				return new esQueryItem(this, LiquidacaoRendaFixaMetadata.ColumnNames.Status, esSystemType.Byte);
			}
		} 
		
		public esQueryItem IdOperacaoVenda
		{
			get
			{
				return new esQueryItem(this, LiquidacaoRendaFixaMetadata.ColumnNames.IdOperacaoVenda, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("LiquidacaoRendaFixaCollection")]
	public partial class LiquidacaoRendaFixaCollection : esLiquidacaoRendaFixaCollection, IEnumerable<LiquidacaoRendaFixa>
	{
		public LiquidacaoRendaFixaCollection()
		{

		}
		
		public static implicit operator List<LiquidacaoRendaFixa>(LiquidacaoRendaFixaCollection coll)
		{
			List<LiquidacaoRendaFixa> list = new List<LiquidacaoRendaFixa>();
			
			foreach (LiquidacaoRendaFixa emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  LiquidacaoRendaFixaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new LiquidacaoRendaFixaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new LiquidacaoRendaFixa(row);
		}

		override protected esEntity CreateEntity()
		{
			return new LiquidacaoRendaFixa();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public LiquidacaoRendaFixaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new LiquidacaoRendaFixaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(LiquidacaoRendaFixaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public LiquidacaoRendaFixa AddNew()
		{
			LiquidacaoRendaFixa entity = base.AddNewEntity() as LiquidacaoRendaFixa;
			
			return entity;
		}

		public LiquidacaoRendaFixa FindByPrimaryKey(System.Int32 idLiquidacao)
		{
			return base.FindByPrimaryKey(idLiquidacao) as LiquidacaoRendaFixa;
		}


		#region IEnumerable<LiquidacaoRendaFixa> Members

		IEnumerator<LiquidacaoRendaFixa> IEnumerable<LiquidacaoRendaFixa>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as LiquidacaoRendaFixa;
			}
		}

		#endregion
		
		private LiquidacaoRendaFixaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'LiquidacaoRendaFixa' table
	/// </summary>

	[Serializable]
	public partial class LiquidacaoRendaFixa : esLiquidacaoRendaFixa
	{
		public LiquidacaoRendaFixa()
		{

		}
	
		public LiquidacaoRendaFixa(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return LiquidacaoRendaFixaMetadata.Meta();
			}
		}
		
		
		
		override protected esLiquidacaoRendaFixaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new LiquidacaoRendaFixaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public LiquidacaoRendaFixaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new LiquidacaoRendaFixaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(LiquidacaoRendaFixaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private LiquidacaoRendaFixaQuery query;
	}



	[Serializable]
	public partial class LiquidacaoRendaFixaQuery : esLiquidacaoRendaFixaQuery
	{
		public LiquidacaoRendaFixaQuery()
		{

		}		
		
		public LiquidacaoRendaFixaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class LiquidacaoRendaFixaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected LiquidacaoRendaFixaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(LiquidacaoRendaFixaMetadata.ColumnNames.IdLiquidacao, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = LiquidacaoRendaFixaMetadata.PropertyNames.IdLiquidacao;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoRendaFixaMetadata.ColumnNames.TipoLancamento, 1, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = LiquidacaoRendaFixaMetadata.PropertyNames.TipoLancamento;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoRendaFixaMetadata.ColumnNames.IdCliente, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = LiquidacaoRendaFixaMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoRendaFixaMetadata.ColumnNames.IdTitulo, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = LiquidacaoRendaFixaMetadata.PropertyNames.IdTitulo;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoRendaFixaMetadata.ColumnNames.DataLiquidacao, 4, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = LiquidacaoRendaFixaMetadata.PropertyNames.DataLiquidacao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoRendaFixaMetadata.ColumnNames.Quantidade, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = LiquidacaoRendaFixaMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoRendaFixaMetadata.ColumnNames.Rendimento, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = LiquidacaoRendaFixaMetadata.PropertyNames.Rendimento;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoRendaFixaMetadata.ColumnNames.ValorIR, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = LiquidacaoRendaFixaMetadata.PropertyNames.ValorIR;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoRendaFixaMetadata.ColumnNames.ValorIOF, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = LiquidacaoRendaFixaMetadata.PropertyNames.ValorIOF;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoRendaFixaMetadata.ColumnNames.IdPosicaoResgatada, 9, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = LiquidacaoRendaFixaMetadata.PropertyNames.IdPosicaoResgatada;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoRendaFixaMetadata.ColumnNames.ValorBruto, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = LiquidacaoRendaFixaMetadata.PropertyNames.ValorBruto;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoRendaFixaMetadata.ColumnNames.ValorLiquido, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = LiquidacaoRendaFixaMetadata.PropertyNames.ValorLiquido;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoRendaFixaMetadata.ColumnNames.PULiquidacao, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = LiquidacaoRendaFixaMetadata.PropertyNames.PULiquidacao;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoRendaFixaMetadata.ColumnNames.RendimentoNaoTributavel, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = LiquidacaoRendaFixaMetadata.PropertyNames.RendimentoNaoTributavel;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoRendaFixaMetadata.ColumnNames.Status, 14, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = LiquidacaoRendaFixaMetadata.PropertyNames.Status;	
			c.NumericPrecision = 3;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(LiquidacaoRendaFixaMetadata.ColumnNames.IdOperacaoVenda, 15, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = LiquidacaoRendaFixaMetadata.PropertyNames.IdOperacaoVenda;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public LiquidacaoRendaFixaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdLiquidacao = "IdLiquidacao";
			 public const string TipoLancamento = "TipoLancamento";
			 public const string IdCliente = "IdCliente";
			 public const string IdTitulo = "IdTitulo";
			 public const string DataLiquidacao = "DataLiquidacao";
			 public const string Quantidade = "Quantidade";
			 public const string Rendimento = "Rendimento";
			 public const string ValorIR = "ValorIR";
			 public const string ValorIOF = "ValorIOF";
			 public const string IdPosicaoResgatada = "IdPosicaoResgatada";
			 public const string ValorBruto = "ValorBruto";
			 public const string ValorLiquido = "ValorLiquido";
			 public const string PULiquidacao = "PULiquidacao";
			 public const string RendimentoNaoTributavel = "RendimentoNaoTributavel";
			 public const string Status = "Status";
			 public const string IdOperacaoVenda = "IdOperacaoVenda";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdLiquidacao = "IdLiquidacao";
			 public const string TipoLancamento = "TipoLancamento";
			 public const string IdCliente = "IdCliente";
			 public const string IdTitulo = "IdTitulo";
			 public const string DataLiquidacao = "DataLiquidacao";
			 public const string Quantidade = "Quantidade";
			 public const string Rendimento = "Rendimento";
			 public const string ValorIR = "ValorIR";
			 public const string ValorIOF = "ValorIOF";
			 public const string IdPosicaoResgatada = "IdPosicaoResgatada";
			 public const string ValorBruto = "ValorBruto";
			 public const string ValorLiquido = "ValorLiquido";
			 public const string PULiquidacao = "PULiquidacao";
			 public const string RendimentoNaoTributavel = "RendimentoNaoTributavel";
			 public const string Status = "Status";
			 public const string IdOperacaoVenda = "IdOperacaoVenda";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(LiquidacaoRendaFixaMetadata))
			{
				if(LiquidacaoRendaFixaMetadata.mapDelegates == null)
				{
					LiquidacaoRendaFixaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (LiquidacaoRendaFixaMetadata.meta == null)
				{
					LiquidacaoRendaFixaMetadata.meta = new LiquidacaoRendaFixaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdLiquidacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoLancamento", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdTitulo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataLiquidacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Quantidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Rendimento", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIR", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIOF", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IdPosicaoResgatada", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ValorBruto", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorLiquido", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PULiquidacao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("RendimentoNaoTributavel", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Status", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("IdOperacaoVenda", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "LiquidacaoRendaFixa";
				meta.Destination = "LiquidacaoRendaFixa";
				
				meta.spInsert = "proc_LiquidacaoRendaFixaInsert";				
				meta.spUpdate = "proc_LiquidacaoRendaFixaUpdate";		
				meta.spDelete = "proc_LiquidacaoRendaFixaDelete";
				meta.spLoadAll = "proc_LiquidacaoRendaFixaLoadAll";
				meta.spLoadByPrimaryKey = "proc_LiquidacaoRendaFixaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private LiquidacaoRendaFixaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
