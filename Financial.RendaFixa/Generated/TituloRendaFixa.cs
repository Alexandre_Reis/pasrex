/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 07/04/2016 17:56:10
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Common;
using Financial.Investidor;
using Financial.Fundo;



namespace Financial.RendaFixa
{

	[Serializable]
	abstract public class esTituloRendaFixaCollection : esEntityCollection
	{
		public esTituloRendaFixaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TituloRendaFixaCollection";
		}

		#region Query Logic
		protected void InitQuery(esTituloRendaFixaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTituloRendaFixaQuery);
		}
		#endregion
		
		virtual public TituloRendaFixa DetachEntity(TituloRendaFixa entity)
		{
			return base.DetachEntity(entity) as TituloRendaFixa;
		}
		
		virtual public TituloRendaFixa AttachEntity(TituloRendaFixa entity)
		{
			return base.AttachEntity(entity) as TituloRendaFixa;
		}
		
		virtual public void Combine(TituloRendaFixaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TituloRendaFixa this[int index]
		{
			get
			{
				return base[index] as TituloRendaFixa;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TituloRendaFixa);
		}
	}



	[Serializable]
	abstract public class esTituloRendaFixa : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTituloRendaFixaQuery GetDynamicQuery()
		{
			return null;
		}

		public esTituloRendaFixa()
		{

		}

		public esTituloRendaFixa(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idTitulo)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTitulo);
			else
				return LoadByPrimaryKeyStoredProcedure(idTitulo);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idTitulo)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTituloRendaFixaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdTitulo == idTitulo);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idTitulo)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTitulo);
			else
				return LoadByPrimaryKeyStoredProcedure(idTitulo);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idTitulo)
		{
			esTituloRendaFixaQuery query = this.GetDynamicQuery();
			query.Where(query.IdTitulo == idTitulo);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idTitulo)
		{
			esParameters parms = new esParameters();
			parms.Add("IdTitulo",idTitulo);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdTitulo": this.str.IdTitulo = (string)value; break;							
						case "IdPapel": this.str.IdPapel = (string)value; break;							
						case "IdIndice": this.str.IdIndice = (string)value; break;							
						case "IdEmissor": this.str.IdEmissor = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;							
						case "Taxa": this.str.Taxa = (string)value; break;							
						case "Percentual": this.str.Percentual = (string)value; break;							
						case "DataEmissao": this.str.DataEmissao = (string)value; break;							
						case "DataVencimento": this.str.DataVencimento = (string)value; break;							
						case "ValorNominal": this.str.ValorNominal = (string)value; break;							
						case "CodigoCustodia": this.str.CodigoCustodia = (string)value; break;							
						case "PUNominal": this.str.PUNominal = (string)value; break;							
						case "DescricaoCompleta": this.str.DescricaoCompleta = (string)value; break;							
						case "IsentoIR": this.str.IsentoIR = (string)value; break;							
						case "IsentoIOF": this.str.IsentoIOF = (string)value; break;							
						case "IdMoeda": this.str.IdMoeda = (string)value; break;							
						case "IdEstrategia": this.str.IdEstrategia = (string)value; break;							
						case "CodigoCDA": this.str.CodigoCDA = (string)value; break;							
						case "CodigoIsin": this.str.CodigoIsin = (string)value; break;							
						case "CodigoCetip": this.str.CodigoCetip = (string)value; break;							
						case "CodigoCBLC": this.str.CodigoCBLC = (string)value; break;							
						case "DebentureConversivel": this.str.DebentureConversivel = (string)value; break;							
						case "CodigoInterface": this.str.CodigoInterface = (string)value; break;							
						case "DebentureInfra": this.str.DebentureInfra = (string)value; break;							
						case "PremioRebate": this.str.PremioRebate = (string)value; break;							
						case "Periodicidade": this.str.Periodicidade = (string)value; break;							
						case "CriterioAmortizacao": this.str.CriterioAmortizacao = (string)value; break;							
						case "PermiteRepactuacao": this.str.PermiteRepactuacao = (string)value; break;							
						case "ExecutaProRataEmissao": this.str.ExecutaProRataEmissao = (string)value; break;							
						case "TipoProRata": this.str.TipoProRata = (string)value; break;							
						case "DefasagemLiquidacao": this.str.DefasagemLiquidacao = (string)value; break;							
						case "ProRataLiquidacao": this.str.ProRataLiquidacao = (string)value; break;							
						case "ProRataEmissaoDia": this.str.ProRataEmissaoDia = (string)value; break;							
						case "AtivoRegra": this.str.AtivoRegra = (string)value; break;							
						case "DataInicioCorrecao": this.str.DataInicioCorrecao = (string)value; break;							
						case "DefasagemMeses": this.str.DefasagemMeses = (string)value; break;							
						case "EJuros": this.str.EJuros = (string)value; break;							
						case "CodigoCusip": this.str.CodigoCusip = (string)value; break;							
						case "EJurosTipo": this.str.EJurosTipo = (string)value; break;							
						case "CodigoBDS": this.str.CodigoBDS = (string)value; break;							
						case "OpcaoEmbutida": this.str.OpcaoEmbutida = (string)value; break;							
						case "TipoOpcao": this.str.TipoOpcao = (string)value; break;							
						case "TipoExercicio": this.str.TipoExercicio = (string)value; break;							
						case "TipoPremio": this.str.TipoPremio = (string)value; break;							
						case "DataInicioExercicio": this.str.DataInicioExercicio = (string)value; break;							
						case "DataVencimentoOpcao": this.str.DataVencimentoOpcao = (string)value; break;							
						case "ValorPremio": this.str.ValorPremio = (string)value; break;							
						case "ETaxa": this.str.ETaxa = (string)value; break;							
						case "Participacao": this.str.Participacao = (string)value; break;							
						case "ParametrosAvancados": this.str.ParametrosAvancados = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdTitulo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTitulo = (System.Int32?)value;
							break;
						
						case "IdPapel":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPapel = (System.Int32?)value;
							break;
						
						case "IdIndice":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdIndice = (System.Int16?)value;
							break;
						
						case "IdEmissor":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdEmissor = (System.Int32?)value;
							break;
						
						case "Taxa":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Taxa = (System.Decimal?)value;
							break;
						
						case "Percentual":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Percentual = (System.Decimal?)value;
							break;
						
						case "DataEmissao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataEmissao = (System.DateTime?)value;
							break;
						
						case "DataVencimento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataVencimento = (System.DateTime?)value;
							break;
						
						case "ValorNominal":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorNominal = (System.Decimal?)value;
							break;
						
						case "PUNominal":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PUNominal = (System.Decimal?)value;
							break;
						
						case "IsentoIR":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.IsentoIR = (System.Byte?)value;
							break;
						
						case "IsentoIOF":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.IsentoIOF = (System.Byte?)value;
							break;
						
						case "IdMoeda":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdMoeda = (System.Int32?)value;
							break;
						
						case "IdEstrategia":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdEstrategia = (System.Int32?)value;
							break;
						
						case "CodigoCDA":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.CodigoCDA = (System.Int32?)value;
							break;
						
						case "PremioRebate":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PremioRebate = (System.Decimal?)value;
							break;
						
						case "Periodicidade":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Periodicidade = (System.Int32?)value;
							break;
						
						case "CriterioAmortizacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.CriterioAmortizacao = (System.Int32?)value;
							break;
						
						case "TipoProRata":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.TipoProRata = (System.Int32?)value;
							break;
						
						case "DefasagemLiquidacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.DefasagemLiquidacao = (System.Int32?)value;
							break;
						
						case "ProRataEmissaoDia":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.ProRataEmissaoDia = (System.Int32?)value;
							break;
						
						case "DataInicioCorrecao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataInicioCorrecao = (System.DateTime?)value;
							break;
						
						case "DefasagemMeses":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.DefasagemMeses = (System.Int32?)value;
							break;
						
						case "EJuros":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.EJuros = (System.Int32?)value;
							break;
						
						case "EJurosTipo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.EJurosTipo = (System.Int32?)value;
							break;
						
						case "CodigoBDS":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.CodigoBDS = (System.Int32?)value;
							break;
						
						case "TipoOpcao":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.TipoOpcao = (System.Int16?)value;
							break;
						
						case "TipoExercicio":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.TipoExercicio = (System.Int16?)value;
							break;
						
						case "TipoPremio":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.TipoPremio = (System.Int16?)value;
							break;
						
						case "DataInicioExercicio":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataInicioExercicio = (System.DateTime?)value;
							break;
						
						case "DataVencimentoOpcao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataVencimentoOpcao = (System.DateTime?)value;
							break;
						
						case "ValorPremio":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorPremio = (System.Decimal?)value;
							break;
						
						case "ETaxa":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.ETaxa = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TituloRendaFixa.IdTitulo
		/// </summary>
		virtual public System.Int32? IdTitulo
		{
			get
			{
				return base.GetSystemInt32(TituloRendaFixaMetadata.ColumnNames.IdTitulo);
			}
			
			set
			{
				base.SetSystemInt32(TituloRendaFixaMetadata.ColumnNames.IdTitulo, value);
			}
		}
		
		/// <summary>
		/// Maps to TituloRendaFixa.IdPapel
		/// </summary>
		virtual public System.Int32? IdPapel
		{
			get
			{
				return base.GetSystemInt32(TituloRendaFixaMetadata.ColumnNames.IdPapel);
			}
			
			set
			{
				if(base.SetSystemInt32(TituloRendaFixaMetadata.ColumnNames.IdPapel, value))
				{
					this._UpToPapelRendaFixaByIdPapel = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TituloRendaFixa.IdIndice
		/// </summary>
		virtual public System.Int16? IdIndice
		{
			get
			{
				return base.GetSystemInt16(TituloRendaFixaMetadata.ColumnNames.IdIndice);
			}
			
			set
			{
				if(base.SetSystemInt16(TituloRendaFixaMetadata.ColumnNames.IdIndice, value))
				{
					this._UpToIndiceByIdIndice = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TituloRendaFixa.IdEmissor
		/// </summary>
		virtual public System.Int32? IdEmissor
		{
			get
			{
				return base.GetSystemInt32(TituloRendaFixaMetadata.ColumnNames.IdEmissor);
			}
			
			set
			{
				if(base.SetSystemInt32(TituloRendaFixaMetadata.ColumnNames.IdEmissor, value))
				{
					this._UpToEmissorByIdEmissor = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TituloRendaFixa.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(TituloRendaFixaMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(TituloRendaFixaMetadata.ColumnNames.Descricao, value);
			}
		}
		
		/// <summary>
		/// Maps to TituloRendaFixa.Taxa
		/// </summary>
		virtual public System.Decimal? Taxa
		{
			get
			{
				return base.GetSystemDecimal(TituloRendaFixaMetadata.ColumnNames.Taxa);
			}
			
			set
			{
				base.SetSystemDecimal(TituloRendaFixaMetadata.ColumnNames.Taxa, value);
			}
		}
		
		/// <summary>
		/// Maps to TituloRendaFixa.Percentual
		/// </summary>
		virtual public System.Decimal? Percentual
		{
			get
			{
				return base.GetSystemDecimal(TituloRendaFixaMetadata.ColumnNames.Percentual);
			}
			
			set
			{
				base.SetSystemDecimal(TituloRendaFixaMetadata.ColumnNames.Percentual, value);
			}
		}
		
		/// <summary>
		/// Maps to TituloRendaFixa.DataEmissao
		/// </summary>
		virtual public System.DateTime? DataEmissao
		{
			get
			{
				return base.GetSystemDateTime(TituloRendaFixaMetadata.ColumnNames.DataEmissao);
			}
			
			set
			{
				base.SetSystemDateTime(TituloRendaFixaMetadata.ColumnNames.DataEmissao, value);
			}
		}
		
		/// <summary>
		/// Maps to TituloRendaFixa.DataVencimento
		/// </summary>
		virtual public System.DateTime? DataVencimento
		{
			get
			{
				return base.GetSystemDateTime(TituloRendaFixaMetadata.ColumnNames.DataVencimento);
			}
			
			set
			{
				base.SetSystemDateTime(TituloRendaFixaMetadata.ColumnNames.DataVencimento, value);
			}
		}
		
		/// <summary>
		/// Maps to TituloRendaFixa.ValorNominal
		/// </summary>
		virtual public System.Decimal? ValorNominal
		{
			get
			{
				return base.GetSystemDecimal(TituloRendaFixaMetadata.ColumnNames.ValorNominal);
			}
			
			set
			{
				base.SetSystemDecimal(TituloRendaFixaMetadata.ColumnNames.ValorNominal, value);
			}
		}
		
		/// <summary>
		/// Maps to TituloRendaFixa.CodigoCustodia
		/// </summary>
		virtual public System.String CodigoCustodia
		{
			get
			{
				return base.GetSystemString(TituloRendaFixaMetadata.ColumnNames.CodigoCustodia);
			}
			
			set
			{
				base.SetSystemString(TituloRendaFixaMetadata.ColumnNames.CodigoCustodia, value);
			}
		}
		
		/// <summary>
		/// Maps to TituloRendaFixa.PUNominal
		/// </summary>
		virtual public System.Decimal? PUNominal
		{
			get
			{
				return base.GetSystemDecimal(TituloRendaFixaMetadata.ColumnNames.PUNominal);
			}
			
			set
			{
				base.SetSystemDecimal(TituloRendaFixaMetadata.ColumnNames.PUNominal, value);
			}
		}
		
		/// <summary>
		/// Maps to TituloRendaFixa.DescricaoCompleta
		/// </summary>
		virtual public System.String DescricaoCompleta
		{
			get
			{
				return base.GetSystemString(TituloRendaFixaMetadata.ColumnNames.DescricaoCompleta);
			}
			
			set
			{
				base.SetSystemString(TituloRendaFixaMetadata.ColumnNames.DescricaoCompleta, value);
			}
		}
		
		/// <summary>
		/// Maps to TituloRendaFixa.IsentoIR
		/// </summary>
		virtual public System.Byte? IsentoIR
		{
			get
			{
				return base.GetSystemByte(TituloRendaFixaMetadata.ColumnNames.IsentoIR);
			}
			
			set
			{
				base.SetSystemByte(TituloRendaFixaMetadata.ColumnNames.IsentoIR, value);
			}
		}
		
		/// <summary>
		/// Maps to TituloRendaFixa.IsentoIOF
		/// </summary>
		virtual public System.Byte? IsentoIOF
		{
			get
			{
				return base.GetSystemByte(TituloRendaFixaMetadata.ColumnNames.IsentoIOF);
			}
			
			set
			{
				base.SetSystemByte(TituloRendaFixaMetadata.ColumnNames.IsentoIOF, value);
			}
		}
		
		/// <summary>
		/// Maps to TituloRendaFixa.IdMoeda
		/// </summary>
		virtual public System.Int32? IdMoeda
		{
			get
			{
				return base.GetSystemInt32(TituloRendaFixaMetadata.ColumnNames.IdMoeda);
			}
			
			set
			{
				base.SetSystemInt32(TituloRendaFixaMetadata.ColumnNames.IdMoeda, value);
			}
		}
		
		/// <summary>
		/// Maps to TituloRendaFixa.IdEstrategia
		/// </summary>
		virtual public System.Int32? IdEstrategia
		{
			get
			{
				return base.GetSystemInt32(TituloRendaFixaMetadata.ColumnNames.IdEstrategia);
			}
			
			set
			{
				if(base.SetSystemInt32(TituloRendaFixaMetadata.ColumnNames.IdEstrategia, value))
				{
					this._UpToEstrategiaByIdEstrategia = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TituloRendaFixa.CodigoCDA
		/// </summary>
		virtual public System.Int32? CodigoCDA
		{
			get
			{
				return base.GetSystemInt32(TituloRendaFixaMetadata.ColumnNames.CodigoCDA);
			}
			
			set
			{
				base.SetSystemInt32(TituloRendaFixaMetadata.ColumnNames.CodigoCDA, value);
			}
		}
		
		/// <summary>
		/// Maps to TituloRendaFixa.CodigoIsin
		/// </summary>
		virtual public System.String CodigoIsin
		{
			get
			{
				return base.GetSystemString(TituloRendaFixaMetadata.ColumnNames.CodigoIsin);
			}
			
			set
			{
				base.SetSystemString(TituloRendaFixaMetadata.ColumnNames.CodigoIsin, value);
			}
		}
		
		/// <summary>
		/// Maps to TituloRendaFixa.CodigoCetip
		/// </summary>
		virtual public System.String CodigoCetip
		{
			get
			{
				return base.GetSystemString(TituloRendaFixaMetadata.ColumnNames.CodigoCetip);
			}
			
			set
			{
				base.SetSystemString(TituloRendaFixaMetadata.ColumnNames.CodigoCetip, value);
			}
		}
		
		/// <summary>
		/// Maps to TituloRendaFixa.CodigoCBLC
		/// </summary>
		virtual public System.String CodigoCBLC
		{
			get
			{
				return base.GetSystemString(TituloRendaFixaMetadata.ColumnNames.CodigoCBLC);
			}
			
			set
			{
				base.SetSystemString(TituloRendaFixaMetadata.ColumnNames.CodigoCBLC, value);
			}
		}
		
		/// <summary>
		/// Maps to TituloRendaFixa.DebentureConversivel
		/// </summary>
		virtual public System.String DebentureConversivel
		{
			get
			{
				return base.GetSystemString(TituloRendaFixaMetadata.ColumnNames.DebentureConversivel);
			}
			
			set
			{
				base.SetSystemString(TituloRendaFixaMetadata.ColumnNames.DebentureConversivel, value);
			}
		}
		
		/// <summary>
		/// Maps to TituloRendaFixa.CodigoInterface
		/// </summary>
		virtual public System.String CodigoInterface
		{
			get
			{
				return base.GetSystemString(TituloRendaFixaMetadata.ColumnNames.CodigoInterface);
			}
			
			set
			{
				base.SetSystemString(TituloRendaFixaMetadata.ColumnNames.CodigoInterface, value);
			}
		}
		
		/// <summary>
		/// Maps to TituloRendaFixa.DebentureInfra
		/// </summary>
		virtual public System.String DebentureInfra
		{
			get
			{
				return base.GetSystemString(TituloRendaFixaMetadata.ColumnNames.DebentureInfra);
			}
			
			set
			{
				base.SetSystemString(TituloRendaFixaMetadata.ColumnNames.DebentureInfra, value);
			}
		}
		
		/// <summary>
		/// Maps to TituloRendaFixa.PremioRebate
		/// </summary>
		virtual public System.Decimal? PremioRebate
		{
			get
			{
				return base.GetSystemDecimal(TituloRendaFixaMetadata.ColumnNames.PremioRebate);
			}
			
			set
			{
				base.SetSystemDecimal(TituloRendaFixaMetadata.ColumnNames.PremioRebate, value);
			}
		}
		
		/// <summary>
		/// Maps to TituloRendaFixa.Periodicidade
		/// </summary>
		virtual public System.Int32? Periodicidade
		{
			get
			{
				return base.GetSystemInt32(TituloRendaFixaMetadata.ColumnNames.Periodicidade);
			}
			
			set
			{
				base.SetSystemInt32(TituloRendaFixaMetadata.ColumnNames.Periodicidade, value);
			}
		}
		
		/// <summary>
		/// Maps to TituloRendaFixa.CriterioAmortizacao
		/// </summary>
		virtual public System.Int32? CriterioAmortizacao
		{
			get
			{
				return base.GetSystemInt32(TituloRendaFixaMetadata.ColumnNames.CriterioAmortizacao);
			}
			
			set
			{
				base.SetSystemInt32(TituloRendaFixaMetadata.ColumnNames.CriterioAmortizacao, value);
			}
		}
		
		/// <summary>
		/// Maps to TituloRendaFixa.PermiteRepactuacao
		/// </summary>
		virtual public System.String PermiteRepactuacao
		{
			get
			{
				return base.GetSystemString(TituloRendaFixaMetadata.ColumnNames.PermiteRepactuacao);
			}
			
			set
			{
				base.SetSystemString(TituloRendaFixaMetadata.ColumnNames.PermiteRepactuacao, value);
			}
		}
		
		/// <summary>
		/// Maps to TituloRendaFixa.ExecutaProRataEmissao
		/// </summary>
		virtual public System.String ExecutaProRataEmissao
		{
			get
			{
				return base.GetSystemString(TituloRendaFixaMetadata.ColumnNames.ExecutaProRataEmissao);
			}
			
			set
			{
				base.SetSystemString(TituloRendaFixaMetadata.ColumnNames.ExecutaProRataEmissao, value);
			}
		}
		
		/// <summary>
		/// Maps to TituloRendaFixa.TipoProRata
		/// </summary>
		virtual public System.Int32? TipoProRata
		{
			get
			{
				return base.GetSystemInt32(TituloRendaFixaMetadata.ColumnNames.TipoProRata);
			}
			
			set
			{
				base.SetSystemInt32(TituloRendaFixaMetadata.ColumnNames.TipoProRata, value);
			}
		}
		
		/// <summary>
		/// Maps to TituloRendaFixa.DefasagemLiquidacao
		/// </summary>
		virtual public System.Int32? DefasagemLiquidacao
		{
			get
			{
				return base.GetSystemInt32(TituloRendaFixaMetadata.ColumnNames.DefasagemLiquidacao);
			}
			
			set
			{
				base.SetSystemInt32(TituloRendaFixaMetadata.ColumnNames.DefasagemLiquidacao, value);
			}
		}
		
		/// <summary>
		/// Maps to TituloRendaFixa.ProRataLiquidacao
		/// </summary>
		virtual public System.String ProRataLiquidacao
		{
			get
			{
				return base.GetSystemString(TituloRendaFixaMetadata.ColumnNames.ProRataLiquidacao);
			}
			
			set
			{
				base.SetSystemString(TituloRendaFixaMetadata.ColumnNames.ProRataLiquidacao, value);
			}
		}
		
		/// <summary>
		/// Maps to TituloRendaFixa.ProRataEmissaoDia
		/// </summary>
		virtual public System.Int32? ProRataEmissaoDia
		{
			get
			{
				return base.GetSystemInt32(TituloRendaFixaMetadata.ColumnNames.ProRataEmissaoDia);
			}
			
			set
			{
				base.SetSystemInt32(TituloRendaFixaMetadata.ColumnNames.ProRataEmissaoDia, value);
			}
		}
		
		/// <summary>
		/// Maps to TituloRendaFixa.AtivoRegra
		/// </summary>
		virtual public System.String AtivoRegra
		{
			get
			{
				return base.GetSystemString(TituloRendaFixaMetadata.ColumnNames.AtivoRegra);
			}
			
			set
			{
				base.SetSystemString(TituloRendaFixaMetadata.ColumnNames.AtivoRegra, value);
			}
		}
		
		/// <summary>
		/// Maps to TituloRendaFixa.DataInicioCorrecao
		/// </summary>
		virtual public System.DateTime? DataInicioCorrecao
		{
			get
			{
				return base.GetSystemDateTime(TituloRendaFixaMetadata.ColumnNames.DataInicioCorrecao);
			}
			
			set
			{
				base.SetSystemDateTime(TituloRendaFixaMetadata.ColumnNames.DataInicioCorrecao, value);
			}
		}
		
		/// <summary>
		/// Maps to TituloRendaFixa.DefasagemMeses
		/// </summary>
		virtual public System.Int32? DefasagemMeses
		{
			get
			{
				return base.GetSystemInt32(TituloRendaFixaMetadata.ColumnNames.DefasagemMeses);
			}
			
			set
			{
				base.SetSystemInt32(TituloRendaFixaMetadata.ColumnNames.DefasagemMeses, value);
			}
		}
		
		/// <summary>
		/// Maps to TituloRendaFixa.EJuros
		/// </summary>
		virtual public System.Int32? EJuros
		{
			get
			{
				return base.GetSystemInt32(TituloRendaFixaMetadata.ColumnNames.EJuros);
			}
			
			set
			{
				base.SetSystemInt32(TituloRendaFixaMetadata.ColumnNames.EJuros, value);
			}
		}
		
		/// <summary>
		/// Maps to TituloRendaFixa.CodigoCusip
		/// </summary>
		virtual public System.String CodigoCusip
		{
			get
			{
				return base.GetSystemString(TituloRendaFixaMetadata.ColumnNames.CodigoCusip);
			}
			
			set
			{
				base.SetSystemString(TituloRendaFixaMetadata.ColumnNames.CodigoCusip, value);
			}
		}
		
		/// <summary>
		/// Maps to TituloRendaFixa.EJurosTipo
		/// </summary>
		virtual public System.Int32? EJurosTipo
		{
			get
			{
				return base.GetSystemInt32(TituloRendaFixaMetadata.ColumnNames.EJurosTipo);
			}
			
			set
			{
				base.SetSystemInt32(TituloRendaFixaMetadata.ColumnNames.EJurosTipo, value);
			}
		}
		
		/// <summary>
		/// Maps to TituloRendaFixa.CodigoBDS
		/// </summary>
		virtual public System.Int32? CodigoBDS
		{
			get
			{
				return base.GetSystemInt32(TituloRendaFixaMetadata.ColumnNames.CodigoBDS);
			}
			
			set
			{
				base.SetSystemInt32(TituloRendaFixaMetadata.ColumnNames.CodigoBDS, value);
			}
		}
		
		/// <summary>
		/// Maps to TituloRendaFixa.OpcaoEmbutida
		/// </summary>
		virtual public System.String OpcaoEmbutida
		{
			get
			{
				return base.GetSystemString(TituloRendaFixaMetadata.ColumnNames.OpcaoEmbutida);
			}
			
			set
			{
				base.SetSystemString(TituloRendaFixaMetadata.ColumnNames.OpcaoEmbutida, value);
			}
		}
		
		/// <summary>
		/// Maps to TituloRendaFixa.TipoOpcao
		/// </summary>
		virtual public System.Int16? TipoOpcao
		{
			get
			{
				return base.GetSystemInt16(TituloRendaFixaMetadata.ColumnNames.TipoOpcao);
			}
			
			set
			{
				base.SetSystemInt16(TituloRendaFixaMetadata.ColumnNames.TipoOpcao, value);
			}
		}
		
		/// <summary>
		/// Maps to TituloRendaFixa.TipoExercicio
		/// </summary>
		virtual public System.Int16? TipoExercicio
		{
			get
			{
				return base.GetSystemInt16(TituloRendaFixaMetadata.ColumnNames.TipoExercicio);
			}
			
			set
			{
				base.SetSystemInt16(TituloRendaFixaMetadata.ColumnNames.TipoExercicio, value);
			}
		}
		
		/// <summary>
		/// Maps to TituloRendaFixa.TipoPremio
		/// </summary>
		virtual public System.Int16? TipoPremio
		{
			get
			{
				return base.GetSystemInt16(TituloRendaFixaMetadata.ColumnNames.TipoPremio);
			}
			
			set
			{
				base.SetSystemInt16(TituloRendaFixaMetadata.ColumnNames.TipoPremio, value);
			}
		}
		
		/// <summary>
		/// Maps to TituloRendaFixa.DataInicioExercicio
		/// </summary>
		virtual public System.DateTime? DataInicioExercicio
		{
			get
			{
				return base.GetSystemDateTime(TituloRendaFixaMetadata.ColumnNames.DataInicioExercicio);
			}
			
			set
			{
				base.SetSystemDateTime(TituloRendaFixaMetadata.ColumnNames.DataInicioExercicio, value);
			}
		}
		
		/// <summary>
		/// Maps to TituloRendaFixa.DataVencimentoOpcao
		/// </summary>
		virtual public System.DateTime? DataVencimentoOpcao
		{
			get
			{
				return base.GetSystemDateTime(TituloRendaFixaMetadata.ColumnNames.DataVencimentoOpcao);
			}
			
			set
			{
				base.SetSystemDateTime(TituloRendaFixaMetadata.ColumnNames.DataVencimentoOpcao, value);
			}
		}
		
		/// <summary>
		/// Maps to TituloRendaFixa.ValorPremio
		/// </summary>
		virtual public System.Decimal? ValorPremio
		{
			get
			{
				return base.GetSystemDecimal(TituloRendaFixaMetadata.ColumnNames.ValorPremio);
			}
			
			set
			{
				base.SetSystemDecimal(TituloRendaFixaMetadata.ColumnNames.ValorPremio, value);
			}
		}
		
		/// <summary>
		/// Maps to TituloRendaFixa.ETaxa
		/// </summary>
		virtual public System.Int32? ETaxa
		{
			get
			{
				return base.GetSystemInt32(TituloRendaFixaMetadata.ColumnNames.ETaxa);
			}
			
			set
			{
				base.SetSystemInt32(TituloRendaFixaMetadata.ColumnNames.ETaxa, value);
			}
		}
		
		/// <summary>
		/// Maps to TituloRendaFixa.Participacao
		/// </summary>
		virtual public System.String Participacao
		{
			get
			{
				return base.GetSystemString(TituloRendaFixaMetadata.ColumnNames.Participacao);
			}
			
			set
			{
				base.SetSystemString(TituloRendaFixaMetadata.ColumnNames.Participacao, value);
			}
		}
		
		/// <summary>
		/// Maps to TituloRendaFixa.ParametrosAvancados
		/// </summary>
		virtual public System.String ParametrosAvancados
		{
			get
			{
				return base.GetSystemString(TituloRendaFixaMetadata.ColumnNames.ParametrosAvancados);
			}
			
			set
			{
				base.SetSystemString(TituloRendaFixaMetadata.ColumnNames.ParametrosAvancados, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Emissor _UpToEmissorByIdEmissor;
		[CLSCompliant(false)]
		internal protected Estrategia _UpToEstrategiaByIdEstrategia;
		[CLSCompliant(false)]
		internal protected Indice _UpToIndiceByIdIndice;
		[CLSCompliant(false)]
		internal protected PapelRendaFixa _UpToPapelRendaFixaByIdPapel;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTituloRendaFixa entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdTitulo
			{
				get
				{
					System.Int32? data = entity.IdTitulo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTitulo = null;
					else entity.IdTitulo = Convert.ToInt32(value);
				}
			}
				
			public System.String IdPapel
			{
				get
				{
					System.Int32? data = entity.IdPapel;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPapel = null;
					else entity.IdPapel = Convert.ToInt32(value);
				}
			}
				
			public System.String IdIndice
			{
				get
				{
					System.Int16? data = entity.IdIndice;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdIndice = null;
					else entity.IdIndice = Convert.ToInt16(value);
				}
			}
				
			public System.String IdEmissor
			{
				get
				{
					System.Int32? data = entity.IdEmissor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdEmissor = null;
					else entity.IdEmissor = Convert.ToInt32(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
				
			public System.String Taxa
			{
				get
				{
					System.Decimal? data = entity.Taxa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Taxa = null;
					else entity.Taxa = Convert.ToDecimal(value);
				}
			}
				
			public System.String Percentual
			{
				get
				{
					System.Decimal? data = entity.Percentual;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Percentual = null;
					else entity.Percentual = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataEmissao
			{
				get
				{
					System.DateTime? data = entity.DataEmissao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataEmissao = null;
					else entity.DataEmissao = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataVencimento
			{
				get
				{
					System.DateTime? data = entity.DataVencimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataVencimento = null;
					else entity.DataVencimento = Convert.ToDateTime(value);
				}
			}
				
			public System.String ValorNominal
			{
				get
				{
					System.Decimal? data = entity.ValorNominal;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorNominal = null;
					else entity.ValorNominal = Convert.ToDecimal(value);
				}
			}
				
			public System.String CodigoCustodia
			{
				get
				{
					System.String data = entity.CodigoCustodia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoCustodia = null;
					else entity.CodigoCustodia = Convert.ToString(value);
				}
			}
				
			public System.String PUNominal
			{
				get
				{
					System.Decimal? data = entity.PUNominal;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PUNominal = null;
					else entity.PUNominal = Convert.ToDecimal(value);
				}
			}
				
			public System.String DescricaoCompleta
			{
				get
				{
					System.String data = entity.DescricaoCompleta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DescricaoCompleta = null;
					else entity.DescricaoCompleta = Convert.ToString(value);
				}
			}
				
			public System.String IsentoIR
			{
				get
				{
					System.Byte? data = entity.IsentoIR;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IsentoIR = null;
					else entity.IsentoIR = Convert.ToByte(value);
				}
			}
				
			public System.String IsentoIOF
			{
				get
				{
					System.Byte? data = entity.IsentoIOF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IsentoIOF = null;
					else entity.IsentoIOF = Convert.ToByte(value);
				}
			}
				
			public System.String IdMoeda
			{
				get
				{
					System.Int32? data = entity.IdMoeda;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdMoeda = null;
					else entity.IdMoeda = Convert.ToInt32(value);
				}
			}
				
			public System.String IdEstrategia
			{
				get
				{
					System.Int32? data = entity.IdEstrategia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdEstrategia = null;
					else entity.IdEstrategia = Convert.ToInt32(value);
				}
			}
				
			public System.String CodigoCDA
			{
				get
				{
					System.Int32? data = entity.CodigoCDA;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoCDA = null;
					else entity.CodigoCDA = Convert.ToInt32(value);
				}
			}
				
			public System.String CodigoIsin
			{
				get
				{
					System.String data = entity.CodigoIsin;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoIsin = null;
					else entity.CodigoIsin = Convert.ToString(value);
				}
			}
				
			public System.String CodigoCetip
			{
				get
				{
					System.String data = entity.CodigoCetip;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoCetip = null;
					else entity.CodigoCetip = Convert.ToString(value);
				}
			}
				
			public System.String CodigoCBLC
			{
				get
				{
					System.String data = entity.CodigoCBLC;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoCBLC = null;
					else entity.CodigoCBLC = Convert.ToString(value);
				}
			}
				
			public System.String DebentureConversivel
			{
				get
				{
					System.String data = entity.DebentureConversivel;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DebentureConversivel = null;
					else entity.DebentureConversivel = Convert.ToString(value);
				}
			}
				
			public System.String CodigoInterface
			{
				get
				{
					System.String data = entity.CodigoInterface;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoInterface = null;
					else entity.CodigoInterface = Convert.ToString(value);
				}
			}
				
			public System.String DebentureInfra
			{
				get
				{
					System.String data = entity.DebentureInfra;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DebentureInfra = null;
					else entity.DebentureInfra = Convert.ToString(value);
				}
			}
				
			public System.String PremioRebate
			{
				get
				{
					System.Decimal? data = entity.PremioRebate;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PremioRebate = null;
					else entity.PremioRebate = Convert.ToDecimal(value);
				}
			}
				
			public System.String Periodicidade
			{
				get
				{
					System.Int32? data = entity.Periodicidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Periodicidade = null;
					else entity.Periodicidade = Convert.ToInt32(value);
				}
			}
				
			public System.String CriterioAmortizacao
			{
				get
				{
					System.Int32? data = entity.CriterioAmortizacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CriterioAmortizacao = null;
					else entity.CriterioAmortizacao = Convert.ToInt32(value);
				}
			}
				
			public System.String PermiteRepactuacao
			{
				get
				{
					System.String data = entity.PermiteRepactuacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PermiteRepactuacao = null;
					else entity.PermiteRepactuacao = Convert.ToString(value);
				}
			}
				
			public System.String ExecutaProRataEmissao
			{
				get
				{
					System.String data = entity.ExecutaProRataEmissao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ExecutaProRataEmissao = null;
					else entity.ExecutaProRataEmissao = Convert.ToString(value);
				}
			}
				
			public System.String TipoProRata
			{
				get
				{
					System.Int32? data = entity.TipoProRata;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoProRata = null;
					else entity.TipoProRata = Convert.ToInt32(value);
				}
			}
				
			public System.String DefasagemLiquidacao
			{
				get
				{
					System.Int32? data = entity.DefasagemLiquidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DefasagemLiquidacao = null;
					else entity.DefasagemLiquidacao = Convert.ToInt32(value);
				}
			}
				
			public System.String ProRataLiquidacao
			{
				get
				{
					System.String data = entity.ProRataLiquidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ProRataLiquidacao = null;
					else entity.ProRataLiquidacao = Convert.ToString(value);
				}
			}
				
			public System.String ProRataEmissaoDia
			{
				get
				{
					System.Int32? data = entity.ProRataEmissaoDia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ProRataEmissaoDia = null;
					else entity.ProRataEmissaoDia = Convert.ToInt32(value);
				}
			}
				
			public System.String AtivoRegra
			{
				get
				{
					System.String data = entity.AtivoRegra;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AtivoRegra = null;
					else entity.AtivoRegra = Convert.ToString(value);
				}
			}
				
			public System.String DataInicioCorrecao
			{
				get
				{
					System.DateTime? data = entity.DataInicioCorrecao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataInicioCorrecao = null;
					else entity.DataInicioCorrecao = Convert.ToDateTime(value);
				}
			}
				
			public System.String DefasagemMeses
			{
				get
				{
					System.Int32? data = entity.DefasagemMeses;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DefasagemMeses = null;
					else entity.DefasagemMeses = Convert.ToInt32(value);
				}
			}
				
			public System.String EJuros
			{
				get
				{
					System.Int32? data = entity.EJuros;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EJuros = null;
					else entity.EJuros = Convert.ToInt32(value);
				}
			}
				
			public System.String CodigoCusip
			{
				get
				{
					System.String data = entity.CodigoCusip;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoCusip = null;
					else entity.CodigoCusip = Convert.ToString(value);
				}
			}
				
			public System.String EJurosTipo
			{
				get
				{
					System.Int32? data = entity.EJurosTipo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.EJurosTipo = null;
					else entity.EJurosTipo = Convert.ToInt32(value);
				}
			}
				
			public System.String CodigoBDS
			{
				get
				{
					System.Int32? data = entity.CodigoBDS;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoBDS = null;
					else entity.CodigoBDS = Convert.ToInt32(value);
				}
			}
				
			public System.String OpcaoEmbutida
			{
				get
				{
					System.String data = entity.OpcaoEmbutida;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.OpcaoEmbutida = null;
					else entity.OpcaoEmbutida = Convert.ToString(value);
				}
			}
				
			public System.String TipoOpcao
			{
				get
				{
					System.Int16? data = entity.TipoOpcao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoOpcao = null;
					else entity.TipoOpcao = Convert.ToInt16(value);
				}
			}
				
			public System.String TipoExercicio
			{
				get
				{
					System.Int16? data = entity.TipoExercicio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoExercicio = null;
					else entity.TipoExercicio = Convert.ToInt16(value);
				}
			}
				
			public System.String TipoPremio
			{
				get
				{
					System.Int16? data = entity.TipoPremio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoPremio = null;
					else entity.TipoPremio = Convert.ToInt16(value);
				}
			}
				
			public System.String DataInicioExercicio
			{
				get
				{
					System.DateTime? data = entity.DataInicioExercicio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataInicioExercicio = null;
					else entity.DataInicioExercicio = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataVencimentoOpcao
			{
				get
				{
					System.DateTime? data = entity.DataVencimentoOpcao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataVencimentoOpcao = null;
					else entity.DataVencimentoOpcao = Convert.ToDateTime(value);
				}
			}
				
			public System.String ValorPremio
			{
				get
				{
					System.Decimal? data = entity.ValorPremio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorPremio = null;
					else entity.ValorPremio = Convert.ToDecimal(value);
				}
			}
				
			public System.String ETaxa
			{
				get
				{
					System.Int32? data = entity.ETaxa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ETaxa = null;
					else entity.ETaxa = Convert.ToInt32(value);
				}
			}
				
			public System.String Participacao
			{
				get
				{
					System.String data = entity.Participacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Participacao = null;
					else entity.Participacao = Convert.ToString(value);
				}
			}
				
			public System.String ParametrosAvancados
			{
				get
				{
					System.String data = entity.ParametrosAvancados;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ParametrosAvancados = null;
					else entity.ParametrosAvancados = Convert.ToString(value);
				}
			}
			

			private esTituloRendaFixa entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTituloRendaFixaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTituloRendaFixa can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TituloRendaFixa : esTituloRendaFixa
	{

				
		#region AgendaRendaFixaCollectionByIdTitulo - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - TituloRendaFixa_AgendaRendaFixa_FK1
		/// </summary>

		[XmlIgnore]
		public AgendaRendaFixaCollection AgendaRendaFixaCollectionByIdTitulo
		{
			get
			{
				if(this._AgendaRendaFixaCollectionByIdTitulo == null)
				{
					this._AgendaRendaFixaCollectionByIdTitulo = new AgendaRendaFixaCollection();
					this._AgendaRendaFixaCollectionByIdTitulo.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("AgendaRendaFixaCollectionByIdTitulo", this._AgendaRendaFixaCollectionByIdTitulo);
				
					if(this.IdTitulo != null)
					{
						this._AgendaRendaFixaCollectionByIdTitulo.Query.Where(this._AgendaRendaFixaCollectionByIdTitulo.Query.IdTitulo == this.IdTitulo);
						this._AgendaRendaFixaCollectionByIdTitulo.Query.Load();

						// Auto-hookup Foreign Keys
						this._AgendaRendaFixaCollectionByIdTitulo.fks.Add(AgendaRendaFixaMetadata.ColumnNames.IdTitulo, this.IdTitulo);
					}
				}

				return this._AgendaRendaFixaCollectionByIdTitulo;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._AgendaRendaFixaCollectionByIdTitulo != null) 
				{ 
					this.RemovePostSave("AgendaRendaFixaCollectionByIdTitulo"); 
					this._AgendaRendaFixaCollectionByIdTitulo = null;
					
				} 
			} 			
		}

		private AgendaRendaFixaCollection _AgendaRendaFixaCollectionByIdTitulo;
		#endregion

				
		#region EmpresaSecuritizadaCollectionByIdTitulo - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FK__EmpresaSe__IdTit__19B5BC39
		/// </summary>

		[XmlIgnore]
		public EmpresaSecuritizadaCollection EmpresaSecuritizadaCollectionByIdTitulo
		{
			get
			{
				if(this._EmpresaSecuritizadaCollectionByIdTitulo == null)
				{
					this._EmpresaSecuritizadaCollectionByIdTitulo = new EmpresaSecuritizadaCollection();
					this._EmpresaSecuritizadaCollectionByIdTitulo.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("EmpresaSecuritizadaCollectionByIdTitulo", this._EmpresaSecuritizadaCollectionByIdTitulo);
				
					if(this.IdTitulo != null)
					{
						this._EmpresaSecuritizadaCollectionByIdTitulo.Query.Where(this._EmpresaSecuritizadaCollectionByIdTitulo.Query.IdTitulo == this.IdTitulo);
						this._EmpresaSecuritizadaCollectionByIdTitulo.Query.Load();

						// Auto-hookup Foreign Keys
						this._EmpresaSecuritizadaCollectionByIdTitulo.fks.Add(EmpresaSecuritizadaMetadata.ColumnNames.IdTitulo, this.IdTitulo);
					}
				}

				return this._EmpresaSecuritizadaCollectionByIdTitulo;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._EmpresaSecuritizadaCollectionByIdTitulo != null) 
				{ 
					this.RemovePostSave("EmpresaSecuritizadaCollectionByIdTitulo"); 
					this._EmpresaSecuritizadaCollectionByIdTitulo = null;
					
				} 
			} 			
		}

		private EmpresaSecuritizadaCollection _EmpresaSecuritizadaCollectionByIdTitulo;
		#endregion

				
		#region EventoRendaFixaCollectionByIdTitulo - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - TituloRendaFixa_EventoRendaFixa_FK1
		/// </summary>

		[XmlIgnore]
		public EventoRendaFixaCollection EventoRendaFixaCollectionByIdTitulo
		{
			get
			{
				if(this._EventoRendaFixaCollectionByIdTitulo == null)
				{
					this._EventoRendaFixaCollectionByIdTitulo = new EventoRendaFixaCollection();
					this._EventoRendaFixaCollectionByIdTitulo.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("EventoRendaFixaCollectionByIdTitulo", this._EventoRendaFixaCollectionByIdTitulo);
				
					if(this.IdTitulo != null)
					{
						this._EventoRendaFixaCollectionByIdTitulo.Query.Where(this._EventoRendaFixaCollectionByIdTitulo.Query.IdTitulo == this.IdTitulo);
						this._EventoRendaFixaCollectionByIdTitulo.Query.Load();

						// Auto-hookup Foreign Keys
						this._EventoRendaFixaCollectionByIdTitulo.fks.Add(EventoRendaFixaMetadata.ColumnNames.IdTitulo, this.IdTitulo);
					}
				}

				return this._EventoRendaFixaCollectionByIdTitulo;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._EventoRendaFixaCollectionByIdTitulo != null) 
				{ 
					this.RemovePostSave("EventoRendaFixaCollectionByIdTitulo"); 
					this._EventoRendaFixaCollectionByIdTitulo = null;
					
				} 
			} 			
		}

		private EventoRendaFixaCollection _EventoRendaFixaCollectionByIdTitulo;
		#endregion

				
		#region ExcecoesTributacaoIRCollectionByIdTituloRendaFixa - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FK__ExcecoesT__IdTit__1C9228E4
		/// </summary>

		[XmlIgnore]
		public ExcecoesTributacaoIRCollection ExcecoesTributacaoIRCollectionByIdTituloRendaFixa
		{
			get
			{
				if(this._ExcecoesTributacaoIRCollectionByIdTituloRendaFixa == null)
				{
					this._ExcecoesTributacaoIRCollectionByIdTituloRendaFixa = new ExcecoesTributacaoIRCollection();
					this._ExcecoesTributacaoIRCollectionByIdTituloRendaFixa.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("ExcecoesTributacaoIRCollectionByIdTituloRendaFixa", this._ExcecoesTributacaoIRCollectionByIdTituloRendaFixa);
				
					if(this.IdTitulo != null)
					{
						this._ExcecoesTributacaoIRCollectionByIdTituloRendaFixa.Query.Where(this._ExcecoesTributacaoIRCollectionByIdTituloRendaFixa.Query.IdTituloRendaFixa == this.IdTitulo);
						this._ExcecoesTributacaoIRCollectionByIdTituloRendaFixa.Query.Load();

						// Auto-hookup Foreign Keys
						this._ExcecoesTributacaoIRCollectionByIdTituloRendaFixa.fks.Add(ExcecoesTributacaoIRMetadata.ColumnNames.IdTituloRendaFixa, this.IdTitulo);
					}
				}

				return this._ExcecoesTributacaoIRCollectionByIdTituloRendaFixa;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._ExcecoesTributacaoIRCollectionByIdTituloRendaFixa != null) 
				{ 
					this.RemovePostSave("ExcecoesTributacaoIRCollectionByIdTituloRendaFixa"); 
					this._ExcecoesTributacaoIRCollectionByIdTituloRendaFixa = null;
					
				} 
			} 			
		}

		private ExcecoesTributacaoIRCollection _ExcecoesTributacaoIRCollectionByIdTituloRendaFixa;
		#endregion

				
		#region LiquidacaoRendaFixaCollectionByIdTitulo - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - TituloRendaFixa_LiquidacaoRendaFixa_FK1
		/// </summary>

		[XmlIgnore]
		public LiquidacaoRendaFixaCollection LiquidacaoRendaFixaCollectionByIdTitulo
		{
			get
			{
				if(this._LiquidacaoRendaFixaCollectionByIdTitulo == null)
				{
					this._LiquidacaoRendaFixaCollectionByIdTitulo = new LiquidacaoRendaFixaCollection();
					this._LiquidacaoRendaFixaCollectionByIdTitulo.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("LiquidacaoRendaFixaCollectionByIdTitulo", this._LiquidacaoRendaFixaCollectionByIdTitulo);
				
					if(this.IdTitulo != null)
					{
						this._LiquidacaoRendaFixaCollectionByIdTitulo.Query.Where(this._LiquidacaoRendaFixaCollectionByIdTitulo.Query.IdTitulo == this.IdTitulo);
						this._LiquidacaoRendaFixaCollectionByIdTitulo.Query.Load();

						// Auto-hookup Foreign Keys
						this._LiquidacaoRendaFixaCollectionByIdTitulo.fks.Add(LiquidacaoRendaFixaMetadata.ColumnNames.IdTitulo, this.IdTitulo);
					}
				}

				return this._LiquidacaoRendaFixaCollectionByIdTitulo;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._LiquidacaoRendaFixaCollectionByIdTitulo != null) 
				{ 
					this.RemovePostSave("LiquidacaoRendaFixaCollectionByIdTitulo"); 
					this._LiquidacaoRendaFixaCollectionByIdTitulo = null;
					
				} 
			} 			
		}

		private LiquidacaoRendaFixaCollection _LiquidacaoRendaFixaCollectionByIdTitulo;
		#endregion

				
		#region OperacaoRendaFixaCollectionByIdTitulo - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - TituloRendaFixa_OperacaoRendaFixa_FK1
		/// </summary>

		[XmlIgnore]
		public OperacaoRendaFixaCollection OperacaoRendaFixaCollectionByIdTitulo
		{
			get
			{
				if(this._OperacaoRendaFixaCollectionByIdTitulo == null)
				{
					this._OperacaoRendaFixaCollectionByIdTitulo = new OperacaoRendaFixaCollection();
					this._OperacaoRendaFixaCollectionByIdTitulo.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("OperacaoRendaFixaCollectionByIdTitulo", this._OperacaoRendaFixaCollectionByIdTitulo);
				
					if(this.IdTitulo != null)
					{
						this._OperacaoRendaFixaCollectionByIdTitulo.Query.Where(this._OperacaoRendaFixaCollectionByIdTitulo.Query.IdTitulo == this.IdTitulo);
						this._OperacaoRendaFixaCollectionByIdTitulo.Query.Load();

						// Auto-hookup Foreign Keys
						this._OperacaoRendaFixaCollectionByIdTitulo.fks.Add(OperacaoRendaFixaMetadata.ColumnNames.IdTitulo, this.IdTitulo);
					}
				}

				return this._OperacaoRendaFixaCollectionByIdTitulo;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._OperacaoRendaFixaCollectionByIdTitulo != null) 
				{ 
					this.RemovePostSave("OperacaoRendaFixaCollectionByIdTitulo"); 
					this._OperacaoRendaFixaCollectionByIdTitulo = null;
					
				} 
			} 			
		}

		private OperacaoRendaFixaCollection _OperacaoRendaFixaCollectionByIdTitulo;
		#endregion

				
		#region OrdemRendaFixaCollectionByIdTitulo - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - TituloRendaFixa_OrdemRendaFixa_FK1
		/// </summary>

		[XmlIgnore]
		public OrdemRendaFixaCollection OrdemRendaFixaCollectionByIdTitulo
		{
			get
			{
				if(this._OrdemRendaFixaCollectionByIdTitulo == null)
				{
					this._OrdemRendaFixaCollectionByIdTitulo = new OrdemRendaFixaCollection();
					this._OrdemRendaFixaCollectionByIdTitulo.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("OrdemRendaFixaCollectionByIdTitulo", this._OrdemRendaFixaCollectionByIdTitulo);
				
					if(this.IdTitulo != null)
					{
						this._OrdemRendaFixaCollectionByIdTitulo.Query.Where(this._OrdemRendaFixaCollectionByIdTitulo.Query.IdTitulo == this.IdTitulo);
						this._OrdemRendaFixaCollectionByIdTitulo.Query.Load();

						// Auto-hookup Foreign Keys
						this._OrdemRendaFixaCollectionByIdTitulo.fks.Add(OrdemRendaFixaMetadata.ColumnNames.IdTitulo, this.IdTitulo);
					}
				}

				return this._OrdemRendaFixaCollectionByIdTitulo;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._OrdemRendaFixaCollectionByIdTitulo != null) 
				{ 
					this.RemovePostSave("OrdemRendaFixaCollectionByIdTitulo"); 
					this._OrdemRendaFixaCollectionByIdTitulo = null;
					
				} 
			} 			
		}

		private OrdemRendaFixaCollection _OrdemRendaFixaCollectionByIdTitulo;
		#endregion

				
		#region PerfilMTMCollectionByIdTitulo - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - PerfilMTM_Titulo_FK
		/// </summary>

		[XmlIgnore]
		public PerfilMTMCollection PerfilMTMCollectionByIdTitulo
		{
			get
			{
				if(this._PerfilMTMCollectionByIdTitulo == null)
				{
					this._PerfilMTMCollectionByIdTitulo = new PerfilMTMCollection();
					this._PerfilMTMCollectionByIdTitulo.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PerfilMTMCollectionByIdTitulo", this._PerfilMTMCollectionByIdTitulo);
				
					if(this.IdTitulo != null)
					{
						this._PerfilMTMCollectionByIdTitulo.Query.Where(this._PerfilMTMCollectionByIdTitulo.Query.IdTitulo == this.IdTitulo);
						this._PerfilMTMCollectionByIdTitulo.Query.Load();

						// Auto-hookup Foreign Keys
						this._PerfilMTMCollectionByIdTitulo.fks.Add(PerfilMTMMetadata.ColumnNames.IdTitulo, this.IdTitulo);
					}
				}

				return this._PerfilMTMCollectionByIdTitulo;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PerfilMTMCollectionByIdTitulo != null) 
				{ 
					this.RemovePostSave("PerfilMTMCollectionByIdTitulo"); 
					this._PerfilMTMCollectionByIdTitulo = null;
					
				} 
			} 			
		}

		private PerfilMTMCollection _PerfilMTMCollectionByIdTitulo;
		#endregion

				
		#region PosicaoRendaFixaCollectionByIdTitulo - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - TituloRendaFixa_PosicaoRendaFixa_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoRendaFixaCollection PosicaoRendaFixaCollectionByIdTitulo
		{
			get
			{
				if(this._PosicaoRendaFixaCollectionByIdTitulo == null)
				{
					this._PosicaoRendaFixaCollectionByIdTitulo = new PosicaoRendaFixaCollection();
					this._PosicaoRendaFixaCollectionByIdTitulo.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoRendaFixaCollectionByIdTitulo", this._PosicaoRendaFixaCollectionByIdTitulo);
				
					if(this.IdTitulo != null)
					{
						this._PosicaoRendaFixaCollectionByIdTitulo.Query.Where(this._PosicaoRendaFixaCollectionByIdTitulo.Query.IdTitulo == this.IdTitulo);
						this._PosicaoRendaFixaCollectionByIdTitulo.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoRendaFixaCollectionByIdTitulo.fks.Add(PosicaoRendaFixaMetadata.ColumnNames.IdTitulo, this.IdTitulo);
					}
				}

				return this._PosicaoRendaFixaCollectionByIdTitulo;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoRendaFixaCollectionByIdTitulo != null) 
				{ 
					this.RemovePostSave("PosicaoRendaFixaCollectionByIdTitulo"); 
					this._PosicaoRendaFixaCollectionByIdTitulo = null;
					
				} 
			} 			
		}

		private PosicaoRendaFixaCollection _PosicaoRendaFixaCollectionByIdTitulo;
		#endregion

				
		#region PosicaoRendaFixaAberturaCollectionByIdTitulo - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - TituloRendaFixa_PosicaoRendaFixaAbertura_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoRendaFixaAberturaCollection PosicaoRendaFixaAberturaCollectionByIdTitulo
		{
			get
			{
				if(this._PosicaoRendaFixaAberturaCollectionByIdTitulo == null)
				{
					this._PosicaoRendaFixaAberturaCollectionByIdTitulo = new PosicaoRendaFixaAberturaCollection();
					this._PosicaoRendaFixaAberturaCollectionByIdTitulo.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoRendaFixaAberturaCollectionByIdTitulo", this._PosicaoRendaFixaAberturaCollectionByIdTitulo);
				
					if(this.IdTitulo != null)
					{
						this._PosicaoRendaFixaAberturaCollectionByIdTitulo.Query.Where(this._PosicaoRendaFixaAberturaCollectionByIdTitulo.Query.IdTitulo == this.IdTitulo);
						this._PosicaoRendaFixaAberturaCollectionByIdTitulo.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoRendaFixaAberturaCollectionByIdTitulo.fks.Add(PosicaoRendaFixaAberturaMetadata.ColumnNames.IdTitulo, this.IdTitulo);
					}
				}

				return this._PosicaoRendaFixaAberturaCollectionByIdTitulo;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoRendaFixaAberturaCollectionByIdTitulo != null) 
				{ 
					this.RemovePostSave("PosicaoRendaFixaAberturaCollectionByIdTitulo"); 
					this._PosicaoRendaFixaAberturaCollectionByIdTitulo = null;
					
				} 
			} 			
		}

		private PosicaoRendaFixaAberturaCollection _PosicaoRendaFixaAberturaCollectionByIdTitulo;
		#endregion

				
		#region PosicaoRendaFixaDetalheCollectionByIdTitulo - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - TituloRendaFixa_PosicaoRendaFixaDetalhe_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoRendaFixaDetalheCollection PosicaoRendaFixaDetalheCollectionByIdTitulo
		{
			get
			{
				if(this._PosicaoRendaFixaDetalheCollectionByIdTitulo == null)
				{
					this._PosicaoRendaFixaDetalheCollectionByIdTitulo = new PosicaoRendaFixaDetalheCollection();
					this._PosicaoRendaFixaDetalheCollectionByIdTitulo.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoRendaFixaDetalheCollectionByIdTitulo", this._PosicaoRendaFixaDetalheCollectionByIdTitulo);
				
					if(this.IdTitulo != null)
					{
						this._PosicaoRendaFixaDetalheCollectionByIdTitulo.Query.Where(this._PosicaoRendaFixaDetalheCollectionByIdTitulo.Query.IdTitulo == this.IdTitulo);
						this._PosicaoRendaFixaDetalheCollectionByIdTitulo.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoRendaFixaDetalheCollectionByIdTitulo.fks.Add(PosicaoRendaFixaDetalheMetadata.ColumnNames.IdTitulo, this.IdTitulo);
					}
				}

				return this._PosicaoRendaFixaDetalheCollectionByIdTitulo;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoRendaFixaDetalheCollectionByIdTitulo != null) 
				{ 
					this.RemovePostSave("PosicaoRendaFixaDetalheCollectionByIdTitulo"); 
					this._PosicaoRendaFixaDetalheCollectionByIdTitulo = null;
					
				} 
			} 			
		}

		private PosicaoRendaFixaDetalheCollection _PosicaoRendaFixaDetalheCollectionByIdTitulo;
		#endregion

				
		#region PosicaoRendaFixaHistoricoCollectionByIdTitulo - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - TituloRendaFixa_PosicaoRendaFixaHistorico_FK1
		/// </summary>

		[XmlIgnore]
		public PosicaoRendaFixaHistoricoCollection PosicaoRendaFixaHistoricoCollectionByIdTitulo
		{
			get
			{
				if(this._PosicaoRendaFixaHistoricoCollectionByIdTitulo == null)
				{
					this._PosicaoRendaFixaHistoricoCollectionByIdTitulo = new PosicaoRendaFixaHistoricoCollection();
					this._PosicaoRendaFixaHistoricoCollectionByIdTitulo.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PosicaoRendaFixaHistoricoCollectionByIdTitulo", this._PosicaoRendaFixaHistoricoCollectionByIdTitulo);
				
					if(this.IdTitulo != null)
					{
						this._PosicaoRendaFixaHistoricoCollectionByIdTitulo.Query.Where(this._PosicaoRendaFixaHistoricoCollectionByIdTitulo.Query.IdTitulo == this.IdTitulo);
						this._PosicaoRendaFixaHistoricoCollectionByIdTitulo.Query.Load();

						// Auto-hookup Foreign Keys
						this._PosicaoRendaFixaHistoricoCollectionByIdTitulo.fks.Add(PosicaoRendaFixaHistoricoMetadata.ColumnNames.IdTitulo, this.IdTitulo);
					}
				}

				return this._PosicaoRendaFixaHistoricoCollectionByIdTitulo;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PosicaoRendaFixaHistoricoCollectionByIdTitulo != null) 
				{ 
					this.RemovePostSave("PosicaoRendaFixaHistoricoCollectionByIdTitulo"); 
					this._PosicaoRendaFixaHistoricoCollectionByIdTitulo = null;
					
				} 
			} 			
		}

		private PosicaoRendaFixaHistoricoCollection _PosicaoRendaFixaHistoricoCollectionByIdTitulo;
		#endregion

				
		#region RepactuacaoRendaFixaCollectionByIdTituloOriginal - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Repac_PapelOrig_FK
		/// </summary>

		[XmlIgnore]
		public RepactuacaoRendaFixaCollection RepactuacaoRendaFixaCollectionByIdTituloOriginal
		{
			get
			{
				if(this._RepactuacaoRendaFixaCollectionByIdTituloOriginal == null)
				{
					this._RepactuacaoRendaFixaCollectionByIdTituloOriginal = new RepactuacaoRendaFixaCollection();
					this._RepactuacaoRendaFixaCollectionByIdTituloOriginal.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("RepactuacaoRendaFixaCollectionByIdTituloOriginal", this._RepactuacaoRendaFixaCollectionByIdTituloOriginal);
				
					if(this.IdTitulo != null)
					{
						this._RepactuacaoRendaFixaCollectionByIdTituloOriginal.Query.Where(this._RepactuacaoRendaFixaCollectionByIdTituloOriginal.Query.IdTituloOriginal == this.IdTitulo);
						this._RepactuacaoRendaFixaCollectionByIdTituloOriginal.Query.Load();

						// Auto-hookup Foreign Keys
						this._RepactuacaoRendaFixaCollectionByIdTituloOriginal.fks.Add(RepactuacaoRendaFixaMetadata.ColumnNames.IdTituloOriginal, this.IdTitulo);
					}
				}

				return this._RepactuacaoRendaFixaCollectionByIdTituloOriginal;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._RepactuacaoRendaFixaCollectionByIdTituloOriginal != null) 
				{ 
					this.RemovePostSave("RepactuacaoRendaFixaCollectionByIdTituloOriginal"); 
					this._RepactuacaoRendaFixaCollectionByIdTituloOriginal = null;
					
				} 
			} 			
		}

		private RepactuacaoRendaFixaCollection _RepactuacaoRendaFixaCollectionByIdTituloOriginal;
		#endregion

				
		#region RepactuacaoRendaFixaCollectionByIdTituloNovo - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Repac_PapelNovo_FK
		/// </summary>

		[XmlIgnore]
		public RepactuacaoRendaFixaCollection RepactuacaoRendaFixaCollectionByIdTituloNovo
		{
			get
			{
				if(this._RepactuacaoRendaFixaCollectionByIdTituloNovo == null)
				{
					this._RepactuacaoRendaFixaCollectionByIdTituloNovo = new RepactuacaoRendaFixaCollection();
					this._RepactuacaoRendaFixaCollectionByIdTituloNovo.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("RepactuacaoRendaFixaCollectionByIdTituloNovo", this._RepactuacaoRendaFixaCollectionByIdTituloNovo);
				
					if(this.IdTitulo != null)
					{
						this._RepactuacaoRendaFixaCollectionByIdTituloNovo.Query.Where(this._RepactuacaoRendaFixaCollectionByIdTituloNovo.Query.IdTituloNovo == this.IdTitulo);
						this._RepactuacaoRendaFixaCollectionByIdTituloNovo.Query.Load();

						// Auto-hookup Foreign Keys
						this._RepactuacaoRendaFixaCollectionByIdTituloNovo.fks.Add(RepactuacaoRendaFixaMetadata.ColumnNames.IdTituloNovo, this.IdTitulo);
					}
				}

				return this._RepactuacaoRendaFixaCollectionByIdTituloNovo;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._RepactuacaoRendaFixaCollectionByIdTituloNovo != null) 
				{ 
					this.RemovePostSave("RepactuacaoRendaFixaCollectionByIdTituloNovo"); 
					this._RepactuacaoRendaFixaCollectionByIdTituloNovo = null;
					
				} 
			} 			
		}

		private RepactuacaoRendaFixaCollection _RepactuacaoRendaFixaCollectionByIdTituloNovo;
		#endregion

				
		#region TabelaEscalonamentoRFCollectionByIdTitulo - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FK_TituloRendaFixa_TabelaEscalonamentoRF_1
		/// </summary>

		[XmlIgnore]
		public TabelaEscalonamentoRFCollection TabelaEscalonamentoRFCollectionByIdTitulo
		{
			get
			{
				if(this._TabelaEscalonamentoRFCollectionByIdTitulo == null)
				{
					this._TabelaEscalonamentoRFCollectionByIdTitulo = new TabelaEscalonamentoRFCollection();
					this._TabelaEscalonamentoRFCollectionByIdTitulo.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TabelaEscalonamentoRFCollectionByIdTitulo", this._TabelaEscalonamentoRFCollectionByIdTitulo);
				
					if(this.IdTitulo != null)
					{
						this._TabelaEscalonamentoRFCollectionByIdTitulo.Query.Where(this._TabelaEscalonamentoRFCollectionByIdTitulo.Query.IdTitulo == this.IdTitulo);
						this._TabelaEscalonamentoRFCollectionByIdTitulo.Query.Load();

						// Auto-hookup Foreign Keys
						this._TabelaEscalonamentoRFCollectionByIdTitulo.fks.Add(TabelaEscalonamentoRFMetadata.ColumnNames.IdTitulo, this.IdTitulo);
					}
				}

				return this._TabelaEscalonamentoRFCollectionByIdTitulo;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TabelaEscalonamentoRFCollectionByIdTitulo != null) 
				{ 
					this.RemovePostSave("TabelaEscalonamentoRFCollectionByIdTitulo"); 
					this._TabelaEscalonamentoRFCollectionByIdTitulo = null;
					
				} 
			} 			
		}

		private TabelaEscalonamentoRFCollection _TabelaEscalonamentoRFCollectionByIdTitulo;
		#endregion

				
		#region UpToEmissorByIdEmissor - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Emissor_TituloRendaFixa_FK1
		/// </summary>

		[XmlIgnore]
		public Emissor UpToEmissorByIdEmissor
		{
			get
			{
				if(this._UpToEmissorByIdEmissor == null
					&& IdEmissor != null					)
				{
					this._UpToEmissorByIdEmissor = new Emissor();
					this._UpToEmissorByIdEmissor.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToEmissorByIdEmissor", this._UpToEmissorByIdEmissor);
					this._UpToEmissorByIdEmissor.Query.Where(this._UpToEmissorByIdEmissor.Query.IdEmissor == this.IdEmissor);
					this._UpToEmissorByIdEmissor.Query.Load();
				}

				return this._UpToEmissorByIdEmissor;
			}
			
			set
			{
				this.RemovePreSave("UpToEmissorByIdEmissor");
				

				if(value == null)
				{
					this.IdEmissor = null;
					this._UpToEmissorByIdEmissor = null;
				}
				else
				{
					this.IdEmissor = value.IdEmissor;
					this._UpToEmissorByIdEmissor = value;
					this.SetPreSave("UpToEmissorByIdEmissor", this._UpToEmissorByIdEmissor);
				}
				
			}
		}
		#endregion
		

				
		#region UpToEstrategiaByIdEstrategia - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Estrategia_TituloRendaFixa_FK1
		/// </summary>

		[XmlIgnore]
		public Estrategia UpToEstrategiaByIdEstrategia
		{
			get
			{
				if(this._UpToEstrategiaByIdEstrategia == null
					&& IdEstrategia != null					)
				{
					this._UpToEstrategiaByIdEstrategia = new Estrategia();
					this._UpToEstrategiaByIdEstrategia.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToEstrategiaByIdEstrategia", this._UpToEstrategiaByIdEstrategia);
					this._UpToEstrategiaByIdEstrategia.Query.Where(this._UpToEstrategiaByIdEstrategia.Query.IdEstrategia == this.IdEstrategia);
					this._UpToEstrategiaByIdEstrategia.Query.Load();
				}

				return this._UpToEstrategiaByIdEstrategia;
			}
			
			set
			{
				this.RemovePreSave("UpToEstrategiaByIdEstrategia");
				

				if(value == null)
				{
					this.IdEstrategia = null;
					this._UpToEstrategiaByIdEstrategia = null;
				}
				else
				{
					this.IdEstrategia = value.IdEstrategia;
					this._UpToEstrategiaByIdEstrategia = value;
					this.SetPreSave("UpToEstrategiaByIdEstrategia", this._UpToEstrategiaByIdEstrategia);
				}
				
			}
		}
		#endregion
		

				
		#region UpToIndiceByIdIndice - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Indice_TituloRendaFixa_FK1
		/// </summary>

		[XmlIgnore]
		public Indice UpToIndiceByIdIndice
		{
			get
			{
				if(this._UpToIndiceByIdIndice == null
					&& IdIndice != null					)
				{
					this._UpToIndiceByIdIndice = new Indice();
					this._UpToIndiceByIdIndice.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToIndiceByIdIndice", this._UpToIndiceByIdIndice);
					this._UpToIndiceByIdIndice.Query.Where(this._UpToIndiceByIdIndice.Query.IdIndice == this.IdIndice);
					this._UpToIndiceByIdIndice.Query.Load();
				}

				return this._UpToIndiceByIdIndice;
			}
			
			set
			{
				this.RemovePreSave("UpToIndiceByIdIndice");
				

				if(value == null)
				{
					this.IdIndice = null;
					this._UpToIndiceByIdIndice = null;
				}
				else
				{
					this.IdIndice = value.IdIndice;
					this._UpToIndiceByIdIndice = value;
					this.SetPreSave("UpToIndiceByIdIndice", this._UpToIndiceByIdIndice);
				}
				
			}
		}
		#endregion
		

				
		#region UpToPapelRendaFixaByIdPapel - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - PapelRendaFixa_TituloRendaFixa_FK1
		/// </summary>

		[XmlIgnore]
		public PapelRendaFixa UpToPapelRendaFixaByIdPapel
		{
			get
			{
				if(this._UpToPapelRendaFixaByIdPapel == null
					&& IdPapel != null					)
				{
					this._UpToPapelRendaFixaByIdPapel = new PapelRendaFixa();
					this._UpToPapelRendaFixaByIdPapel.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToPapelRendaFixaByIdPapel", this._UpToPapelRendaFixaByIdPapel);
					this._UpToPapelRendaFixaByIdPapel.Query.Where(this._UpToPapelRendaFixaByIdPapel.Query.IdPapel == this.IdPapel);
					this._UpToPapelRendaFixaByIdPapel.Query.Load();
				}

				return this._UpToPapelRendaFixaByIdPapel;
			}
			
			set
			{
				this.RemovePreSave("UpToPapelRendaFixaByIdPapel");
				

				if(value == null)
				{
					this.IdPapel = null;
					this._UpToPapelRendaFixaByIdPapel = null;
				}
				else
				{
					this.IdPapel = value.IdPapel;
					this._UpToPapelRendaFixaByIdPapel = value;
					this.SetPreSave("UpToPapelRendaFixaByIdPapel", this._UpToPapelRendaFixaByIdPapel);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "AgendaRendaFixaCollectionByIdTitulo", typeof(AgendaRendaFixaCollection), new AgendaRendaFixa()));
			props.Add(new esPropertyDescriptor(this, "EmpresaSecuritizadaCollectionByIdTitulo", typeof(EmpresaSecuritizadaCollection), new EmpresaSecuritizada()));
			props.Add(new esPropertyDescriptor(this, "EventoRendaFixaCollectionByIdTitulo", typeof(EventoRendaFixaCollection), new EventoRendaFixa()));
			props.Add(new esPropertyDescriptor(this, "ExcecoesTributacaoIRCollectionByIdTituloRendaFixa", typeof(ExcecoesTributacaoIRCollection), new ExcecoesTributacaoIR()));
			props.Add(new esPropertyDescriptor(this, "LiquidacaoRendaFixaCollectionByIdTitulo", typeof(LiquidacaoRendaFixaCollection), new LiquidacaoRendaFixa()));
			props.Add(new esPropertyDescriptor(this, "OperacaoRendaFixaCollectionByIdTitulo", typeof(OperacaoRendaFixaCollection), new OperacaoRendaFixa()));
			props.Add(new esPropertyDescriptor(this, "OrdemRendaFixaCollectionByIdTitulo", typeof(OrdemRendaFixaCollection), new OrdemRendaFixa()));
			props.Add(new esPropertyDescriptor(this, "PerfilMTMCollectionByIdTitulo", typeof(PerfilMTMCollection), new PerfilMTM()));
			props.Add(new esPropertyDescriptor(this, "PosicaoRendaFixaCollectionByIdTitulo", typeof(PosicaoRendaFixaCollection), new PosicaoRendaFixa()));
			props.Add(new esPropertyDescriptor(this, "PosicaoRendaFixaAberturaCollectionByIdTitulo", typeof(PosicaoRendaFixaAberturaCollection), new PosicaoRendaFixaAbertura()));
			props.Add(new esPropertyDescriptor(this, "PosicaoRendaFixaDetalheCollectionByIdTitulo", typeof(PosicaoRendaFixaDetalheCollection), new PosicaoRendaFixaDetalhe()));
			props.Add(new esPropertyDescriptor(this, "PosicaoRendaFixaHistoricoCollectionByIdTitulo", typeof(PosicaoRendaFixaHistoricoCollection), new PosicaoRendaFixaHistorico()));
			props.Add(new esPropertyDescriptor(this, "RepactuacaoRendaFixaCollectionByIdTituloOriginal", typeof(RepactuacaoRendaFixaCollection), new RepactuacaoRendaFixa()));
			props.Add(new esPropertyDescriptor(this, "RepactuacaoRendaFixaCollectionByIdTituloNovo", typeof(RepactuacaoRendaFixaCollection), new RepactuacaoRendaFixa()));
			props.Add(new esPropertyDescriptor(this, "TabelaEscalonamentoRFCollectionByIdTitulo", typeof(TabelaEscalonamentoRFCollection), new TabelaEscalonamentoRF()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToEmissorByIdEmissor != null)
			{
				this.IdEmissor = this._UpToEmissorByIdEmissor.IdEmissor;
			}
			if(!this.es.IsDeleted && this._UpToEstrategiaByIdEstrategia != null)
			{
				this.IdEstrategia = this._UpToEstrategiaByIdEstrategia.IdEstrategia;
			}
			if(!this.es.IsDeleted && this._UpToPapelRendaFixaByIdPapel != null)
			{
				this.IdPapel = this._UpToPapelRendaFixaByIdPapel.IdPapel;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._AgendaRendaFixaCollectionByIdTitulo != null)
			{
				foreach(AgendaRendaFixa obj in this._AgendaRendaFixaCollectionByIdTitulo)
				{
					if(obj.es.IsAdded)
					{
						obj.IdTitulo = this.IdTitulo;
					}
				}
			}
			if(this._EmpresaSecuritizadaCollectionByIdTitulo != null)
			{
				foreach(EmpresaSecuritizada obj in this._EmpresaSecuritizadaCollectionByIdTitulo)
				{
					if(obj.es.IsAdded)
					{
						obj.IdTitulo = this.IdTitulo;
					}
				}
			}
			if(this._EventoRendaFixaCollectionByIdTitulo != null)
			{
				foreach(EventoRendaFixa obj in this._EventoRendaFixaCollectionByIdTitulo)
				{
					if(obj.es.IsAdded)
					{
						obj.IdTitulo = this.IdTitulo;
					}
				}
			}
			if(this._ExcecoesTributacaoIRCollectionByIdTituloRendaFixa != null)
			{
				foreach(ExcecoesTributacaoIR obj in this._ExcecoesTributacaoIRCollectionByIdTituloRendaFixa)
				{
					if(obj.es.IsAdded)
					{
						obj.IdTituloRendaFixa = this.IdTitulo;
					}
				}
			}
			if(this._LiquidacaoRendaFixaCollectionByIdTitulo != null)
			{
				foreach(LiquidacaoRendaFixa obj in this._LiquidacaoRendaFixaCollectionByIdTitulo)
				{
					if(obj.es.IsAdded)
					{
						obj.IdTitulo = this.IdTitulo;
					}
				}
			}
			if(this._OperacaoRendaFixaCollectionByIdTitulo != null)
			{
				foreach(OperacaoRendaFixa obj in this._OperacaoRendaFixaCollectionByIdTitulo)
				{
					if(obj.es.IsAdded)
					{
						obj.IdTitulo = this.IdTitulo;
					}
				}
			}
			if(this._OrdemRendaFixaCollectionByIdTitulo != null)
			{
				foreach(OrdemRendaFixa obj in this._OrdemRendaFixaCollectionByIdTitulo)
				{
					if(obj.es.IsAdded)
					{
						obj.IdTitulo = this.IdTitulo;
					}
				}
			}
			if(this._PerfilMTMCollectionByIdTitulo != null)
			{
				foreach(PerfilMTM obj in this._PerfilMTMCollectionByIdTitulo)
				{
					if(obj.es.IsAdded)
					{
						obj.IdTitulo = this.IdTitulo;
					}
				}
			}
			if(this._PosicaoRendaFixaCollectionByIdTitulo != null)
			{
				foreach(PosicaoRendaFixa obj in this._PosicaoRendaFixaCollectionByIdTitulo)
				{
					if(obj.es.IsAdded)
					{
						obj.IdTitulo = this.IdTitulo;
					}
				}
			}
			if(this._PosicaoRendaFixaAberturaCollectionByIdTitulo != null)
			{
				foreach(PosicaoRendaFixaAbertura obj in this._PosicaoRendaFixaAberturaCollectionByIdTitulo)
				{
					if(obj.es.IsAdded)
					{
						obj.IdTitulo = this.IdTitulo;
					}
				}
			}
			if(this._PosicaoRendaFixaDetalheCollectionByIdTitulo != null)
			{
				foreach(PosicaoRendaFixaDetalhe obj in this._PosicaoRendaFixaDetalheCollectionByIdTitulo)
				{
					if(obj.es.IsAdded)
					{
						obj.IdTitulo = this.IdTitulo;
					}
				}
			}
			if(this._PosicaoRendaFixaHistoricoCollectionByIdTitulo != null)
			{
				foreach(PosicaoRendaFixaHistorico obj in this._PosicaoRendaFixaHistoricoCollectionByIdTitulo)
				{
					if(obj.es.IsAdded)
					{
						obj.IdTitulo = this.IdTitulo;
					}
				}
			}
			if(this._RepactuacaoRendaFixaCollectionByIdTituloOriginal != null)
			{
				foreach(RepactuacaoRendaFixa obj in this._RepactuacaoRendaFixaCollectionByIdTituloOriginal)
				{
					if(obj.es.IsAdded)
					{
						obj.IdTituloOriginal = this.IdTitulo;
					}
				}
			}
			if(this._RepactuacaoRendaFixaCollectionByIdTituloNovo != null)
			{
				foreach(RepactuacaoRendaFixa obj in this._RepactuacaoRendaFixaCollectionByIdTituloNovo)
				{
					if(obj.es.IsAdded)
					{
						obj.IdTituloNovo = this.IdTitulo;
					}
				}
			}
			if(this._TabelaEscalonamentoRFCollectionByIdTitulo != null)
			{
				foreach(TabelaEscalonamentoRF obj in this._TabelaEscalonamentoRFCollectionByIdTitulo)
				{
					if(obj.es.IsAdded)
					{
						obj.IdTitulo = this.IdTitulo;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTituloRendaFixaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TituloRendaFixaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdTitulo
		{
			get
			{
				return new esQueryItem(this, TituloRendaFixaMetadata.ColumnNames.IdTitulo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdPapel
		{
			get
			{
				return new esQueryItem(this, TituloRendaFixaMetadata.ColumnNames.IdPapel, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdIndice
		{
			get
			{
				return new esQueryItem(this, TituloRendaFixaMetadata.ColumnNames.IdIndice, esSystemType.Int16);
			}
		} 
		
		public esQueryItem IdEmissor
		{
			get
			{
				return new esQueryItem(this, TituloRendaFixaMetadata.ColumnNames.IdEmissor, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, TituloRendaFixaMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
		public esQueryItem Taxa
		{
			get
			{
				return new esQueryItem(this, TituloRendaFixaMetadata.ColumnNames.Taxa, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Percentual
		{
			get
			{
				return new esQueryItem(this, TituloRendaFixaMetadata.ColumnNames.Percentual, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataEmissao
		{
			get
			{
				return new esQueryItem(this, TituloRendaFixaMetadata.ColumnNames.DataEmissao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataVencimento
		{
			get
			{
				return new esQueryItem(this, TituloRendaFixaMetadata.ColumnNames.DataVencimento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem ValorNominal
		{
			get
			{
				return new esQueryItem(this, TituloRendaFixaMetadata.ColumnNames.ValorNominal, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem CodigoCustodia
		{
			get
			{
				return new esQueryItem(this, TituloRendaFixaMetadata.ColumnNames.CodigoCustodia, esSystemType.String);
			}
		} 
		
		public esQueryItem PUNominal
		{
			get
			{
				return new esQueryItem(this, TituloRendaFixaMetadata.ColumnNames.PUNominal, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DescricaoCompleta
		{
			get
			{
				return new esQueryItem(this, TituloRendaFixaMetadata.ColumnNames.DescricaoCompleta, esSystemType.String);
			}
		} 
		
		public esQueryItem IsentoIR
		{
			get
			{
				return new esQueryItem(this, TituloRendaFixaMetadata.ColumnNames.IsentoIR, esSystemType.Byte);
			}
		} 
		
		public esQueryItem IsentoIOF
		{
			get
			{
				return new esQueryItem(this, TituloRendaFixaMetadata.ColumnNames.IsentoIOF, esSystemType.Byte);
			}
		} 
		
		public esQueryItem IdMoeda
		{
			get
			{
				return new esQueryItem(this, TituloRendaFixaMetadata.ColumnNames.IdMoeda, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdEstrategia
		{
			get
			{
				return new esQueryItem(this, TituloRendaFixaMetadata.ColumnNames.IdEstrategia, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CodigoCDA
		{
			get
			{
				return new esQueryItem(this, TituloRendaFixaMetadata.ColumnNames.CodigoCDA, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CodigoIsin
		{
			get
			{
				return new esQueryItem(this, TituloRendaFixaMetadata.ColumnNames.CodigoIsin, esSystemType.String);
			}
		} 
		
		public esQueryItem CodigoCetip
		{
			get
			{
				return new esQueryItem(this, TituloRendaFixaMetadata.ColumnNames.CodigoCetip, esSystemType.String);
			}
		} 
		
		public esQueryItem CodigoCBLC
		{
			get
			{
				return new esQueryItem(this, TituloRendaFixaMetadata.ColumnNames.CodigoCBLC, esSystemType.String);
			}
		} 
		
		public esQueryItem DebentureConversivel
		{
			get
			{
				return new esQueryItem(this, TituloRendaFixaMetadata.ColumnNames.DebentureConversivel, esSystemType.String);
			}
		} 
		
		public esQueryItem CodigoInterface
		{
			get
			{
				return new esQueryItem(this, TituloRendaFixaMetadata.ColumnNames.CodigoInterface, esSystemType.String);
			}
		} 
		
		public esQueryItem DebentureInfra
		{
			get
			{
				return new esQueryItem(this, TituloRendaFixaMetadata.ColumnNames.DebentureInfra, esSystemType.String);
			}
		} 
		
		public esQueryItem PremioRebate
		{
			get
			{
				return new esQueryItem(this, TituloRendaFixaMetadata.ColumnNames.PremioRebate, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Periodicidade
		{
			get
			{
				return new esQueryItem(this, TituloRendaFixaMetadata.ColumnNames.Periodicidade, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CriterioAmortizacao
		{
			get
			{
				return new esQueryItem(this, TituloRendaFixaMetadata.ColumnNames.CriterioAmortizacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem PermiteRepactuacao
		{
			get
			{
				return new esQueryItem(this, TituloRendaFixaMetadata.ColumnNames.PermiteRepactuacao, esSystemType.String);
			}
		} 
		
		public esQueryItem ExecutaProRataEmissao
		{
			get
			{
				return new esQueryItem(this, TituloRendaFixaMetadata.ColumnNames.ExecutaProRataEmissao, esSystemType.String);
			}
		} 
		
		public esQueryItem TipoProRata
		{
			get
			{
				return new esQueryItem(this, TituloRendaFixaMetadata.ColumnNames.TipoProRata, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DefasagemLiquidacao
		{
			get
			{
				return new esQueryItem(this, TituloRendaFixaMetadata.ColumnNames.DefasagemLiquidacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ProRataLiquidacao
		{
			get
			{
				return new esQueryItem(this, TituloRendaFixaMetadata.ColumnNames.ProRataLiquidacao, esSystemType.String);
			}
		} 
		
		public esQueryItem ProRataEmissaoDia
		{
			get
			{
				return new esQueryItem(this, TituloRendaFixaMetadata.ColumnNames.ProRataEmissaoDia, esSystemType.Int32);
			}
		} 
		
		public esQueryItem AtivoRegra
		{
			get
			{
				return new esQueryItem(this, TituloRendaFixaMetadata.ColumnNames.AtivoRegra, esSystemType.String);
			}
		} 
		
		public esQueryItem DataInicioCorrecao
		{
			get
			{
				return new esQueryItem(this, TituloRendaFixaMetadata.ColumnNames.DataInicioCorrecao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DefasagemMeses
		{
			get
			{
				return new esQueryItem(this, TituloRendaFixaMetadata.ColumnNames.DefasagemMeses, esSystemType.Int32);
			}
		} 
		
		public esQueryItem EJuros
		{
			get
			{
				return new esQueryItem(this, TituloRendaFixaMetadata.ColumnNames.EJuros, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CodigoCusip
		{
			get
			{
				return new esQueryItem(this, TituloRendaFixaMetadata.ColumnNames.CodigoCusip, esSystemType.String);
			}
		} 
		
		public esQueryItem EJurosTipo
		{
			get
			{
				return new esQueryItem(this, TituloRendaFixaMetadata.ColumnNames.EJurosTipo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CodigoBDS
		{
			get
			{
				return new esQueryItem(this, TituloRendaFixaMetadata.ColumnNames.CodigoBDS, esSystemType.Int32);
			}
		} 
		
		public esQueryItem OpcaoEmbutida
		{
			get
			{
				return new esQueryItem(this, TituloRendaFixaMetadata.ColumnNames.OpcaoEmbutida, esSystemType.String);
			}
		} 
		
		public esQueryItem TipoOpcao
		{
			get
			{
				return new esQueryItem(this, TituloRendaFixaMetadata.ColumnNames.TipoOpcao, esSystemType.Int16);
			}
		} 
		
		public esQueryItem TipoExercicio
		{
			get
			{
				return new esQueryItem(this, TituloRendaFixaMetadata.ColumnNames.TipoExercicio, esSystemType.Int16);
			}
		} 
		
		public esQueryItem TipoPremio
		{
			get
			{
				return new esQueryItem(this, TituloRendaFixaMetadata.ColumnNames.TipoPremio, esSystemType.Int16);
			}
		} 
		
		public esQueryItem DataInicioExercicio
		{
			get
			{
				return new esQueryItem(this, TituloRendaFixaMetadata.ColumnNames.DataInicioExercicio, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataVencimentoOpcao
		{
			get
			{
				return new esQueryItem(this, TituloRendaFixaMetadata.ColumnNames.DataVencimentoOpcao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem ValorPremio
		{
			get
			{
				return new esQueryItem(this, TituloRendaFixaMetadata.ColumnNames.ValorPremio, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ETaxa
		{
			get
			{
				return new esQueryItem(this, TituloRendaFixaMetadata.ColumnNames.ETaxa, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Participacao
		{
			get
			{
				return new esQueryItem(this, TituloRendaFixaMetadata.ColumnNames.Participacao, esSystemType.String);
			}
		} 
		
		public esQueryItem ParametrosAvancados
		{
			get
			{
				return new esQueryItem(this, TituloRendaFixaMetadata.ColumnNames.ParametrosAvancados, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TituloRendaFixaCollection")]
	public partial class TituloRendaFixaCollection : esTituloRendaFixaCollection, IEnumerable<TituloRendaFixa>
	{
		public TituloRendaFixaCollection()
		{

		}
		
		public static implicit operator List<TituloRendaFixa>(TituloRendaFixaCollection coll)
		{
			List<TituloRendaFixa> list = new List<TituloRendaFixa>();
			
			foreach (TituloRendaFixa emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TituloRendaFixaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TituloRendaFixaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TituloRendaFixa(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TituloRendaFixa();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TituloRendaFixaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TituloRendaFixaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TituloRendaFixaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TituloRendaFixa AddNew()
		{
			TituloRendaFixa entity = base.AddNewEntity() as TituloRendaFixa;
			
			return entity;
		}

		public TituloRendaFixa FindByPrimaryKey(System.Int32 idTitulo)
		{
			return base.FindByPrimaryKey(idTitulo) as TituloRendaFixa;
		}


		#region IEnumerable<TituloRendaFixa> Members

		IEnumerator<TituloRendaFixa> IEnumerable<TituloRendaFixa>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TituloRendaFixa;
			}
		}

		#endregion
		
		private TituloRendaFixaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TituloRendaFixa' table
	/// </summary>

	[Serializable]
	public partial class TituloRendaFixa : esTituloRendaFixa
	{
		public TituloRendaFixa()
		{

		}
	
		public TituloRendaFixa(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TituloRendaFixaMetadata.Meta();
			}
		}
		
		
		
		override protected esTituloRendaFixaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TituloRendaFixaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TituloRendaFixaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TituloRendaFixaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TituloRendaFixaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TituloRendaFixaQuery query;
	}



	[Serializable]
	public partial class TituloRendaFixaQuery : esTituloRendaFixaQuery
	{
		public TituloRendaFixaQuery()
		{

		}		
		
		public TituloRendaFixaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TituloRendaFixaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TituloRendaFixaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TituloRendaFixaMetadata.ColumnNames.IdTitulo, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TituloRendaFixaMetadata.PropertyNames.IdTitulo;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TituloRendaFixaMetadata.ColumnNames.IdPapel, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TituloRendaFixaMetadata.PropertyNames.IdPapel;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TituloRendaFixaMetadata.ColumnNames.IdIndice, 2, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = TituloRendaFixaMetadata.PropertyNames.IdIndice;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TituloRendaFixaMetadata.ColumnNames.IdEmissor, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TituloRendaFixaMetadata.PropertyNames.IdEmissor;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TituloRendaFixaMetadata.ColumnNames.Descricao, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = TituloRendaFixaMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TituloRendaFixaMetadata.ColumnNames.Taxa, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TituloRendaFixaMetadata.PropertyNames.Taxa;	
			c.NumericPrecision = 25;
			c.NumericScale = 16;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TituloRendaFixaMetadata.ColumnNames.Percentual, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TituloRendaFixaMetadata.PropertyNames.Percentual;	
			c.NumericPrecision = 10;
			c.NumericScale = 4;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TituloRendaFixaMetadata.ColumnNames.DataEmissao, 7, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TituloRendaFixaMetadata.PropertyNames.DataEmissao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TituloRendaFixaMetadata.ColumnNames.DataVencimento, 8, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TituloRendaFixaMetadata.PropertyNames.DataVencimento;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TituloRendaFixaMetadata.ColumnNames.ValorNominal, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TituloRendaFixaMetadata.PropertyNames.ValorNominal;	
			c.NumericPrecision = 10;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TituloRendaFixaMetadata.ColumnNames.CodigoCustodia, 10, typeof(System.String), esSystemType.String);
			c.PropertyName = TituloRendaFixaMetadata.PropertyNames.CodigoCustodia;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TituloRendaFixaMetadata.ColumnNames.PUNominal, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TituloRendaFixaMetadata.PropertyNames.PUNominal;	
			c.NumericPrecision = 16;
			c.NumericScale = 8;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TituloRendaFixaMetadata.ColumnNames.DescricaoCompleta, 12, typeof(System.String), esSystemType.String);
			c.PropertyName = TituloRendaFixaMetadata.PropertyNames.DescricaoCompleta;
			c.CharacterMaxLength = 255;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TituloRendaFixaMetadata.ColumnNames.IsentoIR, 13, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = TituloRendaFixaMetadata.PropertyNames.IsentoIR;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TituloRendaFixaMetadata.ColumnNames.IsentoIOF, 14, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = TituloRendaFixaMetadata.PropertyNames.IsentoIOF;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TituloRendaFixaMetadata.ColumnNames.IdMoeda, 15, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TituloRendaFixaMetadata.PropertyNames.IdMoeda;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TituloRendaFixaMetadata.ColumnNames.IdEstrategia, 16, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TituloRendaFixaMetadata.PropertyNames.IdEstrategia;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TituloRendaFixaMetadata.ColumnNames.CodigoCDA, 17, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TituloRendaFixaMetadata.PropertyNames.CodigoCDA;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TituloRendaFixaMetadata.ColumnNames.CodigoIsin, 18, typeof(System.String), esSystemType.String);
			c.PropertyName = TituloRendaFixaMetadata.PropertyNames.CodigoIsin;
			c.CharacterMaxLength = 12;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TituloRendaFixaMetadata.ColumnNames.CodigoCetip, 19, typeof(System.String), esSystemType.String);
			c.PropertyName = TituloRendaFixaMetadata.PropertyNames.CodigoCetip;
			c.CharacterMaxLength = 14;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TituloRendaFixaMetadata.ColumnNames.CodigoCBLC, 20, typeof(System.String), esSystemType.String);
			c.PropertyName = TituloRendaFixaMetadata.PropertyNames.CodigoCBLC;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TituloRendaFixaMetadata.ColumnNames.DebentureConversivel, 21, typeof(System.String), esSystemType.String);
			c.PropertyName = TituloRendaFixaMetadata.PropertyNames.DebentureConversivel;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TituloRendaFixaMetadata.ColumnNames.CodigoInterface, 22, typeof(System.String), esSystemType.String);
			c.PropertyName = TituloRendaFixaMetadata.PropertyNames.CodigoInterface;
			c.CharacterMaxLength = 40;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TituloRendaFixaMetadata.ColumnNames.DebentureInfra, 23, typeof(System.String), esSystemType.String);
			c.PropertyName = TituloRendaFixaMetadata.PropertyNames.DebentureInfra;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TituloRendaFixaMetadata.ColumnNames.PremioRebate, 24, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TituloRendaFixaMetadata.PropertyNames.PremioRebate;	
			c.NumericPrecision = 25;
			c.NumericScale = 8;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TituloRendaFixaMetadata.ColumnNames.Periodicidade, 25, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TituloRendaFixaMetadata.PropertyNames.Periodicidade;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TituloRendaFixaMetadata.ColumnNames.CriterioAmortizacao, 26, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TituloRendaFixaMetadata.PropertyNames.CriterioAmortizacao;	
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"('2')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TituloRendaFixaMetadata.ColumnNames.PermiteRepactuacao, 27, typeof(System.String), esSystemType.String);
			c.PropertyName = TituloRendaFixaMetadata.PropertyNames.PermiteRepactuacao;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TituloRendaFixaMetadata.ColumnNames.ExecutaProRataEmissao, 28, typeof(System.String), esSystemType.String);
			c.PropertyName = TituloRendaFixaMetadata.PropertyNames.ExecutaProRataEmissao;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TituloRendaFixaMetadata.ColumnNames.TipoProRata, 29, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TituloRendaFixaMetadata.PropertyNames.TipoProRata;	
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"('0')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TituloRendaFixaMetadata.ColumnNames.DefasagemLiquidacao, 30, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TituloRendaFixaMetadata.PropertyNames.DefasagemLiquidacao;	
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"('0')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TituloRendaFixaMetadata.ColumnNames.ProRataLiquidacao, 31, typeof(System.String), esSystemType.String);
			c.PropertyName = TituloRendaFixaMetadata.PropertyNames.ProRataLiquidacao;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TituloRendaFixaMetadata.ColumnNames.ProRataEmissaoDia, 32, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TituloRendaFixaMetadata.PropertyNames.ProRataEmissaoDia;	
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"('0')";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TituloRendaFixaMetadata.ColumnNames.AtivoRegra, 33, typeof(System.String), esSystemType.String);
			c.PropertyName = TituloRendaFixaMetadata.PropertyNames.AtivoRegra;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TituloRendaFixaMetadata.ColumnNames.DataInicioCorrecao, 34, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TituloRendaFixaMetadata.PropertyNames.DataInicioCorrecao;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TituloRendaFixaMetadata.ColumnNames.DefasagemMeses, 35, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TituloRendaFixaMetadata.PropertyNames.DefasagemMeses;	
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"('0')";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TituloRendaFixaMetadata.ColumnNames.EJuros, 36, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TituloRendaFixaMetadata.PropertyNames.EJuros;	
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"('6')";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TituloRendaFixaMetadata.ColumnNames.CodigoCusip, 37, typeof(System.String), esSystemType.String);
			c.PropertyName = TituloRendaFixaMetadata.PropertyNames.CodigoCusip;
			c.CharacterMaxLength = 9;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TituloRendaFixaMetadata.ColumnNames.EJurosTipo, 38, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TituloRendaFixaMetadata.PropertyNames.EJurosTipo;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TituloRendaFixaMetadata.ColumnNames.CodigoBDS, 39, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TituloRendaFixaMetadata.PropertyNames.CodigoBDS;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TituloRendaFixaMetadata.ColumnNames.OpcaoEmbutida, 40, typeof(System.String), esSystemType.String);
			c.PropertyName = TituloRendaFixaMetadata.PropertyNames.OpcaoEmbutida;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TituloRendaFixaMetadata.ColumnNames.TipoOpcao, 41, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = TituloRendaFixaMetadata.PropertyNames.TipoOpcao;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TituloRendaFixaMetadata.ColumnNames.TipoExercicio, 42, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = TituloRendaFixaMetadata.PropertyNames.TipoExercicio;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TituloRendaFixaMetadata.ColumnNames.TipoPremio, 43, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = TituloRendaFixaMetadata.PropertyNames.TipoPremio;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TituloRendaFixaMetadata.ColumnNames.DataInicioExercicio, 44, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TituloRendaFixaMetadata.PropertyNames.DataInicioExercicio;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TituloRendaFixaMetadata.ColumnNames.DataVencimentoOpcao, 45, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = TituloRendaFixaMetadata.PropertyNames.DataVencimentoOpcao;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TituloRendaFixaMetadata.ColumnNames.ValorPremio, 46, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TituloRendaFixaMetadata.PropertyNames.ValorPremio;	
			c.NumericPrecision = 16;
			c.NumericScale = 8;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TituloRendaFixaMetadata.ColumnNames.ETaxa, 47, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TituloRendaFixaMetadata.PropertyNames.ETaxa;	
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"('5')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TituloRendaFixaMetadata.ColumnNames.Participacao, 48, typeof(System.String), esSystemType.String);
			c.PropertyName = TituloRendaFixaMetadata.PropertyNames.Participacao;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TituloRendaFixaMetadata.ColumnNames.ParametrosAvancados, 49, typeof(System.String), esSystemType.String);
			c.PropertyName = TituloRendaFixaMetadata.PropertyNames.ParametrosAvancados;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TituloRendaFixaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdTitulo = "IdTitulo";
			 public const string IdPapel = "IdPapel";
			 public const string IdIndice = "IdIndice";
			 public const string IdEmissor = "IdEmissor";
			 public const string Descricao = "Descricao";
			 public const string Taxa = "Taxa";
			 public const string Percentual = "Percentual";
			 public const string DataEmissao = "DataEmissao";
			 public const string DataVencimento = "DataVencimento";
			 public const string ValorNominal = "ValorNominal";
			 public const string CodigoCustodia = "CodigoCustodia";
			 public const string PUNominal = "PUNominal";
			 public const string DescricaoCompleta = "DescricaoCompleta";
			 public const string IsentoIR = "IsentoIR";
			 public const string IsentoIOF = "IsentoIOF";
			 public const string IdMoeda = "IdMoeda";
			 public const string IdEstrategia = "IdEstrategia";
			 public const string CodigoCDA = "CodigoCDA";
			 public const string CodigoIsin = "CodigoIsin";
			 public const string CodigoCetip = "CodigoCetip";
			 public const string CodigoCBLC = "CodigoCBLC";
			 public const string DebentureConversivel = "DebentureConversivel";
			 public const string CodigoInterface = "CodigoInterface";
			 public const string DebentureInfra = "DebentureInfra";
			 public const string PremioRebate = "PremioRebate";
			 public const string Periodicidade = "Periodicidade";
			 public const string CriterioAmortizacao = "CriterioAmortizacao";
			 public const string PermiteRepactuacao = "PermiteRepactuacao";
			 public const string ExecutaProRataEmissao = "ExecutaProRataEmissao";
			 public const string TipoProRata = "TipoProRata";
			 public const string DefasagemLiquidacao = "DefasagemLiquidacao";
			 public const string ProRataLiquidacao = "ProRataLiquidacao";
			 public const string ProRataEmissaoDia = "ProRataEmissaoDia";
			 public const string AtivoRegra = "AtivoRegra";
			 public const string DataInicioCorrecao = "DataInicioCorrecao";
			 public const string DefasagemMeses = "DefasagemMeses";
			 public const string EJuros = "EJuros";
			 public const string CodigoCusip = "CodigoCusip";
			 public const string EJurosTipo = "EJurosTipo";
			 public const string CodigoBDS = "CodigoBDS";
			 public const string OpcaoEmbutida = "OpcaoEmbutida";
			 public const string TipoOpcao = "TipoOpcao";
			 public const string TipoExercicio = "TipoExercicio";
			 public const string TipoPremio = "TipoPremio";
			 public const string DataInicioExercicio = "DataInicioExercicio";
			 public const string DataVencimentoOpcao = "DataVencimentoOpcao";
			 public const string ValorPremio = "ValorPremio";
			 public const string ETaxa = "ETaxa";
			 public const string Participacao = "Participacao";
			 public const string ParametrosAvancados = "ParametrosAvancados";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdTitulo = "IdTitulo";
			 public const string IdPapel = "IdPapel";
			 public const string IdIndice = "IdIndice";
			 public const string IdEmissor = "IdEmissor";
			 public const string Descricao = "Descricao";
			 public const string Taxa = "Taxa";
			 public const string Percentual = "Percentual";
			 public const string DataEmissao = "DataEmissao";
			 public const string DataVencimento = "DataVencimento";
			 public const string ValorNominal = "ValorNominal";
			 public const string CodigoCustodia = "CodigoCustodia";
			 public const string PUNominal = "PUNominal";
			 public const string DescricaoCompleta = "DescricaoCompleta";
			 public const string IsentoIR = "IsentoIR";
			 public const string IsentoIOF = "IsentoIOF";
			 public const string IdMoeda = "IdMoeda";
			 public const string IdEstrategia = "IdEstrategia";
			 public const string CodigoCDA = "CodigoCDA";
			 public const string CodigoIsin = "CodigoIsin";
			 public const string CodigoCetip = "CodigoCetip";
			 public const string CodigoCBLC = "CodigoCBLC";
			 public const string DebentureConversivel = "DebentureConversivel";
			 public const string CodigoInterface = "CodigoInterface";
			 public const string DebentureInfra = "DebentureInfra";
			 public const string PremioRebate = "PremioRebate";
			 public const string Periodicidade = "Periodicidade";
			 public const string CriterioAmortizacao = "CriterioAmortizacao";
			 public const string PermiteRepactuacao = "PermiteRepactuacao";
			 public const string ExecutaProRataEmissao = "ExecutaProRataEmissao";
			 public const string TipoProRata = "TipoProRata";
			 public const string DefasagemLiquidacao = "DefasagemLiquidacao";
			 public const string ProRataLiquidacao = "ProRataLiquidacao";
			 public const string ProRataEmissaoDia = "ProRataEmissaoDia";
			 public const string AtivoRegra = "AtivoRegra";
			 public const string DataInicioCorrecao = "DataInicioCorrecao";
			 public const string DefasagemMeses = "DefasagemMeses";
			 public const string EJuros = "EJuros";
			 public const string CodigoCusip = "CodigoCusip";
			 public const string EJurosTipo = "EJurosTipo";
			 public const string CodigoBDS = "CodigoBDS";
			 public const string OpcaoEmbutida = "OpcaoEmbutida";
			 public const string TipoOpcao = "TipoOpcao";
			 public const string TipoExercicio = "TipoExercicio";
			 public const string TipoPremio = "TipoPremio";
			 public const string DataInicioExercicio = "DataInicioExercicio";
			 public const string DataVencimentoOpcao = "DataVencimentoOpcao";
			 public const string ValorPremio = "ValorPremio";
			 public const string ETaxa = "ETaxa";
			 public const string Participacao = "Participacao";
			 public const string ParametrosAvancados = "ParametrosAvancados";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TituloRendaFixaMetadata))
			{
				if(TituloRendaFixaMetadata.mapDelegates == null)
				{
					TituloRendaFixaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TituloRendaFixaMetadata.meta == null)
				{
					TituloRendaFixaMetadata.meta = new TituloRendaFixaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdTitulo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdPapel", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdIndice", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("IdEmissor", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("Taxa", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Percentual", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("DataEmissao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataVencimento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("ValorNominal", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("CodigoCustodia", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("PUNominal", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("DescricaoCompleta", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IsentoIR", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("IsentoIOF", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("IdMoeda", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdEstrategia", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CodigoCDA", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CodigoIsin", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("CodigoCetip", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("CodigoCBLC", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DebentureConversivel", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("CodigoInterface", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DebentureInfra", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("PremioRebate", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Periodicidade", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CriterioAmortizacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("PermiteRepactuacao", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("ExecutaProRataEmissao", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("TipoProRata", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DefasagemLiquidacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ProRataLiquidacao", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("ProRataEmissaoDia", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("AtivoRegra", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DataInicioCorrecao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DefasagemMeses", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("EJuros", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CodigoCusip", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("EJurosTipo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CodigoBDS", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("OpcaoEmbutida", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("TipoOpcao", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("TipoExercicio", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("TipoPremio", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("DataInicioExercicio", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataVencimentoOpcao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("ValorPremio", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ETaxa", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Participacao", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("ParametrosAvancados", new esTypeMap("char", "System.String"));			
				
				
				
				meta.Source = "TituloRendaFixa";
				meta.Destination = "TituloRendaFixa";
				
				meta.spInsert = "proc_TituloRendaFixaInsert";				
				meta.spUpdate = "proc_TituloRendaFixaUpdate";		
				meta.spDelete = "proc_TituloRendaFixaDelete";
				meta.spLoadAll = "proc_TituloRendaFixaLoadAll";
				meta.spLoadByPrimaryKey = "proc_TituloRendaFixaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TituloRendaFixaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
