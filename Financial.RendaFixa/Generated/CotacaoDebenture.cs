/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 12/12/2014 10:53:28 AM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.RendaFixa
{

	[Serializable]
	abstract public class esCotacaoDebentureCollection : esEntityCollection
	{
		public esCotacaoDebentureCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "CotacaoDebentureCollection";
		}

		#region Query Logic
		protected void InitQuery(esCotacaoDebentureQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esCotacaoDebentureQuery);
		}
		#endregion
		
		virtual public CotacaoDebenture DetachEntity(CotacaoDebenture entity)
		{
			return base.DetachEntity(entity) as CotacaoDebenture;
		}
		
		virtual public CotacaoDebenture AttachEntity(CotacaoDebenture entity)
		{
			return base.AttachEntity(entity) as CotacaoDebenture;
		}
		
		virtual public void Combine(CotacaoDebentureCollection collection)
		{
			base.Combine(collection);
		}
		
		new public CotacaoDebenture this[int index]
		{
			get
			{
				return base[index] as CotacaoDebenture;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(CotacaoDebenture);
		}
	}



	[Serializable]
	abstract public class esCotacaoDebenture : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esCotacaoDebentureQuery GetDynamicQuery()
		{
			return null;
		}

		public esCotacaoDebenture()
		{

		}

		public esCotacaoDebenture(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime data, System.String codigoPapel)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(data, codigoPapel);
			else
				return LoadByPrimaryKeyStoredProcedure(data, codigoPapel);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.DateTime data, System.String codigoPapel)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esCotacaoDebentureQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.Data == data, query.CodigoPapel == codigoPapel);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime data, System.String codigoPapel)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(data, codigoPapel);
			else
				return LoadByPrimaryKeyStoredProcedure(data, codigoPapel);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime data, System.String codigoPapel)
		{
			esCotacaoDebentureQuery query = this.GetDynamicQuery();
			query.Where(query.Data == data, query.CodigoPapel == codigoPapel);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime data, System.String codigoPapel)
		{
			esParameters parms = new esParameters();
			parms.Add("Data",data);			parms.Add("CodigoPapel",codigoPapel);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "Data": this.str.Data = (string)value; break;							
						case "CodigoPapel": this.str.CodigoPapel = (string)value; break;							
						case "Pu": this.str.Pu = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "Pu":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Pu = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to CotacaoDebenture.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(CotacaoDebentureMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(CotacaoDebentureMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to CotacaoDebenture.CodigoPapel
		/// </summary>
		virtual public System.String CodigoPapel
		{
			get
			{
				return base.GetSystemString(CotacaoDebentureMetadata.ColumnNames.CodigoPapel);
			}
			
			set
			{
				base.SetSystemString(CotacaoDebentureMetadata.ColumnNames.CodigoPapel, value);
			}
		}
		
		/// <summary>
		/// Maps to CotacaoDebenture.PU
		/// </summary>
		virtual public System.Decimal? Pu
		{
			get
			{
				return base.GetSystemDecimal(CotacaoDebentureMetadata.ColumnNames.Pu);
			}
			
			set
			{
				base.SetSystemDecimal(CotacaoDebentureMetadata.ColumnNames.Pu, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esCotacaoDebenture entity)
			{
				this.entity = entity;
			}
			
	
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String CodigoPapel
			{
				get
				{
					System.String data = entity.CodigoPapel;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoPapel = null;
					else entity.CodigoPapel = Convert.ToString(value);
				}
			}
				
			public System.String Pu
			{
				get
				{
					System.Decimal? data = entity.Pu;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Pu = null;
					else entity.Pu = Convert.ToDecimal(value);
				}
			}
			

			private esCotacaoDebenture entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esCotacaoDebentureQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esCotacaoDebenture can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class CotacaoDebenture : esCotacaoDebenture
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esCotacaoDebentureQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return CotacaoDebentureMetadata.Meta();
			}
		}	
		

		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, CotacaoDebentureMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem CodigoPapel
		{
			get
			{
				return new esQueryItem(this, CotacaoDebentureMetadata.ColumnNames.CodigoPapel, esSystemType.String);
			}
		} 
		
		public esQueryItem Pu
		{
			get
			{
				return new esQueryItem(this, CotacaoDebentureMetadata.ColumnNames.Pu, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("CotacaoDebentureCollection")]
	public partial class CotacaoDebentureCollection : esCotacaoDebentureCollection, IEnumerable<CotacaoDebenture>
	{
		public CotacaoDebentureCollection()
		{

		}
		
		public static implicit operator List<CotacaoDebenture>(CotacaoDebentureCollection coll)
		{
			List<CotacaoDebenture> list = new List<CotacaoDebenture>();
			
			foreach (CotacaoDebenture emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  CotacaoDebentureMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CotacaoDebentureQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new CotacaoDebenture(row);
		}

		override protected esEntity CreateEntity()
		{
			return new CotacaoDebenture();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public CotacaoDebentureQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CotacaoDebentureQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(CotacaoDebentureQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public CotacaoDebenture AddNew()
		{
			CotacaoDebenture entity = base.AddNewEntity() as CotacaoDebenture;
			
			return entity;
		}

		public CotacaoDebenture FindByPrimaryKey(System.DateTime data, System.String codigoPapel)
		{
			return base.FindByPrimaryKey(data, codigoPapel) as CotacaoDebenture;
		}


		#region IEnumerable<CotacaoDebenture> Members

		IEnumerator<CotacaoDebenture> IEnumerable<CotacaoDebenture>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as CotacaoDebenture;
			}
		}

		#endregion
		
		private CotacaoDebentureQuery query;
	}


	/// <summary>
	/// Encapsulates the 'CotacaoDebenture' table
	/// </summary>

	[Serializable]
	public partial class CotacaoDebenture : esCotacaoDebenture
	{
		public CotacaoDebenture()
		{

		}
	
		public CotacaoDebenture(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return CotacaoDebentureMetadata.Meta();
			}
		}
		
		
		
		override protected esCotacaoDebentureQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CotacaoDebentureQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public CotacaoDebentureQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CotacaoDebentureQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(CotacaoDebentureQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private CotacaoDebentureQuery query;
	}



	[Serializable]
	public partial class CotacaoDebentureQuery : esCotacaoDebentureQuery
	{
		public CotacaoDebentureQuery()
		{

		}		
		
		public CotacaoDebentureQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class CotacaoDebentureMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected CotacaoDebentureMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(CotacaoDebentureMetadata.ColumnNames.Data, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = CotacaoDebentureMetadata.PropertyNames.Data;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotacaoDebentureMetadata.ColumnNames.CodigoPapel, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = CotacaoDebentureMetadata.PropertyNames.CodigoPapel;
			c.IsInPrimaryKey = true;
			c.CharacterMaxLength = 12;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotacaoDebentureMetadata.ColumnNames.Pu, 2, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CotacaoDebentureMetadata.PropertyNames.Pu;	
			c.NumericPrecision = 28;
			c.NumericScale = 16;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public CotacaoDebentureMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string Data = "Data";
			 public const string CodigoPapel = "CodigoPapel";
			 public const string Pu = "PU";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string Data = "Data";
			 public const string CodigoPapel = "CodigoPapel";
			 public const string Pu = "Pu";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(CotacaoDebentureMetadata))
			{
				if(CotacaoDebentureMetadata.mapDelegates == null)
				{
					CotacaoDebentureMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (CotacaoDebentureMetadata.meta == null)
				{
					CotacaoDebentureMetadata.meta = new CotacaoDebentureMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("CodigoPapel", new esTypeMap("nvarchar", "System.String"));
				meta.AddTypeMap("PU", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "CotacaoDebenture";
				meta.Destination = "CotacaoDebenture";
				
				meta.spInsert = "proc_CotacaoDebentureInsert";				
				meta.spUpdate = "proc_CotacaoDebentureUpdate";		
				meta.spDelete = "proc_CotacaoDebentureDelete";
				meta.spLoadAll = "proc_CotacaoDebentureLoadAll";
				meta.spLoadByPrimaryKey = "proc_CotacaoDebentureLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private CotacaoDebentureMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
