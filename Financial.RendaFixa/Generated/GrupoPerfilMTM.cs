/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 27/10/2015 17:39:50
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Fundo;



namespace Financial.RendaFixa
{

	[Serializable]
	abstract public class esGrupoPerfilMTMCollection : esEntityCollection
	{
		public esGrupoPerfilMTMCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "GrupoPerfilMTMCollection";
		}

		#region Query Logic
		protected void InitQuery(esGrupoPerfilMTMQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esGrupoPerfilMTMQuery);
		}
		#endregion
		
		virtual public GrupoPerfilMTM DetachEntity(GrupoPerfilMTM entity)
		{
			return base.DetachEntity(entity) as GrupoPerfilMTM;
		}
		
		virtual public GrupoPerfilMTM AttachEntity(GrupoPerfilMTM entity)
		{
			return base.AttachEntity(entity) as GrupoPerfilMTM;
		}
		
		virtual public void Combine(GrupoPerfilMTMCollection collection)
		{
			base.Combine(collection);
		}
		
		new public GrupoPerfilMTM this[int index]
		{
			get
			{
				return base[index] as GrupoPerfilMTM;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(GrupoPerfilMTM);
		}
	}



	[Serializable]
	abstract public class esGrupoPerfilMTM : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esGrupoPerfilMTMQuery GetDynamicQuery()
		{
			return null;
		}

		public esGrupoPerfilMTM()
		{

		}

		public esGrupoPerfilMTM(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idGrupoPerfilMTM)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idGrupoPerfilMTM);
			else
				return LoadByPrimaryKeyStoredProcedure(idGrupoPerfilMTM);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idGrupoPerfilMTM)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idGrupoPerfilMTM);
			else
				return LoadByPrimaryKeyStoredProcedure(idGrupoPerfilMTM);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idGrupoPerfilMTM)
		{
			esGrupoPerfilMTMQuery query = this.GetDynamicQuery();
			query.Where(query.IdGrupoPerfilMTM == idGrupoPerfilMTM);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idGrupoPerfilMTM)
		{
			esParameters parms = new esParameters();
			parms.Add("IdGrupoPerfilMTM",idGrupoPerfilMTM);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdGrupoPerfilMTM": this.str.IdGrupoPerfilMTM = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdGrupoPerfilMTM":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdGrupoPerfilMTM = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to GrupoPerfilMTM.IdGrupoPerfilMTM
		/// </summary>
		virtual public System.Int32? IdGrupoPerfilMTM
		{
			get
			{
				return base.GetSystemInt32(GrupoPerfilMTMMetadata.ColumnNames.IdGrupoPerfilMTM);
			}
			
			set
			{
				base.SetSystemInt32(GrupoPerfilMTMMetadata.ColumnNames.IdGrupoPerfilMTM, value);
			}
		}
		
		/// <summary>
		/// Maps to GrupoPerfilMTM.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(GrupoPerfilMTMMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(GrupoPerfilMTMMetadata.ColumnNames.Descricao, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esGrupoPerfilMTM entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdGrupoPerfilMTM
			{
				get
				{
					System.Int32? data = entity.IdGrupoPerfilMTM;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdGrupoPerfilMTM = null;
					else entity.IdGrupoPerfilMTM = Convert.ToInt32(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
			

			private esGrupoPerfilMTM entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esGrupoPerfilMTMQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esGrupoPerfilMTM can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class GrupoPerfilMTM : esGrupoPerfilMTM
	{

				
		#region CarteiraCollectionByIdGrupoPerfilMTM - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - Carteira_GrupoPerfil_FK
		/// </summary>

		[XmlIgnore]
		public CarteiraCollection CarteiraCollectionByIdGrupoPerfilMTM
		{
			get
			{
				if(this._CarteiraCollectionByIdGrupoPerfilMTM == null)
				{
					this._CarteiraCollectionByIdGrupoPerfilMTM = new CarteiraCollection();
					this._CarteiraCollectionByIdGrupoPerfilMTM.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("CarteiraCollectionByIdGrupoPerfilMTM", this._CarteiraCollectionByIdGrupoPerfilMTM);
				
					if(this.IdGrupoPerfilMTM != null)
					{
						this._CarteiraCollectionByIdGrupoPerfilMTM.Query.Where(this._CarteiraCollectionByIdGrupoPerfilMTM.Query.IdGrupoPerfilMTM == this.IdGrupoPerfilMTM);
						this._CarteiraCollectionByIdGrupoPerfilMTM.Query.Load();

						// Auto-hookup Foreign Keys
						this._CarteiraCollectionByIdGrupoPerfilMTM.fks.Add(CarteiraMetadata.ColumnNames.IdGrupoPerfilMTM, this.IdGrupoPerfilMTM);
					}
				}

				return this._CarteiraCollectionByIdGrupoPerfilMTM;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._CarteiraCollectionByIdGrupoPerfilMTM != null) 
				{ 
					this.RemovePostSave("CarteiraCollectionByIdGrupoPerfilMTM"); 
					this._CarteiraCollectionByIdGrupoPerfilMTM = null;
					
				} 
			} 			
		}

		private CarteiraCollection _CarteiraCollectionByIdGrupoPerfilMTM;
		#endregion

				
		#region PerfilMTMCollectionByIdGrupoPerfilMTM - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - PerfilMTM_GrupoPerfil_FK
		/// </summary>

		[XmlIgnore]
		public PerfilMTMCollection PerfilMTMCollectionByIdGrupoPerfilMTM
		{
			get
			{
				if(this._PerfilMTMCollectionByIdGrupoPerfilMTM == null)
				{
					this._PerfilMTMCollectionByIdGrupoPerfilMTM = new PerfilMTMCollection();
					this._PerfilMTMCollectionByIdGrupoPerfilMTM.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PerfilMTMCollectionByIdGrupoPerfilMTM", this._PerfilMTMCollectionByIdGrupoPerfilMTM);
				
					if(this.IdGrupoPerfilMTM != null)
					{
						this._PerfilMTMCollectionByIdGrupoPerfilMTM.Query.Where(this._PerfilMTMCollectionByIdGrupoPerfilMTM.Query.IdGrupoPerfilMTM == this.IdGrupoPerfilMTM);
						this._PerfilMTMCollectionByIdGrupoPerfilMTM.Query.Load();

						// Auto-hookup Foreign Keys
						this._PerfilMTMCollectionByIdGrupoPerfilMTM.fks.Add(PerfilMTMMetadata.ColumnNames.IdGrupoPerfilMTM, this.IdGrupoPerfilMTM);
					}
				}

				return this._PerfilMTMCollectionByIdGrupoPerfilMTM;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PerfilMTMCollectionByIdGrupoPerfilMTM != null) 
				{ 
					this.RemovePostSave("PerfilMTMCollectionByIdGrupoPerfilMTM"); 
					this._PerfilMTMCollectionByIdGrupoPerfilMTM = null;
					
				} 
			} 			
		}

		private PerfilMTMCollection _PerfilMTMCollectionByIdGrupoPerfilMTM;
		#endregion

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "CarteiraCollectionByIdGrupoPerfilMTM", typeof(CarteiraCollection), new Carteira()));
			props.Add(new esPropertyDescriptor(this, "PerfilMTMCollectionByIdGrupoPerfilMTM", typeof(PerfilMTMCollection), new PerfilMTM()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._CarteiraCollectionByIdGrupoPerfilMTM != null)
			{
				foreach(Carteira obj in this._CarteiraCollectionByIdGrupoPerfilMTM)
				{
					if(obj.es.IsAdded)
					{
						obj.IdGrupoPerfilMTM = this.IdGrupoPerfilMTM;
					}
				}
			}
			if(this._PerfilMTMCollectionByIdGrupoPerfilMTM != null)
			{
				foreach(PerfilMTM obj in this._PerfilMTMCollectionByIdGrupoPerfilMTM)
				{
					if(obj.es.IsAdded)
					{
						obj.IdGrupoPerfilMTM = this.IdGrupoPerfilMTM;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esGrupoPerfilMTMQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return GrupoPerfilMTMMetadata.Meta();
			}
		}	
		

		public esQueryItem IdGrupoPerfilMTM
		{
			get
			{
				return new esQueryItem(this, GrupoPerfilMTMMetadata.ColumnNames.IdGrupoPerfilMTM, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, GrupoPerfilMTMMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("GrupoPerfilMTMCollection")]
	public partial class GrupoPerfilMTMCollection : esGrupoPerfilMTMCollection, IEnumerable<GrupoPerfilMTM>
	{
		public GrupoPerfilMTMCollection()
		{

		}
		
		public static implicit operator List<GrupoPerfilMTM>(GrupoPerfilMTMCollection coll)
		{
			List<GrupoPerfilMTM> list = new List<GrupoPerfilMTM>();
			
			foreach (GrupoPerfilMTM emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  GrupoPerfilMTMMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new GrupoPerfilMTMQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new GrupoPerfilMTM(row);
		}

		override protected esEntity CreateEntity()
		{
			return new GrupoPerfilMTM();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public GrupoPerfilMTMQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new GrupoPerfilMTMQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(GrupoPerfilMTMQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public GrupoPerfilMTM AddNew()
		{
			GrupoPerfilMTM entity = base.AddNewEntity() as GrupoPerfilMTM;
			
			return entity;
		}

		public GrupoPerfilMTM FindByPrimaryKey(System.Int32 idGrupoPerfilMTM)
		{
			return base.FindByPrimaryKey(idGrupoPerfilMTM) as GrupoPerfilMTM;
		}


		#region IEnumerable<GrupoPerfilMTM> Members

		IEnumerator<GrupoPerfilMTM> IEnumerable<GrupoPerfilMTM>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as GrupoPerfilMTM;
			}
		}

		#endregion
		
		private GrupoPerfilMTMQuery query;
	}


	/// <summary>
	/// Encapsulates the 'GrupoPerfilMTM' table
	/// </summary>

	[Serializable]
	public partial class GrupoPerfilMTM : esGrupoPerfilMTM
	{
		public GrupoPerfilMTM()
		{

		}
	
		public GrupoPerfilMTM(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return GrupoPerfilMTMMetadata.Meta();
			}
		}
		
		
		
		override protected esGrupoPerfilMTMQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new GrupoPerfilMTMQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public GrupoPerfilMTMQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new GrupoPerfilMTMQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(GrupoPerfilMTMQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private GrupoPerfilMTMQuery query;
	}



	[Serializable]
	public partial class GrupoPerfilMTMQuery : esGrupoPerfilMTMQuery
	{
		public GrupoPerfilMTMQuery()
		{

		}		
		
		public GrupoPerfilMTMQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class GrupoPerfilMTMMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected GrupoPerfilMTMMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(GrupoPerfilMTMMetadata.ColumnNames.IdGrupoPerfilMTM, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = GrupoPerfilMTMMetadata.PropertyNames.IdGrupoPerfilMTM;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(GrupoPerfilMTMMetadata.ColumnNames.Descricao, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = GrupoPerfilMTMMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public GrupoPerfilMTMMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdGrupoPerfilMTM = "IdGrupoPerfilMTM";
			 public const string Descricao = "Descricao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdGrupoPerfilMTM = "IdGrupoPerfilMTM";
			 public const string Descricao = "Descricao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(GrupoPerfilMTMMetadata))
			{
				if(GrupoPerfilMTMMetadata.mapDelegates == null)
				{
					GrupoPerfilMTMMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (GrupoPerfilMTMMetadata.meta == null)
				{
					GrupoPerfilMTMMetadata.meta = new GrupoPerfilMTMMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdGrupoPerfilMTM", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "GrupoPerfilMTM";
				meta.Destination = "GrupoPerfilMTM";
				
				meta.spInsert = "proc_GrupoPerfilMTMInsert";				
				meta.spUpdate = "proc_GrupoPerfilMTMUpdate";		
				meta.spDelete = "proc_GrupoPerfilMTMDelete";
				meta.spLoadAll = "proc_GrupoPerfilMTMLoadAll";
				meta.spLoadByPrimaryKey = "proc_GrupoPerfilMTMLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private GrupoPerfilMTMMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
