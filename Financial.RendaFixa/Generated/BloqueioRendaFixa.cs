/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 12/12/2014 10:53:28 AM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	
	
using Financial.Investidor;










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.RendaFixa
{

	[Serializable]
	abstract public class esBloqueioRendaFixaCollection : esEntityCollection
	{
		public esBloqueioRendaFixaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "BloqueioRendaFixaCollection";
		}

		#region Query Logic
		protected void InitQuery(esBloqueioRendaFixaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esBloqueioRendaFixaQuery);
		}
		#endregion
		
		virtual public BloqueioRendaFixa DetachEntity(BloqueioRendaFixa entity)
		{
			return base.DetachEntity(entity) as BloqueioRendaFixa;
		}
		
		virtual public BloqueioRendaFixa AttachEntity(BloqueioRendaFixa entity)
		{
			return base.AttachEntity(entity) as BloqueioRendaFixa;
		}
		
		virtual public void Combine(BloqueioRendaFixaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public BloqueioRendaFixa this[int index]
		{
			get
			{
				return base[index] as BloqueioRendaFixa;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(BloqueioRendaFixa);
		}
	}



	[Serializable]
	abstract public class esBloqueioRendaFixa : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esBloqueioRendaFixaQuery GetDynamicQuery()
		{
			return null;
		}

		public esBloqueioRendaFixa()
		{

		}

		public esBloqueioRendaFixa(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idBloqueio)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idBloqueio);
			else
				return LoadByPrimaryKeyStoredProcedure(idBloqueio);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idBloqueio)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esBloqueioRendaFixaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdBloqueio == idBloqueio);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idBloqueio)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idBloqueio);
			else
				return LoadByPrimaryKeyStoredProcedure(idBloqueio);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idBloqueio)
		{
			esBloqueioRendaFixaQuery query = this.GetDynamicQuery();
			query.Where(query.IdBloqueio == idBloqueio);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idBloqueio)
		{
			esParameters parms = new esParameters();
			parms.Add("IdBloqueio",idBloqueio);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdBloqueio": this.str.IdBloqueio = (string)value; break;							
						case "DataOperacao": this.str.DataOperacao = (string)value; break;							
						case "TipoOperacao": this.str.TipoOperacao = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "IdTitulo": this.str.IdTitulo = (string)value; break;							
						case "IdPosicao": this.str.IdPosicao = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "Motivo": this.str.Motivo = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdBloqueio":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdBloqueio = (System.Int32?)value;
							break;
						
						case "DataOperacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataOperacao = (System.DateTime?)value;
							break;
						
						case "TipoOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoOperacao = (System.Byte?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "IdTitulo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTitulo = (System.Int32?)value;
							break;
						
						case "IdPosicao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPosicao = (System.Int32?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Quantidade = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to BloqueioRendaFixa.IdBloqueio
		/// </summary>
		virtual public System.Int32? IdBloqueio
		{
			get
			{
				return base.GetSystemInt32(BloqueioRendaFixaMetadata.ColumnNames.IdBloqueio);
			}
			
			set
			{
				base.SetSystemInt32(BloqueioRendaFixaMetadata.ColumnNames.IdBloqueio, value);
			}
		}
		
		/// <summary>
		/// Maps to BloqueioRendaFixa.DataOperacao
		/// </summary>
		virtual public System.DateTime? DataOperacao
		{
			get
			{
				return base.GetSystemDateTime(BloqueioRendaFixaMetadata.ColumnNames.DataOperacao);
			}
			
			set
			{
				base.SetSystemDateTime(BloqueioRendaFixaMetadata.ColumnNames.DataOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to BloqueioRendaFixa.TipoOperacao
		/// </summary>
		virtual public System.Byte? TipoOperacao
		{
			get
			{
				return base.GetSystemByte(BloqueioRendaFixaMetadata.ColumnNames.TipoOperacao);
			}
			
			set
			{
				base.SetSystemByte(BloqueioRendaFixaMetadata.ColumnNames.TipoOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to BloqueioRendaFixa.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(BloqueioRendaFixaMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				base.SetSystemInt32(BloqueioRendaFixaMetadata.ColumnNames.IdCliente, value);
			}
		}
		
		/// <summary>
		/// Maps to BloqueioRendaFixa.IdTitulo
		/// </summary>
		virtual public System.Int32? IdTitulo
		{
			get
			{
				return base.GetSystemInt32(BloqueioRendaFixaMetadata.ColumnNames.IdTitulo);
			}
			
			set
			{
				base.SetSystemInt32(BloqueioRendaFixaMetadata.ColumnNames.IdTitulo, value);
			}
		}
		
		/// <summary>
		/// Maps to BloqueioRendaFixa.IdPosicao
		/// </summary>
		virtual public System.Int32? IdPosicao
		{
			get
			{
				return base.GetSystemInt32(BloqueioRendaFixaMetadata.ColumnNames.IdPosicao);
			}
			
			set
			{
				base.SetSystemInt32(BloqueioRendaFixaMetadata.ColumnNames.IdPosicao, value);
			}
		}
		
		/// <summary>
		/// Maps to BloqueioRendaFixa.Quantidade
		/// </summary>
		virtual public System.Decimal? Quantidade
		{
			get
			{
				return base.GetSystemDecimal(BloqueioRendaFixaMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemDecimal(BloqueioRendaFixaMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to BloqueioRendaFixa.Motivo
		/// </summary>
		virtual public System.String Motivo
		{
			get
			{
				return base.GetSystemString(BloqueioRendaFixaMetadata.ColumnNames.Motivo);
			}
			
			set
			{
				base.SetSystemString(BloqueioRendaFixaMetadata.ColumnNames.Motivo, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esBloqueioRendaFixa entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdBloqueio
			{
				get
				{
					System.Int32? data = entity.IdBloqueio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdBloqueio = null;
					else entity.IdBloqueio = Convert.ToInt32(value);
				}
			}
				
			public System.String DataOperacao
			{
				get
				{
					System.DateTime? data = entity.DataOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataOperacao = null;
					else entity.DataOperacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String TipoOperacao
			{
				get
				{
					System.Byte? data = entity.TipoOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoOperacao = null;
					else entity.TipoOperacao = Convert.ToByte(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String IdTitulo
			{
				get
				{
					System.Int32? data = entity.IdTitulo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTitulo = null;
					else entity.IdTitulo = Convert.ToInt32(value);
				}
			}
				
			public System.String IdPosicao
			{
				get
				{
					System.Int32? data = entity.IdPosicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPosicao = null;
					else entity.IdPosicao = Convert.ToInt32(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Decimal? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String Motivo
			{
				get
				{
					System.String data = entity.Motivo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Motivo = null;
					else entity.Motivo = Convert.ToString(value);
				}
			}
			

			private esBloqueioRendaFixa entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esBloqueioRendaFixaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esBloqueioRendaFixa can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class BloqueioRendaFixa : esBloqueioRendaFixa
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esBloqueioRendaFixaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return BloqueioRendaFixaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdBloqueio
		{
			get
			{
				return new esQueryItem(this, BloqueioRendaFixaMetadata.ColumnNames.IdBloqueio, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataOperacao
		{
			get
			{
				return new esQueryItem(this, BloqueioRendaFixaMetadata.ColumnNames.DataOperacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem TipoOperacao
		{
			get
			{
				return new esQueryItem(this, BloqueioRendaFixaMetadata.ColumnNames.TipoOperacao, esSystemType.Byte);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, BloqueioRendaFixaMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdTitulo
		{
			get
			{
				return new esQueryItem(this, BloqueioRendaFixaMetadata.ColumnNames.IdTitulo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdPosicao
		{
			get
			{
				return new esQueryItem(this, BloqueioRendaFixaMetadata.ColumnNames.IdPosicao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, BloqueioRendaFixaMetadata.ColumnNames.Quantidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Motivo
		{
			get
			{
				return new esQueryItem(this, BloqueioRendaFixaMetadata.ColumnNames.Motivo, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("BloqueioRendaFixaCollection")]
	public partial class BloqueioRendaFixaCollection : esBloqueioRendaFixaCollection, IEnumerable<BloqueioRendaFixa>
	{
		public BloqueioRendaFixaCollection()
		{

		}
		
		public static implicit operator List<BloqueioRendaFixa>(BloqueioRendaFixaCollection coll)
		{
			List<BloqueioRendaFixa> list = new List<BloqueioRendaFixa>();
			
			foreach (BloqueioRendaFixa emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  BloqueioRendaFixaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new BloqueioRendaFixaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new BloqueioRendaFixa(row);
		}

		override protected esEntity CreateEntity()
		{
			return new BloqueioRendaFixa();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public BloqueioRendaFixaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new BloqueioRendaFixaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(BloqueioRendaFixaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public BloqueioRendaFixa AddNew()
		{
			BloqueioRendaFixa entity = base.AddNewEntity() as BloqueioRendaFixa;
			
			return entity;
		}

		public BloqueioRendaFixa FindByPrimaryKey(System.Int32 idBloqueio)
		{
			return base.FindByPrimaryKey(idBloqueio) as BloqueioRendaFixa;
		}


		#region IEnumerable<BloqueioRendaFixa> Members

		IEnumerator<BloqueioRendaFixa> IEnumerable<BloqueioRendaFixa>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as BloqueioRendaFixa;
			}
		}

		#endregion
		
		private BloqueioRendaFixaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'BloqueioRendaFixa' table
	/// </summary>

	[Serializable]
	public partial class BloqueioRendaFixa : esBloqueioRendaFixa
	{
		public BloqueioRendaFixa()
		{

		}
	
		public BloqueioRendaFixa(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return BloqueioRendaFixaMetadata.Meta();
			}
		}
		
		
		
		override protected esBloqueioRendaFixaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new BloqueioRendaFixaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public BloqueioRendaFixaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new BloqueioRendaFixaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(BloqueioRendaFixaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private BloqueioRendaFixaQuery query;
	}



	[Serializable]
	public partial class BloqueioRendaFixaQuery : esBloqueioRendaFixaQuery
	{
		public BloqueioRendaFixaQuery()
		{

		}		
		
		public BloqueioRendaFixaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class BloqueioRendaFixaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected BloqueioRendaFixaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(BloqueioRendaFixaMetadata.ColumnNames.IdBloqueio, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = BloqueioRendaFixaMetadata.PropertyNames.IdBloqueio;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(BloqueioRendaFixaMetadata.ColumnNames.DataOperacao, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = BloqueioRendaFixaMetadata.PropertyNames.DataOperacao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(BloqueioRendaFixaMetadata.ColumnNames.TipoOperacao, 2, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = BloqueioRendaFixaMetadata.PropertyNames.TipoOperacao;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(BloqueioRendaFixaMetadata.ColumnNames.IdCliente, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = BloqueioRendaFixaMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(BloqueioRendaFixaMetadata.ColumnNames.IdTitulo, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = BloqueioRendaFixaMetadata.PropertyNames.IdTitulo;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(BloqueioRendaFixaMetadata.ColumnNames.IdPosicao, 5, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = BloqueioRendaFixaMetadata.PropertyNames.IdPosicao;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(BloqueioRendaFixaMetadata.ColumnNames.Quantidade, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = BloqueioRendaFixaMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(BloqueioRendaFixaMetadata.ColumnNames.Motivo, 7, typeof(System.String), esSystemType.String);
			c.PropertyName = BloqueioRendaFixaMetadata.PropertyNames.Motivo;
			c.CharacterMaxLength = 500;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public BloqueioRendaFixaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdBloqueio = "IdBloqueio";
			 public const string DataOperacao = "DataOperacao";
			 public const string TipoOperacao = "TipoOperacao";
			 public const string IdCliente = "IdCliente";
			 public const string IdTitulo = "IdTitulo";
			 public const string IdPosicao = "IdPosicao";
			 public const string Quantidade = "Quantidade";
			 public const string Motivo = "Motivo";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdBloqueio = "IdBloqueio";
			 public const string DataOperacao = "DataOperacao";
			 public const string TipoOperacao = "TipoOperacao";
			 public const string IdCliente = "IdCliente";
			 public const string IdTitulo = "IdTitulo";
			 public const string IdPosicao = "IdPosicao";
			 public const string Quantidade = "Quantidade";
			 public const string Motivo = "Motivo";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(BloqueioRendaFixaMetadata))
			{
				if(BloqueioRendaFixaMetadata.mapDelegates == null)
				{
					BloqueioRendaFixaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (BloqueioRendaFixaMetadata.meta == null)
				{
					BloqueioRendaFixaMetadata.meta = new BloqueioRendaFixaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdBloqueio", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataOperacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("TipoOperacao", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdTitulo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdPosicao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Quantidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Motivo", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "BloqueioRendaFixa";
				meta.Destination = "BloqueioRendaFixa";
				
				meta.spInsert = "proc_BloqueioRendaFixaInsert";				
				meta.spUpdate = "proc_BloqueioRendaFixaUpdate";		
				meta.spDelete = "proc_BloqueioRendaFixaDelete";
				meta.spLoadAll = "proc_BloqueioRendaFixaLoadAll";
				meta.spLoadByPrimaryKey = "proc_BloqueioRendaFixaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private BloqueioRendaFixaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
