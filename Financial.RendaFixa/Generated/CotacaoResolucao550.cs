/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 12/12/2014 10:53:30 AM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.RendaFixa
{

	[Serializable]
	abstract public class esCotacaoResolucao550Collection : esEntityCollection
	{
		public esCotacaoResolucao550Collection()
		{

		}

		protected override string GetCollectionName()
		{
			return "CotacaoResolucao550Collection";
		}

		#region Query Logic
		protected void InitQuery(esCotacaoResolucao550Query query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esCotacaoResolucao550Query);
		}
		#endregion
		
		virtual public CotacaoResolucao550 DetachEntity(CotacaoResolucao550 entity)
		{
			return base.DetachEntity(entity) as CotacaoResolucao550;
		}
		
		virtual public CotacaoResolucao550 AttachEntity(CotacaoResolucao550 entity)
		{
			return base.AttachEntity(entity) as CotacaoResolucao550;
		}
		
		virtual public void Combine(CotacaoResolucao550Collection collection)
		{
			base.Combine(collection);
		}
		
		new public CotacaoResolucao550 this[int index]
		{
			get
			{
				return base[index] as CotacaoResolucao550;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(CotacaoResolucao550);
		}
	}



	[Serializable]
	abstract public class esCotacaoResolucao550 : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esCotacaoResolucao550Query GetDynamicQuery()
		{
			return null;
		}

		public esCotacaoResolucao550()
		{

		}

		public esCotacaoResolucao550(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime dataReferencia, System.String codigoSELIC, System.DateTime dataVencimento)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataReferencia, codigoSELIC, dataVencimento);
			else
				return LoadByPrimaryKeyStoredProcedure(dataReferencia, codigoSELIC, dataVencimento);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.DateTime dataReferencia, System.String codigoSELIC, System.DateTime dataVencimento)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esCotacaoResolucao550Query query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.DataReferencia == dataReferencia, query.CodigoSELIC == codigoSELIC, query.DataVencimento == dataVencimento);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime dataReferencia, System.String codigoSELIC, System.DateTime dataVencimento)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataReferencia, codigoSELIC, dataVencimento);
			else
				return LoadByPrimaryKeyStoredProcedure(dataReferencia, codigoSELIC, dataVencimento);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime dataReferencia, System.String codigoSELIC, System.DateTime dataVencimento)
		{
			esCotacaoResolucao550Query query = this.GetDynamicQuery();
			query.Where(query.DataReferencia == dataReferencia, query.CodigoSELIC == codigoSELIC, query.DataVencimento == dataVencimento);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime dataReferencia, System.String codigoSELIC, System.DateTime dataVencimento)
		{
			esParameters parms = new esParameters();
			parms.Add("DataReferencia",dataReferencia);			parms.Add("CodigoSELIC",codigoSELIC);			parms.Add("DataVencimento",dataVencimento);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "DataReferencia": this.str.DataReferencia = (string)value; break;							
						case "CodigoSELIC": this.str.CodigoSELIC = (string)value; break;							
						case "DataVencimento": this.str.DataVencimento = (string)value; break;							
						case "Pu": this.str.Pu = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "DataReferencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataReferencia = (System.DateTime?)value;
							break;
						
						case "DataVencimento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataVencimento = (System.DateTime?)value;
							break;
						
						case "Pu":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Pu = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to CotacaoResolucao550.DataReferencia
		/// </summary>
		virtual public System.DateTime? DataReferencia
		{
			get
			{
				return base.GetSystemDateTime(CotacaoResolucao550Metadata.ColumnNames.DataReferencia);
			}
			
			set
			{
				base.SetSystemDateTime(CotacaoResolucao550Metadata.ColumnNames.DataReferencia, value);
			}
		}
		
		/// <summary>
		/// Maps to CotacaoResolucao550.CodigoSELIC
		/// </summary>
		virtual public System.String CodigoSELIC
		{
			get
			{
				return base.GetSystemString(CotacaoResolucao550Metadata.ColumnNames.CodigoSELIC);
			}
			
			set
			{
				base.SetSystemString(CotacaoResolucao550Metadata.ColumnNames.CodigoSELIC, value);
			}
		}
		
		/// <summary>
		/// Maps to CotacaoResolucao550.DataVencimento
		/// </summary>
		virtual public System.DateTime? DataVencimento
		{
			get
			{
				return base.GetSystemDateTime(CotacaoResolucao550Metadata.ColumnNames.DataVencimento);
			}
			
			set
			{
				base.SetSystemDateTime(CotacaoResolucao550Metadata.ColumnNames.DataVencimento, value);
			}
		}
		
		/// <summary>
		/// Maps to CotacaoResolucao550.PU
		/// </summary>
		virtual public System.Decimal? Pu
		{
			get
			{
				return base.GetSystemDecimal(CotacaoResolucao550Metadata.ColumnNames.Pu);
			}
			
			set
			{
				base.SetSystemDecimal(CotacaoResolucao550Metadata.ColumnNames.Pu, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esCotacaoResolucao550 entity)
			{
				this.entity = entity;
			}
			
	
			public System.String DataReferencia
			{
				get
				{
					System.DateTime? data = entity.DataReferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataReferencia = null;
					else entity.DataReferencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String CodigoSELIC
			{
				get
				{
					System.String data = entity.CodigoSELIC;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoSELIC = null;
					else entity.CodigoSELIC = Convert.ToString(value);
				}
			}
				
			public System.String DataVencimento
			{
				get
				{
					System.DateTime? data = entity.DataVencimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataVencimento = null;
					else entity.DataVencimento = Convert.ToDateTime(value);
				}
			}
				
			public System.String Pu
			{
				get
				{
					System.Decimal? data = entity.Pu;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Pu = null;
					else entity.Pu = Convert.ToDecimal(value);
				}
			}
			

			private esCotacaoResolucao550 entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esCotacaoResolucao550Query query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esCotacaoResolucao550 can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class CotacaoResolucao550 : esCotacaoResolucao550
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esCotacaoResolucao550Query : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return CotacaoResolucao550Metadata.Meta();
			}
		}	
		

		public esQueryItem DataReferencia
		{
			get
			{
				return new esQueryItem(this, CotacaoResolucao550Metadata.ColumnNames.DataReferencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem CodigoSELIC
		{
			get
			{
				return new esQueryItem(this, CotacaoResolucao550Metadata.ColumnNames.CodigoSELIC, esSystemType.String);
			}
		} 
		
		public esQueryItem DataVencimento
		{
			get
			{
				return new esQueryItem(this, CotacaoResolucao550Metadata.ColumnNames.DataVencimento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Pu
		{
			get
			{
				return new esQueryItem(this, CotacaoResolucao550Metadata.ColumnNames.Pu, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("CotacaoResolucao550Collection")]
	public partial class CotacaoResolucao550Collection : esCotacaoResolucao550Collection, IEnumerable<CotacaoResolucao550>
	{
		public CotacaoResolucao550Collection()
		{

		}
		
		public static implicit operator List<CotacaoResolucao550>(CotacaoResolucao550Collection coll)
		{
			List<CotacaoResolucao550> list = new List<CotacaoResolucao550>();
			
			foreach (CotacaoResolucao550 emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  CotacaoResolucao550Metadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CotacaoResolucao550Query();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new CotacaoResolucao550(row);
		}

		override protected esEntity CreateEntity()
		{
			return new CotacaoResolucao550();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public CotacaoResolucao550Query Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CotacaoResolucao550Query();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(CotacaoResolucao550Query query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public CotacaoResolucao550 AddNew()
		{
			CotacaoResolucao550 entity = base.AddNewEntity() as CotacaoResolucao550;
			
			return entity;
		}

		public CotacaoResolucao550 FindByPrimaryKey(System.DateTime dataReferencia, System.String codigoSELIC, System.DateTime dataVencimento)
		{
			return base.FindByPrimaryKey(dataReferencia, codigoSELIC, dataVencimento) as CotacaoResolucao550;
		}


		#region IEnumerable<CotacaoResolucao550> Members

		IEnumerator<CotacaoResolucao550> IEnumerable<CotacaoResolucao550>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as CotacaoResolucao550;
			}
		}

		#endregion
		
		private CotacaoResolucao550Query query;
	}


	/// <summary>
	/// Encapsulates the 'CotacaoResolucao550' table
	/// </summary>

	[Serializable]
	public partial class CotacaoResolucao550 : esCotacaoResolucao550
	{
		public CotacaoResolucao550()
		{

		}
	
		public CotacaoResolucao550(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return CotacaoResolucao550Metadata.Meta();
			}
		}
		
		
		
		override protected esCotacaoResolucao550Query GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CotacaoResolucao550Query();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public CotacaoResolucao550Query Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CotacaoResolucao550Query();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(CotacaoResolucao550Query query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private CotacaoResolucao550Query query;
	}



	[Serializable]
	public partial class CotacaoResolucao550Query : esCotacaoResolucao550Query
	{
		public CotacaoResolucao550Query()
		{

		}		
		
		public CotacaoResolucao550Query(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class CotacaoResolucao550Metadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected CotacaoResolucao550Metadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(CotacaoResolucao550Metadata.ColumnNames.DataReferencia, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = CotacaoResolucao550Metadata.PropertyNames.DataReferencia;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotacaoResolucao550Metadata.ColumnNames.CodigoSELIC, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = CotacaoResolucao550Metadata.PropertyNames.CodigoSELIC;
			c.IsInPrimaryKey = true;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotacaoResolucao550Metadata.ColumnNames.DataVencimento, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = CotacaoResolucao550Metadata.PropertyNames.DataVencimento;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotacaoResolucao550Metadata.ColumnNames.Pu, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CotacaoResolucao550Metadata.PropertyNames.Pu;	
			c.NumericPrecision = 28;
			c.NumericScale = 16;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public CotacaoResolucao550Metadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string DataReferencia = "DataReferencia";
			 public const string CodigoSELIC = "CodigoSELIC";
			 public const string DataVencimento = "DataVencimento";
			 public const string Pu = "PU";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string DataReferencia = "DataReferencia";
			 public const string CodigoSELIC = "CodigoSELIC";
			 public const string DataVencimento = "DataVencimento";
			 public const string Pu = "Pu";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(CotacaoResolucao550Metadata))
			{
				if(CotacaoResolucao550Metadata.mapDelegates == null)
				{
					CotacaoResolucao550Metadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (CotacaoResolucao550Metadata.meta == null)
				{
					CotacaoResolucao550Metadata.meta = new CotacaoResolucao550Metadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("DataReferencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("CodigoSELIC", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DataVencimento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("PU", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "CotacaoResolucao550";
				meta.Destination = "CotacaoResolucao550";
				
				meta.spInsert = "proc_CotacaoResolucao550Insert";				
				meta.spUpdate = "proc_CotacaoResolucao550Update";		
				meta.spDelete = "proc_CotacaoResolucao550Delete";
				meta.spLoadAll = "proc_CotacaoResolucao550LoadAll";
				meta.spLoadByPrimaryKey = "proc_CotacaoResolucao550LoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private CotacaoResolucao550Metadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
