/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 27/10/2015 17:49:25
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.RendaFixa
{

	[Serializable]
	abstract public class esPerfilMTMCollection : esEntityCollection
	{
		public esPerfilMTMCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "PerfilMTMCollection";
		}

		#region Query Logic
		protected void InitQuery(esPerfilMTMQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esPerfilMTMQuery);
		}
		#endregion
		
		virtual public PerfilMTM DetachEntity(PerfilMTM entity)
		{
			return base.DetachEntity(entity) as PerfilMTM;
		}
		
		virtual public PerfilMTM AttachEntity(PerfilMTM entity)
		{
			return base.AttachEntity(entity) as PerfilMTM;
		}
		
		virtual public void Combine(PerfilMTMCollection collection)
		{
			base.Combine(collection);
		}
		
		new public PerfilMTM this[int index]
		{
			get
			{
				return base[index] as PerfilMTM;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(PerfilMTM);
		}
	}



	[Serializable]
	abstract public class esPerfilMTM : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esPerfilMTMQuery GetDynamicQuery()
		{
			return null;
		}

		public esPerfilMTM()
		{

		}

		public esPerfilMTM(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idPerfilMTM)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPerfilMTM);
			else
				return LoadByPrimaryKeyStoredProcedure(idPerfilMTM);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idPerfilMTM)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPerfilMTM);
			else
				return LoadByPrimaryKeyStoredProcedure(idPerfilMTM);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idPerfilMTM)
		{
			esPerfilMTMQuery query = this.GetDynamicQuery();
			query.Where(query.IdPerfilMTM == idPerfilMTM);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idPerfilMTM)
		{
			esParameters parms = new esParameters();
			parms.Add("IdPerfilMTM",idPerfilMTM);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdPerfilMTM": this.str.IdPerfilMTM = (string)value; break;							
						case "DtReferencia": this.str.DtReferencia = (string)value; break;							
						case "IdPapel": this.str.IdPapel = (string)value; break;							
						case "IdSerie": this.str.IdSerie = (string)value; break;							
						case "IdTitulo": this.str.IdTitulo = (string)value; break;							
						case "IdOperacao": this.str.IdOperacao = (string)value; break;							
						case "IdGrupoPerfilMTM": this.str.IdGrupoPerfilMTM = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdPerfilMTM":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPerfilMTM = (System.Int32?)value;
							break;
						
						case "DtReferencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DtReferencia = (System.DateTime?)value;
							break;
						
						case "IdPapel":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPapel = (System.Int32?)value;
							break;
						
						case "IdSerie":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdSerie = (System.Int32?)value;
							break;
						
						case "IdTitulo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTitulo = (System.Int32?)value;
							break;
						
						case "IdOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacao = (System.Int32?)value;
							break;
						
						case "IdGrupoPerfilMTM":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdGrupoPerfilMTM = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to PerfilMTM.IdPerfilMTM
		/// </summary>
		virtual public System.Int32? IdPerfilMTM
		{
			get
			{
				return base.GetSystemInt32(PerfilMTMMetadata.ColumnNames.IdPerfilMTM);
			}
			
			set
			{
				base.SetSystemInt32(PerfilMTMMetadata.ColumnNames.IdPerfilMTM, value);
			}
		}
		
		/// <summary>
		/// Maps to PerfilMTM.DtReferencia
		/// </summary>
		virtual public System.DateTime? DtReferencia
		{
			get
			{
				return base.GetSystemDateTime(PerfilMTMMetadata.ColumnNames.DtReferencia);
			}
			
			set
			{
				base.SetSystemDateTime(PerfilMTMMetadata.ColumnNames.DtReferencia, value);
			}
		}
		
		/// <summary>
		/// Maps to PerfilMTM.IdPapel
		/// </summary>
		virtual public System.Int32? IdPapel
		{
			get
			{
				return base.GetSystemInt32(PerfilMTMMetadata.ColumnNames.IdPapel);
			}
			
			set
			{
				if(base.SetSystemInt32(PerfilMTMMetadata.ColumnNames.IdPapel, value))
				{
					this._UpToPapelRendaFixaByIdPapel = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PerfilMTM.IdSerie
		/// </summary>
		virtual public System.Int32? IdSerie
		{
			get
			{
				return base.GetSystemInt32(PerfilMTMMetadata.ColumnNames.IdSerie);
			}
			
			set
			{
				if(base.SetSystemInt32(PerfilMTMMetadata.ColumnNames.IdSerie, value))
				{
					this._UpToSerieRendaFixaByIdSerie = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PerfilMTM.IdTitulo
		/// </summary>
		virtual public System.Int32? IdTitulo
		{
			get
			{
				return base.GetSystemInt32(PerfilMTMMetadata.ColumnNames.IdTitulo);
			}
			
			set
			{
				if(base.SetSystemInt32(PerfilMTMMetadata.ColumnNames.IdTitulo, value))
				{
					this._UpToTituloRendaFixaByIdTitulo = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PerfilMTM.IdOperacao
		/// </summary>
		virtual public System.Int32? IdOperacao
		{
			get
			{
				return base.GetSystemInt32(PerfilMTMMetadata.ColumnNames.IdOperacao);
			}
			
			set
			{
				if(base.SetSystemInt32(PerfilMTMMetadata.ColumnNames.IdOperacao, value))
				{
					this._UpToOperacaoRendaFixaByIdOperacao = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PerfilMTM.IdGrupoPerfilMTM
		/// </summary>
		virtual public System.Int32? IdGrupoPerfilMTM
		{
			get
			{
				return base.GetSystemInt32(PerfilMTMMetadata.ColumnNames.IdGrupoPerfilMTM);
			}
			
			set
			{
				if(base.SetSystemInt32(PerfilMTMMetadata.ColumnNames.IdGrupoPerfilMTM, value))
				{
					this._UpToGrupoPerfilMTMByIdGrupoPerfilMTM = null;
				}
			}
		}
		
		[CLSCompliant(false)]
		internal protected GrupoPerfilMTM _UpToGrupoPerfilMTMByIdGrupoPerfilMTM;
		[CLSCompliant(false)]
		internal protected OperacaoRendaFixa _UpToOperacaoRendaFixaByIdOperacao;
		[CLSCompliant(false)]
		internal protected PapelRendaFixa _UpToPapelRendaFixaByIdPapel;
		[CLSCompliant(false)]
		internal protected SerieRendaFixa _UpToSerieRendaFixaByIdSerie;
		[CLSCompliant(false)]
		internal protected TituloRendaFixa _UpToTituloRendaFixaByIdTitulo;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esPerfilMTM entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdPerfilMTM
			{
				get
				{
					System.Int32? data = entity.IdPerfilMTM;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPerfilMTM = null;
					else entity.IdPerfilMTM = Convert.ToInt32(value);
				}
			}
				
			public System.String DtReferencia
			{
				get
				{
					System.DateTime? data = entity.DtReferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DtReferencia = null;
					else entity.DtReferencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdPapel
			{
				get
				{
					System.Int32? data = entity.IdPapel;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPapel = null;
					else entity.IdPapel = Convert.ToInt32(value);
				}
			}
				
			public System.String IdSerie
			{
				get
				{
					System.Int32? data = entity.IdSerie;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdSerie = null;
					else entity.IdSerie = Convert.ToInt32(value);
				}
			}
				
			public System.String IdTitulo
			{
				get
				{
					System.Int32? data = entity.IdTitulo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTitulo = null;
					else entity.IdTitulo = Convert.ToInt32(value);
				}
			}
				
			public System.String IdOperacao
			{
				get
				{
					System.Int32? data = entity.IdOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacao = null;
					else entity.IdOperacao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdGrupoPerfilMTM
			{
				get
				{
					System.Int32? data = entity.IdGrupoPerfilMTM;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdGrupoPerfilMTM = null;
					else entity.IdGrupoPerfilMTM = Convert.ToInt32(value);
				}
			}
			

			private esPerfilMTM entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esPerfilMTMQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esPerfilMTM can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class PerfilMTM : esPerfilMTM
	{

				
		#region UpToGrupoPerfilMTMByIdGrupoPerfilMTM - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - PerfilMTM_GrupoPerfil_FK
		/// </summary>

		[XmlIgnore]
		public GrupoPerfilMTM UpToGrupoPerfilMTMByIdGrupoPerfilMTM
		{
			get
			{
				if(this._UpToGrupoPerfilMTMByIdGrupoPerfilMTM == null
					&& IdGrupoPerfilMTM != null					)
				{
					this._UpToGrupoPerfilMTMByIdGrupoPerfilMTM = new GrupoPerfilMTM();
					this._UpToGrupoPerfilMTMByIdGrupoPerfilMTM.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToGrupoPerfilMTMByIdGrupoPerfilMTM", this._UpToGrupoPerfilMTMByIdGrupoPerfilMTM);
					this._UpToGrupoPerfilMTMByIdGrupoPerfilMTM.Query.Where(this._UpToGrupoPerfilMTMByIdGrupoPerfilMTM.Query.IdGrupoPerfilMTM == this.IdGrupoPerfilMTM);
					this._UpToGrupoPerfilMTMByIdGrupoPerfilMTM.Query.Load();
				}

				return this._UpToGrupoPerfilMTMByIdGrupoPerfilMTM;
			}
			
			set
			{
				this.RemovePreSave("UpToGrupoPerfilMTMByIdGrupoPerfilMTM");
				

				if(value == null)
				{
					this.IdGrupoPerfilMTM = null;
					this._UpToGrupoPerfilMTMByIdGrupoPerfilMTM = null;
				}
				else
				{
					this.IdGrupoPerfilMTM = value.IdGrupoPerfilMTM;
					this._UpToGrupoPerfilMTMByIdGrupoPerfilMTM = value;
					this.SetPreSave("UpToGrupoPerfilMTMByIdGrupoPerfilMTM", this._UpToGrupoPerfilMTMByIdGrupoPerfilMTM);
				}
				
			}
		}
		#endregion
		

				
		#region UpToOperacaoRendaFixaByIdOperacao - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - PerfilMTM_Operacao_FK
		/// </summary>

		[XmlIgnore]
		public OperacaoRendaFixa UpToOperacaoRendaFixaByIdOperacao
		{
			get
			{
				if(this._UpToOperacaoRendaFixaByIdOperacao == null
					&& IdOperacao != null					)
				{
					this._UpToOperacaoRendaFixaByIdOperacao = new OperacaoRendaFixa();
					this._UpToOperacaoRendaFixaByIdOperacao.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToOperacaoRendaFixaByIdOperacao", this._UpToOperacaoRendaFixaByIdOperacao);
					this._UpToOperacaoRendaFixaByIdOperacao.Query.Where(this._UpToOperacaoRendaFixaByIdOperacao.Query.IdOperacao == this.IdOperacao);
					this._UpToOperacaoRendaFixaByIdOperacao.Query.Load();
				}

				return this._UpToOperacaoRendaFixaByIdOperacao;
			}
			
			set
			{
				this.RemovePreSave("UpToOperacaoRendaFixaByIdOperacao");
				

				if(value == null)
				{
					this.IdOperacao = null;
					this._UpToOperacaoRendaFixaByIdOperacao = null;
				}
				else
				{
					this.IdOperacao = value.IdOperacao;
					this._UpToOperacaoRendaFixaByIdOperacao = value;
					this.SetPreSave("UpToOperacaoRendaFixaByIdOperacao", this._UpToOperacaoRendaFixaByIdOperacao);
				}
				
			}
		}
		#endregion
		

				
		#region UpToPapelRendaFixaByIdPapel - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - PerfilMTM_Papel_FK
		/// </summary>

		[XmlIgnore]
		public PapelRendaFixa UpToPapelRendaFixaByIdPapel
		{
			get
			{
				if(this._UpToPapelRendaFixaByIdPapel == null
					&& IdPapel != null					)
				{
					this._UpToPapelRendaFixaByIdPapel = new PapelRendaFixa();
					this._UpToPapelRendaFixaByIdPapel.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToPapelRendaFixaByIdPapel", this._UpToPapelRendaFixaByIdPapel);
					this._UpToPapelRendaFixaByIdPapel.Query.Where(this._UpToPapelRendaFixaByIdPapel.Query.IdPapel == this.IdPapel);
					this._UpToPapelRendaFixaByIdPapel.Query.Load();
				}

				return this._UpToPapelRendaFixaByIdPapel;
			}
			
			set
			{
				this.RemovePreSave("UpToPapelRendaFixaByIdPapel");
				

				if(value == null)
				{
					this.IdPapel = null;
					this._UpToPapelRendaFixaByIdPapel = null;
				}
				else
				{
					this.IdPapel = value.IdPapel;
					this._UpToPapelRendaFixaByIdPapel = value;
					this.SetPreSave("UpToPapelRendaFixaByIdPapel", this._UpToPapelRendaFixaByIdPapel);
				}
				
			}
		}
		#endregion
		

				
		#region UpToSerieRendaFixaByIdSerie - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - PerfilMTM_Serie_FK
		/// </summary>

		[XmlIgnore]
		public SerieRendaFixa UpToSerieRendaFixaByIdSerie
		{
			get
			{
				if(this._UpToSerieRendaFixaByIdSerie == null
					&& IdSerie != null					)
				{
					this._UpToSerieRendaFixaByIdSerie = new SerieRendaFixa();
					this._UpToSerieRendaFixaByIdSerie.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToSerieRendaFixaByIdSerie", this._UpToSerieRendaFixaByIdSerie);
					this._UpToSerieRendaFixaByIdSerie.Query.Where(this._UpToSerieRendaFixaByIdSerie.Query.IdSerie == this.IdSerie);
					this._UpToSerieRendaFixaByIdSerie.Query.Load();
				}

				return this._UpToSerieRendaFixaByIdSerie;
			}
			
			set
			{
				this.RemovePreSave("UpToSerieRendaFixaByIdSerie");
				

				if(value == null)
				{
					this.IdSerie = null;
					this._UpToSerieRendaFixaByIdSerie = null;
				}
				else
				{
					this.IdSerie = value.IdSerie;
					this._UpToSerieRendaFixaByIdSerie = value;
					this.SetPreSave("UpToSerieRendaFixaByIdSerie", this._UpToSerieRendaFixaByIdSerie);
				}
				
			}
		}
		#endregion
		

				
		#region UpToTituloRendaFixaByIdTitulo - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - PerfilMTM_Titulo_FK
		/// </summary>

		[XmlIgnore]
		public TituloRendaFixa UpToTituloRendaFixaByIdTitulo
		{
			get
			{
				if(this._UpToTituloRendaFixaByIdTitulo == null
					&& IdTitulo != null					)
				{
					this._UpToTituloRendaFixaByIdTitulo = new TituloRendaFixa();
					this._UpToTituloRendaFixaByIdTitulo.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToTituloRendaFixaByIdTitulo", this._UpToTituloRendaFixaByIdTitulo);
					this._UpToTituloRendaFixaByIdTitulo.Query.Where(this._UpToTituloRendaFixaByIdTitulo.Query.IdTitulo == this.IdTitulo);
					this._UpToTituloRendaFixaByIdTitulo.Query.Load();
				}

				return this._UpToTituloRendaFixaByIdTitulo;
			}
			
			set
			{
				this.RemovePreSave("UpToTituloRendaFixaByIdTitulo");
				

				if(value == null)
				{
					this.IdTitulo = null;
					this._UpToTituloRendaFixaByIdTitulo = null;
				}
				else
				{
					this.IdTitulo = value.IdTitulo;
					this._UpToTituloRendaFixaByIdTitulo = value;
					this.SetPreSave("UpToTituloRendaFixaByIdTitulo", this._UpToTituloRendaFixaByIdTitulo);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToGrupoPerfilMTMByIdGrupoPerfilMTM != null)
			{
				this.IdGrupoPerfilMTM = this._UpToGrupoPerfilMTMByIdGrupoPerfilMTM.IdGrupoPerfilMTM;
			}
			if(!this.es.IsDeleted && this._UpToOperacaoRendaFixaByIdOperacao != null)
			{
				this.IdOperacao = this._UpToOperacaoRendaFixaByIdOperacao.IdOperacao;
			}
			if(!this.es.IsDeleted && this._UpToPapelRendaFixaByIdPapel != null)
			{
				this.IdPapel = this._UpToPapelRendaFixaByIdPapel.IdPapel;
			}
			if(!this.es.IsDeleted && this._UpToSerieRendaFixaByIdSerie != null)
			{
				this.IdSerie = this._UpToSerieRendaFixaByIdSerie.IdSerie;
			}
			if(!this.es.IsDeleted && this._UpToTituloRendaFixaByIdTitulo != null)
			{
				this.IdTitulo = this._UpToTituloRendaFixaByIdTitulo.IdTitulo;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esPerfilMTMQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return PerfilMTMMetadata.Meta();
			}
		}	
		

		public esQueryItem IdPerfilMTM
		{
			get
			{
				return new esQueryItem(this, PerfilMTMMetadata.ColumnNames.IdPerfilMTM, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DtReferencia
		{
			get
			{
				return new esQueryItem(this, PerfilMTMMetadata.ColumnNames.DtReferencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdPapel
		{
			get
			{
				return new esQueryItem(this, PerfilMTMMetadata.ColumnNames.IdPapel, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdSerie
		{
			get
			{
				return new esQueryItem(this, PerfilMTMMetadata.ColumnNames.IdSerie, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdTitulo
		{
			get
			{
				return new esQueryItem(this, PerfilMTMMetadata.ColumnNames.IdTitulo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdOperacao
		{
			get
			{
				return new esQueryItem(this, PerfilMTMMetadata.ColumnNames.IdOperacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdGrupoPerfilMTM
		{
			get
			{
				return new esQueryItem(this, PerfilMTMMetadata.ColumnNames.IdGrupoPerfilMTM, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("PerfilMTMCollection")]
	public partial class PerfilMTMCollection : esPerfilMTMCollection, IEnumerable<PerfilMTM>
	{
		public PerfilMTMCollection()
		{

		}
		
		public static implicit operator List<PerfilMTM>(PerfilMTMCollection coll)
		{
			List<PerfilMTM> list = new List<PerfilMTM>();
			
			foreach (PerfilMTM emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  PerfilMTMMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PerfilMTMQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new PerfilMTM(row);
		}

		override protected esEntity CreateEntity()
		{
			return new PerfilMTM();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public PerfilMTMQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PerfilMTMQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(PerfilMTMQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public PerfilMTM AddNew()
		{
			PerfilMTM entity = base.AddNewEntity() as PerfilMTM;
			
			return entity;
		}

		public PerfilMTM FindByPrimaryKey(System.Int32 idPerfilMTM)
		{
			return base.FindByPrimaryKey(idPerfilMTM) as PerfilMTM;
		}


		#region IEnumerable<PerfilMTM> Members

		IEnumerator<PerfilMTM> IEnumerable<PerfilMTM>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as PerfilMTM;
			}
		}

		#endregion
		
		private PerfilMTMQuery query;
	}


	/// <summary>
	/// Encapsulates the 'PerfilMTM' table
	/// </summary>

	[Serializable]
	public partial class PerfilMTM : esPerfilMTM
	{
		public PerfilMTM()
		{

		}
	
		public PerfilMTM(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return PerfilMTMMetadata.Meta();
			}
		}
		
		
		
		override protected esPerfilMTMQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PerfilMTMQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public PerfilMTMQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PerfilMTMQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(PerfilMTMQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private PerfilMTMQuery query;
	}



	[Serializable]
	public partial class PerfilMTMQuery : esPerfilMTMQuery
	{
		public PerfilMTMQuery()
		{

		}		
		
		public PerfilMTMQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class PerfilMTMMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected PerfilMTMMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(PerfilMTMMetadata.ColumnNames.IdPerfilMTM, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PerfilMTMMetadata.PropertyNames.IdPerfilMTM;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PerfilMTMMetadata.ColumnNames.DtReferencia, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PerfilMTMMetadata.PropertyNames.DtReferencia;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PerfilMTMMetadata.ColumnNames.IdPapel, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PerfilMTMMetadata.PropertyNames.IdPapel;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PerfilMTMMetadata.ColumnNames.IdSerie, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PerfilMTMMetadata.PropertyNames.IdSerie;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PerfilMTMMetadata.ColumnNames.IdTitulo, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PerfilMTMMetadata.PropertyNames.IdTitulo;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PerfilMTMMetadata.ColumnNames.IdOperacao, 5, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PerfilMTMMetadata.PropertyNames.IdOperacao;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PerfilMTMMetadata.ColumnNames.IdGrupoPerfilMTM, 6, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PerfilMTMMetadata.PropertyNames.IdGrupoPerfilMTM;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public PerfilMTMMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdPerfilMTM = "IdPerfilMTM";
			 public const string DtReferencia = "DtReferencia";
			 public const string IdPapel = "IdPapel";
			 public const string IdSerie = "IdSerie";
			 public const string IdTitulo = "IdTitulo";
			 public const string IdOperacao = "IdOperacao";
			 public const string IdGrupoPerfilMTM = "IdGrupoPerfilMTM";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdPerfilMTM = "IdPerfilMTM";
			 public const string DtReferencia = "DtReferencia";
			 public const string IdPapel = "IdPapel";
			 public const string IdSerie = "IdSerie";
			 public const string IdTitulo = "IdTitulo";
			 public const string IdOperacao = "IdOperacao";
			 public const string IdGrupoPerfilMTM = "IdGrupoPerfilMTM";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(PerfilMTMMetadata))
			{
				if(PerfilMTMMetadata.mapDelegates == null)
				{
					PerfilMTMMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (PerfilMTMMetadata.meta == null)
				{
					PerfilMTMMetadata.meta = new PerfilMTMMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdPerfilMTM", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DtReferencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdPapel", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdSerie", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdTitulo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdOperacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdGrupoPerfilMTM", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "PerfilMTM";
				meta.Destination = "PerfilMTM";
				
				meta.spInsert = "proc_PerfilMTMInsert";				
				meta.spUpdate = "proc_PerfilMTMUpdate";		
				meta.spDelete = "proc_PerfilMTMDelete";
				meta.spLoadAll = "proc_PerfilMTMLoadAll";
				meta.spLoadByPrimaryKey = "proc_PerfilMTMLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private PerfilMTMMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
