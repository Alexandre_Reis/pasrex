/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 04/12/2014 15:45:46
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.RendaFixa
{

	[Serializable]
	abstract public class esRepactuacaoRendaFixaCollection : esEntityCollection
	{
		public esRepactuacaoRendaFixaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "RepactuacaoRendaFixaCollection";
		}

		#region Query Logic
		protected void InitQuery(esRepactuacaoRendaFixaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esRepactuacaoRendaFixaQuery);
		}
		#endregion
		
		virtual public RepactuacaoRendaFixa DetachEntity(RepactuacaoRendaFixa entity)
		{
			return base.DetachEntity(entity) as RepactuacaoRendaFixa;
		}
		
		virtual public RepactuacaoRendaFixa AttachEntity(RepactuacaoRendaFixa entity)
		{
			return base.AttachEntity(entity) as RepactuacaoRendaFixa;
		}
		
		virtual public void Combine(RepactuacaoRendaFixaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public RepactuacaoRendaFixa this[int index]
		{
			get
			{
				return base[index] as RepactuacaoRendaFixa;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(RepactuacaoRendaFixa);
		}
	}



	[Serializable]
	abstract public class esRepactuacaoRendaFixa : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esRepactuacaoRendaFixaQuery GetDynamicQuery()
		{
			return null;
		}

		public esRepactuacaoRendaFixa()
		{

		}

		public esRepactuacaoRendaFixa(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idRepactuacao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idRepactuacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idRepactuacao);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idRepactuacao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idRepactuacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idRepactuacao);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idRepactuacao)
		{
			esRepactuacaoRendaFixaQuery query = this.GetDynamicQuery();
			query.Where(query.IdRepactuacao == idRepactuacao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idRepactuacao)
		{
			esParameters parms = new esParameters();
			parms.Add("IdRepactuacao",idRepactuacao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdRepactuacao": this.str.IdRepactuacao = (string)value; break;							
						case "IdTituloOriginal": this.str.IdTituloOriginal = (string)value; break;							
						case "IdTituloNovo": this.str.IdTituloNovo = (string)value; break;							
						case "DataEvento": this.str.DataEvento = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdRepactuacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdRepactuacao = (System.Int32?)value;
							break;
						
						case "IdTituloOriginal":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTituloOriginal = (System.Int32?)value;
							break;
						
						case "IdTituloNovo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTituloNovo = (System.Int32?)value;
							break;
						
						case "DataEvento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataEvento = (System.DateTime?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to RepactuacaoRendaFixa.IdRepactuacao
		/// </summary>
		virtual public System.Int32? IdRepactuacao
		{
			get
			{
				return base.GetSystemInt32(RepactuacaoRendaFixaMetadata.ColumnNames.IdRepactuacao);
			}
			
			set
			{
				base.SetSystemInt32(RepactuacaoRendaFixaMetadata.ColumnNames.IdRepactuacao, value);
			}
		}
		
		/// <summary>
		/// Maps to RepactuacaoRendaFixa.IdTituloOriginal
		/// </summary>
		virtual public System.Int32? IdTituloOriginal
		{
			get
			{
				return base.GetSystemInt32(RepactuacaoRendaFixaMetadata.ColumnNames.IdTituloOriginal);
			}
			
			set
			{
				if(base.SetSystemInt32(RepactuacaoRendaFixaMetadata.ColumnNames.IdTituloOriginal, value))
				{
					this._UpToTituloRendaFixaByIdTituloOriginal = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to RepactuacaoRendaFixa.IdTituloNovo
		/// </summary>
		virtual public System.Int32? IdTituloNovo
		{
			get
			{
				return base.GetSystemInt32(RepactuacaoRendaFixaMetadata.ColumnNames.IdTituloNovo);
			}
			
			set
			{
				if(base.SetSystemInt32(RepactuacaoRendaFixaMetadata.ColumnNames.IdTituloNovo, value))
				{
					this._UpToTituloRendaFixaByIdTituloNovo = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to RepactuacaoRendaFixa.DataEvento
		/// </summary>
		virtual public System.DateTime? DataEvento
		{
			get
			{
				return base.GetSystemDateTime(RepactuacaoRendaFixaMetadata.ColumnNames.DataEvento);
			}
			
			set
			{
				base.SetSystemDateTime(RepactuacaoRendaFixaMetadata.ColumnNames.DataEvento, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected TituloRendaFixa _UpToTituloRendaFixaByIdTituloOriginal;
		[CLSCompliant(false)]
		internal protected TituloRendaFixa _UpToTituloRendaFixaByIdTituloNovo;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esRepactuacaoRendaFixa entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdRepactuacao
			{
				get
				{
					System.Int32? data = entity.IdRepactuacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdRepactuacao = null;
					else entity.IdRepactuacao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdTituloOriginal
			{
				get
				{
					System.Int32? data = entity.IdTituloOriginal;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTituloOriginal = null;
					else entity.IdTituloOriginal = Convert.ToInt32(value);
				}
			}
				
			public System.String IdTituloNovo
			{
				get
				{
					System.Int32? data = entity.IdTituloNovo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTituloNovo = null;
					else entity.IdTituloNovo = Convert.ToInt32(value);
				}
			}
				
			public System.String DataEvento
			{
				get
				{
					System.DateTime? data = entity.DataEvento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataEvento = null;
					else entity.DataEvento = Convert.ToDateTime(value);
				}
			}
			

			private esRepactuacaoRendaFixa entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esRepactuacaoRendaFixaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esRepactuacaoRendaFixa can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class RepactuacaoRendaFixa : esRepactuacaoRendaFixa
	{

				
		#region UpToTituloRendaFixaByIdTituloOriginal - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Repac_PapelOrig_FK
		/// </summary>

		[XmlIgnore]
		public TituloRendaFixa UpToTituloRendaFixaByIdTituloOriginal
		{
			get
			{
				if(this._UpToTituloRendaFixaByIdTituloOriginal == null
					&& IdTituloOriginal != null					)
				{
					this._UpToTituloRendaFixaByIdTituloOriginal = new TituloRendaFixa();
					this._UpToTituloRendaFixaByIdTituloOriginal.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToTituloRendaFixaByIdTituloOriginal", this._UpToTituloRendaFixaByIdTituloOriginal);
					this._UpToTituloRendaFixaByIdTituloOriginal.Query.Where(this._UpToTituloRendaFixaByIdTituloOriginal.Query.IdTitulo == this.IdTituloOriginal);
					this._UpToTituloRendaFixaByIdTituloOriginal.Query.Load();
				}

				return this._UpToTituloRendaFixaByIdTituloOriginal;
			}
			
			set
			{
				this.RemovePreSave("UpToTituloRendaFixaByIdTituloOriginal");
				

				if(value == null)
				{
					this.IdTituloOriginal = null;
					this._UpToTituloRendaFixaByIdTituloOriginal = null;
				}
				else
				{
					this.IdTituloOriginal = value.IdTitulo;
					this._UpToTituloRendaFixaByIdTituloOriginal = value;
					this.SetPreSave("UpToTituloRendaFixaByIdTituloOriginal", this._UpToTituloRendaFixaByIdTituloOriginal);
				}
				
			}
		}
		#endregion
		

				
		#region UpToTituloRendaFixaByIdTituloNovo - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Repac_PapelNovo_FK
		/// </summary>

		[XmlIgnore]
		public TituloRendaFixa UpToTituloRendaFixaByIdTituloNovo
		{
			get
			{
				if(this._UpToTituloRendaFixaByIdTituloNovo == null
					&& IdTituloNovo != null					)
				{
					this._UpToTituloRendaFixaByIdTituloNovo = new TituloRendaFixa();
					this._UpToTituloRendaFixaByIdTituloNovo.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToTituloRendaFixaByIdTituloNovo", this._UpToTituloRendaFixaByIdTituloNovo);
					this._UpToTituloRendaFixaByIdTituloNovo.Query.Where(this._UpToTituloRendaFixaByIdTituloNovo.Query.IdTitulo == this.IdTituloNovo);
					this._UpToTituloRendaFixaByIdTituloNovo.Query.Load();
				}

				return this._UpToTituloRendaFixaByIdTituloNovo;
			}
			
			set
			{
				this.RemovePreSave("UpToTituloRendaFixaByIdTituloNovo");
				

				if(value == null)
				{
					this.IdTituloNovo = null;
					this._UpToTituloRendaFixaByIdTituloNovo = null;
				}
				else
				{
					this.IdTituloNovo = value.IdTitulo;
					this._UpToTituloRendaFixaByIdTituloNovo = value;
					this.SetPreSave("UpToTituloRendaFixaByIdTituloNovo", this._UpToTituloRendaFixaByIdTituloNovo);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToTituloRendaFixaByIdTituloOriginal != null)
			{
				this.IdTituloOriginal = this._UpToTituloRendaFixaByIdTituloOriginal.IdTitulo;
			}
			if(!this.es.IsDeleted && this._UpToTituloRendaFixaByIdTituloNovo != null)
			{
				this.IdTituloNovo = this._UpToTituloRendaFixaByIdTituloNovo.IdTitulo;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esRepactuacaoRendaFixaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return RepactuacaoRendaFixaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdRepactuacao
		{
			get
			{
				return new esQueryItem(this, RepactuacaoRendaFixaMetadata.ColumnNames.IdRepactuacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdTituloOriginal
		{
			get
			{
				return new esQueryItem(this, RepactuacaoRendaFixaMetadata.ColumnNames.IdTituloOriginal, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdTituloNovo
		{
			get
			{
				return new esQueryItem(this, RepactuacaoRendaFixaMetadata.ColumnNames.IdTituloNovo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataEvento
		{
			get
			{
				return new esQueryItem(this, RepactuacaoRendaFixaMetadata.ColumnNames.DataEvento, esSystemType.DateTime);
			}
		} 
		
	}



	[Serializable]
	[XmlType("RepactuacaoRendaFixaCollection")]
	public partial class RepactuacaoRendaFixaCollection : esRepactuacaoRendaFixaCollection, IEnumerable<RepactuacaoRendaFixa>
	{
		public RepactuacaoRendaFixaCollection()
		{

		}
		
		public static implicit operator List<RepactuacaoRendaFixa>(RepactuacaoRendaFixaCollection coll)
		{
			List<RepactuacaoRendaFixa> list = new List<RepactuacaoRendaFixa>();
			
			foreach (RepactuacaoRendaFixa emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  RepactuacaoRendaFixaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new RepactuacaoRendaFixaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new RepactuacaoRendaFixa(row);
		}

		override protected esEntity CreateEntity()
		{
			return new RepactuacaoRendaFixa();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public RepactuacaoRendaFixaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new RepactuacaoRendaFixaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(RepactuacaoRendaFixaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public RepactuacaoRendaFixa AddNew()
		{
			RepactuacaoRendaFixa entity = base.AddNewEntity() as RepactuacaoRendaFixa;
			
			return entity;
		}

		public RepactuacaoRendaFixa FindByPrimaryKey(System.Int32 idRepactuacao)
		{
			return base.FindByPrimaryKey(idRepactuacao) as RepactuacaoRendaFixa;
		}


		#region IEnumerable<RepactuacaoRendaFixa> Members

		IEnumerator<RepactuacaoRendaFixa> IEnumerable<RepactuacaoRendaFixa>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as RepactuacaoRendaFixa;
			}
		}

		#endregion
		
		private RepactuacaoRendaFixaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'RepactuacaoRendaFixa' table
	/// </summary>

	[Serializable]
	public partial class RepactuacaoRendaFixa : esRepactuacaoRendaFixa
	{
		public RepactuacaoRendaFixa()
		{

		}
	
		public RepactuacaoRendaFixa(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return RepactuacaoRendaFixaMetadata.Meta();
			}
		}
		
		
		
		override protected esRepactuacaoRendaFixaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new RepactuacaoRendaFixaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public RepactuacaoRendaFixaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new RepactuacaoRendaFixaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(RepactuacaoRendaFixaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private RepactuacaoRendaFixaQuery query;
	}



	[Serializable]
	public partial class RepactuacaoRendaFixaQuery : esRepactuacaoRendaFixaQuery
	{
		public RepactuacaoRendaFixaQuery()
		{

		}		
		
		public RepactuacaoRendaFixaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class RepactuacaoRendaFixaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected RepactuacaoRendaFixaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(RepactuacaoRendaFixaMetadata.ColumnNames.IdRepactuacao, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = RepactuacaoRendaFixaMetadata.PropertyNames.IdRepactuacao;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RepactuacaoRendaFixaMetadata.ColumnNames.IdTituloOriginal, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = RepactuacaoRendaFixaMetadata.PropertyNames.IdTituloOriginal;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RepactuacaoRendaFixaMetadata.ColumnNames.IdTituloNovo, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = RepactuacaoRendaFixaMetadata.PropertyNames.IdTituloNovo;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(RepactuacaoRendaFixaMetadata.ColumnNames.DataEvento, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = RepactuacaoRendaFixaMetadata.PropertyNames.DataEvento;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public RepactuacaoRendaFixaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdRepactuacao = "IdRepactuacao";
			 public const string IdTituloOriginal = "IdTituloOriginal";
			 public const string IdTituloNovo = "IdTituloNovo";
			 public const string DataEvento = "DataEvento";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdRepactuacao = "IdRepactuacao";
			 public const string IdTituloOriginal = "IdTituloOriginal";
			 public const string IdTituloNovo = "IdTituloNovo";
			 public const string DataEvento = "DataEvento";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(RepactuacaoRendaFixaMetadata))
			{
				if(RepactuacaoRendaFixaMetadata.mapDelegates == null)
				{
					RepactuacaoRendaFixaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (RepactuacaoRendaFixaMetadata.meta == null)
				{
					RepactuacaoRendaFixaMetadata.meta = new RepactuacaoRendaFixaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdRepactuacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdTituloOriginal", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdTituloNovo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataEvento", new esTypeMap("datetime", "System.DateTime"));			
				
				
				
				meta.Source = "RepactuacaoRendaFixa";
				meta.Destination = "RepactuacaoRendaFixa";
				
				meta.spInsert = "proc_RepactuacaoRendaFixaInsert";				
				meta.spUpdate = "proc_RepactuacaoRendaFixaUpdate";		
				meta.spDelete = "proc_RepactuacaoRendaFixaDelete";
				meta.spLoadAll = "proc_RepactuacaoRendaFixaLoadAll";
				meta.spLoadByPrimaryKey = "proc_RepactuacaoRendaFixaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private RepactuacaoRendaFixaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
