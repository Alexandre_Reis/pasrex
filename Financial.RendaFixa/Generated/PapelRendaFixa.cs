/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 15/05/2015 14:39:44
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	




		
using Financial.Enquadra;






		





		




				
				








				




		

		
		
		
		
		





namespace Financial.RendaFixa
{

	[Serializable]
	abstract public class esPapelRendaFixaCollection : esEntityCollection
	{
		public esPapelRendaFixaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "PapelRendaFixaCollection";
		}

		#region Query Logic
		protected void InitQuery(esPapelRendaFixaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esPapelRendaFixaQuery);
		}
		#endregion
		
		virtual public PapelRendaFixa DetachEntity(PapelRendaFixa entity)
		{
			return base.DetachEntity(entity) as PapelRendaFixa;
		}
		
		virtual public PapelRendaFixa AttachEntity(PapelRendaFixa entity)
		{
			return base.AttachEntity(entity) as PapelRendaFixa;
		}
		
		virtual public void Combine(PapelRendaFixaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public PapelRendaFixa this[int index]
		{
			get
			{
				return base[index] as PapelRendaFixa;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(PapelRendaFixa);
		}
	}



	[Serializable]
	abstract public class esPapelRendaFixa : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esPapelRendaFixaQuery GetDynamicQuery()
		{
			return null;
		}

		public esPapelRendaFixa()
		{

		}

		public esPapelRendaFixa(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idPapel)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPapel);
			else
				return LoadByPrimaryKeyStoredProcedure(idPapel);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idPapel)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esPapelRendaFixaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdPapel == idPapel);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idPapel)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPapel);
			else
				return LoadByPrimaryKeyStoredProcedure(idPapel);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idPapel)
		{
			esPapelRendaFixaQuery query = this.GetDynamicQuery();
			query.Where(query.IdPapel == idPapel);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idPapel)
		{
			esParameters parms = new esParameters();
			parms.Add("IdPapel",idPapel);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdPapel": this.str.IdPapel = (string)value; break;							
						case "TipoPapel": this.str.TipoPapel = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;							
						case "TipoRentabilidade": this.str.TipoRentabilidade = (string)value; break;							
						case "CasasDecimaisPU": this.str.CasasDecimaisPU = (string)value; break;							
						case "TipoCurva": this.str.TipoCurva = (string)value; break;							
						case "ContagemDias": this.str.ContagemDias = (string)value; break;							
						case "BaseAno": this.str.BaseAno = (string)value; break;							
						case "TipoVolume": this.str.TipoVolume = (string)value; break;							
						case "TipoCustodia": this.str.TipoCustodia = (string)value; break;							
						case "Classe": this.str.Classe = (string)value; break;							
						case "PagamentoJuros": this.str.PagamentoJuros = (string)value; break;							
						case "CodigoInterface": this.str.CodigoInterface = (string)value; break;							
						case "IdLocalCustodia": this.str.IdLocalCustodia = (string)value; break;							
						case "IdClearing": this.str.IdClearing = (string)value; break;							
						case "IdLocalNegociacao": this.str.IdLocalNegociacao = (string)value; break;							
						case "IsentoIR": this.str.IsentoIR = (string)value; break;
                        case "InvestimentoColetivoCvm": this.str.InvestimentoColetivoCvm = (string)value; break;
                    }
				}
				else
				{
					switch (name)
					{	
						case "IdPapel":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPapel = (System.Int32?)value;
							break;
						
						case "TipoPapel":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoPapel = (System.Byte?)value;
							break;
						
						case "TipoRentabilidade":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoRentabilidade = (System.Byte?)value;
							break;
						
						case "CasasDecimaisPU":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.CasasDecimaisPU = (System.Int16?)value;
							break;
						
						case "TipoCurva":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoCurva = (System.Byte?)value;
							break;
						
						case "ContagemDias":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.ContagemDias = (System.Byte?)value;
							break;
						
						case "BaseAno":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.BaseAno = (System.Int32?)value;
							break;
						
						case "TipoVolume":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoVolume = (System.Byte?)value;
							break;
						
						case "TipoCustodia":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoCustodia = (System.Byte?)value;
							break;
						
						case "Classe":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Classe = (System.Int32?)value;
							break;
						
						case "PagamentoJuros":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.PagamentoJuros = (System.Byte?)value;
							break;
						
						case "IdLocalCustodia":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdLocalCustodia = (System.Int32?)value;
							break;
						
						case "IdClearing":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdClearing = (System.Int32?)value;
							break;
						
						case "IdLocalNegociacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdLocalNegociacao = (System.Int32?)value;
							break;
						
						case "IsentoIR":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IsentoIR = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to PapelRendaFixa.IdPapel
		/// </summary>
		virtual public System.Int32? IdPapel
		{
			get
			{
				return base.GetSystemInt32(PapelRendaFixaMetadata.ColumnNames.IdPapel);
			}
			
			set
			{
				base.SetSystemInt32(PapelRendaFixaMetadata.ColumnNames.IdPapel, value);
			}
		}
		
		/// <summary>
		/// Maps to PapelRendaFixa.TipoPapel
		/// </summary>
		virtual public System.Byte? TipoPapel
		{
			get
			{
				return base.GetSystemByte(PapelRendaFixaMetadata.ColumnNames.TipoPapel);
			}
			
			set
			{
				base.SetSystemByte(PapelRendaFixaMetadata.ColumnNames.TipoPapel, value);
			}
		}
		
		/// <summary>
		/// Maps to PapelRendaFixa.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(PapelRendaFixaMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(PapelRendaFixaMetadata.ColumnNames.Descricao, value);
			}
		}
		
		/// <summary>
		/// Maps to PapelRendaFixa.TipoRentabilidade
		/// </summary>
		virtual public System.Byte? TipoRentabilidade
		{
			get
			{
				return base.GetSystemByte(PapelRendaFixaMetadata.ColumnNames.TipoRentabilidade);
			}
			
			set
			{
				base.SetSystemByte(PapelRendaFixaMetadata.ColumnNames.TipoRentabilidade, value);
			}
		}
		
		/// <summary>
		/// Maps to PapelRendaFixa.CasasDecimaisPU
		/// </summary>
		virtual public System.Int16? CasasDecimaisPU
		{
			get
			{
				return base.GetSystemInt16(PapelRendaFixaMetadata.ColumnNames.CasasDecimaisPU);
			}
			
			set
			{
				base.SetSystemInt16(PapelRendaFixaMetadata.ColumnNames.CasasDecimaisPU, value);
			}
		}
		
		/// <summary>
		/// Maps to PapelRendaFixa.TipoCurva
		/// </summary>
		virtual public System.Byte? TipoCurva
		{
			get
			{
				return base.GetSystemByte(PapelRendaFixaMetadata.ColumnNames.TipoCurva);
			}
			
			set
			{
				base.SetSystemByte(PapelRendaFixaMetadata.ColumnNames.TipoCurva, value);
			}
		}
		
		/// <summary>
		/// Maps to PapelRendaFixa.ContagemDias
		/// </summary>
		virtual public System.Byte? ContagemDias
		{
			get
			{
				return base.GetSystemByte(PapelRendaFixaMetadata.ColumnNames.ContagemDias);
			}
			
			set
			{
				base.SetSystemByte(PapelRendaFixaMetadata.ColumnNames.ContagemDias, value);
			}
		}
		
		/// <summary>
		/// Maps to PapelRendaFixa.BaseAno
		/// </summary>
		virtual public System.Int32? BaseAno
		{
			get
			{
				return base.GetSystemInt32(PapelRendaFixaMetadata.ColumnNames.BaseAno);
			}
			
			set
			{
				base.SetSystemInt32(PapelRendaFixaMetadata.ColumnNames.BaseAno, value);
			}
		}
		
		/// <summary>
		/// Maps to PapelRendaFixa.TipoVolume
		/// </summary>
		virtual public System.Byte? TipoVolume
		{
			get
			{
				return base.GetSystemByte(PapelRendaFixaMetadata.ColumnNames.TipoVolume);
			}
			
			set
			{
				base.SetSystemByte(PapelRendaFixaMetadata.ColumnNames.TipoVolume, value);
			}
		}
		
		/// <summary>
		/// Maps to PapelRendaFixa.TipoCustodia
		/// </summary>
		virtual public System.Byte? TipoCustodia
		{
			get
			{
				return base.GetSystemByte(PapelRendaFixaMetadata.ColumnNames.TipoCustodia);
			}
			
			set
			{
				base.SetSystemByte(PapelRendaFixaMetadata.ColumnNames.TipoCustodia, value);
			}
		}
		
		/// <summary>
		/// Maps to PapelRendaFixa.Classe
		/// </summary>
		virtual public System.Int32? Classe
		{
			get
			{
				return base.GetSystemInt32(PapelRendaFixaMetadata.ColumnNames.Classe);
			}
			
			set
			{
				base.SetSystemInt32(PapelRendaFixaMetadata.ColumnNames.Classe, value);
			}
		}
		
		/// <summary>
		/// Maps to PapelRendaFixa.PagamentoJuros
		/// </summary>
		virtual public System.Byte? PagamentoJuros
		{
			get
			{
				return base.GetSystemByte(PapelRendaFixaMetadata.ColumnNames.PagamentoJuros);
			}
			
			set
			{
				base.SetSystemByte(PapelRendaFixaMetadata.ColumnNames.PagamentoJuros, value);
			}
		}
		
		/// <summary>
		/// Maps to PapelRendaFixa.CodigoInterface
		/// </summary>
		virtual public System.String CodigoInterface
		{
			get
			{
				return base.GetSystemString(PapelRendaFixaMetadata.ColumnNames.CodigoInterface);
			}
			
			set
			{
				base.SetSystemString(PapelRendaFixaMetadata.ColumnNames.CodigoInterface, value);
			}
		}
		
		/// <summary>
		/// Maps to PapelRendaFixa.IdLocalCustodia
		/// </summary>
		virtual public System.Int32? IdLocalCustodia
		{
			get
			{
				return base.GetSystemInt32(PapelRendaFixaMetadata.ColumnNames.IdLocalCustodia);
			}
			
			set
			{
				base.SetSystemInt32(PapelRendaFixaMetadata.ColumnNames.IdLocalCustodia, value);
			}
		}
		
		/// <summary>
		/// Maps to PapelRendaFixa.IdClearing
		/// </summary>
		virtual public System.Int32? IdClearing
		{
			get
			{
				return base.GetSystemInt32(PapelRendaFixaMetadata.ColumnNames.IdClearing);
			}
			
			set
			{
				base.SetSystemInt32(PapelRendaFixaMetadata.ColumnNames.IdClearing, value);
			}
		}
		
		/// <summary>
		/// Maps to PapelRendaFixa.IdLocalNegociacao
		/// </summary>
		virtual public System.Int32? IdLocalNegociacao
		{
			get
			{
				return base.GetSystemInt32(PapelRendaFixaMetadata.ColumnNames.IdLocalNegociacao);
			}
			
			set
			{
				base.SetSystemInt32(PapelRendaFixaMetadata.ColumnNames.IdLocalNegociacao, value);
			}
		}
		
		/// <summary>
		/// Maps to PapelRendaFixa.IsentoIR
		/// </summary>
		virtual public System.Int32? IsentoIR
		{
			get
			{
				return base.GetSystemInt32(PapelRendaFixaMetadata.ColumnNames.IsentoIR);
			}
			
			set
			{
				base.SetSystemInt32(PapelRendaFixaMetadata.ColumnNames.IsentoIR, value);
			}
		}

        /// <summary>
        /// Maps to PapelRendaFixa.InvestimentoColetivoCvm
        /// </summary>
        virtual public System.String InvestimentoColetivoCvm
        {
            get
            {
                return base.GetSystemString(PapelRendaFixaMetadata.ColumnNames.InvestimentoColetivoCvm);
            }

            set
            {
                base.SetSystemString(PapelRendaFixaMetadata.ColumnNames.InvestimentoColetivoCvm, value);
            }
        }

        #endregion

        #region String Properties


        [BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esPapelRendaFixa entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdPapel
			{
				get
				{
					System.Int32? data = entity.IdPapel;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPapel = null;
					else entity.IdPapel = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoPapel
			{
				get
				{
					System.Byte? data = entity.TipoPapel;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoPapel = null;
					else entity.TipoPapel = Convert.ToByte(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
				
			public System.String TipoRentabilidade
			{
				get
				{
					System.Byte? data = entity.TipoRentabilidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoRentabilidade = null;
					else entity.TipoRentabilidade = Convert.ToByte(value);
				}
			}
				
			public System.String CasasDecimaisPU
			{
				get
				{
					System.Int16? data = entity.CasasDecimaisPU;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CasasDecimaisPU = null;
					else entity.CasasDecimaisPU = Convert.ToInt16(value);
				}
			}
				
			public System.String TipoCurva
			{
				get
				{
					System.Byte? data = entity.TipoCurva;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoCurva = null;
					else entity.TipoCurva = Convert.ToByte(value);
				}
			}
				
			public System.String ContagemDias
			{
				get
				{
					System.Byte? data = entity.ContagemDias;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ContagemDias = null;
					else entity.ContagemDias = Convert.ToByte(value);
				}
			}
				
			public System.String BaseAno
			{
				get
				{
					System.Int32? data = entity.BaseAno;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.BaseAno = null;
					else entity.BaseAno = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoVolume
			{
				get
				{
					System.Byte? data = entity.TipoVolume;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoVolume = null;
					else entity.TipoVolume = Convert.ToByte(value);
				}
			}
				
			public System.String TipoCustodia
			{
				get
				{
					System.Byte? data = entity.TipoCustodia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoCustodia = null;
					else entity.TipoCustodia = Convert.ToByte(value);
				}
			}
				
			public System.String Classe
			{
				get
				{
					System.Int32? data = entity.Classe;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Classe = null;
					else entity.Classe = Convert.ToInt32(value);
				}
			}
				
			public System.String PagamentoJuros
			{
				get
				{
					System.Byte? data = entity.PagamentoJuros;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PagamentoJuros = null;
					else entity.PagamentoJuros = Convert.ToByte(value);
				}
			}
				
			public System.String CodigoInterface
			{
				get
				{
					System.String data = entity.CodigoInterface;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoInterface = null;
					else entity.CodigoInterface = Convert.ToString(value);
				}
			}
				
			public System.String IdLocalCustodia
			{
				get
				{
					System.Int32? data = entity.IdLocalCustodia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdLocalCustodia = null;
					else entity.IdLocalCustodia = Convert.ToInt32(value);
				}
			}
				
			public System.String IdClearing
			{
				get
				{
					System.Int32? data = entity.IdClearing;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdClearing = null;
					else entity.IdClearing = Convert.ToInt32(value);
				}
			}
				
			public System.String IdLocalNegociacao
			{
				get
				{
					System.Int32? data = entity.IdLocalNegociacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdLocalNegociacao = null;
					else entity.IdLocalNegociacao = Convert.ToInt32(value);
				}
			}
				
			public System.String IsentoIR
			{
				get
				{
					System.Int32? data = entity.IsentoIR;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IsentoIR = null;
					else entity.IsentoIR = Convert.ToInt32(value);
				}
			}

            public System.String InvestimentoColetivoCvm
            {
                get
                {
                    System.String data = entity.InvestimentoColetivoCvm;
                    return (data == null) ? String.Empty : Convert.ToString(data);
                }

                set
                {
                    if (value == null || value.Length == 0) entity.InvestimentoColetivoCvm = null;
                    else entity.InvestimentoColetivoCvm = Convert.ToString(value);
                }
            }

            private esPapelRendaFixa entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esPapelRendaFixaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esPapelRendaFixa can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class PapelRendaFixa : esPapelRendaFixa
	{

				
		#region EnquadraItemRendaFixaCollectionByIdPapel - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - PapelRendaFixa_EnquadraItemRendaFixa_FK1
		/// </summary>

		[XmlIgnore]
		public EnquadraItemRendaFixaCollection EnquadraItemRendaFixaCollectionByIdPapel
		{
			get
			{
				if(this._EnquadraItemRendaFixaCollectionByIdPapel == null)
				{
					this._EnquadraItemRendaFixaCollectionByIdPapel = new EnquadraItemRendaFixaCollection();
					this._EnquadraItemRendaFixaCollectionByIdPapel.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("EnquadraItemRendaFixaCollectionByIdPapel", this._EnquadraItemRendaFixaCollectionByIdPapel);
				
					if(this.IdPapel != null)
					{
						this._EnquadraItemRendaFixaCollectionByIdPapel.Query.Where(this._EnquadraItemRendaFixaCollectionByIdPapel.Query.IdPapel == this.IdPapel);
						this._EnquadraItemRendaFixaCollectionByIdPapel.Query.Load();

						// Auto-hookup Foreign Keys
						this._EnquadraItemRendaFixaCollectionByIdPapel.fks.Add(EnquadraItemRendaFixaMetadata.ColumnNames.IdPapel, this.IdPapel);
					}
				}

				return this._EnquadraItemRendaFixaCollectionByIdPapel;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._EnquadraItemRendaFixaCollectionByIdPapel != null) 
				{ 
					this.RemovePostSave("EnquadraItemRendaFixaCollectionByIdPapel"); 
					this._EnquadraItemRendaFixaCollectionByIdPapel = null;
					
				} 
			} 			
		}

		private EnquadraItemRendaFixaCollection _EnquadraItemRendaFixaCollectionByIdPapel;
		#endregion

				
		#region PerfilMTMCollectionByIdPapel - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - PerfilMTM_Papel_FK
		/// </summary>

		[XmlIgnore]
		public PerfilMTMCollection PerfilMTMCollectionByIdPapel
		{
			get
			{
				if(this._PerfilMTMCollectionByIdPapel == null)
				{
					this._PerfilMTMCollectionByIdPapel = new PerfilMTMCollection();
					this._PerfilMTMCollectionByIdPapel.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PerfilMTMCollectionByIdPapel", this._PerfilMTMCollectionByIdPapel);
				
					if(this.IdPapel != null)
					{
						this._PerfilMTMCollectionByIdPapel.Query.Where(this._PerfilMTMCollectionByIdPapel.Query.IdPapel == this.IdPapel);
						this._PerfilMTMCollectionByIdPapel.Query.Load();

						// Auto-hookup Foreign Keys
						this._PerfilMTMCollectionByIdPapel.fks.Add(PerfilMTMMetadata.ColumnNames.IdPapel, this.IdPapel);
					}
				}

				return this._PerfilMTMCollectionByIdPapel;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PerfilMTMCollectionByIdPapel != null) 
				{ 
					this.RemovePostSave("PerfilMTMCollectionByIdPapel"); 
					this._PerfilMTMCollectionByIdPapel = null;
					
				} 
			} 			
		}

		private PerfilMTMCollection _PerfilMTMCollectionByIdPapel;
		#endregion

				
		#region TituloRendaFixaCollectionByIdPapel - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - PapelRendaFixa_TituloRendaFixa_FK1
		/// </summary>

		[XmlIgnore]
		public TituloRendaFixaCollection TituloRendaFixaCollectionByIdPapel
		{
			get
			{
				if(this._TituloRendaFixaCollectionByIdPapel == null)
				{
					this._TituloRendaFixaCollectionByIdPapel = new TituloRendaFixaCollection();
					this._TituloRendaFixaCollectionByIdPapel.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("TituloRendaFixaCollectionByIdPapel", this._TituloRendaFixaCollectionByIdPapel);
				
					if(this.IdPapel != null)
					{
						this._TituloRendaFixaCollectionByIdPapel.Query.Where(this._TituloRendaFixaCollectionByIdPapel.Query.IdPapel == this.IdPapel);
						this._TituloRendaFixaCollectionByIdPapel.Query.Load();

						// Auto-hookup Foreign Keys
						this._TituloRendaFixaCollectionByIdPapel.fks.Add(TituloRendaFixaMetadata.ColumnNames.IdPapel, this.IdPapel);
					}
				}

				return this._TituloRendaFixaCollectionByIdPapel;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._TituloRendaFixaCollectionByIdPapel != null) 
				{ 
					this.RemovePostSave("TituloRendaFixaCollectionByIdPapel"); 
					this._TituloRendaFixaCollectionByIdPapel = null;
					
				} 
			} 			
		}

		private TituloRendaFixaCollection _TituloRendaFixaCollectionByIdPapel;
		#endregion

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "EnquadraItemRendaFixaCollectionByIdPapel", typeof(EnquadraItemRendaFixaCollection), new EnquadraItemRendaFixa()));
			props.Add(new esPropertyDescriptor(this, "PerfilMTMCollectionByIdPapel", typeof(PerfilMTMCollection), new PerfilMTM()));
			props.Add(new esPropertyDescriptor(this, "TituloRendaFixaCollectionByIdPapel", typeof(TituloRendaFixaCollection), new TituloRendaFixa()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._EnquadraItemRendaFixaCollectionByIdPapel != null)
			{
				foreach(EnquadraItemRendaFixa obj in this._EnquadraItemRendaFixaCollectionByIdPapel)
				{
					if(obj.es.IsAdded)
					{
						obj.IdPapel = this.IdPapel;
					}
				}
			}
			if(this._PerfilMTMCollectionByIdPapel != null)
			{
				foreach(PerfilMTM obj in this._PerfilMTMCollectionByIdPapel)
				{
					if(obj.es.IsAdded)
					{
						obj.IdPapel = this.IdPapel;
					}
				}
			}
			if(this._TituloRendaFixaCollectionByIdPapel != null)
			{
				foreach(TituloRendaFixa obj in this._TituloRendaFixaCollectionByIdPapel)
				{
					if(obj.es.IsAdded)
					{
						obj.IdPapel = this.IdPapel;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esPapelRendaFixaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return PapelRendaFixaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdPapel
		{
			get
			{
				return new esQueryItem(this, PapelRendaFixaMetadata.ColumnNames.IdPapel, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoPapel
		{
			get
			{
				return new esQueryItem(this, PapelRendaFixaMetadata.ColumnNames.TipoPapel, esSystemType.Byte);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, PapelRendaFixaMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
		public esQueryItem TipoRentabilidade
		{
			get
			{
				return new esQueryItem(this, PapelRendaFixaMetadata.ColumnNames.TipoRentabilidade, esSystemType.Byte);
			}
		} 
		
		public esQueryItem CasasDecimaisPU
		{
			get
			{
				return new esQueryItem(this, PapelRendaFixaMetadata.ColumnNames.CasasDecimaisPU, esSystemType.Int16);
			}
		} 
		
		public esQueryItem TipoCurva
		{
			get
			{
				return new esQueryItem(this, PapelRendaFixaMetadata.ColumnNames.TipoCurva, esSystemType.Byte);
			}
		} 
		
		public esQueryItem ContagemDias
		{
			get
			{
				return new esQueryItem(this, PapelRendaFixaMetadata.ColumnNames.ContagemDias, esSystemType.Byte);
			}
		} 
		
		public esQueryItem BaseAno
		{
			get
			{
				return new esQueryItem(this, PapelRendaFixaMetadata.ColumnNames.BaseAno, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoVolume
		{
			get
			{
				return new esQueryItem(this, PapelRendaFixaMetadata.ColumnNames.TipoVolume, esSystemType.Byte);
			}
		} 
		
		public esQueryItem TipoCustodia
		{
			get
			{
				return new esQueryItem(this, PapelRendaFixaMetadata.ColumnNames.TipoCustodia, esSystemType.Byte);
			}
		} 
		
		public esQueryItem Classe
		{
			get
			{
				return new esQueryItem(this, PapelRendaFixaMetadata.ColumnNames.Classe, esSystemType.Int32);
			}
		} 
		
		public esQueryItem PagamentoJuros
		{
			get
			{
				return new esQueryItem(this, PapelRendaFixaMetadata.ColumnNames.PagamentoJuros, esSystemType.Byte);
			}
		} 
		
		public esQueryItem CodigoInterface
		{
			get
			{
				return new esQueryItem(this, PapelRendaFixaMetadata.ColumnNames.CodigoInterface, esSystemType.String);
			}
		} 
		
		public esQueryItem IdLocalCustodia
		{
			get
			{
				return new esQueryItem(this, PapelRendaFixaMetadata.ColumnNames.IdLocalCustodia, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdClearing
		{
			get
			{
				return new esQueryItem(this, PapelRendaFixaMetadata.ColumnNames.IdClearing, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdLocalNegociacao
		{
			get
			{
				return new esQueryItem(this, PapelRendaFixaMetadata.ColumnNames.IdLocalNegociacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IsentoIR
		{
			get
			{
				return new esQueryItem(this, PapelRendaFixaMetadata.ColumnNames.IsentoIR, esSystemType.Int32);
			}
		}

        public esQueryItem InvestimentoColetivoCvm
        {
            get
            {
                return new esQueryItem(this, PapelRendaFixaMetadata.ColumnNames.InvestimentoColetivoCvm, esSystemType.String);
            }
        }

    }



	[Serializable]
	[XmlType("PapelRendaFixaCollection")]
	public partial class PapelRendaFixaCollection : esPapelRendaFixaCollection, IEnumerable<PapelRendaFixa>
	{
		public PapelRendaFixaCollection()
		{

		}
		
		public static implicit operator List<PapelRendaFixa>(PapelRendaFixaCollection coll)
		{
			List<PapelRendaFixa> list = new List<PapelRendaFixa>();
			
			foreach (PapelRendaFixa emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  PapelRendaFixaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PapelRendaFixaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new PapelRendaFixa(row);
		}

		override protected esEntity CreateEntity()
		{
			return new PapelRendaFixa();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public PapelRendaFixaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PapelRendaFixaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(PapelRendaFixaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public PapelRendaFixa AddNew()
		{
			PapelRendaFixa entity = base.AddNewEntity() as PapelRendaFixa;
			
			return entity;
		}

		public PapelRendaFixa FindByPrimaryKey(System.Int32 idPapel)
		{
			return base.FindByPrimaryKey(idPapel) as PapelRendaFixa;
		}


		#region IEnumerable<PapelRendaFixa> Members

		IEnumerator<PapelRendaFixa> IEnumerable<PapelRendaFixa>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as PapelRendaFixa;
			}
		}

		#endregion
		
		private PapelRendaFixaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'PapelRendaFixa' table
	/// </summary>

	[Serializable]
	public partial class PapelRendaFixa : esPapelRendaFixa
	{
		public PapelRendaFixa()
		{

		}
	
		public PapelRendaFixa(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return PapelRendaFixaMetadata.Meta();
			}
		}
		
		
		
		override protected esPapelRendaFixaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PapelRendaFixaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public PapelRendaFixaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PapelRendaFixaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(PapelRendaFixaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private PapelRendaFixaQuery query;
	}



	[Serializable]
	public partial class PapelRendaFixaQuery : esPapelRendaFixaQuery
	{
		public PapelRendaFixaQuery()
		{

		}		
		
		public PapelRendaFixaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class PapelRendaFixaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected PapelRendaFixaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(PapelRendaFixaMetadata.ColumnNames.IdPapel, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PapelRendaFixaMetadata.PropertyNames.IdPapel;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PapelRendaFixaMetadata.ColumnNames.TipoPapel, 1, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = PapelRendaFixaMetadata.PropertyNames.TipoPapel;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PapelRendaFixaMetadata.ColumnNames.Descricao, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = PapelRendaFixaMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 100;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PapelRendaFixaMetadata.ColumnNames.TipoRentabilidade, 3, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = PapelRendaFixaMetadata.PropertyNames.TipoRentabilidade;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PapelRendaFixaMetadata.ColumnNames.CasasDecimaisPU, 4, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = PapelRendaFixaMetadata.PropertyNames.CasasDecimaisPU;	
			c.NumericPrecision = 5;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PapelRendaFixaMetadata.ColumnNames.TipoCurva, 5, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = PapelRendaFixaMetadata.PropertyNames.TipoCurva;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PapelRendaFixaMetadata.ColumnNames.ContagemDias, 6, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = PapelRendaFixaMetadata.PropertyNames.ContagemDias;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PapelRendaFixaMetadata.ColumnNames.BaseAno, 7, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PapelRendaFixaMetadata.PropertyNames.BaseAno;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PapelRendaFixaMetadata.ColumnNames.TipoVolume, 8, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = PapelRendaFixaMetadata.PropertyNames.TipoVolume;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PapelRendaFixaMetadata.ColumnNames.TipoCustodia, 9, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = PapelRendaFixaMetadata.PropertyNames.TipoCustodia;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PapelRendaFixaMetadata.ColumnNames.Classe, 10, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PapelRendaFixaMetadata.PropertyNames.Classe;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PapelRendaFixaMetadata.ColumnNames.PagamentoJuros, 11, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = PapelRendaFixaMetadata.PropertyNames.PagamentoJuros;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PapelRendaFixaMetadata.ColumnNames.CodigoInterface, 12, typeof(System.String), esSystemType.String);
			c.PropertyName = PapelRendaFixaMetadata.PropertyNames.CodigoInterface;
			c.CharacterMaxLength = 40;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PapelRendaFixaMetadata.ColumnNames.IdLocalCustodia, 13, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PapelRendaFixaMetadata.PropertyNames.IdLocalCustodia;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PapelRendaFixaMetadata.ColumnNames.IdClearing, 14, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PapelRendaFixaMetadata.PropertyNames.IdClearing;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PapelRendaFixaMetadata.ColumnNames.IdLocalNegociacao, 15, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PapelRendaFixaMetadata.PropertyNames.IdLocalNegociacao;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PapelRendaFixaMetadata.ColumnNames.IsentoIR, 16, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PapelRendaFixaMetadata.PropertyNames.IsentoIR;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c);

            c = new esColumnMetadata(PapelRendaFixaMetadata.ColumnNames.InvestimentoColetivoCvm, 17, typeof(System.String), esSystemType.String);
            c.PropertyName = PapelRendaFixaMetadata.PropertyNames.InvestimentoColetivoCvm;
            c.CharacterMaxLength = 1;
            c.NumericPrecision = 0;
            c.HasDefault = true;
            c.Default = @"('N')";
            _columns.Add(c);

        }
		#endregion
	
		static public PapelRendaFixaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdPapel = "IdPapel";
			 public const string TipoPapel = "TipoPapel";
			 public const string Descricao = "Descricao";
			 public const string TipoRentabilidade = "TipoRentabilidade";
			 public const string CasasDecimaisPU = "CasasDecimaisPU";
			 public const string TipoCurva = "TipoCurva";
			 public const string ContagemDias = "ContagemDias";
			 public const string BaseAno = "BaseAno";
			 public const string TipoVolume = "TipoVolume";
			 public const string TipoCustodia = "TipoCustodia";
			 public const string Classe = "Classe";
			 public const string PagamentoJuros = "PagamentoJuros";
			 public const string CodigoInterface = "CodigoInterface";
			 public const string IdLocalCustodia = "IdLocalCustodia";
			 public const string IdClearing = "IdClearing";
			 public const string IdLocalNegociacao = "IdLocalNegociacao";
			 public const string IsentoIR = "IsentoIR";
             public const string InvestimentoColetivoCvm = "InvestimentoColetivoCvm";
        }
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdPapel = "IdPapel";
			 public const string TipoPapel = "TipoPapel";
			 public const string Descricao = "Descricao";
			 public const string TipoRentabilidade = "TipoRentabilidade";
			 public const string CasasDecimaisPU = "CasasDecimaisPU";
			 public const string TipoCurva = "TipoCurva";
			 public const string ContagemDias = "ContagemDias";
			 public const string BaseAno = "BaseAno";
			 public const string TipoVolume = "TipoVolume";
			 public const string TipoCustodia = "TipoCustodia";
			 public const string Classe = "Classe";
			 public const string PagamentoJuros = "PagamentoJuros";
			 public const string CodigoInterface = "CodigoInterface";
			 public const string IdLocalCustodia = "IdLocalCustodia";
			 public const string IdClearing = "IdClearing";
			 public const string IdLocalNegociacao = "IdLocalNegociacao";
			 public const string IsentoIR = "IsentoIR";
             public const string InvestimentoColetivoCvm = "InvestimentoColetivoCvm";
        }
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(PapelRendaFixaMetadata))
			{
				if(PapelRendaFixaMetadata.mapDelegates == null)
				{
					PapelRendaFixaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (PapelRendaFixaMetadata.meta == null)
				{
					PapelRendaFixaMetadata.meta = new PapelRendaFixaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdPapel", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoPapel", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TipoRentabilidade", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("CasasDecimaisPU", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("TipoCurva", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("ContagemDias", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("BaseAno", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoVolume", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("TipoCustodia", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("Classe", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("PagamentoJuros", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("CodigoInterface", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IdLocalCustodia", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdClearing", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdLocalNegociacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IsentoIR", new esTypeMap("int", "System.Int32"));
                meta.AddTypeMap("InvestimentoColetivoCvm", new esTypeMap("char", "System.String"));



                meta.Source = "PapelRendaFixa";
				meta.Destination = "PapelRendaFixa";
				
				meta.spInsert = "proc_PapelRendaFixaInsert";				
				meta.spUpdate = "proc_PapelRendaFixaUpdate";		
				meta.spDelete = "proc_PapelRendaFixaDelete";
				meta.spLoadAll = "proc_PapelRendaFixaLoadAll";
				meta.spLoadByPrimaryKey = "proc_PapelRendaFixaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private PapelRendaFixaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
