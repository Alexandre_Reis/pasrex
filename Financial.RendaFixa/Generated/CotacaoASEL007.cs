/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 23/12/2014 14:37:17
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.RendaFixa
{

	[Serializable]
	abstract public class esCotacaoASEL007Collection : esEntityCollection
	{
		public esCotacaoASEL007Collection()
		{

		}

		protected override string GetCollectionName()
		{
			return "CotacaoASEL007Collection";
		}

		#region Query Logic
		protected void InitQuery(esCotacaoASEL007Query query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esCotacaoASEL007Query);
		}
		#endregion
		
		virtual public CotacaoASEL007 DetachEntity(CotacaoASEL007 entity)
		{
			return base.DetachEntity(entity) as CotacaoASEL007;
		}
		
		virtual public CotacaoASEL007 AttachEntity(CotacaoASEL007 entity)
		{
			return base.AttachEntity(entity) as CotacaoASEL007;
		}
		
		virtual public void Combine(CotacaoASEL007Collection collection)
		{
			base.Combine(collection);
		}
		
		new public CotacaoASEL007 this[int index]
		{
			get
			{
				return base[index] as CotacaoASEL007;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(CotacaoASEL007);
		}
	}



	[Serializable]
	abstract public class esCotacaoASEL007 : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esCotacaoASEL007Query GetDynamicQuery()
		{
			return null;
		}

		public esCotacaoASEL007()
		{

		}

		public esCotacaoASEL007(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey()
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic();
			else
				return LoadByPrimaryKeyStoredProcedure();
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType )
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic();
			else
				return LoadByPrimaryKeyStoredProcedure();
		}

		private bool LoadByPrimaryKeyDynamic()
		{
			esCotacaoASEL007Query query = this.GetDynamicQuery();
			query.Where();
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure()
		{
			esParameters parms = new esParameters();

			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "Data": this.str.Data = (string)value; break;							
						case "CodigoTitulo": this.str.CodigoTitulo = (string)value; break;							
						case "DataVencimento": this.str.DataVencimento = (string)value; break;							
						case "Pu": this.str.Pu = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "DataVencimento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataVencimento = (System.DateTime?)value;
							break;
						
						case "Pu":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Pu = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to CotacaoASEL007.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(CotacaoASEL007Metadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(CotacaoASEL007Metadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to CotacaoASEL007.CodigoTitulo
		/// </summary>
		virtual public System.String CodigoTitulo
		{
			get
			{
				return base.GetSystemString(CotacaoASEL007Metadata.ColumnNames.CodigoTitulo);
			}
			
			set
			{
				base.SetSystemString(CotacaoASEL007Metadata.ColumnNames.CodigoTitulo, value);
			}
		}
		
		/// <summary>
		/// Maps to CotacaoASEL007.DataVencimento
		/// </summary>
		virtual public System.DateTime? DataVencimento
		{
			get
			{
				return base.GetSystemDateTime(CotacaoASEL007Metadata.ColumnNames.DataVencimento);
			}
			
			set
			{
				base.SetSystemDateTime(CotacaoASEL007Metadata.ColumnNames.DataVencimento, value);
			}
		}
		
		/// <summary>
		/// Maps to CotacaoASEL007.PU
		/// </summary>
		virtual public System.Decimal? Pu
		{
			get
			{
				return base.GetSystemDecimal(CotacaoASEL007Metadata.ColumnNames.Pu);
			}
			
			set
			{
				base.SetSystemDecimal(CotacaoASEL007Metadata.ColumnNames.Pu, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esCotacaoASEL007 entity)
			{
				this.entity = entity;
			}
			
	
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String CodigoTitulo
			{
				get
				{
					System.String data = entity.CodigoTitulo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoTitulo = null;
					else entity.CodigoTitulo = Convert.ToString(value);
				}
			}
				
			public System.String DataVencimento
			{
				get
				{
					System.DateTime? data = entity.DataVencimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataVencimento = null;
					else entity.DataVencimento = Convert.ToDateTime(value);
				}
			}
				
			public System.String Pu
			{
				get
				{
					System.Decimal? data = entity.Pu;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Pu = null;
					else entity.Pu = Convert.ToDecimal(value);
				}
			}
			

			private esCotacaoASEL007 entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esCotacaoASEL007Query query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esCotacaoASEL007 can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class CotacaoASEL007 : esCotacaoASEL007
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esCotacaoASEL007Query : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return CotacaoASEL007Metadata.Meta();
			}
		}	
		

		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, CotacaoASEL007Metadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem CodigoTitulo
		{
			get
			{
				return new esQueryItem(this, CotacaoASEL007Metadata.ColumnNames.CodigoTitulo, esSystemType.String);
			}
		} 
		
		public esQueryItem DataVencimento
		{
			get
			{
				return new esQueryItem(this, CotacaoASEL007Metadata.ColumnNames.DataVencimento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Pu
		{
			get
			{
				return new esQueryItem(this, CotacaoASEL007Metadata.ColumnNames.Pu, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("CotacaoASEL007Collection")]
	public partial class CotacaoASEL007Collection : esCotacaoASEL007Collection, IEnumerable<CotacaoASEL007>
	{
		public CotacaoASEL007Collection()
		{

		}
		
		public static implicit operator List<CotacaoASEL007>(CotacaoASEL007Collection coll)
		{
			List<CotacaoASEL007> list = new List<CotacaoASEL007>();
			
			foreach (CotacaoASEL007 emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  CotacaoASEL007Metadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CotacaoASEL007Query();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new CotacaoASEL007(row);
		}

		override protected esEntity CreateEntity()
		{
			return new CotacaoASEL007();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public CotacaoASEL007Query Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CotacaoASEL007Query();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(CotacaoASEL007Query query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public CotacaoASEL007 AddNew()
		{
			CotacaoASEL007 entity = base.AddNewEntity() as CotacaoASEL007;
			
			return entity;
		}

		public CotacaoASEL007 FindByPrimaryKey()
		{
			return base.FindByPrimaryKey() as CotacaoASEL007;
		}


		#region IEnumerable<CotacaoASEL007> Members

		IEnumerator<CotacaoASEL007> IEnumerable<CotacaoASEL007>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as CotacaoASEL007;
			}
		}

		#endregion
		
		private CotacaoASEL007Query query;
	}


	/// <summary>
	/// Encapsulates the 'CotacaoASEL007' table
	/// </summary>

	[Serializable]
	public partial class CotacaoASEL007 : esCotacaoASEL007
	{
		public CotacaoASEL007()
		{

		}
	
		public CotacaoASEL007(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return CotacaoASEL007Metadata.Meta();
			}
		}
		
		
		
		override protected esCotacaoASEL007Query GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CotacaoASEL007Query();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public CotacaoASEL007Query Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CotacaoASEL007Query();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(CotacaoASEL007Query query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private CotacaoASEL007Query query;
	}



	[Serializable]
	public partial class CotacaoASEL007Query : esCotacaoASEL007Query
	{
		public CotacaoASEL007Query()
		{

		}		
		
		public CotacaoASEL007Query(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class CotacaoASEL007Metadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected CotacaoASEL007Metadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(CotacaoASEL007Metadata.ColumnNames.Data, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = CotacaoASEL007Metadata.PropertyNames.Data;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotacaoASEL007Metadata.ColumnNames.CodigoTitulo, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = CotacaoASEL007Metadata.PropertyNames.CodigoTitulo;
			c.CharacterMaxLength = 6;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotacaoASEL007Metadata.ColumnNames.DataVencimento, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = CotacaoASEL007Metadata.PropertyNames.DataVencimento;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotacaoASEL007Metadata.ColumnNames.Pu, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CotacaoASEL007Metadata.PropertyNames.Pu;	
			c.NumericPrecision = 16;
			c.NumericScale = 8;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public CotacaoASEL007Metadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string Data = "Data";
			 public const string CodigoTitulo = "CodigoTitulo";
			 public const string DataVencimento = "DataVencimento";
			 public const string Pu = "PU";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string Data = "Data";
			 public const string CodigoTitulo = "CodigoTitulo";
			 public const string DataVencimento = "DataVencimento";
			 public const string Pu = "Pu";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(CotacaoASEL007Metadata))
			{
				if(CotacaoASEL007Metadata.mapDelegates == null)
				{
					CotacaoASEL007Metadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (CotacaoASEL007Metadata.meta == null)
				{
					CotacaoASEL007Metadata.meta = new CotacaoASEL007Metadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("CodigoTitulo", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DataVencimento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("PU", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "CotacaoASEL007";
				meta.Destination = "CotacaoASEL007";
				
				meta.spInsert = "proc_CotacaoASEL007Insert";				
				meta.spUpdate = "proc_CotacaoASEL007Update";		
				meta.spDelete = "proc_CotacaoASEL007Delete";
				meta.spLoadAll = "proc_CotacaoASEL007LoadAll";
				meta.spLoadByPrimaryKey = "proc_CotacaoASEL007LoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private CotacaoASEL007Metadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
