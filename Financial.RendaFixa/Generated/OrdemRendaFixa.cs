/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 16/12/2015 15:23:24
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	



	
using Financial.Investidor;
using Financial.Common;
using Financial.Fundo;







		





		




				
				








				




		

		
		
		
		
		





namespace Financial.RendaFixa
{

	[Serializable]
	abstract public class esOrdemRendaFixaCollection : esEntityCollection
	{
		public esOrdemRendaFixaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "OrdemRendaFixaCollection";
		}

		#region Query Logic
		protected void InitQuery(esOrdemRendaFixaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esOrdemRendaFixaQuery);
		}
		#endregion
		
		virtual public OrdemRendaFixa DetachEntity(OrdemRendaFixa entity)
		{
			return base.DetachEntity(entity) as OrdemRendaFixa;
		}
		
		virtual public OrdemRendaFixa AttachEntity(OrdemRendaFixa entity)
		{
			return base.AttachEntity(entity) as OrdemRendaFixa;
		}
		
		virtual public void Combine(OrdemRendaFixaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public OrdemRendaFixa this[int index]
		{
			get
			{
				return base[index] as OrdemRendaFixa;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(OrdemRendaFixa);
		}
	}



	[Serializable]
	abstract public class esOrdemRendaFixa : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esOrdemRendaFixaQuery GetDynamicQuery()
		{
			return null;
		}

		public esOrdemRendaFixa()
		{

		}

		public esOrdemRendaFixa(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idOperacao)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idOperacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idOperacao);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idOperacao)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esOrdemRendaFixaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdOperacao == idOperacao);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idOperacao)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idOperacao);
			else
				return LoadByPrimaryKeyStoredProcedure(idOperacao);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idOperacao)
		{
			esOrdemRendaFixaQuery query = this.GetDynamicQuery();
			query.Where(query.IdOperacao == idOperacao);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idOperacao)
		{
			esParameters parms = new esParameters();
			parms.Add("IdOperacao",idOperacao);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdOperacao": this.str.IdOperacao = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "IdTitulo": this.str.IdTitulo = (string)value; break;							
						case "DataOperacao": this.str.DataOperacao = (string)value; break;							
						case "DataLiquidacao": this.str.DataLiquidacao = (string)value; break;							
						case "TipoOperacao": this.str.TipoOperacao = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "PUOperacao": this.str.PUOperacao = (string)value; break;							
						case "Valor": this.str.Valor = (string)value; break;							
						case "TaxaOperacao": this.str.TaxaOperacao = (string)value; break;							
						case "DataVolta": this.str.DataVolta = (string)value; break;							
						case "TaxaVolta": this.str.TaxaVolta = (string)value; break;							
						case "PUVolta": this.str.PUVolta = (string)value; break;							
						case "ValorVolta": this.str.ValorVolta = (string)value; break;							
						case "IdOperacaoResgatada": this.str.IdOperacaoResgatada = (string)value; break;							
						case "TipoNegociacao": this.str.TipoNegociacao = (string)value; break;							
						case "IdPosicaoResgatada": this.str.IdPosicaoResgatada = (string)value; break;							
						case "Status": this.str.Status = (string)value; break;							
						case "Observacao": this.str.Observacao = (string)value; break;							
						case "IdAgenteCorretora": this.str.IdAgenteCorretora = (string)value; break;							
						case "IdCustodia": this.str.IdCustodia = (string)value; break;							
						case "IdLiquidacao": this.str.IdLiquidacao = (string)value; break;							
						case "IdTrader": this.str.IdTrader = (string)value; break;
						case "IdAgenteContraParte": this.str.IdAgenteContraParte = (string)value; break;							
						case "IdOrdemEspelho": this.str.IdOrdemEspelho = (string)value; break;							
						case "IdCarteiraContraParte": this.str.IdCarteiraContraParte = (string)value; break;							
						case "IdCategoriaMovimentacao": this.str.IdCategoriaMovimentacao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacao = (System.Int32?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "IdTitulo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTitulo = (System.Int32?)value;
							break;
						
						case "DataOperacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataOperacao = (System.DateTime?)value;
							break;
						
						case "DataLiquidacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataLiquidacao = (System.DateTime?)value;
							break;
						
						case "TipoOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoOperacao = (System.Byte?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Quantidade = (System.Decimal?)value;
							break;
						
						case "PUOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PUOperacao = (System.Decimal?)value;
							break;
						
						case "Valor":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Valor = (System.Decimal?)value;
							break;
						
						case "TaxaOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TaxaOperacao = (System.Decimal?)value;
							break;
						
						case "DataVolta":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataVolta = (System.DateTime?)value;
							break;
						
						case "TaxaVolta":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TaxaVolta = (System.Decimal?)value;
							break;
						
						case "PUVolta":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PUVolta = (System.Decimal?)value;
							break;
						
						case "ValorVolta":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorVolta = (System.Decimal?)value;
							break;
						
						case "IdOperacaoResgatada":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacaoResgatada = (System.Int32?)value;
							break;
						
						case "TipoNegociacao":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoNegociacao = (System.Byte?)value;
							break;
						
						case "IdPosicaoResgatada":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPosicaoResgatada = (System.Int32?)value;
							break;
						
						case "Status":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.Status = (System.Byte?)value;
							break;
						
						case "IdAgenteCorretora":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgenteCorretora = (System.Int32?)value;
							break;
						
						case "IdCustodia":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.IdCustodia = (System.Byte?)value;
							break;
						
						case "IdLiquidacao":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.IdLiquidacao = (System.Byte?)value;
							break;
						
						case "IdTrader":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTrader = (System.Int32?)value;
							break;
							
						case "IdAgenteContraParte":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgenteContraParte = (System.Int32?)value;
							break;
						
						case "IdOrdemEspelho":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOrdemEspelho = (System.Int32?)value;
							break;
						
						case "IdCarteiraContraParte":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCarteiraContraParte = (System.Int32?)value;
							break;
						
						case "IdCategoriaMovimentacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCategoriaMovimentacao = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to OrdemRendaFixa.IdOperacao
		/// </summary>
		virtual public System.Int32? IdOperacao
		{
			get
			{
				return base.GetSystemInt32(OrdemRendaFixaMetadata.ColumnNames.IdOperacao);
			}
			
			set
			{
				base.SetSystemInt32(OrdemRendaFixaMetadata.ColumnNames.IdOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemRendaFixa.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(OrdemRendaFixaMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(OrdemRendaFixaMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OrdemRendaFixa.IdTitulo
		/// </summary>
		virtual public System.Int32? IdTitulo
		{
			get
			{
				return base.GetSystemInt32(OrdemRendaFixaMetadata.ColumnNames.IdTitulo);
			}
			
			set
			{
				if(base.SetSystemInt32(OrdemRendaFixaMetadata.ColumnNames.IdTitulo, value))
				{
					this._UpToTituloRendaFixaByIdTitulo = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OrdemRendaFixa.DataOperacao
		/// </summary>
		virtual public System.DateTime? DataOperacao
		{
			get
			{
				return base.GetSystemDateTime(OrdemRendaFixaMetadata.ColumnNames.DataOperacao);
			}
			
			set
			{
				base.SetSystemDateTime(OrdemRendaFixaMetadata.ColumnNames.DataOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemRendaFixa.DataLiquidacao
		/// </summary>
		virtual public System.DateTime? DataLiquidacao
		{
			get
			{
				return base.GetSystemDateTime(OrdemRendaFixaMetadata.ColumnNames.DataLiquidacao);
			}
			
			set
			{
				base.SetSystemDateTime(OrdemRendaFixaMetadata.ColumnNames.DataLiquidacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemRendaFixa.TipoOperacao
		/// </summary>
		virtual public System.Byte? TipoOperacao
		{
			get
			{
				return base.GetSystemByte(OrdemRendaFixaMetadata.ColumnNames.TipoOperacao);
			}
			
			set
			{
				base.SetSystemByte(OrdemRendaFixaMetadata.ColumnNames.TipoOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemRendaFixa.Quantidade
		/// </summary>
		virtual public System.Decimal? Quantidade
		{
			get
			{
				return base.GetSystemDecimal(OrdemRendaFixaMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemDecimal(OrdemRendaFixaMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemRendaFixa.PUOperacao
		/// </summary>
		virtual public System.Decimal? PUOperacao
		{
			get
			{
				return base.GetSystemDecimal(OrdemRendaFixaMetadata.ColumnNames.PUOperacao);
			}
			
			set
			{
				base.SetSystemDecimal(OrdemRendaFixaMetadata.ColumnNames.PUOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemRendaFixa.Valor
		/// </summary>
		virtual public System.Decimal? Valor
		{
			get
			{
				return base.GetSystemDecimal(OrdemRendaFixaMetadata.ColumnNames.Valor);
			}
			
			set
			{
				base.SetSystemDecimal(OrdemRendaFixaMetadata.ColumnNames.Valor, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemRendaFixa.TaxaOperacao
		/// </summary>
		virtual public System.Decimal? TaxaOperacao
		{
			get
			{
				return base.GetSystemDecimal(OrdemRendaFixaMetadata.ColumnNames.TaxaOperacao);
			}
			
			set
			{
				base.SetSystemDecimal(OrdemRendaFixaMetadata.ColumnNames.TaxaOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemRendaFixa.DataVolta
		/// </summary>
		virtual public System.DateTime? DataVolta
		{
			get
			{
				return base.GetSystemDateTime(OrdemRendaFixaMetadata.ColumnNames.DataVolta);
			}
			
			set
			{
				base.SetSystemDateTime(OrdemRendaFixaMetadata.ColumnNames.DataVolta, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemRendaFixa.TaxaVolta
		/// </summary>
		virtual public System.Decimal? TaxaVolta
		{
			get
			{
				return base.GetSystemDecimal(OrdemRendaFixaMetadata.ColumnNames.TaxaVolta);
			}
			
			set
			{
				base.SetSystemDecimal(OrdemRendaFixaMetadata.ColumnNames.TaxaVolta, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemRendaFixa.PUVolta
		/// </summary>
		virtual public System.Decimal? PUVolta
		{
			get
			{
				return base.GetSystemDecimal(OrdemRendaFixaMetadata.ColumnNames.PUVolta);
			}
			
			set
			{
				base.SetSystemDecimal(OrdemRendaFixaMetadata.ColumnNames.PUVolta, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemRendaFixa.ValorVolta
		/// </summary>
		virtual public System.Decimal? ValorVolta
		{
			get
			{
				return base.GetSystemDecimal(OrdemRendaFixaMetadata.ColumnNames.ValorVolta);
			}
			
			set
			{
				base.SetSystemDecimal(OrdemRendaFixaMetadata.ColumnNames.ValorVolta, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemRendaFixa.IdOperacaoResgatada
		/// </summary>
		virtual public System.Int32? IdOperacaoResgatada
		{
			get
			{
				return base.GetSystemInt32(OrdemRendaFixaMetadata.ColumnNames.IdOperacaoResgatada);
			}
			
			set
			{
				base.SetSystemInt32(OrdemRendaFixaMetadata.ColumnNames.IdOperacaoResgatada, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemRendaFixa.TipoNegociacao
		/// </summary>
		virtual public System.Byte? TipoNegociacao
		{
			get
			{
				return base.GetSystemByte(OrdemRendaFixaMetadata.ColumnNames.TipoNegociacao);
			}
			
			set
			{
				base.SetSystemByte(OrdemRendaFixaMetadata.ColumnNames.TipoNegociacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemRendaFixa.IdPosicaoResgatada
		/// </summary>
		virtual public System.Int32? IdPosicaoResgatada
		{
			get
			{
				return base.GetSystemInt32(OrdemRendaFixaMetadata.ColumnNames.IdPosicaoResgatada);
			}
			
			set
			{
				base.SetSystemInt32(OrdemRendaFixaMetadata.ColumnNames.IdPosicaoResgatada, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemRendaFixa.Status
		/// </summary>
		virtual public System.Byte? Status
		{
			get
			{
				return base.GetSystemByte(OrdemRendaFixaMetadata.ColumnNames.Status);
			}
			
			set
			{
				base.SetSystemByte(OrdemRendaFixaMetadata.ColumnNames.Status, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemRendaFixa.Observacao
		/// </summary>
		virtual public System.String Observacao
		{
			get
			{
				return base.GetSystemString(OrdemRendaFixaMetadata.ColumnNames.Observacao);
			}
			
			set
			{
				base.SetSystemString(OrdemRendaFixaMetadata.ColumnNames.Observacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemRendaFixa.IdAgenteCorretora
		/// </summary>
		virtual public System.Int32? IdAgenteCorretora
		{
			get
			{
				return base.GetSystemInt32(OrdemRendaFixaMetadata.ColumnNames.IdAgenteCorretora);
			}
			
			set
			{
				base.SetSystemInt32(OrdemRendaFixaMetadata.ColumnNames.IdAgenteCorretora, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemRendaFixa.IdCustodia
		/// </summary>
		virtual public System.Byte? IdCustodia
		{
			get
			{
				return base.GetSystemByte(OrdemRendaFixaMetadata.ColumnNames.IdCustodia);
			}
			
			set
			{
				base.SetSystemByte(OrdemRendaFixaMetadata.ColumnNames.IdCustodia, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemRendaFixa.IdLiquidacao
		/// </summary>
		virtual public System.Byte? IdLiquidacao
		{
			get
			{
				return base.GetSystemByte(OrdemRendaFixaMetadata.ColumnNames.IdLiquidacao);
			}
			
			set
			{
				base.SetSystemByte(OrdemRendaFixaMetadata.ColumnNames.IdLiquidacao, value);
			}
		}
		
		/// <summary>
		/// Maps to OrdemRendaFixa.IdTrader
		/// </summary>
		virtual public System.Int32? IdTrader
		{
			get
			{
				return base.GetSystemInt32(OrdemRendaFixaMetadata.ColumnNames.IdTrader);
			}
			
			set
			{
				if(base.SetSystemInt32(OrdemRendaFixaMetadata.ColumnNames.IdTrader, value))
				{
					this._UpToTraderByIdTrader = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OrdemRendaFixa.IdAgenteContraParte
		/// </summary>
		virtual public System.Int32? IdAgenteContraParte
		{
			get
			{
				return base.GetSystemInt32(OrdemRendaFixaMetadata.ColumnNames.IdAgenteContraParte);
			}
			
			set
			{
				if(base.SetSystemInt32(OrdemRendaFixaMetadata.ColumnNames.IdAgenteContraParte, value))
				{
					this._UpToAgenteMercadoByIdAgenteContraParte = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OrdemRendaFixa.IdOrdemEspelho
		/// </summary>
		virtual public System.Int32? IdOrdemEspelho
		{
			get
			{
				return base.GetSystemInt32(OrdemRendaFixaMetadata.ColumnNames.IdOrdemEspelho);
			}
			
			set
			{
				if(base.SetSystemInt32(OrdemRendaFixaMetadata.ColumnNames.IdOrdemEspelho, value))
				{
					this._UpToOrdemRendaFixaByIdOrdemEspelho = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OrdemRendaFixa.IdCarteiraContraParte
		/// </summary>
		virtual public System.Int32? IdCarteiraContraParte
		{
			get
			{
				return base.GetSystemInt32(OrdemRendaFixaMetadata.ColumnNames.IdCarteiraContraParte);
			}
			
			set
			{
				if(base.SetSystemInt32(OrdemRendaFixaMetadata.ColumnNames.IdCarteiraContraParte, value))
				{
					this._UpToCarteiraByIdCarteiraContraParte = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to OrdemRendaFixa.IdCategoriaMovimentacao
		/// </summary>
		virtual public System.Int32? IdCategoriaMovimentacao
		{
			get
			{
				return base.GetSystemInt32(OrdemRendaFixaMetadata.ColumnNames.IdCategoriaMovimentacao);
			}
			
			set
			{
				if(base.SetSystemInt32(OrdemRendaFixaMetadata.ColumnNames.IdCategoriaMovimentacao, value))
				{
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao = null;
				}
			}
		}
		
		[CLSCompliant(false)]
		internal protected OrdemRendaFixa _UpToOrdemRendaFixaByIdOrdemEspelho;
		[CLSCompliant(false)]
		internal protected AgenteMercado _UpToAgenteMercadoByIdAgenteContraParte;
		[CLSCompliant(false)]
		internal protected Carteira _UpToCarteiraByIdCarteiraContraParte;
		[CLSCompliant(false)]
		internal protected CategoriaMovimentacao _UpToCategoriaMovimentacaoByIdCategoriaMovimentacao;
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		[CLSCompliant(false)]
		internal protected TituloRendaFixa _UpToTituloRendaFixaByIdTitulo;
		[CLSCompliant(false)]
		internal protected Trader _UpToTraderByIdTrader;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esOrdemRendaFixa entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdOperacao
			{
				get
				{
					System.Int32? data = entity.IdOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacao = null;
					else entity.IdOperacao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String IdTitulo
			{
				get
				{
					System.Int32? data = entity.IdTitulo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTitulo = null;
					else entity.IdTitulo = Convert.ToInt32(value);
				}
			}
				
			public System.String DataOperacao
			{
				get
				{
					System.DateTime? data = entity.DataOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataOperacao = null;
					else entity.DataOperacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataLiquidacao
			{
				get
				{
					System.DateTime? data = entity.DataLiquidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataLiquidacao = null;
					else entity.DataLiquidacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String TipoOperacao
			{
				get
				{
					System.Byte? data = entity.TipoOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoOperacao = null;
					else entity.TipoOperacao = Convert.ToByte(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Decimal? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String PUOperacao
			{
				get
				{
					System.Decimal? data = entity.PUOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PUOperacao = null;
					else entity.PUOperacao = Convert.ToDecimal(value);
				}
			}
				
			public System.String Valor
			{
				get
				{
					System.Decimal? data = entity.Valor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Valor = null;
					else entity.Valor = Convert.ToDecimal(value);
				}
			}
				
			public System.String TaxaOperacao
			{
				get
				{
					System.Decimal? data = entity.TaxaOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TaxaOperacao = null;
					else entity.TaxaOperacao = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataVolta
			{
				get
				{
					System.DateTime? data = entity.DataVolta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataVolta = null;
					else entity.DataVolta = Convert.ToDateTime(value);
				}
			}
				
			public System.String TaxaVolta
			{
				get
				{
					System.Decimal? data = entity.TaxaVolta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TaxaVolta = null;
					else entity.TaxaVolta = Convert.ToDecimal(value);
				}
			}
				
			public System.String PUVolta
			{
				get
				{
					System.Decimal? data = entity.PUVolta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PUVolta = null;
					else entity.PUVolta = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorVolta
			{
				get
				{
					System.Decimal? data = entity.ValorVolta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorVolta = null;
					else entity.ValorVolta = Convert.ToDecimal(value);
				}
			}
				
			public System.String IdOperacaoResgatada
			{
				get
				{
					System.Int32? data = entity.IdOperacaoResgatada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacaoResgatada = null;
					else entity.IdOperacaoResgatada = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoNegociacao
			{
				get
				{
					System.Byte? data = entity.TipoNegociacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoNegociacao = null;
					else entity.TipoNegociacao = Convert.ToByte(value);
				}
			}
				
			public System.String IdPosicaoResgatada
			{
				get
				{
					System.Int32? data = entity.IdPosicaoResgatada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPosicaoResgatada = null;
					else entity.IdPosicaoResgatada = Convert.ToInt32(value);
				}
			}
				
			public System.String Status
			{
				get
				{
					System.Byte? data = entity.Status;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Status = null;
					else entity.Status = Convert.ToByte(value);
				}
			}
				
			public System.String Observacao
			{
				get
				{
					System.String data = entity.Observacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Observacao = null;
					else entity.Observacao = Convert.ToString(value);
				}
			}
				
			public System.String IdAgenteCorretora
			{
				get
				{
					System.Int32? data = entity.IdAgenteCorretora;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgenteCorretora = null;
					else entity.IdAgenteCorretora = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCustodia
			{
				get
				{
					System.Byte? data = entity.IdCustodia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCustodia = null;
					else entity.IdCustodia = Convert.ToByte(value);
				}
			}
				
			public System.String IdLiquidacao
			{
				get
				{
					System.Byte? data = entity.IdLiquidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdLiquidacao = null;
					else entity.IdLiquidacao = Convert.ToByte(value);
				}
			}
				
			public System.String IdTrader
			{
				get
				{
					System.Int32? data = entity.IdTrader;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTrader = null;
					else entity.IdTrader = Convert.ToInt32(value);
				}
			}
			
			public System.String IdAgenteContraParte
			{
				get
				{
					System.Int32? data = entity.IdAgenteContraParte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgenteContraParte = null;
					else entity.IdAgenteContraParte = Convert.ToInt32(value);
				}
			}
				
			public System.String IdOrdemEspelho
			{
				get
				{
					System.Int32? data = entity.IdOrdemEspelho;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOrdemEspelho = null;
					else entity.IdOrdemEspelho = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCarteiraContraParte
			{
				get
				{
					System.Int32? data = entity.IdCarteiraContraParte;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCarteiraContraParte = null;
					else entity.IdCarteiraContraParte = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCategoriaMovimentacao
			{
				get
				{
					System.Int32? data = entity.IdCategoriaMovimentacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCategoriaMovimentacao = null;
					else entity.IdCategoriaMovimentacao = Convert.ToInt32(value);
				}
			}
			

			private esOrdemRendaFixa entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esOrdemRendaFixaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esOrdemRendaFixa can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class OrdemRendaFixa : esOrdemRendaFixa
	{

				
		#region OrdemRendaFixaCollectionByIdOrdemEspelho - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - FK__OrdemRend__IdOrd__4D555BD0
		/// </summary>

		[XmlIgnore]
		public OrdemRendaFixaCollection OrdemRendaFixaCollectionByIdOrdemEspelho
		{
			get
			{
				if(this._OrdemRendaFixaCollectionByIdOrdemEspelho == null)
				{
					this._OrdemRendaFixaCollectionByIdOrdemEspelho = new OrdemRendaFixaCollection();
					this._OrdemRendaFixaCollectionByIdOrdemEspelho.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("OrdemRendaFixaCollectionByIdOrdemEspelho", this._OrdemRendaFixaCollectionByIdOrdemEspelho);
				
					if(this.IdOperacao != null)
					{
						this._OrdemRendaFixaCollectionByIdOrdemEspelho.Query.Where(this._OrdemRendaFixaCollectionByIdOrdemEspelho.Query.IdOrdemEspelho == this.IdOperacao);
						this._OrdemRendaFixaCollectionByIdOrdemEspelho.Query.Load();

						// Auto-hookup Foreign Keys
						this._OrdemRendaFixaCollectionByIdOrdemEspelho.fks.Add(OrdemRendaFixaMetadata.ColumnNames.IdOrdemEspelho, this.IdOperacao);
					}
				}

				return this._OrdemRendaFixaCollectionByIdOrdemEspelho;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._OrdemRendaFixaCollectionByIdOrdemEspelho != null) 
				{ 
					this.RemovePostSave("OrdemRendaFixaCollectionByIdOrdemEspelho"); 
					this._OrdemRendaFixaCollectionByIdOrdemEspelho = null;
					
				} 
			} 			
		}

		private OrdemRendaFixaCollection _OrdemRendaFixaCollectionByIdOrdemEspelho;
		#endregion

				
		#region UpToOrdemRendaFixaByIdOrdemEspelho - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK__OrdemRend__IdOrd__4D555BD0
		/// </summary>

		[XmlIgnore]
		public OrdemRendaFixa UpToOrdemRendaFixaByIdOrdemEspelho
		{
			get
			{
				if(this._UpToOrdemRendaFixaByIdOrdemEspelho == null
					&& IdOrdemEspelho != null					)
				{
					this._UpToOrdemRendaFixaByIdOrdemEspelho = new OrdemRendaFixa();
					this._UpToOrdemRendaFixaByIdOrdemEspelho.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToOrdemRendaFixaByIdOrdemEspelho", this._UpToOrdemRendaFixaByIdOrdemEspelho);
					this._UpToOrdemRendaFixaByIdOrdemEspelho.Query.Where(this._UpToOrdemRendaFixaByIdOrdemEspelho.Query.IdOperacao == this.IdOrdemEspelho);
					this._UpToOrdemRendaFixaByIdOrdemEspelho.Query.Load();
				}

				return this._UpToOrdemRendaFixaByIdOrdemEspelho;
			}
			
			set
			{
				this.RemovePreSave("UpToOrdemRendaFixaByIdOrdemEspelho");
				

				if(value == null)
				{
					this.IdOrdemEspelho = null;
					this._UpToOrdemRendaFixaByIdOrdemEspelho = null;
				}
				else
				{
					this.IdOrdemEspelho = value.IdOperacao;
					this._UpToOrdemRendaFixaByIdOrdemEspelho = value;
					this.SetPreSave("UpToOrdemRendaFixaByIdOrdemEspelho", this._UpToOrdemRendaFixaByIdOrdemEspelho);
				}
				
			}
		}
		#endregion
		

				
		#region UpToAgenteMercadoByIdAgenteContraParte - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK__OrdemRend__IdAge__4C613797
		/// </summary>

		[XmlIgnore]
		public AgenteMercado UpToAgenteMercadoByIdAgenteContraParte
		{
			get
			{
				if(this._UpToAgenteMercadoByIdAgenteContraParte == null
					&& IdAgenteContraParte != null					)
				{
					this._UpToAgenteMercadoByIdAgenteContraParte = new AgenteMercado();
					this._UpToAgenteMercadoByIdAgenteContraParte.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAgenteMercadoByIdAgenteContraParte", this._UpToAgenteMercadoByIdAgenteContraParte);
					this._UpToAgenteMercadoByIdAgenteContraParte.Query.Where(this._UpToAgenteMercadoByIdAgenteContraParte.Query.IdAgente == this.IdAgenteContraParte);
					this._UpToAgenteMercadoByIdAgenteContraParte.Query.Load();
				}

				return this._UpToAgenteMercadoByIdAgenteContraParte;
			}
			
			set
			{
				this.RemovePreSave("UpToAgenteMercadoByIdAgenteContraParte");
				

				if(value == null)
				{
					this.IdAgenteContraParte = null;
					this._UpToAgenteMercadoByIdAgenteContraParte = null;
				}
				else
				{
					this.IdAgenteContraParte = value.IdAgente;
					this._UpToAgenteMercadoByIdAgenteContraParte = value;
					this.SetPreSave("UpToAgenteMercadoByIdAgenteContraParte", this._UpToAgenteMercadoByIdAgenteContraParte);
				}
				
			}
		}
		#endregion
		

				
		#region UpToCarteiraByIdCarteiraContraParte - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK__OrdemRend__IdCar__0D3AD6BB
		/// </summary>

		[XmlIgnore]
		public Carteira UpToCarteiraByIdCarteiraContraParte
		{
			get
			{
				if(this._UpToCarteiraByIdCarteiraContraParte == null
					&& IdCarteiraContraParte != null					)
				{
					this._UpToCarteiraByIdCarteiraContraParte = new Carteira();
					this._UpToCarteiraByIdCarteiraContraParte.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCarteiraByIdCarteiraContraParte", this._UpToCarteiraByIdCarteiraContraParte);
					this._UpToCarteiraByIdCarteiraContraParte.Query.Where(this._UpToCarteiraByIdCarteiraContraParte.Query.IdCarteira == this.IdCarteiraContraParte);
					this._UpToCarteiraByIdCarteiraContraParte.Query.Load();
				}

				return this._UpToCarteiraByIdCarteiraContraParte;
			}
			
			set
			{
				this.RemovePreSave("UpToCarteiraByIdCarteiraContraParte");
				

				if(value == null)
				{
					this.IdCarteiraContraParte = null;
					this._UpToCarteiraByIdCarteiraContraParte = null;
				}
				else
				{
					this.IdCarteiraContraParte = value.IdCarteira;
					this._UpToCarteiraByIdCarteiraContraParte = value;
					this.SetPreSave("UpToCarteiraByIdCarteiraContraParte", this._UpToCarteiraByIdCarteiraContraParte);
				}
				
			}
		}
		#endregion
		

				
		#region UpToCategoriaMovimentacaoByIdCategoriaMovimentacao - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - OrdemRF_CatMovimentacao_FK1
		/// </summary>

		[XmlIgnore]
		public CategoriaMovimentacao UpToCategoriaMovimentacaoByIdCategoriaMovimentacao
		{
			get
			{
				if(this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao == null
					&& IdCategoriaMovimentacao != null					)
				{
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao = new CategoriaMovimentacao();
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCategoriaMovimentacaoByIdCategoriaMovimentacao", this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao);
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao.Query.Where(this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao.Query.IdCategoriaMovimentacao == this.IdCategoriaMovimentacao);
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao.Query.Load();
				}

				return this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao;
			}
			
			set
			{
				this.RemovePreSave("UpToCategoriaMovimentacaoByIdCategoriaMovimentacao");
				

				if(value == null)
				{
					this.IdCategoriaMovimentacao = null;
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao = null;
				}
				else
				{
					this.IdCategoriaMovimentacao = value.IdCategoriaMovimentacao;
					this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao = value;
					this.SetPreSave("UpToCategoriaMovimentacaoByIdCategoriaMovimentacao", this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao);
				}
				
			}
		}
		#endregion
		

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_OrdemRendaFixa_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToTituloRendaFixaByIdTitulo - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - TituloRendaFixa_OrdemRendaFixa_FK1
		/// </summary>

		[XmlIgnore]
		public TituloRendaFixa UpToTituloRendaFixaByIdTitulo
		{
			get
			{
				if(this._UpToTituloRendaFixaByIdTitulo == null
					&& IdTitulo != null					)
				{
					this._UpToTituloRendaFixaByIdTitulo = new TituloRendaFixa();
					this._UpToTituloRendaFixaByIdTitulo.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToTituloRendaFixaByIdTitulo", this._UpToTituloRendaFixaByIdTitulo);
					this._UpToTituloRendaFixaByIdTitulo.Query.Where(this._UpToTituloRendaFixaByIdTitulo.Query.IdTitulo == this.IdTitulo);
					this._UpToTituloRendaFixaByIdTitulo.Query.Load();
				}

				return this._UpToTituloRendaFixaByIdTitulo;
			}
			
			set
			{
				this.RemovePreSave("UpToTituloRendaFixaByIdTitulo");
				

				if(value == null)
				{
					this.IdTitulo = null;
					this._UpToTituloRendaFixaByIdTitulo = null;
				}
				else
				{
					this.IdTitulo = value.IdTitulo;
					this._UpToTituloRendaFixaByIdTitulo = value;
					this.SetPreSave("UpToTituloRendaFixaByIdTitulo", this._UpToTituloRendaFixaByIdTitulo);
				}
				
			}
		}
		#endregion
		

				
		#region UpToTraderByIdTrader - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - OrdemRendaFixa_Trader_FK
		/// </summary>

		[XmlIgnore]
		public Trader UpToTraderByIdTrader
		{
			get
			{
				if(this._UpToTraderByIdTrader == null
					&& IdTrader != null					)
				{
					this._UpToTraderByIdTrader = new Trader();
					this._UpToTraderByIdTrader.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToTraderByIdTrader", this._UpToTraderByIdTrader);
					this._UpToTraderByIdTrader.Query.Where(this._UpToTraderByIdTrader.Query.IdTrader == this.IdTrader);
					this._UpToTraderByIdTrader.Query.Load();
				}

				return this._UpToTraderByIdTrader;
			}
			
			set
			{
				this.RemovePreSave("UpToTraderByIdTrader");
				

				if(value == null)
				{
					this.IdTrader = null;
					this._UpToTraderByIdTrader = null;
				}
				else
				{
					this.IdTrader = value.IdTrader;
					this._UpToTraderByIdTrader = value;
					this.SetPreSave("UpToTraderByIdTrader", this._UpToTraderByIdTrader);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "OrdemRendaFixaCollectionByIdOrdemEspelho", typeof(OrdemRendaFixaCollection), new OrdemRendaFixa()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToOrdemRendaFixaByIdOrdemEspelho != null)
			{
				this.IdOrdemEspelho = this._UpToOrdemRendaFixaByIdOrdemEspelho.IdOperacao;
			}
			if(!this.es.IsDeleted && this._UpToAgenteMercadoByIdAgenteContraParte != null)
			{
				this.IdAgenteContraParte = this._UpToAgenteMercadoByIdAgenteContraParte.IdAgente;
			}
			if(!this.es.IsDeleted && this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao != null)
			{
				this.IdCategoriaMovimentacao = this._UpToCategoriaMovimentacaoByIdCategoriaMovimentacao.IdCategoriaMovimentacao;
			}
			if(!this.es.IsDeleted && this._UpToTituloRendaFixaByIdTitulo != null)
			{
				this.IdTitulo = this._UpToTituloRendaFixaByIdTitulo.IdTitulo;
			}
			if(!this.es.IsDeleted && this._UpToTraderByIdTrader != null)
			{
				this.IdTrader = this._UpToTraderByIdTrader.IdTrader;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._OrdemRendaFixaCollectionByIdOrdemEspelho != null)
			{
				foreach(OrdemRendaFixa obj in this._OrdemRendaFixaCollectionByIdOrdemEspelho)
				{
					if(obj.es.IsAdded)
					{
						obj.IdOrdemEspelho = this.IdOperacao;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esOrdemRendaFixaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return OrdemRendaFixaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdOperacao
		{
			get
			{
				return new esQueryItem(this, OrdemRendaFixaMetadata.ColumnNames.IdOperacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, OrdemRendaFixaMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdTitulo
		{
			get
			{
				return new esQueryItem(this, OrdemRendaFixaMetadata.ColumnNames.IdTitulo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataOperacao
		{
			get
			{
				return new esQueryItem(this, OrdemRendaFixaMetadata.ColumnNames.DataOperacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataLiquidacao
		{
			get
			{
				return new esQueryItem(this, OrdemRendaFixaMetadata.ColumnNames.DataLiquidacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem TipoOperacao
		{
			get
			{
				return new esQueryItem(this, OrdemRendaFixaMetadata.ColumnNames.TipoOperacao, esSystemType.Byte);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, OrdemRendaFixaMetadata.ColumnNames.Quantidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PUOperacao
		{
			get
			{
				return new esQueryItem(this, OrdemRendaFixaMetadata.ColumnNames.PUOperacao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Valor
		{
			get
			{
				return new esQueryItem(this, OrdemRendaFixaMetadata.ColumnNames.Valor, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TaxaOperacao
		{
			get
			{
				return new esQueryItem(this, OrdemRendaFixaMetadata.ColumnNames.TaxaOperacao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataVolta
		{
			get
			{
				return new esQueryItem(this, OrdemRendaFixaMetadata.ColumnNames.DataVolta, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem TaxaVolta
		{
			get
			{
				return new esQueryItem(this, OrdemRendaFixaMetadata.ColumnNames.TaxaVolta, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PUVolta
		{
			get
			{
				return new esQueryItem(this, OrdemRendaFixaMetadata.ColumnNames.PUVolta, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorVolta
		{
			get
			{
				return new esQueryItem(this, OrdemRendaFixaMetadata.ColumnNames.ValorVolta, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IdOperacaoResgatada
		{
			get
			{
				return new esQueryItem(this, OrdemRendaFixaMetadata.ColumnNames.IdOperacaoResgatada, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoNegociacao
		{
			get
			{
				return new esQueryItem(this, OrdemRendaFixaMetadata.ColumnNames.TipoNegociacao, esSystemType.Byte);
			}
		} 
		
		public esQueryItem IdPosicaoResgatada
		{
			get
			{
				return new esQueryItem(this, OrdemRendaFixaMetadata.ColumnNames.IdPosicaoResgatada, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Status
		{
			get
			{
				return new esQueryItem(this, OrdemRendaFixaMetadata.ColumnNames.Status, esSystemType.Byte);
			}
		} 
		
		public esQueryItem Observacao
		{
			get
			{
				return new esQueryItem(this, OrdemRendaFixaMetadata.ColumnNames.Observacao, esSystemType.String);
			}
		} 
		
		public esQueryItem IdAgenteCorretora
		{
			get
			{
				return new esQueryItem(this, OrdemRendaFixaMetadata.ColumnNames.IdAgenteCorretora, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCustodia
		{
			get
			{
				return new esQueryItem(this, OrdemRendaFixaMetadata.ColumnNames.IdCustodia, esSystemType.Byte);
			}
		} 
		
		public esQueryItem IdLiquidacao
		{
			get
			{
				return new esQueryItem(this, OrdemRendaFixaMetadata.ColumnNames.IdLiquidacao, esSystemType.Byte);
			}
		} 
		
		public esQueryItem IdTrader
		{
			get
			{
				return new esQueryItem(this, OrdemRendaFixaMetadata.ColumnNames.IdTrader, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdAgenteContraParte
		{
			get
			{
				return new esQueryItem(this, OrdemRendaFixaMetadata.ColumnNames.IdAgenteContraParte, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdOrdemEspelho
		{
			get
			{
				return new esQueryItem(this, OrdemRendaFixaMetadata.ColumnNames.IdOrdemEspelho, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCarteiraContraParte
		{
			get
			{
				return new esQueryItem(this, OrdemRendaFixaMetadata.ColumnNames.IdCarteiraContraParte, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCategoriaMovimentacao
		{
			get
			{
				return new esQueryItem(this, OrdemRendaFixaMetadata.ColumnNames.IdCategoriaMovimentacao, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("OrdemRendaFixaCollection")]
	public partial class OrdemRendaFixaCollection : esOrdemRendaFixaCollection, IEnumerable<OrdemRendaFixa>
	{
		public OrdemRendaFixaCollection()
		{

		}
		
		public static implicit operator List<OrdemRendaFixa>(OrdemRendaFixaCollection coll)
		{
			List<OrdemRendaFixa> list = new List<OrdemRendaFixa>();
			
			foreach (OrdemRendaFixa emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  OrdemRendaFixaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new OrdemRendaFixaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new OrdemRendaFixa(row);
		}

		override protected esEntity CreateEntity()
		{
			return new OrdemRendaFixa();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public OrdemRendaFixaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new OrdemRendaFixaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(OrdemRendaFixaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public OrdemRendaFixa AddNew()
		{
			OrdemRendaFixa entity = base.AddNewEntity() as OrdemRendaFixa;
			
			return entity;
		}

		public OrdemRendaFixa FindByPrimaryKey(System.Int32 idOperacao)
		{
			return base.FindByPrimaryKey(idOperacao) as OrdemRendaFixa;
		}


		#region IEnumerable<OrdemRendaFixa> Members

		IEnumerator<OrdemRendaFixa> IEnumerable<OrdemRendaFixa>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as OrdemRendaFixa;
			}
		}

		#endregion
		
		private OrdemRendaFixaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'OrdemRendaFixa' table
	/// </summary>

	[Serializable]
	public partial class OrdemRendaFixa : esOrdemRendaFixa
	{
		public OrdemRendaFixa()
		{

		}
	
		public OrdemRendaFixa(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return OrdemRendaFixaMetadata.Meta();
			}
		}
		
		
		
		override protected esOrdemRendaFixaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new OrdemRendaFixaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public OrdemRendaFixaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new OrdemRendaFixaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(OrdemRendaFixaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private OrdemRendaFixaQuery query;
	}



	[Serializable]
	public partial class OrdemRendaFixaQuery : esOrdemRendaFixaQuery
	{
		public OrdemRendaFixaQuery()
		{

		}		
		
		public OrdemRendaFixaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class OrdemRendaFixaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected OrdemRendaFixaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(OrdemRendaFixaMetadata.ColumnNames.IdOperacao, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OrdemRendaFixaMetadata.PropertyNames.IdOperacao;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemRendaFixaMetadata.ColumnNames.IdCliente, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OrdemRendaFixaMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemRendaFixaMetadata.ColumnNames.IdTitulo, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OrdemRendaFixaMetadata.PropertyNames.IdTitulo;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemRendaFixaMetadata.ColumnNames.DataOperacao, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OrdemRendaFixaMetadata.PropertyNames.DataOperacao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemRendaFixaMetadata.ColumnNames.DataLiquidacao, 4, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OrdemRendaFixaMetadata.PropertyNames.DataLiquidacao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemRendaFixaMetadata.ColumnNames.TipoOperacao, 5, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = OrdemRendaFixaMetadata.PropertyNames.TipoOperacao;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemRendaFixaMetadata.ColumnNames.Quantidade, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OrdemRendaFixaMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemRendaFixaMetadata.ColumnNames.PUOperacao, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OrdemRendaFixaMetadata.PropertyNames.PUOperacao;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemRendaFixaMetadata.ColumnNames.Valor, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OrdemRendaFixaMetadata.PropertyNames.Valor;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemRendaFixaMetadata.ColumnNames.TaxaOperacao, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OrdemRendaFixaMetadata.PropertyNames.TaxaOperacao;	
			c.NumericPrecision = 25;
			c.NumericScale = 16;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemRendaFixaMetadata.ColumnNames.DataVolta, 10, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = OrdemRendaFixaMetadata.PropertyNames.DataVolta;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemRendaFixaMetadata.ColumnNames.TaxaVolta, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OrdemRendaFixaMetadata.PropertyNames.TaxaVolta;	
			c.NumericPrecision = 8;
			c.NumericScale = 4;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemRendaFixaMetadata.ColumnNames.PUVolta, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OrdemRendaFixaMetadata.PropertyNames.PUVolta;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemRendaFixaMetadata.ColumnNames.ValorVolta, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = OrdemRendaFixaMetadata.PropertyNames.ValorVolta;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemRendaFixaMetadata.ColumnNames.IdOperacaoResgatada, 14, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OrdemRendaFixaMetadata.PropertyNames.IdOperacaoResgatada;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemRendaFixaMetadata.ColumnNames.TipoNegociacao, 15, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = OrdemRendaFixaMetadata.PropertyNames.TipoNegociacao;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemRendaFixaMetadata.ColumnNames.IdPosicaoResgatada, 16, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OrdemRendaFixaMetadata.PropertyNames.IdPosicaoResgatada;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemRendaFixaMetadata.ColumnNames.Status, 17, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = OrdemRendaFixaMetadata.PropertyNames.Status;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemRendaFixaMetadata.ColumnNames.Observacao, 18, typeof(System.String), esSystemType.String);
			c.PropertyName = OrdemRendaFixaMetadata.PropertyNames.Observacao;
			c.CharacterMaxLength = 2147483647;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemRendaFixaMetadata.ColumnNames.IdAgenteCorretora, 19, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OrdemRendaFixaMetadata.PropertyNames.IdAgenteCorretora;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemRendaFixaMetadata.ColumnNames.IdCustodia, 20, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = OrdemRendaFixaMetadata.PropertyNames.IdCustodia;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemRendaFixaMetadata.ColumnNames.IdLiquidacao, 21, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = OrdemRendaFixaMetadata.PropertyNames.IdLiquidacao;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
			c = new esColumnMetadata(OrdemRendaFixaMetadata.ColumnNames.IdTrader, 22, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OrdemRendaFixaMetadata.PropertyNames.IdTrader;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
				
			c = new esColumnMetadata(OrdemRendaFixaMetadata.ColumnNames.IdAgenteContraParte, 22, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OrdemRendaFixaMetadata.PropertyNames.IdAgenteContraParte;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemRendaFixaMetadata.ColumnNames.IdOrdemEspelho, 23, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OrdemRendaFixaMetadata.PropertyNames.IdOrdemEspelho;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemRendaFixaMetadata.ColumnNames.IdCarteiraContraParte, 24, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OrdemRendaFixaMetadata.PropertyNames.IdCarteiraContraParte;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(OrdemRendaFixaMetadata.ColumnNames.IdCategoriaMovimentacao, 25, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = OrdemRendaFixaMetadata.PropertyNames.IdCategoriaMovimentacao;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public OrdemRendaFixaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdOperacao = "IdOperacao";
			 public const string IdCliente = "IdCliente";
			 public const string IdTitulo = "IdTitulo";
			 public const string DataOperacao = "DataOperacao";
			 public const string DataLiquidacao = "DataLiquidacao";
			 public const string TipoOperacao = "TipoOperacao";
			 public const string Quantidade = "Quantidade";
			 public const string PUOperacao = "PUOperacao";
			 public const string Valor = "Valor";
			 public const string TaxaOperacao = "TaxaOperacao";
			 public const string DataVolta = "DataVolta";
			 public const string TaxaVolta = "TaxaVolta";
			 public const string PUVolta = "PUVolta";
			 public const string ValorVolta = "ValorVolta";
			 public const string IdOperacaoResgatada = "IdOperacaoResgatada";
			 public const string TipoNegociacao = "TipoNegociacao";
			 public const string IdPosicaoResgatada = "IdPosicaoResgatada";
			 public const string Status = "Status";
			 public const string Observacao = "Observacao";
			 public const string IdAgenteCorretora = "IdAgenteCorretora";
			 public const string IdCustodia = "IdCustodia";
			 public const string IdLiquidacao = "IdLiquidacao";
			 public const string IdTrader = "IdTrader";
			 public const string IdAgenteContraParte = "IdAgenteContraParte";
			 public const string IdOrdemEspelho = "IdOrdemEspelho";
			 public const string IdCarteiraContraParte = "IdCarteiraContraParte";
			 public const string IdCategoriaMovimentacao = "IdCategoriaMovimentacao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdOperacao = "IdOperacao";
			 public const string IdCliente = "IdCliente";
			 public const string IdTitulo = "IdTitulo";
			 public const string DataOperacao = "DataOperacao";
			 public const string DataLiquidacao = "DataLiquidacao";
			 public const string TipoOperacao = "TipoOperacao";
			 public const string Quantidade = "Quantidade";
			 public const string PUOperacao = "PUOperacao";
			 public const string Valor = "Valor";
			 public const string TaxaOperacao = "TaxaOperacao";
			 public const string DataVolta = "DataVolta";
			 public const string TaxaVolta = "TaxaVolta";
			 public const string PUVolta = "PUVolta";
			 public const string ValorVolta = "ValorVolta";
			 public const string IdOperacaoResgatada = "IdOperacaoResgatada";
			 public const string TipoNegociacao = "TipoNegociacao";
			 public const string IdPosicaoResgatada = "IdPosicaoResgatada";
			 public const string Status = "Status";
			 public const string Observacao = "Observacao";
			 public const string IdAgenteCorretora = "IdAgenteCorretora";
			 public const string IdCustodia = "IdCustodia";
			 public const string IdLiquidacao = "IdLiquidacao";
			 public const string IdTrader = "IdTrader";
			 public const string IdAgenteContraParte = "IdAgenteContraParte";
			 public const string IdOrdemEspelho = "IdOrdemEspelho";
			 public const string IdCarteiraContraParte = "IdCarteiraContraParte";
			 public const string IdCategoriaMovimentacao = "IdCategoriaMovimentacao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(OrdemRendaFixaMetadata))
			{
				if(OrdemRendaFixaMetadata.mapDelegates == null)
				{
					OrdemRendaFixaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (OrdemRendaFixaMetadata.meta == null)
				{
					OrdemRendaFixaMetadata.meta = new OrdemRendaFixaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdOperacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdTitulo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataOperacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataLiquidacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("TipoOperacao", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("Quantidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PUOperacao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Valor", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("TaxaOperacao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("DataVolta", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("TaxaVolta", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PUVolta", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorVolta", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IdOperacaoResgatada", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoNegociacao", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("IdPosicaoResgatada", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Status", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("Observacao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IdAgenteCorretora", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCustodia", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("IdLiquidacao", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("IdTrader", new esTypeMap("int", "System.Int32"));			
				meta.AddTypeMap("IdAgenteContraParte", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdOrdemEspelho", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCarteiraContraParte", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCategoriaMovimentacao", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "OrdemRendaFixa";
				meta.Destination = "OrdemRendaFixa";
				
				meta.spInsert = "proc_OrdemRendaFixaInsert";				
				meta.spUpdate = "proc_OrdemRendaFixaUpdate";		
				meta.spDelete = "proc_OrdemRendaFixaDelete";
				meta.spLoadAll = "proc_OrdemRendaFixaLoadAll";
				meta.spLoadByPrimaryKey = "proc_OrdemRendaFixaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private OrdemRendaFixaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
