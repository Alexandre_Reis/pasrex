/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 12/12/2014 10:53:33 AM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.RendaFixa
{

	[Serializable]
	abstract public class esTabelaEscalonamentoRFCollection : esEntityCollection
	{
		public esTabelaEscalonamentoRFCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "TabelaEscalonamentoRFCollection";
		}

		#region Query Logic
		protected void InitQuery(esTabelaEscalonamentoRFQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esTabelaEscalonamentoRFQuery);
		}
		#endregion
		
		virtual public TabelaEscalonamentoRF DetachEntity(TabelaEscalonamentoRF entity)
		{
			return base.DetachEntity(entity) as TabelaEscalonamentoRF;
		}
		
		virtual public TabelaEscalonamentoRF AttachEntity(TabelaEscalonamentoRF entity)
		{
			return base.AttachEntity(entity) as TabelaEscalonamentoRF;
		}
		
		virtual public void Combine(TabelaEscalonamentoRFCollection collection)
		{
			base.Combine(collection);
		}
		
		new public TabelaEscalonamentoRF this[int index]
		{
			get
			{
				return base[index] as TabelaEscalonamentoRF;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(TabelaEscalonamentoRF);
		}
	}



	[Serializable]
	abstract public class esTabelaEscalonamentoRF : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esTabelaEscalonamentoRFQuery GetDynamicQuery()
		{
			return null;
		}

		public esTabelaEscalonamentoRF()
		{

		}

		public esTabelaEscalonamentoRF(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idTitulo, System.Int32 prazo)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTitulo, prazo);
			else
				return LoadByPrimaryKeyStoredProcedure(idTitulo, prazo);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idTitulo, System.Int32 prazo)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esTabelaEscalonamentoRFQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdTitulo == idTitulo, query.Prazo == prazo);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idTitulo, System.Int32 prazo)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTitulo, prazo);
			else
				return LoadByPrimaryKeyStoredProcedure(idTitulo, prazo);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idTitulo, System.Int32 prazo)
		{
			esTabelaEscalonamentoRFQuery query = this.GetDynamicQuery();
			query.Where(query.IdTitulo == idTitulo, query.Prazo == prazo);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idTitulo, System.Int32 prazo)
		{
			esParameters parms = new esParameters();
			parms.Add("IdTitulo",idTitulo);			parms.Add("Prazo",prazo);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdTitulo": this.str.IdTitulo = (string)value; break;							
						case "Prazo": this.str.Prazo = (string)value; break;							
						case "Taxa": this.str.Taxa = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdTitulo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTitulo = (System.Int32?)value;
							break;
						
						case "Prazo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.Prazo = (System.Int32?)value;
							break;
						
						case "Taxa":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Taxa = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to TabelaEscalonamentoRF.IdTitulo
		/// </summary>
		virtual public System.Int32? IdTitulo
		{
			get
			{
				return base.GetSystemInt32(TabelaEscalonamentoRFMetadata.ColumnNames.IdTitulo);
			}
			
			set
			{
				if(base.SetSystemInt32(TabelaEscalonamentoRFMetadata.ColumnNames.IdTitulo, value))
				{
					this._UpToTituloRendaFixaByIdTitulo = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to TabelaEscalonamentoRF.Prazo
		/// </summary>
		virtual public System.Int32? Prazo
		{
			get
			{
				return base.GetSystemInt32(TabelaEscalonamentoRFMetadata.ColumnNames.Prazo);
			}
			
			set
			{
				base.SetSystemInt32(TabelaEscalonamentoRFMetadata.ColumnNames.Prazo, value);
			}
		}
		
		/// <summary>
		/// Maps to TabelaEscalonamentoRF.Taxa
		/// </summary>
		virtual public System.Decimal? Taxa
		{
			get
			{
				return base.GetSystemDecimal(TabelaEscalonamentoRFMetadata.ColumnNames.Taxa);
			}
			
			set
			{
				base.SetSystemDecimal(TabelaEscalonamentoRFMetadata.ColumnNames.Taxa, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected TituloRendaFixa _UpToTituloRendaFixaByIdTitulo;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esTabelaEscalonamentoRF entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdTitulo
			{
				get
				{
					System.Int32? data = entity.IdTitulo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTitulo = null;
					else entity.IdTitulo = Convert.ToInt32(value);
				}
			}
				
			public System.String Prazo
			{
				get
				{
					System.Int32? data = entity.Prazo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Prazo = null;
					else entity.Prazo = Convert.ToInt32(value);
				}
			}
				
			public System.String Taxa
			{
				get
				{
					System.Decimal? data = entity.Taxa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Taxa = null;
					else entity.Taxa = Convert.ToDecimal(value);
				}
			}
			

			private esTabelaEscalonamentoRF entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esTabelaEscalonamentoRFQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esTabelaEscalonamentoRF can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class TabelaEscalonamentoRF : esTabelaEscalonamentoRF
	{

				
		#region UpToTituloRendaFixaByIdTitulo - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK_TituloRendaFixa_TabelaEscalonamentoRF_1
		/// </summary>

		[XmlIgnore]
		public TituloRendaFixa UpToTituloRendaFixaByIdTitulo
		{
			get
			{
				if(this._UpToTituloRendaFixaByIdTitulo == null
					&& IdTitulo != null					)
				{
					this._UpToTituloRendaFixaByIdTitulo = new TituloRendaFixa();
					this._UpToTituloRendaFixaByIdTitulo.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToTituloRendaFixaByIdTitulo", this._UpToTituloRendaFixaByIdTitulo);
					this._UpToTituloRendaFixaByIdTitulo.Query.Where(this._UpToTituloRendaFixaByIdTitulo.Query.IdTitulo == this.IdTitulo);
					this._UpToTituloRendaFixaByIdTitulo.Query.Load();
				}

				return this._UpToTituloRendaFixaByIdTitulo;
			}
			
			set
			{
				this.RemovePreSave("UpToTituloRendaFixaByIdTitulo");
				

				if(value == null)
				{
					this.IdTitulo = null;
					this._UpToTituloRendaFixaByIdTitulo = null;
				}
				else
				{
					this.IdTitulo = value.IdTitulo;
					this._UpToTituloRendaFixaByIdTitulo = value;
					this.SetPreSave("UpToTituloRendaFixaByIdTitulo", this._UpToTituloRendaFixaByIdTitulo);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToTituloRendaFixaByIdTitulo != null)
			{
				this.IdTitulo = this._UpToTituloRendaFixaByIdTitulo.IdTitulo;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esTabelaEscalonamentoRFQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return TabelaEscalonamentoRFMetadata.Meta();
			}
		}	
		

		public esQueryItem IdTitulo
		{
			get
			{
				return new esQueryItem(this, TabelaEscalonamentoRFMetadata.ColumnNames.IdTitulo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Prazo
		{
			get
			{
				return new esQueryItem(this, TabelaEscalonamentoRFMetadata.ColumnNames.Prazo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Taxa
		{
			get
			{
				return new esQueryItem(this, TabelaEscalonamentoRFMetadata.ColumnNames.Taxa, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("TabelaEscalonamentoRFCollection")]
	public partial class TabelaEscalonamentoRFCollection : esTabelaEscalonamentoRFCollection, IEnumerable<TabelaEscalonamentoRF>
	{
		public TabelaEscalonamentoRFCollection()
		{

		}
		
		public static implicit operator List<TabelaEscalonamentoRF>(TabelaEscalonamentoRFCollection coll)
		{
			List<TabelaEscalonamentoRF> list = new List<TabelaEscalonamentoRF>();
			
			foreach (TabelaEscalonamentoRF emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  TabelaEscalonamentoRFMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaEscalonamentoRFQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new TabelaEscalonamentoRF(row);
		}

		override protected esEntity CreateEntity()
		{
			return new TabelaEscalonamentoRF();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public TabelaEscalonamentoRFQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaEscalonamentoRFQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(TabelaEscalonamentoRFQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public TabelaEscalonamentoRF AddNew()
		{
			TabelaEscalonamentoRF entity = base.AddNewEntity() as TabelaEscalonamentoRF;
			
			return entity;
		}

		public TabelaEscalonamentoRF FindByPrimaryKey(System.Int32 idTitulo, System.Int32 prazo)
		{
			return base.FindByPrimaryKey(idTitulo, prazo) as TabelaEscalonamentoRF;
		}


		#region IEnumerable<TabelaEscalonamentoRF> Members

		IEnumerator<TabelaEscalonamentoRF> IEnumerable<TabelaEscalonamentoRF>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as TabelaEscalonamentoRF;
			}
		}

		#endregion
		
		private TabelaEscalonamentoRFQuery query;
	}


	/// <summary>
	/// Encapsulates the 'TabelaEscalonamentoRF' table
	/// </summary>

	[Serializable]
	public partial class TabelaEscalonamentoRF : esTabelaEscalonamentoRF
	{
		public TabelaEscalonamentoRF()
		{

		}
	
		public TabelaEscalonamentoRF(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return TabelaEscalonamentoRFMetadata.Meta();
			}
		}
		
		
		
		override protected esTabelaEscalonamentoRFQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new TabelaEscalonamentoRFQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public TabelaEscalonamentoRFQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new TabelaEscalonamentoRFQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(TabelaEscalonamentoRFQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private TabelaEscalonamentoRFQuery query;
	}



	[Serializable]
	public partial class TabelaEscalonamentoRFQuery : esTabelaEscalonamentoRFQuery
	{
		public TabelaEscalonamentoRFQuery()
		{

		}		
		
		public TabelaEscalonamentoRFQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class TabelaEscalonamentoRFMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected TabelaEscalonamentoRFMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(TabelaEscalonamentoRFMetadata.ColumnNames.IdTitulo, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaEscalonamentoRFMetadata.PropertyNames.IdTitulo;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaEscalonamentoRFMetadata.ColumnNames.Prazo, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = TabelaEscalonamentoRFMetadata.PropertyNames.Prazo;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(TabelaEscalonamentoRFMetadata.ColumnNames.Taxa, 2, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = TabelaEscalonamentoRFMetadata.PropertyNames.Taxa;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public TabelaEscalonamentoRFMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdTitulo = "IdTitulo";
			 public const string Prazo = "Prazo";
			 public const string Taxa = "Taxa";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdTitulo = "IdTitulo";
			 public const string Prazo = "Prazo";
			 public const string Taxa = "Taxa";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(TabelaEscalonamentoRFMetadata))
			{
				if(TabelaEscalonamentoRFMetadata.mapDelegates == null)
				{
					TabelaEscalonamentoRFMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (TabelaEscalonamentoRFMetadata.meta == null)
				{
					TabelaEscalonamentoRFMetadata.meta = new TabelaEscalonamentoRFMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdTitulo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Prazo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Taxa", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "TabelaEscalonamentoRF";
				meta.Destination = "TabelaEscalonamentoRF";
				
				meta.spInsert = "proc_TabelaEscalonamentoRFInsert";				
				meta.spUpdate = "proc_TabelaEscalonamentoRFUpdate";		
				meta.spDelete = "proc_TabelaEscalonamentoRFDelete";
				meta.spLoadAll = "proc_TabelaEscalonamentoRFLoadAll";
				meta.spLoadByPrimaryKey = "proc_TabelaEscalonamentoRFLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private TabelaEscalonamentoRFMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
