/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 12/12/2014 10:53:29 AM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.RendaFixa
{

	[Serializable]
	abstract public class esCotacaoMercadoTesouroCollection : esEntityCollection
	{
		public esCotacaoMercadoTesouroCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "CotacaoMercadoTesouroCollection";
		}

		#region Query Logic
		protected void InitQuery(esCotacaoMercadoTesouroQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esCotacaoMercadoTesouroQuery);
		}
		#endregion
		
		virtual public CotacaoMercadoTesouro DetachEntity(CotacaoMercadoTesouro entity)
		{
			return base.DetachEntity(entity) as CotacaoMercadoTesouro;
		}
		
		virtual public CotacaoMercadoTesouro AttachEntity(CotacaoMercadoTesouro entity)
		{
			return base.AttachEntity(entity) as CotacaoMercadoTesouro;
		}
		
		virtual public void Combine(CotacaoMercadoTesouroCollection collection)
		{
			base.Combine(collection);
		}
		
		new public CotacaoMercadoTesouro this[int index]
		{
			get
			{
				return base[index] as CotacaoMercadoTesouro;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(CotacaoMercadoTesouro);
		}
	}



	[Serializable]
	abstract public class esCotacaoMercadoTesouro : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esCotacaoMercadoTesouroQuery GetDynamicQuery()
		{
			return null;
		}

		public esCotacaoMercadoTesouro()
		{

		}

		public esCotacaoMercadoTesouro(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime dataReferencia, System.String descricao, System.DateTime dataVencimento)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataReferencia, descricao, dataVencimento);
			else
				return LoadByPrimaryKeyStoredProcedure(dataReferencia, descricao, dataVencimento);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.DateTime dataReferencia, System.String descricao, System.DateTime dataVencimento)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esCotacaoMercadoTesouroQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.DataReferencia == dataReferencia, query.Descricao == descricao, query.DataVencimento == dataVencimento);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime dataReferencia, System.String descricao, System.DateTime dataVencimento)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataReferencia, descricao, dataVencimento);
			else
				return LoadByPrimaryKeyStoredProcedure(dataReferencia, descricao, dataVencimento);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime dataReferencia, System.String descricao, System.DateTime dataVencimento)
		{
			esCotacaoMercadoTesouroQuery query = this.GetDynamicQuery();
			query.Where(query.DataReferencia == dataReferencia, query.Descricao == descricao, query.DataVencimento == dataVencimento);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime dataReferencia, System.String descricao, System.DateTime dataVencimento)
		{
			esParameters parms = new esParameters();
			parms.Add("DataReferencia",dataReferencia);			parms.Add("Descricao",descricao);			parms.Add("DataVencimento",dataVencimento);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "DataReferencia": this.str.DataReferencia = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;							
						case "DataVencimento": this.str.DataVencimento = (string)value; break;							
						case "Pu": this.str.Pu = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "DataReferencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataReferencia = (System.DateTime?)value;
							break;
						
						case "DataVencimento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataVencimento = (System.DateTime?)value;
							break;
						
						case "Pu":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Pu = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to CotacaoMercadoTesouro.DataReferencia
		/// </summary>
		virtual public System.DateTime? DataReferencia
		{
			get
			{
				return base.GetSystemDateTime(CotacaoMercadoTesouroMetadata.ColumnNames.DataReferencia);
			}
			
			set
			{
				base.SetSystemDateTime(CotacaoMercadoTesouroMetadata.ColumnNames.DataReferencia, value);
			}
		}
		
		/// <summary>
		/// Maps to CotacaoMercadoTesouro.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(CotacaoMercadoTesouroMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(CotacaoMercadoTesouroMetadata.ColumnNames.Descricao, value);
			}
		}
		
		/// <summary>
		/// Maps to CotacaoMercadoTesouro.DataVencimento
		/// </summary>
		virtual public System.DateTime? DataVencimento
		{
			get
			{
				return base.GetSystemDateTime(CotacaoMercadoTesouroMetadata.ColumnNames.DataVencimento);
			}
			
			set
			{
				base.SetSystemDateTime(CotacaoMercadoTesouroMetadata.ColumnNames.DataVencimento, value);
			}
		}
		
		/// <summary>
		/// Maps to CotacaoMercadoTesouro.PU
		/// </summary>
		virtual public System.Decimal? Pu
		{
			get
			{
				return base.GetSystemDecimal(CotacaoMercadoTesouroMetadata.ColumnNames.Pu);
			}
			
			set
			{
				base.SetSystemDecimal(CotacaoMercadoTesouroMetadata.ColumnNames.Pu, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esCotacaoMercadoTesouro entity)
			{
				this.entity = entity;
			}
			
	
			public System.String DataReferencia
			{
				get
				{
					System.DateTime? data = entity.DataReferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataReferencia = null;
					else entity.DataReferencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
				
			public System.String DataVencimento
			{
				get
				{
					System.DateTime? data = entity.DataVencimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataVencimento = null;
					else entity.DataVencimento = Convert.ToDateTime(value);
				}
			}
				
			public System.String Pu
			{
				get
				{
					System.Decimal? data = entity.Pu;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Pu = null;
					else entity.Pu = Convert.ToDecimal(value);
				}
			}
			

			private esCotacaoMercadoTesouro entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esCotacaoMercadoTesouroQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esCotacaoMercadoTesouro can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class CotacaoMercadoTesouro : esCotacaoMercadoTesouro
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esCotacaoMercadoTesouroQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return CotacaoMercadoTesouroMetadata.Meta();
			}
		}	
		

		public esQueryItem DataReferencia
		{
			get
			{
				return new esQueryItem(this, CotacaoMercadoTesouroMetadata.ColumnNames.DataReferencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, CotacaoMercadoTesouroMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
		public esQueryItem DataVencimento
		{
			get
			{
				return new esQueryItem(this, CotacaoMercadoTesouroMetadata.ColumnNames.DataVencimento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Pu
		{
			get
			{
				return new esQueryItem(this, CotacaoMercadoTesouroMetadata.ColumnNames.Pu, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("CotacaoMercadoTesouroCollection")]
	public partial class CotacaoMercadoTesouroCollection : esCotacaoMercadoTesouroCollection, IEnumerable<CotacaoMercadoTesouro>
	{
		public CotacaoMercadoTesouroCollection()
		{

		}
		
		public static implicit operator List<CotacaoMercadoTesouro>(CotacaoMercadoTesouroCollection coll)
		{
			List<CotacaoMercadoTesouro> list = new List<CotacaoMercadoTesouro>();
			
			foreach (CotacaoMercadoTesouro emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  CotacaoMercadoTesouroMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CotacaoMercadoTesouroQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new CotacaoMercadoTesouro(row);
		}

		override protected esEntity CreateEntity()
		{
			return new CotacaoMercadoTesouro();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public CotacaoMercadoTesouroQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CotacaoMercadoTesouroQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(CotacaoMercadoTesouroQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public CotacaoMercadoTesouro AddNew()
		{
			CotacaoMercadoTesouro entity = base.AddNewEntity() as CotacaoMercadoTesouro;
			
			return entity;
		}

		public CotacaoMercadoTesouro FindByPrimaryKey(System.DateTime dataReferencia, System.String descricao, System.DateTime dataVencimento)
		{
			return base.FindByPrimaryKey(dataReferencia, descricao, dataVencimento) as CotacaoMercadoTesouro;
		}


		#region IEnumerable<CotacaoMercadoTesouro> Members

		IEnumerator<CotacaoMercadoTesouro> IEnumerable<CotacaoMercadoTesouro>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as CotacaoMercadoTesouro;
			}
		}

		#endregion
		
		private CotacaoMercadoTesouroQuery query;
	}


	/// <summary>
	/// Encapsulates the 'CotacaoMercadoTesouro' table
	/// </summary>

	[Serializable]
	public partial class CotacaoMercadoTesouro : esCotacaoMercadoTesouro
	{
		public CotacaoMercadoTesouro()
		{

		}
	
		public CotacaoMercadoTesouro(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return CotacaoMercadoTesouroMetadata.Meta();
			}
		}
		
		
		
		override protected esCotacaoMercadoTesouroQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CotacaoMercadoTesouroQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public CotacaoMercadoTesouroQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CotacaoMercadoTesouroQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(CotacaoMercadoTesouroQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private CotacaoMercadoTesouroQuery query;
	}



	[Serializable]
	public partial class CotacaoMercadoTesouroQuery : esCotacaoMercadoTesouroQuery
	{
		public CotacaoMercadoTesouroQuery()
		{

		}		
		
		public CotacaoMercadoTesouroQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class CotacaoMercadoTesouroMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected CotacaoMercadoTesouroMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(CotacaoMercadoTesouroMetadata.ColumnNames.DataReferencia, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = CotacaoMercadoTesouroMetadata.PropertyNames.DataReferencia;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotacaoMercadoTesouroMetadata.ColumnNames.Descricao, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = CotacaoMercadoTesouroMetadata.PropertyNames.Descricao;
			c.IsInPrimaryKey = true;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotacaoMercadoTesouroMetadata.ColumnNames.DataVencimento, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = CotacaoMercadoTesouroMetadata.PropertyNames.DataVencimento;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotacaoMercadoTesouroMetadata.ColumnNames.Pu, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CotacaoMercadoTesouroMetadata.PropertyNames.Pu;	
			c.NumericPrecision = 28;
			c.NumericScale = 16;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public CotacaoMercadoTesouroMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string DataReferencia = "DataReferencia";
			 public const string Descricao = "Descricao";
			 public const string DataVencimento = "DataVencimento";
			 public const string Pu = "PU";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string DataReferencia = "DataReferencia";
			 public const string Descricao = "Descricao";
			 public const string DataVencimento = "DataVencimento";
			 public const string Pu = "Pu";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(CotacaoMercadoTesouroMetadata))
			{
				if(CotacaoMercadoTesouroMetadata.mapDelegates == null)
				{
					CotacaoMercadoTesouroMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (CotacaoMercadoTesouroMetadata.meta == null)
				{
					CotacaoMercadoTesouroMetadata.meta = new CotacaoMercadoTesouroMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("DataReferencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DataVencimento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("PU", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "CotacaoMercadoTesouro";
				meta.Destination = "CotacaoMercadoTesouro";
				
				meta.spInsert = "proc_CotacaoMercadoTesouroInsert";				
				meta.spUpdate = "proc_CotacaoMercadoTesouroUpdate";		
				meta.spDelete = "proc_CotacaoMercadoTesouroDelete";
				meta.spLoadAll = "proc_CotacaoMercadoTesouroLoadAll";
				meta.spLoadByPrimaryKey = "proc_CotacaoMercadoTesouroLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private CotacaoMercadoTesouroMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
