/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 28/10/2015 13:41:00
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.RendaFixa
{

	[Serializable]
	abstract public class esMTMManualCollection : esEntityCollection
	{
		public esMTMManualCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "MTMManualCollection";
		}

		#region Query Logic
		protected void InitQuery(esMTMManualQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esMTMManualQuery);
		}
		#endregion
		
		virtual public MTMManual DetachEntity(MTMManual entity)
		{
			return base.DetachEntity(entity) as MTMManual;
		}
		
		virtual public MTMManual AttachEntity(MTMManual entity)
		{
			return base.AttachEntity(entity) as MTMManual;
		}
		
		virtual public void Combine(MTMManualCollection collection)
		{
			base.Combine(collection);
		}
		
		new public MTMManual this[int index]
		{
			get
			{
				return base[index] as MTMManual;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(MTMManual);
		}
	}



	[Serializable]
	abstract public class esMTMManual : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esMTMManualQuery GetDynamicQuery()
		{
			return null;
		}

		public esMTMManual()
		{

		}

		public esMTMManual(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idMTMManual)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idMTMManual);
			else
				return LoadByPrimaryKeyStoredProcedure(idMTMManual);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idMTMManual)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idMTMManual);
			else
				return LoadByPrimaryKeyStoredProcedure(idMTMManual);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idMTMManual)
		{
			esMTMManualQuery query = this.GetDynamicQuery();
			query.Where(query.IdMTMManual == idMTMManual);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idMTMManual)
		{
			esParameters parms = new esParameters();
			parms.Add("IdMTMManual",idMTMManual);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdMTMManual": this.str.IdMTMManual = (string)value; break;							
						case "IdOperacao": this.str.IdOperacao = (string)value; break;							
						case "DtVigencia": this.str.DtVigencia = (string)value; break;							
						case "Taxa252": this.str.Taxa252 = (string)value; break;							
						case "PuMTM": this.str.PuMTM = (string)value; break;							
						case "IdTitulo": this.str.IdTitulo = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdMTMManual":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdMTMManual = (System.Int32?)value;
							break;
						
						case "IdOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacao = (System.Int32?)value;
							break;
						
						case "DtVigencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DtVigencia = (System.DateTime?)value;
							break;
						
						case "Taxa252":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Taxa252 = (System.Decimal?)value;
							break;
						
						case "PuMTM":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PuMTM = (System.Decimal?)value;
							break;
						
						case "IdTitulo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTitulo = (System.Int32?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to MTMManual.IdMTMManual
		/// </summary>
		virtual public System.Int32? IdMTMManual
		{
			get
			{
				return base.GetSystemInt32(MTMManualMetadata.ColumnNames.IdMTMManual);
			}
			
			set
			{
				base.SetSystemInt32(MTMManualMetadata.ColumnNames.IdMTMManual, value);
			}
		}
		
		/// <summary>
		/// Maps to MTMManual.IdOperacao
		/// </summary>
		virtual public System.Int32? IdOperacao
		{
			get
			{
				return base.GetSystemInt32(MTMManualMetadata.ColumnNames.IdOperacao);
			}
			
			set
			{
				base.SetSystemInt32(MTMManualMetadata.ColumnNames.IdOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to MTMManual.DtVigencia
		/// </summary>
		virtual public System.DateTime? DtVigencia
		{
			get
			{
				return base.GetSystemDateTime(MTMManualMetadata.ColumnNames.DtVigencia);
			}
			
			set
			{
				base.SetSystemDateTime(MTMManualMetadata.ColumnNames.DtVigencia, value);
			}
		}
		
		/// <summary>
		/// Maps to MTMManual.Taxa252
		/// </summary>
		virtual public System.Decimal? Taxa252
		{
			get
			{
				return base.GetSystemDecimal(MTMManualMetadata.ColumnNames.Taxa252);
			}
			
			set
			{
				base.SetSystemDecimal(MTMManualMetadata.ColumnNames.Taxa252, value);
			}
		}
		
		/// <summary>
		/// Maps to MTMManual.PuMTM
		/// </summary>
		virtual public System.Decimal? PuMTM
		{
			get
			{
				return base.GetSystemDecimal(MTMManualMetadata.ColumnNames.PuMTM);
			}
			
			set
			{
				base.SetSystemDecimal(MTMManualMetadata.ColumnNames.PuMTM, value);
			}
		}
		
		/// <summary>
		/// Maps to MTMManual.IdTitulo
		/// </summary>
		virtual public System.Int32? IdTitulo
		{
			get
			{
				return base.GetSystemInt32(MTMManualMetadata.ColumnNames.IdTitulo);
			}
			
			set
			{
				base.SetSystemInt32(MTMManualMetadata.ColumnNames.IdTitulo, value);
			}
		}
		
		/// <summary>
		/// Maps to MTMManual.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(MTMManualMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				base.SetSystemInt32(MTMManualMetadata.ColumnNames.IdCliente, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esMTMManual entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdMTMManual
			{
				get
				{
					System.Int32? data = entity.IdMTMManual;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdMTMManual = null;
					else entity.IdMTMManual = Convert.ToInt32(value);
				}
			}
				
			public System.String IdOperacao
			{
				get
				{
					System.Int32? data = entity.IdOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacao = null;
					else entity.IdOperacao = Convert.ToInt32(value);
				}
			}
				
			public System.String DtVigencia
			{
				get
				{
					System.DateTime? data = entity.DtVigencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DtVigencia = null;
					else entity.DtVigencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String Taxa252
			{
				get
				{
					System.Decimal? data = entity.Taxa252;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Taxa252 = null;
					else entity.Taxa252 = Convert.ToDecimal(value);
				}
			}
				
			public System.String PuMTM
			{
				get
				{
					System.Decimal? data = entity.PuMTM;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PuMTM = null;
					else entity.PuMTM = Convert.ToDecimal(value);
				}
			}
				
			public System.String IdTitulo
			{
				get
				{
					System.Int32? data = entity.IdTitulo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTitulo = null;
					else entity.IdTitulo = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
			

			private esMTMManual entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esMTMManualQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esMTMManual can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class MTMManual : esMTMManual
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esMTMManualQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return MTMManualMetadata.Meta();
			}
		}	
		

		public esQueryItem IdMTMManual
		{
			get
			{
				return new esQueryItem(this, MTMManualMetadata.ColumnNames.IdMTMManual, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdOperacao
		{
			get
			{
				return new esQueryItem(this, MTMManualMetadata.ColumnNames.IdOperacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DtVigencia
		{
			get
			{
				return new esQueryItem(this, MTMManualMetadata.ColumnNames.DtVigencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Taxa252
		{
			get
			{
				return new esQueryItem(this, MTMManualMetadata.ColumnNames.Taxa252, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PuMTM
		{
			get
			{
				return new esQueryItem(this, MTMManualMetadata.ColumnNames.PuMTM, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IdTitulo
		{
			get
			{
				return new esQueryItem(this, MTMManualMetadata.ColumnNames.IdTitulo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, MTMManualMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("MTMManualCollection")]
	public partial class MTMManualCollection : esMTMManualCollection, IEnumerable<MTMManual>
	{
		public MTMManualCollection()
		{

		}
		
		public static implicit operator List<MTMManual>(MTMManualCollection coll)
		{
			List<MTMManual> list = new List<MTMManual>();
			
			foreach (MTMManual emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  MTMManualMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new MTMManualQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new MTMManual(row);
		}

		override protected esEntity CreateEntity()
		{
			return new MTMManual();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public MTMManualQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new MTMManualQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(MTMManualQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public MTMManual AddNew()
		{
			MTMManual entity = base.AddNewEntity() as MTMManual;
			
			return entity;
		}

		public MTMManual FindByPrimaryKey(System.Int32 idMTMManual)
		{
			return base.FindByPrimaryKey(idMTMManual) as MTMManual;
		}


		#region IEnumerable<MTMManual> Members

		IEnumerator<MTMManual> IEnumerable<MTMManual>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as MTMManual;
			}
		}

		#endregion
		
		private MTMManualQuery query;
	}


	/// <summary>
	/// Encapsulates the 'MTMManual' table
	/// </summary>

	[Serializable]
	public partial class MTMManual : esMTMManual
	{
		public MTMManual()
		{

		}
	
		public MTMManual(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return MTMManualMetadata.Meta();
			}
		}
		
		
		
		override protected esMTMManualQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new MTMManualQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public MTMManualQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new MTMManualQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(MTMManualQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private MTMManualQuery query;
	}



	[Serializable]
	public partial class MTMManualQuery : esMTMManualQuery
	{
		public MTMManualQuery()
		{

		}		
		
		public MTMManualQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class MTMManualMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected MTMManualMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(MTMManualMetadata.ColumnNames.IdMTMManual, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = MTMManualMetadata.PropertyNames.IdMTMManual;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(MTMManualMetadata.ColumnNames.IdOperacao, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = MTMManualMetadata.PropertyNames.IdOperacao;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(MTMManualMetadata.ColumnNames.DtVigencia, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = MTMManualMetadata.PropertyNames.DtVigencia;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(MTMManualMetadata.ColumnNames.Taxa252, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = MTMManualMetadata.PropertyNames.Taxa252;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(MTMManualMetadata.ColumnNames.PuMTM, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = MTMManualMetadata.PropertyNames.PuMTM;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(MTMManualMetadata.ColumnNames.IdTitulo, 5, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = MTMManualMetadata.PropertyNames.IdTitulo;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(MTMManualMetadata.ColumnNames.IdCliente, 6, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = MTMManualMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public MTMManualMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdMTMManual = "IdMTMManual";
			 public const string IdOperacao = "IdOperacao";
			 public const string DtVigencia = "DtVigencia";
			 public const string Taxa252 = "Taxa252";
			 public const string PuMTM = "PuMTM";
			 public const string IdTitulo = "IdTitulo";
			 public const string IdCliente = "IdCliente";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdMTMManual = "IdMTMManual";
			 public const string IdOperacao = "IdOperacao";
			 public const string DtVigencia = "DtVigencia";
			 public const string Taxa252 = "Taxa252";
			 public const string PuMTM = "PuMTM";
			 public const string IdTitulo = "IdTitulo";
			 public const string IdCliente = "IdCliente";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(MTMManualMetadata))
			{
				if(MTMManualMetadata.mapDelegates == null)
				{
					MTMManualMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (MTMManualMetadata.meta == null)
				{
					MTMManualMetadata.meta = new MTMManualMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdMTMManual", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdOperacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DtVigencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Taxa252", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PuMTM", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IdTitulo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "MTMManual";
				meta.Destination = "MTMManual";
				
				meta.spInsert = "proc_MTMManualInsert";				
				meta.spUpdate = "proc_MTMManualUpdate";		
				meta.spDelete = "proc_MTMManualDelete";
				meta.spLoadAll = "proc_MTMManualLoadAll";
				meta.spLoadByPrimaryKey = "proc_MTMManualLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private MTMManualMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
