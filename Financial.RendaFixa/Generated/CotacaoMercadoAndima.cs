/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 12/12/2014 10:53:29 AM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.RendaFixa
{

	[Serializable]
	abstract public class esCotacaoMercadoAndimaCollection : esEntityCollection
	{
		public esCotacaoMercadoAndimaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "CotacaoMercadoAndimaCollection";
		}

		#region Query Logic
		protected void InitQuery(esCotacaoMercadoAndimaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esCotacaoMercadoAndimaQuery);
		}
		#endregion
		
		virtual public CotacaoMercadoAndima DetachEntity(CotacaoMercadoAndima entity)
		{
			return base.DetachEntity(entity) as CotacaoMercadoAndima;
		}
		
		virtual public CotacaoMercadoAndima AttachEntity(CotacaoMercadoAndima entity)
		{
			return base.AttachEntity(entity) as CotacaoMercadoAndima;
		}
		
		virtual public void Combine(CotacaoMercadoAndimaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public CotacaoMercadoAndima this[int index]
		{
			get
			{
				return base[index] as CotacaoMercadoAndima;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(CotacaoMercadoAndima);
		}
	}



	[Serializable]
	abstract public class esCotacaoMercadoAndima : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esCotacaoMercadoAndimaQuery GetDynamicQuery()
		{
			return null;
		}

		public esCotacaoMercadoAndima()
		{

		}

		public esCotacaoMercadoAndima(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime dataReferencia, System.String descricao, System.String codigoSELIC, System.DateTime dataEmissao, System.DateTime dataVencimento)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataReferencia, descricao, codigoSELIC, dataEmissao, dataVencimento);
			else
				return LoadByPrimaryKeyStoredProcedure(dataReferencia, descricao, codigoSELIC, dataEmissao, dataVencimento);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.DateTime dataReferencia, System.String descricao, System.String codigoSELIC, System.DateTime dataEmissao, System.DateTime dataVencimento)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esCotacaoMercadoAndimaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.DataReferencia == dataReferencia, query.Descricao == descricao, query.CodigoSELIC == codigoSELIC, query.DataEmissao == dataEmissao, query.DataVencimento == dataVencimento);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime dataReferencia, System.String descricao, System.String codigoSELIC, System.DateTime dataEmissao, System.DateTime dataVencimento)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataReferencia, descricao, codigoSELIC, dataEmissao, dataVencimento);
			else
				return LoadByPrimaryKeyStoredProcedure(dataReferencia, descricao, codigoSELIC, dataEmissao, dataVencimento);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime dataReferencia, System.String descricao, System.String codigoSELIC, System.DateTime dataEmissao, System.DateTime dataVencimento)
		{
			esCotacaoMercadoAndimaQuery query = this.GetDynamicQuery();
			query.Where(query.DataReferencia == dataReferencia, query.Descricao == descricao, query.CodigoSELIC == codigoSELIC, query.DataEmissao == dataEmissao, query.DataVencimento == dataVencimento);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime dataReferencia, System.String descricao, System.String codigoSELIC, System.DateTime dataEmissao, System.DateTime dataVencimento)
		{
			esParameters parms = new esParameters();
			parms.Add("DataReferencia",dataReferencia);			parms.Add("Descricao",descricao);			parms.Add("CodigoSELIC",codigoSELIC);			parms.Add("DataEmissao",dataEmissao);			parms.Add("DataVencimento",dataVencimento);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "DataReferencia": this.str.DataReferencia = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;							
						case "CodigoSELIC": this.str.CodigoSELIC = (string)value; break;							
						case "DataEmissao": this.str.DataEmissao = (string)value; break;							
						case "DataVencimento": this.str.DataVencimento = (string)value; break;							
						case "TaxaIndicativa": this.str.TaxaIndicativa = (string)value; break;							
						case "Pu": this.str.Pu = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "DataReferencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataReferencia = (System.DateTime?)value;
							break;
						
						case "DataEmissao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataEmissao = (System.DateTime?)value;
							break;
						
						case "DataVencimento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataVencimento = (System.DateTime?)value;
							break;
						
						case "TaxaIndicativa":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TaxaIndicativa = (System.Decimal?)value;
							break;
						
						case "Pu":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Pu = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to CotacaoMercadoAndima.DataReferencia
		/// </summary>
		virtual public System.DateTime? DataReferencia
		{
			get
			{
				return base.GetSystemDateTime(CotacaoMercadoAndimaMetadata.ColumnNames.DataReferencia);
			}
			
			set
			{
				base.SetSystemDateTime(CotacaoMercadoAndimaMetadata.ColumnNames.DataReferencia, value);
			}
		}
		
		/// <summary>
		/// Maps to CotacaoMercadoAndima.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(CotacaoMercadoAndimaMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(CotacaoMercadoAndimaMetadata.ColumnNames.Descricao, value);
			}
		}
		
		/// <summary>
		/// Maps to CotacaoMercadoAndima.CodigoSELIC
		/// </summary>
		virtual public System.String CodigoSELIC
		{
			get
			{
				return base.GetSystemString(CotacaoMercadoAndimaMetadata.ColumnNames.CodigoSELIC);
			}
			
			set
			{
				base.SetSystemString(CotacaoMercadoAndimaMetadata.ColumnNames.CodigoSELIC, value);
			}
		}
		
		/// <summary>
		/// Maps to CotacaoMercadoAndima.DataEmissao
		/// </summary>
		virtual public System.DateTime? DataEmissao
		{
			get
			{
				return base.GetSystemDateTime(CotacaoMercadoAndimaMetadata.ColumnNames.DataEmissao);
			}
			
			set
			{
				base.SetSystemDateTime(CotacaoMercadoAndimaMetadata.ColumnNames.DataEmissao, value);
			}
		}
		
		/// <summary>
		/// Maps to CotacaoMercadoAndima.DataVencimento
		/// </summary>
		virtual public System.DateTime? DataVencimento
		{
			get
			{
				return base.GetSystemDateTime(CotacaoMercadoAndimaMetadata.ColumnNames.DataVencimento);
			}
			
			set
			{
				base.SetSystemDateTime(CotacaoMercadoAndimaMetadata.ColumnNames.DataVencimento, value);
			}
		}
		
		/// <summary>
		/// Maps to CotacaoMercadoAndima.TaxaIndicativa
		/// </summary>
		virtual public System.Decimal? TaxaIndicativa
		{
			get
			{
				return base.GetSystemDecimal(CotacaoMercadoAndimaMetadata.ColumnNames.TaxaIndicativa);
			}
			
			set
			{
				base.SetSystemDecimal(CotacaoMercadoAndimaMetadata.ColumnNames.TaxaIndicativa, value);
			}
		}
		
		/// <summary>
		/// Maps to CotacaoMercadoAndima.PU
		/// </summary>
		virtual public System.Decimal? Pu
		{
			get
			{
				return base.GetSystemDecimal(CotacaoMercadoAndimaMetadata.ColumnNames.Pu);
			}
			
			set
			{
				base.SetSystemDecimal(CotacaoMercadoAndimaMetadata.ColumnNames.Pu, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esCotacaoMercadoAndima entity)
			{
				this.entity = entity;
			}
			
	
			public System.String DataReferencia
			{
				get
				{
					System.DateTime? data = entity.DataReferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataReferencia = null;
					else entity.DataReferencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
				
			public System.String CodigoSELIC
			{
				get
				{
					System.String data = entity.CodigoSELIC;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoSELIC = null;
					else entity.CodigoSELIC = Convert.ToString(value);
				}
			}
				
			public System.String DataEmissao
			{
				get
				{
					System.DateTime? data = entity.DataEmissao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataEmissao = null;
					else entity.DataEmissao = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataVencimento
			{
				get
				{
					System.DateTime? data = entity.DataVencimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataVencimento = null;
					else entity.DataVencimento = Convert.ToDateTime(value);
				}
			}
				
			public System.String TaxaIndicativa
			{
				get
				{
					System.Decimal? data = entity.TaxaIndicativa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TaxaIndicativa = null;
					else entity.TaxaIndicativa = Convert.ToDecimal(value);
				}
			}
				
			public System.String Pu
			{
				get
				{
					System.Decimal? data = entity.Pu;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Pu = null;
					else entity.Pu = Convert.ToDecimal(value);
				}
			}
			

			private esCotacaoMercadoAndima entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esCotacaoMercadoAndimaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esCotacaoMercadoAndima can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class CotacaoMercadoAndima : esCotacaoMercadoAndima
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esCotacaoMercadoAndimaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return CotacaoMercadoAndimaMetadata.Meta();
			}
		}	
		

		public esQueryItem DataReferencia
		{
			get
			{
				return new esQueryItem(this, CotacaoMercadoAndimaMetadata.ColumnNames.DataReferencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, CotacaoMercadoAndimaMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
		public esQueryItem CodigoSELIC
		{
			get
			{
				return new esQueryItem(this, CotacaoMercadoAndimaMetadata.ColumnNames.CodigoSELIC, esSystemType.String);
			}
		} 
		
		public esQueryItem DataEmissao
		{
			get
			{
				return new esQueryItem(this, CotacaoMercadoAndimaMetadata.ColumnNames.DataEmissao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataVencimento
		{
			get
			{
				return new esQueryItem(this, CotacaoMercadoAndimaMetadata.ColumnNames.DataVencimento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem TaxaIndicativa
		{
			get
			{
				return new esQueryItem(this, CotacaoMercadoAndimaMetadata.ColumnNames.TaxaIndicativa, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Pu
		{
			get
			{
				return new esQueryItem(this, CotacaoMercadoAndimaMetadata.ColumnNames.Pu, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("CotacaoMercadoAndimaCollection")]
	public partial class CotacaoMercadoAndimaCollection : esCotacaoMercadoAndimaCollection, IEnumerable<CotacaoMercadoAndima>
	{
		public CotacaoMercadoAndimaCollection()
		{

		}
		
		public static implicit operator List<CotacaoMercadoAndima>(CotacaoMercadoAndimaCollection coll)
		{
			List<CotacaoMercadoAndima> list = new List<CotacaoMercadoAndima>();
			
			foreach (CotacaoMercadoAndima emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  CotacaoMercadoAndimaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CotacaoMercadoAndimaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new CotacaoMercadoAndima(row);
		}

		override protected esEntity CreateEntity()
		{
			return new CotacaoMercadoAndima();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public CotacaoMercadoAndimaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CotacaoMercadoAndimaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(CotacaoMercadoAndimaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public CotacaoMercadoAndima AddNew()
		{
			CotacaoMercadoAndima entity = base.AddNewEntity() as CotacaoMercadoAndima;
			
			return entity;
		}

		public CotacaoMercadoAndima FindByPrimaryKey(System.DateTime dataReferencia, System.String descricao, System.String codigoSELIC, System.DateTime dataEmissao, System.DateTime dataVencimento)
		{
			return base.FindByPrimaryKey(dataReferencia, descricao, codigoSELIC, dataEmissao, dataVencimento) as CotacaoMercadoAndima;
		}


		#region IEnumerable<CotacaoMercadoAndima> Members

		IEnumerator<CotacaoMercadoAndima> IEnumerable<CotacaoMercadoAndima>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as CotacaoMercadoAndima;
			}
		}

		#endregion
		
		private CotacaoMercadoAndimaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'CotacaoMercadoAndima' table
	/// </summary>

	[Serializable]
	public partial class CotacaoMercadoAndima : esCotacaoMercadoAndima
	{
		public CotacaoMercadoAndima()
		{

		}
	
		public CotacaoMercadoAndima(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return CotacaoMercadoAndimaMetadata.Meta();
			}
		}
		
		
		
		override protected esCotacaoMercadoAndimaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CotacaoMercadoAndimaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public CotacaoMercadoAndimaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CotacaoMercadoAndimaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(CotacaoMercadoAndimaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private CotacaoMercadoAndimaQuery query;
	}



	[Serializable]
	public partial class CotacaoMercadoAndimaQuery : esCotacaoMercadoAndimaQuery
	{
		public CotacaoMercadoAndimaQuery()
		{

		}		
		
		public CotacaoMercadoAndimaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class CotacaoMercadoAndimaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected CotacaoMercadoAndimaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(CotacaoMercadoAndimaMetadata.ColumnNames.DataReferencia, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = CotacaoMercadoAndimaMetadata.PropertyNames.DataReferencia;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotacaoMercadoAndimaMetadata.ColumnNames.Descricao, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = CotacaoMercadoAndimaMetadata.PropertyNames.Descricao;
			c.IsInPrimaryKey = true;
			c.CharacterMaxLength = 20;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotacaoMercadoAndimaMetadata.ColumnNames.CodigoSELIC, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = CotacaoMercadoAndimaMetadata.PropertyNames.CodigoSELIC;
			c.IsInPrimaryKey = true;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotacaoMercadoAndimaMetadata.ColumnNames.DataEmissao, 3, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = CotacaoMercadoAndimaMetadata.PropertyNames.DataEmissao;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotacaoMercadoAndimaMetadata.ColumnNames.DataVencimento, 4, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = CotacaoMercadoAndimaMetadata.PropertyNames.DataVencimento;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotacaoMercadoAndimaMetadata.ColumnNames.TaxaIndicativa, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CotacaoMercadoAndimaMetadata.PropertyNames.TaxaIndicativa;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotacaoMercadoAndimaMetadata.ColumnNames.Pu, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CotacaoMercadoAndimaMetadata.PropertyNames.Pu;	
			c.NumericPrecision = 28;
			c.NumericScale = 16;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public CotacaoMercadoAndimaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string DataReferencia = "DataReferencia";
			 public const string Descricao = "Descricao";
			 public const string CodigoSELIC = "CodigoSELIC";
			 public const string DataEmissao = "DataEmissao";
			 public const string DataVencimento = "DataVencimento";
			 public const string TaxaIndicativa = "TaxaIndicativa";
			 public const string Pu = "PU";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string DataReferencia = "DataReferencia";
			 public const string Descricao = "Descricao";
			 public const string CodigoSELIC = "CodigoSELIC";
			 public const string DataEmissao = "DataEmissao";
			 public const string DataVencimento = "DataVencimento";
			 public const string TaxaIndicativa = "TaxaIndicativa";
			 public const string Pu = "Pu";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(CotacaoMercadoAndimaMetadata))
			{
				if(CotacaoMercadoAndimaMetadata.mapDelegates == null)
				{
					CotacaoMercadoAndimaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (CotacaoMercadoAndimaMetadata.meta == null)
				{
					CotacaoMercadoAndimaMetadata.meta = new CotacaoMercadoAndimaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("DataReferencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("CodigoSELIC", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DataEmissao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataVencimento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("TaxaIndicativa", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PU", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "CotacaoMercadoAndima";
				meta.Destination = "CotacaoMercadoAndima";
				
				meta.spInsert = "proc_CotacaoMercadoAndimaInsert";				
				meta.spUpdate = "proc_CotacaoMercadoAndimaUpdate";		
				meta.spDelete = "proc_CotacaoMercadoAndimaDelete";
				meta.spLoadAll = "proc_CotacaoMercadoAndimaLoadAll";
				meta.spLoadByPrimaryKey = "proc_CotacaoMercadoAndimaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private CotacaoMercadoAndimaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
