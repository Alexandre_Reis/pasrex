/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 24/07/2015 11:54:01
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Common;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.RendaFixa
{

	[Serializable]
	abstract public class esSerieRendaFixaCollection : esEntityCollection
	{
		public esSerieRendaFixaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "SerieRendaFixaCollection";
		}

		#region Query Logic
		protected void InitQuery(esSerieRendaFixaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esSerieRendaFixaQuery);
		}
		#endregion
		
		virtual public SerieRendaFixa DetachEntity(SerieRendaFixa entity)
		{
			return base.DetachEntity(entity) as SerieRendaFixa;
		}
		
		virtual public SerieRendaFixa AttachEntity(SerieRendaFixa entity)
		{
			return base.AttachEntity(entity) as SerieRendaFixa;
		}
		
		virtual public void Combine(SerieRendaFixaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public SerieRendaFixa this[int index]
		{
			get
			{
				return base[index] as SerieRendaFixa;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(SerieRendaFixa);
		}
	}



	[Serializable]
	abstract public class esSerieRendaFixa : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esSerieRendaFixaQuery GetDynamicQuery()
		{
			return null;
		}

		public esSerieRendaFixa()
		{

		}

		public esSerieRendaFixa(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idSerie)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idSerie);
			else
				return LoadByPrimaryKeyStoredProcedure(idSerie);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idSerie)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esSerieRendaFixaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdSerie == idSerie);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idSerie)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idSerie);
			else
				return LoadByPrimaryKeyStoredProcedure(idSerie);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idSerie)
		{
			esSerieRendaFixaQuery query = this.GetDynamicQuery();
			query.Where(query.IdSerie == idSerie);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idSerie)
		{
			esParameters parms = new esParameters();
			parms.Add("IdSerie",idSerie);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdSerie": this.str.IdSerie = (string)value; break;							
						case "Descricao": this.str.Descricao = (string)value; break;							
						case "TipoTaxa": this.str.TipoTaxa = (string)value; break;							
						case "IdFeeder": this.str.IdFeeder = (string)value; break;							
						case "CodigoBDS": this.str.CodigoBDS = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdSerie":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdSerie = (System.Int32?)value;
							break;
						
						case "TipoTaxa":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.TipoTaxa = (System.Int32?)value;
							break;
						
						case "IdFeeder":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdFeeder = (System.Int32?)value;
							break;
						
						case "CodigoBDS":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.CodigoBDS = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to SerieRendaFixa.IdSerie
		/// </summary>
		virtual public System.Int32? IdSerie
		{
			get
			{
				return base.GetSystemInt32(SerieRendaFixaMetadata.ColumnNames.IdSerie);
			}
			
			set
			{
				base.SetSystemInt32(SerieRendaFixaMetadata.ColumnNames.IdSerie, value);
			}
		}
		
		/// <summary>
		/// Maps to SerieRendaFixa.Descricao
		/// </summary>
		virtual public System.String Descricao
		{
			get
			{
				return base.GetSystemString(SerieRendaFixaMetadata.ColumnNames.Descricao);
			}
			
			set
			{
				base.SetSystemString(SerieRendaFixaMetadata.ColumnNames.Descricao, value);
			}
		}
		
		/// <summary>
		/// Maps to SerieRendaFixa.TipoTaxa
		/// </summary>
		virtual public System.Int32? TipoTaxa
		{
			get
			{
				return base.GetSystemInt32(SerieRendaFixaMetadata.ColumnNames.TipoTaxa);
			}
			
			set
			{
				base.SetSystemInt32(SerieRendaFixaMetadata.ColumnNames.TipoTaxa, value);
			}
		}
		
		/// <summary>
		/// Maps to SerieRendaFixa.IdFeeder
		/// </summary>
		virtual public System.Int32? IdFeeder
		{
			get
			{
				return base.GetSystemInt32(SerieRendaFixaMetadata.ColumnNames.IdFeeder);
			}
			
			set
			{
				if(base.SetSystemInt32(SerieRendaFixaMetadata.ColumnNames.IdFeeder, value))
				{
					this._UpToFeederByIdFeeder = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to SerieRendaFixa.CodigoBDS
		/// </summary>
		virtual public System.Int32? CodigoBDS
		{
			get
			{
				return base.GetSystemInt32(SerieRendaFixaMetadata.ColumnNames.CodigoBDS);
			}
			
			set
			{
				base.SetSystemInt32(SerieRendaFixaMetadata.ColumnNames.CodigoBDS, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Feeder _UpToFeederByIdFeeder;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esSerieRendaFixa entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdSerie
			{
				get
				{
					System.Int32? data = entity.IdSerie;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdSerie = null;
					else entity.IdSerie = Convert.ToInt32(value);
				}
			}
				
			public System.String Descricao
			{
				get
				{
					System.String data = entity.Descricao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Descricao = null;
					else entity.Descricao = Convert.ToString(value);
				}
			}
				
			public System.String TipoTaxa
			{
				get
				{
					System.Int32? data = entity.TipoTaxa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoTaxa = null;
					else entity.TipoTaxa = Convert.ToInt32(value);
				}
			}
				
			public System.String IdFeeder
			{
				get
				{
					System.Int32? data = entity.IdFeeder;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdFeeder = null;
					else entity.IdFeeder = Convert.ToInt32(value);
				}
			}
				
			public System.String CodigoBDS
			{
				get
				{
					System.Int32? data = entity.CodigoBDS;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoBDS = null;
					else entity.CodigoBDS = Convert.ToInt32(value);
				}
			}
			

			private esSerieRendaFixa entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esSerieRendaFixaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esSerieRendaFixa can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class SerieRendaFixa : esSerieRendaFixa
	{

				
		#region CotacaoSerieCollectionByIdSerie - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - SerieRendaFixa_CotacaoSerie_FK1
		/// </summary>

		[XmlIgnore]
		public CotacaoSerieCollection CotacaoSerieCollectionByIdSerie
		{
			get
			{
				if(this._CotacaoSerieCollectionByIdSerie == null)
				{
					this._CotacaoSerieCollectionByIdSerie = new CotacaoSerieCollection();
					this._CotacaoSerieCollectionByIdSerie.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("CotacaoSerieCollectionByIdSerie", this._CotacaoSerieCollectionByIdSerie);
				
					if(this.IdSerie != null)
					{
						this._CotacaoSerieCollectionByIdSerie.Query.Where(this._CotacaoSerieCollectionByIdSerie.Query.IdSerie == this.IdSerie);
						this._CotacaoSerieCollectionByIdSerie.Query.Load();

						// Auto-hookup Foreign Keys
						this._CotacaoSerieCollectionByIdSerie.fks.Add(CotacaoSerieMetadata.ColumnNames.IdSerie, this.IdSerie);
					}
				}

				return this._CotacaoSerieCollectionByIdSerie;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._CotacaoSerieCollectionByIdSerie != null) 
				{ 
					this.RemovePostSave("CotacaoSerieCollectionByIdSerie"); 
					this._CotacaoSerieCollectionByIdSerie = null;
					
				} 
			} 			
		}

		private CotacaoSerieCollection _CotacaoSerieCollectionByIdSerie;
		#endregion

				
		#region PerfilMTMCollectionByIdSerie - Zero To Many
		/// <summary>
		/// Zero to Many
		/// Foreign Key Name - PerfilMTM_Serie_FK
		/// </summary>

		[XmlIgnore]
		public PerfilMTMCollection PerfilMTMCollectionByIdSerie
		{
			get
			{
				if(this._PerfilMTMCollectionByIdSerie == null)
				{
					this._PerfilMTMCollectionByIdSerie = new PerfilMTMCollection();
					this._PerfilMTMCollectionByIdSerie.es.Connection.Name = this.es.Connection.Name;
					this.SetPostSave("PerfilMTMCollectionByIdSerie", this._PerfilMTMCollectionByIdSerie);
				
					if(this.IdSerie != null)
					{
						this._PerfilMTMCollectionByIdSerie.Query.Where(this._PerfilMTMCollectionByIdSerie.Query.IdSerie == this.IdSerie);
						this._PerfilMTMCollectionByIdSerie.Query.Load();

						// Auto-hookup Foreign Keys
						this._PerfilMTMCollectionByIdSerie.fks.Add(PerfilMTMMetadata.ColumnNames.IdSerie, this.IdSerie);
					}
				}

				return this._PerfilMTMCollectionByIdSerie;
			}
			
			set 
			{ 
				if (value != null) throw new Exception("'value' Must be null"); 
			 
				if (this._PerfilMTMCollectionByIdSerie != null) 
				{ 
					this.RemovePostSave("PerfilMTMCollectionByIdSerie"); 
					this._PerfilMTMCollectionByIdSerie = null;
					
				} 
			} 			
		}

		private PerfilMTMCollection _PerfilMTMCollectionByIdSerie;
		#endregion

				
		#region UpToFeederByIdFeeder - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - SerieRF_Feeder_FK
		/// </summary>

		[XmlIgnore]
		public Feeder UpToFeederByIdFeeder
		{
			get
			{
				if(this._UpToFeederByIdFeeder == null
					&& IdFeeder != null					)
				{
					this._UpToFeederByIdFeeder = new Feeder();
					this._UpToFeederByIdFeeder.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToFeederByIdFeeder", this._UpToFeederByIdFeeder);
					this._UpToFeederByIdFeeder.Query.Where(this._UpToFeederByIdFeeder.Query.IdFeeder == this.IdFeeder);
					this._UpToFeederByIdFeeder.Query.Load();
				}

				return this._UpToFeederByIdFeeder;
			}
			
			set
			{
				this.RemovePreSave("UpToFeederByIdFeeder");
				

				if(value == null)
				{
					this.IdFeeder = null;
					this._UpToFeederByIdFeeder = null;
				}
				else
				{
					this.IdFeeder = value.IdFeeder;
					this._UpToFeederByIdFeeder = value;
					this.SetPreSave("UpToFeederByIdFeeder", this._UpToFeederByIdFeeder);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
			props.Add(new esPropertyDescriptor(this, "CotacaoSerieCollectionByIdSerie", typeof(CotacaoSerieCollection), new CotacaoSerie()));
			props.Add(new esPropertyDescriptor(this, "PerfilMTMCollectionByIdSerie", typeof(PerfilMTMCollection), new PerfilMTM()));
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToFeederByIdFeeder != null)
			{
				this.IdFeeder = this._UpToFeederByIdFeeder.IdFeeder;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
			if(this._CotacaoSerieCollectionByIdSerie != null)
			{
				foreach(CotacaoSerie obj in this._CotacaoSerieCollectionByIdSerie)
				{
					if(obj.es.IsAdded)
					{
						obj.IdSerie = this.IdSerie;
					}
				}
			}
			if(this._PerfilMTMCollectionByIdSerie != null)
			{
				foreach(PerfilMTM obj in this._PerfilMTMCollectionByIdSerie)
				{
					if(obj.es.IsAdded)
					{
						obj.IdSerie = this.IdSerie;
					}
				}
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esSerieRendaFixaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return SerieRendaFixaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdSerie
		{
			get
			{
				return new esQueryItem(this, SerieRendaFixaMetadata.ColumnNames.IdSerie, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Descricao
		{
			get
			{
				return new esQueryItem(this, SerieRendaFixaMetadata.ColumnNames.Descricao, esSystemType.String);
			}
		} 
		
		public esQueryItem TipoTaxa
		{
			get
			{
				return new esQueryItem(this, SerieRendaFixaMetadata.ColumnNames.TipoTaxa, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdFeeder
		{
			get
			{
				return new esQueryItem(this, SerieRendaFixaMetadata.ColumnNames.IdFeeder, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CodigoBDS
		{
			get
			{
				return new esQueryItem(this, SerieRendaFixaMetadata.ColumnNames.CodigoBDS, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("SerieRendaFixaCollection")]
	public partial class SerieRendaFixaCollection : esSerieRendaFixaCollection, IEnumerable<SerieRendaFixa>
	{
		public SerieRendaFixaCollection()
		{

		}
		
		public static implicit operator List<SerieRendaFixa>(SerieRendaFixaCollection coll)
		{
			List<SerieRendaFixa> list = new List<SerieRendaFixa>();
			
			foreach (SerieRendaFixa emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  SerieRendaFixaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SerieRendaFixaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new SerieRendaFixa(row);
		}

		override protected esEntity CreateEntity()
		{
			return new SerieRendaFixa();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public SerieRendaFixaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SerieRendaFixaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(SerieRendaFixaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public SerieRendaFixa AddNew()
		{
			SerieRendaFixa entity = base.AddNewEntity() as SerieRendaFixa;
			
			return entity;
		}

		public SerieRendaFixa FindByPrimaryKey(System.Int32 idSerie)
		{
			return base.FindByPrimaryKey(idSerie) as SerieRendaFixa;
		}


		#region IEnumerable<SerieRendaFixa> Members

		IEnumerator<SerieRendaFixa> IEnumerable<SerieRendaFixa>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as SerieRendaFixa;
			}
		}

		#endregion
		
		private SerieRendaFixaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'SerieRendaFixa' table
	/// </summary>

	[Serializable]
	public partial class SerieRendaFixa : esSerieRendaFixa
	{
		public SerieRendaFixa()
		{

		}
	
		public SerieRendaFixa(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return SerieRendaFixaMetadata.Meta();
			}
		}
		
		
		
		override protected esSerieRendaFixaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new SerieRendaFixaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public SerieRendaFixaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new SerieRendaFixaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(SerieRendaFixaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private SerieRendaFixaQuery query;
	}



	[Serializable]
	public partial class SerieRendaFixaQuery : esSerieRendaFixaQuery
	{
		public SerieRendaFixaQuery()
		{

		}		
		
		public SerieRendaFixaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class SerieRendaFixaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected SerieRendaFixaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(SerieRendaFixaMetadata.ColumnNames.IdSerie, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SerieRendaFixaMetadata.PropertyNames.IdSerie;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SerieRendaFixaMetadata.ColumnNames.Descricao, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = SerieRendaFixaMetadata.PropertyNames.Descricao;
			c.CharacterMaxLength = 50;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SerieRendaFixaMetadata.ColumnNames.TipoTaxa, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SerieRendaFixaMetadata.PropertyNames.TipoTaxa;	
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"('0')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SerieRendaFixaMetadata.ColumnNames.IdFeeder, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SerieRendaFixaMetadata.PropertyNames.IdFeeder;	
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"('5')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(SerieRendaFixaMetadata.ColumnNames.CodigoBDS, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = SerieRendaFixaMetadata.PropertyNames.CodigoBDS;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public SerieRendaFixaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdSerie = "IdSerie";
			 public const string Descricao = "Descricao";
			 public const string TipoTaxa = "TipoTaxa";
			 public const string IdFeeder = "IdFeeder";
			 public const string CodigoBDS = "CodigoBDS";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdSerie = "IdSerie";
			 public const string Descricao = "Descricao";
			 public const string TipoTaxa = "TipoTaxa";
			 public const string IdFeeder = "IdFeeder";
			 public const string CodigoBDS = "CodigoBDS";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(SerieRendaFixaMetadata))
			{
				if(SerieRendaFixaMetadata.mapDelegates == null)
				{
					SerieRendaFixaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (SerieRendaFixaMetadata.meta == null)
				{
					SerieRendaFixaMetadata.meta = new SerieRendaFixaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdSerie", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Descricao", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("TipoTaxa", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdFeeder", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CodigoBDS", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "SerieRendaFixa";
				meta.Destination = "SerieRendaFixa";
				
				meta.spInsert = "proc_SerieRendaFixaInsert";				
				meta.spUpdate = "proc_SerieRendaFixaUpdate";		
				meta.spDelete = "proc_SerieRendaFixaDelete";
				meta.spLoadAll = "proc_SerieRendaFixaLoadAll";
				meta.spLoadByPrimaryKey = "proc_SerieRendaFixaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private SerieRendaFixaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
