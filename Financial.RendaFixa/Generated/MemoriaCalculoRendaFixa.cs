/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 03/03/2016 16:02:48
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Investidor;



namespace Financial.RendaFixa
{

	[Serializable]
	abstract public class esMemoriaCalculoRendaFixaCollection : esEntityCollection
	{
		public esMemoriaCalculoRendaFixaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "MemoriaCalculoRendaFixaCollection";
		}

		#region Query Logic
		protected void InitQuery(esMemoriaCalculoRendaFixaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esMemoriaCalculoRendaFixaQuery);
		}
		#endregion
		
		virtual public MemoriaCalculoRendaFixa DetachEntity(MemoriaCalculoRendaFixa entity)
		{
			return base.DetachEntity(entity) as MemoriaCalculoRendaFixa;
		}
		
		virtual public MemoriaCalculoRendaFixa AttachEntity(MemoriaCalculoRendaFixa entity)
		{
			return base.AttachEntity(entity) as MemoriaCalculoRendaFixa;
		}
		
		virtual public void Combine(MemoriaCalculoRendaFixaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public MemoriaCalculoRendaFixa this[int index]
		{
			get
			{
				return base[index] as MemoriaCalculoRendaFixa;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(MemoriaCalculoRendaFixa);
		}
	}



	[Serializable]
	abstract public class esMemoriaCalculoRendaFixa : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esMemoriaCalculoRendaFixaQuery GetDynamicQuery()
		{
			return null;
		}

		public esMemoriaCalculoRendaFixa()
		{

		}

		public esMemoriaCalculoRendaFixa(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idMemoriaCalculoRendaFixa)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idMemoriaCalculoRendaFixa);
			else
				return LoadByPrimaryKeyStoredProcedure(idMemoriaCalculoRendaFixa);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idMemoriaCalculoRendaFixa)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idMemoriaCalculoRendaFixa);
			else
				return LoadByPrimaryKeyStoredProcedure(idMemoriaCalculoRendaFixa);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idMemoriaCalculoRendaFixa)
		{
			esMemoriaCalculoRendaFixaQuery query = this.GetDynamicQuery();
			query.Where(query.IdMemoriaCalculoRendaFixa == idMemoriaCalculoRendaFixa);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idMemoriaCalculoRendaFixa)
		{
			esParameters parms = new esParameters();
			parms.Add("IdMemoriaCalculoRendaFixa",idMemoriaCalculoRendaFixa);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdMemoriaCalculoRendaFixa": this.str.IdMemoriaCalculoRendaFixa = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "DataAtual": this.str.DataAtual = (string)value; break;							
						case "IdOperacao": this.str.IdOperacao = (string)value; break;							
						case "TipoPreco": this.str.TipoPreco = (string)value; break;							
						case "TipoProcessamento": this.str.TipoProcessamento = (string)value; break;							
						case "DataFluxo": this.str.DataFluxo = (string)value; break;							
						case "TaxaDesconto": this.str.TaxaDesconto = (string)value; break;							
						case "ValorVencimento": this.str.ValorVencimento = (string)value; break;							
						case "ValorPresente": this.str.ValorPresente = (string)value; break;							
						case "ValorNominalAjustado": this.str.ValorNominalAjustado = (string)value; break;							
						case "ValorNominal": this.str.ValorNominal = (string)value; break;							
						case "TipoEventoTitulo": this.str.TipoEventoTitulo = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdMemoriaCalculoRendaFixa":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdMemoriaCalculoRendaFixa = (System.Int32?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "DataAtual":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataAtual = (System.DateTime?)value;
							break;
						
						case "IdOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacao = (System.Int32?)value;
							break;
						
						case "TipoPreco":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.TipoPreco = (System.Int32?)value;
							break;
						
						case "TipoProcessamento":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.TipoProcessamento = (System.Int32?)value;
							break;
						
						case "DataFluxo":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataFluxo = (System.DateTime?)value;
							break;
						
						case "TaxaDesconto":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TaxaDesconto = (System.Decimal?)value;
							break;
						
						case "ValorVencimento":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorVencimento = (System.Decimal?)value;
							break;
						
						case "ValorPresente":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorPresente = (System.Decimal?)value;
							break;
						
						case "ValorNominalAjustado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorNominalAjustado = (System.Decimal?)value;
							break;
						
						case "ValorNominal":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorNominal = (System.Decimal?)value;
							break;
						
						case "TipoEventoTitulo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.TipoEventoTitulo = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to MemoriaCalculoRendaFixa.IdMemoriaCalculoRendaFixa
		/// </summary>
		virtual public System.Int32? IdMemoriaCalculoRendaFixa
		{
			get
			{
				return base.GetSystemInt32(MemoriaCalculoRendaFixaMetadata.ColumnNames.IdMemoriaCalculoRendaFixa);
			}
			
			set
			{
				base.SetSystemInt32(MemoriaCalculoRendaFixaMetadata.ColumnNames.IdMemoriaCalculoRendaFixa, value);
			}
		}
		
		/// <summary>
		/// Maps to MemoriaCalculoRendaFixa.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(MemoriaCalculoRendaFixaMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(MemoriaCalculoRendaFixaMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to MemoriaCalculoRendaFixa.DataAtual
		/// </summary>
		virtual public System.DateTime? DataAtual
		{
			get
			{
				return base.GetSystemDateTime(MemoriaCalculoRendaFixaMetadata.ColumnNames.DataAtual);
			}
			
			set
			{
				base.SetSystemDateTime(MemoriaCalculoRendaFixaMetadata.ColumnNames.DataAtual, value);
			}
		}
		
		/// <summary>
		/// Maps to MemoriaCalculoRendaFixa.IdOperacao
		/// </summary>
		virtual public System.Int32? IdOperacao
		{
			get
			{
				return base.GetSystemInt32(MemoriaCalculoRendaFixaMetadata.ColumnNames.IdOperacao);
			}
			
			set
			{
				if(base.SetSystemInt32(MemoriaCalculoRendaFixaMetadata.ColumnNames.IdOperacao, value))
				{
					this._UpToOperacaoRendaFixaByIdOperacao = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to MemoriaCalculoRendaFixa.TipoPreco
		/// </summary>
		virtual public System.Int32? TipoPreco
		{
			get
			{
				return base.GetSystemInt32(MemoriaCalculoRendaFixaMetadata.ColumnNames.TipoPreco);
			}
			
			set
			{
				base.SetSystemInt32(MemoriaCalculoRendaFixaMetadata.ColumnNames.TipoPreco, value);
			}
		}
		
		/// <summary>
		/// Maps to MemoriaCalculoRendaFixa.TipoProcessamento
		/// </summary>
		virtual public System.Int32? TipoProcessamento
		{
			get
			{
				return base.GetSystemInt32(MemoriaCalculoRendaFixaMetadata.ColumnNames.TipoProcessamento);
			}
			
			set
			{
				base.SetSystemInt32(MemoriaCalculoRendaFixaMetadata.ColumnNames.TipoProcessamento, value);
			}
		}
		
		/// <summary>
		/// Maps to MemoriaCalculoRendaFixa.DataFluxo
		/// </summary>
		virtual public System.DateTime? DataFluxo
		{
			get
			{
				return base.GetSystemDateTime(MemoriaCalculoRendaFixaMetadata.ColumnNames.DataFluxo);
			}
			
			set
			{
				base.SetSystemDateTime(MemoriaCalculoRendaFixaMetadata.ColumnNames.DataFluxo, value);
			}
		}
		
		/// <summary>
		/// Maps to MemoriaCalculoRendaFixa.TaxaDesconto
		/// </summary>
		virtual public System.Decimal? TaxaDesconto
		{
			get
			{
				return base.GetSystemDecimal(MemoriaCalculoRendaFixaMetadata.ColumnNames.TaxaDesconto);
			}
			
			set
			{
				base.SetSystemDecimal(MemoriaCalculoRendaFixaMetadata.ColumnNames.TaxaDesconto, value);
			}
		}
		
		/// <summary>
		/// Maps to MemoriaCalculoRendaFixa.ValorVencimento
		/// </summary>
		virtual public System.Decimal? ValorVencimento
		{
			get
			{
				return base.GetSystemDecimal(MemoriaCalculoRendaFixaMetadata.ColumnNames.ValorVencimento);
			}
			
			set
			{
				base.SetSystemDecimal(MemoriaCalculoRendaFixaMetadata.ColumnNames.ValorVencimento, value);
			}
		}
		
		/// <summary>
		/// Maps to MemoriaCalculoRendaFixa.ValorPresente
		/// </summary>
		virtual public System.Decimal? ValorPresente
		{
			get
			{
				return base.GetSystemDecimal(MemoriaCalculoRendaFixaMetadata.ColumnNames.ValorPresente);
			}
			
			set
			{
				base.SetSystemDecimal(MemoriaCalculoRendaFixaMetadata.ColumnNames.ValorPresente, value);
			}
		}
		
		/// <summary>
		/// Maps to MemoriaCalculoRendaFixa.ValorNominalAjustado
		/// </summary>
		virtual public System.Decimal? ValorNominalAjustado
		{
			get
			{
				return base.GetSystemDecimal(MemoriaCalculoRendaFixaMetadata.ColumnNames.ValorNominalAjustado);
			}
			
			set
			{
				base.SetSystemDecimal(MemoriaCalculoRendaFixaMetadata.ColumnNames.ValorNominalAjustado, value);
			}
		}
		
		/// <summary>
		/// Maps to MemoriaCalculoRendaFixa.ValorNominal
		/// </summary>
		virtual public System.Decimal? ValorNominal
		{
			get
			{
				return base.GetSystemDecimal(MemoriaCalculoRendaFixaMetadata.ColumnNames.ValorNominal);
			}
			
			set
			{
				base.SetSystemDecimal(MemoriaCalculoRendaFixaMetadata.ColumnNames.ValorNominal, value);
			}
		}
		
		/// <summary>
		/// Maps to MemoriaCalculoRendaFixa.TipoEventoTitulo
		/// </summary>
		virtual public System.Int32? TipoEventoTitulo
		{
			get
			{
				return base.GetSystemInt32(MemoriaCalculoRendaFixaMetadata.ColumnNames.TipoEventoTitulo);
			}
			
			set
			{
				base.SetSystemInt32(MemoriaCalculoRendaFixaMetadata.ColumnNames.TipoEventoTitulo, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		[CLSCompliant(false)]
		internal protected OperacaoRendaFixa _UpToOperacaoRendaFixaByIdOperacao;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esMemoriaCalculoRendaFixa entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdMemoriaCalculoRendaFixa
			{
				get
				{
					System.Int32? data = entity.IdMemoriaCalculoRendaFixa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdMemoriaCalculoRendaFixa = null;
					else entity.IdMemoriaCalculoRendaFixa = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String DataAtual
			{
				get
				{
					System.DateTime? data = entity.DataAtual;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataAtual = null;
					else entity.DataAtual = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdOperacao
			{
				get
				{
					System.Int32? data = entity.IdOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacao = null;
					else entity.IdOperacao = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoPreco
			{
				get
				{
					System.Int32? data = entity.TipoPreco;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoPreco = null;
					else entity.TipoPreco = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoProcessamento
			{
				get
				{
					System.Int32? data = entity.TipoProcessamento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoProcessamento = null;
					else entity.TipoProcessamento = Convert.ToInt32(value);
				}
			}
				
			public System.String DataFluxo
			{
				get
				{
					System.DateTime? data = entity.DataFluxo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataFluxo = null;
					else entity.DataFluxo = Convert.ToDateTime(value);
				}
			}
				
			public System.String TaxaDesconto
			{
				get
				{
					System.Decimal? data = entity.TaxaDesconto;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TaxaDesconto = null;
					else entity.TaxaDesconto = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorVencimento
			{
				get
				{
					System.Decimal? data = entity.ValorVencimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorVencimento = null;
					else entity.ValorVencimento = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorPresente
			{
				get
				{
					System.Decimal? data = entity.ValorPresente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorPresente = null;
					else entity.ValorPresente = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorNominalAjustado
			{
				get
				{
					System.Decimal? data = entity.ValorNominalAjustado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorNominalAjustado = null;
					else entity.ValorNominalAjustado = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorNominal
			{
				get
				{
					System.Decimal? data = entity.ValorNominal;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorNominal = null;
					else entity.ValorNominal = Convert.ToDecimal(value);
				}
			}
				
			public System.String TipoEventoTitulo
			{
				get
				{
					System.Int32? data = entity.TipoEventoTitulo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoEventoTitulo = null;
					else entity.TipoEventoTitulo = Convert.ToInt32(value);
				}
			}
			

			private esMemoriaCalculoRendaFixa entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esMemoriaCalculoRendaFixaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esMemoriaCalculoRendaFixa can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class MemoriaCalculoRendaFixa : esMemoriaCalculoRendaFixa
	{

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK_MemCalcCliente
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToOperacaoRendaFixaByIdOperacao - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - FK_MemCalcOpRF
		/// </summary>

		[XmlIgnore]
		public OperacaoRendaFixa UpToOperacaoRendaFixaByIdOperacao
		{
			get
			{
				if(this._UpToOperacaoRendaFixaByIdOperacao == null
					&& IdOperacao != null					)
				{
					this._UpToOperacaoRendaFixaByIdOperacao = new OperacaoRendaFixa();
					this._UpToOperacaoRendaFixaByIdOperacao.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToOperacaoRendaFixaByIdOperacao", this._UpToOperacaoRendaFixaByIdOperacao);
					this._UpToOperacaoRendaFixaByIdOperacao.Query.Where(this._UpToOperacaoRendaFixaByIdOperacao.Query.IdOperacao == this.IdOperacao);
					this._UpToOperacaoRendaFixaByIdOperacao.Query.Load();
				}

				return this._UpToOperacaoRendaFixaByIdOperacao;
			}
			
			set
			{
				this.RemovePreSave("UpToOperacaoRendaFixaByIdOperacao");
				

				if(value == null)
				{
					this.IdOperacao = null;
					this._UpToOperacaoRendaFixaByIdOperacao = null;
				}
				else
				{
					this.IdOperacao = value.IdOperacao;
					this._UpToOperacaoRendaFixaByIdOperacao = value;
					this.SetPreSave("UpToOperacaoRendaFixaByIdOperacao", this._UpToOperacaoRendaFixaByIdOperacao);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToOperacaoRendaFixaByIdOperacao != null)
			{
				this.IdOperacao = this._UpToOperacaoRendaFixaByIdOperacao.IdOperacao;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esMemoriaCalculoRendaFixaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return MemoriaCalculoRendaFixaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdMemoriaCalculoRendaFixa
		{
			get
			{
				return new esQueryItem(this, MemoriaCalculoRendaFixaMetadata.ColumnNames.IdMemoriaCalculoRendaFixa, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, MemoriaCalculoRendaFixaMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataAtual
		{
			get
			{
				return new esQueryItem(this, MemoriaCalculoRendaFixaMetadata.ColumnNames.DataAtual, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdOperacao
		{
			get
			{
				return new esQueryItem(this, MemoriaCalculoRendaFixaMetadata.ColumnNames.IdOperacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoPreco
		{
			get
			{
				return new esQueryItem(this, MemoriaCalculoRendaFixaMetadata.ColumnNames.TipoPreco, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoProcessamento
		{
			get
			{
				return new esQueryItem(this, MemoriaCalculoRendaFixaMetadata.ColumnNames.TipoProcessamento, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataFluxo
		{
			get
			{
				return new esQueryItem(this, MemoriaCalculoRendaFixaMetadata.ColumnNames.DataFluxo, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem TaxaDesconto
		{
			get
			{
				return new esQueryItem(this, MemoriaCalculoRendaFixaMetadata.ColumnNames.TaxaDesconto, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorVencimento
		{
			get
			{
				return new esQueryItem(this, MemoriaCalculoRendaFixaMetadata.ColumnNames.ValorVencimento, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorPresente
		{
			get
			{
				return new esQueryItem(this, MemoriaCalculoRendaFixaMetadata.ColumnNames.ValorPresente, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorNominalAjustado
		{
			get
			{
				return new esQueryItem(this, MemoriaCalculoRendaFixaMetadata.ColumnNames.ValorNominalAjustado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorNominal
		{
			get
			{
				return new esQueryItem(this, MemoriaCalculoRendaFixaMetadata.ColumnNames.ValorNominal, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TipoEventoTitulo
		{
			get
			{
				return new esQueryItem(this, MemoriaCalculoRendaFixaMetadata.ColumnNames.TipoEventoTitulo, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("MemoriaCalculoRendaFixaCollection")]
	public partial class MemoriaCalculoRendaFixaCollection : esMemoriaCalculoRendaFixaCollection, IEnumerable<MemoriaCalculoRendaFixa>
	{
		public MemoriaCalculoRendaFixaCollection()
		{

		}
		
		public static implicit operator List<MemoriaCalculoRendaFixa>(MemoriaCalculoRendaFixaCollection coll)
		{
			List<MemoriaCalculoRendaFixa> list = new List<MemoriaCalculoRendaFixa>();
			
			foreach (MemoriaCalculoRendaFixa emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  MemoriaCalculoRendaFixaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new MemoriaCalculoRendaFixaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new MemoriaCalculoRendaFixa(row);
		}

		override protected esEntity CreateEntity()
		{
			return new MemoriaCalculoRendaFixa();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public MemoriaCalculoRendaFixaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new MemoriaCalculoRendaFixaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(MemoriaCalculoRendaFixaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public MemoriaCalculoRendaFixa AddNew()
		{
			MemoriaCalculoRendaFixa entity = base.AddNewEntity() as MemoriaCalculoRendaFixa;
			
			return entity;
		}

		public MemoriaCalculoRendaFixa FindByPrimaryKey(System.Int32 idMemoriaCalculoRendaFixa)
		{
			return base.FindByPrimaryKey(idMemoriaCalculoRendaFixa) as MemoriaCalculoRendaFixa;
		}


		#region IEnumerable<MemoriaCalculoRendaFixa> Members

		IEnumerator<MemoriaCalculoRendaFixa> IEnumerable<MemoriaCalculoRendaFixa>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as MemoriaCalculoRendaFixa;
			}
		}

		#endregion
		
		private MemoriaCalculoRendaFixaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'MemoriaCalculoRendaFixa' table
	/// </summary>

	[Serializable]
	public partial class MemoriaCalculoRendaFixa : esMemoriaCalculoRendaFixa
	{
		public MemoriaCalculoRendaFixa()
		{

		}
	
		public MemoriaCalculoRendaFixa(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return MemoriaCalculoRendaFixaMetadata.Meta();
			}
		}
		
		
		
		override protected esMemoriaCalculoRendaFixaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new MemoriaCalculoRendaFixaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public MemoriaCalculoRendaFixaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new MemoriaCalculoRendaFixaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(MemoriaCalculoRendaFixaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private MemoriaCalculoRendaFixaQuery query;
	}



	[Serializable]
	public partial class MemoriaCalculoRendaFixaQuery : esMemoriaCalculoRendaFixaQuery
	{
		public MemoriaCalculoRendaFixaQuery()
		{

		}		
		
		public MemoriaCalculoRendaFixaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class MemoriaCalculoRendaFixaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected MemoriaCalculoRendaFixaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(MemoriaCalculoRendaFixaMetadata.ColumnNames.IdMemoriaCalculoRendaFixa, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = MemoriaCalculoRendaFixaMetadata.PropertyNames.IdMemoriaCalculoRendaFixa;
			c.IsInPrimaryKey = true;
			c.IsAutoIncrement = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(MemoriaCalculoRendaFixaMetadata.ColumnNames.IdCliente, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = MemoriaCalculoRendaFixaMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(MemoriaCalculoRendaFixaMetadata.ColumnNames.DataAtual, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = MemoriaCalculoRendaFixaMetadata.PropertyNames.DataAtual;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(MemoriaCalculoRendaFixaMetadata.ColumnNames.IdOperacao, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = MemoriaCalculoRendaFixaMetadata.PropertyNames.IdOperacao;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(MemoriaCalculoRendaFixaMetadata.ColumnNames.TipoPreco, 4, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = MemoriaCalculoRendaFixaMetadata.PropertyNames.TipoPreco;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(MemoriaCalculoRendaFixaMetadata.ColumnNames.TipoProcessamento, 5, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = MemoriaCalculoRendaFixaMetadata.PropertyNames.TipoProcessamento;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(MemoriaCalculoRendaFixaMetadata.ColumnNames.DataFluxo, 6, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = MemoriaCalculoRendaFixaMetadata.PropertyNames.DataFluxo;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(MemoriaCalculoRendaFixaMetadata.ColumnNames.TaxaDesconto, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = MemoriaCalculoRendaFixaMetadata.PropertyNames.TaxaDesconto;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(MemoriaCalculoRendaFixaMetadata.ColumnNames.ValorVencimento, 8, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = MemoriaCalculoRendaFixaMetadata.PropertyNames.ValorVencimento;	
			c.NumericPrecision = 25;
			c.NumericScale = 8;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(MemoriaCalculoRendaFixaMetadata.ColumnNames.ValorPresente, 9, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = MemoriaCalculoRendaFixaMetadata.PropertyNames.ValorPresente;	
			c.NumericPrecision = 25;
			c.NumericScale = 8;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(MemoriaCalculoRendaFixaMetadata.ColumnNames.ValorNominalAjustado, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = MemoriaCalculoRendaFixaMetadata.PropertyNames.ValorNominalAjustado;	
			c.NumericPrecision = 25;
			c.NumericScale = 8;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(MemoriaCalculoRendaFixaMetadata.ColumnNames.ValorNominal, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = MemoriaCalculoRendaFixaMetadata.PropertyNames.ValorNominal;	
			c.NumericPrecision = 25;
			c.NumericScale = 8;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(MemoriaCalculoRendaFixaMetadata.ColumnNames.TipoEventoTitulo, 12, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = MemoriaCalculoRendaFixaMetadata.PropertyNames.TipoEventoTitulo;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public MemoriaCalculoRendaFixaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdMemoriaCalculoRendaFixa = "IdMemoriaCalculoRendaFixa";
			 public const string IdCliente = "IdCliente";
			 public const string DataAtual = "DataAtual";
			 public const string IdOperacao = "IdOperacao";
			 public const string TipoPreco = "TipoPreco";
			 public const string TipoProcessamento = "TipoProcessamento";
			 public const string DataFluxo = "DataFluxo";
			 public const string TaxaDesconto = "TaxaDesconto";
			 public const string ValorVencimento = "ValorVencimento";
			 public const string ValorPresente = "ValorPresente";
			 public const string ValorNominalAjustado = "ValorNominalAjustado";
			 public const string ValorNominal = "ValorNominal";
			 public const string TipoEventoTitulo = "TipoEventoTitulo";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdMemoriaCalculoRendaFixa = "IdMemoriaCalculoRendaFixa";
			 public const string IdCliente = "IdCliente";
			 public const string DataAtual = "DataAtual";
			 public const string IdOperacao = "IdOperacao";
			 public const string TipoPreco = "TipoPreco";
			 public const string TipoProcessamento = "TipoProcessamento";
			 public const string DataFluxo = "DataFluxo";
			 public const string TaxaDesconto = "TaxaDesconto";
			 public const string ValorVencimento = "ValorVencimento";
			 public const string ValorPresente = "ValorPresente";
			 public const string ValorNominalAjustado = "ValorNominalAjustado";
			 public const string ValorNominal = "ValorNominal";
			 public const string TipoEventoTitulo = "TipoEventoTitulo";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(MemoriaCalculoRendaFixaMetadata))
			{
				if(MemoriaCalculoRendaFixaMetadata.mapDelegates == null)
				{
					MemoriaCalculoRendaFixaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (MemoriaCalculoRendaFixaMetadata.meta == null)
				{
					MemoriaCalculoRendaFixaMetadata.meta = new MemoriaCalculoRendaFixaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdMemoriaCalculoRendaFixa", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataAtual", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdOperacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoPreco", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoProcessamento", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataFluxo", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("TaxaDesconto", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorVencimento", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorPresente", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorNominalAjustado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorNominal", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("TipoEventoTitulo", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "MemoriaCalculoRendaFixa";
				meta.Destination = "MemoriaCalculoRendaFixa";
				
				meta.spInsert = "proc_MemoriaCalculoRendaFixaInsert";				
				meta.spUpdate = "proc_MemoriaCalculoRendaFixaUpdate";		
				meta.spDelete = "proc_MemoriaCalculoRendaFixaDelete";
				meta.spLoadAll = "proc_MemoriaCalculoRendaFixaLoadAll";
				meta.spLoadByPrimaryKey = "proc_MemoriaCalculoRendaFixaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private MemoriaCalculoRendaFixaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
