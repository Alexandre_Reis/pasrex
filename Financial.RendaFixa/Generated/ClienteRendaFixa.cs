/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 04/11/2015 18:56:28
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	
using Financial.Investidor;
using Financial.Common;
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.RendaFixa
{

	[Serializable]
	abstract public class esClienteRendaFixaCollection : esEntityCollection
	{
		public esClienteRendaFixaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "ClienteRendaFixaCollection";
		}

		#region Query Logic
		protected void InitQuery(esClienteRendaFixaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esClienteRendaFixaQuery);
		}
		#endregion
		
		virtual public ClienteRendaFixa DetachEntity(ClienteRendaFixa entity)
		{
			return base.DetachEntity(entity) as ClienteRendaFixa;
		}
		
		virtual public ClienteRendaFixa AttachEntity(ClienteRendaFixa entity)
		{
			return base.AttachEntity(entity) as ClienteRendaFixa;
		}
		
		virtual public void Combine(ClienteRendaFixaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public ClienteRendaFixa this[int index]
		{
			get
			{
				return base[index] as ClienteRendaFixa;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(ClienteRendaFixa);
		}
	}



	[Serializable]
	abstract public class esClienteRendaFixa : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esClienteRendaFixaQuery GetDynamicQuery()
		{
			return null;
		}

		public esClienteRendaFixa()
		{

		}

		public esClienteRendaFixa(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idCliente)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCliente);
			else
				return LoadByPrimaryKeyStoredProcedure(idCliente);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idCliente)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esClienteRendaFixaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdCliente == idCliente);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idCliente)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCliente);
			else
				return LoadByPrimaryKeyStoredProcedure(idCliente);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idCliente)
		{
			esClienteRendaFixaQuery query = this.GetDynamicQuery();
			query.Where(query.IdCliente == idCliente);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idCliente)
		{
			esParameters parms = new esParameters();
			parms.Add("IdCliente",idCliente);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "UsaCustoMedio": this.str.UsaCustoMedio = (string)value; break;							
						case "IsentoIR": this.str.IsentoIR = (string)value; break;							
						case "IsentoIOF": this.str.IsentoIOF = (string)value; break;							
						case "CodigoInterface": this.str.CodigoInterface = (string)value; break;							
						case "IdAssessor": this.str.IdAssessor = (string)value; break;							
						case "CodigoCetip": this.str.CodigoCetip = (string)value; break;
						case "AtivoCetip": this.str.AtivoCetip = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "IdAssessor":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAssessor = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to ClienteRendaFixa.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(ClienteRendaFixaMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				base.SetSystemInt32(ClienteRendaFixaMetadata.ColumnNames.IdCliente, value);
			}
		}
		
		/// <summary>
		/// Maps to ClienteRendaFixa.UsaCustoMedio
		/// </summary>
		virtual public System.String UsaCustoMedio
		{
			get
			{
				return base.GetSystemString(ClienteRendaFixaMetadata.ColumnNames.UsaCustoMedio);
			}
			
			set
			{
				base.SetSystemString(ClienteRendaFixaMetadata.ColumnNames.UsaCustoMedio, value);
			}
		}
		
		/// <summary>
		/// Maps to ClienteRendaFixa.IsentoIR
		/// </summary>
		virtual public System.String IsentoIR
		{
			get
			{
				return base.GetSystemString(ClienteRendaFixaMetadata.ColumnNames.IsentoIR);
			}
			
			set
			{
				base.SetSystemString(ClienteRendaFixaMetadata.ColumnNames.IsentoIR, value);
			}
		}
		
		/// <summary>
		/// Maps to ClienteRendaFixa.IsentoIOF
		/// </summary>
		virtual public System.String IsentoIOF
		{
			get
			{
				return base.GetSystemString(ClienteRendaFixaMetadata.ColumnNames.IsentoIOF);
			}
			
			set
			{
				base.SetSystemString(ClienteRendaFixaMetadata.ColumnNames.IsentoIOF, value);
			}
		}
		
		/// <summary>
		/// Maps to ClienteRendaFixa.CodigoInterface
		/// </summary>
		virtual public System.String CodigoInterface
		{
			get
			{
				return base.GetSystemString(ClienteRendaFixaMetadata.ColumnNames.CodigoInterface);
			}
			
			set
			{
				base.SetSystemString(ClienteRendaFixaMetadata.ColumnNames.CodigoInterface, value);
			}
		}
		
		/// <summary>
		/// Maps to ClienteRendaFixa.IdAssessor
		/// </summary>
		virtual public System.Int32? IdAssessor
		{
			get
			{
				return base.GetSystemInt32(ClienteRendaFixaMetadata.ColumnNames.IdAssessor);
			}
			
			set
			{
				if(base.SetSystemInt32(ClienteRendaFixaMetadata.ColumnNames.IdAssessor, value))
				{
					this._UpToAssessorByIdAssessor = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to ClienteRendaFixa.CodigoCetip
		/// </summary>
		virtual public System.String CodigoCetip
		{
			get
			{
				return base.GetSystemString(ClienteRendaFixaMetadata.ColumnNames.CodigoCetip);
			}
			
			set
			{
				base.SetSystemString(ClienteRendaFixaMetadata.ColumnNames.CodigoCetip, value);
			}
		}
		
		/// <summary>
		/// Maps to ClienteRendaFixa.AtivoCetip
		/// </summary>
		virtual public System.String AtivoCetip
		{
			get
			{
				return base.GetSystemString(ClienteRendaFixaMetadata.ColumnNames.AtivoCetip);
			}
			
			set
			{
				base.SetSystemString(ClienteRendaFixaMetadata.ColumnNames.AtivoCetip, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Assessor _UpToAssessorByIdAssessor;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esClienteRendaFixa entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String UsaCustoMedio
			{
				get
				{
					System.String data = entity.UsaCustoMedio;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.UsaCustoMedio = null;
					else entity.UsaCustoMedio = Convert.ToString(value);
				}
			}
				
			public System.String IsentoIR
			{
				get
				{
					System.String data = entity.IsentoIR;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IsentoIR = null;
					else entity.IsentoIR = Convert.ToString(value);
				}
			}
				
			public System.String IsentoIOF
			{
				get
				{
					System.String data = entity.IsentoIOF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IsentoIOF = null;
					else entity.IsentoIOF = Convert.ToString(value);
				}
			}
				
			public System.String CodigoInterface
			{
				get
				{
					System.String data = entity.CodigoInterface;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoInterface = null;
					else entity.CodigoInterface = Convert.ToString(value);
				}
			}
				
			public System.String IdAssessor
			{
				get
				{
					System.Int32? data = entity.IdAssessor;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAssessor = null;
					else entity.IdAssessor = Convert.ToInt32(value);
				}
			}
				
			public System.String CodigoCetip
			{
				get
				{
					System.String data = entity.CodigoCetip;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoCetip = null;
					else entity.CodigoCetip = Convert.ToString(value);
				}
			}
			
			public System.String AtivoCetip
			{
				get
				{
					System.String data = entity.AtivoCetip;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AtivoCetip = null;
					else entity.AtivoCetip = Convert.ToString(value);
				}
			}
			

			private esClienteRendaFixa entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esClienteRendaFixaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esClienteRendaFixa can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class ClienteRendaFixa : esClienteRendaFixa
	{

				
		#region UpToAssessorByIdAssessor - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Assessor_ClienteRendaFixa_FK1
		/// </summary>

		[XmlIgnore]
		public Assessor UpToAssessorByIdAssessor
		{
			get
			{
				if(this._UpToAssessorByIdAssessor == null
					&& IdAssessor != null					)
				{
					this._UpToAssessorByIdAssessor = new Assessor();
					this._UpToAssessorByIdAssessor.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToAssessorByIdAssessor", this._UpToAssessorByIdAssessor);
					this._UpToAssessorByIdAssessor.Query.Where(this._UpToAssessorByIdAssessor.Query.IdAssessor == this.IdAssessor);
					this._UpToAssessorByIdAssessor.Query.Load();
				}

				return this._UpToAssessorByIdAssessor;
			}
			
			set
			{
				this.RemovePreSave("UpToAssessorByIdAssessor");
				

				if(value == null)
				{
					this.IdAssessor = null;
					this._UpToAssessorByIdAssessor = null;
				}
				else
				{
					this.IdAssessor = value.IdAssessor;
					this._UpToAssessorByIdAssessor = value;
					this.SetPreSave("UpToAssessorByIdAssessor", this._UpToAssessorByIdAssessor);
				}
				
			}
		}
		#endregion
		

		#region UpToCliente - One To One
		/// <summary>
		/// One to One
		/// Foreign Key Name - Cliente_ClienteRendaFixa_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToCliente
		{
			get
			{
				if(this._UpToCliente == null
					&& IdCliente != null					)
				{
					this._UpToCliente = new Cliente();
					this._UpToCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToCliente", this._UpToCliente);
					this._UpToCliente.Query.Where(this._UpToCliente.Query.IdCliente == this.IdCliente);
					this._UpToCliente.Query.Load();
				}

				return this._UpToCliente;
			}
			
			set 
			{ 
				this.RemovePreSave("UpToCliente");

				if(value == null)
				{
					this._UpToCliente = null;
				}
				else
				{
					this._UpToCliente = value;
					this.SetPreSave("UpToCliente", this._UpToCliente);
				}
				
				
			} 
		}

		private Cliente _UpToCliente;
		#endregion

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToAssessorByIdAssessor != null)
			{
				this.IdAssessor = this._UpToAssessorByIdAssessor.IdAssessor;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esClienteRendaFixaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return ClienteRendaFixaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, ClienteRendaFixaMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem UsaCustoMedio
		{
			get
			{
				return new esQueryItem(this, ClienteRendaFixaMetadata.ColumnNames.UsaCustoMedio, esSystemType.String);
			}
		} 
		
		public esQueryItem IsentoIR
		{
			get
			{
				return new esQueryItem(this, ClienteRendaFixaMetadata.ColumnNames.IsentoIR, esSystemType.String);
			}
		} 
		
		public esQueryItem IsentoIOF
		{
			get
			{
				return new esQueryItem(this, ClienteRendaFixaMetadata.ColumnNames.IsentoIOF, esSystemType.String);
			}
		} 
		
		public esQueryItem CodigoInterface
		{
			get
			{
				return new esQueryItem(this, ClienteRendaFixaMetadata.ColumnNames.CodigoInterface, esSystemType.String);
			}
		} 
		
		public esQueryItem IdAssessor
		{
			get
			{
				return new esQueryItem(this, ClienteRendaFixaMetadata.ColumnNames.IdAssessor, esSystemType.Int32);
			}
		} 
		
		public esQueryItem CodigoCetip
		{
			get
			{
				return new esQueryItem(this, ClienteRendaFixaMetadata.ColumnNames.CodigoCetip, esSystemType.String);
			}
		} 
		
		public esQueryItem AtivoCetip
		{
			get
			{
				return new esQueryItem(this, ClienteRendaFixaMetadata.ColumnNames.AtivoCetip, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("ClienteRendaFixaCollection")]
	public partial class ClienteRendaFixaCollection : esClienteRendaFixaCollection, IEnumerable<ClienteRendaFixa>
	{
		public ClienteRendaFixaCollection()
		{

		}
		
		public static implicit operator List<ClienteRendaFixa>(ClienteRendaFixaCollection coll)
		{
			List<ClienteRendaFixa> list = new List<ClienteRendaFixa>();
			
			foreach (ClienteRendaFixa emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  ClienteRendaFixaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ClienteRendaFixaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new ClienteRendaFixa(row);
		}

		override protected esEntity CreateEntity()
		{
			return new ClienteRendaFixa();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public ClienteRendaFixaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ClienteRendaFixaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(ClienteRendaFixaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public ClienteRendaFixa AddNew()
		{
			ClienteRendaFixa entity = base.AddNewEntity() as ClienteRendaFixa;
			
			return entity;
		}

		public ClienteRendaFixa FindByPrimaryKey(System.Int32 idCliente)
		{
			return base.FindByPrimaryKey(idCliente) as ClienteRendaFixa;
		}


		#region IEnumerable<ClienteRendaFixa> Members

		IEnumerator<ClienteRendaFixa> IEnumerable<ClienteRendaFixa>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as ClienteRendaFixa;
			}
		}

		#endregion
		
		private ClienteRendaFixaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'ClienteRendaFixa' table
	/// </summary>

	[Serializable]
	public partial class ClienteRendaFixa : esClienteRendaFixa
	{
		public ClienteRendaFixa()
		{

		}
	
		public ClienteRendaFixa(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return ClienteRendaFixaMetadata.Meta();
			}
		}
		
		
		
		override protected esClienteRendaFixaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new ClienteRendaFixaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public ClienteRendaFixaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new ClienteRendaFixaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(ClienteRendaFixaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private ClienteRendaFixaQuery query;
	}



	[Serializable]
	public partial class ClienteRendaFixaQuery : esClienteRendaFixaQuery
	{
		public ClienteRendaFixaQuery()
		{

		}		
		
		public ClienteRendaFixaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class ClienteRendaFixaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected ClienteRendaFixaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(ClienteRendaFixaMetadata.ColumnNames.IdCliente, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ClienteRendaFixaMetadata.PropertyNames.IdCliente;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteRendaFixaMetadata.ColumnNames.UsaCustoMedio, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = ClienteRendaFixaMetadata.PropertyNames.UsaCustoMedio;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteRendaFixaMetadata.ColumnNames.IsentoIR, 2, typeof(System.String), esSystemType.String);
			c.PropertyName = ClienteRendaFixaMetadata.PropertyNames.IsentoIR;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteRendaFixaMetadata.ColumnNames.IsentoIOF, 3, typeof(System.String), esSystemType.String);
			c.PropertyName = ClienteRendaFixaMetadata.PropertyNames.IsentoIOF;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteRendaFixaMetadata.ColumnNames.CodigoInterface, 4, typeof(System.String), esSystemType.String);
			c.PropertyName = ClienteRendaFixaMetadata.PropertyNames.CodigoInterface;
			c.CharacterMaxLength = 12;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteRendaFixaMetadata.ColumnNames.IdAssessor, 5, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = ClienteRendaFixaMetadata.PropertyNames.IdAssessor;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteRendaFixaMetadata.ColumnNames.CodigoCetip, 6, typeof(System.String), esSystemType.String);
			c.PropertyName = ClienteRendaFixaMetadata.PropertyNames.CodigoCetip;
			c.CharacterMaxLength = 9;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(ClienteRendaFixaMetadata.ColumnNames.AtivoCetip, 7, typeof(System.String), esSystemType.String);
			c.PropertyName = ClienteRendaFixaMetadata.PropertyNames.AtivoCetip;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('S')";
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public ClienteRendaFixaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdCliente = "IdCliente";
			 public const string UsaCustoMedio = "UsaCustoMedio";
			 public const string IsentoIR = "IsentoIR";
			 public const string IsentoIOF = "IsentoIOF";
			 public const string CodigoInterface = "CodigoInterface";
			 public const string IdAssessor = "IdAssessor";
			 public const string CodigoCetip = "CodigoCetip";
			 public const string AtivoCetip = "AtivoCetip";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdCliente = "IdCliente";
			 public const string UsaCustoMedio = "UsaCustoMedio";
			 public const string IsentoIR = "IsentoIR";
			 public const string IsentoIOF = "IsentoIOF";
			 public const string CodigoInterface = "CodigoInterface";
			 public const string IdAssessor = "IdAssessor";
			 public const string CodigoCetip = "CodigoCetip";
			 public const string AtivoCetip = "AtivoCetip";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(ClienteRendaFixaMetadata))
			{
				if(ClienteRendaFixaMetadata.mapDelegates == null)
				{
					ClienteRendaFixaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (ClienteRendaFixaMetadata.meta == null)
				{
					ClienteRendaFixaMetadata.meta = new ClienteRendaFixaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("UsaCustoMedio", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("IsentoIR", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("IsentoIOF", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("CodigoInterface", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("IdAssessor", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("CodigoCetip", new esTypeMap("varchar", "System.String"));			
				meta.AddTypeMap("AtivoCetip", new esTypeMap("char", "System.String"));			
				
				
				
				meta.Source = "ClienteRendaFixa";
				meta.Destination = "ClienteRendaFixa";
				
				meta.spInsert = "proc_ClienteRendaFixaInsert";				
				meta.spUpdate = "proc_ClienteRendaFixaUpdate";		
				meta.spDelete = "proc_ClienteRendaFixaDelete";
				meta.spLoadAll = "proc_ClienteRendaFixaLoadAll";
				meta.spLoadByPrimaryKey = "proc_ClienteRendaFixaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private ClienteRendaFixaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
