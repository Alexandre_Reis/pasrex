/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 12/12/2014 10:53:29 AM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.RendaFixa
{

	[Serializable]
	abstract public class esCotacaoMercadoDebentureCollection : esEntityCollection
	{
		public esCotacaoMercadoDebentureCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "CotacaoMercadoDebentureCollection";
		}

		#region Query Logic
		protected void InitQuery(esCotacaoMercadoDebentureQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esCotacaoMercadoDebentureQuery);
		}
		#endregion
		
		virtual public CotacaoMercadoDebenture DetachEntity(CotacaoMercadoDebenture entity)
		{
			return base.DetachEntity(entity) as CotacaoMercadoDebenture;
		}
		
		virtual public CotacaoMercadoDebenture AttachEntity(CotacaoMercadoDebenture entity)
		{
			return base.AttachEntity(entity) as CotacaoMercadoDebenture;
		}
		
		virtual public void Combine(CotacaoMercadoDebentureCollection collection)
		{
			base.Combine(collection);
		}
		
		new public CotacaoMercadoDebenture this[int index]
		{
			get
			{
				return base[index] as CotacaoMercadoDebenture;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(CotacaoMercadoDebenture);
		}
	}



	[Serializable]
	abstract public class esCotacaoMercadoDebenture : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esCotacaoMercadoDebentureQuery GetDynamicQuery()
		{
			return null;
		}

		public esCotacaoMercadoDebenture()
		{

		}

		public esCotacaoMercadoDebenture(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime dataReferencia, System.String codigoPapel, System.DateTime dataVencimento)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataReferencia, codigoPapel, dataVencimento);
			else
				return LoadByPrimaryKeyStoredProcedure(dataReferencia, codigoPapel, dataVencimento);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.DateTime dataReferencia, System.String codigoPapel, System.DateTime dataVencimento)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esCotacaoMercadoDebentureQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.DataReferencia == dataReferencia, query.CodigoPapel == codigoPapel, query.DataVencimento == dataVencimento);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime dataReferencia, System.String codigoPapel, System.DateTime dataVencimento)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataReferencia, codigoPapel, dataVencimento);
			else
				return LoadByPrimaryKeyStoredProcedure(dataReferencia, codigoPapel, dataVencimento);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime dataReferencia, System.String codigoPapel, System.DateTime dataVencimento)
		{
			esCotacaoMercadoDebentureQuery query = this.GetDynamicQuery();
			query.Where(query.DataReferencia == dataReferencia, query.CodigoPapel == codigoPapel, query.DataVencimento == dataVencimento);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime dataReferencia, System.String codigoPapel, System.DateTime dataVencimento)
		{
			esParameters parms = new esParameters();
			parms.Add("DataReferencia",dataReferencia);			parms.Add("CodigoPapel",codigoPapel);			parms.Add("DataVencimento",dataVencimento);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "DataReferencia": this.str.DataReferencia = (string)value; break;							
						case "CodigoPapel": this.str.CodigoPapel = (string)value; break;							
						case "DataVencimento": this.str.DataVencimento = (string)value; break;							
						case "TaxaIndicativa": this.str.TaxaIndicativa = (string)value; break;							
						case "Pu": this.str.Pu = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "DataReferencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataReferencia = (System.DateTime?)value;
							break;
						
						case "DataVencimento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataVencimento = (System.DateTime?)value;
							break;
						
						case "TaxaIndicativa":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TaxaIndicativa = (System.Decimal?)value;
							break;
						
						case "Pu":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Pu = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to CotacaoMercadoDebenture.DataReferencia
		/// </summary>
		virtual public System.DateTime? DataReferencia
		{
			get
			{
				return base.GetSystemDateTime(CotacaoMercadoDebentureMetadata.ColumnNames.DataReferencia);
			}
			
			set
			{
				base.SetSystemDateTime(CotacaoMercadoDebentureMetadata.ColumnNames.DataReferencia, value);
			}
		}
		
		/// <summary>
		/// Maps to CotacaoMercadoDebenture.CodigoPapel
		/// </summary>
		virtual public System.String CodigoPapel
		{
			get
			{
				return base.GetSystemString(CotacaoMercadoDebentureMetadata.ColumnNames.CodigoPapel);
			}
			
			set
			{
				base.SetSystemString(CotacaoMercadoDebentureMetadata.ColumnNames.CodigoPapel, value);
			}
		}
		
		/// <summary>
		/// Maps to CotacaoMercadoDebenture.DataVencimento
		/// </summary>
		virtual public System.DateTime? DataVencimento
		{
			get
			{
				return base.GetSystemDateTime(CotacaoMercadoDebentureMetadata.ColumnNames.DataVencimento);
			}
			
			set
			{
				base.SetSystemDateTime(CotacaoMercadoDebentureMetadata.ColumnNames.DataVencimento, value);
			}
		}
		
		/// <summary>
		/// Maps to CotacaoMercadoDebenture.TaxaIndicativa
		/// </summary>
		virtual public System.Decimal? TaxaIndicativa
		{
			get
			{
				return base.GetSystemDecimal(CotacaoMercadoDebentureMetadata.ColumnNames.TaxaIndicativa);
			}
			
			set
			{
				base.SetSystemDecimal(CotacaoMercadoDebentureMetadata.ColumnNames.TaxaIndicativa, value);
			}
		}
		
		/// <summary>
		/// Maps to CotacaoMercadoDebenture.PU
		/// </summary>
		virtual public System.Decimal? Pu
		{
			get
			{
				return base.GetSystemDecimal(CotacaoMercadoDebentureMetadata.ColumnNames.Pu);
			}
			
			set
			{
				base.SetSystemDecimal(CotacaoMercadoDebentureMetadata.ColumnNames.Pu, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esCotacaoMercadoDebenture entity)
			{
				this.entity = entity;
			}
			
	
			public System.String DataReferencia
			{
				get
				{
					System.DateTime? data = entity.DataReferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataReferencia = null;
					else entity.DataReferencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String CodigoPapel
			{
				get
				{
					System.String data = entity.CodigoPapel;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoPapel = null;
					else entity.CodigoPapel = Convert.ToString(value);
				}
			}
				
			public System.String DataVencimento
			{
				get
				{
					System.DateTime? data = entity.DataVencimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataVencimento = null;
					else entity.DataVencimento = Convert.ToDateTime(value);
				}
			}
				
			public System.String TaxaIndicativa
			{
				get
				{
					System.Decimal? data = entity.TaxaIndicativa;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TaxaIndicativa = null;
					else entity.TaxaIndicativa = Convert.ToDecimal(value);
				}
			}
				
			public System.String Pu
			{
				get
				{
					System.Decimal? data = entity.Pu;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Pu = null;
					else entity.Pu = Convert.ToDecimal(value);
				}
			}
			

			private esCotacaoMercadoDebenture entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esCotacaoMercadoDebentureQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esCotacaoMercadoDebenture can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class CotacaoMercadoDebenture : esCotacaoMercadoDebenture
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esCotacaoMercadoDebentureQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return CotacaoMercadoDebentureMetadata.Meta();
			}
		}	
		

		public esQueryItem DataReferencia
		{
			get
			{
				return new esQueryItem(this, CotacaoMercadoDebentureMetadata.ColumnNames.DataReferencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem CodigoPapel
		{
			get
			{
				return new esQueryItem(this, CotacaoMercadoDebentureMetadata.ColumnNames.CodigoPapel, esSystemType.String);
			}
		} 
		
		public esQueryItem DataVencimento
		{
			get
			{
				return new esQueryItem(this, CotacaoMercadoDebentureMetadata.ColumnNames.DataVencimento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem TaxaIndicativa
		{
			get
			{
				return new esQueryItem(this, CotacaoMercadoDebentureMetadata.ColumnNames.TaxaIndicativa, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Pu
		{
			get
			{
				return new esQueryItem(this, CotacaoMercadoDebentureMetadata.ColumnNames.Pu, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("CotacaoMercadoDebentureCollection")]
	public partial class CotacaoMercadoDebentureCollection : esCotacaoMercadoDebentureCollection, IEnumerable<CotacaoMercadoDebenture>
	{
		public CotacaoMercadoDebentureCollection()
		{

		}
		
		public static implicit operator List<CotacaoMercadoDebenture>(CotacaoMercadoDebentureCollection coll)
		{
			List<CotacaoMercadoDebenture> list = new List<CotacaoMercadoDebenture>();
			
			foreach (CotacaoMercadoDebenture emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  CotacaoMercadoDebentureMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CotacaoMercadoDebentureQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new CotacaoMercadoDebenture(row);
		}

		override protected esEntity CreateEntity()
		{
			return new CotacaoMercadoDebenture();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public CotacaoMercadoDebentureQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CotacaoMercadoDebentureQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(CotacaoMercadoDebentureQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public CotacaoMercadoDebenture AddNew()
		{
			CotacaoMercadoDebenture entity = base.AddNewEntity() as CotacaoMercadoDebenture;
			
			return entity;
		}

		public CotacaoMercadoDebenture FindByPrimaryKey(System.DateTime dataReferencia, System.String codigoPapel, System.DateTime dataVencimento)
		{
			return base.FindByPrimaryKey(dataReferencia, codigoPapel, dataVencimento) as CotacaoMercadoDebenture;
		}


		#region IEnumerable<CotacaoMercadoDebenture> Members

		IEnumerator<CotacaoMercadoDebenture> IEnumerable<CotacaoMercadoDebenture>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as CotacaoMercadoDebenture;
			}
		}

		#endregion
		
		private CotacaoMercadoDebentureQuery query;
	}


	/// <summary>
	/// Encapsulates the 'CotacaoMercadoDebenture' table
	/// </summary>

	[Serializable]
	public partial class CotacaoMercadoDebenture : esCotacaoMercadoDebenture
	{
		public CotacaoMercadoDebenture()
		{

		}
	
		public CotacaoMercadoDebenture(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return CotacaoMercadoDebentureMetadata.Meta();
			}
		}
		
		
		
		override protected esCotacaoMercadoDebentureQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CotacaoMercadoDebentureQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public CotacaoMercadoDebentureQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CotacaoMercadoDebentureQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(CotacaoMercadoDebentureQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private CotacaoMercadoDebentureQuery query;
	}



	[Serializable]
	public partial class CotacaoMercadoDebentureQuery : esCotacaoMercadoDebentureQuery
	{
		public CotacaoMercadoDebentureQuery()
		{

		}		
		
		public CotacaoMercadoDebentureQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class CotacaoMercadoDebentureMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected CotacaoMercadoDebentureMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(CotacaoMercadoDebentureMetadata.ColumnNames.DataReferencia, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = CotacaoMercadoDebentureMetadata.PropertyNames.DataReferencia;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotacaoMercadoDebentureMetadata.ColumnNames.CodigoPapel, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = CotacaoMercadoDebentureMetadata.PropertyNames.CodigoPapel;
			c.IsInPrimaryKey = true;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotacaoMercadoDebentureMetadata.ColumnNames.DataVencimento, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = CotacaoMercadoDebentureMetadata.PropertyNames.DataVencimento;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotacaoMercadoDebentureMetadata.ColumnNames.TaxaIndicativa, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CotacaoMercadoDebentureMetadata.PropertyNames.TaxaIndicativa;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotacaoMercadoDebentureMetadata.ColumnNames.Pu, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CotacaoMercadoDebentureMetadata.PropertyNames.Pu;	
			c.NumericPrecision = 28;
			c.NumericScale = 16;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public CotacaoMercadoDebentureMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string DataReferencia = "DataReferencia";
			 public const string CodigoPapel = "CodigoPapel";
			 public const string DataVencimento = "DataVencimento";
			 public const string TaxaIndicativa = "TaxaIndicativa";
			 public const string Pu = "PU";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string DataReferencia = "DataReferencia";
			 public const string CodigoPapel = "CodigoPapel";
			 public const string DataVencimento = "DataVencimento";
			 public const string TaxaIndicativa = "TaxaIndicativa";
			 public const string Pu = "Pu";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(CotacaoMercadoDebentureMetadata))
			{
				if(CotacaoMercadoDebentureMetadata.mapDelegates == null)
				{
					CotacaoMercadoDebentureMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (CotacaoMercadoDebentureMetadata.meta == null)
				{
					CotacaoMercadoDebentureMetadata.meta = new CotacaoMercadoDebentureMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("DataReferencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("CodigoPapel", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DataVencimento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("TaxaIndicativa", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PU", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "CotacaoMercadoDebenture";
				meta.Destination = "CotacaoMercadoDebenture";
				
				meta.spInsert = "proc_CotacaoMercadoDebentureInsert";				
				meta.spUpdate = "proc_CotacaoMercadoDebentureUpdate";		
				meta.spDelete = "proc_CotacaoMercadoDebentureDelete";
				meta.spLoadAll = "proc_CotacaoMercadoDebentureLoadAll";
				meta.spLoadByPrimaryKey = "proc_CotacaoMercadoDebentureLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private CotacaoMercadoDebentureMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
