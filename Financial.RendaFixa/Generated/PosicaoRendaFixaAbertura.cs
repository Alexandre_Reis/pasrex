/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 19/08/2015 18:57:31
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.Investidor;
using Financial.Common;



namespace Financial.RendaFixa
{

	[Serializable]
	abstract public class esPosicaoRendaFixaAberturaCollection : esEntityCollection
	{
		public esPosicaoRendaFixaAberturaCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "PosicaoRendaFixaAberturaCollection";
		}

		#region Query Logic
		protected void InitQuery(esPosicaoRendaFixaAberturaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esPosicaoRendaFixaAberturaQuery);
		}
		#endregion
		
		virtual public PosicaoRendaFixaAbertura DetachEntity(PosicaoRendaFixaAbertura entity)
		{
			return base.DetachEntity(entity) as PosicaoRendaFixaAbertura;
		}
		
		virtual public PosicaoRendaFixaAbertura AttachEntity(PosicaoRendaFixaAbertura entity)
		{
			return base.AttachEntity(entity) as PosicaoRendaFixaAbertura;
		}
		
		virtual public void Combine(PosicaoRendaFixaAberturaCollection collection)
		{
			base.Combine(collection);
		}
		
		new public PosicaoRendaFixaAbertura this[int index]
		{
			get
			{
				return base[index] as PosicaoRendaFixaAbertura;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(PosicaoRendaFixaAbertura);
		}
	}



	[Serializable]
	abstract public class esPosicaoRendaFixaAbertura : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esPosicaoRendaFixaAberturaQuery GetDynamicQuery()
		{
			return null;
		}

		public esPosicaoRendaFixaAbertura()
		{

		}

		public esPosicaoRendaFixaAbertura(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idPosicao, System.DateTime dataHistorico)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPosicao, dataHistorico);
			else
				return LoadByPrimaryKeyStoredProcedure(idPosicao, dataHistorico);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idPosicao, System.DateTime dataHistorico)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esPosicaoRendaFixaAberturaQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdPosicao == idPosicao, query.DataHistorico == dataHistorico);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idPosicao, System.DateTime dataHistorico)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idPosicao, dataHistorico);
			else
				return LoadByPrimaryKeyStoredProcedure(idPosicao, dataHistorico);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idPosicao, System.DateTime dataHistorico)
		{
			esPosicaoRendaFixaAberturaQuery query = this.GetDynamicQuery();
			query.Where(query.IdPosicao == idPosicao, query.DataHistorico == dataHistorico);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idPosicao, System.DateTime dataHistorico)
		{
			esParameters parms = new esParameters();
			parms.Add("IdPosicao",idPosicao);			parms.Add("DataHistorico",dataHistorico);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdPosicao": this.str.IdPosicao = (string)value; break;							
						case "DataHistorico": this.str.DataHistorico = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "IdTitulo": this.str.IdTitulo = (string)value; break;							
						case "TipoOperacao": this.str.TipoOperacao = (string)value; break;							
						case "DataVencimento": this.str.DataVencimento = (string)value; break;							
						case "Quantidade": this.str.Quantidade = (string)value; break;							
						case "QuantidadeBloqueada": this.str.QuantidadeBloqueada = (string)value; break;							
						case "DataOperacao": this.str.DataOperacao = (string)value; break;							
						case "DataLiquidacao": this.str.DataLiquidacao = (string)value; break;							
						case "PUOperacao": this.str.PUOperacao = (string)value; break;							
						case "PUCurva": this.str.PUCurva = (string)value; break;							
						case "ValorCurva": this.str.ValorCurva = (string)value; break;							
						case "PUMercado": this.str.PUMercado = (string)value; break;							
						case "ValorMercado": this.str.ValorMercado = (string)value; break;							
						case "PUJuros": this.str.PUJuros = (string)value; break;							
						case "ValorJuros": this.str.ValorJuros = (string)value; break;							
						case "DataVolta": this.str.DataVolta = (string)value; break;							
						case "TaxaVolta": this.str.TaxaVolta = (string)value; break;							
						case "PUVolta": this.str.PUVolta = (string)value; break;							
						case "ValorVolta": this.str.ValorVolta = (string)value; break;							
						case "QuantidadeInicial": this.str.QuantidadeInicial = (string)value; break;							
						case "ValorIR": this.str.ValorIR = (string)value; break;							
						case "ValorIOF": this.str.ValorIOF = (string)value; break;							
						case "TipoNegociacao": this.str.TipoNegociacao = (string)value; break;							
						case "PUCorrecao": this.str.PUCorrecao = (string)value; break;							
						case "ValorCorrecao": this.str.ValorCorrecao = (string)value; break;							
						case "TaxaOperacao": this.str.TaxaOperacao = (string)value; break;							
						case "IdAgente": this.str.IdAgente = (string)value; break;							
						case "IdCustodia": this.str.IdCustodia = (string)value; break;							
						case "CustoCustodia": this.str.CustoCustodia = (string)value; break;							
						case "IdIndiceVolta": this.str.IdIndiceVolta = (string)value; break;							
						case "IdOperacao": this.str.IdOperacao = (string)value; break;							
						case "OperacaoTermo": this.str.OperacaoTermo = (string)value; break;							
						case "ValorCurvaVencimento": this.str.ValorCurvaVencimento = (string)value; break;							
						case "PUCurvaVencimento": this.str.PUCurvaVencimento = (string)value; break;							
						case "AjusteMTM": this.str.AjusteMTM = (string)value; break;							
						case "AjusteVencimento": this.str.AjusteVencimento = (string)value; break;							
						case "TaxaMTM": this.str.TaxaMTM = (string)value; break;							
						case "IdCorretora": this.str.IdCorretora = (string)value; break;							
						case "ValorBrutoGrossUp": this.str.ValorBrutoGrossUp = (string)value; break;							
						case "AliquotaIR": this.str.AliquotaIR = (string)value; break;							
						case "AliquotaIOF": this.str.AliquotaIOF = (string)value; break;							
						case "PrazoDecorridoDC": this.str.PrazoDecorridoDC = (string)value; break;							
						case "PrazoDecorridoDU": this.str.PrazoDecorridoDU = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdPosicao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPosicao = (System.Int32?)value;
							break;
						
						case "DataHistorico":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataHistorico = (System.DateTime?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "IdTitulo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTitulo = (System.Int32?)value;
							break;
						
						case "TipoOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoOperacao = (System.Byte?)value;
							break;
						
						case "DataVencimento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataVencimento = (System.DateTime?)value;
							break;
						
						case "Quantidade":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Quantidade = (System.Decimal?)value;
							break;
						
						case "QuantidadeBloqueada":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QuantidadeBloqueada = (System.Decimal?)value;
							break;
						
						case "DataOperacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataOperacao = (System.DateTime?)value;
							break;
						
						case "DataLiquidacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataLiquidacao = (System.DateTime?)value;
							break;
						
						case "PUOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PUOperacao = (System.Decimal?)value;
							break;
						
						case "PUCurva":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PUCurva = (System.Decimal?)value;
							break;
						
						case "ValorCurva":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorCurva = (System.Decimal?)value;
							break;
						
						case "PUMercado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PUMercado = (System.Decimal?)value;
							break;
						
						case "ValorMercado":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorMercado = (System.Decimal?)value;
							break;
						
						case "PUJuros":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PUJuros = (System.Decimal?)value;
							break;
						
						case "ValorJuros":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorJuros = (System.Decimal?)value;
							break;
						
						case "DataVolta":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataVolta = (System.DateTime?)value;
							break;
						
						case "TaxaVolta":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TaxaVolta = (System.Decimal?)value;
							break;
						
						case "PUVolta":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PUVolta = (System.Decimal?)value;
							break;
						
						case "ValorVolta":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorVolta = (System.Decimal?)value;
							break;
						
						case "QuantidadeInicial":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QuantidadeInicial = (System.Decimal?)value;
							break;
						
						case "ValorIR":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIR = (System.Decimal?)value;
							break;
						
						case "ValorIOF":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorIOF = (System.Decimal?)value;
							break;
						
						case "TipoNegociacao":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.TipoNegociacao = (System.Byte?)value;
							break;
						
						case "PUCorrecao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PUCorrecao = (System.Decimal?)value;
							break;
						
						case "ValorCorrecao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorCorrecao = (System.Decimal?)value;
							break;
						
						case "TaxaOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TaxaOperacao = (System.Decimal?)value;
							break;
						
						case "IdAgente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdAgente = (System.Int32?)value;
							break;
						
						case "IdCustodia":
						
							if (value == null || value.GetType().ToString() == "System.Byte")
								this.IdCustodia = (System.Byte?)value;
							break;
						
						case "CustoCustodia":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.CustoCustodia = (System.Decimal?)value;
							break;
						
						case "IdIndiceVolta":
						
							if (value == null || value.GetType().ToString() == "System.Int16")
								this.IdIndiceVolta = (System.Int16?)value;
							break;
						
						case "IdOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacao = (System.Int32?)value;
							break;
						
						case "ValorCurvaVencimento":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorCurvaVencimento = (System.Decimal?)value;
							break;
						
						case "PUCurvaVencimento":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PUCurvaVencimento = (System.Decimal?)value;
							break;
						
						case "AjusteMTM":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.AjusteMTM = (System.Decimal?)value;
							break;
						
						case "AjusteVencimento":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.AjusteVencimento = (System.Decimal?)value;
							break;
						
						case "TaxaMTM":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.TaxaMTM = (System.Decimal?)value;
							break;
						
						case "IdCorretora":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCorretora = (System.Int32?)value;
							break;
						
						case "ValorBrutoGrossUp":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.ValorBrutoGrossUp = (System.Decimal?)value;
							break;
						
						case "AliquotaIR":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.AliquotaIR = (System.Decimal?)value;
							break;
						
						case "AliquotaIOF":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.AliquotaIOF = (System.Decimal?)value;
							break;
						
						case "PrazoDecorridoDC":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.PrazoDecorridoDC = (System.Int32?)value;
							break;
						
						case "PrazoDecorridoDU":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.PrazoDecorridoDU = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to PosicaoRendaFixaAbertura.IdPosicao
		/// </summary>
		virtual public System.Int32? IdPosicao
		{
			get
			{
				return base.GetSystemInt32(PosicaoRendaFixaAberturaMetadata.ColumnNames.IdPosicao);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoRendaFixaAberturaMetadata.ColumnNames.IdPosicao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixaAbertura.DataHistorico
		/// </summary>
		virtual public System.DateTime? DataHistorico
		{
			get
			{
				return base.GetSystemDateTime(PosicaoRendaFixaAberturaMetadata.ColumnNames.DataHistorico);
			}
			
			set
			{
				base.SetSystemDateTime(PosicaoRendaFixaAberturaMetadata.ColumnNames.DataHistorico, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixaAbertura.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(PosicaoRendaFixaAberturaMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(PosicaoRendaFixaAberturaMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixaAbertura.IdTitulo
		/// </summary>
		virtual public System.Int32? IdTitulo
		{
			get
			{
				return base.GetSystemInt32(PosicaoRendaFixaAberturaMetadata.ColumnNames.IdTitulo);
			}
			
			set
			{
				if(base.SetSystemInt32(PosicaoRendaFixaAberturaMetadata.ColumnNames.IdTitulo, value))
				{
					this._UpToTituloRendaFixaByIdTitulo = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixaAbertura.TipoOperacao
		/// </summary>
		virtual public System.Byte? TipoOperacao
		{
			get
			{
				return base.GetSystemByte(PosicaoRendaFixaAberturaMetadata.ColumnNames.TipoOperacao);
			}
			
			set
			{
				base.SetSystemByte(PosicaoRendaFixaAberturaMetadata.ColumnNames.TipoOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixaAbertura.DataVencimento
		/// </summary>
		virtual public System.DateTime? DataVencimento
		{
			get
			{
				return base.GetSystemDateTime(PosicaoRendaFixaAberturaMetadata.ColumnNames.DataVencimento);
			}
			
			set
			{
				base.SetSystemDateTime(PosicaoRendaFixaAberturaMetadata.ColumnNames.DataVencimento, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixaAbertura.Quantidade
		/// </summary>
		virtual public System.Decimal? Quantidade
		{
			get
			{
				return base.GetSystemDecimal(PosicaoRendaFixaAberturaMetadata.ColumnNames.Quantidade);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoRendaFixaAberturaMetadata.ColumnNames.Quantidade, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixaAbertura.QuantidadeBloqueada
		/// </summary>
		virtual public System.Decimal? QuantidadeBloqueada
		{
			get
			{
				return base.GetSystemDecimal(PosicaoRendaFixaAberturaMetadata.ColumnNames.QuantidadeBloqueada);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoRendaFixaAberturaMetadata.ColumnNames.QuantidadeBloqueada, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixaAbertura.DataOperacao
		/// </summary>
		virtual public System.DateTime? DataOperacao
		{
			get
			{
				return base.GetSystemDateTime(PosicaoRendaFixaAberturaMetadata.ColumnNames.DataOperacao);
			}
			
			set
			{
				base.SetSystemDateTime(PosicaoRendaFixaAberturaMetadata.ColumnNames.DataOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixaAbertura.DataLiquidacao
		/// </summary>
		virtual public System.DateTime? DataLiquidacao
		{
			get
			{
				return base.GetSystemDateTime(PosicaoRendaFixaAberturaMetadata.ColumnNames.DataLiquidacao);
			}
			
			set
			{
				base.SetSystemDateTime(PosicaoRendaFixaAberturaMetadata.ColumnNames.DataLiquidacao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixaAbertura.PUOperacao
		/// </summary>
		virtual public System.Decimal? PUOperacao
		{
			get
			{
				return base.GetSystemDecimal(PosicaoRendaFixaAberturaMetadata.ColumnNames.PUOperacao);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoRendaFixaAberturaMetadata.ColumnNames.PUOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixaAbertura.PUCurva
		/// </summary>
		virtual public System.Decimal? PUCurva
		{
			get
			{
				return base.GetSystemDecimal(PosicaoRendaFixaAberturaMetadata.ColumnNames.PUCurva);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoRendaFixaAberturaMetadata.ColumnNames.PUCurva, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixaAbertura.ValorCurva
		/// </summary>
		virtual public System.Decimal? ValorCurva
		{
			get
			{
				return base.GetSystemDecimal(PosicaoRendaFixaAberturaMetadata.ColumnNames.ValorCurva);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoRendaFixaAberturaMetadata.ColumnNames.ValorCurva, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixaAbertura.PUMercado
		/// </summary>
		virtual public System.Decimal? PUMercado
		{
			get
			{
				return base.GetSystemDecimal(PosicaoRendaFixaAberturaMetadata.ColumnNames.PUMercado);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoRendaFixaAberturaMetadata.ColumnNames.PUMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixaAbertura.ValorMercado
		/// </summary>
		virtual public System.Decimal? ValorMercado
		{
			get
			{
				return base.GetSystemDecimal(PosicaoRendaFixaAberturaMetadata.ColumnNames.ValorMercado);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoRendaFixaAberturaMetadata.ColumnNames.ValorMercado, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixaAbertura.PUJuros
		/// </summary>
		virtual public System.Decimal? PUJuros
		{
			get
			{
				return base.GetSystemDecimal(PosicaoRendaFixaAberturaMetadata.ColumnNames.PUJuros);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoRendaFixaAberturaMetadata.ColumnNames.PUJuros, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixaAbertura.ValorJuros
		/// </summary>
		virtual public System.Decimal? ValorJuros
		{
			get
			{
				return base.GetSystemDecimal(PosicaoRendaFixaAberturaMetadata.ColumnNames.ValorJuros);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoRendaFixaAberturaMetadata.ColumnNames.ValorJuros, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixaAbertura.DataVolta
		/// </summary>
		virtual public System.DateTime? DataVolta
		{
			get
			{
				return base.GetSystemDateTime(PosicaoRendaFixaAberturaMetadata.ColumnNames.DataVolta);
			}
			
			set
			{
				base.SetSystemDateTime(PosicaoRendaFixaAberturaMetadata.ColumnNames.DataVolta, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixaAbertura.TaxaVolta
		/// </summary>
		virtual public System.Decimal? TaxaVolta
		{
			get
			{
				return base.GetSystemDecimal(PosicaoRendaFixaAberturaMetadata.ColumnNames.TaxaVolta);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoRendaFixaAberturaMetadata.ColumnNames.TaxaVolta, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixaAbertura.PUVolta
		/// </summary>
		virtual public System.Decimal? PUVolta
		{
			get
			{
				return base.GetSystemDecimal(PosicaoRendaFixaAberturaMetadata.ColumnNames.PUVolta);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoRendaFixaAberturaMetadata.ColumnNames.PUVolta, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixaAbertura.ValorVolta
		/// </summary>
		virtual public System.Decimal? ValorVolta
		{
			get
			{
				return base.GetSystemDecimal(PosicaoRendaFixaAberturaMetadata.ColumnNames.ValorVolta);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoRendaFixaAberturaMetadata.ColumnNames.ValorVolta, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixaAbertura.QuantidadeInicial
		/// </summary>
		virtual public System.Decimal? QuantidadeInicial
		{
			get
			{
				return base.GetSystemDecimal(PosicaoRendaFixaAberturaMetadata.ColumnNames.QuantidadeInicial);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoRendaFixaAberturaMetadata.ColumnNames.QuantidadeInicial, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixaAbertura.ValorIR
		/// </summary>
		virtual public System.Decimal? ValorIR
		{
			get
			{
				return base.GetSystemDecimal(PosicaoRendaFixaAberturaMetadata.ColumnNames.ValorIR);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoRendaFixaAberturaMetadata.ColumnNames.ValorIR, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixaAbertura.ValorIOF
		/// </summary>
		virtual public System.Decimal? ValorIOF
		{
			get
			{
				return base.GetSystemDecimal(PosicaoRendaFixaAberturaMetadata.ColumnNames.ValorIOF);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoRendaFixaAberturaMetadata.ColumnNames.ValorIOF, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixaAbertura.TipoNegociacao
		/// </summary>
		virtual public System.Byte? TipoNegociacao
		{
			get
			{
				return base.GetSystemByte(PosicaoRendaFixaAberturaMetadata.ColumnNames.TipoNegociacao);
			}
			
			set
			{
				base.SetSystemByte(PosicaoRendaFixaAberturaMetadata.ColumnNames.TipoNegociacao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixaAbertura.PUCorrecao
		/// </summary>
		virtual public System.Decimal? PUCorrecao
		{
			get
			{
				return base.GetSystemDecimal(PosicaoRendaFixaAberturaMetadata.ColumnNames.PUCorrecao);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoRendaFixaAberturaMetadata.ColumnNames.PUCorrecao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixaAbertura.ValorCorrecao
		/// </summary>
		virtual public System.Decimal? ValorCorrecao
		{
			get
			{
				return base.GetSystemDecimal(PosicaoRendaFixaAberturaMetadata.ColumnNames.ValorCorrecao);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoRendaFixaAberturaMetadata.ColumnNames.ValorCorrecao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixaAbertura.TaxaOperacao
		/// </summary>
		virtual public System.Decimal? TaxaOperacao
		{
			get
			{
				return base.GetSystemDecimal(PosicaoRendaFixaAberturaMetadata.ColumnNames.TaxaOperacao);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoRendaFixaAberturaMetadata.ColumnNames.TaxaOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixaAbertura.IdAgente
		/// </summary>
		virtual public System.Int32? IdAgente
		{
			get
			{
				return base.GetSystemInt32(PosicaoRendaFixaAberturaMetadata.ColumnNames.IdAgente);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoRendaFixaAberturaMetadata.ColumnNames.IdAgente, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixaAbertura.IdCustodia
		/// </summary>
		virtual public System.Byte? IdCustodia
		{
			get
			{
				return base.GetSystemByte(PosicaoRendaFixaAberturaMetadata.ColumnNames.IdCustodia);
			}
			
			set
			{
				base.SetSystemByte(PosicaoRendaFixaAberturaMetadata.ColumnNames.IdCustodia, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixaAbertura.CustoCustodia
		/// </summary>
		virtual public System.Decimal? CustoCustodia
		{
			get
			{
				return base.GetSystemDecimal(PosicaoRendaFixaAberturaMetadata.ColumnNames.CustoCustodia);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoRendaFixaAberturaMetadata.ColumnNames.CustoCustodia, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixaAbertura.IdIndiceVolta
		/// </summary>
		virtual public System.Int16? IdIndiceVolta
		{
			get
			{
				return base.GetSystemInt16(PosicaoRendaFixaAberturaMetadata.ColumnNames.IdIndiceVolta);
			}
			
			set
			{
				base.SetSystemInt16(PosicaoRendaFixaAberturaMetadata.ColumnNames.IdIndiceVolta, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixaAbertura.IdOperacao
		/// </summary>
		virtual public System.Int32? IdOperacao
		{
			get
			{
				return base.GetSystemInt32(PosicaoRendaFixaAberturaMetadata.ColumnNames.IdOperacao);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoRendaFixaAberturaMetadata.ColumnNames.IdOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixaAbertura.OperacaoTermo
		/// </summary>
		virtual public System.String OperacaoTermo
		{
			get
			{
				return base.GetSystemString(PosicaoRendaFixaAberturaMetadata.ColumnNames.OperacaoTermo);
			}
			
			set
			{
				base.SetSystemString(PosicaoRendaFixaAberturaMetadata.ColumnNames.OperacaoTermo, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixaAbertura.ValorCurvaVencimento
		/// </summary>
		virtual public System.Decimal? ValorCurvaVencimento
		{
			get
			{
				return base.GetSystemDecimal(PosicaoRendaFixaAberturaMetadata.ColumnNames.ValorCurvaVencimento);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoRendaFixaAberturaMetadata.ColumnNames.ValorCurvaVencimento, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixaAbertura.PUCurvaVencimento
		/// </summary>
		virtual public System.Decimal? PUCurvaVencimento
		{
			get
			{
				return base.GetSystemDecimal(PosicaoRendaFixaAberturaMetadata.ColumnNames.PUCurvaVencimento);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoRendaFixaAberturaMetadata.ColumnNames.PUCurvaVencimento, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixaAbertura.AjusteMTM
		/// </summary>
		virtual public System.Decimal? AjusteMTM
		{
			get
			{
				return base.GetSystemDecimal(PosicaoRendaFixaAberturaMetadata.ColumnNames.AjusteMTM);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoRendaFixaAberturaMetadata.ColumnNames.AjusteMTM, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixaAbertura.AjusteVencimento
		/// </summary>
		virtual public System.Decimal? AjusteVencimento
		{
			get
			{
				return base.GetSystemDecimal(PosicaoRendaFixaAberturaMetadata.ColumnNames.AjusteVencimento);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoRendaFixaAberturaMetadata.ColumnNames.AjusteVencimento, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixaAbertura.TaxaMTM
		/// </summary>
		virtual public System.Decimal? TaxaMTM
		{
			get
			{
				return base.GetSystemDecimal(PosicaoRendaFixaAberturaMetadata.ColumnNames.TaxaMTM);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoRendaFixaAberturaMetadata.ColumnNames.TaxaMTM, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixaAbertura.IdCorretora
		/// </summary>
		virtual public System.Int32? IdCorretora
		{
			get
			{
				return base.GetSystemInt32(PosicaoRendaFixaAberturaMetadata.ColumnNames.IdCorretora);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoRendaFixaAberturaMetadata.ColumnNames.IdCorretora, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixaAbertura.ValorBrutoGrossUp
		/// </summary>
		virtual public System.Decimal? ValorBrutoGrossUp
		{
			get
			{
				return base.GetSystemDecimal(PosicaoRendaFixaAberturaMetadata.ColumnNames.ValorBrutoGrossUp);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoRendaFixaAberturaMetadata.ColumnNames.ValorBrutoGrossUp, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixaAbertura.AliquotaIR
		/// </summary>
		virtual public System.Decimal? AliquotaIR
		{
			get
			{
				return base.GetSystemDecimal(PosicaoRendaFixaAberturaMetadata.ColumnNames.AliquotaIR);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoRendaFixaAberturaMetadata.ColumnNames.AliquotaIR, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixaAbertura.AliquotaIOF
		/// </summary>
		virtual public System.Decimal? AliquotaIOF
		{
			get
			{
				return base.GetSystemDecimal(PosicaoRendaFixaAberturaMetadata.ColumnNames.AliquotaIOF);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoRendaFixaAberturaMetadata.ColumnNames.AliquotaIOF, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixaAbertura.PrazoDecorridoDC
		/// </summary>
		virtual public System.Int32? PrazoDecorridoDC
		{
			get
			{
				return base.GetSystemInt32(PosicaoRendaFixaAberturaMetadata.ColumnNames.PrazoDecorridoDC);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoRendaFixaAberturaMetadata.ColumnNames.PrazoDecorridoDC, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixaAbertura.PrazoDecorridoDU
		/// </summary>
		virtual public System.Int32? PrazoDecorridoDU
		{
			get
			{
				return base.GetSystemInt32(PosicaoRendaFixaAberturaMetadata.ColumnNames.PrazoDecorridoDU);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoRendaFixaAberturaMetadata.ColumnNames.PrazoDecorridoDU, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		[CLSCompliant(false)]
		internal protected TituloRendaFixa _UpToTituloRendaFixaByIdTitulo;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esPosicaoRendaFixaAbertura entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdPosicao
			{
				get
				{
					System.Int32? data = entity.IdPosicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPosicao = null;
					else entity.IdPosicao = Convert.ToInt32(value);
				}
			}
				
			public System.String DataHistorico
			{
				get
				{
					System.DateTime? data = entity.DataHistorico;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataHistorico = null;
					else entity.DataHistorico = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String IdTitulo
			{
				get
				{
					System.Int32? data = entity.IdTitulo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTitulo = null;
					else entity.IdTitulo = Convert.ToInt32(value);
				}
			}
				
			public System.String TipoOperacao
			{
				get
				{
					System.Byte? data = entity.TipoOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoOperacao = null;
					else entity.TipoOperacao = Convert.ToByte(value);
				}
			}
				
			public System.String DataVencimento
			{
				get
				{
					System.DateTime? data = entity.DataVencimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataVencimento = null;
					else entity.DataVencimento = Convert.ToDateTime(value);
				}
			}
				
			public System.String Quantidade
			{
				get
				{
					System.Decimal? data = entity.Quantidade;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Quantidade = null;
					else entity.Quantidade = Convert.ToDecimal(value);
				}
			}
				
			public System.String QuantidadeBloqueada
			{
				get
				{
					System.Decimal? data = entity.QuantidadeBloqueada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeBloqueada = null;
					else entity.QuantidadeBloqueada = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataOperacao
			{
				get
				{
					System.DateTime? data = entity.DataOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataOperacao = null;
					else entity.DataOperacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String DataLiquidacao
			{
				get
				{
					System.DateTime? data = entity.DataLiquidacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataLiquidacao = null;
					else entity.DataLiquidacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String PUOperacao
			{
				get
				{
					System.Decimal? data = entity.PUOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PUOperacao = null;
					else entity.PUOperacao = Convert.ToDecimal(value);
				}
			}
				
			public System.String PUCurva
			{
				get
				{
					System.Decimal? data = entity.PUCurva;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PUCurva = null;
					else entity.PUCurva = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorCurva
			{
				get
				{
					System.Decimal? data = entity.ValorCurva;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorCurva = null;
					else entity.ValorCurva = Convert.ToDecimal(value);
				}
			}
				
			public System.String PUMercado
			{
				get
				{
					System.Decimal? data = entity.PUMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PUMercado = null;
					else entity.PUMercado = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorMercado
			{
				get
				{
					System.Decimal? data = entity.ValorMercado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorMercado = null;
					else entity.ValorMercado = Convert.ToDecimal(value);
				}
			}
				
			public System.String PUJuros
			{
				get
				{
					System.Decimal? data = entity.PUJuros;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PUJuros = null;
					else entity.PUJuros = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorJuros
			{
				get
				{
					System.Decimal? data = entity.ValorJuros;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorJuros = null;
					else entity.ValorJuros = Convert.ToDecimal(value);
				}
			}
				
			public System.String DataVolta
			{
				get
				{
					System.DateTime? data = entity.DataVolta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataVolta = null;
					else entity.DataVolta = Convert.ToDateTime(value);
				}
			}
				
			public System.String TaxaVolta
			{
				get
				{
					System.Decimal? data = entity.TaxaVolta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TaxaVolta = null;
					else entity.TaxaVolta = Convert.ToDecimal(value);
				}
			}
				
			public System.String PUVolta
			{
				get
				{
					System.Decimal? data = entity.PUVolta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PUVolta = null;
					else entity.PUVolta = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorVolta
			{
				get
				{
					System.Decimal? data = entity.ValorVolta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorVolta = null;
					else entity.ValorVolta = Convert.ToDecimal(value);
				}
			}
				
			public System.String QuantidadeInicial
			{
				get
				{
					System.Decimal? data = entity.QuantidadeInicial;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeInicial = null;
					else entity.QuantidadeInicial = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIR
			{
				get
				{
					System.Decimal? data = entity.ValorIR;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIR = null;
					else entity.ValorIR = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorIOF
			{
				get
				{
					System.Decimal? data = entity.ValorIOF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorIOF = null;
					else entity.ValorIOF = Convert.ToDecimal(value);
				}
			}
				
			public System.String TipoNegociacao
			{
				get
				{
					System.Byte? data = entity.TipoNegociacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TipoNegociacao = null;
					else entity.TipoNegociacao = Convert.ToByte(value);
				}
			}
				
			public System.String PUCorrecao
			{
				get
				{
					System.Decimal? data = entity.PUCorrecao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PUCorrecao = null;
					else entity.PUCorrecao = Convert.ToDecimal(value);
				}
			}
				
			public System.String ValorCorrecao
			{
				get
				{
					System.Decimal? data = entity.ValorCorrecao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorCorrecao = null;
					else entity.ValorCorrecao = Convert.ToDecimal(value);
				}
			}
				
			public System.String TaxaOperacao
			{
				get
				{
					System.Decimal? data = entity.TaxaOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TaxaOperacao = null;
					else entity.TaxaOperacao = Convert.ToDecimal(value);
				}
			}
				
			public System.String IdAgente
			{
				get
				{
					System.Int32? data = entity.IdAgente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdAgente = null;
					else entity.IdAgente = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCustodia
			{
				get
				{
					System.Byte? data = entity.IdCustodia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCustodia = null;
					else entity.IdCustodia = Convert.ToByte(value);
				}
			}
				
			public System.String CustoCustodia
			{
				get
				{
					System.Decimal? data = entity.CustoCustodia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CustoCustodia = null;
					else entity.CustoCustodia = Convert.ToDecimal(value);
				}
			}
				
			public System.String IdIndiceVolta
			{
				get
				{
					System.Int16? data = entity.IdIndiceVolta;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdIndiceVolta = null;
					else entity.IdIndiceVolta = Convert.ToInt16(value);
				}
			}
				
			public System.String IdOperacao
			{
				get
				{
					System.Int32? data = entity.IdOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacao = null;
					else entity.IdOperacao = Convert.ToInt32(value);
				}
			}
				
			public System.String OperacaoTermo
			{
				get
				{
					System.String data = entity.OperacaoTermo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.OperacaoTermo = null;
					else entity.OperacaoTermo = Convert.ToString(value);
				}
			}
				
			public System.String ValorCurvaVencimento
			{
				get
				{
					System.Decimal? data = entity.ValorCurvaVencimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorCurvaVencimento = null;
					else entity.ValorCurvaVencimento = Convert.ToDecimal(value);
				}
			}
				
			public System.String PUCurvaVencimento
			{
				get
				{
					System.Decimal? data = entity.PUCurvaVencimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PUCurvaVencimento = null;
					else entity.PUCurvaVencimento = Convert.ToDecimal(value);
				}
			}
				
			public System.String AjusteMTM
			{
				get
				{
					System.Decimal? data = entity.AjusteMTM;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AjusteMTM = null;
					else entity.AjusteMTM = Convert.ToDecimal(value);
				}
			}
				
			public System.String AjusteVencimento
			{
				get
				{
					System.Decimal? data = entity.AjusteVencimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AjusteVencimento = null;
					else entity.AjusteVencimento = Convert.ToDecimal(value);
				}
			}
				
			public System.String TaxaMTM
			{
				get
				{
					System.Decimal? data = entity.TaxaMTM;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.TaxaMTM = null;
					else entity.TaxaMTM = Convert.ToDecimal(value);
				}
			}
				
			public System.String IdCorretora
			{
				get
				{
					System.Int32? data = entity.IdCorretora;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCorretora = null;
					else entity.IdCorretora = Convert.ToInt32(value);
				}
			}
				
			public System.String ValorBrutoGrossUp
			{
				get
				{
					System.Decimal? data = entity.ValorBrutoGrossUp;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.ValorBrutoGrossUp = null;
					else entity.ValorBrutoGrossUp = Convert.ToDecimal(value);
				}
			}
				
			public System.String AliquotaIR
			{
				get
				{
					System.Decimal? data = entity.AliquotaIR;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AliquotaIR = null;
					else entity.AliquotaIR = Convert.ToDecimal(value);
				}
			}
				
			public System.String AliquotaIOF
			{
				get
				{
					System.Decimal? data = entity.AliquotaIOF;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.AliquotaIOF = null;
					else entity.AliquotaIOF = Convert.ToDecimal(value);
				}
			}
				
			public System.String PrazoDecorridoDC
			{
				get
				{
					System.Int32? data = entity.PrazoDecorridoDC;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PrazoDecorridoDC = null;
					else entity.PrazoDecorridoDC = Convert.ToInt32(value);
				}
			}
				
			public System.String PrazoDecorridoDU
			{
				get
				{
					System.Int32? data = entity.PrazoDecorridoDU;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PrazoDecorridoDU = null;
					else entity.PrazoDecorridoDU = Convert.ToInt32(value);
				}
			}
			

			private esPosicaoRendaFixaAbertura entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esPosicaoRendaFixaAberturaQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esPosicaoRendaFixaAbertura can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class PosicaoRendaFixaAbertura : esPosicaoRendaFixaAbertura
	{

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_PosicaoRendaFixaAbertura_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToTituloRendaFixaByIdTitulo - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - TituloRendaFixa_PosicaoRendaFixaAbertura_FK1
		/// </summary>

		[XmlIgnore]
		public TituloRendaFixa UpToTituloRendaFixaByIdTitulo
		{
			get
			{
				if(this._UpToTituloRendaFixaByIdTitulo == null
					&& IdTitulo != null					)
				{
					this._UpToTituloRendaFixaByIdTitulo = new TituloRendaFixa();
					this._UpToTituloRendaFixaByIdTitulo.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToTituloRendaFixaByIdTitulo", this._UpToTituloRendaFixaByIdTitulo);
					this._UpToTituloRendaFixaByIdTitulo.Query.Where(this._UpToTituloRendaFixaByIdTitulo.Query.IdTitulo == this.IdTitulo);
					this._UpToTituloRendaFixaByIdTitulo.Query.Load();
				}

				return this._UpToTituloRendaFixaByIdTitulo;
			}
			
			set
			{
				this.RemovePreSave("UpToTituloRendaFixaByIdTitulo");
				

				if(value == null)
				{
					this.IdTitulo = null;
					this._UpToTituloRendaFixaByIdTitulo = null;
				}
				else
				{
					this.IdTitulo = value.IdTitulo;
					this._UpToTituloRendaFixaByIdTitulo = value;
					this.SetPreSave("UpToTituloRendaFixaByIdTitulo", this._UpToTituloRendaFixaByIdTitulo);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToTituloRendaFixaByIdTitulo != null)
			{
				this.IdTitulo = this._UpToTituloRendaFixaByIdTitulo.IdTitulo;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esPosicaoRendaFixaAberturaQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return PosicaoRendaFixaAberturaMetadata.Meta();
			}
		}	
		

		public esQueryItem IdPosicao
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaAberturaMetadata.ColumnNames.IdPosicao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataHistorico
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaAberturaMetadata.ColumnNames.DataHistorico, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaAberturaMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdTitulo
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaAberturaMetadata.ColumnNames.IdTitulo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem TipoOperacao
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaAberturaMetadata.ColumnNames.TipoOperacao, esSystemType.Byte);
			}
		} 
		
		public esQueryItem DataVencimento
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaAberturaMetadata.ColumnNames.DataVencimento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Quantidade
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaAberturaMetadata.ColumnNames.Quantidade, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem QuantidadeBloqueada
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaAberturaMetadata.ColumnNames.QuantidadeBloqueada, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataOperacao
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaAberturaMetadata.ColumnNames.DataOperacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem DataLiquidacao
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaAberturaMetadata.ColumnNames.DataLiquidacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem PUOperacao
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaAberturaMetadata.ColumnNames.PUOperacao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PUCurva
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaAberturaMetadata.ColumnNames.PUCurva, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorCurva
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaAberturaMetadata.ColumnNames.ValorCurva, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PUMercado
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaAberturaMetadata.ColumnNames.PUMercado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorMercado
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaAberturaMetadata.ColumnNames.ValorMercado, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PUJuros
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaAberturaMetadata.ColumnNames.PUJuros, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorJuros
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaAberturaMetadata.ColumnNames.ValorJuros, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DataVolta
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaAberturaMetadata.ColumnNames.DataVolta, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem TaxaVolta
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaAberturaMetadata.ColumnNames.TaxaVolta, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PUVolta
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaAberturaMetadata.ColumnNames.PUVolta, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorVolta
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaAberturaMetadata.ColumnNames.ValorVolta, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem QuantidadeInicial
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaAberturaMetadata.ColumnNames.QuantidadeInicial, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIR
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaAberturaMetadata.ColumnNames.ValorIR, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorIOF
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaAberturaMetadata.ColumnNames.ValorIOF, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TipoNegociacao
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaAberturaMetadata.ColumnNames.TipoNegociacao, esSystemType.Byte);
			}
		} 
		
		public esQueryItem PUCorrecao
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaAberturaMetadata.ColumnNames.PUCorrecao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem ValorCorrecao
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaAberturaMetadata.ColumnNames.ValorCorrecao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TaxaOperacao
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaAberturaMetadata.ColumnNames.TaxaOperacao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IdAgente
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaAberturaMetadata.ColumnNames.IdAgente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCustodia
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaAberturaMetadata.ColumnNames.IdCustodia, esSystemType.Byte);
			}
		} 
		
		public esQueryItem CustoCustodia
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaAberturaMetadata.ColumnNames.CustoCustodia, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IdIndiceVolta
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaAberturaMetadata.ColumnNames.IdIndiceVolta, esSystemType.Int16);
			}
		} 
		
		public esQueryItem IdOperacao
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaAberturaMetadata.ColumnNames.IdOperacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem OperacaoTermo
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaAberturaMetadata.ColumnNames.OperacaoTermo, esSystemType.String);
			}
		} 
		
		public esQueryItem ValorCurvaVencimento
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaAberturaMetadata.ColumnNames.ValorCurvaVencimento, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PUCurvaVencimento
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaAberturaMetadata.ColumnNames.PUCurvaVencimento, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem AjusteMTM
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaAberturaMetadata.ColumnNames.AjusteMTM, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem AjusteVencimento
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaAberturaMetadata.ColumnNames.AjusteVencimento, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem TaxaMTM
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaAberturaMetadata.ColumnNames.TaxaMTM, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem IdCorretora
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaAberturaMetadata.ColumnNames.IdCorretora, esSystemType.Int32);
			}
		} 
		
		public esQueryItem ValorBrutoGrossUp
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaAberturaMetadata.ColumnNames.ValorBrutoGrossUp, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem AliquotaIR
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaAberturaMetadata.ColumnNames.AliquotaIR, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem AliquotaIOF
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaAberturaMetadata.ColumnNames.AliquotaIOF, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PrazoDecorridoDC
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaAberturaMetadata.ColumnNames.PrazoDecorridoDC, esSystemType.Int32);
			}
		} 
		
		public esQueryItem PrazoDecorridoDU
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaAberturaMetadata.ColumnNames.PrazoDecorridoDU, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("PosicaoRendaFixaAberturaCollection")]
	public partial class PosicaoRendaFixaAberturaCollection : esPosicaoRendaFixaAberturaCollection, IEnumerable<PosicaoRendaFixaAbertura>
	{
		public PosicaoRendaFixaAberturaCollection()
		{

		}
		
		public static implicit operator List<PosicaoRendaFixaAbertura>(PosicaoRendaFixaAberturaCollection coll)
		{
			List<PosicaoRendaFixaAbertura> list = new List<PosicaoRendaFixaAbertura>();
			
			foreach (PosicaoRendaFixaAbertura emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  PosicaoRendaFixaAberturaMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PosicaoRendaFixaAberturaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new PosicaoRendaFixaAbertura(row);
		}

		override protected esEntity CreateEntity()
		{
			return new PosicaoRendaFixaAbertura();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public PosicaoRendaFixaAberturaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PosicaoRendaFixaAberturaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(PosicaoRendaFixaAberturaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public PosicaoRendaFixaAbertura AddNew()
		{
			PosicaoRendaFixaAbertura entity = base.AddNewEntity() as PosicaoRendaFixaAbertura;
			
			return entity;
		}

		public PosicaoRendaFixaAbertura FindByPrimaryKey(System.Int32 idPosicao, System.DateTime dataHistorico)
		{
			return base.FindByPrimaryKey(idPosicao, dataHistorico) as PosicaoRendaFixaAbertura;
		}


		#region IEnumerable<PosicaoRendaFixaAbertura> Members

		IEnumerator<PosicaoRendaFixaAbertura> IEnumerable<PosicaoRendaFixaAbertura>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as PosicaoRendaFixaAbertura;
			}
		}

		#endregion
		
		private PosicaoRendaFixaAberturaQuery query;
	}


	/// <summary>
	/// Encapsulates the 'PosicaoRendaFixaAbertura' table
	/// </summary>

	[Serializable]
	public partial class PosicaoRendaFixaAbertura : esPosicaoRendaFixaAbertura
	{
		public PosicaoRendaFixaAbertura()
		{

		}
	
		public PosicaoRendaFixaAbertura(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return PosicaoRendaFixaAberturaMetadata.Meta();
			}
		}
		
		
		
		override protected esPosicaoRendaFixaAberturaQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PosicaoRendaFixaAberturaQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public PosicaoRendaFixaAberturaQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PosicaoRendaFixaAberturaQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(PosicaoRendaFixaAberturaQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private PosicaoRendaFixaAberturaQuery query;
	}



	[Serializable]
	public partial class PosicaoRendaFixaAberturaQuery : esPosicaoRendaFixaAberturaQuery
	{
		public PosicaoRendaFixaAberturaQuery()
		{

		}		
		
		public PosicaoRendaFixaAberturaQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class PosicaoRendaFixaAberturaMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected PosicaoRendaFixaAberturaMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(PosicaoRendaFixaAberturaMetadata.ColumnNames.IdPosicao, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoRendaFixaAberturaMetadata.PropertyNames.IdPosicao;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaAberturaMetadata.ColumnNames.DataHistorico, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PosicaoRendaFixaAberturaMetadata.PropertyNames.DataHistorico;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaAberturaMetadata.ColumnNames.IdCliente, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoRendaFixaAberturaMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaAberturaMetadata.ColumnNames.IdTitulo, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoRendaFixaAberturaMetadata.PropertyNames.IdTitulo;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaAberturaMetadata.ColumnNames.TipoOperacao, 4, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = PosicaoRendaFixaAberturaMetadata.PropertyNames.TipoOperacao;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaAberturaMetadata.ColumnNames.DataVencimento, 5, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PosicaoRendaFixaAberturaMetadata.PropertyNames.DataVencimento;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaAberturaMetadata.ColumnNames.Quantidade, 6, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoRendaFixaAberturaMetadata.PropertyNames.Quantidade;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaAberturaMetadata.ColumnNames.QuantidadeBloqueada, 7, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoRendaFixaAberturaMetadata.PropertyNames.QuantidadeBloqueada;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaAberturaMetadata.ColumnNames.DataOperacao, 8, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PosicaoRendaFixaAberturaMetadata.PropertyNames.DataOperacao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaAberturaMetadata.ColumnNames.DataLiquidacao, 9, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PosicaoRendaFixaAberturaMetadata.PropertyNames.DataLiquidacao;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaAberturaMetadata.ColumnNames.PUOperacao, 10, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoRendaFixaAberturaMetadata.PropertyNames.PUOperacao;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaAberturaMetadata.ColumnNames.PUCurva, 11, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoRendaFixaAberturaMetadata.PropertyNames.PUCurva;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaAberturaMetadata.ColumnNames.ValorCurva, 12, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoRendaFixaAberturaMetadata.PropertyNames.ValorCurva;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaAberturaMetadata.ColumnNames.PUMercado, 13, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoRendaFixaAberturaMetadata.PropertyNames.PUMercado;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaAberturaMetadata.ColumnNames.ValorMercado, 14, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoRendaFixaAberturaMetadata.PropertyNames.ValorMercado;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaAberturaMetadata.ColumnNames.PUJuros, 15, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoRendaFixaAberturaMetadata.PropertyNames.PUJuros;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaAberturaMetadata.ColumnNames.ValorJuros, 16, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoRendaFixaAberturaMetadata.PropertyNames.ValorJuros;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaAberturaMetadata.ColumnNames.DataVolta, 17, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PosicaoRendaFixaAberturaMetadata.PropertyNames.DataVolta;
			c.NumericPrecision = 0;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaAberturaMetadata.ColumnNames.TaxaVolta, 18, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoRendaFixaAberturaMetadata.PropertyNames.TaxaVolta;	
			c.NumericPrecision = 8;
			c.NumericScale = 4;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaAberturaMetadata.ColumnNames.PUVolta, 19, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoRendaFixaAberturaMetadata.PropertyNames.PUVolta;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaAberturaMetadata.ColumnNames.ValorVolta, 20, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoRendaFixaAberturaMetadata.PropertyNames.ValorVolta;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaAberturaMetadata.ColumnNames.QuantidadeInicial, 21, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoRendaFixaAberturaMetadata.PropertyNames.QuantidadeInicial;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaAberturaMetadata.ColumnNames.ValorIR, 22, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoRendaFixaAberturaMetadata.PropertyNames.ValorIR;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaAberturaMetadata.ColumnNames.ValorIOF, 23, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoRendaFixaAberturaMetadata.PropertyNames.ValorIOF;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaAberturaMetadata.ColumnNames.TipoNegociacao, 24, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = PosicaoRendaFixaAberturaMetadata.PropertyNames.TipoNegociacao;	
			c.NumericPrecision = 3;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaAberturaMetadata.ColumnNames.PUCorrecao, 25, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoRendaFixaAberturaMetadata.PropertyNames.PUCorrecao;	
			c.NumericPrecision = 25;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaAberturaMetadata.ColumnNames.ValorCorrecao, 26, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoRendaFixaAberturaMetadata.PropertyNames.ValorCorrecao;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaAberturaMetadata.ColumnNames.TaxaOperacao, 27, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoRendaFixaAberturaMetadata.PropertyNames.TaxaOperacao;	
			c.NumericPrecision = 25;
			c.NumericScale = 16;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaAberturaMetadata.ColumnNames.IdAgente, 28, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoRendaFixaAberturaMetadata.PropertyNames.IdAgente;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaAberturaMetadata.ColumnNames.IdCustodia, 29, typeof(System.Byte), esSystemType.Byte);
			c.PropertyName = PosicaoRendaFixaAberturaMetadata.PropertyNames.IdCustodia;	
			c.NumericPrecision = 3;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaAberturaMetadata.ColumnNames.CustoCustodia, 30, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoRendaFixaAberturaMetadata.PropertyNames.CustoCustodia;	
			c.NumericPrecision = 16;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"((0))";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaAberturaMetadata.ColumnNames.IdIndiceVolta, 31, typeof(System.Int16), esSystemType.Int16);
			c.PropertyName = PosicaoRendaFixaAberturaMetadata.PropertyNames.IdIndiceVolta;	
			c.NumericPrecision = 5;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaAberturaMetadata.ColumnNames.IdOperacao, 32, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoRendaFixaAberturaMetadata.PropertyNames.IdOperacao;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaAberturaMetadata.ColumnNames.OperacaoTermo, 33, typeof(System.String), esSystemType.String);
			c.PropertyName = PosicaoRendaFixaAberturaMetadata.PropertyNames.OperacaoTermo;
			c.CharacterMaxLength = 1;
			c.NumericPrecision = 0;
			c.HasDefault = true;
			c.Default = @"('N')";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaAberturaMetadata.ColumnNames.ValorCurvaVencimento, 34, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoRendaFixaAberturaMetadata.PropertyNames.ValorCurvaVencimento;	
			c.NumericPrecision = 25;
			c.NumericScale = 2;
			c.HasDefault = true;
			c.Default = @"('0')";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaAberturaMetadata.ColumnNames.PUCurvaVencimento, 35, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoRendaFixaAberturaMetadata.PropertyNames.PUCurvaVencimento;	
			c.NumericPrecision = 25;
			c.NumericScale = 8;
			c.HasDefault = true;
			c.Default = @"('0')";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaAberturaMetadata.ColumnNames.AjusteMTM, 36, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoRendaFixaAberturaMetadata.PropertyNames.AjusteMTM;	
			c.NumericPrecision = 25;
			c.NumericScale = 8;
			c.HasDefault = true;
			c.Default = @"('0')";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaAberturaMetadata.ColumnNames.AjusteVencimento, 37, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoRendaFixaAberturaMetadata.PropertyNames.AjusteVencimento;	
			c.NumericPrecision = 25;
			c.NumericScale = 8;
			c.HasDefault = true;
			c.Default = @"('0')";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaAberturaMetadata.ColumnNames.TaxaMTM, 38, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoRendaFixaAberturaMetadata.PropertyNames.TaxaMTM;	
			c.NumericPrecision = 25;
			c.NumericScale = 8;
			c.HasDefault = true;
			c.Default = @"('0')";
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaAberturaMetadata.ColumnNames.IdCorretora, 39, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoRendaFixaAberturaMetadata.PropertyNames.IdCorretora;	
			c.NumericPrecision = 10;
			c.IsNullable = true;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaAberturaMetadata.ColumnNames.ValorBrutoGrossUp, 40, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoRendaFixaAberturaMetadata.PropertyNames.ValorBrutoGrossUp;	
			c.NumericPrecision = 28;
			c.NumericScale = 10;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaAberturaMetadata.ColumnNames.AliquotaIR, 41, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoRendaFixaAberturaMetadata.PropertyNames.AliquotaIR;	
			c.NumericPrecision = 28;
			c.NumericScale = 10;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaAberturaMetadata.ColumnNames.AliquotaIOF, 42, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoRendaFixaAberturaMetadata.PropertyNames.AliquotaIOF;	
			c.NumericPrecision = 28;
			c.NumericScale = 10;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaAberturaMetadata.ColumnNames.PrazoDecorridoDC, 43, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoRendaFixaAberturaMetadata.PropertyNames.PrazoDecorridoDC;	
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaAberturaMetadata.ColumnNames.PrazoDecorridoDU, 44, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoRendaFixaAberturaMetadata.PropertyNames.PrazoDecorridoDU;	
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"((0))";
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public PosicaoRendaFixaAberturaMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdPosicao = "IdPosicao";
			 public const string DataHistorico = "DataHistorico";
			 public const string IdCliente = "IdCliente";
			 public const string IdTitulo = "IdTitulo";
			 public const string TipoOperacao = "TipoOperacao";
			 public const string DataVencimento = "DataVencimento";
			 public const string Quantidade = "Quantidade";
			 public const string QuantidadeBloqueada = "QuantidadeBloqueada";
			 public const string DataOperacao = "DataOperacao";
			 public const string DataLiquidacao = "DataLiquidacao";
			 public const string PUOperacao = "PUOperacao";
			 public const string PUCurva = "PUCurva";
			 public const string ValorCurva = "ValorCurva";
			 public const string PUMercado = "PUMercado";
			 public const string ValorMercado = "ValorMercado";
			 public const string PUJuros = "PUJuros";
			 public const string ValorJuros = "ValorJuros";
			 public const string DataVolta = "DataVolta";
			 public const string TaxaVolta = "TaxaVolta";
			 public const string PUVolta = "PUVolta";
			 public const string ValorVolta = "ValorVolta";
			 public const string QuantidadeInicial = "QuantidadeInicial";
			 public const string ValorIR = "ValorIR";
			 public const string ValorIOF = "ValorIOF";
			 public const string TipoNegociacao = "TipoNegociacao";
			 public const string PUCorrecao = "PUCorrecao";
			 public const string ValorCorrecao = "ValorCorrecao";
			 public const string TaxaOperacao = "TaxaOperacao";
			 public const string IdAgente = "IdAgente";
			 public const string IdCustodia = "IdCustodia";
			 public const string CustoCustodia = "CustoCustodia";
			 public const string IdIndiceVolta = "IdIndiceVolta";
			 public const string IdOperacao = "IdOperacao";
			 public const string OperacaoTermo = "OperacaoTermo";
			 public const string ValorCurvaVencimento = "ValorCurvaVencimento";
			 public const string PUCurvaVencimento = "PUCurvaVencimento";
			 public const string AjusteMTM = "AjusteMTM";
			 public const string AjusteVencimento = "AjusteVencimento";
			 public const string TaxaMTM = "TaxaMTM";
			 public const string IdCorretora = "IdCorretora";
			 public const string ValorBrutoGrossUp = "ValorBrutoGrossUp";
			 public const string AliquotaIR = "AliquotaIR";
			 public const string AliquotaIOF = "AliquotaIOF";
			 public const string PrazoDecorridoDC = "PrazoDecorridoDC";
			 public const string PrazoDecorridoDU = "PrazoDecorridoDU";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdPosicao = "IdPosicao";
			 public const string DataHistorico = "DataHistorico";
			 public const string IdCliente = "IdCliente";
			 public const string IdTitulo = "IdTitulo";
			 public const string TipoOperacao = "TipoOperacao";
			 public const string DataVencimento = "DataVencimento";
			 public const string Quantidade = "Quantidade";
			 public const string QuantidadeBloqueada = "QuantidadeBloqueada";
			 public const string DataOperacao = "DataOperacao";
			 public const string DataLiquidacao = "DataLiquidacao";
			 public const string PUOperacao = "PUOperacao";
			 public const string PUCurva = "PUCurva";
			 public const string ValorCurva = "ValorCurva";
			 public const string PUMercado = "PUMercado";
			 public const string ValorMercado = "ValorMercado";
			 public const string PUJuros = "PUJuros";
			 public const string ValorJuros = "ValorJuros";
			 public const string DataVolta = "DataVolta";
			 public const string TaxaVolta = "TaxaVolta";
			 public const string PUVolta = "PUVolta";
			 public const string ValorVolta = "ValorVolta";
			 public const string QuantidadeInicial = "QuantidadeInicial";
			 public const string ValorIR = "ValorIR";
			 public const string ValorIOF = "ValorIOF";
			 public const string TipoNegociacao = "TipoNegociacao";
			 public const string PUCorrecao = "PUCorrecao";
			 public const string ValorCorrecao = "ValorCorrecao";
			 public const string TaxaOperacao = "TaxaOperacao";
			 public const string IdAgente = "IdAgente";
			 public const string IdCustodia = "IdCustodia";
			 public const string CustoCustodia = "CustoCustodia";
			 public const string IdIndiceVolta = "IdIndiceVolta";
			 public const string IdOperacao = "IdOperacao";
			 public const string OperacaoTermo = "OperacaoTermo";
			 public const string ValorCurvaVencimento = "ValorCurvaVencimento";
			 public const string PUCurvaVencimento = "PUCurvaVencimento";
			 public const string AjusteMTM = "AjusteMTM";
			 public const string AjusteVencimento = "AjusteVencimento";
			 public const string TaxaMTM = "TaxaMTM";
			 public const string IdCorretora = "IdCorretora";
			 public const string ValorBrutoGrossUp = "ValorBrutoGrossUp";
			 public const string AliquotaIR = "AliquotaIR";
			 public const string AliquotaIOF = "AliquotaIOF";
			 public const string PrazoDecorridoDC = "PrazoDecorridoDC";
			 public const string PrazoDecorridoDU = "PrazoDecorridoDU";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(PosicaoRendaFixaAberturaMetadata))
			{
				if(PosicaoRendaFixaAberturaMetadata.mapDelegates == null)
				{
					PosicaoRendaFixaAberturaMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (PosicaoRendaFixaAberturaMetadata.meta == null)
				{
					PosicaoRendaFixaAberturaMetadata.meta = new PosicaoRendaFixaAberturaMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdPosicao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataHistorico", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdTitulo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("TipoOperacao", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("DataVencimento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Quantidade", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("QuantidadeBloqueada", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("DataOperacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("DataLiquidacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("PUOperacao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PUCurva", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorCurva", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PUMercado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorMercado", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PUJuros", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorJuros", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("DataVolta", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("TaxaVolta", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PUVolta", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorVolta", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("QuantidadeInicial", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIR", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorIOF", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("TipoNegociacao", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("PUCorrecao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("ValorCorrecao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("TaxaOperacao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IdAgente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCustodia", new esTypeMap("tinyint", "System.Byte"));
				meta.AddTypeMap("CustoCustodia", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IdIndiceVolta", new esTypeMap("smallint", "System.Int16"));
				meta.AddTypeMap("IdOperacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("OperacaoTermo", new esTypeMap("char", "System.String"));
				meta.AddTypeMap("ValorCurvaVencimento", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PUCurvaVencimento", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("AjusteMTM", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("AjusteVencimento", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("TaxaMTM", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("IdCorretora", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("ValorBrutoGrossUp", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("AliquotaIR", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("AliquotaIOF", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PrazoDecorridoDC", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("PrazoDecorridoDU", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "PosicaoRendaFixaAbertura";
				meta.Destination = "PosicaoRendaFixaAbertura";
				
				meta.spInsert = "proc_PosicaoRendaFixaAberturaInsert";				
				meta.spUpdate = "proc_PosicaoRendaFixaAberturaUpdate";		
				meta.spDelete = "proc_PosicaoRendaFixaAberturaDelete";
				meta.spLoadAll = "proc_PosicaoRendaFixaAberturaLoadAll";
				meta.spLoadByPrimaryKey = "proc_PosicaoRendaFixaAberturaLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private PosicaoRendaFixaAberturaMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
