/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 25/02/2015 18:29:34
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;

namespace Financial.RendaFixa
{

	[Serializable]
	abstract public class esDetalhePosicaoAfetadaRFCollection : esEntityCollection
	{
		public esDetalhePosicaoAfetadaRFCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "DetalhePosicaoAfetadaRFCollection";
		}

		#region Query Logic
		protected void InitQuery(esDetalhePosicaoAfetadaRFQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esDetalhePosicaoAfetadaRFQuery);
		}
		#endregion
		
		virtual public DetalhePosicaoAfetadaRF DetachEntity(DetalhePosicaoAfetadaRF entity)
		{
			return base.DetachEntity(entity) as DetalhePosicaoAfetadaRF;
		}
		
		virtual public DetalhePosicaoAfetadaRF AttachEntity(DetalhePosicaoAfetadaRF entity)
		{
			return base.AttachEntity(entity) as DetalhePosicaoAfetadaRF;
		}
		
		virtual public void Combine(DetalhePosicaoAfetadaRFCollection collection)
		{
			base.Combine(collection);
		}
		
		new public DetalhePosicaoAfetadaRF this[int index]
		{
			get
			{
				return base[index] as DetalhePosicaoAfetadaRF;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(DetalhePosicaoAfetadaRF);
		}
	}



	[Serializable]
	abstract public class esDetalhePosicaoAfetadaRF : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esDetalhePosicaoAfetadaRFQuery GetDynamicQuery()
		{
			return null;
		}

		public esDetalhePosicaoAfetadaRF()
		{

		}

		public esDetalhePosicaoAfetadaRF(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime dataOperacao, System.Int32 idOperacao, System.Int32 idPosicaoAfetada)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataOperacao, idOperacao, idPosicaoAfetada);
			else
				return LoadByPrimaryKeyStoredProcedure(dataOperacao, idOperacao, idPosicaoAfetada);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime dataOperacao, System.Int32 idOperacao, System.Int32 idPosicaoAfetada)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataOperacao, idOperacao, idPosicaoAfetada);
			else
				return LoadByPrimaryKeyStoredProcedure(dataOperacao, idOperacao, idPosicaoAfetada);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime dataOperacao, System.Int32 idOperacao, System.Int32 idPosicaoAfetada)
		{
			esDetalhePosicaoAfetadaRFQuery query = this.GetDynamicQuery();
			query.Where(query.DataOperacao == dataOperacao, query.IdOperacao == idOperacao, query.IdPosicaoAfetada == idPosicaoAfetada);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime dataOperacao, System.Int32 idOperacao, System.Int32 idPosicaoAfetada)
		{
			esParameters parms = new esParameters();
			parms.Add("DataOperacao",dataOperacao);			parms.Add("IdOperacao",idOperacao);			parms.Add("IdPosicaoAfetada",idPosicaoAfetada);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "DataOperacao": this.str.DataOperacao = (string)value; break;							
						case "IdOperacao": this.str.IdOperacao = (string)value; break;							
						case "IdPosicaoAfetada": this.str.IdPosicaoAfetada = (string)value; break;							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "QtdeMovimentada": this.str.QtdeMovimentada = (string)value; break;							
						case "PuOperacao": this.str.PuOperacao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "DataOperacao":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataOperacao = (System.DateTime?)value;
							break;
						
						case "IdOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdOperacao = (System.Int32?)value;
							break;
						
						case "IdPosicaoAfetada":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPosicaoAfetada = (System.Int32?)value;
							break;
						
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "QtdeMovimentada":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QtdeMovimentada = (System.Decimal?)value;
							break;
						
						case "PuOperacao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.PuOperacao = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to DetalhePosicaoAfetadaRF.DataOperacao
		/// </summary>
		virtual public System.DateTime? DataOperacao
		{
			get
			{
				return base.GetSystemDateTime(DetalhePosicaoAfetadaRFMetadata.ColumnNames.DataOperacao);
			}
			
			set
			{
				base.SetSystemDateTime(DetalhePosicaoAfetadaRFMetadata.ColumnNames.DataOperacao, value);
			}
		}
		
		/// <summary>
		/// Maps to DetalhePosicaoAfetadaRF.IdOperacao
		/// </summary>
		virtual public System.Int32? IdOperacao
		{
			get
			{
				return base.GetSystemInt32(DetalhePosicaoAfetadaRFMetadata.ColumnNames.IdOperacao);
			}
			
			set
			{
				if(base.SetSystemInt32(DetalhePosicaoAfetadaRFMetadata.ColumnNames.IdOperacao, value))
				{
					this._UpToOperacaoRendaFixaByIdOperacao = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to DetalhePosicaoAfetadaRF.IdPosicaoAfetada
		/// </summary>
		virtual public System.Int32? IdPosicaoAfetada
		{
			get
			{
				return base.GetSystemInt32(DetalhePosicaoAfetadaRFMetadata.ColumnNames.IdPosicaoAfetada);
			}
			
			set
			{
				if(base.SetSystemInt32(DetalhePosicaoAfetadaRFMetadata.ColumnNames.IdPosicaoAfetada, value))
				{
					this._UpToPosicaoRendaFixaByIdPosicaoAfetada = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to DetalhePosicaoAfetadaRF.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(DetalhePosicaoAfetadaRFMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				base.SetSystemInt32(DetalhePosicaoAfetadaRFMetadata.ColumnNames.IdCliente, value);
			}
		}
		
		/// <summary>
		/// Maps to DetalhePosicaoAfetadaRF.QtdeMovimentada
		/// </summary>
		virtual public System.Decimal? QtdeMovimentada
		{
			get
			{
				return base.GetSystemDecimal(DetalhePosicaoAfetadaRFMetadata.ColumnNames.QtdeMovimentada);
			}
			
			set
			{
				base.SetSystemDecimal(DetalhePosicaoAfetadaRFMetadata.ColumnNames.QtdeMovimentada, value);
			}
		}
		
		/// <summary>
		/// Maps to DetalhePosicaoAfetadaRF.PuOperacao
		/// </summary>
		virtual public System.Decimal? PuOperacao
		{
			get
			{
				return base.GetSystemDecimal(DetalhePosicaoAfetadaRFMetadata.ColumnNames.PuOperacao);
			}
			
			set
			{
				base.SetSystemDecimal(DetalhePosicaoAfetadaRFMetadata.ColumnNames.PuOperacao, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected OperacaoRendaFixa _UpToOperacaoRendaFixaByIdOperacao;
		[CLSCompliant(false)]
		internal protected PosicaoRendaFixa _UpToPosicaoRendaFixaByIdPosicaoAfetada;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esDetalhePosicaoAfetadaRF entity)
			{
				this.entity = entity;
			}
			
	
			public System.String DataOperacao
			{
				get
				{
					System.DateTime? data = entity.DataOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataOperacao = null;
					else entity.DataOperacao = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdOperacao
			{
				get
				{
					System.Int32? data = entity.IdOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdOperacao = null;
					else entity.IdOperacao = Convert.ToInt32(value);
				}
			}
				
			public System.String IdPosicaoAfetada
			{
				get
				{
					System.Int32? data = entity.IdPosicaoAfetada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPosicaoAfetada = null;
					else entity.IdPosicaoAfetada = Convert.ToInt32(value);
				}
			}
				
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String QtdeMovimentada
			{
				get
				{
					System.Decimal? data = entity.QtdeMovimentada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QtdeMovimentada = null;
					else entity.QtdeMovimentada = Convert.ToDecimal(value);
				}
			}
				
			public System.String PuOperacao
			{
				get
				{
					System.Decimal? data = entity.PuOperacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.PuOperacao = null;
					else entity.PuOperacao = Convert.ToDecimal(value);
				}
			}
			

			private esDetalhePosicaoAfetadaRF entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esDetalhePosicaoAfetadaRFQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esDetalhePosicaoAfetadaRF can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class DetalhePosicaoAfetadaRF : esDetalhePosicaoAfetadaRF
	{

				
		#region UpToOperacaoRendaFixaByIdOperacao - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - DetalhePosAfetadaRF_OpRF_FK
		/// </summary>

		[XmlIgnore]
		public OperacaoRendaFixa UpToOperacaoRendaFixaByIdOperacao
		{
			get
			{
				if(this._UpToOperacaoRendaFixaByIdOperacao == null
					&& IdOperacao != null					)
				{
					this._UpToOperacaoRendaFixaByIdOperacao = new OperacaoRendaFixa();
					this._UpToOperacaoRendaFixaByIdOperacao.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToOperacaoRendaFixaByIdOperacao", this._UpToOperacaoRendaFixaByIdOperacao);
					this._UpToOperacaoRendaFixaByIdOperacao.Query.Where(this._UpToOperacaoRendaFixaByIdOperacao.Query.IdOperacao == this.IdOperacao);
					this._UpToOperacaoRendaFixaByIdOperacao.Query.Load();
				}

				return this._UpToOperacaoRendaFixaByIdOperacao;
			}
			
			set
			{
				this.RemovePreSave("UpToOperacaoRendaFixaByIdOperacao");
				

				if(value == null)
				{
					this.IdOperacao = null;
					this._UpToOperacaoRendaFixaByIdOperacao = null;
				}
				else
				{
					this.IdOperacao = value.IdOperacao;
					this._UpToOperacaoRendaFixaByIdOperacao = value;
					this.SetPreSave("UpToOperacaoRendaFixaByIdOperacao", this._UpToOperacaoRendaFixaByIdOperacao);
				}
				
			}
		}
		#endregion
		

				
		#region UpToPosicaoRendaFixaByIdPosicaoAfetada - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - DetalhePosAfetadaRF_PosRF_FK
		/// </summary>

		[XmlIgnore]
		public PosicaoRendaFixa UpToPosicaoRendaFixaByIdPosicaoAfetada
		{
			get
			{
				if(this._UpToPosicaoRendaFixaByIdPosicaoAfetada == null
					&& IdPosicaoAfetada != null					)
				{
					this._UpToPosicaoRendaFixaByIdPosicaoAfetada = new PosicaoRendaFixa();
					this._UpToPosicaoRendaFixaByIdPosicaoAfetada.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToPosicaoRendaFixaByIdPosicaoAfetada", this._UpToPosicaoRendaFixaByIdPosicaoAfetada);
					this._UpToPosicaoRendaFixaByIdPosicaoAfetada.Query.Where(this._UpToPosicaoRendaFixaByIdPosicaoAfetada.Query.IdPosicao == this.IdPosicaoAfetada);
					this._UpToPosicaoRendaFixaByIdPosicaoAfetada.Query.Load();
				}

				return this._UpToPosicaoRendaFixaByIdPosicaoAfetada;
			}
			
			set
			{
				this.RemovePreSave("UpToPosicaoRendaFixaByIdPosicaoAfetada");
				

				if(value == null)
				{
					this.IdPosicaoAfetada = null;
					this._UpToPosicaoRendaFixaByIdPosicaoAfetada = null;
				}
				else
				{
					this.IdPosicaoAfetada = value.IdPosicao;
					this._UpToPosicaoRendaFixaByIdPosicaoAfetada = value;
					this.SetPreSave("UpToPosicaoRendaFixaByIdPosicaoAfetada", this._UpToPosicaoRendaFixaByIdPosicaoAfetada);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToOperacaoRendaFixaByIdOperacao != null)
			{
				this.IdOperacao = this._UpToOperacaoRendaFixaByIdOperacao.IdOperacao;
			}
			if(!this.es.IsDeleted && this._UpToPosicaoRendaFixaByIdPosicaoAfetada != null)
			{
				this.IdPosicaoAfetada = this._UpToPosicaoRendaFixaByIdPosicaoAfetada.IdPosicao;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esDetalhePosicaoAfetadaRFQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return DetalhePosicaoAfetadaRFMetadata.Meta();
			}
		}	
		

		public esQueryItem DataOperacao
		{
			get
			{
				return new esQueryItem(this, DetalhePosicaoAfetadaRFMetadata.ColumnNames.DataOperacao, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdOperacao
		{
			get
			{
				return new esQueryItem(this, DetalhePosicaoAfetadaRFMetadata.ColumnNames.IdOperacao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdPosicaoAfetada
		{
			get
			{
				return new esQueryItem(this, DetalhePosicaoAfetadaRFMetadata.ColumnNames.IdPosicaoAfetada, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, DetalhePosicaoAfetadaRFMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem QtdeMovimentada
		{
			get
			{
				return new esQueryItem(this, DetalhePosicaoAfetadaRFMetadata.ColumnNames.QtdeMovimentada, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem PuOperacao
		{
			get
			{
				return new esQueryItem(this, DetalhePosicaoAfetadaRFMetadata.ColumnNames.PuOperacao, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("DetalhePosicaoAfetadaRFCollection")]
	public partial class DetalhePosicaoAfetadaRFCollection : esDetalhePosicaoAfetadaRFCollection, IEnumerable<DetalhePosicaoAfetadaRF>
	{
		public DetalhePosicaoAfetadaRFCollection()
		{

		}
		
		public static implicit operator List<DetalhePosicaoAfetadaRF>(DetalhePosicaoAfetadaRFCollection coll)
		{
			List<DetalhePosicaoAfetadaRF> list = new List<DetalhePosicaoAfetadaRF>();
			
			foreach (DetalhePosicaoAfetadaRF emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  DetalhePosicaoAfetadaRFMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new DetalhePosicaoAfetadaRFQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new DetalhePosicaoAfetadaRF(row);
		}

		override protected esEntity CreateEntity()
		{
			return new DetalhePosicaoAfetadaRF();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public DetalhePosicaoAfetadaRFQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new DetalhePosicaoAfetadaRFQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(DetalhePosicaoAfetadaRFQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public DetalhePosicaoAfetadaRF AddNew()
		{
			DetalhePosicaoAfetadaRF entity = base.AddNewEntity() as DetalhePosicaoAfetadaRF;
			
			return entity;
		}

		public DetalhePosicaoAfetadaRF FindByPrimaryKey(System.DateTime dataOperacao, System.Int32 idOperacao, System.Int32 idPosicaoAfetada)
		{
			return base.FindByPrimaryKey(dataOperacao, idOperacao, idPosicaoAfetada) as DetalhePosicaoAfetadaRF;
		}


		#region IEnumerable<DetalhePosicaoAfetadaRF> Members

		IEnumerator<DetalhePosicaoAfetadaRF> IEnumerable<DetalhePosicaoAfetadaRF>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as DetalhePosicaoAfetadaRF;
			}
		}

		#endregion
		
		private DetalhePosicaoAfetadaRFQuery query;
	}


	/// <summary>
	/// Encapsulates the 'DetalhePosicaoAfetadaRF' table
	/// </summary>

	[Serializable]
	public partial class DetalhePosicaoAfetadaRF : esDetalhePosicaoAfetadaRF
	{
		public DetalhePosicaoAfetadaRF()
		{

		}
	
		public DetalhePosicaoAfetadaRF(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return DetalhePosicaoAfetadaRFMetadata.Meta();
			}
		}
		
		
		
		override protected esDetalhePosicaoAfetadaRFQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new DetalhePosicaoAfetadaRFQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public DetalhePosicaoAfetadaRFQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new DetalhePosicaoAfetadaRFQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(DetalhePosicaoAfetadaRFQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private DetalhePosicaoAfetadaRFQuery query;
	}



	[Serializable]
	public partial class DetalhePosicaoAfetadaRFQuery : esDetalhePosicaoAfetadaRFQuery
	{
		public DetalhePosicaoAfetadaRFQuery()
		{

		}		
		
		public DetalhePosicaoAfetadaRFQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class DetalhePosicaoAfetadaRFMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected DetalhePosicaoAfetadaRFMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(DetalhePosicaoAfetadaRFMetadata.ColumnNames.DataOperacao, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = DetalhePosicaoAfetadaRFMetadata.PropertyNames.DataOperacao;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DetalhePosicaoAfetadaRFMetadata.ColumnNames.IdOperacao, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = DetalhePosicaoAfetadaRFMetadata.PropertyNames.IdOperacao;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DetalhePosicaoAfetadaRFMetadata.ColumnNames.IdPosicaoAfetada, 2, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = DetalhePosicaoAfetadaRFMetadata.PropertyNames.IdPosicaoAfetada;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DetalhePosicaoAfetadaRFMetadata.ColumnNames.IdCliente, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = DetalhePosicaoAfetadaRFMetadata.PropertyNames.IdCliente;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DetalhePosicaoAfetadaRFMetadata.ColumnNames.QtdeMovimentada, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = DetalhePosicaoAfetadaRFMetadata.PropertyNames.QtdeMovimentada;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(DetalhePosicaoAfetadaRFMetadata.ColumnNames.PuOperacao, 5, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = DetalhePosicaoAfetadaRFMetadata.PropertyNames.PuOperacao;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public DetalhePosicaoAfetadaRFMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string DataOperacao = "DataOperacao";
			 public const string IdOperacao = "IdOperacao";
			 public const string IdPosicaoAfetada = "IdPosicaoAfetada";
			 public const string IdCliente = "IdCliente";
			 public const string QtdeMovimentada = "QtdeMovimentada";
			 public const string PuOperacao = "PuOperacao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string DataOperacao = "DataOperacao";
			 public const string IdOperacao = "IdOperacao";
			 public const string IdPosicaoAfetada = "IdPosicaoAfetada";
			 public const string IdCliente = "IdCliente";
			 public const string QtdeMovimentada = "QtdeMovimentada";
			 public const string PuOperacao = "PuOperacao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(DetalhePosicaoAfetadaRFMetadata))
			{
				if(DetalhePosicaoAfetadaRFMetadata.mapDelegates == null)
				{
					DetalhePosicaoAfetadaRFMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (DetalhePosicaoAfetadaRFMetadata.meta == null)
				{
					DetalhePosicaoAfetadaRFMetadata.meta = new DetalhePosicaoAfetadaRFMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("DataOperacao", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdOperacao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdPosicaoAfetada", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("QtdeMovimentada", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("PuOperacao", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "DetalhePosicaoAfetadaRF";
				meta.Destination = "DetalhePosicaoAfetadaRF";
				
				meta.spInsert = "proc_DetalhePosicaoAfetadaRFInsert";				
				meta.spUpdate = "proc_DetalhePosicaoAfetadaRFUpdate";		
				meta.spDelete = "proc_DetalhePosicaoAfetadaRFDelete";
				meta.spLoadAll = "proc_DetalhePosicaoAfetadaRFLoadAll";
				meta.spLoadByPrimaryKey = "proc_DetalhePosicaoAfetadaRFLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private DetalhePosicaoAfetadaRFMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
