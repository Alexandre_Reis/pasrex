/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 12/12/2014 10:53:29 AM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.RendaFixa
{

	[Serializable]
	abstract public class esCotacaoMercadoFiduciarioCollection : esEntityCollection
	{
		public esCotacaoMercadoFiduciarioCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "CotacaoMercadoFiduciarioCollection";
		}

		#region Query Logic
		protected void InitQuery(esCotacaoMercadoFiduciarioQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esCotacaoMercadoFiduciarioQuery);
		}
		#endregion
		
		virtual public CotacaoMercadoFiduciario DetachEntity(CotacaoMercadoFiduciario entity)
		{
			return base.DetachEntity(entity) as CotacaoMercadoFiduciario;
		}
		
		virtual public CotacaoMercadoFiduciario AttachEntity(CotacaoMercadoFiduciario entity)
		{
			return base.AttachEntity(entity) as CotacaoMercadoFiduciario;
		}
		
		virtual public void Combine(CotacaoMercadoFiduciarioCollection collection)
		{
			base.Combine(collection);
		}
		
		new public CotacaoMercadoFiduciario this[int index]
		{
			get
			{
				return base[index] as CotacaoMercadoFiduciario;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(CotacaoMercadoFiduciario);
		}
	}



	[Serializable]
	abstract public class esCotacaoMercadoFiduciario : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esCotacaoMercadoFiduciarioQuery GetDynamicQuery()
		{
			return null;
		}

		public esCotacaoMercadoFiduciario()
		{

		}

		public esCotacaoMercadoFiduciario(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idTitulo, System.DateTime data)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTitulo, data);
			else
				return LoadByPrimaryKeyStoredProcedure(idTitulo, data);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idTitulo, System.DateTime data)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esCotacaoMercadoFiduciarioQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdTitulo == idTitulo, query.Data == data);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idTitulo, System.DateTime data)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idTitulo, data);
			else
				return LoadByPrimaryKeyStoredProcedure(idTitulo, data);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idTitulo, System.DateTime data)
		{
			esCotacaoMercadoFiduciarioQuery query = this.GetDynamicQuery();
			query.Where(query.IdTitulo == idTitulo, query.Data == data);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idTitulo, System.DateTime data)
		{
			esParameters parms = new esParameters();
			parms.Add("IdTitulo",idTitulo);			parms.Add("Data",data);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdTitulo": this.str.IdTitulo = (string)value; break;							
						case "Data": this.str.Data = (string)value; break;							
						case "Cotacao": this.str.Cotacao = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdTitulo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTitulo = (System.Int32?)value;
							break;
						
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "Cotacao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Cotacao = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to CotacaoMercadoFiduciario.IdTitulo
		/// </summary>
		virtual public System.Int32? IdTitulo
		{
			get
			{
				return base.GetSystemInt32(CotacaoMercadoFiduciarioMetadata.ColumnNames.IdTitulo);
			}
			
			set
			{
				base.SetSystemInt32(CotacaoMercadoFiduciarioMetadata.ColumnNames.IdTitulo, value);
			}
		}
		
		/// <summary>
		/// Maps to CotacaoMercadoFiduciario.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(CotacaoMercadoFiduciarioMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(CotacaoMercadoFiduciarioMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to CotacaoMercadoFiduciario.Cotacao
		/// </summary>
		virtual public System.Decimal? Cotacao
		{
			get
			{
				return base.GetSystemDecimal(CotacaoMercadoFiduciarioMetadata.ColumnNames.Cotacao);
			}
			
			set
			{
				base.SetSystemDecimal(CotacaoMercadoFiduciarioMetadata.ColumnNames.Cotacao, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esCotacaoMercadoFiduciario entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdTitulo
			{
				get
				{
					System.Int32? data = entity.IdTitulo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTitulo = null;
					else entity.IdTitulo = Convert.ToInt32(value);
				}
			}
				
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String Cotacao
			{
				get
				{
					System.Decimal? data = entity.Cotacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Cotacao = null;
					else entity.Cotacao = Convert.ToDecimal(value);
				}
			}
			

			private esCotacaoMercadoFiduciario entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esCotacaoMercadoFiduciarioQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esCotacaoMercadoFiduciario can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class CotacaoMercadoFiduciario : esCotacaoMercadoFiduciario
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esCotacaoMercadoFiduciarioQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return CotacaoMercadoFiduciarioMetadata.Meta();
			}
		}	
		

		public esQueryItem IdTitulo
		{
			get
			{
				return new esQueryItem(this, CotacaoMercadoFiduciarioMetadata.ColumnNames.IdTitulo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, CotacaoMercadoFiduciarioMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Cotacao
		{
			get
			{
				return new esQueryItem(this, CotacaoMercadoFiduciarioMetadata.ColumnNames.Cotacao, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("CotacaoMercadoFiduciarioCollection")]
	public partial class CotacaoMercadoFiduciarioCollection : esCotacaoMercadoFiduciarioCollection, IEnumerable<CotacaoMercadoFiduciario>
	{
		public CotacaoMercadoFiduciarioCollection()
		{

		}
		
		public static implicit operator List<CotacaoMercadoFiduciario>(CotacaoMercadoFiduciarioCollection coll)
		{
			List<CotacaoMercadoFiduciario> list = new List<CotacaoMercadoFiduciario>();
			
			foreach (CotacaoMercadoFiduciario emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  CotacaoMercadoFiduciarioMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CotacaoMercadoFiduciarioQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new CotacaoMercadoFiduciario(row);
		}

		override protected esEntity CreateEntity()
		{
			return new CotacaoMercadoFiduciario();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public CotacaoMercadoFiduciarioQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CotacaoMercadoFiduciarioQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(CotacaoMercadoFiduciarioQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public CotacaoMercadoFiduciario AddNew()
		{
			CotacaoMercadoFiduciario entity = base.AddNewEntity() as CotacaoMercadoFiduciario;
			
			return entity;
		}

		public CotacaoMercadoFiduciario FindByPrimaryKey(System.Int32 idTitulo, System.DateTime data)
		{
			return base.FindByPrimaryKey(idTitulo, data) as CotacaoMercadoFiduciario;
		}


		#region IEnumerable<CotacaoMercadoFiduciario> Members

		IEnumerator<CotacaoMercadoFiduciario> IEnumerable<CotacaoMercadoFiduciario>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as CotacaoMercadoFiduciario;
			}
		}

		#endregion
		
		private CotacaoMercadoFiduciarioQuery query;
	}


	/// <summary>
	/// Encapsulates the 'CotacaoMercadoFiduciario' table
	/// </summary>

	[Serializable]
	public partial class CotacaoMercadoFiduciario : esCotacaoMercadoFiduciario
	{
		public CotacaoMercadoFiduciario()
		{

		}
	
		public CotacaoMercadoFiduciario(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return CotacaoMercadoFiduciarioMetadata.Meta();
			}
		}
		
		
		
		override protected esCotacaoMercadoFiduciarioQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CotacaoMercadoFiduciarioQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public CotacaoMercadoFiduciarioQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CotacaoMercadoFiduciarioQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(CotacaoMercadoFiduciarioQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private CotacaoMercadoFiduciarioQuery query;
	}



	[Serializable]
	public partial class CotacaoMercadoFiduciarioQuery : esCotacaoMercadoFiduciarioQuery
	{
		public CotacaoMercadoFiduciarioQuery()
		{

		}		
		
		public CotacaoMercadoFiduciarioQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class CotacaoMercadoFiduciarioMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected CotacaoMercadoFiduciarioMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(CotacaoMercadoFiduciarioMetadata.ColumnNames.IdTitulo, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CotacaoMercadoFiduciarioMetadata.PropertyNames.IdTitulo;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotacaoMercadoFiduciarioMetadata.ColumnNames.Data, 1, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = CotacaoMercadoFiduciarioMetadata.PropertyNames.Data;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotacaoMercadoFiduciarioMetadata.ColumnNames.Cotacao, 2, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CotacaoMercadoFiduciarioMetadata.PropertyNames.Cotacao;	
			c.NumericPrecision = 16;
			c.NumericScale = 8;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public CotacaoMercadoFiduciarioMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdTitulo = "IdTitulo";
			 public const string Data = "Data";
			 public const string Cotacao = "Cotacao";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdTitulo = "IdTitulo";
			 public const string Data = "Data";
			 public const string Cotacao = "Cotacao";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(CotacaoMercadoFiduciarioMetadata))
			{
				if(CotacaoMercadoFiduciarioMetadata.mapDelegates == null)
				{
					CotacaoMercadoFiduciarioMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (CotacaoMercadoFiduciarioMetadata.meta == null)
				{
					CotacaoMercadoFiduciarioMetadata.meta = new CotacaoMercadoFiduciarioMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdTitulo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("Cotacao", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "CotacaoMercadoFiduciario";
				meta.Destination = "CotacaoMercadoFiduciario";
				
				meta.spInsert = "proc_CotacaoMercadoFiduciarioInsert";				
				meta.spUpdate = "proc_CotacaoMercadoFiduciarioUpdate";		
				meta.spDelete = "proc_CotacaoMercadoFiduciarioDelete";
				meta.spLoadAll = "proc_CotacaoMercadoFiduciarioLoadAll";
				meta.spLoadByPrimaryKey = "proc_CotacaoMercadoFiduciarioLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private CotacaoMercadoFiduciarioMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
