/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 04/12/2014 13:52:48
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	


	
using Financial.Investidor;










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.RendaFixa
{

	[Serializable]
	abstract public class esPosicaoRendaFixaDetalheCollection : esEntityCollection
	{
		public esPosicaoRendaFixaDetalheCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "PosicaoRendaFixaDetalheCollection";
		}

		#region Query Logic
		protected void InitQuery(esPosicaoRendaFixaDetalheQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esPosicaoRendaFixaDetalheQuery);
		}
		#endregion
		
		virtual public PosicaoRendaFixaDetalhe DetachEntity(PosicaoRendaFixaDetalhe entity)
		{
			return base.DetachEntity(entity) as PosicaoRendaFixaDetalhe;
		}
		
		virtual public PosicaoRendaFixaDetalhe AttachEntity(PosicaoRendaFixaDetalhe entity)
		{
			return base.AttachEntity(entity) as PosicaoRendaFixaDetalhe;
		}
		
		virtual public void Combine(PosicaoRendaFixaDetalheCollection collection)
		{
			base.Combine(collection);
		}
		
		new public PosicaoRendaFixaDetalhe this[int index]
		{
			get
			{
				return base[index] as PosicaoRendaFixaDetalhe;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(PosicaoRendaFixaDetalhe);
		}
	}



	[Serializable]
	abstract public class esPosicaoRendaFixaDetalhe : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esPosicaoRendaFixaDetalheQuery GetDynamicQuery()
		{
			return null;
		}

		public esPosicaoRendaFixaDetalhe()
		{

		}

		public esPosicaoRendaFixaDetalhe(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.Int32 idCliente, System.Int32 idPosicao, System.DateTime dataHistorico)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCliente, idPosicao, dataHistorico);
			else
				return LoadByPrimaryKeyStoredProcedure(idCliente, idPosicao, dataHistorico);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.Int32 idCliente, System.Int32 idPosicao, System.DateTime dataHistorico)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esPosicaoRendaFixaDetalheQuery query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.IdCliente == idCliente, query.IdPosicao == idPosicao, query.DataHistorico == dataHistorico);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.Int32 idCliente, System.Int32 idPosicao, System.DateTime dataHistorico)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(idCliente, idPosicao, dataHistorico);
			else
				return LoadByPrimaryKeyStoredProcedure(idCliente, idPosicao, dataHistorico);
		}

		private bool LoadByPrimaryKeyDynamic(System.Int32 idCliente, System.Int32 idPosicao, System.DateTime dataHistorico)
		{
			esPosicaoRendaFixaDetalheQuery query = this.GetDynamicQuery();
			query.Where(query.IdCliente == idCliente, query.IdPosicao == idPosicao, query.DataHistorico == dataHistorico);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.Int32 idCliente, System.Int32 idPosicao, System.DateTime dataHistorico)
		{
			esParameters parms = new esParameters();
			parms.Add("IdCliente",idCliente);			parms.Add("IdPosicao",idPosicao);			parms.Add("DataHistorico",dataHistorico);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "IdCliente": this.str.IdCliente = (string)value; break;							
						case "IdPosicao": this.str.IdPosicao = (string)value; break;							
						case "DataHistorico": this.str.DataHistorico = (string)value; break;							
						case "IdTitulo": this.str.IdTitulo = (string)value; break;							
						case "QuantidadeBloqueada": this.str.QuantidadeBloqueada = (string)value; break;							
						case "Motivo": this.str.Motivo = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "IdCliente":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdCliente = (System.Int32?)value;
							break;
						
						case "IdPosicao":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdPosicao = (System.Int32?)value;
							break;
						
						case "DataHistorico":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataHistorico = (System.DateTime?)value;
							break;
						
						case "IdTitulo":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdTitulo = (System.Int32?)value;
							break;
						
						case "QuantidadeBloqueada":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.QuantidadeBloqueada = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to PosicaoRendaFixaDetalhe.IdCliente
		/// </summary>
		virtual public System.Int32? IdCliente
		{
			get
			{
				return base.GetSystemInt32(PosicaoRendaFixaDetalheMetadata.ColumnNames.IdCliente);
			}
			
			set
			{
				if(base.SetSystemInt32(PosicaoRendaFixaDetalheMetadata.ColumnNames.IdCliente, value))
				{
					this._UpToClienteByIdCliente = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixaDetalhe.IdPosicao
		/// </summary>
		virtual public System.Int32? IdPosicao
		{
			get
			{
				return base.GetSystemInt32(PosicaoRendaFixaDetalheMetadata.ColumnNames.IdPosicao);
			}
			
			set
			{
				base.SetSystemInt32(PosicaoRendaFixaDetalheMetadata.ColumnNames.IdPosicao, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixaDetalhe.DataHistorico
		/// </summary>
		virtual public System.DateTime? DataHistorico
		{
			get
			{
				return base.GetSystemDateTime(PosicaoRendaFixaDetalheMetadata.ColumnNames.DataHistorico);
			}
			
			set
			{
				base.SetSystemDateTime(PosicaoRendaFixaDetalheMetadata.ColumnNames.DataHistorico, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixaDetalhe.IdTitulo
		/// </summary>
		virtual public System.Int32? IdTitulo
		{
			get
			{
				return base.GetSystemInt32(PosicaoRendaFixaDetalheMetadata.ColumnNames.IdTitulo);
			}
			
			set
			{
				if(base.SetSystemInt32(PosicaoRendaFixaDetalheMetadata.ColumnNames.IdTitulo, value))
				{
					this._UpToTituloRendaFixaByIdTitulo = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixaDetalhe.QuantidadeBloqueada
		/// </summary>
		virtual public System.Decimal? QuantidadeBloqueada
		{
			get
			{
				return base.GetSystemDecimal(PosicaoRendaFixaDetalheMetadata.ColumnNames.QuantidadeBloqueada);
			}
			
			set
			{
				base.SetSystemDecimal(PosicaoRendaFixaDetalheMetadata.ColumnNames.QuantidadeBloqueada, value);
			}
		}
		
		/// <summary>
		/// Maps to PosicaoRendaFixaDetalhe.Motivo
		/// </summary>
		virtual public System.String Motivo
		{
			get
			{
				return base.GetSystemString(PosicaoRendaFixaDetalheMetadata.ColumnNames.Motivo);
			}
			
			set
			{
				base.SetSystemString(PosicaoRendaFixaDetalheMetadata.ColumnNames.Motivo, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected Cliente _UpToClienteByIdCliente;
		[CLSCompliant(false)]
		internal protected TituloRendaFixa _UpToTituloRendaFixaByIdTitulo;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esPosicaoRendaFixaDetalhe entity)
			{
				this.entity = entity;
			}
			
	
			public System.String IdCliente
			{
				get
				{
					System.Int32? data = entity.IdCliente;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdCliente = null;
					else entity.IdCliente = Convert.ToInt32(value);
				}
			}
				
			public System.String IdPosicao
			{
				get
				{
					System.Int32? data = entity.IdPosicao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdPosicao = null;
					else entity.IdPosicao = Convert.ToInt32(value);
				}
			}
				
			public System.String DataHistorico
			{
				get
				{
					System.DateTime? data = entity.DataHistorico;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataHistorico = null;
					else entity.DataHistorico = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdTitulo
			{
				get
				{
					System.Int32? data = entity.IdTitulo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdTitulo = null;
					else entity.IdTitulo = Convert.ToInt32(value);
				}
			}
				
			public System.String QuantidadeBloqueada
			{
				get
				{
					System.Decimal? data = entity.QuantidadeBloqueada;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.QuantidadeBloqueada = null;
					else entity.QuantidadeBloqueada = Convert.ToDecimal(value);
				}
			}
				
			public System.String Motivo
			{
				get
				{
					System.String data = entity.Motivo;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Motivo = null;
					else entity.Motivo = Convert.ToString(value);
				}
			}
			

			private esPosicaoRendaFixaDetalhe entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esPosicaoRendaFixaDetalheQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esPosicaoRendaFixaDetalhe can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class PosicaoRendaFixaDetalhe : esPosicaoRendaFixaDetalhe
	{

				
		#region UpToClienteByIdCliente - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - Cliente_PosicaoRendaFixaDetalhe_FK1
		/// </summary>

		[XmlIgnore]
		public Cliente UpToClienteByIdCliente
		{
			get
			{
				if(this._UpToClienteByIdCliente == null
					&& IdCliente != null					)
				{
					this._UpToClienteByIdCliente = new Cliente();
					this._UpToClienteByIdCliente.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
					this._UpToClienteByIdCliente.Query.Where(this._UpToClienteByIdCliente.Query.IdCliente == this.IdCliente);
					this._UpToClienteByIdCliente.Query.Load();
				}

				return this._UpToClienteByIdCliente;
			}
			
			set
			{
				this.RemovePreSave("UpToClienteByIdCliente");
				

				if(value == null)
				{
					this.IdCliente = null;
					this._UpToClienteByIdCliente = null;
				}
				else
				{
					this.IdCliente = value.IdCliente;
					this._UpToClienteByIdCliente = value;
					this.SetPreSave("UpToClienteByIdCliente", this._UpToClienteByIdCliente);
				}
				
			}
		}
		#endregion
		

				
		#region UpToTituloRendaFixaByIdTitulo - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - TituloRendaFixa_PosicaoRendaFixaDetalhe_FK1
		/// </summary>

		[XmlIgnore]
		public TituloRendaFixa UpToTituloRendaFixaByIdTitulo
		{
			get
			{
				if(this._UpToTituloRendaFixaByIdTitulo == null
					&& IdTitulo != null					)
				{
					this._UpToTituloRendaFixaByIdTitulo = new TituloRendaFixa();
					this._UpToTituloRendaFixaByIdTitulo.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToTituloRendaFixaByIdTitulo", this._UpToTituloRendaFixaByIdTitulo);
					this._UpToTituloRendaFixaByIdTitulo.Query.Where(this._UpToTituloRendaFixaByIdTitulo.Query.IdTitulo == this.IdTitulo);
					this._UpToTituloRendaFixaByIdTitulo.Query.Load();
				}

				return this._UpToTituloRendaFixaByIdTitulo;
			}
			
			set
			{
				this.RemovePreSave("UpToTituloRendaFixaByIdTitulo");
				

				if(value == null)
				{
					this.IdTitulo = null;
					this._UpToTituloRendaFixaByIdTitulo = null;
				}
				else
				{
					this.IdTitulo = value.IdTitulo;
					this._UpToTituloRendaFixaByIdTitulo = value;
					this.SetPreSave("UpToTituloRendaFixaByIdTitulo", this._UpToTituloRendaFixaByIdTitulo);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToTituloRendaFixaByIdTitulo != null)
			{
				this.IdTitulo = this._UpToTituloRendaFixaByIdTitulo.IdTitulo;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esPosicaoRendaFixaDetalheQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return PosicaoRendaFixaDetalheMetadata.Meta();
			}
		}	
		

		public esQueryItem IdCliente
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaDetalheMetadata.ColumnNames.IdCliente, esSystemType.Int32);
			}
		} 
		
		public esQueryItem IdPosicao
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaDetalheMetadata.ColumnNames.IdPosicao, esSystemType.Int32);
			}
		} 
		
		public esQueryItem DataHistorico
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaDetalheMetadata.ColumnNames.DataHistorico, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdTitulo
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaDetalheMetadata.ColumnNames.IdTitulo, esSystemType.Int32);
			}
		} 
		
		public esQueryItem QuantidadeBloqueada
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaDetalheMetadata.ColumnNames.QuantidadeBloqueada, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem Motivo
		{
			get
			{
				return new esQueryItem(this, PosicaoRendaFixaDetalheMetadata.ColumnNames.Motivo, esSystemType.String);
			}
		} 
		
	}



	[Serializable]
	[XmlType("PosicaoRendaFixaDetalheCollection")]
	public partial class PosicaoRendaFixaDetalheCollection : esPosicaoRendaFixaDetalheCollection, IEnumerable<PosicaoRendaFixaDetalhe>
	{
		public PosicaoRendaFixaDetalheCollection()
		{

		}
		
		public static implicit operator List<PosicaoRendaFixaDetalhe>(PosicaoRendaFixaDetalheCollection coll)
		{
			List<PosicaoRendaFixaDetalhe> list = new List<PosicaoRendaFixaDetalhe>();
			
			foreach (PosicaoRendaFixaDetalhe emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  PosicaoRendaFixaDetalheMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PosicaoRendaFixaDetalheQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new PosicaoRendaFixaDetalhe(row);
		}

		override protected esEntity CreateEntity()
		{
			return new PosicaoRendaFixaDetalhe();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public PosicaoRendaFixaDetalheQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PosicaoRendaFixaDetalheQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(PosicaoRendaFixaDetalheQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public PosicaoRendaFixaDetalhe AddNew()
		{
			PosicaoRendaFixaDetalhe entity = base.AddNewEntity() as PosicaoRendaFixaDetalhe;
			
			return entity;
		}

		public PosicaoRendaFixaDetalhe FindByPrimaryKey(System.Int32 idCliente, System.Int32 idPosicao, System.DateTime dataHistorico)
		{
			return base.FindByPrimaryKey(idCliente, idPosicao, dataHistorico) as PosicaoRendaFixaDetalhe;
		}


		#region IEnumerable<PosicaoRendaFixaDetalhe> Members

		IEnumerator<PosicaoRendaFixaDetalhe> IEnumerable<PosicaoRendaFixaDetalhe>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as PosicaoRendaFixaDetalhe;
			}
		}

		#endregion
		
		private PosicaoRendaFixaDetalheQuery query;
	}


	/// <summary>
	/// Encapsulates the 'PosicaoRendaFixaDetalhe' table
	/// </summary>

	[Serializable]
	public partial class PosicaoRendaFixaDetalhe : esPosicaoRendaFixaDetalhe
	{
		public PosicaoRendaFixaDetalhe()
		{

		}
	
		public PosicaoRendaFixaDetalhe(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return PosicaoRendaFixaDetalheMetadata.Meta();
			}
		}
		
		
		
		override protected esPosicaoRendaFixaDetalheQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new PosicaoRendaFixaDetalheQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public PosicaoRendaFixaDetalheQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new PosicaoRendaFixaDetalheQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(PosicaoRendaFixaDetalheQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private PosicaoRendaFixaDetalheQuery query;
	}



	[Serializable]
	public partial class PosicaoRendaFixaDetalheQuery : esPosicaoRendaFixaDetalheQuery
	{
		public PosicaoRendaFixaDetalheQuery()
		{

		}		
		
		public PosicaoRendaFixaDetalheQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class PosicaoRendaFixaDetalheMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected PosicaoRendaFixaDetalheMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(PosicaoRendaFixaDetalheMetadata.ColumnNames.IdCliente, 0, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoRendaFixaDetalheMetadata.PropertyNames.IdCliente;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaDetalheMetadata.ColumnNames.IdPosicao, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoRendaFixaDetalheMetadata.PropertyNames.IdPosicao;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaDetalheMetadata.ColumnNames.DataHistorico, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = PosicaoRendaFixaDetalheMetadata.PropertyNames.DataHistorico;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaDetalheMetadata.ColumnNames.IdTitulo, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = PosicaoRendaFixaDetalheMetadata.PropertyNames.IdTitulo;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaDetalheMetadata.ColumnNames.QuantidadeBloqueada, 4, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = PosicaoRendaFixaDetalheMetadata.PropertyNames.QuantidadeBloqueada;	
			c.NumericPrecision = 26;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(PosicaoRendaFixaDetalheMetadata.ColumnNames.Motivo, 5, typeof(System.String), esSystemType.String);
			c.PropertyName = PosicaoRendaFixaDetalheMetadata.PropertyNames.Motivo;
			c.CharacterMaxLength = 500;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public PosicaoRendaFixaDetalheMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string IdCliente = "IdCliente";
			 public const string IdPosicao = "IdPosicao";
			 public const string DataHistorico = "DataHistorico";
			 public const string IdTitulo = "IdTitulo";
			 public const string QuantidadeBloqueada = "QuantidadeBloqueada";
			 public const string Motivo = "Motivo";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string IdCliente = "IdCliente";
			 public const string IdPosicao = "IdPosicao";
			 public const string DataHistorico = "DataHistorico";
			 public const string IdTitulo = "IdTitulo";
			 public const string QuantidadeBloqueada = "QuantidadeBloqueada";
			 public const string Motivo = "Motivo";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(PosicaoRendaFixaDetalheMetadata))
			{
				if(PosicaoRendaFixaDetalheMetadata.mapDelegates == null)
				{
					PosicaoRendaFixaDetalheMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (PosicaoRendaFixaDetalheMetadata.meta == null)
				{
					PosicaoRendaFixaDetalheMetadata.meta = new PosicaoRendaFixaDetalheMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("IdCliente", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("IdPosicao", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("DataHistorico", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdTitulo", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("QuantidadeBloqueada", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("Motivo", new esTypeMap("varchar", "System.String"));			
				
				
				
				meta.Source = "PosicaoRendaFixaDetalhe";
				meta.Destination = "PosicaoRendaFixaDetalhe";
				
				meta.spInsert = "proc_PosicaoRendaFixaDetalheInsert";				
				meta.spUpdate = "proc_PosicaoRendaFixaDetalheUpdate";		
				meta.spDelete = "proc_PosicaoRendaFixaDetalheDelete";
				meta.spLoadAll = "proc_PosicaoRendaFixaDetalheLoadAll";
				meta.spLoadByPrimaryKey = "proc_PosicaoRendaFixaDetalheLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private PosicaoRendaFixaDetalheMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
