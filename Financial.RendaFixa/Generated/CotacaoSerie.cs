/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 08/05/2015 12:56:46
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;



namespace Financial.RendaFixa
{

	[Serializable]
	abstract public class esCotacaoSerieCollection : esEntityCollection
	{
		public esCotacaoSerieCollection()
		{

		}

		protected override string GetCollectionName()
		{
			return "CotacaoSerieCollection";
		}

		#region Query Logic
		protected void InitQuery(esCotacaoSerieQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esCotacaoSerieQuery);
		}
		#endregion
		
		virtual public CotacaoSerie DetachEntity(CotacaoSerie entity)
		{
			return base.DetachEntity(entity) as CotacaoSerie;
		}
		
		virtual public CotacaoSerie AttachEntity(CotacaoSerie entity)
		{
			return base.AttachEntity(entity) as CotacaoSerie;
		}
		
		virtual public void Combine(CotacaoSerieCollection collection)
		{
			base.Combine(collection);
		}
		
		new public CotacaoSerie this[int index]
		{
			get
			{
				return base[index] as CotacaoSerie;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(CotacaoSerie);
		}
	}



	[Serializable]
	abstract public class esCotacaoSerie : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esCotacaoSerieQuery GetDynamicQuery()
		{
			return null;
		}

		public esCotacaoSerie()
		{

		}

		public esCotacaoSerie(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime data, System.Int32 idSerie)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(data, idSerie);
			else
				return LoadByPrimaryKeyStoredProcedure(data, idSerie);
		}

		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime data, System.Int32 idSerie)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(data, idSerie);
			else
				return LoadByPrimaryKeyStoredProcedure(data, idSerie);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime data, System.Int32 idSerie)
		{
			esCotacaoSerieQuery query = this.GetDynamicQuery();
			query.Where(query.Data == data, query.IdSerie == idSerie);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime data, System.Int32 idSerie)
		{
			esParameters parms = new esParameters();
			parms.Add("Data",data);			parms.Add("IdSerie",idSerie);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "Data": this.str.Data = (string)value; break;							
						case "IdSerie": this.str.IdSerie = (string)value; break;							
						case "Cotacao": this.str.Cotacao = (string)value; break;							
						case "DigitadoImportado": this.str.DigitadoImportado = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "Data":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.Data = (System.DateTime?)value;
							break;
						
						case "IdSerie":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.IdSerie = (System.Int32?)value;
							break;
						
						case "Cotacao":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Cotacao = (System.Decimal?)value;
							break;
						
						case "DigitadoImportado":
						
							if (value == null || value.GetType().ToString() == "System.Int32")
								this.DigitadoImportado = (System.Int32?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to CotacaoSerie.Data
		/// </summary>
		virtual public System.DateTime? Data
		{
			get
			{
				return base.GetSystemDateTime(CotacaoSerieMetadata.ColumnNames.Data);
			}
			
			set
			{
				base.SetSystemDateTime(CotacaoSerieMetadata.ColumnNames.Data, value);
			}
		}
		
		/// <summary>
		/// Maps to CotacaoSerie.IdSerie
		/// </summary>
		virtual public System.Int32? IdSerie
		{
			get
			{
				return base.GetSystemInt32(CotacaoSerieMetadata.ColumnNames.IdSerie);
			}
			
			set
			{
				if(base.SetSystemInt32(CotacaoSerieMetadata.ColumnNames.IdSerie, value))
				{
					this._UpToSerieRendaFixaByIdSerie = null;
				}
			}
		}
		
		/// <summary>
		/// Maps to CotacaoSerie.Cotacao
		/// </summary>
		virtual public System.Decimal? Cotacao
		{
			get
			{
				return base.GetSystemDecimal(CotacaoSerieMetadata.ColumnNames.Cotacao);
			}
			
			set
			{
				base.SetSystemDecimal(CotacaoSerieMetadata.ColumnNames.Cotacao, value);
			}
		}
		
		/// <summary>
		/// Maps to CotacaoSerie.DigitadoImportado
		/// </summary>
		virtual public System.Int32? DigitadoImportado
		{
			get
			{
				return base.GetSystemInt32(CotacaoSerieMetadata.ColumnNames.DigitadoImportado);
			}
			
			set
			{
				base.SetSystemInt32(CotacaoSerieMetadata.ColumnNames.DigitadoImportado, value);
			}
		}
		
		[CLSCompliant(false)]
		internal protected SerieRendaFixa _UpToSerieRendaFixaByIdSerie;
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esCotacaoSerie entity)
			{
				this.entity = entity;
			}
			
	
			public System.String Data
			{
				get
				{
					System.DateTime? data = entity.Data;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Data = null;
					else entity.Data = Convert.ToDateTime(value);
				}
			}
				
			public System.String IdSerie
			{
				get
				{
					System.Int32? data = entity.IdSerie;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.IdSerie = null;
					else entity.IdSerie = Convert.ToInt32(value);
				}
			}
				
			public System.String Cotacao
			{
				get
				{
					System.Decimal? data = entity.Cotacao;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Cotacao = null;
					else entity.Cotacao = Convert.ToDecimal(value);
				}
			}
				
			public System.String DigitadoImportado
			{
				get
				{
					System.Int32? data = entity.DigitadoImportado;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DigitadoImportado = null;
					else entity.DigitadoImportado = Convert.ToInt32(value);
				}
			}
			

			private esCotacaoSerie entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esCotacaoSerieQuery query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esCotacaoSerie can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class CotacaoSerie : esCotacaoSerie
	{

				
		#region UpToSerieRendaFixaByIdSerie - Many To One
		/// <summary>
		/// Many to One
		/// Foreign Key Name - SerieRendaFixa_CotacaoSerie_FK1
		/// </summary>

		[XmlIgnore]
		public SerieRendaFixa UpToSerieRendaFixaByIdSerie
		{
			get
			{
				if(this._UpToSerieRendaFixaByIdSerie == null
					&& IdSerie != null					)
				{
					this._UpToSerieRendaFixaByIdSerie = new SerieRendaFixa();
					this._UpToSerieRendaFixaByIdSerie.es.Connection.Name = this.es.Connection.Name;
					this.SetPreSave("UpToSerieRendaFixaByIdSerie", this._UpToSerieRendaFixaByIdSerie);
					this._UpToSerieRendaFixaByIdSerie.Query.Where(this._UpToSerieRendaFixaByIdSerie.Query.IdSerie == this.IdSerie);
					this._UpToSerieRendaFixaByIdSerie.Query.Load();
				}

				return this._UpToSerieRendaFixaByIdSerie;
			}
			
			set
			{
				this.RemovePreSave("UpToSerieRendaFixaByIdSerie");
				

				if(value == null)
				{
					this.IdSerie = null;
					this._UpToSerieRendaFixaByIdSerie = null;
				}
				else
				{
					this.IdSerie = value.IdSerie;
					this._UpToSerieRendaFixaByIdSerie = value;
					this.SetPreSave("UpToSerieRendaFixaByIdSerie", this._UpToSerieRendaFixaByIdSerie);
				}
				
			}
		}
		#endregion
		

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
			if(!this.es.IsDeleted && this._UpToSerieRendaFixaByIdSerie != null)
			{
				this.IdSerie = this._UpToSerieRendaFixaByIdSerie.IdSerie;
			}
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esCotacaoSerieQuery : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return CotacaoSerieMetadata.Meta();
			}
		}	
		

		public esQueryItem Data
		{
			get
			{
				return new esQueryItem(this, CotacaoSerieMetadata.ColumnNames.Data, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem IdSerie
		{
			get
			{
				return new esQueryItem(this, CotacaoSerieMetadata.ColumnNames.IdSerie, esSystemType.Int32);
			}
		} 
		
		public esQueryItem Cotacao
		{
			get
			{
				return new esQueryItem(this, CotacaoSerieMetadata.ColumnNames.Cotacao, esSystemType.Decimal);
			}
		} 
		
		public esQueryItem DigitadoImportado
		{
			get
			{
				return new esQueryItem(this, CotacaoSerieMetadata.ColumnNames.DigitadoImportado, esSystemType.Int32);
			}
		} 
		
	}



	[Serializable]
	[XmlType("CotacaoSerieCollection")]
	public partial class CotacaoSerieCollection : esCotacaoSerieCollection, IEnumerable<CotacaoSerie>
	{
		public CotacaoSerieCollection()
		{

		}
		
		public static implicit operator List<CotacaoSerie>(CotacaoSerieCollection coll)
		{
			List<CotacaoSerie> list = new List<CotacaoSerie>();
			
			foreach (CotacaoSerie emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  CotacaoSerieMetadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CotacaoSerieQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new CotacaoSerie(row);
		}

		override protected esEntity CreateEntity()
		{
			return new CotacaoSerie();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public CotacaoSerieQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CotacaoSerieQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(CotacaoSerieQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public CotacaoSerie AddNew()
		{
			CotacaoSerie entity = base.AddNewEntity() as CotacaoSerie;
			
			return entity;
		}

		public CotacaoSerie FindByPrimaryKey(System.DateTime data, System.Int32 idSerie)
		{
			return base.FindByPrimaryKey(data, idSerie) as CotacaoSerie;
		}


		#region IEnumerable<CotacaoSerie> Members

		IEnumerator<CotacaoSerie> IEnumerable<CotacaoSerie>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as CotacaoSerie;
			}
		}

		#endregion
		
		private CotacaoSerieQuery query;
	}


	/// <summary>
	/// Encapsulates the 'CotacaoSerie' table
	/// </summary>

	[Serializable]
	public partial class CotacaoSerie : esCotacaoSerie
	{
		public CotacaoSerie()
		{

		}
	
		public CotacaoSerie(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return CotacaoSerieMetadata.Meta();
			}
		}
		
		
		
		override protected esCotacaoSerieQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CotacaoSerieQuery();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public CotacaoSerieQuery Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CotacaoSerieQuery();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(CotacaoSerieQuery query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private CotacaoSerieQuery query;
	}



	[Serializable]
	public partial class CotacaoSerieQuery : esCotacaoSerieQuery
	{
		public CotacaoSerieQuery()
		{

		}		
		
		public CotacaoSerieQuery(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class CotacaoSerieMetadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected CotacaoSerieMetadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(CotacaoSerieMetadata.ColumnNames.Data, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = CotacaoSerieMetadata.PropertyNames.Data;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotacaoSerieMetadata.ColumnNames.IdSerie, 1, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CotacaoSerieMetadata.PropertyNames.IdSerie;
			c.IsInPrimaryKey = true;	
			c.NumericPrecision = 10;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotacaoSerieMetadata.ColumnNames.Cotacao, 2, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CotacaoSerieMetadata.PropertyNames.Cotacao;	
			c.NumericPrecision = 28;
			c.NumericScale = 12;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotacaoSerieMetadata.ColumnNames.DigitadoImportado, 3, typeof(System.Int32), esSystemType.Int32);
			c.PropertyName = CotacaoSerieMetadata.PropertyNames.DigitadoImportado;	
			c.NumericPrecision = 10;
			c.HasDefault = true;
			c.Default = @"('2')";
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public CotacaoSerieMetadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string Data = "Data";
			 public const string IdSerie = "IdSerie";
			 public const string Cotacao = "Cotacao";
			 public const string DigitadoImportado = "DigitadoImportado";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string Data = "Data";
			 public const string IdSerie = "IdSerie";
			 public const string Cotacao = "Cotacao";
			 public const string DigitadoImportado = "DigitadoImportado";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(CotacaoSerieMetadata))
			{
				if(CotacaoSerieMetadata.mapDelegates == null)
				{
					CotacaoSerieMetadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (CotacaoSerieMetadata.meta == null)
				{
					CotacaoSerieMetadata.meta = new CotacaoSerieMetadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("Data", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("IdSerie", new esTypeMap("int", "System.Int32"));
				meta.AddTypeMap("Cotacao", new esTypeMap("decimal", "System.Decimal"));
				meta.AddTypeMap("DigitadoImportado", new esTypeMap("int", "System.Int32"));			
				
				
				
				meta.Source = "CotacaoSerie";
				meta.Destination = "CotacaoSerie";
				
				meta.spInsert = "proc_CotacaoSerieInsert";				
				meta.spUpdate = "proc_CotacaoSerieUpdate";		
				meta.spDelete = "proc_CotacaoSerieDelete";
				meta.spLoadAll = "proc_CotacaoSerieLoadAll";
				meta.spLoadByPrimaryKey = "proc_CotacaoSerieLoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private CotacaoSerieMetadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
