/*
===============================================================================
                    EntitySpaces 2009 by EntitySpaces, LLC
             Persistence Layer and Business Objects for Microsoft .NET
             EntitySpaces(TM) is a legal trademark of EntitySpaces, LLC
                          http://www.entityspaces.net
===============================================================================
EntitySpaces Version : 2009.0.0103.0
EntitySpaces Driver  : SQL
Date Generated       : 12/12/2014 10:53:29 AM
===============================================================================
*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;


using EntitySpaces.Interfaces;
using EntitySpaces.Core;





































		



			
					


		

		
		
		

		

























		






	

















				
		
		




		








		
				
				












	

















					
								
											
	










		





		




				
				








				




		

		
		
		
		
		





namespace Financial.RendaFixa
{

	[Serializable]
	abstract public class esCotacaoResolucao238Collection : esEntityCollection
	{
		public esCotacaoResolucao238Collection()
		{

		}

		protected override string GetCollectionName()
		{
			return "CotacaoResolucao238Collection";
		}

		#region Query Logic
		protected void InitQuery(esCotacaoResolucao238Query query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntityCollection)this).Connection;
		}

		protected bool OnQueryLoaded(DataTable table)
		{
			this.PopulateCollection(table);
			return (this.RowCount > 0) ? true : false;
		}
		
		protected override void HookupQuery(esDynamicQuery query)
		{
			this.InitQuery(query as esCotacaoResolucao238Query);
		}
		#endregion
		
		virtual public CotacaoResolucao238 DetachEntity(CotacaoResolucao238 entity)
		{
			return base.DetachEntity(entity) as CotacaoResolucao238;
		}
		
		virtual public CotacaoResolucao238 AttachEntity(CotacaoResolucao238 entity)
		{
			return base.AttachEntity(entity) as CotacaoResolucao238;
		}
		
		virtual public void Combine(CotacaoResolucao238Collection collection)
		{
			base.Combine(collection);
		}
		
		new public CotacaoResolucao238 this[int index]
		{
			get
			{
				return base[index] as CotacaoResolucao238;
			}
		}

		public override Type GetEntityType()
		{
			return typeof(CotacaoResolucao238);
		}
	}



	[Serializable]
	abstract public class esCotacaoResolucao238 : esEntity
	{
		/// <summary>
		/// Used internally by the entity's DynamicQuery mechanism.
		/// </summary>
		virtual protected esCotacaoResolucao238Query GetDynamicQuery()
		{
			return null;
		}

		public esCotacaoResolucao238()
		{

		}

		public esCotacaoResolucao238(DataRow row)
			: base(row)
		{

		}
		
		#region LoadByPrimaryKey
		public virtual bool LoadByPrimaryKey(System.DateTime dataReferencia, System.String codigoSELIC, System.DateTime dataVencimento)
		{
			if(this.es.Connection.SqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataReferencia, codigoSELIC, dataVencimento);
			else
				return LoadByPrimaryKeyStoredProcedure(dataReferencia, codigoSELIC, dataVencimento);
		}

		/// <summary>
		/// Loads an entity by primary key
		/// </summary>
		/// <remarks>
		/// EntitySpaces requires primary keys be defined on all tables.
		/// If a table does not have a primary key set,
		/// this method will not compile.
		/// Does not support sqlAcessType. It only works with DynamicQuery
		/// </remarks>
		/// <param name="fieldsToReturn">Fields desired</param>
		public virtual bool LoadByPrimaryKey(List<esQueryItem> fieldsToReturn, System.DateTime dataReferencia, System.String codigoSELIC, System.DateTime dataVencimento)
		{
			esQueryItem[] fields = fieldsToReturn.ToArray();			
			esCotacaoResolucao238Query query = this.GetDynamicQuery();
			query
			    .Select(fields)
			    .Where(query.DataReferencia == dataReferencia, query.CodigoSELIC == codigoSELIC, query.DataVencimento == dataVencimento);
		
			return query.Load();
		}
		
		public virtual bool LoadByPrimaryKey(esSqlAccessType sqlAccessType, System.DateTime dataReferencia, System.String codigoSELIC, System.DateTime dataVencimento)
		{
			if (sqlAccessType == esSqlAccessType.DynamicSQL)
				return LoadByPrimaryKeyDynamic(dataReferencia, codigoSELIC, dataVencimento);
			else
				return LoadByPrimaryKeyStoredProcedure(dataReferencia, codigoSELIC, dataVencimento);
		}

		private bool LoadByPrimaryKeyDynamic(System.DateTime dataReferencia, System.String codigoSELIC, System.DateTime dataVencimento)
		{
			esCotacaoResolucao238Query query = this.GetDynamicQuery();
			query.Where(query.DataReferencia == dataReferencia, query.CodigoSELIC == codigoSELIC, query.DataVencimento == dataVencimento);
			return query.Load();
		}

		private bool LoadByPrimaryKeyStoredProcedure(System.DateTime dataReferencia, System.String codigoSELIC, System.DateTime dataVencimento)
		{
			esParameters parms = new esParameters();
			parms.Add("DataReferencia",dataReferencia);			parms.Add("CodigoSELIC",codigoSELIC);			parms.Add("DataVencimento",dataVencimento);
			return this.Load(esQueryType.StoredProcedure, this.es.spLoadByPrimaryKey, parms);
		}
		#endregion
		
		
		
		#region Properties
		
		
		public override void SetProperties(IDictionary values)
		{
			foreach (string propertyName in values.Keys)
			{
				this.SetProperty(propertyName, values[propertyName]);
			}
		}

		public override void SetProperty(string name, object value)
		{
			if(this.Row == null) this.AddNew();
			
			esColumnMetadata col = this.Meta.Columns.FindByPropertyName(name);
			if (col != null)
			{
				if(value == null || value.GetType().ToString() == "System.String")
				{				
					// Use the strongly typed property
					switch (name)
					{							
						case "DataReferencia": this.str.DataReferencia = (string)value; break;							
						case "CodigoSELIC": this.str.CodigoSELIC = (string)value; break;							
						case "DataVencimento": this.str.DataVencimento = (string)value; break;							
						case "Pu": this.str.Pu = (string)value; break;
					}
				}
				else
				{
					switch (name)
					{	
						case "DataReferencia":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataReferencia = (System.DateTime?)value;
							break;
						
						case "DataVencimento":
						
							if (value == null || value.GetType().ToString() == "System.DateTime")
								this.DataVencimento = (System.DateTime?)value;
							break;
						
						case "Pu":
						
							if (value == null || value.GetType().ToString() == "System.Decimal")
								this.Pu = (System.Decimal?)value;
							break;
					

						default:
							break;
					}
				}
			}
			else if(this.Row.Table.Columns.Contains(name))
			{
				this.Row[name] = value;
			}
			else
			{
				throw new Exception("SetProperty Error: '" + name + "' not found");
			}
		}
		
		
		/// <summary>
		/// Maps to CotacaoResolucao238.DataReferencia
		/// </summary>
		virtual public System.DateTime? DataReferencia
		{
			get
			{
				return base.GetSystemDateTime(CotacaoResolucao238Metadata.ColumnNames.DataReferencia);
			}
			
			set
			{
				base.SetSystemDateTime(CotacaoResolucao238Metadata.ColumnNames.DataReferencia, value);
			}
		}
		
		/// <summary>
		/// Maps to CotacaoResolucao238.CodigoSELIC
		/// </summary>
		virtual public System.String CodigoSELIC
		{
			get
			{
				return base.GetSystemString(CotacaoResolucao238Metadata.ColumnNames.CodigoSELIC);
			}
			
			set
			{
				base.SetSystemString(CotacaoResolucao238Metadata.ColumnNames.CodigoSELIC, value);
			}
		}
		
		/// <summary>
		/// Maps to CotacaoResolucao238.DataVencimento
		/// </summary>
		virtual public System.DateTime? DataVencimento
		{
			get
			{
				return base.GetSystemDateTime(CotacaoResolucao238Metadata.ColumnNames.DataVencimento);
			}
			
			set
			{
				base.SetSystemDateTime(CotacaoResolucao238Metadata.ColumnNames.DataVencimento, value);
			}
		}
		
		/// <summary>
		/// Maps to CotacaoResolucao238.PU
		/// </summary>
		virtual public System.Decimal? Pu
		{
			get
			{
				return base.GetSystemDecimal(CotacaoResolucao238Metadata.ColumnNames.Pu);
			}
			
			set
			{
				base.SetSystemDecimal(CotacaoResolucao238Metadata.ColumnNames.Pu, value);
			}
		}
		
		#endregion	

		#region String Properties


		[BrowsableAttribute( false )]
		public esStrings str
		{
			get
			{
				if (esstrings == null)
				{
					esstrings = new esStrings(this);
				}
				return esstrings;
			}
		}


		[Serializable]
		sealed public class esStrings
		{
			public esStrings(esCotacaoResolucao238 entity)
			{
				this.entity = entity;
			}
			
	
			public System.String DataReferencia
			{
				get
				{
					System.DateTime? data = entity.DataReferencia;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataReferencia = null;
					else entity.DataReferencia = Convert.ToDateTime(value);
				}
			}
				
			public System.String CodigoSELIC
			{
				get
				{
					System.String data = entity.CodigoSELIC;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.CodigoSELIC = null;
					else entity.CodigoSELIC = Convert.ToString(value);
				}
			}
				
			public System.String DataVencimento
			{
				get
				{
					System.DateTime? data = entity.DataVencimento;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.DataVencimento = null;
					else entity.DataVencimento = Convert.ToDateTime(value);
				}
			}
				
			public System.String Pu
			{
				get
				{
					System.Decimal? data = entity.Pu;
					return (data == null) ? String.Empty : Convert.ToString(data);
				}

				set
				{
					if (value == null || value.Length == 0) entity.Pu = null;
					else entity.Pu = Convert.ToDecimal(value);
				}
			}
			

			private esCotacaoResolucao238 entity;
		}
		#endregion

		#region Query Logic
		protected void InitQuery(esCotacaoResolucao238Query query)
		{
			query.OnLoadDelegate = this.OnQueryLoaded;
			query.es.Connection = ((IEntity)this).Connection;
		}

		[System.Diagnostics.DebuggerNonUserCode]
		protected bool OnQueryLoaded(DataTable table)
		{
			bool dataFound = this.PopulateEntity(table);

			if (this.RowCount > 1)
			{
				throw new Exception("esCotacaoResolucao238 can only hold one record of data");
			}

			return dataFound;
		}
		#endregion
		
		[NonSerialized]
		private esStrings esstrings;
	}


	
	public partial class CotacaoResolucao238 : esCotacaoResolucao238
	{

		
		/// <summary>
		/// Used internally by the entity's hierarchical properties.
		/// </summary>
		protected override List<esPropertyDescriptor> GetHierarchicalProperties()
		{
			List<esPropertyDescriptor> props = new List<esPropertyDescriptor>();
			
		
			return props;
		}	
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PreSave.
		/// </summary>
		protected override void ApplyPreSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostSave.
		/// </summary>
		protected override void ApplyPostSaveKeys()
		{
		}
		
		/// <summary>
		/// Used internally for retrieving AutoIncrementing keys
		/// during hierarchical PostOneToOneSave.
		/// </summary>
		protected override void ApplyPostOneSaveKeys()
		{
		}
		
	}



	[Serializable]
	abstract public class esCotacaoResolucao238Query : esDynamicQuery
	{
		override protected IMetadata Meta
		{
			get
			{
				return CotacaoResolucao238Metadata.Meta();
			}
		}	
		

		public esQueryItem DataReferencia
		{
			get
			{
				return new esQueryItem(this, CotacaoResolucao238Metadata.ColumnNames.DataReferencia, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem CodigoSELIC
		{
			get
			{
				return new esQueryItem(this, CotacaoResolucao238Metadata.ColumnNames.CodigoSELIC, esSystemType.String);
			}
		} 
		
		public esQueryItem DataVencimento
		{
			get
			{
				return new esQueryItem(this, CotacaoResolucao238Metadata.ColumnNames.DataVencimento, esSystemType.DateTime);
			}
		} 
		
		public esQueryItem Pu
		{
			get
			{
				return new esQueryItem(this, CotacaoResolucao238Metadata.ColumnNames.Pu, esSystemType.Decimal);
			}
		} 
		
	}



	[Serializable]
	[XmlType("CotacaoResolucao238Collection")]
	public partial class CotacaoResolucao238Collection : esCotacaoResolucao238Collection, IEnumerable<CotacaoResolucao238>
	{
		public CotacaoResolucao238Collection()
		{

		}
		
		public static implicit operator List<CotacaoResolucao238>(CotacaoResolucao238Collection coll)
		{
			List<CotacaoResolucao238> list = new List<CotacaoResolucao238>();
			
			foreach (CotacaoResolucao238 emp in coll)
			{
				list.Add(emp);
			}
			
			return list;
		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return  CotacaoResolucao238Metadata.Meta();
			}
		}
		
		
		
		override protected esDynamicQuery GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CotacaoResolucao238Query();
				this.InitQuery(query);
			}
			return this.query;
		}
		
		override protected esEntity CreateEntityForCollection(DataRow row)
		{
			return new CotacaoResolucao238(row);
		}

		override protected esEntity CreateEntity()
		{
			return new CotacaoResolucao238();
		}
		
		
		#endregion


		[BrowsableAttribute( false )]
		public CotacaoResolucao238Query Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CotacaoResolucao238Query();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}

		public bool Load(CotacaoResolucao238Query query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		public CotacaoResolucao238 AddNew()
		{
			CotacaoResolucao238 entity = base.AddNewEntity() as CotacaoResolucao238;
			
			return entity;
		}

		public CotacaoResolucao238 FindByPrimaryKey(System.DateTime dataReferencia, System.String codigoSELIC, System.DateTime dataVencimento)
		{
			return base.FindByPrimaryKey(dataReferencia, codigoSELIC, dataVencimento) as CotacaoResolucao238;
		}


		#region IEnumerable<CotacaoResolucao238> Members

		IEnumerator<CotacaoResolucao238> IEnumerable<CotacaoResolucao238>.GetEnumerator()
		{
			System.Collections.IEnumerable enumer = this as System.Collections.IEnumerable;
			System.Collections.IEnumerator iterator = enumer.GetEnumerator();

			while(iterator.MoveNext())
			{
				yield return iterator.Current as CotacaoResolucao238;
			}
		}

		#endregion
		
		private CotacaoResolucao238Query query;
	}


	/// <summary>
	/// Encapsulates the 'CotacaoResolucao238' table
	/// </summary>

	[Serializable]
	public partial class CotacaoResolucao238 : esCotacaoResolucao238
	{
		public CotacaoResolucao238()
		{

		}
	
		public CotacaoResolucao238(DataRow row)
			: base(row)
		{

		}
		
		#region Housekeeping methods
		override protected IMetadata Meta
		{
			get
			{
				return CotacaoResolucao238Metadata.Meta();
			}
		}
		
		
		
		override protected esCotacaoResolucao238Query GetDynamicQuery()
		{
			if (this.query == null)
			{
				this.query = new CotacaoResolucao238Query();
				this.InitQuery(query);
			}
			return this.query;
		}
		#endregion
		



		[BrowsableAttribute( false )]
		public CotacaoResolucao238Query Query
		{
			get
			{
				if (this.query == null)
				{
					this.query = new CotacaoResolucao238Query();
					base.InitQuery(this.query);
				}

				return this.query;
			}
		}

		public void QueryReset()
		{
			this.query = null;
		}
		

		public bool Load(CotacaoResolucao238Query query)
		{
			this.query = query;
			base.InitQuery(this.query);
			return this.Query.Load();
		}
		
		private CotacaoResolucao238Query query;
	}



	[Serializable]
	public partial class CotacaoResolucao238Query : esCotacaoResolucao238Query
	{
		public CotacaoResolucao238Query()
		{

		}		
		
		public CotacaoResolucao238Query(string joinAlias)
		{
			this.es.JoinAlias = joinAlias;
		}	
		
			
	}



	[Serializable]
	public partial class CotacaoResolucao238Metadata : esMetadata, IMetadata
	{
		#region Protected Constructor
		protected CotacaoResolucao238Metadata()
		{
			_columns = new esColumnMetadataCollection();
			esColumnMetadata c;

			c = new esColumnMetadata(CotacaoResolucao238Metadata.ColumnNames.DataReferencia, 0, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = CotacaoResolucao238Metadata.PropertyNames.DataReferencia;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotacaoResolucao238Metadata.ColumnNames.CodigoSELIC, 1, typeof(System.String), esSystemType.String);
			c.PropertyName = CotacaoResolucao238Metadata.PropertyNames.CodigoSELIC;
			c.IsInPrimaryKey = true;
			c.CharacterMaxLength = 10;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotacaoResolucao238Metadata.ColumnNames.DataVencimento, 2, typeof(System.DateTime), esSystemType.DateTime);
			c.PropertyName = CotacaoResolucao238Metadata.PropertyNames.DataVencimento;
			c.IsInPrimaryKey = true;
			c.NumericPrecision = 0;
			_columns.Add(c); 
			
				
			c = new esColumnMetadata(CotacaoResolucao238Metadata.ColumnNames.Pu, 3, typeof(System.Decimal), esSystemType.Decimal);
			c.PropertyName = CotacaoResolucao238Metadata.PropertyNames.Pu;	
			c.NumericPrecision = 28;
			c.NumericScale = 16;
			_columns.Add(c); 
			
				
		}
		#endregion
	
		static public CotacaoResolucao238Metadata Meta()
		{
			return meta;
		}	
		
		public Guid DataID
		{
			get { return base._dataID; }
		}	
		
		public bool MultiProviderMode
		{
			get { return false; }
		}		

		public esColumnMetadataCollection Columns
		{
			get	{ return base._columns; }
		}
		
		#region ColumnNames
		public class ColumnNames
		{ 
			 public const string DataReferencia = "DataReferencia";
			 public const string CodigoSELIC = "CodigoSELIC";
			 public const string DataVencimento = "DataVencimento";
			 public const string Pu = "PU";
		}
		#endregion	
		
		#region PropertyNames
		public class PropertyNames
		{ 
			 public const string DataReferencia = "DataReferencia";
			 public const string CodigoSELIC = "CodigoSELIC";
			 public const string DataVencimento = "DataVencimento";
			 public const string Pu = "Pu";
		}
		#endregion	

		public esProviderSpecificMetadata GetProviderMetadata(string mapName)
		{
			MapToMeta mapMethod = mapDelegates[mapName];

			if (mapMethod != null)
				return mapMethod(mapName);
			else
				return null;
		}
		
		#region MAP esDefault
		
		static private int RegisterDelegateesDefault()
		{
			// This is only executed once per the life of the application
			lock (typeof(CotacaoResolucao238Metadata))
			{
				if(CotacaoResolucao238Metadata.mapDelegates == null)
				{
					CotacaoResolucao238Metadata.mapDelegates = new Dictionary<string,MapToMeta>();
				}
				
				if (CotacaoResolucao238Metadata.meta == null)
				{
					CotacaoResolucao238Metadata.meta = new CotacaoResolucao238Metadata();
				}
				
				MapToMeta mapMethod = new MapToMeta(meta.esDefault);
				mapDelegates.Add("esDefault", mapMethod);
				mapMethod("esDefault");
			}
			return 0;
		}			

		private esProviderSpecificMetadata esDefault(string mapName)
		{
			if(!_providerMetadataMaps.ContainsKey(mapName))
			{
				esProviderSpecificMetadata meta = new esProviderSpecificMetadata();
				

				meta.AddTypeMap("DataReferencia", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("CodigoSELIC", new esTypeMap("varchar", "System.String"));
				meta.AddTypeMap("DataVencimento", new esTypeMap("datetime", "System.DateTime"));
				meta.AddTypeMap("PU", new esTypeMap("decimal", "System.Decimal"));			
				
				
				
				meta.Source = "CotacaoResolucao238";
				meta.Destination = "CotacaoResolucao238";
				
				meta.spInsert = "proc_CotacaoResolucao238Insert";				
				meta.spUpdate = "proc_CotacaoResolucao238Update";		
				meta.spDelete = "proc_CotacaoResolucao238Delete";
				meta.spLoadAll = "proc_CotacaoResolucao238LoadAll";
				meta.spLoadByPrimaryKey = "proc_CotacaoResolucao238LoadByPrimaryKey";
				
				this._providerMetadataMaps["esDefault"] = meta;
			}
			
			return this._providerMetadataMaps["esDefault"];
		}

		#endregion

		static private CotacaoResolucao238Metadata meta;
		static protected Dictionary<string, MapToMeta> mapDelegates;
		static private int _esDefault = RegisterDelegateesDefault();
	}
}
