﻿using System;
using System.Collections.Generic;
using System.Text;
using Financial.Util;
using System.Reflection;

namespace Financial.RendaFixa.Enums {

    public enum ClasseRendaFixa
    {
        [StringValue("LFT")]
        LFT = 1,

        [StringValue("LTN")]
        LTN = 2,

        [StringValue("Pré")]
        Pre_Descontado = 10, //qualquer título que desconte o PU nominal pela taxa
        [StringValue("Pré")]
        Pre_Descontado_Fluxo = 12, //Qualquer título com a taxa pré-fixada, com fluxo de eventos
        [StringValue("Pré")]
        Pre_Atualizado = 15, //Qualquer título com a taxa pré-fixada atualizando o valor nominal        

        [StringValue("NTN")]
        NTN = 20, //NTN-C, NTN-B, NTN-F

        [StringValue("Pós")]
        PosFixado = 30, //Qualquer título pós-fixado (sem truncamento no cálculo)
        [StringValue("Pós")]
        PosFixado_Truncado = 35, //Qualquer título pós-fixado (com truncamento no cálculo)
        [StringValue("Pós")]
        PosFixado_FluxoCorrigido = 45, //Qualquer título com fluxo de pagto de correção, que se queira avaliar o pricing por correção do fluxo em vez de desconto

        //A partir daqui segue o manual de cálculo da CETIP


        //*******************Titulos de Credito
        [StringValue("CCB")]
        CCB = 1000,

        [StringValue("CCE")]
        CCE = 1005,

        [StringValue("NCE")]
        NCE = 1010,

        [StringValue("ExportNote")]
        ExportNote = 1015,
        //*******************Titulos de Credito


        //*******************Captação Bancária
        [StringValue("CDB")]
        CDB = 1100,

        [StringValue("DPGE")]
        DPGE = 1105,

        [StringValue("LF")] //Letra Financeira
        LF = 1110,
        //*******************Captação Bancária


        //*******************Imobiliarios
        [StringValue("CRI")]
        CRI = 1200,

        [StringValue("CCI")]
        CCI = 1205,
                
        [StringValue("LCI")]
        LCI = 1300,

        [StringValue("LH")]
        LH = 1400,
        //*******************Imobiliarios


        [StringValue("NotaComercial")]
        NotaComercial = 1500,

        [StringValue("LC")] //Letra de Câmbio
        LC = 1700,

        [StringValue("Debênture")]
        Debenture = 2000,


        //*******************Agricolas
        [StringValue("LCA")]
        LCA = 2100,

        [StringValue("CRA")]
        CRA = 2105,

        [StringValue("CDCA")]
        CDCA = 2110,

        [StringValue("CPR")]
        CPR = 2115,

        [StringValue("CDA")]
        CDA = 2120,
        //*******************Agricolas
        
        //*******************OffShore
                [StringValue("GLOBALS")]
        GLOBALS = 2200,

         [StringValue("TBILL")]
        TBILL = 2210,
        //*******************OffShore
        
        [StringValue("TDA")]
        TDA = 5000
    }

    public static class ClasseRendaFixaDescricao
    {
        public static string RetornaDescricao(int classe)
        {
            return Enum.GetName(typeof(ClasseRendaFixa), classe);
        }

        public static string RetornaStringValue(int classe)
        {
            string output = null;
            try
            {
                Enum value = Enum.Parse(typeof(ClasseRendaFixa), RetornaDescricao(classe)) as Enum;
                Type type = value.GetType();
                FieldInfo fi = type.GetField(value.ToString());
                StringValueAttribute[] sva = fi.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
                if (sva.Length > 0)
                {
                    output = sva[0].Value;
                }
            }
            catch { }
            return output;
        }
    }

    public enum TipoOperacaoTitulo
    {
        [StringValue("Compra Final")]
        CompraFinal = 1,

        [StringValue("Venda Final")]
        VendaFinal = 2,

        [StringValue("Compra Revenda")]
        CompraRevenda = 3,

        [StringValue("Venda Recompra")]
        VendaRecompra = 4,

        [StringValue("Venda Total")]
        VendaTotal = 6,

        [StringValue("Compra Casada")]
        CompraCasada = 10,

        [StringValue("Venda Casada")]
        VendaCasada = 11,

        [StringValue("Antecipacao Revenda")]
        AntecipacaoRevenda = 12,

        [StringValue("Antecipacao Recompra")]
        AntecipacaoRecompra = 13,

        [StringValue("Depósito")]
        Deposito = 20,

        [StringValue("Retirada")]
        Retirada = 21,

        [StringValue("Ingresso em Ativos com Impacto na Quantidade")]
        IngressoAtivoImpactoQtde = 22,

        [StringValue("Ingresso em Ativos com Impacto na Cota")]
        IngressoAtivoImpactoCota = 23,

        [StringValue("Retirada em Ativos com Impacto na Quantidade")]
        RetiradaAtivoImpactoQtde = 24,

        [StringValue("Retirada em Ativos com Impacto na Cota")]
        RetiradaAtivoImpactoCota = 25,

        [StringValue("Exercício Opção")]
        ExercicioOpcao = 26,

    }

    public static class TipoOperacaoTituloDescricao
    {
        public static string RetornaDescricao(int tipoOperacaoTitulo)
        {
            return Enum.GetName(typeof(TipoOperacaoTitulo), tipoOperacaoTitulo);
        }

        public static string RetornaStringValue(int tipoOperacaoTitulo)
        {
            string output = null;
            try
            {
                Enum value = Enum.Parse(typeof(TipoOperacaoTitulo), RetornaDescricao(tipoOperacaoTitulo)) as Enum;
                Type type = value.GetType();
                FieldInfo fi = type.GetField(value.ToString());
                StringValueAttribute[] sva = fi.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
                if (sva.Length > 0)
                {
                    output = sva[0].Value;
                }
            }
            catch { }
            return output;
        }
    }

    public enum FonteOperacaoTitulo
    {
        Manual = 1,
        OrdemRendaFixa = 2,
        Sinacor = 3,
        OpenVirtual = 4,
        Automatico = 5
    }

    public enum TipoPapelTitulo
    {
        Publico = 1,
        Privado = 2
    }

    /// <summary>
    /// Forma de cálculo da rentabilidade.
    /// </summary>
    public enum TipoRentabilidadeTitulo
    {
        PosFixado = 1,
        PreFixado = 2,
        Hibrido = 3
    }

    /// <summary>
    /// Tipo de curva usada em apropriações de juros (exponencial vs linear)
    /// </summary>
    public enum TipoCurvaTitulo
    {
        Exponencial = 1,
        Linear = 2
    }

    /// <summary>
    /// Convenção da base de cálculo para ano a ser usada.
    /// </summary>
    public enum BaseCalculoTitulo
    {
        Base252 = 252,
        Base360 = 360,
        Base365 = 365
    }

    /// <summary>
    /// Dias úteis ou corridos.
    /// </summary>
    public enum ContagemDiasTitulo
    {
        Uteis = 1,
        Corridos = 2,
        Dias360 = 3
    }

    public enum TipoVolumeTitulo
    {
        Quantidade = 1,
        Valor = 2
    }

    public enum LiquidacaoTitulo
    {
        NaoInformado = 0,
        Selic = 1,
        Cetip = 2,
        CBLC = 3
    }

    public enum CustodiaTitulo
    {
        [StringValue("N/D")]
        NaoInformado = 0,

        [StringValue("Selic")]
        Selic = 1,

        [StringValue("Cetip")]
        Cetip = 2,

        [StringValue("CBLC")]
        CBLC = 3
    }

    public enum TipoEventoTitulo
    {
        [StringValue("Juros")]
        Juros = 1,

        [StringValue("Juros Correção")]
        JurosCorrecao = 2,

        [StringValue("Amortização")]
        Amortizacao = 3,

        [StringValue("Pagamento Principal")]
        PagamentoPrincipal = 4,

        [StringValue("Pagamento Correção")]
        PagamentoCorrecao = 5,

        [StringValue("Incorporação Juros")]
        IncorporacaoJuros = 6,

        [StringValue("Pagamento PU")]
        PagamentoPU = 7,

        [StringValue("Amortização Corrigida")]
        AmortizacaoCorrigida = 8
    }

    public static class TipoEventoTituloDescricao
    {
        public static string RetornaDescricao(int idTipoEventoTitulo)
        {
            return Enum.GetName(typeof(TipoEventoTitulo), idTipoEventoTitulo);
        }

        public static string RetornaStringValue(int idTipoEventoTitulo)
        {
            string output = null;
            try
            {
                Enum value = Enum.Parse(typeof(TipoEventoTitulo), RetornaDescricao(idTipoEventoTitulo)) as Enum;
                Type type = value.GetType();
                FieldInfo fi = type.GetField(value.ToString());
                StringValueAttribute[] sva = fi.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
                if (sva.Length > 0)
                {
                    output = sva[0].Value;
                }
            }
            catch { }
            return output;
        }
    }



    public enum TipoNegociacaoTitulo
    {
        // TODO: Fazer Tradução Multilingua.
        // Funcionando apenas em Português.

        [StringValue("Negociação")]
        Negociacao = 1,

        [StringValue("Disponível Venda")]
        DisponivelVenda = 2,

        [StringValue("Vencimento")]
        Vencimento = 3
    }

    public enum TipoLancamentoLiquidacao
    {
        [StringValue("Vencimento")]
        Vencimento = 1,

        [StringValue("Revenda")]
        Revenda = 2,

        [StringValue("Recompra")]
        Recompra = 3,

        [StringValue("Amortização")]
        Amortizacao = 4,

        [StringValue("Juros")]
        Juros = 5,

        [StringValue("Pagto. Principal")]
        PagtoPrincipal = 6,

        [StringValue("Venda")]
        Venda = 20,

        [StringValue("Premio")]
        Premio = 21,

        [StringValue("PremioPosicao")]
        PremioPosicao = 22
    }

    public enum TipoMTMTitulo
    {
        [StringValue("Não Faz")]
        NaoFaz = 1,

        [StringValue("Anbima Títulos Públicos")]
        Andima_TituloPublico = 2,

        [StringValue("Mercado Debêntures")]
        Mercado_Debenture = 3,

        [StringValue("Série Cotação")]
        Serie = 4,

        [StringValue("Tesouro Direto")]
        Tesouro = 5,

        [StringValue("Taxa Série")]
        SerieTaxa = 6,

        [StringValue("Fundo Anbima")]
        CotaAnbima = 7,

        [StringValue("Par Debêntures")]
        Par_Debenture = 8,

        [StringValue("Bovespa BMF")]
        BovespaBMF = 9,

        [StringValue("Curva")]
        Curva =  10,

        [StringValue("Série-Taxa - % Pu Face")]
        SerieTaxaPuFace = 11
    }

    public static class TipoMTMTituloDescricao
    {
        public static string RetornaDescricao(int idTipoMTMTitulo)
        {
            return Enum.GetName(typeof(TipoMTMTitulo), idTipoMTMTitulo);
        }

        public static string RetornaStringValue(int idTipoMTMTitulo)
        {
            string output = null;
            try
            {
                Enum value = Enum.Parse(typeof(TipoMTMTitulo), RetornaDescricao(idTipoMTMTitulo)) as Enum;
                Type type = value.GetType();
                FieldInfo fi = type.GetField(value.ToString());
                StringValueAttribute[] sva = fi.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
                if (sva.Length > 0)
                {
                    output = sva[0].Value;
                }
            }
            catch { }
            return output;
        }
    }

    public enum TipoPreco
    {
        [StringValue("Par")]
        Par = 1,

        [StringValue("Curva")]
        Curva = 2,

        [StringValue("MTM")]
        MTM = 3,

        [StringValue("CurvaVencimento")]
        CurvaVencimento = 4,
    }

    public static class TipoPrecoDescricao
    {
        public static string RetornaDescricao(int idTipoPreco)
        {
            return Enum.GetName(typeof(TipoPreco), idTipoPreco);
        }

        public static string RetornaStringValue(int idTipoPreco)
        {
            string output = null;
            try
            {
                Enum value = Enum.Parse(typeof(TipoPreco), RetornaDescricao(idTipoPreco)) as Enum;
                Type type = value.GetType();
                FieldInfo fi = type.GetField(value.ToString());
                StringValueAttribute[] sva = fi.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
                if (sva.Length > 0)
                {
                    output = sva[0].Value;
                }
            }
            catch { }
            return output;
        }
    }

    public enum TipoVigenciaCategoria
    {
        [StringValue("Negociação")]
        Negociacao = 1,

        [StringValue("Vencimento")]
        Vencimento = 2,
    }

    public static class TipoVigenciaCategoriaDescricao
    {
        public static string RetornaDescricao(int idTipoVigenciaCategoria)
        {
            return Enum.GetName(typeof(TipoVigenciaCategoria), idTipoVigenciaCategoria);
        }

        public static string RetornaStringValue(int idTipoVigenciaCategoria)
        {
            string output = null;
            try
            {
                Enum value = Enum.Parse(typeof(TipoVigenciaCategoria), RetornaDescricao(idTipoVigenciaCategoria)) as Enum;
                Type type = value.GetType();
                FieldInfo fi = type.GetField(value.ToString());
                StringValueAttribute[] sva = fi.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
                if (sva.Length > 0)
                {
                    output = sva[0].Value;
                }
            }
            catch { }
            return output;
        }
    }    

    public enum TipoCurva
    {
        [StringValue("Pré")]
        Pre = 1,

        [StringValue("Outros")]
        Outros=2,

        [StringValue("Cupom Limpo")]
        Cupom_Limpo=3,

        [StringValue("Cupom Sujo")]
        Cupom_Sujo=4,

        [StringValue("Cupom Forward")]
        Cupom_Forward=5,

        [StringValue("Fator")]
        Fator=6,

        [StringValue("Spread")]
        Spread=7,

        [StringValue("Composta")]
        Composta=8,

        [StringValue("Indx Flat")]
        Indx_Flat=9

    }

    public static class TipoCurvaDescricao
    {
        public static string RetornaDescricao(int idTipoCurva)
        {
            return Enum.GetName(typeof(TipoCurva), idTipoCurva);
        }

        public static string RetornaStringValue(int idTipoCurva)
        {
            string output = null;
            try
            {
                Enum value = Enum.Parse(typeof(TipoCurva), RetornaDescricao(idTipoCurva)) as Enum;
                Type type = value.GetType();
                FieldInfo fi = type.GetField(value.ToString());
                StringValueAttribute[] sva = fi.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
                if (sva.Length > 0)
                {
                    output = sva[0].Value;
                }
            }
            catch { }
            return output;
        }
    }

    public enum TipoComposicao
    {
        [StringValue("Vértice")]
        Vertice = 1,

        [StringValue("Vencimento")]
        Vencimento = 2
    }

    public static class TipoComposicaoDescricao
    {
        public static string RetornaDescricao(int idTipoComposicao)
        {
            return Enum.GetName(typeof(TipoComposicao), idTipoComposicao);
        }

        public static string RetornaStringValue(int idTipoComposicao)
        {
            string output = null;
            try
            {
                Enum value = Enum.Parse(typeof(TipoComposicao), RetornaDescricao(idTipoComposicao)) as Enum;
                Type type = value.GetType();
                FieldInfo fi = type.GetField(value.ToString());
                StringValueAttribute[] sva = fi.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
                if (sva.Length > 0)
                {
                    output = sva[0].Value;
                }
            }
            catch { }
            return output;
        }
    }

    public enum CriterioInterpolacao
    {
        [StringValue("Não interpola")]
        Nao_interpola = 0,	

        [StringValue("Exponencial")]
    	Exponencial = 1,

        [StringValue("Linear")]
	    Linear = 2,
    }

    public static class CriterioInterpolacaoDescricao
    {
        public static string RetornaDescricao(int idCriterioInterpolacao)
        {
            return Enum.GetName(typeof(CriterioInterpolacao), idCriterioInterpolacao);
        }

        public static string RetornaStringValue(int idCriterioInterpolacao)
        {
            string output = null;
            try
            {
                Enum value = Enum.Parse(typeof(CriterioInterpolacao), RetornaDescricao(idCriterioInterpolacao)) as Enum;
                Type type = value.GetType();
                FieldInfo fi = type.GetField(value.ToString());
                StringValueAttribute[] sva = fi.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
                if (sva.Length > 0)
                {
                    output = sva[0].Value;
                }
            }
            catch { }
            return output;
        }
    }

    public enum ExpressaoTaxaZero
    {
        [StringValue("EXP DU/252")]
        EXP_DU_252 = 1,

        [StringValue("LIN DC/360")]
        LIN_DC_360 = 2,

        [StringValue("LIN 30/360")]
        LIN_30_360 = 3,

        [StringValue("LIN DC/365")]
        LIN_DC_365 = 4,

        [StringValue("EXP 30/360")]
        EXP_30_360 = 5,

        [StringValue("EXP DC/360")]
        EXP_DC_360 = 6,

        [StringValue("EXP 30/365")]
        EXP_30_365 = 7
    }

    public static class ExpressaoTaxaZeroDescricao
    {
        public static string RetornaDescricao(int idExpressaoTaxaZero)
        {
            return Enum.GetName(typeof(ExpressaoTaxaZero), idExpressaoTaxaZero);
        }

        public static string RetornaStringValue(int idExpressaoTaxaZero)
        {
            string output = null;
            try
            {
                Enum value = Enum.Parse(typeof(ExpressaoTaxaZero), RetornaDescricao(idExpressaoTaxaZero)) as Enum;
                Type type = value.GetType();
                FieldInfo fi = type.GetField(value.ToString());
                StringValueAttribute[] sva = fi.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
                if (sva.Length > 0)
                {
                    output = sva[0].Value;
                }
            }
            catch { }
            return output;
        }
    }

    public enum DigitadoImportado
    {
        [StringValue("Importado")]
        Importado = 1,

        [StringValue("Digitado")]
        Digitado = 2
    }

    public static class DigitadoImportadoDescricao
    {
        public static string RetornaDescricao(int idDigitadoImportado)
        {
            return Enum.GetName(typeof(DigitadoImportado), idDigitadoImportado);
        }

        public static string RetornaStringValue(int idDigitadoImportado)
        {
            string output = null;
            try
            {
                Enum value = Enum.Parse(typeof(DigitadoImportado), RetornaDescricao(idDigitadoImportado)) as Enum;
                Type type = value.GetType();
                FieldInfo fi = type.GetField(value.ToString());
                StringValueAttribute[] sva = fi.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
                if (sva.Length > 0)
                {
                    output = sva[0].Value;
                }
            }
            catch { }
            return output;
        }
    }


    public enum SerieNTN
    {
        [StringValue("Não é NTN/Não implementado")]
        naoNTN = 0,

        [StringValue("NTN-C")]
        ntnC = 1,

        [StringValue("NTN-B")]
        ntnB = 2,

        [StringValue("NTN-F")]
        ntnF = 3
    }

    public static class SerieNTNDescricao
    {
        public static string RetornaDescricao(int idSerieNTN)
        {
            return Enum.GetName(typeof(SerieNTN), idSerieNTN);
        }

        public static string RetornaStringValue(int idSerieNTN)
        {
            string output = null;
            try
            {
                Enum value = Enum.Parse(typeof(SerieNTN), RetornaDescricao(idSerieNTN)) as Enum;
                Type type = value.GetType();
                FieldInfo fi = type.GetField(value.ToString());
                StringValueAttribute[] sva = fi.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
                if (sva.Length > 0)
                {
                    output = sva[0].Value;
                }
            }
            catch { }
            return output;
        }
    }

    public enum CriterioAmortizacao
    {
        [StringValue("PUPar")]
        PUPar = 1,

        [StringValue("Valor Nominal")]
        ValorNominal = 2,

        [StringValue("Valor Nominal Atualizado")]
        ValorNominalAtualizado = 3,

        [StringValue("Valor Nominal Inicial")]
        ValorNominalInicial = 4,

        [StringValue("Valor Nominal Inicial Fixo")]
        ValorNominalInicialFixo = 5
    }

    public static class CriterioAmortizacaoDescricao
    {
        public static string RetornaDescricao(int idCriterioAmortizacao)
        {
            return Enum.GetName(typeof(CriterioAmortizacao), idCriterioAmortizacao);
        }

        public static string RetornaStringValue(int idCriterioAmortizacao)
        {
            string output = null;
            try
            {
                Enum value = Enum.Parse(typeof(CriterioAmortizacao), RetornaDescricao(idCriterioAmortizacao)) as Enum;
                Type type = value.GetType();
                FieldInfo fi = type.GetField(value.ToString());
                StringValueAttribute[] sva = fi.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
                if (sva.Length > 0)
                {
                    output = sva[0].Value;
                }
            }
            catch { }
            return output;
        }
    }

    public enum ApropriacaoCurva
    {
        [StringValue("Diário")]
        Diario = 1,

        [StringValue("Mensal")]
        Mensal = 2,

        [StringValue("Anual")]
        Anual = 3,

        [StringValue("Período")]
        Periodo = 4
    }

    public static class ApropriacaoCurvaDescricao
    {
        public static string RetornaDescricao(int idApropriacaoCurva)
        {
            return Enum.GetName(typeof(ApropriacaoCurva), idApropriacaoCurva);
        }

        public static string RetornaStringValue(int idApropriacaoCurva)
        {
            string output = null;
            try
            {
                Enum value = Enum.Parse(typeof(ApropriacaoCurva), RetornaDescricao(idApropriacaoCurva)) as Enum;
                Type type = value.GetType();
                FieldInfo fi = type.GetField(value.ToString());
                StringValueAttribute[] sva = fi.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
                if (sva.Length > 0)
                {
                    output = sva[0].Value;
                }
            }
            catch { }
            return output;
        }
    }

    public enum TipoProRata
    {
        [StringValue("Dias Úteis")]
        DiasUteis = 0,

        [StringValue("Dias Corridos")]
        DiasCorridos = 1
    }

    public static class TipoProRataDescricao
    {
        public static string RetornaDescricao(int idATipoProRata)
        {
            return Enum.GetName(typeof(TipoProRata), idATipoProRata);
        }

        public static string RetornaStringValue(int idATipoProRata)
        {
            string output = null;
            try
            {
                Enum value = Enum.Parse(typeof(TipoProRata), RetornaDescricao(idATipoProRata)) as Enum;
                Type type = value.GetType();
                FieldInfo fi = type.GetField(value.ToString());
                StringValueAttribute[] sva = fi.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
                if (sva.Length > 0)
                {
                    output = sva[0].Value;
                }
            }
            catch { }
            return output;
        }
    }

    public enum TipoOperacaoBloqueio
    {
        Bloqueio = 1,
        Desbloqueio = 2
    }

    public enum TituloIsentoIR
    {
        NaoIsento = 1,
        IsentoPF = 2,
        Isento = 3,
        IsentoCupom = 4
    }

    public enum TituloIsentoIOF
    {
        NaoIsento = 1,
        IsentoPF = 2,
        Isento = 3
    }

    public enum StatusOrdemRendaFixa
    {
        Digitado = 1,
        Aprovado = 2,
        Processado = 3,
        Cancelado = 4
    }

    public enum StatusExportacaoRendaFixa
    {
        [StringValue("Liberado para Exportação")]
        LiberadoExportacao = 1,

        [StringValue("Exportado")]
        Exportado = 5,

        [StringValue("OK")]
        ProcessadoSucesso = 100,

        [StringValue("Falta Liquidação")]
        ProcessadoErroFaltaLiquidacao = 110,

        [StringValue("Falta Papel")]
        ProcessadoErroFaltaPapel = 115,

        [StringValue("Informações Divergentes")]
        ProcessadoErroInfoDivergente = 120,

        [StringValue("Erro no Bloco")]
        ProcessadoErroBloco = 125,

        [StringValue("Erro no Serviço")]
        ProcessadoErroServico = 130,

        [StringValue("Erro no Domínio")]
        ProcessadoErroDominio = 135,

        [StringValue("Erro Outros")]
        ProcessadoErroOutros = 140
        
    }

    public enum PagamentoJurosTitulo
    {
        ContaDias = 1,        
        Cheio = 2,
        ContaDiasCorridos = 3 //Tende a desaparecer
    }

    public enum TipoControleCustodia
    {
        NaoControla = 1,
        ControlaCustodiante = 2,
        ControlaClearing = 3
    }

    public enum CodigoOperacaoCetip
    {
        CompraVendaDefinitiva = 52,
        CompraVendaTermo = 552
    }

    public enum TipoCompraVendaCetip
    {
        VendaCarteiraPropria=1,
        Compra = 2,
        VendaCarteiraRepasse = 3
    }

    public enum CodigoModalidadeLiquidacaoCetip
    {
        SemModalidade = 0,
        Cetip = 1,
        Bruta = 2
    }

    public enum StatusOperacaoRendaFixa
    {
        Digitado = 1,
        Ajustado = 100,
        LiberadoSemAjuste = 200,
        LiberadoComAjuste = 210
    }

    public enum ListaIsentoIR
    {
        [StringValue("Não Isento")]
        NIsento = 1,

        [StringValue("Isento PF")]
        IsentoPF = 2,

        [StringValue("Isento")]
        Isento = 3,

        [StringValue("Pagto Cupom")]
        PagtoCupom = 4,
    }

    public static class ListaIsentoIRDescricao
    {
        public static string RetornaDescricao(int idIsentoIR)
        {
            return Enum.GetName(typeof(ListaIsentoIR), idIsentoIR);
        }

        public static string RetornaStringValue(int idIsentoIR)
        {
            string output = null;
            try
            {
                Enum value = Enum.Parse(typeof(ListaIsentoIR), RetornaDescricao(idIsentoIR)) as Enum;
                Type type = value.GetType();
                FieldInfo fi = type.GetField(value.ToString());
                StringValueAttribute[] sva = fi.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
                if (sva.Length > 0)
                {
                    output = sva[0].Value;
                }
            }
            catch { }
            return output;
        }
    }

    public enum StatusLiquidacaoRendaFixa
    {
        Digitado = 1,
        Ajustado = 100,
        LiberadoSemAjuste = 200,
        LiberadoComAjuste = 210
    }

    public enum TipoOpcao
    {
        Call = 1,
        Put = 2
    }

    public enum TipoExercicio
    {
        Europeia = 1,
        Americana = 2
    }

    public enum TipoPremio
    {
        [StringValue("Valor Fixo")]
        ValorFixo = 1,

        [StringValue("Percentual Valor Face")]
        PercentualValorFace = 2
    } 
}
