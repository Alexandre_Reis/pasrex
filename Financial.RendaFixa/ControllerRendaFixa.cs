﻿using log4net;
using System.Collections.Generic;
using System;
using Financial.Util;
using Financial.Common.Enums;
using System.Text;
using System.Data.SqlClient;
using EntitySpaces.Interfaces;
using EntitySpaces.Core;
using Financial.RendaFixa.Enums;
using Financial.Investidor.Enums;
using Financial.Investidor;
using Financial.Investidor.Controller;
using Financial.Common;
using System.Collections;
using Financial.Fundo.Enums;
using Financial.InvestidorCotista;
using Financial.Fundo;
using Financial.Common;
using Financial.Util.Enums;
using Financial.CRM;

namespace Financial.RendaFixa.Controller
{
    public class ControllerRendaFixa
    {
        /// <summary>
        /// Reprocessa as posições, por tipo = abertura ou tipo = fechamento.
        /// Se fechamento, busca as posições de fechamento do dia, pela PosicaoHistorico.
        /// Se abertura, busca as posições de abertura do dia PosicaoAbertura.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        /// <param name="tipoReprocessamento">Verificar se é necessario</param>
        /// thows ArgumentException se tipoReprocessamento for diferente dos valores do 
        /// Enum TipoReprocessamentoBMF
        public void ReprocessaPosicao(int idCliente, DateTime data, TipoReprocessamento tipoReprocessamento)
        {
            this.DeletaDetalhePosicaoAfetadaRF(idCliente, data);

            #region Delete PosicaoRendaFixa
            PosicaoRendaFixaCollection posicaoRendaFixaCollectionDeletar = new PosicaoRendaFixaCollection();
            try
            {
                posicaoRendaFixaCollectionDeletar.DeletaPosicaoRendaFixa(idCliente);
            }
            catch (SqlException e)
            {
                Console.WriteLine("Delete PosicaoRendaFixa");
                Console.WriteLine(e.LineNumber);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.ToString());
            }
            #endregion

            #region Inserts das Posições baseado nas posições de fechamento, ou de abertura
            //Para SqlServer precisa dar um tratamento especial para a PK Identity
            // Nome da Configuração Default SQL
            if (esConfigSettings.DefaultConnection.Provider == "EntitySpaces.SqlClientProvider")
            {
                using (esTransactionScope scope = new esTransactionScope())
                {
                    esUtility u = new esUtility();
                    string sql = " set identity_insert PosicaoRendaFixa on ";
                    u.ExecuteNonQuery(esQueryType.Text, sql, "");

                    if (tipoReprocessamento == TipoReprocessamento.Fechamento)
                    {
                        #region Insert PosicaoRendaFixa a partir de PosicaoRendaFixaHistorico
                        PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
                        try
                        {
                            posicaoRendaFixaCollection.InserePosicaoRendaFixaHistorico(idCliente, data);
                        }
                        catch (SqlException e)
                        {
                            Console.WriteLine("Insert PosicaoRendaFixa");
                            Console.WriteLine(e.LineNumber);
                            Console.WriteLine(e.Message);
                            Console.WriteLine(e.StackTrace);
                            Console.WriteLine(e.ToString());
                        }
                        #endregion
                    }
                    else
                    {
                        #region Insert PosicaoRendaFixa a partir de PosicaoRendaFixaAbertura
                        PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
                        try
                        {
                            posicaoRendaFixaCollection.InserePosicaoRendaFixaAbertura(idCliente, data);
                        }
                        catch (SqlException e)
                        {
                            Console.WriteLine("Insert PosicaoRendaFixa");
                            Console.WriteLine(e.LineNumber);
                            Console.WriteLine(e.Message);
                            Console.WriteLine(e.StackTrace);
                            Console.WriteLine(e.ToString());
                        }
                        #endregion
                    }

                    u = new esUtility();
                    sql = " set identity_insert PosicaoRendaFixa off ";
                    u.ExecuteNonQuery(esQueryType.Text, sql, "");

                    scope.Complete();
                }
            }
            else
            {
                //TODO ESCREVER NA UNHA O SQL, POIS O ORACLE NÃO PERMITE DESLIGAR A SEQUENCE
                if (tipoReprocessamento == TipoReprocessamento.Fechamento)
                {
                    #region Insert PosicaoRendaFixa a partir de PosicaoRendaFixaHistorico
                    PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
                    try
                    {
                        posicaoRendaFixaCollection.InserePosicaoRendaFixaHistorico(idCliente, data);
                    }
                    catch (SqlException e)
                    {
                        Console.WriteLine("Insert PosicaoRendaFixa");
                        Console.WriteLine(e.LineNumber);
                        Console.WriteLine(e.Message);
                        Console.WriteLine(e.StackTrace);
                        Console.WriteLine(e.ToString());
                    }
                    #endregion
                }
                else
                {
                    #region Insert PosicaoRendaFixa a partir de PosicaoRendaFixaAbertura
                    PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
                    try
                    {
                        posicaoRendaFixaCollection.InserePosicaoRendaFixaAbertura(idCliente, data);
                    }
                    catch (SqlException e)
                    {
                        Console.WriteLine("Insert PosicaoRendaFixa");
                        Console.WriteLine(e.LineNumber);
                        Console.WriteLine(e.Message);
                        Console.WriteLine(e.StackTrace);
                        Console.WriteLine(e.ToString());
                    }
                    #endregion
                }
            }
            #endregion

            this.DeletaPosicoesAbertura(idCliente, data);
            
            this.DeletaPosicoesHistorico(idCliente, data);

            //Cálculo da correção monetária, juros dos PUs dos títulos, tributos
            #region DePara Reprocessamento/Processamento
            TipoProcessamento tipoProcessamento = new TipoProcessamento();
            if (tipoReprocessamento == TipoReprocessamento.CalculoDiario)
                tipoProcessamento = TipoProcessamento.Fechamento;
            else if (tipoReprocessamento == TipoReprocessamento.Abertura)
                tipoProcessamento = TipoProcessamento.Abertura;
            else if (tipoReprocessamento == TipoReprocessamento.Fechamento)
                tipoProcessamento = TipoProcessamento.Divulgacao;
            #endregion

            this.DeletaMemoriaCalculo(idCliente, data, tipoProcessamento);

            Cliente c = new Cliente();
            List<esQueryItem> campos = new List<esQueryItem>(
                                    new esQueryItem[] { c.Query.DataDia, c.Query.DataImplantacao }
                              );
            c.LoadByPrimaryKey(campos, idCliente);

            if (c.DataDia.Value == c.DataImplantacao.Value)
            {
                PosicaoRendaFixa posicaoRendaFixaAux = new PosicaoRendaFixa(tipoProcessamento);
                posicaoRendaFixaAux.AtualizaPosicao(idCliente, data);
            }
        }

        /// <summary>
        /// Executa abertura das posições (PU, tributos). 
        /// Realiza o MTM dos títulos.
        /// Carrega a posição atual para Posicao de Abertura.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ExecutaAbertura(int idCliente, DateTime data)
        {
            TipoProcessamento tipoProcessamento = TipoProcessamento.Abertura;

            #region Cliente
            Cliente cliente = new Cliente();
            cliente.LoadByPrimaryKey(idCliente);
            #endregion

            #region Perfil MTM
            PerfilMTMCollection perfilMTMCollection = new PerfilMTMCollection();
            if (cliente.IdLocal.Value != (short)LocalFeriadoFixo.Brasil)
            {
                DateTime dataAlvo = Calendario.SubtraiDiaUtil(data, 1, cliente.IdLocal.Value, TipoFeriado.Outros);
                dataAlvo = Calendario.AdicionaDiaUtil(dataAlvo, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);

                if (dataAlvo != data)
                    perfilMTMCollection.copiarPerfilMTM(null, dataAlvo, false);
            }

            //Executa a copia da tabela de perfil para o dia util seguinte
            perfilMTMCollection.copiarPerfilMTM(null, data, false);
            //
            #endregion

            PosicaoRendaFixa posicaoRendaFixa = new PosicaoRendaFixa(tipoProcessamento);

            //Repactuação 
            posicaoRendaFixa.RepacutacaoRendaFixa(idCliente, data);
            //

            (new TabelaCustosRendaFixa()).TrataVencimentoCustodiaAnual(idCliente, data);
            (new TabelaCustosRendaFixa()).CalculaCustoCustodiaAnual(idCliente, data);
				
			posicaoRendaFixa.BaixaPosicaoCompromissada(idCliente, data);

            //Cálculo da correção monetária, juros dos PUs dos títulos, tributos
            posicaoRendaFixa.AtualizaPosicao(idCliente, data);
            //

            //Marcação a mercado (MTM) realizada na abertura
            posicaoRendaFixa.MarcaMTM(idCliente, data);
            //
                        
			posicaoRendaFixa.CalculaTributacao(idCliente, data);
            posicaoRendaFixa.BaixaPosicaoVencimento(idCliente, data);

            //Lança a agenda no caixa
            (new AgendaRendaFixa()).TrataLiquidacaoAgenda(idCliente, data, tipoProcessamento);
            //

            if (cliente.AberturaIndexada.GetValueOrDefault(0) != (int)TipoAberturaIndexada.NaoExecuta)
            {
                posicaoRendaFixa.AberturaIndexada(cliente, data, null);
            }

            //Carrega a posição atual para Posicao de Abertura
            posicaoRendaFixa.GeraPosicaoAbertura(idCliente, data);
            //      			

            this.CalculaPrazoMedioRendaFixa(data, idCliente, (int)tipoProcessamento);
            this.CalculaRentabilidadeRendaFixa(data, idCliente);
        }

        /// <summary>
        /// Backup de PosicaoAtual para PosicaoHistorico.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ExecutaDivulgacao(int idCliente, DateTime data)
        {
            this.DeletaPosicoesHistorico(idCliente, data);

            PosicaoRendaFixa posicaoRendaFixa = new PosicaoRendaFixa();
            posicaoRendaFixa.GeraBackup(idCliente, data);
        }

        /// <summary>
        /// Realiza todos os cálculos financeiros e despesas, atualiza a custódia, gera fluxos de pagamento futuros.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void ExecutaFechamento(int idCliente, DateTime data, ParametrosProcessamento parametrosProcessamento)
        {
            TipoProcessamento tipoProcessamento = TipoProcessamento.Fechamento;

            this.ReprocessaPosicao(idCliente, data, TipoReprocessamento.CalculoDiario);

            /* Hash que armazena as principais as informações mais utilizadas para cálculo de MTM e comunicação com o Fincs 
              (PerfilMTM, DePara, Curvas,etc...) */

            Hashtable hsMemoria = new Hashtable();

            PosicaoRendaFixa posicaoRendaFixa = new PosicaoRendaFixa();
            OperacaoRendaFixa operacaoRendaFixa = new OperacaoRendaFixa();
            BloqueioRendaFixa bloqueioRendaFixa = new BloqueioRendaFixa();
            AgendaRendaFixa agendaRendaFixa = new AgendaRendaFixa();
            LiquidacaoRendaFixa liquidacaoRendaFixa = new LiquidacaoRendaFixa();
            OrdemRendaFixa ordemRendaFixa = new OrdemRendaFixa();
            TabelaCustosRendaFixa tabelaCustosRendaFixa = new TabelaCustosRendaFixa();
            PosicaoRendaFixaDetalhe posicaoRendaFixaDetalhe = new PosicaoRendaFixaDetalhe();

            //Repactuação 
            posicaoRendaFixa.RepacutacaoRendaFixa(idCliente, data);

            if (parametrosProcessamento.ListaTabelaInterfaceCliente.Contains((int)ListaInterfaceCliente.RendaFixa.VirtualFull))
            {
                if (!parametrosProcessamento.IgnoraOperacoes)
                {
                    operacaoRendaFixa.IntegraOperacoesVirtual(idCliente, data);

                    //if (parametrosProcessamento.ListaTabelaInterfaceCliente.Contains((int)ListaInterfaceCliente.RendaFixa.Sinacor))
                    //{
                    //    operacaoRendaFixa.IntegraOperacoesSinacor(idCliente, data);
                    //}
                }

                posicaoRendaFixa.IntegraPosicoesVirtual(idCliente, data);
            }
            else
            {
                posicaoRendaFixaDetalhe.TrataProcessamentoDiario(idCliente, data);

                #region TabelaCustosRendaFixa (vencimento e cálculo diário)
                tabelaCustosRendaFixa.TrataVencimentoCustodiaAnual(idCliente, data);
                tabelaCustosRendaFixa.CalculaCustoCustodiaAnual(idCliente, data);
                #endregion

                if (!parametrosProcessamento.IgnoraOperacoes)
                {
                    #region Processa Desbloqueio
                    bloqueioRendaFixa.ProcessaBloqueio(idCliente, data, (byte)TipoOperacaoBloqueio.Desbloqueio);
                    #endregion

                    #region CarregaOrdemRendaFixa
                    ordemRendaFixa.CarregaOrdemRendaFixa(idCliente, data);
                    #endregion

                    #region IntegraOperacoes
                    if (!parametrosProcessamento.ListaTabelaInterfaceCliente.Contains((int)ListaInterfaceCliente.RendaFixa.NaoImporta))
                    {
                        if (parametrosProcessamento.IntegracaoRendaFixa == (int)IntegracaoRendaFixa.Sinacor ||
                            parametrosProcessamento.ListaTabelaInterfaceCliente.Contains((int)ListaInterfaceCliente.RendaFixa.Sinacor))
                        {
                            operacaoRendaFixa.IntegraOperacoesSinacor(idCliente, data);
                        }
                        else if (parametrosProcessamento.IntegracaoRendaFixa == (int)IntegracaoRendaFixa.Virtual ||
                                 parametrosProcessamento.ListaTabelaInterfaceCliente.Contains((int)ListaInterfaceCliente.RendaFixa.VirtualOperacao))
                        {
                            operacaoRendaFixa.IntegraOperacoesVirtual(idCliente, data);
                        }
                    }
                    #endregion

                    #region Deleta operações automáticas de Retirada Especial geradas na mudança de alíquota para Titulos incentivados com GrossUp
                    DeletaOperacaoCotista(idCliente, data);
                    #endregion


                    #region DeletaLiquidacao
                    liquidacaoRendaFixa.DeletaLiquidacao(idCliente, data);
                    #endregion

                    #region ProcessaCompraFinal
                    operacaoRendaFixa.ProcessaCompraFinal(idCliente, data);
                    #endregion
                    
                    #region ProcessaLiquidacaoPosicao (Vendas e Revendas)
                    operacaoRendaFixa.ProcessaLiquidacaoPosicao(idCliente, data);
                    #endregion

                    #region Processa operações de compromisso
                    operacaoRendaFixa.ProcessaOperacaoCompromissada(idCliente, data);
                    #endregion

                    #region Lançamento em CC das operações finais, compromissadas e casadas
                    operacaoRendaFixa.LancaCCOperacao(idCliente, data); //Operações finais e compromissadas
                    operacaoRendaFixa.LancaCCOperacaoCasada(idCliente, data);
                    operacaoRendaFixa.LancaCCOperacaoPremio(idCliente, data);
                    #endregion

                    #region BaixaPosicaoCompromissada
                    posicaoRendaFixa.BaixaPosicaoCompromissada(idCliente, data);
                    #endregion

                    #region Processa Bloqueio
                    bloqueioRendaFixa.ProcessaBloqueio(idCliente, data, (byte)TipoOperacaoBloqueio.Bloqueio);
                    #endregion

                    #region Cálculo de TIR para posições zeradas
                    posicaoRendaFixa.CalculoTIR_PosicoesZeradas(idCliente, data, ref hsMemoria);
                    #endregion

                    if (parametrosProcessamento.RemuneraRF)
                    {
                        PosicaoRendaFixa posicaoRendaFixaAux = new PosicaoRendaFixa(tipoProcessamento);

                        //Cálculo da correção monetária, juros dos PUs dos títulos no próprio dia
                        posicaoRendaFixaAux.AtualizaPosicao(idCliente, data);
                       
                    }

                    #region TrataLiquidacaoAgenda
                    agendaRendaFixa.TrataLiquidacaoAgenda(idCliente, data, tipoProcessamento);
                    #endregion

                }

                #region MTM
                posicaoRendaFixa.MarcaMTM(idCliente, data, ref hsMemoria);

                CurvaRendaFixa curvaRendaFixa = new CurvaRendaFixa();
                curvaRendaFixa.ProcessaCurvaComposta(data, 0, ref hsMemoria);
                #endregion 

                #region Calcula Valores (posições resgatadas no dia)
                posicaoRendaFixa.AtualizaPosicoesResgatadas(idCliente, data);
                #endregion

                #region Cálculo do IR e IOF da posição (apenas para tributáveis)
                posicaoRendaFixa.CalculaTributacao(idCliente, data);
                #endregion

                if (!parametrosProcessamento.IgnoraOperacoes)
                {
                    #region BaixaPosicaoVencimento
                    posicaoRendaFixa.BaixaPosicaoVencimento(idCliente, data);
                    #endregion
                }

                #region Calcula Prazo Médio
                if (parametrosProcessamento.RemuneraRF)
                {
                    this.CalculaPrazoMedioRendaFixa(data, idCliente, (int)tipoProcessamento);                    
                }
                #endregion

                #region Rentabilidade
                this.CalculaRentabilidadeRendaFixa(data, idCliente);
                #endregion
            }

            //Deleta Posições Zeradas
            posicaoRendaFixa.DeletaPosicaoZerada(idCliente, data);
            //

        }

        public void ExecutaCalculoAutomatico(int idCliente, DateTime data)
        {
            TipoProcessamento tipoProcessamento = TipoProcessamento.Fechamento;
            this.ReprocessaPosicao(idCliente, data, TipoReprocessamento.CalculoDiario);

            PosicaoRendaFixa posicaoRendaFixa = new PosicaoRendaFixa();
            OperacaoRendaFixa operacaoRendaFixa = new OperacaoRendaFixa();
            BloqueioRendaFixa bloqueioRendaFixa = new BloqueioRendaFixa();
            AgendaRendaFixa agendaRendaFixa = new AgendaRendaFixa();
            LiquidacaoRendaFixa liquidacaoRendaFixa = new LiquidacaoRendaFixa();
            OrdemRendaFixa ordemRendaFixa = new OrdemRendaFixa();
            TabelaCustosRendaFixa tabelaCustosRendaFixa = new TabelaCustosRendaFixa();
            PosicaoRendaFixaDetalhe posicaoRendaFixaDetalhe = new PosicaoRendaFixaDetalhe();




            posicaoRendaFixaDetalhe.TrataProcessamentoDiario(idCliente, data);

            #region TabelaCustosRendaFixa (vencimento e cálculo diário)
            tabelaCustosRendaFixa.TrataVencimentoCustodiaAnual(idCliente, data);
            tabelaCustosRendaFixa.CalculaCustoCustodiaAnual(idCliente, data);
            #endregion

            
                #region Processa Desbloqueio
                bloqueioRendaFixa.ProcessaBloqueio(idCliente, data, (byte)TipoOperacaoBloqueio.Desbloqueio);
                #endregion

                #region CarregaOrdemRendaFixa
                ordemRendaFixa.CarregaOrdemRendaFixa(idCliente, data);
                #endregion

                //#region IntegraOperacoes
                //if (!parametrosProcessamento.ListaTabelaInterfaceCliente.Contains((int)ListaInterfaceCliente.RendaFixa.NaoImporta))
                //{
                //    if (parametrosProcessamento.IntegracaoRendaFixa == (int)IntegracaoRendaFixa.Sinacor ||
                //        parametrosProcessamento.ListaTabelaInterfaceCliente.Contains((int)ListaInterfaceCliente.RendaFixa.Sinacor))
                //    {
                //        operacaoRendaFixa.IntegraOperacoesSinacor(idCliente, data);
                //    }
                //    else if (parametrosProcessamento.IntegracaoRendaFixa == (int)IntegracaoRendaFixa.Virtual ||
                //             parametrosProcessamento.ListaTabelaInterfaceCliente.Contains((int)ListaInterfaceCliente.RendaFixa.VirtualOperacao))
                //    {
                //        operacaoRendaFixa.IntegraOperacoesVirtual(idCliente, data);
                //    }
                //}
                //#endregion

                #region DeletaLiquidacao
                liquidacaoRendaFixa.DeletaLiquidacao(idCliente, data);
                #endregion

                #region ProcessaCompraFinal
                operacaoRendaFixa.ProcessaCompraFinal(idCliente, data);
                #endregion

                #region TrataLiquidacaoAgenda
                agendaRendaFixa.TrataLiquidacaoAgenda(idCliente, data, tipoProcessamento);
                #endregion

                #region ProcessaLiquidacaoPosicao (Vendas e Revendas)
                operacaoRendaFixa.ProcessaLiquidacaoPosicao(idCliente, data);
                #endregion

                #region Processa operações de compromisso
                operacaoRendaFixa.ProcessaOperacaoCompromissada(idCliente, data);
                #endregion

                #region Lançamento em CC das operações finais, compromissadas e casadas
                operacaoRendaFixa.LancaCCOperacao(idCliente, data); //Operações finais e compromissadas
                operacaoRendaFixa.LancaCCOperacaoCasada(idCliente, data);
                #endregion

                #region BaixaPosicaoCompromissada
                posicaoRendaFixa.BaixaPosicaoCompromissada(idCliente, data);
                #endregion

                #region Processa Bloqueio
                bloqueioRendaFixa.ProcessaBloqueio(idCliente, data, (byte)TipoOperacaoBloqueio.Bloqueio);
                #endregion

                PosicaoRendaFixa posicaoRendaFixaAux = new PosicaoRendaFixa(tipoProcessamento);
                posicaoRendaFixaAux.AtualizaPosicao(idCliente, data);
                
            

            #region MarcaMTM
            posicaoRendaFixa.MarcaMTM(idCliente, data);
            #endregion

            #region Calcula Valores (posições resgatadas no dia)
            posicaoRendaFixa.AtualizaPosicoesResgatadas(idCliente, data);
            #endregion

            #region Cálculo do IR e IOF da posição (apenas para tributáveis)
            posicaoRendaFixa.CalculaTributacao(idCliente, data);
            #endregion

            posicaoRendaFixa.BaixaPosicaoVencimento(idCliente, data);

            


        }

         /// <summary>
        /// Deleta as operações automáticas de Retirada Especial geradas no dia da mudança de aliquota para títulos incentivados GrossUp
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void DeletaOperacaoCotista(int idCliente, DateTime data)
        {
            #region Delete Operação Cotista
            OperacaoCotistaCollection operacaoCotistaCollectionDeletar = new OperacaoCotistaCollection();
            operacaoCotistaCollectionDeletar.Query.Select(operacaoCotistaCollectionDeletar.Query.IdOperacao);
            operacaoCotistaCollectionDeletar.Query.Where(operacaoCotistaCollectionDeletar.Query.IdCarteira.Equal(idCliente),
                                                         operacaoCotistaCollectionDeletar.Query.IdCotista.Equal(idCliente),
                                                         operacaoCotistaCollectionDeletar.Query.TipoOperacao.Equal((byte)Financial.InvestidorCotista.Enums.TipoOperacaoCotista.ResgateCotasEspecial),
                                                         operacaoCotistaCollectionDeletar.Query.Fonte.Equal((byte)Financial.InvestidorCotista.Enums.FonteOperacaoCotista.DiferencaAliquotaGrossUp));
            operacaoCotistaCollectionDeletar.Query.Where(operacaoCotistaCollectionDeletar.Query.DataAgendamento.Equal(data));
            operacaoCotistaCollectionDeletar.Query.Load();
            operacaoCotistaCollectionDeletar.MarkAllAsDeleted();
            operacaoCotistaCollectionDeletar.Save();
            #endregion

        }



        /// <summary>
        /// Deleta as posicoes de abertura em PosicaoRendaFixa.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void DeletaPosicoesAbertura(int idCliente, DateTime data)
        {
            PosicaoRendaFixaAberturaCollection posicaoRendaFixaAberturaCollection = new PosicaoRendaFixaAberturaCollection();
            posicaoRendaFixaAberturaCollection.DeletaPosicaoRendaFixaAberturaDataHistoricoMaior(idCliente, data);
        }

        /// <summary>
        /// Deleta as posicoes de historico em PosicaoRendaFixa.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void DeletaPosicoesHistorico(int idCliente, DateTime data)
        {
            PosicaoRendaFixaHistoricoCollection posicaoRendaFixaHistoricoCollection = new PosicaoRendaFixaHistoricoCollection();
            posicaoRendaFixaHistoricoCollection.DeletaPosicaoRendaFixaHistoricoDataHistoricoMaiorIgual(idCliente, data);
        }

        /// <summary>
        /// Deleta as Memórias de Cáculo RendaFixa.
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void DeletaMemoriaCalculo(int idCliente, DateTime data, TipoProcessamento tipoProcessamento)
        {
            MemoriaCalculoRendaFixaCollection memoriaCalculoColl = new MemoriaCalculoRendaFixaCollection();
            MemoriaCalculoRendaFixaQuery memoriaCalculoQuery = new MemoriaCalculoRendaFixaQuery("memoria");
            OperacaoRendaFixaQuery operacaoRendaFixaQuery = new OperacaoRendaFixaQuery("operacao");

            #region Próprio dia (Tipo especifico)
            memoriaCalculoQuery.Where(memoriaCalculoQuery.IdCliente.Equal(idCliente) 
                                      & memoriaCalculoQuery.DataAtual.GreaterThanOrEqual(data)
                                      & memoriaCalculoQuery.TipoProcessamento.Equal((int)tipoProcessamento));

            if (memoriaCalculoColl.Load(memoriaCalculoQuery))
            {
                memoriaCalculoColl.MarkAllAsDeleted();
                memoriaCalculoColl.Save();
            }
            #endregion

            #region Tudo maior do que a data de processamento
            memoriaCalculoColl = new MemoriaCalculoRendaFixaCollection();
            memoriaCalculoQuery = new MemoriaCalculoRendaFixaQuery("memoria");
            operacaoRendaFixaQuery = new OperacaoRendaFixaQuery("operacao");

            memoriaCalculoQuery.Select(memoriaCalculoQuery);
            memoriaCalculoQuery.Where(memoriaCalculoQuery.IdCliente.Equal(idCliente)
                                      & memoriaCalculoQuery.DataAtual.GreaterThan(data));

            if (memoriaCalculoColl.Load(memoriaCalculoQuery))
            {
                memoriaCalculoColl.MarkAllAsDeleted();
                memoriaCalculoColl.Save();
            }
            #endregion

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idIndice"></param>
        /// <returns></returns>
        public int DescobreSerieDeNTN(short? idIndice)
        {
            if (idIndice.HasValue)
            {
                if (idIndice.Value == (int)ListaIndiceFixo.IGPM)
                    return (int)SerieNTN.ntnC;
                else if (idIndice.Value == (int)ListaIndiceFixo.IPCA)
                    return (int)SerieNTN.ntnB;

            }
            return (int)SerieNTN.ntnF;
        }    
        
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static List<int> RetornaListaClassesOffShore()
        {
            List<int> lstClasseOffShore = new List<int>();
            lstClasseOffShore.Add((int)ClasseRendaFixa.TBILL);
            lstClasseOffShore.Add((int)ClasseRendaFixa.GLOBALS);

            return lstClasseOffShore;
        }

        /// <summary>
        /// Calcula Prazo médio de RendaFixa
        /// </summary>
        /// <param name="data"></param>
        /// <param name="idCliente"></param>
        /// <param name="enumTipoProcessamento"></param>
        public void CalculaPrazoMedioRendaFixa(DateTime data, int idCliente, int enumTipoProcessamento)
        {
            PrazoMedioCollection prazoMedioColl = new PrazoMedioCollection();
            prazoMedioColl.DeletaPrazoMedioMaiorIgual(idCliente, data, TipoAtivoAuxiliar.OperacaoRendaFixa);

            Carteira carteira = new Carteira();
            carteira.LoadByPrimaryKey(idCliente);
            string calculaPrazoMedio = string.IsNullOrEmpty(carteira.CalculaPrazoMedio) ? string.Empty : carteira.CalculaPrazoMedio;

            if (!calculaPrazoMedio.Equals("S"))
                return;

            #region Objetos
            PosicaoRendaFixaQuery posicaoRendaFixaQuery;
            PosicaoRendaFixaCollection posicaoRendaFixaCollection;
            TituloRendaFixaQuery tituloRendaFixaQuery;
            PapelRendaFixaQuery papelRendaFixaQuery;
            IndiceQuery indiceQuery;
            MemoriaCalculoRendaFixaCollection memCalculoRFCollection;
            MemoriaCalculoRendaFixaQuery memCalculoRFQueryCurva;
            MemoriaCalculoRendaFixaQuery memCalculoRFQueryMTM;

            string fieldValorPresente = "ValorPresente";
            string fieldDescricaoTitulo = "DescricaoTitulo";
            string fieldDescricaoIndice = "DescricaoIndice";
            string fieldDtVencimento = "DataVencimento";
            string fieldTaxa = "Taxa";
            string fieldPercentual = "Percentual";
            string tableCalculoCurva = "memCalculoRFCurva";
            string tableCalculoMTM = "memCalculoRFMTM";
            #endregion

            List<int> lstClassesSemPrazoMedio = new List<int>();
            lstClassesSemPrazoMedio.Add((int)ClasseRendaFixa.CCB);
            lstClassesSemPrazoMedio.Add((int)ClasseRendaFixa.GLOBALS);
            lstClassesSemPrazoMedio.Add((int)ClasseRendaFixa.TBILL);

            List<int> lstTipoOperacaoSemMemCal = new List<int>();
            lstTipoOperacaoSemMemCal.Add((int)TipoOperacaoTitulo.CompraRevenda);
            lstTipoOperacaoSemMemCal.Add((int)TipoOperacaoTitulo.VendaRecompra);

            #region Carrega as Posições
            tituloRendaFixaQuery = new TituloRendaFixaQuery("titulo");
            posicaoRendaFixaQuery = new PosicaoRendaFixaQuery("posicao");
            papelRendaFixaQuery = new PapelRendaFixaQuery("papel");
            indiceQuery = new IndiceQuery("indice");
            posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
            posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery.Quantidade.Sum(),
                                         posicaoRendaFixaQuery.ValorCurva.Sum(),
                                         posicaoRendaFixaQuery.IdTitulo,
                                         posicaoRendaFixaQuery.IdCliente,
                                         posicaoRendaFixaQuery.TipoOperacao,
                                         tituloRendaFixaQuery.Descricao.As(fieldDescricaoTitulo),
                                         tituloRendaFixaQuery.Taxa.As(fieldTaxa),
                                         tituloRendaFixaQuery.Percentual.As(fieldPercentual),
                                         tituloRendaFixaQuery.DataVencimento.As(fieldDtVencimento),
                                         indiceQuery.Descricao.As(fieldDescricaoIndice));
            posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(posicaoRendaFixaQuery.IdTitulo.Equal(tituloRendaFixaQuery.IdTitulo));
            posicaoRendaFixaQuery.InnerJoin(papelRendaFixaQuery).On(tituloRendaFixaQuery.IdPapel.Equal(papelRendaFixaQuery.IdPapel));
            posicaoRendaFixaQuery.LeftJoin(indiceQuery).On(tituloRendaFixaQuery.IdIndice.Equal(indiceQuery.IdIndice));
            posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente.Equal(idCliente)
                                        & papelRendaFixaQuery.Classe.NotIn(lstClassesSemPrazoMedio.ToArray()));
            posicaoRendaFixaQuery.GroupBy(posicaoRendaFixaQuery.IdTitulo,
                                         posicaoRendaFixaQuery.IdCliente,
                                         posicaoRendaFixaQuery.TipoOperacao,
                                         tituloRendaFixaQuery.Descricao,
                                         tituloRendaFixaQuery.Taxa,
                                         tituloRendaFixaQuery.Percentual,
                                         tituloRendaFixaQuery.DataVencimento,
                                         indiceQuery.Descricao);
            posicaoRendaFixaQuery.OrderBy(posicaoRendaFixaQuery.IdTitulo.Ascending);
            #endregion

            int idTitulo;
            int prazoDC;
            int idTipoOperacao;
            List<int> lstPrazos;
            List<Decimal> lstValores;
            decimal valorPresente;
            decimal valorPosicao;
            string descricaoCompleta;

            if (posicaoRendaFixaCollection.Load(posicaoRendaFixaQuery))
            {
                foreach (PosicaoRendaFixa posicaoRendaFixa in posicaoRendaFixaCollection)
                {
                    idTitulo = posicaoRendaFixa.IdTitulo.Value;
                    idTipoOperacao = (posicaoRendaFixa.TipoOperacao.HasValue ? (int)posicaoRendaFixa.TipoOperacao.Value : 0);
                    lstPrazos = new List<int>();
                    lstValores = new List<decimal>();
                    valorPresente = 0;
                    valorPosicao = 0;

                    if (lstTipoOperacaoSemMemCal.Contains(idTipoOperacao))
                    {
                        prazoDC = Calendario.NumeroDias(data, posicaoRendaFixa.DataVencimento.Value);
                        valorPresente = posicaoRendaFixa.ValorCurva.Value;
                        descricaoCompleta = this.RetornaDescricaoCompleta(posicaoRendaFixa);

                        PrazoMedio prazoMedio = new PrazoMedio();
                        prazoMedio.InserePrazoMedio(data, idCliente, descricaoCompleta, (int)TipoAtivoAuxiliar.OperacaoRendaFixa, valorPresente, prazoDC, null);
                    }
                    else
                    {

                        #region Popula memória de cálculo
                        memCalculoRFCollection = new MemoriaCalculoRendaFixaCollection();
                        memCalculoRFQueryCurva = new MemoriaCalculoRendaFixaQuery(tableCalculoCurva);
                        memCalculoRFQueryMTM = new MemoriaCalculoRendaFixaQuery(tableCalculoMTM);
                        posicaoRendaFixaQuery = new PosicaoRendaFixaQuery("posicao");

                        memCalculoRFQueryCurva.Select(memCalculoRFQueryCurva.DataFluxo,
                                                      memCalculoRFQueryCurva.DataAtual,
                                                      posicaoRendaFixaQuery.IdTitulo,
                                                      memCalculoRFQueryMTM.ValorPresente.Sum().Coalesce(tableCalculoCurva + "." + fieldValorPresente).As(fieldValorPresente));
                        memCalculoRFQueryCurva.InnerJoin(posicaoRendaFixaQuery).On(memCalculoRFQueryCurva.IdOperacao.Equal(posicaoRendaFixaQuery.IdOperacao));
                        memCalculoRFQueryCurva.LeftJoin(memCalculoRFQueryMTM).On(memCalculoRFQueryCurva.DataAtual.Equal(memCalculoRFQueryMTM.DataAtual) &
                                                                                 memCalculoRFQueryCurva.DataFluxo.Equal(memCalculoRFQueryMTM.DataFluxo) &
                                                                                 memCalculoRFQueryCurva.IdOperacao.Equal(memCalculoRFQueryMTM.IdOperacao) &
                                                                                 memCalculoRFQueryMTM.TipoPreco.Equal((int)TipoPreco.MTM));
                        memCalculoRFQueryCurva.Where(memCalculoRFQueryCurva.TipoProcessamento.Equal(enumTipoProcessamento) &
                                                     memCalculoRFQueryCurva.DataAtual.Equal(data) &
                                                     memCalculoRFQueryCurva.TipoPreco.Equal((int)TipoPreco.Curva) &
                                                     posicaoRendaFixaQuery.IdCliente.Equal(idCliente) &
                                                     posicaoRendaFixaQuery.IdTitulo.Equal(idTitulo));
                        memCalculoRFQueryCurva.GroupBy(memCalculoRFQueryCurva.DataFluxo,
                                                       memCalculoRFQueryCurva.DataAtual,
                                                       posicaoRendaFixaQuery.IdTitulo);
                        memCalculoRFQueryCurva.OrderBy(memCalculoRFQueryCurva.DataFluxo.Ascending);
                        #endregion

                        lstPrazos = new List<int>();
                        lstValores = new List<decimal>();
                        if (memCalculoRFCollection.Load(memCalculoRFQueryCurva))
                        {
                            foreach (MemoriaCalculoRendaFixa memCalculoRendaFixa in memCalculoRFCollection)
                            {
                                prazoDC = Calendario.NumeroDias(data, memCalculoRendaFixa.DataFluxo.Value);
                                lstPrazos.Add(prazoDC);
                                valorPosicao = valorPresente = (decimal)memCalculoRendaFixa.GetColumn(fieldValorPresente);
                                valorPresente *= posicaoRendaFixa.Quantidade.Value;
                                lstValores.Add(valorPresente);
                            }

                            descricaoCompleta = this.RetornaDescricaoCompleta(posicaoRendaFixa);

                            PrazoMedio prazoMedio = new PrazoMedio();
                            prazoMedio.InserePrazoMedioPonderado(data, idCliente, descricaoCompleta, lstPrazos, lstValores, (int)TipoAtivoAuxiliar.OperacaoRendaFixa, null);

                        }

                    }
                }
            }
        }

        /// <summary>
        /// Calcula rentabilidade de Ativos de renda fixa
        /// </summary>
        /// <param name="data"></param>
        /// <param name="idCliente"></param>
        /// <param name="enumTipoProcessamento"></param>
        public void CalculaRentabilidadeRendaFixa(DateTime data, int idCliente)
        {
            #region Se carteira m�e n�o calcula rentabilidade ( precisa desenvolver )
            CarteiraMae carteiraMae = new CarteiraMae();
            if (carteiraMae.IsCarteiraMae(idCliente))
                return;
            #endregion

            #region EntitySpaces
            string tablePosicaoFechamento = "PosicaoFechamento";
            string tablePosicaoHistorico = "PosicaoHistorico";
            string tableOperacaoRendaFixaCredito = "OperacaoRendaFixaCredito";
            string tableOperacaoRendaFixaDebito = "OperacaoRendaFixaDebito";
            string tableTituloRendaFixa = "TituloRendaFixa";
            string tablePapelRendaFixa = "PapelRendaFixa";
            string tableIndice = "Indice";
            string tableLiquidacaoRendaFixa = "Liquidacao";
            string fieldDescricaoTitulo = "DescricaoTitulo";
            string fieldDescricaoIndice = "DescricaoIndice";
            string fieldDtVencimento = "DataVencimento";
            string fieldTaxa = "Taxa";
            string fieldPercentual = "Percentual";
            string fieldValorFechamento = "ValorFechamento";
            string fieldValorAbertura = "ValorAbertura";
            string fieldQtdeAbertura = "qtdeAbertura";
            string fieldQtdeFechamento = "qtdeFechamento";
            string fieldValorOperacao = "valorOperacao";
            string fieldValorIOFHistorico = "valorIOFHistorico";
            string fieldValorIOF = "valorIOFAtual";
            string fieldValorIRHistorico = "valorIRHistorico";
            string fieldValorIR = "valorIRAtual";
            string fieldValorGrossUpAbertura = "valorGrossUpAbertura";
            string fieldValorGrossUpFechamento = "valorGrossUpFechamento";
            #endregion

            #region Objetos
            Dictionary<int, string> dicMoedasAtivo = new Dictionary<int, string>();
            Dictionary<string, decimal> dicCotacao = new Dictionary<string, decimal>();
            Dictionary<string, Rentabilidade.RentabilidadeConversao> dicRentConversao = new Dictionary<string, Rentabilidade.RentabilidadeConversao>();
            List<OperacaoRendaFixa> lstOperacoesCredito = new List<OperacaoRendaFixa>();
            List<OperacaoRendaFixa> lstOperacoesDebito = new List<OperacaoRendaFixa>();
            List<LiquidacaoRendaFixa> lstLiquidacaoRendaFixa = new List<LiquidacaoRendaFixa>();
            List<Rentabilidade> lstRentabilidadeAnterior = new List<Rentabilidade>();
            List<int> lstEventosRendas = new List<int>();
            List<int> lstTipoOperacaoEntrada = new List<int>();
            List<int> lstTipoOperacaoSaida = new List<int>();
            //Querys
            PosicaoRendaFixaQuery posicaoRendaFixaQuery = new PosicaoRendaFixaQuery(tablePosicaoFechamento);
            PosicaoRendaFixaHistoricoQuery posicaoRendaFixaHistoricoQuery = new PosicaoRendaFixaHistoricoQuery(tablePosicaoHistorico);
            OperacaoRendaFixaQuery operacaoRendaFixaCreditoQuery = new OperacaoRendaFixaQuery(tableOperacaoRendaFixaCredito);
            OperacaoRendaFixaQuery operacaoRendaFixaDebitoQuery = new OperacaoRendaFixaQuery(tableOperacaoRendaFixaDebito);
            TituloRendaFixaQuery tituloRendaFixaQuery = new TituloRendaFixaQuery(tableTituloRendaFixa);
            PapelRendaFixaQuery papelRendaFixaQuery = new PapelRendaFixaQuery(tablePapelRendaFixa);
            IndiceQuery indiceQuery = new IndiceQuery(tableIndice);
            LiquidacaoRendaFixaQuery liquidacaoQuery = new LiquidacaoRendaFixaQuery(tableLiquidacaoRendaFixa);
            //Collection
            OperacaoRendaFixaCollection operacaoRendaFixaCreditoColl = new OperacaoRendaFixaCollection();
            OperacaoRendaFixaCollection operacaoRendaFixaDebitoColl = new OperacaoRendaFixaCollection();
            PosicaoRendaFixaCollection posicaoRendaFixaColl = new PosicaoRendaFixaCollection();
            RentabilidadeCollection rentabilidadeColl = new RentabilidadeCollection();
            LiquidacaoRendaFixaCollection liquidacaoColl = new LiquidacaoRendaFixaCollection();
            //Comuns        
            ConversaoMoeda conversaoMoeda = new ConversaoMoeda();
            TituloRendaFixa tituloRendaFixa = new TituloRendaFixa();
            CotacaoIndice cotacaoIndice = new CotacaoIndice();
            Cliente cliente = new Cliente();
            ClienteRendaFixa clienteRendaFixa = new ClienteRendaFixa();
            Moeda moeda = new Moeda();
            OperacaoRendaFixa operacaoAux;
            LiquidacaoRendaFixa liquidacaoAux;
            DateTime dataAnterior = new DateTime();
            Rentabilidade rentabilidade;
            #endregion

            #region Variaveis
            decimal valorFechamentoBruto = 0;
            decimal valorAberturaBruto = 0;
            decimal valorEntrada = 0;
            decimal valorSaidaLiquido = 0;
            decimal valorSaidaBruto = 0;            
            decimal valorRentabilidadeLiquida = 0;
            decimal qtdeEntrada = 0;
            decimal qtdeSaida = 0;
            decimal qtdeAbertura = 0;
            decimal qtdeFechamento = 0;
            decimal cambio = 0;
            decimal valorBruto = 0;
            decimal valorOperacao = 0;
            decimal valorIOFHistorico = 0;
            decimal valorIOF = 0;
            decimal valorIRHistorico = 0;
            decimal valorIR = 0;
            decimal valorRentabilidadeBrutaDiaria = 0;
            decimal valorRentabilidadeBrutaAcumulada = 0;
            string descricaoCompleta = string.Empty;
            string descricaoMoedaCarteira = string.Empty;
            string descricaoMoedaAtivo = string.Empty;
            string keyConversao = string.Empty;
            int idTitulo = 0;
            byte tipoOperacao = 0;
            short idMoedaAtivo = 0;
            short idMoedaCarteira = 0;
            byte tipoConversao;
            decimal valorGrossUpAbertura = 0;
            decimal valorGrossUpFechamento = 0;

            decimal valorFechamentoLiquido = 0;
            decimal valorAberturaLiquido = 0;
            decimal valorRentabilidadeGrossUpAcumulada = 0;
            decimal valorRentabilidadeGrossUpDiaria = 0;
            decimal valorRendaBruto = 0;
            decimal valorRendaLiquido = 0;
            decimal valorSaidaIOF = 0;
            decimal valorSaidaIR = 0;
            #endregion

            #region Popula Listas
            lstTipoOperacaoEntrada.Add((int)TipoOperacaoTitulo.CompraFinal);
            lstTipoOperacaoEntrada.Add((int)TipoOperacaoTitulo.CompraCasada);
            lstTipoOperacaoEntrada.Add((int)TipoOperacaoTitulo.CompraRevenda);
            lstTipoOperacaoEntrada.Add((int)TipoOperacaoTitulo.Deposito);
            lstTipoOperacaoEntrada.Add((int)TipoOperacaoTitulo.IngressoAtivoImpactoCota);
            lstTipoOperacaoEntrada.Add((int)TipoOperacaoTitulo.IngressoAtivoImpactoQtde);

            lstTipoOperacaoSaida.Add((int)TipoOperacaoTitulo.VendaFinal);
            lstTipoOperacaoSaida.Add((int)TipoOperacaoTitulo.VendaCasada);
            lstTipoOperacaoSaida.Add((int)TipoOperacaoTitulo.VendaTotal);
            lstTipoOperacaoSaida.Add((int)TipoOperacaoTitulo.Retirada);
            lstTipoOperacaoSaida.Add((int)TipoOperacaoTitulo.RetiradaAtivoImpactoCota);
            lstTipoOperacaoSaida.Add((int)TipoOperacaoTitulo.RetiradaAtivoImpactoQtde);

            lstEventosRendas.Add((int)TipoLancamentoLiquidacao.Juros);
            lstEventosRendas.Add((int)TipoLancamentoLiquidacao.Amortizacao);
            lstEventosRendas.Add((int)TipoLancamentoLiquidacao.PagtoPrincipal);
            #endregion

            #region Deleta Rentabilidade
            rentabilidadeColl.DeletaRentabilidadeMaiorIgual(idCliente, data, TipoAtivoAuxiliar.OperacaoRendaFixa);
            rentabilidadeColl.DeletaRentabilidadeMaiorIgual(idCliente, data, TipoAtivoAuxiliar.RendaFixaIncentivado);
            rentabilidadeColl.DeletaRentabilidadeMaiorIgual(idCliente, data, TipoAtivoAuxiliar.RendaFixaTributado); 

            #endregion

            #region Carrega Moeda e Carteira
            cliente.LoadByPrimaryKey(idCliente);
            clienteRendaFixa.LoadByPrimaryKey(idCliente);
            if (moeda.LoadByPrimaryKey(cliente.IdMoeda.Value))
            {
                descricaoMoedaCarteira = moeda.Nome;
                idMoedaCarteira = moeda.IdMoeda.Value;
            }
            #endregion

            if (cliente.IdLocal.Value != LocalFeriadoFixo.Brasil)
                dataAnterior = Calendario.SubtraiDiaUtil(data, 1, cliente.IdLocal.Value, TipoFeriado.Outros);
            else
                dataAnterior = Calendario.SubtraiDiaUtil(data, 1, LocalFeriadoFixo.Brasil, TipoFeriado.Brasil);

            #region Carrega as Posições
            posicaoRendaFixaQuery.Select(posicaoRendaFixaQuery.IdCliente,
                                         posicaoRendaFixaQuery.IdTitulo,
                                         posicaoRendaFixaQuery.TipoOperacao,
                                         posicaoRendaFixaHistoricoQuery.Quantidade.Sum().Coalesce(tablePosicaoHistorico + ".Quantidade").As(fieldQtdeAbertura),
                                         posicaoRendaFixaQuery.Quantidade.Sum().Coalesce("0").As(fieldQtdeFechamento),
                                         "< sum(" + tablePosicaoFechamento + ".[puMercado] * " + tablePosicaoFechamento + ".[Quantidade]) as " + fieldValorFechamento + ">",
                                         "< COALESCE(sum(" + tablePosicaoHistorico + ".[puMercado] * " + tablePosicaoHistorico + ".[Quantidade]), 0) as " + fieldValorAbertura + ">",
                                         tituloRendaFixaQuery.Descricao.As(fieldDescricaoTitulo),
                                         tituloRendaFixaQuery.Taxa.As(fieldTaxa),
                                         tituloRendaFixaQuery.Percentual.As(fieldPercentual),
                                         tituloRendaFixaQuery.DataVencimento.As(fieldDtVencimento),
                                         tituloRendaFixaQuery.IdMoeda,
                                         indiceQuery.Descricao.As(fieldDescricaoIndice),
                                         "< sum(" + tablePosicaoFechamento + ".[puOperacao] * " + tablePosicaoFechamento + ".[Quantidade]) as " + fieldValorOperacao + ">",
                                         posicaoRendaFixaQuery.ValorBrutoGrossUp.Sum().As(fieldValorGrossUpFechamento),
                                         posicaoRendaFixaHistoricoQuery.ValorBrutoGrossUp.Sum().As(fieldValorGrossUpAbertura),
                                         posicaoRendaFixaQuery.ValorIOF.Sum().As(fieldValorIOF),
                                         posicaoRendaFixaQuery.ValorIR.Sum().As(fieldValorIR),
                                         posicaoRendaFixaHistoricoQuery.ValorIOF.Sum().As(fieldValorIOFHistorico),
                                         posicaoRendaFixaHistoricoQuery.ValorIR.Sum().As(fieldValorIRHistorico));
            posicaoRendaFixaQuery.InnerJoin(tituloRendaFixaQuery).On(posicaoRendaFixaQuery.IdTitulo.Equal(tituloRendaFixaQuery.IdTitulo));
            posicaoRendaFixaQuery.InnerJoin(papelRendaFixaQuery).On(tituloRendaFixaQuery.IdPapel.Equal(papelRendaFixaQuery.IdPapel));
            posicaoRendaFixaQuery.LeftJoin(indiceQuery).On(tituloRendaFixaQuery.IdIndice.Equal(indiceQuery.IdIndice));
            posicaoRendaFixaQuery.LeftJoin(posicaoRendaFixaHistoricoQuery).On(posicaoRendaFixaHistoricoQuery.IdPosicao.Equal(posicaoRendaFixaQuery.IdPosicao)
                                                                            & posicaoRendaFixaHistoricoQuery.DataHistorico.Equal("'" + dataAnterior.ToString("yyyyMMdd") + "'"));
            posicaoRendaFixaQuery.Where(posicaoRendaFixaQuery.IdCliente.Equal(idCliente));
            posicaoRendaFixaQuery.GroupBy(posicaoRendaFixaQuery.IdCliente,
                                         posicaoRendaFixaQuery.IdTitulo,
                                         posicaoRendaFixaQuery.TipoOperacao,
                                         tituloRendaFixaQuery.Descricao.As(fieldDescricaoTitulo),
                                         tituloRendaFixaQuery.Taxa.As(fieldTaxa),
                                         tituloRendaFixaQuery.Percentual.As(fieldPercentual),
                                         tituloRendaFixaQuery.DataVencimento.As(fieldDtVencimento),
                                         tituloRendaFixaQuery.IdMoeda,
                                         indiceQuery.Descricao.As(fieldDescricaoIndice));
            posicaoRendaFixaQuery.OrderBy(posicaoRendaFixaQuery.IdTitulo.Ascending);

            if (!posicaoRendaFixaColl.Load(posicaoRendaFixaQuery))
                return;
            #endregion

            #region Carrega as Operações que movimentaram a Operação na data de processamento (Crédito)
            operacaoRendaFixaCreditoQuery.Select(operacaoRendaFixaCreditoQuery.IdCliente,
                                                 operacaoRendaFixaCreditoQuery.IdTitulo,
                                                 "< sum(" + tableOperacaoRendaFixaCredito + ".[puOperacao] * " + tableOperacaoRendaFixaCredito + ".[Quantidade]) as Valor >",
                                                 operacaoRendaFixaCreditoQuery.Quantidade.Sum());
            operacaoRendaFixaCreditoQuery.Where(operacaoRendaFixaCreditoQuery.DataRegistro.Equal(data)
                                               & operacaoRendaFixaCreditoQuery.IdCliente.Equal(idCliente)
                                               & operacaoRendaFixaCreditoQuery.TipoOperacao.In(lstTipoOperacaoEntrada.ToArray()));
            operacaoRendaFixaCreditoQuery.GroupBy(operacaoRendaFixaCreditoQuery.IdCliente,
                                                  operacaoRendaFixaCreditoQuery.IdTitulo);
            operacaoRendaFixaCreditoQuery.OrderBy(operacaoRendaFixaCreditoQuery.IdTitulo.Ascending);

            if (operacaoRendaFixaCreditoColl.Load(operacaoRendaFixaCreditoQuery))
                lstOperacoesCredito = (List<OperacaoRendaFixa>)operacaoRendaFixaCreditoColl;
            #endregion

            #region Carrega as Operações que movimentaram a Operação na data de processamento (Débito)
            operacaoRendaFixaDebitoQuery.Select(operacaoRendaFixaDebitoQuery.IdCliente,
                                                operacaoRendaFixaDebitoQuery.IdTitulo,
                                                operacaoRendaFixaDebitoQuery.ValorIR.Sum(),
                                                operacaoRendaFixaDebitoQuery.ValorIOF.Sum(),
                                                 "< sum(" + tableOperacaoRendaFixaDebito + ".[puOperacao] * " + tableOperacaoRendaFixaDebito + ".[Quantidade]) as Valor >",
                                                operacaoRendaFixaDebitoQuery.Quantidade.Sum());
            operacaoRendaFixaDebitoQuery.Where(operacaoRendaFixaDebitoQuery.DataRegistro.Equal(data)
                                               & operacaoRendaFixaDebitoQuery.IdCliente.Equal(idCliente)
                                               & operacaoRendaFixaDebitoQuery.TipoOperacao.In(lstTipoOperacaoSaida.ToArray()));
            operacaoRendaFixaDebitoQuery.GroupBy(operacaoRendaFixaDebitoQuery.IdCliente,
                                                  operacaoRendaFixaDebitoQuery.IdTitulo);
            operacaoRendaFixaDebitoQuery.OrderBy(operacaoRendaFixaDebitoQuery.IdTitulo.Ascending);

            if (operacaoRendaFixaDebitoColl.Load(operacaoRendaFixaDebitoQuery))
                lstOperacoesDebito = (List<OperacaoRendaFixa>)operacaoRendaFixaDebitoColl;
            #endregion

            #region Carrega Rendas dos Ativos
            liquidacaoQuery.Select(liquidacaoQuery.IdCliente,
                                   liquidacaoQuery.IdTitulo,
                                   liquidacaoQuery.ValorBruto.Sum(),
                                   liquidacaoQuery.ValorLiquido.Sum());
            liquidacaoQuery.Where(liquidacaoQuery.IdCliente.Equal(idCliente)
                                  & liquidacaoQuery.DataLiquidacao.Equal(data)
                                  & liquidacaoQuery.TipoLancamento.In(lstEventosRendas.ToArray()));
            liquidacaoQuery.GroupBy(liquidacaoQuery.IdCliente,
                                    liquidacaoQuery.IdTitulo);
            liquidacaoQuery.OrderBy(liquidacaoQuery.IdTitulo.Ascending);

            if (liquidacaoColl.Load(liquidacaoQuery))
                lstLiquidacaoRendaFixa = (List<LiquidacaoRendaFixa>)liquidacaoColl;
            #endregion

            #region Carrega Rentabilidade do dia anterior
            RentabilidadeCollection rentabilidadeAntCollection = new RentabilidadeCollection();
            rentabilidadeAntCollection.Query.Where(rentabilidadeAntCollection.Query.IdCliente.Equal(idCliente) &
                                                   rentabilidadeAntCollection.Query.TipoAtivo.In((int)TipoAtivoAuxiliar.RendaFixaIncentivado, (int)TipoAtivoAuxiliar.RendaFixaTributado) &
                                                   rentabilidadeAntCollection.Query.Data.Equal(dataAnterior));

            if (rentabilidadeAntCollection.Query.Load())
                lstRentabilidadeAnterior = (List<Rentabilidade>)rentabilidadeAntCollection; 
            #endregion

            foreach (PosicaoRendaFixa posicaoRendaFixa in posicaoRendaFixaColl)
            {
                idTitulo = posicaoRendaFixa.IdTitulo.Value;
                idMoedaAtivo = Convert.ToInt16(posicaoRendaFixa.GetColumn(TituloRendaFixaMetadata.ColumnNames.IdMoeda));
                tipoOperacao = posicaoRendaFixa.TipoOperacao.Value;
                valorFechamentoBruto = 0;
                valorAberturaBruto = 0;
                valorEntrada = 0;
                valorSaidaBruto = 0;
                valorSaidaLiquido = 0;
                valorRendaLiquido = 0;
                valorRendaBruto = 0;
                valorRentabilidadeLiquida = 0;
                qtdeEntrada = 0;
                qtdeSaida = 0;
                qtdeAbertura = 0;
                qtdeFechamento = 0;
                valorOperacao = 0;
                valorBruto = 0;
                valorIOF = 0;
                valorIR = 0;
                valorIOFHistorico = 0;
                valorIRHistorico = 0;
                valorRentabilidadeBrutaDiaria = 0;
                valorRentabilidadeBrutaAcumulada = 0;
                valorGrossUpFechamento = 0;
                valorGrossUpAbertura = 0;
                valorFechamentoLiquido = 0;
                valorAberturaLiquido = 0;
                valorRentabilidadeGrossUpAcumulada = 0;
                valorRentabilidadeGrossUpDiaria = 0;
                valorSaidaIOF = 0;
                valorSaidaIR = 0;

                tituloRendaFixa = new TituloRendaFixa();
                tituloRendaFixa.LoadByPrimaryKey(idTitulo);

                #region Carrega moeda do Ativo
                if (!dicMoedasAtivo.ContainsKey(idMoedaAtivo))
                {
                    moeda = new Moeda();
                    moeda.LoadByPrimaryKey(idMoedaAtivo);
                    dicMoedasAtivo.Add(moeda.IdMoeda.Value, moeda.Nome);
                }
                descricaoMoedaAtivo = dicMoedasAtivo[idMoedaAtivo];
                #endregion

                #region Verifica se o título é incentivado
                int tipoAtivo = (int)TipoAtivoAuxiliar.RendaFixaTributado;
                decimal? aliquotaExcecaoTributaria = null;
                if (ControllerRendaFixa.ClienteIsentoIR(cliente, data, clienteRendaFixa, tituloRendaFixa, null, tipoOperacao, out aliquotaExcecaoTributaria))
                {
                    tipoAtivo = (int)TipoAtivoAuxiliar.RendaFixaIncentivado;
                }
                #endregion

                #region Operações Entrada
                operacaoAux = new OperacaoRendaFixa();
                operacaoAux = lstOperacoesCredito.Find(delegate(OperacaoRendaFixa x) { return x.IdTitulo == idTitulo; });
                if (operacaoAux != null && operacaoAux.IdTitulo.Value > 0)
                {
                    valorEntrada = valorEntrada == 0 ? Convert.ToDecimal(operacaoAux.Valor.Value) : valorEntrada;
                    qtdeEntrada = Convert.ToDecimal(operacaoAux.Quantidade.Value);
                }
                else
                {
                    valorEntrada = 0;
                    qtdeEntrada = 0;
                }
                #endregion

                #region Operações Saida
                operacaoAux = new OperacaoRendaFixa();
                operacaoAux = lstOperacoesDebito.Find(delegate(OperacaoRendaFixa x) { return x.IdTitulo == idTitulo; });
                if (operacaoAux != null && operacaoAux.IdTitulo.Value > 0)
                {
                    valorSaidaLiquido = valorSaidaBruto = Convert.ToDecimal(operacaoAux.Valor.Value);
                    qtdeSaida = Convert.ToDecimal(operacaoAux.Quantidade.Value);
                    valorSaidaIOF = Convert.ToDecimal(operacaoAux.ValorIOF.GetValueOrDefault(0));
                    valorSaidaIR = Convert.ToDecimal(operacaoAux.ValorIR.GetValueOrDefault(0));

                }
                else
                {
                    valorSaidaLiquido = 0;
                    valorSaidaBruto = 0;
                    qtdeSaida = 0;
                    valorSaidaIOF = 0;
                    valorSaidaIR = 0;
                }
                #endregion

                #region Rendas
                liquidacaoAux = new LiquidacaoRendaFixa();
                liquidacaoAux = lstLiquidacaoRendaFixa.Find(delegate(LiquidacaoRendaFixa x) { return x.IdTitulo == idTitulo && x.IdCliente == idCliente; });
                if (liquidacaoAux != null && liquidacaoAux.IdTitulo.Value > 0)
                {
                    valorRendaLiquido = Convert.ToDecimal(liquidacaoAux.ValorLiquido.Value);
                    valorRendaBruto = Convert.ToDecimal(liquidacaoAux.ValorBruto.Value);
                }
                else
                {
                    valorRendaBruto = 0;
                    valorRendaLiquido = 0;
                }
                #endregion

                if (!Decimal.TryParse(posicaoRendaFixa.GetColumn(fieldValorFechamento).ToString(), out valorFechamentoBruto))
                    valorFechamentoBruto = 0;

                if (!Decimal.TryParse(posicaoRendaFixa.GetColumn(fieldValorAbertura).ToString(), out valorAberturaBruto))
                    valorAberturaBruto = 0;

                if (!Decimal.TryParse(posicaoRendaFixa.GetColumn(fieldQtdeAbertura).ToString(), out qtdeAbertura))
                    qtdeAbertura = 0;

                if (!Decimal.TryParse(posicaoRendaFixa.GetColumn(fieldQtdeFechamento).ToString(), out qtdeFechamento))
                    qtdeFechamento = 0;

                if (!Decimal.TryParse(posicaoRendaFixa.GetColumn(fieldValorOperacao).ToString(), out valorOperacao))
                    valorOperacao = 0;

                if (!Decimal.TryParse(posicaoRendaFixa.GetColumn(fieldValorIOF).ToString(), out valorIOF))
                    valorIOF = 0;

                if (!Decimal.TryParse(posicaoRendaFixa.GetColumn(fieldValorIR).ToString(), out valorIR))
                    valorIR = 0;

                if (!Decimal.TryParse(posicaoRendaFixa.GetColumn(fieldValorIOFHistorico).ToString(), out valorIOFHistorico))
                    valorIOFHistorico = 0;

                if (!Decimal.TryParse(posicaoRendaFixa.GetColumn(fieldValorIRHistorico).ToString(), out valorIRHistorico))
                    valorIRHistorico = 0;

                if (!Decimal.TryParse(posicaoRendaFixa.GetColumn(fieldValorGrossUpAbertura).ToString(), out valorGrossUpAbertura))
                    valorGrossUpAbertura = 0;

                if (!Decimal.TryParse(posicaoRendaFixa.GetColumn(fieldValorGrossUpFechamento).ToString(), out valorGrossUpFechamento))
                    valorGrossUpFechamento = 0;

                //Subtrai impostos para chegar no valor liquido
                valorFechamentoLiquido = valorFechamentoBruto - (valorIR + valorIOF);
                valorAberturaLiquido = valorAberturaBruto - (valorIRHistorico + valorIOFHistorico);

                #region Rentabilidade Liquida
                try
                {
                    valorRentabilidadeLiquida = ((valorFechamentoLiquido + valorSaidaLiquido) / (valorAberturaLiquido + valorEntrada - valorRendaLiquido)) - 1;
                }
                catch (Exception ex)
                {
                    valorRentabilidadeLiquida = 0;
                }
                #endregion

                #region Rentabilidade Bruta

                #region Rentabilidade com GrossUp
                if (tipoAtivo == (int)TipoAtivoAuxiliar.RendaFixaIncentivado && valorGrossUpFechamento != 0)
                {
                    decimal valorSaidaGrossUp = valorSaidaBruto;
                    decimal valorRendaGrossUp = valorRendaBruto;

                    //Valor Bruto é igual ao valor liquido, quando o título é grossup
                    valorFechamentoBruto = valorFechamentoLiquido;
                    valorAberturaBruto = valorAberturaLiquido;
                    valorAberturaBruto = valorAberturaLiquido;                    
                    valorRendaBruto = valorRendaLiquido;
                    
                    if(qtdeFechamento == 0 )
                    {
                        valorGrossUpFechamento = qtdeFechamento == 0 ? 0 : valorGrossUpFechamento;
                        valorSaidaGrossUp = valorSaidaGrossUp + (valorSaidaIOF + valorSaidaIR);
                    }
                    //Diária GrossUP
                    try
                    {
                        //valorRentabilidadeGrossUpDiaria = ((valorGrossUpFechamento + valorSaidaGrossUp) / (valorGrossUpAbertura + valorEntrada - valorRendaGrossUp)) - 1;
                        valorRentabilidadeGrossUpDiaria = valorRentabilidadeLiquida / (decimal)0.85;
                    }
                    catch (Exception ex)
                    {
                        valorRentabilidadeGrossUpDiaria = 0;
                    }

                }
                #endregion

                #region Rentabilidade Sem GrossUp

                //Diária
                try
                {
                    valorRentabilidadeBrutaDiaria = ((valorFechamentoBruto + valorSaidaBruto) / (valorAberturaBruto + valorEntrada - valorRendaBruto)) - 1;
                }
                catch (Exception ex)
                {
                    valorRentabilidadeBrutaDiaria = 0;
                }

                //Valor Bruto
                valorBruto = valorFechamentoBruto;

                #endregion

                #endregion

                #region Rentabilidade Acumulada
                valorRentabilidadeBrutaAcumulada = valorRentabilidadeBrutaDiaria;
                valorRentabilidadeGrossUpAcumulada = valorRentabilidadeGrossUpDiaria;

                Rentabilidade rentabilidadeAux = new Rentabilidade();
                rentabilidadeAux = lstRentabilidadeAnterior.Find(delegate(Rentabilidade x) { return x.IdTituloRendaFixa == idTitulo; });
                if (rentabilidadeAux != null && rentabilidadeAux.IdTituloRendaFixa.Value > 0)
                {
                    if (rentabilidadeAux.RentabilidadeBrutaAcumMoedaPortfolio.GetValueOrDefault(0) > 0)
                        valorRentabilidadeBrutaAcumulada = ((1 + rentabilidadeAux.RentabilidadeBrutaAcumMoedaPortfolio.Value) / 100) * ((1 + valorRentabilidadeBrutaDiaria) / 100);

                    if (rentabilidadeAux.RentabilidadeGrossUpAcumMoedaAtivo.GetValueOrDefault(0) > 0)
                        valorRentabilidadeGrossUpAcumulada = ((1 + rentabilidadeAux.RentabilidadeGrossUpAcumMoedaAtivo.Value) / 100) * ((1 + valorRentabilidadeGrossUpDiaria) / 100);
                }

                #endregion

                descricaoCompleta = this.RetornaDescricaoCompleta(posicaoRendaFixa);

                #region Popula objeto
                rentabilidade = rentabilidadeColl.AddNew();
                rentabilidade.CodigoAtivo = descricaoCompleta;
                rentabilidade.TipoAtivo = tipoAtivo;
                rentabilidade.Data = data;
                rentabilidade.IdCliente = idCliente;
                rentabilidade.IdTituloRendaFixa = idTitulo;
                rentabilidade.QuantidadeFinalAtivo = qtdeFechamento;
                rentabilidade.QuantidadeInicialAtivo = qtdeAbertura;
                rentabilidade.QuantidadeTotalEntradaAtivo = qtdeEntrada;
                rentabilidade.QuantidadeTotalSaidaAtivo = qtdeSaida;

                //Ativo
                rentabilidade.CodigoMoedaAtivo = descricaoMoedaAtivo;
                rentabilidade.PatrimonioFinalMoedaPortfolio = rentabilidade.PatrimonioFinalMoedaAtivo = valorFechamentoLiquido;
                rentabilidade.PatrimonioInicialMoedaPortfolio = rentabilidade.PatrimonioInicialMoedaAtivo = valorAberturaLiquido;
                rentabilidade.RentabilidadeMoedaPortfolio = rentabilidade.RentabilidadeMoedaAtivo = valorRentabilidadeLiquida;
                rentabilidade.ValorFinanceiroEntradaMoedaPortfolio = rentabilidade.ValorFinanceiroEntradaMoedaAtivo = valorEntrada;
                rentabilidade.ValorFinanceiroSaidaMoedaPortfolio = rentabilidade.ValorFinanceiroSaidaMoedaAtivo = valorSaidaLiquido;
                rentabilidade.ValorFinanceiroRendasMoedaPortfolio = rentabilidade.ValorFinanceiroRendasMoedaAtivo = valorRendaLiquido;
                rentabilidade.RentabilidadeBrutaDiariaMoedaAtivo = rentabilidade.RentabilidadeBrutaDiariaMoedaPortfolio = valorRentabilidadeBrutaDiaria;
                rentabilidade.RentabilidadeBrutaAcumMoedaAtivo = rentabilidade.RentabilidadeBrutaAcumMoedaPortfolio = valorRentabilidadeBrutaAcumulada;
                rentabilidade.RentabilidadeGrossUpDiariaMoedaAtivo = rentabilidade.RentabilidadeGrossUpDiariaMoedaPortfolio = valorRentabilidadeGrossUpDiaria;
                rentabilidade.RentabilidadeGrossUpAcumMoedaAtivo = rentabilidade.RentabilidadeGrossUpAcumMoedaPortfolio = valorRentabilidadeGrossUpAcumulada;
                rentabilidade.PatrimonioFinalBrutoMoedaAtivo = rentabilidade.PatrimonioFinalBrutoMoedaPortfolio = valorBruto;
                rentabilidade.PatrimonioInicialBrutoMoedaAtivo = rentabilidade.PatrimonioInicialBrutoMoedaPortfolio = valorAberturaBruto;
                //Soma o patrimonio bruto de um título, mesmo que não seja grossup
                rentabilidade.PatrimonioFinalGrossUpMoedaAtivo = rentabilidade.PatrimonioFinalGrossUpMoedaPortfolio = (valorGrossUpFechamento == 0 ? valorBruto : valorGrossUpFechamento);
                rentabilidade.PatrimonioInicialGrossUpMoedaPortfolio = rentabilidade.PatrimonioInicialGrossUpMoedaAtivo = (valorGrossUpAbertura == 0 ? valorAberturaBruto : valorGrossUpAbertura);

                //Portfolio
                rentabilidade.CodigoMoedaPortfolio = descricaoMoedaCarteira;

                //Converte os valores para a moeda da carteira
                rentabilidade.ConverteValores(idMoedaAtivo, idMoedaCarteira, ref dicRentConversao);
                #endregion

            }

            #region Vencimento Renda Fixa            
            PosicaoRendaFixaHistoricoCollection posicaoRendaFixaHistoricoColl = new PosicaoRendaFixaHistoricoCollection();
            posicaoRendaFixaHistoricoQuery = new PosicaoRendaFixaHistoricoQuery(tablePosicaoHistorico);
            LiquidacaoRendaFixaQuery liquidacaoRendaFixaQuery = new LiquidacaoRendaFixaQuery("liqRendaFixa");

            posicaoRendaFixaHistoricoQuery.Select(  liquidacaoRendaFixaQuery.Quantidade.Sum(),
                                                    liquidacaoRendaFixaQuery.ValorBruto.Sum(),
                                                    liquidacaoRendaFixaQuery.ValorLiquido.Sum(),
                                                    posicaoRendaFixaHistoricoQuery.ValorMercado.Sum(),
                                                    posicaoRendaFixaHistoricoQuery.ValorIR.Sum(),
                                                    posicaoRendaFixaHistoricoQuery.ValorIOF.Sum(),
                                                    posicaoRendaFixaHistoricoQuery.ValorBrutoGrossUp.Sum(),
                                                    posicaoRendaFixaHistoricoQuery.IdCliente,
                                                    posicaoRendaFixaHistoricoQuery.IdTitulo,
                                                    posicaoRendaFixaHistoricoQuery.TipoOperacao,                                                    
                                                    tituloRendaFixaQuery.Descricao.As(fieldDescricaoTitulo),
                                                    tituloRendaFixaQuery.Taxa.As(fieldTaxa),
                                                    tituloRendaFixaQuery.Percentual.As(fieldPercentual),
                                                    tituloRendaFixaQuery.DataVencimento.As(fieldDtVencimento),
                                                    tituloRendaFixaQuery.IdMoeda,
                                                    indiceQuery.Descricao.As(fieldDescricaoIndice));
            posicaoRendaFixaHistoricoQuery.InnerJoin(tituloRendaFixaQuery).On(posicaoRendaFixaHistoricoQuery.IdTitulo.Equal(tituloRendaFixaQuery.IdTitulo));
            posicaoRendaFixaHistoricoQuery.InnerJoin(papelRendaFixaQuery).On(tituloRendaFixaQuery.IdPapel.Equal(papelRendaFixaQuery.IdPapel));
            posicaoRendaFixaHistoricoQuery.InnerJoin(liquidacaoRendaFixaQuery).On(posicaoRendaFixaHistoricoQuery.IdPosicao.Equal(liquidacaoRendaFixaQuery.IdPosicaoResgatada));
            posicaoRendaFixaHistoricoQuery.LeftJoin(indiceQuery).On(tituloRendaFixaQuery.IdIndice.Equal(indiceQuery.IdIndice));
            posicaoRendaFixaHistoricoQuery.Where(posicaoRendaFixaHistoricoQuery.IdCliente.Equal(idCliente) & 
                                                liquidacaoRendaFixaQuery.TipoLancamento.Equal((int)TipoLancamentoLiquidacao.Vencimento) &
                                                liquidacaoRendaFixaQuery.DataLiquidacao.Equal(data) &
                                                posicaoRendaFixaHistoricoQuery.DataHistorico.Equal(dataAnterior));
            posicaoRendaFixaHistoricoQuery.GroupBy(posicaoRendaFixaHistoricoQuery.IdCliente,
                                         posicaoRendaFixaHistoricoQuery.IdTitulo,
                                         posicaoRendaFixaHistoricoQuery.TipoOperacao,
                                         tituloRendaFixaQuery.Descricao.As(fieldDescricaoTitulo),
                                         tituloRendaFixaQuery.Taxa.As(fieldTaxa),
                                         tituloRendaFixaQuery.Percentual.As(fieldPercentual),
                                         tituloRendaFixaQuery.DataVencimento.As(fieldDtVencimento),
                                         tituloRendaFixaQuery.IdMoeda,
                                         indiceQuery.Descricao.As(fieldDescricaoIndice));
            posicaoRendaFixaHistoricoQuery.OrderBy(posicaoRendaFixaHistoricoQuery.IdTitulo.Ascending);

            if (posicaoRendaFixaHistoricoColl.Load(posicaoRendaFixaHistoricoQuery))
            {
                foreach (PosicaoRendaFixaHistorico posicaoRendaFixaHistorico in posicaoRendaFixaHistoricoColl)
                {
                    idTitulo = posicaoRendaFixaHistorico.IdTitulo.Value;
                    idMoedaAtivo = Convert.ToInt16(posicaoRendaFixaHistorico.GetColumn(TituloRendaFixaMetadata.ColumnNames.IdMoeda));
                    tipoOperacao = posicaoRendaFixaHistorico.TipoOperacao.Value;

                    #region Carrega moeda do Ativo
                    if (!dicMoedasAtivo.ContainsKey(idMoedaAtivo))
                    {
                        moeda = new Moeda();
                        moeda.LoadByPrimaryKey(idMoedaAtivo);
                        dicMoedasAtivo.Add(moeda.IdMoeda.Value, moeda.Nome);
                    }
                    descricaoMoedaAtivo = dicMoedasAtivo[idMoedaAtivo];
                    #endregion

                    #region Verifica se o título é incentivado
                    int tipoAtivo = (int)TipoAtivoAuxiliar.RendaFixaTributado;
                    decimal? aliquotaExcecaoTributaria = null;
                    if (ControllerRendaFixa.ClienteIsentoIR(cliente, data, clienteRendaFixa, tituloRendaFixa, null, tipoOperacao, out aliquotaExcecaoTributaria) ||
                        ControllerRendaFixa.ClienteIsentoIOF(cliente, clienteRendaFixa, tituloRendaFixa))
                    {
                        tipoAtivo = (int)TipoAtivoAuxiliar.RendaFixaIncentivado;
                    }
                    #endregion                    

                    decimal valorLiquidoVencimento, valorBrutoVencimento;
                    if (!Decimal.TryParse(posicaoRendaFixaHistorico.GetColumn(LiquidacaoRendaFixaMetadata.ColumnNames.Quantidade).ToString(), out qtdeFechamento))
                        qtdeFechamento = 0;

                    if (!Decimal.TryParse(posicaoRendaFixaHistorico.GetColumn(LiquidacaoRendaFixaMetadata.ColumnNames.ValorLiquido).ToString(), out valorLiquidoVencimento))
                        valorLiquidoVencimento = 0;

                    if (!Decimal.TryParse(posicaoRendaFixaHistorico.GetColumn(LiquidacaoRendaFixaMetadata.ColumnNames.ValorBruto).ToString(), out valorBrutoVencimento))
                        valorBrutoVencimento = 0;

                    valorFechamentoBruto = 0;
                    valorFechamentoLiquido = 0;
                    valorRendaLiquido = 0;
                    valorEntrada = 0;
                    valorGrossUpAbertura = posicaoRendaFixaHistorico.ValorBrutoGrossUp.GetValueOrDefault(0);
                    valorIRHistorico = posicaoRendaFixaHistorico.ValorIR.GetValueOrDefault(0);
                    valorIOFHistorico = posicaoRendaFixaHistorico.ValorIOF.GetValueOrDefault(0);
                    valorAberturaBruto = posicaoRendaFixaHistorico.ValorMercado.GetValueOrDefault(0);

                    //Subtrai impostos para chegar no valor liquido
                    valorAberturaLiquido = valorAberturaBruto - valorIOFHistorico - valorIRHistorico;
                    
                    #region Rentabilidade Liquida
                    try
                    {
                        valorRentabilidadeLiquida = ((valorFechamentoLiquido + valorLiquidoVencimento) / (valorAberturaLiquido + valorEntrada - valorRendaLiquido)) - 1;
                    }
                    catch (Exception ex)
                    {
                        valorRentabilidadeLiquida = 0;
                    }
                    #endregion

                    #region Rentabilidade Bruta

                    #region Rentabilidade com GrossUp
                    if (tipoAtivo == (int)TipoAtivoAuxiliar.RendaFixaIncentivado && valorGrossUpAbertura != 0)
                    {
                        //Diária GrossUP
                        try
                        {                            
                            valorRentabilidadeGrossUpDiaria = valorRentabilidadeLiquida / (decimal)0.85;
                        }
                        catch (Exception ex)
                        {
                            valorRentabilidadeGrossUpDiaria = 0;
                        }
                    }
                    #endregion

                    #region Rentabilidade Sem GrossUp

                    //Diária
                    try
                    {
                        valorRentabilidadeBrutaDiaria = ((valorFechamentoBruto + valorBrutoVencimento) / (valorAberturaBruto + valorEntrada - valorRendaBruto)) - 1;
                    }
                    catch (Exception ex)
                    {
                        valorRentabilidadeBrutaDiaria = 0;
                    }

                    #endregion

                    #endregion

                    #region Rentabilidade Acumulada
                    valorRentabilidadeBrutaAcumulada = valorRentabilidadeBrutaDiaria;
                    valorRentabilidadeGrossUpAcumulada = valorRentabilidadeGrossUpDiaria;

                    Rentabilidade rentabilidadeAux = new Rentabilidade();
                    rentabilidadeAux = lstRentabilidadeAnterior.Find(delegate(Rentabilidade x) { return x.IdTituloRendaFixa == idTitulo; });
                    if (rentabilidadeAux != null && rentabilidadeAux.IdTituloRendaFixa.Value > 0)
                    {
                        if (rentabilidadeAux.RentabilidadeBrutaAcumMoedaPortfolio.GetValueOrDefault(0) > 0)
                            valorRentabilidadeBrutaAcumulada = ((1 + rentabilidadeAux.RentabilidadeBrutaAcumMoedaPortfolio.Value) / 100) * ((1 + valorRentabilidadeBrutaDiaria) / 100);

                        if (rentabilidadeAux.RentabilidadeGrossUpAcumMoedaAtivo.GetValueOrDefault(0) > 0)
                            valorRentabilidadeGrossUpAcumulada = ((1 + rentabilidadeAux.RentabilidadeGrossUpAcumMoedaAtivo.Value) / 100) * ((1 + valorRentabilidadeGrossUpDiaria) / 100);
                    }

                    #endregion

                    descricaoCompleta = "Vencimento " + this.RetornaDescricaoCompleta(posicaoRendaFixaHistorico);

                    #region Popula objeto
                    rentabilidade = rentabilidadeColl.AddNew();
                    rentabilidade.CodigoAtivo = descricaoCompleta;
                    rentabilidade.TipoAtivo = tipoAtivo;
                    rentabilidade.Data = data;
                    rentabilidade.IdCliente = idCliente;
                    rentabilidade.IdTituloRendaFixa = idTitulo;
                    rentabilidade.QuantidadeFinalAtivo = 0;
                    rentabilidade.QuantidadeInicialAtivo = qtdeFechamento;
                    rentabilidade.QuantidadeTotalEntradaAtivo = 0;
                    rentabilidade.QuantidadeTotalSaidaAtivo = qtdeFechamento;

                    //Ativo
                    rentabilidade.CodigoMoedaAtivo = descricaoMoedaAtivo;
                    rentabilidade.PatrimonioFinalMoedaPortfolio = rentabilidade.PatrimonioFinalMoedaAtivo = 0;
                    rentabilidade.PatrimonioInicialMoedaPortfolio = rentabilidade.PatrimonioInicialMoedaAtivo = valorAberturaLiquido;
                    rentabilidade.RentabilidadeMoedaPortfolio = rentabilidade.RentabilidadeMoedaAtivo = valorRentabilidadeLiquida;
                    rentabilidade.ValorFinanceiroEntradaMoedaPortfolio = rentabilidade.ValorFinanceiroEntradaMoedaAtivo = 0;
                    rentabilidade.ValorFinanceiroSaidaMoedaPortfolio = rentabilidade.ValorFinanceiroSaidaMoedaAtivo = valorLiquidoVencimento;
                    rentabilidade.ValorFinanceiroRendasMoedaPortfolio = rentabilidade.ValorFinanceiroRendasMoedaAtivo = valorRendaLiquido;
                    rentabilidade.RentabilidadeBrutaDiariaMoedaAtivo = rentabilidade.RentabilidadeBrutaDiariaMoedaPortfolio = valorRentabilidadeBrutaDiaria;
                    rentabilidade.RentabilidadeBrutaAcumMoedaAtivo = rentabilidade.RentabilidadeBrutaAcumMoedaPortfolio = valorRentabilidadeBrutaAcumulada;
                    rentabilidade.RentabilidadeGrossUpDiariaMoedaAtivo = rentabilidade.RentabilidadeGrossUpDiariaMoedaPortfolio = valorRentabilidadeGrossUpDiaria;
                    rentabilidade.RentabilidadeGrossUpAcumMoedaAtivo = rentabilidade.RentabilidadeGrossUpAcumMoedaPortfolio = valorRentabilidadeGrossUpAcumulada;
                    rentabilidade.PatrimonioFinalBrutoMoedaAtivo = rentabilidade.PatrimonioFinalBrutoMoedaPortfolio = 0;
                    rentabilidade.PatrimonioInicialBrutoMoedaAtivo = rentabilidade.PatrimonioInicialBrutoMoedaPortfolio = valorAberturaBruto;
                    //Soma o patrimonio bruto de um título, mesmo que não seja grossup
                    rentabilidade.PatrimonioFinalGrossUpMoedaAtivo = rentabilidade.PatrimonioFinalGrossUpMoedaPortfolio = 0;
                    rentabilidade.PatrimonioInicialGrossUpMoedaPortfolio = rentabilidade.PatrimonioInicialGrossUpMoedaAtivo = 0;

                    //Portfolio
                    rentabilidade.CodigoMoedaPortfolio = descricaoMoedaCarteira;

                    //Converte os valores para a moeda da carteira
                    rentabilidade.ConverteValores(idMoedaAtivo, idMoedaCarteira, ref dicRentConversao);

                    #endregion
                }
            }
            #endregion

            rentabilidadeColl.Save();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="posicaoRendaFixa"></param>
        /// <returns></returns>
        private string RetornaDescricaoCompleta(PosicaoRendaFixa posicaoRendaFixa)
        {
            #region Objetos
            string fieldDescricaoTitulo = "DescricaoTitulo";
            string fieldDescricaoIndice = "DescricaoIndice";
            string fieldDtVencimento = "DataVencimento";
            string fieldTaxa = "Taxa";
            string fieldPercentual = "Percentual";
            StringBuilder strDescricaoCompleta = new StringBuilder();
            #endregion 
            
            strDescricaoCompleta = new StringBuilder();

            if (posicaoRendaFixa.TipoOperacao == (int)TipoOperacaoTitulo.AntecipacaoRecompra)
                strDescricaoCompleta.Append("Recompra Antecipada - ");
            else if (posicaoRendaFixa.TipoOperacao == (int)TipoOperacaoTitulo.AntecipacaoRevenda)
                strDescricaoCompleta.Append("Revenda Antecipada - ");
            else if (posicaoRendaFixa.TipoOperacao == (int)TipoOperacaoTitulo.CompraCasada)
                strDescricaoCompleta.Append("Compra Casada - ");
            else if (posicaoRendaFixa.TipoOperacao == (int)TipoOperacaoTitulo.CompraFinal)
                strDescricaoCompleta.Append("Compra Final - ");
            else if (posicaoRendaFixa.TipoOperacao == (int)TipoOperacaoTitulo.CompraRevenda)
                strDescricaoCompleta.Append("Compra/Revenda - ");
            else if (posicaoRendaFixa.TipoOperacao == (int)TipoOperacaoTitulo.Deposito)
                strDescricaoCompleta.Append("Depósito - ");
            else if (posicaoRendaFixa.TipoOperacao == (int)TipoOperacaoTitulo.Retirada)
                strDescricaoCompleta.Append("Retirada - ");
            else if (posicaoRendaFixa.TipoOperacao == (int)TipoOperacaoTitulo.VendaCasada)
                strDescricaoCompleta.Append("Venda Casada - ");
            else if (posicaoRendaFixa.TipoOperacao == (int)TipoOperacaoTitulo.VendaFinal)
                strDescricaoCompleta.Append("Venda Final - ");
            else if (posicaoRendaFixa.TipoOperacao == (int)TipoOperacaoTitulo.VendaRecompra)
                strDescricaoCompleta.Append("Venda/Recompra - ");
            else if (posicaoRendaFixa.TipoOperacao == (int)TipoOperacaoTitulo.VendaTotal)
                strDescricaoCompleta.Append("Venda Total - ");

            strDescricaoCompleta.Append((string)posicaoRendaFixa.GetColumn(fieldDescricaoTitulo));
            strDescricaoCompleta.Append(" - Vcto: ");
            strDescricaoCompleta.Append(String.Format("{0:dd/MM/yyyy}", (DateTime)posicaoRendaFixa.GetColumn(fieldDtVencimento)));

            #region Complemento
            if (!string.IsNullOrEmpty(posicaoRendaFixa.GetColumn(fieldDescricaoIndice).ToString()))
            {
                strDescricaoCompleta.Append(" -> ");
                if (!string.IsNullOrEmpty(posicaoRendaFixa.GetColumn(fieldPercentual).ToString()))
                    strDescricaoCompleta.Append(posicaoRendaFixa.GetColumn(fieldPercentual).ToString());
                else
                    strDescricaoCompleta.Append("0,00");
                strDescricaoCompleta.Append("% ");

                strDescricaoCompleta.Append(posicaoRendaFixa.GetColumn(fieldDescricaoIndice).ToString());
            }

            if (!string.IsNullOrEmpty(posicaoRendaFixa.GetColumn(fieldTaxa).ToString()))
            {
                string taxa = posicaoRendaFixa.GetColumn(fieldTaxa).ToString();
                if (!taxa.Equals("0,0000") && !taxa.Equals("0.0000"))
                {
                    strDescricaoCompleta.Append(" + ");
                    strDescricaoCompleta.Append(posicaoRendaFixa.GetColumn(fieldTaxa).ToString());
                    strDescricaoCompleta.Append("%");
                }
            }
            #endregion
            
            return strDescricaoCompleta.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="posicaoRendaFixa"></param>
        /// <returns></returns>
        private string RetornaDescricaoCompleta(PosicaoRendaFixaHistorico posicaoRendaFixa)
        {
            #region Objetos
            string fieldDescricaoTitulo = "DescricaoTitulo";
            string fieldDescricaoIndice = "DescricaoIndice";
            string fieldDtVencimento = "DataVencimento";
            string fieldTaxa = "Taxa";
            string fieldPercentual = "Percentual";
            StringBuilder strDescricaoCompleta = new StringBuilder();
            #endregion

            strDescricaoCompleta = new StringBuilder();

            if (posicaoRendaFixa.TipoOperacao == (int)TipoOperacaoTitulo.AntecipacaoRecompra)
                strDescricaoCompleta.Append("Recompra Antecipada - ");
            else if (posicaoRendaFixa.TipoOperacao == (int)TipoOperacaoTitulo.AntecipacaoRevenda)
                strDescricaoCompleta.Append("Revenda Antecipada - ");
            else if (posicaoRendaFixa.TipoOperacao == (int)TipoOperacaoTitulo.CompraCasada)
                strDescricaoCompleta.Append("Compra Casada - ");
            else if (posicaoRendaFixa.TipoOperacao == (int)TipoOperacaoTitulo.CompraFinal)
                strDescricaoCompleta.Append("Compra Final - ");
            else if (posicaoRendaFixa.TipoOperacao == (int)TipoOperacaoTitulo.CompraRevenda)
                strDescricaoCompleta.Append("Compra/Revenda - ");
            else if (posicaoRendaFixa.TipoOperacao == (int)TipoOperacaoTitulo.Deposito)
                strDescricaoCompleta.Append("Depósito - ");
            else if (posicaoRendaFixa.TipoOperacao == (int)TipoOperacaoTitulo.Retirada)
                strDescricaoCompleta.Append("Retirada - ");
            else if (posicaoRendaFixa.TipoOperacao == (int)TipoOperacaoTitulo.VendaCasada)
                strDescricaoCompleta.Append("Venda Casada - ");
            else if (posicaoRendaFixa.TipoOperacao == (int)TipoOperacaoTitulo.VendaFinal)
                strDescricaoCompleta.Append("Venda Final - ");
            else if (posicaoRendaFixa.TipoOperacao == (int)TipoOperacaoTitulo.VendaRecompra)
                strDescricaoCompleta.Append("Venda/Recompra - ");
            else if (posicaoRendaFixa.TipoOperacao == (int)TipoOperacaoTitulo.VendaTotal)
                strDescricaoCompleta.Append("Venda Total - ");

            strDescricaoCompleta.Append((string)posicaoRendaFixa.GetColumn(fieldDescricaoTitulo));
            strDescricaoCompleta.Append(" - Vcto: ");
            strDescricaoCompleta.Append(String.Format("{0:dd/MM/yyyy}", (DateTime)posicaoRendaFixa.GetColumn(fieldDtVencimento)));

            #region Complemento
            if (!string.IsNullOrEmpty(posicaoRendaFixa.GetColumn(fieldDescricaoIndice).ToString()))
            {
                strDescricaoCompleta.Append(" -> ");
                if (!string.IsNullOrEmpty(posicaoRendaFixa.GetColumn(fieldPercentual).ToString()))
                    strDescricaoCompleta.Append(posicaoRendaFixa.GetColumn(fieldPercentual).ToString());
                else
                    strDescricaoCompleta.Append("0,00");
                strDescricaoCompleta.Append("% ");

                strDescricaoCompleta.Append(posicaoRendaFixa.GetColumn(fieldDescricaoIndice).ToString());
            }

            if (!string.IsNullOrEmpty(posicaoRendaFixa.GetColumn(fieldTaxa).ToString()))
            {
                string taxa = posicaoRendaFixa.GetColumn(fieldTaxa).ToString();
                if (!taxa.Equals("0,0000") && !taxa.Equals("0.0000"))
                {
                    strDescricaoCompleta.Append(" + ");
                    strDescricaoCompleta.Append(posicaoRendaFixa.GetColumn(fieldTaxa).ToString());
                    strDescricaoCompleta.Append("%");
                }
            }
            #endregion

            return strDescricaoCompleta.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void AberturaAutomatica(int idCliente, DateTime data)
        {
            PosicaoRendaFixaCollection posicaoRendaFixaCollection = new PosicaoRendaFixaCollection();
            posicaoRendaFixaCollection.BuscaPosicao(idCliente, data);
            CalculoRendaFixa calculoRendaFixa = new CalculoRendaFixa();
            PosicaoRendaFixa.PosicaoAtualizada posicaoAtualizada = new PosicaoRendaFixa.PosicaoAtualizada();
            List<double> lstTaxas = new List<double>();
            Hashtable hsMemoria = new Hashtable();
            TituloRendaFixa titulo;
            PapelRendaFixa papel;
            foreach (PosicaoRendaFixa posicaoRendaFixa in posicaoRendaFixaCollection)
            {
                if (posicaoRendaFixa.TaxaMTM.Value == 0)
                    continue;

                titulo = posicaoRendaFixa.UpToTituloRendaFixaByIdTitulo;
                papel = titulo.UpToPapelRendaFixaByIdPapel;

                lstTaxas.Add((double)posicaoRendaFixa.TaxaMTM);
                posicaoAtualizada = calculoRendaFixa.CalculaPosicao_Fincs(data, papel, titulo, null, lstTaxas, null, posicaoRendaFixa.IdOperacao.Value, (int)TipoPreco.MTM, (int)TipoProcessamento.Abertura, true, ref hsMemoria);

                posicaoRendaFixa.PUMercado = posicaoAtualizada.PuAtualizado;
            }

            posicaoRendaFixaCollection.Save();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="data"></param>
        public void DeletaDetalhePosicaoAfetadaRF(int idCliente, DateTime data)
        {
            DetalhePosicaoAfetadaRFCollection detalhePosicaoAfetadaRFColl = new DetalhePosicaoAfetadaRFCollection();
            detalhePosicaoAfetadaRFColl.Query.Where(detalhePosicaoAfetadaRFColl.Query.IdCliente.Equal(idCliente) & detalhePosicaoAfetadaRFColl.Query.DataOperacao.Equal(data));

            if (detalhePosicaoAfetadaRFColl.Query.Load())
            {
                detalhePosicaoAfetadaRFColl.MarkAllAsDeleted();
                detalhePosicaoAfetadaRFColl.Save();
            }
        }

        public static bool ClienteIsentoIOF(Cliente cliente, ClienteRendaFixa clienteRendaFixa, TituloRendaFixa tituloRendaFixa)
        {
            TituloIsentoIOF tituloIsentoIOF;
            tituloIsentoIOF = (TituloIsentoIOF)tituloRendaFixa.IsentoIOF.Value;

            #region Verifica se o cliente é isento
            //Nivel Titulo/Papel
            if (tituloIsentoIOF == TituloIsentoIOF.Isento)
                return true;

            if (tituloIsentoIOF == TituloIsentoIOF.IsentoPF && cliente.IdTipo.Value == (byte)TipoClienteFixo.ClientePessoaFisica)
                return true;

            //Nivel ClienteRendaFixa
            if (clienteRendaFixa.IsIsentoIOF())
                return true;

            //Nivel Cliente
            if (cliente.IsentoIOF.Equals("S"))
                return true;            
            #endregion

            return false;
        }

        public static bool ClienteIsentoIR(Cliente cliente, DateTime data, ClienteRendaFixa clienteRendaFixa, TituloRendaFixa tituloRendaFixa, ExcecoesTributacaoIR excecao, byte tipoOperacao, out decimal? aliquotaExcecaoTributaria)
        {
            bool isPosicaoCompromisso = tipoOperacao == (byte)TipoOperacaoTitulo.CompraRevenda;
            bool? isIsentoIR = null;
            bool regimeEspecialTributacao = !string.IsNullOrEmpty(cliente.RegimeEspecialTributacao) && cliente.RegimeEspecialTributacao.Equals("S");
            aliquotaExcecaoTributaria = null;

            #region Carrega excecoes
            if (!isPosicaoCompromisso)
            {
                if (excecao == null)
                {
                    ExcecoesTributacaoIRCollection excecoesColl = new ExcecoesTributacaoIRCollection();
                    excecoesColl.RetornaExcessaoTributacaoIR(cliente.UpToPessoaByIdPessoa.Tipo, data, (int)TipoMercado.RendaFixa);
                    excecao = excecoesColl.ObtemPrioridade(cliente.UpToPessoaByIdPessoa.Tipo, (int)TipoMercado.RendaFixa, tituloRendaFixa.IdPapel.Value, tituloRendaFixa.IdTitulo.Value);
                }

                if (excecao.IdExcecoesTributacaoIR.HasValue && excecao.IdExcecoesTributacaoIR.Value > 0)
                {
                    isIsentoIR = (excecao.IsencaoIR.GetValueOrDefault(0) == 1);
                    aliquotaExcecaoTributaria = Convert.ToDecimal((string.IsNullOrEmpty(excecao.AliquotaIR) ? "0" : excecao.AliquotaIR));
                }
            }

            #endregion

            TituloIsentoIR tituloIsentoIR;
            if (tituloRendaFixa.IsentoIR.GetValueOrDefault(0) > 0)
                tituloIsentoIR = (TituloIsentoIR)tituloRendaFixa.IsentoIR.Value;
            else
            {
                PapelRendaFixa papel = tituloRendaFixa.UpToPapelRendaFixaByIdPapel;
                tituloIsentoIR = (TituloIsentoIR)papel.IsentoIR.Value;
            }

            #region Verifica se o cliente é isento
             //Nivel Titulo/Papel
             if (tituloIsentoIR == TituloIsentoIR.Isento)
                 return true;

             if (tituloIsentoIR == TituloIsentoIR.IsentoPF && cliente.IdTipo.Value == (byte)TipoClienteFixo.ClientePessoaFisica)
                 return true;

             //Nivel ClienteRendaFixa
             if (clienteRendaFixa.IsIsentoIR())
                 return true;

             //Nivel Cliente
             if (cliente.IsentoIR.Equals("S"))
                 return true;                

            #endregion

            return false;
        }
    }
}